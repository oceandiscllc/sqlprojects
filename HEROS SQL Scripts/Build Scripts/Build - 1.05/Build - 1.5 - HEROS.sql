-- File Name:	Build - 1.5 - HEROS.sql
-- Build Key:	Build - 1.5 - 2018.10.08 09.33.27

--USE HEROS
GO

-- ==============================================================================================================================
-- Tables:
--		dropdown.Commodity
--		dropdown.EquipmentConsignmentDispositionStatus
--		dropdown.SubCommodity
--
-- Functions:
--		eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
--		procurement.GetConsignmentStatusByConsignmentCode
--		procurement.GetConsignmentStatusByEquipmentConsignmentID
--		procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode
--		procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID
--		procurement.GetProjectNameByConsignmentCode
--		procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID
--
-- Procedures:
--		document.processGeneralFileUploads
--		dropdown.GetCommodityData
--		dropdown.GetEquipmentConsignmentDispositionStatusData
--		dropdown.GetEquipmentOrderData
--		dropdown.GetSubCommodityData
--		eventlog.LogEquipmentConsignmentAction
--		eventlog.LogEquipmentConsignmentDispositionAction
--		procurement.GetEquipmentConsignmentByEquipmentConsignmentID
--		procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID
--		procurement.GetEquipmentConsignmentFilterData
--		procurement.GetEquipmentItemByEquipmentItemID
--		procurement.GetEquipmentOrdersByConsignmentCode
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE HEROS
GO

--Begin table dropdown.Commodity
DECLARE @TableName VARCHAR(250) = 'dropdown.Commodity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Commodity
	(
	CommodityID INT IDENTITY(0,1) NOT NULL,
	CommodityCode VARCHAR(50),
	CommodityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommodityID'
EXEC utility.SetIndexClustered @TableName, 'IX_Commodity', 'DisplayOrder,CommodityName'
GO
--End table dropdown.Commodity

--Begin table dropdown.EquipmentConsignmentDispositionStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.EquipmentConsignmentDispositionStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EquipmentConsignmentDispositionStatus
	(
	EquipmentConsignmentDispositionStatusID INT IDENTITY(0,1) NOT NULL,
	EquipmentConsignmentDispositionStatusCode VARCHAR(50),
	EquipmentConsignmentDispositionStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentConsignmentDispositionStatusID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentConsignmentDispositionStatus', 'DisplayOrder,EquipmentConsignmentDispositionStatusName'
GO
--End table dropdown.EquipmentConsignmentDispositionStatus

--Begin table dropdown.SubCommodity
DECLARE @TableName VARCHAR(250) = 'dropdown.SubCommodity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SubCommodity
	(
	SubCommodityID INT IDENTITY(0,1) NOT NULL,
	CommodityID INT,
	SubCommodityCode VARCHAR(50),
	SubCommodityName VARCHAR(50),
	SubCommodityTypeCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommodityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SubCommodityID'
EXEC utility.SetIndexClustered @TableName, 'IX_SubCommodity', 'DisplayOrder,SubCommodityName'
GO
--End table dropdown.SubCommodity

--Begin table procurement.EquipmentConsignmentDisposition
DECLARE @cSQLText VARCHAR(MAX)

SELECT @cSQLText = 'ALTER TABLE procurement.EquipmentConsignmentDisposition DROP CONSTRAINT ' + DC.Name
FROM sys.default_constraints DC
WHERE DC.Parent_Object_ID = OBJECT_ID('procurement.EquipmentConsignmentDisposition')
	AND DC.Name LIKE '%DateTime%'

IF @cSQLText IS NOT NULL
	EXEC (@cSQLText)
--ENDIF
GO

DECLARE @cSQLText VARCHAR(MAX)

SELECT @cSQLText = 'EXEC sp_RENAME ''' + S.Name + '.' + T.Name + '.' + C.Name + ''', ''EquipmentConsignmentDispositionDate'', ''COLUMN'''
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND S.Name = 'procurement'
		AND T.Name = 'EquipmentConsignmentDisposition'
		AND C.Name = 'EquipmentConsignmentDispositionDateTime'

IF @cSQLText IS NOT NULL
	EXEC (@cSQLText)
--ENDIF
GO

DECLARE @cSQLText VARCHAR(MAX)

SELECT @cSQLText = 'EXEC sp_RENAME ''' + S.Name + '.' + T.Name + '.' + C.Name + ''', ''QuantityReceived'', ''COLUMN'''
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND S.Name = 'procurement'
		AND T.Name = 'EquipmentConsignmentDisposition'
		AND C.Name = 'QuantityRecieved'

IF @cSQLText IS NOT NULL
	EXEC (@cSQLText)
--ENDIF
GO

ALTER TABLE procurement.EquipmentConsignmentDisposition ALTER COLUMN EquipmentConsignmentDispositionDate DATE
GO

EXEC utility.SetDefaultConstraint 'procurement.EquipmentConsignmentDisposition', 'EquipmentConsignmentDispositionDate', 'DATE', 'getDate()'
GO

EXEC utility.AddColumn 'procurement.EquipmentConsignmentDisposition', 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.AddColumn 'procurement.EquipmentConsignmentDisposition', 'CreatePersonID', 'INT', '0'
EXEC utility.AddColumn 'procurement.EquipmentConsignmentDisposition', 'ParentEquipmentConsignmentDispositionID', 'INT', '0'
GO

EXEC utility.SetDefaultConstraint 'procurement.EquipmentConsignmentDisposition', 'CreateDateTime', 'DATETIME', 'getDate()', 1
GO

DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentConsignmentDisposition'

EXEC utility.DropColumn @TableName, 'StatusID'
EXEC utility.AddColumn @TableName, 'EquipmentConsignmentDispositionStatusID', 'INT', '0'
GO
--End table procurement.EquipmentConsignmentDisposition

--Begin table procurement.EquipmentItem
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentItem'

EXEC utility.AddColumn @TableName, 'SubCommodityID', 'INT', '0'
GO
--End table procurement.EquipmentItem
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE HEROS
GO
--Begin function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A function to return document data for a specific entity record
-- ============================================================================

CREATE FUNCTION eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cDocuments VARCHAR(MAX) = '<Documents></Documents>'
	
	IF EXISTS (SELECT 1 FROM document.DocumentEntity T WHERE T.EntityTypeCode = @EntityTypeCode AND T.EntityID = @EntityID)
		BEGIN

		SELECT 
			@cDocuments = COALESCE(@cDocuments, '') + D.Document 
		FROM
			(
			SELECT
				(SELECT T.DocumentID FOR XML RAW('Documents'), ELEMENTS) AS Document
			FROM document.DocumentEntity T 
			WHERE T.EntityTypeCode = @EntityTypeCode
				AND T.EntityID = @EntityID
			) D
	
		IF @cDocuments IS NULL OR LEN(LTRIM(@cDocuments)) = 0
			SET @cDocuments = '<Documents></Documents>'
		--ENDIF

		END
	--ENDIF
		
	RETURN @cDocuments

END
GO
--End function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID

--Begin function procurement.GetConsignmentStatusByConsignmentCode
EXEC utility.DropObject 'procurement.GetConsignmentStatusByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to get a consignment status based on a consignmentcode
-- ==============================================================================

CREATE FUNCTION procurement.GetConsignmentStatusByConsignmentCode
(
@ConsignmentCode VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cConsignmentStatus VARCHAR(50)

	SELECT @cConsignmentStatus = 
		CASE
			WHEN NOT EXISTS 
				(
				SELECT 1 
				FROM procurement.EquipmentConsignmentDisposition ECD 
					JOIN procurement.EquipmentConsignment EC ON EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID 
						AND EC.ConsignmentCode = @ConsignmentCode
				)
			THEN 'Dispatched'
			ELSE
				CASE
					WHEN EXISTS 
						(
						SELECT 1 
						FROM procurement.EquipmentConsignmentDisposition ECD 
							JOIN procurement.EquipmentConsignment EC ON EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID 
								AND EC.ConsignmentCode = @ConsignmentCode
								AND ECD.QuantityShrink + ECD.QuantityWaste + ECD.QuantityReceived < EC.Quantity
						)
					THEN 'Partially Completed'
					ELSE 'Complete'
				END
		END 

	RETURN ISNULL(@cConsignmentStatus, '')

END
GO
--End function procurement.GetConsignmentStatusByConsignmentCode

--Begin function procurement.GetConsignmentStatusByEquipmentConsignmentID
EXEC utility.DropObject 'procurement.GetConsignmentStatusByEquipmentConsignmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.09
-- Description:	A function to get a consignment status based on an equipmentconsignmentid
-- ======================================================================================

CREATE FUNCTION procurement.GetConsignmentStatusByEquipmentConsignmentID
(
@EquipmentConsignmentID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cConsignmentStatus VARCHAR(50) = 'Dispatched'
	DECLARE @nQuantity INT
	DECLARE @nQuantityDisposed INT

	SELECT
		@nQuantity = ISNULL((SELECT EC.Quantity FROM procurement.EquipmentConsignment EC WHERE EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID), 0),
		@nQuantityDisposed = ISNULL(SUM(ECD.QuantityReceived), 0) + ISNULL(SUM(ECD.QuantityShrink), 0) + ISNULL(SUM(ECD.QuantityWaste), 0)
	FROM procurement.EquipmentConsignmentDisposition ECD
	WHERE ECD.EquipmentConsignmentID = @EquipmentConsignmentID
	GROUP BY ECD.EquipmentConsignmentID

	IF @nQuantity > 0 AND @nQuantity - @nQuantityDisposed = 0
		SET @cConsignmentStatus = 'Complete'
	ELSE IF @nQuantityDisposed > 0
		SET @cConsignmentStatus = 'Partially Completed'
	--ENDIF

	RETURN ISNULL(@cConsignmentStatus, '')

END
GO
--End function procurement.GetConsignmentStatusByEquipmentConsignmentID

--Begin function procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode
EXEC utility.DropObject 'procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to return a table with aggregated quantities from the procurement.EquipmentConsignmentDisposition table
-- ===============================================================================================================================

CREATE FUNCTION procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode
(
@ConsignmentCode VARCHAR(50)
)

RETURNS @tTable TABLE 
	(
	EquipmentConsignmentID INT NOT NULL PRIMARY KEY, 
	QuantityReceived INT NOT NULL DEFAULT 0, 
	QuantityShrink INT NOT NULL DEFAULT 0, 
	QuantityWaste INT NOT NULL DEFAULT 0, 
	QuantityDisposed INT NOT NULL DEFAULT 0
	)  

AS
BEGIN

	INSERT INTO @tTable
		(EquipmentConsignmentID,QuantityReceived,QuantityShrink,QuantityWaste,QuantityDisposed)
	SELECT
		ECD.EquipmentConsignmentID,
		SUM(ECD.QuantityReceived) AS QuantityReceived,
		SUM(ECD.QuantityShrink) AS QuantityShrink,
		SUM(ECD.QuantityWaste) AS QuantityWaste,
		SUM(ECD.QuantityReceived) + SUM(ECD.QuantityShrink) + SUM(ECD.QuantityWaste) AS QuantityDisposed
	FROM procurement.EquipmentConsignmentDisposition ECD
	WHERE @ConsignmentCode IS NULL
		OR EXISTS
			(
			SELECT 
				EC.EquipmentConsignmentID
			FROM procurement.EquipmentConsignment EC
			WHERE EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID
				AND EC.ConsignmentCode = @ConsignmentCode
			)
	GROUP BY ECD.EquipmentConsignmentID
	ORDER BY ECD.EquipmentConsignmentID

	RETURN

END
GO
--End function procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode

--Begin function procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID
EXEC utility.DropObject 'procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to return a table with aggregated quantities from the procurement.EquipmentConsignmentDisposition table
-- ===============================================================================================================================

CREATE FUNCTION procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID
(
@EquipmentConsignmentID INT
)

RETURNS @tTable TABLE 
	(
	EquipmentConsignmentID INT NOT NULL PRIMARY KEY, 
	QuantityReceived INT NOT NULL DEFAULT 0, 
	QuantityShrink INT NOT NULL DEFAULT 0, 
	QuantityWaste INT NOT NULL DEFAULT 0, 
	QuantityDisposed INT NOT NULL DEFAULT 0
	)  

AS
BEGIN

	INSERT INTO @tTable
		(EquipmentConsignmentID,QuantityReceived,QuantityShrink,QuantityWaste,QuantityDisposed)
	SELECT
		ECD.EquipmentConsignmentID,
		SUM(ECD.QuantityReceived) AS QuantityReceived,
		SUM(ECD.QuantityShrink) AS QuantityShrink,
		SUM(ECD.QuantityWaste) AS QuantityWaste,
		SUM(ECD.QuantityReceived) + SUM(ECD.QuantityShrink) + SUM(ECD.QuantityWaste) AS QuantityDisposed
	FROM procurement.EquipmentConsignmentDisposition ECD
	WHERE ECD.EquipmentConsignmentID = @EquipmentConsignmentID
	GROUP BY ECD.EquipmentConsignmentID
	ORDER BY ECD.EquipmentConsignmentID

	RETURN

END
GO
--End function procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID

--Begin function procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID
EXEC utility.DropObject 'procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.10
-- Description:	A function to get a recipient name from a recipiententitytypecode and a recipiententityid
-- ======================================================================================================

CREATE FUNCTION procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID
(
@RecipientEntityTypeCode VARCHAR(50),
@RecipientEntityID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cRecipientName VARCHAR(100)

	SELECT @cRecipientName = 
		CASE 
			WHEN @RecipientEntityTypeCode = 'Asset'
			THEN (SELECT A.AssetName from asset.Asset A WHERE A.AssetID = @RecipientEntityID)
			WHEN @RecipientEntityTypeCode = 'Force'
			THEN (SELECT F.ForceName from force.Force F WHERE F.ForceID = @RecipientEntityID)
			WHEN @RecipientEntityTypeCode = 'Person'
			THEN person.FormatPersonNameByPersonID(@RecipientEntityID, 'LastFirst')
			ELSE NULL
		END

	RETURN ISNULL(@cRecipientName, '')

END
GO
--Begin function procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID

--Begin function procurement.GetProjectNameByConsignmentCode
EXEC utility.DropObject 'procurement.GetProjectNameByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to get a project name based on a consignmentcode
-- ========================================================================

CREATE FUNCTION procurement.GetProjectNameByConsignmentCode
(
@ConsignmentCode VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cProjectName VARCHAR(50)

	SELECT TOP 1 @cProjectName = P.ProjectName 
	FROM procurement.EquipmentOrder EO 
		JOIN dropdown.Project P ON P.ProjectID = EO.ProjectID 
		JOIN procurement.EquipmentConsignment EC ON EC.EquipmentOrderID = EO.EquipmentOrderID 
			AND EC.ConsignmentCode = @ConsignmentCode 
	ORDER BY P.ProjectName

	RETURN ISNULL(@cProjectName, '')

END
GO
--End function procurement.GetProjectNameByConsignmentCode


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE HEROS
GO

--Begin procedure document.processGeneralFileUploads
EXEC Utility.DropObject 'document.processGeneralFileUploads'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.16
-- Description:	A stored procedure to manage document and document entity data
-- ===========================================================================
CREATE PROCEDURE document.processGeneralFileUploads

@DocumentData VARBINARY(MAX),
@ContentType VARCHAR(50), 
@ContentSubtype VARCHAR(50), 
@CreatePersonID INT = 0, 
@DocumentDescription VARCHAR(1000) = NULL, 
@DocumentTitle VARCHAR(250) = NULL, 
@Extension VARCHAR(10) = NULL,

@DocumentEntityCode VARCHAR(50) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID VARCHAR(MAX) = NULL,

@AllowMultipleDocuments BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nDocumentID INT
	DECLARE @tEntity TABLE (EntityID INT NOT NULL PRIMARY KEY)
	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)

	IF @EntityID IS NOT NULL
		BEGIN

		INSERT INTO @tEntity (EntityID) SELECT CAST(LTT.ListItem AS INT) FROM core.ListToTable(@EntityID, ',') LTT

		INSERT INTO @tOutput
			(DocumentID)
		SELECT 
			D.DocumentID
		FROM document.Document D
		WHERE D.DocumentData = @DocumentData

		IF NOT EXISTS (SELECT 1 FROM @tOutput O)
			BEGIN

			INSERT INTO document.Document
				(ContentType, ContentSubtype, CreatePersonID, DocumentData, DocumentDescription, DocumentGUID, DocumentTitle, Extension)
			OUTPUT INSERTED.DocumentID INTO @tOutput
			VALUES
				(
				@ContentType,
				@ContentSubtype,
				@CreatePersonID,
				@DocumentData,
				@DocumentDescription,
				newID(),
				@DocumentTitle,
				@Extension
				)

			END
		--ENDIF

		SELECT @nDocumentID = O.DocumentID FROM @tOutput O

		INSERT INTO document.DocumentEntity
			(DocumentID, DocumentEntityCode, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			@nDocumentID,
			@DocumentEntityCode,
			@EntityTypeCode,
			@EntityTypeSubCode,
			E.EntityID
		FROM @tEntity E
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM document.DocumentEntity DE
			WHERE DE.DocumentID = @nDocumentID
				AND (@DocumentEntityCode IS NULL OR DE.DocumentEntityCode = @DocumentEntityCode)
				AND DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND DE.EntityID = E.EntityID
			)

		DELETE DE
		FROM document.DocumentEntity DE
		WHERE DE.DocumentID = 0
			AND DATEDIFF(HOUR, DE.CreateDateTime, getDate()) > 6

		IF @AllowMultipleDocuments = 0
			BEGIN
					
			DELETE DE
			FROM document.DocumentEntity DE
			WHERE DE.EntityTypeCode = @EntityTypeCode
				AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
				AND @EntityID > 0
				AND DE.DocumentID = @nDocumentID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tEntity E
					WHERE E.EntityID = DE.EntityID
					)

			DELETE D
			FROM document.Document D
				JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
					AND DE.EntityTypeCode = @EntityTypeCode
					AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
					AND @EntityID > 0
					AND DE.DocumentID = @nDocumentID
					AND NOT EXISTS
						(
						SELECT 1
						FROM @tEntity E
						WHERE E.EntityID = DE.EntityID
						)

			END
		--ENDIF

		END
	--ENDIF

END
GO	
--End procedure document.processGeneralFileUploads

--Begin procedure dropdown.GetCommodityData
EXEC Utility.DropObject 'dropdown.GetCommodityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.14
-- Description:	A stored procedure to return data from the dropdown.Commodity table
-- ================================================================================
CREATE PROCEDURE dropdown.GetCommodityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommodityID,
		T.CommodityCode,
		T.CommodityName
	FROM dropdown.Commodity T
	WHERE (T.CommodityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommodityName, T.CommodityID

END
GO
--End procedure dropdown.GetCommodityData

--Begin procedure dropdown.GetSubCommodityData
EXEC Utility.DropObject 'dropdown.GetSubCommodityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.14
-- Description:	A stored procedure to return data from the dropdown.SubCommodity table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSubCommodityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommodityCode,
		C.CommodityName,
		T.SubCommodityID,
		T.SubCommodityCode,
		T.SubCommodityName
	FROM dropdown.SubCommodity T
		JOIN dropdown.Commodity C ON C.CommodityID = T.CommodityID
			AND ((T.CommodityID > 0 AND T.SubCommodityID > 0) OR @IncludeZero = 1)
			AND C.IsActive = 1 
			AND T.IsActive = 1
	ORDER BY C.DisplayOrder, C.CommodityName, C.CommodityID, T.DisplayOrder, T.SubCommodityName, T.SubCommodityID

END
GO
--End procedure dropdown.GetSubCommodityData

--Begin procedure dropdown.GetEquipmentConsignmentDispositionStatusData
EXEC Utility.DropObject 'dropdown.GetEquipmentConsignmentDispositionStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.07
-- Description:	A stored procedure to return data from the dropdown.EquipmentConsignmentDispositionStatus table
-- ============================================================================================================
CREATE PROCEDURE dropdown.GetEquipmentConsignmentDispositionStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentConsignmentDispositionStatusID,
		T.EquipmentConsignmentDispositionStatusCode,
		T.EquipmentConsignmentDispositionStatusName
	FROM dropdown.EquipmentConsignmentDispositionStatus T
	WHERE (T.EquipmentConsignmentDispositionStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EquipmentConsignmentDispositionStatusName, T.EquipmentConsignmentDispositionStatusID

END
GO
--End procedure dropdown.GetEquipmentConsignmentDispositionStatusData

--Begin procedure dropdown.GetEquipmentOrderData
EXEC Utility.DropObject 'dropdown.GetEquipmentOrderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.31
-- Description:	A stored procedure to return data from the procurement.EquipmentOrder table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetEquipmentOrderData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentOrderID,
		T.EquipmentOrderName
	FROM procurement.EquipmentOrder T
	ORDER BY T.EquipmentOrderName, T.EquipmentOrderID

END
GO
--End procedure dropdown.GetEquipmentOrderData

--Begin procedure eventlog.LogEquipmentConsignmentAction
EXEC utility.DropObject 'eventlog.LogEquipmentConsignmentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2018.08.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentConsignmentAction

@EntityID INT = 0,
@EventCode VARCHAR(50) = '',
@PersonID INT = 0,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EquipmentConsignment'

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, T.EquipmentConsignmentID) AS XML)
			FOR XML RAW('EquipmentConsignment'), ELEMENTS
			)
		FROM procurement.EquipmentConsignment T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments
		FROM procurement.EquipmentConsignment T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentConsignmentAction

--Begin procedure eventlog.LogEquipmentConsignmentDispositionAction
EXEC utility.DropObject 'eventlog.LogEquipmentConsignmentDispositionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2018.08.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentConsignmentDispositionAction

@EntityID INT = 0,
@EventCode VARCHAR(50) = '',
@PersonID INT = 0,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EquipmentConsignmentDisposition'

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments,
			(
			SELECT T.*,
			CAST(eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID(@cEntityTypeCode, T.EquipmentConsignmentDispositionID) AS XML)
			FOR XML RAW('EquipmentConsignmentDisposition'), ELEMENTS
			)
		FROM procurement.EquipmentConsignmentDisposition T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentDispositionID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			CAST(LTT.ListItem AS INT),
			@Comments
		FROM procurement.EquipmentConsignmentDisposition T
			JOIN core.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.EquipmentConsignmentDispositionID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentConsignmentDispositionAction

--Begin procedure procurement.GetEquipmentConsignmentByEquipmentConsignmentID
EXEC Utility.DropObject 'procurement.GetEquipmentConsignmentByEquipmentConsignmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.09
-- Description:	A stored procedure to return data from the procurement.EquipmentConsignment table
-- ==============================================================================================
CREATE PROCEDURE procurement.GetEquipmentConsignmentByEquipmentConsignmentID

@EquipmentConsignmentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentConsignmentID,
		EC.EquipmentItemID,
		EC.Quantity,
		EO.EquipmentOrderName,
		EOCI.ItemDescription,
		ISNULL(EC.Quantity, 0) - ISNULL(ECD.QuantityDisposed, 0) AS QuantityOutstanding,
		FORMAT(ROUND(((EOCI.UnitCost * EC.Quantity) * ((100 + EOCI.VAT)/100)), 2), 'C', 'en-GB') AS TotalCostWithVAT
	FROM procurement.EquipmentConsignment EC
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EC.EquipmentOrderID
		JOIN procurement.EquipmentOrderCatalogItem EOCI ON EOCI.EquipmentOrderID = EC.EquipmentOrderID
			AND EOCI.EquipmentItemID = EC.EquipmentItemID
			AND EC.EquipmentConsignmentID = @EquipmentConsignmentID
		LEFT JOIN procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID(@EquipmentConsignmentID) ECD ON ECD.EquipmentConsignmentID = EC.EquipmentConsignmentID

END
GO
--End procedure procurement.GetEquipmentConsignmentByEquipmentConsignmentID

--Begin procedure procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID
EXEC Utility.DropObject 'procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.11
-- Description:	A stored procedure to return data from the procurement.EquipmentConsignmentDisposition table
-- =========================================================================================================
CREATE PROCEDURE procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID

@EquipmentConsignmentDispositionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentConsignmentID,
		EC.EquipmentItemID,
		ECD.EquipmentConsignmentDispositionID,
		ECD.EquipmentConsignmentDispositionStatusID,

		CASE
			WHEN ECD.QuantityReceived > 0
			THEN ECD.QuantityReceived
			WHEN ECD.QuantityShrink > 0
			THEN ECD.QuantityShrink
			WHEN ECD.QuantityWaste > 0
			THEN ECD.QuantityWaste
			ELSE 0
		END AS Quantity,

		ECD.RecipientEntityTypeCode,
		ECD.RecipientEntityID,
		EO.EquipmentOrderName,
		EOCI.ItemDescription
	FROM procurement.EquipmentConsignmentDisposition ECD
		JOIN procurement.EquipmentConsignment EC ON EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EC.EquipmentOrderID
		JOIN procurement.EquipmentOrderCatalogItem EOCI ON EOCI.EquipmentOrderID = EC.EquipmentOrderID
			AND EOCI.EquipmentItemID = EC.EquipmentItemID
			AND ECD.EquipmentConsignmentDispositionID = @EquipmentConsignmentDispositionID

END
GO
--End procedure procurement.GetEquipmentConsignmentDispositionByEquipmentConsignmentDispositionID

--Begin procedure procurement.GetEquipmentConsignmentFilterData
EXEC Utility.DropObject 'procurement.GetEquipmentConsignmentFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.09
-- Description:	A stored procedure to return data from the procurement.EquipmentConsignment table
-- ==============================================================================================
CREATE PROCEDURE procurement.GetEquipmentConsignmentFilterData

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT EC.ConsignmentCode
	FROM procurement.EquipmentConsignment EC
	ORDER BY 1

	SELECT DISTINCT 
		EC.EquipmentItemID,
		ISNULL(ISNULL(EOCI.ItemDescription, EI.ItemDescription), 'None Listed') AS ItemDescription
	FROM procurement.EquipmentConsignment EC
		JOIN procurement.EquipmentOrderCatalogItem EOCI ON EOCI.EquipmentOrderID = EC.EquipmentOrderID
			AND EOCI.EquipmentItemID = EC.EquipmentItemID
		JOIN procurement.EquipmentItem EI ON EI.EquipmentItemID = EC.EquipmentItemID
	ORDER BY 2, 1

	SELECT DISTINCT 
		EC.EquipmentOrderID,
		EO.EquipmentOrderName
	FROM procurement.EquipmentConsignment EC
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EC.EquipmentOrderID
	ORDER BY 2, 1

END
GO
--End procedure procurement.GetEquipmentConsignmentFilterData

--Begin procedure procurement.GetEquipmentItemByEquipmentItemID
EXEC Utility.DropObject 'procurement.GetEquipmentItemByEquipmentItemID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE procurement.GetEquipmentItemByEquipmentItemID

@EquipmentItemID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EI.EquipmentItemID,
		EI.ItemName,
		EI.ItemDescription,
		EI.BudgetValue,
		EI.IsActive,
		IIF(EI.IsActive = 1, 'Yes ', 'No ') AS IsActiveFormatted,
		EI.IsPerishable,
		IIF(EI.IsPerishable = 1, 'Yes ', 'No ') AS IsPerishableFormatted,
		EI.IsColdChainRequired,
		IIF(EI.IsColdChainRequired = 1, 'Yes ', 'No ') AS IsColdChainRequiredFormatted,
		EI.IsReturnable,
		IIF(EI.IsReturnable = 1, 'Yes ', 'No ') AS IsReturnableFormatted,
		EI.IsControlledItem,
		IIF(EI.IsControlledItem = 1, 'Yes ', 'No ') AS IsControlledItemFormatted,
		EI.IsLotControlled,
		IIF(EI.IsLotControlled = 1, 'Yes ', 'No ') AS IsLotControlledFormatted,
		EI.CurrencyID,
		EI.MaximumShelfLife,
		EI.MinimumQuantity,
		EI.Model,
		EI.Size,
		EI.Length,
		EI.Height,
		EI.Width,
		EI.Weight,
		EI.ProjectID,
		dropdown.GetProjectNameByProjectID(EI.ProjectID) AS ProjectName,
		EIT.EquipmentItemTypeID,
		EIT.EquipmentItemTypeName,
		GDU.GovernmentDepartmentUnitID,
		GDU.GovernmentDepartmentUnitName,
		SC.SubCommodityID,
		SC.SubCommodityName
	FROM procurement.EquipmentItem EI
		JOIN dropdown.GovernmentDepartmentUnit GDU ON GDU.GovernmentDepartmentUnitID = EI.GovernmentDepartmentUnitID
		JOIN dropdown.EquipmentItemType EIT ON EIT.EquipmentItemTypeID = EI.EquipmentItemTypeID
		JOIN dropdown.SubCommodity SC ON SC.SubCommodityID = EI.SubCommodityID
			AND EI.EquipmentItemID = @EquipmentItemID
		
END
GO
--End procedure procurement.GetEquipmentItemByEquipmentItemID

--Begin procedure procurement.GetEquipmentOrdersByConsignmentCode
EXEC Utility.DropObject 'procurement.GetEquipmentOrdersByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A stored procedure to return data from the procurement.EquipmentOrder table
-- ========================================================================================
CREATE PROCEDURE procurement.GetEquipmentOrdersByConsignmentCode

@ConsignmentCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EO.EquipmentOrderID,
		EO.EquipmentOrderName,
		EOT.EquipmentOrderTypeName,
		procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID(EO.RecipientEntityTypeCode, EO.RecipientEntityID) AS RecipientName,
		C.CountryName AS RecipientCountryName,
		P.ProjectCode,
		EO.TaskOrderNumber,
		EO.ClientPONumber,
		person.FormatPersonNameByPersonID(EO.OrderedByPersonID, 'LastFirst') AS OrderedByPersonNameFormatted,
		core.FormatDate(EO.OrderDate) AS OrderDateFormatted,
		core.FormatDate(EO.OrderCompletionDate) AS OrderCompletionDateFormatted
	FROM procurement.EquipmentOrder EO
		JOIN dropdown.EquipmentOrderType EOT ON EOT.EquipmentOrderTypeID = EO.EquipmentOrderTypeID
		JOIN dropdown.Country C ON C.ISOCountryCode2 = EO.RecipientISOCountryCode2
		JOIN dropdown.Project P ON P.ProjectID = EO.ProjectID
			AND EXISTS
				(
				SELECT 1
				FROM procurement.EquipmentConsignment EC
				WHERE EC.ConsignmentCode = @ConsignmentCode
					AND EC.EquipmentOrderID = EO.EquipmentOrderID
				)
	ORDER BY EO.EquipmentOrderName, EO.EquipmentOrderID

END
GO
--End procedure procurement.GetEquipmentOrdersByConsignmentCode

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE HEROS
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EquipmentOrderList',
	@NewMenuItemCode = 'EquipmentConsignmentList',
	@NewMenuItemLink = '/equipmentconsignment/list',
	@NewMenuItemText = 'Consignments',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentConsignment.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EquipmentConsignmentList',
	@NewMenuItemCode = 'EquipmentConsignmentListDistributedItems',
	@NewMenuItemLink = '/equipmentconsignment/listdistributeditems',
	@NewMenuItemText = 'Distributed Items',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentConsignment.ListDistributedItems'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EquipmentConsignmentListDistributedItems',
	@NewMenuItemCode = 'EquipmentConsignmentListDispatchedItems',
	@NewMenuItemLink = '/equipmentconsignment/listdispatcheditems',
	@NewMenuItemText = 'Dispatched Items',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentConsignment.ListDispatchedItems'
GO

--Begin table dropdown.Commodity
TRUNCATE TABLE dropdown.Commodity
GO

EXEC utility.InsertIdentityValue 'dropdown.Commodity', 'CommodityID', 0
GO

INSERT INTO dropdown.Commodity
	(CommodityName, CommodityCode)
VALUES
	('Accommodation & Subsistence', '555-003-036'),
	('Bags & Cases', '555-003-001'),
	('Blankets', '555-003-002'),
	('Charges', '555-003-037'),
	('Cleaning & Waste Management - Goods', '555-003-003'),
	('Cleaning & Waste Management - Service', '555-003-038'),
	('Clothing', '555-003-004'),
	('Cold Chain', '555-003-005'),
	('Communications - Goods', '555-003-006'),
	('Communications - Service', '555-003-039'),
	('Consumables', '555-003-007'),
	('Contingency', '555-003-040'),
	('Cooking Equipment', '555-003-008'),
	('Deployment Kit', '555-003-009'),
	('Discounts', '555-003-041'),
	('Disposal', '555-003-042'),
	('Environment Control', '555-003-010'),
	('Expenses', '555-003-043'),
	('Food', '555-003-011'),
	('Fuel', '555-003-012'),
	('Infrastructure', '555-003-013'),
	('Innovation', '555-003-044'),
	('Inspection & Testing', '555-003-045'),
	('IT Equipment', '555-003-014'),
	('Lighting', '555-003-015'),
	('Local office', '555-003-046'),
	('Logistics Equipment', '555-003-016'),
	('Logistics Services', '555-003-047'),
	('Medical', '555-003-017'),
	('Miscellaneous - Goods', '555-003-018'),
	('Miscellaneous - Service', '555-003-048'),
	('NFI Household', '555-003-019'),
	('NFI Shelter', '555-003-020'),
	('NFI Solar', '555-003-021'),
	('NFI WASH', '555-003-022'),
	('Operational Travel', '555-003-049'),
	('Power', '555-003-023'),
	('PPE', '555-003-024'),
	('Safety & Security', '555-003-025'),
	('Services', '555-003-050'),
	('Shelter', '555-003-026'),
	('Staffing', '555-003-051'),
	('Stationery', '555-003-027'),
	('Storage', '555-003-028'),
	('Tools', '555-003-029'),
	('Training', '555-003-052'),
	('Vector Control', '555-003-030'),
	('Vehicles - Goods', '555-003-031'),
	('Vehicles - Service', '555-003-053'),
	('Warehousing', '555-003-054'),
	('WASH', '555-003-032'),
	('Water', '555-003-033')
GO
--End table dropdown.Commodity

--Begin table dropdown.SubCommodity
TRUNCATE TABLE dropdown.SubCommodity
GO

EXEC utility.InsertIdentityValue 'dropdown.SubCommodity', 'SubCommodityID', 0
GO

INSERT INTO dropdown.SubCommodity
	(SubCommodityName, SubCommodityTypeCode)
VALUES
	('2nd passport', 'Service'),
	('Air-Conditioners', 'Goods'),
	('Airport taxes', 'Service'),
	('Armoured vehicles', 'Goods'),
	('Audio & Visual Equipment', 'Goods'),
	('Automotive', 'Service'),
	('Bank Charges', 'Service'),
	('Barrier Equipment', 'Goods'),
	('Batteries', 'Goods'),
	('Bicycles', 'Goods'),
	('Biological Control', 'Goods'),
	('Blankets', 'Goods'),
	('Body Armour', 'Goods'),
	('Boilers/Kettles', 'Goods'),
	('Bottled Water', 'Goods'),
	('Boxes', 'Goods'),
	('Cabling', 'Goods'),
	('Car Hire', 'Service'),
	('Cargo Screening', 'Service'),
	('Chairs', 'Goods'),
	('Chargeable expenses', 'Service'),
	('Chemical Control', 'Goods'),
	('Cleaning & Waste Management', 'Service'),
	('Cleaning Consumables', 'Goods'),
	('Cleaning Equipment', 'Goods'),
	('Climbing Safety / Fall Arrest Equipment', 'Goods'),
	('Clothing Cleaning Equipment', 'Goods'),
	('Cold-Chain Equipment', 'Goods'),
	('Collapsible Jerrycans', 'Goods'),
	('Computer Equipment', 'Goods'),
	('Construction materials', 'Goods'),
	('Consumables', 'Goods'),
	('Containers', 'Goods'),
	('Contingency', 'Service'),
	('Copying and Printing', 'Goods'),
	('Course Fees', 'Service'),
	('Customs', 'Service'),
	('Cutlery/Crockery/Drinking', 'Goods'),
	('Demurrage', 'Service'),
	('Deployment', 'Goods'),
	('Diesel', 'Goods'),
	('Discount', 'Service'),
	('Disposal', 'Goods'),
	('Disposal Services', 'Service'),
	('Distribution', 'Goods'),
	('Distribution Boxes', 'Goods'),
	('Domestic Courier', 'Service'),
	('Domestic Haulage', 'Service'),
	('Duty', 'Service'),
	('Electrical Equipment', 'Goods'),
	('Equipment', 'Goods'),
	('Equipment Protective Cases', 'Goods'),
	('Exchange rate variations', 'Service'),
	('Expenses', 'Service'),
	('Export', 'Service'),
	('Fire & Gas Safety equipment', 'Goods'),
	('First Aid/Trauma Kits', 'Goods'),
	('Flatpack Building', 'Goods'),
	('Flights', 'Service'),
	('Flooring', 'Goods'),
	('Food and drink equipment', 'Goods'),
	('Food Baskets', 'Goods'),
	('Footwear', 'Goods'),
	('Fuel Storage', 'Goods'),
	('Furniture', 'Goods'),
	('Gas', 'Goods'),
	('Generators', 'Goods'),
	('GPS Equipment', 'Goods'),
	('Hand Tools', 'Goods'),
	('Handheld lights', 'Goods'),
	('Handling', 'Service'),
	('Hearing Protection', 'Goods'),
	('Heaters', 'Goods'),
	('High Visability Clothing', 'Goods'),
	('Home Textiles', 'Goods'),
	('Hotels', 'Service'),
	('Household tents', 'Goods'),
	('Hygiene/dignity Kit Goods', 'Goods'),
	('Import', 'Service'),
	('In-Country Travel', 'Service'),
	('Insect Repellant', 'Goods'),
	('Internal Lights', 'Goods'),
	('International Courier', 'Service'),
	('International Haulage', 'Service'),
	('International Postage', 'Service'),
	('Internet Connection', 'Service'),
	('Kit-Building', 'Service'),
	('Kitchen Appliances', 'Goods'),
	('Kitchen sets', 'Goods'),
	('Landline Connection', 'Service'),
	('Lanterns', 'Goods'),
	('Locks / Safes / Keys', 'Goods'),
	('Manual Handling Equipment', 'Goods'),
	('Maritime Safety Equipment', 'Goods'),
	('Mechanical Handling Equipment', 'Goods'),
	('Miscellaneous', 'Service'),
	('Miscellaneous Goods', 'Goods'),
	('Miscellaneous Service', 'Service'),
	('Mobile phone', 'Goods'),
	('Modification', 'Service'),
	('Mosquito Nets', 'Goods'),
	('Motor Vehicles', 'Goods'),
	('MSU', 'Goods'),
	('Multipurpose/Infrastructure Tents', 'Goods'),
	('Multitools', 'Goods'),
	('Non-Uniform Clothing', 'Goods'),
	('Office Consumables', 'Goods'),
	('Office Stationery', 'Goods'),
	('Oils and Lubricants', 'Goods'),
	('Other consumables', 'Goods'),
	('Other Protective Clothing', 'Goods'),
	('Packaging', 'Goods'),
	('Palletisation', 'Service'),
	('Pallets & Crates', 'Goods'),
	('Patient Blankets', 'Goods'),
	('Payroll allowances', 'Service'),
	('Permits', 'Service'),
	('Personal Safety Equipment', 'Goods'),
	('Petrol', 'Goods'),
	('Pharmaceuticals', 'Goods'),
	('Plastic Buckets', 'Goods'),
	('Plastic sheeting', 'Goods'),
	('Plugs/Adaptors', 'Goods'),
	('Plumbing', 'Goods'),
	('Pole & Peg Sets', 'Goods'),
	('Power Supply', 'Goods'),
	('Power Tools', 'Goods'),
	('PPE', 'Goods'),
	('Printer Equipment', 'Goods'),
	('Product & Loading Inspection', 'Service'),
	('Product Recertification', 'Service'),
	('Product Testing', 'Service'),
	('Protective Eyewear', 'Goods'),
	('Protective Footwear', 'Goods'),
	('Protective Gloves', 'Goods'),
	('Protective Headwear', 'Goods'),
	('Racking', 'Goods'),
	('Ration Packs', 'Goods'),
	('Recruitment, Briefing and Debriefing', 'Service'),
	('Rent', 'Service'),
	('Repair and maintenance', 'Service'),
	('Repairs & Maintenance', 'Service'),
	('Research and development', 'Service'),
	('Rigid Jerrycans', 'Goods'),
	('Ropes and Lashings', 'Goods'),
	('Rucksacks', 'Goods'),
	('Running costs', 'Service'),
	('Sanitation & Toilet', 'Goods'),
	('Satcomms Equipment', 'Goods'),
	('Scheduled Air Freight', 'Service'),
	('Scheduled Sea Freight', 'Service'),
	('Search Equipment', 'Goods'),
	('Shelter Kits', 'Goods'),
	('Shower & Washing Equipment', 'Goods'),
	('Signage & Labels', 'Goods'),
	('Sim cards', 'Goods'),
	('Site Lights', 'Goods'),
	('Sleeping Equipment', 'Goods'),
	('Sleeping mats', 'Goods'),
	('Stoves', 'Goods'),
	('Subscription charges', 'Service'),
	('Subsistence', 'Service'),
	('Systems', 'Goods'),
	('Tables', 'Goods'),
	('Tarpaulins', 'Goods'),
	('Tents/Accommodation', 'Goods'),
	('Therapeutic Foods', 'Goods'),
	('Toilet & Sanitation Equipment', 'Goods'),
	('Torches/Headtorches', 'Goods'),
	('Trailers', 'Goods'),
	('Traps & Physical Control', 'Goods'),
	('UK / Country of Domicile Travel', 'Service'),
	('Uniform', 'Goods'),
	('Utensils', 'Goods'),
	('Utilities', 'Service'),
	('VAT', 'Service'),
	('Vehicle Running Costs', 'Service'),
	('Vehicle Spares', 'Goods'),
	('Vehicle Tax & Insurance', 'Service'),
	('Vehicle Tools', 'Goods'),
	('VHF', 'Goods'),
	('Visa Fees', 'Service'),
	('Warehouse consumables', 'Goods'),
	('Waste Management', 'Service'),
	('Water filtration', 'Goods'),
	('Water storage', 'Goods'),
	('Water Supply', 'Goods'),
	('Water Tanks', 'Goods'),
	('Water treatment', 'Goods'),
	('Weatherproof Clothing', 'Goods')
GO

UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Accommodation & Subsistence') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hotels'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Accommodation & Subsistence') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Subsistence'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Bags & Cases') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Deployment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Bags & Cases') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment Protective Cases'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Bags & Cases') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rucksacks'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Blankets') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Patient Blankets'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Bank Charges'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Customs'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Duty'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Exchange rate variations'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Export'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Import'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Charges') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VAT'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cleaning & Waste Management - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cleaning & Waste Management - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cleaning & Waste Management - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Waste Management'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Clothing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Clothing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Clothing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Clothing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cold Chain') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cold-Chain Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Communications - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internet Connection'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Communications - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Landline Connection'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Communications - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mobile phone'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Communications - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Communications - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sim cards'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Communications - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Subscription charges'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Communications - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Consumables') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other consumables'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Consumables') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Warehouse consumables'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Contingency') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Contingency'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cooking Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boilers/Kettles'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cooking Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cooking Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen Appliances'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cooking Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Cooking Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Deployment Kit') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Food and drink equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Deployment Kit') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'GPS Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Deployment Kit') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multitools'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Deployment Kit') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Deployment Kit') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tents/Accommodation'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Deployment Kit') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Toilet & Sanitation Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Discounts') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Discount'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Disposal') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal Services'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Environment Control') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Environment Control') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Heaters'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Expenses') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chargeable expenses'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Food ') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Therapeutic Foods'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Food') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Food Baskets'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Food') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Fuel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Diesel'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Fuel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fuel Storage'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Fuel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Gas'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Fuel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Petrol'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Infrastructure') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Infrastructure') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Infrastructure') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Infrastructure') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Infrastructure') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tables'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Infrastructure') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tarpaulins'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Innovation') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Research and development'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Inspection & Testing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Product & Loading Inspection'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Inspection & Testing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Product Recertification'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Inspection & Testing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Product Testing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'IT Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'IT Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'IT Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'IT Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Systems'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Lighting') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Lighting') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Site Lights'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Lighting') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Local office') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Running costs'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Manual Handling Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Equipment') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pallets & Crates'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cargo Screening'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Demurrage'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Domestic Courier'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Domestic Haulage'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Handling'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'International Courier'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'International Haulage'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'International Postage'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kit-Building'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Palletisation'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Permits'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Scheduled Air Freight'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Logistics Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Scheduled Sea Freight'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Medical') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Medical') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Medical') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Medical') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'First Aid/Trauma Kits'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Medical') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Medical') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Miscellaneous - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Miscellaneous - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Service'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Household') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Blankets'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Household') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen sets'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Household') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping mats'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Shelter') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Construction materials'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Shelter') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Household tents'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Shelter') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plastic sheeting'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Shelter') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pole & Peg Sets'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Shelter') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shelter Kits'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Solar') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Handheld lights'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI Solar') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Lanterns'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Collapsible Jerrycans'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hygiene/dignity Kit Goods'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plastic Buckets'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rigid Jerrycans'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water filtration'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water storage'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'NFI WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water treatment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = '2nd passport'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Airport taxes'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Car Hire'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Expenses'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flights'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'In-Country Travel'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'UK / Country of Domicile Travel'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Operational Travel') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Visa Fees'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Power') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Power') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Power') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Power') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Power') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Power') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Supply'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Body Armour'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Climbing Safety / Fall Arrest Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hearing Protection'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'High Visability Clothing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Maritime Safety Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Eyewear'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Footwear'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Gloves'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'PPE') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Headwear'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Safety & Security') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Safety & Security') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Safety & Security') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Safety & Security') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Personal Safety Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Safety & Security') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Search Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Safety & Security') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Automotive'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning & Waste Management'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Modification'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Services') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Repair and maintenance'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Shelter') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flatpack Building'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Shelter') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Staffing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Payroll allowances'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Staffing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Recruitment, Briefing and Debriefing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Stationery') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Stationery') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Stationery') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Storage') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boxes'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Storage') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Containers'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Storage') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'MSU'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Storage') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Racking'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Tools') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Tools') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Tools') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Tools') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Tools') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Tools'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Training') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Course Fees'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vector Control') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Biological Control'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vector Control') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vector Control') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Insect Repellant'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vector Control') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vector Control') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Armoured vehicles'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Bicycles'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mechanical Handling Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Oils and Lubricants'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Repairs & Maintenance'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Trailers'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Running Costs'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Goods') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Spares'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Vehicles - Service') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Tax & Insurance'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Warehousing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Warehousing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rent'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Warehousing') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utilities'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'WASH') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Water') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Bottled Water'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Water') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Water') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Water') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Water') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply'
UPDATE SC SET SC.CommodityID = (SELECT C.CommodityID FROM dropdown.Commodity C WHERE C.CommodityName = 'Water') FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks'
GO
--End table dropdown.SubCommodity

--Begin table dropdown.EquipmentConsignmentDispositionStatus
TRUNCATE TABLE dropdown.EquipmentConsignmentDispositionStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.EquipmentConsignmentDispositionStatus', 'EquipmentConsignmentDispositionStatusID', 0
GO

INSERT INTO dropdown.EquipmentConsignmentDispositionStatus
	(EquipmentConsignmentDispositionStatusName, EquipmentConsignmentDispositionStatusCode, DisplayOrder)
VALUES
	('In Use', 'USE', 1),
	('Consumed' , 'CONS', 2),
	('Returned' , 'RTND', 3),
	('Gifted', 'GIFT', 4),
	('Sold', 'SOLD', 5),
	('Scrapped', 'SCRAP', 6),
	('Lost', 'LOST', 7),
	('Stolen', 'STOL', 8)
GO
--End table dropdown.EquipmentConsignmentDispositionStatus

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='EquipmentConsignment', 
	@DESCRIPTION='View the list of consignments', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Activity', 
	@PERMISSIONABLELINEAGE='EquipmentConsignment.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='EquipmentConsignment', 
	@DESCRIPTION='View the list of dispatched items', 
	@METHODNAME='ListDispatchedItems', 
	@PERMISSIONABLEGROUPCODE='Activity', 
	@PERMISSIONABLELINEAGE='EquipmentConsignment.ListDispatchedItems', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='EquipmentConsignment', 
	@DESCRIPTION='View the list of distributed items', 
	@METHODNAME='ListDistributedItems', 
	@PERMISSIONABLEGROUPCODE='Activity', 
	@PERMISSIONABLELINEAGE='EquipmentConsignment.ListDistributedItems', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='EquipmentConsignment', 
	@DESCRIPTION='View the consignment contents', 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Activity', 
	@PERMISSIONABLELINEAGE='EquipmentConsignment.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.UpdateSuperAdministratorPersonPermissionables
GO

--Begin table procurement.EquipmentItem
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13455
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13456
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13457
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13458
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13459
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13460
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13461
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13462
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13463
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Heaters') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13464
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13465
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13466
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13467
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13468
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13469
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13470
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13471
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13472
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Bicycles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13473
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13474
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Warehouse consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13475
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13476
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13477
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13478
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13479
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13480
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13481
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13482
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13483
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13484
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13485
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13486
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13487
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen Appliances') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13488
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13489
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13490
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13491
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13492
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13493
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13494
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13495
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13496
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13497
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13498
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13499
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13500
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Gloves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13501
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13502
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13503
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13504
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13505
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13506
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13507
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13508
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13509
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13510
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13511
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13512
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13513
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13514
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13515
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13516
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen Appliances') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13517
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13518
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13519
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13520
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mechanical Handling Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13521
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13522
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13523
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13524
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13525
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13526
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13527
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13528
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13529
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13530
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13531
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13532
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13533
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13534
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13535
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13536
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13537
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13538
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13539
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13540
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13541
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13542
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13543
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13544
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13545
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13546
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13547
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13548
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13549
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13550
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13551
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Systems') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13552
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13553
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13554
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13555
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13556
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13557
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13558
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13559
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13560
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13561
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13562
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13563
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13564
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13565
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13566
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13567
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Warehouse consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13568
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13569
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13570
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13571
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13572
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13573
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13574
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13575
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13576
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13577
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13578
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13579
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13580
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13581
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13582
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13583
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13584
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13585
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13586
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13587
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13588
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13589
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13590
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13591
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13592
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13593
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13594
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13595
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13596
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13597
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13598
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13599
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13600
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13601
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13602
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13603
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13604
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13605
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13606
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13607
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rucksacks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13608
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13609
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13610
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13611
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13612
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13613
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13614
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13615
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13616
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13617
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13618
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13619
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13620
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13621
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13622
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13623
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13624
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13625
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13626
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13627
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13628
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13629
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13630
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13631
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13632
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13633
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13634
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13635
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13636
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13637
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13638
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13639
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13640
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13641
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Racking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13642
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13643
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13644
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13645
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13646
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13647
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13648
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13649
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13650
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13651
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13652
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13653
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13654
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13655
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13656
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13657
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13658
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13659
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13660
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13661
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13662
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13663
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13664
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13665
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13666
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13667
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13668
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13669
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13670
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13671
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13672
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13673
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13674
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13675
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13676
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13677
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13678
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13679
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13680
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13681
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13682
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13683
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13684
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Heaters') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13685
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13686
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13687
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13688
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13689
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13690
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13691
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13692
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13693
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13694
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13695
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13696
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13697
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13698
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13699
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13700
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13701
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13702
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13703
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flatpack Building') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13704
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13705
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13706
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13707
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13708
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13709
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13710
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13711
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13712
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13713
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13714
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13715
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13716
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13717
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13718
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13719
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13720
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13721
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13722
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13723
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13724
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13725
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13726
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13727
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13728
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13729
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13730
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13731
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13732
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13733
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13734
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13735
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13736
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13737
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13738
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13739
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13740
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13741
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13742
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13743
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13744
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13745
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13746
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13747
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13748
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13749
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13750
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13751
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13752
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13753
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13754
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13755
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13756
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13757
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13758
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13759
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13760
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13761
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13762
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13763
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13764
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13765
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13766
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13767
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13768
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13769
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13770
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13771
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13772
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13773
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13774
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13775
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13776
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13777
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13778
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13779
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13780
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13781
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13782
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13783
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13784
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13785
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13786
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13787
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13788
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13789
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13790
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13791
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13792
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13793
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13794
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13795
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13796
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13797
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13798
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13799
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13800
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13801
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13802
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13803
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13804
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13805
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13806
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13807
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13808
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13809
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13810
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13811
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13812
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13813
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13814
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13815
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13816
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13817
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13818
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13819
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13820
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13821
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13822
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13823
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13824
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13825
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13826
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13827
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13828
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13829
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13830
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13831
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13832
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13833
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13834
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13835
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13836
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13837
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13838
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13839
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13840
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13841
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13842
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13843
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13844
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13845
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13846
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13847
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13848
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13849
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13850
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13851
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13852
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13853
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13854
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13855
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13856
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13857
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13858
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13859
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13860
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13861
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13862
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13863
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13864
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13865
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13866
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13867
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13868
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13869
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13870
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13871
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13872
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13873
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13874
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13875
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13876
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13877
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13878
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13879
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13880
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13881
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13882
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13883
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13884
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13885
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13886
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13887
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13888
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13889
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13890
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13891
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13892
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13893
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13894
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13895
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13896
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13897
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13898
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13899
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13900
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13901
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13902
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13903
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13904
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13905
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13906
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13907
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13908
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13909
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13910
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13911
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13912
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13913
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13914
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13915
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13916
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13917
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13918
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13919
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13920
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13921
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13922
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13923
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13924
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13925
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13926
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13927
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13928
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13929
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13930
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13931
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13932
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13933
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13934
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13935
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13936
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13937
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13938
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13939
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13940
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13941
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13942
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13943
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13944
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13945
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13946
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13947
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13948
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13949
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13950
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13951
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13952
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13953
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13954
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13955
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13956
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13957
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13958
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13959
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13960
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13961
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13962
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13963
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13964
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13965
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13966
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13967
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13968
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13969
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13970
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13971
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13972
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13973
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13974
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13975
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13976
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13977
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13978
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13979
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13980
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13981
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13982
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13983
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13984
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13985
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13986
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13987
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13988
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13989
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13990
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13991
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13992
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13993
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13994
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13995
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13996
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13997
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13998
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 13999
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14000
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14001
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14002
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14003
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14004
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14005
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14006
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14007
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14008
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14009
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14010
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14011
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14012
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14013
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14014
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14015
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14016
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14017
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14018
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14019
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14020
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14021
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14022
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14023
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14024
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14025
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14026
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14027
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14028
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14029
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14030
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14031
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14032
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14033
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14034
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14035
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14036
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14037
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14038
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rigid Jerrycans') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14039
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14040
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14041
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Heaters') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14042
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14043
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14044
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14045
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14046
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14047
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14048
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14049
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14050
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14051
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14052
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14053
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14054
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14055
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14056
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14057
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Lanterns') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14058
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14059
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14060
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14061
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boilers/Kettles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14062
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14063
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14064
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14065
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14066
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14067
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14068
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14069
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14070
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14071
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14072
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14073
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14074
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14075
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14076
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14077
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14078
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14079
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14080
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14081
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14082
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14083
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14084
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14085
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14086
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14087
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14088
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14089
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14090
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14091
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14092
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14093
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14094
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14095
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14096
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14097
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14098
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14099
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14100
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14101
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14102
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14103
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14104
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14105
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14106
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14107
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14108
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rigid Jerrycans') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14109
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14110
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14111
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14112
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14113
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14114
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14115
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14116
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14117
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Blankets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14118
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plastic Buckets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14119
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14120
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14121
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14122
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14123
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14124
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14125
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Lanterns') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14126
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14127
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14128
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14129
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14130
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Insect Repellant') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14131
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Insect Repellant') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14132
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14133
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14134
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14135
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14136
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14137
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14138
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14139
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14140
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14141
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14142
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14143
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14144
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14145
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14146
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14147
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14148
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14149
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14150
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14151
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14152
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14153
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14154
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14155
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14156
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14157
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14158
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Eyewear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14159
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14160
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14161
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14162
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14163
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14164
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14165
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14166
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14167
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14168
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14169
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14170
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14171
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14172
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14173
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14174
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14175
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14176
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14177
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14178
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14179
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14180
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14181
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14182
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14183
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14184
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Bicycles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14185
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14186
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14187
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14188
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14189
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plastic Buckets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14190
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14191
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14192
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14193
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water filtration') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14194
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14195
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14196
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14197
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14198
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14199
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14200
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14201
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14202
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14203
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14204
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14205
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14206
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14207
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14208
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Handheld lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14209
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14210
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14211
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14212
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14213
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Food and drink equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14214
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14215
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14216
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14217
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14218
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14219
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14220
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14221
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14222
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14223
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14224
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14225
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14226
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14227
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14228
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14229
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14230
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14231
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14232
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14233
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14234
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14235
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14236
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14237
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14238
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14239
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14240
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14241
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14242
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14243
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14244
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14245
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14246
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14247
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14248
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14249
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14250
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen sets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14251
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14252
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tents/Accommodation') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14253
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14254
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14255
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14256
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14257
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multitools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14258
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14259
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14260
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14261
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14262
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14263
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14264
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14265
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14266
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14267
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14268
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14269
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14270
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14271
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14272
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14273
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14274
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14275
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14276
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14277
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14278
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14279
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14280
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14281
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14282
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14283
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14284
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Therapeutic Foods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14285
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14286
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14287
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14288
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14289
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Therapeutic Foods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14290
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14291
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14292
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14293
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14294
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14295
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14296
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14297
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14298
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flatpack Building') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14299
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14300
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14301
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14302
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14303
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14304
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14305
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14306
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boilers/Kettles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14307
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14308
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14309
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14310
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14311
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14312
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14313
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14314
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14315
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14316
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14317
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14318
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14319
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14320
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14321
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14322
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14323
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14324
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14325
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Bicycles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14326
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14327
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14328
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14329
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14330
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14331
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14332
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14333
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14334
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14335
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14336
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen Appliances') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14337
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14338
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14339
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Therapeutic Foods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14340
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Systems') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14341
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14342
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14343
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14344
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14345
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14346
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14347
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14348
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14349
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14350
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14351
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14352
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14353
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14354
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14355
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14356
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14357
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14358
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14359
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Deployment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14360
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14361
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14362
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14363
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tarpaulins') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14364
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14365
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14366
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14367
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14368
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14369
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14370
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14371
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14372
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14373
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14374
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14375
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14376
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Site Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14377
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14378
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14379
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14380
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14381
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14382
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14383
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14384
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14385
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14386
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14387
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14388
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14389
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Site Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14390
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14391
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14392
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14393
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14394
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14395
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14396
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14397
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14398
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14399
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14400
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14401
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14402
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14403
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14404
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Eyewear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14405
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14406
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14407
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14408
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14409
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14410
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14412
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14413
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14414
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14415
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14416
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14417
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14418
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14419
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14420
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14421
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14422
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14423
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14424
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14425
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14426
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14427
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14428
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14429
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14430
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14431
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14432
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14433
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14434
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14435
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14436
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14437
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14438
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14439
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14440
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14441
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14442
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14443
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fuel Storage') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14444
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Bottled Water') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14445
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14446
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14447
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14448
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14449
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14450
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14451
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14452
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14453
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14454
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14455
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14456
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14457
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14458
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14459
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14460
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14461
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14462
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14463
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14464
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14465
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14466
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14467
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14468
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14469
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14470
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14471
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14472
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14473
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14474
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14475
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14476
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14477
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14478
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14479
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14480
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14481
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14482
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14483
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14484
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14485
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14486
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Toilet & Sanitation Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14487
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14488
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14489
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14490
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14491
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14492
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14493
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14494
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14495
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14496
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14497
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14498
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14499
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14500
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14501
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14502
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14503
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14504
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14505
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14506
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14507
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14508
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14509
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14510
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14511
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14512
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14513
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14514
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14515
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14516
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14517
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14518
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14519
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Armoured vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14520
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14521
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14522
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14523
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14524
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14525
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14526
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14527
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14528
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Food and drink equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14529
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14530
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14531
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Climbing Safety / Fall Arrest Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14532
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14533
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14534
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14535
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14536
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14537
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14538
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14539
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14540
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14541
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14542
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14543
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14544
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14545
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14546
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14547
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cold-Chain Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14548
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14549
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14550
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14551
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14552
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14553
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14554
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14555
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14556
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14557
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14558
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14559
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14560
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14561
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14562
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14563
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14564
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14565
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14566
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14567
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14568
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14569
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14570
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14571
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14572
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14573
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14574
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14575
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14576
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14577
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14578
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14579
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14580
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14581
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14582
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14583
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14584
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14585
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14586
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14587
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14588
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14589
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14590
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14591
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14592
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14593
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14594
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14595
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14596
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14597
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14598
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Warehouse consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14599
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14600
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14601
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14602
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14603
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14604
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14605
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14606
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14607
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14608
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14609
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14610
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14611
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14612
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hearing Protection') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14613
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14614
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14615
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14616
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14617
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14618
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14619
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14620
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14621
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14622
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14623
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14624
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14625
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14626
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14627
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14628
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14629
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14630
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14631
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14632
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14633
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14634
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14635
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14636
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14637
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14638
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14639
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14640
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cold-Chain Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14641
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14642
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14643
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14644
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14645
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14646
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14647
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14648
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14649
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14650
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14651
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14652
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14653
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14654
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14655
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14656
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14657
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14658
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14659
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14660
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14661
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14662
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fuel Storage') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14663
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14664
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14665
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14666
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14667
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14668
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14669
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14670
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14671
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14672
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14673
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14674
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14675
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14676
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14677
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14678
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14679
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14680
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14681
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14682
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14683
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14684
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14685
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14686
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14687
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rigid Jerrycans') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14688
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14689
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Insect Repellant') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14690
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14691
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14692
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Headwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14693
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Site Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14694
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14695
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14696
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14697
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14698
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14699
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14700
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14701
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14702
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14703
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14704
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14705
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14706
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14707
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14708
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14709
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14710
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14711
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14712
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14713
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Food and drink equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14714
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Armoured vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14715
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Armoured vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14716
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14717
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14718
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14719
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14720
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14721
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14722
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14723
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14724
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14725
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Site Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14726
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14727
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14728
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14729
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14730
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14731
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14732
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14733
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14734
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14735
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14736
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14737
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14738
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14739
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14740
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14741
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14742
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14743
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14744
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14745
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14746
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14747
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14748
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14749
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14750
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14751
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14752
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14753
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14754
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14755
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14756
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14757
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14758
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14759
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14760
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14761
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14762
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14763
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14764
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14765
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14766
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14767
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14768
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14769
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14770
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14771
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14772
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14773
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14774
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14775
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14776
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14777
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14778
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14779
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14780
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14781
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14782
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hygiene/dignity Kit Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14783
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14784
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14785
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14786
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'MSU') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14787
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14788
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multitools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14789
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14790
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14791
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14792
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14793
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14794
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14795
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14796
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14797
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14798
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14799
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14800
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14801
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14802
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14803
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Containers') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14804
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14805
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boilers/Kettles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14806
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14807
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14808
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14809
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14810
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14811
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14812
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Handheld lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14813
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14814
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14815
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14816
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14817
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14818
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14819
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14820
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14821
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14822
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14823
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14824
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14825
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14826
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Site Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14827
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14828
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14829
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14830
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14831
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14832
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14833
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14834
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14835
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14836
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14837
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14838
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14839
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'GPS Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14840
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14841
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14842
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14843
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14844
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14845
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14846
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14847
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14848
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14849
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14850
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14851
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14852
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14853
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14854
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14855
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14856
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14857
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14858
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14859
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14860
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14861
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14862
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14863
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14864
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14865
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14866
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14867
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14868
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14869
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14870
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14871
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14872
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14873
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14874
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14875
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14876
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14877
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14878
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14879
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14880
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14881
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14882
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14883
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14884
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14885
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14886
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14887
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14888
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14889
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14890
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14891
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14892
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14893
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14894
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14895
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14896
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14897
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14898
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14899
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14900
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14901
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14902
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14903
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14904
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14905
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14906
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14907
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Spares') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14908
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Spares') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14909
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14910
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14911
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14912
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tarpaulins') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14913
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14914
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14915
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14916
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14917
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14918
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14919
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14920
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14921
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14922
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14923
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Gloves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14924
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Site Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14925
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14926
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14927
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14928
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14929
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14930
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14931
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14932
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14933
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14934
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14935
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Gloves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14936
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14937
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14938
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14939
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14940
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14941
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14942
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14943
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14944
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14945
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14946
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14947
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Food and drink equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14948
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14949
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shelter Kits') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14950
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14951
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14952
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14953
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14954
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14955
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14956
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14957
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14958
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14959
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14960
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14961
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14962
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14963
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14964
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14965
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14966
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14967
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14968
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14969
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14970
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14971
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14972
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14973
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14974
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14975
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14976
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14977
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14978
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Food and drink equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14979
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14980
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14981
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14982
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14983
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14984
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14985
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14986
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14987
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14988
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14989
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14990
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14991
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14992
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14993
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14994
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14995
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14996
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14997
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Toilet & Sanitation Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14998
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 14999
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15000
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15001
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15002
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15003
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15004
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15005
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15006
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15007
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15008
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15009
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15010
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15011
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15012
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15013
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15014
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15015
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15016
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15017
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15018
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15019
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15020
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15021
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15022
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tents/Accommodation') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15023
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15024
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15025
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15026
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15027
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15028
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15029
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15030
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15031
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Racking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15032
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15033
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15034
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15035
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15036
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15037
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15038
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15039
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15040
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15041
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15042
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15043
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Oils and Lubricants') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15044
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15045
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15046
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15047
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15048
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15049
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15050
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15051
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15052
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15053
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15054
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15055
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15056
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15057
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15058
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15059
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15060
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15061
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15062
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15063
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15064
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15065
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15066
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15067
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cold-Chain Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15068
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15069
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15070
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15071
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15072
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15073
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15074
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15075
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15076
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15077
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15078
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15079
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15080
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15081
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15082
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15083
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15084
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15085
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15086
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'MSU') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15087
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15088
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15089
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15090
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15091
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15092
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15093
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15094
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15095
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15096
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15097
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15098
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15099
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rigid Jerrycans') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15100
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15101
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15102
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15103
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15104
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15105
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15106
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15107
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15108
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15109
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15110
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15111
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15112
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15113
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15114
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15115
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15116
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15117
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15118
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15119
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15120
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15121
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15122
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15123
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15124
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15125
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15126
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15127
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15128
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15129
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15130
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15131
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15132
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15133
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15134
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15135
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15136
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15137
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15138
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15139
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15140
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15141
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15142
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15143
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15144
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15145
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15146
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15147
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15148
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15149
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15150
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15151
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15152
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15153
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15154
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15155
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15156
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15157
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15158
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15159
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15160
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15161
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15162
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15163
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15164
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15165
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15166
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'First Aid/Trauma Kits') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15167
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15168
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15169
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15170
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15171
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15172
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15173
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15174
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15175
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15176
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15177
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'MSU') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15178
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15179
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15180
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15181
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15182
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15183
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15184
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15185
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15186
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15187
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15188
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15189
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15190
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15191
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15192
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15193
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15194
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15195
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15196
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15197
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15198
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15199
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15200
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15201
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15202
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15203
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boilers/Kettles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15204
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15205
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15206
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15207
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15208
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15209
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15210
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cold-Chain Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15211
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15212
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15213
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15214
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15215
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15216
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15217
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15218
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15219
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15220
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15221
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15222
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15223
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15224
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Systems') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15225
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15226
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15227
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15228
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15229
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Deployment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15230
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15231
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15232
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15233
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15234
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15235
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15236
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15237
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15238
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15239
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15240
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15241
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15242
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15243
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15244
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15245
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15246
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'First Aid/Trauma Kits') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15247
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15248
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15249
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15250
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15251
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15252
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15253
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15254
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15255
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15256
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15257
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15258
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15259
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15260
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15261
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15262
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15263
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15264
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15265
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15266
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15267
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15268
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15269
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15270
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15271
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15272
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15273
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15274
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15275
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15276
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Computer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15277
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15278
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15279
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15280
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15281
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15282
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen Appliances') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15283
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15284
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15285
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15286
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15287
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15288
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15289
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15290
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15291
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15292
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15293
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15294
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15295
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15296
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15297
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Oils and Lubricants') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15298
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15299
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15300
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15301
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15302
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15303
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tents/Accommodation') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15304
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15305
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15306
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15307
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15308
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15309
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15310
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15311
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15312
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15313
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15314
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15315
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15316
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15317
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15318
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15319
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15320
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15321
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15322
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15323
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15324
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15325
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15326
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15327
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15328
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15329
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Blankets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15330
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15331
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15332
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15333
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15334
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15335
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15336
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15337
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15338
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15339
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'First Aid/Trauma Kits') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15340
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15341
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15342
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15343
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15344
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15345
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15346
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15347
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15348
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15349
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15350
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15351
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15352
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15353
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15354
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15355
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15356
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15357
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15358
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15359
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15360
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15361
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15362
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15363
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15364
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15365
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15366
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15367
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15368
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15369
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15370
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15371
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15372
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15373
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15374
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15375
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15376
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15377
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15378
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15379
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15380
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15381
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15382
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15383
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cold-Chain Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15384
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15385
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15386
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15387
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15388
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15389
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15390
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15391
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15392
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15393
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15394
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15395
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15396
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15397
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15398
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15399
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15400
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Heaters') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15401
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15402
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15403
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15404
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15405
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15406
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15407
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15408
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15409
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Warehouse consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15410
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15411
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15412
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15413
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15414
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15415
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15416
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mechanical Handling Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15417
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15418
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15419
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hygiene/dignity Kit Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15420
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15421
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15422
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15423
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15424
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15425
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15426
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15427
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15428
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15429
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15430
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15431
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tents/Accommodation') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15432
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15433
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15434
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15435
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15436
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15437
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15438
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15439
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15440
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15441
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15442
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15443
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15444
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15445
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15446
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15447
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15448
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15449
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15450
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15451
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15452
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15453
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15454
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15455
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15456
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15457
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15458
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15459
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15460
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15461
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15462
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15463
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15464
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Headwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15465
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15466
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15467
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15468
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15469
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Kitchen sets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15470
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15471
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15472
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15473
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15474
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15475
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15476
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15477
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15478
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15479
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15480
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'GPS Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15481
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15482
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15483
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fuel Storage') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15484
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15485
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15486
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15487
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15488
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15489
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15490
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15491
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15492
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15493
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15494
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15495
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15496
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15497
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15498
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15499
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15500
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15501
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15502
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15503
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15504
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15505
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15506
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15507
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15508
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15509
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15510
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15511
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15512
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15513
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15514
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15515
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15516
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15517
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15518
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15519
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15520
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15521
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15522
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15523
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15524
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15525
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15526
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15527
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15528
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15529
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15530
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15531
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15532
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15533
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15534
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15535
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15536
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Racking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15537
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15538
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15539
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15540
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15541
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Insect Repellant') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15542
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15543
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15544
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15545
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15546
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15547
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fuel Storage') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15548
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15549
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15550
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15551
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15552
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15553
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15554
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15555
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15556
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15557
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15558
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15559
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15560
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15561
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15562
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15563
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15564
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15565
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15566
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15567
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15568
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Air-Conditioners') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15569
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15570
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15571
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15572
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15573
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15574
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15575
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15576
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rigid Jerrycans') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15577
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Printer Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15578
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15579
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15580
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15581
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15582
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Collapsible Jerrycans') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15583
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15584
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15585
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15586
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15587
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15588
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15589
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15590
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15591
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15592
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15593
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15594
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15595
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15596
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15597
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15598
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15599
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15600
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15601
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15602
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15603
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15604
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15605
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15606
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15607
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15608
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15609
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15610
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15611
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15612
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15613
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15614
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15615
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15616
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15617
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15618
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15619
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15620
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'VHF') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15621
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15622
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15623
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15624
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15625
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15626
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15627
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15628
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15629
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15630
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15631
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15632
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15633
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15634
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15635
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15636
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15637
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15638
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15639
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15640
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15641
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15642
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15643
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15644
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15645
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15646
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15647
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15648
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15649
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15650
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15651
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15652
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15653
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15654
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15655
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15656
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15657
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15658
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15659
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15660
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15661
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15662
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15663
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15664
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15665
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15666
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15667
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15668
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15669
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15670
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15671
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15672
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15673
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15674
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15675
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15676
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15677
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15678
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15679
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15680
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15681
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15682
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15683
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15684
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15685
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15686
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15687
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15688
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15689
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15690
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15691
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15692
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15693
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15694
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15695
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15696
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15697
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15698
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15699
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15700
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15701
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Packaging') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15702
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15703
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15704
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15705
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15706
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15707
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15708
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15709
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15710
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15711
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15712
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15713
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15714
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15715
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15716
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15717
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15718
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15719
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15720
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15721
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15722
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15723
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15724
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15725
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15726
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15727
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15728
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15729
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15730
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15731
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15732
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15733
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15734
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hearing Protection') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15735
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15736
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15737
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15738
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15739
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15740
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15741
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15742
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15743
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15744
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15745
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15746
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15747
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15748
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15749
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15750
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15751
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15752
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15753
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15754
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15755
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15756
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15757
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15758
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15759
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15760
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15761
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15762
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15763
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15764
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15765
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15766
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15767
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15768
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Eyewear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15769
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15770
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15771
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15772
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15773
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15774
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15775
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15776
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15777
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15778
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15779
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15780
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15781
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15782
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15783
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15784
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rucksacks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15785
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15786
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15787
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15788
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15789
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15790
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15791
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15792
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15793
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15794
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15795
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15796
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15797
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15798
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15799
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15800
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15801
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15802
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15803
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chemical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15804
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15805
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mosquito Nets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15806
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15807
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15808
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15809
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15810
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15811
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15812
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15813
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15814
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15815
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15816
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15817
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15818
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Traps & Physical Control') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15819
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15820
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15821
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15822
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15823
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15824
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15825
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15826
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15827
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15828
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15829
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15830
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mechanical Handling Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15831
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15832
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15833
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15834
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15835
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15836
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15837
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15838
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15839
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15840
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15841
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15842
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15843
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Blankets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15844
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15845
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15846
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15847
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15848
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15849
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15850
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15851
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15852
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15853
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15854
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Personal Safety Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15855
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15856
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15857
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15858
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15859
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15860
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15861
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15862
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15863
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15864
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15865
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15866
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15867
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15868
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15869
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15870
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15871
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15872
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15873
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15874
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15875
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15876
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15877
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15878
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15879
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15880
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15881
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15882
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15883
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15884
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15885
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15886
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15887
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15888
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15889
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15890
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15891
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15892
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15893
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15894
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15895
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15896
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15897
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15898
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15899
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15900
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15901
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Gloves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15902
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boilers/Kettles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15903
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15904
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15905
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15906
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15907
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15908
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15909
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15910
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15911
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15912
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15913
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15914
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15915
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15916
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15917
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Copying and Printing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15918
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15919
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15920
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15921
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15922
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15923
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15924
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15925
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15926
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15927
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15928
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15929
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15930
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15931
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15932
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15933
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15934
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15935
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15936
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15937
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15938
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15939
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15940
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15941
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15942
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15943
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15944
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15945
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15946
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15947
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15948
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15949
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15950
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15951
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15952
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15953
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15954
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15955
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15956
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15957
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15958
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15959
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15960
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15961
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15962
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15963
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15964
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15965
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15966
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15967
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15968
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15969
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15970
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15971
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15972
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15973
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15974
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15975
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15976
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15977
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15978
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Chairs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15979
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15980
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15981
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Disposal') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15982
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15983
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15984
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15985
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15986
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15987
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15988
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15989
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15990
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15991
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15992
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15993
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15994
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15995
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cold-Chain Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15996
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mechanical Handling Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15997
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15998
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 15999
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16000
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16001
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16002
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16003
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16004
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16005
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16006
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16007
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16008
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16009
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Rigid Jerrycans') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16010
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16011
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16012
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16013
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16014
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16015
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16016
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16017
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16018
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16019
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16020
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16021
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16022
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16023
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16024
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16025
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16026
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16027
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16028
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16029
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16030
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16031
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16032
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16033
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16034
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16035
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16036
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16037
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16038
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16039
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16040
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Clothing Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16041
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16042
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16043
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16044
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16045
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16046
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16047
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16048
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16049
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16050
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16051
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16052
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16053
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16054
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16055
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16056
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16057
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'First Aid/Trauma Kits') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16058
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16059
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16060
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16061
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16062
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16063
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16064
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16065
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16066
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16067
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16068
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16069
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Tanks') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16070
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16071
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16072
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16073
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16074
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16075
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16076
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16077
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16078
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16079
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16080
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16081
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16082
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16083
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16084
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16085
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16086
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16087
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16088
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16089
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mechanical Handling Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16090
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16091
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16092
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16093
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16094
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16095
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16096
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16097
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16098
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16099
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fuel Storage') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16100
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16101
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16102
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16103
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16104
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16105
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16106
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16107
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16108
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Signage & Labels') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16109
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16110
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16111
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16112
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16113
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Armoured vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16114
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Armoured vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16115
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16116
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16117
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16118
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Furniture') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16119
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16120
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16121
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16122
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16123
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16124
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16125
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16126
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'First Aid/Trauma Kits') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16127
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16128
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16129
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16130
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16131
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16132
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16133
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16134
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16135
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16136
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16137
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16138
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Heaters') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16139
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16140
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Satcomms Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16141
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16142
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Barrier Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16143
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16144
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16145
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16146
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16147
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16148
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16149
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16150
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16151
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16152
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16153
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16154
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16155
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16156
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16157
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16158
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16159
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16160
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16161
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16162
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16163
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16164
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16165
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16166
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16167
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16168
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16169
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16170
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16171
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16172
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16173
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16174
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16175
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16176
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16177
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16178
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16179
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Internal Lights') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16180
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16181
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16182
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16183
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fuel Storage') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16184
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16185
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16186
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16187
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16188
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16189
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16190
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16191
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16192
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hygiene/dignity Kit Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16193
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16194
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16195
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16196
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16197
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16198
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16199
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16200
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16201
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16202
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16203
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16204
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16205
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Protective Eyewear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16206
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16207
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plugs/Adaptors') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16208
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16209
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16210
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16211
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16212
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16213
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16214
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16215
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16216
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16217
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16218
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16219
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16220
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16221
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16222
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16223
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16224
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16225
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Water Supply') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16226
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16227
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16228
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Torches/Headtorches') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16229
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sleeping Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16230
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other Protective Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16231
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16232
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16233
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16234
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16235
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16236
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16237
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16238
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16239
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16240
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16241
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Generators') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16242
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16243
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16244
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16245
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16246
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16247
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16248
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16249
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16250
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16251
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16252
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16253
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16254
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Shower & Washing Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16255
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16256
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16257
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16258
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16259
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16260
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16261
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16262
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16263
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16264
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16265
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16266
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16267
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Pharmaceuticals') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16268
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16269
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16270
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16271
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16272
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16273
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16274
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16275
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16276
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16277
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16278
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16279
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16280
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16281
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16282
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Audio & Visual Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16283
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16284
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Fire & Gas Safety equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16285
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16286
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Boilers/Kettles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16287
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Utensils') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16288
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Vehicle Spares') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16289
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16290
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16291
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16292
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16293
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16294
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16295
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'PPE') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16296
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution Boxes') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16297
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Sanitation & Toilet') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16298
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16299
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16300
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16301
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16302
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16303
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cleaning Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16304
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16305
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16306
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Motor Vehicles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16307
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16308
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ropes and Lashings') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16309
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16310
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16311
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16312
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16313
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16314
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16315
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Power Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16316
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Locks / Safes / Keys') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16317
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Batteries') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16318
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Home Textiles') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16319
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16320
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Mechanical Handling Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16321
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16322
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16323
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16324
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16325
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Hand Tools') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16326
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Other consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16327
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16328
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16329
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16330
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Footwear') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16331
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cutlery/Crockery/Drinking') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16332
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16333
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16334
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16335
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Office Stationery') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16336
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16337
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16338
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16339
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Stoves') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16340
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Cabling') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16341
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Flooring') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16342
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Non-Uniform Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16343
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16344
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Multipurpose/Infrastructure Tents') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16345
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Uniform') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16346
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16347
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Distribution') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16348
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16349
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plumbing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16350
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16351
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16352
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16353
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16354
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16355
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16356
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16357
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16358
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16359
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16360
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16361
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16362
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16363
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16364
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16365
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16366
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16367
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16368
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16369
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16370
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16371
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16372
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16373
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16374
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16375
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16376
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16377
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16378
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16379
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16380
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16381
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16382
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16383
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16384
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16385
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16386
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16387
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16388
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16389
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16390
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16391
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16392
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16393
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16394
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16395
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16396
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16397
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16398
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16399
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16400
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16401
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16402
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16403
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16404
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16405
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16406
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16407
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16408
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16409
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16410
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16411
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16412
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16413
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16414
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16415
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16416
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16417
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16418
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16419
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16420
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16421
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16422
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16423
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16424
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16425
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16426
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16427
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16428
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16429
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16430
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16431
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16432
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16433
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16434
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16435
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16436
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16437
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16438
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16439
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16440
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16441
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16442
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16443
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16444
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16445
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16446
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16447
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16448
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16449
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16450
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16451
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16452
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16453
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16454
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16455
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16456
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16457
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16458
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16459
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16460
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16461
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16462
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16463
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16464
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16465
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16466
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16467
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16468
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16469
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16470
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16471
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16472
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16473
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16474
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16475
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16476
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16477
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16478
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16479
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16480
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16481
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16482
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16483
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16484
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16485
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16486
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16487
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16488
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16489
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16490
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16491
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16492
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16493
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16494
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16495
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16496
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16497
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16498
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16499
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16500
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16501
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16502
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16503
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16504
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16505
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Ration Packs') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16506
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16507
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Weatherproof Clothing') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16508
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Plastic Buckets') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16509
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Miscellaneous Goods') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16510
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16511
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16512
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16513
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16514
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16515
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16516
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Tarpaulins') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16517
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16518
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Consumables') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16519
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16520
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16521
UPDATE EI SET EI.SubCommodityID = (SELECT SC.SubCommodityID FROM dropdown.SubCommodity SC WHERE SC.SubCommodityName = 'Electrical Equipment') FROM procurement.EquipmentItem EI WHERE EI.EquipmentItemID = 16522
GO
--End table procurement.EquipmentItem
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 1;
EXEC permissionable.SavePermissionableGroup 'Document', 'Documents', 2;
EXEC permissionable.SavePermissionableGroup 'ForceAsset', 'Organizations & Facilities', 3;
EXEC permissionable.SavePermissionableGroup 'Insight', 'Incidents & Reports', 4;
EXEC permissionable.SavePermissionableGroup 'Activity', 'Activity', 5;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 6;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the CTM Travel Approval menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CTMTravelApproval', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.CTMTravelApproval', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the DeployAdviser menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='DeployAdviser', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.DeployAdviser', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Compliance Checker menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumComplianceChecker', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumComplianceChecker', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Staff Time Collection menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumStaffTimeCollection', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumStaffTimeCollection', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of Facilities', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of Organisations', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Add / edit a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Allows access to response report exports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View the list of response reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Share an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.ShareIncident', @PERMISSIONCODE='ShareIncident';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='Add / edit a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View the list of Report Updates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Situational report pdf export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Situational report aggregator export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalogue list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of consignments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of dispatched items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDispatchedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDispatchedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of distributed items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDistributedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the consignment contents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='Add / edit an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment inventory list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='InventoryList', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.InventoryList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment item list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='Add / edit an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View the list of Equipment Movements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='Add / edit an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View the equipment order list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Error message for DA users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='DAView', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.DAView', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the Humanitarian roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.HumanitarianRoster', @PERMISSIONCODE='HumanitarianRoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the SU roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.SURoster', @PERMISSIONCODE='SURoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;

--Begin update document type permissions
EXEC document.UpdateDocumentPermissions
--End update document type permissions
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.5 - 2018.10.08 09.33.27')
GO
--End build tracking

