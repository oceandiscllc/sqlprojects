USE HEROS
GO
--Begin function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A function to return document data for a specific entity record
-- ============================================================================

CREATE FUNCTION eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cDocuments VARCHAR(MAX) = '<Documents></Documents>'
	
	IF EXISTS (SELECT 1 FROM document.DocumentEntity T WHERE T.EntityTypeCode = @EntityTypeCode AND T.EntityID = @EntityID)
		BEGIN

		SELECT 
			@cDocuments = COALESCE(@cDocuments, '') + D.Document 
		FROM
			(
			SELECT
				(SELECT T.DocumentID FOR XML RAW('Documents'), ELEMENTS) AS Document
			FROM document.DocumentEntity T 
			WHERE T.EntityTypeCode = @EntityTypeCode
				AND T.EntityID = @EntityID
			) D
	
		IF @cDocuments IS NULL OR LEN(LTRIM(@cDocuments)) = 0
			SET @cDocuments = '<Documents></Documents>'
		--ENDIF

		END
	--ENDIF
		
	RETURN @cDocuments

END
GO
--End function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID

--Begin function procurement.GetConsignmentStatusByConsignmentCode
EXEC utility.DropObject 'procurement.GetConsignmentStatusByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to get a consignment status based on a consignmentcode
-- ==============================================================================

CREATE FUNCTION procurement.GetConsignmentStatusByConsignmentCode
(
@ConsignmentCode VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cConsignmentStatus VARCHAR(50)

	SELECT @cConsignmentStatus = 
		CASE
			WHEN NOT EXISTS 
				(
				SELECT 1 
				FROM procurement.EquipmentConsignmentDisposition ECD 
					JOIN procurement.EquipmentConsignment EC ON EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID 
						AND EC.ConsignmentCode = @ConsignmentCode
				)
			THEN 'Dispatched'
			ELSE
				CASE
					WHEN EXISTS 
						(
						SELECT 1 
						FROM procurement.EquipmentConsignmentDisposition ECD 
							JOIN procurement.EquipmentConsignment EC ON EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID 
								AND EC.ConsignmentCode = @ConsignmentCode
								AND ECD.QuantityShrink + ECD.QuantityWaste + ECD.QuantityReceived < EC.Quantity
						)
					THEN 'Partially Completed'
					ELSE 'Complete'
				END
		END 

	RETURN ISNULL(@cConsignmentStatus, '')

END
GO
--End function procurement.GetConsignmentStatusByConsignmentCode

--Begin function procurement.GetConsignmentStatusByEquipmentConsignmentID
EXEC utility.DropObject 'procurement.GetConsignmentStatusByEquipmentConsignmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.09
-- Description:	A function to get a consignment status based on an equipmentconsignmentid
-- ======================================================================================

CREATE FUNCTION procurement.GetConsignmentStatusByEquipmentConsignmentID
(
@EquipmentConsignmentID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cConsignmentStatus VARCHAR(50) = 'Dispatched'
	DECLARE @nQuantity INT
	DECLARE @nQuantityDisposed INT

	SELECT
		@nQuantity = ISNULL((SELECT EC.Quantity FROM procurement.EquipmentConsignment EC WHERE EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID), 0),
		@nQuantityDisposed = ISNULL(SUM(ECD.QuantityReceived), 0) + ISNULL(SUM(ECD.QuantityShrink), 0) + ISNULL(SUM(ECD.QuantityWaste), 0)
	FROM procurement.EquipmentConsignmentDisposition ECD
	WHERE ECD.EquipmentConsignmentID = @EquipmentConsignmentID
	GROUP BY ECD.EquipmentConsignmentID

	IF @nQuantity > 0 AND @nQuantity - @nQuantityDisposed = 0
		SET @cConsignmentStatus = 'Complete'
	ELSE IF @nQuantityDisposed > 0
		SET @cConsignmentStatus = 'Partially Completed'
	--ENDIF

	RETURN ISNULL(@cConsignmentStatus, '')

END
GO
--End function procurement.GetConsignmentStatusByEquipmentConsignmentID

--Begin function procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode
EXEC utility.DropObject 'procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to return a table with aggregated quantities from the procurement.EquipmentConsignmentDisposition table
-- ===============================================================================================================================

CREATE FUNCTION procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode
(
@ConsignmentCode VARCHAR(50)
)

RETURNS @tTable TABLE 
	(
	EquipmentConsignmentID INT NOT NULL PRIMARY KEY, 
	QuantityReceived INT NOT NULL DEFAULT 0, 
	QuantityShrink INT NOT NULL DEFAULT 0, 
	QuantityWaste INT NOT NULL DEFAULT 0, 
	QuantityDisposed INT NOT NULL DEFAULT 0
	)  

AS
BEGIN

	INSERT INTO @tTable
		(EquipmentConsignmentID,QuantityReceived,QuantityShrink,QuantityWaste,QuantityDisposed)
	SELECT
		ECD.EquipmentConsignmentID,
		SUM(ECD.QuantityReceived) AS QuantityReceived,
		SUM(ECD.QuantityShrink) AS QuantityShrink,
		SUM(ECD.QuantityWaste) AS QuantityWaste,
		SUM(ECD.QuantityReceived) + SUM(ECD.QuantityShrink) + SUM(ECD.QuantityWaste) AS QuantityDisposed
	FROM procurement.EquipmentConsignmentDisposition ECD
	WHERE @ConsignmentCode IS NULL
		OR EXISTS
			(
			SELECT 
				EC.EquipmentConsignmentID
			FROM procurement.EquipmentConsignment EC
			WHERE EC.EquipmentConsignmentID = ECD.EquipmentConsignmentID
				AND EC.ConsignmentCode = @ConsignmentCode
			)
	GROUP BY ECD.EquipmentConsignmentID
	ORDER BY ECD.EquipmentConsignmentID

	RETURN

END
GO
--End function procurement.GetEquipmentConsignmentDispositionQuantitiesByConsignmentCode

--Begin function procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID
EXEC utility.DropObject 'procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to return a table with aggregated quantities from the procurement.EquipmentConsignmentDisposition table
-- ===============================================================================================================================

CREATE FUNCTION procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID
(
@EquipmentConsignmentID INT
)

RETURNS @tTable TABLE 
	(
	EquipmentConsignmentID INT NOT NULL PRIMARY KEY, 
	QuantityReceived INT NOT NULL DEFAULT 0, 
	QuantityShrink INT NOT NULL DEFAULT 0, 
	QuantityWaste INT NOT NULL DEFAULT 0, 
	QuantityDisposed INT NOT NULL DEFAULT 0
	)  

AS
BEGIN

	INSERT INTO @tTable
		(EquipmentConsignmentID,QuantityReceived,QuantityShrink,QuantityWaste,QuantityDisposed)
	SELECT
		ECD.EquipmentConsignmentID,
		SUM(ECD.QuantityReceived) AS QuantityReceived,
		SUM(ECD.QuantityShrink) AS QuantityShrink,
		SUM(ECD.QuantityWaste) AS QuantityWaste,
		SUM(ECD.QuantityReceived) + SUM(ECD.QuantityShrink) + SUM(ECD.QuantityWaste) AS QuantityDisposed
	FROM procurement.EquipmentConsignmentDisposition ECD
	WHERE ECD.EquipmentConsignmentID = @EquipmentConsignmentID
	GROUP BY ECD.EquipmentConsignmentID
	ORDER BY ECD.EquipmentConsignmentID

	RETURN

END
GO
--End function procurement.GetEquipmentConsignmentDispositionQuantitiesByEquipmentConsignmentID

--Begin function procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID
EXEC utility.DropObject 'procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.10
-- Description:	A function to get a recipient name from a recipiententitytypecode and a recipiententityid
-- ======================================================================================================

CREATE FUNCTION procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID
(
@RecipientEntityTypeCode VARCHAR(50),
@RecipientEntityID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cRecipientName VARCHAR(100)

	SELECT @cRecipientName = 
		CASE 
			WHEN @RecipientEntityTypeCode = 'Asset'
			THEN (SELECT A.AssetName from asset.Asset A WHERE A.AssetID = @RecipientEntityID)
			WHEN @RecipientEntityTypeCode = 'Force'
			THEN (SELECT F.ForceName from force.Force F WHERE F.ForceID = @RecipientEntityID)
			WHEN @RecipientEntityTypeCode = 'Person'
			THEN person.FormatPersonNameByPersonID(@RecipientEntityID, 'LastFirst')
			ELSE NULL
		END

	RETURN ISNULL(@cRecipientName, '')

END
GO
--Begin function procurement.GetRecipientNameByRecipientEntityTypeCodeAndRecipientEntityID

--Begin function procurement.GetProjectNameByConsignmentCode
EXEC utility.DropObject 'procurement.GetProjectNameByConsignmentCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.08
-- Description:	A function to get a project name based on a consignmentcode
-- ========================================================================

CREATE FUNCTION procurement.GetProjectNameByConsignmentCode
(
@ConsignmentCode VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cProjectName VARCHAR(50)

	SELECT TOP 1 @cProjectName = P.ProjectName 
	FROM procurement.EquipmentOrder EO 
		JOIN dropdown.Project P ON P.ProjectID = EO.ProjectID 
		JOIN procurement.EquipmentConsignment EC ON EC.EquipmentOrderID = EO.EquipmentOrderID 
			AND EC.ConsignmentCode = @ConsignmentCode 
	ORDER BY P.ProjectName

	RETURN ISNULL(@cProjectName, '')

END
GO
--End function procurement.GetProjectNameByConsignmentCode

