USE HEROS
GO

--Begin procedure dropdown.GetPoliceForceData
EXEC utility.DropObject 'dropdown.GetPoliceForceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.08
-- Description:	A stored procedure to return data from the dropdown.PoliceForce table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetPoliceForceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PoliceForceID, 
		T.PoliceForceName
	FROM dropdown.PoliceForce T
	WHERE (T.PoliceForceID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PoliceForceName, T.PoliceForceID

END
GO
--End procedure dropdown.GetPoliceForceData

--Begin procedure dropdown.GetPoliceRankData
EXEC utility.DropObject 'dropdown.GetPoliceRankData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.08
-- Description:	A stored procedure to return data from the dropdown.PoliceRank table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetPoliceRankData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PoliceRankID, 
		T.PoliceRankName
	FROM dropdown.PoliceRank T
	WHERE (T.PoliceRankID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PoliceRankName, T.PoliceRankID

END
GO
--End procedure dropdown.GetPoliceRankData

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAPersonID,
		P.DefaultProjectID,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,		
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKWorkEligible,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		P.MiddleName,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR.PoliceRankID,
		PR.PoliceRankName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName,
		ISNULL((SELECT TOP 1 SCL.SecurityClearanceLevelName + ' - ' + core.FormatDate(PSC.LapseDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearanceLevel SCL ON SCL.SecurityClearanceLevelID = PSC.SecurityClearanceLevelID AND PSC.LapseDate > getDate() AND PSC.PersonID = P.PersonID ORDER BY SCL.DisplayOrder DESC), 'None') AS Vetting
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR ON PR.PoliceRankID = P.PoliceRankID
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
	WHERE P.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'Correspondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEXC
		JOIN dropdown.Country C ON C.CountryID = PEXC.CountryID
			AND PEXC.PersonID = @PersonID
	
	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertStatus
	SELECT
		ES.ExpertStatusID,
		ES.ExpertStatusName
	FROM person.PersonExpertStatus PES
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PES.ExpertStatusID
			AND ES.ExpertStatusID > 0
			AND PES.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonMedicalClearance
	SELECT
		newID() AS PersonMedicalClearanceGUID,
		MCT.MedicalClearanceTypeID,
		MCT.MedicalClearanceTypeName,
		PMC.ClearanceDate,
		core.FormatDate(PMC.ClearanceDate) AS ClearanceDateFormatted,
		PMC.ExpiryDate,
		core.FormatDate(PMC.ExpiryDate) AS ExpiryDateFormatted,
		PMC.PersonMedicalClearanceID,
		PMC.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'MedicalClearance'
				AND DE.EntityID = PMC.PersonMedicalClearanceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonMedicalClearance PMC
		JOIN dropdown.MedicalClearanceType MCT ON MCT.MedicalClearanceTypeID = PMC.MedicalClearanceTypeID
			AND PMC.PersonID = @PersonID
	ORDER BY PMC.ExpiryDate DESC, PMC.PersonMedicalClearanceID

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID
