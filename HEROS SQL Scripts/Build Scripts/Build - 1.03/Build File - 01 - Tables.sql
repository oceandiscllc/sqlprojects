USE HEROS
GO

--Begin table dropdown.PoliceForce
DECLARE @TableName VARCHAR(250) = 'dropdown.PoliceForce'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PoliceForce
	(
	PoliceForceID INT IDENTITY(0,1) NOT NULL,
	PoliceForceName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PoliceForceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PoliceForceName', 'DisplayOrder,PoliceForceName', 'PoliceForceID'
GO
--End table dropdown.PoliceForce

--Begin table dropdown.PoliceRank
DECLARE @TableName VARCHAR(250) = 'dropdown.PoliceRank'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PoliceRank
	(
	PoliceRankID INT IDENTITY(0,1) NOT NULL,
	PoliceRankName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PoliceRankID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PoliceRankName', 'DisplayOrder,PoliceRankName', 'PoliceRankID'
GO
--End table dropdown.PoliceRank

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'PoliceForceID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PoliceRankID', 'INT', '0'
GO
--End table person.Person
