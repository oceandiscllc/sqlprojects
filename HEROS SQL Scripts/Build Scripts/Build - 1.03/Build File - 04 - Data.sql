USE HEROS
GO

--Begin table dropdown.PoliceForce
TRUNCATE TABLE dropdown.PoliceForce
GO

EXEC utility.InsertIdentityValue 'dropdown.PoliceForce', 'PoliceForceID', 0
GO

INSERT INTO dropdown.PoliceForce 
	(PoliceForceName) 
VALUES
	('Avon and Somerset Constabulary'),
	('Bedfordshire Police'),
	('British Transport Police'),
	('Cambridgeshire Constabulary'),
	('Cheshire Constabulary'),
	('City of London Police'),
	('Civil Nuclear Constabulary'),
	('Cleveland Police'),
	('Cumbria Constabulary'),
	('Derbyshire Constabulary'),
	('Devon and Cornwall Police'),
	('Dorset Police'),
	('Durham Constabulary'),
	('Dyfed-Powys Police'),
	('Essex Police'),
	('Gloucestershire Constabulary'),
	('Greater Manchester Police'),
	('Gwent Police'),
	('Hampshire Constabulary'),
	('Hertfordshire Constabulary'),
	('Humberside Police'),
	('Kent Police'),
	('Lancashire Constabulary'),
	('Leicestershire Police'),
	('Lincolnshire Police'),
	('Merseyside Police'),
	('Metropolitan Police Service'),
	('Ministry of Defence Police'),
	('Norfolk Constabulary'),
	('North Wales Police'),
	('Northamptonshire Police'),
	('Northumbria Police'),
	('North Yorkshire Police'),
	('Nottinghamshire Police'),
	('Police Service of Northern Ireland'),
	('Police Service of Scotland'),
	('South Wales Police'),
	('South Yorkshire Police'),
	('Staffordshire Police'),
	('Suffolk Constabulary'),
	('Surrey Police'),
	('Sussex Police'),
	('Thames Valley Police'),
	('Warwickshire Police'),
	('West Mercia Police'),
	('West Midlands Police'),
	('West Yorkshire Police'),
	('Wiltshire Police')

GO
--End table dropdown.PoliceForce

--Begin table dropdown.PoliceRank
TRUNCATE TABLE dropdown.PoliceRank
GO

EXEC utility.InsertIdentityValue 'dropdown.PoliceRank', 'PoliceRankID', 0
GO

INSERT INTO dropdown.PoliceRank 
	(PoliceRankName, DisplayOrder) 
VALUES
	('Constable', 1),
	('Sergeant', 2),
	('Inspector', 3),
	('Chief Inspector', 4),
	('Superintendent', 5),
	('Chief Superintendent', 6),
	('Assistant Chief Constable/Commander', 7),
	('Deputy Chief Constable/Assistant Commissioner/Deputy Assistant Commissioner', 8),
	('Chief Constable/Commissioner', 9),
	('Detective Constable', 10),
	('Detective Sergeant', 11),
	('Detective Inspector', 12),
	('Detective Chief Inspector', 13),
	('Detective Superintendent', 14),
	('Detective Chief Superintendent', 14)
GO
--End table dropdown.PoliceRank