USE HermisCloud
GO

--Begin procedure document.GetEntityDocumentData
EXEC Utility.DropObject 'document.GetEntityDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return document data for an entity
-- =====================================================================
CREATE PROCEDURE document.GetEntityDocumentData

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		DE.EntityTypeSubCode,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID

END
GO
--End procedure document.GetEntityDocumentData

--Begin procedure dropdown.GetApplicantStatusData
EXEC Utility.DropObject 'dropdown.GetApplicantStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ApplicantStatus table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetApplicantStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ApplicantStatusID, 
		T.ApplicantStatusCode,
		T.ApplicantStatusName
	FROM dropdown.ApplicantStatus T
	WHERE (T.ApplicantStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ApplicantStatusName, T.ApplicantStatusID

END
GO
--End procedure dropdown.GetApplicantStatusData

--Begin procedure dropdown.GetExternalInterviewOutcomeData
EXEC Utility.DropObject 'dropdown.GetExternalInterviewOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ExternalInterviewOutcome table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetExternalInterviewOutcomeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ExternalInterviewOutcomeID, 
		T.ExternalInterviewOutcomeCode,
		T.ExternalInterviewOutcomeName
	FROM dropdown.ExternalInterviewOutcome T
	WHERE (T.ExternalInterviewOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ExternalInterviewOutcomeName, T.ExternalInterviewOutcomeID

END
GO
--End procedure dropdown.GetExternalInterviewOutcomeData

--Begin procedure dropdown.GetClientOrganizationData
EXEC Utility.DropObject 'dropdown.GetClientOrganizationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ClientOrganization table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetClientOrganizationData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientOrganizationID, 
		T.ClientOrganizationCode,
		T.ClientOrganizationName
	FROM dropdown.ClientOrganization T
	WHERE (T.ClientOrganizationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientOrganizationName, T.ClientOrganizationID

END
GO
--End procedure dropdown.GetClientOrganizationData

--Begin procedure dropdown.GetClientRegionData
EXEC Utility.DropObject 'dropdown.GetClientRegionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ClientRegion table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetClientRegionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientRegionID, 
		T.ClientRegionCode,
		T.ClientRegionName
	FROM dropdown.ClientRegion T
	WHERE (T.ClientRegionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientRegionName, T.ClientRegionID

END
GO
--End procedure dropdown.GetClientRegionData

--Begin procedure dropdown.GetClientVacancyRoleStatusData
EXEC Utility.DropObject 'dropdown.GetClientVacancyRoleStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ClientVacancyRoleStatus table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetClientVacancyRoleStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientVacancyRoleStatusID, 
		T.ClientVacancyRoleStatusCode,
		T.ClientVacancyRoleStatusName
	FROM dropdown.ClientVacancyRoleStatus T
	WHERE (T.ClientVacancyRoleStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientVacancyRoleStatusName, T.ClientVacancyRoleStatusID

END
GO
--End procedure dropdown.GetClientVacancyRoleStatusData

--Begin procedure dropdown.GetEmploymentTypeData
EXEC Utility.DropObject 'dropdown.GetEmploymentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.EmploymentType table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEmploymentTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EmploymentTypeID, 
		T.EmploymentTypeCode,
		T.EmploymentTypeName
	FROM dropdown.EmploymentType T
	WHERE (T.EmploymentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EmploymentTypeName, T.EmploymentTypeID

END
GO
--End procedure dropdown.GetEmploymentTypeData

--Begin procedure dropdown.GetImplementationDifficultyData
EXEC Utility.DropObject 'dropdown.GetImplementatonDifficultyData'
EXEC Utility.DropObject 'dropdown.GetImplementationDifficultyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.ImplementationDifficulty table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetImplementationDifficultyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImplementationDifficultyID, 
		T.ImplementationDifficultyCode,
		T.ImplementationDifficultyName
	FROM dropdown.ImplementationDifficulty T
	WHERE (T.ImplementationDifficultyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImplementationDifficultyName, T.ImplementationDifficultyID

END
GO
--End procedure dropdown.GetImplementationDifficultyData

--Begin procedure dropdown.GetImplementationImpactData
EXEC Utility.DropObject 'dropdown.GetImplementatonImpactData'
EXEC Utility.DropObject 'dropdown.GetImplementationImpactData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.ImplementationImpact table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetImplementationImpactData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImplementationImpactID, 
		T.ImplementationImpactCode,
		T.ImplementationImpactName
	FROM dropdown.ImplementationImpact T
	WHERE (T.ImplementationImpactID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImplementationImpactName, T.ImplementationImpactID

END
GO
--End procedure dropdown.GetImplementationImpactData

--Begin procedure dropdown.GetIncidentTypeCategoryData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.30
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MIN(T.IncidentTypeID) AS IncidentTypeCategoryID, 
		T.IncidentTypeCategory AS IncidentTypeCategoryCode,
		T.IncidentTypeCategory AS IncidentTypeCategoryName
	FROM dropdown.IncidentType T
	WHERE (T.IncidentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	GROUP BY T.IncidentTypeCategory
	ORDER BY T.IncidentTypeCategory, MIN(T.IncidentTypeID)

END
GO
--End procedure dropdown.GetIncidentTypeCategoryData

--Begin procedure dropdown.GetLessonCategoryData
EXEC Utility.DropObject 'dropdown.GetLessonCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetLessonCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonCategoryID, 
		T.LessonCategoryCode,
		T.LessonCategoryName
	FROM dropdown.LessonCategory T
	WHERE (T.LessonCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonCategoryName, T.LessonCategoryID

END
GO
--End procedure dropdown.GetLessonCategoryData

--Begin procedure dropdown.GetLessonPublicationStatusData
EXEC Utility.DropObject 'dropdown.GetLessonPublicationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.LessonPublicationStatus table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetLessonPublicationStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonPublicationStatusID, 
		T.LessonPublicationStatusCode,
		T.LessonPublicationStatusName
	FROM dropdown.LessonPublicationStatus T
	WHERE (T.LessonPublicationStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonPublicationStatusName, T.LessonPublicationStatusID

END
GO
--End procedure dropdown.GetLessonPublicationStatusData

--Begin procedure dropdown.GetLessonOwnerData
EXEC Utility.DropObject 'dropdown.GetLessonOwnerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.LessonOwner table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetLessonOwnerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonOwnerID, 
		T.LessonOwnerCode,
		T.LessonOwnerName
	FROM dropdown.LessonOwner T
	WHERE (T.LessonOwnerID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonOwnerName, T.LessonOwnerID

END
GO
--End procedure dropdown.GetLessonOwnerData

--Begin procedure dropdown.GetLessonStatusData
EXEC Utility.DropObject 'dropdown.GetLessonStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonStatus table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetLessonStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonStatusID, 
		T.LessonStatusCode,
		T.LessonStatusName
	FROM dropdown.LessonStatus T
	WHERE (T.LessonStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonStatusName, T.LessonStatusID

END
GO
--End procedure dropdown.GetLessonStatusData

--Begin procedure dropdown.GetLessonTypeData
EXEC Utility.DropObject 'dropdown.GetLessonTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetLessonTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonTypeID, 
		T.LessonTypeCode,
		T.LessonTypeName
	FROM dropdown.LessonType T
	WHERE (T.LessonTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonTypeName, T.LessonTypeID

END
GO
--End procedure dropdown.GetLessonTypeData

--Begin procedure dropdown.GetLessonUserClassData
EXEC Utility.DropObject 'dropdown.GetLessonUserClassData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.LessonUserClass table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetLessonUserClassData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonUserClassID, 
		T.LessonUserClassCode,
		T.LessonUserClassName
	FROM dropdown.LessonUserClass T
	WHERE (T.LessonUserClassID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonUserClassName, T.LessonUserClassID

END
GO
--End procedure dropdown.GetLessonUserClassData

--Begin procedure dropdown.GetMedicalClearanceReviewTypeData
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceReviewTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.17
-- Description:	A stored procedure to return data from the dropdown.MedicalClearanceReviewType table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetMedicalClearanceReviewTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MedicalClearanceReviewTypeID, 
		T.MedicalClearanceReviewTypeCode,
		T.MedicalClearanceReviewTypeName
	FROM dropdown.MedicalClearanceReviewType T
	WHERE (T.MedicalClearanceReviewTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MedicalClearanceReviewTypeName, T.MedicalClearanceReviewTypeID

END
GO
--End procedure dropdown.GetMedicalClearanceReviewTypeData

--Begin procedure dropdown.GetProjectLaborCodeData
EXEC Utility.DropObject 'dropdown.GetProjectLaborCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ProjectLaborCode table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetProjectLaborCodeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectLaborCodeID, 
		T.ProjectLaborCodeCode,
		T.ProjectLaborCodeName
	FROM dropdown.ProjectLaborCode T
	WHERE (T.ProjectLaborCodeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectLaborCodeName, T.ProjectLaborCodeID

END
GO
--End procedure dropdown.GetProjectLaborCodeData

--Begin procedure dropdown.GetPublishToData
EXEC Utility.DropObject 'dropdown.GetPublishToData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.PublishTo table
-- ================================================================================
CREATE PROCEDURE dropdown.GetPublishToData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PublishToID, 
		T.PublishToCode,
		T.PublishToName
	FROM dropdown.PublishTo T
	WHERE (T.PublishToID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PublishToName, T.PublishToID

END
GO
--End procedure dropdown.GetPublishToData

--Begin procedure dropdown.GetRecruitmentPathwayData
EXEC Utility.DropObject 'dropdown.GetRecruitmentPathwayData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RecruitmentPathway table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetRecruitmentPathwayData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RecruitmentPathwayID, 
		T.RecruitmentPathwayCode,
		T.RecruitmentPathwayName
	FROM dropdown.RecruitmentPathway T
	WHERE (T.RecruitmentPathwayID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RecruitmentPathwayName, T.RecruitmentPathwayID

END
GO
--End procedure dropdown.GetRecruitmentPathwayData

--Begin procedure dropdown.GetRegionalPriorityTierData
EXEC Utility.DropObject 'dropdown.GetRegionalPriorityTierData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RegionalPriorityTier table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetRegionalPriorityTierData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RegionalPriorityTierID, 
		T.RegionalPriorityTierCode,
		T.RegionalPriorityTierName
	FROM dropdown.RegionalPriorityTier T
	WHERE (T.RegionalPriorityTierID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RegionalPriorityTierName, T.RegionalPriorityTierID

END
GO
--End procedure dropdown.GetRegionalPriorityTierData

--Begin procedure dropdown.GetRoleCategoryData
EXEC Utility.DropObject 'dropdown.GetRoleCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RoleCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetRoleCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleCategoryID, 
		T.RoleCategoryCode,
		T.RoleCategoryName
	FROM dropdown.RoleCategory T
	WHERE (T.RoleCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleCategoryName, T.RoleCategoryID

END
GO
--End procedure dropdown.GetRoleCategoryData

--Begin procedure dropdown.GetRoleSubCategoryData
EXEC Utility.DropObject 'dropdown.GetRoleSubCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RoleSubCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetRoleSubCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleSubCategoryID, 
		T.RoleSubCategoryCode,
		T.RoleSubCategoryName
	FROM dropdown.RoleSubCategory T
	WHERE (T.RoleSubCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleSubCategoryName, T.RoleSubCategoryID

END
GO
--End procedure dropdown.GetRoleSubCategoryData

--Begin procedure dropdown.GetSecurityClearanceReviewTypeData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceReviewTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.SecurityClearanceReviewType table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceReviewTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SecurityClearanceReviewTypeID, 
		T.SecurityClearanceReviewTypeCode,
		T.SecurityClearanceReviewTypeName
	FROM dropdown.SecurityClearanceReviewType T
	WHERE (T.SecurityClearanceReviewTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SecurityClearanceReviewTypeName, T.SecurityClearanceReviewTypeID

END
GO
--End procedure dropdown.GetSecurityClearanceReviewTypeData

--Begin procedure dropdown.GetSecurityClearanceTypeData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.SecurityClearance table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MIN(T.SecurityClearanceID) AS SecurityClearanceTypeID, 
		T.SecurityClearanceCode AS SecurityClearanceTypeCode,
		T.SecurityClearanceCode AS SecurityClearanceTypeName
	FROM dropdown.SecurityClearance T
	WHERE (T.SecurityClearanceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	GROUP BY T.SecurityClearanceCode
	ORDER BY T.SecurityClearanceCode

END
GO
--End procedure dropdown.GetSecurityClearanceTypeData

--Begin procedure dropdown.GetTaskTypeData
EXEC Utility.DropObject 'dropdown.GetTaskTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.TaskType table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetTaskTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TaskTypeID, 
		T.TaskTypeCode,
		T.TaskTypeName
	FROM dropdown.TaskType T
	WHERE (T.TaskTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TaskTypeName, T.TaskTypeID

END
GO
--End procedure dropdown.GetTaskTypeData

--Begin procedure dropdown.GetThematicFocusData
EXEC Utility.DropObject 'dropdown.GetThematicFocusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ThematicFocus table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetThematicFocusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ThematicFocusID, 
		T.ThematicFocusCode,
		T.ThematicFocusName
	FROM dropdown.ThematicFocus T
	WHERE (T.ThematicFocusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ThematicFocusName, T.ThematicFocusID

END
GO
--End procedure dropdown.GetThematicFocusData

--Begin procedure eventlog.LogApplicationAction
EXEC utility.DropObject 'eventlog.LogApplicationAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.03.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogApplicationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Application',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogApplicationTable', 'u')) IS NOT NULL
			DROP TABLE #LogApplicationTable
		--ENDIF
		
		SELECT *
		INTO #LogApplicationTable
		FROM hrms.Application V
		WHERE V.ApplicationID = @EntityID
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Application',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Application'), ELEMENTS
			)
		FROM #LogApplicationTable T
			JOIN hrms.Application V ON V.ApplicationID = T.ApplicationID

		DROP TABLE #LogApplicationTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogApplicationAction

--Begin procedure eventlog.LogClientVacancyAction
EXEC utility.DropObject 'eventlog.LogClientVacancyAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientVacancyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ClientVacancy',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogClientVacancyTable', 'u')) IS NOT NULL
			DROP TABLE #LogClientVacancyTable
		--ENDIF
		
		SELECT *
		INTO #LogClientVacancyTable
		FROM hrms.ClientVacancy A
		WHERE A.ClientVacancyID = @EntityID

		DECLARE @cClientVacancyClientOrganization VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyClientOrganization = COALESCE(@cClientVacancyClientOrganization, '') + D.ClientVacancyClientOrganization 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyClientOrganization'), ELEMENTS) AS ClientVacancyClientOrganization
			FROM hrms.ClientVacancyClientOrganization T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyDeploymentManagerPerson VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyDeploymentManagerPerson = COALESCE(@cClientVacancyDeploymentManagerPerson, '') + D.ClientVacancyDeploymentManagerPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyDeploymentManagerPerson'), ELEMENTS) AS ClientVacancyDeploymentManagerPerson
			FROM hrms.ClientVacancyDeploymentManagerPerson T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyInterviewDateTime VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyInterviewDateTime = COALESCE(@cClientVacancyInterviewDateTime, '') + D.ClientVacancyInterviewDateTime 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyInterviewDateTime'), ELEMENTS) AS ClientVacancyInterviewDateTime
			FROM hrms.ClientVacancyInterviewDateTime T
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyRecruitmentManagerPerson VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyRecruitmentManagerPerson = COALESCE(@cClientVacancyRecruitmentManagerPerson, '') + D.ClientVacancyRecruitmentManagerPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyRecruitmentManagerPerson'), ELEMENTS) AS ClientVacancyRecruitmentManagerPerson
			FROM hrms.ClientVacancyRecruitmentManagerPerson T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyReviewerPerson VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyReviewerPerson = COALESCE(@cClientVacancyReviewerPerson, '') + D.ClientVacancyReviewerPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyReviewerPerson'), ELEMENTS) AS ClientVacancyReviewerPerson
			FROM hrms.ClientVacancyReviewerPerson T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyRoleCategory VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyRoleCategory = COALESCE(@cClientVacancyRoleCategory, '') + D.ClientVacancyRoleCategory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyRoleCategory'), ELEMENTS) AS ClientVacancyRoleCategory
			FROM hrms.ClientVacancyRoleCategory T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyRoleSubCategory VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyRoleSubCategory = COALESCE(@cClientVacancyRoleSubCategory, '') + D.ClientVacancyRoleSubCategory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyRoleSubCategory'), ELEMENTS) AS ClientVacancyRoleSubCategory
			FROM hrms.ClientVacancyRoleSubCategory T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancySkillsDesirable VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancySkillsDesirable = COALESCE(@cClientVacancySkillsDesirable, '') + D.ClientVacancySkillsDesirable 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancySkillsDesirable'), ELEMENTS) AS ClientVacancySkillsDesirable
			FROM hrms.ClientVacancyCompetency T 
			WHERE T.ClientVacancyID = @EntityID
			AND IsEssential <> 1
			) D

		DECLARE @cClientVacancySkillsEssential VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancySkillsEssential = COALESCE(@cClientVacancySkillsEssential, '') + D.ClientVacancySkillsEssential 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancySkillsEssential'), ELEMENTS) AS ClientVacancySkillsEssential
			FROM hrms.ClientVacancyCompetency T 
			WHERE T.ClientVacancyID = @EntityID
			AND IsEssential = 1
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ClientVacancy',
			@EntityID,
			@Comments,
			(
			SELECT T.*,

			CAST(('<ClientVacancyClientOrganization>' + ISNULL(@cClientVacancyClientOrganization,'') + '</ClientVacancyClientOrganization>') AS XML),
			CAST(('<ClientVacancyDeploymentManagerPerson>' + ISNULL(@cClientVacancyDeploymentManagerPerson,'') + '</ClientVacancyDeploymentManagerPerson>') AS XML),
			CAST(('<ClientVacancyInterviewDateTime>' + ISNULL(@cClientVacancyInterviewDateTime,'') + '</ClientVacancyInterviewDateTime>') AS XML),
			CAST(('<ClientVacancyRecruitmentManagerPerson>' + ISNULL(@cClientVacancyRecruitmentManagerPerson,'') + '</ClientVacancyRecruitmentManagerPerson>') AS XML),
			CAST(('<ClientVacancyReviewerPerson>' + ISNULL(@cClientVacancyReviewerPerson,'') + '</ClientVacancyReviewerPerson>') AS XML),
			CAST(('<ClientVacancyRoleCategory>' + ISNULL(@cClientVacancyRoleCategory,'') + '</ClientVacancyRoleCategory>') AS XML),
			CAST(('<ClientVacancyRoleSubCategory>' + ISNULL(@cClientVacancyRoleSubCategory,'') + '</ClientVacancyRoleSubCategory>') AS XML),
			CAST(('<ClientVacancySkillsDesirable>' + ISNULL(@cClientVacancySkillsDesirable,'') + '</ClientVacancySkillsDesirable>') AS XML),
			CAST(('<ClientVacancySkillsEssential>' + ISNULL(@cClientVacancySkillsEssential,'') + '</ClientVacancySkillsEssential>') AS XML)
			FOR XML RAW('ClientVacancy'), ELEMENTS
			)
		FROM #LogClientVacancyTable T
			JOIN hrms.ClientVacancy V ON V.ClientVacancyID = T.ClientVacancyID

		DROP TABLE #LogClientVacancyTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientVacancyAction

--Begin procedure eventlog.LogClientVacancyApplicationAction
EXEC utility.DropObject 'eventlog.LogClientVacancyApplicationAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE [eventlog].[LogClientVacancyApplicationAction]

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
	VALUES
		(
		@PersonID,
		@EventCode,
		'ClientVacancyApplication',
		@EntityID,
		@Comments
		)

END
GO
--End procedure eventlog.LogClientVacancyApplicationAction

--Begin procedure eventlog.LogLessonAction
EXEC utility.DropObject 'eventlog.LogLessonAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.03.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLessonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EventName VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventName)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Lesson',
			@EntityID,
			@Comments,
			@EventName
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogLessonTable', 'u')) IS NOT NULL
			DROP TABLE #LogLessonTable
		--ENDIF
		
		SELECT *
		INTO #LogLessonTable
		FROM lesson.Lesson V
		WHERE V.LessonID = @EntityID
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventName, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Lesson',
			@EntityID,
			@Comments,
			@EventName,
			(
			SELECT T.*
			FOR XML RAW('Lesson'), ELEMENTS
			)
		FROM #LogLessonTable T
			JOIN lesson.Lesson V ON V.LessonID = T.LessonID

		DROP TABLE #LogLessonTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLessonAction

--Begin procedure hrms.GetApplicationByApplicationID
EXEC utility.DropObject 'hrms.GetApplicationByApplicationID'
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.23
-- Description:	A stored procedure to return data from the hrms.Application table
-- ==============================================================================
CREATE PROCEDURE hrms.GetApplicationByApplicationID

@ApplicationID INT,
@PersonID INT = 0,
@VacancyID INT = 0,
@ClientVacancyID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @ApplicationID = 0
		BEGIN

		DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Application', @ApplicationID)

		--Application
		SELECT
			0 AS ApplicationID,
			NULL AS Availability,
			@ClientVacancyID AS ClientVacancyID,
			NULL AS ConflictsOfInterest,
			0 AS HasCertifiedApplication,
			0 AS HasConfirmedTerminationDate,
			CASE WHEN P.DAPersonID > 0 THEN 1 ELSE 0 END AS HasDAAccount,
			0 AS IsGovernmetEmployee,
			0 AS IsGuaranteedInterview,
			0 AS IsApplicationFinal,
			0 AS IsNotCriminalCivilService,
			0 AS IsNotCriminalPolice,
			0 AS IsDisabled,
			0 AS IsDueDilligenceComplete,
			0 AS IsEligible,
			0 AS IsHermisProfileUpdated,
			0 AS IsHRContactNotified,
			0 AS IsInterviewAvailableAMDay1,
			0 AS IsInterviewAvailableAMDay2,
			0 AS IsInterviewAvailableAMDay3,
			0 AS IsInterviewAvailableAMDay4,
			0 AS IsInterviewAvailableAMDay5,
			0 AS IsInterviewAvailableAMDay6,
			0 AS IsInterviewAvailableAMDay7,
			0 AS IsInterviewAvailablePMDay1,
			0 AS IsInterviewAvailablePMDay2,
			0 AS IsInterviewAvailablePMDay3,
			0 AS IsInterviewAvailablePMDay4,
			0 AS IsInterviewAvailablePMDay5,
			0 AS IsInterviewAvailablePMDay6,
			0 AS IsInterviewAvailablePMDay7,
			0 AS IsInterviewComplete,
			0 AS IsInterviewConfirmed,
			0 AS IsLineManagerNotified,
			0 AS IsOfferAccepted,
			0 AS IsOfferExtended,
			0 AS IsOnRoster,
			0 AS IsPreQualified,
			0 AS IsVetted,
			NULL AS ManagerComments,
			NULL AS Notes,
			@PersonID AS PersonID,
			person.FormatPersonNameByPersonID(@PersonID,'TitleFirstLast') AS PersonNameFormatted,
			NULL AS SubmittedDateTime,
			NULL AS SubmittedDateTimeFormatted,
			NULL AS Suitability,
			NULL AS TechnicalManagerComments,
			NULL AS TerminationDate,
			NULL AS TerminationDateFormatted,
			@VacancyID AS VacancyID,
			0 AS AgeRangeID,
			NULL AS AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			0 AS ExpertCategoryID,
			NULL AS ExpertCategoryName,
			0 AS GenderID,
			NULL AS GenderName,
			0 AS InterviewOutcomeID,
			NULL AS InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			PT.PersonTypeName,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName,
			'Not Submitted' AS WorkflowStepStatusName,
			'' AS IntervieweeEmailedDateTime,
			'' AS IntervieweeEmailedDateTimeFormatted
		FROM person.Person P
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND P.PersonID = @PersonID

		END
	ELSE
		BEGIN

		SELECT 
			@ClientVacancyID = A.ClientVacancyID,
			@PersonID = A.PersonID,
			@VacancyID = A.VacancyID
		FROM hrms.Application A 
		WHERE A.ApplicationID = @ApplicationID

		--Application
		SELECT
			A.ApplicationID,
			A.Availability,
			A.ClientVacancyID,
			A.ConflictsOfInterest,
			A.HasCertifiedApplication,
			A.HasConfirmedTerminationDate,
			A.HasDAAccount,
			A.IsGuaranteedInterview,
			A.IsApplicationFinal,
			A.IsNotCriminalCivilService,
			A.IsNotCriminalPolice,
			A.IsDisabled,
			A.IsDueDilligenceComplete,
			A.IsEligible,
			A.IsGovernmetEmployee,
			A.IsHermisProfileUpdated,
			A.IsHRContactNotified,
			A.IsInterviewAvailableAMDay1,
			A.IsInterviewAvailableAMDay2,
			A.IsInterviewAvailableAMDay3,
			A.IsInterviewAvailableAMDay4,
			A.IsInterviewAvailableAMDay5,
			A.IsInterviewAvailableAMDay6,
			A.IsInterviewAvailableAMDay7,
			A.IsInterviewAvailablePMDay1,
			A.IsInterviewAvailablePMDay2,
			A.IsInterviewAvailablePMDay3,
			A.IsInterviewAvailablePMDay4,
			A.IsInterviewAvailablePMDay5,
			A.IsInterviewAvailablePMDay6,
			A.IsInterviewAvailablePMDay7,
			A.IsInterviewComplete,
			A.IsInterviewConfirmed,
			A.IsLineManagerNotified,
			A.IsOfferAccepted,
			A.IsOfferExtended,
			A.IsOnRoster,
			A.IsPreQualified,
			A.IsVetted,
			A.ManagerComments,
			A.Notes,
			A.PersonID,
			person.FormatPersonNameByPersonID(A.PersonID,'TitleFirstLast') AS PersonNameFormatted,
			A.SubmittedDateTime,
			core.FormatDate(A.SubmittedDateTime) AS SubmittedDateTimeFormatted,
			A.Suitability,
			A.TechnicalManagerComments,
			A.TerminationDate,
			core.FormatDate(A.TerminationDate) AS TerminationDateFormatted,
			A.VacancyID,
			AR.AgeRangeID,
			AR.AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			EC.ExpertCategoryID,
			EC.ExpertCategoryName,
			G.GenderID,
			G.GenderName,
			IO.InterviewOutcomeID,
			IO.InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			PT.PersonTypeName,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName,
			workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID('Application', A.ApplicationID, 'Not Submitted') AS WorkflowStepStatusName,
			(SELECT TOP 1 SentDateTime FROM core.PendingEmail WHERE ApplicationID = A.ApplicationID AND EmailTemplateCode = 'InterviewScheduled') AS IntervieweeEmailedDateTime,
			core.FormatDateTime((SELECT TOP 1 SentDateTime FROM core.PendingEmail WHERE EntityID = A.ApplicationID AND EmailTemplateCode = 'InterviewScheduled' ORDER BY SentDateTime DESC)) AS IntervieweeEmailedDateTimeFormatted
		FROM hrms.Application A
			JOIN person.Person P ON P.PersonID = A.PersonID
			JOIN dropdown.AgeRange AR ON AR.AgeRangeID = A.AgeRangeID
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = A.ExpertCategoryID
			JOIN dropdown.Gender G ON G.GenderID = A.GenderID
			JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = A.InterviewOutcomeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND A.ApplicationID = @ApplicationID

		END
	--ENDIF

	IF @ClientVacancyID = 0
		BEGIN

		--ApplicationDocumentCV
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'Application'
				AND DE.EntityTypeSubCode = 'CV'
				AND DE.EntityID = @ApplicationID

		--ApplicationDocumentPassport
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'Application'
				AND DE.EntityTypeSubCode = 'Passport'
				AND DE.EntityID = @ApplicationID

		--ApplicationEthnicity
		SELECT 
			E.EthnicityCategoryName,
			E.EthnicityID,
			E.EthnicityName,
			AE.EthnicityDescription
		FROM hrms.ApplicationEthnicity AE
			JOIN dropdown.Ethnicity E ON E.EthnicityID = AE.EthnicityID
				AND AE.ApplicationID = @ApplicationID
		ORDER BY E.EthnicityCategorDisplayOrder, E.EthnicityDisplayOrder, E.EthnicityName, E.EthnicityID

		--ApplicationInterviewAvailability
		SELECT 
			AIA.ApplicationInterviewAvailabilityID,
			AIA.ApplicationInterviewScheduleID,
			AIA.ApplicationPersonID,
			AIA.IsAvailable,
			core.yesNoFormat(AIA.IsAvailable) AS IsAvailableYesNoFormat,
			AIA.ApplicationID,
			AIA.PersonID
		FROM hrms.ApplicationInterviewAvailability AIA
		WHERE AIA.ApplicationPersonID IN (SELECT AP.ApplicationPersonID FROM hrms.ApplicationPerson AP WHERE AP.ApplicationID = @ApplicationID)

		--ApplicationInterviewSchedule
		SELECT 
			AIS.ApplicationInterviewScheduleID,
			AIS.InterviewDateTime,
			core.FormatDateTime(AIS.InterviewDateTime) AS InterviewDateTimeFormatted,
			AIS.IsSelectedDate,
			core.YesNoFormat(AIS.IsSelectedDate) AS IsSelectedDateYesNoFormat
		FROM hrms.ApplicationInterviewSchedule AIS
		WHERE AIS.ApplicationID = @ApplicationID

	--ApplicationPerson
		SELECT 
			AP.ApplicationID,
			AP.ApplicationPersonID,
			AP.InterviewOutcomeID,
			AP.IsScheduleConfirmed,
			core.YesNoFormat(AP.IsScheduleConfirmed) AS IsScheduleConfirmedYesNoFormat,
			AP.PersonID,
			person.FormatPersonNameByPersonID(AP.PersonID, 'FirstLast') AS PersonNameFormatted,
			AP.PersonTypeCode,
			AP.ProposesInterviewDates,
			core.YesNoFormat(AP.ProposesInterviewDates) AS ProposesInterviewDatesYesNoFormat,
			IO.InterviewOutcomeName,
			P.FirstName,
			P.LastName
		FROM hrms.ApplicationPerson AP
			JOIN person.Person P ON P.PersonID = AP.PersonID
			JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = AP.InterviewOutcomeID
				AND AP.ApplicationID = @ApplicationID
		ORDER BY P.LastName, P.FirstName

		--InterviewCommentsDocument
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
			DE.EntityTypeSubCode AS FileEntityTypeSubCode
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'InterviewComments'
				AND DE.EntityID = @ApplicationID

		--PersonApplicantSource
		SELECT
			APS.ApplicantSourceID,
			APS.ApplicantSourceName
		FROM person.PersonApplicantSource PAS
			JOIN dropdown.ApplicantSource APS ON APS.ApplicantSourceID = PAS.ApplicantSourceID
				AND PAS.PersonID = @PersonID
		ORDER BY APS.DisplayOrder, APS.ApplicantSourceName, APS.ApplicantSourceID

		--PersonCountry
		SELECT
			C.CountryID,
			C.CountryName
		FROM person.PersonCountry PC
			JOIN dropdown.Country C ON C.CountryID = PC.CountryID
				AND PC.PersonID = @PersonID
		ORDER BY C.CountryName, C.CountryID

		--PersonLanguage
		SELECT
			L.LanguageName,
			LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
			LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
			LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
			PL.IsVerified
		FROM person.PersonLanguage PL
			JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
			JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
			JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
				AND PL.PersonID = @PersonID
		ORDER BY L.LanguageName

		--Vacancy
		SELECT
			EC.ExpertCategoryName,
			P.ProjectName,
			PS.ProjectSponsorName,
			V.ProjectID,
			V.VacancyName,
			V.VacancyReference,
			VT.VacancyTypeName
		FROM hrms.Vacancy V
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = V.ExpertCategoryID
			JOIN dropdown.Project P ON P.ProjectID = V.ProjectID
			JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
			JOIN dropdown.VacancyType VT ON VT.VacancyTypeID = V.VacancyTypeID
				AND V.VacancyID = @VacancyID

		--VacancyCompetency
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAAC.Suitability,
			OAAC.SuitabilityRatingName,
			ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
		FROM hrms.VacancyCompetency VC
			JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
			JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
				AND VC.VacancyID = @VacancyID
				AND CT.CompetencyTypeCode = 'Competency'
				AND C.IsActive = 1
			OUTER APPLY
				(
				SELECT
					AC.Suitability,
					AC.SuitabilityRatingID,
					SR.SuitabilityRatingName
				FROM hrms.ApplicationCompetency AC
					JOIN dropdown.SuitabilityRating SR ON SR.SuitabilityRatingID = AC.SuitabilityRatingID
						AND AC.CompetencyID = C.CompetencyID
						AND AC.ApplicationID = @ApplicationID
				) OAAC
		ORDER BY C.CompetencyName, C.CompetencyID

		--VacancyTechnicalCapability
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAAC.Suitability,
			ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
		FROM hrms.VacancyCompetency VC
			JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
			JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
				AND VC.VacancyID = @VacancyID
				AND CT.CompetencyTypeCode = 'TechnicalCapability'
				AND C.IsActive = 1
			OUTER APPLY
				(
				SELECT
					AC.Suitability,
					AC.SuitabilityRatingID,
					SR.SuitabilityRatingName
				FROM hrms.ApplicationCompetency AC
					JOIN dropdown.SuitabilityRating SR ON SR.SuitabilityRatingID = AC.SuitabilityRatingID
						AND AC.CompetencyID = C.CompetencyID
						AND AC.ApplicationID = @ApplicationID

				) OAAC
		ORDER BY C.CompetencyName, C.CompetencyID

		--WorkflowData
		EXEC workflow.GetEntityWorkflowData 'Application', @ApplicationID

		--WorkflowPeople
		EXEC workflow.GetEntityWorkflowPeople 'Application', @ApplicationID, @nWorkflowStepNumber

		--WorkflowEventLog
		SELECT
			EL.EventLogID,
			person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
			core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		
			CASE
				WHEN EL.EventCode = 'create'
				THEN 'Created Person Update'
				WHEN EL.EventCode = 'decrementworkflow'
				THEN 'Rejected Person Update'
				WHEN EL.EventCode = 'incrementworkflow'
				THEN 'Approved Person Update'
				WHEN EL.EventCode = 'update'
				THEN 'Updated Person Update'
			END AS EventAction,
		
			EL.Comments
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode = 'Application'
			AND EL.EntityID = @ApplicationID
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
		ORDER BY EL.CreateDateTime

		END
	ELSE
		BEGIN

		--ApplicationContactData
		SELECT
			ACD.ContactEmailAddress,
			ACD.ContactJobTitle,
			ACD.ContactName,
			ACD.ContactPhone,
			ACD.ContactTypeCode
		FROM hrms.ApplicationContactData ACD
		WHERE ACD.ApplicationID = @ApplicationID
		ORDER BY ACD.ContactTypeCode

		--ApplicationDocumentContingentLiabilityProforma
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'Application'
				AND DE.EntityTypeSubCode = 'ContingentLiabilityProforma'
				AND DE.EntityID = @ApplicationID

		--ClientVacancy
		SELECT
			CV.RoleName,
			CV.TaskCode,
			CV.TaskLocation
		FROM hrms.ClientVacancy CV
		WHERE CV.ClientVacancyID = @ClientVacancyID

		--ClientVacancyCompetencyDesired
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAS.Suitability
		FROM hrms.ClientVacancyCompetency CVC
			JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
				AND CVC.ClientVacancyID = @ClientVacancyID
				AND CVC.IsEssential = 0
			OUTER APPLY
				(
				SELECT
					AC.Suitability
				FROM hrms.ApplicationCompetency AC
				WHERE AC.ApplicationID = @ApplicationID
					AND AC.CompetencyID = C.CompetencyID
				) OAS

		--ClientVacancyCompetencyEssential
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAS.Suitability
		FROM hrms.ClientVacancyCompetency CVC
			JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
				AND CVC.ClientVacancyID = @ClientVacancyID
				AND CVC.IsEssential = 1
			OUTER APPLY
				(
				SELECT
					AC.Suitability
				FROM hrms.ApplicationCompetency AC
				WHERE AC.ApplicationID = @ApplicationID
					AND AC.CompetencyID = C.CompetencyID
				) OAS

		--ClientVacancyInterviewDateTime
		SELECT
			IIF (EXISTS (SELECT 1 FROM HRMS.ApplicationInterviewAvailability AIA WHERE AIA.ClientVacancyInterviewDateTimeID = CVIDT.ClientVacancyInterviewDateTimeID), 1, 0) AS IsAvailable,
			CVIDT.ClientVacancyInterviewDateTimeID,
			CVIDT.InterviewDateTime,
			core.FormatDateTime(CVIDT.InterviewDateTime) AS InterviewDateTimeFormatted
		FROM hrms.ClientVacancyInterviewDateTime CVIDT
		WHERE CVIDT.ClientVacancyID = @ClientVacancyID

		END
	--ENDIF

END
GO
--End procedure hrms.GetApplicationByApplicationID

--Begin procedure hrms.GetClientVacancyByClientVacancyID
EXEC utility.DropObject 'hrms.GetClientVacancyByClientVacancyID'
GO
-- =============================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.26
-- Description:	A stored procedure to get data from the hrms.ClientVacancy table
-- =============================================================================
CREATE PROCEDURE hrms.GetClientVacancyByClientVacancyID

@ClientVacancyID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ClientVacancy', @ClientVacancyID)
	DECLARE @nApplicationWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ClientVacancyApplication', @ClientVacancyID)

	--ClientVacancy
	SELECT
		CV.ClientVacancyID,
	--task columns
		CV.TaskName,
		CV.ClientPersonID,
		person.FormatPersonNameByPersonID(CV.ClientPersonID, 'FirstLast') AS ClientPersonNameFormatted,
		CV.TaskCode,
		CV.TaskStartDate,
		core.FormatDate(CV.TaskStartDate) AS TaskStartDateFormatted,
		CV.TaskEndDate,
		core.FormatDate(CV.TaskEndDate) AS TaskEndDateFormatted,
		CV.CountryID,
		C.CountryName,
		CV.TaskOwnerPersonID,
		person.FormatPersonNameByPersonID(CV.TaskOwnerPersonID, 'FirstLast') AS TaskOwnerPersonNameFormatted,
		CV.RegionalPriorityTierID,
		RPT.RegionalPriorityTierName,
		CV.ClientLocation,
		CV.ClientRegionID,
		CR.ClientRegionName,
		CV.ClientDepartment,
		CV.ClientContact1Name,
		CV.ClientContact1Team,
		CV.ClientContact1Email,
		CV.ClientContact1Phone,
		CV.ClientContact2Name,
		CV.ClientContact2Team,
		CV.ClientContact2Email,
		CV.ClientContact2Phone,
		CV.RecruitmentPathwayID,
		RP.RecruitmentPathwayName,
		CV.TaskLocation,
	--role details columns
		CV.RoleName,
		CV.RoleCode,
		CV.ApplicationOpenDate,
		core.FormatDate(CV.ApplicationOpenDate) AS ApplicationOpenDateFormatted,
		CV.ApplicationCloseDate,
		core.FormatDate(CV.ApplicationCloseDate) AS ApplicationCloseDateFormatted,
		CV.ClientVacancyRoleStatusID,
		CVRS.ClientVacancyRoleStatusName,
		CV.ProjectLaborCodeID,
		PLC.ProjectLaborCodeName,
		CV.RoleTeam,
		CV.PositionCount,
		CV.ExpectedStartDate,
		core.FormatDate(CV.ExpectedStartDate) AS ExpectedStartDateFormatted,
		CV.ExpectedEndDate,
		core.FormatDate(CV.ExpectedEndDate) AS ExpectedEndDateFormatted,
		CV.WorkingDaysCount,
		CV.HoursWorkedPerDay,
		CV.Salary,
		CV.EmploymentTypeID,
		ET.EmploymentTypeName,
		CV.ThematicFocusID,
		TF.ThematicFocusName,
		CV.TaskTypeID,
		TT.TaskTypeName,
		CV.PublishToID,
		PT.PublishToName,
		CV.SecurityClearanceTypeID,
		SC.SecurityClearanceCode AS SecurityClearanceTypeName,
		CV.OperationalTrainingCourseID,
		OTT.OperationalTrainingCourseName,
		CV.IsFirearmsTrainingRequired,
		core.YesNoFormat(CV.IsFirearmsTrainingRequired) AS IsFirearmsTrainingRequiredYesNoFormat,
	--Role Information
		CV.RoleContext,
		CV.KeyDeliverables,
		CV.SiftDate,
		core.FormatDate(CV.SiftDate) AS SiftDateFormatted,
		CV.InterviewShortlistDate,
		core.FormatDate(CV.InterviewShortlistDate) AS InterviewShortlistDateFormatted,
		CV.AdvertisedInterviewDate,
		core.FormatDate(CV.AdvertisedInterviewDate) AS AdvertisedInterviewDateFormatted,
		CV.RoleNotes,
		CV.VacancyStatusID,
		VS.VacancyStatusName,
		--uncomment once the application table is in place
		--(SELECT COUNT (PersonID) FROM hrms.ClientVacancyApplication CVA WHERE CVA.ClientVacancyID = CV.ClinetVacancyID) AS ApplicationCount,
		0 AS ApplicationCount,
		iif(RP.RecruitmentPathwayName='Direct','N/A',
		iif(
			ApplicationOpenDate IS NULL OR ApplicationCloseDate IS NULL OR ApplicationOpenDate > getDate(), 0, 
			iif(ApplicationCloseDate<getDate(),
				DATEDIFF(d, ApplicationOpenDate, ApplicationCloseDate),
				DATEDIFF(d, ApplicationOpenDate, getDate())
			)
			)	
		) AS DaysAdvertised,
		workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID('ClientVacancy', CV.ClientVacancyID, 'Not Submitted') AS WorkflowStepStatusName,
		workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID('ClientVacancyApplication', CV.ClientVacancyID, 'Not Submitted') AS ApplicationWorkflowStepStatusName

	FROM hrms.ClientVacancy CV
		JOIN dropdown.ClientRegion CR ON CR.ClientRegionID = CV.ClientRegionID
		JOIN dropdown.ClientVacancyRoleStatus CVRS ON CVRS.ClientVacancyRoleStatusID = CV.ClientVacancyRoleStatusID
		JOIN dropdown.Country C ON C.CountryID = CV.CountryID
		JOIN dropdown.EmploymentType ET ON ET.EmploymentTypeID = CV.EmploymentTypeID
		JOIN dropdown.OperationalTrainingCourse OTT ON OTT.OperationalTrainingCourseID = CV.OperationalTrainingCourseID
		JOIN dropdown.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = CV.ProjectLaborCodeID
		JOIN dropdown.PublishTo PT ON PT.PublishToID = CV.PublishToID
		JOIN dropdown.RecruitmentPathway RP ON RP.RecruitmentPathwayID = CV.RecruitmentPathwayID
		JOIN dropdown.RegionalPriorityTier RPT ON RPT.RegionalPriorityTierID = CV.RegionalPriorityTierID
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = CV.SecurityClearanceTypeID
		JOIN dropdown.TaskType TT ON TT.TaskTypeID = CV.TaskTypeID
		JOIN dropdown.ThematicFocus TF ON TF.ThematicFocusID = CV.ThematicFocusID
		JOIN dropdown.VacancyStatus VS ON VS.VacancyStatusID = CV.VacancyStatusID
			AND CV.ClientVacancyID = @ClientVacancyID

	--ClientVacancyClientOrganization
	SELECT
		CO.ClientOrganizationID,
		CO.ClientOrganizationName,
		CO.ClientOrganizationCode
	FROM dropdown.ClientOrganization CO
		JOIN hrms.ClientVacancyClientOrganization CVCO ON CVCO.ClientOrganizationID = CO.ClientOrganizationID
	WHERE CVCO.ClientVacancyID = @ClientVacancyID
	ORDER BY CO.DisplayOrder

	--ClientVacancyDeploymentManagerPerson	
	SELECT 
		DMP.ClientVacancyDeploymentManagerPersonID,
		DMP.PersonID,
		person.FormatPersonNameByPersonID(DMP.PersonID,'LastFirst') AS PersonNameFormatted
	FROM hrms.ClientVacancyDeploymentManagerPerson DMP
	WHERE DMP.ClientVacancyID = @ClientVacancyID
	
	--ClientVacancyDocument
	EXEC document.GetEntityDocumentData 'ClientVacancy', @ClientVacancyID

	--ClientVacancyInterviewDateTime
	SELECT 
		CVID.ClientVacancyInterviewDateTimeID,
		CVID.InterviewDateTime,
		core.FormatDateTime(CVID.InterviewDateTime) AS InterviewDateTimeFormatted
	FROM hrms.ClientVacancyInterviewDateTime CVID
	WHERE CVID.ClientVacancyID = @ClientVacancyID

	--ClientVacancyRecruitmentManagerPerson
	SELECT 
		RMP.ClientVacancyRecruitmentManagerPersonID,
		RMP.PersonID,
		person.FormatPersonNameByPersonID(RMP.PersonID,'LastFirst') AS PersonNameFormatted
	FROM hrms.ClientVacancyRecruitmentManagerPerson RMP
	WHERE RMP.ClientVacancyID = @ClientVacancyID

	--ClientVacancyReviewerPerson
	SELECT 
		RP.ClientVacancyReviewerPersonID,
		RP.PersonID,
		person.FormatPersonNameByPersonID(RP.PersonID,'LastFirst') AS PersonNameFormatted
	FROM hrms.ClientVacancyReviewerPerson RP
	WHERE RP.ClientVacancyID = @ClientVacancyID

	--ClientVacancyRoleCategory
	SELECT
		RC.RoleCategoryID,
		RC.RoleCategoryName,
		RC.RoleCategoryCode
	FROM dropdown.RoleCategory RC
		JOIN hrms.ClientVacancyRoleCategory CVRC ON CVRC.RoleCategoryID = RC.RoleCategoryID
	WHERE CVRC.ClientVacancyID = @ClientVacancyID
	ORDER BY RC.DisplayOrder

	--ClientVacancyRoleSubCategory
	SELECT
		RC.RoleSubCategoryID,
		RC.RoleSubCategoryName,
		RC.RoleSubCategoryCode
	FROM dropdown.RoleSubCategory RC
		JOIN hrms.ClientVacancyRoleSubCategory CVRC ON CVRC.RoleSubCategoryID = RC.RoleSubCategoryID
	WHERE CVRC.ClientVacancyID = @ClientVacancyID
	ORDER BY RC.DisplayOrder

	--ClientVacancySkillsDesirable
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		CVC.CompetencyID,
		CVC.ClientVacancyCompetencyID
	FROM hrms.ClientVacancyCompetency CVC
		JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND CVC.ClientVacancyID = @ClientVacancyID
			AND CVC.IsEssential <> 1

	--ClientVacancySkillsEssential
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		CVC.CompetencyID,
		CVC.ClientVacancyCompetencyID
	FROM hrms.ClientVacancyCompetency CVC
		JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND CVC.ClientVacancyID = @ClientVacancyID
			AND CVC.IsEssential = 1
	
	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'ClientVacancy', @ClientVacancyID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'ClientVacancy', @ClientVacancyID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created ClientVacancy'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected ClientVacancy'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved ClientVacancy'
			WHEN EL.EventCode = 'update'
			THEN 'Updated ClientVacancy'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode LIKE 'ClientVacancy%'
		AND EL.EntityID = @ClientVacancyID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
	
	--Workflow for Client Vacancy Application Handling
	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'ClientVacancyApplication', @ClientVacancyID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'ClientVacancyApplication', @ClientVacancyID, @nApplicationWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created ClientVacancy'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected ClientVacancy'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved ClientVacancy'
			WHEN EL.EventCode = 'update'
			THEN 'Updated ClientVacancy'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode LIKE 'ClientVacancyApplication'
		AND EL.EntityID = @ClientVacancyID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure hrms.GetClientVacancyByClientVacancyID

--Begin procedure lesson.GetLessonActionByLessonActionID
EXEC Utility.DropObject 'lesson.GetLessonActionByLessonActionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2019.03.12
-- Description:	A procedure to return data from the lesson.LessonAction table
-- ==========================================================================
CREATE PROCEDURE lesson.GetLessonActionByLessonActionID

@LessonActionID INT

AS
BEGIN
  SET NOCOUNT ON;

	--LessonAction
	SELECT
		'MEAL-' + RIGHT('00000' + CAST(L.LessonID AS VARCHAR(5)), 5) AS LessonIDFormatted,
		LA.CreateDateTime,
		core.FormatDate(LA.CreateDateTime) AS CreateDateTimeFormatted,
		LA.CreatePersonID,
		person.FormatPersonNameByPersonID(LA.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		LA.DueDate,
		core.FormatDate(LA.DueDate) AS DueDateFormatted,
		LA.IsComplete,
		core.YesNoFormat(LA.IsComplete) AS IsCompleteFormatted,
		LA.LessonActionDescription,
		LA.LessonActionID,
		LA.LessonActionName,
		LA.LessonActionPersonID,
		person.FormatPersonNameByPersonID(LA.LessonActionPersonID, 'LastFirst') AS LessonActionPersonNameFormatted,
		LA.LessonID,
		LA.UpdateDateTime,
		core.FormatDate(LA.UpdateDateTime) AS UpdateDateTimeFormatted,
		LA.UpdatePersonID,
		person.FormatPersonNameByPersonID(LA.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted,
		P.ProjectName
	FROM lesson.LessonAction LA
		JOIN lesson.Lesson L ON L.LessonID = LA.LessonID
		JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
			AND LA.LessonActionID = @LessonActionID

END
GO
--End procedure lesson.GetLessonActionByLessonActionID

--Begin procedure lesson.GetLessonByLessonID
EXEC Utility.DropObject 'lesson.GetLessonByLessonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date: 2019.03.12
-- Description:	A procedure to return data from the lesson.Lesson table
-- ====================================================================
CREATE PROCEDURE lesson.GetLessonByLessonID

@LessonID INT

AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Lesson', @LessonID)

	--Lesson
	SELECT
		ID.ImplementationDifficultyID,
		ID.ImplementationDifficultyName,
		II.ImplementationImpactID,
		II.ImplementationImpactName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		L.Cause,
		L.ClientCode,
		L.CreatePersonID,
		person.FormatPersonNameByPersonID(L.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		L.LessonDate,
		core.FormatDate(L.LessonDate) AS LessonDateFormatted,
		L.Event,
		L.Impact,
		L.Indicators,
		L.IsActive,
		L.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(L.ISOCountryCode2) AS CountryName,
		L.ISOCurrencyCode,
		L.LessonDescription,
		L.LessonID,
		LPS.LessonPublicationStatusCode + '-' + RIGHT('00000' + CAST(L.LessonID AS VARCHAR(5)), 5) AS LessonIDFormatted,
		workflow.IsWorkflowComplete('Lesson', L.LessonID) AS IsWorkflowComplete,
		L.LessonName,
		L.Recommendations,
		L.ScaleOfResponse,
		LC.LessonCategoryID,
		LC.LessonCategoryName,
		LO.LessonOwnerID,
		LO.LessonOwnerName,
		LPS.LessonPublicationStatusID,
		LPS.LessonPublicationStatusName,
		LS.LessonStatusID,
		LS.LessonStatusName,
		LT.LessonTypeID,
		LT.LessonTypeName,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectSponsorID,
		PS.ProjectSponsorName,
		@nWorkflowStepNumber AS WorkflowStepNumber
	FROM lesson.Lesson L
		JOIN dropdown.ImplementationDifficulty ID ON ID.ImplementationDifficultyID = L.ImplementationDifficultyID
		JOIN dropdown.ImplementationImpact II ON II.ImplementationImpactID = L.ImplementationImpactID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = L.IncidentTypeID
		JOIN dropdown.LessonCategory LC ON LC.LessonCategoryID = L.LessonCategoryID
		JOIN dropdown.LessonOwner LO ON LO.LessonOwnerID = L.LessonOwnerID
		JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = L.LessonPublicationStatusID
		JOIN dropdown.LessonStatus LS ON LS.LessonStatusID = L.LessonStatusID
		JOIN dropdown.LessonType LT ON LT.LessonTypeID = L.LessonTypeID
		JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = L.ProjectSponsorID
			AND L.LessonID = @LessonID

	--LessonDocument
	EXEC document.GetEntityDocumentData 'Lesson', @LessonID

	--LessonLessonUserClass
	SELECT
		LUC.LessonUserClassID,
		LUC.LessonUserClassName
	FROM dropdown.LessonUserClass LUC
		JOIN lesson.LessonLessonUserClass LLUC ON LLUC.LessonUserClassID = LUC.LessonUserClassID
			AND LLUC.LessonID = @LessonID

	--LessonWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Lesson', @LessonID

	--LessonWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		(SELECT EmailAddress FROM Person.Person WHERE PersonID  = EL.PersonID) as EmailAddress,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EventName AS EventAction,
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN lesson.Lesson L ON L.LessonID = EL.EntityID
			AND L.LessonID = @LessonID
			AND EL.EntityTypeCode = 'Lesson'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--LessonWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Lesson', @LessonID, @nWorkflowStepNumber

END
GO
--End procedure lesson.GetLessonByLessonID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Activity'
		SELECT @bHasAccess = 1 FROM activity.Activity T WHERE T.ActivityID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ActivityReport'
		SELECT @bHasAccess = 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Asset'
		SELECT @bHasAccess = 1 FROM asset.Asset T WHERE T.AssetID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ClientVacancy'
		SELECT @bHasAccess = 1 FROM hrms.ClientVacancy CV WHERE CV.ClientVacancyID = @EntityID
	ELSE IF @EntityTypeCode = 'ClientVacancyApplication'
		SELECT @bHasAccess = 1 FROM hrms.ClientVacancy CV WHERE CV.ClientVacancyID = @EntityID
	ELSE IF @EntityTypeCode = 'Contact'
		SELECT @bHasAccess = 1 FROM contact.Contact T WHERE T.ContactID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Course'
		SELECT @bHasAccess = 1 FROM training.Course T WHERE T.CourseID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Document'
		SELECT @bHasAccess = 1 FROM document.Document T WHERE T.DocumentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentCatalog'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentCatalog T WHERE T.EquipmentCatalogID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentItem'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentItem T WHERE T.EquipmentItemID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentOrder'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentOrder T WHERE T.EquipmentOrderID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentMovement'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentMovement T WHERE T.EquipmentMovementID = @EntityID
	ELSE IF @EntityTypeCode = 'Feedback'
		SELECT @bHasAccess = 1 FROM person.Feedback T WHERE T.FeedbackID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Force'
		SELECT @bHasAccess = 1 FROM force.Force T WHERE T.ForceID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Incident'
		SELECT @bHasAccess = 1 FROM core.Incident T WHERE T.IncidentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Lesson'
		SELECT @bHasAccess = 1 FROM lesson.Lesson T JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = T.LessonPublicationStatusID AND LPS.LessonPublicationStatusCode = 'LP'
	ELSE IF @EntityTypeCode = 'Module'
		SELECT @bHasAccess = 1 FROM training.Module T WHERE T.ModuleID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'PersonUpdate'
		SELECT @bHasAccess = 1 FROM personupdate.PersonUpdate PU WHERE PU.PersonUpdateID = @EntityID
	ELSE IF @EntityTypeCode = 'ReportUpdate'
		SELECT @bHasAccess = 1 FROM core.ReportUpdate T WHERE T.ReportUpdateID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ResponseDashboard'
		SELECT @bHasAccess = 1 FROM responsedashboard.ResponseDashboard T WHERE T.ResponseDashboardID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'SpotReport'
		SELECT @bHasAccess = 1 FROM spotreport.SpotReport T WHERE T.SpotReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Territory'
		SELECT @bHasAccess = 1 FROM territory.Territory T JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode AND T.TerritoryID = @EntityID AND TT.IsReadOnly = 0
	ELSE IF @EntityTypeCode = 'TrendReport'
		SELECT @bHasAccess = 1 FROM trendreport.TrendReport T WHERE T.TrendReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Vacancy'
		SELECT @bHasAccess = 1 FROM hrms.Vacancy V WHERE V.VacancyID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = V.ProjectID)
	ELSE IF @EntityTypeCode = 'VacancyApplication'
		SELECT @bHasAccess = 1 FROM hrms.Application A WHERE @EntityID = 0 OR (A.ApplicationID = @EntityID AND A.PersonID = @PersonID)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.CreateDAPersonByPersonID
EXEC utility.DropObject 'person.CreateDAPersonByPersonID'
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.04.06
-- Description:	A stored procedure to create a consultant record in DA from a Person ID
-- ====================================================================================
CREATE PROCEDURE person.CreateDAPersonByPersonID

@PersonID INT,
@InviterPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cUserName VARCHAR(50)
	DECLARE @nDAPersonID INT
	DECLARE @nDocumentID INT
	DECLARE @tOutput TABLE (EntityID INT, UserName VARCHAR(250), OldUserName VARCHAR(250))

	INSERT INTO DeployAdviserCloud.person.Person
		(BirthDate,CellPhone,ChestSize,Citizenship1ISOCountryCode2,Citizenship2ISOCountryCode2,CollarSize,CountryCallingCodeID,EmailAddress,FirstName,GenderID,HasAcceptedTerms,HeadSize,Height,HomePhone,HomePhoneCountryCallingCodeID,IsActive,IsPhoneVerified,IsSuperAdministrator,IsUKEUNational,LastName,MailAddress1,MailAddress2,MailAddress3,MailISOCountryCode2,MailMunicipality,MailPostalCode,MailRegion,MiddleName,MobilePIN,Organization,Password,PasswordExpirationDateTime,PasswordSalt,PlaceOfBirthISOCountryCode2,PlaceOfBirthMunicipality,PreferredName,RegistrationCode,Suffix,SummaryBiography,Title,WorkPhone,WorkPhoneCountryCallingCodeID)
	OUTPUT INSERTED.PersonID, INSERTED.UserName, NULL INTO @tOutput
	SELECT
		P.BirthDate,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		P.Citizenship2ISOCountryCode2,
		P.CollarSize,
		P.CountryCallingCodeID,
		P.EmailAddress,
		P.FirstName,
		ISNULL((SELECT DAG.GenderID FROM DeployAdviserCloud.dropdown.Gender DAG WHERE DAG.GenderCode = G.GenderCode), 0),
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.HomePhoneCountryCallingCodeID,
		P.IsActive,
		P.IsPhoneVerified,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.Organization,
		P.Password,
		P.PasswordExpirationDateTime,
		P.PasswordSalt,
		P.PlaceOfBirthISOCountryCode2,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.WorkPhone,
		P.WorkPhoneCountryCallingCodeID
	FROM person.Person P
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
			AND P.PersonID = @PersonID	

	UPDATE O
	SET 
		O.OldUserName = (SELECT P1.UserName FROM person.Person P1 WHERE P1.PersonID = @PersonID),
		O.UserName = P2.UserName 
	FROM @tOutput O
		JOIN DeployAdviserCloud.person.Person P2 ON P2.PersonID = O.EntityID
	
	UPDATE P
	SET
		P.DApersonID = O.EntityID,
		P.UserName = O.UserName
	FROM @tOutput O
		JOIN person.Person P ON P.PersonID = O.EntityID

	SELECT 
		@nDAPersonID = O.EntityID,
		@cUserName = O.UserName
	FROM @tOutput O

	INSERT INTO DeployAdviserCloud.person.PersonLanguage
		(PersonID, ISOLanguageCode2, SpeakingLanguageProficiencyID, ReadingLanguageProficiencyID, WritingLanguageProficiencyID)
	SELECT	
		@nDAPersonID,
		PL.ISOLanguageCode2,
		ISNULL((SELECT DLP1.LanguageProficiencyID FROM DeployAdviserCloud.dropdown.LanguageProficiency DLP1 WHERE DLP1.LanguageProficiencyCode = LP1.LanguageProficiencyCode), 0),
		ISNULL((SELECT DLP2.LanguageProficiencyID FROM DeployAdviserCloud.dropdown.LanguageProficiency DLP2 WHERE DLP2.LanguageProficiencyCode = LP2.LanguageProficiencyCode), 0),
		ISNULL((SELECT DLP3.LanguageProficiencyID FROM DeployAdviserCloud.dropdown.LanguageProficiency DLP3 WHERE DLP3.LanguageProficiencyCode = LP3.LanguageProficiencyCode), 0)
	FROM person.PersonLanguage PL
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonPassport
		(PersonID, PassportExpirationDate, PassportIssuer, PassportNumber, PassportTypeID)
	SELECT
		@nDAPersonID,
		PP.PassportExpirationDate, 
		PP.PassportIssuer, 
		PP.PassportNumber, 
		ISNULL((SELECT DPT.PassportTypeID FROM DeployAdviserCloud.dropdown.PassportType DPT WHERE DPT.PassportTypeCode = PT.PassportTypeCode), 0)
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonPasswordSecurity
		(PersonID, PasswordSecurityQuestionID, PasswordSecurityQuestionAnswer)
	SELECT
		@nDAPersonID,
		ISNULL((SELECT DPSQ.PasswordSecurityQuestionID FROM DeployAdviserCloud.dropdown.PasswordSecurityQuestion DPSQ WHERE DPSQ.PasswordSecurityQuestionCode = PSQ.PasswordSecurityQuestionCode), 0),
		PPS.PasswordSecurityQuestionAnswer
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonQualificationAcademic
		(PersonID, Institution, SubjectArea, GraduationDate, StartDate, Degree)
	SELECT
		@nDAPersonID,
		PQA.Institution, 
		PQA.SubjectArea, 
		PQA.GraduationDate, 
		PQA.StartDate, 
		PQA.Degree
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonQualificationCertification
		(PersonID, Provider, Course, CompletionDate, ExpirationDate)
	SELECT
		@nDAPersonID,
		PQC.Provider, 
		PQC.Course, 
		PQC.CompletionDate, 
		PQC.ExpirationDate
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.client.ClientPerson
		(ClientID, PersonID, ClientPersonRoleCode)
	SELECT
		C.ClientID,
		@nDAPersonID,
		'Consultant'
	FROM DeployAdviserCloud.client.Client C
	WHERE C.IntegrationCode = 'Hermis'

	SELECT @nDocumentID = DE.DocumentID 
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = 'Person'
		AND DE.EntityTypeSubCode = 'CV'
		AND DE.EntityID = @PersonID

	IF @nDocumentID IS NOT NULL
		BEGIN

		DELETE O FROM @tOutput O

		INSERT INTO DeployAdviserCloud.document.Document
			(DocumentDate, DocumentDescription, DocumentGUID, DocumentTitle, Extension, CreatePersonID, ContentType, ContentSubtype, PhysicalFileSize, DocumentData)
		OUTPUT INSERTED.DocumentID, NULL, NULL INTO @tOutput
		SELECT
			D.DocumentDate,
			D.DocumentDescription,
			D.DocumentGUID,
			D.DocumentTitle,
			D.Extension,
			@nDAPersonID,
			D.ContentType,
			D.ContentSubtype,
			D.PhysicalFileSize,
			D.DocumentData
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		INSERT INTO DeployAdviserCloud.document.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			O.EntityID,
			'Person',
			'CV',
			@nDAPersonID
		FROM @tOutput O

		END
	--ENDIF

	SELECT
		O.OldUserName,
		O.UserName,
		P.EmailAddress AS PersonEmailAddress,
		(SELECT P1.EmailAddress FROM person.Person P1 WHERE P1.PersonID = @InviterPersonID) AS InviterPersonEmailAddress,
		person.FormatPersonNameByPersonID(@InviterPersonID, 'TitleFirstLast') AS PersonNameFormattedLong,
		person.FormatPersonNameByPersonID(@InviterPersonID, 'FirstLast') AS PersonNameFormattedShort,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS SiteURL,
		core.GetSystemSetupValueBySystemSetupKey('SystemName', '') AS SystemName
	FROM @tOutput O
		JOIN person.Person P ON P.PersonID = @PersonID

END
GO
--End procedure person.CreateDAPersonByPersonID

--Begin procedure person.GetFeedbackByFeedbackID
EXEC Utility.DropObject 'person.GetFeedbackByFeedbackID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the person.Feedback table
-- =============================================================================
CREATE PROCEDURE person.GetFeedbackByFeedbackID

@FeedbackID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Feedback', @FeedbackID)

	--Feedback
	SELECT
		F.CreateDate,
		core.FormatDate(F.CreateDate) AS CreateDateFormatted,
		F.CreatePersonID,
		person.FormatPersonNameByPersonID(F.CreatePersonID, 'LastFirst') AS CreatePersonNameFormated,
		F.CSGMemberComment,
		F.CSGMemberPersonID,
		person.FormatPersonNameByPersonID(F.CSGMemberPersonID, 'LastFirst') AS CSGMemberPersonNameFormated,
		F.EndDate,
		core.FormatDate(F.EndDate) AS EndDateFormatted,
		F.FeedbackDate,
		core.FormatDate(F.FeedbackDate) AS FeedbackDateFormatted,
		F.FeedbackID,
		F.FeedbackName,
		F.FeedbackStatusCode,
		F.IsExternal,
		F.Question4,
		F.Question5,
		F.Question7,
		F.StartDate,
		core.FormatDate(F.StartDate) AS StartDateFormatted,
		F.TaskID,
		(SELECT CV.TaskName FROM hrms.ClientVacancy CV WHERE CV.CLientVacancyID = F.TaskID) AS TaskName,
		(SELECT CV.TaskStartDate FROM hrms.ClientVacancy CV WHERE CV.CLientVacancyID = F.TaskID) AS TaskStartDate,
		core.FormatDate((SELECT CV.TaskStartDate FROM hrms.ClientVacancy CV WHERE CV.CLientVacancyID = F.TaskID)) AS TaskStartDateFormatted,
		MPR.CSGMemberPerformanceRatingCode,
		MPR.CSGMemberPerformanceRatingID,
		MPR.CSGMemberPerformanceRatingName,
		P.ProjectID,
		P.ProjectName,
		PR1.SUPerformanceRatingCode AS SUPerformanceRatingCode1,
		PR1.SUPerformanceRatingID AS SUPerformanceRatingID1,
		PR1.SUPerformanceRatingName AS SUPerformanceRatingName1,
		PR2.SUPerformanceRatingCode AS SUPerformanceRatingCode2,
		PR2.SUPerformanceRatingID AS SUPerformanceRatingID2,
		PR2.SUPerformanceRatingName AS SUPerformanceRatingName2,
		PR3.SUPerformanceRatingCode AS SUPerformanceRatingCode3,
		PR3.SUPerformanceRatingID AS SUPerformanceRatingID3,
		PR3.SUPerformanceRatingName AS SUPerformanceRatingName3
	FROM person.Feedback F
		JOIN dropdown.CSGMemberPerformanceRating MPR ON MPR.CSGMemberPerformanceRatingID = F.CSGMemberPerformanceRatingID
		JOIN dropdown.Project P ON P.ProjectID = F.ProjectID
		JOIN dropdown.SUPerformanceRating PR1 ON PR1.SUPerformanceRatingID = F.SUPerformanceRatingID1
		JOIN dropdown.SUPerformanceRating PR2 ON PR2.SUPerformanceRatingID = F.SUPerformanceRatingID2
		JOIN dropdown.SUPerformanceRating PR3 ON PR3.SUPerformanceRatingID = F.SUPerformanceRatingID3
			AND F.FeedbackID = @FeedbackID

	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'Feedback', @FeedbackID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Feedback', @FeedbackID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Feedback'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Feedback'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Feedback'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Feedback'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode LIKE '%Feedback'
		AND EL.EntityID = @FeedbackID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure person.GetFeedbackByFeedbackID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.19
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.26
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.29
-- Description: Added IsOnSURoster to the select list
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.30
-- Description: Added perosn record fields for personupdate workflow
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.ApplicantSourceOther,
		P.ApplicantTypeCode,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CivilServiceDepartment,
		P.CivilServiceGrade1,
		P.CivilServiceGrade2,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAAccountDecomissionedDate,
		core.FormatDate(P.DAAccountDecomissionedDate) AS DAAccountDecomissionedDateFormatted,
		P.DAPersonID,
		IIF(P.DAPersonID > 0, 'Yes ', 'No ') AS HasDAAccountYesNoFormat,
		P.DaysToRemoval,
		P.DefaultProjectID,
		P.DeleteByPersonID,
		person.FormatPersonNameByPersonID(P.DeleteByPersonID, 'FirstLast') AS DeleteByPersonNameFormatted,
		P.DeleteDate,
		core.FormatDate(P.DeleteDate) AS DeleteDateFormatted,
		P.DepartmentName,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FARemoveByPersonID,
		person.FormatPersonNameByPersonID(P.FARemoveByPersonID, 'FirstLast') AS FARemoveByPersonNameFormatted,
		P.FARemoveDate,
		core.FormatDate(P.FARemoveDate) AS FARemoveDateFormatted,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.HRContactCountryCallingCodeID,
		P.HRContactEmailAddress,
		P.HRContactName,
		P.HRContactPhone,
		P.InvalidLoginAttempts,
		P.InvoiceLimit,
		P.IsAccountLockedOut,
		P.IsActive,
		P.ISDAAccountDecomissioned,
		IIF(P.ISDAAccountDecomissioned = 1, 'Yes', 'No') AS ISDAAccountDecomissionedYesNoFormat,
		P.IsPhoneVerified,		
		P.IsPreviousApplicant,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKResidencyEligible,
		P.IsUKWorkEligible,
		P.JobTitle,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastContractEndDate,
		core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.LineManagerCountryCallingCodeID,
		P.LineManagerEmailAddress,
		P.LineManagerName,
		P.LineManagerPhone,
		P.LockOutByPersonID,
		person.FormatPersonNameByPersonID(P.LockOutByPersonID, 'FirstLast') AS LockOutByPersonNameFormatted,
		P.LockOutDate,
		core.FormatDate(P.LockOutDate) AS LockOutDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.OrganizationName,
		P.OtherGrade,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'Humanitarian'), 0) AS IsOnHSOTRoster,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'SU'), 0) AS IsOnSURoster,
		ISNULL((SELECT TOP 1 SC.SecurityClearanceName + ' - ' + core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID AND PSC.IsActive = 1 AND PSC.PersonID = P.PersonID), 'None') AS Vetting,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.PreviousApplicationDate,
		core.FormatDateTime(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
		P.PurchaseOrderLimit,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.SecurityClearanceID,
		P.SecurityClearanceSponsorName,
		P.Suffix,
		P.SummaryBiography,
		P.TeamName,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR1.PoliceRankID AS PoliceRankID1,
		PR1.PoliceRankName AS PoliceRankName1,
		PR2.PoliceRankID AS PoliceRankID2,
		PR2.PoliceRankName AS PoliceRankName2,
		PT.PersonTypeCode,
		PT.PersonTypeID,
		PT.PersonTypeName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
		JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

	--PersonApplicantSource
	SELECT 
		A.ApplicantSourceID,
		A.ApplicantSourceName
	FROM person.PersonApplicantSource PAS
		JOIN dropdown.ApplicantSource A ON A.ApplicantSourceID = PAS.ApplicantSourceID
			AND PAS.PersonID = @PersonID

	--PersonClientRoster
	SELECT 
		CR.ClientRosterCode,
		CR.ClientRosterName,
		ES.ExpertStatusID,
		ES.ExpertStatusName,
		PCR.ClientRosterID,
		PCR.ExpertStatusID,
		PCR.PersonClientRosterID,
		PCR.TerminationDate,
		core.FormatDate(PCR.TerminationDate) AS TerminationDateFormatted,
		PCR.TerminationReason
	FROM person.PersonClientRoster PCR
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
			AND PCR.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'PersonCorrespondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEC
		JOIN dropdown.Country C ON C.CountryID = PEC.CountryID
			AND PEC.PersonID = @PersonID
	ORDER BY C.CountryName, C.CountryID

	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonGroupPerson
	SELECT 
		PGP.PersonGroupPersonID,
		PGP.PersonGroupID,
		PGP.CreateDateTime,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateFormatted,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PGP2.PersonID) FROM person.PersonGroupPerson PGP2 WHERE PGP2.PersonGroupID = PG.PersonGroupID) AS UserCount,
		PG.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName
	FROM person.PersonGroupPerson PGP
		JOIN person.PersonGroup PG ON PG.PersonGroupID = PGP.PersonGroupID
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PGP.PersonID = @PersonID

	--PersonIR35
	SELECT
		newID() AS PersonIR35GUID,
		PIR.TaskName,
		PIR.IR35StatusDate,
		PIR.IR35StatusID,
		core.FormatDate(PIR.IR35StatusDate) AS IR35StatusDateFormatted,
		PIR.PersonIR35ID,
		PIR.Issues,
		IRS.IR35StatusName
	FROM person.PersonIR35 PIR
		JOIN dropdown.IR35Status IRS ON IRS.IR35StatusID = PIR.IR35StatusID
			AND PIR.PersonID = @PersonID
	ORDER BY PIR.TaskName, PIR.PersonIR35ID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetVettingEmailData
EXEC Utility.DropObject 'person.GetVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data for outgoing vetting e-mail
-- ==========================================================================
CREATE PROCEDURE person.GetVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@ReviewTypeCode VARCHAR(50),
@SecurityClearanceID INT,
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonSecurityClearanceProcessStep
		(PersonID, SecurityClearanceID, SecurityClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		@SecurityClearanceID,
		(SELECT SCPS.SecurityClearanceProcessStepID FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = @ReviewTypeCode),
		getDate(),
		1,
		'Security clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.DiscretionaryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS DiscretionaryReviewDateFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS MandatoryReviewDateFormatted
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		0 AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL,
		NULL

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetVettingEmailData

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDAPersonID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		DAPersonID INT NOT NULL DEFAULT 0,
		DefaultProjectID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		RoleName VARCHAR(50),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsPasswordExpired = CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN 1 ELSE 0 END,
		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDAPersonID = ISNULL(P.DAPersonID, 0),
		@nDefaultProjectID = P.DefaultProjectID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(
			CellPhone,
			CountryCallingCodeID,
			DAPersonID,
			DefaultProjectID,
			EmailAddress,
			FullName,
			IsAccountLockedOut,
			IsActive,
			IsPhoneVerified,
			IsProfileUpdateRequired,
			IsSuperAdministrator,
			IsValidPassword,
			IsValidUserName,
			PersonID,
			RequiredProfileUpdate,
			RoleName,
			UserName
			) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@nDAPersonID,
			@nDefaultProjectID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cRoleName,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT 
		P.*,
		person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM @tPerson P

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure procurement.GetEquipmentOrderByEquipmentOrderID
EXEC Utility.DropObject 'procurement.GetEquipmentOrderByEquipmentOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE procurement.GetEquipmentOrderByEquipmentOrderID

@EquipmentOrderID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('EquipmentOrder', @EquipmentOrderID)

	SELECT
		EO.EquipmentOrderID,
		EO.EquipmentOrderName,
		EO.RecipientEntityTypeCode,
		EO.RecipientEntityID,

		CASE
			WHEN EO.RecipientEntityTypeCode = 'Asset'
			THEN (SELECT IntegrationCode FROM asset.Asset A WHERE A.AssetID = EO.RecipientEntityID ) 
			ELSE '' 
		END AS RecipientEntityIntegrationCode,

		CASE
			WHEN EO.RecipientEntityTypeCode = 'Asset'
			THEN (SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = EO.RecipientEntityID)
			WHEN EO.RecipientEntityTypeCode = 'Contact'
			THEN contact.FormatContactNameByContactID(EO.RecipientEntityID, 'LastFirst')
			WHEN EO.RecipientEntityTypeCode = 'Force'
			THEN (SELECT F.ForceName FROM force.Force F WHERE F.ForceID = EO.RecipientEntityID)
			WHEN EO.RecipientEntityTypeCode = 'Person'
			THEN person.FormatPersonNameByPersonID(EO.RecipientEntityID, 'LastFirst')
		END AS RecipientEntityName,

		CASE
			WHEN EO.RecipientEntityTypeCode = 'Asset'
			THEN 'Facility'
			WHEN EO.RecipientEntityTypeCode = 'Contact'
			THEN 'Person'
			WHEN EO.RecipientEntityTypeCode = 'Force'
			THEN 'Organisation'
			WHEN EO.RecipientEntityTypeCode = 'Person'
			THEN 'Person'
			ELSE ''
		END AS RecipientEntityTypeName,

		EO.OrderedByPersonID,
		person.FormatPersonNameByPersonID(EO.OrderedByPersonID, 'LastFirst') AS OrderedByPersonNameFormatted,
		convert(varchar(10) , EO.OrderDate, 120) AS  OrderDate,
		core.FormatDate(EO.OrderDate) AS OrderDateFormatted,
		EO.Outcome,
		EO.RecipientAddress1,
		EO.RecipientAddress2,
		EO.RecipientAddress3,
		EO.RecipientCity,
		EO.RecipientRegion,
		EO.RecipientPostalCode,
		EO.RecipientISOCountryCode2,
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.RecipientISOCountryCode2) AS RecipientISOCOuntryCode3,
		dropdown.GetCountryNameByISOCountryCode(EO.RecipientISOCountryCode2) AS RecipientISOCountryName,
		EO.RecipientAddressNotes,
		EO.FinalMileAddress1,
		EO.FinalMileAddress2,
		EO.FinalMileAddress3,
		EO.FinalMileCity,
		EO.FinalMileRegion,
		EO.FinalMilePostalCode,
		EO.FinalMileISOCountryCode2,
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.FinalMileISOCountryCode2) AS FinalMileISOCOuntryCode3,
		dropdown.GetCountryNameByISOCountryCode(EO.FinalMileISOCountryCode2) AS FinalMileISOCountryName,
		EO.FinalMileAddressNotes,
		EO.ProjectID,
		EO.ProjectCode,
		EO.ClientPONumber,
		convert(varchar(10) , EO.OrderCompletionDate, 120) AS  OrderCompletionDate,
		core.FormatDate(EO.OrderCompletionDate) AS OrderCompletionDateFormatted,
		EO.ItemsNotesAndConditions,
		EO.ServicesNotesAndConditions,
		EO.TaskOrderNumber,
		EOT.EquipmentOrderTypeID,
		EOT.EquipmentOrderTypeName,
		dropdown.GetProjectClientNameByProjectID(EO.ProjectID) AS ProjectClientName,
		dropdown.GetProjectNameByProjectID(EO.ProjectID) AS ProjectName
	FROM procurement.EquipmentOrder EO
		JOIN dropdown.EquipmentOrderType EOT ON EO.EquipmentOrderTypeID = EOT.EquipmentOrderTypeID
			AND EO.EquipmentOrderID = @EquipmentOrderID

	SELECT 
		EOCI.EquipmentOrderCatalogItemID,
		EOCI.EquipmentItemID,
		EOCI.EquipmentCatalogID,
		EOCI.Quantity,
		EOCI.AccountCode,
		ROUND(EOCI.UnitCost, 2) AS UnitCost,
		CAST(ROUND((EOCI.UnitCost * EOCI.Quantity),2) AS numeric(18,2)) AS TotalCostexVAT,
		CAST(ROUND(((EOCI.UnitCost * EOCI.Quantity) * ((100 + EOCI.VAT)/100)) ,2) AS numeric(18,2)) AS TotalCostwithVAT,
		ROUND(EOCI.VAT,2) AS VAT,
		EOCI.Footnote,
		EOCI.ItemDescription,
		PM.PricingMethodID,
		PM.PricingMethodName,
		EI.EquipmentItemID,
		EI.ItemName,
		EI.IsColdChainRequired,
		IIF(EI.IsColdChainRequired = 1, 'Yes ', 'No ') AS IsColdChainRequiredFormatted,
		EI.IsControlledItem,
		IIF(EI.IsControlledItem = 1, 'Yes ', 'No ') AS IsControlledItemFormatted,
		EI.MinimumQuantity,
		EIT.EquipmentItemTypeID,
		EIT.EquipmentItemTypeName,
		EIT.EquipmentItemTypeCode
	FROM procurement.EquipmentOrderCatalogItem EOCI
		JOIN procurement.EquipmentItem EI ON EI.EquipmentItemID = EOCI.EquipmentItemID
		JOIN dropdown.PricingMethod PM ON EOCI.PricingMethodID = PM.PricingMethodID
		JOIN dropdown.EquipmentItemType EIT ON EI.EquipmentItemTypeID = EIT.EquipmentItemTypeID
			AND EOCI.EquipmentOrderID = @EquipmentOrderID
		
	EXEC workflow.GetEntityWorkflowData 'EquipmentOrder', @EquipmentOrderID

	EXEC workflow.GetEntityWorkflowPeople 'EquipmentOrder', @EquipmentOrderID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Equipment Requisition'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Equipment Requisition'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Equipment Requisition'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Equipment Requisition'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EL.EntityID
			AND EO.EquipmentOrderID = @EquipmentOrderID
			AND EL.EntityTypeCode = 'EquipmentOrder'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure procurement.GetEquipmentOrderByEquipmentOrderID

--Begin procedure reporting.GetEquipmentWorkOrderDetailsList
EXEC Utility.DropObject 'reporting.GetEquipmentWorkOrderDetailsList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Inderjeet Kaur
-- Create date: 2018.08.08
-- Description:	A procedure to return Equipment Work Order details for reporting
-- =============================================================================
CREATE PROCEDURE reporting.GetEquipmentWorkOrderDetailsList

@PersonID INT

AS
BEGIN
  SET NOCOUNT ON;
	
	SELECT
		dropdown.GetProjectClientNameByProjectID(EO.ProjectID) AS ProjectClientName
		,dropdown.GetProjectNameByProjectID(EO.ProjectID) AS ProjectName	 
		,EO.EquipmentOrderName
		,EO.EquipmentOrderTypeID
		,EOT.EquipmentOrderTypeName 
		,EO.TaskOrderNumber
		,EO.ClientPONumber
		,EO.ProjectCode
		,person.FormatPersonNameByPersonID(EO.OrderedByPersonID, 'LastFirst') AS OrderedByPersonNameFormatted
		,core.FormatDate(EO.OrderDate) AS OrderDateFormatted
		,core.FormatDate(EO.OrderCompletionDate) AS OrderCompletionDateFormatted
		,CASE
		WHEN EO.RecipientEntityTypeCode = 'Asset'
		THEN (SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = EO.RecipientEntityID)
		WHEN EO.RecipientEntityTypeCode = 'Contact'
		THEN contact.FormatContactNameByContactID(EO.RecipientEntityID, 'LastFirst')
		WHEN EO.RecipientEntityTypeCode = 'Force'
		THEN (SELECT F.ForceName FROM force.Force F WHERE F.ForceID = EO.RecipientEntityID)
		WHEN EO.RecipientEntityTypeCode = 'Person'
		THEN person.FormatPersonNameByPersonID(EO.RecipientEntityID, 'LastFirst')
		END AS RecipientOfOrder
		,CASE
		WHEN EO.RecipientEntityTypeCode = 'Asset'
		THEN 'Facility'
		WHEN EO.RecipientEntityTypeCode = 'Contact'
		THEN 'Person'
		WHEN EO.RecipientEntityTypeCode = 'Force'
		THEN 'Organisation'
		WHEN EO.RecipientEntityTypeCode = 'Person'
		THEN 'Person'
		ELSE ''
		END AS RecipientType
		,CONCAT(EO.RecipientAddress1 , ',', EO.RecipientAddress2 , ',' , EO.RecipientAddress3 , ',' , EO.RecipientCity , ',' , EO.RecipientRegion , ',', EO.RecipientPostalCode , ',' , EO.RecipientISOCountryCode2 , ',' , 
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.RecipientISOCountryCode2)) AS RecipientAddress
		,dropdown.GetCountryNameByISOCountryCode(EO.RecipientISOCountryCode2) AS RecipientISOCountryName
		,EO.RecipientAddressNotes
		,CONCAT(EO.FinalMileAddress1 , ',', EO.FinalMileAddress2 + ',' + EO.FinalMileAddress3 , ',', EO.FinalMileCity , ',', EO.FinalMileRegion , ',', EO.FinalMilePostalCode , ',', EO.FinalMileISOCountryCode2 , ',',
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.FinalMileISOCountryCode2)) as FinalMileAddress
		, dropdown.GetCountryNameByISOCountryCode(EO.FinalMileISOCountryCode2) AS FinalMileISOCountryName
		,EO.FinalMileAddressNotes
		,CASE WHEN workflow.GetWorkflowStepNumber('EquipmentOrder', EO.EquipmentOrderID) <= workflow.GetWorkflowStepCount('EquipmentOrder', EO.EquipmentOrderID) THEN 'Pending'
		ELSE  'Approved' END AS WorkflowStatus
		FROM     procurement.EquipmentOrder EO
		JOIN dropdown.EquipmentOrderType EOT ON EO.EquipmentOrderTypeID = EOT.EquipmentOrderTypeID
		JOIN Reporting.SearchResult SR ON SR.EntityID = EO.EquipmentOrderID
		AND SR.EntityTypeCode = 'EquipmentOrder'
		AND SR.PersonID = @PersonID

END
GO
--End procedure reporting.GetEquipmentWorkOrderDetailsList

--Begin procedure reporting.GetLessonList
EXEC Utility.DropObject 'reporting.GetLessonList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Inderjeet Kaur
-- Create date: 2019.03.25
-- Description:	A procedure to return Lessons for reporting
-- ========================================================
CREATE PROCEDURE reporting.GetLessonList 

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SqlQuery NVARCHAR(3000)
	 DECLARE @ParamDefinition AS NVARCHAR(MAX)

	DECLARE @ColumnListWithConversion VARCHAR(4000) = (SELECT STUFF((SELECT ', CONVERT(VARCHAR(50), ' + ColumnCode + ' ) AS ' + ColumnCode  FROM reporting.SearchSetup WHERE PersonID = @PersonID AND EntityTypeCode = 'Lesson'
																							ORDER BY SearchSetupID FOR XML PATH('')), 1, 2, '') );

	DECLARE @ColumnList VARCHAR(4000) = (SELECT STUFF((SELECT ', '  + ColumnCode FROM reporting.SearchSetup WHERE PersonID = @PersonID AND EntityTypeCode = 'Lesson' AND ColumnCode != 'ID'
																					ORDER BY SearchSetupID FOR XML PATH('')), 1, 2, '') );
																													
	SET  @SqlQuery =     'SELECT ID, ColumnName, Valuess
										   FROM (SELECT 	
										                                L.LessonID AS ID,											
																		P.ProjectName AS ActivityStream,	   
																     	CONVERT(VARCHAR(50),PS.ProjectSponsorName) AS Client,
																	   	CONVERT(VARCHAR(50),	person.FormatPersonNameByPersonID(	CONVERT(VARCHAR(50), L.CreatePersonID), ''LastFirst'')) AS [User],				            
																		CONVERT(VARCHAR(50), dropdown.GetCountryNameByISOCountryCode(CONVERT(VARCHAR(50), L.ISOCountryCode2) )) AS Country,						  
																		LT.LessonTypeName AS [Type],
																		LC.LessonCategoryName AS Category,
																		LPS.LessonPublicationStatusName PublicationStatus,
																	   	CONVERT(VARCHAR(50), ''MEAL-'' + RIGHT(''00000'' + CAST(L.LessonID AS VARCHAR(5)), 5)) AS LogNumber,
																		' + @ColumnListWithConversion +' 				   
														  FROM  lesson.Lesson L
																		JOIN dropdown.LessonCategory LC ON LC.LessonCategoryID = L.LessonCategoryID
																		JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = L.LessonPublicationStatusID
																		JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
																		JOIN dropdown.LessonType LT ON LT.LessonTypeID = L.LessonTypeID  
																		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = L.ProjectSponsorID
																	   JOIN Reporting.SearchResult SR ON SR.EntityID = L.LessonID
																		AND SR.EntityTypeCode = ''Lesson''
																		AND SR.PersonID = ' + CAST(@PersonID AS NVARCHAR(10)) + ')p
										 	   UNPIVOT
										   (Valuess FOR ColumnName IN(ActivityStream, Client, [User], Country,  [Type], Category, PublicationStatus, LogNumber,   ' + @ColumnList +' )
										   ) AS UnPvt;'
	
	
											
		
		  Set @ParamDefinition =      ' @PersonID INT'
			  
		 PRINT @SQLQuery
         Execute sp_Executesql     @SQLQuery, @ParamDefinition, @PersonID

		RETURN

END
GO
--End procedure reporting.GetLessonList

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
			) AS WorkflowStepCount,
			ISNULL(WS1.WorkflowStepStatusName, 'In Work') AS WorkflowStepStatusName
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND WS1.WorkflowStepNumber = 1
				AND (@ProjectID = 0 OR W1.ProjectID = @ProjectID)
		
		END
	--ENDIF

	ELSE
		BEGIN
	
			IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
				BEGIN
				
				SELECT
					WS.WorkflowStepName,
					D.WorkflowStepNumber,
					workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
					ISNULL(WS.WorkflowStepStatusName, 'In Work') AS WorkflowStepStatusName
				FROM
					(
					SELECT
						EWSGP.WorkflowID,
						MIN(EWSGP.WorkflowStepNumber) AS WorkflowStepNumber
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = @EntityTypeCode
						AND EWSGP.EntityID = @EntityID
						AND EWSGP.IsComplete = 0
					GROUP BY EWSGP.WorkflowID

					UNION

					SELECT
						EWSGPG.WorkflowID,
						MIN(EWSGPG.WorkflowStepNumber) AS WorkflowStepNumber
					FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
					WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
						AND EWSGPG.EntityID = @EntityID
						AND EWSGPG.IsComplete = 0			
					GROUP BY EWSGPG.WorkflowID
					) D
						JOIN workflow.WorkflowStep WS ON WS.WorkflowID = D.WorkflowID
							AND WS.WorkflowStepNumber = D.WorkflowStepNumber

				END
			--ENDIF
			
			ELSE
				BEGIN

				SELECT
					'Workflow Complete' AS WorkflowStepName,
					workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
					workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
					ISNULL(W.WorkflowCompleteStatusName, 'Approved') AS WorkflowStepStatusName
				FROM workflow.Workflow W
				WHERE EXISTS
					(
					SELECT 1
					FROM 
						(
						SELECT
							EWSGP.WorkflowID
						FROM workflow.EntityWorkflowStepGroupPerson EWSGP
						WHERE EWSGP.EntityTypeCode = @EntityTypeCode
							AND EWSGP.EntityID = @EntityID

						UNION

						SELECT
							EWSGPG.WorkflowID
						FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
						WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
							AND EWSGPG.EntityID = @EntityID
						) D
					WHERE D.WorkflowID = W.WorkflowID
					)
				
				END
			--ENDIF
			
		END
	--ENDIF

END
GO
--End procedure workflow.GetEntityWorkflowData

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.EntityTypeSubCode,	
		W.IsActive,
		W.ProjectID,
		dropdown.GetProjectNameByProjectID(W.ProjectID) AS ProjectName,
		W.WorkflowCompleteStatusName,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT 
		WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT 
		WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepStatusName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.IsFinancialApprovalRequired,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		person.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGPG.PersonGroupID,
		WSGPG.WorkflowStepGroupID,
		PG.PersonGroupName
	FROM workflow.WorkflowStepGroupPersonGroup WSGPG
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
		JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT,
@ProjectID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN

	DELETE EWSGP 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID

	DELETE EWSGPG
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

	INSERT INTO workflow.EntityWorkflowStepGroupPersonGroup
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonGroupID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGPG.PersonGroupID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

	IF @WorkflowStepNumber > 0
		BEGIN

		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.WorkflowStepNumber <= @WorkflowStepNumber

		UPDATE EWSGPG
		SET EWSGPG.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID
			AND EWSGPG.WorkflowStepNumber <= @WorkflowStepNumber

		END
	--ENDIF

END
GO
--End procedure workflow.InitializeEntityWorkflow