USE HermisCloud
GO

--Begin table document.Document
EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cDocumentGUID VARCHAR(50)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			CAST(ISNULL(I.DocumentGUID, newID()) AS VARCHAR(50)) AS DocumentGUID,
			NULLIF(I.ContentType, '') AS ContentType,
			NULLIF(I.ContentSubType, '') AS ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE document.Document ENABLE TRIGGER TR_Document
GO
--End table document.Document

--Begin table dropdown.ApplicantStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ApplicantStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ApplicantStatus
	(
	ApplicantStatusID INT IDENTITY(0,1) NOT NULL,
	ApplicantStatusCode VARCHAR(50),
	ApplicantStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicantStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ApplicantStatus', 'DisplayOrder,ApplicantStatusName', 'ApplicantStatusID'
GO
--End table dropdown.ApplicantStatus

--Begin table dropdown.ExternalInterviewOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.ExternalInterviewOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ExternalInterviewOutcome
	(
	ExternalInterviewOutcomeID INT IDENTITY(0,1) NOT NULL,
	ExternalInterviewOutcomeCode VARCHAR(50),
	ExternalInterviewOutcomeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ExternalInterviewOutcomeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ExternalInterviewOutcome', 'DisplayOrder,ExternalInterviewOutcomeName', 'ExternalInterviewOutcomeID'
GO
--End table dropdown.ExternalInterviewOutcome

--Begin table dropdown.ClientOrganization
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientOrganization'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientOrganization
	(
	ClientOrganizationID INT IDENTITY(0,1) NOT NULL,
	ClientOrganizationCode VARCHAR(50),
	ClientOrganizationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientOrganizationID'
GO
--End table dropdown.ClientOrganization

--Begin table dropdown.ClientRegion
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientRegion'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientRegion
	(
	ClientRegionID INT IDENTITY(0,1) NOT NULL,
	ClientRegionCode VARCHAR(50),
	ClientRegionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientRegionID'
GO
--End table dropdown.ClientRegion

--Begin table dropdown.ClientVacancyRoleStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientVacancyRoleStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientVacancyRoleStatus
	(
	ClientVacancyRoleStatusID INT IDENTITY(0,1) NOT NULL,
	ClientVacancyRoleStatusCode VARCHAR(50),
	ClientVacancyRoleStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientVacancyRoleStatusID'
GO
--End table dropdown.ClientVacancyRoleStatus

--Begin table dropdown.EmploymentType
DECLARE @TableName VARCHAR(250) = 'dropdown.EmploymentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EmploymentType
	(
	EmploymentTypeID INT IDENTITY(0,1) NOT NULL,
	EmploymentTypeCode VARCHAR(50),
	EmploymentTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EmploymentTypeID'
GO
--End table dropdown.EmploymentType

--Begin table dropdown.ImplementationDifficulty
DECLARE @TableName VARCHAR(250) = 'dropdown.ImplementationDifficulty'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.ImplementatonDifficulty'

CREATE TABLE dropdown.ImplementationDifficulty
	(
	ImplementationDifficultyID INT IDENTITY(0,1) NOT NULL,
	ImplementationDifficultyCode VARCHAR(50),
	ImplementationDifficultyName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementationDifficultyID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ImplementationDifficulty', 'DisplayOrder,ImplementationDifficultyName', 'ImplementationDifficultyID'
GO
--End table dropdown.ImplementationDifficulty

--Begin table dropdown.ImplementationImpact
DECLARE @TableName VARCHAR(250) = 'dropdown.ImplementationImpact'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.ImplementatonImpact'

CREATE TABLE dropdown.ImplementationImpact
	(
	ImplementationImpactID INT IDENTITY(0,1) NOT NULL,
	ImplementationImpactCode VARCHAR(50),
	ImplementationImpactName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementationImpactID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ImplementationImpact', 'DisplayOrder,ImplementationImpactName', 'ImplementationImpactID'
GO
--End table dropdown.ImplementationImpact

--Begin table dropdown.LessonCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonCategory
	(
	LessonCategoryID INT IDENTITY(0,1) NOT NULL,
	LessonCategoryCode VARCHAR(50),
	LessonCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonCategory', 'DisplayOrder,LessonCategoryName', 'LessonCategoryID'
GO
--End table dropdown.LessonCategory

--Begin table dropdown.LessonOwner
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonOwner'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonOwner
	(
	LessonOwnerID INT IDENTITY(0,1) NOT NULL,
	LessonOwnerCode VARCHAR(50),
	LessonOwnerName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonOwnerID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonOwner', 'DisplayOrder,LessonOwnerName', 'LessonOwnerID'
GO
--End table dropdown.LessonOwner

--Begin table dropdown.LessonPublicationStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonPublicationStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonPublicationStatus
	(
	LessonPublicationStatusID INT IDENTITY(0,1) NOT NULL,
	LessonPublicationStatusCode VARCHAR(50),
	LessonPublicationStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonPublicationStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonPublicationStatus', 'DisplayOrder,LessonPublicationStatusName', 'LessonPublicationStatusID'
GO
--End table dropdown.LessonPublicationStatus

--Begin table dropdown.LessonStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonStatus
	(
	LessonStatusID INT IDENTITY(0,1) NOT NULL,
	LessonStatusCode VARCHAR(50),
	LessonStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonStatus', 'DisplayOrder,LessonStatusName', 'LessonStatusID'
GO
--End table dropdown.LessonStatus

--Begin table dropdown.LessonType
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonType
	(
	LessonTypeID INT IDENTITY(0,1) NOT NULL,
	LessonTypeCode VARCHAR(50),
	LessonTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonType', 'DisplayOrder,LessonTypeName', 'LessonTypeID'
GO
--End table dropdown.LessonType

--Begin table dropdown.LessonUserClass
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonUserClass'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonUserClass
	(
	LessonUserClassID INT IDENTITY(0,1) NOT NULL,
	LessonUserClassCode VARCHAR(50),
	LessonUserClassName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonUserClassID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonUserClass', 'DisplayOrder,LessonUserClassName', 'LessonUserClassID'
GO
--End table dropdown.LessonUserClass

--Begin table dropdown.MedicalClearanceReviewType
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceReviewType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MedicalClearanceReviewType
	(
	MedicalClearanceReviewTypeID INT IDENTITY(0,1) NOT NULL,
	MedicalClearanceReviewTypeCode VARCHAR(50),
	MedicalClearanceReviewTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MedicalClearanceReviewTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MedicalClearanceReviewType', 'DisplayOrder,MedicalClearanceReviewTypeName', 'MedicalClearanceReviewTypeID'
GO
--End table dropdown.MedicalClearanceReviewType

--Begin table dropdown.ProjectLaborCode
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectLaborCode'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectLaborCode
	(
	ProjectLaborCodeID INT IDENTITY(0,1) NOT NULL,
	ProjectLaborCodeCode VARCHAR(50),
	ProjectLaborCodeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectLaborCodeID'
GO
--End table dropdown.ProjectLaborCode

--Begin table dropdown.PublishTo
DECLARE @TableName VARCHAR(250) = 'dropdown.PublishTo'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PublishTo
	(
	PublishToID INT IDENTITY(0,1) NOT NULL,
	PublishToCode VARCHAR(50),
	PublishToName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PublishToID'
GO
--End table dropdown.PublishTo

--Begin table dropdown.RecruitmentPathway
DECLARE @TableName VARCHAR(250) = 'dropdown.RecruitmentPathway'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RecruitmentPathway
	(
	RecruitmentPathwayID INT IDENTITY(0,1) NOT NULL,
	RecruitmentPathwayCode VARCHAR(50),
	RecruitmentPathwayName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecruitmentPathwayID'
GO
--End table dropdown.RecruitmentPathway

--Begin table dropdown.RegionalPriorityTier
DECLARE @TableName VARCHAR(250) = 'dropdown.RegionalPriorityTier'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RegionalPriorityTier
	(
	RegionalPriorityTierID INT IDENTITY(0,1) NOT NULL,
	RegionalPriorityTierCode VARCHAR(50),
	RegionalPriorityTierName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RegionalPriorityTierID'
GO
--End table dropdown.RegionalPriorityTier

--Begin table dropdown.RoleCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.RoleCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RoleCategory
	(
	RoleCategoryID INT IDENTITY(0,1) NOT NULL,
	RoleCategoryCode VARCHAR(50),
	RoleCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleCategoryID'
GO
--End table dropdown.RoleCategory

--Begin table dropdown.RoleSubCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.RoleSubCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RoleSubCategory
	(
	RoleSubCategoryID INT IDENTITY(0,1) NOT NULL,
	RoleSubCategoryCode VARCHAR(50),
	RoleSubCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleSubCategoryID'
GO
--End table dropdown.RoleSubCategory

--Begin table dropdown.SecurityClearanceReviewType
DECLARE @TableName VARCHAR(250) = 'dropdown.SecurityClearanceReviewType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SecurityClearanceReviewType
	(
	SecurityClearanceReviewTypeID INT IDENTITY(0,1) NOT NULL,
	SecurityClearanceReviewTypeCode VARCHAR(50),
	SecurityClearanceReviewTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SecurityClearanceReviewTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SecurityClearanceReviewType', 'DisplayOrder,SecurityClearanceReviewTypeName', 'SecurityClearanceReviewTypeID'
GO
--End table dropdown.SecurityClearanceReviewType

--Begin table dropdown.TaskType
DECLARE @TableName VARCHAR(250) = 'dropdown.TaskType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TaskType
	(
	TaskTypeID INT IDENTITY(0,1) NOT NULL,
	TaskTypeCode VARCHAR(50),
	TaskTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TaskTypeID'
GO
--End table dropdown.TaskType

--Begin table dropdown.ThematicFocus
DECLARE @TableName VARCHAR(250) = 'dropdown.ThematicFocus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ThematicFocus
	(
	ThematicFocusID INT IDENTITY(0,1) NOT NULL,
	ThematicFocusCode VARCHAR(50),
	ThematicFocusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ThematicFocusID'
GO
--End table dropdown.ThematicFocus

--Begin table eventlog.EventLog
EXEC utility.AddColumn 'eventlog.EventLog', 'EventName', 'VARCHAR(250)'
GO
--End table eventlog.EventLog

--Begin table hrms.Application
EXEC utility.AddColumn 'hrms.Application', 'ApplicantStatusID', 'INT', '0'
EXEC utility.AddColumn 'hrms.Application', 'Availability', 'VARCHAR(500)'
EXEC utility.AddColumn 'hrms.Application', 'ClientComments', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'hrms.Application', 'ClientVacancyID', 'INT', '0'
EXEC utility.AddColumn 'hrms.Application', 'DAPersonID', 'INT', '0'
EXEC utility.AddColumn 'hrms.Application', 'ExternalInterviewOutcomeID', 'INT', '0'
EXEC utility.AddColumn 'hrms.Application', 'HasClearanceRequirement', 'BIT', '0'
EXEC utility.AddColumn 'hrms.Application', 'HasFirearmsTrainingRequirement', 'BIT', '0'
EXEC utility.AddColumn 'hrms.Application', 'HasOperationalTrainingRequirement', 'BIT', '0'
EXEC utility.AddColumn 'hrms.Application', 'IsInterviewConfirmed', 'BIT', '0'
EXEC utility.AddColumn 'hrms.Application', 'ManagerComments', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'hrms.Application', 'Notes', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'hrms.Application', 'RecruiterComments', 'VARCHAR(MAX)'
GO
--End table hrms.Application

--Begin table hrms.ApplicationContactData
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationContactData'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationContactData
	(
	ApplicationContactDataID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	ContactTypeCode VARCHAR(50),
	ContactName VARCHAR(250),
	ContactJobTitle VARCHAR(250),
	ContactEmailAddress VARCHAR(320),
	ContactPhone VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationContactData', 'ApplicationID,ContactTypeCode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationContactDataID'
GO
--End table hrms.ApplicationContactData

--Begin table hrms.ApplicationInterviewAvailability
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationInterviewAvailability'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationInterviewAvailability
	(
	ApplicationInterviewAvailabilityID INT IDENTITY(1,1) NOT NULL,
	ApplicationPersonID INT,
	ApplicationInterviewScheduleID INT,
	ClientVacancyInterviewDateTimeID INT,
	IsAvailable BIT,
	ApplicationID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationInterviewScheduleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyInterviewDateTimeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAvailable', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationInterviewAvailability', 'ApplicationPersonID,ApplicationInterviewScheduleID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationInterviewAvailabilityID'
GO
--End table hrms.ApplicationInterviewAvailability

--Begin table hrms.ClientVacancy
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancy'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancy
	(
	ClientVacancyID INT IDENTITY(1,1) NOT NULL,
	TaskName VARCHAR(250),
	TaskCode VARCHAR(250),
	TaskStartDate DATE,
	TaskEndDate DATE,
	CountryID INT,
	TaskOwnerPersonID INT,
	RegionalPriorityTierID INT,
	ClientLocation VARCHAR(250),
	ClientRegionID INT,
	ClientDepartment VARCHAR(250),
	ClientContact1Name VARCHAR(250),
	ClientContact1Team VARCHAR(250),
	ClientContact1Email VARCHAR(250),
	ClientContact1Phone VARCHAR(250),
	ClientContact2Name VARCHAR(250),
	ClientContact2Team VARCHAR(250),
	ClientContact2Email VARCHAR(250),
	ClientContact2Phone VARCHAR(250),
	ClientPersonID INT,
	RecruitmentPathwayID INT,
	TaskLocation VARCHAR(250),
	RoleName VARCHAR(250),
	RoleCode VARCHAR(250),
	ApplicationOpenDate DATETIME,
	ApplicationCloseDate DATETIME,
	ClientVacancyRoleStatusID INT,
	ProjectLaborCodeID INT,
	RoleTeam VARCHAR(250),
	PositionCount INT,
	ExpectedStartDate DATE,
	ExpectedEndDate DATE,
	WorkingDaysCount NUMERIC(10,2),
	HoursWorkedPerDay NUMERIC(10,2),
	Salary NUMERIC(10,2),
	EmploymentTypeID INT,
	ThematicFocusID INT,
	TaskTypeID INT,
	PublishToID INT,
	SecurityClearanceTypeID INT,
	OperationalTrainingCourseID INT,
	VacancyStatusID INT,
	IsFirearmsTrainingRequired BIT,
	RoleContext VARCHAR(MAX),
	KeyDeliverables VARCHAR(MAX),
	SiftDate DATE,
	InterviewShortlistDate DATE,
	AdvertisedInterviewDate DATE,
	RoleNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientRegionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyRoleStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EmploymentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFirearmsTrainingRequired', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'OperationalTrainingCourseID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PositionCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PositionCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLaborCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLaborCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PublishToID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RecruitmentPathwayID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RegionalPriorityTierID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaskOwnerPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaskTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ThematicFocusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyStatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientVacancyID'
GO
--End table hrms.ClientVacancy

--Begin table hrms.ClientVacancyClientOrganization
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyClientOrganization'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyClientOrganization
	(
	ClientVacancyClientOrganizationID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	ClientOrganizationID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientOrganizationID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyClientOrganization', 'ClientVacancyID,ClientOrganizationID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyClientOrganizationID'
GO
--End table hrms.ClientVacancyClientOrganization

--Begin table hrms.ClientVacancyCompetency
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyCompetency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyCompetency
	(
	ClientVacancyCompetencyID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	CompetencyID INT,
	IsEssential BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CompetencyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsEssential', 'BIT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyCompetency', 'ClientVacancyID,CompetencyID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyCompetencyID'
GO
--End table hrms.ClientVacancyCompetency

--Begin table hrms.ClientVacancyDeploymentManagerPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyDeploymentManagerPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyDeploymentManagerPerson
	(
	ClientVacancyDeploymentManagerPersonID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyDeploymentManagerPerson', 'ClientVacancyID,PersonID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyDeploymentManagerPersonID'
GO
--End table hrms.ClientVacancyDeploymentManagerPerson

--Begin table hrms.ClientVacancyInterviewDateTime
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyInterviewDateTime'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'hrms.ClientVacancynterviewDateTime'

CREATE TABLE hrms.ClientVacancyInterviewDateTime
	(
	ClientVacancyInterviewDateTimeID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	InterviewDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyInterviewDateTime', 'ClientVacancyID,InterviewDateTime'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyInterviewDateTimeID'
GO
--End table hrms.ClientVacancyInterviewDateTime

--Begin table hrms.ClientVacancyRecruitmentManagerPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyRecruitmentManagerPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyRecruitmentManagerPerson
	(
	ClientVacancyRecruitmentManagerPersonID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyRecruitmentManagerPerson', 'ClientVacancyID,PersonID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyRecruitmentManagerPersonID'
GO
--End table hrms.ClientVacancyRecruitmentManagerPerson

--Begin table hrms.ClientVacancyReviewerPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyReviewerPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyReviewerPerson
	(
	ClientVacancyReviewerPersonID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyReviewerPerson', 'ClientVacancyID,PersonID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyReviewerPersonID'
GO
--End table hrms.ClientVacancyReviewerPerson

--Begin table hrms.ClientVacancyRoleCategory
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyRoleCategory'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyRoleCategory
	(
	ClientVacancyRoleCategoryID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	RoleCategoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleCategoryID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyRoleCategory', 'ClientVacancyID,RoleCategoryID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyRoleCategoryID'
GO
--End table hrms.ClientVacancyRoleSubCategory

--Begin table hrms.ClientVacancyRoleSubCategory
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyRoleSubCategory'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyRoleSubCategory
	(
	ClientVacancyRoleSubCategoryID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	RoleSubCategoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleSubCategoryID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyRoleSubCategory', 'ClientVacancyID,RoleSubCategoryID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyRoleSubCategoryID'
GO
--End table hrms.ClientVacancyRoleSubCategory

--Begin table lesson.Lesson
DECLARE @TableName VARCHAR(250) = 'lesson.Lesson'

EXEC utility.DropObject @TableName

CREATE TABLE lesson.Lesson
	(
	LessonID INT IDENTITY(1,1) NOT NULL,
	LessonName VARCHAR(250),
	ProjectID INT,
	ProjectSponsorID INT,
	LessonOwnerID INT,
	LessonStatusID INT,
	LessonPublicationStatusID INT,
	ISOCountryCode2 CHAR(2),
	ISOCurrencyCode CHAR(3),
	ScaleOfResponse NUMERIC(18,2),
	ClientCode VARCHAR(250),
	LessonCategoryID INT,
	LessonTypeID INT,
	LessonDescription VARCHAR(MAX),
	Event VARCHAR(MAX),
	Cause VARCHAR(MAX),
	Impact VARCHAR(MAX),
	Indicators VARCHAR(MAX),
	Recommendations VARCHAR(MAX),
	ImplementationImpactID INT,
	ImplementationDifficultyID INT,
	IncidentTypeID INT, --this is Event cat / sub cat
	LessonDate DATE,
	CreatePersonID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImplementationDifficultyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImplementationImpactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'LessonOwnerID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonPublicationStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectSponsorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ScaleOfResponse', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonID'
GO
--End table lesson.Lesson

--Begin table lesson.LessonAction
DECLARE @TableName VARCHAR(250) = 'lesson.LessonAction'

EXEC utility.DropObject @TableName

CREATE TABLE lesson.LessonAction
	(
	LessonActionID INT IDENTITY(1,1) NOT NULL,
	LessonID INT,
	LessonActionName VARCHAR(250),
	LessonActionDescription VARCHAR(MAX),
	LessonActionPersonID INT,
	DueDate DATE,
	IsComplete BIT,
	CreatePersonID INT,
	CreateDateTime DATETIME,
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonActionPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_LessonAction', 'LessonID,LessonActionID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LessonActionID'
GO
--End table lesson.LessonAction

--Begin table lesson.LessonLessonUserClass
DECLARE @TableName VARCHAR(250) = 'lesson.LessonLessonUserClass'

EXEC utility.DropObject @TableName

CREATE TABLE lesson.LessonLessonUserClass
	(
	LessonLessonUserClassID INT IDENTITY(1,1) NOT NULL,
	LessonID INT,
	LessonUserClassID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'LessonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonUserClassID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_LessonLessonUserClass', 'LessonID,LessonUserClassID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LessonLessonUserClassID'
GO
--End table lesson.LessonLessonUserClass

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'DepartmentName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'JobTitle', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'OrganizationName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'TeamName', 'VARCHAR(250)'
GO

EXEC utility.DropObject 'person.TR_Person'

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A trigger to create a DeployAdviser standard username
-- ==================================================================
CREATE TRIGGER person.TR_Person ON person.Person FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cLastName VARCHAR(100)
	DECLARE @cUserName VARCHAR(6)
	DECLARE @nIndex INT
	DECLARE @nPersonID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.PersonID,
			utility.StripCharacters(I.LastName, '^a-z') + 'XXX' AS LastName
		FROM INSERTED I
		WHERE I.UserName IS NULL
			OR EXISTS
				(
				SELECT 1
				FROM person.Person P
				WHERE P.UserName = I.UserName
					AND P.PersonID <> I.PersonID
				)

	OPEN oCursor
	FETCH oCursor INTO @nPersonID, @cLastName
	WHILE @@fetch_status = 0
		BEGIN

		SET @cUserName = UPPER(LEFT(@cLastName, 3))
		SET @nIndex = (SELECT TOP 1 CAST(RIGHT(P.UserName, 3) AS INT) FROM person.Person P WHERE LEFT(P.UserName, 3) = LEFT(@cLastName, 3) ORDER BY 1 DESC)
		SET @nIndex = ISNULL(@nIndex, 0) + 1

		SET @cUserName += RIGHT('000' + CAST(@nIndex AS VARCHAR(10)), 3) + 'HCP'

		UPDATE P
		SET P.UserName = @cUserName
		FROM person.Person P
		WHERE P.PersonID = @nPersonID

		FETCH oCursor INTO @nPersonID, @cLastName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End table person.Person

--Begin table person.Feedback
DECLARE @TableName VARCHAR(250) = 'person.Feedback'

EXEC utility.AddColumn @TableName, 'FeedbackDate', 'DATE'
EXEC utility.AddColumn @TableName, 'FeedbackStatusCode', 'VARCHAR(50)'
GO
--End table person.Feedback

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC utility.AddColumn @TableName, 'IsSUperAdminOnly', 'BIT', '0'
GO
--End table workflow.Workflow
