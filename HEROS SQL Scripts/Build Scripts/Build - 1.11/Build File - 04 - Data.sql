USE HermisCloud
GO

--Begin table core.EmailTemplate
UPDATE ET
SET ET.EmailTemplateCode = 'InitialMedicalClearanceReview'
FROM core.EmailTemplate ET
WHERE ET.EmailTemplateCode = 'MedicalClearanceReview'
GO

EXEC core.EmailTemplateAddUpdate 
	'Lesson',
	'DecrementWorkflow',
	'Sent when a lesson has been rejected',
	'A Lesson Has Been Disapproved',
	'<p>A lesson was disapproved:</p><br/><p><a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><p><strong>Comments:</strong><br />[[WorkflowComments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the workflow for this lesson. Please log in and click the link above to review.</p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[WorkflowComments]]":"Workflow comments."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Lesson',
	'IncrementWorkflow',
	'Sent when a lesson has been approved',
	'A Lesson Has Been Approved',
	'<p>A lesson was approved:</p><br/><p><a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><p><strong>Comments:</strong><br />[[WorkflowComments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the workflow for this lesson. Please log in and click the link above to review.</p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[WorkflowComments]]":"Workflow comments."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

UPDATE ET
SET ET.WorkflowActionCode = ET.EmailTemplateCode
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode = 'Lesson'
	AND ET.EmailTemplateCode LIKE '%Workflow'
GO

EXEC core.EmailTemplateAddUpdate 
	'LessonAction',
	'LessonActionAdd',
	'Sent when a lesson action has been added',
	'A Lesson Action Has Been Added',
	'<p>A lesson action has been added to the HERMIS system and assigned to you as the Assigned Action Owner</p><br/><p>The name of the lesson action is [[LessonActionName]] and you can view the lesson to which this action is associated here: <a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[LessonActionName]]":"Lesson action name."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'LessonAction',
	'LessonActionUpdate',
	'Sent when a lesson action has been updated',
	'A Lesson Action Has Been Updated',
	'<p>A lesson action has been in the HERMIS system on you which you are designated as the Assigned Action Owner has been updated.</p><br/><p>The name of the lesson action is [[LessonActionName]] and you can view the lesson to which this action is associated here: <a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[LessonActionName]]":"Lesson action name."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'MedicalVetting', 
	'FollowupMedicalClearanceReview', 
	'Sent to notifiy a user that one or more medical reviews are required', 
	'Medical Clearance Review Followup Email', 
	'<p>Dear [[ToFullName]],<br /><br />You are required to undergo a [[MedicalClearanceTypeName]] medical check.<br />Please find attached the relevant instructions.<br />Please arrange you check directly with the service provider: Medfit.<br />If you need assistance please contact [Palladium email].<br />Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br /><br />[[Comments]]<br /><br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br /><br />The Palladium HR Team</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[MedicalClearanceTypeName]]":"Medical Clearance Types"},{"[[Comments]]":"Sender comments"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'InviteDAPerson', 
	'Sent when a person is invited to DeployAdviser', 
	'You Have Been Invited To Join DeployAdviser', 
	'<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with DFFD in the DeployAdviser system with a role of consultant.&nbsp; Accepting this offer will allow DFFD to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[LoginLink]] system to accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[LoginURL]]</p><p>PLEASE NOTE:  Your User Name for both [[SystemName]] AND DeployAdviser has been changed to [[UserName]].  Your password remains unchanged.</p><p>Please do not reply to this email as it is generated automatically by the [[SystemName]] system.</p><p>&nbsp;</p><p>Thank you,<br />The [[SystemName]] Team</p>',
	'[{"[[EmailAddress]]":"Inviter email address"},{"[[LoginLink]]":"Link to DA"},{"[[LoginURL]]":"URL to DA"},{"[[PersonNameFormattedLong]]":"Inviter person name (long)"},{"[[PersonNameFormattedShort]]":"Inviter person name (short)"},{"[[SystemName]]":"System name"},{"[[UserName]]":"DA user name"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupBPSS', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupBPSS', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupCTC', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a CTC security clearance require completion', 
	'CTC Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupCTC', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a CTC security clearance require completion', 
	'CTC Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupDV', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a DV security clearance require completion', 
	'DV Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupDV', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a DV security clearance require completion', 
	'DV Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupSC', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a SC security clearance require completion', 
	'SC Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupSC', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a SC security clearance require completion', 
	'SC Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'InvitePerson', 
	'Sent when a new person is invited to Hermis', 
	'You Have Been Invited To Join Hermis', 
	'<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to join the Hermis system.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your Hermis username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the Hermis system.</p><p>&nbsp;</p><p>Thank you,<br />The Hermis Team</p>',
	'[{"[[EmailAddress]]": "Sender Email Address"},{"[[PasswordTokenLink]]": "Embedded Reset Password Link"},{"[[PasswordTokenURL]]": "Site Reset Password URL"},{"[[PersonNameFormattedLong]]": "Sender Name (Long)"},{"[[PersonNameFormattedShort]]": "Sender Name (Short)"},{"[[UserName]]": "User Name"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'NewApplication',
	'Steps 7 – 8 New Application for Recruitment to Roster – to CSG Coordinator',
	'HERMIS – A new application for Recruitment to Roster has been received',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to review this vacancy.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_1',
	'Steps 12 – 13 Competency Review Required – to CSG Manager',
	'HERMIS – An application to join the roster is ready for a competency review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to provide your comments. If you have no comments please indicate you have none.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_2',
	'Steps 14 – 15 Technical Review Required – to CSG Manager',
	'HERMIS – An application to join the roster is ready for a technical review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to provide your comments. If you have no comments please indicate you have none.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_DecrementWorkflow_2',
	'Steps 14 – 39 Unsuccessful Candidate – to CSG Coordinator',
	'HERMIS – An applicant has not passed the competency review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to notify the applicant and update the vacancy matrix.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_DecrementWorkflow_3',
	'Steps 14 – 39 Unsuccessful Candidate – to CSG Coordinator',
	'HERMIS – An applicant has not passed the technical capability review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to notify the applicant and update the vacancy matrix.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_3',
	'Steps 16 – 17 An Application has passed Competency and Technical reviews – to CSG Coordinator',
	'HERMIS – A new application for Recruitment to Roster has passed competency and technical reviews',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to update the vacancy matrix and arrange an interview with the candidate.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_4',
	'Steps 17 - 18 Select Interview Dates – to Interview Panel Member',
	'HERMIS – A new applicant for Recruitment to Roster is ready for interview',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to indicate the dates and time you can be available to participate in the interview of this candidate.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_5',
	'Steps 18 - 19 Select Interview Dates – to Technical CSG Manager',
	'HERMIS – A new applicant for Recruitment to Roster is ready for interview',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to indicate the dates and time you can be available to participate in the interview of this candidate.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_6',
	'Steps 19 - 20 Arrange Interview with Candidate – to CSG Coordinator',
	'HERMIS – A new applicant for Recruitment to Roster is ready for interview',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to arrange the interview with the candidate on the date and time indicated.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'EmailInterviewee',
	'Steps 20 - 21 Interview for CGS Roster – to Candidate Roster Member',
	'Interview for CSG Vacancy',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Following your recent application to join the CSG Roster - [[ExpertCategoryName]] category, we are pleased to inform you that we would like to offer you an interview as follows:	<br><br>
	Date/Time: [[InterviewDateTimeFormatted]]<br>
	Location: [[InterviewLocation]]<br><br>
	Please confirm by email to [[CSGCoordinatorEmailAddress]] that you are available to attend.<br><br>
	Please do not reply to this email as it is generated automatically.<br><br>
	
		All the best,<br>
		The SU Recruitment Team<br>
		[[SURosterContactEmailAddress]]<br>
	<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[CSGCoordinatorEmailAddress]]":"CSG Coordinator Eemail address."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."},
		{"[[InterviewDateTimeFormatted]]":"Date and time of selected interview."},
		{"[[InterviewLocation]]":"Location of interview."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_7',
	'Steps 22 – 24 Interview Notification – to Technical CSG Manager and Interview Panel Member',
	'HERMIS – An interview has been scheduled',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	An interview has been arranged for the above vacancy and applicant as follows:<br><br>
	Date/Time: [[InterviewDateTimeFormatted]]<br>
	Location: [[InterviewLocation]]<br><br>
	The interview proforma is attached.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."},
		{"[[InterviewDateTimeFormatted]]":"Date and time of selected interview."},
		{"[[InterviewLocation]]":"Location of interview."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationCandidateSelected',
	'Steps 26 - 27 Candidate has been selected – to CSG Coordinator',
	'HERMIS – A candidate has been selected for Recruitment to the CSG Roster process',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to update the vacancy matrix and arrange the provisional offer for the candidate and request acceptance(s) for the role.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationCandidateNotSelected',
	'Steps 26 – 39 Candidate has not been selected – to CSG Coordinator',
	'HERMIS – A candidate has not been selected for Recruitment to the CSG Roster process',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] – CSG [[ExpertCategoryName]] – [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to update the vacancy matrix and notify the candidate they were unsuccessful.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationProvisionalMembership',
	'Steps 27 – 28 Provisional Offer – to Candidate Roster Member',
	'Provisional Membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Thank you for applying to become a member of the UK Civilian Stabilisation Group (CSG).<br><br>
	We’re pleased to inform that pending confirmation of security clearance you will be registered as a member of the CSG.<br><br>
	You will shortly receive an invitation to register your profile on a system called DeployAdviser.<br><br>
	All the best,<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationDAProvisioned',
	'Steps 29 – 30 Provisional Offer – to Candidate Roster Member',
	'Provisional Membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Confirmation of your membership is pending subject to your security clearance being confirmed or obtained.<br><br>
	You must now login to DeployAdviser to create a profile. Please note that failure to do so within five working days may result in the withdrawal of the provisional offer.<br><br>
	Once you have created your profile, you will be contacted by Palladium International Development Limited who will assist you with the security check process on our behalf.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://v2.deployadviser.com/login/.<br><br>
	All the best,<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationVetting',
	'Steps 31 - 33 New Provisional CSG Roster Member for Vetting – to Palladium Vetting Team',
	'New Provisional CSG Roster Member for Vetting',
	'
	We advise that the candidate below has accepted a provisional offer to join the CSG Roster. Membership can only be confirmed after security clearance has been confirmed or obtained.<br><br>
	Please assist the candidate to complete the vetting process.<br><br>
	Vacancy: [[VacancyReference]] – CSG [[ExpertCategoryName]]<br>
	Candidate: [[PersonNameFormatted]]<br>
	Email: [[ApplicantEmailAddress]]<br>
	Mobile: [[ApplicantCellPhone]]<br><br>
	Please do not reply to this email as it is generated automatically.<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[ApplicantEmailAddress]]":"Applicant email address."},
		{"[[ApplicantCellPhone]]":"Applicant cell phone."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationMembershipCOnfirmation',
	'Steps 36 – 37 Confirmation of Membership – to Candidate Roster Member',
	'Membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Following confirmation / obtainment of your security clearance we are pleased to confirm your membership of the CSG Roster.<br><br>
	We look forward to your application for CSG Tasks.<br><br>
	Tasks for which you are eligible are advertised through DeployAdviser.<br><br>
	Please check regularly for opportunities.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://v2.deployadviser.com/login/.<br><br>
	All the best,<br>
	The SU Recruitment Team<br>
	[[SURosterContactEmailAddress]]<br>
	<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationRejection',
	'Step 39 - 40 Notify Unsuccessful Candidates – to Candidate Roster Member',
	'Application for membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	We regret to inform that your application to join the CSG Roster has not been successful.<br><br>
	We really appreciate the effort you put into applying to join the CSG Roster.<br><br>
	
	All the best,<br>
	The SU Recruitment Team<br>
	Please do not reply to this email as it is generated automatically.<br>
	<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO
--End table core.EmailTemplate

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ClientVacancy', 
	@EntityTypeName = 'Client Vacancy', 
	@EntityTypeNamePlural = 'Client Vacancies',
	@HasWorkflow = 1,
	@SchemaName = 'hrms', 
	@TableName = 'ClientVacancy', 
	@PrimaryKeyFieldName = 'ClientVacancyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ClientVacancyApplication', 
	@EntityTypeName = 'ClientVacancyApplication', 
	@EntityTypeNamePlural = 'ClientVacancyApplications',
	@HasWorkflow = 0,
	@SchemaName = 'hrms', 
	@TableName = 'ClientVacancy', 
	@PrimaryKeyFieldName = 'ClientVacancyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Lesson', 
	@EntityTypeName = 'Lesson Tool', 
	@EntityTypeNamePlural = 'Lessons Tool',
	@HasWorkflow = 1,
	@SchemaName = 'lesson', 
	@TableName = 'Lesson', 
	@PrimaryKeyFieldName = 'LessonID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'DocumentList',
	@NewMenuItemCode = 'LessonList',
	@NewMenuItemLink = '/lesson/list',
	@NewMenuItemText = 'Lesson Tool',
	@PermissionableLineageList = 'Lesson.List',
	@ParentMenuItemCode = 'Reporting'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonUpdate',
	@NewMenuItemCode = 'PersonInvite',
	@NewMenuItemLink = '/person/invite',
	@NewMenuItemText = 'Invite Client',
	@PermissionableLineageList = 'Person.Invite',
	@ParentMenuItemCode = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@BeforeMenuItemCode = 'Vacancy',
	@NewMenuItemCode = 'ClientVacancy',
	@NewMenuItemLink = '/clientvacancy/list',
	@NewMenuItemText = 'Recruitment from Roster',
	@PermissionableLineageList = 'ClientVacancy.List',
	@ParentMenuItemCode = 'HumanResources'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'HumanResources'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Reporting'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'CSGCategoryProfileLink', 'The hyperlink displayed on the vacancy application edit page for CSG Category has a profile information', '<a href="http://www.google.com" style="font-color:blue;" target="_blank">here</a>'
EXEC core.SystemSetupAddUpdate 'PersonalDataProcessingInformation', 'The hyperlink to the personal data handling information page', '<a href="http://www.google.com" style="font-color:blue;" target="_blank">here</a>'
EXEC core.SystemSetupAddUpdate 'PersonLanguageReferenceText', 'The text string displayed above the person language table on the person profile add update page', 'If you have a language skill, please use the <strong>Common European Framework of Reference for Languages and Self-Assessment Tool (PDF, 127KB, 4 pages)</strong> found <a href="https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/615703/Common_European_Framework_of_Reference_for_Languages_and_Self_Assessment_Tool.pdf" style="font-color:blue;" target="_blank">here</a> as a guide to indicate the level of proficiency you have attained in relation to each language. If you have listed your proficiency for a language as above "basic", you may be asked to complete a language skill assessment.'
EXEC core.SystemSetupAddUpdate 'PrivacyPolicy', 'The text of the system privacy policy', 'JC - Please give Todd the actual words for this to add to the build'
EXEC core.SystemSetupAddUpdate 'TermsAndConditions', 'The text of the system terms and conditions', 'JC - Please give Todd the actual words for this to add to the build'

IF core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') = 'http://hermis.local'
	BEGIN

	EXEC core.SystemSetupAddUpdate 'ApplicantSiteURL', '', 'https://client.hermis.local'
	EXEC core.SystemSetupAddUpdate 'ClientSiteURL', '', 'https://client.hermis.local'

	END
ELSE IF core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') = 'https://heros-hermis-dev.azurewebsites.net/'
	BEGIN

	EXEC core.SystemSetupAddUpdate 'ApplicantSiteURL', '', 'https://hsot-applicant.skhosting.co.uk'
	EXEC core.SystemSetupAddUpdate 'ClientSiteURL', '', 'https://hsotclientportal.skhosting.co.uk'

	END
--ENDIF
GO
--End table core.SystemSetup

--Begin table dropdown.ApplicantStatus
TRUNCATE TABLE dropdown.ApplicantStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.ApplicantStatus', 'ApplicantStatusID', 0
GO

INSERT INTO dropdown.ApplicantStatus 
	(ApplicantStatusName, ApplicantStatusCode, DisplayOrder)
VALUES
	('Under Consideration', 'UnderConsideration', 1),
	('Unsuccessful in Initial Sift', 'UnsuccessfulInitialSift', 2),
	('Unsuccessful at Shortlist ', 'UnsuccessfulShortlist ', 3),
	('Unsuccessful at Interview', 'UnsuccessfulInterview', 4),
	('Successful', 'Successful', 5)
GO
--End table dropdown.ApplicantStatus

--Begin table dropdown.ExternalInterviewOutcome
TRUNCATE TABLE dropdown.ExternalInterviewOutcome
GO

EXEC utility.InsertIdentityValue 'dropdown.ExternalInterviewOutcome', 'ExternalInterviewOutcomeID', 0
GO

INSERT INTO dropdown.ExternalInterviewOutcome 
	(ExternalInterviewOutcomeName, ExternalInterviewOutcomeCode, DisplayOrder)
VALUES
	('Reject', 'Reject', 1),
	('1st Preference', 'Choice1', 2),
	('2nd Preference', 'Choice2', 3),
	('3nd Preference', 'Choice3', 4),
	('Candidate', 'Candidate', 5)
GO
--End table dropdown.ExternalInterviewOutcome

--Begin table dropdown.ClientOrganization
TRUNCATE TABLE dropdown.ClientOrganization
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientOrganization', 'ClientOrganizationID', 0
GO

INSERT INTO dropdown.ClientOrganization 
	(ClientOrganizationName, ClientOrganizationCode, DisplayOrder)
VALUES
	('Cabinet Office', 'CO', 1),
	('CPS', 'CPS', 2),
	('DFID', 'DFID',3),
	('FCO', 'FCO',4),
	('Home Office', 'HO', 5),
	('HMT', 'HMT', 6),
	('MOD', 'MOD', 7),
	('MOJ', 'MOJ', 8),
	('NCA', 'NCA', 9),
	('NSGI', 'NSGI', 10),
	('SU', 'SU', 11)
GO
--End table dropdown.ClientOrganization

--Begin table dropdown.ClientRegion
TRUNCATE TABLE dropdown.ClientRegion
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientRegion', 'ClientRegionID', 0
GO

INSERT INTO dropdown.ClientRegion 
	(ClientRegionName, ClientRegionCode, DisplayOrder)
VALUES
	('Africa', 'AF', 1),
	('Asia and ROTW', 'AR', 2),
	('Europe and Wider Multilaterals', 'EWM', 3),
	('MENA', 'MENA', 4)
GO
--End table dropdown.ClientRegion

--Begin table dropdown.ClientVacancyRoleStatus
TRUNCATE TABLE dropdown.ClientVacancyRoleStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientVacancyRoleStatus', 'ClientVacancyRoleStatusID', 0
GO

INSERT INTO dropdown.ClientVacancyRoleStatus 
	(ClientVacancyRoleStatusName, ClientVacancyRoleStatusCode, DisplayOrder)
VALUES
	('Active', 'Active', 1),
	('Inavtive', 'Inavtive', 2)
GO
--End table dropdown.ClientVacancyRoleStatus

--Begin table dropdown.Country
UPDATE dropdown.Country Set CountryName = 'Afghanistan' WHERE ISOCountryCode3 = 'AFG'
UPDATE dropdown.Country Set CountryName = 'Åland Islands' WHERE ISOCountryCode3 = 'ALA'
UPDATE dropdown.Country Set CountryName = 'Albania' WHERE ISOCountryCode3 = 'ALB'
UPDATE dropdown.Country Set CountryName = 'Algeria' WHERE ISOCountryCode3 = 'DZA'
UPDATE dropdown.Country Set CountryName = 'American Samoa' WHERE ISOCountryCode3 = 'ASM'
UPDATE dropdown.Country Set CountryName = 'Andorra' WHERE ISOCountryCode3 = 'AND'
UPDATE dropdown.Country Set CountryName = 'Angola' WHERE ISOCountryCode3 = 'AGO'
UPDATE dropdown.Country Set CountryName = 'Anguilla' WHERE ISOCountryCode3 = 'AIA'
UPDATE dropdown.Country Set CountryName = 'Antarctica' WHERE ISOCountryCode3 = 'ATA'
UPDATE dropdown.Country Set CountryName = 'Antigua and Barbuda' WHERE ISOCountryCode3 = 'ATG'
UPDATE dropdown.Country Set CountryName = 'Argentina' WHERE ISOCountryCode3 = 'ARG'
UPDATE dropdown.Country Set CountryName = 'Armenia' WHERE ISOCountryCode3 = 'ARM'
UPDATE dropdown.Country Set CountryName = 'Aruba' WHERE ISOCountryCode3 = 'ABW'
UPDATE dropdown.Country Set CountryName = 'Australia' WHERE ISOCountryCode3 = 'AUS'
UPDATE dropdown.Country Set CountryName = 'Austria' WHERE ISOCountryCode3 = 'AUT'
UPDATE dropdown.Country Set CountryName = 'Azerbaijan' WHERE ISOCountryCode3 = 'AZE'
UPDATE dropdown.Country Set CountryName = 'Bahamas (the)' WHERE ISOCountryCode3 = 'BHS'
UPDATE dropdown.Country Set CountryName = 'Bahrain' WHERE ISOCountryCode3 = 'BHR'
UPDATE dropdown.Country Set CountryName = 'Bangladesh' WHERE ISOCountryCode3 = 'BGD'
UPDATE dropdown.Country Set CountryName = 'Barbados' WHERE ISOCountryCode3 = 'BRB'
UPDATE dropdown.Country Set CountryName = 'Belarus' WHERE ISOCountryCode3 = 'BLR'
UPDATE dropdown.Country Set CountryName = 'Belgium' WHERE ISOCountryCode3 = 'BEL'
UPDATE dropdown.Country Set CountryName = 'Belize' WHERE ISOCountryCode3 = 'BLZ'
UPDATE dropdown.Country Set CountryName = 'Benin' WHERE ISOCountryCode3 = 'BEN'
UPDATE dropdown.Country Set CountryName = 'Bermuda' WHERE ISOCountryCode3 = 'BMU'
UPDATE dropdown.Country Set CountryName = 'Bhutan' WHERE ISOCountryCode3 = 'BTN'
UPDATE dropdown.Country Set CountryName = 'Bolivia (Plurinational State of)' WHERE ISOCountryCode3 = 'BOL'
UPDATE dropdown.Country Set CountryName = 'Bonaire, Sint Eustatius and Saba' WHERE ISOCountryCode3 = 'BES'
UPDATE dropdown.Country Set CountryName = 'Bosnia and Herzegovina' WHERE ISOCountryCode3 = 'BIH'
UPDATE dropdown.Country Set CountryName = 'Botswana' WHERE ISOCountryCode3 = 'BWA'
UPDATE dropdown.Country Set CountryName = 'Bouvet Island' WHERE ISOCountryCode3 = 'BVT'
UPDATE dropdown.Country Set CountryName = 'Brazil' WHERE ISOCountryCode3 = 'BRA'
UPDATE dropdown.Country Set CountryName = 'British Indian Ocean Territory (the)' WHERE ISOCountryCode3 = 'IOT'
UPDATE dropdown.Country Set CountryName = 'Brunei Darussalam' WHERE ISOCountryCode3 = 'BRN'
UPDATE dropdown.Country Set CountryName = 'Bulgaria' WHERE ISOCountryCode3 = 'BGR'
UPDATE dropdown.Country Set CountryName = 'Burkina Faso' WHERE ISOCountryCode3 = 'BFA'
UPDATE dropdown.Country Set CountryName = 'Burundi' WHERE ISOCountryCode3 = 'BDI'
UPDATE dropdown.Country Set CountryName = 'Cabo Verde' WHERE ISOCountryCode3 = 'CPV'
UPDATE dropdown.Country Set CountryName = 'Cambodia' WHERE ISOCountryCode3 = 'KHM'
UPDATE dropdown.Country Set CountryName = 'Cameroon' WHERE ISOCountryCode3 = 'CMR'
UPDATE dropdown.Country Set CountryName = 'Canada' WHERE ISOCountryCode3 = 'CAN'
UPDATE dropdown.Country Set CountryName = 'Cayman Islands (the)' WHERE ISOCountryCode3 = 'CYM'
UPDATE dropdown.Country Set CountryName = 'Central African Republic (the)' WHERE ISOCountryCode3 = 'CAF'
UPDATE dropdown.Country Set CountryName = 'Chad' WHERE ISOCountryCode3 = 'TCD'
UPDATE dropdown.Country Set CountryName = 'Chile' WHERE ISOCountryCode3 = 'CHL'
UPDATE dropdown.Country Set CountryName = 'China' WHERE ISOCountryCode3 = 'CHN'
UPDATE dropdown.Country Set CountryName = 'Christmas Island' WHERE ISOCountryCode3 = 'CXR'
UPDATE dropdown.Country Set CountryName = 'Cocos (Keeling) Islands (the)' WHERE ISOCountryCode3 = 'CCK'
UPDATE dropdown.Country Set CountryName = 'Colombia' WHERE ISOCountryCode3 = 'COL'
UPDATE dropdown.Country Set CountryName = 'Comoros (the)' WHERE ISOCountryCode3 = 'COM'
UPDATE dropdown.Country Set CountryName = 'Congo (the Democratic Republic of the)' WHERE ISOCountryCode3 = 'COD'
UPDATE dropdown.Country Set CountryName = 'Congo (the)' WHERE ISOCountryCode3 = 'COG'
UPDATE dropdown.Country Set CountryName = 'Cook Islands (the)' WHERE ISOCountryCode3 = 'COK'
UPDATE dropdown.Country Set CountryName = 'Costa Rica' WHERE ISOCountryCode3 = 'CRI'
UPDATE dropdown.Country Set CountryName = 'Côte d''Ivoire' WHERE ISOCountryCode3 = 'CIV'
UPDATE dropdown.Country Set CountryName = 'Croatia' WHERE ISOCountryCode3 = 'HRV'
UPDATE dropdown.Country Set CountryName = 'Cuba' WHERE ISOCountryCode3 = 'CUB'
UPDATE dropdown.Country Set CountryName = 'Curaçao' WHERE ISOCountryCode3 = 'CUW'
UPDATE dropdown.Country Set CountryName = 'Cyprus' WHERE ISOCountryCode3 = 'CYP'
UPDATE dropdown.Country Set CountryName = 'Czechia' WHERE ISOCountryCode3 = 'CZE'
UPDATE dropdown.Country Set CountryName = 'Denmark' WHERE ISOCountryCode3 = 'DNK'
UPDATE dropdown.Country Set CountryName = 'Djibouti' WHERE ISOCountryCode3 = 'DJI'
UPDATE dropdown.Country Set CountryName = 'Dominica' WHERE ISOCountryCode3 = 'DMA'
UPDATE dropdown.Country Set CountryName = 'Dominican Republic (the)' WHERE ISOCountryCode3 = 'DOM'
UPDATE dropdown.Country Set CountryName = 'Ecuador' WHERE ISOCountryCode3 = 'ECU'
UPDATE dropdown.Country Set CountryName = 'Egypt' WHERE ISOCountryCode3 = 'EGY'
UPDATE dropdown.Country Set CountryName = 'El Salvador' WHERE ISOCountryCode3 = 'SLV'
UPDATE dropdown.Country Set CountryName = 'Equatorial Guinea' WHERE ISOCountryCode3 = 'GNQ'
UPDATE dropdown.Country Set CountryName = 'Eritrea' WHERE ISOCountryCode3 = 'ERI'
UPDATE dropdown.Country Set CountryName = 'Estonia' WHERE ISOCountryCode3 = 'EST'
UPDATE dropdown.Country Set CountryName = 'Eswatini' WHERE ISOCountryCode3 = 'SWZ'
UPDATE dropdown.Country Set CountryName = 'Ethiopia' WHERE ISOCountryCode3 = 'ETH'
UPDATE dropdown.Country Set CountryName = 'Falkland Islands (the) [Malvinas]' WHERE ISOCountryCode3 = 'FLK'
UPDATE dropdown.Country Set CountryName = 'Faroe Islands (the)' WHERE ISOCountryCode3 = 'FRO'
UPDATE dropdown.Country Set CountryName = 'Fiji' WHERE ISOCountryCode3 = 'FJI'
UPDATE dropdown.Country Set CountryName = 'Finland' WHERE ISOCountryCode3 = 'FIN'
UPDATE dropdown.Country Set CountryName = 'France' WHERE ISOCountryCode3 = 'FRA'
UPDATE dropdown.Country Set CountryName = 'French Guiana' WHERE ISOCountryCode3 = 'GUF'
UPDATE dropdown.Country Set CountryName = 'French Polynesia' WHERE ISOCountryCode3 = 'PYF'
UPDATE dropdown.Country Set CountryName = 'French Southern Territories (the)' WHERE ISOCountryCode3 = 'ATF'
UPDATE dropdown.Country Set CountryName = 'Gabon' WHERE ISOCountryCode3 = 'GAB'
UPDATE dropdown.Country Set CountryName = 'Gambia (the)' WHERE ISOCountryCode3 = 'GMB'
UPDATE dropdown.Country Set CountryName = 'Georgia' WHERE ISOCountryCode3 = 'GEO'
UPDATE dropdown.Country Set CountryName = 'Germany' WHERE ISOCountryCode3 = 'DEU'
UPDATE dropdown.Country Set CountryName = 'Ghana' WHERE ISOCountryCode3 = 'GHA'
UPDATE dropdown.Country Set CountryName = 'Gibraltar' WHERE ISOCountryCode3 = 'GIB'
UPDATE dropdown.Country Set CountryName = 'Greece' WHERE ISOCountryCode3 = 'GRC'
UPDATE dropdown.Country Set CountryName = 'Greenland' WHERE ISOCountryCode3 = 'GRL'
UPDATE dropdown.Country Set CountryName = 'Grenada' WHERE ISOCountryCode3 = 'GRD'
UPDATE dropdown.Country Set CountryName = 'Guadeloupe' WHERE ISOCountryCode3 = 'GLP'
UPDATE dropdown.Country Set CountryName = 'Guam' WHERE ISOCountryCode3 = 'GUM'
UPDATE dropdown.Country Set CountryName = 'Guatemala' WHERE ISOCountryCode3 = 'GTM'
UPDATE dropdown.Country Set CountryName = 'Guernsey' WHERE ISOCountryCode3 = 'GGY'
UPDATE dropdown.Country Set CountryName = 'Guinea' WHERE ISOCountryCode3 = 'GIN'
UPDATE dropdown.Country Set CountryName = 'Guinea-Bissau' WHERE ISOCountryCode3 = 'GNB'
UPDATE dropdown.Country Set CountryName = 'Guyana' WHERE ISOCountryCode3 = 'GUY'
UPDATE dropdown.Country Set CountryName = 'Haiti' WHERE ISOCountryCode3 = 'HTI'
UPDATE dropdown.Country Set CountryName = 'Heard Island and McDonald Islands' WHERE ISOCountryCode3 = 'HMD'
UPDATE dropdown.Country Set CountryName = 'Holy See (the)' WHERE ISOCountryCode3 = 'VAT'
UPDATE dropdown.Country Set CountryName = 'Honduras' WHERE ISOCountryCode3 = 'HND'
UPDATE dropdown.Country Set CountryName = 'Hong Kong' WHERE ISOCountryCode3 = 'HKG'
UPDATE dropdown.Country Set CountryName = 'Hungary' WHERE ISOCountryCode3 = 'HUN'
UPDATE dropdown.Country Set CountryName = 'Iceland' WHERE ISOCountryCode3 = 'ISL'
UPDATE dropdown.Country Set CountryName = 'India' WHERE ISOCountryCode3 = 'IND'
UPDATE dropdown.Country Set CountryName = 'Indonesia' WHERE ISOCountryCode3 = 'IDN'
UPDATE dropdown.Country Set CountryName = 'Iran (Islamic Republic of)' WHERE ISOCountryCode3 = 'IRN'
UPDATE dropdown.Country Set CountryName = 'Iraq' WHERE ISOCountryCode3 = 'IRQ'
UPDATE dropdown.Country Set CountryName = 'Ireland' WHERE ISOCountryCode3 = 'IRL'
UPDATE dropdown.Country Set CountryName = 'Isle of Man' WHERE ISOCountryCode3 = 'IMN'
UPDATE dropdown.Country Set CountryName = 'Israel' WHERE ISOCountryCode3 = 'ISR'
UPDATE dropdown.Country Set CountryName = 'Italy' WHERE ISOCountryCode3 = 'ITA'
UPDATE dropdown.Country Set CountryName = 'Jamaica' WHERE ISOCountryCode3 = 'JAM'
UPDATE dropdown.Country Set CountryName = 'Japan' WHERE ISOCountryCode3 = 'JPN'
UPDATE dropdown.Country Set CountryName = 'Jersey' WHERE ISOCountryCode3 = 'JEY'
UPDATE dropdown.Country Set CountryName = 'Jordan' WHERE ISOCountryCode3 = 'JOR'
UPDATE dropdown.Country Set CountryName = 'Kazakhstan' WHERE ISOCountryCode3 = 'KAZ'
UPDATE dropdown.Country Set CountryName = 'Kenya' WHERE ISOCountryCode3 = 'KEN'
UPDATE dropdown.Country Set CountryName = 'Kiribati' WHERE ISOCountryCode3 = 'KIR'
UPDATE dropdown.Country Set CountryName = 'Korea (the Democratic People''s Republic of)' WHERE ISOCountryCode3 = 'PRK'
UPDATE dropdown.Country Set CountryName = 'Korea (the Republic of)' WHERE ISOCountryCode3 = 'KOR'
UPDATE dropdown.Country Set CountryName = 'Kuwait' WHERE ISOCountryCode3 = 'KWT'
UPDATE dropdown.Country Set CountryName = 'Kyrgyzstan' WHERE ISOCountryCode3 = 'KGZ'
UPDATE dropdown.Country Set CountryName = 'Lao People''s Democratic Republic (the)' WHERE ISOCountryCode3 = 'LAO'
UPDATE dropdown.Country Set CountryName = 'Latvia' WHERE ISOCountryCode3 = 'LVA'
UPDATE dropdown.Country Set CountryName = 'Lebanon' WHERE ISOCountryCode3 = 'LBN'
UPDATE dropdown.Country Set CountryName = 'Lesotho' WHERE ISOCountryCode3 = 'LSO'
UPDATE dropdown.Country Set CountryName = 'Liberia' WHERE ISOCountryCode3 = 'LBR'
UPDATE dropdown.Country Set CountryName = 'Libya' WHERE ISOCountryCode3 = 'LBY'
UPDATE dropdown.Country Set CountryName = 'Liechtenstein' WHERE ISOCountryCode3 = 'LIE'
UPDATE dropdown.Country Set CountryName = 'Lithuania' WHERE ISOCountryCode3 = 'LTU'
UPDATE dropdown.Country Set CountryName = 'Luxembourg' WHERE ISOCountryCode3 = 'LUX'
UPDATE dropdown.Country Set CountryName = 'Macao' WHERE ISOCountryCode3 = 'MAC'
UPDATE dropdown.Country Set CountryName = 'Madagascar' WHERE ISOCountryCode3 = 'MDG'
UPDATE dropdown.Country Set CountryName = 'Malawi' WHERE ISOCountryCode3 = 'MWI'
UPDATE dropdown.Country Set CountryName = 'Malaysia' WHERE ISOCountryCode3 = 'MYS'
UPDATE dropdown.Country Set CountryName = 'Maldives' WHERE ISOCountryCode3 = 'MDV'
UPDATE dropdown.Country Set CountryName = 'Mali' WHERE ISOCountryCode3 = 'MLI'
UPDATE dropdown.Country Set CountryName = 'Malta' WHERE ISOCountryCode3 = 'MLT'
UPDATE dropdown.Country Set CountryName = 'Marshall Islands (the)' WHERE ISOCountryCode3 = 'MHL'
UPDATE dropdown.Country Set CountryName = 'Martinique' WHERE ISOCountryCode3 = 'MTQ'
UPDATE dropdown.Country Set CountryName = 'Mauritania' WHERE ISOCountryCode3 = 'MRT'
UPDATE dropdown.Country Set CountryName = 'Mauritius' WHERE ISOCountryCode3 = 'MUS'
UPDATE dropdown.Country Set CountryName = 'Mayotte' WHERE ISOCountryCode3 = 'MYT'
UPDATE dropdown.Country Set CountryName = 'Mexico' WHERE ISOCountryCode3 = 'MEX'
UPDATE dropdown.Country Set CountryName = 'Micronesia (Federated States of)' WHERE ISOCountryCode3 = 'FSM'
UPDATE dropdown.Country Set CountryName = 'Moldova (the Republic of)' WHERE ISOCountryCode3 = 'MDA'
UPDATE dropdown.Country Set CountryName = 'Monaco' WHERE ISOCountryCode3 = 'MCO'
UPDATE dropdown.Country Set CountryName = 'Mongolia' WHERE ISOCountryCode3 = 'MNG'
UPDATE dropdown.Country Set CountryName = 'Montenegro' WHERE ISOCountryCode3 = 'MNE'
UPDATE dropdown.Country Set CountryName = 'Montserrat' WHERE ISOCountryCode3 = 'MSR'
UPDATE dropdown.Country Set CountryName = 'Morocco' WHERE ISOCountryCode3 = 'MAR'
UPDATE dropdown.Country Set CountryName = 'Mozambique' WHERE ISOCountryCode3 = 'MOZ'
UPDATE dropdown.Country Set CountryName = 'Myanmar' WHERE ISOCountryCode3 = 'MMR'
UPDATE dropdown.Country Set CountryName = 'Namibia' WHERE ISOCountryCode3 = 'NAM'
UPDATE dropdown.Country Set CountryName = 'Nauru' WHERE ISOCountryCode3 = 'NRU'
UPDATE dropdown.Country Set CountryName = 'Nepal' WHERE ISOCountryCode3 = 'NPL'
UPDATE dropdown.Country Set CountryName = 'Netherlands (the)' WHERE ISOCountryCode3 = 'NLD'
UPDATE dropdown.Country Set CountryName = 'New Caledonia' WHERE ISOCountryCode3 = 'NCL'
UPDATE dropdown.Country Set CountryName = 'New Zealand' WHERE ISOCountryCode3 = 'NZL'
UPDATE dropdown.Country Set CountryName = 'Nicaragua' WHERE ISOCountryCode3 = 'NIC'
UPDATE dropdown.Country Set CountryName = 'Niger (the)' WHERE ISOCountryCode3 = 'NER'
UPDATE dropdown.Country Set CountryName = 'Nigeria' WHERE ISOCountryCode3 = 'NGA'
UPDATE dropdown.Country Set CountryName = 'Niue' WHERE ISOCountryCode3 = 'NIU'
UPDATE dropdown.Country Set CountryName = 'Norfolk Island' WHERE ISOCountryCode3 = 'NFK'
UPDATE dropdown.Country Set CountryName = 'North Macedonia' WHERE ISOCountryCode3 = 'MKD'
UPDATE dropdown.Country Set CountryName = 'Northern Mariana Islands (the)' WHERE ISOCountryCode3 = 'MNP'
UPDATE dropdown.Country Set CountryName = 'Norway' WHERE ISOCountryCode3 = 'NOR'
UPDATE dropdown.Country Set CountryName = 'Oman' WHERE ISOCountryCode3 = 'OMN'
UPDATE dropdown.Country Set CountryName = 'Pakistan' WHERE ISOCountryCode3 = 'PAK'
UPDATE dropdown.Country Set CountryName = 'Palau' WHERE ISOCountryCode3 = 'PLW'
UPDATE dropdown.Country Set CountryName = 'Palestine, State of' WHERE ISOCountryCode3 = 'PSE'
UPDATE dropdown.Country Set CountryName = 'Panama' WHERE ISOCountryCode3 = 'PAN'
UPDATE dropdown.Country Set CountryName = 'Papua New Guinea' WHERE ISOCountryCode3 = 'PNG'
UPDATE dropdown.Country Set CountryName = 'Paraguay' WHERE ISOCountryCode3 = 'PRY'
UPDATE dropdown.Country Set CountryName = 'Peru' WHERE ISOCountryCode3 = 'PER'
UPDATE dropdown.Country Set CountryName = 'Philippines (the)' WHERE ISOCountryCode3 = 'PHL'
UPDATE dropdown.Country Set CountryName = 'Pitcairn' WHERE ISOCountryCode3 = 'PCN'
UPDATE dropdown.Country Set CountryName = 'Poland' WHERE ISOCountryCode3 = 'POL'
UPDATE dropdown.Country Set CountryName = 'Portugal' WHERE ISOCountryCode3 = 'PRT'
UPDATE dropdown.Country Set CountryName = 'Puerto Rico' WHERE ISOCountryCode3 = 'PRI'
UPDATE dropdown.Country Set CountryName = 'Qatar' WHERE ISOCountryCode3 = 'QAT'
UPDATE dropdown.Country Set CountryName = 'Réunion' WHERE ISOCountryCode3 = 'REU'
UPDATE dropdown.Country Set CountryName = 'Romania' WHERE ISOCountryCode3 = 'ROU'
UPDATE dropdown.Country Set CountryName = 'Russian Federation (the)' WHERE ISOCountryCode3 = 'RUS'
UPDATE dropdown.Country Set CountryName = 'Rwanda' WHERE ISOCountryCode3 = 'RWA'
UPDATE dropdown.Country Set CountryName = 'Saint Barthélemy' WHERE ISOCountryCode3 = 'BLM'
UPDATE dropdown.Country Set CountryName = 'Saint Helena, Ascension and Tristan da Cunha' WHERE ISOCountryCode3 = 'SHN'
UPDATE dropdown.Country Set CountryName = 'Saint Kitts and Nevis' WHERE ISOCountryCode3 = 'KNA'
UPDATE dropdown.Country Set CountryName = 'Saint Lucia' WHERE ISOCountryCode3 = 'LCA'
UPDATE dropdown.Country Set CountryName = 'Saint Martin (French part)' WHERE ISOCountryCode3 = 'MAF'
UPDATE dropdown.Country Set CountryName = 'Saint Pierre and Miquelon' WHERE ISOCountryCode3 = 'SPM'
UPDATE dropdown.Country Set CountryName = 'Saint Vincent and the Grenadines' WHERE ISOCountryCode3 = 'VCT'
UPDATE dropdown.Country Set CountryName = 'Samoa' WHERE ISOCountryCode3 = 'WSM'
UPDATE dropdown.Country Set CountryName = 'San Marino' WHERE ISOCountryCode3 = 'SMR'
UPDATE dropdown.Country Set CountryName = 'Sao Tome and Principe' WHERE ISOCountryCode3 = 'STP'
UPDATE dropdown.Country Set CountryName = 'Saudi Arabia' WHERE ISOCountryCode3 = 'SAU'
UPDATE dropdown.Country Set CountryName = 'Senegal' WHERE ISOCountryCode3 = 'SEN'
UPDATE dropdown.Country Set CountryName = 'Serbia' WHERE ISOCountryCode3 = 'SRB'
UPDATE dropdown.Country Set CountryName = 'Seychelles' WHERE ISOCountryCode3 = 'SYC'
UPDATE dropdown.Country Set CountryName = 'Sierra Leone' WHERE ISOCountryCode3 = 'SLE'
UPDATE dropdown.Country Set CountryName = 'Singapore' WHERE ISOCountryCode3 = 'SGP'
UPDATE dropdown.Country Set CountryName = 'Sint Maarten (Dutch part)' WHERE ISOCountryCode3 = 'SXM'
UPDATE dropdown.Country Set CountryName = 'Slovakia' WHERE ISOCountryCode3 = 'SVK'
UPDATE dropdown.Country Set CountryName = 'Slovenia' WHERE ISOCountryCode3 = 'SVN'
UPDATE dropdown.Country Set CountryName = 'Solomon Islands' WHERE ISOCountryCode3 = 'SLB'
UPDATE dropdown.Country Set CountryName = 'Somalia' WHERE ISOCountryCode3 = 'SOM'
UPDATE dropdown.Country Set CountryName = 'South Africa' WHERE ISOCountryCode3 = 'ZAF'
UPDATE dropdown.Country Set CountryName = 'South Georgia and the South Sandwich Islands' WHERE ISOCountryCode3 = 'SGS'
UPDATE dropdown.Country Set CountryName = 'South Sudan' WHERE ISOCountryCode3 = 'SSD'
UPDATE dropdown.Country Set CountryName = 'Spain' WHERE ISOCountryCode3 = 'ESP'
UPDATE dropdown.Country Set CountryName = 'Sri Lanka' WHERE ISOCountryCode3 = 'LKA'
UPDATE dropdown.Country Set CountryName = 'Sudan (the)' WHERE ISOCountryCode3 = 'SDN'
UPDATE dropdown.Country Set CountryName = 'Suriname' WHERE ISOCountryCode3 = 'SUR'
UPDATE dropdown.Country Set CountryName = 'Svalbard and Jan Mayen' WHERE ISOCountryCode3 = 'SJM'
UPDATE dropdown.Country Set CountryName = 'Sweden' WHERE ISOCountryCode3 = 'SWE'
UPDATE dropdown.Country Set CountryName = 'Switzerland' WHERE ISOCountryCode3 = 'CHE'
UPDATE dropdown.Country Set CountryName = 'Syrian Arab Republic (the)' WHERE ISOCountryCode3 = 'SYR'
UPDATE dropdown.Country Set CountryName = 'Taiwan (Province of China)' WHERE ISOCountryCode3 = 'TWN'
UPDATE dropdown.Country Set CountryName = 'Tajikistan' WHERE ISOCountryCode3 = 'TJK'
UPDATE dropdown.Country Set CountryName = 'Tanzania, the United Republic of' WHERE ISOCountryCode3 = 'TZA'
UPDATE dropdown.Country Set CountryName = 'Thailand' WHERE ISOCountryCode3 = 'THA'
UPDATE dropdown.Country Set CountryName = 'Timor-Leste' WHERE ISOCountryCode3 = 'TLS'
UPDATE dropdown.Country Set CountryName = 'Togo' WHERE ISOCountryCode3 = 'TGO'
UPDATE dropdown.Country Set CountryName = 'Tokelau' WHERE ISOCountryCode3 = 'TKL'
UPDATE dropdown.Country Set CountryName = 'Tonga' WHERE ISOCountryCode3 = 'TON'
UPDATE dropdown.Country Set CountryName = 'Trinidad and Tobago' WHERE ISOCountryCode3 = 'TTO'
UPDATE dropdown.Country Set CountryName = 'Tunisia' WHERE ISOCountryCode3 = 'TUN'
UPDATE dropdown.Country Set CountryName = 'Turkey' WHERE ISOCountryCode3 = 'TUR'
UPDATE dropdown.Country Set CountryName = 'Turkmenistan' WHERE ISOCountryCode3 = 'TKM'
UPDATE dropdown.Country Set CountryName = 'Turks and Caicos Islands (the)' WHERE ISOCountryCode3 = 'TCA'
UPDATE dropdown.Country Set CountryName = 'Tuvalu' WHERE ISOCountryCode3 = 'TUV'
UPDATE dropdown.Country Set CountryName = 'Uganda' WHERE ISOCountryCode3 = 'UGA'
UPDATE dropdown.Country Set CountryName = 'Ukraine' WHERE ISOCountryCode3 = 'UKR'
UPDATE dropdown.Country Set CountryName = 'United Arab Emirates (the)' WHERE ISOCountryCode3 = 'ARE'
UPDATE dropdown.Country Set CountryName = 'United Kingdom of Great Britain and Northern Ireland (the)' WHERE ISOCountryCode3 = 'GBR'
UPDATE dropdown.Country Set CountryName = 'United States Minor Outlying Islands (the)' WHERE ISOCountryCode3 = 'UMI'
UPDATE dropdown.Country Set CountryName = 'United States of America (the)' WHERE ISOCountryCode3 = 'USA'
UPDATE dropdown.Country Set CountryName = 'Uruguay' WHERE ISOCountryCode3 = 'URY'
UPDATE dropdown.Country Set CountryName = 'Uzbekistan' WHERE ISOCountryCode3 = 'UZB'
UPDATE dropdown.Country Set CountryName = 'Vanuatu' WHERE ISOCountryCode3 = 'VUT'
UPDATE dropdown.Country Set CountryName = 'Venezuela (Bolivarian Republic of)' WHERE ISOCountryCode3 = 'VEN'
UPDATE dropdown.Country Set CountryName = 'Viet Nam' WHERE ISOCountryCode3 = 'VNM'
UPDATE dropdown.Country Set CountryName = 'Virgin Islands (British)' WHERE ISOCountryCode3 = 'VGB'
UPDATE dropdown.Country Set CountryName = 'Virgin Islands (U.S.)' WHERE ISOCountryCode3 = 'VIR'
UPDATE dropdown.Country Set CountryName = 'Wallis and Futuna' WHERE ISOCountryCode3 = 'WLF'
UPDATE dropdown.Country Set CountryName = 'Western Sahara*' WHERE ISOCountryCode3 = 'ESH'
UPDATE dropdown.Country Set CountryName = 'Yemen' WHERE ISOCountryCode3 = 'YEM'
UPDATE dropdown.Country Set CountryName = 'Zambia' WHERE ISOCountryCode3 = 'ZMB'
UPDATE dropdown.Country Set CountryName = 'Zimbabwe' WHERE ISOCountryCode3 = 'ZWE'
GO

UPDATE C
SET C.DisplayOrder = 100
FROM dropdown.Country C
WHERE C.ISOCountryCode2 = 'DE'
GO
--End table dropdown.Country

--Begin table dropdown.EmploymentType
TRUNCATE TABLE dropdown.EmploymentType
GO

EXEC utility.InsertIdentityValue 'dropdown.EmploymentType', 'EmploymentTypeID', 0
GO

INSERT INTO dropdown.EmploymentType 
	(EmploymentTypeName, EmploymentTypeCode, DisplayOrder)
VALUES
	('CSG', 'CSG', 1),
	('Senior Adviser', 'SA', 2),
	('Core Staff', 'CS', 3),
	('Serving Police', 'SP', 4),
	('FCO Contract', 'FCO', 5)
GO
--End table dropdown.EmploymentType

--Begin table dropdown.ImplementationDifficulty
TRUNCATE TABLE dropdown.ImplementationDifficulty
GO

EXEC utility.InsertIdentityValue 'dropdown.ImplementationDifficulty', 'ImplementationDifficultyID', 0
GO

INSERT INTO dropdown.ImplementationDifficulty 
	(ImplementationDifficultyName, ImplementationDifficultyCode, DisplayOrder)
VALUES
	('Low', 'Low', 1),
	('Medium', 'Medium', 2),
	('High', 'High', 3)
GO
--End table dropdown.ImplementationDifficulty

--Begin table dropdown.ImplementationImpact
TRUNCATE TABLE dropdown.ImplementationImpact
GO

EXEC utility.InsertIdentityValue 'dropdown.ImplementationImpact', 'ImplementationImpactID', 0
GO

INSERT INTO dropdown.ImplementationImpact 
	(ImplementationImpactName, ImplementationImpactCode, DisplayOrder)
VALUES
	('Low', 'Low', 1),
	('Medium', 'Medium', 2),
	('High', 'High', 3)
GO
--End table dropdown.ImplementationImpact

--Begin table dropdown.LessonCategory
TRUNCATE TABLE dropdown.LessonCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonCategory', 'LessonCategoryID', 0
GO

INSERT INTO dropdown.LessonCategory 
	(LessonCategoryName, LessonCategoryCode, DisplayOrder)
VALUES
	('Staffing', 'Staffing', 1),
	('Coordination', 'Coordination', 2),
	('Partners', 'Partners', 3),
	('Leadership', 'Leadership', 4),
	('Preparedness', 'Preparedness', 5)
GO
--End table dropdown.LessonCategory

--Begin table dropdown.LessonOwner
TRUNCATE TABLE dropdown.LessonOwner
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonOwner', 'LessonOwnerID', 0
GO

INSERT INTO dropdown.LessonOwner 
	(LessonOwnerName, LessonOwnerCode)
VALUES
	('Advisory', 'Advisory'),
	('HR', 'HR'),
	('HRG', 'HRG'),
	('HSOT Senior Management', 'HSOTSeniorManagement'),
	('HSOT', 'HSOT'),
	('P&L', 'P&L'),
	('Readiness & Response', 'Readiness&Response')
GO
--End table dropdown.LessonOwner

--Begin table dropdown.LessonPublicationStatus
TRUNCATE TABLE dropdown.LessonPublicationStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonPublicationStatus', 'LessonPublicationStatusID', 0
GO

INSERT INTO dropdown.LessonPublicationStatus 
	(LessonPublicationStatusName, LessonPublicationStatusCode, DisplayOrder)
VALUES
	('For publication', 'LP', 1),
	('Not for publication', 'LUP', 2)
GO

UPDATE LPS
SET 
	LPS.LessonPublicationStatusName = 'Pending initial review',
	LPS.LessonPublicationStatusCode = 'L'
FROM dropdown.LessonPublicationStatus LPS
WHERE LPS.LessonPublicationStatusID = 0
GO
--End table dropdown.LessonPublicationStatus

--Begin table dropdown.LessonStatus
TRUNCATE TABLE dropdown.LessonStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonStatus', 'LessonStatusID', 0
GO

INSERT INTO dropdown.LessonStatus 
	(LessonStatusName, LessonStatusCode, DisplayOrder)
VALUES
	('Assigned', 'Assigned', 1),
	('Progressing', 'Progressing', 2),
	('Implemented', 'Implemented', 3),
	('Best Practice', 'BestPractice', 4)
GO
--End table dropdown.LessonStatus

--Begin table dropdown.LessonType
TRUNCATE TABLE dropdown.LessonType
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonType', 'LessonTypeID', 0
GO

INSERT INTO dropdown.LessonType 
	(LessonTypeName, LessonTypeCode, DisplayOrder)
VALUES
	('Project', 'Project', 1),
	('Programme', 'Program', 2),
	('Project and Programme', 'ProjectProgram', 3)
GO
--End table dropdown.LessonType

--Begin table dropdown.LessonUserClass
TRUNCATE TABLE dropdown.LessonUserClass
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonUserClass', 'LessonUserClassID', 0
GO

INSERT INTO dropdown.LessonUserClass 
	(LessonUserClassName, LessonUserClassCode)
VALUES
	('Advisory', 'Advisory'),
	('HR', 'HR'),
	('HRG', 'HRG'),
	('HSOT Senior Management', 'HSOTSeniorManagement'),
	('HSOT', 'HSOT'),
	('P&L', 'P&L'),
	('Readiness & Response', 'Readiness&Response')
GO
--End table dropdown.LessonUserClass

--Begin table dropdown.MedicalClearanceReviewType
TRUNCATE TABLE dropdown.MedicalClearanceReviewType
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceReviewType', 'MedicalClearanceReviewTypeID', 0
GO

INSERT INTO dropdown.MedicalClearanceReviewType 
	(MedicalClearanceReviewTypeName, MedicalClearanceReviewTypeCode, DisplayOrder)
VALUES
	('Initial', 'Initial', 1),
	('Followup', 'Followup', 2)
GO
--End table dropdown.MedicalClearanceReviewType

--Begin table dropdown.PersonType
IF NOT EXISTS (SELECT 1 FROM dropdown.PersonType PT WHERE PT.PersonTypeCode = 'Client')
	BEGIN

	INSERT INTO dropdown.PersonType 
		(PersonTypeName, PersonTypeCode, DisplayOrder, IsActive)
	VALUES
		('Client', 'Client', 3, 0)

	END
--ENDIF
GO
--End table dropdown.PersonType

--Begin table dropdown.ProjectLaborCode
TRUNCATE TABLE dropdown.ProjectLaborCode
GO

EXEC utility.InsertIdentityValue 'dropdown.ProjectLaborCode', 'ProjectLaborCodeID', 0
GO

INSERT INTO dropdown.ProjectLaborCode 
	(ProjectLaborCodeName, ProjectLaborCodeCode, DisplayOrder)
VALUES
	('Project Labour Code 1', 'Code1', 1),
	('Project Labour COde 2', 'Code 2', 2)
GO
--End table dropdown.ProjectLaborCode

--Begin table dropdown.PublishTo
TRUNCATE TABLE dropdown.PublishTo
GO

EXEC utility.InsertIdentityValue 'dropdown.PublishTo', 'PublishToID', 0
GO

INSERT INTO dropdown.PublishTo 
	(PublishToName, PublishToCode, DisplayOrder)
VALUES
	('CSG Core','CSGCore',1),
	('CSG Senior','CSGSenior',2),
	('Category','Category',3),
	('SubCategory','SubCategory',4)
GO
--End table dropdown.PublishTo

--Begin table dropdown.RecruitmentPathway
TRUNCATE TABLE dropdown.RecruitmentPathway
GO

EXEC utility.InsertIdentityValue 'dropdown.RecruitmentPathway', 'RecruitmentPathwayID', 0
GO

INSERT INTO dropdown.RecruitmentPathway 
	(RecruitmentPathwayName, RecruitmentPathwayCode, DisplayOrder)
VALUES
	('Direct', 'Direct', 1),
	('Competitive', 'Competitive', 2)
GO
--End table dropdown.RecruitmentPathway

--Begin table dropdown.RegionalPriorityTier
TRUNCATE TABLE dropdown.RegionalPriorityTier
GO

EXEC utility.InsertIdentityValue 'dropdown.RegionalPriorityTier', 'RegionalPriorityTierID', 0
GO

INSERT INTO dropdown.RegionalPriorityTier 
	(RegionalPriorityTierName, RegionalPriorityTierCode, DisplayOrder)
VALUES
	('Tier 0', 'Tier0', 1),
	('Tier 1', 'Tier1', 2),
	('Tier 2', 'Tier2', 3),
	('Tier 3', 'Tier3', 4),
	('N/A', 'NA', 5)
GO
--End table dropdown.RegionalPriorityTier

--Begin table dropdown.RoleCategory
TRUNCATE TABLE dropdown.RoleCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.RoleCategory', 'RoleCategoryID', 0
GO

INSERT INTO dropdown.RoleCategory 
	(RoleCategoryName, RoleCategoryCode, DisplayOrder)
VALUES
	('Community Safety, Security & Access to Justice','CSSAJ',1),
	('Conflict','Conflict',2),
	('Defence','Defence',3),
	('Extremism, Violent Extremism and Terrorism (EVET)','EVET',4),
	('Gender, Conflict and Stability','GCS',5),
	('Governance','Governance',6),
	('Integrated Border Management','IBM',7),
	('Justice','Justice',8),
	('Monitoring and Evaluation (M&E)','M&E',9),
	('Multilateral','Multilateral',10),
	('Government Partnerships International (GPI)','GPI',11),
	('Operations','Operations',12),
	('Organised Crime','OC',13),
	('Policing Adviser','PA',14),
	('PSVI Criminal Lawyers','CL',15),
	('PSVI Gender-Based Violence Experts','GBVE',16),
	('PSVI International Investigating Officers','IIO',17),
	('PSVI Psychosocial Experts and Social Workers','PESW',18),
	('PSVI Sexual Offences Examiners','SOE',19),
	('PSVI Training Experts','TE',20),
	('Security Institutions','SI',21),
	('SMS','SMS',22),
	('Stabilisation','Stabilisation',23),
	('Strategic Communications','SC',24),
	('Serving Police','SP',25)
GO
--End table dropdown.RoleCategory

--Begin table dropdown.RoleSubCategory
TRUNCATE TABLE dropdown.RoleSubCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.RoleSubCategory', 'RoleSubCategoryID', 0
GO

INSERT INTO dropdown.RoleSubCategory 
	(RoleSubCategoryName, RoleSubCategoryCode, DisplayOrder)
VALUES
	('Community Safety, Security & Access to Justice','CSSAJ',1),
	('Conflict','Conflict',2),
	('Defence','Defence',3),
	('Extremism, Violent Extremism and Terrorism (EVET)','EVET',4),
	('Gender, Conflict and Stability','GCS',5),
	('Governance','Governance',6),
	('Integrated Border Management','IBM',7),
	('Justice','Justice',8),
	('Monitoring and Evaluation (M&E)','M&E',9),
	('Multilateral','Multilateral',10),
	('Government Partnerships International (GPI)','GPI',11),
	('Operations','Operations',12),
	('Organised Crime','OC',13),
	('Policing Adviser','PA',14),
	('PSVI Criminal Lawyers','CL',15),
	('PSVI Gender-Based Violence Experts','GBVE',16),
	('PSVI International Investigating Officers','IIO',17),
	('PSVI Psychosocial Experts and Social Workers','PESW',18),
	('PSVI Sexual Offences Examiners','SOE',19),
	('PSVI Training Experts','TE',20),
	('Security Institutions','SI',21),
	('SMS','SMS',22),
	('Stabilisation','Stabilisation',23),
	('Strategic Communications','SC',24),
	('Serving Police','SP',25)
GO
--End table dropdown.RoleSubCategory

--Begin table dropdown.SecurityClearanceProcessStep
IF NOT EXISTS (SELECT 1 FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = 'PREPFOLLOWUP')
	BEGIN

	UPDATE SCPS
	SET SCPS.DisplayOrder = SCPS.DisplayOrder + 1
	FROM dropdown.SecurityClearanceProcessStep SCPS
	WHERE SCPS.DisplayOrder > 1

	INSERT INTO dropdown.SecurityClearanceProcessStep 
		(SecurityClearanceProcessStepName, SecurityClearanceProcessStepCode, DisplayOrder)
	VALUES
		('Application in Preperation - Follow Up', 'PREPFOLLOWUP', 2)

	END
--ENDIF
GO
--End table dropdown.SecurityClearanceProcessStep

--Begin table dropdown.SecurityClearanceReviewType
TRUNCATE TABLE dropdown.SecurityClearanceReviewType
GO

EXEC utility.InsertIdentityValue 'dropdown.SecurityClearanceReviewType', 'SecurityClearanceReviewTypeID', 0
GO

INSERT INTO dropdown.SecurityClearanceReviewType 
	(SecurityClearanceReviewTypeName, SecurityClearanceReviewTypeCode, DisplayOrder)
VALUES
	('Initial', 'Initial', 1),
	('Initial - Followup', 'InitialFollowup', 2),
	('Discretionary', 'Discretionary', 3),
	('Discretionary - Followup', 'DiscretionaryFollowup', 4)
GO
--End table dropdown.SecurityClearanceReviewType

--Begin table dropdown.TaskType
TRUNCATE TABLE dropdown.TaskType
GO

EXEC utility.InsertIdentityValue 'dropdown.TaskType', 'TaskTypeID', 0
GO

INSERT INTO dropdown.TaskType 
	(TaskTypeName, TaskTypeCode, DisplayOrder)
VALUES
	('Analysis','Analysis',1),
	('Annual Review','AnnualReview',2),
	('Commissioned Lessons Review','CommissionedLessonsReview',3),
	('JACS','JACS',4),
	('Ongoing Engagement','OngoingEngagement',5),
	('Programme Inputs: Scope','Scope',6),
	('Programme Inputs: Design','Design',7),
	('Programme Inputs: Review','Review',8),
	('Sector Assessments','SectorAssessments',9),
	('SOCJA','SOCJA',10),
	('Strategy Development Review','StrategyDevelopmentReview',11)
GO
--End table dropdown.TaskType

--Begin table dropdown.ThematicFocus
TRUNCATE TABLE dropdown.ThematicFocus
GO

EXEC utility.InsertIdentityValue 'dropdown.ThematicFocus', 'ThematicFocusID', 0
GO

INSERT INTO dropdown.ThematicFocus 
	(ThematicFocusName, ThematicFocusCode, DisplayOrder)
VALUES
	('CT', 'CT', 1),
	('CVE', 'CVE', 2),
	('Conflict Sensitivity', 'ConflictSensitivity', 3),
	('Gender', 'Gender', 4),
	('Lessons', 'Lessons', 5),
	('M&E', 'M&E', 6),
	('Migration', 'Migration', 7),
	('Modern Slavery', 'ModernSlavery', 8),
	('International Policing', 'InternationalPolicing', 9),
	('Political Deals', 'PoliticalDeals', 10),
	('Programme Inputs', 'ProgrammeInputs', 11),
	('Security and Justice', 'SecurityJustice', 12),
	('SOC', 'SOC', 13),
	('Stabilisation', 'Stabilisation', 14),
	('Other', 'Other', 15)
GO
--End table dropdown.ThematicFocus

--Begin table permissionable.Permissionable

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='Add / edit feedback', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='FeedbackAddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.FeedbackAddUpdate', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='View feedback', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='FeedbackView', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.FeedbackView', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='Add / edit a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ClientAddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.VacancyAddUpdate', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='View a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.VacancyView', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='Add / edit a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ClientVacancy.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='Manage Sift Matric', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ApplicationList', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ClientVacancy.ApplicationList', 
	@PERMISSIONCODE=NULL;
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='View the list of client vacancies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ClientVacancy.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='View a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ClientVacancy.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='Add / edit a MEAL record', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='View the list of MEAL records', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='Export the list of MEAL records', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.List.Export', 
	@PERMISSIONCODE='Export';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='View a MEAL record', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Invite a client into the system', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='Invite', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Person.Invite', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Send an invite to a user', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='SendInvite', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.SendInvite', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='Save application management', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ManageApplicationSave', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.ManageApplicationSave', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='View application management', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ManageApplicationView', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.ManageApplicationView', 
	@PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.AddColumn @TableName, 'PermissionableTemplateCode', 'VARCHAR(50)'
GO
--End table permissionable.PermissionableTemplate

--Begin table person.PersonGroup
IF NOT EXISTS (SELECT 1 FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'Interview Panel Member - Scheduler')
	BEGIN

	INSERT INTO person.PersonGroup 
		(PersonGroupName, PersonGroupDescription, ApprovalAuthorityLevelID) 
	VALUES 
		--('CSG Coordinator', 'Coordinates CSG''s', 1),
		('CSG Manager', 'Manages CSGs', 2),
		('CSG Technical Manager', 'Technically, Manages CSGs', 2),
		('Interview Panel', 'This group is managed programmatically from the application workflow', 2),
		('Interview Panel Member - Scheduler', 'This group is managed programmatically from the application workflow', 2)

	END
--ENDIF
GO
--End table person.PersonGroup

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.WorkflowName = 'Application')
	BEGIN

	DECLARE @tOutput1 TABLE (WorkflowID INT NOT NULL PRIMARY KEY)
	DECLARE @tOutput2 TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepNumber INT)
	DECLARE @tOutput3 TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupName VARCHAR(250))

	DECLARE @nWorkflowID INT

	INSERT INTO workflow.Workflow 
		(WorkflowName, EntityTypeCode, IsActive, ProjectID, WorkflowCompleteStatusName, IsSUperAdminOnly)
	OUTPUT INSERTED.WorkflowID INTO @tOutput1
	SELECT
		'Application', 
		'Application', 
		1, 
		P.ProjectID, 
		'Workflow Complete', 
		1
	FROM dropdown.Project P
	WHERE P.ProjectName = 'SU'

	SELECT @nWorkflowID = O.WorkflowID FROM @tOutput1 O

	INSERT INTO workflow.WorkflowStep 
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStepStatusName) 
	OUTPUT INSERTED.WorkflowStepID, INSERTED.WorkflowStepNumber INTO @tOutput2
	VALUES 
		(@nWorkflowID, 1, 'Preparing for Competency Review', 'Application Submitted'),
		(@nWorkflowID, 2, 'Competency Review', 'Competency Review'),
		(@nWorkflowID, 3, 'Technical Review', 'Technical Review'),
		(@nWorkflowID, 4, 'Coordinate Interview', 'Ready for Interview'),
		(@nWorkflowID, 5, 'Propose Dates', 'Scheduling'),
		(@nWorkflowID, 6, 'Confirm Dates', 'Pending Date Confirmation'),
		(@nWorkflowID, 7, 'Notify Interviewee', 'Contacting Reviewee for Interview'),
		(@nWorkflowID, 8, 'Interview Scheduled', 'Interview Pending'),
		(@nWorkflowID, 9, 'Interview Complete', 'Interview Complete')

	INSERT INTO workflow.WorkflowStepGroup 
		(WorkflowStepID, WorkflowStepGroupName, IsFinancialApprovalRequired) 
	OUTPUT INSERTED.WorkflowStepGroupID, INSERTED.WorkflowStepGroupName INTO @tOutput3
	SELECT
		O.WorkflowStepID,
		'Application Workflow Step Group ' + CAST(O.WorkflowStepNumber AS VARCHAR(5)),
		0
	FROM @tOutput2 O

	INSERT INTO workflow.WorkflowStepGroupPersonGroup 
		(WorkflowStepGroupID, PersonGroupID) 
	SELECT
		O.WorkflowStepGroupID,
		
		CASE
			WHEN O.WorkflowStepGroupName IN ('Application Workflow Step Group 1', 'Application Workflow Step Group 4', 'Application Workflow Step Group 7', 'Application Workflow Step Group 9')
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'CSG Coordinator')
			WHEN O.WorkflowStepGroupName = 'Application Workflow Step Group 2'
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'CSG Manager')
			WHEN O.WorkflowStepGroupName = 'Application Workflow Step Group 3'
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'CSG Technical Manager')
			WHEN O.WorkflowStepGroupName = 'Application Workflow Step Group 5'
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'Interview Panel Member - Scheduler')
			ELSE (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'Interview Panel')
		END

	FROM @tOutput3 O

	END
--ENDIF
GO