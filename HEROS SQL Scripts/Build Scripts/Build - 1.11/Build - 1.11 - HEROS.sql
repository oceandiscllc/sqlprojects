-- File Name:	Build - 1.11 - HEROS.sql
-- Build Key:	Build - 1.11 - 2019.04.13 18.38.52

USE HermisCloud
GO

-- ==============================================================================================================================
-- Schemas:
--		lesson
--
-- Tables:
--		dropdown.ClientOrganization
--		dropdown.ClientRegion
--		dropdown.ClientVacancyRoleStatus
--		dropdown.EmploymentType
--		dropdown.ImplementationDifficulty
--		dropdown.ImplementationImpact
--		dropdown.LessonCategory
--		dropdown.LessonOwner
--		dropdown.LessonPublicationStatus
--		dropdown.LessonStatus
--		dropdown.LessonType
--		dropdown.LessonUserClass
--		dropdown.MedicalClearanceReviewType
--		dropdown.ProjectLaborCode
--		dropdown.PublishTo
--		dropdown.RecruitmentPathway
--		dropdown.RegionalPriorityTier
--		dropdown.RoleCategory
--		dropdown.RoleSubCategory
--		dropdown.SecurityClearanceReviewType
--		dropdown.TaskType
--		dropdown.ThematicFocus
--		hrms.ApplicationContactData
--		hrms.ApplicationInterviewAvailability
--		hrms.ClientVacancy
--		hrms.ClientVacancyClientOrganization
--		hrms.ClientVacancyCompetency
--		hrms.ClientVacancyDeploymentManagerPerson
--		hrms.ClientVacancyInterviewDateTime
--		hrms.ClientVacancyRecruitmentManagerPerson
--		hrms.ClientVacancyReviewerPerson
--		hrms.ClientVacancyRoleCategory
--		hrms.ClientVacancyRoleSubCategory
--		lesson.Lesson
--		lesson.LessonAction
--		lesson.LessonLessonUserClass
--
-- Triggers:
--		document.TR_Document ON document.Document
--		person.TR_Person ON person.Person
--
-- Functions:
--		person.CanHaveAccess
--		person.GetPersonIDByDAPersonID
--		workflow.IsPersonInCurrentWorkflowStep
--
-- Procedures:
--		document.GetEntityDocumentData
--		dropdown.GetClientOrganizationData
--		dropdown.GetClientRegionData
--		dropdown.GetClientVacancyRoleStatusData
--		dropdown.GetEmploymentTypeData
--		dropdown.GetImplementationDifficultyData
--		dropdown.GetImplementationImpactData
--		dropdown.GetIncidentTypeCategoryData
--		dropdown.GetLessonCategoryData
--		dropdown.GetLessonOwnerData
--		dropdown.GetLessonPublicationStatusData
--		dropdown.GetLessonStatusData
--		dropdown.GetLessonTypeData
--		dropdown.GetLessonUserClassData
--		dropdown.GetMedicalClearanceReviewTypeData
--		dropdown.GetProjectLaborCodeData
--		dropdown.GetPublishToData
--		dropdown.GetRecruitmentPathwayData
--		dropdown.GetRegionalPriorityTierData
--		dropdown.GetRoleCategoryData
--		dropdown.GetRoleSubCategoryData
--		dropdown.GetSecurityClearanceReviewTypeData
--		dropdown.GetSecurityClearanceTypeData
--		dropdown.GetTaskTypeData
--		dropdown.GetThematicFocusData
--		eventlog.LogApplicationAction
--		eventlog.LogClientVacancyAction
--		eventlog.LogLessonAction
--		hrms.GetApplicationByApplicationID
--		hrms.GetClientVacancyByClientVacancyID
--		lesson.GetLessonActionByLessonActionID
--		lesson.GetLessonByLessonID
--		person.CheckAccess
--		person.CreateDAPersonByPersonID
--		person.GetFeedbackByFeedbackID
--		person.GetPersonByPersonID
--		person.GetVettingEmailData
--		person.ValidateLogin
--		procurement.GetEquipmentOrderByEquipmentOrderID
--		reporting.GetEquipmentWorkOrderDetailsList
--		reporting.GetLessonList
--		workflow.GetEntityWorkflowData
--		workflow.GetWorkflowByWorkflowID
--		workflow.InitializeEntityWorkflow
-- ==============================================================================================================================

--Begin file Build File - 00 - Prequisites.sql
USE HermisCloud
GO

--Begin schema lesson
EXEC utility.AddSchema 'lesson'
GO
--End schema lesson

--The 2 Client Vacancy Workflows should be created with these names prior to SQL
UPDATE workflow.Workflow
SET EntityTypeSubCode = 'Direct'
WHERE WorkflowName = 'Client Vacancy - Direct'
GO

UPDATE workflow.Workflow
SET EntityTypeSubCode = 'Open'
WHERE WorkflowName = 'Client Vacancy - Open'
GO

--End file Build File - 00 - Prequisites.sql

--Begin file Build File - 01 - Tables.sql
USE HermisCloud
GO

--Begin table document.Document
EXEC utility.DropObject 'document.TR_Document'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A trigger to update the document.Document table
-- ============================================================
CREATE TRIGGER document.TR_Document ON document.Document FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cContentType VARCHAR(250)
	DECLARE @cContentSubType VARCHAR(100)
	DECLARE @cDocumentGUID VARCHAR(50)
	DECLARE @cExtenstion VARCHAR(10)
	DECLARE @cFileExtenstion VARCHAR(10)
	DECLARE @cMimeType VARCHAR(100)
	DECLARE @nDocumentID INT
	DECLARE @nPhysicalFileSize BIGINT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.DocumentID, 
			CAST(ISNULL(I.DocumentGUID, newID()) AS VARCHAR(50)) AS DocumentGUID,
			NULLIF(I.ContentType, '') AS ContentType,
			NULLIF(I.ContentSubType, '') AS ContentSubType,
			REVERSE(LEFT(REVERSE(I.DocumentTitle), CHARINDEX('.', REVERSE(I.DocumentTitle)))) AS Extenstion,
			ISNULL(DATALENGTH(I.DocumentData), 0) AS PhysicalFileSize
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
	WHILE @@fetch_status = 0
		BEGIN

		SET @cFileExtenstion = NULL

		IF '.' + @cContentSubType = @cExtenstion OR EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.MimeType = @cContentType + '/' + @cContentSubType AND FT.Extension = @cExtenstion)
			BEGIN

			UPDATE D
			SET
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE IF @cContentType IS NULL AND @cContentSubType IS NULL AND EXISTS (SELECT 1 FROM document.FileType FT WHERE FT.Extension = @cExtenstion)
			BEGIN

			SELECT @cMimeType = FT.MimeType
			FROM document.FileType FT 
			WHERE FT.Extension = @cExtenstion

			UPDATE D
			SET
				D.ContentType = LEFT(@cMimeType, CHARINDEX('/', @cMimeType) - 1),
				D.ContentSubType = REVERSE(LEFT(REVERSE(@cMimeType), CHARINDEX('/', REVERSE(@cMimeType)) - 1)),
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = @cExtenstion,
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		ELSE
			BEGIN

			SELECT TOP 1 @cFileExtenstion = FT.Extension
			FROM document.FileType FT
			WHERE FT.MimeType = @cContentType + '/' + @cContentSubType

			UPDATE D
			SET 
				D.DocumentGUID = @cDocumentGUID,
				D.Extension = ISNULL(@cFileExtenstion, @cExtenstion),
				D.PhysicalFileSize = CASE WHEN D.PhysicalFileSize IS NULL OR D.PhysicalFileSize = 0 THEN @nPhysicalFileSize ELSE D.PhysicalFileSize END
			FROM document.Document D
			WHERE D.DocumentID = @nDocumentID

			END
		--ENDIF

		FETCH oCursor INTO @nDocumentID, @cDocumentGUID, @cContentType, @cContentSubType, @cExtenstion, @nPhysicalFileSize
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE document.Document ENABLE TRIGGER TR_Document
GO
--End table document.Document

--Begin table dropdown.ClientOrganization
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientOrganization'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientOrganization
	(
	ClientOrganizationID INT IDENTITY(0,1) NOT NULL,
	ClientOrganizationCode VARCHAR(50),
	ClientOrganizationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientOrganizationID'
GO
--End table dropdown.ClientOrganization

--Begin table dropdown.ClientRegion
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientRegion'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientRegion
	(
	ClientRegionID INT IDENTITY(0,1) NOT NULL,
	ClientRegionCode VARCHAR(50),
	ClientRegionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientRegionID'
GO
--End table dropdown.ClientRegion

--Begin table dropdown.ClientVacancyRoleStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientVacancyRoleStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientVacancyRoleStatus
	(
	ClientVacancyRoleStatusID INT IDENTITY(0,1) NOT NULL,
	ClientVacancyRoleStatusCode VARCHAR(50),
	ClientVacancyRoleStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientVacancyRoleStatusID'
GO
--End table dropdown.ClientVacancyRoleStatus

--Begin table dropdown.EmploymentType
DECLARE @TableName VARCHAR(250) = 'dropdown.EmploymentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EmploymentType
	(
	EmploymentTypeID INT IDENTITY(0,1) NOT NULL,
	EmploymentTypeCode VARCHAR(50),
	EmploymentTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EmploymentTypeID'
GO
--End table dropdown.EmploymentType

--Begin table dropdown.ImplementationDifficulty
DECLARE @TableName VARCHAR(250) = 'dropdown.ImplementationDifficulty'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.ImplementatonDifficulty'

CREATE TABLE dropdown.ImplementationDifficulty
	(
	ImplementationDifficultyID INT IDENTITY(0,1) NOT NULL,
	ImplementationDifficultyCode VARCHAR(50),
	ImplementationDifficultyName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementationDifficultyID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ImplementationDifficulty', 'DisplayOrder,ImplementationDifficultyName', 'ImplementationDifficultyID'
GO
--End table dropdown.ImplementationDifficulty

--Begin table dropdown.ImplementationImpact
DECLARE @TableName VARCHAR(250) = 'dropdown.ImplementationImpact'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.ImplementatonImpact'

CREATE TABLE dropdown.ImplementationImpact
	(
	ImplementationImpactID INT IDENTITY(0,1) NOT NULL,
	ImplementationImpactCode VARCHAR(50),
	ImplementationImpactName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementationImpactID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ImplementationImpact', 'DisplayOrder,ImplementationImpactName', 'ImplementationImpactID'
GO
--End table dropdown.ImplementationImpact

--Begin table dropdown.LessonCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonCategory
	(
	LessonCategoryID INT IDENTITY(0,1) NOT NULL,
	LessonCategoryCode VARCHAR(50),
	LessonCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonCategory', 'DisplayOrder,LessonCategoryName', 'LessonCategoryID'
GO
--End table dropdown.LessonCategory

--Begin table dropdown.LessonOwner
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonOwner'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonOwner
	(
	LessonOwnerID INT IDENTITY(0,1) NOT NULL,
	LessonOwnerCode VARCHAR(50),
	LessonOwnerName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonOwnerID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonOwner', 'DisplayOrder,LessonOwnerName', 'LessonOwnerID'
GO
--End table dropdown.LessonOwner

--Begin table dropdown.LessonPublicationStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonPublicationStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonPublicationStatus
	(
	LessonPublicationStatusID INT IDENTITY(0,1) NOT NULL,
	LessonPublicationStatusCode VARCHAR(50),
	LessonPublicationStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonPublicationStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonPublicationStatus', 'DisplayOrder,LessonPublicationStatusName', 'LessonPublicationStatusID'
GO
--End table dropdown.LessonPublicationStatus

--Begin table dropdown.LessonStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonStatus
	(
	LessonStatusID INT IDENTITY(0,1) NOT NULL,
	LessonStatusCode VARCHAR(50),
	LessonStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonStatus', 'DisplayOrder,LessonStatusName', 'LessonStatusID'
GO
--End table dropdown.LessonStatus

--Begin table dropdown.LessonType
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonType
	(
	LessonTypeID INT IDENTITY(0,1) NOT NULL,
	LessonTypeCode VARCHAR(50),
	LessonTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonType', 'DisplayOrder,LessonTypeName', 'LessonTypeID'
GO
--End table dropdown.LessonType

--Begin table dropdown.LessonUserClass
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonUserClass'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonUserClass
	(
	LessonUserClassID INT IDENTITY(0,1) NOT NULL,
	LessonUserClassCode VARCHAR(50),
	LessonUserClassName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonUserClassID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonUserClass', 'DisplayOrder,LessonUserClassName', 'LessonUserClassID'
GO
--End table dropdown.LessonUserClass

--Begin table dropdown.MedicalClearanceReviewType
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceReviewType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MedicalClearanceReviewType
	(
	MedicalClearanceReviewTypeID INT IDENTITY(0,1) NOT NULL,
	MedicalClearanceReviewTypeCode VARCHAR(50),
	MedicalClearanceReviewTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MedicalClearanceReviewTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MedicalClearanceReviewType', 'DisplayOrder,MedicalClearanceReviewTypeName', 'MedicalClearanceReviewTypeID'
GO
--End table dropdown.MedicalClearanceReviewType

--Begin table dropdown.ProjectLaborCode
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectLaborCode'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectLaborCode
	(
	ProjectLaborCodeID INT IDENTITY(0,1) NOT NULL,
	ProjectLaborCodeCode VARCHAR(50),
	ProjectLaborCodeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectLaborCodeID'
GO
--End table dropdown.ProjectLaborCode

--Begin table dropdown.PublishTo
DECLARE @TableName VARCHAR(250) = 'dropdown.PublishTo'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PublishTo
	(
	PublishToID INT IDENTITY(0,1) NOT NULL,
	PublishToCode VARCHAR(50),
	PublishToName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PublishToID'
GO
--End table dropdown.PublishTo

--Begin table dropdown.RecruitmentPathway
DECLARE @TableName VARCHAR(250) = 'dropdown.RecruitmentPathway'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RecruitmentPathway
	(
	RecruitmentPathwayID INT IDENTITY(0,1) NOT NULL,
	RecruitmentPathwayCode VARCHAR(50),
	RecruitmentPathwayName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecruitmentPathwayID'
GO
--End table dropdown.RecruitmentPathway

--Begin table dropdown.RegionalPriorityTier
DECLARE @TableName VARCHAR(250) = 'dropdown.RegionalPriorityTier'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RegionalPriorityTier
	(
	RegionalPriorityTierID INT IDENTITY(0,1) NOT NULL,
	RegionalPriorityTierCode VARCHAR(50),
	RegionalPriorityTierName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RegionalPriorityTierID'
GO
--End table dropdown.RegionalPriorityTier

--Begin table dropdown.RoleCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.RoleCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RoleCategory
	(
	RoleCategoryID INT IDENTITY(0,1) NOT NULL,
	RoleCategoryCode VARCHAR(50),
	RoleCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleCategoryID'
GO
--End table dropdown.RoleCategory

--Begin table dropdown.RoleSubCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.RoleSubCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RoleSubCategory
	(
	RoleSubCategoryID INT IDENTITY(0,1) NOT NULL,
	RoleSubCategoryCode VARCHAR(50),
	RoleSubCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleSubCategoryID'
GO
--End table dropdown.RoleSubCategory

--Begin table dropdown.SecurityClearanceReviewType
DECLARE @TableName VARCHAR(250) = 'dropdown.SecurityClearanceReviewType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SecurityClearanceReviewType
	(
	SecurityClearanceReviewTypeID INT IDENTITY(0,1) NOT NULL,
	SecurityClearanceReviewTypeCode VARCHAR(50),
	SecurityClearanceReviewTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SecurityClearanceReviewTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SecurityClearanceReviewType', 'DisplayOrder,SecurityClearanceReviewTypeName', 'SecurityClearanceReviewTypeID'
GO
--End table dropdown.SecurityClearanceReviewType

--Begin table dropdown.TaskType
DECLARE @TableName VARCHAR(250) = 'dropdown.TaskType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TaskType
	(
	TaskTypeID INT IDENTITY(0,1) NOT NULL,
	TaskTypeCode VARCHAR(50),
	TaskTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TaskTypeID'
GO
--End table dropdown.TaskType

--Begin table dropdown.ThematicFocus
DECLARE @TableName VARCHAR(250) = 'dropdown.ThematicFocus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ThematicFocus
	(
	ThematicFocusID INT IDENTITY(0,1) NOT NULL,
	ThematicFocusCode VARCHAR(50),
	ThematicFocusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ThematicFocusID'
GO
--End table dropdown.ThematicFocus

--Begin table eventlog.EventLog
EXEC utility.AddColumn 'eventlog.EventLog', 'EventName', 'VARCHAR(250)'
GO
--End table eventlog.EventLog

--Begin table hrms.Application
EXEC utility.AddColumn 'hrms.Application', 'Availability', 'VARCHAR(500)'
EXEC utility.AddColumn 'hrms.Application', 'ClientVacancyID', 'INT', '0'
EXEC utility.AddColumn 'hrms.Application', 'DAPersonID', 'INT', '0'
EXEC utility.AddColumn 'hrms.Application', 'IsInterviewConfirmed', 'BIT', '0'
EXEC utility.AddColumn 'hrms.Application', 'Notes', 'VARCHAR(MAX)'
GO
--End table hrms.Application

--Begin table hrms.ApplicationContactData
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationContactData'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationContactData
	(
	ApplicationContactDataID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	ContactTypeCode VARCHAR(50),
	ContactName VARCHAR(250),
	ContactJobTitle VARCHAR(250),
	ContactEmailAddress VARCHAR(320),
	ContactPhone VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationContactData', 'ApplicationID,ContactTypeCode'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationContactDataID'
GO
--End table hrms.ApplicationContactData

--Begin table hrms.ApplicationInterviewAvailability
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationInterviewAvailability'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationInterviewAvailability
	(
	ApplicationInterviewAvailabilityID INT IDENTITY(1,1) NOT NULL,
	ApplicationPersonID INT,
	ApplicationInterviewScheduleID INT,
	ClientVacancyInterviewDateTimeID INT,
	IsAvailable BIT,
	ApplicationID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationInterviewScheduleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyInterviewDateTimeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAvailable', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationInterviewAvailability', 'ApplicationPersonID,ApplicationInterviewScheduleID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationInterviewAvailabilityID'
GO
--End table hrms.ApplicationInterviewAvailability

--Begin table hrms.ClientVacancy
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancy'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancy
	(
	ClientVacancyID INT IDENTITY(1,1) NOT NULL,
	TaskName VARCHAR(250),
	TaskCode VARCHAR(250),
	TaskStartDate DATE,
	TaskEndDate DATE,
	CountryID INT,
	TaskOwnerPersonID INT,
	RegionalPriorityTierID INT,
	ClientLocation VARCHAR(250),
	ClientRegionID INT,
	ClientDepartment VARCHAR(250),
	ClientContact1Name VARCHAR(250),
	ClientContact1Team VARCHAR(250),
	ClientContact1Email VARCHAR(250),
	ClientContact1Phone VARCHAR(250),
	ClientContact2Name VARCHAR(250),
	ClientContact2Team VARCHAR(250),
	ClientContact2Email VARCHAR(250),
	ClientContact2Phone VARCHAR(250),
	ClientPersonID INT,
	RecruitmentPathwayID INT,
	TaskLocation VARCHAR(250),
	RoleName VARCHAR(250),
	RoleCode VARCHAR(250),
	ApplicationOpenDate DATETIME,
	ApplicationCloseDate DATETIME,
	ClientVacancyRoleStatusID INT,
	ProjectLaborCodeID INT,
	RoleTeam VARCHAR(250),
	PositionCount INT,
	ExpectedStartDate DATE,
	ExpectedEndDate DATE,
	WorkingDaysCount NUMERIC(10,2),
	HoursWorkedPerDay NUMERIC(10,2),
	Salary NUMERIC(10,2),
	EmploymentTypeID INT,
	ThematicFocusID INT,
	TaskTypeID INT,
	PublishToID INT,
	SecurityClearanceTypeID INT,
	OperationalTrainingCourseID INT,
	VacancyStatusID INT,
	IsFirearmsTrainingRequired BIT,
	RoleContext VARCHAR(MAX),
	KeyDeliverables VARCHAR(MAX),
	SiftDate DATE,
	InterviewShortlistDate DATE,
	AdvertisedInterviewDate DATE,
	RoleNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientRegionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyRoleStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EmploymentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFirearmsTrainingRequired', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'OperationalTrainingCourseID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PositionCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PositionCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLaborCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectLaborCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PublishToID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RecruitmentPathwayID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RegionalPriorityTierID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaskOwnerPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaskTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ThematicFocusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyStatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientVacancyID'
GO
--End table hrms.ClientVacancy

--Begin table hrms.ClientVacancyClientOrganization
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyClientOrganization'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyClientOrganization
	(
	ClientVacancyClientOrganizationID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	ClientOrganizationID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientOrganizationID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyClientOrganization', 'ClientVacancyID,ClientOrganizationID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyClientOrganizationID'
GO
--End table hrms.ClientVacancyClientOrganization

--Begin table hrms.ClientVacancyCompetency
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyCompetency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyCompetency
	(
	ClientVacancyCompetencyID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	CompetencyID INT,
	IsEssential BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CompetencyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsEssential', 'BIT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyCompetency', 'ClientVacancyID,CompetencyID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyCompetencyID'
GO
--End table hrms.ClientVacancyCompetency

--Begin table hrms.ClientVacancyDeploymentManagerPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyDeploymentManagerPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyDeploymentManagerPerson
	(
	ClientVacancyDeploymentManagerPersonID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyDeploymentManagerPerson', 'ClientVacancyID,PersonID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyDeploymentManagerPersonID'
GO
--End table hrms.ClientVacancyDeploymentManagerPerson

--Begin table hrms.ClientVacancyInterviewDateTime
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyInterviewDateTime'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'hrms.ClientVacancynterviewDateTime'

CREATE TABLE hrms.ClientVacancyInterviewDateTime
	(
	ClientVacancyInterviewDateTimeID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	InterviewDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyInterviewDateTime', 'ClientVacancyID,InterviewDateTime'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyInterviewDateTimeID'
GO
--End table hrms.ClientVacancyInterviewDateTime

--Begin table hrms.ClientVacancyRecruitmentManagerPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyRecruitmentManagerPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyRecruitmentManagerPerson
	(
	ClientVacancyRecruitmentManagerPersonID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyRecruitmentManagerPerson', 'ClientVacancyID,PersonID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyRecruitmentManagerPersonID'
GO
--End table hrms.ClientVacancyRecruitmentManagerPerson

--Begin table hrms.ClientVacancyReviewerPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyReviewerPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyReviewerPerson
	(
	ClientVacancyReviewerPersonID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyReviewerPerson', 'ClientVacancyID,PersonID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyReviewerPersonID'
GO
--End table hrms.ClientVacancyReviewerPerson

--Begin table hrms.ClientVacancyRoleCategory
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyRoleCategory'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyRoleCategory
	(
	ClientVacancyRoleCategoryID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	RoleCategoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleCategoryID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyRoleCategory', 'ClientVacancyID,RoleCategoryID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyRoleCategoryID'
GO
--End table hrms.ClientVacancyRoleSubCategory

--Begin table hrms.ClientVacancyRoleSubCategory
DECLARE @TableName VARCHAR(250) = 'hrms.ClientVacancyRoleSubCategory'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ClientVacancyRoleSubCategory
	(
	ClientVacancyRoleSubCategoryID INT IDENTITY(1,1) NOT NULL,
	ClientVacancyID INT,
	RoleSubCategoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientVacancyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleSubCategoryID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_ClientVacancyRoleSubCategory', 'ClientVacancyID,RoleSubCategoryID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientVacancyRoleSubCategoryID'
GO
--End table hrms.ClientVacancyRoleSubCategory

--Begin table lesson.Lesson
DECLARE @TableName VARCHAR(250) = 'lesson.Lesson'

EXEC utility.DropObject @TableName

CREATE TABLE lesson.Lesson
	(
	LessonID INT IDENTITY(1,1) NOT NULL,
	LessonName VARCHAR(250),
	ProjectID INT,
	ProjectSponsorID INT,
	LessonOwnerID INT,
	LessonStatusID INT,
	LessonPublicationStatusID INT,
	ISOCountryCode2 CHAR(2),
	ISOCurrencyCode CHAR(3),
	ScaleOfResponse NUMERIC(18,2),
	ClientCode VARCHAR(250),
	LessonCategoryID INT,
	LessonTypeID INT,
	LessonDescription VARCHAR(MAX),
	Event VARCHAR(MAX),
	Cause VARCHAR(MAX),
	Impact VARCHAR(MAX),
	Indicators VARCHAR(MAX),
	Recommendations VARCHAR(MAX),
	ImplementationImpactID INT,
	ImplementationDifficultyID INT,
	IncidentTypeID INT, --this is Event cat / sub cat
	LessonDate DATE,
	CreatePersonID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImplementationDifficultyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImplementationImpactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'LessonOwnerID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonPublicationStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectSponsorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ScaleOfResponse', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonID'
GO
--End table lesson.Lesson

--Begin table lesson.LessonAction
DECLARE @TableName VARCHAR(250) = 'lesson.LessonAction'

EXEC utility.DropObject @TableName

CREATE TABLE lesson.LessonAction
	(
	LessonActionID INT IDENTITY(1,1) NOT NULL,
	LessonID INT,
	LessonActionName VARCHAR(250),
	LessonActionDescription VARCHAR(MAX),
	LessonActionPersonID INT,
	DueDate DATE,
	IsComplete BIT,
	CreatePersonID INT,
	CreateDateTime DATETIME,
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonActionPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_LessonAction', 'LessonID,LessonActionID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LessonActionID'
GO
--End table lesson.LessonAction

--Begin table lesson.LessonLessonUserClass
DECLARE @TableName VARCHAR(250) = 'lesson.LessonLessonUserClass'

EXEC utility.DropObject @TableName

CREATE TABLE lesson.LessonLessonUserClass
	(
	LessonLessonUserClassID INT IDENTITY(1,1) NOT NULL,
	LessonID INT,
	LessonUserClassID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'LessonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LessonUserClassID', 'INT', '0'

EXEC utility.SetIndexClustered @TableName, 'IX_LessonLessonUserClass', 'LessonID,LessonUserClassID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LessonLessonUserClassID'
GO
--End table lesson.LessonLessonUserClass

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.AddColumn @TableName, 'DepartmentName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'JobTitle', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'OrganizationName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'TeamName', 'VARCHAR(250)'
GO

EXEC utility.DropObject 'person.TR_Person'

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.21
-- Description:	A trigger to create a DeployAdviser standard username
-- ==================================================================
CREATE TRIGGER person.TR_Person ON person.Person FOR INSERT
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cLastName VARCHAR(100)
	DECLARE @cUserName VARCHAR(6)
	DECLARE @nIndex INT
	DECLARE @nPersonID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT
			I.PersonID,
			utility.StripCharacters(I.LastName, '^a-z') + 'XXX' AS LastName
		FROM INSERTED I
		WHERE I.UserName IS NULL
			OR EXISTS
				(
				SELECT 1
				FROM person.Person P
				WHERE P.UserName = I.UserName
					AND P.PersonID <> I.PersonID
				)

	OPEN oCursor
	FETCH oCursor INTO @nPersonID, @cLastName
	WHILE @@fetch_status = 0
		BEGIN

		SET @cUserName = UPPER(LEFT(@cLastName, 3))
		SET @nIndex = (SELECT TOP 1 CAST(RIGHT(P.UserName, 3) AS INT) FROM person.Person P WHERE LEFT(P.UserName, 3) = LEFT(@cLastName, 3) ORDER BY 1 DESC)
		SET @nIndex = ISNULL(@nIndex, 0) + 1

		SET @cUserName += RIGHT('000' + CAST(@nIndex AS VARCHAR(10)), 3) + 'HCP'

		UPDATE P
		SET P.UserName = @cUserName
		FROM person.Person P
		WHERE P.PersonID = @nPersonID

		FETCH oCursor INTO @nPersonID, @cLastName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End table person.Person

--Begin table person.Feedback
DECLARE @TableName VARCHAR(250) = 'person.Feedback'

EXEC utility.AddColumn @TableName, 'FeedbackDate', 'DATE'
EXEC utility.AddColumn @TableName, 'FeedbackStatusCode', 'VARCHAR(50)'
GO
--End table person.Feedback

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC utility.AddColumn @TableName, 'IsSUperAdminOnly', 'BIT', '0'
GO
--End table workflow.Workflow

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE HermisCloud
GO

--Begin function dropdown.GetCountryNameFromISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameFromISOCountryCode'
GO
--End function dropdown.GetCountryNameFromISOCountryCode

--Begin function person.CanHaveAccess
EXEC utility.DropObject 'person.CanHaveAccess'
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A function to determine if a PersonID has access to a specific entity type / entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
--
-- Author:			Jonathan Burnham
-- Create date:	2019.02.02
-- Description:	refactored to support the hrms workflow
-- ===================================================================================================

CREATE FUNCTION person.CanHaveAccess
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bCanHaveAccess BIT = 0

	IF @EntityTypeCode IN ('ActivityReport','Application','ClientVacancy','EquipmentOrder','Feedback','PersonUpdate','SpotReport','TrendReport','Vacancy','VacancyApplication')
		BEGIN

		IF @EntityTypeCode = 'VacancyApplication' AND (SELECT A.SubmittedDateTime FROM hrms.Application A WHERE A.ApplicationID = @EntityID AND A.PersonID = @PersonID) IS NULL
			SET @bCanHaveAccess = 1
		ELSE
			BEGIN

			SET @bCanHaveAccess = workflow.IsWorkflowComplete(@EntityTypeCode, @EntityID)

			IF @bCanHaveAccess = 0
				SET @bCanHaveAccess = workflow.IsPersonInCurrentWorkflowStep(@PersonID, @EntityTypeCode, @EntityID, 0)
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF

	IF @EntityTypeCode = 'Application' AND @bCanHaveAccess = 0
		BEGIN

		SELECT @bCanHaveAccess = CASE WHEN A.SubmittedDateTime IS NULL THEN 1 ELSE 0 END
		FROM hrms.Application A
		WHERE A.ApplicationID = @EntityID
			AND A.PersonID = @PersonID

		END
	--ENDIF

	RETURN ISNULL(@bCanHaveAccess, 0)

END
GO
--End function person.CanHaveAccess

--Begin function person.GetPersonIDByDAPersonID
EXEC utility.DropObject 'person.GetPersonIDByDAPersonID'
GO
-- =================================================================
-- Author:			Todd Pires
-- Create date:	2019.04.08
-- Description:	A function to get a Person ID based on a DA PersonID
-- =================================================================

CREATE FUNCTION person.GetPersonIDByDAPersonID
(
@DAPersonID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nPersonID INT = 0

	SELECT @nPersonID = P.PersonID
	FROM person.Person P
	WHERE P.DAPersonID = @DAPersonID

	RETURN ISNULL(@nPersonID, 0)

END
GO
--End function person.GetPersonIDByDAPersonID

--Begin function workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to indicate if a personid exists in the current step of an entity's workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =====================================================================================================

CREATE FUNCTION workflow.IsPersonInCurrentWorkflowStep
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflowStep BIT = 0

	IF (@EntityID = 0 AND EXISTS 
		(
		SELECT 1
		FROM workflow.WorkflowStepGroupPerson WSGP
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND WSGP.PersonID = @PersonID
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode 
					OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback')
					OR (@EntityTypeCode = 'ClientVacancy' AND W.EntityTypeCode LIKE 'ClientVacancy%')
				)
				AND W.IsActive = 1
				AND (
					@ProjectID = 0 
						OR (
							W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
							AND w.ProjectID = @ProjectID
						)
				)

		UNION

		SELECT 1
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = WSGPG.PersonGroupID
					)
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode
					OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback')
					OR (@EntityTypeCode = 'ClientVacancy' AND W.EntityTypeCode LIKE 'ClientVacancy%')
				)
				AND W.IsActive = 1
		)) OR EXISTS
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE (EWSGP.EntityTypeCode = @EntityTypeCode
				OR (@EntityTypeCode = 'Feedback' AND EWSGP.EntityTypeCode LIKE '%Feedback')
				OR (@EntityTypeCode = 'ClientVacancy' AND EWSGP.EntityTypeCode LIKE 'ClientVacancy%')
			)
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EWSGP.PersonID = @PersonID

			UNION

			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
			WHERE (EWSGPG.EntityTypeCode = @EntityTypeCode
				OR (@EntityTypeCode = 'Feedback' AND EWSGPG.EntityTypeCode LIKE '%Feedback')
				OR (@EntityTypeCode = 'ClientVacancy' AND EWSGPG.EntityTypeCode LIKE 'ClientVacancy%')
			)
				AND EWSGPG.EntityID = @EntityID
				AND EWSGPG.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = EWSGPG.PersonGroupID
					)
			)

		SET @bIsPersonInCurrentWorkflowStep = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflowStep

END
GO
--End function workflow.IsPersonInCurrentWorkflowStep
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE HermisCloud
GO

--Begin procedure document.GetEntityDocumentData
EXEC Utility.DropObject 'document.GetEntityDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return document data for an entity
-- =====================================================================
CREATE PROCEDURE document.GetEntityDocumentData

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		DE.EntityTypeSubCode,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID

END
GO
--End procedure document.GetEntityDocumentData

--Begin procedure dropdown.GetClientOrganizationData
EXEC Utility.DropObject 'dropdown.GetClientOrganizationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ClientOrganization table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetClientOrganizationData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientOrganizationID, 
		T.ClientOrganizationCode,
		T.ClientOrganizationName
	FROM dropdown.ClientOrganization T
	WHERE (T.ClientOrganizationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientOrganizationName, T.ClientOrganizationID

END
GO
--End procedure dropdown.GetClientOrganizationData

--Begin procedure dropdown.GetClientRegionData
EXEC Utility.DropObject 'dropdown.GetClientRegionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ClientRegion table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetClientRegionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientRegionID, 
		T.ClientRegionCode,
		T.ClientRegionName
	FROM dropdown.ClientRegion T
	WHERE (T.ClientRegionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientRegionName, T.ClientRegionID

END
GO
--End procedure dropdown.GetClientRegionData

--Begin procedure dropdown.GetClientVacancyRoleStatusData
EXEC Utility.DropObject 'dropdown.GetClientVacancyRoleStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ClientVacancyRoleStatus table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetClientVacancyRoleStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientVacancyRoleStatusID, 
		T.ClientVacancyRoleStatusCode,
		T.ClientVacancyRoleStatusName
	FROM dropdown.ClientVacancyRoleStatus T
	WHERE (T.ClientVacancyRoleStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientVacancyRoleStatusName, T.ClientVacancyRoleStatusID

END
GO
--End procedure dropdown.GetClientVacancyRoleStatusData

--Begin procedure dropdown.GetEmploymentTypeData
EXEC Utility.DropObject 'dropdown.GetEmploymentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.EmploymentType table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEmploymentTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EmploymentTypeID, 
		T.EmploymentTypeCode,
		T.EmploymentTypeName
	FROM dropdown.EmploymentType T
	WHERE (T.EmploymentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EmploymentTypeName, T.EmploymentTypeID

END
GO
--End procedure dropdown.GetEmploymentTypeData

--Begin procedure dropdown.GetImplementationDifficultyData
EXEC Utility.DropObject 'dropdown.GetImplementatonDifficultyData'
EXEC Utility.DropObject 'dropdown.GetImplementationDifficultyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.ImplementationDifficulty table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetImplementationDifficultyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImplementationDifficultyID, 
		T.ImplementationDifficultyCode,
		T.ImplementationDifficultyName
	FROM dropdown.ImplementationDifficulty T
	WHERE (T.ImplementationDifficultyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImplementationDifficultyName, T.ImplementationDifficultyID

END
GO
--End procedure dropdown.GetImplementationDifficultyData

--Begin procedure dropdown.GetImplementationImpactData
EXEC Utility.DropObject 'dropdown.GetImplementatonImpactData'
EXEC Utility.DropObject 'dropdown.GetImplementationImpactData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.ImplementationImpact table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetImplementationImpactData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImplementationImpactID, 
		T.ImplementationImpactCode,
		T.ImplementationImpactName
	FROM dropdown.ImplementationImpact T
	WHERE (T.ImplementationImpactID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImplementationImpactName, T.ImplementationImpactID

END
GO
--End procedure dropdown.GetImplementationImpactData

--Begin procedure dropdown.GetIncidentTypeCategoryData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.30
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MIN(T.IncidentTypeID) AS IncidentTypeCategoryID, 
		T.IncidentTypeCategory AS IncidentTypeCategoryCode,
		T.IncidentTypeCategory AS IncidentTypeCategoryName
	FROM dropdown.IncidentType T
	WHERE (T.IncidentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	GROUP BY T.IncidentTypeCategory
	ORDER BY T.IncidentTypeCategory, MIN(T.IncidentTypeID)

END
GO
--End procedure dropdown.GetIncidentTypeCategoryData

--Begin procedure dropdown.GetLessonCategoryData
EXEC Utility.DropObject 'dropdown.GetLessonCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetLessonCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonCategoryID, 
		T.LessonCategoryCode,
		T.LessonCategoryName
	FROM dropdown.LessonCategory T
	WHERE (T.LessonCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonCategoryName, T.LessonCategoryID

END
GO
--End procedure dropdown.GetLessonCategoryData

--Begin procedure dropdown.GetLessonPublicationStatusData
EXEC Utility.DropObject 'dropdown.GetLessonPublicationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.LessonPublicationStatus table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetLessonPublicationStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonPublicationStatusID, 
		T.LessonPublicationStatusCode,
		T.LessonPublicationStatusName
	FROM dropdown.LessonPublicationStatus T
	WHERE (T.LessonPublicationStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonPublicationStatusName, T.LessonPublicationStatusID

END
GO
--End procedure dropdown.GetLessonPublicationStatusData

--Begin procedure dropdown.GetLessonOwnerData
EXEC Utility.DropObject 'dropdown.GetLessonOwnerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.LessonOwner table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetLessonOwnerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonOwnerID, 
		T.LessonOwnerCode,
		T.LessonOwnerName
	FROM dropdown.LessonOwner T
	WHERE (T.LessonOwnerID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonOwnerName, T.LessonOwnerID

END
GO
--End procedure dropdown.GetLessonOwnerData

--Begin procedure dropdown.GetLessonStatusData
EXEC Utility.DropObject 'dropdown.GetLessonStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonStatus table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetLessonStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonStatusID, 
		T.LessonStatusCode,
		T.LessonStatusName
	FROM dropdown.LessonStatus T
	WHERE (T.LessonStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonStatusName, T.LessonStatusID

END
GO
--End procedure dropdown.GetLessonStatusData

--Begin procedure dropdown.GetLessonTypeData
EXEC Utility.DropObject 'dropdown.GetLessonTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetLessonTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonTypeID, 
		T.LessonTypeCode,
		T.LessonTypeName
	FROM dropdown.LessonType T
	WHERE (T.LessonTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonTypeName, T.LessonTypeID

END
GO
--End procedure dropdown.GetLessonTypeData

--Begin procedure dropdown.GetLessonUserClassData
EXEC Utility.DropObject 'dropdown.GetLessonUserClassData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.LessonUserClass table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetLessonUserClassData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonUserClassID, 
		T.LessonUserClassCode,
		T.LessonUserClassName
	FROM dropdown.LessonUserClass T
	WHERE (T.LessonUserClassID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonUserClassName, T.LessonUserClassID

END
GO
--End procedure dropdown.GetLessonUserClassData

--Begin procedure dropdown.GetMedicalClearanceReviewTypeData
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceReviewTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.17
-- Description:	A stored procedure to return data from the dropdown.MedicalClearanceReviewType table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetMedicalClearanceReviewTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MedicalClearanceReviewTypeID, 
		T.MedicalClearanceReviewTypeCode,
		T.MedicalClearanceReviewTypeName
	FROM dropdown.MedicalClearanceReviewType T
	WHERE (T.MedicalClearanceReviewTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MedicalClearanceReviewTypeName, T.MedicalClearanceReviewTypeID

END
GO
--End procedure dropdown.GetMedicalClearanceReviewTypeData

--Begin procedure dropdown.GetProjectLaborCodeData
EXEC Utility.DropObject 'dropdown.GetProjectLaborCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ProjectLaborCode table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetProjectLaborCodeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectLaborCodeID, 
		T.ProjectLaborCodeCode,
		T.ProjectLaborCodeName
	FROM dropdown.ProjectLaborCode T
	WHERE (T.ProjectLaborCodeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectLaborCodeName, T.ProjectLaborCodeID

END
GO
--End procedure dropdown.GetProjectLaborCodeData

--Begin procedure dropdown.GetPublishToData
EXEC Utility.DropObject 'dropdown.GetPublishToData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.PublishTo table
-- ================================================================================
CREATE PROCEDURE dropdown.GetPublishToData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PublishToID, 
		T.PublishToCode,
		T.PublishToName
	FROM dropdown.PublishTo T
	WHERE (T.PublishToID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PublishToName, T.PublishToID

END
GO
--End procedure dropdown.GetPublishToData

--Begin procedure dropdown.GetRecruitmentPathwayData
EXEC Utility.DropObject 'dropdown.GetRecruitmentPathwayData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RecruitmentPathway table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetRecruitmentPathwayData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RecruitmentPathwayID, 
		T.RecruitmentPathwayCode,
		T.RecruitmentPathwayName
	FROM dropdown.RecruitmentPathway T
	WHERE (T.RecruitmentPathwayID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RecruitmentPathwayName, T.RecruitmentPathwayID

END
GO
--End procedure dropdown.GetRecruitmentPathwayData

--Begin procedure dropdown.GetRegionalPriorityTierData
EXEC Utility.DropObject 'dropdown.GetRegionalPriorityTierData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RegionalPriorityTier table
-- ==============================================================================================
CREATE PROCEDURE dropdown.GetRegionalPriorityTierData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RegionalPriorityTierID, 
		T.RegionalPriorityTierCode,
		T.RegionalPriorityTierName
	FROM dropdown.RegionalPriorityTier T
	WHERE (T.RegionalPriorityTierID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RegionalPriorityTierName, T.RegionalPriorityTierID

END
GO
--End procedure dropdown.GetRegionalPriorityTierData

--Begin procedure dropdown.GetRoleCategoryData
EXEC Utility.DropObject 'dropdown.GetRoleCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RoleCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetRoleCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleCategoryID, 
		T.RoleCategoryCode,
		T.RoleCategoryName
	FROM dropdown.RoleCategory T
	WHERE (T.RoleCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleCategoryName, T.RoleCategoryID

END
GO
--End procedure dropdown.GetRoleCategoryData

--Begin procedure dropdown.GetRoleSubCategoryData
EXEC Utility.DropObject 'dropdown.GetRoleSubCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.RoleSubCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetRoleSubCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleSubCategoryID, 
		T.RoleSubCategoryCode,
		T.RoleSubCategoryName
	FROM dropdown.RoleSubCategory T
	WHERE (T.RoleSubCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleSubCategoryName, T.RoleSubCategoryID

END
GO
--End procedure dropdown.GetRoleSubCategoryData

--Begin procedure dropdown.GetSecurityClearanceReviewTypeData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceReviewTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.12
-- Description:	A stored procedure to return data from the dropdown.SecurityClearanceReviewType table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceReviewTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SecurityClearanceReviewTypeID, 
		T.SecurityClearanceReviewTypeCode,
		T.SecurityClearanceReviewTypeName
	FROM dropdown.SecurityClearanceReviewType T
	WHERE (T.SecurityClearanceReviewTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SecurityClearanceReviewTypeName, T.SecurityClearanceReviewTypeID

END
GO
--End procedure dropdown.GetSecurityClearanceReviewTypeData

--Begin procedure dropdown.GetSecurityClearanceTypeData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.SecurityClearance table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MIN(T.SecurityClearanceID) AS SecurityClearanceTypeID, 
		T.SecurityClearanceCode AS SecurityClearanceTypeCode,
		T.SecurityClearanceCode AS SecurityClearanceTypeName
	FROM dropdown.SecurityClearance T
	WHERE (T.SecurityClearanceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	GROUP BY T.SecurityClearanceCode
	ORDER BY T.SecurityClearanceCode

END
GO
--End procedure dropdown.GetSecurityClearanceTypeData

--Begin procedure dropdown.GetTaskTypeData
EXEC Utility.DropObject 'dropdown.GetTaskTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.TaskType table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetTaskTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TaskTypeID, 
		T.TaskTypeCode,
		T.TaskTypeName
	FROM dropdown.TaskType T
	WHERE (T.TaskTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TaskTypeName, T.TaskTypeID

END
GO
--End procedure dropdown.GetTaskTypeData

--Begin procedure dropdown.GetThematicFocusData
EXEC Utility.DropObject 'dropdown.GetThematicFocusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.24
-- Description:	A stored procedure to return data from the dropdown.ThematicFocus table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetThematicFocusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ThematicFocusID, 
		T.ThematicFocusCode,
		T.ThematicFocusName
	FROM dropdown.ThematicFocus T
	WHERE (T.ThematicFocusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ThematicFocusName, T.ThematicFocusID

END
GO
--End procedure dropdown.GetThematicFocusData

--Begin procedure eventlog.LogApplicationAction
EXEC utility.DropObject 'eventlog.LogApplicationAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.03.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogApplicationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Application',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogApplicationTable', 'u')) IS NOT NULL
			DROP TABLE #LogApplicationTable
		--ENDIF
		
		SELECT *
		INTO #LogApplicationTable
		FROM hrms.Application V
		WHERE V.ApplicationID = @EntityID
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Application',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Application'), ELEMENTS
			)
		FROM #LogApplicationTable T
			JOIN hrms.Application V ON V.ApplicationID = T.ApplicationID

		DROP TABLE #LogApplicationTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogApplicationAction

--Begin procedure eventlog.LogClientVacancyAction
EXEC utility.DropObject 'eventlog.LogClientVacancyAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientVacancyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ClientVacancy',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogClientVacancyTable', 'u')) IS NOT NULL
			DROP TABLE #LogClientVacancyTable
		--ENDIF
		
		SELECT *
		INTO #LogClientVacancyTable
		FROM hrms.ClientVacancy A
		WHERE A.ClientVacancyID = @EntityID

		DECLARE @cClientVacancyClientOrganization VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyClientOrganization = COALESCE(@cClientVacancyClientOrganization, '') + D.ClientVacancyClientOrganization 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyClientOrganization'), ELEMENTS) AS ClientVacancyClientOrganization
			FROM hrms.ClientVacancyClientOrganization T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyDeploymentManagerPerson VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyDeploymentManagerPerson = COALESCE(@cClientVacancyDeploymentManagerPerson, '') + D.ClientVacancyDeploymentManagerPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyDeploymentManagerPerson'), ELEMENTS) AS ClientVacancyDeploymentManagerPerson
			FROM hrms.ClientVacancyDeploymentManagerPerson T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyInterviewDateTime VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyInterviewDateTime = COALESCE(@cClientVacancyInterviewDateTime, '') + D.ClientVacancyInterviewDateTime 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyInterviewDateTime'), ELEMENTS) AS ClientVacancyInterviewDateTime
			FROM hrms.ClientVacancyInterviewDateTime T
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyRecruitmentManagerPerson VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyRecruitmentManagerPerson = COALESCE(@cClientVacancyRecruitmentManagerPerson, '') + D.ClientVacancyRecruitmentManagerPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyRecruitmentManagerPerson'), ELEMENTS) AS ClientVacancyRecruitmentManagerPerson
			FROM hrms.ClientVacancyRecruitmentManagerPerson T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyReviewerPerson VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyReviewerPerson = COALESCE(@cClientVacancyReviewerPerson, '') + D.ClientVacancyReviewerPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyReviewerPerson'), ELEMENTS) AS ClientVacancyReviewerPerson
			FROM hrms.ClientVacancyReviewerPerson T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyRoleCategory VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyRoleCategory = COALESCE(@cClientVacancyRoleCategory, '') + D.ClientVacancyRoleCategory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyRoleCategory'), ELEMENTS) AS ClientVacancyRoleCategory
			FROM hrms.ClientVacancyRoleCategory T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancyRoleSubCategory VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancyRoleSubCategory = COALESCE(@cClientVacancyRoleSubCategory, '') + D.ClientVacancyRoleSubCategory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancyRoleSubCategory'), ELEMENTS) AS ClientVacancyRoleSubCategory
			FROM hrms.ClientVacancyRoleSubCategory T 
			WHERE T.ClientVacancyID = @EntityID
			) D

		DECLARE @cClientVacancySkillsDesirable VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancySkillsDesirable = COALESCE(@cClientVacancySkillsDesirable, '') + D.ClientVacancySkillsDesirable 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancySkillsDesirable'), ELEMENTS) AS ClientVacancySkillsDesirable
			FROM hrms.ClientVacancyCompetency T 
			WHERE T.ClientVacancyID = @EntityID
			AND IsEssential <> 1
			) D

		DECLARE @cClientVacancySkillsEssential VARCHAR(MAX) 
	
		SELECT 
			@cClientVacancySkillsEssential = COALESCE(@cClientVacancySkillsEssential, '') + D.ClientVacancySkillsEssential 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClientVacancySkillsEssential'), ELEMENTS) AS ClientVacancySkillsEssential
			FROM hrms.ClientVacancyCompetency T 
			WHERE T.ClientVacancyID = @EntityID
			AND IsEssential = 1
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ClientVacancy',
			@EntityID,
			@Comments,
			(
			SELECT T.*,

			CAST(('<ClientVacancyClientOrganization>' + ISNULL(@cClientVacancyClientOrganization,'') + '</ClientVacancyClientOrganization>') AS XML),
			CAST(('<ClientVacancyDeploymentManagerPerson>' + ISNULL(@cClientVacancyDeploymentManagerPerson,'') + '</ClientVacancyDeploymentManagerPerson>') AS XML),
			CAST(('<ClientVacancyInterviewDateTime>' + ISNULL(@cClientVacancyInterviewDateTime,'') + '</ClientVacancyInterviewDateTime>') AS XML),
			CAST(('<ClientVacancyRecruitmentManagerPerson>' + ISNULL(@cClientVacancyRecruitmentManagerPerson,'') + '</ClientVacancyRecruitmentManagerPerson>') AS XML),
			CAST(('<ClientVacancyReviewerPerson>' + ISNULL(@cClientVacancyReviewerPerson,'') + '</ClientVacancyReviewerPerson>') AS XML),
			CAST(('<ClientVacancyRoleCategory>' + ISNULL(@cClientVacancyRoleCategory,'') + '</ClientVacancyRoleCategory>') AS XML),
			CAST(('<ClientVacancyRoleSubCategory>' + ISNULL(@cClientVacancyRoleSubCategory,'') + '</ClientVacancyRoleSubCategory>') AS XML),
			CAST(('<ClientVacancySkillsDesirable>' + ISNULL(@cClientVacancySkillsDesirable,'') + '</ClientVacancySkillsDesirable>') AS XML),
			CAST(('<ClientVacancySkillsEssential>' + ISNULL(@cClientVacancySkillsEssential,'') + '</ClientVacancySkillsEssential>') AS XML)
			FOR XML RAW('ClientVacancy'), ELEMENTS
			)
		FROM #LogClientVacancyTable T
			JOIN hrms.ClientVacancy V ON V.ClientVacancyID = T.ClientVacancyID

		DROP TABLE #LogClientVacancyTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientVacancyAction

--Begin procedure eventlog.LogLessonAction
EXEC utility.DropObject 'eventlog.LogLessonAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.03.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLessonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EventName VARCHAR(250) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventName)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Lesson',
			@EntityID,
			@Comments,
			@EventName
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogLessonTable', 'u')) IS NOT NULL
			DROP TABLE #LogLessonTable
		--ENDIF
		
		SELECT *
		INTO #LogLessonTable
		FROM lesson.Lesson V
		WHERE V.LessonID = @EntityID
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventName, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Lesson',
			@EntityID,
			@Comments,
			@EventName,
			(
			SELECT T.*
			FOR XML RAW('Lesson'), ELEMENTS
			)
		FROM #LogLessonTable T
			JOIN lesson.Lesson V ON V.LessonID = T.LessonID

		DROP TABLE #LogLessonTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLessonAction

--Begin procedure hrms.GetApplicationByApplicationID
EXEC utility.DropObject 'hrms.GetApplicationByApplicationID'
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.23
-- Description:	A stored procedure to return data from the hrms.Application table
-- ==============================================================================
CREATE PROCEDURE hrms.GetApplicationByApplicationID

@ApplicationID INT,
@PersonID INT = 0,
@VacancyID INT = 0,
@ClientVacancyID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @ApplicationID = 0
		BEGIN

		DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Application', @ApplicationID)

		--Application
		SELECT
			0 AS ApplicationID,
			NULL AS Availability,
			@ClientVacancyID AS ClientVacancyID,
			NULL AS ConflictsOfInterest,
			0 AS HasCertifiedApplication,
			0 AS HasConfirmedTerminationDate,
			CASE WHEN P.DAPersonID > 0 THEN 1 ELSE 0 END AS HasDAAccount,
			0 AS IsGovernmetEmployee,
			0 AS IsGuaranteedInterview,
			0 AS IsApplicationFinal,
			0 AS IsNotCriminalCivilService,
			0 AS IsNotCriminalPolice,
			0 AS IsDisabled,
			0 AS IsDueDilligenceComplete,
			0 AS IsEligible,
			0 AS IsHermisProfileUpdated,
			0 AS IsHRContactNotified,
			0 AS IsInterviewAvailableAMDay1,
			0 AS IsInterviewAvailableAMDay2,
			0 AS IsInterviewAvailableAMDay3,
			0 AS IsInterviewAvailableAMDay4,
			0 AS IsInterviewAvailableAMDay5,
			0 AS IsInterviewAvailableAMDay6,
			0 AS IsInterviewAvailableAMDay7,
			0 AS IsInterviewAvailablePMDay1,
			0 AS IsInterviewAvailablePMDay2,
			0 AS IsInterviewAvailablePMDay3,
			0 AS IsInterviewAvailablePMDay4,
			0 AS IsInterviewAvailablePMDay5,
			0 AS IsInterviewAvailablePMDay6,
			0 AS IsInterviewAvailablePMDay7,
			0 AS IsInterviewComplete,
			0 AS IsInterviewConfirmed,
			0 AS IsLineManagerNotified,
			0 AS IsOfferAccepted,
			0 AS IsOfferExtended,
			0 AS IsOnRoster,
			0 AS IsPreQualified,
			0 AS IsVetted,
			NULL AS ManagerComments,
			NULL AS Notes,
			@PersonID AS PersonID,
			person.FormatPersonNameByPersonID(@PersonID,'TitleFirstLast') AS PersonNameFormatted,
			NULL AS SubmittedDateTime,
			NULL AS SubmittedDateTimeFormatted,
			NULL AS Suitability,
			NULL AS TechnicalManagerComments,
			NULL AS TerminationDate,
			NULL AS TerminationDateFormatted,
			@VacancyID AS VacancyID,
			0 AS AgeRangeID,
			NULL AS AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			0 AS ExpertCategoryID,
			NULL AS ExpertCategoryName,
			0 AS GenderID,
			NULL AS GenderName,
			0 AS InterviewOutcomeID,
			NULL AS InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			PT.PersonTypeName,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName,
			'Not Submitted' AS WorkflowStepStatusName,
			'' AS IntervieweeEmailedDateTime,
			'' AS IntervieweeEmailedDateTimeFormatted
		FROM person.Person P
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND P.PersonID = @PersonID

		END
	ELSE
		BEGIN

		SELECT 
			@ClientVacancyID = A.ClientVacancyID,
			@PersonID = A.PersonID,
			@VacancyID = A.VacancyID
		FROM hrms.Application A 
		WHERE A.ApplicationID = @ApplicationID

		--Application
		SELECT
			A.ApplicationID,
			A.Availability,
			A.ClientVacancyID,
			A.ConflictsOfInterest,
			A.HasCertifiedApplication,
			A.HasConfirmedTerminationDate,
			A.HasDAAccount,
			A.IsGuaranteedInterview,
			A.IsApplicationFinal,
			A.IsNotCriminalCivilService,
			A.IsNotCriminalPolice,
			A.IsDisabled,
			A.IsDueDilligenceComplete,
			A.IsEligible,
			A.IsGovernmetEmployee,
			A.IsHermisProfileUpdated,
			A.IsHRContactNotified,
			A.IsInterviewAvailableAMDay1,
			A.IsInterviewAvailableAMDay2,
			A.IsInterviewAvailableAMDay3,
			A.IsInterviewAvailableAMDay4,
			A.IsInterviewAvailableAMDay5,
			A.IsInterviewAvailableAMDay6,
			A.IsInterviewAvailableAMDay7,
			A.IsInterviewAvailablePMDay1,
			A.IsInterviewAvailablePMDay2,
			A.IsInterviewAvailablePMDay3,
			A.IsInterviewAvailablePMDay4,
			A.IsInterviewAvailablePMDay5,
			A.IsInterviewAvailablePMDay6,
			A.IsInterviewAvailablePMDay7,
			A.IsInterviewComplete,
			A.IsInterviewConfirmed,
			A.IsLineManagerNotified,
			A.IsOfferAccepted,
			A.IsOfferExtended,
			A.IsOnRoster,
			A.IsPreQualified,
			A.IsVetted,
			A.ManagerComments,
			A.Notes,
			A.PersonID,
			person.FormatPersonNameByPersonID(A.PersonID,'TitleFirstLast') AS PersonNameFormatted,
			A.SubmittedDateTime,
			core.FormatDate(A.SubmittedDateTime) AS SubmittedDateTimeFormatted,
			A.Suitability,
			A.TechnicalManagerComments,
			A.TerminationDate,
			core.FormatDate(A.TerminationDate) AS TerminationDateFormatted,
			A.VacancyID,
			AR.AgeRangeID,
			AR.AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			EC.ExpertCategoryID,
			EC.ExpertCategoryName,
			G.GenderID,
			G.GenderName,
			IO.InterviewOutcomeID,
			IO.InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			PT.PersonTypeName,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName,
			workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID('Application', A.ApplicationID, 'Not Submitted') AS WorkflowStepStatusName,
			(SELECT TOP 1 SentDateTime FROM core.PendingEmail WHERE ApplicationID = A.ApplicationID AND EmailTemplateCode = 'InterviewScheduled') AS IntervieweeEmailedDateTime,
			core.FormatDateTime((SELECT TOP 1 SentDateTime FROM core.PendingEmail WHERE EntityID = A.ApplicationID AND EmailTemplateCode = 'InterviewScheduled' ORDER BY SentDateTime DESC)) AS IntervieweeEmailedDateTimeFormatted
		FROM hrms.Application A
			JOIN person.Person P ON P.PersonID = A.PersonID
			JOIN dropdown.AgeRange AR ON AR.AgeRangeID = A.AgeRangeID
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = A.ExpertCategoryID
			JOIN dropdown.Gender G ON G.GenderID = A.GenderID
			JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = A.InterviewOutcomeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND A.ApplicationID = @ApplicationID

		END
	--ENDIF

	IF @ClientVacancyID = 0
		BEGIN

		--ApplicationDocumentCV
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'Application'
				AND DE.EntityTypeSubCode = 'CV'
				AND DE.EntityID = @ApplicationID

		--ApplicationDocumentPassport
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'Application'
				AND DE.EntityTypeSubCode = 'Passport'
				AND DE.EntityID = @ApplicationID

		--ApplicationEthnicity
		SELECT 
			E.EthnicityCategoryName,
			E.EthnicityID,
			E.EthnicityName,
			AE.EthnicityDescription
		FROM hrms.ApplicationEthnicity AE
			JOIN dropdown.Ethnicity E ON E.EthnicityID = AE.EthnicityID
				AND AE.ApplicationID = @ApplicationID
		ORDER BY E.EthnicityCategorDisplayOrder, E.EthnicityDisplayOrder, E.EthnicityName, E.EthnicityID

		--ApplicationInterviewAvailability
		SELECT 
			AIA.ApplicationInterviewAvailabilityID,
			AIA.ApplicationInterviewScheduleID,
			AIA.ApplicationPersonID,
			AIA.IsAvailable,
			core.yesNoFormat(AIA.IsAvailable) AS IsAvailableYesNoFormat,
			AIA.ApplicationID,
			AIA.PersonID
		FROM hrms.ApplicationInterviewAvailability AIA
		WHERE AIA.ApplicationPersonID IN (SELECT AP.ApplicationPersonID FROM hrms.ApplicationPerson AP WHERE AP.ApplicationID = @ApplicationID)

		--ApplicationInterviewSchedule
		SELECT 
			AIS.ApplicationInterviewScheduleID,
			AIS.InterviewDateTime,
			core.FormatDateTime(AIS.InterviewDateTime) AS InterviewDateTimeFormatted,
			AIS.IsSelectedDate,
			core.YesNoFormat(AIS.IsSelectedDate) AS IsSelectedDateYesNoFormat
		FROM hrms.ApplicationInterviewSchedule AIS
		WHERE AIS.ApplicationID = @ApplicationID

	--ApplicationPerson
		SELECT 
			AP.ApplicationID,
			AP.ApplicationPersonID,
			AP.InterviewOutcomeID,
			AP.IsScheduleConfirmed,
			core.YesNoFormat(AP.IsScheduleConfirmed) AS IsScheduleConfirmedYesNoFormat,
			AP.PersonID,
			person.FormatPersonNameByPersonID(AP.PersonID, 'FirstLast') AS PersonNameFormatted,
			AP.PersonTypeCode,
			AP.ProposesInterviewDates,
			core.YesNoFormat(AP.ProposesInterviewDates) AS ProposesInterviewDatesYesNoFormat,
			IO.InterviewOutcomeName,
			P.FirstName,
			P.LastName
		FROM hrms.ApplicationPerson AP
			JOIN person.Person P ON P.PersonID = AP.PersonID
			JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = AP.InterviewOutcomeID
				AND AP.ApplicationID = @ApplicationID
		ORDER BY P.LastName, P.FirstName

		--InterviewCommentsDocument
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink,
			DE.EntityTypeSubCode AS FileEntityTypeSubCode
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'InterviewComments'
				AND DE.EntityID = @ApplicationID

		--PersonApplicantSource
		SELECT
			APS.ApplicantSourceID,
			APS.ApplicantSourceName
		FROM person.PersonApplicantSource PAS
			JOIN dropdown.ApplicantSource APS ON APS.ApplicantSourceID = PAS.ApplicantSourceID
				AND PAS.PersonID = @PersonID
		ORDER BY APS.DisplayOrder, APS.ApplicantSourceName, APS.ApplicantSourceID

		--PersonCountry
		SELECT
			C.CountryID,
			C.CountryName
		FROM person.PersonCountry PC
			JOIN dropdown.Country C ON C.CountryID = PC.CountryID
				AND PC.PersonID = @PersonID
		ORDER BY C.CountryName, C.CountryID

		--PersonLanguage
		SELECT
			L.LanguageName,
			LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
			LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
			LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
			PL.IsVerified
		FROM person.PersonLanguage PL
			JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
			JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
			JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
			JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
				AND PL.PersonID = @PersonID
		ORDER BY L.LanguageName

		--Vacancy
		SELECT
			EC.ExpertCategoryName,
			P.ProjectName,
			PS.ProjectSponsorName,
			V.ProjectID,
			V.VacancyName,
			V.VacancyReference,
			VT.VacancyTypeName
		FROM hrms.Vacancy V
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = V.ExpertCategoryID
			JOIN dropdown.Project P ON P.ProjectID = V.ProjectID
			JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
			JOIN dropdown.VacancyType VT ON VT.VacancyTypeID = V.VacancyTypeID
				AND V.VacancyID = @VacancyID

		--VacancyCompetency
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAAC.Suitability,
			OAAC.SuitabilityRatingName,
			ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
		FROM hrms.VacancyCompetency VC
			JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
			JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
				AND VC.VacancyID = @VacancyID
				AND CT.CompetencyTypeCode = 'Competency'
				AND C.IsActive = 1
			OUTER APPLY
				(
				SELECT
					AC.Suitability,
					AC.SuitabilityRatingID,
					SR.SuitabilityRatingName
				FROM hrms.ApplicationCompetency AC
					JOIN dropdown.SuitabilityRating SR ON SR.SuitabilityRatingID = AC.SuitabilityRatingID
						AND AC.CompetencyID = C.CompetencyID
						AND AC.ApplicationID = @ApplicationID
				) OAAC
		ORDER BY C.CompetencyName, C.CompetencyID

		--VacancyTechnicalCapability
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAAC.Suitability,
			ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
		FROM hrms.VacancyCompetency VC
			JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
			JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
				AND VC.VacancyID = @VacancyID
				AND CT.CompetencyTypeCode = 'TechnicalCapability'
				AND C.IsActive = 1
			OUTER APPLY
				(
				SELECT
					AC.Suitability,
					AC.SuitabilityRatingID,
					SR.SuitabilityRatingName
				FROM hrms.ApplicationCompetency AC
					JOIN dropdown.SuitabilityRating SR ON SR.SuitabilityRatingID = AC.SuitabilityRatingID
						AND AC.CompetencyID = C.CompetencyID
						AND AC.ApplicationID = @ApplicationID

				) OAAC
		ORDER BY C.CompetencyName, C.CompetencyID

		--WorkflowData
		EXEC workflow.GetEntityWorkflowData 'Application', @ApplicationID

		--WorkflowPeople
		EXEC workflow.GetEntityWorkflowPeople 'Application', @ApplicationID, @nWorkflowStepNumber

		--WorkflowEventLog
		SELECT
			EL.EventLogID,
			person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
			core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		
			CASE
				WHEN EL.EventCode = 'create'
				THEN 'Created Person Update'
				WHEN EL.EventCode = 'decrementworkflow'
				THEN 'Rejected Person Update'
				WHEN EL.EventCode = 'incrementworkflow'
				THEN 'Approved Person Update'
				WHEN EL.EventCode = 'update'
				THEN 'Updated Person Update'
			END AS EventAction,
		
			EL.Comments
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode = 'Application'
			AND EL.EntityID = @ApplicationID
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
		ORDER BY EL.CreateDateTime

		END
	ELSE
		BEGIN

		--ApplicationContactData
		SELECT
			ACD.ContactEmailAddress,
			ACD.ContactJobTitle,
			ACD.ContactName,
			ACD.ContactPhone,
			ACD.ContactTypeCode
		FROM hrms.ApplicationContactData ACD
		WHERE ACD.ApplicationID = @ApplicationID
		ORDER BY ACD.ContactTypeCode

		--ApplicationDocumentContingentLiabilityProforma
		SELECT
			core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
			D.DocumentGUID,
			D.DocumentTitle,
			'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
			'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
		FROM document.DocumentEntity DE
			JOIN document.Document D ON D.DocumentID = DE.DocumentID
				AND DE.EntityTypeCode = 'Application'
				AND DE.EntityTypeSubCode = 'ContingentLiabilityProforma'
				AND DE.EntityID = @ApplicationID

		--ClientVacancy
		SELECT
			CV.RoleName,
			CV.TaskCode,
			CV.TaskLocation
		FROM hrms.ClientVacancy CV
		WHERE CV.ClientVacancyID = @ClientVacancyID

		--ClientVacancyCompetencyDesired
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAS.Suitability
		FROM hrms.ClientVacancyCompetency CVC
			JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
				AND CVC.ClientVacancyID = @ClientVacancyID
				AND CVC.IsEssential = 0
			OUTER APPLY
				(
				SELECT
					AC.Suitability
				FROM hrms.ApplicationCompetency AC
				WHERE AC.ApplicationID = @ApplicationID
					AND AC.CompetencyID = C.CompetencyID
				) OAS

		--ClientVacancyCompetencyEssential
		SELECT
			C.CompetencyDescription,
			C.CompetencyID,
			C.CompetencyName,
			OAS.Suitability
		FROM hrms.ClientVacancyCompetency CVC
			JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
				AND CVC.ClientVacancyID = @ClientVacancyID
				AND CVC.IsEssential = 1
			OUTER APPLY
				(
				SELECT
					AC.Suitability
				FROM hrms.ApplicationCompetency AC
				WHERE AC.ApplicationID = @ApplicationID
					AND AC.CompetencyID = C.CompetencyID
				) OAS

		--ClientVacancyInterviewDateTime
		SELECT
			IIF (EXISTS (SELECT 1 FROM HRMS.ApplicationInterviewAvailability AIA WHERE AIA.ClientVacancyInterviewDateTimeID = CVIDT.ClientVacancyInterviewDateTimeID), 1, 0) AS IsAvailable,
			CVIDT.ClientVacancyInterviewDateTimeID,
			CVIDT.InterviewDateTime,
			core.FormatDateTime(CVIDT.InterviewDateTime) AS InterviewDateTimeFormatted
		FROM hrms.ClientVacancyInterviewDateTime CVIDT
		WHERE CVIDT.ClientVacancyID = @ClientVacancyID

		END
	--ENDIF

END
GO
--End procedure hrms.GetApplicationByApplicationID

--Begin procedure hrms.GetClientVacancyByClientVacancyID
EXEC utility.DropObject 'hrms.GetClientVacancyByClientVacancyID'
GO
-- =============================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.26
-- Description:	A stored procedure to get data from the hrms.ClientVacancy table
-- =============================================================================
CREATE PROCEDURE hrms.GetClientVacancyByClientVacancyID

@ClientVacancyID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ClientVacancy', @ClientVacancyID)

	--ClientVacancy
	SELECT
		CV.ClientVacancyID,
	--task columns
		CV.TaskName,
		CV.ClientPersonID,
		person.FormatPersonNameByPersonID(CV.ClientPersonID, 'FirstLast') AS ClientPersonNameFormatted,
		CV.TaskCode,
		CV.TaskStartDate,
		core.FormatDate(CV.TaskStartDate) AS TaskStartDateFormatted,
		CV.TaskEndDate,
		core.FormatDate(CV.TaskEndDate) AS TaskEndDateFormatted,
		CV.CountryID,
		C.CountryName,
		CV.TaskOwnerPersonID,
		person.FormatPersonNameByPersonID(CV.TaskOwnerPersonID, 'FirstLast') AS TaskOwnerPersonNameFormatted,
		CV.RegionalPriorityTierID,
		RPT.RegionalPriorityTierName,
		CV.ClientLocation,
		CV.ClientRegionID,
		CR.ClientRegionName,
		CV.ClientDepartment,
		CV.ClientContact1Name,
		CV.ClientContact1Team,
		CV.ClientContact1Email,
		CV.ClientContact1Phone,
		CV.ClientContact2Name,
		CV.ClientContact2Team,
		CV.ClientContact2Email,
		CV.ClientContact2Phone,
		CV.RecruitmentPathwayID,
		RP.RecruitmentPathwayName,
		CV.TaskLocation,
	--role details columns
		CV.RoleName,
		CV.RoleCode,
		CV.ApplicationOpenDate,
		core.FormatDate(CV.ApplicationOpenDate) AS ApplicationOpenDateFormatted,
		CV.ApplicationCloseDate,
		core.FormatDate(CV.ApplicationCloseDate) AS ApplicationCloseDateFormatted,
		CV.ClientVacancyRoleStatusID,
		CVRS.ClientVacancyRoleStatusName,
		CV.ProjectLaborCodeID,
		PLC.ProjectLaborCodeName,
		CV.RoleTeam,
		CV.PositionCount,
		CV.ExpectedStartDate,
		core.FormatDate(CV.ExpectedStartDate) AS ExpectedStartDateFormatted,
		CV.ExpectedEndDate,
		core.FormatDate(CV.ExpectedEndDate) AS ExpectedEndDateFormatted,
		CV.WorkingDaysCount,
		CV.HoursWorkedPerDay,
		CV.Salary,
		CV.EmploymentTypeID,
		ET.EmploymentTypeName,
		CV.ThematicFocusID,
		TF.ThematicFocusName,
		CV.TaskTypeID,
		TT.TaskTypeName,
		CV.PublishToID,
		PT.PublishToName,
		CV.SecurityClearanceTypeID,
		SC.SecurityClearanceCode AS SecurityClearanceTypeName,
		CV.OperationalTrainingCourseID,
		OTT.OperationalTrainingCourseName,
		CV.IsFirearmsTrainingRequired,
		core.YesNoFormat(CV.IsFirearmsTrainingRequired) AS IsFirearmsTrainingRequiredYesNoFormat,
	--Role Information
		CV.RoleContext,
		CV.KeyDeliverables,
		CV.SiftDate,
		core.FormatDate(CV.SiftDate) AS SiftDateFormatted,
		CV.InterviewShortlistDate,
		core.FormatDate(CV.InterviewShortlistDate) AS InterviewShortlistDateFormatted,
		CV.AdvertisedInterviewDate,
		core.FormatDate(CV.AdvertisedInterviewDate) AS AdvertisedInterviewDateFormatted,
		CV.RoleNotes,
		CV.VacancyStatusID,
		VS.VacancyStatusName,
		--uncomment once the application table is in place
		--(SELECT COUNT (PersonID) FROM hrms.ClientVacancyApplication CVA WHERE CVA.ClientVacancyID = CV.ClinetVacancyID) AS ApplicationCount,
		0 AS ApplicationCount,
		iif(RP.RecruitmentPathwayName='Direct','N/A',
		iif(
			ApplicationOpenDate IS NULL OR ApplicationCloseDate IS NULL OR ApplicationOpenDate > getDate(), 0, 
			iif(ApplicationCloseDate<getDate(),
				DATEDIFF(d, ApplicationOpenDate, ApplicationCloseDate),
				DATEDIFF(d, ApplicationOpenDate, getDate())
			)
			)	
		) AS DaysAdvertised

	FROM hrms.ClientVacancy CV
		JOIN dropdown.ClientRegion CR ON CR.ClientRegionID = CV.ClientRegionID
		JOIN dropdown.ClientVacancyRoleStatus CVRS ON CVRS.ClientVacancyRoleStatusID = CV.ClientVacancyRoleStatusID
		JOIN dropdown.Country C ON C.CountryID = CV.CountryID
		JOIN dropdown.EmploymentType ET ON ET.EmploymentTypeID = CV.EmploymentTypeID
		JOIN dropdown.OperationalTrainingCourse OTT ON OTT.OperationalTrainingCourseID = CV.OperationalTrainingCourseID
		JOIN dropdown.ProjectLaborCode PLC ON PLC.ProjectLaborCodeID = CV.ProjectLaborCodeID
		JOIN dropdown.PublishTo PT ON PT.PublishToID = CV.PublishToID
		JOIN dropdown.RecruitmentPathway RP ON RP.RecruitmentPathwayID = CV.RecruitmentPathwayID
		JOIN dropdown.RegionalPriorityTier RPT ON RPT.RegionalPriorityTierID = CV.RegionalPriorityTierID
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = CV.SecurityClearanceTypeID
		JOIN dropdown.TaskType TT ON TT.TaskTypeID = CV.TaskTypeID
		JOIN dropdown.ThematicFocus TF ON TF.ThematicFocusID = CV.ThematicFocusID
		JOIN dropdown.VacancyStatus VS ON VS.VacancyStatusID = CV.VacancyStatusID
			AND CV.ClientVacancyID = @ClientVacancyID

	--ClientVacancyClientOrganization
	SELECT
		CO.ClientOrganizationID,
		CO.ClientOrganizationName,
		CO.ClientOrganizationCode
	FROM dropdown.ClientOrganization CO
		JOIN hrms.ClientVacancyClientOrganization CVCO ON CVCO.ClientOrganizationID = CO.ClientOrganizationID
	WHERE CVCO.ClientVacancyID = @ClientVacancyID
	ORDER BY CO.DisplayOrder

	--ClientVacancyDeploymentManagerPerson	
	SELECT 
		DMP.ClientVacancyDeploymentManagerPersonID,
		DMP.PersonID,
		person.FormatPersonNameByPersonID(DMP.PersonID,'LastFirst') AS PersonNameFormatted
	FROM hrms.ClientVacancyDeploymentManagerPerson DMP
	WHERE DMP.ClientVacancyID = @ClientVacancyID
	
	--ClientVacancyDocument
	EXEC document.GetEntityDocumentData 'ClientVacancy', @ClientVacancyID

	--ClientVacancyInterviewDateTime
	SELECT 
		CVID.ClientVacancyInterviewDateTimeID,
		CVID.InterviewDateTime,
		core.FormatDateTime(CVID.InterviewDateTime) AS InterviewDateTimeFormatted
	FROM hrms.ClientVacancyInterviewDateTime CVID
	WHERE CVID.ClientVacancyID = @ClientVacancyID

	--ClientVacancyRecruitmentManagerPerson
	SELECT 
		RMP.ClientVacancyRecruitmentManagerPersonID,
		RMP.PersonID,
		person.FormatPersonNameByPersonID(RMP.PersonID,'LastFirst') AS PersonNameFormatted
	FROM hrms.ClientVacancyRecruitmentManagerPerson RMP
	WHERE RMP.ClientVacancyID = @ClientVacancyID

	--ClientVacancyReviewerPerson
	SELECT 
		RP.ClientVacancyReviewerPersonID,
		RP.PersonID,
		person.FormatPersonNameByPersonID(RP.PersonID,'LastFirst') AS PersonNameFormatted
	FROM hrms.ClientVacancyReviewerPerson RP
	WHERE RP.ClientVacancyID = @ClientVacancyID

	--ClientVacancyRoleCategory
	SELECT
		RC.RoleCategoryID,
		RC.RoleCategoryName,
		RC.RoleCategoryCode
	FROM dropdown.RoleCategory RC
		JOIN hrms.ClientVacancyRoleCategory CVRC ON CVRC.RoleCategoryID = RC.RoleCategoryID
	WHERE CVRC.ClientVacancyID = @ClientVacancyID
	ORDER BY RC.DisplayOrder

	--ClientVacancyRoleSubCategory
	SELECT
		RC.RoleSubCategoryID,
		RC.RoleSubCategoryName,
		RC.RoleSubCategoryCode
	FROM dropdown.RoleSubCategory RC
		JOIN hrms.ClientVacancyRoleSubCategory CVRC ON CVRC.RoleSubCategoryID = RC.RoleSubCategoryID
	WHERE CVRC.ClientVacancyID = @ClientVacancyID
	ORDER BY RC.DisplayOrder

	--ClientVacancySkillsDesirable
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		CVC.CompetencyID,
		CVC.ClientVacancyCompetencyID
	FROM hrms.ClientVacancyCompetency CVC
		JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND CVC.ClientVacancyID = @ClientVacancyID
			AND CVC.IsEssential <> 1

	--ClientVacancySkillsEssential
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		CVC.CompetencyID,
		CVC.ClientVacancyCompetencyID
	FROM hrms.ClientVacancyCompetency CVC
		JOIN hrms.Competency C ON C.CompetencyID = CVC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND CVC.ClientVacancyID = @ClientVacancyID
			AND CVC.IsEssential = 1
	
	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'ClientVacancy', @ClientVacancyID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'ClientVacancy', @ClientVacancyID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created ClientVacancy'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected ClientVacancy'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved ClientVacancy'
			WHEN EL.EventCode = 'update'
			THEN 'Updated ClientVacancy'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode LIKE 'ClientVacancy%'
		AND EL.EntityID = @ClientVacancyID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure hrms.GetClientVacancyByClientVacancyID

--Begin procedure lesson.GetLessonActionByLessonActionID
EXEC Utility.DropObject 'lesson.GetLessonActionByLessonActionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2019.03.12
-- Description:	A procedure to return data from the lesson.LessonAction table
-- ==========================================================================
CREATE PROCEDURE lesson.GetLessonActionByLessonActionID

@LessonActionID INT

AS
BEGIN
  SET NOCOUNT ON;

	--LessonAction
	SELECT
		'MEAL-' + RIGHT('00000' + CAST(L.LessonID AS VARCHAR(5)), 5) AS LessonIDFormatted,
		LA.CreateDateTime,
		core.FormatDate(LA.CreateDateTime) AS CreateDateTimeFormatted,
		LA.CreatePersonID,
		person.FormatPersonNameByPersonID(LA.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		LA.DueDate,
		core.FormatDate(LA.DueDate) AS DueDateFormatted,
		LA.IsComplete,
		core.YesNoFormat(LA.IsComplete) AS IsCompleteFormatted,
		LA.LessonActionDescription,
		LA.LessonActionID,
		LA.LessonActionName,
		LA.LessonActionPersonID,
		person.FormatPersonNameByPersonID(LA.LessonActionPersonID, 'LastFirst') AS LessonActionPersonNameFormatted,
		LA.LessonID,
		LA.UpdateDateTime,
		core.FormatDate(LA.UpdateDateTime) AS UpdateDateTimeFormatted,
		LA.UpdatePersonID,
		person.FormatPersonNameByPersonID(LA.UpdatePersonID, 'LastFirst') AS UpdatePersonNameFormatted,
		P.ProjectName
	FROM lesson.LessonAction LA
		JOIN lesson.Lesson L ON L.LessonID = LA.LessonID
		JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
			AND LA.LessonActionID = @LessonActionID

END
GO
--End procedure lesson.GetLessonActionByLessonActionID

--Begin procedure lesson.GetLessonByLessonID
EXEC Utility.DropObject 'lesson.GetLessonByLessonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date: 2019.03.12
-- Description:	A procedure to return data from the lesson.Lesson table
-- ====================================================================
CREATE PROCEDURE lesson.GetLessonByLessonID

@LessonID INT

AS
BEGIN
  SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Lesson', @LessonID)

	--Lesson
	SELECT
		ID.ImplementationDifficultyID,
		ID.ImplementationDifficultyName,
		II.ImplementationImpactID,
		II.ImplementationImpactName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		L.Cause,
		L.ClientCode,
		L.CreatePersonID,
		person.FormatPersonNameByPersonID(L.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		L.LessonDate,
		core.FormatDate(L.LessonDate) AS LessonDateFormatted,
		L.Event,
		L.Impact,
		L.Indicators,
		L.IsActive,
		L.ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(L.ISOCountryCode2) AS CountryName,
		L.ISOCurrencyCode,
		L.LessonDescription,
		L.LessonID,
		LPS.LessonPublicationStatusCode + '-' + RIGHT('00000' + CAST(L.LessonID AS VARCHAR(5)), 5) AS LessonIDFormatted,
		workflow.IsWorkflowComplete('Lesson', L.LessonID) AS IsWorkflowComplete,
		L.LessonName,
		L.Recommendations,
		L.ScaleOfResponse,
		LC.LessonCategoryID,
		LC.LessonCategoryName,
		LO.LessonOwnerID,
		LO.LessonOwnerName,
		LPS.LessonPublicationStatusID,
		LPS.LessonPublicationStatusName,
		LS.LessonStatusID,
		LS.LessonStatusName,
		LT.LessonTypeID,
		LT.LessonTypeName,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectSponsorID,
		PS.ProjectSponsorName,
		@nWorkflowStepNumber AS WorkflowStepNumber
	FROM lesson.Lesson L
		JOIN dropdown.ImplementationDifficulty ID ON ID.ImplementationDifficultyID = L.ImplementationDifficultyID
		JOIN dropdown.ImplementationImpact II ON II.ImplementationImpactID = L.ImplementationImpactID
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = L.IncidentTypeID
		JOIN dropdown.LessonCategory LC ON LC.LessonCategoryID = L.LessonCategoryID
		JOIN dropdown.LessonOwner LO ON LO.LessonOwnerID = L.LessonOwnerID
		JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = L.LessonPublicationStatusID
		JOIN dropdown.LessonStatus LS ON LS.LessonStatusID = L.LessonStatusID
		JOIN dropdown.LessonType LT ON LT.LessonTypeID = L.LessonTypeID
		JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = L.ProjectSponsorID
			AND L.LessonID = @LessonID

	--LessonDocument
	EXEC document.GetEntityDocumentData 'Lesson', @LessonID

	--LessonLessonUserClass
	SELECT
		LUC.LessonUserClassID,
		LUC.LessonUserClassName
	FROM dropdown.LessonUserClass LUC
		JOIN lesson.LessonLessonUserClass LLUC ON LLUC.LessonUserClassID = LUC.LessonUserClassID
			AND LLUC.LessonID = @LessonID

	--LessonWorkflowData
	EXEC workflow.GetEntityWorkflowData 'Lesson', @LessonID

	--LessonWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		(SELECT EmailAddress FROM Person.Person WHERE PersonID  = EL.PersonID) as EmailAddress,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EventName AS EventAction,
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN lesson.Lesson L ON L.LessonID = EL.EntityID
			AND L.LessonID = @LessonID
			AND EL.EntityTypeCode = 'Lesson'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--LessonWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Lesson', @LessonID, @nWorkflowStepNumber

END
GO
--End procedure lesson.GetLessonByLessonID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Activity'
		SELECT @bHasAccess = 1 FROM activity.Activity T WHERE T.ActivityID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ActivityReport'
		SELECT @bHasAccess = 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Asset'
		SELECT @bHasAccess = 1 FROM asset.Asset T WHERE T.AssetID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ClientVacancy'
		SELECT @bHasAccess = 1 FROM hrms.ClientVacancy CV WHERE CV.ClientVacancyID = @EntityID
	ELSE IF @EntityTypeCode = 'Contact'
		SELECT @bHasAccess = 1 FROM contact.Contact T WHERE T.ContactID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Course'
		SELECT @bHasAccess = 1 FROM training.Course T WHERE T.CourseID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Document'
		SELECT @bHasAccess = 1 FROM document.Document T WHERE T.DocumentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentCatalog'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentCatalog T WHERE T.EquipmentCatalogID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentItem'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentItem T WHERE T.EquipmentItemID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentOrder'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentOrder T WHERE T.EquipmentOrderID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentMovement'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentMovement T WHERE T.EquipmentMovementID = @EntityID
	ELSE IF @EntityTypeCode = 'Feedback'
		SELECT @bHasAccess = 1 FROM person.Feedback T WHERE T.FeedbackID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Force'
		SELECT @bHasAccess = 1 FROM force.Force T WHERE T.ForceID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Incident'
		SELECT @bHasAccess = 1 FROM core.Incident T WHERE T.IncidentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Lesson'
		SELECT @bHasAccess = 1 FROM lesson.Lesson T JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = T.LessonPublicationStatusID AND LPS.LessonPublicationStatusCode = 'LP'
	ELSE IF @EntityTypeCode = 'Module'
		SELECT @bHasAccess = 1 FROM training.Module T WHERE T.ModuleID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'PersonUpdate'
		SELECT @bHasAccess = 1 FROM personupdate.PersonUpdate PU WHERE PU.PersonUpdateID = @EntityID
	ELSE IF @EntityTypeCode = 'ReportUpdate'
		SELECT @bHasAccess = 1 FROM core.ReportUpdate T WHERE T.ReportUpdateID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ResponseDashboard'
		SELECT @bHasAccess = 1 FROM responsedashboard.ResponseDashboard T WHERE T.ResponseDashboardID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'SpotReport'
		SELECT @bHasAccess = 1 FROM spotreport.SpotReport T WHERE T.SpotReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Territory'
		SELECT @bHasAccess = 1 FROM territory.Territory T JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode AND T.TerritoryID = @EntityID AND TT.IsReadOnly = 0
	ELSE IF @EntityTypeCode = 'TrendReport'
		SELECT @bHasAccess = 1 FROM trendreport.TrendReport T WHERE T.TrendReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Vacancy'
		SELECT @bHasAccess = 1 FROM hrms.Vacancy V WHERE V.VacancyID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = V.ProjectID)
	ELSE IF @EntityTypeCode = 'VacancyApplication'
		SELECT @bHasAccess = 1 FROM hrms.Application A WHERE @EntityID = 0 OR (A.ApplicationID = @EntityID AND A.PersonID = @PersonID)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.CreateDAPersonByPersonID
EXEC utility.DropObject 'person.CreateDAPersonByPersonID'
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.04.06
-- Description:	A stored procedure to create a consultant record in DA from a Person ID
-- ====================================================================================
CREATE PROCEDURE person.CreateDAPersonByPersonID

@PersonID INT,
@InviterPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cUserName VARCHAR(50)
	DECLARE @nDAPersonID INT
	DECLARE @nDocumentID INT
	DECLARE @tOutput TABLE (EntityID INT, UserName VARCHAR(250))

	INSERT INTO DeployAdviserCloud.person.Person
		(BirthDate,CellPhone,ChestSize,Citizenship1ISOCountryCode2,Citizenship2ISOCountryCode2,CollarSize,CountryCallingCodeID,EmailAddress,FirstName,GenderID,HasAcceptedTerms,HeadSize,Height,HomePhone,HomePhoneCountryCallingCodeID,IsActive,IsPhoneVerified,IsSuperAdministrator,IsUKEUNational,LastName,MailAddress1,MailAddress2,MailAddress3,MailISOCountryCode2,MailMunicipality,MailPostalCode,MailRegion,MiddleName,MobilePIN,Organization,Password,PasswordExpirationDateTime,PasswordSalt,PlaceOfBirthISOCountryCode2,PlaceOfBirthMunicipality,PreferredName,RegistrationCode,Suffix,SummaryBiography,Title,WorkPhone,WorkPhoneCountryCallingCodeID)
	OUTPUT INSERTED.PersonID, INSERTED.UserName INTO @tOutput
	SELECT
		P.BirthDate,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		P.Citizenship2ISOCountryCode2,
		P.CollarSize,
		P.CountryCallingCodeID,
		P.EmailAddress,
		P.FirstName,
		ISNULL((SELECT DAG.GenderID FROM DeployAdviserCloud.dropdown.Gender DAG WHERE DAG.GenderCode = G.GenderCode), 0),
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.HomePhoneCountryCallingCodeID,
		P.IsActive,
		P.IsPhoneVerified,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.LastName,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.MiddleName,
		P.MobilePIN,
		P.Organization,
		P.Password,
		P.PasswordExpirationDateTime,
		P.PasswordSalt,
		P.PlaceOfBirthISOCountryCode2,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.WorkPhone,
		P.WorkPhoneCountryCallingCodeID
	FROM person.Person P
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
			AND P.PersonID = @PersonID	

	UPDATE P
	SET
		P.DApersonID = O.EntityID,
		P.UserName = O.UserName
	FROM @tOutput O
		JOIN person.Person P ON P.PersonID = O.EntityID

	SELECT 
		@nDAPersonID = O.EntityID,
		@cUserName = O.UserName
	FROM @tOutput O

	INSERT INTO DeployAdviserCloud.person.PersonLanguage
		(PersonID, ISOLanguageCode2, SpeakingLanguageProficiencyID, ReadingLanguageProficiencyID, WritingLanguageProficiencyID)
	SELECT	
		@nDAPersonID,
		PL.ISOLanguageCode2,
		ISNULL((SELECT DLP1.LanguageProficiencyID FROM DeployAdviserCloud.dropdown.LanguageProficiency DLP1 WHERE DLP1.LanguageProficiencyCode = LP1.LanguageProficiencyCode), 0),
		ISNULL((SELECT DLP2.LanguageProficiencyID FROM DeployAdviserCloud.dropdown.LanguageProficiency DLP2 WHERE DLP2.LanguageProficiencyCode = LP2.LanguageProficiencyCode), 0),
		ISNULL((SELECT DLP3.LanguageProficiencyID FROM DeployAdviserCloud.dropdown.LanguageProficiency DLP3 WHERE DLP3.LanguageProficiencyCode = LP3.LanguageProficiencyCode), 0)
	FROM person.PersonLanguage PL
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonPassport
		(PersonID, PassportExpirationDate, PassportIssuer, PassportNumber, PassportTypeID)
	SELECT
		@nDAPersonID,
		PP.PassportExpirationDate, 
		PP.PassportIssuer, 
		PP.PassportNumber, 
		ISNULL((SELECT DPT.PassportTypeID FROM DeployAdviserCloud.dropdown.PassportType DPT WHERE DPT.PassportTypeCode = PT.PassportTypeCode), 0)
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonPasswordSecurity
		(PersonID, PasswordSecurityQuestionID, PasswordSecurityQuestionAnswer)
	SELECT
		@nDAPersonID,
		ISNULL((SELECT DPSQ.PasswordSecurityQuestionID FROM DeployAdviserCloud.dropdown.PasswordSecurityQuestion DPSQ WHERE DPSQ.PasswordSecurityQuestionCode = PSQ.PasswordSecurityQuestionCode), 0),
		PPS.PasswordSecurityQuestionAnswer
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonQualificationAcademic
		(PersonID, Institution, SubjectArea, GraduationDate, StartDate, Degree)
	SELECT
		@nDAPersonID,
		PQA.Institution, 
		PQA.SubjectArea, 
		PQA.GraduationDate, 
		PQA.StartDate, 
		PQA.Degree
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.person.PersonQualificationCertification
		(PersonID, Provider, Course, CompletionDate, ExpirationDate)
	SELECT
		@nDAPersonID,
		PQC.Provider, 
		PQC.Course, 
		PQC.CompletionDate, 
		PQC.ExpirationDate
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID	

	INSERT INTO DeployAdviserCloud.client.ClientPerson
		(ClientID, PersonID, ClientPersonRoleCode)
	SELECT
		C.ClientID,
		@nDAPersonID,
		'Consultant'
	FROM DeployAdviserCloud.client.Client C
	WHERE C.IntegrationCode = 'Hermis'

	SELECT @nDocumentID = DE.DocumentID 
	FROM document.DocumentEntity DE
	WHERE DE.EntityTypeCode = 'Person'
		AND DE.EntityTypeSubCode = 'CV'
		AND DE.EntityID = @PersonID

	IF @nDocumentID IS NOT NULL
		BEGIN

		DELETE O FROM @tOutput O

		INSERT INTO DeployAdviserCloud.document.Document
			(DocumentDate, DocumentDescription, DocumentGUID, DocumentTitle, Extension, CreatePersonID, ContentType, ContentSubtype, PhysicalFileSize, DocumentData)
		OUTPUT INSERTED.DocumentID, NULL INTO @tOutput
		SELECT
			D.DocumentDate,
			D.DocumentDescription,
			D.DocumentGUID,
			D.DocumentTitle,
			D.Extension,
			@nDAPersonID,
			D.ContentType,
			D.ContentSubtype,
			D.PhysicalFileSize,
			D.DocumentData
		FROM document.Document D
		WHERE D.DocumentID = @nDocumentID

		INSERT INTO DeployAdviserCloud.document.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		SELECT
			O.EntityID,
			'Person',
			'CV',
			@nDAPersonID
		FROM @tOutput O

		END
	--ENDIF

	SELECT
		O.UserName,
		P.EmailAddress AS PersonEmailAddress,
		(SELECT P1.EmailAddress FROM person.Person P1 WHERE P1.PersonID = @InviterPersonID) AS InviterPersonEmailAddress,
		person.FormatPersonNameByPersonID(@InviterPersonID, 'TitleFirstLast') AS PersonNameFormattedLong,
		person.FormatPersonNameByPersonID(@InviterPersonID, 'FirstLast') AS PersonNameFormattedShort,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS SiteURL,
		core.GetSystemSetupValueBySystemSetupKey('SystemName', '') AS SystemName
	FROM @tOutput O
		JOIN person.Person P ON P.PersonID = O.EntityID

END
GO
--End procedure person.CreateDAPersonByPersonID

--Begin procedure person.GetFeedbackByFeedbackID
EXEC Utility.DropObject 'person.GetFeedbackByFeedbackID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the person.Feedback table
-- =============================================================================
CREATE PROCEDURE person.GetFeedbackByFeedbackID

@FeedbackID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Feedback', @FeedbackID)

	--Feedback
	SELECT
		F.CreateDate,
		core.FormatDate(F.CreateDate) AS CreateDateFormatted,
		F.CreatePersonID,
		person.FormatPersonNameByPersonID(F.CreatePersonID, 'LastFirst') AS CreatePersonNameFormated,
		F.CSGMemberComment,
		F.CSGMemberPersonID,
		person.FormatPersonNameByPersonID(F.CSGMemberPersonID, 'LastFirst') AS CSGMemberPersonNameFormated,
		F.EndDate,
		core.FormatDate(F.EndDate) AS EndDateFormatted,
		F.FeedbackDate,
		core.FormatDate(F.FeedbackDate) AS FeedbackDateFormatted,
		F.FeedbackID,
		F.FeedbackName,
		F.FeedbackStatusCode,
		F.IsExternal,
		F.Question4,
		F.Question5,
		F.Question7,
		F.StartDate,
		core.FormatDate(F.StartDate) AS StartDateFormatted,
		F.TaskID,
		(SELECT CV.TaskName FROM hrms.ClientVacancy CV WHERE CV.CLientVacancyID = F.TaskID) AS TaskName,
		(SELECT CV.TaskStartDate FROM hrms.ClientVacancy CV WHERE CV.CLientVacancyID = F.TaskID) AS TaskStartDate,
		core.FormatDate((SELECT CV.TaskStartDate FROM hrms.ClientVacancy CV WHERE CV.CLientVacancyID = F.TaskID)) AS TaskStartDateFormatted,
		MPR.CSGMemberPerformanceRatingCode,
		MPR.CSGMemberPerformanceRatingID,
		MPR.CSGMemberPerformanceRatingName,
		P.ProjectID,
		P.ProjectName,
		PR1.SUPerformanceRatingCode AS SUPerformanceRatingCode1,
		PR1.SUPerformanceRatingID AS SUPerformanceRatingID1,
		PR1.SUPerformanceRatingName AS SUPerformanceRatingName1,
		PR2.SUPerformanceRatingCode AS SUPerformanceRatingCode2,
		PR2.SUPerformanceRatingID AS SUPerformanceRatingID2,
		PR2.SUPerformanceRatingName AS SUPerformanceRatingName2,
		PR3.SUPerformanceRatingCode AS SUPerformanceRatingCode3,
		PR3.SUPerformanceRatingID AS SUPerformanceRatingID3,
		PR3.SUPerformanceRatingName AS SUPerformanceRatingName3
	FROM person.Feedback F
		JOIN dropdown.CSGMemberPerformanceRating MPR ON MPR.CSGMemberPerformanceRatingID = F.CSGMemberPerformanceRatingID
		JOIN dropdown.Project P ON P.ProjectID = F.ProjectID
		JOIN dropdown.SUPerformanceRating PR1 ON PR1.SUPerformanceRatingID = F.SUPerformanceRatingID1
		JOIN dropdown.SUPerformanceRating PR2 ON PR2.SUPerformanceRatingID = F.SUPerformanceRatingID2
		JOIN dropdown.SUPerformanceRating PR3 ON PR3.SUPerformanceRatingID = F.SUPerformanceRatingID3
			AND F.FeedbackID = @FeedbackID

	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'Feedback', @FeedbackID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Feedback', @FeedbackID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Feedback'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Feedback'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Feedback'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Feedback'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode LIKE '%Feedback'
		AND EL.EntityID = @FeedbackID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure person.GetFeedbackByFeedbackID

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.19
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.26
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.29
-- Description: Added IsOnSURoster to the select list
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.30
-- Description: Added perosn record fields for personupdate workflow
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.ApplicantSourceOther,
		P.ApplicantTypeCode,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CivilServiceDepartment,
		P.CivilServiceGrade1,
		P.CivilServiceGrade2,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAAccountDecomissionedDate,
		core.FormatDate(P.DAAccountDecomissionedDate) AS DAAccountDecomissionedDateFormatted,
		P.DAPersonID,
		IIF(P.DAPersonID > 0, 'Yes ', 'No ') AS HasDAAccountYesNoFormat,
		P.DaysToRemoval,
		P.DefaultProjectID,
		P.DeleteByPersonID,
		person.FormatPersonNameByPersonID(P.DeleteByPersonID, 'FirstLast') AS DeleteByPersonNameFormatted,
		P.DeleteDate,
		core.FormatDate(P.DeleteDate) AS DeleteDateFormatted,
		P.DepartmentName,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FARemoveByPersonID,
		person.FormatPersonNameByPersonID(P.FARemoveByPersonID, 'FirstLast') AS FARemoveByPersonNameFormatted,
		P.FARemoveDate,
		core.FormatDate(P.FARemoveDate) AS FARemoveDateFormatted,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.HRContactCountryCallingCodeID,
		P.HRContactEmailAddress,
		P.HRContactName,
		P.HRContactPhone,
		P.InvalidLoginAttempts,
		P.InvoiceLimit,
		P.IsAccountLockedOut,
		P.IsActive,
		P.ISDAAccountDecomissioned,
		IIF(P.ISDAAccountDecomissioned = 1, 'Yes', 'No') AS ISDAAccountDecomissionedYesNoFormat,
		P.IsPhoneVerified,		
		P.IsPreviousApplicant,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKResidencyEligible,
		P.IsUKWorkEligible,
		P.JobTitle,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastContractEndDate,
		core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.LineManagerCountryCallingCodeID,
		P.LineManagerEmailAddress,
		P.LineManagerName,
		P.LineManagerPhone,
		P.LockOutByPersonID,
		person.FormatPersonNameByPersonID(P.LockOutByPersonID, 'FirstLast') AS LockOutByPersonNameFormatted,
		P.LockOutDate,
		core.FormatDate(P.LockOutDate) AS LockOutDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.OrganizationName,
		P.OtherGrade,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'Humanitarian'), 0) AS IsOnHSOTRoster,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'SU'), 0) AS IsOnSURoster,
		ISNULL((SELECT TOP 1 SC.SecurityClearanceName + ' - ' + core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID AND PSC.IsActive = 1 AND PSC.PersonID = P.PersonID), 'None') AS Vetting,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.PreviousApplicationDate,
		core.FormatDateTime(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
		P.PurchaseOrderLimit,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.SecurityClearanceID,
		P.SecurityClearanceSponsorName,
		P.Suffix,
		P.SummaryBiography,
		P.TeamName,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR1.PoliceRankID AS PoliceRankID1,
		PR1.PoliceRankName AS PoliceRankName1,
		PR2.PoliceRankID AS PoliceRankID2,
		PR2.PoliceRankName AS PoliceRankName2,
		PT.PersonTypeCode,
		PT.PersonTypeID,
		PT.PersonTypeName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
		JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

	--PersonApplicantSource
	SELECT 
		A.ApplicantSourceID,
		A.ApplicantSourceName
	FROM person.PersonApplicantSource PAS
		JOIN dropdown.ApplicantSource A ON A.ApplicantSourceID = PAS.ApplicantSourceID
			AND PAS.PersonID = @PersonID

	--PersonClientRoster
	SELECT 
		CR.ClientRosterCode,
		CR.ClientRosterName,
		ES.ExpertStatusID,
		ES.ExpertStatusName,
		PCR.ClientRosterID,
		PCR.ExpertStatusID,
		PCR.PersonClientRosterID,
		PCR.TerminationDate,
		core.FormatDate(PCR.TerminationDate) AS TerminationDateFormatted,
		PCR.TerminationReason
	FROM person.PersonClientRoster PCR
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
			AND PCR.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'PersonCorrespondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEC
		JOIN dropdown.Country C ON C.CountryID = PEC.CountryID
			AND PEC.PersonID = @PersonID
	ORDER BY C.CountryName, C.CountryID

	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonGroupPerson
	SELECT 
		PGP.PersonGroupPersonID,
		PGP.PersonGroupID,
		PGP.CreateDateTime,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateFormatted,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PGP2.PersonID) FROM person.PersonGroupPerson PGP2 WHERE PGP2.PersonGroupID = PG.PersonGroupID) AS UserCount,
		PG.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName
	FROM person.PersonGroupPerson PGP
		JOIN person.PersonGroup PG ON PG.PersonGroupID = PGP.PersonGroupID
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PGP.PersonID = @PersonID

	--PersonIR35
	SELECT
		newID() AS PersonIR35GUID,
		PIR.TaskName,
		PIR.IR35StatusDate,
		PIR.IR35StatusID,
		core.FormatDate(PIR.IR35StatusDate) AS IR35StatusDateFormatted,
		PIR.PersonIR35ID,
		PIR.Issues,
		IRS.IR35StatusName
	FROM person.PersonIR35 PIR
		JOIN dropdown.IR35Status IRS ON IRS.IR35StatusID = PIR.IR35StatusID
			AND PIR.PersonID = @PersonID
	ORDER BY PIR.TaskName, PIR.PersonIR35ID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetVettingEmailData
EXEC Utility.DropObject 'person.GetVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data for outgoing vetting e-mail
-- ==========================================================================
CREATE PROCEDURE person.GetVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@ReviewTypeCode VARCHAR(50),
@SecurityClearanceID INT,
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonSecurityClearanceProcessStep
		(PersonID, SecurityClearanceID, SecurityClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		@SecurityClearanceID,
		(SELECT SCPS.SecurityClearanceProcessStepID FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = @ReviewTypeCode),
		getDate(),
		1,
		'Security clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.DiscretionaryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS DiscretionaryReviewDateFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS MandatoryReviewDateFormatted
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		0 AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL,
		NULL

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetVettingEmailData

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDAPersonID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		DAPersonID INT NOT NULL DEFAULT 0,
		DefaultProjectID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		RoleName VARCHAR(50),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsPasswordExpired = CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN 1 ELSE 0 END,
		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDAPersonID = ISNULL(P.DAPersonID, 0),
		@nDefaultProjectID = P.DefaultProjectID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(
			CellPhone,
			CountryCallingCodeID,
			DAPersonID,
			DefaultProjectID,
			EmailAddress,
			FullName,
			IsAccountLockedOut,
			IsActive,
			IsPhoneVerified,
			IsProfileUpdateRequired,
			IsSuperAdministrator,
			IsValidPassword,
			IsValidUserName,
			PersonID,
			RequiredProfileUpdate,
			RoleName,
			UserName
			) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@nDAPersonID,
			@nDefaultProjectID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cRoleName,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT 
		P.*,
		person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted
	FROM @tPerson P

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure procurement.GetEquipmentOrderByEquipmentOrderID
EXEC Utility.DropObject 'procurement.GetEquipmentOrderByEquipmentOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE procurement.GetEquipmentOrderByEquipmentOrderID

@EquipmentOrderID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('EquipmentOrder', @EquipmentOrderID)

	SELECT
		EO.EquipmentOrderID,
		EO.EquipmentOrderName,
		EO.RecipientEntityTypeCode,
		EO.RecipientEntityID,

		CASE
			WHEN EO.RecipientEntityTypeCode = 'Asset'
			THEN (SELECT IntegrationCode FROM asset.Asset A WHERE A.AssetID = EO.RecipientEntityID ) 
			ELSE '' 
		END AS RecipientEntityIntegrationCode,

		CASE
			WHEN EO.RecipientEntityTypeCode = 'Asset'
			THEN (SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = EO.RecipientEntityID)
			WHEN EO.RecipientEntityTypeCode = 'Contact'
			THEN contact.FormatContactNameByContactID(EO.RecipientEntityID, 'LastFirst')
			WHEN EO.RecipientEntityTypeCode = 'Force'
			THEN (SELECT F.ForceName FROM force.Force F WHERE F.ForceID = EO.RecipientEntityID)
			WHEN EO.RecipientEntityTypeCode = 'Person'
			THEN person.FormatPersonNameByPersonID(EO.RecipientEntityID, 'LastFirst')
		END AS RecipientEntityName,

		CASE
			WHEN EO.RecipientEntityTypeCode = 'Asset'
			THEN 'Facility'
			WHEN EO.RecipientEntityTypeCode = 'Contact'
			THEN 'Person'
			WHEN EO.RecipientEntityTypeCode = 'Force'
			THEN 'Organisation'
			WHEN EO.RecipientEntityTypeCode = 'Person'
			THEN 'Person'
			ELSE ''
		END AS RecipientEntityTypeName,

		EO.OrderedByPersonID,
		person.FormatPersonNameByPersonID(EO.OrderedByPersonID, 'LastFirst') AS OrderedByPersonNameFormatted,
		convert(varchar(10) , EO.OrderDate, 120) AS  OrderDate,
		core.FormatDate(EO.OrderDate) AS OrderDateFormatted,
		EO.Outcome,
		EO.RecipientAddress1,
		EO.RecipientAddress2,
		EO.RecipientAddress3,
		EO.RecipientCity,
		EO.RecipientRegion,
		EO.RecipientPostalCode,
		EO.RecipientISOCountryCode2,
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.RecipientISOCountryCode2) AS RecipientISOCOuntryCode3,
		dropdown.GetCountryNameByISOCountryCode(EO.RecipientISOCountryCode2) AS RecipientISOCountryName,
		EO.RecipientAddressNotes,
		EO.FinalMileAddress1,
		EO.FinalMileAddress2,
		EO.FinalMileAddress3,
		EO.FinalMileCity,
		EO.FinalMileRegion,
		EO.FinalMilePostalCode,
		EO.FinalMileISOCountryCode2,
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.FinalMileISOCountryCode2) AS FinalMileISOCOuntryCode3,
		dropdown.GetCountryNameByISOCountryCode(EO.FinalMileISOCountryCode2) AS FinalMileISOCountryName,
		EO.FinalMileAddressNotes,
		EO.ProjectID,
		EO.ProjectCode,
		EO.ClientPONumber,
		convert(varchar(10) , EO.OrderCompletionDate, 120) AS  OrderCompletionDate,
		core.FormatDate(EO.OrderCompletionDate) AS OrderCompletionDateFormatted,
		EO.ItemsNotesAndConditions,
		EO.ServicesNotesAndConditions,
		EO.TaskOrderNumber,
		EOT.EquipmentOrderTypeID,
		EOT.EquipmentOrderTypeName,
		dropdown.GetProjectClientNameByProjectID(EO.ProjectID) AS ProjectClientName,
		dropdown.GetProjectNameByProjectID(EO.ProjectID) AS ProjectName
	FROM procurement.EquipmentOrder EO
		JOIN dropdown.EquipmentOrderType EOT ON EO.EquipmentOrderTypeID = EOT.EquipmentOrderTypeID
			AND EO.EquipmentOrderID = @EquipmentOrderID

	SELECT 
		EOCI.EquipmentOrderCatalogItemID,
		EOCI.EquipmentItemID,
		EOCI.EquipmentCatalogID,
		EOCI.Quantity,
		EOCI.AccountCode,
		ROUND(EOCI.UnitCost, 2) AS UnitCost,
		CAST(ROUND((EOCI.UnitCost * EOCI.Quantity),2) AS numeric(18,2)) AS TotalCostexVAT,
		CAST(ROUND(((EOCI.UnitCost * EOCI.Quantity) * ((100 + EOCI.VAT)/100)) ,2) AS numeric(18,2)) AS TotalCostwithVAT,
		ROUND(EOCI.VAT,2) AS VAT,
		EOCI.Footnote,
		EOCI.ItemDescription,
		PM.PricingMethodID,
		PM.PricingMethodName,
		EI.EquipmentItemID,
		EI.ItemName,
		EI.IsColdChainRequired,
		IIF(EI.IsColdChainRequired = 1, 'Yes ', 'No ') AS IsColdChainRequiredFormatted,
		EI.IsControlledItem,
		IIF(EI.IsControlledItem = 1, 'Yes ', 'No ') AS IsControlledItemFormatted,
		EI.MinimumQuantity,
		EIT.EquipmentItemTypeID,
		EIT.EquipmentItemTypeName,
		EIT.EquipmentItemTypeCode
	FROM procurement.EquipmentOrderCatalogItem EOCI
		JOIN procurement.EquipmentItem EI ON EI.EquipmentItemID = EOCI.EquipmentItemID
		JOIN dropdown.PricingMethod PM ON EOCI.PricingMethodID = PM.PricingMethodID
		JOIN dropdown.EquipmentItemType EIT ON EI.EquipmentItemTypeID = EIT.EquipmentItemTypeID
			AND EOCI.EquipmentOrderID = @EquipmentOrderID
		
	EXEC workflow.GetEntityWorkflowData 'EquipmentOrder', @EquipmentOrderID

	EXEC workflow.GetEntityWorkflowPeople 'EquipmentOrder', @EquipmentOrderID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Equipment Requisition'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Equipment Requisition'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Equipment Requisition'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Equipment Requisition'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN procurement.EquipmentOrder EO ON EO.EquipmentOrderID = EL.EntityID
			AND EO.EquipmentOrderID = @EquipmentOrderID
			AND EL.EntityTypeCode = 'EquipmentOrder'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure procurement.GetEquipmentOrderByEquipmentOrderID

--Begin procedure reporting.GetEquipmentWorkOrderDetailsList
EXEC Utility.DropObject 'reporting.GetEquipmentWorkOrderDetailsList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Inderjeet Kaur
-- Create date: 2018.08.08
-- Description:	A procedure to return Equipment Work Order details for reporting
-- =============================================================================
CREATE PROCEDURE reporting.GetEquipmentWorkOrderDetailsList

@PersonID INT

AS
BEGIN
  SET NOCOUNT ON;
	
	SELECT
		dropdown.GetProjectClientNameByProjectID(EO.ProjectID) AS ProjectClientName
		,dropdown.GetProjectNameByProjectID(EO.ProjectID) AS ProjectName	 
		,EO.EquipmentOrderName
		,EO.EquipmentOrderTypeID
		,EOT.EquipmentOrderTypeName 
		,EO.TaskOrderNumber
		,EO.ClientPONumber
		,EO.ProjectCode
		,person.FormatPersonNameByPersonID(EO.OrderedByPersonID, 'LastFirst') AS OrderedByPersonNameFormatted
		,core.FormatDate(EO.OrderDate) AS OrderDateFormatted
		,core.FormatDate(EO.OrderCompletionDate) AS OrderCompletionDateFormatted
		,CASE
		WHEN EO.RecipientEntityTypeCode = 'Asset'
		THEN (SELECT A.AssetName FROM asset.Asset A WHERE A.AssetID = EO.RecipientEntityID)
		WHEN EO.RecipientEntityTypeCode = 'Contact'
		THEN contact.FormatContactNameByContactID(EO.RecipientEntityID, 'LastFirst')
		WHEN EO.RecipientEntityTypeCode = 'Force'
		THEN (SELECT F.ForceName FROM force.Force F WHERE F.ForceID = EO.RecipientEntityID)
		WHEN EO.RecipientEntityTypeCode = 'Person'
		THEN person.FormatPersonNameByPersonID(EO.RecipientEntityID, 'LastFirst')
		END AS RecipientOfOrder
		,CASE
		WHEN EO.RecipientEntityTypeCode = 'Asset'
		THEN 'Facility'
		WHEN EO.RecipientEntityTypeCode = 'Contact'
		THEN 'Person'
		WHEN EO.RecipientEntityTypeCode = 'Force'
		THEN 'Organisation'
		WHEN EO.RecipientEntityTypeCode = 'Person'
		THEN 'Person'
		ELSE ''
		END AS RecipientType
		,CONCAT(EO.RecipientAddress1 , ',', EO.RecipientAddress2 , ',' , EO.RecipientAddress3 , ',' , EO.RecipientCity , ',' , EO.RecipientRegion , ',', EO.RecipientPostalCode , ',' , EO.RecipientISOCountryCode2 , ',' , 
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.RecipientISOCountryCode2)) AS RecipientAddress
		,dropdown.GetCountryNameByISOCountryCode(EO.RecipientISOCountryCode2) AS RecipientISOCountryName
		,EO.RecipientAddressNotes
		,CONCAT(EO.FinalMileAddress1 , ',', EO.FinalMileAddress2 + ',' + EO.FinalMileAddress3 , ',', EO.FinalMileCity , ',', EO.FinalMileRegion , ',', EO.FinalMilePostalCode , ',', EO.FinalMileISOCountryCode2 , ',',
		(SELECT ISOCOuntryCode3 FROM Dropdown.Country WHERE ISOCountryCode2 = EO.FinalMileISOCountryCode2)) as FinalMileAddress
		, dropdown.GetCountryNameByISOCountryCode(EO.FinalMileISOCountryCode2) AS FinalMileISOCountryName
		,EO.FinalMileAddressNotes
		,CASE WHEN workflow.GetWorkflowStepNumber('EquipmentOrder', EO.EquipmentOrderID) <= workflow.GetWorkflowStepCount('EquipmentOrder', EO.EquipmentOrderID) THEN 'Pending'
		ELSE  'Approved' END AS WorkflowStatus
		FROM     procurement.EquipmentOrder EO
		JOIN dropdown.EquipmentOrderType EOT ON EO.EquipmentOrderTypeID = EOT.EquipmentOrderTypeID
		JOIN Reporting.SearchResult SR ON SR.EntityID = EO.EquipmentOrderID
		AND SR.EntityTypeCode = 'EquipmentOrder'
		AND SR.PersonID = @PersonID

END
GO
--End procedure reporting.GetEquipmentWorkOrderDetailsList

--Begin procedure reporting.GetLessonList
EXEC Utility.DropObject 'reporting.GetLessonList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Inderjeet Kaur
-- Create date: 2019.03.25
-- Description:	A procedure to return Lessons for reporting
-- ========================================================
CREATE PROCEDURE reporting.GetLessonList 

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SqlQuery NVARCHAR(3000)
	 DECLARE @ParamDefinition AS NVARCHAR(MAX)

	DECLARE @ColumnListWithConversion VARCHAR(4000) = (SELECT STUFF((SELECT ', CONVERT(VARCHAR(50), ' + ColumnCode + ' ) AS ' + ColumnCode  FROM reporting.SearchSetup WHERE PersonID = @PersonID AND EntityTypeCode = 'Lesson'
																							ORDER BY SearchSetupID FOR XML PATH('')), 1, 2, '') );

	DECLARE @ColumnList VARCHAR(4000) = (SELECT STUFF((SELECT ', '  + ColumnCode FROM reporting.SearchSetup WHERE PersonID = @PersonID AND EntityTypeCode = 'Lesson' AND ColumnCode != 'ID'
																					ORDER BY SearchSetupID FOR XML PATH('')), 1, 2, '') );
																													
	SET  @SqlQuery =     'SELECT ID, ColumnName, Valuess
										   FROM (SELECT 	
										                                L.LessonID AS ID,											
																		P.ProjectName AS ActivityStream,	   
																     	CONVERT(VARCHAR(50),PS.ProjectSponsorName) AS Client,
																	   	CONVERT(VARCHAR(50),	person.FormatPersonNameByPersonID(	CONVERT(VARCHAR(50), L.CreatePersonID), ''LastFirst'')) AS [User],				            
																		CONVERT(VARCHAR(50), dropdown.GetCountryNameByISOCountryCode(CONVERT(VARCHAR(50), L.ISOCountryCode2) )) AS Country,						  
																		LT.LessonTypeName AS [Type],
																		LC.LessonCategoryName AS Category,
																		LPS.LessonPublicationStatusName PublicationStatus,
																	   	CONVERT(VARCHAR(50), ''MEAL-'' + RIGHT(''00000'' + CAST(L.LessonID AS VARCHAR(5)), 5)) AS LogNumber,
																		' + @ColumnListWithConversion +' 				   
														  FROM  lesson.Lesson L
																		JOIN dropdown.LessonCategory LC ON LC.LessonCategoryID = L.LessonCategoryID
																		JOIN dropdown.LessonPublicationStatus LPS ON LPS.LessonPublicationStatusID = L.LessonPublicationStatusID
																		JOIN dropdown.Project P ON P.ProjectID = L.ProjectID
																		JOIN dropdown.LessonType LT ON LT.LessonTypeID = L.LessonTypeID  
																		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = L.ProjectSponsorID
																	   JOIN Reporting.SearchResult SR ON SR.EntityID = L.LessonID
																		AND SR.EntityTypeCode = ''Lesson''
																		AND SR.PersonID = ' + CAST(@PersonID AS NVARCHAR(10)) + ')p
										 	   UNPIVOT
										   (Valuess FOR ColumnName IN(ActivityStream, Client, [User], Country,  [Type], Category, PublicationStatus, LogNumber,   ' + @ColumnList +' )
										   ) AS UnPvt;'
	
	
											
		
		  Set @ParamDefinition =      ' @PersonID INT'
			  
		 PRINT @SQLQuery
         Execute sp_Executesql     @SQLQuery, @ParamDefinition, @PersonID

		RETURN

END
GO
--End procedure reporting.GetLessonList

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
			) AS WorkflowStepCount,
			ISNULL(WS1.WorkflowStepStatusName, 'In Work') AS WorkflowStepStatusName
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND WS1.WorkflowStepNumber = 1
				AND (@ProjectID = 0 OR W1.ProjectID = @ProjectID)
		
		END
	--ENDIF

	ELSE
		BEGIN
	
			IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
				BEGIN
				
				SELECT
					WS.WorkflowStepName,
					D.WorkflowStepNumber,
					workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
					ISNULL(WS.WorkflowStepStatusName, 'In Work') AS WorkflowStepStatusName
				FROM
					(
					SELECT
						EWSGP.WorkflowID,
						MIN(EWSGP.WorkflowStepNumber) AS WorkflowStepNumber
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = @EntityTypeCode
						AND EWSGP.EntityID = @EntityID
						AND EWSGP.IsComplete = 0
					GROUP BY EWSGP.WorkflowID

					UNION

					SELECT
						EWSGPG.WorkflowID,
						MIN(EWSGPG.WorkflowStepNumber) AS WorkflowStepNumber
					FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
					WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
						AND EWSGPG.EntityID = @EntityID
						AND EWSGPG.IsComplete = 0			
					GROUP BY EWSGPG.WorkflowID
					) D
						JOIN workflow.WorkflowStep WS ON WS.WorkflowID = D.WorkflowID
							AND WS.WorkflowStepNumber = D.WorkflowStepNumber

				END
			--ENDIF
			
			ELSE
				BEGIN

				SELECT
					'Workflow Complete' AS WorkflowStepName,
					workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
					workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
					ISNULL(W.WorkflowCompleteStatusName, 'Approved') AS WorkflowStepStatusName
				FROM workflow.Workflow W
				WHERE EXISTS
					(
					SELECT 1
					FROM 
						(
						SELECT
							EWSGP.WorkflowID
						FROM workflow.EntityWorkflowStepGroupPerson EWSGP
						WHERE EWSGP.EntityTypeCode = @EntityTypeCode
							AND EWSGP.EntityID = @EntityID

						UNION

						SELECT
							EWSGPG.WorkflowID
						FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
						WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
							AND EWSGPG.EntityID = @EntityID
						) D
					WHERE D.WorkflowID = W.WorkflowID
					)
				
				END
			--ENDIF
			
		END
	--ENDIF

END
GO
--End procedure workflow.GetEntityWorkflowData

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.EntityTypeSubCode,	
		W.IsActive,
		W.ProjectID,
		dropdown.GetProjectNameByProjectID(W.ProjectID) AS ProjectName,
		W.WorkflowCompleteStatusName,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT 
		WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT 
		WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepStatusName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.IsFinancialApprovalRequired,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		person.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGPG.PersonGroupID,
		WSGPG.WorkflowStepGroupID,
		PG.PersonGroupName
	FROM workflow.WorkflowStepGroupPersonGroup WSGPG
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
		JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT,
@ProjectID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN

	DELETE EWSGP 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID

	DELETE EWSGPG
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

	INSERT INTO workflow.EntityWorkflowStepGroupPersonGroup
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonGroupID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGPG.PersonGroupID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

	IF @WorkflowStepNumber > 0
		BEGIN

		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.WorkflowStepNumber <= @WorkflowStepNumber

		UPDATE EWSGPG
		SET EWSGPG.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID
			AND EWSGPG.WorkflowStepNumber <= @WorkflowStepNumber

		END
	--ENDIF

END
GO
--End procedure workflow.InitializeEntityWorkflow
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE HermisCloud
GO

--Begin table core.EmailTemplate
UPDATE ET
SET ET.EmailTemplateCode = 'InitialMedicalClearanceReview'
FROM core.EmailTemplate ET
WHERE ET.EmailTemplateCode = 'MedicalClearanceReview'
GO

EXEC core.EmailTemplateAddUpdate 
	'Lesson',
	'DecrementWorkflow',
	'Sent when a lesson has been rejected',
	'A Lesson Has Been Disapproved',
	'<p>A lesson was disapproved:</p><br/><p><a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><p><strong>Comments:</strong><br />[[WorkflowComments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the workflow for this lesson. Please log in and click the link above to review.</p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[WorkflowComments]]":"Workflow comments."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Lesson',
	'IncrementWorkflow',
	'Sent when a lesson has been approved',
	'A Lesson Has Been Approved',
	'<p>A lesson was approved:</p><br/><p><a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><p><strong>Comments:</strong><br />[[WorkflowComments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the workflow for this lesson. Please log in and click the link above to review.</p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[WorkflowComments]]":"Workflow comments."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

UPDATE ET
SET ET.WorkflowActionCode = ET.EmailTemplateCode
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode = 'Lesson'
	AND ET.EmailTemplateCode LIKE '%Workflow'
GO

EXEC core.EmailTemplateAddUpdate 
	'LessonAction',
	'LessonActionAdd',
	'Sent when a lesson action has been added',
	'A Lesson Action Has Been Added',
	'<p>A lesson action has been added to the HERMIS system and assigned to you as the Assigned Action Owner</p><br/><p>The name of the lesson action is [[LessonActionName]] and you can view the lesson to which this action is associated here: <a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[LessonActionName]]":"Lesson action name."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'LessonAction',
	'LessonActionUpdate',
	'Sent when a lesson action has been updated',
	'A Lesson Action Has Been Updated',
	'<p>A lesson action has been in the HERMIS system on you which you are designated as the Assigned Action Owner has been updated.</p><br/><p>The name of the lesson action is [[LessonActionName]] and you can view the lesson to which this action is associated here: <a href="[[SiteURL]]/lesson/view/id/[[LessonID]]">[[LessonName]]</a></p><br/>Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>HERMIS Support Team<br><br>If you need support, please email techsupport@skotkonung.com.<br><br>',
	'[{"[[LessonActionName]]":"Lesson action name."},{"[[LessonID]]":"ID of the lesson"},{"[[LessonName]]":"Name of the lesson"},{"[[SiteURL]]":"Link to site."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'MedicalVetting', 
	'FollowupMedicalClearanceReview', 
	'Sent to notifiy a user that one or more medical reviews are required', 
	'Medical Clearance Review Followup Email', 
	'<p>Dear [[ToFullName]],<br /><br />You are required to undergo a [[MedicalClearanceTypeName]] medical check.<br />Please find attached the relevant instructions.<br />Please arrange you check directly with the service provider: Medfit.<br />If you need assistance please contact [Palladium email].<br />Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br /><br />[[Comments]]<br /><br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br /><br />The Palladium HR Team</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[MedicalClearanceTypeName]]":"Medical Clearance Types"},{"[[Comments]]":"Sender comments"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'InviteDAPerson', 
	'Sent when a person is invited to DeployAdviser', 
	'You Have Been Invited To Join DeployAdviser', 
	'<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to become affilliated with DFFD in the DeployAdviser system with a role of consultant.&nbsp; Accepting this offer will allow DFFD to make future offers of assignment to you.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[LoginLink]] system to accept or reject this invitation.&nbsp; Your DeployAdviser username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[LoginURL]]</p><p>PLEASE NOTE:  Your User Name for both [[SystemName]] AND DeployAdviser has been changed to [[UserName]].  Your password remains unchanged.</p><p>Please do not reply to this email as it is generated automatically by the [[SystemName]] system.</p><p>&nbsp;</p><p>Thank you,<br />The [[SystemName]] Team</p>',
	'[{"[[EmailAddress]]":"Inviter email address"},{"[[LoginLink]]":"Link to DA"},{"[[LoginURL]]":"URL to DA"},{"[[PersonNameFormattedLong]]":"Inviter person name (long)"},{"[[PersonNameFormattedShort]]":"Inviter person name (short)"},{"[[SystemName]]":"System name"},{"[[UserName]]":"DA user name"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupBPSS', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupBPSS', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupCTC', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a CTC security clearance require completion', 
	'CTC Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupCTC', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a CTC security clearance require completion', 
	'CTC Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupDV', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a DV security clearance require completion', 
	'DV Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupDV', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a DV security clearance require completion', 
	'DV Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryFollowupSC', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a SC security clearance require completion', 
	'SC Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialFollowupSC', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a SC security clearance require completion', 
	'SC Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'InvitePerson', 
	'Sent when a new person is invited to Hermis', 
	'You Have Been Invited To Join Hermis', 
	'<p>You have been invited by&nbsp;[[PersonNameFormattedLong]] to join the Hermis system.</p><p>For questions regarding this invitation, you may contact [[PersonNameFormattedShort]] via email at [[EmailAddress]].</p><p>You may log in to the [[PasswordTokenLink]] system to set a password and accept or reject this invitation.&nbsp; Your Hermis username is:&nbsp; [[UserName]]</p><p>If the link above does not work, cut and paste the following url into the address bar of your browser:<br />[[PasswordTokenURL]]</p><p>This link is valid for the next 24 hours.</p><p>Please do not reply to this email as it is generated automatically by the Hermis system.</p><p>&nbsp;</p><p>Thank you,<br />The Hermis Team</p>',
	'[{"[[EmailAddress]]": "Sender Email Address"},{"[[PasswordTokenLink]]": "Embedded Reset Password Link"},{"[[PasswordTokenURL]]": "Site Reset Password URL"},{"[[PersonNameFormattedLong]]": "Sender Name (Long)"},{"[[PersonNameFormattedShort]]": "Sender Name (Short)"},{"[[UserName]]": "User Name"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'NewApplication',
	'Steps 7 â 8 New Application for Recruitment to Roster â to CSG Coordinator',
	'HERMIS â A new application for Recruitment to Roster has been received',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to review this vacancy.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_1',
	'Steps 12 â 13 Competency Review Required â to CSG Manager',
	'HERMIS â An application to join the roster is ready for a competency review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to provide your comments. If you have no comments please indicate you have none.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_2',
	'Steps 14 â 15 Technical Review Required â to CSG Manager',
	'HERMIS â An application to join the roster is ready for a technical review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to provide your comments. If you have no comments please indicate you have none.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_DecrementWorkflow_2',
	'Steps 14 â 39 Unsuccessful Candidate â to CSG Coordinator',
	'HERMIS â An applicant has not passed the competency review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to notify the applicant and update the vacancy matrix.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_DecrementWorkflow_3',
	'Steps 14 â 39 Unsuccessful Candidate â to CSG Coordinator',
	'HERMIS â An applicant has not passed the technical capability review',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to notify the applicant and update the vacancy matrix.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_3',
	'Steps 16 â 17 An Application has passed Competency and Technical reviews â to CSG Coordinator',
	'HERMIS â A new application for Recruitment to Roster has passed competency and technical reviews',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to update the vacancy matrix and arrange an interview with the candidate.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_4',
	'Steps 17 - 18 Select Interview Dates â to Interview Panel Member',
	'HERMIS â A new applicant for Recruitment to Roster is ready for interview',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to indicate the dates and time you can be available to participate in the interview of this candidate.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_5',
	'Steps 18 - 19 Select Interview Dates â to Technical CSG Manager',
	'HERMIS â A new applicant for Recruitment to Roster is ready for interview',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to indicate the dates and time you can be available to participate in the interview of this candidate.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_6',
	'Steps 19 - 20 Arrange Interview with Candidate â to CSG Coordinator',
	'HERMIS â A new applicant for Recruitment to Roster is ready for interview',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to arrange the interview with the candidate on the date and time indicated.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'EmailInterviewee',
	'Steps 20 - 21 Interview for CGS Roster â to Candidate Roster Member',
	'Interview for CSG Vacancy',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Following your recent application to join the CSG Roster - [[ExpertCategoryName]] category, we are pleased to inform you that we would like to offer you an interview as follows:	<br><br>
	Date/Time: [[InterviewDateTimeFormatted]]<br>
	Location: [[InterviewLocation]]<br><br>
	Please confirm by email to [[CSGCoordinatorEmailAddress]] that you are available to attend.<br><br>
	Please do not reply to this email as it is generated automatically.<br><br>
	
		All the best,<br>
		The SU Recruitment Team<br>
		[[SURosterContactEmailAddress]]<br>
	<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[CSGCoordinatorEmailAddress]]":"CSG Coordinator Eemail address."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."},
		{"[[InterviewDateTimeFormatted]]":"Date and time of selected interview."},
		{"[[InterviewLocation]]":"Location of interview."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'Application_IncrementWorkflow_7',
	'Steps 22 â 24 Interview Notification â to Technical CSG Manager and Interview Panel Member',
	'HERMIS â An interview has been scheduled',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	An interview has been arranged for the above vacancy and applicant as follows:<br><br>
	Date/Time: [[InterviewDateTimeFormatted]]<br>
	Location: [[InterviewLocation]]<br><br>
	The interview proforma is attached.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."},
		{"[[InterviewDateTimeFormatted]]":"Date and time of selected interview."},
		{"[[InterviewLocation]]":"Location of interview."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationCandidateSelected',
	'Steps 26 - 27 Candidate has been selected â to CSG Coordinator',
	'HERMIS â A candidate has been selected for Recruitment to the CSG Roster process',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to update the vacancy matrix and arrange the provisional offer for the candidate and request acceptance(s) for the role.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationCandidateNotSelected',
	'Steps 26 â 39 Candidate has not been selected â to CSG Coordinator',
	'HERMIS â A candidate has not been selected for Recruitment to the CSG Roster process',
	'
	Vacancy: <a href="[[SiteURL]]/vacancy/view/id/[[VacancyID]]">[[VacancyReference]] â CSG [[ExpertCategoryName]] â [[PersonNameFormatted]]</a><br><br>
	Please log in to HERMIS to update the vacancy matrix and notify the candidate they were unsuccessful.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://uk.hermis.online/login.<br><br>
	Please do not reply to this email as it is generated automatically by the HERMIS system.<br><br>
	HERMIS Support Team<br><br>
	If you need support, please email techsupport@skotkonung.com.<br><br>
	',
	'[
		{"[[SiteURL]]":"Link to site."},
		{"[[VacancyID]]":"ID for vacancy link"},
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationProvisionalMembership',
	'Steps 27 â 28 Provisional Offer â to Candidate Roster Member',
	'Provisional Membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Thank you for applying to become a member of the UK Civilian Stabilisation Group (CSG).<br><br>
	Weâre pleased to inform that pending confirmation of security clearance you will be registered as a member of the CSG.<br><br>
	You will shortly receive an invitation to register your profile on a system called DeployAdviser.<br><br>
	All the best,<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationDAProvisioned',
	'Steps 29 â 30 Provisional Offer â to Candidate Roster Member',
	'Provisional Membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Confirmation of your membership is pending subject to your security clearance being confirmed or obtained.<br><br>
	You must now login to DeployAdviser to create a profile. Please note that failure to do so within five working days may result in the withdrawal of the provisional offer.<br><br>
	Once you have created your profile, you will be contacted by Palladium International Development Limited who will assist you with the security check process on our behalf.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://v2.deployadviser.com/login/.<br><br>
	All the best,<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationVetting',
	'Steps 31 - 33 New Provisional CSG Roster Member for Vetting â to Palladium Vetting Team',
	'New Provisional CSG Roster Member for Vetting',
	'
	We advise that the candidate below has accepted a provisional offer to join the CSG Roster. Membership can only be confirmed after security clearance has been confirmed or obtained.<br><br>
	Please assist the candidate to complete the vetting process.<br><br>
	Vacancy: [[VacancyReference]] â CSG [[ExpertCategoryName]]<br>
	Candidate: [[PersonNameFormatted]]<br>
	Email: [[ApplicantEmailAddress]]<br>
	Mobile: [[ApplicantCellPhone]]<br><br>
	Please do not reply to this email as it is generated automatically.<br><br>
	The SU Recruitment Team<br><br>
	[[SURosterContactEmailAddress]]<br><br>
	',
	'[
		{"[[VacancyReference]]":"Ref code for vacancy."},
		{"[[ExpertCategoryName]]":"Expert category name."},
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[ApplicantEmailAddress]]":"Applicant email address."},
		{"[[ApplicantCellPhone]]":"Applicant cell phone."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationMembershipCOnfirmation',
	'Steps 36 â 37 Confirmation of Membership â to Candidate Roster Member',
	'Membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	Following confirmation / obtainment of your security clearance we are pleased to confirm your membership of the CSG Roster.<br><br>
	We look forward to your application for CSG Tasks.<br><br>
	Tasks for which you are eligible are advertised through DeployAdviser.<br><br>
	Please check regularly for opportunities.<br><br>
	If the link above does not work, cut and paste the following URL into the address bar of your browser: https://v2.deployadviser.com/login/.<br><br>
	All the best,<br>
	The SU Recruitment Team<br>
	[[SURosterContactEmailAddress]]<br>
	<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."},
		{"[[SURosterContactEmailAddress]]":"Contact email address for SU."}
	]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Application',
	'ApplicationRejection',
	'Step 39 - 40 Notify Unsuccessful Candidates â to Candidate Roster Member',
	'Application for membership of the CSG Roster',
	'
	Dear [[PersonNameFormatted]],<br><br>
	We regret to inform that your application to join the CSG Roster has not been successful.<br><br>
	We really appreciate the effort you put into applying to join the CSG Roster.<br><br>
	
	All the best,<br>
	The SU Recruitment Team<br>
	Please do not reply to this email as it is generated automatically.<br>
	<br><br>
	',
	'[
		{"[[PersonNameFormatted]]":"Formatted name of applicant with title."}
	]'
GO
--End table core.EmailTemplate

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ClientVacancy', 
	@EntityTypeName = 'Client Vacancy', 
	@EntityTypeNamePlural = 'Client Vacancies',
	@HasWorkflow = 1,
	@SchemaName = 'hrms', 
	@TableName = 'ClientVacancy', 
	@PrimaryKeyFieldName = 'ClientVacancyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Lesson', 
	@EntityTypeName = 'Lesson Tool', 
	@EntityTypeNamePlural = 'Lessons Tool',
	@HasWorkflow = 1,
	@SchemaName = 'lesson', 
	@TableName = 'Lesson', 
	@PrimaryKeyFieldName = 'LessonID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'DocumentList',
	@NewMenuItemCode = 'LessonList',
	@NewMenuItemLink = '/lesson/list',
	@NewMenuItemText = 'Lesson Tool',
	@PermissionableLineageList = 'Lesson.List',
	@ParentMenuItemCode = 'Reporting'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonUpdate',
	@NewMenuItemCode = 'PersonInvite',
	@NewMenuItemLink = '/person/invite',
	@NewMenuItemText = 'Invite Client',
	@PermissionableLineageList = 'Person.Invite',
	@ParentMenuItemCode = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@BeforeMenuItemCode = 'Vacancy',
	@NewMenuItemCode = 'ClientVacancy',
	@NewMenuItemLink = '/clientvacancy/list',
	@NewMenuItemText = 'Recruitment from Roster',
	@PermissionableLineageList = 'ClientVacancy.List',
	@ParentMenuItemCode = 'HumanResources'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'HumanResources'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Reporting'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'CSGCategoryProfileLink', 'The hyperlink displayed on the vacancy application edit page for CSG Category has a profile information', '<a href="http://www.google.com" style="font-color:blue;" target="_blank">here</a>'
EXEC core.SystemSetupAddUpdate 'PersonalDataProcessingInformation', 'The hyperlink to the personal data handling information page', '<a href="http://www.google.com" style="font-color:blue;" target="_blank">here</a>'
EXEC core.SystemSetupAddUpdate 'PersonLanguageReferenceText', 'The text string displayed above the person language table on the person profile add update page', 'If you have a language skill, please use the <strong>Common European Framework of Reference for Languages and Self-Assessment Tool (PDF, 127KB, 4 pages)</strong> found <a href="https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/615703/Common_European_Framework_of_Reference_for_Languages_and_Self_Assessment_Tool.pdf" style="font-color:blue;" target="_blank">here</a> as a guide to indicate the level of proficiency you have attained in relation to each language. If you have listed your proficiency for a language as above "basic", you may be asked to complete a language skill assessment.'
EXEC core.SystemSetupAddUpdate 'PrivacyPolicy', 'The text of the system privacy policy', 'JC - Please give Todd the actual words for this to add to the build'
EXEC core.SystemSetupAddUpdate 'TermsAndConditions', 'The text of the system terms and conditions', 'JC - Please give Todd the actual words for this to add to the build'

IF core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') = 'http://hermis.local'
	BEGIN

	EXEC core.SystemSetupAddUpdate 'ApplicantSiteURL', '', 'https://client.hermis.local'
	EXEC core.SystemSetupAddUpdate 'ClientSiteURL', '', 'https://client.hermis.local'

	END
ELSE IF core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') = 'https://heros-hermis-dev.azurewebsites.net/'
	BEGIN

	EXEC core.SystemSetupAddUpdate 'ApplicantSiteURL', '', 'https://hsot-applicant.skhosting.co.uk'
	EXEC core.SystemSetupAddUpdate 'ClientSiteURL', '', 'https://hsotclientportal.skhosting.co.uk'

	END
--ENDIF
GO
--End table core.SystemSetup

--Begin table dropdown.ClientOrganization
TRUNCATE TABLE dropdown.ClientOrganization
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientOrganization', 'ClientOrganizationID', 0
GO

INSERT INTO dropdown.ClientOrganization 
	(ClientOrganizationName, ClientOrganizationCode, DisplayOrder)
VALUES
	('Cabinet Office', 'CO', 1),
	('CPS', 'CPS', 2),
	('DFID', 'DFID',3),
	('FCO', 'FCO',4),
	('Home Office', 'HO', 5),
	('HMT', 'HMT', 6),
	('MOD', 'MOD', 7),
	('MOJ', 'MOJ', 8),
	('NCA', 'NCA', 9),
	('NSGI', 'NSGI', 10),
	('SU', 'SU', 11)
GO
--End table dropdown.ClientOrganization

--Begin table dropdown.ClientRegion
TRUNCATE TABLE dropdown.ClientRegion
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientRegion', 'ClientRegionID', 0
GO

INSERT INTO dropdown.ClientRegion 
	(ClientRegionName, ClientRegionCode, DisplayOrder)
VALUES
	('Africa', 'AF', 1),
	('Asia and ROTW', 'AR', 2),
	('Europe and Wider Multilaterals', 'EWM', 3),
	('MENA', 'MENA', 4)
GO
--End table dropdown.ClientRegion

--Begin table dropdown.ClientVacancyRoleStatus
TRUNCATE TABLE dropdown.ClientVacancyRoleStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientVacancyRoleStatus', 'ClientVacancyRoleStatusID', 0
GO

INSERT INTO dropdown.ClientVacancyRoleStatus 
	(ClientVacancyRoleStatusName, ClientVacancyRoleStatusCode, DisplayOrder)
VALUES
	('Active', 'Active', 1),
	('Inavtive', 'Inavtive', 2)
GO
--End table dropdown.ClientVacancyRoleStatus

--Begin table dropdown.Country
UPDATE dropdown.Country Set CountryName = 'Afghanistan' WHERE ISOCountryCode3 = 'AFG'
UPDATE dropdown.Country Set CountryName = 'Ãland Islands' WHERE ISOCountryCode3 = 'ALA'
UPDATE dropdown.Country Set CountryName = 'Albania' WHERE ISOCountryCode3 = 'ALB'
UPDATE dropdown.Country Set CountryName = 'Algeria' WHERE ISOCountryCode3 = 'DZA'
UPDATE dropdown.Country Set CountryName = 'American Samoa' WHERE ISOCountryCode3 = 'ASM'
UPDATE dropdown.Country Set CountryName = 'Andorra' WHERE ISOCountryCode3 = 'AND'
UPDATE dropdown.Country Set CountryName = 'Angola' WHERE ISOCountryCode3 = 'AGO'
UPDATE dropdown.Country Set CountryName = 'Anguilla' WHERE ISOCountryCode3 = 'AIA'
UPDATE dropdown.Country Set CountryName = 'Antarctica' WHERE ISOCountryCode3 = 'ATA'
UPDATE dropdown.Country Set CountryName = 'Antigua and Barbuda' WHERE ISOCountryCode3 = 'ATG'
UPDATE dropdown.Country Set CountryName = 'Argentina' WHERE ISOCountryCode3 = 'ARG'
UPDATE dropdown.Country Set CountryName = 'Armenia' WHERE ISOCountryCode3 = 'ARM'
UPDATE dropdown.Country Set CountryName = 'Aruba' WHERE ISOCountryCode3 = 'ABW'
UPDATE dropdown.Country Set CountryName = 'Australia' WHERE ISOCountryCode3 = 'AUS'
UPDATE dropdown.Country Set CountryName = 'Austria' WHERE ISOCountryCode3 = 'AUT'
UPDATE dropdown.Country Set CountryName = 'Azerbaijan' WHERE ISOCountryCode3 = 'AZE'
UPDATE dropdown.Country Set CountryName = 'Bahamas (the)' WHERE ISOCountryCode3 = 'BHS'
UPDATE dropdown.Country Set CountryName = 'Bahrain' WHERE ISOCountryCode3 = 'BHR'
UPDATE dropdown.Country Set CountryName = 'Bangladesh' WHERE ISOCountryCode3 = 'BGD'
UPDATE dropdown.Country Set CountryName = 'Barbados' WHERE ISOCountryCode3 = 'BRB'
UPDATE dropdown.Country Set CountryName = 'Belarus' WHERE ISOCountryCode3 = 'BLR'
UPDATE dropdown.Country Set CountryName = 'Belgium' WHERE ISOCountryCode3 = 'BEL'
UPDATE dropdown.Country Set CountryName = 'Belize' WHERE ISOCountryCode3 = 'BLZ'
UPDATE dropdown.Country Set CountryName = 'Benin' WHERE ISOCountryCode3 = 'BEN'
UPDATE dropdown.Country Set CountryName = 'Bermuda' WHERE ISOCountryCode3 = 'BMU'
UPDATE dropdown.Country Set CountryName = 'Bhutan' WHERE ISOCountryCode3 = 'BTN'
UPDATE dropdown.Country Set CountryName = 'Bolivia (Plurinational State of)' WHERE ISOCountryCode3 = 'BOL'
UPDATE dropdown.Country Set CountryName = 'Bonaire, Sint Eustatius and Saba' WHERE ISOCountryCode3 = 'BES'
UPDATE dropdown.Country Set CountryName = 'Bosnia and Herzegovina' WHERE ISOCountryCode3 = 'BIH'
UPDATE dropdown.Country Set CountryName = 'Botswana' WHERE ISOCountryCode3 = 'BWA'
UPDATE dropdown.Country Set CountryName = 'Bouvet Island' WHERE ISOCountryCode3 = 'BVT'
UPDATE dropdown.Country Set CountryName = 'Brazil' WHERE ISOCountryCode3 = 'BRA'
UPDATE dropdown.Country Set CountryName = 'British Indian Ocean Territory (the)' WHERE ISOCountryCode3 = 'IOT'
UPDATE dropdown.Country Set CountryName = 'Brunei Darussalam' WHERE ISOCountryCode3 = 'BRN'
UPDATE dropdown.Country Set CountryName = 'Bulgaria' WHERE ISOCountryCode3 = 'BGR'
UPDATE dropdown.Country Set CountryName = 'Burkina Faso' WHERE ISOCountryCode3 = 'BFA'
UPDATE dropdown.Country Set CountryName = 'Burundi' WHERE ISOCountryCode3 = 'BDI'
UPDATE dropdown.Country Set CountryName = 'Cabo Verde' WHERE ISOCountryCode3 = 'CPV'
UPDATE dropdown.Country Set CountryName = 'Cambodia' WHERE ISOCountryCode3 = 'KHM'
UPDATE dropdown.Country Set CountryName = 'Cameroon' WHERE ISOCountryCode3 = 'CMR'
UPDATE dropdown.Country Set CountryName = 'Canada' WHERE ISOCountryCode3 = 'CAN'
UPDATE dropdown.Country Set CountryName = 'Cayman Islands (the)' WHERE ISOCountryCode3 = 'CYM'
UPDATE dropdown.Country Set CountryName = 'Central African Republic (the)' WHERE ISOCountryCode3 = 'CAF'
UPDATE dropdown.Country Set CountryName = 'Chad' WHERE ISOCountryCode3 = 'TCD'
UPDATE dropdown.Country Set CountryName = 'Chile' WHERE ISOCountryCode3 = 'CHL'
UPDATE dropdown.Country Set CountryName = 'China' WHERE ISOCountryCode3 = 'CHN'
UPDATE dropdown.Country Set CountryName = 'Christmas Island' WHERE ISOCountryCode3 = 'CXR'
UPDATE dropdown.Country Set CountryName = 'Cocos (Keeling) Islands (the)' WHERE ISOCountryCode3 = 'CCK'
UPDATE dropdown.Country Set CountryName = 'Colombia' WHERE ISOCountryCode3 = 'COL'
UPDATE dropdown.Country Set CountryName = 'Comoros (the)' WHERE ISOCountryCode3 = 'COM'
UPDATE dropdown.Country Set CountryName = 'Congo (the Democratic Republic of the)' WHERE ISOCountryCode3 = 'COD'
UPDATE dropdown.Country Set CountryName = 'Congo (the)' WHERE ISOCountryCode3 = 'COG'
UPDATE dropdown.Country Set CountryName = 'Cook Islands (the)' WHERE ISOCountryCode3 = 'COK'
UPDATE dropdown.Country Set CountryName = 'Costa Rica' WHERE ISOCountryCode3 = 'CRI'
UPDATE dropdown.Country Set CountryName = 'CÃ´te d''Ivoire' WHERE ISOCountryCode3 = 'CIV'
UPDATE dropdown.Country Set CountryName = 'Croatia' WHERE ISOCountryCode3 = 'HRV'
UPDATE dropdown.Country Set CountryName = 'Cuba' WHERE ISOCountryCode3 = 'CUB'
UPDATE dropdown.Country Set CountryName = 'CuraÃ§ao' WHERE ISOCountryCode3 = 'CUW'
UPDATE dropdown.Country Set CountryName = 'Cyprus' WHERE ISOCountryCode3 = 'CYP'
UPDATE dropdown.Country Set CountryName = 'Czechia' WHERE ISOCountryCode3 = 'CZE'
UPDATE dropdown.Country Set CountryName = 'Denmark' WHERE ISOCountryCode3 = 'DNK'
UPDATE dropdown.Country Set CountryName = 'Djibouti' WHERE ISOCountryCode3 = 'DJI'
UPDATE dropdown.Country Set CountryName = 'Dominica' WHERE ISOCountryCode3 = 'DMA'
UPDATE dropdown.Country Set CountryName = 'Dominican Republic (the)' WHERE ISOCountryCode3 = 'DOM'
UPDATE dropdown.Country Set CountryName = 'Ecuador' WHERE ISOCountryCode3 = 'ECU'
UPDATE dropdown.Country Set CountryName = 'Egypt' WHERE ISOCountryCode3 = 'EGY'
UPDATE dropdown.Country Set CountryName = 'El Salvador' WHERE ISOCountryCode3 = 'SLV'
UPDATE dropdown.Country Set CountryName = 'Equatorial Guinea' WHERE ISOCountryCode3 = 'GNQ'
UPDATE dropdown.Country Set CountryName = 'Eritrea' WHERE ISOCountryCode3 = 'ERI'
UPDATE dropdown.Country Set CountryName = 'Estonia' WHERE ISOCountryCode3 = 'EST'
UPDATE dropdown.Country Set CountryName = 'Eswatini' WHERE ISOCountryCode3 = 'SWZ'
UPDATE dropdown.Country Set CountryName = 'Ethiopia' WHERE ISOCountryCode3 = 'ETH'
UPDATE dropdown.Country Set CountryName = 'Falkland Islands (the) [Malvinas]' WHERE ISOCountryCode3 = 'FLK'
UPDATE dropdown.Country Set CountryName = 'Faroe Islands (the)' WHERE ISOCountryCode3 = 'FRO'
UPDATE dropdown.Country Set CountryName = 'Fiji' WHERE ISOCountryCode3 = 'FJI'
UPDATE dropdown.Country Set CountryName = 'Finland' WHERE ISOCountryCode3 = 'FIN'
UPDATE dropdown.Country Set CountryName = 'France' WHERE ISOCountryCode3 = 'FRA'
UPDATE dropdown.Country Set CountryName = 'French Guiana' WHERE ISOCountryCode3 = 'GUF'
UPDATE dropdown.Country Set CountryName = 'French Polynesia' WHERE ISOCountryCode3 = 'PYF'
UPDATE dropdown.Country Set CountryName = 'French Southern Territories (the)' WHERE ISOCountryCode3 = 'ATF'
UPDATE dropdown.Country Set CountryName = 'Gabon' WHERE ISOCountryCode3 = 'GAB'
UPDATE dropdown.Country Set CountryName = 'Gambia (the)' WHERE ISOCountryCode3 = 'GMB'
UPDATE dropdown.Country Set CountryName = 'Georgia' WHERE ISOCountryCode3 = 'GEO'
UPDATE dropdown.Country Set CountryName = 'Germany' WHERE ISOCountryCode3 = 'DEU'
UPDATE dropdown.Country Set CountryName = 'Ghana' WHERE ISOCountryCode3 = 'GHA'
UPDATE dropdown.Country Set CountryName = 'Gibraltar' WHERE ISOCountryCode3 = 'GIB'
UPDATE dropdown.Country Set CountryName = 'Greece' WHERE ISOCountryCode3 = 'GRC'
UPDATE dropdown.Country Set CountryName = 'Greenland' WHERE ISOCountryCode3 = 'GRL'
UPDATE dropdown.Country Set CountryName = 'Grenada' WHERE ISOCountryCode3 = 'GRD'
UPDATE dropdown.Country Set CountryName = 'Guadeloupe' WHERE ISOCountryCode3 = 'GLP'
UPDATE dropdown.Country Set CountryName = 'Guam' WHERE ISOCountryCode3 = 'GUM'
UPDATE dropdown.Country Set CountryName = 'Guatemala' WHERE ISOCountryCode3 = 'GTM'
UPDATE dropdown.Country Set CountryName = 'Guernsey' WHERE ISOCountryCode3 = 'GGY'
UPDATE dropdown.Country Set CountryName = 'Guinea' WHERE ISOCountryCode3 = 'GIN'
UPDATE dropdown.Country Set CountryName = 'Guinea-Bissau' WHERE ISOCountryCode3 = 'GNB'
UPDATE dropdown.Country Set CountryName = 'Guyana' WHERE ISOCountryCode3 = 'GUY'
UPDATE dropdown.Country Set CountryName = 'Haiti' WHERE ISOCountryCode3 = 'HTI'
UPDATE dropdown.Country Set CountryName = 'Heard Island and McDonald Islands' WHERE ISOCountryCode3 = 'HMD'
UPDATE dropdown.Country Set CountryName = 'Holy See (the)' WHERE ISOCountryCode3 = 'VAT'
UPDATE dropdown.Country Set CountryName = 'Honduras' WHERE ISOCountryCode3 = 'HND'
UPDATE dropdown.Country Set CountryName = 'Hong Kong' WHERE ISOCountryCode3 = 'HKG'
UPDATE dropdown.Country Set CountryName = 'Hungary' WHERE ISOCountryCode3 = 'HUN'
UPDATE dropdown.Country Set CountryName = 'Iceland' WHERE ISOCountryCode3 = 'ISL'
UPDATE dropdown.Country Set CountryName = 'India' WHERE ISOCountryCode3 = 'IND'
UPDATE dropdown.Country Set CountryName = 'Indonesia' WHERE ISOCountryCode3 = 'IDN'
UPDATE dropdown.Country Set CountryName = 'Iran (Islamic Republic of)' WHERE ISOCountryCode3 = 'IRN'
UPDATE dropdown.Country Set CountryName = 'Iraq' WHERE ISOCountryCode3 = 'IRQ'
UPDATE dropdown.Country Set CountryName = 'Ireland' WHERE ISOCountryCode3 = 'IRL'
UPDATE dropdown.Country Set CountryName = 'Isle of Man' WHERE ISOCountryCode3 = 'IMN'
UPDATE dropdown.Country Set CountryName = 'Israel' WHERE ISOCountryCode3 = 'ISR'
UPDATE dropdown.Country Set CountryName = 'Italy' WHERE ISOCountryCode3 = 'ITA'
UPDATE dropdown.Country Set CountryName = 'Jamaica' WHERE ISOCountryCode3 = 'JAM'
UPDATE dropdown.Country Set CountryName = 'Japan' WHERE ISOCountryCode3 = 'JPN'
UPDATE dropdown.Country Set CountryName = 'Jersey' WHERE ISOCountryCode3 = 'JEY'
UPDATE dropdown.Country Set CountryName = 'Jordan' WHERE ISOCountryCode3 = 'JOR'
UPDATE dropdown.Country Set CountryName = 'Kazakhstan' WHERE ISOCountryCode3 = 'KAZ'
UPDATE dropdown.Country Set CountryName = 'Kenya' WHERE ISOCountryCode3 = 'KEN'
UPDATE dropdown.Country Set CountryName = 'Kiribati' WHERE ISOCountryCode3 = 'KIR'
UPDATE dropdown.Country Set CountryName = 'Korea (the Democratic People''s Republic of)' WHERE ISOCountryCode3 = 'PRK'
UPDATE dropdown.Country Set CountryName = 'Korea (the Republic of)' WHERE ISOCountryCode3 = 'KOR'
UPDATE dropdown.Country Set CountryName = 'Kuwait' WHERE ISOCountryCode3 = 'KWT'
UPDATE dropdown.Country Set CountryName = 'Kyrgyzstan' WHERE ISOCountryCode3 = 'KGZ'
UPDATE dropdown.Country Set CountryName = 'Lao People''s Democratic Republic (the)' WHERE ISOCountryCode3 = 'LAO'
UPDATE dropdown.Country Set CountryName = 'Latvia' WHERE ISOCountryCode3 = 'LVA'
UPDATE dropdown.Country Set CountryName = 'Lebanon' WHERE ISOCountryCode3 = 'LBN'
UPDATE dropdown.Country Set CountryName = 'Lesotho' WHERE ISOCountryCode3 = 'LSO'
UPDATE dropdown.Country Set CountryName = 'Liberia' WHERE ISOCountryCode3 = 'LBR'
UPDATE dropdown.Country Set CountryName = 'Libya' WHERE ISOCountryCode3 = 'LBY'
UPDATE dropdown.Country Set CountryName = 'Liechtenstein' WHERE ISOCountryCode3 = 'LIE'
UPDATE dropdown.Country Set CountryName = 'Lithuania' WHERE ISOCountryCode3 = 'LTU'
UPDATE dropdown.Country Set CountryName = 'Luxembourg' WHERE ISOCountryCode3 = 'LUX'
UPDATE dropdown.Country Set CountryName = 'Macao' WHERE ISOCountryCode3 = 'MAC'
UPDATE dropdown.Country Set CountryName = 'Madagascar' WHERE ISOCountryCode3 = 'MDG'
UPDATE dropdown.Country Set CountryName = 'Malawi' WHERE ISOCountryCode3 = 'MWI'
UPDATE dropdown.Country Set CountryName = 'Malaysia' WHERE ISOCountryCode3 = 'MYS'
UPDATE dropdown.Country Set CountryName = 'Maldives' WHERE ISOCountryCode3 = 'MDV'
UPDATE dropdown.Country Set CountryName = 'Mali' WHERE ISOCountryCode3 = 'MLI'
UPDATE dropdown.Country Set CountryName = 'Malta' WHERE ISOCountryCode3 = 'MLT'
UPDATE dropdown.Country Set CountryName = 'Marshall Islands (the)' WHERE ISOCountryCode3 = 'MHL'
UPDATE dropdown.Country Set CountryName = 'Martinique' WHERE ISOCountryCode3 = 'MTQ'
UPDATE dropdown.Country Set CountryName = 'Mauritania' WHERE ISOCountryCode3 = 'MRT'
UPDATE dropdown.Country Set CountryName = 'Mauritius' WHERE ISOCountryCode3 = 'MUS'
UPDATE dropdown.Country Set CountryName = 'Mayotte' WHERE ISOCountryCode3 = 'MYT'
UPDATE dropdown.Country Set CountryName = 'Mexico' WHERE ISOCountryCode3 = 'MEX'
UPDATE dropdown.Country Set CountryName = 'Micronesia (Federated States of)' WHERE ISOCountryCode3 = 'FSM'
UPDATE dropdown.Country Set CountryName = 'Moldova (the Republic of)' WHERE ISOCountryCode3 = 'MDA'
UPDATE dropdown.Country Set CountryName = 'Monaco' WHERE ISOCountryCode3 = 'MCO'
UPDATE dropdown.Country Set CountryName = 'Mongolia' WHERE ISOCountryCode3 = 'MNG'
UPDATE dropdown.Country Set CountryName = 'Montenegro' WHERE ISOCountryCode3 = 'MNE'
UPDATE dropdown.Country Set CountryName = 'Montserrat' WHERE ISOCountryCode3 = 'MSR'
UPDATE dropdown.Country Set CountryName = 'Morocco' WHERE ISOCountryCode3 = 'MAR'
UPDATE dropdown.Country Set CountryName = 'Mozambique' WHERE ISOCountryCode3 = 'MOZ'
UPDATE dropdown.Country Set CountryName = 'Myanmar' WHERE ISOCountryCode3 = 'MMR'
UPDATE dropdown.Country Set CountryName = 'Namibia' WHERE ISOCountryCode3 = 'NAM'
UPDATE dropdown.Country Set CountryName = 'Nauru' WHERE ISOCountryCode3 = 'NRU'
UPDATE dropdown.Country Set CountryName = 'Nepal' WHERE ISOCountryCode3 = 'NPL'
UPDATE dropdown.Country Set CountryName = 'Netherlands (the)' WHERE ISOCountryCode3 = 'NLD'
UPDATE dropdown.Country Set CountryName = 'New Caledonia' WHERE ISOCountryCode3 = 'NCL'
UPDATE dropdown.Country Set CountryName = 'New Zealand' WHERE ISOCountryCode3 = 'NZL'
UPDATE dropdown.Country Set CountryName = 'Nicaragua' WHERE ISOCountryCode3 = 'NIC'
UPDATE dropdown.Country Set CountryName = 'Niger (the)' WHERE ISOCountryCode3 = 'NER'
UPDATE dropdown.Country Set CountryName = 'Nigeria' WHERE ISOCountryCode3 = 'NGA'
UPDATE dropdown.Country Set CountryName = 'Niue' WHERE ISOCountryCode3 = 'NIU'
UPDATE dropdown.Country Set CountryName = 'Norfolk Island' WHERE ISOCountryCode3 = 'NFK'
UPDATE dropdown.Country Set CountryName = 'North Macedonia' WHERE ISOCountryCode3 = 'MKD'
UPDATE dropdown.Country Set CountryName = 'Northern Mariana Islands (the)' WHERE ISOCountryCode3 = 'MNP'
UPDATE dropdown.Country Set CountryName = 'Norway' WHERE ISOCountryCode3 = 'NOR'
UPDATE dropdown.Country Set CountryName = 'Oman' WHERE ISOCountryCode3 = 'OMN'
UPDATE dropdown.Country Set CountryName = 'Pakistan' WHERE ISOCountryCode3 = 'PAK'
UPDATE dropdown.Country Set CountryName = 'Palau' WHERE ISOCountryCode3 = 'PLW'
UPDATE dropdown.Country Set CountryName = 'Palestine, State of' WHERE ISOCountryCode3 = 'PSE'
UPDATE dropdown.Country Set CountryName = 'Panama' WHERE ISOCountryCode3 = 'PAN'
UPDATE dropdown.Country Set CountryName = 'Papua New Guinea' WHERE ISOCountryCode3 = 'PNG'
UPDATE dropdown.Country Set CountryName = 'Paraguay' WHERE ISOCountryCode3 = 'PRY'
UPDATE dropdown.Country Set CountryName = 'Peru' WHERE ISOCountryCode3 = 'PER'
UPDATE dropdown.Country Set CountryName = 'Philippines (the)' WHERE ISOCountryCode3 = 'PHL'
UPDATE dropdown.Country Set CountryName = 'Pitcairn' WHERE ISOCountryCode3 = 'PCN'
UPDATE dropdown.Country Set CountryName = 'Poland' WHERE ISOCountryCode3 = 'POL'
UPDATE dropdown.Country Set CountryName = 'Portugal' WHERE ISOCountryCode3 = 'PRT'
UPDATE dropdown.Country Set CountryName = 'Puerto Rico' WHERE ISOCountryCode3 = 'PRI'
UPDATE dropdown.Country Set CountryName = 'Qatar' WHERE ISOCountryCode3 = 'QAT'
UPDATE dropdown.Country Set CountryName = 'RÃ©union' WHERE ISOCountryCode3 = 'REU'
UPDATE dropdown.Country Set CountryName = 'Romania' WHERE ISOCountryCode3 = 'ROU'
UPDATE dropdown.Country Set CountryName = 'Russian Federation (the)' WHERE ISOCountryCode3 = 'RUS'
UPDATE dropdown.Country Set CountryName = 'Rwanda' WHERE ISOCountryCode3 = 'RWA'
UPDATE dropdown.Country Set CountryName = 'Saint BarthÃ©lemy' WHERE ISOCountryCode3 = 'BLM'
UPDATE dropdown.Country Set CountryName = 'Saint Helena, Ascension and Tristan da Cunha' WHERE ISOCountryCode3 = 'SHN'
UPDATE dropdown.Country Set CountryName = 'Saint Kitts and Nevis' WHERE ISOCountryCode3 = 'KNA'
UPDATE dropdown.Country Set CountryName = 'Saint Lucia' WHERE ISOCountryCode3 = 'LCA'
UPDATE dropdown.Country Set CountryName = 'Saint Martin (French part)' WHERE ISOCountryCode3 = 'MAF'
UPDATE dropdown.Country Set CountryName = 'Saint Pierre and Miquelon' WHERE ISOCountryCode3 = 'SPM'
UPDATE dropdown.Country Set CountryName = 'Saint Vincent and the Grenadines' WHERE ISOCountryCode3 = 'VCT'
UPDATE dropdown.Country Set CountryName = 'Samoa' WHERE ISOCountryCode3 = 'WSM'
UPDATE dropdown.Country Set CountryName = 'San Marino' WHERE ISOCountryCode3 = 'SMR'
UPDATE dropdown.Country Set CountryName = 'Sao Tome and Principe' WHERE ISOCountryCode3 = 'STP'
UPDATE dropdown.Country Set CountryName = 'Saudi Arabia' WHERE ISOCountryCode3 = 'SAU'
UPDATE dropdown.Country Set CountryName = 'Senegal' WHERE ISOCountryCode3 = 'SEN'
UPDATE dropdown.Country Set CountryName = 'Serbia' WHERE ISOCountryCode3 = 'SRB'
UPDATE dropdown.Country Set CountryName = 'Seychelles' WHERE ISOCountryCode3 = 'SYC'
UPDATE dropdown.Country Set CountryName = 'Sierra Leone' WHERE ISOCountryCode3 = 'SLE'
UPDATE dropdown.Country Set CountryName = 'Singapore' WHERE ISOCountryCode3 = 'SGP'
UPDATE dropdown.Country Set CountryName = 'Sint Maarten (Dutch part)' WHERE ISOCountryCode3 = 'SXM'
UPDATE dropdown.Country Set CountryName = 'Slovakia' WHERE ISOCountryCode3 = 'SVK'
UPDATE dropdown.Country Set CountryName = 'Slovenia' WHERE ISOCountryCode3 = 'SVN'
UPDATE dropdown.Country Set CountryName = 'Solomon Islands' WHERE ISOCountryCode3 = 'SLB'
UPDATE dropdown.Country Set CountryName = 'Somalia' WHERE ISOCountryCode3 = 'SOM'
UPDATE dropdown.Country Set CountryName = 'South Africa' WHERE ISOCountryCode3 = 'ZAF'
UPDATE dropdown.Country Set CountryName = 'South Georgia and the South Sandwich Islands' WHERE ISOCountryCode3 = 'SGS'
UPDATE dropdown.Country Set CountryName = 'South Sudan' WHERE ISOCountryCode3 = 'SSD'
UPDATE dropdown.Country Set CountryName = 'Spain' WHERE ISOCountryCode3 = 'ESP'
UPDATE dropdown.Country Set CountryName = 'Sri Lanka' WHERE ISOCountryCode3 = 'LKA'
UPDATE dropdown.Country Set CountryName = 'Sudan (the)' WHERE ISOCountryCode3 = 'SDN'
UPDATE dropdown.Country Set CountryName = 'Suriname' WHERE ISOCountryCode3 = 'SUR'
UPDATE dropdown.Country Set CountryName = 'Svalbard and Jan Mayen' WHERE ISOCountryCode3 = 'SJM'
UPDATE dropdown.Country Set CountryName = 'Sweden' WHERE ISOCountryCode3 = 'SWE'
UPDATE dropdown.Country Set CountryName = 'Switzerland' WHERE ISOCountryCode3 = 'CHE'
UPDATE dropdown.Country Set CountryName = 'Syrian Arab Republic (the)' WHERE ISOCountryCode3 = 'SYR'
UPDATE dropdown.Country Set CountryName = 'Taiwan (Province of China)' WHERE ISOCountryCode3 = 'TWN'
UPDATE dropdown.Country Set CountryName = 'Tajikistan' WHERE ISOCountryCode3 = 'TJK'
UPDATE dropdown.Country Set CountryName = 'Tanzania, the United Republic of' WHERE ISOCountryCode3 = 'TZA'
UPDATE dropdown.Country Set CountryName = 'Thailand' WHERE ISOCountryCode3 = 'THA'
UPDATE dropdown.Country Set CountryName = 'Timor-Leste' WHERE ISOCountryCode3 = 'TLS'
UPDATE dropdown.Country Set CountryName = 'Togo' WHERE ISOCountryCode3 = 'TGO'
UPDATE dropdown.Country Set CountryName = 'Tokelau' WHERE ISOCountryCode3 = 'TKL'
UPDATE dropdown.Country Set CountryName = 'Tonga' WHERE ISOCountryCode3 = 'TON'
UPDATE dropdown.Country Set CountryName = 'Trinidad and Tobago' WHERE ISOCountryCode3 = 'TTO'
UPDATE dropdown.Country Set CountryName = 'Tunisia' WHERE ISOCountryCode3 = 'TUN'
UPDATE dropdown.Country Set CountryName = 'Turkey' WHERE ISOCountryCode3 = 'TUR'
UPDATE dropdown.Country Set CountryName = 'Turkmenistan' WHERE ISOCountryCode3 = 'TKM'
UPDATE dropdown.Country Set CountryName = 'Turks and Caicos Islands (the)' WHERE ISOCountryCode3 = 'TCA'
UPDATE dropdown.Country Set CountryName = 'Tuvalu' WHERE ISOCountryCode3 = 'TUV'
UPDATE dropdown.Country Set CountryName = 'Uganda' WHERE ISOCountryCode3 = 'UGA'
UPDATE dropdown.Country Set CountryName = 'Ukraine' WHERE ISOCountryCode3 = 'UKR'
UPDATE dropdown.Country Set CountryName = 'United Arab Emirates (the)' WHERE ISOCountryCode3 = 'ARE'
UPDATE dropdown.Country Set CountryName = 'United Kingdom of Great Britain and Northern Ireland (the)' WHERE ISOCountryCode3 = 'GBR'
UPDATE dropdown.Country Set CountryName = 'United States Minor Outlying Islands (the)' WHERE ISOCountryCode3 = 'UMI'
UPDATE dropdown.Country Set CountryName = 'United States of America (the)' WHERE ISOCountryCode3 = 'USA'
UPDATE dropdown.Country Set CountryName = 'Uruguay' WHERE ISOCountryCode3 = 'URY'
UPDATE dropdown.Country Set CountryName = 'Uzbekistan' WHERE ISOCountryCode3 = 'UZB'
UPDATE dropdown.Country Set CountryName = 'Vanuatu' WHERE ISOCountryCode3 = 'VUT'
UPDATE dropdown.Country Set CountryName = 'Venezuela (Bolivarian Republic of)' WHERE ISOCountryCode3 = 'VEN'
UPDATE dropdown.Country Set CountryName = 'Viet Nam' WHERE ISOCountryCode3 = 'VNM'
UPDATE dropdown.Country Set CountryName = 'Virgin Islands (British)' WHERE ISOCountryCode3 = 'VGB'
UPDATE dropdown.Country Set CountryName = 'Virgin Islands (U.S.)' WHERE ISOCountryCode3 = 'VIR'
UPDATE dropdown.Country Set CountryName = 'Wallis and Futuna' WHERE ISOCountryCode3 = 'WLF'
UPDATE dropdown.Country Set CountryName = 'Western Sahara*' WHERE ISOCountryCode3 = 'ESH'
UPDATE dropdown.Country Set CountryName = 'Yemen' WHERE ISOCountryCode3 = 'YEM'
UPDATE dropdown.Country Set CountryName = 'Zambia' WHERE ISOCountryCode3 = 'ZMB'
UPDATE dropdown.Country Set CountryName = 'Zimbabwe' WHERE ISOCountryCode3 = 'ZWE'
GO

UPDATE C
SET C.DisplayOrder = 100
FROM dropdown.Country C
WHERE C.ISOCountryCode2 = 'DE'
GO
--End table dropdown.Country

--Begin table dropdown.EmploymentType
TRUNCATE TABLE dropdown.EmploymentType
GO

EXEC utility.InsertIdentityValue 'dropdown.EmploymentType', 'EmploymentTypeID', 0
GO

INSERT INTO dropdown.EmploymentType 
	(EmploymentTypeName, EmploymentTypeCode, DisplayOrder)
VALUES
	('CSG', 'CSG', 1),
	('Senior Adviser', 'SA', 2),
	('Core Staff', 'CS', 3),
	('Serving Police', 'SP', 4),
	('FCO Contract', 'FCO', 5)
GO
--End table dropdown.EmploymentType

--Begin table dropdown.ImplementationDifficulty
TRUNCATE TABLE dropdown.ImplementationDifficulty
GO

EXEC utility.InsertIdentityValue 'dropdown.ImplementationDifficulty', 'ImplementationDifficultyID', 0
GO

INSERT INTO dropdown.ImplementationDifficulty 
	(ImplementationDifficultyName, ImplementationDifficultyCode, DisplayOrder)
VALUES
	('Low', 'Low', 1),
	('Medium', 'Medium', 2),
	('High', 'High', 3)
GO
--End table dropdown.ImplementationDifficulty

--Begin table dropdown.ImplementationImpact
TRUNCATE TABLE dropdown.ImplementationImpact
GO

EXEC utility.InsertIdentityValue 'dropdown.ImplementationImpact', 'ImplementationImpactID', 0
GO

INSERT INTO dropdown.ImplementationImpact 
	(ImplementationImpactName, ImplementationImpactCode, DisplayOrder)
VALUES
	('Low', 'Low', 1),
	('Medium', 'Medium', 2),
	('High', 'High', 3)
GO
--End table dropdown.ImplementationImpact

--Begin table dropdown.LessonCategory
TRUNCATE TABLE dropdown.LessonCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonCategory', 'LessonCategoryID', 0
GO

INSERT INTO dropdown.LessonCategory 
	(LessonCategoryName, LessonCategoryCode, DisplayOrder)
VALUES
	('Staffing', 'Staffing', 1),
	('Coordination', 'Coordination', 2),
	('Partners', 'Partners', 3),
	('Leadership', 'Leadership', 4),
	('Preparedness', 'Preparedness', 5)
GO
--End table dropdown.LessonCategory

--Begin table dropdown.LessonOwner
TRUNCATE TABLE dropdown.LessonOwner
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonOwner', 'LessonOwnerID', 0
GO

INSERT INTO dropdown.LessonOwner 
	(LessonOwnerName, LessonOwnerCode)
VALUES
	('Advisory', 'Advisory'),
	('HR', 'HR'),
	('HRG', 'HRG'),
	('HSOT Senior Management', 'HSOTSeniorManagement'),
	('HSOT', 'HSOT'),
	('P&L', 'P&L'),
	('Readiness & Response', 'Readiness&Response')
GO
--End table dropdown.LessonOwner

--Begin table dropdown.LessonPublicationStatus
TRUNCATE TABLE dropdown.LessonPublicationStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonPublicationStatus', 'LessonPublicationStatusID', 0
GO

INSERT INTO dropdown.LessonPublicationStatus 
	(LessonPublicationStatusName, LessonPublicationStatusCode, DisplayOrder)
VALUES
	('For publication', 'LP', 1),
	('Not for publication', 'LUP', 2)
GO

UPDATE LPS
SET 
	LPS.LessonPublicationStatusName = 'Pending initial review',
	LPS.LessonPublicationStatusCode = 'L'
FROM dropdown.LessonPublicationStatus LPS
WHERE LPS.LessonPublicationStatusID = 0
GO
--End table dropdown.LessonPublicationStatus

--Begin table dropdown.LessonStatus
TRUNCATE TABLE dropdown.LessonStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonStatus', 'LessonStatusID', 0
GO

INSERT INTO dropdown.LessonStatus 
	(LessonStatusName, LessonStatusCode, DisplayOrder)
VALUES
	('Assigned', 'Assigned', 1),
	('Progressing', 'Progressing', 2),
	('Implemented', 'Implemented', 3),
	('Best Practice', 'BestPractice', 4)
GO
--End table dropdown.LessonStatus

--Begin table dropdown.LessonType
TRUNCATE TABLE dropdown.LessonType
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonType', 'LessonTypeID', 0
GO

INSERT INTO dropdown.LessonType 
	(LessonTypeName, LessonTypeCode, DisplayOrder)
VALUES
	('Project', 'Project', 1),
	('Programme', 'Program', 2),
	('Project and Programme', 'ProjectProgram', 3)
GO
--End table dropdown.LessonType

--Begin table dropdown.LessonUserClass
TRUNCATE TABLE dropdown.LessonUserClass
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonUserClass', 'LessonUserClassID', 0
GO

INSERT INTO dropdown.LessonUserClass 
	(LessonUserClassName, LessonUserClassCode)
VALUES
	('Advisory', 'Advisory'),
	('HR', 'HR'),
	('HRG', 'HRG'),
	('HSOT Senior Management', 'HSOTSeniorManagement'),
	('HSOT', 'HSOT'),
	('P&L', 'P&L'),
	('Readiness & Response', 'Readiness&Response')
GO
--End table dropdown.LessonUserClass

--Begin table dropdown.MedicalClearanceReviewType
TRUNCATE TABLE dropdown.MedicalClearanceReviewType
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceReviewType', 'MedicalClearanceReviewTypeID', 0
GO

INSERT INTO dropdown.MedicalClearanceReviewType 
	(MedicalClearanceReviewTypeName, MedicalClearanceReviewTypeCode, DisplayOrder)
VALUES
	('Initial', 'Initial', 1),
	('Followup', 'Followup', 2)
GO
--End table dropdown.MedicalClearanceReviewType

--Begin table dropdown.PersonType
IF NOT EXISTS (SELECT 1 FROM dropdown.PersonType PT WHERE PT.PersonTypeCode = 'Client')
	BEGIN

	INSERT INTO dropdown.PersonType 
		(PersonTypeName, PersonTypeCode, DisplayOrder, IsActive)
	VALUES
		('Client', 'Client', 3, 0)

	END
--ENDIF
GO
--End table dropdown.PersonType

--Begin table dropdown.ProjectLaborCode
TRUNCATE TABLE dropdown.ProjectLaborCode
GO

EXEC utility.InsertIdentityValue 'dropdown.ProjectLaborCode', 'ProjectLaborCodeID', 0
GO

INSERT INTO dropdown.ProjectLaborCode 
	(ProjectLaborCodeName, ProjectLaborCodeCode, DisplayOrder)
VALUES
	('Project Labour Code 1', 'Code1', 1),
	('Project Labour COde 2', 'Code 2', 2)
GO
--End table dropdown.ProjectLaborCode

--Begin table dropdown.PublishTo
TRUNCATE TABLE dropdown.PublishTo
GO

EXEC utility.InsertIdentityValue 'dropdown.PublishTo', 'PublishToID', 0
GO

INSERT INTO dropdown.PublishTo 
	(PublishToName, PublishToCode, DisplayOrder)
VALUES
	('CSG Core','CSGCore',1),
	('CSG Senior','CSGSenior',2),
	('Category','Category',3),
	('SubCategory','SubCategory',4)
GO
--End table dropdown.PublishTo

--Begin table dropdown.RecruitmentPathway
TRUNCATE TABLE dropdown.RecruitmentPathway
GO

EXEC utility.InsertIdentityValue 'dropdown.RecruitmentPathway', 'RecruitmentPathwayID', 0
GO

INSERT INTO dropdown.RecruitmentPathway 
	(RecruitmentPathwayName, RecruitmentPathwayCode, DisplayOrder)
VALUES
	('Direct', 'Direct', 1),
	('Competitive', 'Competitive', 2)
GO
--End table dropdown.RecruitmentPathway

--Begin table dropdown.RegionalPriorityTier
TRUNCATE TABLE dropdown.RegionalPriorityTier
GO

EXEC utility.InsertIdentityValue 'dropdown.RegionalPriorityTier', 'RegionalPriorityTierID', 0
GO

INSERT INTO dropdown.RegionalPriorityTier 
	(RegionalPriorityTierName, RegionalPriorityTierCode, DisplayOrder)
VALUES
	('Tier 0', 'Tier0', 1),
	('Tier 1', 'Tier1', 2),
	('Tier 2', 'Tier2', 3),
	('Tier 3', 'Tier3', 4),
	('N/A', 'NA', 5)
GO
--End table dropdown.RegionalPriorityTier

--Begin table dropdown.RoleCategory
TRUNCATE TABLE dropdown.RoleCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.RoleCategory', 'RoleCategoryID', 0
GO

INSERT INTO dropdown.RoleCategory 
	(RoleCategoryName, RoleCategoryCode, DisplayOrder)
VALUES
	('Community Safety, Security & Access to Justice','CSSAJ',1),
	('Conflict','Conflict',2),
	('Defence','Defence',3),
	('Extremism, Violent Extremism and Terrorism (EVET)','EVET',4),
	('Gender, Conflict and Stability','GCS',5),
	('Governance','Governance',6),
	('Integrated Border Management','IBM',7),
	('Justice','Justice',8),
	('Monitoring and Evaluation (M&E)','M&E',9),
	('Multilateral','Multilateral',10),
	('Government Partnerships International (GPI)','GPI',11),
	('Operations','Operations',12),
	('Organised Crime','OC',13),
	('Policing Adviser','PA',14),
	('PSVI Criminal Lawyers','CL',15),
	('PSVI Gender-Based Violence Experts','GBVE',16),
	('PSVI International Investigating Officers','IIO',17),
	('PSVI Psychosocial Experts and Social Workers','PESW',18),
	('PSVI Sexual Offences Examiners','SOE',19),
	('PSVI Training Experts','TE',20),
	('Security Institutions','SI',21),
	('SMS','SMS',22),
	('Stabilisation','Stabilisation',23),
	('Strategic Communications','SC',24),
	('Serving Police','SP',25)
GO
--End table dropdown.RoleCategory

--Begin table dropdown.RoleSubCategory
TRUNCATE TABLE dropdown.RoleSubCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.RoleSubCategory', 'RoleSubCategoryID', 0
GO

INSERT INTO dropdown.RoleSubCategory 
	(RoleSubCategoryName, RoleSubCategoryCode, DisplayOrder)
VALUES
	('Community Safety, Security & Access to Justice','CSSAJ',1),
	('Conflict','Conflict',2),
	('Defence','Defence',3),
	('Extremism, Violent Extremism and Terrorism (EVET)','EVET',4),
	('Gender, Conflict and Stability','GCS',5),
	('Governance','Governance',6),
	('Integrated Border Management','IBM',7),
	('Justice','Justice',8),
	('Monitoring and Evaluation (M&E)','M&E',9),
	('Multilateral','Multilateral',10),
	('Government Partnerships International (GPI)','GPI',11),
	('Operations','Operations',12),
	('Organised Crime','OC',13),
	('Policing Adviser','PA',14),
	('PSVI Criminal Lawyers','CL',15),
	('PSVI Gender-Based Violence Experts','GBVE',16),
	('PSVI International Investigating Officers','IIO',17),
	('PSVI Psychosocial Experts and Social Workers','PESW',18),
	('PSVI Sexual Offences Examiners','SOE',19),
	('PSVI Training Experts','TE',20),
	('Security Institutions','SI',21),
	('SMS','SMS',22),
	('Stabilisation','Stabilisation',23),
	('Strategic Communications','SC',24),
	('Serving Police','SP',25)
GO
--End table dropdown.RoleSubCategory

--Begin table dropdown.SecurityClearanceProcessStep
IF NOT EXISTS (SELECT 1 FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = 'PREPFOLLOWUP')
	BEGIN

	UPDATE SCPS
	SET SCPS.DisplayOrder = SCPS.DisplayOrder + 1
	FROM dropdown.SecurityClearanceProcessStep SCPS
	WHERE SCPS.DisplayOrder > 1

	INSERT INTO dropdown.SecurityClearanceProcessStep 
		(SecurityClearanceProcessStepName, SecurityClearanceProcessStepCode, DisplayOrder)
	VALUES
		('Application in Preperation - Follow Up', 'PREPFOLLOWUP', 2)

	END
--ENDIF
GO
--End table dropdown.SecurityClearanceProcessStep

--Begin table dropdown.SecurityClearanceReviewType
TRUNCATE TABLE dropdown.SecurityClearanceReviewType
GO

EXEC utility.InsertIdentityValue 'dropdown.SecurityClearanceReviewType', 'SecurityClearanceReviewTypeID', 0
GO

INSERT INTO dropdown.SecurityClearanceReviewType 
	(SecurityClearanceReviewTypeName, SecurityClearanceReviewTypeCode, DisplayOrder)
VALUES
	('Initial', 'Initial', 1),
	('Initial - Followup', 'InitialFollowup', 2),
	('Discretionary', 'Discretionary', 3),
	('Discretionary - Followup', 'DiscretionaryFollowup', 4)
GO
--End table dropdown.SecurityClearanceReviewType

--Begin table dropdown.TaskType
TRUNCATE TABLE dropdown.TaskType
GO

EXEC utility.InsertIdentityValue 'dropdown.TaskType', 'TaskTypeID', 0
GO

INSERT INTO dropdown.TaskType 
	(TaskTypeName, TaskTypeCode, DisplayOrder)
VALUES
	('Analysis','Analysis',1),
	('Annual Review','AnnualReview',2),
	('Commissioned Lessons Review','CommissionedLessonsReview',3),
	('JACS','JACS',4),
	('Ongoing Engagement','OngoingEngagement',5),
	('Programme Inputs: Scope','Scope',6),
	('Programme Inputs: Design','Design',7),
	('Programme Inputs: Review','Review',8),
	('Sector Assessments','SectorAssessments',9),
	('SOCJA','SOCJA',10),
	('Strategy Development Review','StrategyDevelopmentReview',11)
GO
--End table dropdown.TaskType

--Begin table dropdown.ThematicFocus
TRUNCATE TABLE dropdown.ThematicFocus
GO

EXEC utility.InsertIdentityValue 'dropdown.ThematicFocus', 'ThematicFocusID', 0
GO

INSERT INTO dropdown.ThematicFocus 
	(ThematicFocusName, ThematicFocusCode, DisplayOrder)
VALUES
	('CT', 'CT', 1),
	('CVE', 'CVE', 2),
	('Conflict Sensitivity', 'ConflictSensitivity', 3),
	('Gender', 'Gender', 4),
	('Lessons', 'Lessons', 5),
	('M&E', 'M&E', 6),
	('Migration', 'Migration', 7),
	('Modern Slavery', 'ModernSlavery', 8),
	('International Policing', 'InternationalPolicing', 9),
	('Political Deals', 'PoliticalDeals', 10),
	('Programme Inputs', 'ProgrammeInputs', 11),
	('Security and Justice', 'SecurityJustice', 12),
	('SOC', 'SOC', 13),
	('Stabilisation', 'Stabilisation', 14),
	('Other', 'Other', 15)
GO
--End table dropdown.ThematicFocus

--Begin table permissionable.Permissionable

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='Add / edit feedback', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='FeedbackAddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.FeedbackAddUpdate', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='View feedback', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='FeedbackView', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.FeedbackView', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='Add / edit a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ClientAddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.VacancyAddUpdate', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Client', 
	@DESCRIPTION='View a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Client.VacancyView', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='Add / edit a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ClientVacancy.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='View the list of client vacancies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ClientVacancy.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ClientVacancy', 
	@DESCRIPTION='View a vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ClientVacancy.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='Add / edit a MEAL record', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='View the list of MEAL records', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='Export the list of MEAL records', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.List.Export', 
	@PERMISSIONCODE='Export';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Lesson', 
	@DESCRIPTION='View a MEAL record', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Lesson.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Invite a client into the system', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='Invite', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Person.Invite', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Send an invite to a user', 
	@DISPLAYORDER=0, 
	@ISACTIVE=1, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='SendInvite', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.SendInvite', 
	@PERMISSIONCODE=NULL;

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='Save application management', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ManageApplicationSave', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.ManageApplicationSave', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='View application management', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ManageApplicationView', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.ManageApplicationView', 
	@PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.AddColumn @TableName, 'PermissionableTemplateCode', 'VARCHAR(50)'
GO
--End table permissionable.PermissionableTemplate

--Begin table person.PersonGroup
IF NOT EXISTS (SELECT 1 FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'Interview Panel Member - Scheduler')
	BEGIN

	INSERT INTO person.PersonGroup 
		(PersonGroupName, PersonGroupDescription, ApprovalAuthorityLevelID) 
	VALUES 
		--('CSG Coordinator', 'Coordinates CSG''s', 1),
		('CSG Manager', 'Manages CSGs', 2),
		('CSG Technical Manager', 'Technically, Manages CSGs', 2),
		('Interview Panel', 'This group is managed programmatically from the application workflow', 2),
		('Interview Panel Member - Scheduler', 'This group is managed programmatically from the application workflow', 2)

	END
--ENDIF
GO
--End table person.PersonGroup

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.WorkflowName = 'Application')
	BEGIN

	DECLARE @tOutput1 TABLE (WorkflowID INT NOT NULL PRIMARY KEY)
	DECLARE @tOutput2 TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepNumber INT)
	DECLARE @tOutput3 TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupName VARCHAR(250))

	DECLARE @nWorkflowID INT

	INSERT INTO workflow.Workflow 
		(WorkflowName, EntityTypeCode, IsActive, ProjectID, WorkflowCompleteStatusName, IsSUperAdminOnly)
	OUTPUT INSERTED.WorkflowID INTO @tOutput1
	SELECT
		'Application', 
		'Application', 
		1, 
		P.ProjectID, 
		'Workflow Complete', 
		1
	FROM dropdown.Project P
	WHERE P.ProjectName = 'SU'

	SELECT @nWorkflowID = O.WorkflowID FROM @tOutput1 O

	INSERT INTO workflow.WorkflowStep 
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStepStatusName) 
	OUTPUT INSERTED.WorkflowStepID, INSERTED.WorkflowStepNumber INTO @tOutput2
	VALUES 
		(@nWorkflowID, 1, 'Preparing for Competency Review', 'Application Submitted'),
		(@nWorkflowID, 2, 'Competency Review', 'Competency Review'),
		(@nWorkflowID, 3, 'Technical Review', 'Technical Review'),
		(@nWorkflowID, 4, 'Coordinate Interview', 'Ready for Interview'),
		(@nWorkflowID, 5, 'Propose Dates', 'Scheduling'),
		(@nWorkflowID, 6, 'Confirm Dates', 'Pending Date Confirmation'),
		(@nWorkflowID, 7, 'Notify Interviewee', 'Contacting Reviewee for Interview'),
		(@nWorkflowID, 8, 'Interview Scheduled', 'Interview Pending'),
		(@nWorkflowID, 9, 'Interview Complete', 'Interview Complete')

	INSERT INTO workflow.WorkflowStepGroup 
		(WorkflowStepID, WorkflowStepGroupName, IsFinancialApprovalRequired) 
	OUTPUT INSERTED.WorkflowStepGroupID, INSERTED.WorkflowStepGroupName INTO @tOutput3
	SELECT
		O.WorkflowStepID,
		'Application Workflow Step Group ' + CAST(O.WorkflowStepNumber AS VARCHAR(5)),
		0
	FROM @tOutput2 O

	INSERT INTO workflow.WorkflowStepGroupPersonGroup 
		(WorkflowStepGroupID, PersonGroupID) 
	SELECT
		O.WorkflowStepGroupID,
		
		CASE
			WHEN O.WorkflowStepGroupName IN ('Application Workflow Step Group 1', 'Application Workflow Step Group 4', 'Application Workflow Step Group 7', 'Application Workflow Step Group 9')
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'CSG Coordinator')
			WHEN O.WorkflowStepGroupName = 'Application Workflow Step Group 2'
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'CSG Manager')
			WHEN O.WorkflowStepGroupName = 'Application Workflow Step Group 3'
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'CSG Technical Manager')
			WHEN O.WorkflowStepGroupName = 'Application Workflow Step Group 5'
			THEN (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'Interview Panel Member - Scheduler')
			ELSE (SELECT PG.PersonGroupID FROM person.PersonGroup PG WHERE PG.PersonGroupName = 'Interview Panel')
		END

	FROM @tOutput3 O

	END
--ENDIF
GO
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 1;
EXEC permissionable.SavePermissionableGroup 'Document', 'Documents', 2;
EXEC permissionable.SavePermissionableGroup 'ForceAsset', 'Organizations & Facilities', 3;
EXEC permissionable.SavePermissionableGroup 'Insight', 'Incidents & Reports', 4;
EXEC permissionable.SavePermissionableGroup 'Activity', 'Activity', 5;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 6;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='Add / edit a roster member feedback item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='View the list of roster member feedback items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='Add an roster member feedback item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.List.AddFeedback', @PERMISSIONCODE='AddFeedback';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='View a roster member feedback item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Lesson', @DESCRIPTION='Add / edit a MEAL record', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Lesson.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Lesson', @DESCRIPTION='View the list of MEAL records', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Lesson.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Lesson', @DESCRIPTION='Export the list of MEAL records', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Lesson.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Lesson', @DESCRIPTION='View a MEAL record', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Lesson.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the CTM Travel Approval menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CTMTravelApproval', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.CTMTravelApproval', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the DeployAdviser menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='DeployAdviser', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.DeployAdviser', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Compliance Checker menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumComplianceChecker', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumComplianceChecker', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Staff Time Collection menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumStaffTimeCollection', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumStaffTimeCollection', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the firearms training data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyFirearmsTraining', @PERMISSIONCODE='ModifyFirearmsTraining';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the IR35 data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyIR35', @PERMISSIONCODE='ModifyIR35';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the operational training data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyOperationalTraining', @PERMISSIONCODE='ModifyOperationalTraining';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the account tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the activitystreams tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the address tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the application tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the clearances tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the engagement tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the expertise tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the permissions tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the personal tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the qualification tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the medical clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='MedicalVettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.MedicalVettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send bulk emails from the medical clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='MedicalVettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.MedicalVettingList.BullkEmail', @PERMISSIONCODE='BullkEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send an invite to a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SendInvite', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.SendInvite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the security clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.VettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send bulk emails from the security clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.VettingList.BullkEmail', @PERMISSIONCODE='BullkEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the account tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the activitystreams tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the address tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the application tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the clearances tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the engagement tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the expertise tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the permissions tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the personal tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the qualification tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ResponseDashboard', @DESCRIPTION='Add / edit a response dashboard', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ResponseDashboard.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ResponseDashboard', @DESCRIPTION='View list of response dashboards', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ResponseDashboard.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ResponseDashboard', @DESCRIPTION='View a response dashboard', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ResponseDashboard.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VacancyApplication', @DESCRIPTION='Add / edit an application', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='VacancyApplication.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VacancyApplication', @DESCRIPTION='View the list of applications', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='VacancyApplication.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VacancyApplication', @DESCRIPTION='View an application', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='VacancyApplication.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - images in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Images', @PERMISSIONCODE='Images';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - media in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - other in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - reports in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Reports', @PERMISSIONCODE='Reports';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - images in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Images', @PERMISSIONCODE='Images';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - media in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - other in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - reports in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Reports', @PERMISSIONCODE='Reports';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of Facilities', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of Organisations', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Add / edit a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Allows access to response report exports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View the list of response reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Share an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.ShareIncident', @PERMISSIONCODE='ShareIncident';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='Add / edit a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View the list of Report Updates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Situational report pdf export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Situational report aggregator export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalogue list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of consignments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of dispatched items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDispatchedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDispatchedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of distributed items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDistributedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the consignment contents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='Add / edit an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment inventory list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='InventoryList', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.InventoryList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment item list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='Add / edit an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View the list of Equipment Movements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='Add / edit an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View the equipment order list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Client Add / Edit a Client Vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ClientAddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Client.ClientAddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Add / edit a vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ClientAddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Client.ClientAddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Add / edit feedback', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FeedbackAddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Client.FeedbackAddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View feedback', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FeedbackView', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Client.FeedbackView', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='Client View a Client Vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Client', @DESCRIPTION='View a vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Client.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientVacancy', @DESCRIPTION='Add / edit a vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ClientVacancy.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientVacancy', @DESCRIPTION='Manage Sift Matric', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ApplicationList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ClientVacancy.ApplicationList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientVacancy', @DESCRIPTION='Client Add / Edit a ClientVacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ClientAddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ClientVacancy.ClientAddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientVacancy', @DESCRIPTION='View the list of client vacancies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ClientVacancy.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientVacancy', @DESCRIPTION='View a vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ClientVacancy.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ClientVacancy', @DESCRIPTION='View a vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ClientVacancy.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Competency', @DESCRIPTION='Add and update competencies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Competency.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Competency', @DESCRIPTION='View the list of competencies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Competency.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Competency', @DESCRIPTION='View competencies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Competency.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PendingEmail', @DESCRIPTION='View the sent and pending mail log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PendingEmail.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Error message for DA users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='DAView', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.DAView', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Invite a client into the system', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Invite', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Invite', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the Humanitarian roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.HumanitarianRoster', @PERMISSIONCODE='HumanitarianRoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the SU roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.SURoster', @PERMISSIONCODE='SURoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonGroup', @DESCRIPTION='Add / edit a user group', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonGroup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonGroup', @DESCRIPTION='View the list of user groups', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonGroup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonGroup', @DESCRIPTION='View a user group', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonGroup.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonUpdate', @DESCRIPTION='Add / edit a PersonUpdate', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonUpdate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonUpdate', @DESCRIPTION='View the list of PersonUpdates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonUpdate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonUpdate', @DESCRIPTION='View a PersonUpdate', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonUpdate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit an activity stream', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of activity streams', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View an activity stream', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='Add / edit an activity stream sponsor', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='View the list of activity stream sponsors', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='View an activity stream sponsor', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='Add / edit a Vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='View the list of Vacancies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='Manage applications', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ManageApplication', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.ManageApplication', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='Save application management', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ManageApplicationSave', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.ManageApplicationSave', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='View application management', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ManageApplicationView', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.ManageApplicationView', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='View a Vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;

--Begin update document type permissions
EXEC document.UpdateDocumentPermissions
--End update document type permissions
--End table permissionable.Permissionable

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.11 - 2019.04.13 18.38.52')
GO
--End build tracking

