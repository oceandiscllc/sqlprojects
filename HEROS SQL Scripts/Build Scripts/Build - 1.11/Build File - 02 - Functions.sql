USE HermisCloud
GO

--Begin function dropdown.GetCountryNameFromISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameFromISOCountryCode'
GO
--End function dropdown.GetCountryNameFromISOCountryCode

--Begin function person.CanHaveAccess
EXEC utility.DropObject 'person.CanHaveAccess'
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A function to determine if a PersonID has access to a specific entity type / entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
--
-- Author:			Jonathan Burnham
-- Create date:	2019.02.02
-- Description:	refactored to support the hrms workflow
-- ===================================================================================================

CREATE FUNCTION person.CanHaveAccess
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bCanHaveAccess BIT = 0

	IF @EntityTypeCode IN ('ActivityReport','Application','ClientVacancy','ClientVacancyApplication','EquipmentOrder','Feedback','PersonUpdate','SpotReport','TrendReport','Vacancy','VacancyApplication')
		BEGIN

		IF @EntityTypeCode = 'VacancyApplication' AND (SELECT A.SubmittedDateTime FROM hrms.Application A WHERE A.ApplicationID = @EntityID AND A.PersonID = @PersonID) IS NULL
			SET @bCanHaveAccess = 1
		ELSE
			BEGIN

			SET @bCanHaveAccess = workflow.IsWorkflowComplete(@EntityTypeCode, @EntityID)

			IF @bCanHaveAccess = 0
				SET @bCanHaveAccess = workflow.IsPersonInCurrentWorkflowStep(@PersonID, @EntityTypeCode, @EntityID, 0)
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF

	IF @EntityTypeCode = 'Application' AND @bCanHaveAccess = 0
		BEGIN

		SELECT @bCanHaveAccess = CASE WHEN A.SubmittedDateTime IS NULL THEN 1 ELSE 0 END
		FROM hrms.Application A
		WHERE A.ApplicationID = @EntityID
			AND A.PersonID = @PersonID

		END
	--ENDIF

	RETURN ISNULL(@bCanHaveAccess, 0)

END
GO
--End function person.CanHaveAccess

--Begin function person.GetPersonIDByDAPersonID
EXEC utility.DropObject 'person.GetPersonIDByDAPersonID'
GO
-- =================================================================
-- Author:			Todd Pires
-- Create date:	2019.04.08
-- Description:	A function to get a Person ID based on a DA PersonID
-- =================================================================

CREATE FUNCTION person.GetPersonIDByDAPersonID
(
@DAPersonID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nPersonID INT = 0

	SELECT @nPersonID = P.PersonID
	FROM person.Person P
	WHERE P.DAPersonID = @DAPersonID

	RETURN ISNULL(@nPersonID, 0)

END
GO
--End function person.GetPersonIDByDAPersonID

--Begin function workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to indicate if a personid exists in the current step of an entity's workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =====================================================================================================

CREATE FUNCTION workflow.IsPersonInCurrentWorkflowStep
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflowStep BIT = 0

	IF (@EntityID = 0 AND EXISTS 
		(
		SELECT 1
		FROM workflow.WorkflowStepGroupPerson WSGP
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND WSGP.PersonID = @PersonID
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode 
					OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback')
					OR (@EntityTypeCode = 'ClientVacancy' AND W.EntityTypeCode LIKE 'ClientVacancy%')
				)
				AND W.IsActive = 1
				AND (
					@ProjectID = 0 
						OR (
							W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
							AND w.ProjectID = @ProjectID
						)
				)

		UNION

		SELECT 1
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = WSGPG.PersonGroupID
					)
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode
					OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback')
					OR (@EntityTypeCode = 'ClientVacancy' AND W.EntityTypeCode LIKE 'ClientVacancy%')
				)
				AND W.IsActive = 1
		)) OR EXISTS
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE (EWSGP.EntityTypeCode = @EntityTypeCode
				OR (@EntityTypeCode = 'Feedback' AND EWSGP.EntityTypeCode LIKE '%Feedback')
				OR (@EntityTypeCode = 'ClientVacancy' AND EWSGP.EntityTypeCode LIKE 'ClientVacancy%')
			)
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EWSGP.PersonID = @PersonID

			UNION

			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
			WHERE (EWSGPG.EntityTypeCode = @EntityTypeCode
				OR (@EntityTypeCode = 'Feedback' AND EWSGPG.EntityTypeCode LIKE '%Feedback')
				OR (@EntityTypeCode = 'ClientVacancy' AND EWSGPG.EntityTypeCode LIKE 'ClientVacancy%')
			)
				AND EWSGPG.EntityID = @EntityID
				AND EWSGPG.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = EWSGPG.PersonGroupID
					)
			)

		SET @bIsPersonInCurrentWorkflowStep = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflowStep

END
GO
--End function workflow.IsPersonInCurrentWorkflowStep