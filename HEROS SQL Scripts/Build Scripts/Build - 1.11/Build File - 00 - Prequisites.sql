USE HermisCloud
GO

--Begin schema lesson
EXEC utility.AddSchema 'lesson'
GO
--End schema lesson

--The 2 Client Vacancy Workflows should be created with these names prior to SQL
UPDATE workflow.Workflow
SET EntityTypeSubCode = 'Direct'
WHERE WorkflowName = 'Client Vacancy - Direct'
GO

UPDATE workflow.Workflow
SET EntityTypeSubCode = 'Open'
WHERE WorkflowName = 'Client Vacancy - Open'
GO
