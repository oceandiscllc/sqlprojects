USE HermisCloud
GO

--Begin procedure dropdown.GetExpertStatusData
EXEC Utility.DropObject 'dropdown.GetExpertStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.ExpertStatus table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetExpertStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ExpertStatusID, 
		T.ExpertStatusCode,
		T.ExpertStatusName
	FROM dropdown.ExpertStatus T
	WHERE (T.ExpertStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ExpertStatusName, T.ExpertStatusID

END
GO
--End procedure dropdown.GetExpertStatusData

--Begin procedure dropdown.GetIR35StatusData
EXEC Utility.DropObject 'dropdown.GetIR35StatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.IR35Status table
-- =================================================================================
CREATE PROCEDURE dropdown.GetIR35StatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IR35StatusID, 
		T.IR35StatusCode,
		T.IR35StatusName
	FROM dropdown.IR35Status T
	WHERE (T.IR35StatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IR35StatusName, T.IR35StatusID

END
GO
--End procedure dropdown.GetIR35StatusData

--Begin procedure eventlog.LogTrendReportAggregatorAction
EXEC utility.DropObject 'eventlog.LogTrendReportAggregatorAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTrendReportAggregatorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'TrendReportAggregator'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('TrendReportAggregator'), ELEMENTS
			)
		FROM aggregator.TrendReportAggregator T
		WHERE T.TrendReportAggregatorID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTrendReportAggregatorAction

--Begin procedure eventlog.LogWorkflowAction
EXEC utility.DropObject 'eventlog.LogWorkflowAction'
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE eventlog.LogWorkflowAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Workflow'

	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cWorkflowSteps VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowSteps = COALESCE(@cWorkflowSteps, '') + D.WorkflowStep 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStep'), ELEMENTS) AS WorkflowStep
			FROM workflow.WorkflowStep T 
			WHERE T.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroups VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroups = COALESCE(@cWorkflowStepGroups, '') + D.WorkflowStepGroup 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroup'), ELEMENTS) AS WorkflowStepGroup
			FROM workflow.WorkflowStepGroup T
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = T.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroupPersons VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroupPersons = COALESCE(@cWorkflowStepGroupPersons, '') + D.WorkflowStepGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroupPerson'), ELEMENTS) AS WorkflowStepGroupPerson
			FROM workflow.WorkflowStepGroupPerson T 
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = T.WorkflowStepGroupID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroupPersonGroups VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroupPersonGroups = COALESCE(@cWorkflowStepGroupPersonGroups, '') + D.WorkflowStepGroupPersonGroup 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroupPersonGroup'), ELEMENTS) AS WorkflowStepGroupPersonGroup
			FROM workflow.WorkflowStepGroupPersonGroup T 
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = T.WorkflowStepGroupID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<WorkflowSteps>' + ISNULL(@cWorkflowSteps, '') + '</WorkflowSteps>') AS XML),
			CAST(('<WorkflowStepGroups>' + ISNULL(@cWorkflowStepGroups, '') + '</WorkflowStepGroups>') AS XML),
			CAST(('<WorkflowStepGroupPersons>' + ISNULL(@cWorkflowStepGroupPersons, '') + '</WorkflowStepGroupPersons>') AS XML),
			CAST(('<WorkflowStepGroupPersonGroups>' + ISNULL(@cWorkflowStepGroupPersonGroups, '') + '</WorkflowStepGroupPersonGroups>') AS XML)
			FOR XML RAW('Workflow'), ELEMENTS
			),
			T.ProjectID
		FROM workflow.Workflow T
		WHERE T.WorkflowID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			T.ProjectID
		FROM workflow.Workflow T
		WHERE T.WorkflowID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWorkflowAction

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.19
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.26
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.29
-- Description: Added IsOnSURoster to the select list
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAPersonID,
		P.DefaultProjectID,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.InvoiceLimit,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,		
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKWorkEligible,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		P.MiddleName,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'Humanitarian'), 0) AS IsOnHSOTRoster,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'SU'), 0) AS IsOnSURoster,
		ISNULL((SELECT TOP 1 SC.SecurityClearanceName + ' - ' + core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID AND PSC.IsActive = 1 AND PSC.PersonID = P.PersonID), 'None') AS Vetting,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.PurchaseOrderLimit,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR.PoliceRankID,
		PR.PoliceRankName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR ON PR.PoliceRankID = P.PoliceRankID
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
	WHERE P.PersonID = @PersonID

	--PersonClientRoster
	SELECT 
		CR.ClientRosterCode,
		CR.ClientRosterName,
		ES.ExpertStatusID,
		ES.ExpertStatusName,
		PCR.ClientRosterID,
		PCR.ExpertStatusID,
		PCR.PersonClientRosterID,
		PCR.TerminationDate,
		core.FormatDate(PCR.TerminationDate) AS TerminationDateFormatted,
		PCR.TerminationReason
	FROM person.PersonClientRoster PCR
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
			AND PCR.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'PersonCorrespondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEXC
		JOIN dropdown.Country C ON C.CountryID = PEXC.CountryID
			AND PEXC.PersonID = @PersonID
	
	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertStatus
	SELECT
		ES.ExpertStatusID,
		ES.ExpertStatusName
	FROM person.PersonExpertStatus PES
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PES.ExpertStatusID
			AND ES.ExpertStatusID > 0
			AND PES.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonGroupPerson
	SELECT 
		PGP.PersonGroupPersonID,
		PGP.PersonGroupID,
		PGP.CreateDateTime,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateFormatted,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PGP2.PersonID) FROM person.PersonGroupPerson PGP2 WHERE PGP2.PersonGroupID = PG.PersonGroupID) AS UserCount,
		PG.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName
	FROM person.PersonGroupPerson PGP
		JOIN person.PersonGroup PG ON PG.PersonGroupID = PGP.PersonGroupID
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PGP.PersonID = @PersonID

	--PersonIR35
	SELECT
		newID() AS PersonIR35GUID,
		PIR.TaskName,
		PIR.IR35StatusDate,
		PIR.IR35StatusID,
		core.FormatDate(PIR.IR35StatusDate) AS IR35StatusDateFormatted,
		PIR.PersonIR35ID,
		PIR.Issues,
		IRS.IR35StatusName
	FROM person.PersonIR35 PIR
		JOIN dropdown.IR35Status IRS ON IRS.IR35StatusID = PIR.IR35StatusID
			AND PIR.PersonID = @PersonID
	ORDER BY PIR.TaskName, PIR.PersonIR35ID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonGroupByPersonGroupID
EXEC utility.DropObject 'person.GetPersonGroupByPersonGroupID'
GO

-- ==========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.10.15
-- Description:	A stored procedure to return data from the person.PersonGroup table
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE person.GetPersonGroupByPersonGroupID

@PersonGroupID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonGroup
	SELECT 
		PG.PersonGroupID,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PersonID) FROM person.PersonGroupPerson PGP WHERE PGP.PersonGroupID = PG.PersonGroupID) AS UserCount,
		AAL.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName,
		PG.CreateDateTime,
		core.FormatDate(PG.CreateDateTime) AS CreateDateTimeFormatted
	FROM person.PersonGroup PG
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PG.PersonGroupID = @PersonGroupID

	--PersonGroupPerson
	SELECT
		PGP.PersonID,
		person.FormatPersonNameByPersonID(PGP.PersonID,'LASTFIRST') AS FullName,
		P.UserName,
		P.Organization,
		R.RoleName,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateTimeFormatted
	FROM person.PersonGroupPerson PGP
	JOIN person.Person P ON P.PersonID = PGP.PersonID
	JOIN dropdown.Role R ON R.RoleID = P.RoleID
	WHERE PGP.PersonGroupID = @PersonGroupID
	ORDER BY P.LastName

	--PersonGroupWorkflows
	SELECT W.WorkflowID, P.ProjectName, P.ProjectID, W.WorkflowName, ET.EntityTypeName, WS.WorkflowStepNumber, core.FormatDate(WSGPG.CreateDateTime) AS CreateDateFormatted
	FROM workflow.WorkflowStepGroupPersonGroup WSGPG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		JOIN dropdown.Project P ON P.ProjectID = W.ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = W.EntityTypeCode
	WHERE W.IsActive = 1
	AND P.IsActive = 1
	AND WSGPG.PersonGroupID = @PersonGroupID

END
GO
--End procedure person.GetPersonGroupByPersonGroupID

--Begin procedure person.GetVettingEmailData
EXEC Utility.DropObject 'person.GetVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data for outgoing vetting e-mail
-- ==========================================================================
CREATE PROCEDURE person.GetVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonSecurityClearanceProcessStep
		(PersonID, SecurityClearanceID, SecurityClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		(SELECT SCPS.SecurityClearanceID FROM dropdown.SecurityClearance SCPS WHERE SCPS.SecurityClearanceCode = 'BPSS'),
		(SELECT SCPS.SecurityClearanceProcessStepID FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = 'PREP'),
		getDate(),
		1,
		'Security clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.DiscretionaryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS DiscretionaryReviewDateFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS MandatoryReviewDateFormatted
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL,
		NULL

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetVettingEmailData

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDAPersonID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		DAPersonID INT NOT NULL DEFAULT 0,
		DefaultProjectID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		RoleName VARCHAR(50),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsPasswordExpired = CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN 1 ELSE 0 END,
		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN ',PasswordExpiration' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDAPersonID = ISNULL(P.DAPersonID, 0),
		@nDefaultProjectID = P.DefaultProjectID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(
			CellPhone,
			CountryCallingCodeID,
			DAPersonID,
			DefaultProjectID,
			EmailAddress,
			FullName,
			IsAccountLockedOut,
			IsActive,
			IsPhoneVerified,
			IsProfileUpdateRequired,
			IsSuperAdministrator,
			IsValidPassword,
			IsValidUserName,
			PersonID,
			RequiredProfileUpdate,
			RoleName,
			UserName
			) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@nDAPersonID,
			@nDefaultProjectID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cRoleName,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT 
		P.*
	FROM @tPerson P

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure reporting.GetConsultantList
EXEC utility.DropObject 'reporting.GetConsultantList'
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to Decrement a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE reporting.GetConsultantList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.UserName AS UserName,
		P.FirstName AS FirstName,
		P.LastName AS LastName,
		P.PreferredName,
		P.EmailAddress,
		PP.FirstName + ' ' + PP.LastName AS ManagerName,
		STUFF((SELECT  ',' + ES.ExpertStatusName
			FROM person.PersonClientRoster PCR
			JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
				AND PCR.PersonID = P.PersonID
				FOR XML PATH('')), 1, 1, '') AS [Status],
		STUFF((SELECT  ',' + ET.ExpertTypeName
			FROM person.PersonExpertType PET
			JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
				AND PET.PersonID = P.PersonID
		FOR XML PATH('')), 1, 1, '') AS [Type],
		STUFF((SELECT  ',' + EC.ExpertCategoryName
			FROM person.PersonExpertCategory PEC
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
				AND PEC.PersonID = P.PersonID
		FOR XML PATH('')), 1, 1, '') AS PrimaryCategory,
		STUFF((SELECT  ',' + EC.ExpertCategoryName
			FROM person.PersonSecondaryExpertCategory PSEC
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PSEC.ExpertCategoryID
				AND PSEC.PersonID = P.PersonID
		FOR XML PATH(''), TYPE).value('.', 'varchar(max)'), 1, 1, '') AS SecondaryCategories,
		P.SummaryBiography
	FROM person.Person P
		LEFT JOIN person.Person PP ON P.ManagerPersonID = PP.PersonID
		JOIN Reporting.SearchResult SR ON SR.EntityID = P.PersonID
			AND SR.EntityTypeCode = 'ConsultantList'
			AND SR.PersonID = @PersonID
	ORDER BY P.LastName

END
GO
--End procedure reporting.GetConsultantList

--Begin procedure workflow.DecrementWorkflow
EXEC utility.DropObject 'workflow.DecrementWorkflow'
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to Decrement a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.DecrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	UPDATE EWSGP
	SET EWSGP.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	UPDATE EWSGPG
	SET EWSGPG.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
		AND EWSGPG.EntityID = @EntityID
		AND EWSGPG.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	SELECT
		@nWorkflowStepNumber AS OldWorkflowStepNumber,
		workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
		workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount

END
GO
--End procedure workflow.DecrementWorkflow

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO
-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
			) AS WorkflowStepCount
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND WS1.WorkflowStepNumber = 1
		
		END
	--ENDIF

	ELSE
		BEGIN
	
			IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
				BEGIN
				
				SELECT TOP 1 
					D.WorkflowStepName,
					D.WorkflowStepNumber,
					D.WorkflowStepCount
				FROM 
					(
					SELECT
						EWSGP.WorkflowStepName,
						EWSGP.WorkflowStepNumber,
						workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = @EntityTypeCode
						AND EWSGP.EntityID = @EntityID
						AND EWSGP.IsComplete = 0

					UNION

					SELECT 
						EWSGPG.WorkflowStepName,
						EWSGPG.WorkflowStepNumber,
						workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
					FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
					WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
						AND EWSGPG.EntityID = @EntityID
						AND EWSGPG.IsComplete = 0			
				) AS D
				ORDER BY 2

				END
			--ENDIF
			
			ELSE
				BEGIN

				SELECT
					'Approved' AS WorkflowStepName,
					workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
					workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
				
				END
			--ENDIF
			
		END
	--ENDIF

END
GO
--End procedure workflow.GetEntityWorkflowData

--Begin procedure workflow.GetEntityWorkflowPeople
EXEC Utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to return people assigned to an entityy's current workflow step
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.GetEntityWorkflowPeople

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT

AS
BEGIN

	DECLARE @tTable TABLE
		(
		WorkflowStepGroupName VARCHAR(1000), 
		WorkflowStepGroupPersonGroupName VARCHAR(1000), 
		PersonGroupID INT,
		IsFinancialApprovalRequired BIT DEFAULT 0,
		PersonID INT,
		FullName VARCHAR(1000),
		EmailAddress VARCHAR(1000),
		IsComplete BIT DEFAULT 0
		)

	IF @EntityID = 0
		BEGIN
		
		INSERT INTO @tTable 
			(WorkflowStepGroupName,WorkflowStepGroupPersonGroupName,PersonGroupID,IsFinancialApprovalRequired,PersonID,FullName,EmailAddress,IsComplete)
		SELECT
			WSG.WorkflowStepGroupName,
			'',
			0,
			WSG.IsFinancialApprovalRequired,
			WSGP.PersonID,
			person.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN person.Person P ON P.PersonID = WSGP.PersonID

		UNION

		SELECT
			WSG.WorkflowStepGroupName,
			PG.PersonGroupName,
			PG.PersonGroupID,
			WSG.IsFinancialApprovalRequired,
			PGP.PersonID,
			person.FormatPersonnameByPersonID(PGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID
			JOIN person.PersonGroupPerson PGP ON PGP.PersonGroupID = PG.PersonGroupID
			JOIN person.Person P ON P.PersonID = PGP.PersonID
	
		END
	ELSE
		BEGIN
	
		INSERT INTO @tTable 
			(WorkflowStepGroupName,WorkflowStepGroupPersonGroupName,PersonGroupID,IsFinancialApprovalRequired,PersonID,FullName,EmailAddress,IsComplete)
		SELECT
			EWSGP.WorkflowStepGroupName, 
			'',
			0,
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.PersonID,
			person.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber

		UNION

		SELECT
			EWSGPG.WorkflowStepGroupName, 
			PG.PersonGroupName,
			PG.PersonGroupID,
			EWSGPG.IsFinancialApprovalRequired,
			PGP.PersonID,
			person.FormatPersonnameByPersonID(PGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGPG.IsComplete
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
			JOIN person.PersonGroup PG ON PG.PersonGroupID = EWSGPG.PersonGroupID
			JOIN person.PersonGroupPerson PGP ON PGP.PersonGroupID = PG.PersonGroupID
			JOIN person.Person P ON P.PersonID = PGP.PersonID
				AND EWSGPG.EntityTypeCode = @EntityTypeCode
				AND EWSGPG.EntityID = @EntityID
				AND EWSGPG.WorkflowStepNumber = @WorkflowStepNumber

		END
	--ENDIF

	SELECT 
		T.WorkflowStepGroupName,
		T.WorkflowStepGroupPersonGroupName,
		T.PersonGroupID,
		T.IsFinancialApprovalRequired,
		T.PersonID,
		T.FullName,
		T.EmailAddress,
		T.IsComplete
	FROM @tTable T
	ORDER BY T.WorkflowStepGroupName, T.WorkflowStepGroupPersonGroupName, T.FullName
	
END
GO
--End procedure workflow.GetEntityWorkflowPeople

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.ProjectID,
		dropdown.GetProjectNameByProjectID(W.ProjectID) AS ProjectName,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT 
		WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT 
		WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.IsFinancialApprovalRequired,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		person.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGPG.PersonGroupID,
		WSGPG.WorkflowStepGroupID,
		PG.PersonGroupName
	FROM workflow.WorkflowStepGroupPersonGroup WSGPG
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
		JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.IncrementWorkflow
EXEC utility.DropObject 'workflow.IncrementWorkflow'
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to increment a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.IncrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @nWorkflowStepGroupID INT
	
	IF EXISTS 
		(
		SELECT TOP 1 
			EWSGP.EntityWorkflowStepGroupPersonID
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.PersonID = @PersonID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
		ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID
		)
		BEGIN

		SELECT TOP 1
			@nWorkflowStepNumber = EWSGP.WorkflowStepNumber,
			@nWorkflowStepGroupID = EWSGP.WorkflowStepGroupID	
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.PersonID = @PersonID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
		ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID

		END
	--ENDIF

	ELSE
		BEGIN

		SELECT TOP 1
			@nWorkflowStepNumber = EWSGPG.WorkflowStepNumber,
			@nWorkflowStepGroupID = EWSGPG.WorkflowStepGroupID	
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
			AND EWSGPG.EntityID = @EntityID
			AND EWSGPG.PersonGroupID IN (SELECT PGP.PersonGroupID FROM person.PersonGroupPerson PGP WHERE PGP.PersonID = @PersonID)
			AND EWSGPG.IsComplete = 0
			AND EWSGPG.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
		ORDER BY EWSGPG.WorkflowStepNumber, EWSGPG.WorkflowStepGroupID

		END
	--ENDIF
	
	IF @nWorkflowStepNumber > 0 AND @nWorkflowStepGroupID > 0
		BEGIN
	
		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGP.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		UPDATE EWSGPG
		SET EWSGPG.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
			AND EWSGPG.EntityID = @EntityID
			AND EWSGPG.IsComplete = 0
			AND EWSGPG.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGPG.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	ELSE
		BEGIN
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			0 AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	--ENDIF

END
GO
--End procedure workflow.IncrementWorkflow

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

	INSERT INTO workflow.EntityWorkflowStepGroupPersonGroup
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonGroupID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGPG.PersonGroupID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

END
GO
--End procedure workflow.InitializeEntityWorkflow

--Begin procedure workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to indicate if a personid exists in the current step of an entity's workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =====================================================================================================

CREATE PROCEDURE workflow.IsPersonInCurrentWorkflowStep

@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT = 0

AS
BEGIN

	IF @EntityID = 0
		BEGIN

		SELECT 1
		FROM workflow.WorkflowStepGroupPerson WSGP
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND WSGP.PersonID = @PersonID
				AND WS.WorkflowStepNumber = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND W.IsActive = 1

		UNION

		SELECT 1
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = WSGPG.PersonGroupID
					)
				AND WS.WorkflowStepNumber = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND W.IsActive = 1

		END
	ELSE
		BEGIN

		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
			AND EWSGP.PersonID = @PersonID

		UNION

		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
			AND EWSGPG.EntityID = @EntityID
			AND EWSGPG.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonGroupPerson PGP
				WHERE PGP.PersonID = @PersonID
					AND PGP.PersonGroupID = EWSGPG.PersonGroupID
				)

		END
	--ENDIF
	
END
GO
--End procedure workflow.IsPersonInCurrentWorkflowStep

--Begin procedure workflow.ResetEntityWorkflowStepGroupPerson
EXEC utility.DropObject 'workflow.ResetEntityWorkflowStepGroupPerson'
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.06
-- Description:	A stored procedure to update already crated workflows with the current personnel assignments
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =========================================================================================================
CREATE PROCEDURE workflow.ResetEntityWorkflowStepGroupPerson

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tEntityWorkflowStepGroupPerson TABLE
		(
		EntityTypeCode VARCHAR(50),
		EntityID INT,
		WorkflowID INT,
		WorkflowStepID INT,
		WorkflowStepGroupID INT,
		WorkflowName VARCHAR(250),
		WorkflowStepNumber INT,
		WorkflowStepName VARCHAR(250),
		WorkflowStepGroupName VARCHAR(250),
		IsFinancialApprovalRequired BIT,
		PersonID INT,
		IsComplete BIT
		)	
		
	;
	WITH WSGP AS
		(
		SELECT
			W.WorkflowID,
			WS.WorkflowStepID, 
			WSG.WorkflowStepGroupID, 
			WSGP.PersonID
		FROM workflow.WorkflowStepGroupPerson WSGP 
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.WorkflowID = @WorkflowID
		)
	
	INSERT INTO @tEntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID, IsComplete)
	SELECT
		D.EntityTypeCode, 
		D.EntityID, 
		D.WorkflowID, 
		D.WorkflowStepID, 
		D.WorkflowStepGroupID, 
		D.WorkflowName, 
		D.WorkflowStepNumber, 
		D.WorkflowStepName, 
		D.WorkflowStepGroupName, 
		D.IsFinancialApprovalRequired,
		E.PersonID,
		D.IsComplete
	FROM
		(
		SELECT
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.WorkflowID = @WorkflowID
		GROUP BY
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.IsComplete
		) D
		CROSS APPLY
			(
			SELECT
				WSGP.PersonID
			FROM WSGP
			WHERE WSGP.WorkflowID = D.WorkflowID
				AND WSGP.WorkflowStepID = D.WorkflowStepID
				AND WSGP.WorkflowStepGroupID = D.WorkflowStepGroupID
			) E
	ORDER BY 
		D.EntityID,
		D.WorkflowID,
		D.WorkflowStepID,
		D.WorkflowStepGroupID, 
		D.IsComplete

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.WorkflowID = @WorkflowID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID, IsComplete)
	SELECT
		T.EntityTypeCode, 
		T.EntityID, 
		T.WorkflowID, 
		T.WorkflowStepID, 
		T.WorkflowStepGroupID, 
		T.WorkflowName, 
		T.WorkflowStepNumber, 
		T.WorkflowStepName, 
		T.WorkflowStepGroupName, 
		T.IsFinancialApprovalRequired,
		T.PersonID, 
		T.IsComplete
	FROM @tEntityWorkflowStepGroupPerson T

END
GO
--End procedure workflow.ResetEntityWorkflowStepGroupPerson