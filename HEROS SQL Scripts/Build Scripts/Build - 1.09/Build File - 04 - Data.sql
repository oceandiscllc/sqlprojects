USE HermisCloud
GO

--Begin table core.EmailTemplate
UPDATE ET
SET ET.EmailTemplateCode = 'DiscretionaryBPSS'
FROM core.EmailTemplate ET
WHERE ET.EmailTemplateCode = 'BPPSDiscretionaryClearanceReview'
GO

UPDATE ET
SET ET.EmailTemplateCode = 'InitialBPSS'
FROM core.EmailTemplate ET
WHERE ET.EmailTemplateCode = 'BPPSInitialClearanceReview'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryBPSS', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialBPSS', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryCTC', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a CTC security clearance require completion', 
	'CTC Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialCTC', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a CTC security clearance require completion', 
	'CTC Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionaryDV', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a DV security clearance require completion', 
	'DV Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialDV', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a DV security clearance require completion', 
	'DV Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'DiscretionarySC', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a SC security clearance require completion', 
	'SC Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'InitialSC', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a SC security clearance require completion', 
	'SC Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO
--End table core.EmailTemplate

--End table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'HumanitarianRosterTerminationEmialAddressTo', NULL, NULL
EXEC core.SystemSetupAddUpdate 'SURosterTerminationEmialAddressTo', NULL, NULL
GO
--End table core.SystemSetup

--Begin table dropdown.ExpertStatus
UPDATE ES
SET ES.ExpertStatusCode = 
	CASE
		WHEN ES.ExpertStatusName IN ('Active', 'Paused', 'Pending')
		THEN ES.ExpertStatusName
		WHEN ES.ExpertStatusName = 'Removed from roster'
		THEN 'Removed'
		ELSE NULL
	END
FROM dropdown.ExpertStatus ES
GO
--End table dropdown.ExpertStatus

--Begin table dropdown.IR35Status
TRUNCATE TABLE dropdown.IR35Status
GO

EXEC utility.InsertIdentityValue 'dropdown.IR35Status', 'IR35StatusID', 0
GO

INSERT INTO dropdown.IR35Status 
	(IR35StatusName, IR35StatusCode, DisplayOrder)
VALUES
	('Consultant with IR35', 'IR35Consultant', 1),
	('Consultant not IR35', 'Consultant', 2),
	('Employed', 'Employed', 3),
	('Other', 'Other', 4)

GO
--End table dropdown.IR35Status

--Begin table dropdown.SecurityClearance
IF NOT EXISTS (SELECT 1 FROM dropdown.SecurityClearance SC WHERE SC.SecurityClearanceCode = 'CTC')
	BEGIN

	UPDATE SC
	SET SC.DisplayOrder = SC.DisplayOrder + 1
	FROM dropdown.SecurityClearance SC
	WHERE SC.DisplayOrder > 2

	INSERT INTO dropdown.SecurityClearance 
		(SecurityClearanceCode, SecurityClearanceName, DisplayOrder) 
	VALUES
		('CTC', 'CTC', 3)

	END
--ENDIF
GO

UPDATE SC
SET SC.DisplayOrder = 4
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'SC'
GO

UPDATE SC
SET SC.DisplayOrder = 5
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'SC + NATO Equivalent Certificate'
GO

UPDATE SC
SET SC.DisplayOrder = 6
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'SC + EU Equivalent Certificate'
GO

UPDATE SC
SET SC.DisplayOrder = 7
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'SC + NATO and EU Equivalent Certificates'
GO

UPDATE SC
SET SC.DisplayOrder = 8
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'DV'
GO

UPDATE SC
SET SC.DisplayOrder = 9
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'DV + NATO Equivalent Certificate'
GO

UPDATE SC
SET SC.DisplayOrder = 10
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'DV + EU Equivalent Certificate'
GO

UPDATE SC
SET SC.DisplayOrder = 11
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceName = 'DV + NATO and EU Equivalent Certificates'
GO
--End table dropdown.SecurityClearance

--Begin table person.PersonClientRoster
UPDATE PCR
SET PCR.ExpertStatusID = CASE WHEN CR.ClientRosterCode = 'SU' THEN ISNULL(PES.ExpertStatusID, 0) ELSE (SELECT ES.ExpertStatusID FROM dropdown.ExpertStatus ES WHERE ES.ExpertStatusCode = 'Active') END
FROM person.PersonClientRoster PCR
	JOIN person.PersonExpertStatus PES ON PES.PersonID = PCR.PersonID
	JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
GO
--End table person.PersonClientRoster
