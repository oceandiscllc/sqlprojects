USE HermisCloud
GO

--Begin function person.CanHaveAccess
EXEC utility.DropObject 'person.CanHaveAccess'
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A function to determine if a PersonID has access to a specific entity type / entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ===================================================================================================

CREATE FUNCTION person.CanHaveAccess
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAccess BIT = 0

	IF @EntityTypeCode IN ('SpotReport', 'TrendReport', 'ActivityReport', 'EquipmentOrder')
		BEGIN

		IF EXISTS
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EWSGP.PersonID = @PersonID
			)
			OR EXISTS
				(
				SELECT 1
				FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
				WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
					AND EWSGPG.EntityID = @EntityID
					AND EWSGPG.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
					AND EWSGPG.PersonGroupID IN (SELECT DISTINCT PPG.PersonGroupID FROM person.PersonGroupPerson PPG WHERE PPG.PersonID = @PersonID)
				)
			OR (workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) - workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)) > 0
			BEGIN

				SET @nCanHaveAccess = 1

			END
		--ENDIF

		END
	--ENDIF

	RETURN @nCanHaveAccess

END
GO
--End function person.CanHaveAccess

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO
-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT
	DECLARE @tTable TABLE (WorkflowStepNumber INT)

	INSERT INTO @tTable 
		(WorkflowStepNumber)
 	SELECT 
 		EWSGP.WorkflowStepNumber
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID

	UNION

	SELECT 
		EWSGPG.WorkflowStepNumber
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID

	SELECT 
		@nWorkflowStepCount = MAX(T.WorkflowStepNumber) 
	FROM @tTable T
	
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO
-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @tTable TABLE(WorkflowStepNumber INT, WorkflowStepGroupID INT)

	INSERT INTO @tTable 
		(WorkflowStepNumber,WorkflowStepGroupID)
 	SELECT 
 		EWSGP.WorkflowStepNumber, 
 		EWSGP.WorkflowStepGroupID
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.IsComplete = 0 

	UNION

	SELECT 
		EWSGPG.WorkflowStepNumber, 
		EWSGPG.WorkflowStepGroupID
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID
		AND EWSGPG.IsComplete = 0 

	SELECT TOP 1 
		@nWorkflowStepNumber = T.WorkflowStepNumber 
	FROM @tTable T 
	ORDER BY T.WorkflowStepNumber, T.WorkflowStepGroupID

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber

END
GO
--End function workflow.GetWorkflowStepNumber
