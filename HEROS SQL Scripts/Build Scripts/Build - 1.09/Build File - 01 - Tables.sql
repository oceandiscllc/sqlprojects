USE HermisCloud
GO

--Begin table document.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'document.DocumentEntity'

EXEC utility.DropObject 'document.TR_DocumentEntity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.28
-- Description:	A trigger to update the document.DocumentEntity table
-- ==================================================================
CREATE TRIGGER document.TR_DocumentEntity ON document.DocumentEntity AFTER INSERT
AS
	SET ARITHABORT ON;

	DECLARE @tOutput TABLE (EntityTypeCode VARCHAR(50), DocumentEntityCode VARCHAR(50), EntityID INT)

	;
	WITH ID AS
		(
		SELECT
			I.EntityTypeCode,
			I.DocumentEntityCode,

			CASE
				WHEN I.EntityTypeCode = 'PersonMedicalClearance'
				THEN ISNULL((SELECT PMC.PersonMedicalClearanceID FROM person.PersonMedicalClearance PMC WHERE PMC.DocumentEntityCode = I.DocumentEntityCode), 0)
				WHEN I.EntityTypeCode = 'PersonSecurityClearance'
				THEN ISNULL((SELECT PSC.PersonSecurityClearanceID FROM person.PersonSecurityClearance PSC WHERE PSC.DocumentEntityCode = I.DocumentEntityCode), 0)
				ELSE 0
			END AS EntityID

		FROM INSERTED I
		WHERE I.DocumentEntityCode IS NOT NULL
			AND I.EntityTypeCode IN ('PersonMedicalClearance', 'PersonSecurityClearance')
		)

	UPDATE DE
	SET DE.EntityID = ID.EntityID
	FROM document.DocumentEntity DE
		JOIN ID ON ID.DocumentEntityCode = DE.DocumentEntityCode
			AND ID.EntityID > 0

GO
--End table document.DocumentEntity

--Begin table dropdown.ExpertStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ExpertStatus'

EXEC utility.AddColumn @TableName, 'ExpertStatusCode', 'VARCHAR(50)'
GO
--End table dropdown.ExpertStatus

--Begin table dropdown.IR35Status
DECLARE @TableName VARCHAR(250) = 'dropdown.IR35Status'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IR35Status
	(
	IR35StatusID INT IDENTITY(0,1) NOT NULL,
	IR35StatusCode VARCHAR(50) NULL,
	IR35StatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IR35StatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IR35Status', 'DisplayOrder,IR35StatusName', 'IR35StatusID'
GO
--End table dropdown.IR35eStatus

--Begin table person.Person
EXEC Utility.DropColumn 'person.Person', 'HSOTRosterTerminationDate'
EXEC Utility.DropColumn 'person.Person', 'SpendingAuthorityLimit'
EXEC Utility.DropColumn 'person.Person', 'SURosterTerminationDate'
GO

EXEC Utility.AddColumn 'person.Person', 'InvoiceLimit', 'NUMERIC(18,2)', '0'
EXEC Utility.AddColumn 'person.Person', 'PurchaseOrderLimit', 'NUMERIC(18,2)', '0'
GO
--End table person.Person

--Begin table person.PersonClientRoster
EXEC Utility.AddColumn 'person.PersonClientRoster', 'ExpertStatusID', 'INT', '0'
EXEC Utility.AddColumn 'person.PersonClientRoster', 'TerminationDate', 'DATE'
EXEC Utility.AddColumn 'person.PersonClientRoster', 'TerminationReason', 'VARCHAR(MAX)'
GO
--End table person.PersonClientRoster

--Begin table person.PersonIR35
DECLARE @TableName VARCHAR(250) = 'person.PersonIR35'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonIR35
	(
	PersonIR35ID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	TaskName VARCHAR(250),
	IR35StatusID INT,
	IR35StatusDate DATE,
	Issues VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IR35StatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonIR35ID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonIR35', 'PersonID,TaskName'
GO
--End table person.PersonIR35

--Begin table person.PersonMedicalClearance
DECLARE @TableName VARCHAR(250) = 'person.PersonMedicalClearance'

EXEC utility.AddColumn @TableName, 'DocumentEntityCode', 'VARCHAR(50)'
GO

EXEC utility.DropObject 'person.TR_PersonMedicalClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonMedicalClearance table
--
-- Author:			Justin Branum
-- Create Date: 2019.01.24
-- Description: Trigger needs to set the other medical clearances to inactive ONLY if that record itself is set to Active
-- ======================================================================================================================
CREATE TRIGGER person.TR_PersonMedicalClearance ON person.PersonMedicalClearance AFTER INSERT, UPDATE
AS
	SET ARITHABORT ON;

	UPDATE PMC
	SET PMC.IsActive = 0
	FROM person.PersonMedicalClearance PMC
		JOIN INSERTED I ON I.PersonID = PMC.PersonID
			AND I.PersonMedicalClearanceID <> PMC.PersonMedicalClearanceID
			AND I.MedicalClearanceTypeID = PMC.MedicalClearanceTypeID
			AND I.IsActive = 1

GO
--End table person.PersonMedicalClearance

--Begin table person.PersonProject
EXEC Utility.AddColumn 'person.PersonProject', 'IsViewDefault', 'BIT', '0'
GO
--End table person.PersonProject

--Begin table person.PersonSecurityClearance
DECLARE @TableName VARCHAR(250) = 'person.PersonSecurityClearance'

EXEC utility.AddColumn @TableName, 'DocumentEntityCode', 'VARCHAR(50)'
GO

EXEC utility.DropObject 'person.TR_PersonSecurityClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonSecurityClearance table
--
-- Author:			Justin Branum
-- Create Date: 2019.01.24
-- Description: Trigger needs to set the other medical clearances to inactive ONLY if that record itself is set to Active
-- ======================================================================================================================
CREATE TRIGGER person.TR_PersonSecurityClearance ON person.PersonSecurityClearance AFTER INSERT, UPDATE
AS
	SET ARITHABORT ON;

	UPDATE PSC
	SET PSC.IsActive = 0
	FROM person.PersonSecurityClearance PSC
		JOIN INSERTED I ON I.PersonID = PSC.PersonID
			AND I.PersonSecurityClearanceID <> PSC.PersonSecurityClearanceID
			AND I.IsActive = 1

GO
--End table person.PersonSecurityClearance

--Begin table workflow.EntityWorkflowStepGroupPerson
EXEC Utility.AddColumn 'workflow.EntityWorkflowStepGroupPerson', 'IsFinancialApprovalRequired', 'BIT', '0'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.EntityWorkflowStepGroupPersonGroup
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowStepGroupPersonGroup'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.EntityWorkflowStepGroupPersonGroup
	(
	EntityWorkflowStepGroupPersonGroupID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	WorkflowID INT,
	WorkflowStepID INT,
	WorkflowStepGroupID INT,
	WorkflowName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepName VARCHAR(250),
	WorkflowStepGroupName VARCHAR(250),
	PersonGroupID INT,
	IsComplete BIT,
	CreateDateTime DATETIME,
	IsFinancialApprovalRequired BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFinancialApprovalRequired', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityWorkflowStepGroupPersonGroupID'
GO
--End table workflow.EntityWorkflowStepGroupPersonGroup

--Begin table workflow.WorkflowStepGroup
EXEC Utility.AddColumn 'workflow.WorkflowStepGroup', 'IsFinancialApprovalRequired', 'BIT', '0'
GO
--End table workflow.WorkflowStepGroup

--Begin table workflow.WorkflowStepGroupPersonGroup
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroupPersonGroup'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStepGroupPersonGroup
	(
	WorkflowStepGroupPersonGroupID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepGroupID INT,
	PersonGroupID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'PersonGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepGroupID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'WorkflowStepGroupPersonGroupID'
GO
--End table workflow.WorkflowStepGroupPersonGroup
