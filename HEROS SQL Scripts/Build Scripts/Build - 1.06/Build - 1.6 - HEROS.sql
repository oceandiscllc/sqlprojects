-- File Name:	Build - 1.6 - HEROS.sql
-- Build Key:	Build - 1.6 - 2018.10.29 18.16.00

--USE HermisCloud
GO

-- ==============================================================================================================================
-- Tables:
--		dropdown.ProjectSponsor
--
-- Procedures:
--		core.GetIncidentByIncidentID
--		document.GetDocumentByDocumentID
--		dropdown.GetProjectByProjectID
--		dropdown.GetProjectSponsorByProjectSponsorID
--		dropdown.GetProjectSponsorData
--		eventlog.LogDocumentAction
--		eventlog.LogProjectAction
--		eventlog.LogProjectSponsorAction
--		force.GetForceByForceID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--Begin table core.Incident
DECLARE @TableName VARCHAR(250) = 'core.Incident'

EXEC utility.AddColumn @TableName, 'IsForResponseDashboard', 'BIT', '0'
GO
--End table core.Incident

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.AddColumn @TableName, 'IsForResponseDashboard', 'BIT', '0'
GO
--End table document.Document

--Begin table dropdown.Project
DECLARE @TableName VARCHAR(250) = 'dropdown.Project'

EXEC utility.AddColumn @TableName, 'StartDate', 'DATE'
EXEC utility.AddColumn @TableName, 'EndDate', 'DATE'
EXEC utility.AddColumn @TableName, 'ProjectDescription', 'VARCHAR(500)'
EXEC utility.AddColumn @TableName, 'ProjectSponsorID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'IsActive', 'BIT', '0'
GO
--End table dropdown.Project

--Begin table dropdown.ProjectSponsor
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectSponsor'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectSponsor
	(
	ProjectSponsorID INT IDENTITY(0,1) NOT NULL,
	ProjectSponsorName VARCHAR(250),
	ProjectSponsorCode VARCHAR(50),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectSponsorID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectSponsor', 'ProjectSponsorName,ProjectSponsorID'
GO
--End table dropdown.ProjectSponsor

--Begin table force.Force
DECLARE @TableName VARCHAR(250) = 'force.Force'

EXEC utility.AddColumn @TableName, 'IsForResponseDashboard', 'BIT', '0'
GO
--End table force.Force

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--Begin procedure core.GetIncidentByIncidentID
EXEC utility.DropObject 'core.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the dbo.Incident table
-- =======================================================================
CREATE PROCEDURE core.GetIncidentByIncidentID

@IncidentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentDateTime,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IsForResponseDashboard,
		I.KeyPoints,
		I.Location.STAsText() AS Location,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.Source,
		I.Summary,
		I.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,
		I.Mission,
		I.ImpactDescription,
		I.MitigationEffectiveness,
		I.Lessons,
		I.UpdateDateTime,
		core.FormatDateTime(I.UpdateDateTime) AS UpdateDateTimeFormatted,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		ST.SourceTypeID,
		ST.SourceTypeName,
		SR.SourceReliabilityID,
		SR.SourceReliabilityName,
		IV.InformationValidityID,
		IV.InformationValidityName,
		II.IncidentImpactID,
		II.IncidentImpactName,
		IC.IncidentCategoryID,
		IC.IncidentCategoryName,
		IC.IncidentCategoryCode
	FROM core.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = I.SourceTypeID
		JOIN dropdown.SourceReliability SR ON SR.SourceReliabilityID = I.SourceReliabilityID
		JOIN dropdown.InformationValidity IV ON IV.InformationValidityID = I.InformationValidityID
		JOIN dropdown.IncidentImpact II ON II.IncidentImpactID = I.IncidentImpactID
		JOIN dropdown.IncidentCategory IC ON IC.IncidentCategoryID = I.IncidentCategoryID
			AND I.IncidentID = @IncidentID
	
END
GO
--End procedure core.GetIncidentByIncidentID

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentTitle,
		D.Extension,
		D.IsForResponseDashboard,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure dropdown.GetProjectByProjectID
EXEC Utility.DropObject 'dropdown.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.15
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Project
	SELECT 
		P.ProjectDescription,
		P.ProjectID,
		P.ProjectName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.IsActive,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted,
		PS.ProjectSponsorID,
		PS.ProjectSponsorName
	FROM dropdown.Project P
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
			AND P.ProjectID = @ProjectID

	--ProjectTerritory
	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryName
	FROM territory.ProjectTerritory PT
	WHERE PT.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetProjectByProjectID

--Begin procedure dropdown.GetProjectSponsorByProjectSponsorID
EXEC Utility.DropObject 'dropdown.GetProjectSponsorByProjectSponsorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.15
-- Description:	A stored procedure to return data from the dropdown.ProjectSponsor table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetProjectSponsorByProjectSponsorID

@ProjectSponsorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PS.ProjectSponsorID,
		PS.ProjectSponsorCode,
		PS.ProjectSponsorName,
		PS.IsActive
	FROM dropdown.ProjectSponsor PS
	WHERE PS.ProjectSponsorID = @ProjectSponsorID

END
GO
--End procedure dropdown.GetProjectSponsorByProjectSponsorID

--Begin procedure dropdown.GetProjectSponsorData
EXEC Utility.DropObject 'dropdown.GetProjectSponsorData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.14
-- Description:	A stored procedure to return data from the dropdown.ProjectSponsor table
-- ================================================================================
CREATE PROCEDURE dropdown.GetProjectSponsorData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectSponsorID,
		T.ProjectSponsorCode,
		T.ProjectSponsorName
	FROM dropdown.ProjectSponsor T
	WHERE (T.ProjectSponsorID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.ProjectSponsorName, T.ProjectSponsorID

END
GO
--End procedure dropdown.GetProjectSponsorData

--Begin procedure eventlog.LogDocumentAction
EXEC utility.DropObject 'eventlog.LogDocumentAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDocumentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocumentEntities VARCHAR(MAX) 
	
		SELECT 
			@cDocumentEntities = COALESCE(@cDocumentEntities, '') + D.DocumentEntity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DocumentEntity'), ELEMENTS) AS DocumentEntity
			FROM document.DocumentEntity T 
			WHERE T.DocumentID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogDocumentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogDocumentActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogDocumentActionTable
		FROM document.Document T
		WHERE T.DocumentID = @EntityID
		
		ALTER TABLE #LogDocumentActionTable DROP COLUMN DocumentData
		ALTER TABLE #LogDocumentActionTable DROP COLUMN ThumbnailData

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST(('<DocumentEntities>' + ISNULL(@cDocumentEntities, '') + '</DocumentEntities>') AS XML)
			FOR XML RAW('Document'), ELEMENTS
			)
		FROM #LogDocumentActionTable T
			JOIN document.Document D ON D.DocumentID = T.DocumentID

		DROP TABLE #LogDocumentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDocumentAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.10.24
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Project'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @ProjectTerritories VARCHAR(MAX) 
	
		SELECT 
			@ProjectTerritories = COALESCE(@ProjectTerritories, '') + D.ProjectTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectTerritory'), ELEMENTS) AS ProjectTerritory
			FROM territory.ProjectTerritory T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ProjectTerritories>' + ISNULL(@ProjectTerritories, '') + '</ProjectTerritories>') AS XML)
			FOR XML RAW('Project'), ELEMENTS
			),
			0
		FROM dropdown.Project T
		WHERE T.ProjectID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			0
		FROM dropdown.Project T
		WHERE T.ProjectID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction

--Begin procedure eventlog.LogProjectSponsorAction
EXEC utility.DropObject 'eventlog.LogProjectSponsorAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.10.24
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectSponsorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ProjectSponsor'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('ProjectSponsor'), ELEMENTS
			),
			0
		FROM dropdown.ProjectSponsor T
		WHERE T.ProjectSponsorID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			0
		FROM dropdown.ProjectSponsor T
		WHERE T.ProjectSponsorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectSponsorAction
USE [HermisCloud]
GO

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Force
	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.IsForResponseDashboard,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName,
		F.UpdateDateTime,
		core.FormatDateTime(F.UpdateDateTime) AS UpdateDateTimeFormatted,
		F.WebLinks,
		FT.ForceTypeID, 
		FT.ForceTypeName,
		FT.ForceTypePermissionCode,
		FT.ForceTypeCategory
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
			AND F.ForceID = @ForceID

	--ForceEquipmentResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceFinancialResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceUnit
	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullname,
		FU.ForceUnitID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID



--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'ProjectSponsorList',
	@NewMenuItemLink = '/projectsponsor/list',
	@NewMenuItemText = 'Activity Stream Sponsors',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'ProjectSponsor.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ProjectSponsorList',
	@NewMenuItemCode = 'ProjectList',
	@NewMenuItemLink = '/project/list',
	@NewMenuItemText = 'Activity Streams',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Project.List'
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Project', 
	@DESCRIPTION='Add / edit an activity stream', 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Project.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Project', 
	@DESCRIPTION='View the list of activity streams', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Project.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Project', 
	@DESCRIPTION='View an activity stream', 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Project.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ProjectSponsor', 
	@DESCRIPTION='Add / edit an activity stream sponsor', 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ProjectSponsor.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ProjectSponsor', 
	@DESCRIPTION='View the list of activity stream sponsors', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ProjectSponsor.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ProjectSponsor', 
	@DESCRIPTION='View an activity stream sponsor', 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ProjectSponsor.View', 
	@PERMISSIONCODE=NULL;
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('ActivityStream', 'Department')
GO

DELETE MIPL
FROM core.MenuItemPermissionableLineage MIPL
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = MIPL.PermissionableLineage
	)
GO

DELETE PP
FROM person.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType D WHERE D.DocumentTypeCode = 'Reports')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCategory, DocumentTypeCode, DocumentTypeName)
	VALUES
		('Response', 'Reports', 'Response - Reports'),
		('Response', 'Media', 'Response - Media'),
		('Response', 'Images', 'Response - Images'),
		('Response', 'Other', 'Response - Other')

	END
--ENDIF
GO
--End table dropdown.DocumentType

--Begin table dropdown.ProjectSponsor
IF NOT EXISTS (SELECT 1 FROM dropdown.ProjectSponsor D WHERE D.ProjectSponsorID = 0)
	EXEC utility.InsertIdentityValue 'dropdown.ProjectSponsor', 'ProjectSponsorID', 0
--ENDIF
GO
--End table dropdown.ProjectSponsor

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 1;
EXEC permissionable.SavePermissionableGroup 'Document', 'Documents', 2;
EXEC permissionable.SavePermissionableGroup 'ForceAsset', 'Organizations & Facilities', 3;
EXEC permissionable.SavePermissionableGroup 'Insight', 'Incidents & Reports', 4;
EXEC permissionable.SavePermissionableGroup 'Activity', 'Activity', 5;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 6;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the CTM Travel Approval menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CTMTravelApproval', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.CTMTravelApproval', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the DeployAdviser menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='DeployAdviser', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.DeployAdviser', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Compliance Checker menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumComplianceChecker', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumComplianceChecker', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Staff Time Collection menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumStaffTimeCollection', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumStaffTimeCollection', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - images in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Images', @PERMISSIONCODE='Images';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - media in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - other in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - reports in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Reports', @PERMISSIONCODE='Reports';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - images in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Images', @PERMISSIONCODE='Images';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - media in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - other in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - reports in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Reports', @PERMISSIONCODE='Reports';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of Facilities', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of Organisations', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Add / edit a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Allows access to response report exports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View the list of response reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Share an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.ShareIncident', @PERMISSIONCODE='ShareIncident';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='Add / edit a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View the list of Report Updates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Situational report pdf export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Situational report aggregator export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalogue list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of consignments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of dispatched items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDispatchedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDispatchedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of distributed items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDistributedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the consignment contents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='Add / edit an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment inventory list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='InventoryList', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.InventoryList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment item list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='Add / edit an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View the list of Equipment Movements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='Add / edit an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View the equipment order list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Error message for DA users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='DAView', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.DAView', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the Humanitarian roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.HumanitarianRoster', @PERMISSIONCODE='HumanitarianRoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the SU roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.SURoster', @PERMISSIONCODE='SURoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit an activity stream', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of activity streams', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View an activity stream', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='Add / edit an activity stream sponsor', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='View the list of activity stream sponsors', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='View an activity stream sponsor', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;

--Begin update document type permissions
EXEC document.UpdateDocumentPermissions
--End update document type permissions
--End table permissionable.Permissionable

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.6 - 2018.10.29 18.16.00')
GO
--End build tracking

