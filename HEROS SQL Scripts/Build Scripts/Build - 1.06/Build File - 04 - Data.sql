
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'ProjectSponsorList',
	@NewMenuItemLink = '/projectsponsor/list',
	@NewMenuItemText = 'Activity Stream Sponsors',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'ProjectSponsor.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ProjectSponsorList',
	@NewMenuItemCode = 'ProjectList',
	@NewMenuItemLink = '/project/list',
	@NewMenuItemText = 'Activity Streams',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Project.List'
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Project', 
	@DESCRIPTION='Add / edit an activity stream', 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Project.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Project', 
	@DESCRIPTION='View the list of activity streams', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Project.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Project', 
	@DESCRIPTION='View an activity stream', 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Project.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ProjectSponsor', 
	@DESCRIPTION='Add / edit an activity stream sponsor', 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ProjectSponsor.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ProjectSponsor', 
	@DESCRIPTION='View the list of activity stream sponsors', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ProjectSponsor.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='ProjectSponsor', 
	@DESCRIPTION='View an activity stream sponsor', 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='ProjectSponsor.View', 
	@PERMISSIONCODE=NULL;
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('ActivityStream', 'Department')
GO

DELETE MIPL
FROM core.MenuItemPermissionableLineage MIPL
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = MIPL.PermissionableLineage
	)
GO

DELETE PP
FROM person.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType D WHERE D.DocumentTypeCode = 'Reports')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCategory, DocumentTypeCode, DocumentTypeName)
	VALUES
		('Response', 'Reports', 'Response - Reports'),
		('Response', 'Media', 'Response - Media'),
		('Response', 'Images', 'Response - Images'),
		('Response', 'Other', 'Response - Other')

	END
--ENDIF
GO
--End table dropdown.DocumentType

--Begin table dropdown.ProjectSponsor
IF NOT EXISTS (SELECT 1 FROM dropdown.ProjectSponsor D WHERE D.ProjectSponsorID = 0)
	EXEC utility.InsertIdentityValue 'dropdown.ProjectSponsor', 'ProjectSponsorID', 0
--ENDIF
GO
--End table dropdown.ProjectSponsor
