--Begin procedure core.GetIncidentByIncidentID
EXEC utility.DropObject 'core.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the dbo.Incident table
-- =======================================================================
CREATE PROCEDURE core.GetIncidentByIncidentID

@IncidentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentDateTime,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentID,
		I.IncidentName,
		I.IsForResponseDashboard,
		I.KeyPoints,
		I.Location.STAsText() AS Location,
		I.ProjectID,
		dropdown.GetProjectNameByProjectID(I.ProjectID) AS ProjectName,
		I.Source,
		I.Summary,
		I.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(I.TerritoryID) AS TerritoryName,
		I.Mission,
		I.ImpactDescription,
		I.MitigationEffectiveness,
		I.Lessons,
		I.UpdateDateTime,
		core.FormatDateTime(I.UpdateDateTime) AS UpdateDateTimeFormatted,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		ST.SourceTypeID,
		ST.SourceTypeName,
		SR.SourceReliabilityID,
		SR.SourceReliabilityName,
		IV.InformationValidityID,
		IV.InformationValidityName,
		II.IncidentImpactID,
		II.IncidentImpactName,
		IC.IncidentCategoryID,
		IC.IncidentCategoryName,
		IC.IncidentCategoryCode
	FROM core.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = I.SourceTypeID
		JOIN dropdown.SourceReliability SR ON SR.SourceReliabilityID = I.SourceReliabilityID
		JOIN dropdown.InformationValidity IV ON IV.InformationValidityID = I.InformationValidityID
		JOIN dropdown.IncidentImpact II ON II.IncidentImpactID = I.IncidentImpactID
		JOIN dropdown.IncidentCategory IC ON IC.IncidentCategoryID = I.IncidentCategoryID
			AND I.IncidentID = @IncidentID
	
END
GO
--End procedure core.GetIncidentByIncidentID

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentTitle,
		D.Extension,
		D.IsForResponseDashboard,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure dropdown.GetProjectByProjectID
EXEC Utility.DropObject 'dropdown.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.15
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Project
	SELECT 
		P.ProjectDescription,
		P.ProjectID,
		P.ProjectName,
		P.EndDate,
		core.FormatDate(P.EndDate) AS EndDateFormatted,
		P.IsActive,
		P.StartDate,
		core.FormatDate(P.StartDate) AS StartDateFormatted,
		PS.ProjectSponsorID,
		PS.ProjectSponsorName
	FROM dropdown.Project P
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
			AND P.ProjectID = @ProjectID

	--ProjectTerritory
	SELECT
		PT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(PT.TerritoryID) AS TerritoryName
	FROM territory.ProjectTerritory PT
	WHERE PT.ProjectID = @ProjectID
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetProjectByProjectID

--Begin procedure dropdown.GetProjectSponsorByProjectSponsorID
EXEC Utility.DropObject 'dropdown.GetProjectSponsorByProjectSponsorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.15
-- Description:	A stored procedure to return data from the dropdown.ProjectSponsor table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetProjectSponsorByProjectSponsorID

@ProjectSponsorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PS.ProjectSponsorID,
		PS.ProjectSponsorCode,
		PS.ProjectSponsorName,
		PS.IsActive
	FROM dropdown.ProjectSponsor PS
	WHERE PS.ProjectSponsorID = @ProjectSponsorID

END
GO
--End procedure dropdown.GetProjectSponsorByProjectSponsorID

--Begin procedure dropdown.GetProjectSponsorData
EXEC Utility.DropObject 'dropdown.GetProjectSponsorData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.08.14
-- Description:	A stored procedure to return data from the dropdown.ProjectSponsor table
-- ================================================================================
CREATE PROCEDURE dropdown.GetProjectSponsorData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectSponsorID,
		T.ProjectSponsorCode,
		T.ProjectSponsorName
	FROM dropdown.ProjectSponsor T
	WHERE (T.ProjectSponsorID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.ProjectSponsorName, T.ProjectSponsorID

END
GO
--End procedure dropdown.GetProjectSponsorData

--Begin procedure eventlog.LogDocumentAction
EXEC utility.DropObject 'eventlog.LogDocumentAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDocumentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@ProjectID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			@ProjectID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocumentEntities VARCHAR(MAX) 
	
		SELECT 
			@cDocumentEntities = COALESCE(@cDocumentEntities, '') + D.DocumentEntity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DocumentEntity'), ELEMENTS) AS DocumentEntity
			FROM document.DocumentEntity T 
			WHERE T.DocumentID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogDocumentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogDocumentActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogDocumentActionTable
		FROM document.Document T
		WHERE T.DocumentID = @EntityID
		
		ALTER TABLE #LogDocumentActionTable DROP COLUMN DocumentData
		ALTER TABLE #LogDocumentActionTable DROP COLUMN ThumbnailData

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			@ProjectID,
			(
			SELECT T.*,
			CAST(('<DocumentEntities>' + ISNULL(@cDocumentEntities, '') + '</DocumentEntities>') AS XML)
			FOR XML RAW('Document'), ELEMENTS
			)
		FROM #LogDocumentActionTable T
			JOIN document.Document D ON D.DocumentID = T.DocumentID

		DROP TABLE #LogDocumentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDocumentAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.10.24
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Project'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @ProjectTerritories VARCHAR(MAX) 
	
		SELECT 
			@ProjectTerritories = COALESCE(@ProjectTerritories, '') + D.ProjectTerritory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectTerritory'), ELEMENTS) AS ProjectTerritory
			FROM territory.ProjectTerritory T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ProjectTerritories>' + ISNULL(@ProjectTerritories, '') + '</ProjectTerritories>') AS XML)
			FOR XML RAW('Project'), ELEMENTS
			),
			0
		FROM dropdown.Project T
		WHERE T.ProjectID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			0
		FROM dropdown.Project T
		WHERE T.ProjectID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction

--Begin procedure eventlog.LogProjectSponsorAction
EXEC utility.DropObject 'eventlog.LogProjectSponsorAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.10.24
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectSponsorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'ProjectSponsor'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('ProjectSponsor'), ELEMENTS
			),
			0
		FROM dropdown.ProjectSponsor T
		WHERE T.ProjectSponsorID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, ProjectID)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			0
		FROM dropdown.ProjectSponsor T
		WHERE T.ProjectSponsorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectSponsorAction
USE [HermisCloud]
GO

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Force
	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.IsActive,
		F.IsForResponseDashboard,
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.ProjectID,
		dropdown.GetProjectNameByProjectID(F.ProjectID) AS ProjectName,
		F.TerritoryID, 
		territory.FormatTerritoryNameByTerritoryID(F.TerritoryID) AS TerritoryName,
		F.UpdateDateTime,
		core.FormatDateTime(F.UpdateDateTime) AS UpdateDateTimeFormatted,
		F.WebLinks,
		FT.ForceTypeID, 
		FT.ForceTypeName,
		FT.ForceTypePermissionCode,
		FT.ForceTypeCategory
	FROM force.Force F
		JOIN dropdown.ForceType FT ON FT.ForceTypeID = F.ForceTypeID
			AND F.ForceID = @ForceID

	--ForceEquipmentResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceFinancialResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceUnit
	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullname,
		FU.ForceUnitID,
		FU.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(FU.TerritoryID) AS TerritoryNameFormatted,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID


