--Begin table core.Incident
DECLARE @TableName VARCHAR(250) = 'core.Incident'

EXEC utility.AddColumn @TableName, 'IsForResponseDashboard', 'BIT', '0'
GO
--End table core.Incident

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.AddColumn @TableName, 'IsForResponseDashboard', 'BIT', '0'
GO
--End table document.Document

--Begin table dropdown.Project
DECLARE @TableName VARCHAR(250) = 'dropdown.Project'

EXEC utility.AddColumn @TableName, 'StartDate', 'DATE'
EXEC utility.AddColumn @TableName, 'EndDate', 'DATE'
EXEC utility.AddColumn @TableName, 'ProjectDescription', 'VARCHAR(500)'
EXEC utility.AddColumn @TableName, 'ProjectSponsorID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'IsActive', 'BIT', '0'
GO
--End table dropdown.Project

--Begin table dropdown.ProjectSponsor
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectSponsor'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectSponsor
	(
	ProjectSponsorID INT IDENTITY(0,1) NOT NULL,
	ProjectSponsorName VARCHAR(250),
	ProjectSponsorCode VARCHAR(50),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectSponsorID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProjectSponsor', 'ProjectSponsorName,ProjectSponsorID'
GO
--End table dropdown.ProjectSponsor

--Begin table force.Force
DECLARE @TableName VARCHAR(250) = 'force.Force'

EXEC utility.AddColumn @TableName, 'IsForResponseDashboard', 'BIT', '0'
GO
--End table force.Force
