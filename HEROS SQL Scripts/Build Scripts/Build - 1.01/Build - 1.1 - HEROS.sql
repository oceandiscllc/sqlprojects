-- File Name:	Build - 1.1 - HEROS.sql
-- Build Key:	Build - 1.1 - 2017.10.17 17.30.47

--USE HEROS
GO

-- ==============================================================================================================================
-- Schemas:
--		activityreport
--		spotreport
--
-- Tables:
--		activityreport.ActivityReport
--		activityreport.ActivityReportAsset
--		activityreport.ActivityReportForce
--		activityreport.ActivityReportIncident
--		activityreport.ActivityReportRelevantTheme
--		activityreport.ActivityReportTerritory
--		dropdown.ActivityReportType
--		dropdown.DateFilter
--		spotreport.SpotReport
--
-- Procedures:
--		activityreport.GetActivityReportByActivityReportID
--		dropdown.GetActivityReportData
--		dropdown.GetDateFilterData
--		dropdown.GetIncidentTypeData
--		spotreport.GetSpotReportBySpotReportID
--		workflow.InitializeEntityWorkflow
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--USE HEROS
GO

EXEC utility.AddSchema 'activityreport'
EXEC utility.AddSchema 'spotreport'
GO

--Begin table activityreport.ActivityReport
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReport'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReport
	(
	ActivityReportID INT NOT NULL IDENTITY(1,1),
	ActivityReportTypeID INT,
	ActivityReportTitle VARCHAR(250),
	ActivityReportStartDate DATE,
	ActivityReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX),
	ProjectID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportID'
GO
--End table activityreport.ActivityReport

--Begin table activityreport.ActivityReportAsset
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportAsset'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportAsset
	(
	ActivityReportAssetID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	AssetID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportAsset', 'ActivityReportID,AssetID'
GO
--End table activityreport.ActivityReportAsset

--Begin table activityreport.ActivityReportForce
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportForce'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportForce
	(
	ActivityReportForceID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ForceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportForceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportForce', 'ActivityReportID,ForceID'
GO
--End table activityreport.ActivityReportForce

--Begin table activityreport.ActivityReportIncident
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportIncident'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportIncident
	(
	ActivityReportIncidentID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	IncidentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportIncidentID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportIncident', 'ActivityReportID,IncidentID'
GO
--End table activityreport.ActivityReportIncident

--Begin table activityreport.ActivityReportRelevantTheme
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportRelevantTheme
	(
	ActivityReportRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	RelevantThemeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportRelevantTheme', 'ActivityReportID,RelevantThemeID'
GO
--End table activityreport.ActivityReportRelevantTheme

--Begin table activityreport.ActivityReportTerritory
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportTerritory
	(
	ActivityReportTerritoryID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportTerritory', 'ActivityReportID,TerritoryID'
GO
--End table activityreport.ActivityReportTerritory

--Begin table dropdown.ActivityReportType
DECLARE @TableName VARCHAR(250) = 'dropdown.ActivityReportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ActivityReportType
	(
	ActivityReportTypeID INT IDENTITY(0,1) NOT NULL,
	ActivityReportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ActivityReportTypeName', 'DisplayOrder,ActivityReportTypeName', 'ActivityReportTypeID'
GO
--End table dropdown.ActivityReportType

--Begin table dropdown.DateFilter
DECLARE @TableName VARCHAR(250) = 'dropdown.DateFilter'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DateFilter
	(
	DateFilterID INT IDENTITY(0,1) NOT NULL,
	DateFilterName VARCHAR(50),
	DateNumber INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DateNumber', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DateFilterID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_DateFilterName', 'DisplayOrder,DateFilterName', 'DateFilterID'
GO
--End table dropdown.DateFilter

--Begin table spotreport.SpotReport
DECLARE @TableName VARCHAR(250) = 'spotreport.SpotReport'

EXEC utility.DropObject @TableName

CREATE TABLE spotreport.SpotReport
	(
	SpotReportID INT IDENTITY(1,1) NOT NULL,
	SpotReportTitle VARCHAR(100),
	SpotReportDate DATE,
	ReportSource NVARCHAR(250),
	MINNumber INT,
	Summary NVARCHAR(MAX),
	Response NVARCHAR(MAX),
	Recommendation NVARCHAR(MAX),
	ManagementActions NVARCHAR(MAX),
	PoINTOfContactPersonID INT,
	ProjectID INT,
	UpdateDateTime DATETIME,
	SummaryMap VARBINARY(MAX),
	SummaryMapZoom INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MINNumber', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PoINTOfContactPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SummaryMapZoom', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SpotReportID'
GO
--End table spotreport.SpotReport

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE HEROS
GO


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE HEROS
GO

--Begin procedure activityreport.GetActivityReportByActivityReportID
EXEC utility.DropObject 'activityreport.GetActivityReportByActivityReportID'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the activityreport.ActivityReport table
-- ========================================================================================
CREATE PROCEDURE activityreport.GetActivityReportByActivityReportID

@ActivityReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReport', @ActivityReportID)

	--ActivityReport
	SELECT
		AR.ProjectID,
		dropdown.GetProjectNameByProjectID(AR.ProjectID) AS ProjectName,
		AR.ActivityReportID,
		AR.ActivityReportTitle,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.Summary,
		AR.ReportDetail,
		ART.ActivityReportTypeID,
		ART.ActivityReportTypeName
	FROM activityreport.ActivityReport AR
		JOIN dropdown.ActivityReportType ART ON ART.ActivityReportTypeID = AR.ActivityReportTypeID
			AND AR.ActivityReportID = @ActivityReportID
		AND AR.ProjectID = @ProjectID

	--ActivityReportAsset
	SELECT
		A.AssetID,
		A.AssetName
	FROM activityreport.ActivityReportAsset ARA
		JOIN asset.Asset A ON A.AssetID = ARA.AssetID
			AND ARA.ActivityReportID = @ActivityReportID
	ORDER BY A.AssetName, A.AssetID

	--ActivityReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM activityreport.ActivityReportForce ARF
		JOIN force.Force F ON F.ForceID = ARF.ForceID
			AND ARF.ActivityReportID = @ActivityReportID
	ORDER BY F.ForceName, F.ForceID

	--ActivityReportIncident
	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM activityreport.ActivityReportIncident ARI
		JOIN core.Incident I ON I.IncidentID = ARI.IncidentID
			AND ARI.ActivityReportID = @ActivityReportID
	ORDER BY I.IncidentName, I.IncidentID

	--ActivityReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM activityreport.ActivityReportRelevantTheme ARRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = ARRT.RelevantThemeID
			AND ARRT.ActivityReportID = @ActivityReportID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ActivityReportTerritory
	SELECT
		ART.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(ART.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM activityreport.ActivityReportTerritory ART
		JOIN territory.Territory T ON T.TerritoryID = ART.TerritoryID
			AND ART.ActivityReportID = @ActivityReportID
	ORDER BY 2, 1

	--ActivityReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'ActivityReport', @ActivityReportID

	--ActivityReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activityreport.ActivityReport AR ON AR.ActivityReportID = EL.EntityID
			AND AR.ActivityReportID = @ActivityReportID
			AND AR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'ActivityReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--ActivityReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'ActivityReport', @ActivityReportID, @nWorkflowStepNumber

END
GO
--End procedure activityreport.GetActivityReportByActivityReportID

--Begin procedure dropdown.GetActivityReportData
EXEC utility.DropObject 'dropdown.GetActivityReportData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ActivityReport table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetActivityReportData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityReportID, 
		T.ActivityReportName
	FROM dropdown.ActivityReport T
	WHERE (T.ActivityReportID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityReportName, T.ActivityReportID

END
GO
--End procedure dropdown.GetActivityReportData

--Begin procedure dropdown.GetDateFilterData
EXEC utility.DropObject 'dropdown.GetDateFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DateFilter table
-- =================================================================================
CREATE PROCEDURE dropdown.GetDateFilterData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DateFilterID, 
		T.DateFilterName
	FROM dropdown.DateFilter T
	WHERE (T.DateFilterID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DateFilterName, T.DateFilterID

END
GO
--End procedure dropdown.GetDateFilterData

--Begin procedure dropdown.GetIncidentTypeData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentTypeID,
		T.IncidentTypeCategory,
		T.IncidentTypeName,
		T.Icon
	FROM dropdown.IncidentType T
	WHERE (T.IncidentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IncidentTypeName, T.IncidentTypeID

END
GO
--End procedure dropdown.GetIncidentTypeData

--Begin procedure spotreport.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'spotreport.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE spotreport.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectID INT = (SELECT SR.ProjectID FROM spotreport.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)
	
	SELECT
		SR.PointOfContactPersonID,
		person.FormatPersonNameByPersonID(SR.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		SR.ManagementActions,
		SR.MINNumber,
		SR.ProjectID,
		dropdown.GetProjectNameByProjectID(SR.ProjectID) AS ProjectName,
		SR.Recommendation,
		SR.ReportSource,
		SR.Response,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateTimeFormatted
	FROM spotreport.SpotReport SR
	WHERE SR.SpotReportID = @SpotReportID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM spotreport.SpotReportIncident SRI
		JOIN core.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		SRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(SRT.TerritoryID) AS TerritoryName
	FROM spotreport.SpotReportTerritory SRT
	WHERE SRT.SpotReportID = @SpotReportID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'SpotReport', @SpotReportID

	EXEC workflow.GetEntityWorkflowPeople 'SpotReport', @SpotReportID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN spotreport.SpotReport SR ON SR.SpotReportID = EL.EntityID
			AND SR.SpotReportID = @SpotReportID
			AND EL.EntityTypeCode = 'SpotReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure spotreport.GetSpotReportBySpotReportID

--Begin procedure workflow.InitializeEntityWorkflow
EXEC utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

END
GO
--End procedure workflow.InitializeEntityWorkflow
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE HEROS
GO

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ActivityReport', 
	@EntityTypeName = 'Response Report', 
	@EntityTypeNamePlural = 'Response Reports',
	@SchemaName = 'activityreport', 
	@TableName = 'ActivityReport', 
	@PrimaryKeyFieldName = 'ActivityReportID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItem
GO

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dashboard',
	@NewMenuItemCode = 'Dashboard',
	@NewMenuItemLink = '/main',
	@NewMenuItemText = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-fw fa-folder',
	@NewMenuItemCode = 'DocumentList',
	@NewMenuItemLink = '/document/list',
	@NewMenuItemText = 'Reference Library',
	@PermissionableLineageList = 'Document.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'DocumentList',
	@Icon = 'fa fa-fw fa-map',
	@NewMenuItemCode = 'TerritoryList',
	@NewMenuItemLink = '/territory/list',
	@NewMenuItemText = 'Place Profiles',
	@PermissionableLineageList = 'Territory.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TerritoryList',
	@Icon = 'fa fa-fw fa-question',
	@NewMenuItemCode = 'RequestForInformationList',
	@NewMenuItemLink = '/requestforinformation/list',
	@NewMenuItemText = 'Requests For Information',
	@PermissionableLineageList = 'RequestForInformation.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'RequestForInformationList',
	@Icon = 'fa fa-fw fa-flag',
	@NewMenuItemCode = 'ForceAsset',
	@NewMenuItemText = 'Organisations & Facilities'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ForceList',
	@NewMenuItemLink = '/force/list',
	@NewMenuItemText = 'Organisations',
	@ParentMenuItemCode = 'ForceAsset',
	@PermissionableLineageList = 'Force.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ForceList',
	@NewMenuItemCode = 'AssetList',
	@NewMenuItemLink = '/asset/list',
	@NewMenuItemText = 'Facilities',
	@ParentMenuItemCode = 'ForceAsset',
	@PermissionableLineageList = 'Asset.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ForceAsset',
	@Icon = 'fa fa-fw fa-briefcase',
	@NewMenuItemCode = 'Insight',
	@NewMenuItemText = 'Incidents & Reports'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'IncidentList',
	@NewMenuItemLink = '/incident/list',
	@NewMenuItemText = 'Incidents',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'Incident.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'IncidentList',
	@NewMenuItemCode = 'SpotReportList',
	@NewMenuItemLink = '/spotreport/list',
	@NewMenuItemText = 'MIN Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'SpotReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'SpotReportList',
	@NewMenuItemCode = 'ActivityReportList',
	@NewMenuItemLink = '/activityreport/list',
	@NewMenuItemText = 'Response Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'ActivityReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ActivityReportList',
	@NewMenuItemCode = 'TrendReportList',
	@NewMenuItemLink = '/trendreport/list',
	@NewMenuItemText = 'Situational Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'TrendReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TrendReportList',
	@NewMenuItemCode = 'TrendReportAggregatorList',
	@NewMenuItemLink = '/trendreportaggregator/list',
	@NewMenuItemText = 'Situational Report Aggregator',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'TrendReportAggregator.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Insight',
	@Icon = 'fa fa-fw fa-bars',
	@NewMenuItemCode = 'Activity',
	@NewMenuItemText = 'Activity'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = 'Activity',
	@NewMenuItemCode = 'Equipment',
	@NewMenuItemText = 'Equipment'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'EquipmentCatalogList',
	@NewMenuItemLink = '/equipmentcatalog/list',
	@NewMenuItemText = 'Equipment Catalog',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentCatalog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EquipmentCatalogList',
	@NewMenuItemCode = 'EquipmentInventoryList',
	@NewMenuItemLink = '/equipmentinventory/list',
	@NewMenuItemText = 'Equipment Inventory',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentInventory.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Equipment',
	@ParentMenuItemCode = 'Activity',
	@NewMenuItemCode = 'Training',
	@NewMenuItemText = 'Training'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ModuleList',
	@NewMenuItemLink = '/Module/list',
	@NewMenuItemText = 'Modules',
	@ParentMenuItemCode = 'Training',
	@PermissionableLineageList = 'Module.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ModuleList',
	@NewMenuItemCode = 'CourseList',
	@NewMenuItemLink = '/Course/list',
	@NewMenuItemText = 'Courses',
	@ParentMenuItemCode = 'Training',
	@PermissionableLineageList = 'Course.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Training',
	@NewMenuItemCode = 'ContactList',
	@NewMenuItemLink = '/contact/list',
	@NewMenuItemText = 'Contacts',
	@ParentMenuItemCode = 'Activity',
	@PermissionableLineageList = 'Contact.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Activity',
	@Icon = 'fa fa-fw fa-cogs',
	@NewMenuItemCode = 'Admin',
	@NewMenuItemText = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@NewMenuItemLink = '/announcement/list',
	@NewMenuItemText = 'Announcements',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'EmailTemplateList',
	@NewMenuItemLink = '/emailtemplate/list',
	@NewMenuItemText = 'Email Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EmailTemplateList',
	@NewMenuItemCode = 'EventLogList',
	@NewMenuItemLink = '/eventlog/list',
	@NewMenuItemText = 'Event Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PermissionableList',
	@NewMenuItemLink = '/permissionable/list',
	@NewMenuItemText = 'Permissionables',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableList',
	@NewMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemLink = '/permissionabletemplate/list',
	@NewMenuItemText = 'Permissionable Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemCode = 'PersonList',
	@NewMenuItemLink = '/person/list',
	@NewMenuItemText = 'Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'SystemSetupList',
	@NewMenuItemLink = '/systemsetup/list',
	@NewMenuItemText = 'System Setup Keys',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'SystemSetup.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'SystemSetupList',
	@NewMenuItemCode = 'WorkflowList',
	@NewMenuItemLink = '/workflow/list',
	@NewMenuItemText = 'Workflows',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Workflow.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Activity'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'ForceAsset'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Insight'
GO
--End table core.MenuItem

--Begin table dropdown.ActivityReportType
TRUNCATE TABLE dropdown.ActivityReportType
GO

EXEC utility.InsertIdentityValue 'dropdown.ActivityReportType', 'ActivityReportTypeID', 0
GO

INSERT INTO dropdown.ActivityReportType 
	(ActivityReportTypeName, DisplayOrder) 
VALUES
	('Daily Report', 1),
	('Routine Report', 2),
	('Ad-hoc Report', 3),
	('Weekly Overview', 4),
	('Monthly Overview', 5),
	('Quarterly Overview', 6)
GO
--End table dropdown.ActivityReportType

--Begin table dropdown.DateFilter
TRUNCATE TABLE dropdown.DateFilter
GO

EXEC utility.InsertIdentityValue 'dropdown.DateFilter', 'DateFilterID', 0
GO

INSERT INTO dropdown.DateFilter 
	(DateFilterName,DateNumber,DisplayOrder)
VALUES
	('Today', 1, 1),
	('Last Week', 7, 2),
	('Last 2 Weeks', 14, 3),
	('Last 30 days', 30, 4),
	('Last 90 days', 90, 5)
GO
--End table dropdown.DateFilter

--Begin table dropdown.IncidentType
TRUNCATE TABLE dropdown.IncidentType
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentType', 'IncidentTypeID', 0
GO

INSERT INTO dropdown.IncidentType 
	(IncidentTypeCategory, IncidentTypeName, Icon, DisplayOrder) 
VALUES 
	('Biological','Epidemic','epidemic.png',16),
	('Biological','Infestation','epidemic.png',17),
	('Climatic','Drought','drought.png',13),
	('Climatic','Extreme Temperature','drought.png',14),
	('Climatic','Wildfires','drought.png',15),
	('Geophysical','Avalanch','volcano.png',4),
	('Geophysical','Earthquake','volcano.png',1),
	('Geophysical','Landslide','volcano.png',3),
	('Geophysical','Tsunamis','volcano.png',5),
	('Geophysical','Volcano','volcano.png',2),
	('Human','Conflict','conflict.png',18),
	('Human','Environmental Degradation','conflict.png',19),
	('Human','Industrial Accidents','conflict.png',21),
	('Human','Pollution','conflict.png',20),
	('Hydrological','Flood','storm_surge.png',11),
	('Hydrological','Storm Surge','storm_surge.png',12),
	('Meteorological','Blizzard','storm.png',10),
	('Meteorological','Cyclone','storm.png',7),
	('Meteorological','Hurricane','storm.png',8),
	('Meteorological','Storm','storm.png',6),
	('Meteorological','Typhoon','storm.png',9),
	('Other','Other','multicluster.png',28),
	('Security','Civil Activity','security.png',22),
	('Security','Criminality','security.png',23),
	('Security','Military Activity','security.png',25),
	('Security','Other','security.png',27),
	('Security','Security Sector Activity','security.png',26),
	('Security','Terrorism','security.png',24)
GO
--End table dropdown.IncidentType

--Begin table dropdown.RelevantTheme
TRUNCATE TABLE dropdown.RelevantTheme
GO

EXEC utility.InsertIdentityValue 'dropdown.RelevantTheme', 'RelevantThemeID', 0
GO

INSERT INTO dropdown.RelevantTheme 
	(RelevantThemeName, DisplayOrder) 
VALUES
	('Early Warning', 1),
	('Response', 2),
	('Geo Event', 3)
GO
--End table dropdown.RelevantTheme

--Begin table dropdown.ResourceProvider
TRUNCATE TABLE dropdown.ResourceProvider
GO

EXEC utility.InsertIdentityValue 'dropdown.ResourceProvider', 'ResourceProviderID', 0
GO

INSERT INTO dropdown.ResourceProvider 
	(ResourceProviderName, DisplayOrder) 
VALUES
	('Government Entities', 1),
	('Business', 2),
	('NGO and Charities', 3),
	('Private Individuals', 4)
GO
--End table dropdown.ResourceProvider

UPDATE permissionable.PermissionableGroup
SET PermissionableGroupName = 'Incidents & Reports'
WHERE PermissionableGroupCode = 'Insight'
GO
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 1;
EXEC permissionable.SavePermissionableGroup 'Document', 'Documents', 2;
EXEC permissionable.SavePermissionableGroup 'ForceAsset', 'Organizations & Facilities', 3;
EXEC permissionable.SavePermissionableGroup 'Insight', 'Incidents & Reports', 4;
EXEC permissionable.SavePermissionableGroup 'Activity', 'Activity', 5;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 6;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 11 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT11', @PERMISSIONCODE='DT11';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 12 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT12', @PERMISSIONCODE='DT12';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 13 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT13', @PERMISSIONCODE='DT13';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 21 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT21', @PERMISSIONCODE='DT21';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 22 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT22', @PERMISSIONCODE='DT22';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 23 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT23', @PERMISSIONCODE='DT23';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 31 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT31', @PERMISSIONCODE='DT31';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 32 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT32', @PERMISSIONCODE='DT32';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type document type 33 in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DT33', @PERMISSIONCODE='DT33';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of Facilities', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Governmental Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Governmental', @PERMISSIONCODE='Governmental';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit an NGO Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.NGO', @PERMISSIONCODE='NGO';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Response Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.ResponseOrganisation', @PERMISSIONCODE='ResponseOrganisation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of Organisations', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Governmental Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Governmental', @PERMISSIONCODE='Governmental';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View an NGO Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.NGO', @PERMISSIONCODE='NGO';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Response Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.ResponseOrganisation', @PERMISSIONCODE='ResponseOrganisation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Add / edit a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View the list of response reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Situational report pdf export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Situational report aggregator export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalog item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalog item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of permissionabless on the user view page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables.ShowPermissionables', @PERMISSIONCODE='ShowPermissionables';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;

--Begin update document type permissions
EXEC document.UpdateDocumentPermissions
--End update document type permissions
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.1 - 2017.10.17 17.30.47')
GO
--End build tracking

