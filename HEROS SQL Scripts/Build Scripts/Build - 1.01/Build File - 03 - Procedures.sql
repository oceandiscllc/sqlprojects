USE HEROS
GO

--Begin procedure activityreport.GetActivityReportByActivityReportID
EXEC utility.DropObject 'activityreport.GetActivityReportByActivityReportID'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the activityreport.ActivityReport table
-- ========================================================================================
CREATE PROCEDURE activityreport.GetActivityReportByActivityReportID

@ActivityReportID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ActivityReport', @ActivityReportID)

	--ActivityReport
	SELECT
		AR.ProjectID,
		dropdown.GetProjectNameByProjectID(AR.ProjectID) AS ProjectName,
		AR.ActivityReportID,
		AR.ActivityReportTitle,
		AR.ActivityReportStartDate,
		core.FormatDate(AR.ActivityReportStartDate) AS ActivityReportStartDateFormatted,
		AR.ActivityReportEndDate,
		core.FormatDate(AR.ActivityReportEndDate) AS ActivityReportEndDateFormatted,
		AR.Summary,
		AR.ReportDetail,
		ART.ActivityReportTypeID,
		ART.ActivityReportTypeName
	FROM activityreport.ActivityReport AR
		JOIN dropdown.ActivityReportType ART ON ART.ActivityReportTypeID = AR.ActivityReportTypeID
			AND AR.ActivityReportID = @ActivityReportID
		AND AR.ProjectID = @ProjectID

	--ActivityReportAsset
	SELECT
		A.AssetID,
		A.AssetName
	FROM activityreport.ActivityReportAsset ARA
		JOIN asset.Asset A ON A.AssetID = ARA.AssetID
			AND ARA.ActivityReportID = @ActivityReportID
	ORDER BY A.AssetName, A.AssetID

	--ActivityReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM activityreport.ActivityReportForce ARF
		JOIN force.Force F ON F.ForceID = ARF.ForceID
			AND ARF.ActivityReportID = @ActivityReportID
	ORDER BY F.ForceName, F.ForceID

	--ActivityReportIncident
	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM activityreport.ActivityReportIncident ARI
		JOIN core.Incident I ON I.IncidentID = ARI.IncidentID
			AND ARI.ActivityReportID = @ActivityReportID
	ORDER BY I.IncidentName, I.IncidentID

	--ActivityReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM activityreport.ActivityReportRelevantTheme ARRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = ARRT.RelevantThemeID
			AND ARRT.ActivityReportID = @ActivityReportID
	ORDER BY RT.DisplayOrder, RT.RelevantThemeName, RT.RelevantThemeID

	--ActivityReportTerritory
	SELECT
		ART.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(ART.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM activityreport.ActivityReportTerritory ART
		JOIN territory.Territory T ON T.TerritoryID = ART.TerritoryID
			AND ART.ActivityReportID = @ActivityReportID
	ORDER BY 2, 1

	--ActivityReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'ActivityReport', @ActivityReportID

	--ActivityReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN activityreport.ActivityReport AR ON AR.ActivityReportID = EL.EntityID
			AND AR.ActivityReportID = @ActivityReportID
			AND AR.ProjectID = @ProjectID
			AND EL.EntityTypeCode = 'ActivityReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--ActivityReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'ActivityReport', @ActivityReportID, @nWorkflowStepNumber

END
GO
--End procedure activityreport.GetActivityReportByActivityReportID

--Begin procedure dropdown.GetActivityReportData
EXEC utility.DropObject 'dropdown.GetActivityReportData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ActivityReport table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetActivityReportData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ActivityReportID, 
		T.ActivityReportName
	FROM dropdown.ActivityReport T
	WHERE (T.ActivityReportID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ActivityReportName, T.ActivityReportID

END
GO
--End procedure dropdown.GetActivityReportData

--Begin procedure dropdown.GetDateFilterData
EXEC utility.DropObject 'dropdown.GetDateFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.DateFilter table
-- =================================================================================
CREATE PROCEDURE dropdown.GetDateFilterData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DateFilterID, 
		T.DateFilterName
	FROM dropdown.DateFilter T
	WHERE (T.DateFilterID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DateFilterName, T.DateFilterID

END
GO
--End procedure dropdown.GetDateFilterData

--Begin procedure dropdown.GetIncidentTypeData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentTypeID,
		T.IncidentTypeCategory,
		T.IncidentTypeName,
		T.Icon
	FROM dropdown.IncidentType T
	WHERE (T.IncidentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IncidentTypeName, T.IncidentTypeID

END
GO
--End procedure dropdown.GetIncidentTypeData

--Begin procedure spotreport.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'spotreport.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to data from the spotreport.SpotReport table
-- ============================================================================
CREATE PROCEDURE spotreport.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nProjectID INT = (SELECT SR.ProjectID FROM spotreport.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)
	
	SELECT
		SR.PointOfContactPersonID,
		person.FormatPersonNameByPersonID(SR.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		SR.ManagementActions,
		SR.MINNumber,
		SR.ProjectID,
		dropdown.GetProjectNameByProjectID(SR.ProjectID) AS ProjectName,
		SR.Recommendation,
		SR.ReportSource,
		SR.Response,
		SR.SpotReportDate,
		core.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateTimeFormatted
	FROM spotreport.SpotReport SR
	WHERE SR.SpotReportID = @SpotReportID

	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM spotreport.SpotReportIncident SRI
		JOIN core.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID

	SELECT
		SRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(SRT.TerritoryID) AS TerritoryName
	FROM spotreport.SpotReportTerritory SRT
	WHERE SRT.SpotReportID = @SpotReportID
	ORDER BY 2, 1

	EXEC workflow.GetEntityWorkflowData 'SpotReport', @SpotReportID

	EXEC workflow.GetEntityWorkflowPeople 'SpotReport', @SpotReportID, @nWorkflowStepNumber

	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN spotreport.SpotReport SR ON SR.SpotReportID = EL.EntityID
			AND SR.SpotReportID = @SpotReportID
			AND EL.EntityTypeCode = 'SpotReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure spotreport.GetSpotReportBySpotReportID

--Begin procedure workflow.InitializeEntityWorkflow
EXEC utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

END
GO
--End procedure workflow.InitializeEntityWorkflow