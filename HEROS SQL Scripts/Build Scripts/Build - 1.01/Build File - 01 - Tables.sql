USE HEROS
GO

EXEC utility.AddSchema 'activityreport'
EXEC utility.AddSchema 'spotreport'
GO

--Begin table activityreport.ActivityReport
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReport'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReport
	(
	ActivityReportID INT NOT NULL IDENTITY(1,1),
	ActivityReportTypeID INT,
	ActivityReportTitle VARCHAR(250),
	ActivityReportStartDate DATE,
	ActivityReportEndDate DATE,
	Summary VARCHAR(MAX),
	ReportDetail VARCHAR(MAX),
	ProjectID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportID'
GO
--End table activityreport.ActivityReport

--Begin table activityreport.ActivityReportAsset
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportAsset'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportAsset
	(
	ActivityReportAssetID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	AssetID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportAsset', 'ActivityReportID,AssetID'
GO
--End table activityreport.ActivityReportAsset

--Begin table activityreport.ActivityReportForce
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportForce'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportForce
	(
	ActivityReportForceID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	ForceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportForceID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportForce', 'ActivityReportID,ForceID'
GO
--End table activityreport.ActivityReportForce

--Begin table activityreport.ActivityReportIncident
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportIncident'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportIncident
	(
	ActivityReportIncidentID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	IncidentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportIncidentID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportIncident', 'ActivityReportID,IncidentID'
GO
--End table activityreport.ActivityReportIncident

--Begin table activityreport.ActivityReportRelevantTheme
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportRelevantTheme
	(
	ActivityReportRelevantThemeID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	RelevantThemeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportRelevantTheme', 'ActivityReportID,RelevantThemeID'
GO
--End table activityreport.ActivityReportRelevantTheme

--Begin table activityreport.ActivityReportTerritory
DECLARE @TableName VARCHAR(250) = 'activityreport.ActivityReportTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE activityreport.ActivityReportTerritory
	(
	ActivityReportTerritoryID INT NOT NULL IDENTITY(1,1),
	ActivityReportID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ActivityReportTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ActivityReportTerritory', 'ActivityReportID,TerritoryID'
GO
--End table activityreport.ActivityReportTerritory

--Begin table dropdown.ActivityReportType
DECLARE @TableName VARCHAR(250) = 'dropdown.ActivityReportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ActivityReportType
	(
	ActivityReportTypeID INT IDENTITY(0,1) NOT NULL,
	ActivityReportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ActivityReportTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ActivityReportTypeName', 'DisplayOrder,ActivityReportTypeName', 'ActivityReportTypeID'
GO
--End table dropdown.ActivityReportType

--Begin table dropdown.DateFilter
DECLARE @TableName VARCHAR(250) = 'dropdown.DateFilter'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DateFilter
	(
	DateFilterID INT IDENTITY(0,1) NOT NULL,
	DateFilterName VARCHAR(50),
	DateNumber INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DateNumber', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DateFilterID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_DateFilterName', 'DisplayOrder,DateFilterName', 'DateFilterID'
GO
--End table dropdown.DateFilter

--Begin table spotreport.SpotReport
DECLARE @TableName VARCHAR(250) = 'spotreport.SpotReport'

EXEC utility.DropObject @TableName

CREATE TABLE spotreport.SpotReport
	(
	SpotReportID INT IDENTITY(1,1) NOT NULL,
	SpotReportTitle VARCHAR(100),
	SpotReportDate DATE,
	ReportSource NVARCHAR(250),
	MINNumber INT,
	Summary NVARCHAR(MAX),
	Response NVARCHAR(MAX),
	Recommendation NVARCHAR(MAX),
	ManagementActions NVARCHAR(MAX),
	PoINTOfContactPersonID INT,
	ProjectID INT,
	UpdateDateTime DATETIME,
	SummaryMap VARBINARY(MAX),
	SummaryMapZoom INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MINNumber', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PoINTOfContactPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SummaryMapZoom', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SpotReportID'
GO
--End table spotreport.SpotReport
