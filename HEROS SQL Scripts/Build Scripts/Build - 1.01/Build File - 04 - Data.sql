USE HEROS
GO

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'ActivityReport', 
	@EntityTypeName = 'Response Report', 
	@EntityTypeNamePlural = 'Response Reports',
	@SchemaName = 'activityreport', 
	@TableName = 'ActivityReport', 
	@PrimaryKeyFieldName = 'ActivityReportID'
GO
--End table core.EntityType

--Begin table core.MenuItem
TRUNCATE TABLE core.MenuItem
GO

TRUNCATE TABLE core.MenuItemPermissionableLineage
GO

EXEC core.MenuItemAddUpdate
	@Icon = 'fa fa-fw fa-dashboard',
	@NewMenuItemCode = 'Dashboard',
	@NewMenuItemLink = '/main',
	@NewMenuItemText = 'Dashboard'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Dashboard',
	@Icon = 'fa fa-fw fa-folder',
	@NewMenuItemCode = 'DocumentList',
	@NewMenuItemLink = '/document/list',
	@NewMenuItemText = 'Reference Library',
	@PermissionableLineageList = 'Document.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'DocumentList',
	@Icon = 'fa fa-fw fa-map',
	@NewMenuItemCode = 'TerritoryList',
	@NewMenuItemLink = '/territory/list',
	@NewMenuItemText = 'Place Profiles',
	@PermissionableLineageList = 'Territory.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TerritoryList',
	@Icon = 'fa fa-fw fa-question',
	@NewMenuItemCode = 'RequestForInformationList',
	@NewMenuItemLink = '/requestforinformation/list',
	@NewMenuItemText = 'Requests For Information',
	@PermissionableLineageList = 'RequestForInformation.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'RequestForInformationList',
	@Icon = 'fa fa-fw fa-flag',
	@NewMenuItemCode = 'ForceAsset',
	@NewMenuItemText = 'Organisations & Facilities'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ForceList',
	@NewMenuItemLink = '/force/list',
	@NewMenuItemText = 'Organisations',
	@ParentMenuItemCode = 'ForceAsset',
	@PermissionableLineageList = 'Force.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ForceList',
	@NewMenuItemCode = 'AssetList',
	@NewMenuItemLink = '/asset/list',
	@NewMenuItemText = 'Facilities',
	@ParentMenuItemCode = 'ForceAsset',
	@PermissionableLineageList = 'Asset.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ForceAsset',
	@Icon = 'fa fa-fw fa-briefcase',
	@NewMenuItemCode = 'Insight',
	@NewMenuItemText = 'Incidents & Reports'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'IncidentList',
	@NewMenuItemLink = '/incident/list',
	@NewMenuItemText = 'Incidents',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'Incident.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'IncidentList',
	@NewMenuItemCode = 'SpotReportList',
	@NewMenuItemLink = '/spotreport/list',
	@NewMenuItemText = 'MIN Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'SpotReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'SpotReportList',
	@NewMenuItemCode = 'ActivityReportList',
	@NewMenuItemLink = '/activityreport/list',
	@NewMenuItemText = 'Response Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'ActivityReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ActivityReportList',
	@NewMenuItemCode = 'TrendReportList',
	@NewMenuItemLink = '/trendreport/list',
	@NewMenuItemText = 'Situational Reports',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'TrendReport.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'TrendReportList',
	@NewMenuItemCode = 'TrendReportAggregatorList',
	@NewMenuItemLink = '/trendreportaggregator/list',
	@NewMenuItemText = 'Situational Report Aggregator',
	@ParentMenuItemCode = 'Insight',
	@PermissionableLineageList = 'TrendReportAggregator.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Insight',
	@Icon = 'fa fa-fw fa-bars',
	@NewMenuItemCode = 'Activity',
	@NewMenuItemText = 'Activity'
GO

EXEC core.MenuItemAddUpdate
	@ParentMenuItemCode = 'Activity',
	@NewMenuItemCode = 'Equipment',
	@NewMenuItemText = 'Equipment'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'EquipmentCatalogList',
	@NewMenuItemLink = '/equipmentcatalog/list',
	@NewMenuItemText = 'Equipment Catalog',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentCatalog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EquipmentCatalogList',
	@NewMenuItemCode = 'EquipmentInventoryList',
	@NewMenuItemLink = '/equipmentinventory/list',
	@NewMenuItemText = 'Equipment Inventory',
	@ParentMenuItemCode = 'Equipment',
	@PermissionableLineageList = 'EquipmentInventory.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Equipment',
	@ParentMenuItemCode = 'Activity',
	@NewMenuItemCode = 'Training',
	@NewMenuItemText = 'Training'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'ModuleList',
	@NewMenuItemLink = '/Module/list',
	@NewMenuItemText = 'Modules',
	@ParentMenuItemCode = 'Training',
	@PermissionableLineageList = 'Module.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ModuleList',
	@NewMenuItemCode = 'CourseList',
	@NewMenuItemLink = '/Course/list',
	@NewMenuItemText = 'Courses',
	@ParentMenuItemCode = 'Training',
	@PermissionableLineageList = 'Course.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Training',
	@NewMenuItemCode = 'ContactList',
	@NewMenuItemLink = '/contact/list',
	@NewMenuItemText = 'Contacts',
	@ParentMenuItemCode = 'Activity',
	@PermissionableLineageList = 'Contact.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Activity',
	@Icon = 'fa fa-fw fa-cogs',
	@NewMenuItemCode = 'Admin',
	@NewMenuItemText = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@NewMenuItemCode = 'AnnouncementList',
	@NewMenuItemLink = '/announcement/list',
	@NewMenuItemText = 'Announcements',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Announcement.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'AnnouncementList',
	@NewMenuItemCode = 'EmailTemplateList',
	@NewMenuItemLink = '/emailtemplate/list',
	@NewMenuItemText = 'Email Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EmailTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EmailTemplateList',
	@NewMenuItemCode = 'EventLogList',
	@NewMenuItemLink = '/eventlog/list',
	@NewMenuItemText = 'Event Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'EventLog.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PermissionableList',
	@NewMenuItemLink = '/permissionable/list',
	@NewMenuItemText = 'Permissionables',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Permissionable.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableList',
	@NewMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemLink = '/permissionabletemplate/list',
	@NewMenuItemText = 'Permissionable Templates',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PermissionableTemplate.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PermissionableTemplateList',
	@NewMenuItemCode = 'PersonList',
	@NewMenuItemLink = '/person/list',
	@NewMenuItemText = 'Users',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Person.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'SystemSetupList',
	@NewMenuItemLink = '/systemsetup/list',
	@NewMenuItemText = 'System Setup Keys',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'SystemSetup.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'SystemSetupList',
	@NewMenuItemCode = 'WorkflowList',
	@NewMenuItemLink = '/workflow/list',
	@NewMenuItemText = 'Workflows',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'Workflow.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Activity'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'ForceAsset'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Insight'
GO
--End table core.MenuItem

--Begin table dropdown.ActivityReportType
TRUNCATE TABLE dropdown.ActivityReportType
GO

EXEC utility.InsertIdentityValue 'dropdown.ActivityReportType', 'ActivityReportTypeID', 0
GO

INSERT INTO dropdown.ActivityReportType 
	(ActivityReportTypeName, DisplayOrder) 
VALUES
	('Daily Report', 1),
	('Routine Report', 2),
	('Ad-hoc Report', 3),
	('Weekly Overview', 4),
	('Monthly Overview', 5),
	('Quarterly Overview', 6)
GO
--End table dropdown.ActivityReportType

--Begin table dropdown.DateFilter
TRUNCATE TABLE dropdown.DateFilter
GO

EXEC utility.InsertIdentityValue 'dropdown.DateFilter', 'DateFilterID', 0
GO

INSERT INTO dropdown.DateFilter 
	(DateFilterName,DateNumber,DisplayOrder)
VALUES
	('Today', 1, 1),
	('Last Week', 7, 2),
	('Last 2 Weeks', 14, 3),
	('Last 30 days', 30, 4),
	('Last 90 days', 90, 5)
GO
--End table dropdown.DateFilter

--Begin table dropdown.IncidentType
TRUNCATE TABLE dropdown.IncidentType
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentType', 'IncidentTypeID', 0
GO

INSERT INTO dropdown.IncidentType 
	(IncidentTypeCategory, IncidentTypeName, Icon, DisplayOrder) 
VALUES 
	('Biological','Epidemic','epidemic.png',16),
	('Biological','Infestation','epidemic.png',17),
	('Climatic','Drought','drought.png',13),
	('Climatic','Extreme Temperature','drought.png',14),
	('Climatic','Wildfires','drought.png',15),
	('Geophysical','Avalanch','volcano.png',4),
	('Geophysical','Earthquake','volcano.png',1),
	('Geophysical','Landslide','volcano.png',3),
	('Geophysical','Tsunamis','volcano.png',5),
	('Geophysical','Volcano','volcano.png',2),
	('Human','Conflict','conflict.png',18),
	('Human','Environmental Degradation','conflict.png',19),
	('Human','Industrial Accidents','conflict.png',21),
	('Human','Pollution','conflict.png',20),
	('Hydrological','Flood','storm_surge.png',11),
	('Hydrological','Storm Surge','storm_surge.png',12),
	('Meteorological','Blizzard','storm.png',10),
	('Meteorological','Cyclone','storm.png',7),
	('Meteorological','Hurricane','storm.png',8),
	('Meteorological','Storm','storm.png',6),
	('Meteorological','Typhoon','storm.png',9),
	('Other','Other','multicluster.png',28),
	('Security','Civil Activity','security.png',22),
	('Security','Criminality','security.png',23),
	('Security','Military Activity','security.png',25),
	('Security','Other','security.png',27),
	('Security','Security Sector Activity','security.png',26),
	('Security','Terrorism','security.png',24)
GO
--End table dropdown.IncidentType

--Begin table dropdown.RelevantTheme
TRUNCATE TABLE dropdown.RelevantTheme
GO

EXEC utility.InsertIdentityValue 'dropdown.RelevantTheme', 'RelevantThemeID', 0
GO

INSERT INTO dropdown.RelevantTheme 
	(RelevantThemeName, DisplayOrder) 
VALUES
	('Early Warning', 1),
	('Response', 2),
	('Geo Event', 3)
GO
--End table dropdown.RelevantTheme

--Begin table dropdown.ResourceProvider
TRUNCATE TABLE dropdown.ResourceProvider
GO

EXEC utility.InsertIdentityValue 'dropdown.ResourceProvider', 'ResourceProviderID', 0
GO

INSERT INTO dropdown.ResourceProvider 
	(ResourceProviderName, DisplayOrder) 
VALUES
	('Government Entities', 1),
	('Business', 2),
	('NGO and Charities', 3),
	('Private Individuals', 4)
GO
--End table dropdown.ResourceProvider

UPDATE permissionable.PermissionableGroup
SET PermissionableGroupName = 'Incidents & Reports'
WHERE PermissionableGroupCode = 'Insight'
GO