/* Build File - 03 - Procedures - Dropdown */
USE HEROS
GO

--Begin procedure dropdown.GetAssetCategoryData
EXEC Utility.DropObject 'dropdown.GetAssetCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.AssetCategory table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetAssetCategoryData

@IncludeZero BIT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetCategoryID,
		T.AssetCategoryCode,
		T.AssetCategoryName
	FROM dropdown.AssetCategory T
		JOIN 
			(
			SELECT PARSENAME(PP.PermissionableLineage, 1) AS AssetCategoryCode
			FROM person.PersonPermissionable PP
			WHERE PP.PersonID = @PersonID
				AND PP.PermissionableLineage LIKE 'Asset.AddUpdate.%'
			) TC ON TC.AssetCategoryCode = T.AssetCategoryCode
			AND (T.AssetCategoryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetCategoryName, T.AssetCategoryID

END
GO
--End procedure dropdown.GetAssetCategoryData

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID,
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
	WHERE (T.AssetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetContactStatusData
EXEC Utility.DropObject 'dropdown.GetContactStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.ContactStatus table
-- =================================================================================
CREATE PROCEDURE dropdown.GetContactStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactStatusID,
		T.ContactStatusName
	FROM dropdown.ContactStatus T
	WHERE (T.ContactStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactStatusName, T.ContactStatusID

END
GO
--End procedure dropdown.GetContactStatusData

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID,
		T.ContactTypeName
	FROM dropdown.ContactType T
	WHERE (T.ContactTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		ET.EntityTypeCode AS ControllerCode,
		ET.EntityTypeName AS ControllerName
	FROM permissionable.Permissionable P
		JOIN core.EntityType ET ON ET.EntityTypeCode = P.ControllerName
	ORDER BY ET.EntityTypeName

END
GO
--End procedure dropdown.GetControllerData

--Begin table dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID,
		T.ISOCountryCode2,
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin table dropdown.GetCountryCallingCodeData
EXEC Utility.DropObject 'dropdown.GetCountryCallingCodeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.CountryCallingCode table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCountryCallingCodeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryCallingCodeID,
		C.CountryName + ' (' + CAST(T.CountryCallingCode AS VARCHAR(5)) + ')' AS CountryCallingCodeName
	FROM dropdown.CountryCallingCode T
		JOIN dropdown.Country C ON C.ISOCountryCode2 = T.ISOCountryCode2
			AND (T.CountryCallingCodeID > 0 OR @IncludeZero = 1)
			AND C.IsActive = 1
	ORDER BY C.DisplayOrder, C.CountryName, T.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryCallingCodeData

--Begin procedure dropdown.GetCurrencyData
EXEC Utility.DropObject 'dropdown.GetCurrencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.Currency table
-- =================================================================================
CREATE PROCEDURE dropdown.GetCurrencyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CurrencyID,
		T.CurrencyName,
		T.ISOCurrencyCode
	FROM dropdown.Currency T
	WHERE (T.CurrencyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CurrencyName, T.CurrencyID

END
GO
--End procedure dropdown.GetCurrencyData

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentTypeID, 
		T.DocumentTypeCategory,
		T.DocumentTypeCode,
		T.DocumentTypeName
	FROM dropdown.DocumentType T
	WHERE (T.DocumentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DocumentTypeCategoryDisplayOrder, T.DocumentTypeCategory, T.DocumentTypeDisplayOrder, T.DocumentTypeName, T.DocumentTypeID

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetEntityTypeNameData
EXEC Utility.DropObject 'dropdown.GetEntityTypeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.26
-- Description:	A stored procedure to return data from the dbo.EntityTypeName table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEntityTypeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EntityTypeCode,
		T.EntityTypeName
	FROM core.EntityType T
	ORDER BY T.EntityTypeName, T.EntityTypeCode

END
GO
--End procedure dropdown.GetEntityTypeNameData

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		eventlog.getEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin procedure dropdown.GetForceCategoryData
EXEC Utility.DropObject 'dropdown.GetForceCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.ForceCategory table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetForceCategoryData

@IncludeZero BIT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ForceCategoryID,
		T.ForceCategoryCode,
		T.ForceCategoryName
	FROM dropdown.ForceCategory T
		JOIN 
			(
			SELECT PARSENAME(PP.PermissionableLineage, 1) AS ForceCategoryCode
			FROM person.PersonPermissionable PP
			WHERE PP.PersonID = @PersonID
				AND PP.PermissionableLineage LIKE 'Force.AddUpdate.%'
			) TC ON TC.ForceCategoryCode = T.ForceCategoryCode
			AND (T.ForceCategoryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ForceCategoryName, T.ForceCategoryID

END
GO
--End procedure dropdown.GetForceCategoryData

--Begin procedure dropdown.GetForceTypeData
EXEC Utility.DropObject 'dropdown.GetForceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.ForceType table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetForceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ForceTypeID,
		T.ForceTypeName,
		T.HexColor
	FROM dropdown.ForceType T
	WHERE (T.ForceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ForceTypeName, T.ForceTypeID

END
GO
--End procedure dropdown.GetForceTypeData

--Begin procedure dropdown.GetFundingSourceData
EXEC Utility.DropObject 'dropdown.GetFundingSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.FundingSource table
-- =================================================================================
CREATE PROCEDURE dropdown.GetFundingSourceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FundingSourceID,
		T.FundingSourceName
	FROM dropdown.FundingSource T
	WHERE (T.FundingSourceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FundingSourceName, T.FundingSourceID

END
GO
--End procedure dropdown.GetFundingSourceData

--Begin procedure dropdown.GetImpactDecisionData
EXEC Utility.DropObject 'dropdown.GetImpactDecisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.ImpactDecision table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetImpactDecisionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImpactDecisionID,
		T.ImpactDecisionName,
		T.HexColor
	FROM dropdown.ImpactDecision T
	WHERE (T.ImpactDecisionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImpactDecisionName, T.ImpactDecisionID

END
GO
--End procedure dropdown.GetImpactDecisionData

--Begin procedure dropdown.GetIncidentCategoryData
EXEC Utility.DropObject 'dropdown.GetIncidentCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.IncidentCategory table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetIncidentCategoryData

@IncludeZero BIT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentCategoryID,
		T.IncidentCategoryCode,
		T.IncidentCategoryName
	FROM dropdown.IncidentCategory T
		JOIN 
			(
			SELECT PARSENAME(PP.PermissionableLineage, 1) AS IncidentCategoryCode
			FROM person.PersonPermissionable PP
			WHERE PP.PersonID = @PersonID
				AND PP.PermissionableLineage LIKE 'Incident.AddUpdate.%'
			) TC ON TC.IncidentCategoryCode = T.IncidentCategoryCode
			AND (T.IncidentCategoryID > 0 OR @IncludeZero = 1)
			AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IncidentCategoryName, T.IncidentCategoryID

END
GO
--End procedure dropdown.GetIncidentCategoryData

--Begin procedure dropdown.GetIncidentImpactData
EXEC Utility.DropObject 'dropdown.GetIncidentImpactData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.IncidentImpact table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetIncidentImpactData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentImpactID,
		T.IncidentImpactName
	FROM dropdown.IncidentImpact T
	WHERE (T.IncidentImpactID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IncidentImpactName, T.IncidentImpactID

END
GO
--End procedure dropdown.GetIncidentImpactData

--Begin procedure dropdown.GetIncidentTypeData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentTypeID, 
		T.IncidentTypeCategory,
		T.IncidentTypeName,
		T.Icon
	FROM dropdown.IncidentType T
	WHERE (T.IncidentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.IncidentTypeCategory,T.DisplayOrder, T.IncidentTypeName, T.IncidentTypeID

END
GO
--End procedure dropdown.GetIncidentTypeData

--Begin procedure dropdown.GetInformationValidityData
EXEC Utility.DropObject 'dropdown.GetInformationValidityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.InformationValidity table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetInformationValidityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InformationValidityID,
		T.InformationValidityName
	FROM dropdown.InformationValidity T
	WHERE (T.InformationValidityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InformationValidityName, T.InformationValidityID

END
GO
--End procedure dropdown.GetInformationValidityData

--Begin procedure dropdown.GetISOCountryCodeByCountryCallingCodeID
EXEC utility.DropObject 'dropdown.GetISOCountryCodeByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dropdown.GetISOCountryCodeByCountryCallingCodeID

@CountryCallingCodeID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CCC.ISOCountryCode2,
		CCC.CountryCallingCode
	FROM dropdown.CountryCallingCode CCC
	WHERE CCC.CountryCallingCodeID = @CountryCallingCodeID

END
GO
--End procedure dropdown.GetISOCountryCodeByCountryCallingCodeID

--Begin procedure dropdown.GetLearnerProfileTypeData
EXEC Utility.DropObject 'dropdown.GetLearnerProfileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.LearnerProfileType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetLearnerProfileTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LearnerProfileTypeID,
		T.LearnerProfileTypeName
	FROM dropdown.LearnerProfileType T
	WHERE (T.LearnerProfileTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LearnerProfileTypeName, T.LearnerProfileTypeID

END
GO
--End procedure dropdown.GetLearnerProfileTypeData

--Begin procedure dropdown.GetMethodData
EXEC Utility.DropObject 'dropdown.GetMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.MethodName
	FROM permissionable.Permissionable T
	ORDER BY T.MethodName

END
GO
--End procedure dropdown.GetMethodData

--Begin procedure dropdown.GetModuleTypeData
EXEC Utility.DropObject 'dropdown.GetModuleTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ModuleType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetModuleTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ModuleTypeID,
		T.ModuleTypeName
	FROM dropdown.ModuleType T
	WHERE (T.ModuleTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ModuleTypeName, T.ModuleTypeID

END
GO
--End procedure dropdown.GetModuleTypeData

--Begin table dropdown.GetPasswordSecurityQuestionData
EXEC Utility.DropObject 'dropdown.GetPasswordSecurityQuestionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the dropdown.PasswordSecurityQuestion table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetPasswordSecurityQuestionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PasswordSecurityQuestionID,
		T.GroupID,
		T.PasswordSecurityQuestionName
	FROM dropdown.PasswordSecurityQuestion T
	WHERE (T.PasswordSecurityQuestionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.GroupID, T.DisplayOrder, T.PasswordSecurityQuestionName, T.PasswordSecurityQuestionID

END
GO
--End procedure dropdown.GetPasswordSecurityQuestionData

--Begin procedure dropdown.GetProgramTypeData
EXEC Utility.DropObject 'dropdown.GetProgramTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the dropdown.ProgramType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetProgramTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProgramTypeID,
		T.ProgramTypeName
	FROM dropdown.ProgramType T
	WHERE (T.ProgramTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProgramTypeName, T.ProgramTypeID

END
GO
--End procedure dropdown.GetProgramTypeData

--Begin procedure dropdown.GetProjectData
EXEC Utility.DropObject 'dropdown.GetProjectData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetProjectData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectID, 
		T.ProjectName
	FROM dropdown.Project T
	WHERE (T.ProjectID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectName, T.ProjectID

END
GO
--End procedure dropdown.GetProjectData

--Begin procedure dropdown.GetRelevantThemeData
EXEC Utility.DropObject 'dropdown.GetRelevantThemeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.RelevantTheme table
-- =================================================================================
CREATE PROCEDURE dropdown.GetRelevantThemeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RelevantThemeID,
		T.RelevantThemeName
	FROM dropdown.RelevantTheme T
	WHERE (T.RelevantThemeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RelevantThemeName, T.RelevantThemeID

END
GO
--End procedure dropdown.GetRelevantThemeData

--Begin procedure dropdown.GetRequestForInformationResultTypeData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationResultTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationResultType table
-- ======================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationResultTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationResultTypeID, 
		T.RequestForInformationResultTypeCode,
		T.RequestForInformationResultTypeName
	FROM dropdown.RequestForInformationResultType T
	WHERE (T.RequestForInformationResultTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationResultTypeName, T.RequestForInformationResultTypeID

END
GO
--End procedure dropdown.GetRequestForInformationResultTypeData

--Begin procedure dropdown.GetRequestForInformationStatusData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationStatus table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationStatusID, 
		T.RequestForInformationStatusCode,
		T.RequestForInformationStatusName
	FROM dropdown.RequestForInformationStatus T
	WHERE (T.RequestForInformationStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationStatusName, T.RequestForInformationStatusID

END
GO
--End procedure dropdown.GetRequestForInformationStatusData

--Begin procedure dropdown.GetResourceProviderData
EXEC Utility.DropObject 'dropdown.GetResourceProviderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.ResourceProvider table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetResourceProviderData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ResourceProviderID, 
		T.ResourceProviderName
	FROM dropdown.ResourceProvider T
	WHERE (T.ResourceProviderID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ResourceProviderName, T.ResourceProviderID

END
GO
--End procedure dropdown.GetResourceProviderData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID,
		T.RoleCode,
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSourceReliabilityData
EXEC Utility.DropObject 'dropdown.GetSourceReliabilityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.SourceReliability table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetSourceReliabilityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceReliabilityID,
		T.SourceReliabilityName
	FROM dropdown.SourceReliability T
	WHERE (T.SourceReliabilityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceReliabilityName, T.SourceReliabilityID

END
GO
--End procedure dropdown.GetSourceReliabilityData

--Begin procedure dropdown.GetSourceTypeData
EXEC Utility.DropObject 'dropdown.GetSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.SourceType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetSourceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceTypeID,
		T.SourceTypeName
	FROM dropdown.SourceType T
	WHERE (T.SourceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceTypeName, T.SourceTypeID

END
GO
--End procedure dropdown.GetSourceTypeData

--Begin procedure dropdown.GetStatusChangeData
EXEC Utility.DropObject 'dropdown.GetStatusChangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.StatusChange table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetStatusChangeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusChangeID,
		T.StatusChangeName,
		T.HexColor
	FROM dropdown.StatusChange T
	WHERE (T.StatusChangeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusChangeName, T.StatusChangeID

END
GO
--End procedure dropdown.GetStatusChangeData

--Begin procedure dropdown.GetTerritoryStatusData
EXEC Utility.DropObject 'dropdown.GetTerritoryStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to return data from the dropdown.TerritoryStatus table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetTerritoryStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryStatusID, 
		T.TerritoryStatusCode,
		T.TerritoryStatusName
	FROM dropdown.TerritoryStatus T
	WHERE (T.TerritoryStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TerritoryStatusName, T.TerritoryStatusID

END
GO
--End procedure dropdown.GetTerritoryStatusData

--Begin procedure dropdown.GetTerritoryTypeData
EXEC Utility.DropObject 'dropdown.GetTerritoryTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.TerritoryType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetTerritoryTypeData

@ImplementerCode VARCHAR(50),
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TerritoryTypeID,
		T.TerritoryTypeCode,
		T.TerritoryTypeName
	FROM dropdown.TerritoryType T
	ORDER BY T.TerritoryTypeName, T.TerritoryTypeID

END
GO
--End procedure dropdown.GetTerritoryTypeData

--Begin procedure dropdown.GetUnitTypeData
EXEC Utility.DropObject 'dropdown.GetUnitTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.26
-- Description:	A stored procedure to return data from the dropdown.UnitType table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetUnitTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.UnitTypeID,
		T.UnitTypeName
	FROM dropdown.UnitType T
	WHERE (T.UnitTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.UnitTypeName, T.UnitTypeID

END
GO
--End procedure dropdown.GetUnitTypeData