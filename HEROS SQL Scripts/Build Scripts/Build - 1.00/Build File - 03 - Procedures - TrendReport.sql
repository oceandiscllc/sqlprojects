/* Build File - 03 - Procedures - Activity & TrendReport */
USE HEROS
GO

--Begin procedure trendreport.GetTrendReportByTrendReportID
EXEC Utility.DropObject 'trendreport.GetTrendReportByTrendReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Kevin Ross
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the trendreport.TrendReport table
-- ==================================================================================
CREATE PROCEDURE trendreport.GetTrendReportByTrendReportID

@TrendReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('TrendReport', @TrendReportID)

	--TrendReport
	SELECT
		TR.ProjectID,
		dropdown.GetProjectNameByProjectID(TR.ProjectID) AS ProjectName,
		TR.TrendReportID,
		TR.TrendReportTitle,
		TR.TrendReportStartDate,
		core.FormatDate(TR.TrendReportStartDate) AS TrendReportStartDateFormatted,
		TR.TrendReportEndDate,
		core.FormatDate(TR.TrendReportEndDate) AS TrendReportEndDateFormatted,
		TR.Summary,
		TR.ReportDetail,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("TR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		TR.SummaryMapZoom
	FROM trendreport.TrendReport TR
	WHERE TR.TrendReportID = @TrendReportID

	--TrendReportAsset
	SELECT
		A.AssetID,
		A.AssetName
	FROM trendreport.TrendReportAsset TRA
		JOIN asset.Asset A ON A.AssetID = TRA.AssetID
			AND TRA.TrendReportID = @TrendReportID
	ORDER BY A.AssetName, A.AssetID

	--TrendReportForce
	SELECT
		F.ForceID,
		F.ForceName
	FROM trendreport.TrendReportForce TRF
		JOIN force.Force F ON F.ForceID = TRF.ForceID
			AND TRF.TrendReportID = @TrendReportID
	ORDER BY F.ForceName, F.ForceID

	--TrendReportIncident
	SELECT
		I.IncidentID,
		core.FormatDateTime(I.IncidentDateTime) AS IncidentDateTimeFormatted,
		I.IncidentName
	FROM trendreport.TrendReportIncident TRI
		JOIN core.Incident I ON I.IncidentID = TRI.IncidentID
			AND TRI.TrendReportID = @TrendReportID
	ORDER BY I.IncidentName, I.IncidentID

	--TrendReportRelevantTheme
	SELECT
		RT.RelevantThemeID,
		RT.RelevantThemeName
	FROM trendreport.TrendReportRelevantTheme TRRT
		JOIN dropdown.RelevantTheme RT ON RT.RelevantThemeID = TRRT.RelevantThemeID
			AND TRRT.TrendReportID = @TrendReportID
	ORDER BY RT.RelevantThemeName, RT.RelevantThemeID

	--TrendReportTerritory
	SELECT
		TRT.TerritoryID,
		territory.FormatTerritoryNameByTerritoryID(TRT.TerritoryID) AS TerritoryName,
		T.TerritoryTypeCode
	FROM trendreport.TrendReportTerritory TRT
		JOIN territory.Territory T ON T.TerritoryID = TRT.TerritoryID
			AND TRT.TrendReportID = @TrendReportID
	ORDER BY 2, 1

	--TrendReportWorkflowData
	EXEC workflow.GetEntityWorkflowData 'TrendReport', @TrendReportID

	--TrendReportWorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Trend Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Trend Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Trend Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Trend Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
		JOIN trendreport.TrendReport TR ON TR.TrendReportID = EL.EntityID
			AND TR.TrendReportID = @TrendReportID
			AND EL.EntityTypeCode = 'TrendReport'
			AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

	--TrendReportWorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'TrendReport', @TrendReportID, @nWorkflowStepNumber

END
GO
--End procedure trendreport.GetTrendReportByTrendReportID
