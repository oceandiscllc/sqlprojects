/* Build File - 04 - Data - Dropdown */
USE HEROS
GO

--Begin table dropdown.AssetCategory
TRUNCATE TABLE dropdown.AssetCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.AssetCategory', 'AssetCategoryID', 0
GO

INSERT INTO dropdown.AssetCategory 
	(AssetCategoryCode, AssetCategoryName, DisplayOrder) 
VALUES
	('StockLocation', 'Stock Location', 1),
	('ResponseFacility', 'Response Facility', 2),
	('LocalFacility', 'Local Facility', 3)
GO
--End table dropdown.AssetCategory

--Begin table dropdown.AssetType
TRUNCATE TABLE dropdown.AssetType
GO

EXEC utility.InsertIdentityValue 'dropdown.AssetType', 'AssetTypeID', 0
GO

INSERT INTO dropdown.AssetType 
	(AssetTypeName, Icon) 
VALUES
	('HQ', 'asset-hq.png'),
	('Border Infrastructure', 'asset-military-logistic.png'),
	('Educational Infrastructure', 'asset-military-logistic.png'),
	('Water and Waste Infrastructure', 'asset-military-logistic.png'),
	('Media Office', 'asset-media-office.png'),
	('Logistic Hub', 'asset-military-logistic.png'),
	('Medical Facility', 'asset-military-medical.png'),
	('Communication Facility', 'asset-military-communications.png'),
	('Airport', 'asset-airport.png'),
	('Oil and Gas Field', 'asset-oil-and-gas-field.png'),
	('Police Statio', 'asset-police-station.png'),
	('Power Plant', 'asset-power-plant.png')
--End table dropdown.AssetType

--Begin table dropdown.ContactStatus
TRUNCATE TABLE dropdown.ContactStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactStatus', 'ContactStatusID', 0
GO

INSERT INTO dropdown.ContactStatus 
	(ContactStatusName) 
VALUES
	('Active'),
	('Active - Mobile')
GO
--End table dropdown.ContactStatus

--Begin table dropdown.ContactType
TRUNCATE TABLE dropdown.ContactType
GO

EXEC utility.InsertIdentityValue 'dropdown.ContactType', 'ContactTypeID', 0
GO

INSERT INTO dropdown.ContactType 
	(ContactTypeName) 
VALUES
	('Commander'),
	('Deputy'),
	('Stringer'),
	('Media Officer'),
	('Point of Contact'),
	('Staff/Partner'),
	('Beneficiary'),
	('Credible Voice'),
	('Key Influencer')
GO
--End table dropdown.ContactType

--Begin table dropdown.Country
TRUNCATE TABLE dropdown.Country
GO

EXEC utility.InsertIdentityValue 'dropdown.Country', 'CountryID', 0
GO

INSERT INTO dropdown.Country 
	(ISOCountryCode2, ISOCountryCode3, CountryName, DisplayOrder) 
VALUES
	('AD', 'AND', 'Andorra', 100),
	('AE', 'ARE', 'United Arab Emirates', 100),
	('AF', 'AFG', 'Afghanistan', 100),
	('AG', 'ATG', 'Antigua and Barbuda', 100),
	('AI', 'AIA', 'Anguilla', 100),
	('AL', 'ALB', 'Albania', 100),
	('AM', 'ARM', 'Armenia', 100),
	('AO', 'AGO', 'Angola', 100),
	('AR', 'ARG', 'Argentina', 100),
	('AS', 'ASM', 'American Samoa', 100),
	('AT', 'AUT', 'Austria', 100),
	('AU', 'AUS', 'Australia', 100),
	('AW', 'ABW', 'Aruba', 100),
	('AX', 'ALA', 'Åland Islands', 100),
	('AZ', 'AZE', 'Azerbaijan', 100),
	('BA', 'BIH', 'Bosnia and Herzegovina', 100),
	('BB', 'BRB', 'Barbados', 100),
	('BD', 'BGD', 'Bangladesh', 100),
	('BE', 'BEL', 'Belgium', 100),
	('BF', 'BFA', 'Burkina Faso', 100),
	('BG', 'BGR', 'Bulgaria', 100),
	('BH', 'BHR', 'Bahrain', 100),
	('BI', 'BDI', 'Burundi', 100),
	('BJ', 'BEN', 'Benin', 100),
	('BL', 'BLM', 'Saint Barthélemy', 100),
	('BM', 'BMU', 'Bermuda', 100),
	('BN', 'BRN', 'Brunei Darussalam', 100),
	('BO', 'BOL', 'Bolivia', 100),
	('BR', 'BRA', 'Brazil', 100),
	('BS', 'BHS', 'Bahamas', 100),
	('BT', 'BTN', 'Bhutan', 100),
	('BU', 'BES', 'Bonaire, Sint Eustatius and Saba', 100),
	('BV', 'BVT', 'Bouvet Island', 100),
	('BW', 'BWA', 'Botswana', 100),
	('BY', 'BLR', 'Belarus', 100),
	('BZ', 'BLZ', 'Belize', 100),
	('CA', 'CAN', 'Canada', 100),
	('CC', 'CCK', 'Cocos (Keeling) Islands', 100),
	('CD', 'COD', 'Congo, The Democratic Republic Of The', 100),
	('CF', 'CAF', 'Central African Republic', 100),
	('CG', 'COG', 'Congo', 100),
	('CH', 'CHE', 'Switzerland', 100),
	('CI', 'CIV', 'Côte D''Ivoire', 100),
	('CK', 'COK', 'Cook Islands', 100),
	('CL', 'CHL', 'Chile', 100),
	('CM', 'CMR', 'Cameroon', 100),
	('CN', 'CHN', 'China', 100),
	('CO', 'COL', 'Colombia', 100),
	('CR', 'CRI', 'Costa Rica', 100),
	('CU', 'CUB', 'Cuba', 100),
	('CV', 'CPV', 'Cape Verde', 100),
	('CW', 'CUW', 'Curaçao', 100),
	('CX', 'CXR', 'Christmas Island', 100),
	('CY', 'CYP', 'Cyprus', 100),
	('CZ', 'CZE', 'Czech Republic', 100),
	('DE', 'DEU', 'Germany', 1),
	('DJ', 'DJI', 'Djibouti', 100),
	('DK', 'DNK', 'Denmark', 100),
	('DM', 'DMA', 'Dominica', 100),
	('DO', 'DOM', 'Dominican Republic', 100),
	('DZ', 'DZA', 'Algeria', 100),
	('EC', 'ECU', 'Ecuador', 100),
	('EE', 'EST', 'Estonia', 100),
	('EG', 'EGY', 'Egypt', 100),
	('EH', 'ESH', 'Western Sahara', 100),
	('ER', 'ERI', 'Eritrea', 100),
	('ES', 'ESP', 'Spain', 100),
	('ET', 'ETH', 'Ethiopia', 100),
	('EU', 'EUR', 'European Union', 100),
	('FI', 'FIN', 'Finland', 100),
	('FJ', 'FJI', 'Fiji', 100),
	('FK', 'FLK', 'Falkland Islands', 100),
	('FM', 'FSM', 'Micronesia, Federated States Of', 100),
	('FO', 'FRO', 'Faroe Islands', 100),
	('FR', 'FRA', 'France', 100),
	('GA', 'GAB', 'Gabon', 100),
	('GB', 'GBR', 'United Kingdom', 2),
	('GD', 'GRD', 'Grenada', 100),
	('GE', 'GEO', 'Georgia', 100),
	('GF', 'GUF', 'French Guiana', 100),
	('GG', 'GGY', 'Guernsey', 100),
	('GH', 'GHA', 'Ghana', 100),
	('GI', 'GIB', 'Gibraltar', 100),
	('GL', 'GRL', 'Greenland', 100),
	('GM', 'GMB', 'Gambia', 100),
	('GN', 'GIN', 'Guinea', 100),
	('GP', 'GLP', 'Guadeloupe', 100),
	('GQ', 'GNQ', 'Equatorial Guinea', 100),
	('GR', 'GRC', 'Greece', 100),
	('GT', 'GTM', 'Guatemala', 100),
	('GU', 'GUM', 'Guam', 100),
	('GW', 'GNB', 'Guinea-Bissau', 100),
	('GY', 'GUY', 'Guyana', 100),
	('HK', 'HKG', 'Hong Kong', 100),
	('HM', 'HMD', 'Heard Island and McDonald Islands', 100),
	('HN', 'HND', 'Honduras', 100),
	('HR', 'HRV', 'Croatia', 100),
	('HT', 'HTI', 'Haiti', 100),
	('HU', 'HUN', 'Hungary', 100),
	('ID', 'IDN', 'Indonesia', 100),
	('IE', 'IRL', 'Ireland', 100),
	('IL', 'ISR', 'Israel', 100),
	('IM', 'IMN', 'Isle of Man', 100),
	('IN', 'IND', 'India', 100),
	('IO', 'IOT', 'British Indian Ocean Territory', 100),
	('IQ', 'IRQ', 'Iraq', 100),
	('IR', 'IRN', 'Iran, Islamic Republic Of', 100),
	('IS', 'ISL', 'Iceland', 100),
	('IT', 'ITA', 'Italy', 100),
	('JE', 'JEY', 'Jersey', 100),
	('JM', 'JAM', 'Jamaica', 100),
	('JO', 'JOR', 'Jordan', 100),
	('JP', 'JPN', 'Japan', 100),
	('KE', 'KEN', 'Kenya', 100),
	('KG', 'KGZ', 'Kyrgyzstan', 100),
	('KH', 'KHM', 'Cambodia', 100),
	('KI', 'KIR', 'Kiribati', 100),
	('KM', 'COM', 'Comoros', 100),
	('KN', 'KNA', 'Saint Kitts And Nevis', 100),
	('KP', 'PRK', 'Korea, Democratic People''s Republic Of', 100),
	('KR', 'KOR', 'Korea, Republic of', 100),
	('KW', 'KWT', 'Kuwait', 100),
	('KY', 'CYM', 'Cayman Islands', 100),
	('KZ', 'KAZ', 'Kazakhstan', 100),
	('LA', 'LAO', 'Lao People''s Democratic Republic', 100),
	('LB', 'LBN', 'Lebanon', 100),
	('LC', 'LCA', 'Saint Lucia', 100),
	('LI', 'LIE', 'Liechtenstein', 100),
	('LK', 'LKA', 'Sri Lanka', 100),
	('LR', 'LBR', 'Liberia', 100),
	('LS', 'LSO', 'Lesotho', 100),
	('LT', 'LTU', 'Lithuania', 100),
	('LU', 'LUX', 'Luxembourg', 100),
	('LV', 'LVA', 'Latvia', 100),
	('LY', 'LBY', 'Libya', 100),
	('MA', 'MAR', 'Morocco', 100),
	('MC', 'MCO', 'Monaco', 100),
	('MD', 'MDA', 'Moldova, Republic of', 100),
	('ME', 'MNE', 'Montenegro', 100),
	('MF', 'MAF', 'Saint Martin (French Part)', 100),
	('MG', 'MDG', 'Madagascar', 100),
	('MH', 'MHL', 'Marshall Islands', 100),
	('MK', 'MKD', 'Macedonia', 100),
	('ML', 'MLI', 'Mali', 100),
	('MM', 'MMR', 'Myanmar', 100),
	('MN', 'MNG', 'Mongolia', 100),
	('MO', 'MAC', 'Macao', 100),
	('MP', 'MNP', 'Northern Mariana Islands', 100),
	('MQ', 'MTQ', 'Martinique', 100),
	('MR', 'MRT', 'Mauritania', 100),
	('MS', 'MSR', 'Montserrat', 100),
	('MT', 'MLT', 'Malta', 100),
	('MU', 'MUS', 'Mauritius', 100),
	('MV', 'MDV', 'Maldives', 100),
	('MW', 'MWI', 'Malawi', 100),
	('MX', 'MEX', 'Mexico', 100),
	('MY', 'MYS', 'Malaysia', 100),
	('MZ', 'MOZ', 'Mozambique', 100),
	('NA', 'NAM', 'Namibia', 100),
	('NC', 'NCL', 'New Caledonia', 100),
	('NE', 'NER', 'Niger', 100),
	('NF', 'NFK', 'Norfolk Island', 100),
	('NG', 'NGA', 'Nigeria', 100),
	('NI', 'NIC', 'Nicaragua', 100),
	('NL', 'NLD', 'Netherlands', 100),
	('NO', 'NOR', 'Norway', 100),
	('NP', 'NPL', 'Nepal', 100),
	('NR', 'NRU', 'Nauru', 100),
	('NU', 'NIU', 'Niue', 100),
	('NZ', 'NZL', 'New Zealand', 100),
	('OM', 'OMN', 'Oman', 100),
	('PA', 'PAN', 'Panama', 100),
	('PE', 'PER', 'Peru', 100),
	('PF', 'PYF', 'French Polynesia', 100),
	('PG', 'PNG', 'Papua New Guinea', 100),
	('PH', 'PHL', 'Philippines', 100),
	('PK', 'PAK', 'Pakistan', 100),
	('PL', 'POL', 'Poland', 100),
	('PM', 'SPM', 'Saint Pierre And Miquelon', 100),
	('PN', 'PCN', 'Pitcairn', 100),
	('PR', 'PRI', 'Puerto Rico', 100),
	('PT', 'PRT', 'Portugal', 100),
	('PW', 'PLW', 'Palau', 100),
	('PY', 'PRY', 'Paraguay', 100),
	('QA', 'QAT', 'Qatar', 100),
	('RE', 'REU', 'Réunion', 100),
	('RO', 'ROU', 'Romania', 100),
	('RS', 'SRB', 'Serbia', 100),
	('RU', 'RUS', 'Russian Federation', 100),
	('RW', 'RWA', 'Rwanda', 100),
	('SA', 'SAU', 'Saudi Arabia', 100),
	('SB', 'SLB', 'Solomon Islands', 100),
	('SC', 'SYC', 'Seychelles', 100),
	('SD', 'SDN', 'Sudan', 100),
	('SE', 'SWE', 'Sweden', 100),
	('SG', 'SGP', 'Singapore', 100),
	('SH', 'SHN', 'Saint Helena, Ascension and Tristan Da Cunha', 100),
	('SI', 'SVN', 'Slovenia', 100),
	('SJ', 'SJM', 'Svalbard And Jan Mayen', 100),
	('SK', 'SVK', 'Slovakia', 100),
	('SL', 'SLE', 'Sierra Leone', 100),
	('SM', 'SMR', 'San Marino', 100),
	('SN', 'SEN', 'Senegal', 100),
	('SO', 'SOM', 'Somalia', 100),
	('SR', 'SUR', 'Suriname', 100),
	('SS', 'SSD', 'South Sudan', 100),
	('ST', 'STP', 'Sao Tome and Principe', 100),
	('SV', 'SLV', 'El Salvador', 100),
	('SX', 'SXM', 'Sint Maarten (Dutch part)', 100),
	('SY', 'SYR', 'Syrian Arab Republic', 100),
	('SZ', 'SWZ', 'Swaziland', 100),
	('TC', 'TCA', 'Turks and Caicos Islands', 100),
	('TD', 'TCD', 'Chad', 100),
	('TF', 'ATF', 'French Southern Territories', 100),
	('TG', 'TGO', 'Togo', 100),
	('TH', 'THA', 'Thailand', 100),
	('TJ', 'TJK', 'Tajikistan', 100),
	('TK', 'TKL', 'Tokelau', 100),
	('TL', 'TLS', 'Timor-Leste', 100),
	('TM', 'TKM', 'Turkmenistan', 100),
	('TN', 'TUN', 'Tunisia', 100),
	('TO', 'TON', 'Tonga', 100),
	('TR', 'TUR', 'Turkey', 100),
	('TT', 'TTO', 'Trinidad and Tobago', 100),
	('TV', 'TUV', 'Tuvalu', 100),
	('TW', 'TWN', 'Taiwan', 100),
	('TZ', 'TZA', 'Tanzania, United Republic of', 100),
	('UA', 'UKR', 'Ukraine', 100),
	('UG', 'UGA', 'Uganda', 100),
	('UM', 'UMI', 'United States Minor Outlying Islands', 100),
	('US', 'USA', 'United States', 3),
	('UY', 'URY', 'Uruguay', 100),
	('UZ', 'UZB', 'Uzbekistan', 100),
	('VA', 'VAT', 'Holy See (Vatican City State)', 100),
	('VC', 'VCT', 'Saint Vincent And The Grenadines', 100),
	('VE', 'VEN', 'Venezuela, Bolivarian Republic of', 100),
	('VG', 'VGB', 'Virgin Islands, British', 100),
	('VI', 'VIR', 'Virgin Islands, U.S.', 100),
	('VN', 'VNM', 'Viet Nam', 100),
	('VU', 'VUT', 'Vanuatu', 100),
	('WF', 'WLF', 'Wallis and Futuna', 100),
	('WS', 'WSM', 'Samoa', 100),
	('YE', 'YEM', 'Yemen', 100),
	('YT', 'MYT', 'Mayotte', 100),
	('ZA', 'ZAF', 'South Africa', 100),
	('ZM', 'ZMB', 'Zambia', 100),
	('ZW', 'ZWE', 'Zimbabwe', 100)
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
TRUNCATE TABLE dropdown.CountryCallingCode
GO

EXEC utility.InsertIdentityValue 'dropdown.CountryCallingCode', 'CountryCallingCodeID', 0
GO

INSERT INTO dropdown.CountryCallingCode
	(ISOCountryCode2, CountryCallingCode)
VALUES
	('AD', 376),
	('AE', 971),
	('AF', 93),
	('AG', 1268),
	('AI', 1264),
	('AL', 355),
	('AM', 374),
	('AO', 244),
	('AR', 54),
	('AS', 1684),
	('AT', 43),
	('AU', 61),
	('AW', 297),
	('AX', 358),
	('AZ', 994),
	('BA', 387),
	('BB', 1246),
	('BD', 880),
	('BE', 32),
	('BF', 226),
	('BG', 359),
	('BH', 973),
	('BI', 257),
	('BJ', 229),
	('BL', 590),
	('BM', 1441),
	('BN', 673),
	('BO', 591),
	('BR', 55),
	('BS', 1242),
	('BT', 975),
	('BW', 267),
	('BY', 375),
	('BZ', 501),
	('CA', 1),
	('CC', 61),
	('CD', 243),
	('CF', 236),
	('CG', 242),
	('CH', 41),
	('CI', 225),
	('CK', 682),
	('CL', 56),
	('CM', 237),
	('CN', 86),
	('CO', 57),
	('CR', 506),
	('CU', 53),
	('CV', 238),
	('CW', 5999),
	('CX', 61),
	('CY', 357),
	('CZ', 420),
	('DE', 49),
	('DJ', 253),
	('DK', 45),
	('DM', 1767),
	('DO', 180),
	('DO', 291),
	('DO', 849),
	('DO', 918),
	('DZ', 213),
	('EC', 593),
	('EE', 372),
	('EG', 20),
	('EH', 212),
	('ER', 291),
	('ES', 34),
	('ET', 251),
	('FI', 358),
	('FJ', 679),
	('FK', 500),
	('FM', 691),
	('FO', 298),
	('FR', 33),
	('GA', 241),
	('GB', 44),
	('GD', 1473),
	('GE', 995),
	('GF', 594),
	('GG', 44),
	('GH', 233),
	('GI', 350),
	('GL', 299),
	('GM', 220),
	('GN', 224),
	('GP', 590),
	('GQ', 240),
	('GR', 30),
	('GT', 502),
	('GU', 1671),
	('GW', 245),
	('GY', 592),
	('HK', 852),
	('HN', 504),
	('HR', 385),
	('HT', 509),
	('HU', 36),
	('ID', 62),
	('IE', 353),
	('IL', 972),
	('IM', 44),
	('IN', 91),
	('IO', 246),
	('IQ', 964),
	('IR', 98),
	('IS', 354),
	('IT', 39),
	('JE', 44),
	('JM', 1876),
	('JO', 962),
	('JP', 81),
	('KE', 254),
	('KG', 996),
	('KH', 855),
	('KI', 686),
	('KM', 269),
	('KN', 1869),
	('KP', 850),
	('KR', 82),
	('KW', 965),
	('KY', 1345),
	('KZ', 76),
	('KZ', 77),
	('LA', 856),
	('LB', 961),
	('LC', 1758),
	('LI', 423),
	('LK', 94),
	('LR', 231),
	('LS', 266),
	('LT', 370),
	('LU', 352),
	('LV', 371),
	('LY', 218),
	('MA', 212),
	('MC', 377),
	('MD', 373),
	('ME', 382),
	('MF', 590),
	('MG', 261),
	('MH', 692),
	('MK', 389),
	('ML', 223),
	('MM', 95),
	('MN', 976),
	('MO', 853),
	('MP', 1670),
	('MQ', 596),
	('MR', 222),
	('MS', 1664),
	('MT', 356),
	('MU', 230),
	('MV', 960),
	('MW', 265),
	('MX', 52),
	('MY', 60),
	('MZ', 258),
	('NA', 264),
	('NC', 687),
	('NE', 227),
	('NF', 672),
	('NG', 234),
	('NI', 505),
	('NL', 31),
	('NO', 47),
	('NP', 977),
	('NR', 674),
	('NU', 683),
	('NZ', 64),
	('OM', 968),
	('PA', 507),
	('PE', 51),
	('PF', 689),
	('PG', 675),
	('PH', 63),
	('PK', 92),
	('PL', 48),
	('PM', 508),
	('PN', 64),
	('PR', 17),
	('PR', 871),
	('PR', 939),
	('PT', 351),
	('PW', 680),
	('PY', 595),
	('QA', 974),
	('RE', 262),
	('RO', 40),
	('RS', 381),
	('RU', 7),
	('RW', 250),
	('SA', 966),
	('SB', 677),
	('SC', 248),
	('SD', 249),
	('SE', 46),
	('SG', 65),
	('SI', 386),
	('SJ', 4779),
	('SK', 421),
	('SL', 232),
	('SM', 378),
	('SN', 221),
	('SO', 252),
	('SR', 597),
	('SS', 211),
	('ST', 239),
	('SV', 503),
	('SX', 1721),
	('SY', 963),
	('SZ', 268),
	('TC', 1649),
	('TD', 235),
	('TG', 228),
	('TH', 66),
	('TJ', 992),
	('TK', 690),
	('TL', 670),
	('TM', 993),
	('TN', 216),
	('TO', 676),
	('TR', 90),
	('TT', 1868),
	('TV', 688),
	('TW', 886),
	('TZ', 255),
	('UA', 380),
	('UG', 256),
	('US', 1),
	('UY', 598),
	('UZ', 998),
	('VA', 3),
	('VA', 379),
	('VA', 698),
	('VA', 906),
	('VC', 1784),
	('VE', 58),
	('VG', 1284),
	('VI', 1340),
	('VN', 84),
	('VU', 678),
	('WF', 681),
	('WS', 685),
	('YE', 967),
	('YT', 262),
	('ZA', 27),
	('ZM', 260),
	('ZW', 263)
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.Currency
TRUNCATE TABLE dropdown.Currency
GO

EXEC utility.InsertIdentityValue 'dropdown.Currency', 'CurrencyID', 0
GO

INSERT INTO dropdown.Currency 
	(ISOCurrencyCode, CurrencyName, DisplayOrder) 
VALUES 
	('USD', 'US Dollar', 1),
	('AED', 'UAE Dirham', 6),
	('AZN', 'Azerbaijanian Manat', 100),
	('NGN', 'Nigerian Naira', 100),
	('NIO', 'Nicaraguan Cordoba Oro', 100),
	('NOK', 'Norwegian Krone', 100),
	('NPR', 'Nepalese Rupee', 100),
	('NZD', 'New Zealand Dollar', 100),
	('OMR', 'Omani Rial', 100),
	('PAB', 'Panamean Balboa', 100),
	('PEN', 'Puruvian Nuevo Sol', 100),
	('PGK', 'Papa New Guinean Kina', 100),
	('PHP', 'Philippine Peso', 100),
	('BAM', 'Bosnia and Herzegovina Convertible Mark', 100),
	('PKR', 'Pakistan Rupee', 100),
	('PLN', 'Polish Zloty', 100),
	('PYG', 'Paraguay Guarani', 100),
	('QAR', 'Qatari Rial', 100),
	('RON', 'New Romanian Leu', 100),
	('RSD', 'Serbian Dinar', 100),
	('RUB', 'Russian Ruble', 100),
	('RWF', 'Rwandan Franc', 100),
	('SAR', 'Saudi Riyal', 100),
	('SBD', 'Solomon Islands Dollar', 100),
	('BBD', 'Barbados Dollar', 100),
	('SCR', 'Seychelles Rupee', 100),
	('SDG', 'Sudanese Pound', 100),
	('SEK', 'Swedish Krona', 100),
	('SGD', 'Singapore Dollar', 100),
	('SHP', 'Saint Helena Pound', 100),
	('SLL', 'Sierra Leone Leone', 100),
	('SOS', 'Somali Shilling', 100),
	('SRD', 'Surinam Dollar', 100),
	('SSP', 'South Sudanese Pound', 100),
	('STD', 'Sao Tome and Principe Dobra', 100),
	('BDT', 'Bangladeshi Taka', 100),
	('SVC', 'El Salvador Colon', 100),
	('SYP', 'Syrian Pound', 100),
	('SZL', 'Swaziland Lilangeni', 100),
	('THB', 'Thai Baht', 100),
	('TJS', 'Tajikistan Somoni', 100),
	('TMT', 'Turkmenistan New Manat', 100),
	('TND', 'Tunisian Dinar', 100),
	('TOP', 'Tongan PaÃ¢â?¬â?¢anga', 100),
	('TRY', 'Turkish Lira', 4),
	('TTD', 'Trinidad and Tobago Dollar', 100),
	('BGN', 'Bulgarian Lev', 100),
	('TWD', 'New Taiwan Dollar', 100),
	('TZS', 'Tanzanian Shilling', 100),
	('UAH', 'Ukranian Hryvnia', 100),
	('UGX', 'Uganda Shilling', 100),
	('UYU', 'Uragyan Peso Uruguayo', 100),
	('UZS', 'Uzbekistan Sum', 100),
	('VEF', 'Venezuelan Bolivar Fuerte', 100),
	('VND', 'Vietnamise Dong', 100),
	('VUV', 'Vanuatun Vatu', 100),
	('BHD', 'Bahraini Dinar', 100),
	('WST', 'Samoan Tala', 100),
	('XAF', 'CFA Franc BEAC', 100),
	('XCD', 'East Caribbean Dollar', 100),
	('XOF', 'CFA Franc BCEAO', 100),
	('XPF', 'CFP Franc', 100),
	('YER', 'Yemeni Rial', 100),
	('ZAR', 'South African Rand', 100),
	('ZMW', 'New Zambian Kwacha', 100),
	('ZWL', 'Zimbabwe Dollar', 100),
	('BIF', 'Burundi Franc', 100),
	('BMD', 'Bermudian Dollar', 100),
	('BND', 'Brunei Dollar', 100),
	('BOB', 'Bolivian Boliviano', 100),
	('AFN', 'Afghani Afghani', 100),
	('BRL', 'Brazilian Real', 100),
	('BSD', 'Bahamian Dollar', 100),
	('BTN', 'Bhutanise Ngultrum', 100),
	('BWP', 'Botswanan Pula', 100),
	('BYR', 'Belarussian Ruble', 100),
	('BZD', 'Belize Dollar', 100),
	('CAD', 'Canadian Dollar', 100),
	('CDF', 'Congolais Franc', 100),
	('CHF', 'Swiss Franc', 100),
	('CLP', 'Chilean Peso', 100),
	('ALL', 'Albanian Lek', 100),
	('CNY', 'Chinese Yuan Renminbi', 100),
	('COP', 'Colombian Peso', 100),
	('CRC', 'Costa Rican Colon', 100),
	('CUP', 'Cuban Peso', 100),
	('CVE', 'Cape Verde Escudo', 100),
	('CZK', 'Czech Koruna', 100),
	('DJF', 'Djibouti Franc', 100),
	('DKK', 'Danish Krone', 100),
	('DOP', 'Dominican Peso', 100),
	('DZD', 'Algerian Dinar', 100),
	('AMD', 'Armenian Dram', 100),
	('EGP', 'Egyptian Pound', 100),
	('ERN', 'Eritrean Nakfa', 100),
	('ETB', 'Ethiopian Birr', 100),
	('EUR', 'Euro', 3),
	('FJD', 'Fiji Dollar', 100),
	('FKP', 'Falkland Islands Pound', 100),
	('GBP', 'UK Pound Sterling', 2),
	('GEL', 'Geogrian Lari', 100),
	('GHS', 'Ghanan Cedi', 100),
	('GIP', 'Gibraltar Pound', 100),
	('ANG', 'Netherlands Antillean Guilder', 100),
	('GMD', 'Gambian Dalasi', 100),
	('GNF', 'Guinea Franc', 100),
	('GTQ', 'Guatemalian Quetzal', 100),
	('GYD', 'Guyana Dollar', 100),
	('HKD', 'Hong Kong Dollar', 100),
	('HNL', 'Honduran Lempira', 100),
	('HRK', 'Croatian Kuna', 100),
	('HTG', 'Hatian Gourde', 100),
	('HUF', 'Hungarian Forint', 100),
	('IDR', 'Indonesian Rupiah', 100),
	('AOA', 'Angolan Kwanza', 100),
	('ILS', 'Israeli New Shekel', 100),
	('INR', 'Indian Rupee', 100),
	('IQD', 'Iraqi Dinar', 100),
	('IRR', 'Iranian Rial', 100),
	('ISK', 'Iceland Krona', 100),
	('JMD', 'Jamaican Dollar', 100),
	('JOD', 'Jordanian Dinar', 5),
	('JPY', 'Japanese Yen', 100),
	('KES', 'Kenyan Shilling', 100),
	('KGS', 'Kyrgyzstan Som', 100),
	('ARS', 'Argentine Peso', 100),
	('KHR', 'Cambodian Riel', 100),
	('KMF', 'Comoro Franc', 100),
	('KPW', 'North Korean Won', 100),
	('KRW', 'South Korean Won', 100),
	('KWD', 'Kuwaiti Dinar', 100),
	('KYD', 'Cayman Islands Dollar', 100),
	('KZT', 'Kazakhstan Tenge', 100),
	('LAK', 'Lao Kip', 100),
	('LBP', 'Lebanese Pound', 100),
	('LKR', 'Sri Lanka Rupee', 100),
	('AUD', 'Australian Dollar', 100),
	('LRD', 'Liberian Dollar', 100),
	('LSL', 'Lesotho Loti', 100),
	('LTL', 'Lithuanian Litas', 100),
	('LVL', 'Latvian Lats', 100),
	('LYD', 'Libyan Dinar', 100),
	('MAD', 'Moroccan Dirham', 100),
	('MDL', 'Moldovan Leu', 100),
	('MGA', 'Malagasy Ariary', 100),
	('MKD', 'Macedonian Denar', 100),
	('MMK', 'Myanmar Kyat', 100),
	('AWG', 'Aruban Florin', 100),
	('MNT', 'Mongolian Tugrik', 100),
	('MOP', 'Macaoan Pataca', 100),
	('MRO', 'Maurutanian Ouguiya', 100),
	('MUR', 'Mauritius Rupee', 100),
	('MVR', 'Maldiven Rufiyaa', 100),
	('MWK', 'Malawian Kwacha', 100),
	('MXN', 'Mexican Peso', 100),
	('MYR', 'Malaysian Ringgit', 100),
	('MZN', 'Mozambique Metical', 100),
	('NAD', 'Namibia Dollar', 100)
GO
--End table dropdown.Currency

--Begin table dropdown.DocumentType
TRUNCATE TABLE dropdown.DocumentType
GO

EXEC utility.InsertIdentityValue 'dropdown.DocumentType', 'DocumentTypeID', 0
GO

INSERT INTO dropdown.DocumentType 
	(DocumentTypeCategory, DocumentTypeCode, DocumentTypeName) 
VALUES
	('Document Type Category 1', 'DT11', 'Document Type 11'),
	('Document Type Category 1', 'DT12', 'Document Type 12'),
	('Document Type Category 1', 'DT13', 'Document Type 13'),
	('Document Type Category 2', 'DT21', 'Document Type 21'),
	('Document Type Category 2', 'DT22', 'Document Type 22'),
	('Document Type Category 2', 'DT23', 'Document Type 23'),
	('Document Type Category 3', 'DT31', 'Document Type 31'),
	('Document Type Category 3', 'DT32', 'Document Type 32'),
	('Document Type Category 3', 'DT33', 'Document Type 33')
GO
--End table dropdown.DocumentType

--Begin table dropdown.ForceCategory
TRUNCATE TABLE dropdown.ForceCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.ForceCategory', 'ForceCategoryID', 0
GO

INSERT INTO dropdown.ForceCategory 
	(ForceCategoryCode, ForceCategoryName, DisplayOrder) 
VALUES
	('Governmental', 'Governmental', 1),
	('NGO', 'NGO', 2),
	('ResponseOrganisation', 'Response Organisation', 3)
GO
--End table dropdown.ForceCategory

--Begin table dropdown.ForceType
TRUNCATE TABLE dropdown.ForceType
GO

EXEC utility.InsertIdentityValue 'dropdown.ForceType', 'ForceTypeID', 0
GO

INSERT INTO dropdown.ForceType 
	(ForceTypeName, HexColor) 
VALUES
	('Armed Groups', '#ff0000'),
	('Local Partners', '#32cd32'),
	('Other Local Organisations', '#ffff00'),
	('International Organisations', '#0000ff')
GO
--End table dropdown.ForceType

--Begin table dropdown.FundingSource
TRUNCATE TABLE dropdown.FundingSource
GO

EXEC utility.InsertIdentityValue 'dropdown.FundingSource', 'FundingSourceID', 0
GO

INSERT INTO dropdown.FundingSource 
	(FundingSourceName) 
VALUES
	('DFID'),
	('FCO'),
	('MOD'),
	('Other HMG'),
	('Other')
GO
--End table dropdown.FundingSource

--Begin table dropdown.ImpactDecision
TRUNCATE TABLE dropdown.ImpactDecision
GO

SET IDENTITY_INSERT dropdown.ImpactDecision ON
GO

INSERT INTO dropdown.ImpactDecision 
	(ImpactDecisionID, ImpactDecisionName, HexColor, DisplayOrder) 
VALUES
	(0, 'Unknown', '#d3d3d3', 0),
	(1, 'Disengagement', '#ff0000', 1),
	(2, 'Partial Disengagement', '#ffa500', 2),
	(3, 'Engagement With Caution', '#ffff00', 3),
	(4, 'Engagement', '#32cd32', 4)
GO

SET IDENTITY_INSERT dropdown.ImpactDecision OFF
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.IncidentCategory
TRUNCATE TABLE dropdown.IncidentCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentCategory', 'IncidentCategoryID', 0
GO

INSERT INTO dropdown.IncidentCategory 
	(IncidentCategoryCode, IncidentCategoryName, DisplayOrder) 
VALUES
	('EarlyAlert', 'Early Alert', 1),
	('GeoEvent', 'Geo Event', 2),
	('Security', 'Security', 3)
GO
--End table dropdown.IncidentCategory

--Begin table dropdown.IncidentImpact
TRUNCATE TABLE dropdown.IncidentImpact
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentImpact', 'IncidentImpactID', 0
GO

INSERT INTO dropdown.IncidentImpact 
	(IncidentImpactName, DisplayOrder) 
VALUES
	('Information Only', 1),
	('Near Miss', 2),
	('Minor', 3),
	('Moderate', 4),
	('Major', 5),
	('Severe', 6),
	('Unknown at this time', 7)
GO
--End table dropdown.IncidentImpact

--Begin table dropdown.IncidentType
TRUNCATE TABLE dropdown.IncidentType
GO

EXEC utility.InsertIdentityValue 'dropdown.IncidentType', 'IncidentTypeID', 0
GO

INSERT INTO dropdown.IncidentType 
	(IncidentTypeCategory, IncidentTypeName, Icon, DisplayOrder) 
VALUES 
	('Geophysical','Earthquake','volcano.png'),
	('Geophysical','Volcano','volcano.png'),
	('Geophysical','Landslide','volcano.png'),
	('Geophysical','Avalanch','volcano.png'),
	('Geophysical','Tsunamis','volcano.png'),
	('Meteorological','Storm','storm.png'),
	('Meteorological','Cyclone','storm.png'),
	('Meteorological','Hurricane','storm.png'),
	('Meteorological','Typhoon','storm.png'),
	('Meteorological','Blizzard','storm.png'),
	('Hydrological','Flood','storm_surge.png'),
	('Hydrological','Storm Surge','storm_surge.png'),
	('Climatic','Drought','drought.png'),
	('Climatic','Extreme Temperature','drought.png'),
	('Climatic','Wildfires','drought.png'),
	('Biological','Epidemic','epidemic.png'),
	('Biological','Infestation','epidemic.png'),
	('Human','Conflict','conflict.png'),
	('Human','Environmental Degradation','conflict.png'),
	('Human','Pollution','conflict.png'),
	('Human','Industrial Accidents','conflict.png'),
	('Other','Other','multicluster.png'),
	('Security','Civil Activity','security.png'),
	('Security','Criminality','security.png'),
	('Security','Terrorism','security.png'),
	('Security','Military Activity','security.png'),
	('Security','Security Sector Activity','security.png'),
	('Security','Other','security.png')
GO
--End table dropdown.IncidentType

--Begin table dropdown.InformationValidity
TRUNCATE TABLE dropdown.InformationValidity
GO

EXEC utility.InsertIdentityValue 'dropdown.InformationValidity', 'InformationValidityID', 0
GO

INSERT INTO dropdown.InformationValidity 
	(InformationValidityName, DisplayOrder, IsActive) 
VALUES
	('Known to be true without reservation', 1, 1),
	('The information is known personally by the resource but not to the person reporting', 2, 1),
	('The information is not known personally to the resource but can be corroborated by other information', 3, 1),
	('The information cannot be judged', 4, 1),
	('Suspected to be false', 5, 1)
GO
--End table dropdown.InformationValidity

--Begin table dropdown.LearnerProfileType
TRUNCATE TABLE dropdown.LearnerProfileType
GO

EXEC utility.InsertIdentityValue 'dropdown.LearnerProfileType', 'LearnerProfileTypeID', 0
GO

INSERT INTO dropdown.LearnerProfileType 
	(LearnerProfileTypeName, DisplayOrder, IsActive) 
VALUES
	('Stringers', 1, 1),
	('MAO Media Officers', 2, 1),
	('MAO Leadership', 3, 1),
	('Internal Staff', 4, 1),
	('Consultants', 5, 1),
	('Syrian Public', 6, 1)
GO
--End table dropdown.LearnerProfileType

--Begin table dropdown.ModuleType
TRUNCATE TABLE dropdown.ModuleType
GO

EXEC utility.InsertIdentityValue 'dropdown.ModuleType', 'ModuleTypeID', 0
GO

INSERT INTO dropdown.ModuleType 
	(ModuleTypeName) 
VALUES
	('Training'),
	('Workshop')
GO
--End table dropdown.ModuleType

--Begin table dropdown.PasswordSecurityQuestion
TRUNCATE TABLE dropdown.PasswordSecurityQuestion
GO

EXEC utility.InsertIdentityValue 'dropdown.PasswordSecurityQuestion', 'PasswordSecurityQuestionID', 0
GO

INSERT INTO dropdown.PasswordSecurityQuestion 
	(GroupID, PasswordSecurityQuestionName, DisplayOrder) 
VALUES 
	(1, 'What is the name of your first niece/nephew?', 1),
	(1, 'What is your maternal grandmother''s first name?', 2),
	(1, 'What is your maternal grandfather''s first name?', 3),
	(1, 'What was the name of your first pet?', 4),
	(2, 'What is your father''s middle name?', 1),
	(2, 'What is your mother''s middle name?', 2),
	(2, 'In what city were you married?', 3),
	(2, 'What is the first name of your first child?', 4),
	(3, 'In what city was your mother born? (Enter full name of city only)', 1),
	(3, 'In what city was your father born?  (Enter full name of city only)', 2),
	(3, 'In what city was your high school? (Enter only "Charlotte" for Charlotte High School)', 3),
	(3, 'In what city were you born? (Enter full name of city only)', 4)
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table dropdown.ProgramType
TRUNCATE TABLE dropdown.ProgramType
GO

EXEC utility.InsertIdentityValue 'dropdown.ProgramType', 'ProgramTypeID', 0
GO

INSERT INTO dropdown.ProgramType 
	(ProgramTypeName, DisplayOrder, IsActive) 
VALUES
	('Production', 1, 1),
	('Trauma', 2, 1),
	('Security', 3, 1),
	('Media Awareness', 4, 1)
GO
--End table dropdown.ProgramType

--Begin table dropdown.Project
TRUNCATE TABLE dropdown.Project
GO

EXEC utility.InsertIdentityValue 'dropdown.Project', 'ProjectID', 0
GO

INSERT INTO dropdown.Project 
	(ProjectCode, ProjectName) 
VALUES
	('Project1', 'Project 1'),
	('Project2', 'Project 2'),
	('Project3', 'Project 3')
GO
--End table dropdown.Project

--Begin table dropdown.ResourceProvider
TRUNCATE TABLE dropdown.ResourceProvider
GO

EXEC utility.InsertIdentityValue 'dropdown.ResourceProvider', 'ResourceProviderID', 0
GO

INSERT INTO dropdown.ResourceProvider 
	(ResourceProviderName, DisplayOrder) 
VALUES

	('GOI', 0),
	('GOK', 0),
	('Groups', 0),
	('IC North', 0),
	('IC South', 0),
	('Individuals', 0),
	('Iran', 0),
	('Israel', 0),
	('Qatar', 0),
	('Russia', 0),
	('Saudi Arabia', 0),
	('Turkey', 0),
	('UAE', 0),
	('UK', 0),
	('US', 0),
	('Other', 99)
GO
--End table dropdown.ResourceProvider

--Begin table dropdown.RelevantTheme
TRUNCATE TABLE dropdown.RelevantTheme
GO

EXEC utility.InsertIdentityValue 'dropdown.RelevantTheme', 'RelevantThemeID', 0
GO

INSERT INTO dropdown.RelevantTheme 
	(RelevantThemeName) 
VALUES
	('Extremism and extremist groups'),
	('Foreign intervention'),
	('Fragmentation of regime control'),
	('Gender and women''s issues'),
	('Humanitarian relief'),
	('Livelihoods and economy'),
	('Military advances/retreats'),
	('Opposition fragmentation'),
	('Political shifts'),
	('Religious minorities'),
	('Sectarianism'),
	('Services (e.g. electricity, water)'),
	('Truces and evacuation agreements')
GO
--End table dropdown.RelevantTheme

--Begin table dropdown.RequestForInformationResultType
TRUNCATE TABLE dropdown.RequestForInformationResultType
GO

EXEC utility.InsertIdentityValue 'dropdown.RequestForInformationResultType', 'RequestForInformationResultTypeID', 0
GO

INSERT INTO dropdown.RequestForInformationResultType 
	(RequestForInformationResultTypeCode,RequestForInformationResultTypeName,DisplayOrder)
VALUES
	('RFIResponse', 'RFI Response', 1),
	('SpotReport', 'Spot Report', 2),
	('Assessment', 'Critical Assessment', 3)
GO
--End table dropdown.RequestForInformationResultType

--Begin table dropdown.RequestForInformationStatus
TRUNCATE TABLE dropdown.RequestForInformationStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.RequestForInformationStatus', 'RequestForInformationStatusID', 0
GO

INSERT INTO dropdown.RequestForInformationStatus 
	(RequestForInformationStatusCode,RequestForInformationStatusName,DisplayOrder)
VALUES
	('New', 'New', 1),
	('InProgress', 'In Progress', 2),
	('Completed', 'Completed', 3)
GO
--End table dropdown.RequestForInformationStatus

--Begin table dropdown.Role
TRUNCATE TABLE dropdown.Role
GO

EXEC utility.InsertIdentityValue 'dropdown.Role', 'RoleID', 0
GO

INSERT INTO dropdown.Role 
	(RoleName) 
VALUES
	('Administrator'),
	('Analyst'),
	('Donor'),
	('Project Management'),
	('Program Officer')
GO
--End table dropdown.Role

--Begin table dropdown.SourceReliability
TRUNCATE TABLE dropdown.SourceReliability
GO

EXEC utility.InsertIdentityValue 'dropdown.SourceReliability', 'SourceReliabilityID', 0
GO

INSERT INTO dropdown.SourceReliability 
	(SourceReliabilityName, DisplayOrder) 
VALUES
	('Always Reliable', 1),
	('Mostly Reliable', 2),
	('Sometimes Reliable ', 3),
	('Unreliable', 4),
	('Untested Source', 5)

GO
--End table dropdown.SourceReliability

--Begin table dropdown.SourceType
TRUNCATE TABLE dropdown.SourceType
GO

EXEC utility.InsertIdentityValue 'dropdown.SourceType', 'SourceTypeID', 0
GO

INSERT INTO dropdown.SourceType 
	(SourceTypeName, DisplayOrder) 
VALUES
	('Media', 0),
	('Network', 0),
	('Other', 99),
	('Stringer', 0),
	('Unit', 0)
GO
--End table dropdown.SourceType

--Begin table dropdown.StatusChange
TRUNCATE TABLE dropdown.StatusChange
GO

EXEC utility.InsertIdentityValue 'dropdown.StatusChange', 'StatusChangeID', 0
GO

INSERT INTO dropdown.StatusChange 
	(StatusChangeName, HexColor, Direction, DisplayOrder, IsActive) 
VALUES
	('Continued Engagement', '#3333CC', 'left', 3, 1),
	('Continued No Engagement', '#663333', 'left', 5, 1),
	('Engage to No Engagement', '#663333', 'down', 2, 1),
	('No Engagement to Engage', '#66FF99', 'up', 1, 1),
	('Unknown/Pending RAP', '#999999', 'circle', 4, 1)
GO
--End table dropdown.StatusChange

--Begin table dropdown.TerritoryStatus
TRUNCATE TABLE dropdown.TerritoryStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.TerritoryStatus', 'TerritoryStatusID', 0
GO

INSERT INTO dropdown.TerritoryStatus 
	(TerritoryStatusCode, TerritoryStatusName, DisplayOrder)
VALUES
	('Active', 'Active', 1),
	('Pending', 'Pending', 2),
	('Inactive', 'Inactive', 3)
GO
--End table dropdown.TerritoryStatus

--Begin table dropdown.TerritoryType
TRUNCATE TABLE dropdown.TerritoryType
GO

INSERT dropdown.TerritoryType 
	(TerritoryTypeCode, TerritoryTypeCodePlural, TerritoryTypeName, TerritoryTypeNamePlural, IsReadOnly, DisplayOrder) 
VALUES 
	('Governorate', 'Governorates', 'Governorate', 'Governorates', 1, 1),
	('Community', 'Communities', 'Community', 'Communities', 0, 2)
GO
--End table dropdown.TerritoryType

--Begin table dropdown.UnitType
TRUNCATE TABLE dropdown.UnitType
GO

EXEC utility.InsertIdentityValue 'dropdown.UnitType', 'UnitTypeID', 0
GO

INSERT INTO dropdown.UnitType 
	(UnitTypeName, DisplayOrder, IsActive) 
VALUES
	('Division', 1, 1),
	('Brigade', 2, 1),
	('Unit', 3, 1),
	('Sub-Unit', 4, 1),
	('Operations Room', 5, 1),
	('Other', 6, 1)
GO
--End table dropdown.UnitType
