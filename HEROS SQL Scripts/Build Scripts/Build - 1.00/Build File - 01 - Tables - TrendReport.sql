/* Build File - 01 - Tables - Activity & TrendReport */
USE HEROS
GO

--Begin table trendreport.TrendReport
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReport'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReport
	(
	TrendReportID INT IDENTITY(1,1) NOT NULL,
	TrendReportTitle VARCHAR(100),
	TrendReportStartDate DATE,
	TrendReportEndDate DATE,
	Summary NVARCHAR(MAX),
	ProjectID INT,
	ReportDetail NVARCHAR(MAX),
	SummaryMap VARBINARY(MAX),
	SummaryMapZoom INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SummaryMapZoom', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TrendReportID'
GO
--End table trendreport.TrendReport

--Begin table trendreport.TrendReportAsset
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportAsset'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReportAsset
	(
	TrendReportAssetID INT IDENTITY(1,1) NOT NULL,
	TrendReportID INT,
	AssetID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrendReportAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_TrendReportAsset', 'TrendReportID,AssetID'
GO
--End table trendreport.TrendReportAsset

--Begin table trendreport.TrendReportForce
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportForce'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReportForce
	(
	TrendReportForceID INT IDENTITY(1,1) NOT NULL,
	TrendReportID INT,
	ForceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrendReportForceID'
EXEC utility.SetIndexClustered @TableName, 'IX_TrendReportForce', 'TrendReportID,ForceID'
GO
--End table trendreport.TrendReportForce

--Begin table trendreport.TrendReportIncident
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportIncident'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReportIncident
	(
	TrendReportIncidentID INT IDENTITY(1,1) NOT NULL,
	TrendReportID INT,
	IncidentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrendReportIncidentID'
EXEC utility.SetIndexClustered @TableName, 'IX_TrendReportIncident', 'TrendReportID,IncidentID'
GO
--End table trendreport.TrendReportIncident

--Begin table trendreport.TrendReportRelevantTheme
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportRelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReportRelevantTheme
	(
	TrendReportRelevantThemeID INT IDENTITY(1,1) NOT NULL,
	TrendReportID INT,
	RelevantThemeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RelevantThemeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrendReportRelevantThemeID'
EXEC utility.SetIndexClustered @TableName, 'IX_TrendReportRelevantTheme', 'TrendReportID,RelevantThemeID'
GO
--End table trendreport.TrendReportRelevantTheme

--Begin table trendreport.TrendReportTerritory
DECLARE @TableName VARCHAR(250) = 'trendreport.TrendReportTerritory'

EXEC utility.DropObject @TableName

CREATE TABLE trendreport.TrendReportTerritory
	(
	TrendReportTerritoryID INT IDENTITY(1,1) NOT NULL,
	TrendReportID INT,
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'TrendReportID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TrendReportTerritoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_TrendReportTerritory', 'TrendReportID,TerritoryID'
GO
--End table trendreport.TrendReportTerritory