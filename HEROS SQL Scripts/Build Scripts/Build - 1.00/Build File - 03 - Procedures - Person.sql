/* Build File - 03 - Procedures - Person */
USE HEROS
GO

--Begin procedure person.AddPersonPermissionable
EXEC utility.DropObject 'person.AddPersonPermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE person.AddPersonPermissionable

@PersonID INT, 
@PermissionableLineage VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	VALUES
		(@PersonID, @PermissionableLineage)

END
GO
--End procedure person.AddPersonPermissionable

--Begin procedure person.ApplyPermissionableTemplate
EXEC utility.DropObject 'person.ApplyPermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get apply a permissionable tempalte to one or more person id's
-- =================================================================================================
CREATE PROCEDURE person.ApplyPermissionableTemplate

@PermissionableTemplateID INT,
@PersonIDList VARCHAR(MAX),
@Mode VARCHAR(50) = 'Additive'

AS
BEGIN

	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM core.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.PermissionableTemplatePermissionable PTP
			JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PTP.PermissionableLineage
				AND PTP.PermissionableTemplateID = @PermissionableTemplateID
		) D

	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	OUTPUT INSERTED.PersonID INTO @tOutput
	SELECT
		T.PersonID,
		T.PermissionableLineage
	FROM @tTable T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = T.PersonID
			AND PP.PermissionableLineage = T.PermissionableLineage
		)

	IF @Mode = 'Exclusive'
		BEGIN

		DELETE PP
		OUTPUT DELETED.PersonID INTO @tOutput
		FROM person.PersonPermissionable PP
			JOIN @tTable T ON T.PersonID = PP.PersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tTable T
					WHERE T.PermissionableLineage = PP.PermissionableLineage
					)
						
		END
	--ENDIF

END
GO
--End procedure person.ApplyPermissionableTemplate

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Activity'
		SELECT @bHasAccess = 1 FROM activity.Activity T WHERE T.ActivityID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ActivityReport'
		SELECT @bHasAccess = 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Asset'
		SELECT @bHasAccess = 1 FROM asset.Asset T WHERE T.AssetID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ActivityReport'
		SELECT @bHasAccess = 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Contact'
		SELECT @bHasAccess = 1 FROM contact.Contact T WHERE T.ContactID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Course'
		SELECT @bHasAccess = 1 FROM training.Course T WHERE T.CourseID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Document'
		SELECT @bHasAccess = 1 FROM document.Document T WHERE T.DocumentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentCatalog'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentCatalog T WHERE T.EquipmentCatalogID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentInventory'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentInventory T JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = T.EquipmentCatalogID AND T.EquipmentInventoryID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = EC.ProjectID)
	ELSE IF @EntityTypeCode = 'Force'
		SELECT @bHasAccess = 1 FROM force.Force T WHERE T.ForceID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Incident'
		SELECT @bHasAccess = 1 FROM core.Incident T WHERE T.IncidentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Module'
		SELECT @bHasAccess = 1 FROM training.Module T WHERE T.ModuleID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'SpotReport'
		SELECT @bHasAccess = 1 FROM spotreport.SpotReport T WHERE T.SpotReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Territory'
		SELECT @bHasAccess = 1 FROM territory.Territory T JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode AND T.TerritoryID = @EntityID AND TT.IsReadOnly = 0
	ELSE IF @EntityTypeCode = 'TrendReport'
		SELECT @bHasAccess = 1 FROM trendreport.TrendReport T WHERE T.TrendReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetEmailAddressesByPermissionableLineage
EXEC Utility.DropObject 'person.GetEmailAddressesByPermissionableLineage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- =====================================================================
CREATE PROCEDURE person.GetEmailAddressesByPermissionableLineage

@PermissionableLineage VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		P.EmailAddress,
		P.PersonID
	FROM person.Person P
	WHERE EXISTS
		(
		SELECT 1
		FROM person.PersonPermissionable PP
			JOIN core.ListToTable(@PermissionableLineage, ',') LTT ON LTT.ListItem = PP.PermissionableLineage
				AND PP.PersonID = P.PersonID
		)
		AND P.PersonID <> @PersonID
	ORDER BY P.EmailAddress
		
END
GO
--End procedure person.GetEmailAddressesByPermissionableLineage

--Begin procedure person.GetNewsFeed
EXEC Utility.DropObject 'person.GetNewsFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2017.02.01
-- Description:	A stored procedure to get data for the news feed
-- =============================================================
CREATE PROCEDURE person.GetNewsFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		RFI.RequestForInformationID AS EntityID,
		RFI.RequestForInformationTitle AS Title,
		RFI.UpdateDateTime,
		core.FormatDate(RFI.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM core.RequestForInformation RFI
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'RequestForInformation'
		JOIN dropdown.Project P ON P.ProjectID = RFI.ProjectID
			AND RFI.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = RFI.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, RFI.RequestForInformationID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, RFI.RequestForInformationID)
				)
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFIS.RequestForInformationStatusCode = 'Completed'

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS Title,
		SR.UpdateDateTime,
		core.FormatDate(SR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM spotreport.SpotReport SR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'SpotReport'
		JOIN dropdown.Project P ON P.ProjectID = SR.ProjectID
			AND SR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = SR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, SR.SpotReportID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, SR.SpotReportID)
				)

	UNION

	SELECT
		'fa fa-fw fa-line-chart' AS Icon,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS Title,
		TR.UpdateDateTime,
		core.FormatDate(TR.UpdateDateTime) AS UpdateDateFormatted,
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		ET.EntityTypeName,
		P.ProjectID,

		CASE
			WHEN person.HasPermission('Main.Default.CanHaveDashboardLinks', @PersonID) = 1
			THEN P.ProjectName
			ELSE P.ProjectAlias
		END AS ProjectName

	FROM trendreport.TrendReport TR
		JOIN core.EntityType ET ON ET.EntityTypeCode = 'TrendReport'
		JOIN dropdown.Project P ON P.ProjectID = TR.ProjectID
			AND TR.UpdateDateTime >= DATEADD(d, -14, getDate())
			AND person.HasPermission(ET.EntityTypeCode + '.View', @PersonID) = 1
			AND EXISTS
				(
				SELECT 1
				FROM person.PersonProject PP
				WHERE PP.PersonID = @PersonID
					AND PP.ProjectID = TR.ProjectID
				)
			AND 
				(
				ET.HasWorkflow = 0
					OR workflow.GetWorkflowStepNumber(ET.EntityTypeCode, TR.TrendReportID) > workflow.GetWorkflowStepCount(ET.EntityTypeCode, TR.TrendReportID)
				)

	ORDER BY 4 DESC, 8, 10

END
GO
--End procedure person.GetNewsFeed

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		P.CellPhone,
		P.DefaultProjectID,
		P.EmailAddress,
		P.FirstName,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,
		P.IsSuperAdministrator,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.Organization,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.Suffix,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UserName,
		R.RoleID,
		R.RoleName
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonByPersonToken
EXEC utility.DropObject 'person.GetPersonByPersonToken'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to return data from the person.Person table based on a Token
-- ============================================================================================
CREATE PROCEDURE person.GetPersonByPersonToken

@Token VARCHAR(36)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.PersonID,
		P.TokenCreateDateTime,

		CASE
			WHEN DATEDIFF("hour", P.TokenCreateDateTime, getdate()) <= 24
			THEN 0
			ELSE 1
		END AS IsTokenExpired

	FROM person.Person P
	WHERE P.Token = @Token

END
GO
--End procedure person.GetPersonByPersonToken

--Begin procedure person.GetPersonPermissionables
EXEC utility.DropObject 'person.GetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.PersonPermissionable table
-- ======================================================================================
CREATE PROCEDURE person.GetPersonPermissionables

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PP.PermissionableLineage
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
	ORDER BY PP.PermissionableLineage

END
GO
--End procedure person.GetPersonPermissionables

--Begin procedure person.SavePersonPermissionables
EXEC Utility.DropObject 'person.SavePersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE person.SavePersonPermissionables

@PersonID INT, 
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	UNION

	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE EXISTS
		(
		SELECT 1 
		FROM core.ListToTable(@PermissionableIDList, ',') LTT 
		WHERE CAST(LTT.ListItem AS INT) = P.PermissionableID
			AND CAST(LTT.ListItem AS INT) > 0
		)
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonPermissionable PP
			WHERE PP.PermissionableLineage = P.PermissionableLineage
				AND PP.PersonID = @PersonID
			)

END
GO
--End procedure person.SavePersonPermissionables

--Begin procedure person.SetAccountLockedOut
EXEC utility.DropObject 'person.SetAccountLockedOut'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.18
-- Description:	A stored procedure to set the person.Person.IsAccountLockedOut bit to 1
-- ====================================================================================
CREATE PROCEDURE person.SetAccountLockedOut

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE P
	SET P.IsAccountLockedOut = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.SetAccountLockedOut

--Begin procedure person.SetPersonPermissionables
EXEC utility.DropObject 'person.SetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to add data to the person.PersonPermissionables table based on a PersonID
-- =========================================================================================================
CREATE PROCEDURE person.SetPersonPermissionables

@PersonID INT,
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	IF @PermissionableIDList IS NOT NULL AND LEN(RTRIM(@PermissionableIDList)) > 0
		BEGIN

		INSERT INTO person.PersonPermissionable
			(PersonID, PermissionableLineage)
		SELECT
			@PersonID,
			P.PermissionableLineage
		FROM permissionable.Permissionable P
			JOIN core.ListToTable(@PermissionableIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PermissionableID

		END
	--ENDIF

END
GO
--End procedure person.SetPersonPermissionables

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		DefaultProjectID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RoleName VARCHAR(50),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
					OR (@bIsTwoFactorEnabled = 1 AND (P.CellPhone IS NULL OR LEN(LTRIM(P.CellPhone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsPhoneVerified = P.IsPhoneVerified,
		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDefaultProjectID = P.DefaultProjectID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(
			PersonID,
			IsAccountLockedOut,
			IsActive,
			IsPhoneVerified,
			IsProfileUpdateRequired,
			IsSuperAdministrator,
			IsValidPassword,
			IsValidUserName,
			CellPhone,
			EmailAddress,
			FullName,
			RoleName,
			UserName,
			CountryCallingCodeID,
			DefaultProjectID
			) 
		VALUES 
			(
			@nPersonID,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cCellPhone,
			@cEmailAddress,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@nDefaultProjectID
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure person.ValidateUserName
EXEC Utility.DropObject 'person.ValidateUserName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.19
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateUserName

@UserName VARCHAR(250),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.UserName = @UserName
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateUserName