/* Build File - 01 - Tables - Core */
USE HEROS
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

/*
--Begin table core.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplate
	(
	EmailTemplateID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EmailTemplateCode VARCHAR(50),
	WorkflowActionCode VARCHAR(50),
	EmailSubject VARCHAR(500),
	EmailText VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode'
GO
--End table core.EmailTemplate
*/

--Begin table core.EmailTemplateField
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplateField'

EXEC utility.DropObject @TableName

CREATE TABLE core.EmailTemplateField
	(
	EmailTemplateFieldID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	PlaceHolderText VARCHAR(50),
	PlaceHolderDescription VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateFieldID'
EXEC utility.SetIndexClustered @TableName, 'IX_EmailTemplate', 'EntityTypeCode,DisplayOrder'
GO
--End table core.EmailTemplateField

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50),
	HasWorkflow BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.Incident
DECLARE @TableName VARCHAR(250) = 'core.Incident'

EXEC utility.DropObject @TableName

CREATE TABLE core.Incident
	(
	IncidentID INT IDENTITY(1,1) NOT NULL,
	IncidentName VARCHAR(250),
	IncidentDateTime DATETIME,
	IncidentCategoryID INT,
	IncidentImpactID INT,
	IncidentTypeID INT,
	InformationValidityID INT,
	ProjectID INT,
	Source VARCHAR(250),
	SourceReliabilityID INT,
	SourceTypeID INT,
	TerritoryID INT,
	Location GEOMETRY,
	ImpactDescription VARCHAR(MAX),
	KeyPoints VARCHAR(MAX),
	Lessons VARCHAR(MAX),
	Mission VARCHAR(250),
	MitigationEffectiveness VARCHAR(MAX),
	Summary VARCHAR(MAX),
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'IncidentCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentImpactID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IncidentTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InformationValidityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SourceReliabilityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SourceTypeID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IncidentID'
GO
--End table core.Incident

--Begin table core.MenuItem
DECLARE @TableName VARCHAR(250) = 'core.MenuItem'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItem
	(
	MenuItemID INT IDENTITY(1,1) NOT NULL,
	ParentMenuItemID INT,
	MenuItemCode VARCHAR(50),
	MenuItemText VARCHAR(250),
	MenuItemLink VARCHAR(500),
	DisplayOrder INT,
	Icon VARCHAR(50),
	IsForNewTab BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsForNewTab', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentMenuItemID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MenuItemID'
GO
--End table core.MenuItem

--Begin table core.MenuItemPermissionableLineage
DECLARE @TableName VARCHAR(250) = 'core.MenuItemPermissionableLineage'

EXEC utility.DropObject @TableName

CREATE TABLE core.MenuItemPermissionableLineage
	(
	MenuItemPermissionableLineageID INT IDENTITY(1,1) NOT NULL,
	MenuItemID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuItemPermissionableLineageID'
EXEC utility.SetIndexClustered @TableName, 'IX_MenuItemPermissionable', 'MenuItemID'
GO
--End table core.MenuItemPermissionableLineage

--Begin table core.RequestForInformation
DECLARE @TableName VARCHAR(250) = 'core.RequestForInformation'

EXEC utility.DropObject @TableName

CREATE TABLE core.RequestForInformation
	(
	RequestForInformationID INT IDENTITY(1,1) NOT NULL,
	RequestForInformationStatusID INT,
	RequestForInformationResultTypeID INT,
	RequestPersonID INT,
	PointOfContactPersonID INT,
	RequestForInformationTitle VARCHAR(100),
	KnownDetails VARCHAR(MAX),
	InformationRequested VARCHAR(MAX),
	IncidentDate DATE,
	RequestDate DATE,
	InProgressDate DATE,
	CompletedDate DATE,
	SummaryAnswer VARCHAR(MAX),
	DesiredResponseDate DATE,
	TerritoryID INT,
	ProjectID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'PointOfContactPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RequestDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'RequestForInformationResultTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RequestForInformationStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RequestPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RequestForInformationID'
GO
--End table core.RequestForInformation

/*
--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX),
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup
*/

--Begin table syslog.ApplicationErrorLog
DECLARE @TableName VARCHAR(250) = 'syslog.ApplicationErrorLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.ApplicationErrorLog
	(
	ApplicationErrorLogID INT IDENTITY(1,1) NOT NULL,
	ErrorDateTime	DATETIME,
	Error	VARCHAR(MAX),
	ErrorRCData	VARCHAR(MAX),
	ErrorSession VARCHAR(MAX),
	ErrorCGIData VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ErrorDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationErrorLogID'
GO
--End table syslog.ApplicationErrorLog

--Begin table syslog.BuildLog
DECLARE @TableName VARCHAR(250) = 'syslog.BuildLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.BuildLog
	(
	BuildLogID INT IDENTITY(1,1) NOT NULL,
	BuildKey VARCHAR(100) NULL,
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'BuildLogID DESC'
GO
--End table syslog.BuildLog

--Begin table syslog.DuoWebTwoFactorLog
DECLARE @TableName VARCHAR(250) = 'syslog.DuoWebTwoFactorLog'

EXEC utility.DropObject @TableName

CREATE TABLE syslog.DuoWebTwoFactorLog
	(
	DuoWebTwoFactorLogID INT IDENTITY(1,1) NOT NULL,
	HttpBaseURL NVARCHAR(MAX),
	DuoWebRequest NVARCHAR(MAX),
	DuoWebAuth NVARCHAR(MAX),
	DuoWebSig NVARCHAR(MAX),
	DuoMessage NVARCHAR(MAX),
	DuoWebRequestHttpDateTime	NVARCHAR(MAX),
	DuoWebResponse NVARCHAR(MAX),
	RequestDateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'RequestDateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DuoWebTwoFactorLogID DESC'
GO
--End table syslog.DuoWebTwoFactorLog
