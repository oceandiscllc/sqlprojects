/* Build File - 01 - Tables - Dropdown */
USE HEROS
GO

--Begin table dropdown.AssetCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetCategory
	(
	AssetCategoryID INT IDENTITY(0,1) NOT NULL,
	AssetCategoryCode VARCHAR(50) NULL,
	AssetCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AssetCategory', 'DisplayOrder,AssetCategoryName', 'AssetCategoryID'
GO
--End table dropdown.AssetCategory

--Begin table dropdown.AssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetType
	(
	AssetTypeID INT IDENTITY(0,1) NOT NULL,
	AssetTypeName VARCHAR(50),
	Icon VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AssetType', 'DisplayOrder,AssetTypeName', 'AssetTypeID'
GO
--End table dropdown.AssetType

--Begin table dropdown.ContactStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactStatus
	(
	ContactStatusID INT IDENTITY(0,1) NOT NULL,
	ContactStatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ContactStatus', 'DisplayOrder,ContactStatusName', 'ContactStatusID'
GO
--End table dropdown.ContactStatus

--Begin table dropdown.ContactType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactType
	(
	ContactTypeID INT IDENTITY(0,1) NOT NULL,
	ContactTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ContactType', 'DisplayOrder,ContactTypeName', 'ContactTypeID'
GO
--End table dropdown.ContactType

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	ISOCountryCode3 CHAR(3),
	CountryName NVARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryID'
EXEC utility.SetIndexClustered @TableName, 'IX_Country', 'DisplayOrder,CountryName'
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
DECLARE @TableName VARCHAR(250) = 'dropdown.CountryCallingCode'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CountryCallingCode
	(
	CountryCallingCodeID INT IDENTITY(0,1) NOT NULL,
	ISOCountryCode2 CHAR(2),
	CountryCallingCode INT 
	)

EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCode', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryCallingCodeID'
EXEC utility.SetIndexClustered @TableName, 'IX_CountryCallingCode', 'ISOCountryCode2,CountryCallingCode'
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.Currency
DECLARE @TableName VARCHAR(250) = 'dropdown.Currency'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Currency
	(
	CurrencyID INT IDENTITY(0,1) NOT NULL,
	ISOCurrencyCode CHAR(3),
	CurrencyName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CurrencyID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Currency', 'DisplayOrder,CurrencyName', 'CurrencyID'
GO
--End table dropdown.Currency

--Begin table dropdown.DocumentType
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentType
	(
	DocumentTypeID INT IDENTITY(0,1) NOT NULL,
	DocumentTypeCategory VARCHAR(50),
	DocumentTypeCode VARCHAR(50),
	DocumentTypeName VARCHAR(50),
	DocumentTypeCategoryDisplayOrder INT,
	DocumentTypeDisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeCategoryDisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DocumentTypeDisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_DocumentTypeName', 'DocumentTypeCategoryDisplayOrder,DocumentTypeCategory,DocumentTypeDisplayOrder,DocumentTypeName', 'DocumentTypeID'
GO

CREATE TRIGGER dropdown.TR_DocumentType ON dropdown.DocumentType AFTER INSERT, UPDATE
AS
SET ARITHABORT ON

UPDATE DT
SET DT.DocumentTypeCode = REPLACE(I.DocumentTypeName, ' ', '')
FROM INSERTED I
	JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = I.DocumentTypeID
		AND DT.DocumentTypeCode IS NULL
		AND DT.DocumentTypeID > 0

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description)
SELECT
	'Document',
	'View',
	DT.DocumentTypeCode,
	ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = 'Document'), 0),
	'View documents of type ' + LOWER(DT.DocumentTypeName) + ' in the library'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IS NOT NULL
	AND DT.DocumentTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Document.View.' + DT.DocumentTypeCode
		)
GO
--End table dropdown.DocumentType

--Begin table dropdown.ForceCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.ForceCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ForceCategory
	(
	ForceCategoryID INT IDENTITY(0,1) NOT NULL,
	ForceCategoryCode VARCHAR(50),
	ForceCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ForceCategory', 'DisplayOrder,ForceCategoryName', 'ForceCategoryID'
GO
--End table dropdown.ForceCategory

--Begin table dropdown.ForceType
DECLARE @TableName VARCHAR(250) = 'dropdown.ForceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ForceType
	(
	ForceTypeID INT IDENTITY(0,1) NOT NULL,
	ForceTypeName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ForceType', 'DisplayOrder,ForceTypeName', 'ForceTypeID'
GO
--End table dropdown.ForceType

--Begin table dropdown.FundingSource
DECLARE @TableName VARCHAR(250) = 'dropdown.FundingSource'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FundingSource
	(
	FundingSourceID INT IDENTITY(0,1) NOT NULL,
	FundingSourceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'FundingSourceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FundingSource', 'DisplayOrder,FundingSourceName', 'FundingSourceID'
GO
--End table dropdown.FundingSource

--Begin table dropdown.ImpactDecision
DECLARE @TableName VARCHAR(250) = 'dropdown.ImpactDecision'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ImpactDecision
	(
	ImpactDecisionID INT IDENTITY(0,1) NOT NULL,
	ImpactDecisionName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImpactDecisionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ImpactDecisionName', 'DisplayOrder,ImpactDecisionName', 'ImpactDecisionID'
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.IncidentCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.IncidentCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IncidentCategory
	(
	IncidentCategoryID INT IDENTITY(0,1) NOT NULL,
	IncidentCategoryCode VARCHAR(50),
	IncidentCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IncidentCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IncidentCategory', 'DisplayOrder,IncidentCategoryName', 'IncidentCategoryID'
GO
--End table dropdown.IncidentCategory

--Begin table dropdown.IncidentImpact
DECLARE @TableName VARCHAR(250) = 'dropdown.IncidentImpact'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IncidentImpact
	(
	IncidentImpactID INT IDENTITY(0,1) NOT NULL,
	IncidentImpactName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IncidentImpactID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IncidentImpact', 'DisplayOrder,IncidentImpactName', 'IncidentImpactID'
GO
--End table dropdown.IncidentImpact

--Begin table dropdown.IncidentType
DECLARE @TableName VARCHAR(250) = 'dropdown.IncidentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IncidentType
	(
	IncidentTypeID INT IDENTITY(0,1) NOT NULL,
	IncidentTypeCategory VARCHAR(50),
	IncidentTypeName VARCHAR(50),
	Icon VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IncidentTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IncidentTypeName', 'IncidentTypeCategory,DisplayOrder,IncidentTypeName', 'IncidentTypeID'
GO
--End table dropdown.IncidentType

--Begin table dropdown.InformationValidity
DECLARE @TableName VARCHAR(250) = 'dropdown.InformationValidity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InformationValidity
	(
	InformationValidityID INT IDENTITY(0,1) NOT NULL,
	InformationValidityName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'InformationValidityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InformationValidity', 'DisplayOrder,InformationValidityName', 'InformationValidityID'
GO
--End table dropdown.InformationValidity

--Begin table dropdown.LearnerProfileType
DECLARE @TableName VARCHAR(250) = 'dropdown.LearnerProfileType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LearnerProfileType
	(
	LearnerProfileTypeID INT IDENTITY(0,1) NOT NULL,
	LearnerProfileTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LearnerProfileTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LearnerProfileType', 'DisplayOrder,LearnerProfileTypeName', 'LearnerProfileTypeID'
GO
--End table dropdown.LearnerProfileType

--Begin table dropdown.ModuleType
DECLARE @TableName VARCHAR(250) = 'dropdown.ModuleType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ModuleType
	(
	ModuleTypeID INT IDENTITY(0,1) NOT NULL,
	ModuleTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ModuleTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ModuleType', 'DisplayOrder,ModuleTypeName', 'ModuleTypeID'
GO
--End table dropdown.ModuleType

--Begin table dropdown.PasswordSecurityQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.PasswordSecurityQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PasswordSecurityQuestion
	(
	PasswordSecurityQuestionID INT IDENTITY(0, 1) NOT NULL,
	GroupID INT,
	PasswordSecurityQuestionName VARCHAR(500),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GroupID', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PasswordSecurityQuestionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PasswordSecurityQuestion', 'GroupID,DisplayOrder,PasswordSecurityQuestionName'
GO
--End table dropdown.PasswordSecurityQuestion

--Begin table dropdown.ProgramType
DECLARE @TableName VARCHAR(250) = 'dropdown.ProgramType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProgramType
	(
	ProgramTypeID INT IDENTITY(0,1) NOT NULL,
	ProgramTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProgramTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProgramType', 'DisplayOrder,ProgramTypeName', 'ProgramTypeID'
GO
--End table dropdown.ProgramType

--Begin table dropdown.Project
DECLARE @TableName VARCHAR(250) = 'dropdown.Project'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Project
	(
	ProjectID INT IDENTITY(0,1) NOT NULL,
	ProjectCode VARCHAR(50),
	ProjectAlias VARCHAR(50),
	ProjectName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_Project', 'DisplayOrder,ProjectName'
GO

EXEC utility.DropObject 'dropdown.TR_Project'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create date:	2017.09.29
-- Description:	A trigger to update the dropdown.Project table
-- ===========================================================
CREATE TRIGGER dropdown.TR_Project ON dropdown.Project AFTER INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	INSERT INTO territory.TerritoryAnnex
		(TerritoryID, ProjectID)
	SELECT
		T.TerritoryID,
		I.ProjectID
	FROM INSERTED I, territory.Territory T
	WHERE I.ProjectID > 0
		AND I.IsActive = 1
		AND NOT EXISTS
			(
			SELECT 1
			FROM territory.TerritoryAnnex TA
			WHERE TA.TerritoryID = T.TerritoryID
				AND TA.ProjectID = I.ProjectID
			)

	END
--ENDIF

GO
--End table dropdown.Project

--Begin table dropdown.RelevantTheme
DECLARE @TableName VARCHAR(250) = 'dropdown.RelevantTheme'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RelevantTheme
	(
	RelevantThemeID INT IDENTITY(0,1) NOT NULL,
	RelevantThemeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RelevantThemeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_RelevantTheme', 'DisplayOrder,RelevantThemeName', 'RelevantThemeID'
GO
--End table dropdown.RelevantTheme

--Begin table dropdown.RequestForInformationResultType
DECLARE @TableName VARCHAR(250) = 'dropdown.RequestForInformationResultType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RequestForInformationResultType
	(
	RequestForInformationResultTypeID INT IDENTITY(0,1) NOT NULL,
	RequestForInformationResultTypeCode VARCHAR(50),
	RequestForInformationResultTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RequestForInformationResultTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_RequestForInformationResultTypeName', 'DisplayOrder,RequestForInformationResultTypeName', 'RequestForInformationResultTypeID'
GO
--End table dropdown.RequestForInformationResultType

--Begin table dropdown.RequestForInformationStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.RequestForInformationStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RequestForInformationStatus
	(
	RequestForInformationStatusID INT IDENTITY(0,1) NOT NULL,
	RequestForInformationStatusCode VARCHAR(50),
	RequestForInformationStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RequestForInformationStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_RequestForInformationStatusName', 'DisplayOrder,RequestForInformationStatusName', 'RequestForInformationStatusID'
GO
--End table dropdown.RequestForInformationStatus

--Begin table dropdown.ResourceProvider
DECLARE @TableName VARCHAR(250) = 'dropdown.ResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ResourceProvider
	(
	ResourceProviderID INT IDENTITY(0,1) NOT NULL,
	ResourceProviderName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ResourceProviderID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ResourceProvider', 'DisplayOrder,ResourceProviderName', 'ResourceProviderID'
GO
--End table dropdown.ResourceProvider

--Begin table dropdown.Role
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleName VARCHAR(50),
	RoleCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Role', 'DisplayOrder,RoleName', 'RoleID'
GO
--End table dropdown.Role

--Begin table dropdown.SourceReliability
DECLARE @TableName VARCHAR(250) = 'dropdown.SourceReliability'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SourceReliability
	(
	SourceReliabilityID INT IDENTITY(0,1) NOT NULL,
	SourceReliabilityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SourceReliabilityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SourceReliability', 'DisplayOrder,SourceReliabilityName', 'SourceReliabilityID'
GO
--End table dropdown.SourceReliability

--Begin table dropdown.SourceType
DECLARE @TableName VARCHAR(250) = 'dropdown.SourceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SourceType
	(
	SourceTypeID INT IDENTITY(0,1) NOT NULL,
	SourceTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SourceTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SourceType', 'DisplayOrder,SourceTypeName', 'SourceTypeID'
GO
--End table dropdown.SourceType

--Begin table dropdown.StatusChange
DECLARE @TableName VARCHAR(250) = 'dropdown.StatusChange'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.StatusChange
	(
	StatusChangeID INT IDENTITY(0,1) NOT NULL,
	StatusChangeName VARCHAR(50),
	HexColor VARCHAR(7),
	Direction VARCHAR(6),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'StatusChangeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_StatusChangeName', 'DisplayOrder,StatusChangeName', 'StatusChangeID'
GO
--End table dropdown.StatusChange

--Begin table dropdown.TerritoryStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.TerritoryStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TerritoryStatus
	(
	TerritoryStatusID INT IDENTITY(0,1) NOT NULL,
	TerritoryStatusCode VARCHAR(50),
	TerritoryStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TerritoryStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_TerritoryStatusName', 'DisplayOrder,TerritoryStatusName', 'TerritoryStatusID'
GO
--End table dropdown.TerritoryStatus

--Begin table dropdown.TerritoryType
DECLARE @TableName VARCHAR(250) = 'dropdown.TerritoryType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TerritoryType
	(
	TerritoryTypeID INT IDENTITY(1,1) NOT NULL,
	TerritoryTypeCode VARCHAR(50),
	TerritoryTypeCodePlural VARCHAR(50),
	TerritoryTypeName VARCHAR(50),
	TerritoryTypeNamePlural VARCHAR(50),
	IsReadOnly BIT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsReadOnly', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'TerritoryTypeID'
GO
--End table dropdown.TerritoryType

--Begin table dropdown.UnitType
DECLARE @TableName VARCHAR(250) = 'dropdown.UnitType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.UnitType
	(
	UnitTypeID INT IDENTITY(0,1) NOT NULL,
	UnitTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'UnitTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_UnitType', 'DisplayOrder,UnitTypeName', 'UnitTypeID'
GO
--End table dropdown.UnitType