/* Build File - 01 - Tables - Person */
USE HEROS
GO

/*
--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject @TableName

CREATE TABLE person.Person
	(
	--Begin standard fields
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	NickName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	RoleID INT,
	EmailAddress VARCHAR(320),
	Organization NVARCHAR(250),
	CountryCallingCodeID INT,
	CellPhone VARCHAR(15),
	IsPhoneVerified BIT,
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	IsActive BIT,
	IsSuperAdministrator BIT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,
	DefaultProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCodeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DefaultProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsPhoneVerified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person
*/

--Begin table person.PersonPasswordSecurity
DECLARE @TableName VARCHAR(250) = 'person.PersonPasswordSecurity'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonPasswordSecurity
	(
	PersonPasswordSecurityID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PasswordSecurityQuestionID INT,
	PasswordSecurityQuestionAnswer VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PasswordSecurityQuestionID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPasswordSecurityID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPasswordSecurity', 'PersonID,PasswordSecurityQuestionID'
GO
--End table person.PersonPasswordSecurity

--Begin table person.PersonPermissionable
DECLARE @TableName VARCHAR(250) = 'person.PersonPermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonPermissionable
	(
	PersonPermissionableID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PermissionableLineage VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPermissionableID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonPermissionable', 'PersonID,PermissionableLineage'
GO
--End table person.PersonPermissionable

--Begin table person.PersonProject
DECLARE @TableName VARCHAR(250) = 'person.PersonProject'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonProject
	(
	PersonProjectID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ProjectID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonProjectID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonProject', 'PersonID,ProjectID'
GO
--End table person.PersonPermissionable
