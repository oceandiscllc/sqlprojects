/* Build File - 02 - Functions */
USE HEROS
GO

--Begin function contact.FormatContactNameByContactID
EXEC utility.DropObject 'contact.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return the name of a contact in a specified format from a ContactID
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Add middle name support
-- ==============================================================================================

CREATE FUNCTION contact.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName NVARCHAR(200)
	DECLARE @cMiddleName NVARCHAR(200)
	DECLARE @cLastName NVARCHAR(200)
	DECLARE @cTitle NVARCHAR(100)
	DECLARE @cRetVal VARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cMiddleName = ISNULL(C.MiddleName, ''),
			@cLastName = ISNULL(C.LastName, '')
		FROM contact.Contact C
		WHERE C.ContactID = @ContactID
	
		IF @Format = 'FirstLast' OR @Format = 'FirstMiddleLast'
			BEGIN
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal = @cFirstName + ' '
			--ENDIF

			IF @Format = 'FirstMiddleLast' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal += @cLastName + ' '
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstMiddle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal = @cLastName + ', '
			--ENDIF
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal += @cFirstName + ' '
			--ENDIF
	
			IF @Format = 'LastFirstMiddle' AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			END
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function contact.FormatContactNameByContactID

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(17), @DateTimeData, 113)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(8), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION core.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM core.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin function core.GetEntityTypeNamePluralByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNamePluralByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return the plural name of an entity type based on an entity type code
-- ================================================================================================

CREATE FUNCTION core.GetEntityTypeNamePluralByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeNamePlural
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNamePluralByEntityTypeCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a table from a delimted list of values - based on a script by Kshitij Satpute from SQLSystemCentral.com
-- =========================================================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function document.FormatPhysicalFileSize
EXEC utility.DropObject 'document.FormatPhysicalFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a formatted file size
-- =======================================================

CREATE FUNCTION document.FormatPhysicalFileSize
(
@PhysicalFileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cFileSize VARCHAR(50)

SELECT @cFileSize = 

	CASE
		WHEN @PhysicalFileSize < 1000
		THEN CAST(@PhysicalFileSize as VARCHAR(50)) + ' b'
		WHEN @PhysicalFileSize < 1000000
		THEN CAST(ROUND((@PhysicalFileSize/1000), 2) AS VARCHAR(50)) + ' kb'
		WHEN @PhysicalFileSize < 1000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
		WHEN @PhysicalFileSize < 1000000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
		ELSE 'More than 1000 gb'
	END

RETURN ISNULL(@cFileSize, '0 b')

END
GO
--End function document.FormatPhysicalFileSize

--Begin function dropdown.GetCountryNameFromISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameFromISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.17
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameFromISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameFromISOCountryCode

--Begin function dropdown.GetProjectNameByProjectID
EXEC utility.DropObject 'dropdown.GetProjectNameByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date: 2015.12.22
-- Description:	A funtion to get a project name from a project id
-- ==============================================================
CREATE FUNCTION dropdown.GetProjectNameByProjectID
(
@ProjectID INT
)
RETURNS VARCHAR(50)

AS
BEGIN
	DECLARE @cReturn VARCHAR(50)

	SELECT @cReturn = P.ProjectName
	FROM dropdown.Project P
	WHERE P.ProjectID = @ProjectID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dropdown.GetProjectNameByProjectID

--Begin function eventlog.GetEventNameByEventCode
EXEC utility.DropObject 'core.GetEventNameByEventCode'
EXEC utility.DropObject 'eventlog.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION eventlog.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'add'
			THEN 'Add'
			WHEN @EventCode = 'addupdate'
			THEN 'Update'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'logout'
			THEN 'Logout'
			WHEN @EventCode = 'subscribe'
			THEN 'Subscribe'
			WHEN @EventCode = 'update'
			THEN 'Update'
			WHEN @EventCode = 'view'
			THEN 'View'
			ELSE @EventCodeName
		END

	RETURN @EventCodeName

END
GO
--End function eventlog.GetEventNameByEventCode

--Begin function person.CanAccessProject
EXEC utility.DropObject 'person.CanAccessProject'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.20
-- Description:	A function to return a bit indicating weather or not a person id can access a projectid
-- ====================================================================================================
CREATE FUNCTION person.CanAccessProject
(
@PersonID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanAccessProject BIT = 0

	IF EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = @ProjectID)
		SET @nCanAccessProject = 1
	--ENDIF

	RETURN @nCanAccessProject

END
GO
--End function person.CanAccessProject

--Begin function person.CanHaveAccess
EXEC utility.DropObject 'person.CanHaveAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A function to determine if a PersonID has access to a specific entity type / entity id
-- ===================================================================================================

CREATE FUNCTION person.CanHaveAccess
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAccess BIT = 0

	IF @EntityTypeCode IN ('SpotReport', 'TrendReport')
		BEGIN

		IF EXISTS
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EWSGP.PersonID = @PersonID
			) 
			OR (workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) - workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)) > 0

			BEGIN

				SET @nCanHaveAccess = 1

			END
		--ENDIF

		END
	--ENDIF

	RETURN @nCanHaveAccess

END
GO
--End function person.CanHaveAccess

--Begin function person.CheckFileAccess
EXEC utility.DropObject 'person.CheckFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A function to determine if a PeronID has permission to a file
-- ==========================================================================

CREATE FUNCTION person.CheckFileAccess
(
@PersonID INT,
@DocumentName VARCHAR(50)
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCheckFileAccess BIT = 0

	IF EXISTS 
		(
		SELECT 1
		FROM person.PersonPermissionable PP
		WHERE PP.PersonID = @PersonID
			AND 
				(
				PP.PermissionableLineage = 
					(
					SELECT 'Document.View.' + DT.DocumentTypeCode
					FROM document.Document D
						JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
							AND D.DocumentName = @DocumentName
					)
					OR
						( 
						(SELECT D.DocumentTypeID FROM document.Document D WHERE D.DocumentName = @DocumentName) = 0
						)
				)
		)		
		SET @nCheckFileAccess = 1
	--ENDIF
	
	RETURN @nCheckFileAccess

END
GO
--End function person.CheckFileAccess

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @FirstName VARCHAR(25)
	DECLARE @LastName VARCHAR(25)
	DECLARE @cRetVal VARCHAR(250)
	DECLARE @Title VARCHAR(50)
	
	SET @cRetVal = ''
	
	SELECT
		@FirstName = ISNULL(P.FirstName, ''),
		@LastName = ISNULL(P.LastName, ''),
		@Title = ISNULL(P.Title, '')
	FROM person.Person P
	WHERE P.PersonID = @PersonID
	
	IF @Format = 'FirstLast' OR @Format = 'TitleFirstLast'
		BEGIN
			
		SET @cRetVal = @FirstName + ' ' + @LastName
	
		IF @Format = 'TitleFirstLast' AND LEN(RTRIM(@Title)) > 0
			SET @cRetVal = @Title + ' ' + RTRIM(LTRIM(@cRetVal))
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format = 'LastFirst' OR @Format = 'LastFirstTitle'
		BEGIN
			
		IF LEN(RTRIM(@LastName)) > 0
			SET @cRetVal = @LastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @FirstName + ' '
	
		IF @Format = 'LastFirstTitle' AND LEN(RTRIM(@Title)) > 0
			SET @cRetVal = @cRetVal + @Title
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION person.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PermissionableLineage = @PermissionableLineage AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function person.HasPermission

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION person.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = Password,
		@cPasswordSalt = PasswordSalt
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function person.ValidatePassword

--Begin function territory.FormatParentTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.FormatParentTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.03
-- Description:	A function to return the formatted name of a territory parent
-- ==========================================================================

CREATE FUNCTION territory.FormatParentTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cTerritoryNameFormatted VARCHAR(300) = ''
	
	SELECT @cTerritoryNameFormatted = T2.TerritoryName + ' (' + TT.TerritoryTypeName + ')' 
	FROM territory.Territory T1
		JOIN territory.Territory T2 ON T2.TerritoryID = T1.ParentTerritoryID
		JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T2.TerritoryTypeCode
			AND T1.TerritoryID = @TerritoryID
	
	RETURN ISNULL(@cTerritoryNameFormatted, '')
	
END
GO
--End function territory.FormatParentTerritoryNameByTerritoryID

--Begin function territory.FormatTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.FormatTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create Date:	2017.01.08
-- Description:	A function to return the formatted name of a territory
-- ===================================================================

CREATE FUNCTION territory.FormatTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cTerritoryNameFormatted VARCHAR(300) = ''
	
	SELECT @cTerritoryNameFormatted = T.TerritoryName + ' (' + TT.TerritoryTypeName + ')' 
	FROM territory.Territory T 
		JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode
			AND T.TerritoryID = @TerritoryID
	
	RETURN ISNULL(@cTerritoryNameFormatted, '')
	
END
GO
--End function territory.FormatTerritoryNameByTerritoryID

--Begin function territory.GetDescendantTerritoriesByByTerritoryID
EXEC utility.DropObject 'territory.GetDescendantTerritoriesByByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2017.01.08
-- Description:	A function to return the descendant territories of a territory
-- ===========================================================================

CREATE FUNCTION territory.GetDescendantTerritoriesByByTerritoryID
(
@TerritoryID INT
)

RETURNS @tTable TABLE (TerritoryID INT NOT NULL PRIMARY KEY)

AS
BEGIN

	WITH HD (ParentTerritoryID,TerritoryID,NodeLevel) AS
		(
		SELECT
			T.ParentTerritoryID,
			T.TerritoryID,
			1
		FROM territory.Territory T
		WHERE T.ParentTerritoryID = @TerritoryID

		UNION ALL

		SELECT
			HD.TerritoryID AS ParentTerritoryID,
			T.TerritoryID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM territory.Territory T
				JOIN HD ON HD.TerritoryID = T.ParentTerritoryID
		)	

	INSERT INTO @tTable
		(TerritoryID)
	SELECT @TerritoryID

	UNION

	SELECT HD.TerritoryID
	FROM HD

	RETURN
	
END
GO
--End function territory.GetDescendantTerritoriesByByTerritoryID

--Begin function territory.GetTerritoryNameByTerritoryID
EXEC utility.DropObject 'territory.GetTerritoryNameByTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2017.09.29
-- Description:	A function to return the name of a territory
-- =========================================================

CREATE FUNCTION territory.GetTerritoryNameByTerritoryID
(
@TerritoryID INT
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cTerritoryName VARCHAR(250) = ''
	
	SELECT @cTerritoryName = T.TerritoryName
	FROM territory.Territory T 
	WHERE T.TerritoryID = @TerritoryID
	
	RETURN ISNULL(@cTerritoryName, '')
	
END
GO
--End function territory.GetTerritoryNameByTerritoryID

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT

	SELECT @nWorkflowStepCount = MAX(EWSGP.WorkflowStepNumber) 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID
	
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT

	SELECT TOP 1 @nWorkflowStepNumber = EWSGP.WorkflowStepNumber 
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID 
		AND EWSGP.IsComplete = 0 
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber

END
GO
--End function workflow.GetWorkflowStepNumber