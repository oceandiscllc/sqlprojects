/* Build File - 03 - Procedures - Permissionable */
USE HEROS
GO

--Begin procedure permissionable.DeletePermissionable
EXEC Utility.DropObject 'permissionable.DeletePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to delete data from the permissionable.Permissionable and person.PersonPermissionable tables
-- ============================================================================================================================
CREATE PROCEDURE permissionable.DeletePermissionable

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPermissionableLineage VARCHAR(MAX)

	SELECT
		@cPermissionableLineage = P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE P
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = MIPL.PermissionableLineage
		)

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PP.PermissionableLineage
		)

	DELETE PTP
	FROM permissionable.PermissionableTemplatePermissionable PTP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PTP.PermissionableLineage
		)

	SELECT @cPermissionableLineage AS PermissionableLineage
		
END
GO
--End procedure permissionable.DeletePermissionable

--Begin procedure permissionable.DeletePermissionableTemplate
EXEC Utility.DropObject 'permissionable.DeletePermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to delete Permissionable Template data
-- ======================================================================
CREATE PROCEDURE permissionable.DeletePermissionableTemplate

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PT
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID

END
GO
--End procedure permissionable.DeletePermissionableTemplate

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the permissionable.Permissionable table
-- ====================================================================================
CREATE PROCEDURE permissionable.GetPermissionableByPermissionableID

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PermissionableID,
		P.ControllerName,
		P.MethodName,
		P.PermissionCode,
		P.PermissionableGroupID,
		P.Description,
		P.IsActive,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.DisplayOrder
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID
		
END
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionableGroups
EXEC Utility.DropObject 'permissionable.GetPermissionableGroups'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.PermissionableGroup table
-- =============================================================================================
CREATE PROCEDURE permissionable.GetPermissionableGroups

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PG.PermissionableGroupID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		PG.DisplayOrder,
		(SELECT COUNT(P.PermissionableGroupID) FROM permissionable.Permissionable P WHERE P.PermissionableGroupID = PG.PermissionableGroupID) AS ItemCount
	FROM permissionable.PermissionableGroup PG
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, PG.PermissionableGroupID
		
END
GO
--End procedure permissionable.GetPermissionableGroups

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.Permissionable table
-- ========================================================================================
CREATE PROCEDURE permissionable.GetPermissionables

@PersonID INT = 0,
@PermissionableTemplateID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ControllerName,
		P.Description,
		P.DisplayOrder,
		P.IsActive,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.MethodName,
		P.PermissionableLineage,
		P.PermissionCode,
		P.PermissionableID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM person.PersonPermissionable PP WHERE PP.PersonID = @PersonID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			WHEN @PermissionableTemplateID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID AND PTP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			ELSE 0
		END AS HasPermissionable
						
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage
		
END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionableTemplateByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplate table
-- ================================================================================================
CREATE PROCEDURE permissionable.GetPermissionableTemplateByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PT.PermissionableTemplateID,
		PT.PermissionableTemplateName
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID

--Begin procedure permissionable.GetPermissionsByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionsByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplatePermission table
-- ==========================================================================================================
CREATE PROCEDURE permissionable.GetPermissionsByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.PermissionableID
	FROM permissionable.PermissionableTemplatePermissionable PTP
		JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
			AND PTP.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionsByPermissionableTemplateID

--Begin procedure permissionable.SavePermissionable
EXEC utility.DropObject 'permissionable.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE permissionable.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@PermissionableGroupCode VARCHAR(50),
@Description VARCHAR(MAX), 
@DisplayOrder INT = 0,
@IsActive BIT = 1,
@IsGlobal BIT = 0, 
@IsSuperAdministrator BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description,
			P.DisplayOrder = @DisplayOrder,
			P.IsActive = @IsActive,
			P.IsGlobal = @IsGlobal,
			P.IsSuperAdministrator = @IsSuperAdministrator
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, DisplayOrder, IsActive, IsGlobal, IsSuperAdministrator) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@DisplayOrder,
			@IsActive, 
			@IsGlobal,
			@IsSuperAdministrator
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionable

--Begin procedure permissionable.SavePermissionableGroup
EXEC utility.DropObject 'permissionable.SavePermissionableGroup'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to save data to the permissionable.PermissionableGroup table
-- ============================================================================================
CREATE PROCEDURE permissionable.SavePermissionableGroup

@PermissionableGroupCode VARCHAR(50),
@PermissionableGroupName VARCHAR(250),
@DisplayOrder INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode)
		BEGIN
		
		UPDATE PG
		SET 
			PG.PermissionableGroupName = @PermissionableGroupName,
			PG.DisplayOrder = @DisplayOrder
		FROM permissionable.PermissionableGroup PG
		WHERE PG.PermissionableGroupCode = @PermissionableGroupCode
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.PermissionableGroup 
			(PermissionableGroupCode, PermissionableGroupName, DisplayOrder) 
		VALUES 
			(
			@PermissionableGroupCode, 
			@PermissionableGroupName, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure permissionable.SavePermissionableGroup

--Begin procedure permissionable.UpdateParentPermissionableLineageByMenuItemCode
EXEC utility.DropObject 'permissionable.UpdateParentPermissionableLineageByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to update the menuitem permissionable linage records of a parent from its descendants
-- =====================================================================================================================
CREATE PROCEDURE permissionable.UpdateParentPermissionableLineageByMenuItemCode

@MenuItemCode VARCHAR(50)

AS
BEGIN

	DELETE MIPL
	FROM core.MenuItemPermissionableLineage MIPL
		JOIN core.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
			AND MI.MenuItemCode = @MenuItemCode
	
	INSERT INTO core.MenuItemPermissionableLineage
		(MenuItemID,PermissionableLineage)
	SELECT 
		(SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode),
		MIPL1.PermissionableLineage
	FROM core.MenuItemPermissionableLineage MIPL1
		JOIN core.GetDescendantMenuItemsByMenuItemCode(@MenuItemCode) MI ON MI.MenuItemID = MIPL1.MenuItemID
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.MenuItemPermissionableLineage MIPL2
				WHERE MIPL2.MenuItemID = (SELECT MI.MenuItemID FROM core.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode)
					AND MIPL2.PermissionableLineage = MIPL1.PermissionableLineage
				)

END
GO
--End procedure permissionable.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure permissionable.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'permissionable.UpdateSuperAdministratorPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to update PersonPermissionable data for Super Admins
-- ====================================================================================
CREATE PROCEDURE permissionable.UpdateSuperAdministratorPersonPermissionables

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
		JOIN person.Person P ON P.PersonID = PP.PersonID
			AND P.IsSuperAdministrator = 1
	
	INSERT INTO person.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM person.Person P1
		CROSS JOIN permissionable.Permissionable P2
	WHERE P1.IsSuperAdministrator = 1
		AND P1.PersonID > 0

END	
GO
--End procedure permissionable.UpdateSuperAdministratorPersonPermissionables