--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(5)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(5), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin procedure responsedashboard.FormatContactNameByContactID
EXEC Utility.DropObject 'responsedashboard.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A function to return the name of a contact in a specified format from a ContactID
-- ==============================================================================================

CREATE FUNCTION responsedashboard.FormatContactNameByContactID
(
@ContactID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(50)
	DECLARE @cLastName VARCHAR(50)
	DECLARE @cOrganizationName VARCHAR(50)
	DECLARE @cRetVal VARCHAR(250) = ''
	DECLARE @cRoleName VARCHAR(50)
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cLastName = ISNULL(C.LastName, ''),
			@cOrganizationName = ISNULL(C.OrganizationName, ''),
			@cRoleName = ISNULL(C.RoleName, '')
		FROM responsedashboard.Contact C
		WHERE C.ContactID = @ContactID
	
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal += @cLastName + ' '
		--ENDIF

		IF LEN(RTRIM(@cLastName)) > 0 AND LEN(RTRIM(@cFirstName)) > 0
			SET @cRetVal = RTRIM(@cRetVal) + ', '
		--ENDIF
			
		IF LEN(RTRIM(@cFirstName)) > 0
			SET @cRetVal += @cFirstName + ' '
		--ENDIF
			
		IF LEN(RTRIM(@cRoleName)) > 0
			SET @cRetVal += '- ' + @cRoleName + ' '
		--ENDIF
	
		IF LEN(RTRIM(@cOrganizationName)) > 0
			SET @cRetVal += '(' + @cOrganizationName + ')'
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO


