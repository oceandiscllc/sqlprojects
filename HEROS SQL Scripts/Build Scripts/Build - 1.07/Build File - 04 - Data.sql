
--Begin table dropdown.MetricType
TRUNCATE TABLE dropdown.MetricType
GO

EXEC utility.InsertIdentityValue 'dropdown.MetricType', 'MetricTypeID', 0
GO

INSERT INTO dropdown.MetricType
	(MetricTypeCode, MetricTypeName)
VALUES
	('Aid', 'UK aid funds committed'), 
	('Casualty', 'confirmed dead'), 
	('IDP', 'internally displaced people')

GO
--End table dropdown.MetricType

--Begin table dropdown.TimeLineItemType
TRUNCATE TABLE dropdown.TimeLineItemType
GO

EXEC utility.InsertIdentityValue 'dropdown.TimeLineItemType', 'TimeLineItemTypeID', 0
GO

INSERT INTO dropdown.TimeLineItemType
	(TimeLineItemTypeCode, TimeLineItemTypeName, HexColor)
VALUES
	('Activity', 'Activity', '#fff4b2'),
	('Event', 'Event', '#e1d4f7'),
	('Milestone', 'Milestone', '#ddefc9')
GO
--End table dropdown.TimeLineItemType


