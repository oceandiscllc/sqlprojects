USE [HermisCloud]
GO
SET IDENTITY_INSERT [responsedashboard].[Contact] ON 
GO
INSERT [responsedashboard].[Contact] ([ContactID], [ResponseDashboardID], [FirstName], [LastName], [RoleName], [ContactDetails], [OrganizationName]) VALUES (1, 1, N'Jack', N'Smith', N'HSOT Response Lead', N'Tel 12345678', N'Palladium')
GO
SET IDENTITY_INSERT [responsedashboard].[Contact] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[FAQ] ON 
GO
INSERT [responsedashboard].[FAQ] ([FAQID], [ResponseDashboardID], [FAQKey], [FAQValue]) VALUES (1, 1, N'Have the UK deployed any SAR specialists to the region', N'2 teams of specialists totalling 18 people were deployed on the 30th October and returned on the 4th November')
GO
INSERT [responsedashboard].[FAQ] ([FAQID], [ResponseDashboardID], [FAQKey], [FAQValue]) VALUES (2, 1, N'Has the UK sent any aid', N'Over 40 tons of aid including tents, water purification tools and shovels have been recieved ')
GO
SET IDENTITY_INSERT [responsedashboard].[FAQ] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[Location] ON 
GO
INSERT [responsedashboard].[Location] ([LocationID], [ResponseDashboardID], [LocationName], [DeployeeCount]) VALUES (1, 1, N'Palu', 10)
GO
INSERT [responsedashboard].[Location] ([LocationID], [ResponseDashboardID], [LocationName], [DeployeeCount]) VALUES (2, 1, N'London', 5)
GO
INSERT [responsedashboard].[Location] ([LocationID], [ResponseDashboardID], [LocationName], [DeployeeCount]) VALUES (3, 1, N'Jakarta', 2)
GO
SET IDENTITY_INSERT [responsedashboard].[Location] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[Metric] ON 
GO
INSERT [responsedashboard].[Metric] ([MetricID], [ResponseDashboardID], [MetricTypeID], [MetricValue], [MetricDateTime]) VALUES (1, 1, 1, N'£25M', CAST(N'2018-10-31T12:00:00.000' AS DateTime))
GO
INSERT [responsedashboard].[Metric] ([MetricID], [ResponseDashboardID], [MetricTypeID], [MetricValue], [MetricDateTime]) VALUES (2, 1, 2, N'874', CAST(N'2018-11-01T12:00:00.000' AS DateTime))
GO
INSERT [responsedashboard].[Metric] ([MetricID], [ResponseDashboardID], [MetricTypeID], [MetricValue], [MetricDateTime]) VALUES (3, 1, 3, N'34,208', CAST(N'2018-11-02T13:14:26.000' AS DateTime))
GO
SET IDENTITY_INSERT [responsedashboard].[Metric] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[ResponseDashboard] ON 
GO
INSERT [responsedashboard].[ResponseDashboard] ([ResponseDashboardID], [ResponseName], [NewsfeedURL], [ProjectID], [IsActive]) VALUES (1, N'Indonesia Tsunami 2018', N'https://reliefweb.int/disasters/rss.xml?country=120', 3, 1)
GO
SET IDENTITY_INSERT [responsedashboard].[ResponseDashboard] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[TimeLineItem] ON 
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (1, 1, N'Magnitude 7.5 earthquake', N'Magnitude 7.5 earthquake struck Sulawesi regional of Indonesia at approximately 1002 GMT
Governor of Central Sulawesi called an emergency response for 14 days from 28/09 to 11/10', CAST(N'2018-09-28T10:05:00.000' AS DateTime), 2)
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (2, 1, N'Government of Indonesia welcomed offer of international assistance', NULL, CAST(N'2018-09-29T09:00:00.000' AS DateTime), 2)
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (3, 1, N'Contact with GoI Established', N'SOS has approved funding envelope of £2 million for immediate response programming 
Call with HMA Indonesia 
Coordination call with post (DFID, HSOT, MOD)
Indonesian Deputy FM has established contact between DFID Indonesia and the Foreign Ministry, Coordinating Ministry and disaster agency 
DFID Indonesia spoken with ICRC and Save the Children. Waiting for updates from World Vision and CARE. DEC appeal meeting due today
Consider NFI response options 
British Embassy Jakarta shared a Note Verbale with GoI detailing an initial indicative offer of assistance which included several thousand shelter kits (exact number to be confirmed) and air transport (AN12, A400m or C130 aircrafts). At 1915 local time GoI accepted the offer of NFIs and associated air transport to Palu. 
HRG/HSOT team arrive in post (22:15 local time)
1 additional Programme Manager deployed ', CAST(N'2018-10-03T10:00:00.000' AS DateTime), 1)
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (4, 1, N'First UK Aid arives', N'Team of four now in Palu. 
30t of UK Aid supplies via IL76 landed at Balikpapan including air cargo handling equipment, shelter kits, solar lanterns and DFID Field Team Life Support.
Having delivered 17.5t of shelter and hygiene kits to BPN on 05/10, RAF A400 aircraft has completed its second journey from Jakarta to BPN with 20t of GoI supplies (06/10). Current plans are for a further A400 shuttle tomorrow with similar cargo (07/10).
120 UK Aid shelter kits (delivered to BPN by RAF A400 on 05/10) arrived in Palu via NZ C-130 (06/10). ', CAST(N'2018-10-06T11:00:00.000' AS DateTime), 3)
GO
SET IDENTITY_INSERT [responsedashboard].[TimeLineItem] OFF
GO
