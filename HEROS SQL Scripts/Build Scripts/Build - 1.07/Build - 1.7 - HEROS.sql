-- File Name:	Build - 1.7 - HEROS.sql
-- Build Key:	Build - 1.7 - 2018.11.29 16.25.56

--USE HermisCloud
GO

-- ==============================================================================================================================
-- Schemas:
--		responsedashboard
--
-- Tables:
--		dropdown.MetricType
--		dropdown.TimeLineItemType
--		responsedashboard.Contact
--		responsedashboard.FAQ
--		responsedashboard.Location
--		responsedashboard.Metric
--		responsedashboard.ResponseDashboard
--		responsedashboard.TimeLineItem
--
-- Functions:
--		core.FormatTime
--		responsedashboard.FormatContactNameByContactID
--
-- Procedures:
--		document.GetDocumentByDocumentID
--		dropdown.GetMetricTypeData
--		dropdown.GetTimeLineItemTypeData
--		responsedashboard.GetDashboardDataByResponseDashboardID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--Begin schema responsedashboard
EXEC utility.AddSchema 'responsedashboard'
GO
--End schema responsedashboard

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.AddColumn @TableName, 'CopyRight', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'IsForCarousel', 'BIT', '0'
GO
--End table document.Document

--Begin table dropdown.MetricType
DECLARE @TableName VARCHAR(250) = 'dropdown.MetricType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MetricType
	(
	MetricTypeID INT IDENTITY(0,1) NOT NULL,
	MetricTypeName VARCHAR(250),
	MetricTypeCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MetricTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_MetricType', 'MetricTypeName,MetricTypeID'
GO
--End table dropdown.MetricType

--Begin table dropdown.TimeLineItemType
DECLARE @TableName VARCHAR(250) = 'dropdown.TimeLineItemType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TimeLineItemType
	(
	TimeLineItemTypeID INT IDENTITY(0,1) NOT NULL,
	TimeLineItemTypeName VARCHAR(250),
	TimeLineItemTypeCode VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TimeLineItemTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_TimeLineItemType', 'TimeLineItemTypeName,TimeLineItemTypeID'
GO
--End table dropdown.TimeLineItemType

--Begin table responsedashboard.Contact
DECLARE @TableName VARCHAR(250) = 'responsedashboard.Contact'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.Contact
	(
	ContactID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	RoleName VARCHAR(50),
	ContactDetails VARCHAR(320),
	OrganizationName VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_Contact', 'ResponseDashboardID'
GO
--End table responsedashboard.Contact

--Begin table responsedashboard.FAQ
DECLARE @TableName VARCHAR(250) = 'responsedashboard.FAQ'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.FAQ
	(
	FAQID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	FAQKey VARCHAR(MAX),
	FAQValue VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FAQID'
EXEC utility.SetIndexClustered @TableName, 'IX_FAQ', 'ResponseDashboardID'
GO
--End table responsedashboard.FAQ

--Begin table responsedashboard.Location
DECLARE @TableName VARCHAR(250) = 'responsedashboard.Location'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.Location
	(
	LocationID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	LocationName VARCHAR(250),
	DeployeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_Location', 'ResponseDashboardID'
GO
--End table responsedashboard.Location

--Begin table responsedashboard.Metric
DECLARE @TableName VARCHAR(250) = 'responsedashboard.Metric'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.Metric
	(
	MetricID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	MetricTypeID INT,
	MetricValue VARCHAR(50),
	MetricDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MetricDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'MetricTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MetricID'
EXEC utility.SetIndexClustered @TableName, 'IX_Metric', 'ResponseDashboardID,MetricTypeID'
GO
--End table responsedashboard.Metric

--Begin table responsedashboard.ResponseDashboard
DECLARE @TableName VARCHAR(250) = 'responsedashboard.ResponseDashboard'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'responsedashboard.Response'

CREATE TABLE responsedashboard.ResponseDashboard
	(
	ResponseDashboardID INT NOT NULL IDENTITY(1,1),
	ResponseName VARCHAR(250),
	NewsfeedURL VARCHAR(2000),
	ProjectID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ResponseDashboardID'
GO
--End table responsedashboard.ResponseDashboard

--Begin table responsedashboard.TimeLineItem
DECLARE @TableName VARCHAR(250) = 'responsedashboard.TimeLineItem'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.TimeLineItem
	(
	TimeLineItemID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	TimeLineItemName VARCHAR(250),
	TimeLineItemDescription VARCHAR(MAX),
	TimeLineItemDateTime DATETIME,
	TimeLineItemTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TimeLineItemDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'TimeLineItemTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TimeLineItemID'
EXEC utility.SetIndexClustered @TableName, 'IX_TimeLineItem', 'ResponseDashboardID,TimeLineItemDateTime'
GO
--End table responsedashboard.TimeLineItem

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(5)

AS
BEGIN

	RETURN ISNULL(CONVERT(CHAR(5), @TimeData, 113), '')

END
GO
--End function core.FormatTime

--Begin procedure responsedashboard.FormatContactNameByContactID
EXEC Utility.DropObject 'responsedashboard.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A function to return the name of a contact in a specified format from a ContactID
-- ==============================================================================================

CREATE FUNCTION responsedashboard.FormatContactNameByContactID
(
@ContactID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(50)
	DECLARE @cLastName VARCHAR(50)
	DECLARE @cOrganizationName VARCHAR(50)
	DECLARE @cRetVal VARCHAR(250) = ''
	DECLARE @cRoleName VARCHAR(50)
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cLastName = ISNULL(C.LastName, ''),
			@cOrganizationName = ISNULL(C.OrganizationName, ''),
			@cRoleName = ISNULL(C.RoleName, '')
		FROM responsedashboard.Contact C
		WHERE C.ContactID = @ContactID
	
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal += @cLastName + ' '
		--ENDIF

		IF LEN(RTRIM(@cLastName)) > 0 AND LEN(RTRIM(@cFirstName)) > 0
			SET @cRetVal = RTRIM(@cRetVal) + ', '
		--ENDIF
			
		IF LEN(RTRIM(@cFirstName)) > 0
			SET @cRetVal += @cFirstName + ' '
		--ENDIF
			
		IF LEN(RTRIM(@cRoleName)) > 0
			SET @cRetVal += '- ' + @cRoleName + ' '
		--ENDIF
	
		IF LEN(RTRIM(@cOrganizationName)) > 0
			SET @cRetVal += '(' + @cOrganizationName + ')'
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO



--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql

--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType,
		D.CopyRight,
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentTitle,
		D.Extension,
		D.IsForCarousel,
		D.IsForResponseDashboard,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure dropdown.GetMetricTypeData
EXEC Utility.DropObject 'dropdown.GetMetricTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A stored procedure to return data from the dropdown.MetricType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetMetricTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MetricTypeID,
		T.MetricTypeCode,
		T.MetricTypeName
	FROM dropdown.MetricType T
	WHERE (T.MetricTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MetricTypeName, T.MetricTypeID

END
GO
--End procedure dropdown.GetMetricTypeData

--Begin procedure dropdown.GetTimeLineItemTypeData
EXEC Utility.DropObject 'dropdown.GetTimeLineItemTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A stored procedure to return data from the dropdown.TimeLineItemType table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetTimeLineItemTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TimeLineItemTypeID,
		T.TimeLineItemTypeCode,
		T.TimeLineItemTypeName,
		T.HexColor
	FROM dropdown.TimeLineItemType T
	WHERE (T.TimeLineItemTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TimeLineItemTypeName, T.TimeLineItemTypeID

END
GO
--End procedure dropdown.GetTimeLineItemTypeData

--Begin procedure responsedashboard.GetDashboardDataByResponseDashboardID
EXEC Utility.DropObject 'responsedashboard.GetDashboardDataByResponseID'
EXEC Utility.DropObject 'responsedashboard.GetDashboardDataByResponseDashboardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A stored procedure to return data for the responsedashboard
-- ========================================================================
CREATE PROCEDURE responsedashboard.GetDashboardDataByResponseDashboardID

@ResponseDashboardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ResponseDashboard
	SELECT
		P.ProjectID,
		P.ProjectName,
		RD.IsActive,
		RD.NewsfeedURL,
		RD.ResponseDashboardID,
		RD.ResponseName
	FROM responsedashboard.ResponseDashboard RD
		JOIN dropdown.Project P ON P.ProjectID = RD.ProjectID
			AND RD.ResponseDashboardID = @ResponseDashboardID

	--ResponseDashboardContact
	SELECT
		C.ContactID,
		responsedashboard.FormatContactNameByContactID(C.ContactID) AS ContactNameFormatted
	FROM responsedashboard.Contact C
	WHERE C.ResponseDashboardID = @ResponseDashboardID
	ORDER BY 2, 1

	--ResponseDashboardDocument
	SELECT
		D.DocumentDescription,
		D.DocumentGUID
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = D.ProjectID
			AND D.IsForResponseDashboard = 1
			AND DT.DocumentTypeCode NOT IN ('Images', 'Media')
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY D.DocumentDate DESC, D.DocumentDescription

	--ResponseDashboardFAQ
	SELECT
		F.FAQID,
		F.FAQKey,
		F.FAQValue
	FROM responsedashboard.FAQ F
	WHERE F.ResponseDashboardID = @ResponseDashboardID
	ORDER BY F.FAQKey, F.FAQID

	--ResponseDashboardImage
	SELECT
		D.CopyRight,
		REPLACE(core.FormatDate(D.DocumentDate), ' ', '') AS DocumentDateFormatted,
		D.DocumentDescription,
		D.DocumentGUID,
		D.IsForCarousel
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = D.ProjectID
			AND D.IsForResponseDashboard = 1
			AND DT.DocumentTypeCode = 'Images'
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY D.DocumentDate DESC, D.DocumentDescription

	--ResponseDashboardIncident
	SELECT
		I.IncidentID,
		I.IncidentName
	FROM core.Incident I
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = I.ProjectID
			AND I.IsForResponseDashboard = 1
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY I.IncidentName, I.IncidentID

	--ResponseDashboardLocation
	SELECT
		L.DeployeeCount,
		L.LocationID,
		L.LocationName
	FROM responsedashboard.Location L
	WHERE L.ResponseDashboardID = @ResponseDashboardID
	ORDER BY L.LocationName, L.LocationID

	--ResponseDashboardMediaRelease
	SELECT
		D.DocumentDescription,
		D.DocumentGUID
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = D.ProjectID
			AND D.IsForResponseDashboard = 1
			AND DT.DocumentTypeCode = 'Media'
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY D.DocumentDate DESC, D.DocumentDescription

	--ResponseDashboardMetric
	SELECT
		M.MetricID,
		core.FormatDate(M.MetricDateTime) AS MetricDateFormatted,
		M.MetricValue,
		MT.MetricTypeName
	FROM responsedashboard.Metric M
		JOIN dropdown.MetricType MT ON MT.MetricTypeID = M.MetricTypeID
			AND M.ResponseDashboardID = @ResponseDashboardID
	ORDER BY M.MetricDateTime DESC, MT.MetricTypeName

	--ResponseDashboardPartners
	SELECT
		F.ForceID,
		F.ForceName
	FROM force.Force F
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = F.ProjectID
			AND F.IsForResponseDashboard = 1
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY F.ForceName, F.ForceID

	--ResponseDashboardReports
	SELECT
		'SpotReport' AS EntityTypeCode,
		SR.SpotReportDate AS EntityDate,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS EntityName
	FROM spotreport.SpotReport SR
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = SR.ProjectID
			AND RD.ResponseDashboardID = @ResponseDashboardID
			AND workflow.GetWorkflowStepNumber('SpotReport', SR.SpotReportID) > workflow.GetWorkflowStepCount('SpotReport', SR.SpotReportID)

	UNION

	SELECT
		'TrendReport' AS EntityTypeCode,
		TR.TrendReportDateTime AS EntityDate,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS EntityName
	FROM trendreport.TrendReport TR
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = TR.ProjectID
			AND RD.ResponseDashboardID = @ResponseDashboardID
			AND workflow.GetWorkflowStepNumber('TrendReport', TR.TrendReportID) > workflow.GetWorkflowStepCount('TrendReport', TR.TrendReportID)

	ORDER BY 2 DESC, 4, 1, 3

	--ResponseDashboardTimeLineItem
	SELECT
		TLI.TimeLineItemID,
		TLI.TimeLineItemName,
		TLI.TimeLineItemDescription,
		core.FormatDate(TLI.TimeLineItemDateTime) AS TimeLineItemDateFormatted,
		core.FormatTime(TLI.TimeLineItemDateTime) AS TimeLineItemTimeFormatted,
		TLIT.TimeLineItemTypeID,
		TLIT.TimeLineItemTypeName
	FROM responsedashboard.TimeLineItem TLI
		JOIN dropdown.TimeLineItemType TLIT ON TLIT.TimeLineItemTypeID = TLI.TimeLineItemTypeID
			AND TLI.ResponseDashboardID = @ResponseDashboardID
	ORDER BY TLI.TimeLineItemDateTime DESC

END
GO
--End procedure responsedashboard.GetDashboardDataByResponseDashboardID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql

--Begin table dropdown.MetricType
TRUNCATE TABLE dropdown.MetricType
GO

EXEC utility.InsertIdentityValue 'dropdown.MetricType', 'MetricTypeID', 0
GO

INSERT INTO dropdown.MetricType
	(MetricTypeCode, MetricTypeName)
VALUES
	('Aid', 'UK aid funds committed'), 
	('Casualty', 'confirmed dead'), 
	('IDP', 'internally displaced people')

GO
--End table dropdown.MetricType

--Begin table dropdown.TimeLineItemType
TRUNCATE TABLE dropdown.TimeLineItemType
GO

EXEC utility.InsertIdentityValue 'dropdown.TimeLineItemType', 'TimeLineItemTypeID', 0
GO

INSERT INTO dropdown.TimeLineItemType
	(TimeLineItemTypeCode, TimeLineItemTypeName, HexColor)
VALUES
	('Activity', 'Activity', '#fff4b2'),
	('Event', 'Event', '#e1d4f7'),
	('Milestone', 'Milestone', '#ddefc9')
GO
--End table dropdown.TimeLineItemType



--End file Build File - 04 - Data.sql

--Begin file Build File 05 RD Staged Data.sql
USE [HermisCloud]
GO
SET IDENTITY_INSERT [responsedashboard].[Contact] ON 
GO
INSERT [responsedashboard].[Contact] ([ContactID], [ResponseDashboardID], [FirstName], [LastName], [RoleName], [ContactDetails], [OrganizationName]) VALUES (1, 1, N'Jack', N'Smith', N'HSOT Response Lead', N'Tel 12345678', N'Palladium')
GO
SET IDENTITY_INSERT [responsedashboard].[Contact] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[FAQ] ON 
GO
INSERT [responsedashboard].[FAQ] ([FAQID], [ResponseDashboardID], [FAQKey], [FAQValue]) VALUES (1, 1, N'Have the UK deployed any SAR specialists to the region', N'2 teams of specialists totalling 18 people were deployed on the 30th October and returned on the 4th November')
GO
INSERT [responsedashboard].[FAQ] ([FAQID], [ResponseDashboardID], [FAQKey], [FAQValue]) VALUES (2, 1, N'Has the UK sent any aid', N'Over 40 tons of aid including tents, water purification tools and shovels have been recieved ')
GO
SET IDENTITY_INSERT [responsedashboard].[FAQ] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[Location] ON 
GO
INSERT [responsedashboard].[Location] ([LocationID], [ResponseDashboardID], [LocationName], [DeployeeCount]) VALUES (1, 1, N'Palu', 10)
GO
INSERT [responsedashboard].[Location] ([LocationID], [ResponseDashboardID], [LocationName], [DeployeeCount]) VALUES (2, 1, N'London', 5)
GO
INSERT [responsedashboard].[Location] ([LocationID], [ResponseDashboardID], [LocationName], [DeployeeCount]) VALUES (3, 1, N'Jakarta', 2)
GO
SET IDENTITY_INSERT [responsedashboard].[Location] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[Metric] ON 
GO
INSERT [responsedashboard].[Metric] ([MetricID], [ResponseDashboardID], [MetricTypeID], [MetricValue], [MetricDateTime]) VALUES (1, 1, 1, N'Â£25M', CAST(N'2018-10-31T12:00:00.000' AS DateTime))
GO
INSERT [responsedashboard].[Metric] ([MetricID], [ResponseDashboardID], [MetricTypeID], [MetricValue], [MetricDateTime]) VALUES (2, 1, 2, N'874', CAST(N'2018-11-01T12:00:00.000' AS DateTime))
GO
INSERT [responsedashboard].[Metric] ([MetricID], [ResponseDashboardID], [MetricTypeID], [MetricValue], [MetricDateTime]) VALUES (3, 1, 3, N'34,208', CAST(N'2018-11-02T13:14:26.000' AS DateTime))
GO
SET IDENTITY_INSERT [responsedashboard].[Metric] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[ResponseDashboard] ON 
GO
INSERT [responsedashboard].[ResponseDashboard] ([ResponseDashboardID], [ResponseName], [NewsfeedURL], [ProjectID], [IsActive]) VALUES (1, N'Indonesia Tsunami 2018', N'https://reliefweb.int/disasters/rss.xml?country=120', 3, 1)
GO
SET IDENTITY_INSERT [responsedashboard].[ResponseDashboard] OFF
GO
SET IDENTITY_INSERT [responsedashboard].[TimeLineItem] ON 
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (1, 1, N'Magnitude 7.5 earthquake', N'Magnitude 7.5 earthquake struck Sulawesi regional of Indonesia at approximately 1002 GMT
Governor of Central Sulawesi called an emergency response for 14 days from 28/09 to 11/10', CAST(N'2018-09-28T10:05:00.000' AS DateTime), 2)
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (2, 1, N'Government of Indonesia welcomed offer of international assistance', NULL, CAST(N'2018-09-29T09:00:00.000' AS DateTime), 2)
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (3, 1, N'Contact with GoI Established', N'SOS has approved funding envelope of Â£2 million for immediate response programming 
Call with HMA Indonesia 
Coordination call with post (DFID, HSOT, MOD)
Indonesian Deputy FM has established contact between DFID Indonesia and the Foreign Ministry, Coordinating Ministry and disaster agency 
DFID Indonesia spoken with ICRC and Save the Children. Waiting for updates from World Vision and CARE. DEC appeal meeting due today
Consider NFI response options 
British Embassy Jakarta shared a Note Verbale with GoI detailing an initial indicative offer of assistance which included several thousand shelter kits (exact number to be confirmed) and air transport (AN12, A400m or C130 aircrafts). At 1915 local time GoI accepted the offer of NFIs and associated air transport to Palu. 
HRG/HSOT team arrive in post (22:15 local time)
1 additional Programme Manager deployed ', CAST(N'2018-10-03T10:00:00.000' AS DateTime), 1)
GO
INSERT [responsedashboard].[TimeLineItem] ([TimeLineItemID], [ResponseDashboardID], [TimeLineItemName], [TimeLineItemDescription], [TimeLineItemDateTime], [TimeLineItemTypeID]) VALUES (4, 1, N'First UK Aid arives', N'Team of four now in Palu. 
30t of UK Aid supplies via IL76 landed at Balikpapan including air cargo handling equipment, shelter kits, solar lanterns and DFID Field Team Life Support.
Having delivered 17.5t of shelter and hygiene kits to BPN on 05/10, RAF A400 aircraft has completed its second journey from Jakarta to BPN with 20t of GoI supplies (06/10). Current plans are for a further A400 shuttle tomorrow with similar cargo (07/10).
120 UK Aid shelter kits (delivered to BPN by RAF A400 on 05/10) arrived in Palu via NZ C-130 (06/10). ', CAST(N'2018-10-06T11:00:00.000' AS DateTime), 3)
GO
SET IDENTITY_INSERT [responsedashboard].[TimeLineItem] OFF
GO

--End file Build File 05 RD Staged Data.sql

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.7 - 2018.11.29 16.25.56')
GO
--End build tracking

