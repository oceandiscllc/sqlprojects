--Begin schema responsedashboard
EXEC utility.AddSchema 'responsedashboard'
GO
--End schema responsedashboard

--Begin table document.Document
DECLARE @TableName VARCHAR(250) = 'document.Document'

EXEC utility.AddColumn @TableName, 'CopyRight', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'IsForCarousel', 'BIT', '0'
GO
--End table document.Document

--Begin table dropdown.MetricType
DECLARE @TableName VARCHAR(250) = 'dropdown.MetricType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MetricType
	(
	MetricTypeID INT IDENTITY(0,1) NOT NULL,
	MetricTypeName VARCHAR(250),
	MetricTypeCode VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MetricTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_MetricType', 'MetricTypeName,MetricTypeID'
GO
--End table dropdown.MetricType

--Begin table dropdown.TimeLineItemType
DECLARE @TableName VARCHAR(250) = 'dropdown.TimeLineItemType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TimeLineItemType
	(
	TimeLineItemTypeID INT IDENTITY(0,1) NOT NULL,
	TimeLineItemTypeName VARCHAR(250),
	TimeLineItemTypeCode VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TimeLineItemTypeID'
EXEC utility.SetIndexClustered @TableName, 'IX_TimeLineItemType', 'TimeLineItemTypeName,TimeLineItemTypeID'
GO
--End table dropdown.TimeLineItemType

--Begin table responsedashboard.Contact
DECLARE @TableName VARCHAR(250) = 'responsedashboard.Contact'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.Contact
	(
	ContactID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	RoleName VARCHAR(50),
	ContactDetails VARCHAR(320),
	OrganizationName VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_Contact', 'ResponseDashboardID'
GO
--End table responsedashboard.Contact

--Begin table responsedashboard.FAQ
DECLARE @TableName VARCHAR(250) = 'responsedashboard.FAQ'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.FAQ
	(
	FAQID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	FAQKey VARCHAR(MAX),
	FAQValue VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FAQID'
EXEC utility.SetIndexClustered @TableName, 'IX_FAQ', 'ResponseDashboardID'
GO
--End table responsedashboard.FAQ

--Begin table responsedashboard.Location
DECLARE @TableName VARCHAR(250) = 'responsedashboard.Location'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.Location
	(
	LocationID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	LocationName VARCHAR(250),
	DeployeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_Location', 'ResponseDashboardID'
GO
--End table responsedashboard.Location

--Begin table responsedashboard.Metric
DECLARE @TableName VARCHAR(250) = 'responsedashboard.Metric'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.Metric
	(
	MetricID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	MetricTypeID INT,
	MetricValue VARCHAR(50),
	MetricDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MetricDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'MetricTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MetricID'
EXEC utility.SetIndexClustered @TableName, 'IX_Metric', 'ResponseDashboardID,MetricTypeID'
GO
--End table responsedashboard.Metric

--Begin table responsedashboard.ResponseDashboard
DECLARE @TableName VARCHAR(250) = 'responsedashboard.ResponseDashboard'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'responsedashboard.Response'

CREATE TABLE responsedashboard.ResponseDashboard
	(
	ResponseDashboardID INT NOT NULL IDENTITY(1,1),
	ResponseName VARCHAR(250),
	NewsfeedURL VARCHAR(2000),
	ProjectID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ResponseDashboardID'
GO
--End table responsedashboard.ResponseDashboard

--Begin table responsedashboard.TimeLineItem
DECLARE @TableName VARCHAR(250) = 'responsedashboard.TimeLineItem'

EXEC utility.DropObject @TableName

CREATE TABLE responsedashboard.TimeLineItem
	(
	TimeLineItemID INT NOT NULL IDENTITY(1,1),
	ResponseDashboardID INT,
	TimeLineItemName VARCHAR(250),
	TimeLineItemDescription VARCHAR(MAX),
	TimeLineItemDateTime DATETIME,
	TimeLineItemTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ResponseDashboardID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TimeLineItemDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'TimeLineItemTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TimeLineItemID'
EXEC utility.SetIndexClustered @TableName, 'IX_TimeLineItem', 'ResponseDashboardID,TimeLineItemDateTime'
GO
--End table responsedashboard.TimeLineItem
