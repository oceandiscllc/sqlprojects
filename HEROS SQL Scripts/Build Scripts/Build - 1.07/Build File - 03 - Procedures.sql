
--Begin procedure document.GetDocumentByDocumentID
EXEC utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType,
		D.CopyRight,
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentTitle,
		D.Extension,
		D.IsForCarousel,
		D.IsForResponseDashboard,
		D.PhysicalFileSize,
		D.ProjectID,
		dropdown.GetProjectNameByProjectID(D.ProjectID) AS ProjectName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
	
	SELECT 
		DE.EntityTypeSubCode,
		DE.EntityID,
		ET.EntityTypeCode,
		ET.EntityTypeName
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND D.DocumentID = @DocumentID
			AND D.ProjectID = @ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = DE.EntityTypeCode
	ORDER BY ET.EntityTypeName, DE.EntityID, DE.DocumentEntityID

END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure dropdown.GetMetricTypeData
EXEC Utility.DropObject 'dropdown.GetMetricTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A stored procedure to return data from the dropdown.MetricType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetMetricTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MetricTypeID,
		T.MetricTypeCode,
		T.MetricTypeName
	FROM dropdown.MetricType T
	WHERE (T.MetricTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MetricTypeName, T.MetricTypeID

END
GO
--End procedure dropdown.GetMetricTypeData

--Begin procedure dropdown.GetTimeLineItemTypeData
EXEC Utility.DropObject 'dropdown.GetTimeLineItemTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A stored procedure to return data from the dropdown.TimeLineItemType table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetTimeLineItemTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TimeLineItemTypeID,
		T.TimeLineItemTypeCode,
		T.TimeLineItemTypeName,
		T.HexColor
	FROM dropdown.TimeLineItemType T
	WHERE (T.TimeLineItemTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TimeLineItemTypeName, T.TimeLineItemTypeID

END
GO
--End procedure dropdown.GetTimeLineItemTypeData

--Begin procedure responsedashboard.GetDashboardDataByResponseDashboardID
EXEC Utility.DropObject 'responsedashboard.GetDashboardDataByResponseID'
EXEC Utility.DropObject 'responsedashboard.GetDashboardDataByResponseDashboardID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.30
-- Description:	A stored procedure to return data for the responsedashboard
-- ========================================================================
CREATE PROCEDURE responsedashboard.GetDashboardDataByResponseDashboardID

@ResponseDashboardID INT

AS
BEGIN
	SET NOCOUNT ON;

	--ResponseDashboard
	SELECT
		P.ProjectID,
		P.ProjectName,
		RD.IsActive,
		RD.NewsfeedURL,
		RD.ResponseDashboardID,
		RD.ResponseName
	FROM responsedashboard.ResponseDashboard RD
		JOIN dropdown.Project P ON P.ProjectID = RD.ProjectID
			AND RD.ResponseDashboardID = @ResponseDashboardID

	--ResponseDashboardContact
	SELECT
		C.ContactID,
		responsedashboard.FormatContactNameByContactID(C.ContactID) AS ContactNameFormatted
	FROM responsedashboard.Contact C
	WHERE C.ResponseDashboardID = @ResponseDashboardID
	ORDER BY 2, 1

	--ResponseDashboardDocument
	SELECT
		D.DocumentDescription,
		D.DocumentGUID
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = D.ProjectID
			AND D.IsForResponseDashboard = 1
			AND DT.DocumentTypeCode NOT IN ('Images', 'Media')
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY D.DocumentDate DESC, D.DocumentDescription

	--ResponseDashboardFAQ
	SELECT
		F.FAQID,
		F.FAQKey,
		F.FAQValue
	FROM responsedashboard.FAQ F
	WHERE F.ResponseDashboardID = @ResponseDashboardID
	ORDER BY F.FAQKey, F.FAQID

	--ResponseDashboardImage
	SELECT
		D.CopyRight,
		REPLACE(core.FormatDate(D.DocumentDate), ' ', '') AS DocumentDateFormatted,
		D.DocumentDescription,
		D.DocumentGUID,
		D.IsForCarousel
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = D.ProjectID
			AND D.IsForResponseDashboard = 1
			AND DT.DocumentTypeCode = 'Images'
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY D.DocumentDate DESC, D.DocumentDescription

	--ResponseDashboardIncident
	SELECT
		I.IncidentID,
		I.IncidentName
	FROM core.Incident I
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = I.ProjectID
			AND I.IsForResponseDashboard = 1
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY I.IncidentName, I.IncidentID

	--ResponseDashboardLocation
	SELECT
		L.DeployeeCount,
		L.LocationID,
		L.LocationName
	FROM responsedashboard.Location L
	WHERE L.ResponseDashboardID = @ResponseDashboardID
	ORDER BY L.LocationName, L.LocationID

	--ResponseDashboardMediaRelease
	SELECT
		D.DocumentDescription,
		D.DocumentGUID
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = D.ProjectID
			AND D.IsForResponseDashboard = 1
			AND DT.DocumentTypeCode = 'Media'
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY D.DocumentDate DESC, D.DocumentDescription

	--ResponseDashboardMetric
	SELECT
		M.MetricID,
		core.FormatDate(M.MetricDateTime) AS MetricDateFormatted,
		M.MetricValue,
		MT.MetricTypeName
	FROM responsedashboard.Metric M
		JOIN dropdown.MetricType MT ON MT.MetricTypeID = M.MetricTypeID
			AND M.ResponseDashboardID = @ResponseDashboardID
	ORDER BY M.MetricDateTime DESC, MT.MetricTypeName

	--ResponseDashboardPartners
	SELECT
		F.ForceID,
		F.ForceName
	FROM force.Force F
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = F.ProjectID
			AND F.IsForResponseDashboard = 1
			AND RD.ResponseDashboardID = @ResponseDashboardID
	ORDER BY F.ForceName, F.ForceID

	--ResponseDashboardReports
	SELECT
		'SpotReport' AS EntityTypeCode,
		SR.SpotReportDate AS EntityDate,
		SR.SpotReportID AS EntityID,
		SR.SpotReportTitle AS EntityName
	FROM spotreport.SpotReport SR
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = SR.ProjectID
			AND RD.ResponseDashboardID = @ResponseDashboardID
			AND workflow.GetWorkflowStepNumber('SpotReport', SR.SpotReportID) > workflow.GetWorkflowStepCount('SpotReport', SR.SpotReportID)

	UNION

	SELECT
		'TrendReport' AS EntityTypeCode,
		TR.TrendReportDateTime AS EntityDate,
		TR.TrendReportID AS EntityID,
		TR.TrendReportTitle AS EntityName
	FROM trendreport.TrendReport TR
		JOIN responsedashboard.ResponseDashboard RD ON RD.ProjectID = TR.ProjectID
			AND RD.ResponseDashboardID = @ResponseDashboardID
			AND workflow.GetWorkflowStepNumber('TrendReport', TR.TrendReportID) > workflow.GetWorkflowStepCount('TrendReport', TR.TrendReportID)

	ORDER BY 2 DESC, 4, 1, 3

	--ResponseDashboardTimeLineItem
	SELECT
		TLI.TimeLineItemID,
		TLI.TimeLineItemName,
		TLI.TimeLineItemDescription,
		core.FormatDate(TLI.TimeLineItemDateTime) AS TimeLineItemDateFormatted,
		core.FormatTime(TLI.TimeLineItemDateTime) AS TimeLineItemTimeFormatted,
		TLIT.TimeLineItemTypeID,
		TLIT.TimeLineItemTypeName
	FROM responsedashboard.TimeLineItem TLI
		JOIN dropdown.TimeLineItemType TLIT ON TLIT.TimeLineItemTypeID = TLI.TimeLineItemTypeID
			AND TLI.ResponseDashboardID = @ResponseDashboardID
	ORDER BY TLI.TimeLineItemDateTime DESC

END
GO
--End procedure responsedashboard.GetDashboardDataByResponseDashboardID
