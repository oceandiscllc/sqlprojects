USE HEROS
GO

--Begin table dropdown.ClientRoster
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientRoster'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientRoster
	(
	ClientRosterID INT IDENTITY(0,1) NOT NULL,
	ClientRosterCode VARCHAR(50),
	ClientRosterName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientRosterID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientRoster', 'DisplayOrder,ClientRosterName'
GO
--End table dropdown.ClientRoster

--Begin table person.PersonClientRoster
DECLARE @TableName VARCHAR(250) = 'person.PersonClientRoster'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonClientRoster
	(
	PersonClientRosterID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ClientRosterID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientRosterID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonClientRosterID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientRoster', 'PersonID,ClientRosterID'
GO
--End table person.PersonClientRoster

--Begin table procurement.EquipmentConsignment
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentConsignment'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentConsignment
	(
	EquipmentConsignmentID INT IDENTITY(1,1) NOT NULL,
	ConsignmentCode VARCHAR(50),
	ConsignmentDate DATE,
	EquipmentOrderID INT,
	EquipmentItemID INT,
	Quantity INT,
	SerialNumber VARCHAR(50),
	ExpirationDate DATE,
	EquipmentConsignmentStatus VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentItemID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentOrderID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentConsignmentID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentConsignment', 'EquipmentOrderID,EquipmentItemID'
GO
--End table procurement.EquipmentConsignment

--Begin table procurement.EquipmentConsignmentDisposition
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentConsignmentDisposition'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentConsignmentDisposition
	(
	EquipmentConsignmentDispositionID INT IDENTITY(1,1) NOT NULL,
	EquipmentConsignmentID INT,
	QuantityShrink INT,
	QuantityWaste INT,
	QuantityRecieved INT,
	Notes VARCHAR(MAX),
	EquipmentConsignmentDispositionDateTime DATETIME,
	RecipientEntityTypeCode VARCHAR(50),
	RecipientEntityID INT,
	StatusID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentConsignmentDispositionDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentConsignmentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantityRecieved', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantityShrink', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantityWaste', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RecipientEntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentConsignmentDispositionID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentConsignmentDisposition', 'EquipmentConsignmentID'
GO
--End table procurement.EquipmentConsignmentDisposition