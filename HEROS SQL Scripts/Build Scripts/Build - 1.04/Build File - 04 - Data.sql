USE HEROS
GO

--Begin table dropdown.ClientRoster
TRUNCATE TABLE dropdown.ClientRoster
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientRoster', 'ClientRosterID', 0
GO

INSERT INTO dropdown.ClientRoster 
	(ClientRosterCode, ClientRosterName) 
VALUES
	('Humanitarian', 'Humanitarian Roster'),
	('SU', 'SU Roster')
GO
--End table dropdown.ClientRoster

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the list of users on the Humanitarian roster', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Person.List.HumanitarianRoster', 
	@PERMISSIONCODE='HumanitarianRoster';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the list of users on the SU roster', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Person.List.SURoster', 
	@PERMISSIONCODE='SURoster';
GO
