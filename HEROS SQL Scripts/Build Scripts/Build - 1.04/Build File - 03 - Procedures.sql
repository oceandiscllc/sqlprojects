USE HEROS
GO

--Begin procedure dropdown.GetClientRosterData
EXEC Utility.DropObject 'dropdown.GetClientRosterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.ClientRoster table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetClientRosterData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientRosterID,
		T.ClientRosterCode,
		T.ClientRosterName
	FROM dropdown.ClientRoster T
	WHERE (T.ClientRosterID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientRosterName, T.ClientRosterID

END
GO
--End procedure dropdown.GetClientRosterData

