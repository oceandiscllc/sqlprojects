-- File Name:	Build - 1.4 - HEROS.sql
-- Build Key:	Build - 1.4 - 2018.07.31 17.12.14

USE HEROS
GO

-- ==============================================================================================================================
-- Tables:
--		dropdown.ClientRoster
--		person.PersonClientRoster
--		procurement.EquipmentConsignment
--		procurement.EquipmentConsignmentDisposition
--
-- Functions:
--		document.FormatFullTextSearchString
--
-- Procedures:
--		dropdown.GetClientRosterData
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE HEROS
GO

--Begin table dropdown.ClientRoster
DECLARE @TableName VARCHAR(250) = 'dropdown.ClientRoster'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ClientRoster
	(
	ClientRosterID INT IDENTITY(0,1) NOT NULL,
	ClientRosterCode VARCHAR(50),
	ClientRosterName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientRosterID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientRoster', 'DisplayOrder,ClientRosterName'
GO
--End table dropdown.ClientRoster

--Begin table person.PersonClientRoster
DECLARE @TableName VARCHAR(250) = 'person.PersonClientRoster'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonClientRoster
	(
	PersonClientRosterID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ClientRosterID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientRosterID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonClientRosterID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientRoster', 'PersonID,ClientRosterID'
GO
--End table person.PersonClientRoster

--Begin table procurement.EquipmentConsignment
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentConsignment'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentConsignment
	(
	EquipmentConsignmentID INT IDENTITY(1,1) NOT NULL,
	ConsignmentCode VARCHAR(50),
	ConsignmentDate DATE,
	EquipmentOrderID INT,
	EquipmentItemID INT,
	Quantity INT,
	SerialNumber VARCHAR(50),
	ExpirationDate DATE,
	EquipmentConsignmentStatus VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentItemID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentOrderID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentConsignmentID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentConsignment', 'EquipmentOrderID,EquipmentItemID'
GO
--End table procurement.EquipmentConsignment

--Begin table procurement.EquipmentConsignmentDisposition
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentConsignmentDisposition'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentConsignmentDisposition
	(
	EquipmentConsignmentDispositionID INT IDENTITY(1,1) NOT NULL,
	EquipmentConsignmentID INT,
	QuantityShrink INT,
	QuantityWaste INT,
	QuantityRecieved INT,
	Notes VARCHAR(MAX),
	EquipmentConsignmentDispositionDateTime DATETIME,
	RecipientEntityTypeCode VARCHAR(50),
	RecipientEntityID INT,
	StatusID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentConsignmentDispositionDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentConsignmentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantityRecieved', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantityShrink', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'QuantityWaste', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RecipientEntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentConsignmentDispositionID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentConsignmentDisposition', 'EquipmentConsignmentID'
GO
--End table procurement.EquipmentConsignmentDisposition
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE HEROS
GO

--Begin function document.FormatFullTextSearchString
EXEC utility.DropObject 'document.FormatFullTextSearchString'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format a string for use in a full text search
-- ========================================================================

CREATE FUNCTION document.FormatFullTextSearchString
(
@SearchString VARCHAR(1000),
@MatchTypeCode VARCHAR(10),
@MatchPartial BIT
)

RETURNS VARCHAR(2000)

AS
BEGIN

	DECLARE @cConjunction VARCHAR(3) = 'AND'
	DECLARE @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ListItem VARCHAR(1000))

	SET @SearchString = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@SearchString, '"', ''), ',', ''), '!', ''), ')', ''), '(', '')))

	IF @MatchTypeCode = 'Exact'
		SET @SearchString = '"' + @SearchString + '"'
	ELSE
		BEGIN

		IF @MatchTypeCode = 'Any'
			SET @cConjunction = 'OR'
		--ENDIF

		IF @MatchPartial = 1
			BEGIN

			INSERT INTO @tTable (ListItem) SELECT LTT.ListItem FROM core.ListToTable(@SearchString, ' ') LTT
			
			SET @SearchString = STUFF((SELECT ' ' + @cConjunction + ' "*' + T.ListItem + '*"' FROM @tTable T FOR XML PATH('')), 1, LEN(@cConjunction) + 2, '')

			END
		ELSE
			BEGIN

			IF CHARINDEX(' ', @SearchString) > 0
				SET @SearchString = REPLACE(@SearchString, ' ', ' ' + @cConjunction + ' ')
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF

	RETURN @SearchString

END
GO
--End function document.FormatFullTextSearchString
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE HEROS
GO

--Begin procedure dropdown.GetClientRosterData
EXEC Utility.DropObject 'dropdown.GetClientRosterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the dropdown.ClientRoster table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetClientRosterData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientRosterID,
		T.ClientRosterCode,
		T.ClientRosterName
	FROM dropdown.ClientRoster T
	WHERE (T.ClientRosterID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientRosterName, T.ClientRosterID

END
GO
--End procedure dropdown.GetClientRosterData


--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE HEROS
GO

--Begin table dropdown.ClientRoster
TRUNCATE TABLE dropdown.ClientRoster
GO

EXEC utility.InsertIdentityValue 'dropdown.ClientRoster', 'ClientRosterID', 0
GO

INSERT INTO dropdown.ClientRoster 
	(ClientRosterCode, ClientRosterName) 
VALUES
	('Humanitarian', 'Humanitarian Roster'),
	('SU', 'SU Roster')
GO
--End table dropdown.ClientRoster

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the list of users on the Humanitarian roster', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Person.List.HumanitarianRoster', 
	@PERMISSIONCODE='HumanitarianRoster';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the list of users on the SU roster', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Person.List.SURoster', 
	@PERMISSIONCODE='SURoster';
GO

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.4 - 2018.07.31 17.12.14')
GO
--End build tracking

