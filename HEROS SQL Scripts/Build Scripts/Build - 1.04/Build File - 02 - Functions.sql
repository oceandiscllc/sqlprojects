USE HEROS
GO

--Begin function document.FormatFullTextSearchString
EXEC utility.DropObject 'document.FormatFullTextSearchString'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format a string for use in a full text search
-- ========================================================================

CREATE FUNCTION document.FormatFullTextSearchString
(
@SearchString VARCHAR(1000),
@MatchTypeCode VARCHAR(10),
@MatchPartial BIT
)

RETURNS VARCHAR(2000)

AS
BEGIN

	DECLARE @cConjunction VARCHAR(3) = 'AND'
	DECLARE @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ListItem VARCHAR(1000))

	SET @SearchString = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@SearchString, '"', ''), ',', ''), '!', ''), ')', ''), '(', '')))

	IF @MatchTypeCode = 'Exact'
		SET @SearchString = '"' + @SearchString + '"'
	ELSE
		BEGIN

		IF @MatchTypeCode = 'Any'
			SET @cConjunction = 'OR'
		--ENDIF

		IF @MatchPartial = 1
			BEGIN

			INSERT INTO @tTable (ListItem) SELECT LTT.ListItem FROM core.ListToTable(@SearchString, ' ') LTT
			
			SET @SearchString = STUFF((SELECT ' ' + @cConjunction + ' "*' + T.ListItem + '*"' FROM @tTable T FOR XML PATH('')), 1, LEN(@cConjunction) + 2, '')

			END
		ELSE
			BEGIN

			IF CHARINDEX(' ', @SearchString) > 0
				SET @SearchString = REPLACE(@SearchString, ' ', ' ' + @cConjunction + ' ')
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF

	RETURN @SearchString

END
GO
--End function document.FormatFullTextSearchString