USE HermisCloud
GO

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.AddColumn @TableName, 'EntityTypeSubCode', 'VARCHAR(50)'
GO
--End table core.EntityType

--Begin table core.PendingEmail
DECLARE @TableName VARCHAR(250) = 'core.PendingEmail'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.AddColumn @TableName, 'DocumentIDList', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'SendDate', 'DATE'
EXEC utility.AddColumn @TableName, 'SentDateTime', 'DATETIME'
GO
--End table core.PendingEmail

--Begin table dropdown.AgeRange
DECLARE @TableName VARCHAR(250) = 'dropdown.AgeRange'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AgeRange
	(
	AgeRangeID INT IDENTITY(0,1) NOT NULL,
	AgeRangeCode VARCHAR(50),
	AgeRangeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AgeRangeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AgeRange', 'DisplayOrder,AgeRangeName', 'AgeRangeID'
GO
--End table dropdown.AgeRange

--Begin table dropdown.ApplicantSource
DECLARE @TableName VARCHAR(250) = 'dropdown.ApplicantSource'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ApplicantSource
	(
	ApplicantSourceID INT IDENTITY(0,1) NOT NULL,
	ApplicantSourceCode VARCHAR(50),
	ApplicantSourceName VARCHAR(250),
	HasOther BIT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasOther', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicantSourceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ApplicantSource', 'DisplayOrder,ApplicantSourceName', 'ApplicantSourceID'
GO
--End table dropdown.ApplicantSource

--Begin table dropdown.Competency
DECLARE @TableName VARCHAR(250) = 'dropdown.Competency'

EXEC utility.DropObject @TableName
GO
--End table dropdown.Competency

--Begin table dropdown.CompetencyType
DECLARE @TableName VARCHAR(250) = 'dropdown.CompetencyType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CompetencyType
	(
	CompetencyTypeID INT IDENTITY(0,1) NOT NULL,
	CompetencyTypeCode VARCHAR(50),
	CompetencyTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CompetencyTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_CompetencyType', 'DisplayOrder,CompetencyTypeName', 'CompetencyTypeID'
GO
--End table dropdown.CompetencyType

--Begin table dropdown.CSGMemberPerformanceRating
DECLARE @TableName VARCHAR(250) = 'dropdown.CSGMemberPerformanceRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CSGMemberPerformanceRating
	(
	CSGMemberPerformanceRatingID INT IDENTITY(0,1) NOT NULL,
	CSGMemberPerformanceRatingCode VARCHAR(50),
	CSGMemberPerformanceRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CSGMemberPerformanceRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_CSGMemberPerformanceRating', 'DisplayOrder,CSGMemberPerformanceRatingName', 'CSGMemberPerformanceRatingID'
GO
--End table dropdown.CSGMemberPerformanceRating

--Begin table dropdown.Ethnicity
DECLARE @TableName VARCHAR(250) = 'dropdown.Ethnicity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Ethnicity
	(
	EthnicityID INT IDENTITY(0,1) NOT NULL,
	EthnicityCategoryCode VARCHAR(50),
	EthnicityCategoryName VARCHAR(250),
	EthnicityCode VARCHAR(50),
	EthnicityName VARCHAR(250),
	HasOther BIT,
	EthnicityCategorDisplayOrder INT,
	EthnicityDisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EthnicityCategorDisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EthnicityDisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasOther', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EthnicityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Ethnicity', 'EthnicityCategorDisplayOrder,EthnicityDisplayOrder,EthnicityName', 'EthnicityID'
GO
--End table dropdown.Ethnicity

--Begin table dropdown.FeedbackQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.FeedbackQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FeedbackQuestion
	(
	FeedbackQuestionID INT IDENTITY(0,1) NOT NULL,
	FeedbackQuestionCode VARCHAR(50),
	FeedbackQuestionName VARCHAR(250),
	DisplayOrder INT,
	IsExternal BIT,
	IsInternal BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsExternal', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInternal', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'FeedbackQuestionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FeedbackQuestion', 'DisplayOrder,FeedbackQuestionName', 'FeedbackQuestionID'
GO
--End table dropdown.FeedbackQuestion

--Begin table dropdown.InterviewOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.InterviewOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InterviewOutcome
	(
	InterviewOutcomeID INT IDENTITY(0,1) NOT NULL,
	InterviewOutcomeCode VARCHAR(50),
	InterviewOutcomeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'InterviewOutcomeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InterviewOutcome', 'DisplayOrder,InterviewOutcomeName', 'InterviewOutcomeID'
GO
--End table dropdown.InterviewOutcome

--Begin table dropdown.PersonType
DECLARE @TableName VARCHAR(250) = 'dropdown.PersonType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PersonType
	(
	PersonTypeID INT IDENTITY(0,1) NOT NULL,
	PersonTypeCode VARCHAR(50),
	PersonTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PersonType', 'DisplayOrder,PersonTypeName', 'PersonTypeID'
GO
--End table dropdown.PersonType

--Begin table dropdown.PersonUpdateType
DECLARE @TableName VARCHAR(250) = 'dropdown.PersonUpdateType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PersonUpdateType
	(
	PersonUpdateTypeID INT IDENTITY(0,1) NOT NULL,
	PersonUpdateTypeCode VARCHAR(50),
	PersonUpdateTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdateTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PersonUpdateType', 'DisplayOrder,PersonUpdateTypeName', 'PersonUpdateTypeID'
GO
--End table dropdown.PersonUpdateType

--Begin table dropdown.SuitabilityRating
DECLARE @TableName VARCHAR(250) = 'dropdown.SuitabilityRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SuitabilityRating
	(
	SuitabilityRatingID INT IDENTITY(0,1) NOT NULL,
	SuitabilityRatingCode VARCHAR(50),
	SuitabilityRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SuitabilityRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SuitabilityRating', 'DisplayOrder,SuitabilityRatingName', 'SuitabilityRatingID'
GO
--End table dropdown.SuitabilityRating

--Begin table dropdown.SUPerformanceRating
DECLARE @TableName VARCHAR(250) = 'dropdown.SUPerformanceRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SUPerformanceRating
	(
	SUPerformanceRatingID INT IDENTITY(0,1) NOT NULL,
	SUPerformanceRatingCode VARCHAR(50),
	SUPerformanceRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SUPerformanceRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SUPerformanceRating', 'DisplayOrder,SUPerformanceRatingName', 'SUPerformanceRatingID'
GO
--End table dropdown.SUPerformanceRating

--Begin table dropdown.VacancyStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.VacancyStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VacancyStatus
	(
	VacancyStatusID INT IDENTITY(0,1) NOT NULL,
	VacancyStatusCode VARCHAR(50),
	VacancyStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VacancyStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VacancyStatus', 'DisplayOrder,VacancyStatusName', 'VacancyStatusID'
GO
--End table dropdown.VacancyStatus

--Begin table dropdown.VacancyType
DECLARE @TableName VARCHAR(250) = 'dropdown.VacancyType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VacancyType
	(
	VacancyTypeID INT IDENTITY(0,1) NOT NULL,
	VacancyTypeCode VARCHAR(50),
	VacancyTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VacancyTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VacancyType', 'DisplayOrder,VacancyTypeName', 'VacancyTypeID'
GO
--End table dropdown.VacancyType

--Begin table hrms.Application
DECLARE @TableName VARCHAR(250) = 'hrms.Application'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.Application
	(
	ApplicationID INT IDENTITY(1,1) NOT NULL,
	AgeRangeID INT,
	ConflictsOfInterest VARCHAR(MAX),
	ExpertCategoryID INT,
	GenderID INT,
	HasCertifiedApplication BIT,
	HasConfirmedTerminationDate BIT,
	HasDAAccount BIT,
	InterviewOutcomeID INT,
	IsGuaranteedInterview BIT,
	IsApplicationFinal BIT,
	IsNotCriminalCivilService BIT,
	IsNotCriminalPolice BIT,
	IsDisabled BIT,
	IsDueDilligenceComplete BIT,
	IsEligible BIT,
	IsGovernmetEmployee BIT,
	IsHermisProfileUpdated BIT,
	IsHRContactNotified BIT,
	IsInterviewAvailableAMDay1 BIT,
	IsInterviewAvailableAMDay2 BIT,
	IsInterviewAvailableAMDay3 BIT,
	IsInterviewAvailableAMDay4 BIT,
	IsInterviewAvailableAMDay5 BIT,
	IsInterviewAvailableAMDay6 BIT,
	IsInterviewAvailableAMDay7 BIT,
	IsInterviewAvailablePMDay1 BIT,
	IsInterviewAvailablePMDay2 BIT,
	IsInterviewAvailablePMDay3 BIT,
	IsInterviewAvailablePMDay4 BIT,
	IsInterviewAvailablePMDay5 BIT,
	IsInterviewAvailablePMDay6 BIT,
	IsInterviewAvailablePMDay7 BIT,
	IsInterviewComplete BIT,
	IsLineManagerNotified BIT,
	IsOfferAccepted BIT,
	IsOfferExtended BIT,
	IsOnRoster BIT,
	IsPreQualified BIT,
	IsVetted BIT,
	ManagerComments VARCHAR(MAX),
	PersonID INT,
	Suitability VARCHAR(2500),
	SubmittedDateTime DATETIME,
	TechnicalManagerComments VARCHAR(MAX),
	TerminationDate DATE,
	VacancyID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AgeRangeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpertCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GenderID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasCertifiedApplication', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasConfirmedTerminationDate', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasDAAccount', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InterviewOutcomeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsApplicationFinal', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsDisabled', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsGovernmetEmployee', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsGuaranteedInterview', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsHermisProfileUpdated', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsHRContactNotified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay1', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay2', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay3', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay4', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay5', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay6', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay7', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay1', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay2', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay3', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay4', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay5', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay6', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay7', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsLineManagerNotified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNotCriminalCivilService', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNotCriminalPolice', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsOfferAccepted', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsOfferExtended', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsOnRoster', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsPreQualified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsVetted', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicationID'
GO
--End table hrms.Application

--Begin table hrms.ApplicationCompetency
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationCompetency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationCompetency
	(
	ApplicationCompetencyID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	CompetencyID INT,
	Suitability VARCHAR(1250),
	SuitabilityRatingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CompetencyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SuitabilityRatingID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationCompetencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationCompetency', 'ApplicationID,CompetencyID'
GO
--End table hrms.ApplicationCompetency

--Begin table hrms.ApplicationEthnicity
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationEthnicity'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationEthnicity
	(
	ApplicationEthnicityID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	EthnicityID INT,
	EthnicityDescription VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'EthnicityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationEthnicityID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationEthnicity', 'ApplicationID,EthnicityID'
GO
--End table hrms.ApplicationEthnicity

--Begin table hrms.ApplicationInterviewAvailability
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationInterviewAvailability'

EXEC utility.DropObject 'hrms.ApplicationInterviewConfirmation'
EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationInterviewAvailability
	(
	ApplicationInterviewAvailabilityID INT IDENTITY(1,1) NOT NULL,
	ApplicationPersonID INT,
	ApplicationInterviewScheduleID INT,
	IsAvailable BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationInterviewScheduleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAvailable', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationInterviewAvailabilityID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationInterviewAvailability', 'ApplicationPersonID,ApplicationInterviewScheduleID'
GO
--End table hrms.ApplicationInterviewAvailability

--Begin table hrms.ApplicationInterviewSchedule
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationInterviewSchedule'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationInterviewSchedule
	(
	ApplicationInterviewScheduleID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	InterviewDateTime DATETIME,
	IsSelectedDate BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSelectedDate', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicationInterviewScheduleID'
GO
--End table hrms.ApplicationInterviewSchedule

--Begin table hrms.ApplicationPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationPerson
	(
	ApplicationPersonID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	PersonID INT,
	InterviewOutcomeID INT,
	PersonTypeCode VARCHAR(50),
	IsScheduleConfirmed BIT,
	ProposesInterviewDates BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InterviewOutcomeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsScheduleConfirmed', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProposesInterviewDates', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationPerson', 'ApplicationID,PersonID'
GO
--End table hrms.ApplicationPerson

--Begin table hrms.Competency
DECLARE @TableName VARCHAR(250) = 'hrms.Competency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.Competency
	(
	CompetencyID INT IDENTITY(1,1) NOT NULL,
	CompetencyName VARCHAR(250),
	CompetencyTypeID INT,
	CompetencyDescription VARCHAR(MAX),
	IsActive INT
	)

EXEC utility.SetDefaultConstraint 'hrms.Competency', 'CompetencyTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'hrms.Competency', 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered 'hrms.Competency', 'CompetencyID'
GO
--End table hrms.Competency

--Begin table hrms.Vacancy
DECLARE @TableName VARCHAR(250) = 'hrms.Vacancy'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.Vacancy
	(
	VacancyID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	VacancyName VARCHAR(250),
	VacancyStatusID INT,
	VacancyTypeID INT,
	VacancyReference VARCHAR(MAX),
	ExpertCategoryID INT,
	OpenDateTime DATETIME,
	CloseDateTime DATETIME,
	CoordinatorPersonID INT,
	RoleDescription VARCHAR(MAX),
	VacancyNotes VARCHAR(MAX),
	VacancyContactName VARCHAR(250),
	VacancyContactEmailAddress VARCHAR(320),
	VacancyContactPhone VARCHAR(50),
	CreateDateTime DATETIME,
	CreatePersonID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CoordinatorPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpertCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VacancyID'
GO
--End table hrms.Vacancy

--Begin table hrms.VacancyCompetency
DECLARE @TableName VARCHAR(250) = 'hrms.VacancyCompetency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.VacancyCompetency
	(
	VacancyCompetencyID INT IDENTITY(1,1) NOT NULL,
	VacancyID INT,
	CompetencyID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CompetencyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VacancyCompetencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_VacancyCompetency', 'VacancyID,CompetencyID'
GO
--End table hrms.VacancyCompetency

--Begin table hrms.VacancyPerson
DECLARE @TableName VARCHAR(250) = 'hrms.VacancyPerson'

EXEC utility.DropObject @TableName
GO
--End table hrms.VacancyPerson

--Begin table person.Feedback
DECLARE @TableName VARCHAR(250) = 'person.Feedback'

EXEC utility.DropObject @TableName

CREATE TABLE person.Feedback
	(
	FeedbackID INT IDENTITY(1,1) NOT NULL,
	FeedbackName VARCHAR(500),
	TaskID INT,
	ProjectID INT,
	StartDate DATE,
	EndDate DATE,
	CreatePersonID INT,
	CreateDate DATE,
	SUPerformanceRatingID1 INT,
	SUPerformanceRatingID2 INT,
	SUPerformanceRatingID3 INT,
	Question4 VARCHAR(MAX),
	Question5 VARCHAR(MAX),
	CSGMemberPersonID INT,
	CSGMemberPerformanceRatingID INT,
	Question7 VARCHAR(MAX),
	CSGMemberComment VARCHAR(MAX),
	IsExternal BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CSGMemberPerformanceRatingID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CSGMemberPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'IsExternal', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SUPerformanceRatingID1', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SUPerformanceRatingID2', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SUPerformanceRatingID3', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaskID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'FeedbackID'
GO
--End table person.Feedback

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropColumn @TableName, 'GenderCode'
EXEC utility.DropColumn @TableName, 'HasConfirmedTerminationDate'
EXEC utility.DropColumn @TableName, 'IsGovernmetEmployee'
EXEC utility.DropColumn @TableName, 'SponsorName'
EXEC utility.DropColumn @TableName, 'TerminationDate'

IF utility.HasColumn(@TableName, 'PoliceRankID') = 1
	EXEC sp_RENAME 'person.Person.PoliceRankID', 'PoliceRankID1', 'COLUMN'
--ENDIF	

EXEC utility.AddColumn @TableName, 'ApplicantSourceOther', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'ApplicantTypeCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'CivilServiceDepartment', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'CivilServiceGrade1', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'CivilServiceGrade2', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'DAAccountDecomissionedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'DaysToRemoval', 'INT', '0'
EXEC utility.AddColumn @TableName, 'DeleteByPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'DeleteDate', 'DATE'
EXEC utility.AddColumn @TableName, 'FARemoveByPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'FARemoveDate', 'DATE'
EXEC utility.AddColumn @TableName, 'HRContactCountryCallingCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'HRContactEmailAddress', 'VARCHAR(320)'
EXEC utility.AddColumn @TableName, 'HRContactName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'HRContactPhone', 'VARCHAR(25)'
EXEC utility.AddColumn @TableName, 'ISDAAccountDecomissioned', 'BIT', '0'
EXEC utility.AddColumn @TableName, 'IsPreviousApplicant', 'BIT', '0' 
EXEC utility.AddColumn @TableName, 'IsUKResidencyEligible', 'BIT', '0'
EXEC utility.AddColumn @TableName, 'LastContractEndDate', 'DATE'
EXEC utility.AddColumn @TableName, 'LineManagerCountryCallingCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'LineManagerEmailAddress', 'VARCHAR(320)'
EXEC utility.AddColumn @TableName, 'LineManagerName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'LineManagerPhone', 'VARCHAR(25)'
EXEC utility.AddColumn @TableName, 'LockOutByPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'LockOutDate', 'DATE'
EXEC utility.AddColumn @TableName, 'OtherGrade', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'PersonTypeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PoliceRankID2', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PreviousApplicationDate', 'DATE'
EXEC utility.AddColumn @TableName, 'SecurityClearanceID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'SecurityClearanceSponsorName', 'VARCHAR(250)'
GO
--End table person.Person

--Begin table person.PersonApplicantSource
DECLARE @TableName VARCHAR(250) = 'person.PersonApplicantSource'

EXEC utility.DropObject 'hrms.ApplicationApplicantSource'
EXEC utility.DropObject @TableName

CREATE TABLE person.PersonApplicantSource
	(
	PersonApplicantSourceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ApplicantSourceID INT,
	ApplicantSourceDescription VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicantSourceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonApplicantSourceID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonApplicantSource', 'PersonID,ApplicantSourceID'
GO
--End table person.PersonApplicantSource

--Begin table person.PersonClientRoster
EXEC utility.DropObject 'person.TR_PersonClientRoster'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.09
-- Description:	A trigger to insert data into the core.PendingEmail table
-- ======================================================================
CREATE TRIGGER person.TR_PersonClientRoster ON person.PersonClientRoster AFTER INSERT, UPDATE
AS
	SET ARITHABORT ON;

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.SentDateTime IS NULL
		AND PE.EmailTemplateCode IN ('SURosterTerminationFinal', 'SURosterTerminationInitial')
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonClientRoster PCR
				JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
				JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
					AND CR.ClientRosterCode = 'SU'
					AND ES.ExpertStatusCode = 'Removed'
					AND PE.PersonID = PCR.PersonID
			)

	INSERT INTO core.PendingEmail
		(EntityTypeCode, EmailTemplateCode, EntityID, SendDate, PersonID)
	SELECT
		'PersonClientRoster',
		'SURosterTerminationInitial',
		PCR.PersonClientRosterID,
		getDate(),
		PCR.PersonID
	FROM person.PersonClientRoster PCR
		JOIN INSERTED I ON I.PersonClientRosterID = PCR.PersonClientRosterID
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = I.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = I.ExpertStatusID
			AND CR.ClientRosterCode = 'SU'
			AND ES.ExpertStatusCode = 'Removed'
			AND I.TerminationDate IS NOT NULL
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.PendingEmail PE
				WHERE PE.EntityTypeCode = 'Person'
					AND PE.EmailTemplateCode = 'SURosterTerminationInitial'
					AND PE.EntityID = PCR.PersonID
				)

	UNION

	SELECT
		'PersonClientRoster',
		'SURosterTerminationFinal',
		PCR.PersonClientRosterID,
		DATEADD(m, 11, PCR.TerminationDate),
		PCR.PersonID
	FROM person.PersonClientRoster PCR
		JOIN INSERTED I ON I.PersonClientRosterID = PCR.PersonClientRosterID
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = I.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = I.ExpertStatusID
			AND CR.ClientRosterCode = 'SU'
			AND ES.ExpertStatusCode = 'Removed'
			AND I.TerminationDate IS NOT NULL
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.PendingEmail PE
				WHERE PE.EntityTypeCode = 'Person'
					AND PE.EmailTemplateCode = 'SURosterTerminationFinal'
					AND PE.EntityID = PCR.PersonID
				)

	UPDATE PE
	SET PE.SendDate = DATEADD(m, 11, PCR.TerminationDate)
	FROM core.PendingEmail PE
		JOIN person.PersonClientRoster PCR ON PCR.PersonClientRosterID = PE.EntityID
		JOIN INSERTED I ON I.PersonClientRosterID = PCR.PersonClientRosterID
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = I.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = I.ExpertStatusID
			AND CR.ClientRosterCode = 'SU'
			AND ES.ExpertStatusCode = 'Removed'
			AND I.TerminationDate IS NOT NULL
			AND PE.SentDateTime IS NULL
			AND PE.EmailTemplateCode = 'SURosterTerminationFinal'

GO

ALTER TABLE person.PersonClientRoster ENABLE TRIGGER TR_PersonClientRoster
GO
--End table person.PersonClientRoster

--Begin table person.PersonCountry
DECLARE @TableName VARCHAR(250) = 'person.PersonCountry'

EXEC utility.DropObject 'hrms.ApplicationCountry'
EXEC utility.DropObject @TableName

CREATE TABLE person.PersonCountry
	(
	PersonCountryID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	CountryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonCountryID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonCountry', 'PersonID,CountryID'
GO
--End table person.PersonCountry

--Begin table person.PersonExpertStatus
EXEC Utility.DropObject 'person.PersonExpertStatus'
GO
--End table person.PersonExpertStatus

--Begin table person.PersonOperationalTraining
EXEC utility.DropObject 'person.TR_PersonOperationalTraining'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.09
-- Description:	A trigger to manage data in the core.PendingEmail table
-- ====================================================================
CREATE TRIGGER person.TR_PersonOperationalTraining ON person.PersonOperationalTraining AFTER DELETE, INSERT, UPDATE
AS
	SET ARITHABORT ON;
	
	DECLARE @nPersonOperationalTrainingID INT

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT I.PersonOperationalTrainingID
		FROM INSERTED I
		ORDER BY I.PersonOperationalTrainingID

	OPEN oCursor
	FETCH oCursor INTO @nPersonOperationalTrainingID
	WHILE @@fetch_status = 0
		BEGIN

		EXEC core.PendingEmailAddUpdate 'PersonOperationalTraining', 'OperationalTrainingExpiration', @nPersonOperationalTrainingID, 'OperationalTrainingExpirationEmailDayCount'
		
		FETCH oCursor INTO @nPersonOperationalTrainingID
	
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor	

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.SentDateTime IS NULL
		AND PE.EmailTemplateCode = 'OperationalTrainingExpiration'
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonOperationalTraining POT
			WHERE POT.PersonOperationalTrainingID = PE.EntityID
			)

GO

ALTER TABLE person.PersonOperationalTraining ENABLE TRIGGER TR_PersonOperationalTraining
GO
--End table person.PersonOperationalTraining

--Begin table person.PersonSecurityClearance
EXEC utility.DropObject 'person.TR_PersonSecurityClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonSecurityClearance table
--
-- Author:			Justin Branum
-- Create Date: 2019.01.24
-- Description: Trigger needs to set the other medical clearances to inactive ONLY if that record itself is set to Active
--
-- Author:			Todd Pires
-- Create date:	2018.02.09
-- Description:	Added code to manage data in the core.PendingEmail table
-- ======================================================================================================================
CREATE TRIGGER person.TR_PersonSecurityClearance ON person.PersonSecurityClearance AFTER DELETE, INSERT, UPDATE
AS
	SET ARITHABORT ON;
	
	DECLARE @nPersonSecurityClearanceID INT

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT I.PersonSecurityClearanceID
		FROM INSERTED I
		ORDER BY I.PersonSecurityClearanceID

	OPEN oCursor
	FETCH oCursor INTO @nPersonSecurityClearanceID
	WHILE @@fetch_status = 0
		BEGIN

		EXEC core.PendingEmailAddUpdate 'PersonSecurityClearance', 'SecurityClearanceExpiration', @nPersonSecurityClearanceID, 'SecurityClearanceExpirationEmailDayCount'
		
		FETCH oCursor INTO @nPersonSecurityClearanceID
	
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor	

	UPDATE PSC
	SET PSC.IsActive = 0
	FROM person.PersonSecurityClearance PSC
		JOIN INSERTED I ON I.PersonID = PSC.PersonID
			AND I.PersonSecurityClearanceID <> PSC.PersonSecurityClearanceID
			AND I.IsActive = 1

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.SentDateTime IS NULL
		AND PE.EmailTemplateCode = 'SecurityClearanceExpiration'
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonSecurityClearance PSC
			WHERE PSC.PersonSecurityClearanceID = PE.EntityID
			)

GO

ALTER TABLE person.PersonSecurityClearance ENABLE TRIGGER TR_PersonSecurityClearance
GO
--End table person.PersonSecurityClearance

--Begin table personupdate.Person
DECLARE @TableName VARCHAR(250) = 'personupdate.Person'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.Person
	(
	PersonUpdatePersonID INT IDENTITY(1,1) NOT NULL,
	PersonUpdateID INT,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	ManagerPersonID INT,
	DefaultProjectID INT,
	Organization VARCHAR(250),
	PersonID INT,
	RoleID INT,
	InvoiceLimit NUMERIC(18, 2),
	PurchaseOrderLimit NUMERIC(18, 2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DefaultProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceLimit', 'NUMERIC(18, 2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ManagerPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PurchaseOrderLimit', 'NUMERIC(18, 2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonID'
GO
--End table personupdate.Person

--Begin table personupdate.PersonGroupPerson
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonGroupPerson'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonGroupPerson
	(
	PersonUpdatePersonGroupPersonID INT IDENTITY(1,1) NOT NULL,
	PersonGroupID INT,
	PersonID INT,
	PersonUpdateID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonUpdateID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonGroupPersonID'
GO
--End table personupdate.PersonGroupPerson

--Begin table personupdate.PersonPermissionable
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonPermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonPermissionable
	(
	PersonUpdatePersonPermissionableID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PermissionableLineage VARCHAR(250),
	PersonUpdateID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonUpdateID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonPermissionableID'
GO
--End table personupdate.PersonPermissionable

--Begin table personupdate.PersonProject
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonProject'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonProject
	(
	PersonUpdatePersonProjectID INT IDENTITY(1,1) NOT NULL,
	PersonUpdateID INT,
	PersonID INT,
	ProjectID INT,
	IsViewDefault BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsViewDefault', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonProjectID'
GO
--End table personupdate.PersonProject

--Begin table personupdate.PersonUpdate
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonUpdate
	(
	PersonUpdateID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	UserName VARCHAR(250),
	PersonUpdateTypeID INT,
	RequestDate DATE,
	RequestedByPersonID INT,
	ActionDate DATE,
	WorkflowStepNumber INT,
	Notes VARCHAR(MAX),
	ISDAAccountDecomissioned BIT,
	DAAccountDecomissionedDate DATE,
	LockOutByPersonID INT,
	LockOutDate DATE,
	FARemoveByPersonID INT,
	FARemoveDate DATE,
	DeleteByPersonID INT,
	DeleteDate DATE,
	DaysToRemoval INT
	)

EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'DaysToRemoval', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'DeleteByPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'FARemoveByPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'ISDAAccountDecomissioned', 'BIT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'LockOutByPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'RequestDate', 'DATE', 'getdate()'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'WorkflowStepNumber', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered 'personupdate.PersonUpdate', 'PersonUpdateID'
GO
--End table personupdate.PersonUpdate

--Begin table reporting.SearchSetup
DECLARE @TableName VARCHAR(250) = 'reporting.SearchSetup'

EXEC utility.DropObject @TableName

CREATE TABLE reporting.SearchSetup
	(
	SearchSetupID BIGINT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	PersonID INT,
	ColumnCode VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SearchSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SearchSetup', 'EntityTypeCode,PersonID'
GO
--End table reporting.SearchSetup

--Begin table workflow.EntityWorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowStepGroupPerson'

EXEC utility.DropColumn 'workflow.EntityWorkflowStepGroupPerson', 'WorkflowStepStatusName'		
EXEC utility.DropColumn 'workflow.EntityWorkflowStepGroupPersonGroup', 'WorkflowStepStatusName'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC utility.AddColumn @TableName, 'EntityTypeSubCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'WorkflowCompleteStatusName', 'VARCHAR(50)'
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC utility.AddColumn @TableName, 'WorkflowStepStatusName', 'VARCHAR(50)'
GO
--End table workflow.WorkflowStep

ALTER TABLE hrms.Application ALTER COLUMN Suitability VARCHAR(MAX)
ALTER TABLE person.Person ALTER COLUMN SummaryBiography VARCHAR(MAX)
ALTER TABLE hrms.ApplicationCompetency ALTER COLUMN Suitability VARCHAR(MAX)
GO
