USE HermisCloud
GO

--Begin procedure core.DeletePendingEmailByPendingEmailID
EXEC utility.DropObject 'core.DeletePendingEmailByPendingEmailID'
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure delete data from the core.PendingEmail table
-- ============================================================================
CREATE PROCEDURE core.DeletePendingEmailByPendingEmailID

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.PendingEmailID = @PendingEmailID

END
GO
--End procedure core.DeletePendingEmailByPendingEmailID

--Begin procedure core.GetEmailTemplateData
EXEC utility.DropObject 'core.GetEmailTemplateData'
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateData

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50) = NULL,
@WorkflowActionCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	--EmailTemplateData
	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND (@EmailTemplateCode IS NULL OR ET.EmailTemplateCode = @EmailTemplateCode)
		AND (@WorkflowActionCode IS NULL OR ET.WorkflowActionCode = @WorkflowActionCode)

	--EmailTemplateFieldData
	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

	--EmailTemplateGlobalData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS SiteURL,
		core.GetSystemSetupValueBySystemSetupKey('SystemName', '') AS SystemName

END
GO
--End procedure core.GetEmailTemplateData

--Begin procedure core.GetPendingEmail
EXEC utility.DropObject 'core.GetPendingEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure get data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		core.GetSystemSetupValueBySystemSetupKey('FeedBackMailTo', '') AS FeedBackEmailAddress,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReplyEmailAddress,
		PE.EmailTemplateCode
	FROM core.PendingEmail PE
	WHERE PE.PendingEmailID = @PendingEmailID
		OR 
			(
			@PendingEmailID = 0
				AND PE.SentDateTime IS NULL
				AND PE.SendDate <= getDate()
			) 
	ORDER BY PE.EmailTemplateCode

END
GO
--End procedure core.GetPendingEmail

--Begin procedure core.GetPendingOperationalTrainingExpirationEmail
EXEC utility.DropObject 'core.GetPendingOperationalTrainingExpirationEmail'
EXEC utility.DropObject 'core.GetPendingPersonOperationalTrainingExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingOperationalTrainingExpirationEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		OTC.OperationalTrainingCourseName,
		P.EmailAddress,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM person.PersonOperationalTraining POT
		JOIN person.Person P ON P.PersonID = POT.PersonID
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonOperationalTraining'
			AND PE.EmailTemplateCode = 'OperationalTrainingExpiration'
			AND PE.EntityID = POT.PersonOperationalTrainingID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingOperationalTrainingExpirationEmail

--Begin procedure core.GetPendingProfileReviewEmail
EXEC utility.DropObject 'core.GetPendingProfileReviewEmail'
EXEC utility.DropObject 'core.GetPendingPersonProfileReviewEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingProfileReviewEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted,
		'<a href="' + DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') + '">DeployAdviser</a>' AS DeployAdviserLink,
		DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS DeployAdviserURL
	FROM person.Person P
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'ProfileReview'
			AND PE.EntityID = P.PersonID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingProfileReviewEmail

--Begin procedure core.GetPendingSecurityClearanceExpirationEmail
EXEC utility.DropObject 'core.GetPendingSecurityClearanceExpirationEmail'
EXEC utility.DropObject 'core.GetPendingPersonSecurityClearanceExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingSecurityClearanceExpirationEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		core.FormatDate(PSC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		SC.SecurityClearanceName,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted
	FROM person.PersonSecurityClearance PSC
		JOIN person.Person P ON P.PersonID = PSC.PersonID
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonSecurityClearance'
			AND PE.EmailTemplateCode = 'SecurityClearanceExpiration'
			AND PE.EntityID = PSC.PersonSecurityClearanceID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingSecurityClearanceExpirationEmail

--Begin procedure core.GetPendingSURosterTerminationFinalEmail
EXEC utility.DropObject 'core.GetPendingSURosterTerminationFinalEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.09
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingSURosterTerminationFinalEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		core.FormatDate(DATEADD(m, 12, PCR.TerminationDate)) AS DeletionDateFormatted,
		core.GetSystemSetupValueBySystemSetupKey('SURosterTerminationEmailAddressToFinal', '') AS SURosterTerminationEmailAddressToFinal,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM person.PersonClientRoster PCR
		JOIN person.Person P ON P.PersonID = PCR.PersonID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonClientRoster'
			AND PE.EmailTemplateCode = 'SURosterTerminationFinal'
			AND PE.EntityID = PCR.PersonClientRosterID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingSURosterTerminationFinalEmail

--Begin procedure core.GetPendingSURosterTerminationInitialEmail
EXEC utility.DropObject 'core.GetPendingSURosterTerminationInitialEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.09
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingSURosterTerminationInitialEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		PCR.TerminationReason,
		core.GetSystemSetupValueBySystemSetupKey('SURosterContactEmailAddress', '') AS SURosterContactEmailAddress,
		core.GetSystemSetupValueBySystemSetupKey('SURosterTerminationEmailAddressToInitial', '') AS SURosterTerminationEmailAddressToInitial,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM person.PersonClientRoster PCR
		JOIN person.Person P ON P.PersonID = PCR.PersonID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonClientRoster'
			AND PE.EmailTemplateCode = 'SURosterTerminationInitial'
			AND PE.EntityID = PCR.PersonClientRosterID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingSURosterTerminationInitialEmail

--Begin procedure core.PendingEmailAddUpdate
EXEC utility.DropObject 'core.CreatePendingEmail'
EXEC utility.DropObject 'core.PendingEmailAddUpdate'
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.09
-- Description:	A stored procedure populate the core.PendingEmail table
-- ====================================================================
CREATE PROCEDURE core.PendingEmailAddUpdate

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50),
@EntityID INT,
@SystemSetupKey VARCHAR(250),
@DocumentIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @dSendDate DATE
	DECLARE @nPendingEmailID INT = ISNULL(
		(
		SELECT PE.PendingEmailID
		FROM core.PendingEmail PE 
		WHERE PE.EmailTemplateCode = @EmailTemplateCode 
			AND PE.EntityID = @EntityID 
			AND PE.SentDateTime IS NULL
		), 0)
	DECLARE @nPersonID INT

	IF @EmailTemplateCode = 'ProfileReview'
		BEGIN

		SELECT 
			@dSendDate = DATEADD(d, CAST(core.GetSystemSetupValueBySystemSetupKey(@SystemSetupKey, '30') AS INT) * -1, P.NextReviewDate),
			@nPersonID = P.PersonID
		FROM person.Person P 
		WHERE P.PersonID = @EntityID

		END
	ELSE IF @EmailTemplateCode = 'OperationalTrainingExpiration'
		BEGIN

		SELECT 
			@dSendDate = DATEADD(d, CAST(core.GetSystemSetupValueBySystemSetupKey(@SystemSetupKey, '30') AS INT) * -1, POT.ExpiryDate),
			@nPersonID = POT.PersonID
		FROM person.PersonOperationalTraining POT 
		WHERE POT.PersonOperationalTrainingID = @EntityID

		END
	ELSE IF @EmailTemplateCode = 'SecurityClearanceExpiration'
		BEGIN

		SELECT 
			@dSendDate = DATEADD(d, CAST(core.GetSystemSetupValueBySystemSetupKey(@SystemSetupKey, '30') AS INT) * -1, PSC.MandatoryReviewDate),
			@nPersonID = PSC.PersonID
		FROM person.PersonSecurityClearance PSC 
		WHERE PSC.PersonSecurityClearanceID = @EntityID

		END
	--ENDIF

	IF @dSendDate IS NOT NULL
		BEGIN

		IF @nPendingEmailID = 0
			BEGIN

			INSERT INTO core.PendingEmail 
				(EntityTypeCode, EmailTemplateCode, EntityID, PersonID, SendDate, DocumentIDList) 
			VALUES 
				(@EntityTypeCode, @EmailTemplateCode, @EntityID, ISNULL(@nPersonID, 0), @dSendDate, @DocumentIDList)

			END
		ELSE
			BEGIN

			UPDATE PE 
			SET PE.SendDate = @dSendDate 
			FROM core.PendingEmail PE 
			WHERE PE.PendingEmailID = @nPendingEmailID

			END
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure core.PendingEmailAddUpdate

--Begin procedure core.GetProjectIDByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'core.GetProjectIDByEntityTypeCodeAndEntityID'
GO
--End procedure core.GetProjectIDByEntityTypeCodeAndEntityID

--Begin procedure dropdown.GetAgeRangeData
EXEC Utility.DropObject 'dropdown.GetAgeRangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.AgeRange table
-- =================================================================================
CREATE PROCEDURE dropdown.GetAgeRangeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AgeRangeID, 
		T.AgeRangeCode,
		T.AgeRangeName
	FROM dropdown.AgeRange T
	WHERE (T.AgeRangeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AgeRangeName, T.AgeRangeID

END
GO
--End procedure dropdown.GetAgeRangeData

--Begin procedure dropdown.GetApplicantSourceData
EXEC Utility.DropObject 'dropdown.GetApplicantSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.ApplicantSource table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetApplicantSourceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ApplicantSourceID, 
		T.ApplicantSourceCode,
		T.ApplicantSourceName,
		T.HasOther
	FROM dropdown.ApplicantSource T
	WHERE (T.ApplicantSourceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ApplicantSourceName, T.ApplicantSourceID

END
GO
--End procedure dropdown.GetApplicantSourceData

--Begin procedure dropdown.GetCompetencyData
EXEC Utility.DropObject 'dropdown.GetCompetencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.Competency table
-- =================================================================================
CREATE PROCEDURE dropdown.GetCompetencyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CompetencyID, 
		T.CompetencyCode,
		T.CompetencyName
	FROM dropdown.Competency T
	WHERE (T.CompetencyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CompetencyName, T.CompetencyID

END
GO
--End procedure dropdown.GetCompetencyData

--Begin procedure dropdown.GetCompetencyTypeData
EXEC Utility.DropObject 'dropdown.GetCompetencyTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.CompetencyType table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetCompetencyTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CompetencyTypeID, 
		T.CompetencyTypeCode,
		T.CompetencyTypeName
	FROM dropdown.CompetencyType T
	WHERE (T.CompetencyTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CompetencyTypeName, T.CompetencyTypeID

END
GO
--End procedure dropdown.GetCompetencyTypeData

--Begin procedure dropdown.GetCSGMemberPerformanceRatingData
EXEC Utility.DropObject 'dropdown.GetCSGMemberPerformanceRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.CSGMemberPerformanceRating table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetCSGMemberPerformanceRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CSGMemberPerformanceRatingID, 
		T.CSGMemberPerformanceRatingCode,
		T.CSGMemberPerformanceRatingName
	FROM dropdown.CSGMemberPerformanceRating T
	WHERE (T.CSGMemberPerformanceRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CSGMemberPerformanceRatingName, T.CSGMemberPerformanceRatingID

END
GO
--End procedure dropdown.GetCSGMemberPerformanceRatingData

--Begin procedure dropdown.GetEthnicityData
EXEC Utility.DropObject 'dropdown.GetEthnicityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.Ethnicity table
-- ================================================================================
CREATE PROCEDURE dropdown.GetEthnicityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EthnicityID, 
		T.EthnicityCode,
		T.EthnicityName,
		T.EthnicityCategoryCode,
		T.EthnicityCategoryName,
		T.HasOther
	FROM dropdown.Ethnicity T
	WHERE (T.EthnicityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.EthnicityCategorDisplayOrder, T.EthnicityDisplayOrder, T.EthnicityName, T.EthnicityID

END
GO
--End procedure dropdown.GetEthnicityData

--Begin procedure dropdown.GetFeedbackQuestionData
EXEC Utility.DropObject 'dropdown.GetFeedbackQuestionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.FeedbackQuestion table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetFeedbackQuestionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FeedbackQuestionID, 
		T.FeedbackQuestionCode,
		T.FeedbackQuestionName,
		T.IsExternal,
		T.IsInternal
	FROM dropdown.FeedbackQuestion T
	WHERE (T.FeedbackQuestionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FeedbackQuestionName, T.FeedbackQuestionID

END
GO
--End procedure dropdown.GetFeedbackQuestionData

--Begin procedure dropdown.GetInterviewOutcomeData
EXEC Utility.DropObject 'dropdown.GetInterviewOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.InterviewOutcome table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetInterviewOutcomeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InterviewOutcomeID, 
		T.InterviewOutcomeCode,
		T.InterviewOutcomeName
	FROM dropdown.InterviewOutcome T
	WHERE (T.InterviewOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InterviewOutcomeName, T.InterviewOutcomeID

END
GO
--End procedure dropdown.GetInterviewOutcomeData

--Begin procedure dropdown.GetPersonTypeData
EXEC Utility.DropObject 'dropdown.GetPersonTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.PersonType table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetPersonTypeData

@IncludeZero BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PersonTypeID, 
		T.PersonTypeCode,
		T.PersonTypeName
	FROM dropdown.PersonType T
	WHERE (T.PersonTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PersonTypeName, T.PersonTypeID

END
GO
--End procedure dropdown.GetPersonTypeData

--Begin procedure dropdown.GetSuitabilityRatingData
EXEC Utility.DropObject 'dropdown.GetSuitabilityRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.SuitabilityRating table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetSuitabilityRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SuitabilityRatingID, 
		T.SuitabilityRatingCode,
		T.SuitabilityRatingName
	FROM dropdown.SuitabilityRating T
	WHERE (T.SuitabilityRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SuitabilityRatingName, T.SuitabilityRatingID

END
GO
--End procedure dropdown.GetSuitabilityRatingData

--Begin procedure dropdown.GetSUPerformanceRatingData
EXEC Utility.DropObject 'dropdown.GetSUPerformanceRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.SUPerformanceRating table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetSUPerformanceRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SUPerformanceRatingID, 
		T.SUPerformanceRatingCode,
		T.SUPerformanceRatingName
	FROM dropdown.SUPerformanceRating T
	WHERE (T.SUPerformanceRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SUPerformanceRatingName, T.SUPerformanceRatingID

END
GO
--End procedure dropdown.GetSUPerformanceRatingData

--Begin procedure dropdown.GetPersonUpdateTypeData
EXEC Utility.DropObject 'dropdown.GetPersonUpdateTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.02.12
-- Description:	A stored procedure to return data from the dropdown.PersonUpdateType table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetPersonUpdateTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PersonUpdateTypeID, 
		T.PersonUpdateTypeCode,
		T.PersonUpdateTypeName
	FROM dropdown.PersonUpdateType T
	WHERE (T.PersonUpdateTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PersonUpdateTypeName, T.PersonUpdateTypeID

END
GO
--End procedure dropdown.GetPersonUpdateTypeData

--Begin procedure dropdown.GetVacancyStatusData
EXEC Utility.DropObject 'dropdown.GetVacancyStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.VacancyStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetVacancyStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VacancyStatusID, 
		T.VacancyStatusCode,
		T.VacancyStatusName
	FROM dropdown.VacancyStatus T
	WHERE (T.VacancyStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VacancyStatusName, T.VacancyStatusID

END
GO
--End procedure dropdown.GetVacancyStatusData

--Begin procedure dropdown.GetVacancyTypeData
EXEC Utility.DropObject 'dropdown.GetVacancyTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.VacancyType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetVacancyTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VacancyTypeID, 
		T.VacancyTypeCode,
		T.VacancyTypeName
	FROM dropdown.VacancyType T
	WHERE (T.VacancyTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VacancyTypeName, T.VacancyTypeID

END
GO
--End procedure dropdown.GetVacancyTypeData

--Begin procedure eventlog.LogFeedbackAction
EXEC utility.DropObject 'eventlog.LogFeedbackAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFeedbackAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Feedback',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogFeedbackTable', 'u')) IS NOT NULL
			DROP TABLE #LogFeedbackTable
		--ENDIF
		
		SELECT *
		INTO #LogFeedbackTable
		FROM person.Feedback F
		WHERE F.FeedbackID = @EntityID

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Feedback',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Feedback'), ELEMENTS
			)
		FROM #LogFeedbackTable T
			JOIN person.Feedback F ON F.FeedbackID = T.FeedbackID

		DROP TABLE #LogFeedbackTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFeedbackAction

--Begin procedure eventlog.LogPersonUpdateAction
EXEC utility.DropObject 'eventlog.LogPersonUpdateAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PersonUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogPersonUpdateTable', 'u')) IS NOT NULL
			DROP TABLE #LogPersonUpdateTable
		--ENDIF
		
		SELECT *
		INTO #LogPersonUpdateTable
		FROM personupdate.PersonUpdate PU
		WHERE PU.PersonUpdateID = @EntityID

		DECLARE @cPersonUpdatePerson VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePerson = COALESCE(@cPersonUpdatePerson, '') + D.PersonUpdatePerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePerson'), ELEMENTS) AS PersonUpdatePerson
			FROM personupdate.Person T 
			WHERE T.PersonUpdateID = @EntityID
			) D

		DECLARE @cPersonUpdatePersonGroupPerson VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePersonGroupPerson = COALESCE(@cPersonUpdatePersonGroupPerson, '') + D.PersonUpdatePersonGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePersonGroupPerson'), ELEMENTS) AS PersonUpdatePersonGroupPerson
			FROM personupdate.PersonGroupPerson T 
			WHERE T.PersonUpdateID = @EntityID
			) D

		DECLARE @cPersonUpdatePersonPermissionable VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePersonPermissionable = COALESCE(@cPersonUpdatePersonPermissionable, '') + D.PersonUpdatePersonPermissionable 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePersonPermissionable'), ELEMENTS) AS PersonUpdatePersonPermissionable
			FROM personupdate.PersonPermissionable T 
			WHERE T.PersonUpdateID = @EntityID
			) D

		DECLARE @cPersonUpdatePersonProject VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePersonProject = COALESCE(@cPersonUpdatePersonProject, '') + D.PersonUpdatePersonProject 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePersonProject'), ELEMENTS) AS PersonUpdatePersonProject
			FROM personupdate.PersonProject T 
			WHERE T.PersonUpdateID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PersonUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<PersonUpdatePerson>' + ISNULL(@cPersonUpdatePerson, '') + '</PersonUpdatePerson>') AS XML),
			CAST(('<PersonUpdatePersonGroupPerson>' + ISNULL(@cPersonUpdatePersonGroupPerson, '') + '</PersonUpdatePersonGroupPerson>') AS XML),
			CAST(('<PersonUpdatePersonPermissionable>' + ISNULL(@cPersonUpdatePersonPermissionable, '') + '</PersonUpdatePersonPermissionable>') AS XML),
			CAST(('<PersonUpdatePersonProject>' + ISNULL(@cPersonUpdatePersonProject, '') + '</PersonUpdatePersonProject>') AS XML)
			FOR XML RAW('PersonUpdate'), ELEMENTS
			)
		FROM #LogPersonUpdateTable T
			JOIN personupdate.PersonUpdate PU ON PU.PersonUpdateID = T.PersonUpdateID

		DROP TABLE #LogPersonUpdateTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonUpdateAction

--Begin procedure eventlog.LogVacancyAction
EXEC utility.DropObject 'eventlog.LogVacancyAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogVacancyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Vacancy',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogVacancyTable', 'u')) IS NOT NULL
			DROP TABLE #LogVacancyTable
		--ENDIF
		
		SELECT *
		INTO #LogVacancyTable
		FROM hrms.Vacancy V
		WHERE V.VacancyID = @EntityID

		DECLARE @cVacancyCompetency VARCHAR(MAX) 
	
		SELECT 
			@cVacancyCompetency = COALESCE(@cVacancyCompetency, '') + D.VacancyCompetency 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('VacancyCompetency'), ELEMENTS) AS VacancyCompetency
			FROM hrms.VacancyCompetency T 
			WHERE T.VacancyID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Vacancy',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<VacancyCompetency>' + ISNULL(@cVacancyCompetency, '') + '</VacancyCompetency>') AS XML)
			FOR XML RAW('Vacancy'), ELEMENTS
			)
		FROM #LogVacancyTable T
			JOIN hrms.Vacancy V ON V.VacancyID = T.VacancyID

		DROP TABLE #LogVacancyTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogVacancyAction

--Begin procedure eventlog.LogVacancyApplicationAction
EXEC utility.DropObject 'eventlog.LogVacancyApplicationAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogVacancyApplicationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'VacancyApplication',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogApplicationTable', 'u')) IS NOT NULL
			DROP TABLE #LogApplicationTable
		--ENDIF
		
		SELECT *
		INTO #LogApplicationTable
		FROM hrms.Application A
		WHERE A.ApplicationID = @EntityID

		DECLARE @cApplicationCompetency VARCHAR(MAX) 
	
		SELECT 
			@cApplicationCompetency = COALESCE(@cApplicationCompetency, '') + D.ApplicationCompetency 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationCompetency'), ELEMENTS) AS ApplicationCompetency
			FROM hrms.ApplicationCompetency T 
			WHERE T.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationEthnicity VARCHAR(MAX) 
	
		SELECT 
			@cApplicationEthnicity = COALESCE(@cApplicationEthnicity, '') + D.ApplicationEthnicity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationEthnicity'), ELEMENTS) AS ApplicationEthnicity
			FROM hrms.ApplicationEthnicity T 
			WHERE T.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationInterviewAvailability VARCHAR(MAX) 
	
		SELECT 
			@cApplicationInterviewAvailability = COALESCE(@cApplicationInterviewAvailability, '') + D.ApplicationInterviewAvailability 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationInterviewAvailability'), ELEMENTS) AS ApplicationInterviewAvailability
			FROM hrms.ApplicationInterviewAvailability T
				JOIN hrms.ApplicationPerson AP ON AP.ApplicationPersonID = T.ApplicationPersonID
					AND AP.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationInterviewSchedule VARCHAR(MAX) 
	
		SELECT 
			@cApplicationInterviewSchedule = COALESCE(@cApplicationInterviewSchedule, '') + D.ApplicationInterviewSchedule 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationInterviewSchedule'), ELEMENTS) AS ApplicationInterviewSchedule
			FROM hrms.ApplicationInterviewSchedule T 
			WHERE T.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationPerson VARCHAR(MAX) 
	
		SELECT 
			@cApplicationPerson = COALESCE(@cApplicationPerson, '') + D.ApplicationPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationPerson'), ELEMENTS) AS ApplicationPerson
			FROM hrms.ApplicationPerson T 
			WHERE T.ApplicationID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'VacancyApplication',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ApplicationCompetency>' + ISNULL(@cApplicationCompetency, '') + '</ApplicationCompetency>') AS XML),
			CAST(('<ApplicationEthnicity>' + ISNULL(@cApplicationEthnicity, '') + '</ApplicationEthnicity>') AS XML),
			CAST(('<ApplicationInterviewAvailability>' + ISNULL(@cApplicationInterviewAvailability, '') + '</ApplicationInterviewAvailability>') AS XML),
			CAST(('<ApplicationInterviewSchedule>' + ISNULL(@cApplicationInterviewSchedule, '') + '</ApplicationInterviewSchedule>') AS XML),
			CAST(('<ApplicationPerson>' + ISNULL(@cApplicationPerson, '') + '</ApplicationPerson>') AS XML)
			FOR XML RAW('VacancyApplication'), ELEMENTS
			)
		FROM #LogApplicationTable T
			JOIN hrms.Application V ON V.ApplicationID = T.ApplicationID

		DROP TABLE #LogApplicationTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogVacancyApplicationAction

--Begin procedure hrms.GetApplicationByApplicationID
EXEC utility.DropObject 'hrms.GetApplicationByApplicationID'
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.23
-- Description:	A stored procedure to return data from the hrms.Application table
-- ==============================================================================
CREATE PROCEDURE hrms.GetApplicationByApplicationID

@ApplicationID INT,
@PersonID INT,
@VacancyID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @ApplicationID = 0
		BEGIN

		--Application
		SELECT
			0 AS ApplicationID,
			NULL AS ConflictsOfInterest,
			0 AS HasCertifiedApplication,
			0 AS HasConfirmedTerminationDate,
			CASE WHEN P.DAPersonID > 0 THEN 1 ELSE 0 END AS HasDAAccount,
			0 AS IsGovernmetEmployee,
			0 AS IsGuaranteedInterview,
			0 AS IsApplicationFinal,
			0 AS IsNotCriminalCivilService,
			0 AS IsNotCriminalPolice,
			0 AS IsDisabled,
			0 AS IsDueDilligenceComplete,
			0 AS IsEligible,
			0 AS IsHermisProfileUpdated,
			0 AS IsHRContactNotified,
			0 AS IsInterviewAvailableAMDay1,
			0 AS IsInterviewAvailableAMDay2,
			0 AS IsInterviewAvailableAMDay3,
			0 AS IsInterviewAvailableAMDay4,
			0 AS IsInterviewAvailableAMDay5,
			0 AS IsInterviewAvailableAMDay6,
			0 AS IsInterviewAvailableAMDay7,
			0 AS IsInterviewAvailablePMDay1,
			0 AS IsInterviewAvailablePMDay2,
			0 AS IsInterviewAvailablePMDay3,
			0 AS IsInterviewAvailablePMDay4,
			0 AS IsInterviewAvailablePMDay5,
			0 AS IsInterviewAvailablePMDay6,
			0 AS IsInterviewAvailablePMDay7,
			0 AS IsInterviewComplete,
			0 AS IsLineManagerNotified,
			0 AS IsOfferAccepted,
			0 AS IsOfferExtended,
			0 AS IsOnRoster,
			0 AS IsPreQualified,
			0 AS IsVetted,
			NULL AS ManagerComments,
			@PersonID AS PersonID,
			NULL AS SubmittedDateTime,
			NULL AS SubmittedDateTimeFormatted,
			NULL AS Suitability,
			NULL AS TechnicalManagerComments,
			NULL AS TerminationDate,
			NULL AS TerminationDateFormatted,
			@VacancyID AS VacancyID,
			0 AS AgeRangeID,
			NULL AS AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			0 AS ExpertCategoryID,
			NULL AS ExpertCategoryName,
			0 AS GenderID,
			NULL AS GenderName,
			0 AS InterviewOutcomeID,
			NULL AS InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName		
		FROM person.Person P
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND P.PersonID = @PersonID

		END
	ELSE
		BEGIN

		SELECT 
			@PersonID = A.PersonID,
			@VacancyID = A.VacancyID
		FROM hrms.Application A 
		WHERE A.ApplicationID = @ApplicationID

		--Application
		SELECT
			A.ApplicationID,
			A.ConflictsOfInterest,
			A.HasCertifiedApplication,
			A.HasConfirmedTerminationDate,
			A.HasDAAccount,
			A.IsGuaranteedInterview,
			A.IsApplicationFinal,
			A.IsNotCriminalCivilService,
			A.IsNotCriminalPolice,
			A.IsDisabled,
			A.IsDueDilligenceComplete,
			A.IsEligible,
			A.IsGovernmetEmployee,
			A.IsHermisProfileUpdated,
			A.IsHRContactNotified,
			A.IsInterviewAvailableAMDay1,
			A.IsInterviewAvailableAMDay2,
			A.IsInterviewAvailableAMDay3,
			A.IsInterviewAvailableAMDay4,
			A.IsInterviewAvailableAMDay5,
			A.IsInterviewAvailableAMDay6,
			A.IsInterviewAvailableAMDay7,
			A.IsInterviewAvailablePMDay1,
			A.IsInterviewAvailablePMDay2,
			A.IsInterviewAvailablePMDay3,
			A.IsInterviewAvailablePMDay4,
			A.IsInterviewAvailablePMDay5,
			A.IsInterviewAvailablePMDay6,
			A.IsInterviewAvailablePMDay7,
			A.IsInterviewComplete,
			A.IsLineManagerNotified,
			A.IsOfferAccepted,
			A.IsOfferExtended,
			A.IsOnRoster,
			A.IsPreQualified,
			A.IsVetted,
			A.ManagerComments,
			A.PersonID,
			A.SubmittedDateTime,
			core.FormatDate(A.SubmittedDateTime) AS SubmittedDateTimeFormatted,
			A.Suitability,
			A.TechnicalManagerComments,
			A.TerminationDate,
			core.FormatDate(A.TerminationDate) AS TerminationDateFormatted,
			A.VacancyID,
			AR.AgeRangeID,
			AR.AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			EC.ExpertCategoryID,
			EC.ExpertCategoryName,
			G.GenderID,
			G.GenderName,
			IO.InterviewOutcomeID,
			IO.InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName		
		FROM hrms.Application A
			JOIN person.Person P ON P.PersonID = A.PersonID
			JOIN dropdown.AgeRange AR ON AR.AgeRangeID = A.AgeRangeID
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = A.ExpertCategoryID
			JOIN dropdown.Gender G ON G.GenderID = A.GenderID
			JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = A.InterviewOutcomeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND A.ApplicationID = @ApplicationID

		END
	--ENDIF

	--ApplicationDocumentCV
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Application'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @ApplicationID

	--ApplicationDocumentPassport
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Application'
			AND DE.EntityTypeSubCode = 'Passport'
			AND DE.EntityID = @ApplicationID

	--ApplicationEthnicity
	SELECT 
		E.EthnicityID,
		E.EthnicityName,
		AE.EthnicityDescription
	FROM hrms.ApplicationEthnicity AE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = AE.EthnicityID
			AND AE.ApplicationID = @ApplicationID
	ORDER BY E.EthnicityName, E.EthnicityID

	--ApplicationInterviewAvailability
	SELECT 
		AIA.ApplicationInterviewAvailabilityID,
		AIA.ApplicationInterviewScheduleID,
		AIA.ApplicationPersonID,
		AIA.IsAvailable,
		core.yesNoFormat(AIA.IsAvailable) AS IsAvailableYesNoFormat
	FROM hrms.ApplicationInterviewAvailability AIA
	WHERE AIA.ApplicationPersonID IN (SELECT AP.ApplicationPersonID FROM hrms.ApplicationPerson AP WHERE AP.ApplicationID = @ApplicationID)

	--ApplicationInterviewSchedule
	SELECT 
		AIS.ApplicationInterviewScheduleID,
		AIS.InterviewDateTime,
		core.FormatDateTime(AIS.InterviewDateTime) AS InterviewDateTimeFormatted,
		AIS.IsSelectedDate,
		core.YesNoFormat(AIS.IsSelectedDate) AS IsSelectedDateYesNoFormat
	FROM hrms.ApplicationInterviewSchedule AIS
	WHERE AIS.ApplicationID = @ApplicationID

	--ApplicationPerson
	SELECT 
		AP.ApplicationID,
		AP.ApplicationPersonID,
		AP.InterviewOutcomeID,
		AP.IsScheduleConfirmed,
		core.YesNoFormat(AP.IsScheduleConfirmed) AS IsScheduleConfirmedYesNoFormat,
		AP.PersonID,
		person.FormatPersonNameByPersonID(AP.PersonID, 'FirstLast') AS PersonNameFormatted,
		AP.PersonTypeCode,
		AP.ProposesInterviewDates,
		core.YesNoFormat(AP.ProposesInterviewDates) AS ProposesInterviewDatesYesNoFormat,
		IO.InterviewOutcomeName,
		P.FirstName,
		P.LastName
	FROM hrms.ApplicationPerson AP
		JOIN person.Person P ON P.PersonID = AP.PersonID
		JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = AP.InterviewOutcomeID
			AND AP.ApplicationID = @ApplicationID
	ORDER BY P.LastName, P.FirstName

	--PersonApplicantSource
	SELECT
		APS.ApplicantSourceID,
		APS.ApplicantSourceName
	FROM person.PersonApplicantSource PAS
		JOIN dropdown.ApplicantSource APS ON APS.ApplicantSourceID = PAS.ApplicantSourceID
			AND PAS.PersonID = @PersonID
	ORDER BY APS.ApplicantSourceName, APS.ApplicantSourceID

	--PersonCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonCountry PC
		JOIN dropdown.Country C ON C.CountryID = PC.CountryID
			AND PC.PersonID = @PersonID
	ORDER BY C.CountryName, C.CountryID

	--PersonLanguage
	SELECT
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--Vacancy
	SELECT
		EC.ExpertCategoryName,
		P.ProjectName,
		PS.ProjectSponsorName,
		V.ProjectID,
		V.VacancyName,
		V.VacancyReference,
		VT.VacancyTypeName
	FROM hrms.Vacancy V
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = V.ExpertCategoryID
		JOIN dropdown.Project P ON P.ProjectID = V.ProjectID
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
		JOIN dropdown.VacancyType VT ON VT.VacancyTypeID = V.VacancyTypeID
			AND V.VacancyID = @VacancyID

	--VacancyCompetency
	SELECT
		C.CompetencyDescription,
		C.CompetencyID,
		C.CompetencyName,
		OAAC.Suitability,
		ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'Competency'
			AND C.IsActive = 1
		OUTER APPLY
			(
			SELECT
				AC.Suitability,
				AC.SuitabilityRatingID
			FROM hrms.ApplicationCompetency AC
			WHERE AC.CompetencyID = C.CompetencyID
				AND AC.ApplicationID = @ApplicationID
			) OAAC
	ORDER BY C.CompetencyName, C.CompetencyID

	--VacancyTechnicalCapability
	SELECT
		C.CompetencyDescription,
		C.CompetencyID,
		C.CompetencyName,
		OAAC.Suitability,
		ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'TechnicalCapability'
			AND C.IsActive = 1
		OUTER APPLY
			(
			SELECT
				AC.Suitability,
				AC.SuitabilityRatingID
			FROM hrms.ApplicationCompetency AC
			WHERE AC.CompetencyID = C.CompetencyID
				AND AC.ApplicationID = @ApplicationID
			) OAAC
	ORDER BY C.CompetencyName, C.CompetencyID

END
GO
--End procedure hrms.GetApplicationByApplicationID

--Begin procedure hrms.GetCompetencyByCompetencyID
EXEC utility.DropObject 'hrms.GetCompetencyByCompetencyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:2019.01.26
-- Description:	A stored procedure to get data from the hrms.Competency table
-- ================================================================================
CREATE PROCEDURE [hrms].[GetCompetencyByCompetencyID]

@CompetencyID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Competency
	SELECT
		C.CompetencyDescription,
		C.CompetencyID,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeCode,
		CT.CompetencyTypeName
	FROM hrms.Competency C
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND C.CompetencyID = @CompetencyID

END 
GO
--End procedure hrms.GetCompetencyByCompetencyID

--Begin procedure hrms.GetVacancyByVacancyID
EXEC utility.DropObject 'hrms.GetVacancyByVacancyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:2019.01.26
-- Description:	A stored procedure to get data from the hrms.Vacancy table
-- ================================================================================
CREATE PROCEDURE hrms.GetVacancyByVacancyID

@VacancyID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Vacancy', @VacancyID)

	--Vacancy
	SELECT
		EC.ExpertCategoryName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(V.CoordinatorPersonID, 'FirstLast') AS CoordinatorPersonNameFormatted,
		PS.ProjectSponsorName,
		V.CloseDateTime,
		core.FormatDateTime(V.CloseDateTime) AS CloseDateTimeFormatted,
		iif(
			OpenDateTime IS NULL OR CloseDateTime IS NULL OR OpenDateTime > getDate(), 0, 
			iif(CloseDateTime<getDate(),
				DATEDIFF(d, OpenDateTime, CloseDateTime),
				DATEDIFF(d, OpenDateTime, getDate())
			)
		) AS DaysAdvertised,
		V.CoordinatorPersonID,
		V.ExpertCategoryID,
		V.OpenDateTime,
		core.FormatDateTime(V.OpenDateTime) AS OpenDateTimeFormatted,
		V.ProjectID,
		V.RoleDescription,
		V.VacancyContactEmailAddress,
		V.VacancyContactName,
		V.VacancyContactPhone,
		V.VacancyID,
		(SELECT COUNT(A.PersonID) FROM hrms.Application A WHERE A.VacancyID = V.VacancyID AND A.SubmittedDateTime IS NOT NULL) AS ApplicationCount,
		V.VacancyName,
		V.VacancyNotes,
		V.VacancyReference,
		V.VacancyStatusID,
		V.VacancyTypeID,
		VS.VacancyStatusCode,
		VS.VacancyStatusName,
		VT.VacancyTypeName
	FROM hrms.Vacancy V
		JOIN dropdown.Project P ON P.ProjectID = V.ProjectID
		JOIN dropdown.VacancyStatus VS ON VS.VacancyStatusID = V.VacancyStatusID
		JOIN dropdown.VacancyType VT ON VT.VacancyTypeID = V.VacancyTypeID
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = V.ExpertCategoryID
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
			AND V.VacancyID = @VacancyID

	--VacancyApplication
	SELECT
		A.ApplicationID,
		A.ConflictsOfInterest,
		A.HasCertifiedApplication,
		A.HasDAAccount,
		A.IsGuaranteedInterview,
		A.IsApplicationFinal,
		A.IsNotCriminalCivilService,
		A.IsNotCriminalPolice,
		A.IsDisabled,
		A.IsDueDilligenceComplete,
		A.IsEligible,
		A.IsHermisProfileUpdated,
		A.IsHRContactNotified,
		A.IsInterviewAvailableAMDay1,
		A.IsInterviewAvailableAMDay2,
		A.IsInterviewAvailableAMDay3,
		A.IsInterviewAvailableAMDay4,
		A.IsInterviewAvailableAMDay5,
		A.IsInterviewAvailableAMDay6,
		A.IsInterviewAvailableAMDay7,
		A.IsInterviewAvailablePMDay1,
		A.IsInterviewAvailablePMDay2,
		A.IsInterviewAvailablePMDay3,
		A.IsInterviewAvailablePMDay4,
		A.IsInterviewAvailablePMDay5,
		A.IsInterviewAvailablePMDay6,
		A.IsInterviewAvailablePMDay7,
		A.IsInterviewComplete,
		A.IsLineManagerNotified,
		A.IsOfferAccepted,
		A.IsOfferExtended,
		A.IsOnRoster,
		A.IsPreQualified,
		A.IsVetted,
		A.ManagerComments,
		A.PersonID,
		A.SubmittedDateTime,
		core.FormatDate(A.SubmittedDateTime) AS SubmittedDateTimeFormatted,
		A.Suitability,
		A.TechnicalManagerComments,
		A.VacancyID,
		AR.AgeRangeID,
		AR.AgeRangeName,
		NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
		CCC1.CountryCallingCodeID,
		NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
		NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
		CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
		CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
		EC.ExpertCategoryID,
		EC.ExpertCategoryName,
		G.GenderID,
		G.GenderName,
		IO.InterviewOutcomeID,
		IO.InterviewOutcomeName,
		P.ApplicantSourceOther,
		P.CellPhone,
		P.CivilServiceDepartment,
		P.CivilServiceGrade1,
		P.CivilServiceGrade2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.EmailAddress,
		P.FirstName,
		P.HomePhone,
		P.HRContactEmailAddress,
		P.HRContactName,
		P.HRContactPhone,
		P.IsPreviousApplicant,
		P.IsUKEUNational,
		P.IsUKResidencyEligible,
		P.LastContractEndDate,
		core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
		P.LastName,
		P.LineManagerEmailAddress,
		P.LineManagerName,
		P.LineManagerPhone,
		P.MiddleName,
		P.NickName,
		P.OtherGrade,
		P.PersonID,
		P.PreviousApplicationDate,
		core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
		P.SecurityClearanceSponsorName,
		P.Suffix,
		P.Title,
		P.WorkPhone,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR1.PoliceRankID AS PoliceRankID1,
		PR1.PoliceRankName AS PoliceRankName1,
		PR2.PoliceRankID AS PoliceRankID2,
		PR2.PoliceRankName AS PoliceRankName2,
		PT.PersonTypeID,
		PT.PersonTypeCode,
		SC.SecurityClearanceID,
		SC.SecurityClearanceName,
		CASE
			WHEN workflow.GetWorkflowStepNumber('Application', A.ApplicationID) > workflow.GetWorkflowStepCount('Application', A.ApplicationID)
			THEN 'Approved'
			ELSE 'Step ' + CAST(workflow.GetWorkflowStepNumber('Application', A.ApplicationID) AS VARCHAR(5)) + ' of ' + CAST(workflow.GetWorkflowStepCount('Application', A.ApplicationID) AS VARCHAR(5)) 
		END AS WorkflowStep
	FROM hrms.Application A
		JOIN person.Person P ON P.PersonID = A.PersonID
		JOIN dropdown.AgeRange AR ON AR.AgeRangeID = A.AgeRangeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = A.ExpertCategoryID
		JOIN dropdown.Gender G ON G.GenderID = A.GenderID
		JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = A.InterviewOutcomeID
		JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
		JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
			WHERE A.VacancyID = @VacancyID
			ORDER BY P.LastName, P.FirstName

	--VacancyCompetency
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		VC.CompetencyID,
		VC.VacancyCompetencyID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'Competency'

	--VacancyDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Vacancy'
			AND DE.EntityID = @VacancyID

	--VacancyTechnicalCapability
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		VC.CompetencyID,
		VC.VacancyCompetencyID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'TechnicalCapability'
	
	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'Vacancy', @VacancyID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Vacancy', @VacancyID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Vacancy'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Vacancy'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Vacancy'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Vacancy'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'Vacancy'
		AND EL.EntityID = @VacancyID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure hrms.GetVacancyByVacancyID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Activity'
		SELECT @bHasAccess = 1 FROM activity.Activity T WHERE T.ActivityID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ActivityReport'
		SELECT @bHasAccess = 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Asset'
		SELECT @bHasAccess = 1 FROM asset.Asset T WHERE T.AssetID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Contact'
		SELECT @bHasAccess = 1 FROM contact.Contact T WHERE T.ContactID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Course'
		SELECT @bHasAccess = 1 FROM training.Course T WHERE T.CourseID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Document'
		SELECT @bHasAccess = 1 FROM document.Document T WHERE T.DocumentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentCatalog'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentCatalog T WHERE T.EquipmentCatalogID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentItem'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentItem T WHERE T.EquipmentItemID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentOrder'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentOrder T WHERE T.EquipmentOrderID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentMovement'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentMovement T WHERE T.EquipmentMovementID = @EntityID
	ELSE IF @EntityTypeCode = 'Feedback'
		SELECT @bHasAccess = 1 FROM person.Feedback T WHERE T.FeedbackID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Force'
		SELECT @bHasAccess = 1 FROM force.Force T WHERE T.ForceID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Incident'
		SELECT @bHasAccess = 1 FROM core.Incident T WHERE T.IncidentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Module'
		SELECT @bHasAccess = 1 FROM training.Module T WHERE T.ModuleID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'PersonUpdate'
		SELECT @bHasAccess = 1 FROM personupdate.PersonUpdate PU WHERE PU.PersonUpdateID = @EntityID
	ELSE IF @EntityTypeCode = 'ReportUpdate'
		SELECT @bHasAccess = 1 FROM core.ReportUpdate T WHERE T.ReportUpdateID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ResponseDashboard'
		SELECT @bHasAccess = 1 FROM responsedashboard.ResponseDashboard T WHERE T.ResponseDashboardID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'SpotReport'
		SELECT @bHasAccess = 1 FROM spotreport.SpotReport T WHERE T.SpotReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Territory'
		SELECT @bHasAccess = 1 FROM territory.Territory T JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode AND T.TerritoryID = @EntityID AND TT.IsReadOnly = 0
	ELSE IF @EntityTypeCode = 'TrendReport'
		SELECT @bHasAccess = 1 FROM trendreport.TrendReport T WHERE T.TrendReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Vacancy'
		SELECT @bHasAccess = 1 FROM hrms.Vacancy V WHERE V.VacancyID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = V.ProjectID)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetFeedbackByFeedbackID
EXEC Utility.DropObject 'person.GetFeedbackByFeedbackID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the person.Feedback table
-- =============================================================================
CREATE PROCEDURE person.GetFeedbackByFeedbackID

@FeedbackID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Feedback', @FeedbackID)

	--Feedback
	SELECT
		F.CreateDate,
		core.FormatDate(F.CreateDate) AS CreateDateFormatted,
		F.CreatePersonID,
		person.FormatPersonNameByPersonID(F.CreatePersonID, 'LastFirst') AS CreatePersonNameFormated,
		F.CSGMemberComment,
		F.CSGMemberPersonID,
		person.FormatPersonNameByPersonID(F.CSGMemberPersonID, 'LastFirst') AS CSGMemberPersonNameFormated,
		F.EndDate,
		core.FormatDate(F.EndDate) AS EndDateFormatted,
		F.FeedbackID,
		F.FeedbackName,
		F.IsExternal,
		F.Question4,
		F.Question5,
		F.Question7,
		F.StartDate,
		core.FormatDate(F.StartDate) AS StartDateFormatted,
		F.TaskID,
		MPR.CSGMemberPerformanceRatingCode,
		MPR.CSGMemberPerformanceRatingID,
		MPR.CSGMemberPerformanceRatingName,
		P.ProjectID,
		P.ProjectName,
		PR1.SUPerformanceRatingCode AS SUPerformanceRatingCode1,
		PR1.SUPerformanceRatingID AS SUPerformanceRatingID1,
		PR1.SUPerformanceRatingName AS SUPerformanceRatingName1,
		PR2.SUPerformanceRatingCode AS SUPerformanceRatingCode2,
		PR2.SUPerformanceRatingID AS SUPerformanceRatingID2,
		PR2.SUPerformanceRatingName AS SUPerformanceRatingName2,
		PR3.SUPerformanceRatingCode AS SUPerformanceRatingCode3,
		PR3.SUPerformanceRatingID AS SUPerformanceRatingID3,
		PR3.SUPerformanceRatingName AS SUPerformanceRatingName3
	FROM person.Feedback F
		JOIN dropdown.CSGMemberPerformanceRating MPR ON MPR.CSGMemberPerformanceRatingID = F.CSGMemberPerformanceRatingID
		JOIN dropdown.Project P ON P.ProjectID = F.ProjectID
		JOIN dropdown.SUPerformanceRating PR1 ON PR1.SUPerformanceRatingID = F.SUPerformanceRatingID1
		JOIN dropdown.SUPerformanceRating PR2 ON PR2.SUPerformanceRatingID = F.SUPerformanceRatingID2
		JOIN dropdown.SUPerformanceRating PR3 ON PR3.SUPerformanceRatingID = F.SUPerformanceRatingID3
			AND F.FeedbackID = @FeedbackID

	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'Feedback', @FeedbackID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Feedback', @FeedbackID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Feedback'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Feedback'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Feedback'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Feedback'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode LIKE '%Feedback'
		AND EL.EntityID = @FeedbackID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure person.GetFeedbackByFeedbackID

--Begin procedure person.GetMedicalVettingEmailData
EXEC Utility.DropObject 'person.GetMedicalVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.16
-- Description:	A stored procedure to return data for outgoing medical vetting e-mail
-- ==================================================================================
CREATE PROCEDURE person.GetMedicalVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@MedicalClearanceTypeIDList VARCHAR(MAX),
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonMedicalClearanceProcessStep
		(PersonID, MedicalClearanceTypeID, MedicalClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		CAST(LTT2.ListItem AS INT),
		(SELECT MCPS.MedicalClearanceProcessStepID FROM dropdown.MedicalClearanceProcessStep MCPS WHERE MCPS.MedicalClearanceProcessStepCode = 'InPreparation'),
		getDate(),
		1,
		'Medical clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT1 ON CAST(LTT1.ListItem AS INT) = P.PersonID
		CROSS APPLY core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT2

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		(SELECT 
			REPLACE(STUFF((
				SELECT ',' + MedicalClearanceTypeName
				FROM dropdown.MedicalClearanceType MCT
					JOIN core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MCT.MedicalClearanceTypeID
				ORDER BY 1
				FOR XML PATH('')), 1, 1, ''), ',', ' and ')) AS MedicalClearanceTypeName
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		0 AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL AS MedicalClearanceTypeName

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetMedicalVettingEmailData

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.19
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.26
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.29
-- Description: Added IsOnSURoster to the select list
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.30
-- Description: Added perosn record fields for personupdate workflow
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.ApplicantSourceOther,
		P.ApplicantTypeCode,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CivilServiceDepartment,
		P.CivilServiceGrade1,
		P.CivilServiceGrade2,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAAccountDecomissionedDate,
		core.FormatDate(P.DAAccountDecomissionedDate) AS DAAccountDecomissionedDateFormatted,
		P.DAPersonID,
		IIF(P.DAPersonID > 0, 'Yes ', 'No ') AS HasDAAccountYesNoFormat,
		P.DaysToRemoval,
		P.DefaultProjectID,
		P.DeleteByPersonID,
		person.FormatPersonNameByPersonID(P.DeleteByPersonID, 'FirstLast') AS DeleteByPersonNameFormatted,
		P.DeleteDate,
		core.FormatDate(P.DeleteDate) AS DeleteDateFormatted,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FARemoveByPersonID,
		person.FormatPersonNameByPersonID(P.FARemoveByPersonID, 'FirstLast') AS FARemoveByPersonNameFormatted,
		P.FARemoveDate,
		core.FormatDate(P.FARemoveDate) AS FARemoveDateFormatted,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.HRContactCountryCallingCodeID,
		P.HRContactEmailAddress,
		P.HRContactName,
		P.HRContactPhone,
		P.InvalidLoginAttempts,
		P.InvoiceLimit,
		P.IsAccountLockedOut,
		P.IsActive,
		P.ISDAAccountDecomissioned,
		IIF(P.ISDAAccountDecomissioned = 1, 'Yes', 'No') AS ISDAAccountDecomissionedYesNoFormat,
		P.IsPhoneVerified,		
		P.IsPreviousApplicant,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKResidencyEligible,
		P.IsUKWorkEligible,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastContractEndDate,
		core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.LineManagerCountryCallingCodeID,
		P.LineManagerEmailAddress,
		P.LineManagerName,
		P.LineManagerPhone,
		P.LockOutByPersonID,
		person.FormatPersonNameByPersonID(P.LockOutByPersonID, 'FirstLast') AS LockOutByPersonNameFormatted,
		P.LockOutDate,
		core.FormatDate(P.LockOutDate) AS LockOutDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.OtherGrade,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'Humanitarian'), 0) AS IsOnHSOTRoster,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'SU'), 0) AS IsOnSURoster,
		ISNULL((SELECT TOP 1 SC.SecurityClearanceName + ' - ' + core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID AND PSC.IsActive = 1 AND PSC.PersonID = P.PersonID), 'None') AS Vetting,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.PreviousApplicationDate,
		core.FormatDateTime(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
		P.PurchaseOrderLimit,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.SecurityClearanceID,
		P.SecurityClearanceSponsorName,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR1.PoliceRankID AS PoliceRankID1,
		PR1.PoliceRankName AS PoliceRankName1,
		PR2.PoliceRankID AS PoliceRankID2,
		PR2.PoliceRankName AS PoliceRankName2,
		PT.PersonTypeCode,
		PT.PersonTypeID,
		PT.PersonTypeName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
		JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

	--PersonClientRoster
	SELECT 
		CR.ClientRosterCode,
		CR.ClientRosterName,
		ES.ExpertStatusID,
		ES.ExpertStatusName,
		PCR.ClientRosterID,
		PCR.ExpertStatusID,
		PCR.PersonClientRosterID,
		PCR.TerminationDate,
		core.FormatDate(PCR.TerminationDate) AS TerminationDateFormatted,
		PCR.TerminationReason
	FROM person.PersonClientRoster PCR
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
			AND PCR.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'PersonCorrespondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEC
		JOIN dropdown.Country C ON C.CountryID = PEC.CountryID
			AND PEC.PersonID = @PersonID
	ORDER BY C.CountryName, C.CountryID

	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonGroupPerson
	SELECT 
		PGP.PersonGroupPersonID,
		PGP.PersonGroupID,
		PGP.CreateDateTime,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateFormatted,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PGP2.PersonID) FROM person.PersonGroupPerson PGP2 WHERE PGP2.PersonGroupID = PG.PersonGroupID) AS UserCount,
		PG.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName
	FROM person.PersonGroupPerson PGP
		JOIN person.PersonGroup PG ON PG.PersonGroupID = PGP.PersonGroupID
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PGP.PersonID = @PersonID

	--PersonIR35
	SELECT
		newID() AS PersonIR35GUID,
		PIR.TaskName,
		PIR.IR35StatusDate,
		PIR.IR35StatusID,
		core.FormatDate(PIR.IR35StatusDate) AS IR35StatusDateFormatted,
		PIR.PersonIR35ID,
		PIR.Issues,
		IRS.IR35StatusName
	FROM person.PersonIR35 PIR
		JOIN dropdown.IR35Status IRS ON IRS.IR35StatusID = PIR.IR35StatusID
			AND PIR.PersonID = @PersonID
	ORDER BY PIR.TaskName, PIR.PersonIR35ID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonUsernameByEmailAddress
EXEC utility.DropObject 'person.GetPersonUsernameByEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.04.07
-- Description:	A stored procedure to return a username from the person.Person table based on an email address
-- ===========================================================================================================
CREATE PROCEDURE person.GetPersonUsernameByEmailAddress

@EmailAddress VARCHAR(320)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.UserName,
		P.EmailAddress
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress

END
GO
--End procedure person.GetPersonUsernameByEmailAddress

--Begin procedure person.GetVettingEmailData
EXEC Utility.DropObject 'person.GetVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data for outgoing vetting e-mail
-- ==========================================================================
CREATE PROCEDURE person.GetVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@SecurityClearanceID INT,
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonSecurityClearanceProcessStep
		(PersonID, SecurityClearanceID, SecurityClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		@SecurityClearanceID,
		(SELECT SCPS.SecurityClearanceProcessStepID FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = 'PREP'),
		getDate(),
		1,
		'Security clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.DiscretionaryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS DiscretionaryReviewDateFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS MandatoryReviewDateFormatted
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		0 AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL,
		NULL

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetVettingEmailData

--Begin procedure person.SaveDAPersonClientRoster
EXEC utility.DropObject 'person.SaveDAPersonClientRoster'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date: 2016.11.01
-- Description: A stored procedure to add data to the person.PersonClientRoster table
-- ==================================================================================
CREATE PROCEDURE person.SaveDAPersonClientRoster

@cJSONData VARCHAR(MAX) 

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonClientRoster
		(PersonID, ClientRosterID)
	SELECT
		P.PersonID,
		CR.ClientRosterID
	FROM OPENJSON(@cJSONData)
	WITH
		(
		DAPersonID INT '$.DAPersonID',
		ClientRosterCode VARCHAR(50) '$.ClientRosterCode'
		) DAPCR
		JOIN Person.Person P ON P.DAPersonID = DAPCR.DAPersonID
		JOIN dropdown.ClientRoster CR on CR.ClientRosterCode = DAPCR.ClientRosterCode
			AND NOT EXISTS	
				(
				SELECT 1
				FROM person.PersonClientRoster PCR
				WHERE PCR.PersonID = P.PersonID
					AND PCR.ClientRosterID = CR.ClientRosterID
				)

END
GO
--End procedure person.SaveDAPersonClientRoster

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDAPersonID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		DAPersonID INT NOT NULL DEFAULT 0,
		DefaultProjectID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		RoleName VARCHAR(50),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsPasswordExpired = CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN 1 ELSE 0 END,
		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDAPersonID = ISNULL(P.DAPersonID, 0),
		@nDefaultProjectID = P.DefaultProjectID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(
			CellPhone,
			CountryCallingCodeID,
			DAPersonID,
			DefaultProjectID,
			EmailAddress,
			FullName,
			IsAccountLockedOut,
			IsActive,
			IsPhoneVerified,
			IsProfileUpdateRequired,
			IsSuperAdministrator,
			IsValidPassword,
			IsValidUserName,
			PersonID,
			RequiredProfileUpdate,
			RoleName,
			UserName
			) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@nDAPersonID,
			@nDefaultProjectID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cRoleName,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT 
		P.*
	FROM @tPerson P

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure personupdate.ApprovePersonUpdateByPersonUpdateID
EXEC utility.DropObject 'personupdate.ApprovePersonUpdateByPersonUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.26
-- Description:	A stored procedure to submit Person, PersonGroupPerson, and PersonProject in a person update for approval
-- ======================================================================================================================
CREATE PROCEDURE personupdate.ApprovePersonUpdateByPersonUpdateID

@PersonUpdateID INT,
@PersonUpdatePersonID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cPersonUpdateTypeCode VARCHAR(50) = (SELECT PUT.PersonUpdateTypeCode FROM personupdate.PersonUpdate PU JOIN dropdown.PersonUpdateType PUT ON PUT.PersonUpdateTypeID = PU.PersonUpdateTypeID AND PU.PersonUpdateID = @PersonUpdateID)
	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tPGPSaveCreateDate TABLE (PersonGroupID INT, PersonID INT, CreateDateTime DATETIME)

	IF (@cPersonUpdateTypeCode <> 'DELETE')
		BEGIN

			IF (SELECT PersonID FROM personupdate.PersonUpdate PU WHERE PU.PersonUpdateID = @PersonUpdateID) = 0
				BEGIN

					INSERT INTO person.Person (
						Title,
						FirstName,
						LastName,
						Suffix,
						UserName,
						EmailAddress,
						ManagerPersonID,
						DefaultProjectID,
						Organization,
						RoleID,
						InvoiceLimit,
						PurchaseOrderLimit
					)
					OUTPUT INSERTED.PersonID INTO @tOutput
					SELECT 
						Title,
						FirstName,
						LastName,
						Suffix,
						UserName,
						EmailAddress,
						ManagerPersonID,
						DefaultProjectID,
						Organization,
						RoleID,
						InvoiceLimit,
						PurchaseOrderLimit
					FROM personupdate.Person
					WHERE PersonUpdateID = @PersonUpdateID

				END
			ELSE
				BEGIN

					UPDATE P
					SET
						P.Title = PUP.Title,
						P.FirstName = PUP.FirstName,
						P.LastName = PUP.LastName,
						P.Suffix = PUP.Suffix,
						P.UserName = PUP.UserName,
						P.EmailAddress = PUP.EmailAddress,
						P.ManagerPersonID = PUP.ManagerPersonID,
						P.DefaultProjectID = PUP.DefaultProjectID,
						P.Organization = PUP.Organization,
						P.RoleID = PUP.RoleID,
						P.InvoiceLimit = PUP.InvoiceLimit,
						P.PurchaseOrderLimit = PUP.PurchaseOrderLimit
					FROM person.Person P
						JOIN personupdate.Person PUP ON PUP.PersonID = P.PersonID
							AND PUP.PersonUpdateID = @PersonUpdateID

					INSERT INTO @tOutput(PersonID)
					SELECT PersonID
					FROM personupdate.Person
					WHERE PersonUpdateID = @PersonUpdateID

				END
			--ENDIF

			DECLARE @nPersonID INT = (SELECT TOP 1 PersonID FROM @tOutput);

			UPDATE personupdate.PersonGroupPerson SET PersonID = @nPersonID WHERE PersonUpdateID = @PersonUpdateID
			UPDATE personupdate.PersonPermissionable SET PersonID = @nPersonID WHERE PersonUpdateID = @PersonUpdateID
			UPDATE personupdate.PersonProject SET PersonID = @nPersonID WHERE PersonUpdateID = @PersonUpdateID


			INSERT INTO @tPGPSaveCreateDate(PersonGroupID, PersonID, CreateDateTime)
			SELECT PersonGroupID, PersonID, CreateDateTime
			FROM person.PersonGroupPerson 
			WHERE PersonID = @nPersonID

			DELETE FROM person.PersonGroupPerson
			WHERE PersonID = @nPersonID

			INSERT INTO person.PersonGroupPerson(PersonGroupID, PersonID)
			SELECT PGP.PersonGroupID, PGP.PersonID
			FROM personupdate.PersonGroupPerson PGP
			WHERE PGP.PersonUpdateID = @PersonUpdateID

			UPDATE PGP
			SET PGP.CreateDateTime = tPGP.CreateDateTime
			FROM @tPGPSaveCreateDate AS tPGP
			JOIN person.PersonGroupPerson AS PGP ON PGP.PersonID = tPGP.PersonID
				AND PGP.PersonGroupID = tPGP.PersonGroupID


			DELETE PP
			FROM person.PersonPermissionable PP
			WHERE PersonID = @PersonUpdatePersonID

			INSERT INTO person.PersonPermissionable(PersonID,PermissionableLineage)
			SELECT PUPP.PersonID,PUPP.PermissionableLineage
			FROM personupdate.PersonPermissionable PUPP
			WHERE PUPP.PersonUpdateID = @PersonUpdateID

			DELETE PP
			FROM person.PersonProject PP
			WHERE PersonID = @PersonUpdatePersonID

			INSERT INTO person.PersonProject(PersonID,ProjectID,IsViewDefault)
			SELECT PUPP.PersonID,PUPP.ProjectID,PUPP.IsViewDefault
			FROM personupdate.PersonProject PUPP
			WHERE PUPP.PersonUpdateID = @PersonUpdateID

		END
	--ENDIF

	SELECT TOP 1 PersonID FROM @tOutput

	COMMIT TRANSACTION

	EXEC eventlog.LogPersonUpdateAction @PersonUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogPersonUpdateAction @PersonUpdateID, 'update', @PersonID, NULL

END
GO
--End procedure personupdate.ApprovePersonUpdateByPersonUpdateID

--Begin procedure personupdate.GetPersonUpdateByPersonUpdateID
EXEC utility.DropObject 'personupdate.GetPersonUpdateByPersonUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.26
-- Description:	A stored procedure to get data from the personupdate.PersonUpdate table
-- ====================================================================================
CREATE PROCEDURE personupdate.GetPersonUpdateByPersonUpdateID

@PersonUpdateID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('PersonUpdate', @PersonUpdateID)

	--PersonUpdate
	SELECT
		PU.PersonUpdateID, 
		PU.PersonID, 
		person.FormatPersonNameByPersonID(PU.PersonID,'FirstLast') AS PersonNameFormatted,
		PU.FirstName,
		PU.LastName,
		PU.UserName,
		PU.PersonUpdateTypeID, 
		PUT.PersonUpdateTypeName,
		PU.RequestDate,
		core.FormatDate(PU.RequestDate) AS RequestDateFormatted,
		PU.RequestedByPersonID, 
		person.FormatPersonNameByPersonID(PU.RequestedByPersonID,'FirstLast') AS RequestedByNameFormatted,
		PU.ActionDate,
		core.FormatDate(PU.ActionDate) AS ActionDateFormatted,
		PU.WorkflowStepNumber,
		PU.Notes,
		IIF((SELECT PE.DAPersonID FROM person.Person PE WHERE PE.PersonID = PU.PersonID) > 0,'Yes','No') AS HasDAAccountYesNoFormat,
		PU.ISDAAccountDecomissioned,
		IIF(PU.ISDAAccountDecomissioned = 1, 'Yes', 'No') AS ISDAAccountDecomissionedYesNoFormat,
		PU.DAAccountDecomissionedDate,
		core.FormatDate(PU.DAAccountDecomissionedDate) AS DAAccountDecomissionedDateFormatted,
		PU.LockOutByPersonID,
		person.FormatPersonNameByPersonID(PU.LockOutByPersonID, 'FirstLast') AS LockOutByPersonNameFormatted,
		PU.LockOutDate,
		core.FormatDate(PU.LockOutDate) AS LockOutDateFormatted,
		PU.FARemoveByPersonID,
		person.FormatPersonNameByPersonID(PU.FARemoveByPersonID, 'FirstLast') AS FARemoveByPersonNameFormatted,
		PU.FARemoveDate,
		core.FormatDate(PU.FARemoveDate) AS FARemoveDateFormatted,
		PU.DeleteByPersonID,
		person.FormatPersonNameByPersonID(PU.DeleteByPersonID, 'FirstLast') AS DeleteByPersonNameFormatted,
		PU.DeleteDate,
		core.FormatDate(PU.DeleteDate) AS DeleteDateFormatted,
		PU.DaysToRemoval
	FROM personupdate.PersonUpdate PU
		JOIN dropdown.PersonUpdateType PUT ON PUT.PersonUpdateTypeID = PU.PersonUpdateTypeID
	WHERE PU.PersonUpdateID = @PersonUpdateID

	--PersonUpdateDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'PersonUpdate'
			AND DE.EntityID = @PersonUpdateID

	--PersonUpdatePerson
	SELECT
		P.PersonUpdatePersonID,
		P.Title,
		P.FirstName,
		P.LastName,
		P.Suffix,
		P.UserName,
		P.EmailAddress,
		P.ManagerPersonID,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		P.DefaultProjectID,
		DP.ProjectName AS DefaultProjectName,
		P.Organization,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		P.RoleID,
		R.RoleName,
		P.InvoiceLimit,
		P.PurchaseOrderLimit

	FROM personupdate.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
		JOIN dropdown.project DP ON DP.ProjectID = P.DefaultProjectID
	WHERE P.PersonUpdateID = @PersonUpdateID

	--PersonGroupPerson
	SELECT 
		PGP.PersonUpdatePersonGroupPersonID,
		PGP.PersonGroupID,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PGP2.PersonID) FROM person.PersonGroupPerson PGP2 WHERE PGP2.PersonGroupID = PG.PersonGroupID) AS UserCount,
		PG.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName
	FROM personupdate.PersonGroupPerson PGP
		JOIN person.PersonGroup PG ON PG.PersonGroupID = PGP.PersonGroupID
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PGP.PersonUpdateID = @PersonUpdateID

	--PersonPermissionable
	SELECT P.PermissionableLineage
		,P.PermissionCode
		,P.PermissionableID
		,PG.PermissionableGroupCode
		,PG.PermissionableGroupName
		,P.ControllerName
		,P.Description
		,P.DisplayOrder
		,P.IsActive
		,P.IsGlobal
		,P.IsSuperAdministrator
		,P.MethodName
		,CASE
			WHEN @PersonUpdateID > 0 AND EXISTS (SELECT 1 FROM personupdate.PersonPermissionable PP WHERE PP.PersonUpdateID = @PersonUpdateID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			ELSE 0
		END AS HasPermissionable
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage

	-- PersonProject
	SELECT
		PP.PersonUpdatePersonProjectID,
		PP.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM personupdate.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonUpdateID = @PersonUpdateID
	ORDER BY P.ProjectName
	
	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'PersonUpdate', @PersonUpdateID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'PersonUpdate', @PersonUpdateID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Person Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Person Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Person Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Person Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'PersonUpdate'
		AND EL.EntityID = @PersonUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure personupdate.GetPersonUpdateByPersonUpdateID

--Begin procedure personupdate.SavePersonPermissionables
EXEC utility.DropObject 'personupdate.SavePersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.10.02
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE personupdate.SavePersonPermissionables

@PersonUpdateID INT, 
@PersonID INT, 
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	DELETE PUPP
	FROM personupdate.PersonPermissionable PUPP
	WHERE PUPP.PersonUpdateID = @PersonUpdateID
		
	INSERT INTO personupdate.PersonPermissionable
		(PersonUpdateID,PersonID,PermissionableLineage)
	SELECT
		@PersonUpdateID,
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	UNION

	SELECT
		@PersonUpdateID,
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE EXISTS
		(
		SELECT 1 
		FROM core.ListToTable(@PermissionableIDList, ',') LTT 
		WHERE CAST(LTT.ListItem AS INT) = P.PermissionableID
			AND CAST(LTT.ListItem AS INT) > 0
		)
		AND NOT EXISTS
			(
			SELECT 1
			FROM personupdate.PersonPermissionable PUPP
			WHERE PUPP.PermissionableLineage = P.PermissionableLineage
				AND PUPP.PersonUpdateID = @PersonUpdateID
			)

END
GO
--End procedure personupdate.SavePersonPermissionables

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
			) AS WorkflowStepCount
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND WS1.WorkflowStepNumber = 1
				AND (
					@ProjectID = 0
					OR W1.ProjectID = @ProjectID
				)
		
		END
	--ENDIF

	ELSE
		BEGIN
	
			IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
				BEGIN
				
				SELECT TOP 1 
					D.WorkflowStepName,
					D.WorkflowStepNumber,
					D.WorkflowStepCount
				FROM 
					(
					SELECT
						EWSGP.WorkflowStepName,
						EWSGP.WorkflowStepNumber,
						workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = @EntityTypeCode
						AND EWSGP.EntityID = @EntityID
						AND EWSGP.IsComplete = 0

					UNION

					SELECT 
						EWSGPG.WorkflowStepName,
						EWSGPG.WorkflowStepNumber,
						workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
					FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
					WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
						AND EWSGPG.EntityID = @EntityID
						AND EWSGPG.IsComplete = 0			
				) AS D
				ORDER BY 2

				END
			--ENDIF
			
			ELSE
				BEGIN

				SELECT
					'Approved' AS WorkflowStepName,
					workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
					workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
				
				END
			--ENDIF
			
		END
	--ENDIF

END
GO
--End procedure workflow.GetEntityWorkflowData

--Begin procedure workflow.GetEntityWorkflowPeople
EXEC utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to return people assigned to an entityy's current workflow step
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
--
-- Author:			Jonathan Burnham
-- Create date:	2019.02.19
-- Description:	refactored to check against a 
-- ==========================================================================================
CREATE PROCEDURE workflow.GetEntityWorkflowPeople

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT,
@ProjectID INT = 0

AS
BEGIN

	DECLARE @tTable TABLE
		(
		WorkflowStepGroupName VARCHAR(1000), 
		WorkflowStepGroupPersonGroupName VARCHAR(1000), 
		PersonGroupID INT,
		IsFinancialApprovalRequired BIT DEFAULT 0,
		PersonID INT,
		FullName VARCHAR(1000),
		EmailAddress VARCHAR(1000),
		IsComplete BIT DEFAULT 0
		)

	IF @EntityID = 0
		BEGIN
		
		INSERT INTO @tTable 
			(WorkflowStepGroupName,WorkflowStepGroupPersonGroupName,PersonGroupID,IsFinancialApprovalRequired,PersonID,FullName,EmailAddress,IsComplete)
		SELECT
			WSG.WorkflowStepGroupName,
			'',
			0,
			WSG.IsFinancialApprovalRequired,
			WSGP.PersonID,
			person.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
				AND (
					@ProjectID = 0 
					OR W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
				)
			JOIN person.Person P ON P.PersonID = WSGP.PersonID

		UNION

		SELECT
			WSG.WorkflowStepGroupName,
			PG.PersonGroupName,
			PG.PersonGroupID,
			WSG.IsFinancialApprovalRequired,
			PGP.PersonID,
			person.FormatPersonnameByPersonID(PGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID
			JOIN person.PersonGroupPerson PGP ON PGP.PersonGroupID = PG.PersonGroupID
			JOIN person.Person P ON P.PersonID = PGP.PersonID
	
		END
	ELSE
		BEGIN
	
		INSERT INTO @tTable 
			(WorkflowStepGroupName,WorkflowStepGroupPersonGroupName,PersonGroupID,IsFinancialApprovalRequired,PersonID,FullName,EmailAddress,IsComplete)
		SELECT
			EWSGP.WorkflowStepGroupName, 
			'',
			0,
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.PersonID,
			person.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber

		UNION

		SELECT
			EWSGPG.WorkflowStepGroupName, 
			PG.PersonGroupName,
			PG.PersonGroupID,
			EWSGPG.IsFinancialApprovalRequired,
			PGP.PersonID,
			person.FormatPersonnameByPersonID(PGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGPG.IsComplete
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
			JOIN person.PersonGroup PG ON PG.PersonGroupID = EWSGPG.PersonGroupID
			JOIN person.PersonGroupPerson PGP ON PGP.PersonGroupID = PG.PersonGroupID
			JOIN person.Person P ON P.PersonID = PGP.PersonID
				AND EWSGPG.EntityTypeCode = @EntityTypeCode
				AND EWSGPG.EntityID = @EntityID
				AND EWSGPG.WorkflowStepNumber = @WorkflowStepNumber

		END
	--ENDIF

	SELECT 
		T.WorkflowStepGroupName,
		T.WorkflowStepGroupPersonGroupName,
		T.PersonGroupID,
		T.IsFinancialApprovalRequired,
		T.PersonID,
		T.FullName,
		T.EmailAddress,
		T.IsComplete
	FROM @tTable T
	ORDER BY T.WorkflowStepGroupName, T.WorkflowStepGroupPersonGroupName, T.FullName
	
END
GO
--End procedure workflow.GetEntityWorkflowPeople

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.ProjectID,
		dropdown.GetProjectNameByProjectID(W.ProjectID) AS ProjectName,
		W.WorkflowID,	
		W.WorkflowName,
		W.WorkflowCompleteStatusName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT 
		WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT 
		WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepStatusName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.IsFinancialApprovalRequired,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		person.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGPG.PersonGroupID,
		WSGPG.WorkflowStepGroupID,
		PG.PersonGroupName
	FROM workflow.WorkflowStepGroupPersonGroup WSGPG
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
		JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

	INSERT INTO workflow.EntityWorkflowStepGroupPersonGroup
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonGroupID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGPG.PersonGroupID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

END
GO
--End procedure workflow.InitializeEntityWorkflow

--Begin procedure workflow.ResetEntityWorkflowStepGroupPerson
EXEC utility.DropObject 'workflow.ResetEntityWorkflowStepGroupPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.06
-- Description:	A stored procedure to update already crated workflows with the current personnel assignments
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =========================================================================================================
CREATE PROCEDURE workflow.ResetEntityWorkflowStepGroupPerson

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tEntityWorkflowStepGroupPerson TABLE
		(
		EntityTypeCode VARCHAR(50),
		EntityID INT,
		WorkflowID INT,
		WorkflowStepID INT,
		WorkflowStepGroupID INT,
		WorkflowName VARCHAR(250),
		WorkflowStepNumber INT,
		WorkflowStepName VARCHAR(250),
		WorkflowStepGroupName VARCHAR(250),
		IsFinancialApprovalRequired BIT,
		PersonID INT,
		IsComplete BIT
		)	
		
	;
	WITH WSGP AS
		(
		SELECT
			W.WorkflowID,
			WS.WorkflowStepID, 
			WSG.WorkflowStepGroupID, 
			WSGP.PersonID
		FROM workflow.WorkflowStepGroupPerson WSGP 
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.WorkflowID = @WorkflowID
				AND (
					W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
					OR W.EntityTypeCode IN ('TrendReportAggregator','PersonUpdate')
				)
		)
	
	INSERT INTO @tEntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID, IsComplete)
	SELECT
		D.EntityTypeCode, 
		D.EntityID, 
		D.WorkflowID, 
		D.WorkflowStepID, 
		D.WorkflowStepGroupID, 
		D.WorkflowName, 
		D.WorkflowStepNumber, 
		D.WorkflowStepName, 
		D.WorkflowStepGroupName, 
		D.IsFinancialApprovalRequired,
		E.PersonID,
		D.IsComplete
	FROM
		(
		SELECT
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.WorkflowID = @WorkflowID
		GROUP BY
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.IsComplete
		) D
		CROSS APPLY
			(
			SELECT
				WSGP.PersonID
			FROM WSGP
			WHERE WSGP.WorkflowID = D.WorkflowID
				AND WSGP.WorkflowStepID = D.WorkflowStepID
				AND WSGP.WorkflowStepGroupID = D.WorkflowStepGroupID
			) E
	ORDER BY 
		D.EntityID,
		D.WorkflowID,
		D.WorkflowStepID,
		D.WorkflowStepGroupID, 
		D.IsComplete

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.WorkflowID = @WorkflowID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID, IsComplete)
	SELECT
		T.EntityTypeCode, 
		T.EntityID, 
		T.WorkflowID, 
		T.WorkflowStepID, 
		T.WorkflowStepGroupID, 
		T.WorkflowName, 
		T.WorkflowStepNumber, 
		T.WorkflowStepName, 
		T.WorkflowStepGroupName, 
		T.IsFinancialApprovalRequired,
		T.PersonID, 
		T.IsComplete
	FROM @tEntityWorkflowStepGroupPerson T

END
GO
--End procedure workflow.ResetEntityWorkflowStepGroupPerson