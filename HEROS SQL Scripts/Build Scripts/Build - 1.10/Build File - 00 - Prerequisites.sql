USE HermisCloud
GO

--Begin function utility.HasColumn
EXEC utility.DropObject 'utility.HasColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.09
-- Description:	A function to return a bit indicating if a column exists in a table
-- ================================================================================

CREATE FUNCTION utility.HasColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250)
)

RETURNS BIT

AS
BEGIN

	DECLARE @bHasColumn BIT = 0

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		SET @bHasColumn = 1
	--ENDIF

	RETURN @bHasColumn

END
GO
--End function utility.HasColumn

--Begin schemas
EXEC utility.AddSchema 'hrms'
EXEC utility.AddSchema 'personupdate'
GO
--End schemas
