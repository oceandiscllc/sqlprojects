USE HermisCloud
GO

--Begin function person.CanHaveAccess
EXEC utility.DropObject 'person.CanHaveAccess'
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A function to determine if a PersonID has access to a specific entity type / entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
--
-- Author:			Jonathan Burnham
-- Create date:	2019.02.02
-- Description:	refactored to support the hrms workflow
-- ===================================================================================================

CREATE FUNCTION person.CanHaveAccess
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bCanHaveAccess BIT = 0

	IF @EntityTypeCode IN ('ActivityReport', 'EquipmentOrder', 'Feedback', 'PersonUpdate', 'SpotReport', 'TrendReport', 'Vacancy', 'VacancyApplication')
		BEGIN

		SET @bCanHaveAccess = workflow.IsWorkflowComplete(@EntityTypeCode, @EntityID)

		IF @bCanHaveAccess = 0
			SET @bCanHaveAccess = workflow.IsPersonInCurrentWorkflowStep(@PersonID, @EntityTypeCode, @EntityID, 0)
		--ENDIF

		END
	--ENDIF

	IF @EntityTypeCode = 'VacancyApplication' AND @bCanHaveAccess = 0
		BEGIN

		SELECT @bCanHaveAccess = CASE WHEN A.SubmittedDateTime IS NULL THEN 1 ELSE 0 END
		FROM hrms.Application A
		WHERE A.ApplicationID = @EntityID
			AND A.PersonID = @PersonID

		END
	--ENDIF

	RETURN ISNULL(@bCanHaveAccess, 0)

END
GO
--End function person.CanHaveAccess

--Begin function person.IsProfileUpdateRequired
EXEC utility.DropObject 'person.IsProfileUpdateRequired'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.24
-- Description:	A function to return data from the person.Person table
-- ===================================================================
CREATE FUNCTION person.IsProfileUpdateRequired
(
@PersonID INT,
@IsTwoFactorEnabled BIT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsProfileUpdateRequired BIT

	SELECT @bIsProfileUpdateRequired = 
		CASE
			WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
				OR core.NullIfEmpty(P.FirstName) IS NULL
				OR core.NullIfEmpty(P.LastName) IS NULL
				OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				OR P.HasAcceptedTerms = 0
				OR (@IsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
				OR (@IsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
				OR (@IsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
			THEN 1
			ELSE 0
		END

	FROM person.Person P
	WHERE P.PersonID = @PersonID

	RETURN ISNULL(@bIsProfileUpdateRequired, 1)

END
GO
--End function project.IsProfileUpdateRequired

--Begin function utility.HasColumn
EXEC utility.DropObject 'utility.HasColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.09
-- Description:	A function to return a bit indicating if a column exists in a table
-- ================================================================================

CREATE FUNCTION utility.HasColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250)
)

RETURNS BIT

AS
BEGIN

	DECLARE @bHasColumn BIT = 0

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		SET @bHasColumn = 1
	--ENDIF

	RETURN @bHasColumn

END
GO
--End function utility.HasColumn

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT
	DECLARE @tTable TABLE (WorkflowStepNumber INT)

	INSERT INTO @tTable 
		(WorkflowStepNumber)
 	SELECT 
 		EWSGP.WorkflowStepNumber
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
		JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID

	UNION

	SELECT 
		EWSGPG.WorkflowStepNumber
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID

	SELECT 
		@nWorkflowStepCount = MAX(T.WorkflowStepNumber) 
	FROM @tTable T
	
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @tTable TABLE(WorkflowStepNumber INT, WorkflowStepGroupID INT)

	INSERT INTO @tTable 
		(WorkflowStepNumber,WorkflowStepGroupID)
 	SELECT 
 		EWSGP.WorkflowStepNumber, 
 		EWSGP.WorkflowStepGroupID
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
		JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.IsComplete = 0 

	UNION

	SELECT 
		EWSGPG.WorkflowStepNumber, 
		EWSGPG.WorkflowStepGroupID
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID
		AND EWSGPG.IsComplete = 0 

	SELECT TOP 1 
		@nWorkflowStepNumber = T.WorkflowStepNumber 
	FROM @tTable T 
	ORDER BY T.WorkflowStepNumber, T.WorkflowStepGroupID

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber

END
GO
--End function workflow.GetWorkflowStepNumber

--Begin function workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID'
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.25
-- Description:	A function to get the status of an entity under workflow
-- =====================================================================

CREATE FUNCTION workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@DefaultWorkflowStepStatusName VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cWorkflowStepStatusName VARCHAR(50)

	IF NOT EXISTS 
		(
		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID

		UNION

		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID
		)
		SET @cWorkflowStepStatusName = @DefaultWorkflowStepStatusName
	ELSE IF NOT EXISTS 
		(
		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID 
			AND EWSGP.IsComplete = 0

		UNION

		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID 
			AND EWSGPG.IsComplete = 0
		)
		BEGIN

		SELECT TOP 1 @cWorkflowStepStatusName = D.WorkflowCompleteStatusName
		FROM
			(
			SELECT TOP 1
				EWSGP.WorkflowStepID,
				ISNULL(W.WorkflowCompleteStatusName, 'Workflow Complete') AS WorkflowCompleteStatusName
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
			GROUP BY EWSGP.WorkflowStepID, W.WorkflowCompleteStatusName

			UNION

			SELECT TOP 1
				EWSGPG.WorkflowStepID,
				ISNULL(W.WorkflowCompleteStatusName, 'Workflow Complete') AS WorkflowCompleteStatusName
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGPG.WorkflowID
					AND EWSGPG.EntityTypeCode = @EntityTypeCode
					AND EWSGPG.EntityID = @EntityID
			GROUP BY EWSGPG.WorkflowStepID, W.WorkflowCompleteStatusName

			ORDER BY 1 DESC
			) D
		ORDER BY D.WorkflowStepID

		END
	ELSE
		BEGIN

		SELECT TOP 1 @cWorkflowStepStatusName = D.WorkflowStepStatusName
		FROM
			(
			SELECT TOP 1
				EWSGP.WorkflowStepID,
				ISNULL(WS.WorkflowStepStatusName, 'No Workflow Setp Status Name Assigned') AS WorkflowStepStatusName
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWSGP.WorkflowStepID
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			GROUP BY EWSGP.WorkflowStepID, WS.WorkflowStepStatusName

			UNION

			SELECT TOP 1
				EWSGPG.WorkflowStepID,
				ISNULL(WS.WorkflowStepStatusName, 'No Workflow Setp Status Name Assigned') AS WorkflowStepStatusName
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGPG.WorkflowID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWSGPG.WorkflowStepID
					AND EWSGPG.EntityTypeCode = @EntityTypeCode
					AND EWSGPG.EntityID = @EntityID
					AND EWSGPG.IsComplete = 0
			GROUP BY EWSGPG.WorkflowStepID, WS.WorkflowStepStatusName

			ORDER BY 1
			) D
		ORDER BY D.WorkflowStepID

		END
	--ENDIF

	RETURN ISNULL(@cWorkflowStepStatusName, @DefaultWorkflowStepStatusName)

END
GO
--End function workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID

--Begin function workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to indicate if a personid exists in the current step of an entity's workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =====================================================================================================

CREATE FUNCTION workflow.IsPersonInCurrentWorkflowStep
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflowStep BIT = 0

	IF (@EntityID = 0 AND EXISTS 
		(
		SELECT 1
		FROM workflow.WorkflowStepGroupPerson WSGP
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND WSGP.PersonID = @PersonID
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback'))
				AND W.IsActive = 1
				AND (
					@ProjectID = 0 
						OR (
							W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
							AND w.ProjectID = @ProjectID
						)
				)

		UNION

		SELECT 1
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = WSGPG.PersonGroupID
					)
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback'))
				AND W.IsActive = 1
		)) OR EXISTS
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE (EWSGP.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND EWSGP.EntityTypeCode LIKE '%Feedback'))
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EWSGP.PersonID = @PersonID

			UNION

			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
			WHERE (EWSGPG.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND EWSGPG.EntityTypeCode LIKE '%Feedback'))
				AND EWSGPG.EntityID = @EntityID
				AND EWSGPG.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = EWSGPG.PersonGroupID
					)
			)

		SET @bIsPersonInCurrentWorkflowStep = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflowStep

END
GO
--End function workflow.IsPersonInCurrentWorkflowStep

--Begin function workflow.IsWorkflowComplete
EXEC utility.DropObject 'workflow.IsWorkflowComplete'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.25
-- Description:	A function to return a bit indicating if an entity has completed a workflow
-- ========================================================================================

CREATE FUNCTION workflow.IsWorkflowComplete
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsWorkflowComplete BIT = 0

	IF EXISTS 
		(
		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID

		UNION

		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID
		) 	
		AND NOT EXISTS 
			(
			SELECT 1 
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
				AND EWSGP.EntityID = @EntityID 
				AND EWSGP.IsComplete = 0

			UNION

			SELECT 1 
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
			WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
				AND EWSGPG.EntityID = @EntityID 
				AND EWSGPG.IsComplete = 0
			)

		SET @bIsWorkflowComplete = 1
	--ENDIF

	RETURN @bIsWorkflowComplete

END
GO
--End function workflow.IsWorkflowComplete