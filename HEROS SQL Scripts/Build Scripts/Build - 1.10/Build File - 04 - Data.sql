USE HermisCloud
GO

DELETE FROM eventlog.EventLog WHERE EntityTypeCode LIKE '%Feedback' OR EntityTypeCode = 'Application'
DELETE FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityTypeCode LIKE '%Feedback'
DELETE FROM workflow.EntityWorkflowStepGroupPersonGroup WHERE EntityTypeCode LIKE '%Feedback'

--Begin table core.EmailTemplate
DELETE ET 
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode LIKE '%Feedback%'
GO

DELETE ETF
FROM core.EmailTemplateField ETF
WHERE NOT EXISTS 
	(
	SELECT 1
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = ETF.EntityTypeCode
	)
GO

EXEC core.EmailTemplateAddUpdate 
	'Feedback', 
	'Reject', 
	'Sent when feedback has been rejected.', 
	'Feedback Has Been Disapproved', 
	'<p>Roster member feedback was disapproved:</p><br/><p><strong>Title: </strong>[[FeedbackNameLink]]</p><p><strong>Roster Member: </strong>[[CSGMemberPersonNameFormatted]]</p><p><strong>Dates: </strong>[[DateRangeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the feedback workflow. Please log in and click the link above to review.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[FeedbackNameLink]]":"Link back to feedback record."},{"[[DateRangeFormatted]]":"Formatted date range of project."},{"[[CSGMemberPersonNameFormatted]]":"Name of the CSG member"},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Feedback', 
	'Submit', 
	'Sent when feedback has been submitted for approval.', 
	'Feedback Was Submitted For Your Review', 
	'<p>Roster member feedback has been submitted for your review:</p><br/><p><strong>Title: </strong>[[FeedbackNameLink]]</p><p><strong>Roster Member: </strong>[[CSGMemberPersonNameFormatted]]</p><p><strong>Dates: </strong>[[DateRangeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the feedback workflow. Please log in and click the link above to review.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[FeedbackNameLink]]":"Link back to feedback record."},{"[[DateRangeFormatted]]":"Formatted date range of project."},{"[[CSGMemberPersonNameFormatted]]":"Name of the CSG member"},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Feedback', 
	'Release', 
	'Sent when feedback has been approved.', 
	'Feedback Has Been Approved', 
	'<p>Roster member feedback was approved:</p><br/><p><strong>Title: </strong>[[FeedbackNameLink]]</p><p><strong>Roster Member: </strong>[[CSGMemberPersonNameFormatted]]</p><p><strong>Dates: </strong>[[DateRangeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the feedback workflow. Please log in and click the link above to review.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[FeedbackNameLink]]":"Link back to feedback record."},{"[[DateRangeFormatted]]":"Formatted date range of project."},{"[[CSGMemberPersonNameFormatted]]":"Name of the CSG member"},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'SURosterTerminationFinal', 
	'Sent to notifiy the SU roster management team that a member is about to be permanently deleted from the SU roster', 
	'Data Deletion', 
	'<p>Data regarding the following Roster Member will be deleted on [[DeletionDateFormatted]].<br /><br />Roster member: [[RosterPersonNameFormatted]]<br /><br />Please do not reply to this email as it is generated automatically by the HERMIS system.<br /><br />HERMIS Support Team<br /><br />If you need support, please email [[FeedBackMailTo]].</p>',
	'[{"[[DeletionDateFormatted]]":"Roster member deletion date"},{"[[RosterPersonNameFormatted]]":"The name of the person being deleted"},{"[[FeedBackMailTo]]":"Support email address"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'SURosterTerminationInitial', 
	'Sent to notifiy a user that he has been terminated from the SU roster', 
	'CSG Roster - Profile Update', 
	'<p>Dear [[ToFullName]],<br /><br />We write to confirm that your membership of the CGS Roster has terminated by reason of [[TerminationReason]].<br /><br />We will continue to hold your data in accordance with legal requirements.&nbsp;&nbsp;You can contact us at [[SURosterContactEmailAddress]]if you would like to discuss this.<br /><br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br />The SU Recruitment Team<br />[[SURosterContactEmailAddress]]</p>',
	'[{"[[TerminationReason]]":"Roster termination reason"},{"[[SURosterContactEmailAddress]]":"SU contact email address"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vacancy', 
	'DecrementWorkflow', 
	'Sent when a vacancy submission has been rejected.', 
	'A Vacancy Has Been Disapproved', 
	'<p>A previously submitted vacancy has been disapproved</p><br/><p><strong>Name: </strong>[[VacancyNameLink]]</p><p><strong>Opening Date: </strong>[[OpenDateTimeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the vacancy workflow. Please log in and click the link above to review this vacancy.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[VacancyNameLink]]":"Link back to vacancy record."},{"[[OpenDateTimeFormatted]]":"Formatted date of proposed vacancy opening."},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vacancy', 
	'IncrementWorkflow', 
	'Sent when a vacancy has been submitted for approval.', 
	'A Vacancy Was Submitted For Your Review', 
	'<p>A vacancy has been submitted for your review:</p><br/><p><strong>Name: </strong>[[VacancyNameLink]]</p><p><strong>Opening Date: </strong>[[OpenDateTimeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the vacancy workflow. Please log in and click the link above to review this vacancy.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[VacancyNameLink]]":"Link back to vacancy record."},{"[[OpenDateTimeFormatted]]":"Formatted date of proposed vacancy opening."},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vacancy', 
	'Release', 
	'Sent when a vacancy submission has been approved.', 
	'A Vacancy Has Been Approved', 
	'<p>A previously submitted vacancy has been approved</p><br/><p><strong>Name: </strong>[[VacancyNameLink]]</p><p><strong>Opening Date: </strong>[[OpenDateTimeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the vacancy workflow. Please log in and click the link above to review this vacancy.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[VacancyNameLink]]":"Link back to vacancy record."},{"[[OpenDateTimeFormatted]]":"Formatted date of proposed vacancy opening."},{"[[Comments]]":"Workflow comments."}]'
GO

UPDATE ET
SET ET.WorkflowActionCode = 
	CASE 
		WHEN ET.EntityTypeCode = 'Feedback'
		THEN 
			CASE
				WHEN ET.EmailTemplateCode = 'Reject'
				THEN 'DecrementWorkflow'
				WHEN ET.EmailTemplateCode = 'Submit'
				THEN 'IncrementWorkflow'
				ELSE ET.EmailTemplateCode
			END
		WHEN ET.EntityTypeCode = 'Vacancy'
		THEN ET.EmailTemplateCode
		ELSE ET.WorkflowActionCode
	END
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode IN ('Feedback', 'Vacancy')
GO
--End table core.EmailTemplate

--Begin table core.EntityType
DELETE ET 
FROM core.EntityType ET
WHERE ET.EntityTypeCode IN ('ExternalFeedback', 'InternalFeedback')
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Application', 
	@EntityTypeName = 'Application', 
	@EntityTypeNamePlural = 'Applications',
	@HasWorkflow = 0,
	@SchemaName = 'hrms', 
	@TableName = 'Application', 
	@PrimaryKeyFieldName = 'ApplicationID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Competency', 
	@EntityTypeName = 'Competency', 
	@EntityTypeNamePlural = 'Competencies',
	@HasWorkflow = 0,
	@SchemaName = 'hrms', 
	@TableName = 'Competency', 
	@PrimaryKeyFieldName = 'CompetencyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Feedback', 
	@EntityTypeName = 'Feedback', 
	@EntityTypeNamePlural = 'Feedback',
	@HasWorkflow = 0,
	@SchemaName = 'person', 
	@TableName = 'Feedback', 
	@PrimaryKeyFieldName = 'FeedbackID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonUpdate', 
	@EntityTypeName = 'Person Update', 
	@EntityTypeNamePlural = 'PersonUpdates',
	@HasWorkflow = 1,
	@SchemaName = 'personupdate', 
	@TableName = 'PersonUpdate', 
	@PrimaryKeyFieldName = 'PersonUpdateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Vacancy', 
	@EntityTypeName = 'Vacancy', 
	@EntityTypeNamePlural = 'Vacancies',
	@HasWorkflow = 1,
	@SchemaName = 'hrms', 
	@TableName = 'Vacancy', 
	@PrimaryKeyFieldName = 'VacancyID'
GO
--End table core.EntityTypeAddUpdate

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@BeforeMenuItemCode = 'ConsultantList',
	@NewMenuItemCode = 'Vacancy',
	@NewMenuItemLink = '/vacancy/list',
	@NewMenuItemText = 'Recruitment to Roster',
	@PermissionableLineageList = 'Vacancy.List',
	@ParentMenuItemCode = 'HumanResources'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonGroup',
	@NewMenuItemCode = 'PersonUpdate',
	@NewMenuItemLink = '/personupdate/list',
	@NewMenuItemText = 'User Management',
	@PermissionableLineageList = 'personupdate.list',
	@ParentMenuItemCode = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonUpdate',
	@NewMenuItemCode = 'Competency',
	@NewMenuItemLink = '/competency/list',
	@NewMenuItemText = 'Competencies',
	@PermissionableLineageList = 'competency.list',
	@ParentMenuItemCode = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Training',
	@NewMenuItemCode = 'FeedbackList',
	@NewMenuItemLink = '/feedback/list',
	@NewMenuItemText = 'Member Feedback',
	@ParentMenuItemCode = 'HumanResources',
	@PermissionableLineageList = 'Feedback.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PendingEmailList',
	@NewMenuItemLink = '/pendingemail/list',
	@NewMenuItemText = 'Mail Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PendingEmail.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Competency'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'HumanResources'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'PersonUpdate'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Vacancy'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
DELETE FROM core.SystemSetup WHERE SystemSetupKey IN ('SURosterTerminationEmialAddressTo', 'HumanitarianRosterTerminationEmialAddressTo')
GO

EXEC core.SystemSetupAddUpdate 'ApplicantSiteURL', '', 'https://applicant.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SURosterContactEmailAddress', 'The email address list to use to contact te roster management team', 'jcole@skotkonung.com'
EXEC core.SystemSetupAddUpdate 'SURosterTerminationEmailAddressToFinal', 'The email address list used when the final roster member termination email is sent', 'jcole@skotkonung.com'
EXEC core.SystemSetupAddUpdate 'SURosterTerminationEmailAddressToInitial', 'The email address list used when the initial roster member termination email is sent', 'jcole@skotkonung.com'
GO
--End table core.SystemSetup

--Begin table dropdown.AgeRange
TRUNCATE TABLE dropdown.AgeRange
GO

EXEC utility.InsertIdentityValue 'dropdown.AgeRange', 'AgeRangeID', 0
GO

INSERT INTO dropdown.AgeRange 
	(AgeRangeName, AgeRangeCode, DisplayOrder)
VALUES
	('18-24', '18-24', 1),
	('25-34', '25-34', 2),
	('35-44', '35-44', 3),
	('45-54', '45-54', 4),
	('55-64', '55-64', 5),
	('65-74', '65-74', 6),
	('74+', '74+', 7)
GO
--End table dropdown.AgeRange

--Begin table dropdown.ApplicantSource
TRUNCATE TABLE dropdown.ApplicantSource
GO

EXEC utility.InsertIdentityValue 'dropdown.ApplicantSource', 'ApplicantSourceID', 0
GO

INSERT INTO dropdown.ApplicantSource 
	(ApplicantSourceName, ApplicantSourceCode, HasOther, DisplayOrder)
VALUES
	('Online Advertisement (state website below)', 'OnlineAd', 1, 1),
	('Print Advertisement (state publication name below)', 'PrintAd', 1, 2),
	('Recruitment Fair (state event name below)', 'JobFair', 1, 3),
	('Through a colleague or contact (state their organisation/place of work below)', 'Network', 1, 4),
	('I am a former member of the UK CSG', 'PriorMember', 0, 5),
	('Other (Please State)', 'Other', 1, 6)

GO
--End table dropdown.ApplicantSource

--Begin table dropdown.CompetencyType
TRUNCATE TABLE dropdown.CompetencyType
GO

EXEC utility.InsertIdentityValue 'dropdown.CompetencyType', 'CompetencyTypeID', 0
GO

INSERT INTO dropdown.CompetencyType 
	(CompetencyTypeName, CompetencyTypeCode, DisplayOrder)
VALUES
	('Competency', 'Competency', 1),
	('Technical Capability', 'TechnicalCapability', 2)
GO
--End table dropdown.CompetencyType

--Begin table dropdown.CSGMemberPerformanceRating
TRUNCATE TABLE dropdown.CSGMemberPerformanceRating
GO

EXEC utility.InsertIdentityValue 'dropdown.CSGMemberPerformanceRating', 'CSGMemberPerformanceRatingID', 0
GO

INSERT INTO dropdown.CSGMemberPerformanceRating 
	(CSGMemberPerformanceRatingName, CSGMemberPerformanceRatingCode, DisplayOrder)
VALUES
	('Very Poor', '1', 1),
	('Poor', '2', 2),
	('Fair', '3', 3),
	('Good', '4', 4),
	('Excellent', '5', 5)
GO
--End table dropdown.CSGMemberPerformanceRating

--Begin table dropdown.Ethnicity
TRUNCATE TABLE dropdown.Ethnicity
GO

EXEC utility.InsertIdentityValue 'dropdown.Ethnicity', 'EthnicityID', 0
GO

INSERT INTO dropdown.Ethnicity 
	(EthnicityName, EthnicityCategoryName, HasOther, EthnicityCategorDisplayOrder, EthnicityDisplayOrder)
VALUES
	('English', 'White', 0, 1, 1),
	('Welsh', 'White', 0, 1, 2),
	('Scottish', 'White', 0, 1, 3),
	('Northern Irish', 'White', 0, 1, 4),
	('British', 'White', 0, 1, 5),
	('Irish', 'White', 0, 1, 6),
	('Gypsy or Irish Traveller', 'White', 0, 1, 7),
	('Any other White background (please describe)', 'White', 1, 1, 8),
	('White and Black Caribbean', 'Mixed/Multiple Ethnic Groups', 0, 2, 9),
	('White and Black African', 'Mixed/Multiple Ethnic Groups', 0, 2, 10),
	('White and Asian', 'Mixed/Multiple Ethnic Groups', 0, 2, 11),
	('Any other Mixed/Multiple Ethnic background (please describe)', 'Mixed/Multiple Ethnic Groups', 1, 2, 12),
	('Indian', 'Asian/Asian British', 0, 3, 13),
	('Pakistani', 'Asian/Asian British', 0, 3, 14),
	('Bangladeshi', 'Asian/Asian British', 0, 3, 15),
	('Chinese', 'Asian/Asian British', 0, 3, 16),
	('Any other Asian/Asian British background (please describe)', 'Asian/Asian British', 1, 3, 17),
	('African', 'Black African / Caribbean / Black British', 0, 4, 18),
	('Caribbean', 'Black African / Caribbean / Black British', 0, 4, 19),
	('Any other Black African / Caribbean / Black British background (please describe)', 'Black African / Caribbean / Black British', 1, 4, 20),
	('Arab', 'Other Ethnic Groups', 0, 5, 21),
	('Any other Ethnic Group background (Please describe)', 'Other Ethnic Groups', 1, 5, 22)
GO
--End table dropdown.Ethnicity

--Begin table dropdown.FeedbackQuestion
TRUNCATE TABLE dropdown.FeedbackQuestion
GO

EXEC utility.InsertIdentityValue 'dropdown.FeedbackQuestion', 'FeedbackQuestionID', 0
GO

INSERT INTO dropdown.FeedbackQuestion 
	(FeedbackQuestionName, FeedbackQuestionCode, DisplayOrder, IsExternal, IsInternal)
VALUES
	('SU support, advice, or analysis was relevant to our needs', 'SUPerformanceRatingID1', 1, 1, 0),
	('SU support, advice, or analysis was high quality', 'SUPerformanceRatingID2', 2, 1, 0),
	('I would recommend SU to other HMG colleagues', 'SUPerformanceRatingID3', 3, 1, 0),
	('What was the result of having SU support, advice, or analysis? (Please provide examples)', 'Question4', 4, 1, 0),
	('What could SU do to improve its support to similar taskings in future?', 'Question5', 5, 1, 0),
	('CSG Member Performance Rating', 'CSGMemberPerformanceRatingID', 6, 0, 1),
	('Please explain why you gave the rating above and provide feedback on the CSG member''s performance:', 'Question7', 7, 0, 1)
GO
--End table dropdown.FeedbackQuestion

--Begin table dropdown.InterviewOutcome
TRUNCATE TABLE dropdown.InterviewOutcome
GO

EXEC utility.InsertIdentityValue 'dropdown.InterviewOutcome', 'InterviewOutcomeID', 0
GO

INSERT INTO dropdown.InterviewOutcome 
	(InterviewOutcomeName, InterviewOutcomeCode, DisplayOrder)
VALUES
	('Pass', 'Pass', 1),
	('Fail', 'Fail', 2)
GO
--End table dropdown.InterviewOutcome

--Begin table dropdown.PersonType
TRUNCATE TABLE dropdown.PersonType
GO

EXEC utility.InsertIdentityValue 'dropdown.PersonType', 'PersonTypeID', 0
GO

INSERT INTO dropdown.PersonType 
	(PersonTypeName, PersonTypeCode, DisplayOrder)
VALUES
	('Serving Civil Servant', 'CivilServant', 1),
	('Serving Police', 'Police', 2)
GO

UPDATE PT
SET 
	PT.DisplayOrder = 99,
	PT.PersonTypeCode = 'Other',
	PT.PersonTypeName = 'Other'
FROM dropdown.PersonType PT
WHERE PT.PersonTypeID = 0
GO
--End table dropdown.PersonType

--Begin table dropdown.SuitabilityRating
TRUNCATE TABLE dropdown.SuitabilityRating
GO

EXEC utility.InsertIdentityValue 'dropdown.SuitabilityRating', 'SuitabilityRatingID', 0
GO

INSERT INTO dropdown.SuitabilityRating 
	(SuitabilityRatingName, SuitabilityRatingCode, DisplayOrder)
VALUES
	('Competency Met', 'Met', 1),
	('Competency partialy met', 'PartialMet', 2),
	('Competency not met', 'NotMet', 3)
GO
--End table dropdown.SuitabilityRating

--Begin table dropdown.SUPerformanceRating
TRUNCATE TABLE dropdown.SUPerformanceRating
GO

EXEC utility.InsertIdentityValue 'dropdown.SUPerformanceRating', 'SUPerformanceRatingID', 0
GO

INSERT INTO dropdown.SUPerformanceRating 
	(SUPerformanceRatingName, SUPerformanceRatingCode, DisplayOrder)
VALUES
	('Strongly Disagree', '1', 1),
	('Disagree', '2', 2),
	('Neutral', '3', 3),
	('Agree', '4', 4),
	('Strongly Agree', '5', 5)
GO
--End table dropdown.SUPerformanceRating

--Begin table dropdown.PersonUpdateType
TRUNCATE TABLE dropdown.PersonUpdateType
GO

EXEC utility.InsertIdentityValue 'dropdown.PersonUpdateType', 'PersonUpdateTypeID', 0
GO

INSERT INTO dropdown.PersonUpdateType 
	(PersonUpdateTypeName, PersonUpdateTypeCode, DisplayOrder)
VALUES
	('New User', 'NEW', 1),
	('Modify User', 'EDIT', 2),
	('Delete User', 'DELETE', 3)

GO
--End table dropdown.PersonUpdateType

--Begin table dropdown.VacancyStatus
TRUNCATE TABLE dropdown.VacancyStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.VacancyStatus', 'VacancyStatusID', 0
GO

INSERT INTO dropdown.VacancyStatus 
	(VacancyStatusName, VacancyStatusCode, DisplayOrder)
VALUES
	('Preparing', 'Preparing', 1),
	('Open', 'Open', 2),
	('Paused', 'Paused', 3),
	('Closed', 'Closed', 4)
GO
--End table dropdown.VacancyStatus

--Begin table dropdown.VacancyType
TRUNCATE TABLE dropdown.VacancyType
GO

EXEC utility.InsertIdentityValue 'dropdown.VacancyType', 'VacancyTypeID', 0
GO

INSERT INTO dropdown.VacancyType 
	(VacancyTypeName, VacancyTypeCode, DisplayOrder)
VALUES
	('Roster Member Application', 'RosterMemberApp', 1)
GO
--End table dropdown.VacancyType

--Begin table permissionable.Permissionable
DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('ExternalFeedback', 'InternalFeedback')
	OR P.PermissionableLineage LIKE '%AddInternalFeedback'
	OR P.PermissionableLineage LIKE '%ShowEngagements'
	OR P.PermissionableLineage LIKE '%ShowPermissionables'
GO

DELETE T FROM core.MenuItemPermissionableLineage T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM permissionable.Permissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM permissionable.PermissionableTemplatePermissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM person.PersonPermissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM personupdate.PersonPermissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Competency', 
	@DESCRIPTION='Add and update competencies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Competency.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Competency', 
	@DESCRIPTION='View the list of competencies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Competency.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Competency', 
	@DESCRIPTION='View competencies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Competency.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='Add / edit a roster member feedback item', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='View the list of roster member feedback items', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='Add an roster member feedback item', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.List.AddFeedback', 
	@PERMISSIONCODE='AddFeedback';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='View a roster member feedback item', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonUpdate', 
	@DESCRIPTION='Add / edit a PersonUpdate', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonUpdate.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PendingEmail', 
	@DESCRIPTION='View the sent and pending mail log', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PendingEmail.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonUpdate', 
	@DESCRIPTION='View the list of PersonUpdates', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonUpdate.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonUpdate', 
	@DESCRIPTION='View a PersonUpdate', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonUpdate.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='Add / edit a Vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='View the list of Vacancies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='Manage applications', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ManageApplication', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.ManageApplication', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='View a Vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='VacancyApplication', 
	@DESCRIPTION='Add / edit an application', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='VacancyApplication.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='VacancyApplication', 
	@DESCRIPTION='View the list of applications', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='VacancyApplication.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='VacancyApplication', 
	@DESCRIPTION='View an application', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='VacancyApplication.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the account tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the activitystreams tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the address tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the application tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the clearances tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the engagement tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the expertise tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the permissions tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the personal tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the qualification tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab'; 
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the IR35 data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyIR35', @PERMISSIONCODE='ModifyIR35'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the operational training data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyOperationalTraining', @PERMISSIONCODE='ModifyOperationalTraining'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the firearms training data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyFirearmsTraining', @PERMISSIONCODE='ModifyFirearmsTraining'; 
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the account tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the activitystreams tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the address tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the application tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the clearances tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the engagement tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the expertise tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the permissions tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the personal tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the qualification tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab'; 
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table workflow.Workflow
UPDATE W
SET W.EntityTypeSubCode = 'External'
FROM workflow.Workflow W
WHERE W.WorkflowName LIKE '%External%'
	AND W.EntityTypeCode IN ('ExternalFeedback', 'Feedback', 'InternalFeedback')
GO

UPDATE W
SET W.EntityTypeSubCode = 'Internal'
FROM workflow.Workflow W
WHERE W.WorkflowName LIKE '%Internal%'
	AND W.EntityTypeCode IN ('ExternalFeedback', 'Feedback', 'InternalFeedback')
GO

UPDATE W
SET W.EntityTypeCode = 'Feedback'
FROM workflow.Workflow W
WHERE W.EntityTypeCode IN ('ExternalFeedback', 'InternalFeedback')
GO
--End table workflow.Workflow