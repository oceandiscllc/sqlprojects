-- File Name:	Build - 1.10 - HEROS.sql
-- Build Key:	Build - 1.10 - 2019.03.07 19.58.36

--USE HermisCloud
GO

-- ==============================================================================================================================
-- Schemas:
--		hrms
--		lesson
--		personupdate
--
-- Tables:
--		dropdown.AgeRange
--		dropdown.ApplicantSource
--		dropdown.CompetencyType
--		dropdown.CSGMemberPerformanceRating
--		dropdown.Ethnicity
--		dropdown.FeedbackQuestion
--		dropdown.InterviewOutcome
--		dropdown.LessonCategory
--		dropdown.LessonStatus
--		dropdown.LessonType
--		dropdown.PersonType
--		dropdown.PersonUpdateType
--		dropdown.SuitabilityRating
--		dropdown.SUPerformanceRating
--		dropdown.VacancyStatus
--		dropdown.VacancyType
--		hrms.Application
--		hrms.ApplicationCompetency
--		hrms.ApplicationEthnicity
--		hrms.ApplicationInterviewAvailability
--		hrms.ApplicationInterviewSchedule
--		hrms.ApplicationPerson
--		hrms.Competency
--		hrms.Vacancy
--		hrms.VacancyCompetency
--		lesson.Lesson
--		person.Feedback
--		person.PersonApplicantSource
--		person.PersonCountry
--		personupdate.Person
--		personupdate.PersonGroupPerson
--		personupdate.PersonPermissionable
--		personupdate.PersonProject
--		personupdate.PersonUpdate
--		reporting.SearchSetup
--
-- Triggers:
--		person.TR_PersonClientRoster ON person.PersonClientRoster
--		person.TR_PersonOperationalTraining ON person.PersonOperationalTraining
--		person.TR_PersonSecurityClearance ON person.PersonSecurityClearance
--
-- Functions:
--		person.CanHaveAccess
--		person.IsProfileUpdateRequired
--		utility.HasColumn
--		workflow.GetWorkflowStepCount
--		workflow.GetWorkflowStepNumber
--		workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID
--		workflow.IsPersonInCurrentWorkflowStep
--		workflow.IsWorkflowComplete
--
-- Procedures:
--		[hrms].[GetCompetencyByCompetencyID]
--		core.DeletePendingEmailByPendingEmailID
--		core.GetEmailTemplateData
--		core.GetPendingEmail
--		core.GetPendingOperationalTrainingExpirationEmail
--		core.GetPendingProfileReviewEmail
--		core.GetPendingSecurityClearanceExpirationEmail
--		core.GetPendingSURosterTerminationFinalEmail
--		core.GetPendingSURosterTerminationInitialEmail
--		core.PendingEmailAddUpdate
--		dropdown.GetAgeRangeData
--		dropdown.GetApplicantSourceData
--		dropdown.GetCompetencyData
--		dropdown.GetCompetencyTypeData
--		dropdown.GetCSGMemberPerformanceRatingData
--		dropdown.GetEthnicityData
--		dropdown.GetFeedbackQuestionData
--		dropdown.GetInterviewOutcomeData
--		dropdown.GetLessonCategoryData
--		dropdown.GetLessonStatusData
--		dropdown.GetLessonTypeData
--		dropdown.GetPersonTypeData
--		dropdown.GetPersonUpdateTypeData
--		dropdown.GetSuitabilityRatingData
--		dropdown.GetSUPerformanceRatingData
--		dropdown.GetVacancyStatusData
--		dropdown.GetVacancyTypeData
--		eventlog.LogFeedbackAction
--		eventlog.LogPersonUpdateAction
--		eventlog.LogVacancyAction
--		eventlog.LogVacancyApplicationAction
--		hrms.GetApplicationByApplicationID
--		hrms.GetVacancyByVacancyID
--		person.CheckAccess
--		person.GetFeedbackByFeedbackID
--		person.GetMedicalVettingEmailData
--		person.GetPersonByPersonID
--		person.GetPersonUsernameByEmailAddress
--		person.GetVettingEmailData
--		person.SaveDAPersonClientRoster
--		person.ValidateLogin
--		personupdate.ApprovePersonUpdateByPersonUpdateID
--		personupdate.GetPersonUpdateByPersonUpdateID
--		personupdate.SavePersonPermissionables
--		workflow.GetEntityWorkflowData
--		workflow.GetEntityWorkflowPeople
--		workflow.GetWorkflowByWorkflowID
--		workflow.InitializeEntityWorkflow
--		workflow.ResetEntityWorkflowStepGroupPerson
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
--USE HermisCloud
GO

--Begin function utility.HasColumn
EXEC utility.DropObject 'utility.HasColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.09
-- Description:	A function to return a bit indicating if a column exists in a table
-- ================================================================================

CREATE FUNCTION utility.HasColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250)
)

RETURNS BIT

AS
BEGIN

	DECLARE @bHasColumn BIT = 0

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		SET @bHasColumn = 1
	--ENDIF

	RETURN @bHasColumn

END
GO
--End function utility.HasColumn

--Begin schemas
EXEC utility.AddSchema 'hrms'
EXEC utility.AddSchema 'personupdate'
GO
--End schemas

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
--USE HermisCloud
GO

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.AddColumn @TableName, 'EntityTypeSubCode', 'VARCHAR(50)'
GO
--End table core.EntityType

--Begin table core.PendingEmail
DECLARE @TableName VARCHAR(250) = 'core.PendingEmail'

EXEC utility.AddColumn @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.AddColumn @TableName, 'DocumentIDList', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'SendDate', 'DATE'
EXEC utility.AddColumn @TableName, 'SentDateTime', 'DATETIME'
GO
--End table core.PendingEmail

--Begin table dropdown.AgeRange
DECLARE @TableName VARCHAR(250) = 'dropdown.AgeRange'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AgeRange
	(
	AgeRangeID INT IDENTITY(0,1) NOT NULL,
	AgeRangeCode VARCHAR(50),
	AgeRangeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AgeRangeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AgeRange', 'DisplayOrder,AgeRangeName', 'AgeRangeID'
GO
--End table dropdown.AgeRange

--Begin table dropdown.ApplicantSource
DECLARE @TableName VARCHAR(250) = 'dropdown.ApplicantSource'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ApplicantSource
	(
	ApplicantSourceID INT IDENTITY(0,1) NOT NULL,
	ApplicantSourceCode VARCHAR(50),
	ApplicantSourceName VARCHAR(250),
	HasOther BIT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasOther', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicantSourceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ApplicantSource', 'DisplayOrder,ApplicantSourceName', 'ApplicantSourceID'
GO
--End table dropdown.ApplicantSource

--Begin table dropdown.Competency
DECLARE @TableName VARCHAR(250) = 'dropdown.Competency'

EXEC utility.DropObject @TableName
GO
--End table dropdown.Competency

--Begin table dropdown.CompetencyType
DECLARE @TableName VARCHAR(250) = 'dropdown.CompetencyType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CompetencyType
	(
	CompetencyTypeID INT IDENTITY(0,1) NOT NULL,
	CompetencyTypeCode VARCHAR(50),
	CompetencyTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CompetencyTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_CompetencyType', 'DisplayOrder,CompetencyTypeName', 'CompetencyTypeID'
GO
--End table dropdown.CompetencyType

--Begin table dropdown.CSGMemberPerformanceRating
DECLARE @TableName VARCHAR(250) = 'dropdown.CSGMemberPerformanceRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CSGMemberPerformanceRating
	(
	CSGMemberPerformanceRatingID INT IDENTITY(0,1) NOT NULL,
	CSGMemberPerformanceRatingCode VARCHAR(50),
	CSGMemberPerformanceRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CSGMemberPerformanceRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_CSGMemberPerformanceRating', 'DisplayOrder,CSGMemberPerformanceRatingName', 'CSGMemberPerformanceRatingID'
GO
--End table dropdown.CSGMemberPerformanceRating

--Begin table dropdown.Ethnicity
DECLARE @TableName VARCHAR(250) = 'dropdown.Ethnicity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Ethnicity
	(
	EthnicityID INT IDENTITY(0,1) NOT NULL,
	EthnicityCategoryCode VARCHAR(50),
	EthnicityCategoryName VARCHAR(250),
	EthnicityCode VARCHAR(50),
	EthnicityName VARCHAR(250),
	HasOther BIT,
	EthnicityCategorDisplayOrder INT,
	EthnicityDisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EthnicityCategorDisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EthnicityDisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasOther', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'EthnicityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Ethnicity', 'EthnicityCategorDisplayOrder,EthnicityDisplayOrder,EthnicityName', 'EthnicityID'
GO
--End table dropdown.Ethnicity

--Begin table dropdown.FeedbackQuestion
DECLARE @TableName VARCHAR(250) = 'dropdown.FeedbackQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FeedbackQuestion
	(
	FeedbackQuestionID INT IDENTITY(0,1) NOT NULL,
	FeedbackQuestionCode VARCHAR(50),
	FeedbackQuestionName VARCHAR(250),
	DisplayOrder INT,
	IsExternal BIT,
	IsInternal BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsExternal', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInternal', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'FeedbackQuestionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_FeedbackQuestion', 'DisplayOrder,FeedbackQuestionName', 'FeedbackQuestionID'
GO
--End table dropdown.FeedbackQuestion

--Begin table dropdown.InterviewOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.InterviewOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InterviewOutcome
	(
	InterviewOutcomeID INT IDENTITY(0,1) NOT NULL,
	InterviewOutcomeCode VARCHAR(50),
	InterviewOutcomeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'InterviewOutcomeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_InterviewOutcome', 'DisplayOrder,InterviewOutcomeName', 'InterviewOutcomeID'
GO
--End table dropdown.InterviewOutcome

--Begin table dropdown.PersonType
DECLARE @TableName VARCHAR(250) = 'dropdown.PersonType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PersonType
	(
	PersonTypeID INT IDENTITY(0,1) NOT NULL,
	PersonTypeCode VARCHAR(50),
	PersonTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PersonType', 'DisplayOrder,PersonTypeName', 'PersonTypeID'
GO
--End table dropdown.PersonType

--Begin table dropdown.PersonUpdateType
DECLARE @TableName VARCHAR(250) = 'dropdown.PersonUpdateType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PersonUpdateType
	(
	PersonUpdateTypeID INT IDENTITY(0,1) NOT NULL,
	PersonUpdateTypeCode VARCHAR(50),
	PersonUpdateTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdateTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PersonUpdateType', 'DisplayOrder,PersonUpdateTypeName', 'PersonUpdateTypeID'
GO
--End table dropdown.PersonUpdateType

--Begin table dropdown.SuitabilityRating
DECLARE @TableName VARCHAR(250) = 'dropdown.SuitabilityRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SuitabilityRating
	(
	SuitabilityRatingID INT IDENTITY(0,1) NOT NULL,
	SuitabilityRatingCode VARCHAR(50),
	SuitabilityRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SuitabilityRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SuitabilityRating', 'DisplayOrder,SuitabilityRatingName', 'SuitabilityRatingID'
GO
--End table dropdown.SuitabilityRating

--Begin table dropdown.SUPerformanceRating
DECLARE @TableName VARCHAR(250) = 'dropdown.SUPerformanceRating'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SUPerformanceRating
	(
	SUPerformanceRatingID INT IDENTITY(0,1) NOT NULL,
	SUPerformanceRatingCode VARCHAR(50),
	SUPerformanceRatingName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SUPerformanceRatingID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SUPerformanceRating', 'DisplayOrder,SUPerformanceRatingName', 'SUPerformanceRatingID'
GO
--End table dropdown.SUPerformanceRating

--Begin table dropdown.VacancyStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.VacancyStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VacancyStatus
	(
	VacancyStatusID INT IDENTITY(0,1) NOT NULL,
	VacancyStatusCode VARCHAR(50),
	VacancyStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VacancyStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VacancyStatus', 'DisplayOrder,VacancyStatusName', 'VacancyStatusID'
GO
--End table dropdown.VacancyStatus

--Begin table dropdown.VacancyType
DECLARE @TableName VARCHAR(250) = 'dropdown.VacancyType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VacancyType
	(
	VacancyTypeID INT IDENTITY(0,1) NOT NULL,
	VacancyTypeCode VARCHAR(50),
	VacancyTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VacancyTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_VacancyType', 'DisplayOrder,VacancyTypeName', 'VacancyTypeID'
GO
--End table dropdown.VacancyType

--Begin table hrms.Application
DECLARE @TableName VARCHAR(250) = 'hrms.Application'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.Application
	(
	ApplicationID INT IDENTITY(1,1) NOT NULL,
	AgeRangeID INT,
	ConflictsOfInterest VARCHAR(MAX),
	ExpertCategoryID INT,
	GenderID INT,
	HasCertifiedApplication BIT,
	HasConfirmedTerminationDate BIT,
	HasDAAccount BIT,
	InterviewOutcomeID INT,
	IsGuaranteedInterview BIT,
	IsApplicationFinal BIT,
	IsNotCriminalCivilService BIT,
	IsNotCriminalPolice BIT,
	IsDisabled BIT,
	IsDueDilligenceComplete BIT,
	IsEligible BIT,
	IsGovernmetEmployee BIT,
	IsHermisProfileUpdated BIT,
	IsHRContactNotified BIT,
	IsInterviewAvailableAMDay1 BIT,
	IsInterviewAvailableAMDay2 BIT,
	IsInterviewAvailableAMDay3 BIT,
	IsInterviewAvailableAMDay4 BIT,
	IsInterviewAvailableAMDay5 BIT,
	IsInterviewAvailableAMDay6 BIT,
	IsInterviewAvailableAMDay7 BIT,
	IsInterviewAvailablePMDay1 BIT,
	IsInterviewAvailablePMDay2 BIT,
	IsInterviewAvailablePMDay3 BIT,
	IsInterviewAvailablePMDay4 BIT,
	IsInterviewAvailablePMDay5 BIT,
	IsInterviewAvailablePMDay6 BIT,
	IsInterviewAvailablePMDay7 BIT,
	IsInterviewComplete BIT,
	IsLineManagerNotified BIT,
	IsOfferAccepted BIT,
	IsOfferExtended BIT,
	IsOnRoster BIT,
	IsPreQualified BIT,
	IsVetted BIT,
	ManagerComments VARCHAR(MAX),
	PersonID INT,
	Suitability VARCHAR(2500),
	SubmittedDateTime DATETIME,
	TechnicalManagerComments VARCHAR(MAX),
	TerminationDate DATE,
	VacancyID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AgeRangeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpertCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'GenderID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasCertifiedApplication', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasConfirmedTerminationDate', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'HasDAAccount', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InterviewOutcomeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsApplicationFinal', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsDisabled', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsGovernmetEmployee', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsGuaranteedInterview', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsHermisProfileUpdated', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsHRContactNotified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay1', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay2', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay3', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay4', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay5', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay6', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailableAMDay7', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay1', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay2', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay3', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay4', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay5', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay6', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewAvailablePMDay7', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsInterviewComplete', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsLineManagerNotified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNotCriminalCivilService', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsNotCriminalPolice', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsOfferAccepted', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsOfferExtended', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsOnRoster', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsPreQualified', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsVetted', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicationID'
GO
--End table hrms.Application

--Begin table hrms.ApplicationCompetency
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationCompetency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationCompetency
	(
	ApplicationCompetencyID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	CompetencyID INT,
	Suitability VARCHAR(1250),
	SuitabilityRatingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CompetencyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SuitabilityRatingID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationCompetencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationCompetency', 'ApplicationID,CompetencyID'
GO
--End table hrms.ApplicationCompetency

--Begin table hrms.ApplicationEthnicity
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationEthnicity'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationEthnicity
	(
	ApplicationEthnicityID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	EthnicityID INT,
	EthnicityDescription VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'EthnicityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationEthnicityID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationEthnicity', 'ApplicationID,EthnicityID'
GO
--End table hrms.ApplicationEthnicity

--Begin table hrms.ApplicationInterviewAvailability
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationInterviewAvailability'

EXEC utility.DropObject 'hrms.ApplicationInterviewConfirmation'
EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationInterviewAvailability
	(
	ApplicationInterviewAvailabilityID INT IDENTITY(1,1) NOT NULL,
	ApplicationPersonID INT,
	ApplicationInterviewScheduleID INT,
	IsAvailable BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ApplicationInterviewScheduleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAvailable', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationInterviewAvailabilityID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationInterviewAvailability', 'ApplicationPersonID,ApplicationInterviewScheduleID'
GO
--End table hrms.ApplicationInterviewAvailability

--Begin table hrms.ApplicationInterviewSchedule
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationInterviewSchedule'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationInterviewSchedule
	(
	ApplicationInterviewScheduleID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	InterviewDateTime DATETIME,
	IsSelectedDate BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsSelectedDate', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApplicationInterviewScheduleID'
GO
--End table hrms.ApplicationInterviewSchedule

--Begin table hrms.ApplicationPerson
DECLARE @TableName VARCHAR(250) = 'hrms.ApplicationPerson'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.ApplicationPerson
	(
	ApplicationPersonID INT IDENTITY(1,1) NOT NULL,
	ApplicationID INT,
	PersonID INT,
	InterviewOutcomeID INT,
	PersonTypeCode VARCHAR(50),
	IsScheduleConfirmed BIT,
	ProposesInterviewDates BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InterviewOutcomeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsScheduleConfirmed', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProposesInterviewDates', 'BIT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ApplicationPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ApplicationPerson', 'ApplicationID,PersonID'
GO
--End table hrms.ApplicationPerson

--Begin table hrms.Competency
DECLARE @TableName VARCHAR(250) = 'hrms.Competency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.Competency
	(
	CompetencyID INT IDENTITY(1,1) NOT NULL,
	CompetencyName VARCHAR(250),
	CompetencyTypeID INT,
	CompetencyDescription VARCHAR(MAX),
	IsActive INT
	)

EXEC utility.SetDefaultConstraint 'hrms.Competency', 'CompetencyTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'hrms.Competency', 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered 'hrms.Competency', 'CompetencyID'
GO
--End table hrms.Competency

--Begin table hrms.Vacancy
DECLARE @TableName VARCHAR(250) = 'hrms.Vacancy'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.Vacancy
	(
	VacancyID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	VacancyName VARCHAR(250),
	VacancyStatusID INT,
	VacancyTypeID INT,
	VacancyReference VARCHAR(MAX),
	ExpertCategoryID INT,
	OpenDateTime DATETIME,
	CloseDateTime DATETIME,
	CoordinatorPersonID INT,
	RoleDescription VARCHAR(MAX),
	VacancyNotes VARCHAR(MAX),
	VacancyContactName VARCHAR(250),
	VacancyContactEmailAddress VARCHAR(320),
	VacancyContactPhone VARCHAR(50),
	CreateDateTime DATETIME,
	CreatePersonID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CoordinatorPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ExpertCategoryID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'VacancyID'
GO
--End table hrms.Vacancy

--Begin table hrms.VacancyCompetency
DECLARE @TableName VARCHAR(250) = 'hrms.VacancyCompetency'

EXEC utility.DropObject @TableName

CREATE TABLE hrms.VacancyCompetency
	(
	VacancyCompetencyID INT IDENTITY(1,1) NOT NULL,
	VacancyID INT,
	CompetencyID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CompetencyID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'VacancyID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'VacancyCompetencyID'
EXEC utility.SetIndexClustered @TableName, 'IX_VacancyCompetency', 'VacancyID,CompetencyID'
GO
--End table hrms.VacancyCompetency

--Begin table hrms.VacancyPerson
DECLARE @TableName VARCHAR(250) = 'hrms.VacancyPerson'

EXEC utility.DropObject @TableName
GO
--End table hrms.VacancyPerson

--Begin table person.Feedback
DECLARE @TableName VARCHAR(250) = 'person.Feedback'

EXEC utility.DropObject @TableName

CREATE TABLE person.Feedback
	(
	FeedbackID INT IDENTITY(1,1) NOT NULL,
	FeedbackName VARCHAR(500),
	TaskID INT,
	ProjectID INT,
	StartDate DATE,
	EndDate DATE,
	CreatePersonID INT,
	CreateDate DATE,
	SUPerformanceRatingID1 INT,
	SUPerformanceRatingID2 INT,
	SUPerformanceRatingID3 INT,
	Question4 VARCHAR(MAX),
	Question5 VARCHAR(MAX),
	CSGMemberPersonID INT,
	CSGMemberPerformanceRatingID INT,
	Question7 VARCHAR(MAX),
	CSGMemberComment VARCHAR(MAX),
	IsExternal BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CSGMemberPerformanceRatingID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CSGMemberPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'IsExternal', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SUPerformanceRatingID1', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SUPerformanceRatingID2', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SUPerformanceRatingID3', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TaskID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'FeedbackID'
GO
--End table person.Feedback

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropColumn @TableName, 'GenderCode'
EXEC utility.DropColumn @TableName, 'HasConfirmedTerminationDate'
EXEC utility.DropColumn @TableName, 'IsGovernmetEmployee'
EXEC utility.DropColumn @TableName, 'SponsorName'
EXEC utility.DropColumn @TableName, 'TerminationDate'

IF utility.HasColumn(@TableName, 'PoliceRankID') = 1
	EXEC sp_RENAME 'person.Person.PoliceRankID', 'PoliceRankID1', 'COLUMN'
--ENDIF	

EXEC utility.AddColumn @TableName, 'ApplicantSourceOther', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'ApplicantTypeCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'CivilServiceDepartment', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'CivilServiceGrade1', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'CivilServiceGrade2', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'DAAccountDecomissionedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'DaysToRemoval', 'INT', '0'
EXEC utility.AddColumn @TableName, 'DeleteByPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'DeleteDate', 'DATE'
EXEC utility.AddColumn @TableName, 'FARemoveByPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'FARemoveDate', 'DATE'
EXEC utility.AddColumn @TableName, 'HRContactCountryCallingCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'HRContactEmailAddress', 'VARCHAR(320)'
EXEC utility.AddColumn @TableName, 'HRContactName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'HRContactPhone', 'VARCHAR(25)'
EXEC utility.AddColumn @TableName, 'ISDAAccountDecomissioned', 'BIT', '0'
EXEC utility.AddColumn @TableName, 'IsPreviousApplicant', 'BIT', '0' 
EXEC utility.AddColumn @TableName, 'IsUKResidencyEligible', 'BIT', '0'
EXEC utility.AddColumn @TableName, 'LastContractEndDate', 'DATE'
EXEC utility.AddColumn @TableName, 'LineManagerCountryCallingCodeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'LineManagerEmailAddress', 'VARCHAR(320)'
EXEC utility.AddColumn @TableName, 'LineManagerName', 'VARCHAR(100)'
EXEC utility.AddColumn @TableName, 'LineManagerPhone', 'VARCHAR(25)'
EXEC utility.AddColumn @TableName, 'LockOutByPersonID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'LockOutDate', 'DATE'
EXEC utility.AddColumn @TableName, 'OtherGrade', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'PersonTypeID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PoliceRankID2', 'INT', '0'
EXEC utility.AddColumn @TableName, 'PreviousApplicationDate', 'DATE'
EXEC utility.AddColumn @TableName, 'SecurityClearanceID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'SecurityClearanceSponsorName', 'VARCHAR(250)'
GO
--End table person.Person

--Begin table person.PersonApplicantSource
DECLARE @TableName VARCHAR(250) = 'person.PersonApplicantSource'

EXEC utility.DropObject 'hrms.ApplicationApplicantSource'
EXEC utility.DropObject @TableName

CREATE TABLE person.PersonApplicantSource
	(
	PersonApplicantSourceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	ApplicantSourceID INT,
	ApplicantSourceDescription VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApplicantSourceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonApplicantSourceID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonApplicantSource', 'PersonID,ApplicantSourceID'
GO
--End table person.PersonApplicantSource

--Begin table person.PersonClientRoster
EXEC utility.DropObject 'person.TR_PersonClientRoster'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.09
-- Description:	A trigger to insert data into the core.PendingEmail table
-- ======================================================================
CREATE TRIGGER person.TR_PersonClientRoster ON person.PersonClientRoster AFTER INSERT, UPDATE
AS
	SET ARITHABORT ON;

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.SentDateTime IS NULL
		AND PE.EmailTemplateCode IN ('SURosterTerminationFinal', 'SURosterTerminationInitial')
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonClientRoster PCR
				JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
				JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
					AND CR.ClientRosterCode = 'SU'
					AND ES.ExpertStatusCode = 'Removed'
					AND PE.PersonID = PCR.PersonID
			)

	INSERT INTO core.PendingEmail
		(EntityTypeCode, EmailTemplateCode, EntityID, SendDate, PersonID)
	SELECT
		'PersonClientRoster',
		'SURosterTerminationInitial',
		PCR.PersonClientRosterID,
		getDate(),
		PCR.PersonID
	FROM person.PersonClientRoster PCR
		JOIN INSERTED I ON I.PersonClientRosterID = PCR.PersonClientRosterID
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = I.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = I.ExpertStatusID
			AND CR.ClientRosterCode = 'SU'
			AND ES.ExpertStatusCode = 'Removed'
			AND I.TerminationDate IS NOT NULL
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.PendingEmail PE
				WHERE PE.EntityTypeCode = 'Person'
					AND PE.EmailTemplateCode = 'SURosterTerminationInitial'
					AND PE.EntityID = PCR.PersonID
				)

	UNION

	SELECT
		'PersonClientRoster',
		'SURosterTerminationFinal',
		PCR.PersonClientRosterID,
		DATEADD(m, 11, PCR.TerminationDate),
		PCR.PersonID
	FROM person.PersonClientRoster PCR
		JOIN INSERTED I ON I.PersonClientRosterID = PCR.PersonClientRosterID
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = I.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = I.ExpertStatusID
			AND CR.ClientRosterCode = 'SU'
			AND ES.ExpertStatusCode = 'Removed'
			AND I.TerminationDate IS NOT NULL
			AND NOT EXISTS
				(
				SELECT 1
				FROM core.PendingEmail PE
				WHERE PE.EntityTypeCode = 'Person'
					AND PE.EmailTemplateCode = 'SURosterTerminationFinal'
					AND PE.EntityID = PCR.PersonID
				)

	UPDATE PE
	SET PE.SendDate = DATEADD(m, 11, PCR.TerminationDate)
	FROM core.PendingEmail PE
		JOIN person.PersonClientRoster PCR ON PCR.PersonClientRosterID = PE.EntityID
		JOIN INSERTED I ON I.PersonClientRosterID = PCR.PersonClientRosterID
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = I.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = I.ExpertStatusID
			AND CR.ClientRosterCode = 'SU'
			AND ES.ExpertStatusCode = 'Removed'
			AND I.TerminationDate IS NOT NULL
			AND PE.SentDateTime IS NULL
			AND PE.EmailTemplateCode = 'SURosterTerminationFinal'

GO

ALTER TABLE person.PersonClientRoster ENABLE TRIGGER TR_PersonClientRoster
GO
--End table person.PersonClientRoster

--Begin table person.PersonCountry
DECLARE @TableName VARCHAR(250) = 'person.PersonCountry'

EXEC utility.DropObject 'hrms.ApplicationCountry'
EXEC utility.DropObject @TableName

CREATE TABLE person.PersonCountry
	(
	PersonCountryID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	CountryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonCountryID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonCountry', 'PersonID,CountryID'
GO
--End table person.PersonCountry

--Begin table person.PersonExpertStatus
EXEC Utility.DropObject 'person.PersonExpertStatus'
GO
--End table person.PersonExpertStatus

--Begin table person.PersonOperationalTraining
EXEC utility.DropObject 'person.TR_PersonOperationalTraining'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.09
-- Description:	A trigger to manage data in the core.PendingEmail table
-- ====================================================================
CREATE TRIGGER person.TR_PersonOperationalTraining ON person.PersonOperationalTraining AFTER DELETE, INSERT, UPDATE
AS
	SET ARITHABORT ON;
	
	DECLARE @nPersonOperationalTrainingID INT

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT I.PersonOperationalTrainingID
		FROM INSERTED I
		ORDER BY I.PersonOperationalTrainingID

	OPEN oCursor
	FETCH oCursor INTO @nPersonOperationalTrainingID
	WHILE @@fetch_status = 0
		BEGIN

		EXEC core.PendingEmailAddUpdate 'PersonOperationalTraining', 'OperationalTrainingExpiration', @nPersonOperationalTrainingID, 'OperationalTrainingExpirationEmailDayCount'
		
		FETCH oCursor INTO @nPersonOperationalTrainingID
	
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor	

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.SentDateTime IS NULL
		AND PE.EmailTemplateCode = 'OperationalTrainingExpiration'
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonOperationalTraining POT
			WHERE POT.PersonOperationalTrainingID = PE.EntityID
			)

GO

ALTER TABLE person.PersonOperationalTraining ENABLE TRIGGER TR_PersonOperationalTraining
GO
--End table person.PersonOperationalTraining

--Begin table person.PersonSecurityClearance
EXEC utility.DropObject 'person.TR_PersonSecurityClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonSecurityClearance table
--
-- Author:			Justin Branum
-- Create Date: 2019.01.24
-- Description: Trigger needs to set the other medical clearances to inactive ONLY if that record itself is set to Active
--
-- Author:			Todd Pires
-- Create date:	2018.02.09
-- Description:	Added code to manage data in the core.PendingEmail table
-- ======================================================================================================================
CREATE TRIGGER person.TR_PersonSecurityClearance ON person.PersonSecurityClearance AFTER DELETE, INSERT, UPDATE
AS
	SET ARITHABORT ON;
	
	DECLARE @nPersonSecurityClearanceID INT

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT I.PersonSecurityClearanceID
		FROM INSERTED I
		ORDER BY I.PersonSecurityClearanceID

	OPEN oCursor
	FETCH oCursor INTO @nPersonSecurityClearanceID
	WHILE @@fetch_status = 0
		BEGIN

		EXEC core.PendingEmailAddUpdate 'PersonSecurityClearance', 'SecurityClearanceExpiration', @nPersonSecurityClearanceID, 'SecurityClearanceExpirationEmailDayCount'
		
		FETCH oCursor INTO @nPersonSecurityClearanceID
	
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor	

	UPDATE PSC
	SET PSC.IsActive = 0
	FROM person.PersonSecurityClearance PSC
		JOIN INSERTED I ON I.PersonID = PSC.PersonID
			AND I.PersonSecurityClearanceID <> PSC.PersonSecurityClearanceID
			AND I.IsActive = 1

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.SentDateTime IS NULL
		AND PE.EmailTemplateCode = 'SecurityClearanceExpiration'
		AND NOT EXISTS
			(
			SELECT 1
			FROM person.PersonSecurityClearance PSC
			WHERE PSC.PersonSecurityClearanceID = PE.EntityID
			)

GO

ALTER TABLE person.PersonSecurityClearance ENABLE TRIGGER TR_PersonSecurityClearance
GO
--End table person.PersonSecurityClearance

--Begin table personupdate.Person
DECLARE @TableName VARCHAR(250) = 'personupdate.Person'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.Person
	(
	PersonUpdatePersonID INT IDENTITY(1,1) NOT NULL,
	PersonUpdateID INT,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	UserName VARCHAR(250),
	EmailAddress VARCHAR(320),
	ManagerPersonID INT,
	DefaultProjectID INT,
	Organization VARCHAR(250),
	PersonID INT,
	RoleID INT,
	InvoiceLimit NUMERIC(18, 2),
	PurchaseOrderLimit NUMERIC(18, 2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DefaultProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InvoiceLimit', 'NUMERIC(18, 2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ManagerPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PurchaseOrderLimit', 'NUMERIC(18, 2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonID'
GO
--End table personupdate.Person

--Begin table personupdate.PersonGroupPerson
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonGroupPerson'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonGroupPerson
	(
	PersonUpdatePersonGroupPersonID INT IDENTITY(1,1) NOT NULL,
	PersonGroupID INT,
	PersonID INT,
	PersonUpdateID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonUpdateID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonGroupPersonID'
GO
--End table personupdate.PersonGroupPerson

--Begin table personupdate.PersonPermissionable
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonPermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonPermissionable
	(
	PersonUpdatePersonPermissionableID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PermissionableLineage VARCHAR(250),
	PersonUpdateID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonUpdateID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonPermissionableID'
GO
--End table personupdate.PersonPermissionable

--Begin table personupdate.PersonProject
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonProject'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonProject
	(
	PersonUpdatePersonProjectID INT IDENTITY(1,1) NOT NULL,
	PersonUpdateID INT,
	PersonID INT,
	ProjectID INT,
	IsViewDefault BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsViewDefault', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonUpdatePersonProjectID'
GO
--End table personupdate.PersonProject

--Begin table personupdate.PersonUpdate
DECLARE @TableName VARCHAR(250) = 'personupdate.PersonUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE personupdate.PersonUpdate
	(
	PersonUpdateID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	UserName VARCHAR(250),
	PersonUpdateTypeID INT,
	RequestDate DATE,
	RequestedByPersonID INT,
	ActionDate DATE,
	WorkflowStepNumber INT,
	Notes VARCHAR(MAX),
	ISDAAccountDecomissioned BIT,
	DAAccountDecomissionedDate DATE,
	LockOutByPersonID INT,
	LockOutDate DATE,
	FARemoveByPersonID INT,
	FARemoveDate DATE,
	DeleteByPersonID INT,
	DeleteDate DATE,
	DaysToRemoval INT
	)

EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'DaysToRemoval', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'DeleteByPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'FARemoveByPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'ISDAAccountDecomissioned', 'BIT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'LockOutByPersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'RequestDate', 'DATE', 'getdate()'
EXEC utility.SetDefaultConstraint 'personupdate.PersonUpdate', 'WorkflowStepNumber', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered 'personupdate.PersonUpdate', 'PersonUpdateID'
GO
--End table personupdate.PersonUpdate

--Begin table reporting.SearchSetup
DECLARE @TableName VARCHAR(250) = 'reporting.SearchSetup'

EXEC utility.DropObject @TableName

CREATE TABLE reporting.SearchSetup
	(
	SearchSetupID BIGINT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	PersonID INT,
	ColumnCode VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SearchSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SearchSetup', 'EntityTypeCode,PersonID'
GO
--End table reporting.SearchSetup

--Begin table workflow.EntityWorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowStepGroupPerson'

EXEC utility.DropColumn 'workflow.EntityWorkflowStepGroupPerson', 'WorkflowStepStatusName'		
EXEC utility.DropColumn 'workflow.EntityWorkflowStepGroupPersonGroup', 'WorkflowStepStatusName'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC utility.AddColumn @TableName, 'EntityTypeSubCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'WorkflowCompleteStatusName', 'VARCHAR(50)'
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC utility.AddColumn @TableName, 'WorkflowStepStatusName', 'VARCHAR(50)'
GO
--End table workflow.WorkflowStep

ALTER TABLE hrms.Application ALTER COLUMN Suitability VARCHAR(MAX)
ALTER TABLE person.Person ALTER COLUMN SummaryBiography VARCHAR(MAX)
ALTER TABLE hrms.ApplicationCompetency ALTER COLUMN Suitability VARCHAR(MAX)
GO

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--USE HermisCloud
GO

--Begin function person.CanHaveAccess
EXEC utility.DropObject 'person.CanHaveAccess'
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.10.03
-- Description:	A function to determine if a PersonID has access to a specific entity type / entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
--
-- Author:			Jonathan Burnham
-- Create date:	2019.02.02
-- Description:	refactored to support the hrms workflow
-- ===================================================================================================

CREATE FUNCTION person.CanHaveAccess
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bCanHaveAccess BIT = 0

	IF @EntityTypeCode IN ('ActivityReport', 'EquipmentOrder', 'Feedback', 'PersonUpdate', 'SpotReport', 'TrendReport', 'Vacancy', 'VacancyApplication')
		BEGIN

		SET @bCanHaveAccess = workflow.IsWorkflowComplete(@EntityTypeCode, @EntityID)

		IF @bCanHaveAccess = 0
			SET @bCanHaveAccess = workflow.IsPersonInCurrentWorkflowStep(@PersonID, @EntityTypeCode, @EntityID, 0)
		--ENDIF

		END
	--ENDIF

	IF @EntityTypeCode = 'VacancyApplication' AND @bCanHaveAccess = 0
		BEGIN

		SELECT @bCanHaveAccess = CASE WHEN A.SubmittedDateTime IS NULL THEN 1 ELSE 0 END
		FROM hrms.Application A
		WHERE A.ApplicationID = @EntityID
			AND A.PersonID = @PersonID

		END
	--ENDIF

	RETURN ISNULL(@bCanHaveAccess, 0)

END
GO
--End function person.CanHaveAccess

--Begin function person.IsProfileUpdateRequired
EXEC utility.DropObject 'person.IsProfileUpdateRequired'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2018.10.24
-- Description:	A function to return data from the person.Person table
-- ===================================================================
CREATE FUNCTION person.IsProfileUpdateRequired
(
@PersonID INT,
@IsTwoFactorEnabled BIT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsProfileUpdateRequired BIT

	SELECT @bIsProfileUpdateRequired = 
		CASE
			WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
				OR core.NullIfEmpty(P.FirstName) IS NULL
				OR core.NullIfEmpty(P.LastName) IS NULL
				OR P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				OR P.HasAcceptedTerms = 0
				OR (@IsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
				OR (@IsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
				OR (@IsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
			THEN 1
			ELSE 0
		END

	FROM person.Person P
	WHERE P.PersonID = @PersonID

	RETURN ISNULL(@bIsProfileUpdateRequired, 1)

END
GO
--End function project.IsProfileUpdateRequired

--Begin function utility.HasColumn
EXEC utility.DropObject 'utility.HasColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.09
-- Description:	A function to return a bit indicating if a column exists in a table
-- ================================================================================

CREATE FUNCTION utility.HasColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250)
)

RETURNS BIT

AS
BEGIN

	DECLARE @bHasColumn BIT = 0

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		SET @bHasColumn = 1
	--ENDIF

	RETURN @bHasColumn

END
GO
--End function utility.HasColumn

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT
	DECLARE @tTable TABLE (WorkflowStepNumber INT)

	INSERT INTO @tTable 
		(WorkflowStepNumber)
 	SELECT 
 		EWSGP.WorkflowStepNumber
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
		JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID

	UNION

	SELECT 
		EWSGPG.WorkflowStepNumber
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID

	SELECT 
		@nWorkflowStepCount = MAX(T.WorkflowStepNumber) 
	FROM @tTable T
	
	RETURN ISNULL(@nWorkflowStepCount, 0)

END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @tTable TABLE(WorkflowStepNumber INT, WorkflowStepGroupID INT)

	INSERT INTO @tTable 
		(WorkflowStepNumber,WorkflowStepGroupID)
 	SELECT 
 		EWSGP.WorkflowStepNumber, 
 		EWSGP.WorkflowStepGroupID
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
		JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.IsComplete = 0 

	UNION

	SELECT 
		EWSGPG.WorkflowStepNumber, 
		EWSGPG.WorkflowStepGroupID
	FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
	WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
		AND EWSGPG.EntityID = @EntityID
		AND EWSGPG.IsComplete = 0 

	SELECT TOP 1 
		@nWorkflowStepNumber = T.WorkflowStepNumber 
	FROM @tTable T 
	ORDER BY T.WorkflowStepNumber, T.WorkflowStepGroupID

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber

END
GO
--End function workflow.GetWorkflowStepNumber

--Begin function workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID'
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.25
-- Description:	A function to get the status of an entity under workflow
-- =====================================================================

CREATE FUNCTION workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@DefaultWorkflowStepStatusName VARCHAR(50)
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cWorkflowStepStatusName VARCHAR(50)

	IF NOT EXISTS 
		(
		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID

		UNION

		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID
		)
		SET @cWorkflowStepStatusName = @DefaultWorkflowStepStatusName
	ELSE IF NOT EXISTS 
		(
		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID 
			AND EWSGP.IsComplete = 0

		UNION

		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID 
			AND EWSGPG.IsComplete = 0
		)
		BEGIN

		SELECT TOP 1 @cWorkflowStepStatusName = D.WorkflowCompleteStatusName
		FROM
			(
			SELECT TOP 1
				EWSGP.WorkflowStepID,
				ISNULL(W.WorkflowCompleteStatusName, 'Workflow Complete') AS WorkflowCompleteStatusName
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
			GROUP BY EWSGP.WorkflowStepID, W.WorkflowCompleteStatusName

			UNION

			SELECT TOP 1
				EWSGPG.WorkflowStepID,
				ISNULL(W.WorkflowCompleteStatusName, 'Workflow Complete') AS WorkflowCompleteStatusName
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGPG.WorkflowID
					AND EWSGPG.EntityTypeCode = @EntityTypeCode
					AND EWSGPG.EntityID = @EntityID
			GROUP BY EWSGPG.WorkflowStepID, W.WorkflowCompleteStatusName

			ORDER BY 1 DESC
			) D
		ORDER BY D.WorkflowStepID

		END
	ELSE
		BEGIN

		SELECT TOP 1 @cWorkflowStepStatusName = D.WorkflowStepStatusName
		FROM
			(
			SELECT TOP 1
				EWSGP.WorkflowStepID,
				ISNULL(WS.WorkflowStepStatusName, 'No Workflow Setp Status Name Assigned') AS WorkflowStepStatusName
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGP.WorkflowID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWSGP.WorkflowStepID
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			GROUP BY EWSGP.WorkflowStepID, WS.WorkflowStepStatusName

			UNION

			SELECT TOP 1
				EWSGPG.WorkflowStepID,
				ISNULL(WS.WorkflowStepStatusName, 'No Workflow Setp Status Name Assigned') AS WorkflowStepStatusName
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
				JOIN workflow.Workflow W ON W.WorkflowID = EWSGPG.WorkflowID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWSGPG.WorkflowStepID
					AND EWSGPG.EntityTypeCode = @EntityTypeCode
					AND EWSGPG.EntityID = @EntityID
					AND EWSGPG.IsComplete = 0
			GROUP BY EWSGPG.WorkflowStepID, WS.WorkflowStepStatusName

			ORDER BY 1
			) D
		ORDER BY D.WorkflowStepID

		END
	--ENDIF

	RETURN ISNULL(@cWorkflowStepStatusName, @DefaultWorkflowStepStatusName)

END
GO
--End function workflow.GetWorkflowStepStatusNameByEntityTypeCodeAndEntityID

--Begin function workflow.IsPersonInCurrentWorkflowStep
EXEC utility.DropObject 'workflow.IsPersonInCurrentWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to indicate if a personid exists in the current step of an entity's workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =====================================================================================================

CREATE FUNCTION workflow.IsPersonInCurrentWorkflowStep
(
@PersonID INT,
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsPersonInCurrentWorkflowStep BIT = 0

	IF (@EntityID = 0 AND EXISTS 
		(
		SELECT 1
		FROM workflow.WorkflowStepGroupPerson WSGP
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND WSGP.PersonID = @PersonID
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback'))
				AND W.IsActive = 1
				AND (
					@ProjectID = 0 
						OR (
							W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
							AND w.ProjectID = @ProjectID
						)
				)

		UNION

		SELECT 1
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = WSGPG.PersonGroupID
					)
				AND WS.WorkflowStepNumber = 1
				AND (W.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND W.EntityTypeCode LIKE '%Feedback'))
				AND W.IsActive = 1
		)) OR EXISTS
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE (EWSGP.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND EWSGP.EntityTypeCode LIKE '%Feedback'))
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EWSGP.PersonID = @PersonID

			UNION

			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
			WHERE (EWSGPG.EntityTypeCode = @EntityTypeCode OR (@EntityTypeCode = 'Feedback' AND EWSGPG.EntityTypeCode LIKE '%Feedback'))
				AND EWSGPG.EntityID = @EntityID
				AND EWSGPG.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
				AND EXISTS
					(
					SELECT 1
					FROM person.PersonGroupPerson PGP
					WHERE PGP.PersonID = @PersonID
						AND PGP.PersonGroupID = EWSGPG.PersonGroupID
					)
			)

		SET @bIsPersonInCurrentWorkflowStep = 1
	--ENDIF
	
	RETURN @bIsPersonInCurrentWorkflowStep

END
GO
--End function workflow.IsPersonInCurrentWorkflowStep

--Begin function workflow.IsWorkflowComplete
EXEC utility.DropObject 'workflow.IsWorkflowComplete'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.25
-- Description:	A function to return a bit indicating if an entity has completed a workflow
-- ========================================================================================

CREATE FUNCTION workflow.IsWorkflowComplete
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bIsWorkflowComplete BIT = 0

	IF EXISTS 
		(
		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
			AND EWSGP.EntityID = @EntityID

		UNION

		SELECT 1 
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
		WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
			AND EWSGPG.EntityID = @EntityID
		) 	
		AND NOT EXISTS 
			(
			SELECT 1 
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			WHERE EWSGP.EntityTypeCode = @EntityTypeCode 
				AND EWSGP.EntityID = @EntityID 
				AND EWSGP.IsComplete = 0

			UNION

			SELECT 1 
			FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG 
			WHERE EWSGPG.EntityTypeCode = @EntityTypeCode 
				AND EWSGPG.EntityID = @EntityID 
				AND EWSGPG.IsComplete = 0
			)

		SET @bIsWorkflowComplete = 1
	--ENDIF

	RETURN @bIsWorkflowComplete

END
GO
--End function workflow.IsWorkflowComplete
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--USE HermisCloud
GO

--Begin procedure core.DeletePendingEmailByPendingEmailID
EXEC utility.DropObject 'core.DeletePendingEmailByPendingEmailID'
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure delete data from the core.PendingEmail table
-- ============================================================================
CREATE PROCEDURE core.DeletePendingEmailByPendingEmailID

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.PendingEmailID = @PendingEmailID

END
GO
--End procedure core.DeletePendingEmailByPendingEmailID

--Begin procedure core.GetEmailTemplateData
EXEC utility.DropObject 'core.GetEmailTemplateData'
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateData

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50) = NULL,
@WorkflowActionCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	--EmailTemplateData
	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND (@EmailTemplateCode IS NULL OR ET.EmailTemplateCode = @EmailTemplateCode)
		AND (@WorkflowActionCode IS NULL OR ET.WorkflowActionCode = @WorkflowActionCode)

	--EmailTemplateFieldData
	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

	--EmailTemplateGlobalData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReply,
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS SiteURL,
		core.GetSystemSetupValueBySystemSetupKey('SystemName', '') AS SystemName

END
GO
--End procedure core.GetEmailTemplateData

--Begin procedure core.GetPendingEmail
EXEC utility.DropObject 'core.GetPendingEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure get data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		core.GetSystemSetupValueBySystemSetupKey('FeedBackMailTo', '') AS FeedBackEmailAddress,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReplyEmailAddress,
		PE.EmailTemplateCode
	FROM core.PendingEmail PE
	WHERE PE.PendingEmailID = @PendingEmailID
		OR 
			(
			@PendingEmailID = 0
				AND PE.SentDateTime IS NULL
				AND PE.SendDate <= getDate()
			) 
	ORDER BY PE.EmailTemplateCode

END
GO
--End procedure core.GetPendingEmail

--Begin procedure core.GetPendingOperationalTrainingExpirationEmail
EXEC utility.DropObject 'core.GetPendingOperationalTrainingExpirationEmail'
EXEC utility.DropObject 'core.GetPendingPersonOperationalTrainingExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingOperationalTrainingExpirationEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		OTC.OperationalTrainingCourseName,
		P.EmailAddress,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM person.PersonOperationalTraining POT
		JOIN person.Person P ON P.PersonID = POT.PersonID
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonOperationalTraining'
			AND PE.EmailTemplateCode = 'OperationalTrainingExpiration'
			AND PE.EntityID = POT.PersonOperationalTrainingID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingOperationalTrainingExpirationEmail

--Begin procedure core.GetPendingProfileReviewEmail
EXEC utility.DropObject 'core.GetPendingProfileReviewEmail'
EXEC utility.DropObject 'core.GetPendingPersonProfileReviewEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingProfileReviewEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted,
		'<a href="' + DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') + '">DeployAdviser</a>' AS DeployAdviserLink,
		DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS DeployAdviserURL
	FROM person.Person P
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'ProfileReview'
			AND PE.EntityID = P.PersonID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingProfileReviewEmail

--Begin procedure core.GetPendingSecurityClearanceExpirationEmail
EXEC utility.DropObject 'core.GetPendingSecurityClearanceExpirationEmail'
EXEC utility.DropObject 'core.GetPendingPersonSecurityClearanceExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingSecurityClearanceExpirationEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		core.FormatDate(PSC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		SC.SecurityClearanceName,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted
	FROM person.PersonSecurityClearance PSC
		JOIN person.Person P ON P.PersonID = PSC.PersonID
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonSecurityClearance'
			AND PE.EmailTemplateCode = 'SecurityClearanceExpiration'
			AND PE.EntityID = PSC.PersonSecurityClearanceID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingSecurityClearanceExpirationEmail

--Begin procedure core.GetPendingSURosterTerminationFinalEmail
EXEC utility.DropObject 'core.GetPendingSURosterTerminationFinalEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.09
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingSURosterTerminationFinalEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		core.FormatDate(DATEADD(m, 12, PCR.TerminationDate)) AS DeletionDateFormatted,
		core.GetSystemSetupValueBySystemSetupKey('SURosterTerminationEmailAddressToFinal', '') AS SURosterTerminationEmailAddressToFinal,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM person.PersonClientRoster PCR
		JOIN person.Person P ON P.PersonID = PCR.PersonID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonClientRoster'
			AND PE.EmailTemplateCode = 'SURosterTerminationFinal'
			AND PE.EntityID = PCR.PersonClientRosterID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingSURosterTerminationFinalEmail

--Begin procedure core.GetPendingSURosterTerminationInitialEmail
EXEC utility.DropObject 'core.GetPendingSURosterTerminationInitialEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.09
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingSURosterTerminationInitialEmail

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		PCR.TerminationReason,
		core.GetSystemSetupValueBySystemSetupKey('SURosterContactEmailAddress', '') AS SURosterContactEmailAddress,
		core.GetSystemSetupValueBySystemSetupKey('SURosterTerminationEmailAddressToInitial', '') AS SURosterTerminationEmailAddressToInitial,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM person.PersonClientRoster PCR
		JOIN person.Person P ON P.PersonID = PCR.PersonID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'PersonClientRoster'
			AND PE.EmailTemplateCode = 'SURosterTerminationInitial'
			AND PE.EntityID = PCR.PersonClientRosterID
			AND 
				(
				PE.PendingEmailID = @PendingEmailID
					OR 
						(
						@PendingEmailID = 0
							AND PE.SentDateTime IS NULL
							AND PE.SendDate <= getDate()
						)
				)
	ORDER BY 1

END
GO
--End procedure core.GetPendingSURosterTerminationInitialEmail

--Begin procedure core.PendingEmailAddUpdate
EXEC utility.DropObject 'core.CreatePendingEmail'
EXEC utility.DropObject 'core.PendingEmailAddUpdate'
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.09
-- Description:	A stored procedure populate the core.PendingEmail table
-- ====================================================================
CREATE PROCEDURE core.PendingEmailAddUpdate

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50),
@EntityID INT,
@SystemSetupKey VARCHAR(250),
@DocumentIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @dSendDate DATE
	DECLARE @nPendingEmailID INT = ISNULL(
		(
		SELECT PE.PendingEmailID
		FROM core.PendingEmail PE 
		WHERE PE.EmailTemplateCode = @EmailTemplateCode 
			AND PE.EntityID = @EntityID 
			AND PE.SentDateTime IS NULL
		), 0)
	DECLARE @nPersonID INT

	IF @EmailTemplateCode = 'ProfileReview'
		BEGIN

		SELECT 
			@dSendDate = DATEADD(d, CAST(core.GetSystemSetupValueBySystemSetupKey(@SystemSetupKey, '30') AS INT) * -1, P.NextReviewDate),
			@nPersonID = P.PersonID
		FROM person.Person P 
		WHERE P.PersonID = @EntityID

		END
	ELSE IF @EmailTemplateCode = 'OperationalTrainingExpiration'
		BEGIN

		SELECT 
			@dSendDate = DATEADD(d, CAST(core.GetSystemSetupValueBySystemSetupKey(@SystemSetupKey, '30') AS INT) * -1, POT.ExpiryDate),
			@nPersonID = POT.PersonID
		FROM person.PersonOperationalTraining POT 
		WHERE POT.PersonOperationalTrainingID = @EntityID

		END
	ELSE IF @EmailTemplateCode = 'SecurityClearanceExpiration'
		BEGIN

		SELECT 
			@dSendDate = DATEADD(d, CAST(core.GetSystemSetupValueBySystemSetupKey(@SystemSetupKey, '30') AS INT) * -1, PSC.MandatoryReviewDate),
			@nPersonID = PSC.PersonID
		FROM person.PersonSecurityClearance PSC 
		WHERE PSC.PersonSecurityClearanceID = @EntityID

		END
	--ENDIF

	IF @dSendDate IS NOT NULL
		BEGIN

		IF @nPendingEmailID = 0
			BEGIN

			INSERT INTO core.PendingEmail 
				(EntityTypeCode, EmailTemplateCode, EntityID, PersonID, SendDate, DocumentIDList) 
			VALUES 
				(@EntityTypeCode, @EmailTemplateCode, @EntityID, ISNULL(@nPersonID, 0), @dSendDate, @DocumentIDList)

			END
		ELSE
			BEGIN

			UPDATE PE 
			SET PE.SendDate = @dSendDate 
			FROM core.PendingEmail PE 
			WHERE PE.PendingEmailID = @nPendingEmailID

			END
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure core.PendingEmailAddUpdate

--Begin procedure core.GetProjectIDByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'core.GetProjectIDByEntityTypeCodeAndEntityID'
GO
--End procedure core.GetProjectIDByEntityTypeCodeAndEntityID

--Begin procedure dropdown.GetAgeRangeData
EXEC Utility.DropObject 'dropdown.GetAgeRangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.AgeRange table
-- =================================================================================
CREATE PROCEDURE dropdown.GetAgeRangeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AgeRangeID, 
		T.AgeRangeCode,
		T.AgeRangeName
	FROM dropdown.AgeRange T
	WHERE (T.AgeRangeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AgeRangeName, T.AgeRangeID

END
GO
--End procedure dropdown.GetAgeRangeData

--Begin procedure dropdown.GetApplicantSourceData
EXEC Utility.DropObject 'dropdown.GetApplicantSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.ApplicantSource table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetApplicantSourceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ApplicantSourceID, 
		T.ApplicantSourceCode,
		T.ApplicantSourceName,
		T.HasOther
	FROM dropdown.ApplicantSource T
	WHERE (T.ApplicantSourceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ApplicantSourceName, T.ApplicantSourceID

END
GO
--End procedure dropdown.GetApplicantSourceData

--Begin procedure dropdown.GetCompetencyData
EXEC Utility.DropObject 'dropdown.GetCompetencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.Competency table
-- =================================================================================
CREATE PROCEDURE dropdown.GetCompetencyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CompetencyID, 
		T.CompetencyCode,
		T.CompetencyName
	FROM dropdown.Competency T
	WHERE (T.CompetencyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CompetencyName, T.CompetencyID

END
GO
--End procedure dropdown.GetCompetencyData

--Begin procedure dropdown.GetCompetencyTypeData
EXEC Utility.DropObject 'dropdown.GetCompetencyTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.CompetencyType table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetCompetencyTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CompetencyTypeID, 
		T.CompetencyTypeCode,
		T.CompetencyTypeName
	FROM dropdown.CompetencyType T
	WHERE (T.CompetencyTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CompetencyTypeName, T.CompetencyTypeID

END
GO
--End procedure dropdown.GetCompetencyTypeData

--Begin procedure dropdown.GetCSGMemberPerformanceRatingData
EXEC Utility.DropObject 'dropdown.GetCSGMemberPerformanceRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.CSGMemberPerformanceRating table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetCSGMemberPerformanceRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CSGMemberPerformanceRatingID, 
		T.CSGMemberPerformanceRatingCode,
		T.CSGMemberPerformanceRatingName
	FROM dropdown.CSGMemberPerformanceRating T
	WHERE (T.CSGMemberPerformanceRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CSGMemberPerformanceRatingName, T.CSGMemberPerformanceRatingID

END
GO
--End procedure dropdown.GetCSGMemberPerformanceRatingData

--Begin procedure dropdown.GetEthnicityData
EXEC Utility.DropObject 'dropdown.GetEthnicityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.Ethnicity table
-- ================================================================================
CREATE PROCEDURE dropdown.GetEthnicityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EthnicityID, 
		T.EthnicityCode,
		T.EthnicityName,
		T.EthnicityCategoryCode,
		T.EthnicityCategoryName,
		T.HasOther
	FROM dropdown.Ethnicity T
	WHERE (T.EthnicityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.EthnicityCategorDisplayOrder, T.EthnicityDisplayOrder, T.EthnicityName, T.EthnicityID

END
GO
--End procedure dropdown.GetEthnicityData

--Begin procedure dropdown.GetFeedbackQuestionData
EXEC Utility.DropObject 'dropdown.GetFeedbackQuestionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.FeedbackQuestion table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetFeedbackQuestionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FeedbackQuestionID, 
		T.FeedbackQuestionCode,
		T.FeedbackQuestionName,
		T.IsExternal,
		T.IsInternal
	FROM dropdown.FeedbackQuestion T
	WHERE (T.FeedbackQuestionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FeedbackQuestionName, T.FeedbackQuestionID

END
GO
--End procedure dropdown.GetFeedbackQuestionData

--Begin procedure dropdown.GetInterviewOutcomeData
EXEC Utility.DropObject 'dropdown.GetInterviewOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.InterviewOutcome table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetInterviewOutcomeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InterviewOutcomeID, 
		T.InterviewOutcomeCode,
		T.InterviewOutcomeName
	FROM dropdown.InterviewOutcome T
	WHERE (T.InterviewOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InterviewOutcomeName, T.InterviewOutcomeID

END
GO
--End procedure dropdown.GetInterviewOutcomeData

--Begin procedure dropdown.GetPersonTypeData
EXEC Utility.DropObject 'dropdown.GetPersonTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.PersonType table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetPersonTypeData

@IncludeZero BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PersonTypeID, 
		T.PersonTypeCode,
		T.PersonTypeName
	FROM dropdown.PersonType T
	WHERE (T.PersonTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PersonTypeName, T.PersonTypeID

END
GO
--End procedure dropdown.GetPersonTypeData

--Begin procedure dropdown.GetSuitabilityRatingData
EXEC Utility.DropObject 'dropdown.GetSuitabilityRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.22
-- Description:	A stored procedure to return data from the dropdown.SuitabilityRating table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetSuitabilityRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SuitabilityRatingID, 
		T.SuitabilityRatingCode,
		T.SuitabilityRatingName
	FROM dropdown.SuitabilityRating T
	WHERE (T.SuitabilityRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SuitabilityRatingName, T.SuitabilityRatingID

END
GO
--End procedure dropdown.GetSuitabilityRatingData

--Begin procedure dropdown.GetSUPerformanceRatingData
EXEC Utility.DropObject 'dropdown.GetSUPerformanceRatingData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the dropdown.SUPerformanceRating table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetSUPerformanceRatingData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SUPerformanceRatingID, 
		T.SUPerformanceRatingCode,
		T.SUPerformanceRatingName
	FROM dropdown.SUPerformanceRating T
	WHERE (T.SUPerformanceRatingID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SUPerformanceRatingName, T.SUPerformanceRatingID

END
GO
--End procedure dropdown.GetSUPerformanceRatingData

--Begin procedure dropdown.GetPersonUpdateTypeData
EXEC Utility.DropObject 'dropdown.GetPersonUpdateTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.02.12
-- Description:	A stored procedure to return data from the dropdown.PersonUpdateType table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetPersonUpdateTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PersonUpdateTypeID, 
		T.PersonUpdateTypeCode,
		T.PersonUpdateTypeName
	FROM dropdown.PersonUpdateType T
	WHERE (T.PersonUpdateTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PersonUpdateTypeName, T.PersonUpdateTypeID

END
GO
--End procedure dropdown.GetPersonUpdateTypeData

--Begin procedure dropdown.GetVacancyStatusData
EXEC Utility.DropObject 'dropdown.GetVacancyStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.VacancyStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetVacancyStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VacancyStatusID, 
		T.VacancyStatusCode,
		T.VacancyStatusName
	FROM dropdown.VacancyStatus T
	WHERE (T.VacancyStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VacancyStatusName, T.VacancyStatusID

END
GO
--End procedure dropdown.GetVacancyStatusData

--Begin procedure dropdown.GetVacancyTypeData
EXEC Utility.DropObject 'dropdown.GetVacancyTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.26
-- Description:	A stored procedure to return data from the dropdown.VacancyType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetVacancyTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VacancyTypeID, 
		T.VacancyTypeCode,
		T.VacancyTypeName
	FROM dropdown.VacancyType T
	WHERE (T.VacancyTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VacancyTypeName, T.VacancyTypeID

END
GO
--End procedure dropdown.GetVacancyTypeData

--Begin procedure eventlog.LogFeedbackAction
EXEC utility.DropObject 'eventlog.LogFeedbackAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFeedbackAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Feedback',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogFeedbackTable', 'u')) IS NOT NULL
			DROP TABLE #LogFeedbackTable
		--ENDIF
		
		SELECT *
		INTO #LogFeedbackTable
		FROM person.Feedback F
		WHERE F.FeedbackID = @EntityID

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Feedback',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Feedback'), ELEMENTS
			)
		FROM #LogFeedbackTable T
			JOIN person.Feedback F ON F.FeedbackID = T.FeedbackID

		DROP TABLE #LogFeedbackTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFeedbackAction

--Begin procedure eventlog.LogPersonUpdateAction
EXEC utility.DropObject 'eventlog.LogPersonUpdateAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PersonUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogPersonUpdateTable', 'u')) IS NOT NULL
			DROP TABLE #LogPersonUpdateTable
		--ENDIF
		
		SELECT *
		INTO #LogPersonUpdateTable
		FROM personupdate.PersonUpdate PU
		WHERE PU.PersonUpdateID = @EntityID

		DECLARE @cPersonUpdatePerson VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePerson = COALESCE(@cPersonUpdatePerson, '') + D.PersonUpdatePerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePerson'), ELEMENTS) AS PersonUpdatePerson
			FROM personupdate.Person T 
			WHERE T.PersonUpdateID = @EntityID
			) D

		DECLARE @cPersonUpdatePersonGroupPerson VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePersonGroupPerson = COALESCE(@cPersonUpdatePersonGroupPerson, '') + D.PersonUpdatePersonGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePersonGroupPerson'), ELEMENTS) AS PersonUpdatePersonGroupPerson
			FROM personupdate.PersonGroupPerson T 
			WHERE T.PersonUpdateID = @EntityID
			) D

		DECLARE @cPersonUpdatePersonPermissionable VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePersonPermissionable = COALESCE(@cPersonUpdatePersonPermissionable, '') + D.PersonUpdatePersonPermissionable 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePersonPermissionable'), ELEMENTS) AS PersonUpdatePersonPermissionable
			FROM personupdate.PersonPermissionable T 
			WHERE T.PersonUpdateID = @EntityID
			) D

		DECLARE @cPersonUpdatePersonProject VARCHAR(MAX) 
	
		SELECT 
			@cPersonUpdatePersonProject = COALESCE(@cPersonUpdatePersonProject, '') + D.PersonUpdatePersonProject 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonUpdatePersonProject'), ELEMENTS) AS PersonUpdatePersonProject
			FROM personupdate.PersonProject T 
			WHERE T.PersonUpdateID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PersonUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<PersonUpdatePerson>' + ISNULL(@cPersonUpdatePerson, '') + '</PersonUpdatePerson>') AS XML),
			CAST(('<PersonUpdatePersonGroupPerson>' + ISNULL(@cPersonUpdatePersonGroupPerson, '') + '</PersonUpdatePersonGroupPerson>') AS XML),
			CAST(('<PersonUpdatePersonPermissionable>' + ISNULL(@cPersonUpdatePersonPermissionable, '') + '</PersonUpdatePersonPermissionable>') AS XML),
			CAST(('<PersonUpdatePersonProject>' + ISNULL(@cPersonUpdatePersonProject, '') + '</PersonUpdatePersonProject>') AS XML)
			FOR XML RAW('PersonUpdate'), ELEMENTS
			)
		FROM #LogPersonUpdateTable T
			JOIN personupdate.PersonUpdate PU ON PU.PersonUpdateID = T.PersonUpdateID

		DROP TABLE #LogPersonUpdateTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonUpdateAction

--Begin procedure eventlog.LogVacancyAction
EXEC utility.DropObject 'eventlog.LogVacancyAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogVacancyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Vacancy',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogVacancyTable', 'u')) IS NOT NULL
			DROP TABLE #LogVacancyTable
		--ENDIF
		
		SELECT *
		INTO #LogVacancyTable
		FROM hrms.Vacancy V
		WHERE V.VacancyID = @EntityID

		DECLARE @cVacancyCompetency VARCHAR(MAX) 
	
		SELECT 
			@cVacancyCompetency = COALESCE(@cVacancyCompetency, '') + D.VacancyCompetency 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('VacancyCompetency'), ELEMENTS) AS VacancyCompetency
			FROM hrms.VacancyCompetency T 
			WHERE T.VacancyID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Vacancy',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<VacancyCompetency>' + ISNULL(@cVacancyCompetency, '') + '</VacancyCompetency>') AS XML)
			FOR XML RAW('Vacancy'), ELEMENTS
			)
		FROM #LogVacancyTable T
			JOIN hrms.Vacancy V ON V.VacancyID = T.VacancyID

		DROP TABLE #LogVacancyTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogVacancyAction

--Begin procedure eventlog.LogVacancyApplicationAction
EXEC utility.DropObject 'eventlog.LogVacancyApplicationAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.26
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogVacancyApplicationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'VacancyApplication',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogApplicationTable', 'u')) IS NOT NULL
			DROP TABLE #LogApplicationTable
		--ENDIF
		
		SELECT *
		INTO #LogApplicationTable
		FROM hrms.Application A
		WHERE A.ApplicationID = @EntityID

		DECLARE @cApplicationCompetency VARCHAR(MAX) 
	
		SELECT 
			@cApplicationCompetency = COALESCE(@cApplicationCompetency, '') + D.ApplicationCompetency 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationCompetency'), ELEMENTS) AS ApplicationCompetency
			FROM hrms.ApplicationCompetency T 
			WHERE T.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationEthnicity VARCHAR(MAX) 
	
		SELECT 
			@cApplicationEthnicity = COALESCE(@cApplicationEthnicity, '') + D.ApplicationEthnicity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationEthnicity'), ELEMENTS) AS ApplicationEthnicity
			FROM hrms.ApplicationEthnicity T 
			WHERE T.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationInterviewAvailability VARCHAR(MAX) 
	
		SELECT 
			@cApplicationInterviewAvailability = COALESCE(@cApplicationInterviewAvailability, '') + D.ApplicationInterviewAvailability 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationInterviewAvailability'), ELEMENTS) AS ApplicationInterviewAvailability
			FROM hrms.ApplicationInterviewAvailability T
				JOIN hrms.ApplicationPerson AP ON AP.ApplicationPersonID = T.ApplicationPersonID
					AND AP.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationInterviewSchedule VARCHAR(MAX) 
	
		SELECT 
			@cApplicationInterviewSchedule = COALESCE(@cApplicationInterviewSchedule, '') + D.ApplicationInterviewSchedule 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationInterviewSchedule'), ELEMENTS) AS ApplicationInterviewSchedule
			FROM hrms.ApplicationInterviewSchedule T 
			WHERE T.ApplicationID = @EntityID
			) D

		DECLARE @cApplicationPerson VARCHAR(MAX) 
	
		SELECT 
			@cApplicationPerson = COALESCE(@cApplicationPerson, '') + D.ApplicationPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ApplicationPerson'), ELEMENTS) AS ApplicationPerson
			FROM hrms.ApplicationPerson T 
			WHERE T.ApplicationID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'VacancyApplication',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ApplicationCompetency>' + ISNULL(@cApplicationCompetency, '') + '</ApplicationCompetency>') AS XML),
			CAST(('<ApplicationEthnicity>' + ISNULL(@cApplicationEthnicity, '') + '</ApplicationEthnicity>') AS XML),
			CAST(('<ApplicationInterviewAvailability>' + ISNULL(@cApplicationInterviewAvailability, '') + '</ApplicationInterviewAvailability>') AS XML),
			CAST(('<ApplicationInterviewSchedule>' + ISNULL(@cApplicationInterviewSchedule, '') + '</ApplicationInterviewSchedule>') AS XML),
			CAST(('<ApplicationPerson>' + ISNULL(@cApplicationPerson, '') + '</ApplicationPerson>') AS XML)
			FOR XML RAW('VacancyApplication'), ELEMENTS
			)
		FROM #LogApplicationTable T
			JOIN hrms.Application V ON V.ApplicationID = T.ApplicationID

		DROP TABLE #LogApplicationTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogVacancyApplicationAction

--Begin procedure hrms.GetApplicationByApplicationID
EXEC utility.DropObject 'hrms.GetApplicationByApplicationID'
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.23
-- Description:	A stored procedure to return data from the hrms.Application table
-- ==============================================================================
CREATE PROCEDURE hrms.GetApplicationByApplicationID

@ApplicationID INT,
@PersonID INT,
@VacancyID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @ApplicationID = 0
		BEGIN

		--Application
		SELECT
			0 AS ApplicationID,
			NULL AS ConflictsOfInterest,
			0 AS HasCertifiedApplication,
			0 AS HasConfirmedTerminationDate,
			CASE WHEN P.DAPersonID > 0 THEN 1 ELSE 0 END AS HasDAAccount,
			0 AS IsGovernmetEmployee,
			0 AS IsGuaranteedInterview,
			0 AS IsApplicationFinal,
			0 AS IsNotCriminalCivilService,
			0 AS IsNotCriminalPolice,
			0 AS IsDisabled,
			0 AS IsDueDilligenceComplete,
			0 AS IsEligible,
			0 AS IsHermisProfileUpdated,
			0 AS IsHRContactNotified,
			0 AS IsInterviewAvailableAMDay1,
			0 AS IsInterviewAvailableAMDay2,
			0 AS IsInterviewAvailableAMDay3,
			0 AS IsInterviewAvailableAMDay4,
			0 AS IsInterviewAvailableAMDay5,
			0 AS IsInterviewAvailableAMDay6,
			0 AS IsInterviewAvailableAMDay7,
			0 AS IsInterviewAvailablePMDay1,
			0 AS IsInterviewAvailablePMDay2,
			0 AS IsInterviewAvailablePMDay3,
			0 AS IsInterviewAvailablePMDay4,
			0 AS IsInterviewAvailablePMDay5,
			0 AS IsInterviewAvailablePMDay6,
			0 AS IsInterviewAvailablePMDay7,
			0 AS IsInterviewComplete,
			0 AS IsLineManagerNotified,
			0 AS IsOfferAccepted,
			0 AS IsOfferExtended,
			0 AS IsOnRoster,
			0 AS IsPreQualified,
			0 AS IsVetted,
			NULL AS ManagerComments,
			@PersonID AS PersonID,
			NULL AS SubmittedDateTime,
			NULL AS SubmittedDateTimeFormatted,
			NULL AS Suitability,
			NULL AS TechnicalManagerComments,
			NULL AS TerminationDate,
			NULL AS TerminationDateFormatted,
			@VacancyID AS VacancyID,
			0 AS AgeRangeID,
			NULL AS AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			0 AS ExpertCategoryID,
			NULL AS ExpertCategoryName,
			0 AS GenderID,
			NULL AS GenderName,
			0 AS InterviewOutcomeID,
			NULL AS InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName		
		FROM person.Person P
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND P.PersonID = @PersonID

		END
	ELSE
		BEGIN

		SELECT 
			@PersonID = A.PersonID,
			@VacancyID = A.VacancyID
		FROM hrms.Application A 
		WHERE A.ApplicationID = @ApplicationID

		--Application
		SELECT
			A.ApplicationID,
			A.ConflictsOfInterest,
			A.HasCertifiedApplication,
			A.HasConfirmedTerminationDate,
			A.HasDAAccount,
			A.IsGuaranteedInterview,
			A.IsApplicationFinal,
			A.IsNotCriminalCivilService,
			A.IsNotCriminalPolice,
			A.IsDisabled,
			A.IsDueDilligenceComplete,
			A.IsEligible,
			A.IsGovernmetEmployee,
			A.IsHermisProfileUpdated,
			A.IsHRContactNotified,
			A.IsInterviewAvailableAMDay1,
			A.IsInterviewAvailableAMDay2,
			A.IsInterviewAvailableAMDay3,
			A.IsInterviewAvailableAMDay4,
			A.IsInterviewAvailableAMDay5,
			A.IsInterviewAvailableAMDay6,
			A.IsInterviewAvailableAMDay7,
			A.IsInterviewAvailablePMDay1,
			A.IsInterviewAvailablePMDay2,
			A.IsInterviewAvailablePMDay3,
			A.IsInterviewAvailablePMDay4,
			A.IsInterviewAvailablePMDay5,
			A.IsInterviewAvailablePMDay6,
			A.IsInterviewAvailablePMDay7,
			A.IsInterviewComplete,
			A.IsLineManagerNotified,
			A.IsOfferAccepted,
			A.IsOfferExtended,
			A.IsOnRoster,
			A.IsPreQualified,
			A.IsVetted,
			A.ManagerComments,
			A.PersonID,
			A.SubmittedDateTime,
			core.FormatDate(A.SubmittedDateTime) AS SubmittedDateTimeFormatted,
			A.Suitability,
			A.TechnicalManagerComments,
			A.TerminationDate,
			core.FormatDate(A.TerminationDate) AS TerminationDateFormatted,
			A.VacancyID,
			AR.AgeRangeID,
			AR.AgeRangeName,
			NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
			CCC1.CountryCallingCodeID,
			NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
			CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
			NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
			CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
			NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
			CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
			NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
			CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
			EC.ExpertCategoryID,
			EC.ExpertCategoryName,
			G.GenderID,
			G.GenderName,
			IO.InterviewOutcomeID,
			IO.InterviewOutcomeName,
			P.ApplicantSourceOther,
			P.CellPhone,
			P.CivilServiceDepartment,
			P.CivilServiceGrade1,
			P.CivilServiceGrade2,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
			dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
			P.EmailAddress,
			P.FirstName,
			P.HomePhone,
			P.HRContactEmailAddress,
			P.HRContactName,
			P.HRContactPhone,
			P.IsPreviousApplicant,
			P.IsUKEUNational,
			P.IsUKResidencyEligible,
			P.LastContractEndDate,
			core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
			P.LastName,
			P.LineManagerEmailAddress,
			P.LineManagerName,
			P.LineManagerPhone,
			P.MiddleName,
			P.NickName,
			P.OtherGrade,
			P.PersonID,
			P.PreviousApplicationDate,
			core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
			P.SecurityClearanceSponsorName,
			P.Suffix,
			P.Title,
			P.WorkPhone,
			PF.PoliceForceID,
			PF.PoliceForceName,
			PR1.PoliceRankID AS PoliceRankID1,
			PR1.PoliceRankName AS PoliceRankName1,
			PR2.PoliceRankID AS PoliceRankID2,
			PR2.PoliceRankName AS PoliceRankName2,
			PT.PersonTypeID,
			PT.PersonTypeCode,
			SC.SecurityClearanceID,
			SC.SecurityClearanceName		
		FROM hrms.Application A
			JOIN person.Person P ON P.PersonID = A.PersonID
			JOIN dropdown.AgeRange AR ON AR.AgeRangeID = A.AgeRangeID
			JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
			JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
			JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = A.ExpertCategoryID
			JOIN dropdown.Gender G ON G.GenderID = A.GenderID
			JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = A.InterviewOutcomeID
			JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
			JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
			JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
			JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
			JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
				AND A.ApplicationID = @ApplicationID

		END
	--ENDIF

	--ApplicationDocumentCV
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Application'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @ApplicationID

	--ApplicationDocumentPassport
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Application'
			AND DE.EntityTypeSubCode = 'Passport'
			AND DE.EntityID = @ApplicationID

	--ApplicationEthnicity
	SELECT 
		E.EthnicityID,
		E.EthnicityName,
		AE.EthnicityDescription
	FROM hrms.ApplicationEthnicity AE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = AE.EthnicityID
			AND AE.ApplicationID = @ApplicationID
	ORDER BY E.EthnicityName, E.EthnicityID

	--ApplicationInterviewAvailability
	SELECT 
		AIA.ApplicationInterviewAvailabilityID,
		AIA.ApplicationInterviewScheduleID,
		AIA.ApplicationPersonID,
		AIA.IsAvailable,
		core.yesNoFormat(AIA.IsAvailable) AS IsAvailableYesNoFormat
	FROM hrms.ApplicationInterviewAvailability AIA
	WHERE AIA.ApplicationPersonID IN (SELECT AP.ApplicationPersonID FROM hrms.ApplicationPerson AP WHERE AP.ApplicationID = @ApplicationID)

	--ApplicationInterviewSchedule
	SELECT 
		AIS.ApplicationInterviewScheduleID,
		AIS.InterviewDateTime,
		core.FormatDateTime(AIS.InterviewDateTime) AS InterviewDateTimeFormatted,
		AIS.IsSelectedDate,
		core.YesNoFormat(AIS.IsSelectedDate) AS IsSelectedDateYesNoFormat
	FROM hrms.ApplicationInterviewSchedule AIS
	WHERE AIS.ApplicationID = @ApplicationID

	--ApplicationPerson
	SELECT 
		AP.ApplicationID,
		AP.ApplicationPersonID,
		AP.InterviewOutcomeID,
		AP.IsScheduleConfirmed,
		core.YesNoFormat(AP.IsScheduleConfirmed) AS IsScheduleConfirmedYesNoFormat,
		AP.PersonID,
		person.FormatPersonNameByPersonID(AP.PersonID, 'FirstLast') AS PersonNameFormatted,
		AP.PersonTypeCode,
		AP.ProposesInterviewDates,
		core.YesNoFormat(AP.ProposesInterviewDates) AS ProposesInterviewDatesYesNoFormat,
		IO.InterviewOutcomeName,
		P.FirstName,
		P.LastName
	FROM hrms.ApplicationPerson AP
		JOIN person.Person P ON P.PersonID = AP.PersonID
		JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = AP.InterviewOutcomeID
			AND AP.ApplicationID = @ApplicationID
	ORDER BY P.LastName, P.FirstName

	--PersonApplicantSource
	SELECT
		APS.ApplicantSourceID,
		APS.ApplicantSourceName
	FROM person.PersonApplicantSource PAS
		JOIN dropdown.ApplicantSource APS ON APS.ApplicantSourceID = PAS.ApplicantSourceID
			AND PAS.PersonID = @PersonID
	ORDER BY APS.ApplicantSourceName, APS.ApplicantSourceID

	--PersonCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonCountry PC
		JOIN dropdown.Country C ON C.CountryID = PC.CountryID
			AND PC.PersonID = @PersonID
	ORDER BY C.CountryName, C.CountryID

	--PersonLanguage
	SELECT
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--Vacancy
	SELECT
		EC.ExpertCategoryName,
		P.ProjectName,
		PS.ProjectSponsorName,
		V.ProjectID,
		V.VacancyName,
		V.VacancyReference,
		VT.VacancyTypeName
	FROM hrms.Vacancy V
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = V.ExpertCategoryID
		JOIN dropdown.Project P ON P.ProjectID = V.ProjectID
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
		JOIN dropdown.VacancyType VT ON VT.VacancyTypeID = V.VacancyTypeID
			AND V.VacancyID = @VacancyID

	--VacancyCompetency
	SELECT
		C.CompetencyDescription,
		C.CompetencyID,
		C.CompetencyName,
		OAAC.Suitability,
		ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'Competency'
			AND C.IsActive = 1
		OUTER APPLY
			(
			SELECT
				AC.Suitability,
				AC.SuitabilityRatingID
			FROM hrms.ApplicationCompetency AC
			WHERE AC.CompetencyID = C.CompetencyID
				AND AC.ApplicationID = @ApplicationID
			) OAAC
	ORDER BY C.CompetencyName, C.CompetencyID

	--VacancyTechnicalCapability
	SELECT
		C.CompetencyDescription,
		C.CompetencyID,
		C.CompetencyName,
		OAAC.Suitability,
		ISNULL(OAAC.SuitabilityRatingID, 0) AS SuitabilityRatingID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'TechnicalCapability'
			AND C.IsActive = 1
		OUTER APPLY
			(
			SELECT
				AC.Suitability,
				AC.SuitabilityRatingID
			FROM hrms.ApplicationCompetency AC
			WHERE AC.CompetencyID = C.CompetencyID
				AND AC.ApplicationID = @ApplicationID
			) OAAC
	ORDER BY C.CompetencyName, C.CompetencyID

END
GO
--End procedure hrms.GetApplicationByApplicationID

--Begin procedure hrms.GetCompetencyByCompetencyID
EXEC utility.DropObject 'hrms.GetCompetencyByCompetencyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:2019.01.26
-- Description:	A stored procedure to get data from the hrms.Competency table
-- ================================================================================
CREATE PROCEDURE [hrms].[GetCompetencyByCompetencyID]

@CompetencyID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Competency
	SELECT
		C.CompetencyDescription,
		C.CompetencyID,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeCode,
		CT.CompetencyTypeName
	FROM hrms.Competency C
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND C.CompetencyID = @CompetencyID

END 
GO
--End procedure hrms.GetCompetencyByCompetencyID

--Begin procedure hrms.GetVacancyByVacancyID
EXEC utility.DropObject 'hrms.GetVacancyByVacancyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:2019.01.26
-- Description:	A stored procedure to get data from the hrms.Vacancy table
-- ================================================================================
CREATE PROCEDURE hrms.GetVacancyByVacancyID

@VacancyID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Vacancy', @VacancyID)

	--Vacancy
	SELECT
		EC.ExpertCategoryName,
		P.ProjectName,
		person.FormatPersonNameByPersonID(V.CoordinatorPersonID, 'FirstLast') AS CoordinatorPersonNameFormatted,
		PS.ProjectSponsorName,
		V.CloseDateTime,
		core.FormatDateTime(V.CloseDateTime) AS CloseDateTimeFormatted,
		iif(
			OpenDateTime IS NULL OR CloseDateTime IS NULL OR OpenDateTime > getDate(), 0, 
			iif(CloseDateTime<getDate(),
				DATEDIFF(d, OpenDateTime, CloseDateTime),
				DATEDIFF(d, OpenDateTime, getDate())
			)
		) AS DaysAdvertised,
		V.CoordinatorPersonID,
		V.ExpertCategoryID,
		V.OpenDateTime,
		core.FormatDateTime(V.OpenDateTime) AS OpenDateTimeFormatted,
		V.ProjectID,
		V.RoleDescription,
		V.VacancyContactEmailAddress,
		V.VacancyContactName,
		V.VacancyContactPhone,
		V.VacancyID,
		(SELECT COUNT(A.PersonID) FROM hrms.Application A WHERE A.VacancyID = V.VacancyID AND A.SubmittedDateTime IS NOT NULL) AS ApplicationCount,
		V.VacancyName,
		V.VacancyNotes,
		V.VacancyReference,
		V.VacancyStatusID,
		V.VacancyTypeID,
		VS.VacancyStatusCode,
		VS.VacancyStatusName,
		VT.VacancyTypeName
	FROM hrms.Vacancy V
		JOIN dropdown.Project P ON P.ProjectID = V.ProjectID
		JOIN dropdown.VacancyStatus VS ON VS.VacancyStatusID = V.VacancyStatusID
		JOIN dropdown.VacancyType VT ON VT.VacancyTypeID = V.VacancyTypeID
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = V.ExpertCategoryID
		JOIN dropdown.ProjectSponsor PS ON PS.ProjectSponsorID = P.ProjectSponsorID
			AND V.VacancyID = @VacancyID

	--VacancyApplication
	SELECT
		A.ApplicationID,
		A.ConflictsOfInterest,
		A.HasCertifiedApplication,
		A.HasDAAccount,
		A.IsGuaranteedInterview,
		A.IsApplicationFinal,
		A.IsNotCriminalCivilService,
		A.IsNotCriminalPolice,
		A.IsDisabled,
		A.IsDueDilligenceComplete,
		A.IsEligible,
		A.IsHermisProfileUpdated,
		A.IsHRContactNotified,
		A.IsInterviewAvailableAMDay1,
		A.IsInterviewAvailableAMDay2,
		A.IsInterviewAvailableAMDay3,
		A.IsInterviewAvailableAMDay4,
		A.IsInterviewAvailableAMDay5,
		A.IsInterviewAvailableAMDay6,
		A.IsInterviewAvailableAMDay7,
		A.IsInterviewAvailablePMDay1,
		A.IsInterviewAvailablePMDay2,
		A.IsInterviewAvailablePMDay3,
		A.IsInterviewAvailablePMDay4,
		A.IsInterviewAvailablePMDay5,
		A.IsInterviewAvailablePMDay6,
		A.IsInterviewAvailablePMDay7,
		A.IsInterviewComplete,
		A.IsLineManagerNotified,
		A.IsOfferAccepted,
		A.IsOfferExtended,
		A.IsOnRoster,
		A.IsPreQualified,
		A.IsVetted,
		A.ManagerComments,
		A.PersonID,
		A.SubmittedDateTime,
		core.FormatDate(A.SubmittedDateTime) AS SubmittedDateTimeFormatted,
		A.Suitability,
		A.TechnicalManagerComments,
		A.VacancyID,
		AR.AgeRangeID,
		AR.AgeRangeName,
		NULLIF(CCC1.CountryCallingCode, 0) AS CountryCallingCode,
		CCC1.CountryCallingCodeID,
		NULLIF(CCC2.CountryCallingCode, 0) AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		NULLIF(CCC3.CountryCallingCode, 0) AS LineManagerPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS LineManagerPhoneCountryCallingCodeID,
		NULLIF(CCC4.CountryCallingCode, 0) AS WorkPhoneCountryCallingCode,
		CCC4.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		NULLIF(CCC5.CountryCallingCode, 0) AS HRContactPhoneCountryCallingCode,
		CCC5.CountryCallingCodeID AS HRContactPhoneCountryCallingCodeID,
		EC.ExpertCategoryID,
		EC.ExpertCategoryName,
		G.GenderID,
		G.GenderName,
		IO.InterviewOutcomeID,
		IO.InterviewOutcomeName,
		P.ApplicantSourceOther,
		P.CellPhone,
		P.CivilServiceDepartment,
		P.CivilServiceGrade1,
		P.CivilServiceGrade2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.EmailAddress,
		P.FirstName,
		P.HomePhone,
		P.HRContactEmailAddress,
		P.HRContactName,
		P.HRContactPhone,
		P.IsPreviousApplicant,
		P.IsUKEUNational,
		P.IsUKResidencyEligible,
		P.LastContractEndDate,
		core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
		P.LastName,
		P.LineManagerEmailAddress,
		P.LineManagerName,
		P.LineManagerPhone,
		P.MiddleName,
		P.NickName,
		P.OtherGrade,
		P.PersonID,
		P.PreviousApplicationDate,
		core.FormatDate(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
		P.SecurityClearanceSponsorName,
		P.Suffix,
		P.Title,
		P.WorkPhone,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR1.PoliceRankID AS PoliceRankID1,
		PR1.PoliceRankName AS PoliceRankName1,
		PR2.PoliceRankID AS PoliceRankID2,
		PR2.PoliceRankName AS PoliceRankName2,
		PT.PersonTypeID,
		PT.PersonTypeCode,
		SC.SecurityClearanceID,
		SC.SecurityClearanceName,
		CASE
			WHEN workflow.GetWorkflowStepNumber('Application', A.ApplicationID) > workflow.GetWorkflowStepCount('Application', A.ApplicationID)
			THEN 'Approved'
			ELSE 'Step ' + CAST(workflow.GetWorkflowStepNumber('Application', A.ApplicationID) AS VARCHAR(5)) + ' of ' + CAST(workflow.GetWorkflowStepCount('Application', A.ApplicationID) AS VARCHAR(5)) 
		END AS WorkflowStep
	FROM hrms.Application A
		JOIN person.Person P ON P.PersonID = A.PersonID
		JOIN dropdown.AgeRange AR ON AR.AgeRangeID = A.AgeRangeID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.LineManagerCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC4 ON CCC4.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC5 ON CCC5.CountryCallingCodeID = P.HRContactCountryCallingCodeID
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = A.ExpertCategoryID
		JOIN dropdown.Gender G ON G.GenderID = A.GenderID
		JOIN dropdown.InterviewOutcome IO ON IO.InterviewOutcomeID = A.InterviewOutcomeID
		JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
		JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = P.SecurityClearanceID
			WHERE A.VacancyID = @VacancyID
			ORDER BY P.LastName, P.FirstName

	--VacancyCompetency
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		VC.CompetencyID,
		VC.VacancyCompetencyID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'Competency'

	--VacancyDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Vacancy'
			AND DE.EntityID = @VacancyID

	--VacancyTechnicalCapability
	SELECT
		C.CompetencyDescription,
		C.CompetencyName,
		C.CompetencyTypeID,
		C.IsActive,
		core.YesNoFormat(C.IsActive) AS IsActiveYesNoFormat,
		CT.CompetencyTypeName,
		VC.CompetencyID,
		VC.VacancyCompetencyID
	FROM hrms.VacancyCompetency VC
		JOIN hrms.Competency C ON C.CompetencyID = VC.CompetencyID
		JOIN dropdown.CompetencyType CT ON CT.CompetencyTypeID = C.CompetencyTypeID
			AND VC.VacancyID = @VacancyID
			AND CT.CompetencyTypeCode = 'TechnicalCapability'
	
	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'Vacancy', @VacancyID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Vacancy', @VacancyID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Vacancy'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Vacancy'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Vacancy'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Vacancy'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'Vacancy'
		AND EL.EntityID = @VacancyID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure hrms.GetVacancyByVacancyID

--Begin procedure person.CheckAccess
EXEC utility.DropObject 'person.CheckAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.27
-- Description:	A stored procedure to check access to an entity based on permissions and project access
-- ====================================================================================================
CREATE PROCEDURE person.CheckAccess

@EntityTypeCode VARCHAR(50),
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN

	DECLARE @bHasAccess BIT

	IF @EntityTypeCode = 'Activity'
		SELECT @bHasAccess = 1 FROM activity.Activity T WHERE T.ActivityID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ActivityReport'
		SELECT @bHasAccess = 1 FROM activityreport.ActivityReport T WHERE T.ActivityReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Asset'
		SELECT @bHasAccess = 1 FROM asset.Asset T WHERE T.AssetID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Contact'
		SELECT @bHasAccess = 1 FROM contact.Contact T WHERE T.ContactID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Course'
		SELECT @bHasAccess = 1 FROM training.Course T WHERE T.CourseID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Document'
		SELECT @bHasAccess = 1 FROM document.Document T WHERE T.DocumentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentCatalog'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentCatalog T WHERE T.EquipmentCatalogID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentItem'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentItem T WHERE T.EquipmentItemID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentOrder'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentOrder T WHERE T.EquipmentOrderID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'EquipmentMovement'
		SELECT @bHasAccess = 1 FROM procurement.EquipmentMovement T WHERE T.EquipmentMovementID = @EntityID
	ELSE IF @EntityTypeCode = 'Feedback'
		SELECT @bHasAccess = 1 FROM person.Feedback T WHERE T.FeedbackID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Force'
		SELECT @bHasAccess = 1 FROM force.Force T WHERE T.ForceID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Incident'
		SELECT @bHasAccess = 1 FROM core.Incident T WHERE T.IncidentID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Module'
		SELECT @bHasAccess = 1 FROM training.Module T WHERE T.ModuleID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'PersonUpdate'
		SELECT @bHasAccess = 1 FROM personupdate.PersonUpdate PU WHERE PU.PersonUpdateID = @EntityID
	ELSE IF @EntityTypeCode = 'ReportUpdate'
		SELECT @bHasAccess = 1 FROM core.ReportUpdate T WHERE T.ReportUpdateID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'ResponseDashboard'
		SELECT @bHasAccess = 1 FROM responsedashboard.ResponseDashboard T WHERE T.ResponseDashboardID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'SpotReport'
		SELECT @bHasAccess = 1 FROM spotreport.SpotReport T WHERE T.SpotReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Territory'
		SELECT @bHasAccess = 1 FROM territory.Territory T JOIN dropdown.TerritoryType TT ON TT.TerritoryTypeCode = T.TerritoryTypeCode AND T.TerritoryID = @EntityID AND TT.IsReadOnly = 0
	ELSE IF @EntityTypeCode = 'TrendReport'
		SELECT @bHasAccess = 1 FROM trendreport.TrendReport T WHERE T.TrendReportID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = T.ProjectID)
	ELSE IF @EntityTypeCode = 'Vacancy'
		SELECT @bHasAccess = 1 FROM hrms.Vacancy V WHERE V.VacancyID = @EntityID AND EXISTS (SELECT 1 FROM person.PersonProject PP WHERE PP.PersonID = @PersonID AND PP.ProjectID = V.ProjectID)
	--ENDIF

	SELECT ISNULL(@bHasAccess, 0) AS HasAccess

END
GO
--End procedure person.CheckAccess

--Begin procedure person.GetFeedbackByFeedbackID
EXEC Utility.DropObject 'person.GetFeedbackByFeedbackID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.19
-- Description:	A stored procedure to return data from the person.Feedback table
-- =============================================================================
CREATE PROCEDURE person.GetFeedbackByFeedbackID

@FeedbackID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('Feedback', @FeedbackID)

	--Feedback
	SELECT
		F.CreateDate,
		core.FormatDate(F.CreateDate) AS CreateDateFormatted,
		F.CreatePersonID,
		person.FormatPersonNameByPersonID(F.CreatePersonID, 'LastFirst') AS CreatePersonNameFormated,
		F.CSGMemberComment,
		F.CSGMemberPersonID,
		person.FormatPersonNameByPersonID(F.CSGMemberPersonID, 'LastFirst') AS CSGMemberPersonNameFormated,
		F.EndDate,
		core.FormatDate(F.EndDate) AS EndDateFormatted,
		F.FeedbackID,
		F.FeedbackName,
		F.IsExternal,
		F.Question4,
		F.Question5,
		F.Question7,
		F.StartDate,
		core.FormatDate(F.StartDate) AS StartDateFormatted,
		F.TaskID,
		MPR.CSGMemberPerformanceRatingCode,
		MPR.CSGMemberPerformanceRatingID,
		MPR.CSGMemberPerformanceRatingName,
		P.ProjectID,
		P.ProjectName,
		PR1.SUPerformanceRatingCode AS SUPerformanceRatingCode1,
		PR1.SUPerformanceRatingID AS SUPerformanceRatingID1,
		PR1.SUPerformanceRatingName AS SUPerformanceRatingName1,
		PR2.SUPerformanceRatingCode AS SUPerformanceRatingCode2,
		PR2.SUPerformanceRatingID AS SUPerformanceRatingID2,
		PR2.SUPerformanceRatingName AS SUPerformanceRatingName2,
		PR3.SUPerformanceRatingCode AS SUPerformanceRatingCode3,
		PR3.SUPerformanceRatingID AS SUPerformanceRatingID3,
		PR3.SUPerformanceRatingName AS SUPerformanceRatingName3
	FROM person.Feedback F
		JOIN dropdown.CSGMemberPerformanceRating MPR ON MPR.CSGMemberPerformanceRatingID = F.CSGMemberPerformanceRatingID
		JOIN dropdown.Project P ON P.ProjectID = F.ProjectID
		JOIN dropdown.SUPerformanceRating PR1 ON PR1.SUPerformanceRatingID = F.SUPerformanceRatingID1
		JOIN dropdown.SUPerformanceRating PR2 ON PR2.SUPerformanceRatingID = F.SUPerformanceRatingID2
		JOIN dropdown.SUPerformanceRating PR3 ON PR3.SUPerformanceRatingID = F.SUPerformanceRatingID3
			AND F.FeedbackID = @FeedbackID

	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'Feedback', @FeedbackID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'Feedback', @FeedbackID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Feedback'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Feedback'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Feedback'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Feedback'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode LIKE '%Feedback'
		AND EL.EntityID = @FeedbackID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure person.GetFeedbackByFeedbackID

--Begin procedure person.GetMedicalVettingEmailData
EXEC Utility.DropObject 'person.GetMedicalVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.16
-- Description:	A stored procedure to return data for outgoing medical vetting e-mail
-- ==================================================================================
CREATE PROCEDURE person.GetMedicalVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@MedicalClearanceTypeIDList VARCHAR(MAX),
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonMedicalClearanceProcessStep
		(PersonID, MedicalClearanceTypeID, MedicalClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		CAST(LTT2.ListItem AS INT),
		(SELECT MCPS.MedicalClearanceProcessStepID FROM dropdown.MedicalClearanceProcessStep MCPS WHERE MCPS.MedicalClearanceProcessStepCode = 'InPreparation'),
		getDate(),
		1,
		'Medical clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT1 ON CAST(LTT1.ListItem AS INT) = P.PersonID
		CROSS APPLY core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT2

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		(SELECT 
			REPLACE(STUFF((
				SELECT ',' + MedicalClearanceTypeName
				FROM dropdown.MedicalClearanceType MCT
					JOIN core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MCT.MedicalClearanceTypeID
				ORDER BY 1
				FOR XML PATH('')), 1, 1, ''), ',', ' and ')) AS MedicalClearanceTypeName
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		0 AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL AS MedicalClearanceTypeName

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetMedicalVettingEmailData

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.19
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.26
-- Description: Added PersonPersonProject to the select list
--
-- Author:			Justin Branum
-- Create Date: 2019.01.29
-- Description: Added IsOnSURoster to the select list
--
-- Author:			Jonathan Burnham
-- Create Date: 2019.01.30
-- Description: Added perosn record fields for personupdate workflow
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC1.CountryCallingCode,
		CCC1.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.ApplicantSourceOther,
		P.ApplicantTypeCode,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CivilServiceDepartment,
		P.CivilServiceGrade1,
		P.CivilServiceGrade2,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAAccountDecomissionedDate,
		core.FormatDate(P.DAAccountDecomissionedDate) AS DAAccountDecomissionedDateFormatted,
		P.DAPersonID,
		IIF(P.DAPersonID > 0, 'Yes ', 'No ') AS HasDAAccountYesNoFormat,
		P.DaysToRemoval,
		P.DefaultProjectID,
		P.DeleteByPersonID,
		person.FormatPersonNameByPersonID(P.DeleteByPersonID, 'FirstLast') AS DeleteByPersonNameFormatted,
		P.DeleteDate,
		core.FormatDate(P.DeleteDate) AS DeleteDateFormatted,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FARemoveByPersonID,
		person.FormatPersonNameByPersonID(P.FARemoveByPersonID, 'FirstLast') AS FARemoveByPersonNameFormatted,
		P.FARemoveDate,
		core.FormatDate(P.FARemoveDate) AS FARemoveDateFormatted,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.HRContactCountryCallingCodeID,
		P.HRContactEmailAddress,
		P.HRContactName,
		P.HRContactPhone,
		P.InvalidLoginAttempts,
		P.InvoiceLimit,
		P.IsAccountLockedOut,
		P.IsActive,
		P.ISDAAccountDecomissioned,
		IIF(P.ISDAAccountDecomissioned = 1, 'Yes', 'No') AS ISDAAccountDecomissionedYesNoFormat,
		P.IsPhoneVerified,		
		P.IsPreviousApplicant,
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKResidencyEligible,
		P.IsUKWorkEligible,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastContractEndDate,
		core.FormatDate(P.LastContractEndDate) AS LastContractEndDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.LineManagerCountryCallingCodeID,
		P.LineManagerEmailAddress,
		P.LineManagerName,
		P.LineManagerPhone,
		P.LockOutByPersonID,
		person.FormatPersonNameByPersonID(P.LockOutByPersonID, 'FirstLast') AS LockOutByPersonNameFormatted,
		P.LockOutDate,
		core.FormatDate(P.LockOutDate) AS LockOutDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.OtherGrade,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'Humanitarian'), 0) AS IsOnHSOTRoster,
		ISNULL((SELECT 1 FROM person.PersonClientRoster PCR JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID	AND PCR.PersonID = P.PersonID AND CR.ClientRosterCode = 'SU'), 0) AS IsOnSURoster,
		ISNULL((SELECT TOP 1 SC.SecurityClearanceName + ' - ' + core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID AND PSC.IsActive = 1 AND PSC.PersonID = P.PersonID), 'None') AS Vetting,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.PreviousApplicationDate,
		core.FormatDateTime(P.PreviousApplicationDate) AS PreviousApplicationDateFormatted,
		P.PurchaseOrderLimit,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.SecurityClearanceID,
		P.SecurityClearanceSponsorName,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR1.PoliceRankID AS PoliceRankID1,
		PR1.PoliceRankName AS PoliceRankName1,
		PR2.PoliceRankID AS PoliceRankID2,
		PR2.PoliceRankName AS PoliceRankName2,
		PT.PersonTypeCode,
		PT.PersonTypeID,
		PT.PersonTypeName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PersonType PT ON PT.PersonTypeID = P.PersonTypeID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR1 ON PR1.PoliceRankID = P.PoliceRankID1
		JOIN dropdown.PoliceRank PR2 ON PR2.PoliceRankID = P.PoliceRankID2
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

	--PersonClientRoster
	SELECT 
		CR.ClientRosterCode,
		CR.ClientRosterName,
		ES.ExpertStatusID,
		ES.ExpertStatusName,
		PCR.ClientRosterID,
		PCR.ExpertStatusID,
		PCR.PersonClientRosterID,
		PCR.TerminationDate,
		core.FormatDate(PCR.TerminationDate) AS TerminationDateFormatted,
		PCR.TerminationReason
	FROM person.PersonClientRoster PCR
		JOIN dropdown.ClientRoster CR ON CR.ClientRosterID = PCR.ClientRosterID
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PCR.ExpertStatusID
			AND PCR.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'PersonCorrespondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEC
		JOIN dropdown.Country C ON C.CountryID = PEC.CountryID
			AND PEC.PersonID = @PersonID
	ORDER BY C.CountryName, C.CountryID

	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonGroupPerson
	SELECT 
		PGP.PersonGroupPersonID,
		PGP.PersonGroupID,
		PGP.CreateDateTime,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateFormatted,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PGP2.PersonID) FROM person.PersonGroupPerson PGP2 WHERE PGP2.PersonGroupID = PG.PersonGroupID) AS UserCount,
		PG.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName
	FROM person.PersonGroupPerson PGP
		JOIN person.PersonGroup PG ON PG.PersonGroupID = PGP.PersonGroupID
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PGP.PersonID = @PersonID

	--PersonIR35
	SELECT
		newID() AS PersonIR35GUID,
		PIR.TaskName,
		PIR.IR35StatusDate,
		PIR.IR35StatusID,
		core.FormatDate(PIR.IR35StatusDate) AS IR35StatusDateFormatted,
		PIR.PersonIR35ID,
		PIR.Issues,
		IRS.IR35StatusName
	FROM person.PersonIR35 PIR
		JOIN dropdown.IR35Status IRS ON IRS.IR35StatusID = PIR.IR35StatusID
			AND PIR.PersonID = @PersonID
	ORDER BY PIR.TaskName, PIR.PersonIR35ID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonUsernameByEmailAddress
EXEC utility.DropObject 'person.GetPersonUsernameByEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.04.07
-- Description:	A stored procedure to return a username from the person.Person table based on an email address
-- ===========================================================================================================
CREATE PROCEDURE person.GetPersonUsernameByEmailAddress

@EmailAddress VARCHAR(320)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.UserName,
		P.EmailAddress
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress

END
GO
--End procedure person.GetPersonUsernameByEmailAddress

--Begin procedure person.GetVettingEmailData
EXEC Utility.DropObject 'person.GetVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data for outgoing vetting e-mail
-- ==========================================================================
CREATE PROCEDURE person.GetVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@SecurityClearanceID INT,
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonSecurityClearanceProcessStep
		(PersonID, SecurityClearanceID, SecurityClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		@SecurityClearanceID,
		(SELECT SCPS.SecurityClearanceProcessStepID FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = 'PREP'),
		getDate(),
		1,
		'Security clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.DiscretionaryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS DiscretionaryReviewDateFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS MandatoryReviewDateFormatted
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		0 AS PersonID,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL,
		NULL

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetVettingEmailData

--Begin procedure person.SaveDAPersonClientRoster
EXEC utility.DropObject 'person.SaveDAPersonClientRoster'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date: 2016.11.01
-- Description: A stored procedure to add data to the person.PersonClientRoster table
-- ==================================================================================
CREATE PROCEDURE person.SaveDAPersonClientRoster

@cJSONData VARCHAR(MAX) 

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonClientRoster
		(PersonID, ClientRosterID)
	SELECT
		P.PersonID,
		CR.ClientRosterID
	FROM OPENJSON(@cJSONData)
	WITH
		(
		DAPersonID INT '$.DAPersonID',
		ClientRosterCode VARCHAR(50) '$.ClientRosterCode'
		) DAPCR
		JOIN Person.Person P ON P.DAPersonID = DAPCR.DAPersonID
		JOIN dropdown.ClientRoster CR on CR.ClientRosterCode = DAPCR.ClientRosterCode
			AND NOT EXISTS	
				(
				SELECT 1
				FROM person.PersonClientRoster PCR
				WHERE PCR.PersonID = P.PersonID
					AND PCR.ClientRosterID = CR.ClientRosterID
				)

END
GO
--End procedure person.SaveDAPersonClientRoster

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cCellPhone VARCHAR(64)
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRequiredProfileUpdate VARCHAR(250) = ''
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nDAPersonID INT
	DECLARE @nDefaultProjectID INT = 0
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		CellPhone VARCHAR(64),
		CountryCallingCodeID INT,
		DAPersonID INT NOT NULL DEFAULT 0,
		DefaultProjectID INT,
		EmailAddress VARCHAR(320),
		FullName VARCHAR(250),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsPhoneVerified BIT,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		RequiredProfileUpdate VARCHAR(250),
		RoleName VARCHAR(50),
		UserName VARCHAR(250)
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT core.GetSystemSetupValueBySystemSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsPasswordExpired = CASE WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime() THEN 1 ELSE 0 END,
		@bIsPhoneVerified = P.IsPhoneVerified,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN core.NullIfEmpty(P.EmailAddress) IS NULL
					OR core.NullIfEmpty(P.FirstName) IS NULL
					OR core.NullIfEmpty(P.LastName) IS NULL
					OR P.HasAcceptedTerms = 0
					OR (@bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL)
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@cCellPhone = P.CellPhone,
		@cEmailAddress = P.EmailAddress,
		@cFullName = person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,

		@cRequiredProfileUpdate =
			CASE WHEN core.NullIfEmpty(P.EmailAddress) IS NULL THEN ',EmailAddress' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.FirstName) IS NULL THEN ',FirstName' ELSE '' END 
			+ CASE WHEN core.NullIfEmpty(P.LastName) IS NULL THEN ',LastName' ELSE '' END 
			+ CASE WHEN P.HasAcceptedTerms = 0 THEN ',AcceptTerms' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND core.NullIfEmpty(P.CellPhone) IS NULL THEN ',CellPhone' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0 THEN ',CountryCallingCodeID' ELSE '' END 
			+ CASE WHEN @bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0 THEN ',IsPhoneVerified' ELSE '' END,

		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@nDAPersonID = ISNULL(P.DAPersonID, 0),
		@nDefaultProjectID = P.DefaultProjectID,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@nPasswordDuration = CAST(core.GetSystemSetupValueBySystemSetupKey('PasswordDuration', '30') AS INT),
		@nPersonID = ISNULL(P.PersonID, 0)
	FROM person.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			
		INSERT INTO @tPerson 
			(
			CellPhone,
			CountryCallingCodeID,
			DAPersonID,
			DefaultProjectID,
			EmailAddress,
			FullName,
			IsAccountLockedOut,
			IsActive,
			IsPhoneVerified,
			IsProfileUpdateRequired,
			IsSuperAdministrator,
			IsValidPassword,
			IsValidUserName,
			PersonID,
			RequiredProfileUpdate,
			RoleName,
			UserName
			) 
		VALUES 
			(
			@cCellPhone,
			@nCountryCallingCodeID,
			@nDAPersonID,
			@nDefaultProjectID,
			@cEmailAddress,
			@cFullName,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsPhoneVerified,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@nPersonID,
			@cRequiredProfileUpdate,
			@cRoleName,
			@cUserName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
		
				UPDATE @tPerson
				SET 
					IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			--ENDIF

			UPDATE person.Person
			SET 
				InvalidLoginAttempts = 0,
				LastLoginDateTime = getDate()
			WHERE PersonID = @nPersonID

			END
		--ENDIF
		
		END
	--ENDIF
		
	SELECT 
		P.*
	FROM @tPerson P

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1
	ORDER BY 1

	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM dropdown.Project P
		JOIN person.PersonProject PP ON PP.ProjectID = P.ProjectID
			AND PP.PersonID = @nPersonID
	ORDER BY P.DisplayOrder, P.ProjectName

END
GO
--End procedure person.ValidateLogin

--Begin procedure personupdate.ApprovePersonUpdateByPersonUpdateID
EXEC utility.DropObject 'personupdate.ApprovePersonUpdateByPersonUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.26
-- Description:	A stored procedure to submit Person, PersonGroupPerson, and PersonProject in a person update for approval
-- ======================================================================================================================
CREATE PROCEDURE personupdate.ApprovePersonUpdateByPersonUpdateID

@PersonUpdateID INT,
@PersonUpdatePersonID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cPersonUpdateTypeCode VARCHAR(50) = (SELECT PUT.PersonUpdateTypeCode FROM personupdate.PersonUpdate PU JOIN dropdown.PersonUpdateType PUT ON PUT.PersonUpdateTypeID = PU.PersonUpdateTypeID AND PU.PersonUpdateID = @PersonUpdateID)
	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tPGPSaveCreateDate TABLE (PersonGroupID INT, PersonID INT, CreateDateTime DATETIME)

	IF (@cPersonUpdateTypeCode <> 'DELETE')
		BEGIN

			IF (SELECT PersonID FROM personupdate.PersonUpdate PU WHERE PU.PersonUpdateID = @PersonUpdateID) = 0
				BEGIN

					INSERT INTO person.Person (
						Title,
						FirstName,
						LastName,
						Suffix,
						UserName,
						EmailAddress,
						ManagerPersonID,
						DefaultProjectID,
						Organization,
						RoleID,
						InvoiceLimit,
						PurchaseOrderLimit
					)
					OUTPUT INSERTED.PersonID INTO @tOutput
					SELECT 
						Title,
						FirstName,
						LastName,
						Suffix,
						UserName,
						EmailAddress,
						ManagerPersonID,
						DefaultProjectID,
						Organization,
						RoleID,
						InvoiceLimit,
						PurchaseOrderLimit
					FROM personupdate.Person
					WHERE PersonUpdateID = @PersonUpdateID

				END
			ELSE
				BEGIN

					UPDATE P
					SET
						P.Title = PUP.Title,
						P.FirstName = PUP.FirstName,
						P.LastName = PUP.LastName,
						P.Suffix = PUP.Suffix,
						P.UserName = PUP.UserName,
						P.EmailAddress = PUP.EmailAddress,
						P.ManagerPersonID = PUP.ManagerPersonID,
						P.DefaultProjectID = PUP.DefaultProjectID,
						P.Organization = PUP.Organization,
						P.RoleID = PUP.RoleID,
						P.InvoiceLimit = PUP.InvoiceLimit,
						P.PurchaseOrderLimit = PUP.PurchaseOrderLimit
					FROM person.Person P
						JOIN personupdate.Person PUP ON PUP.PersonID = P.PersonID
							AND PUP.PersonUpdateID = @PersonUpdateID

					INSERT INTO @tOutput(PersonID)
					SELECT PersonID
					FROM personupdate.Person
					WHERE PersonUpdateID = @PersonUpdateID

				END
			--ENDIF

			DECLARE @nPersonID INT = (SELECT TOP 1 PersonID FROM @tOutput);

			UPDATE personupdate.PersonGroupPerson SET PersonID = @nPersonID WHERE PersonUpdateID = @PersonUpdateID
			UPDATE personupdate.PersonPermissionable SET PersonID = @nPersonID WHERE PersonUpdateID = @PersonUpdateID
			UPDATE personupdate.PersonProject SET PersonID = @nPersonID WHERE PersonUpdateID = @PersonUpdateID


			INSERT INTO @tPGPSaveCreateDate(PersonGroupID, PersonID, CreateDateTime)
			SELECT PersonGroupID, PersonID, CreateDateTime
			FROM person.PersonGroupPerson 
			WHERE PersonID = @nPersonID

			DELETE FROM person.PersonGroupPerson
			WHERE PersonID = @nPersonID

			INSERT INTO person.PersonGroupPerson(PersonGroupID, PersonID)
			SELECT PGP.PersonGroupID, PGP.PersonID
			FROM personupdate.PersonGroupPerson PGP
			WHERE PGP.PersonUpdateID = @PersonUpdateID

			UPDATE PGP
			SET PGP.CreateDateTime = tPGP.CreateDateTime
			FROM @tPGPSaveCreateDate AS tPGP
			JOIN person.PersonGroupPerson AS PGP ON PGP.PersonID = tPGP.PersonID
				AND PGP.PersonGroupID = tPGP.PersonGroupID


			DELETE PP
			FROM person.PersonPermissionable PP
			WHERE PersonID = @PersonUpdatePersonID

			INSERT INTO person.PersonPermissionable(PersonID,PermissionableLineage)
			SELECT PUPP.PersonID,PUPP.PermissionableLineage
			FROM personupdate.PersonPermissionable PUPP
			WHERE PUPP.PersonUpdateID = @PersonUpdateID

			DELETE PP
			FROM person.PersonProject PP
			WHERE PersonID = @PersonUpdatePersonID

			INSERT INTO person.PersonProject(PersonID,ProjectID,IsViewDefault)
			SELECT PUPP.PersonID,PUPP.ProjectID,PUPP.IsViewDefault
			FROM personupdate.PersonProject PUPP
			WHERE PUPP.PersonUpdateID = @PersonUpdateID

		END
	--ENDIF

	SELECT TOP 1 PersonID FROM @tOutput

	COMMIT TRANSACTION

	EXEC eventlog.LogPersonUpdateAction @PersonUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogPersonUpdateAction @PersonUpdateID, 'update', @PersonID, NULL

END
GO
--End procedure personupdate.ApprovePersonUpdateByPersonUpdateID

--Begin procedure personupdate.GetPersonUpdateByPersonUpdateID
EXEC utility.DropObject 'personupdate.GetPersonUpdateByPersonUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.26
-- Description:	A stored procedure to get data from the personupdate.PersonUpdate table
-- ====================================================================================
CREATE PROCEDURE personupdate.GetPersonUpdateByPersonUpdateID

@PersonUpdateID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('PersonUpdate', @PersonUpdateID)

	--PersonUpdate
	SELECT
		PU.PersonUpdateID, 
		PU.PersonID, 
		person.FormatPersonNameByPersonID(PU.PersonID,'FirstLast') AS PersonNameFormatted,
		PU.FirstName,
		PU.LastName,
		PU.UserName,
		PU.PersonUpdateTypeID, 
		PUT.PersonUpdateTypeName,
		PU.RequestDate,
		core.FormatDate(PU.RequestDate) AS RequestDateFormatted,
		PU.RequestedByPersonID, 
		person.FormatPersonNameByPersonID(PU.RequestedByPersonID,'FirstLast') AS RequestedByNameFormatted,
		PU.ActionDate,
		core.FormatDate(PU.ActionDate) AS ActionDateFormatted,
		PU.WorkflowStepNumber,
		PU.Notes,
		IIF((SELECT PE.DAPersonID FROM person.Person PE WHERE PE.PersonID = PU.PersonID) > 0,'Yes','No') AS HasDAAccountYesNoFormat,
		PU.ISDAAccountDecomissioned,
		IIF(PU.ISDAAccountDecomissioned = 1, 'Yes', 'No') AS ISDAAccountDecomissionedYesNoFormat,
		PU.DAAccountDecomissionedDate,
		core.FormatDate(PU.DAAccountDecomissionedDate) AS DAAccountDecomissionedDateFormatted,
		PU.LockOutByPersonID,
		person.FormatPersonNameByPersonID(PU.LockOutByPersonID, 'FirstLast') AS LockOutByPersonNameFormatted,
		PU.LockOutDate,
		core.FormatDate(PU.LockOutDate) AS LockOutDateFormatted,
		PU.FARemoveByPersonID,
		person.FormatPersonNameByPersonID(PU.FARemoveByPersonID, 'FirstLast') AS FARemoveByPersonNameFormatted,
		PU.FARemoveDate,
		core.FormatDate(PU.FARemoveDate) AS FARemoveDateFormatted,
		PU.DeleteByPersonID,
		person.FormatPersonNameByPersonID(PU.DeleteByPersonID, 'FirstLast') AS DeleteByPersonNameFormatted,
		PU.DeleteDate,
		core.FormatDate(PU.DeleteDate) AS DeleteDateFormatted,
		PU.DaysToRemoval
	FROM personupdate.PersonUpdate PU
		JOIN dropdown.PersonUpdateType PUT ON PUT.PersonUpdateTypeID = PU.PersonUpdateTypeID
	WHERE PU.PersonUpdateID = @PersonUpdateID

	--PersonUpdateDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'PersonUpdate'
			AND DE.EntityID = @PersonUpdateID

	--PersonUpdatePerson
	SELECT
		P.PersonUpdatePersonID,
		P.Title,
		P.FirstName,
		P.LastName,
		P.Suffix,
		P.UserName,
		P.EmailAddress,
		P.ManagerPersonID,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		P.DefaultProjectID,
		DP.ProjectName AS DefaultProjectName,
		P.Organization,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		P.RoleID,
		R.RoleName,
		P.InvoiceLimit,
		P.PurchaseOrderLimit

	FROM personupdate.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
		JOIN dropdown.project DP ON DP.ProjectID = P.DefaultProjectID
	WHERE P.PersonUpdateID = @PersonUpdateID

	--PersonGroupPerson
	SELECT 
		PGP.PersonUpdatePersonGroupPersonID,
		PGP.PersonGroupID,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
		SELECT COUNT(DISTINCT W.WorkflowID) 
		FROM workflow.WorkflowStepGroupPersonGroup WSGPG
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.IsActive = 1
				AND WSGPG.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT(DISTINCT PGP2.PersonID) FROM person.PersonGroupPerson PGP2 WHERE PGP2.PersonGroupID = PG.PersonGroupID) AS UserCount,
		PG.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName
	FROM personupdate.PersonGroupPerson PGP
		JOIN person.PersonGroup PG ON PG.PersonGroupID = PGP.PersonGroupID
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PGP.PersonUpdateID = @PersonUpdateID

	--PersonPermissionable
	SELECT P.PermissionableLineage
		,P.PermissionCode
		,P.PermissionableID
		,PG.PermissionableGroupCode
		,PG.PermissionableGroupName
		,P.ControllerName
		,P.Description
		,P.DisplayOrder
		,P.IsActive
		,P.IsGlobal
		,P.IsSuperAdministrator
		,P.MethodName
		,CASE
			WHEN @PersonUpdateID > 0 AND EXISTS (SELECT 1 FROM personupdate.PersonPermissionable PP WHERE PP.PersonUpdateID = @PersonUpdateID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			ELSE 0
		END AS HasPermissionable
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage

	-- PersonProject
	SELECT
		PP.PersonUpdatePersonProjectID,
		PP.ProjectID,
		P.ProjectName,
		PP.IsViewDefault
	FROM personupdate.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonUpdateID = @PersonUpdateID
	ORDER BY P.ProjectName
	
	--WorkflowData
	EXEC workflow.GetEntityWorkflowData 'PersonUpdate', @PersonUpdateID
	
	--WorkflowPeople
	EXEC workflow.GetEntityWorkflowPeople 'PersonUpdate', @PersonUpdateID, @nWorkflowStepNumber

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Person Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Person Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Person Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Person Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'PersonUpdate'
		AND EL.EntityID = @PersonUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure personupdate.GetPersonUpdateByPersonUpdateID

--Begin procedure personupdate.SavePersonPermissionables
EXEC utility.DropObject 'personupdate.SavePersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2015.10.02
-- Description:	A stored procedure to add data to the person.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE personupdate.SavePersonPermissionables

@PersonUpdateID INT, 
@PersonID INT, 
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	DELETE PUPP
	FROM personupdate.PersonPermissionable PUPP
	WHERE PUPP.PersonUpdateID = @PersonUpdateID
		
	INSERT INTO personupdate.PersonPermissionable
		(PersonUpdateID,PersonID,PermissionableLineage)
	SELECT
		@PersonUpdateID,
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	UNION

	SELECT
		@PersonUpdateID,
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE EXISTS
		(
		SELECT 1 
		FROM core.ListToTable(@PermissionableIDList, ',') LTT 
		WHERE CAST(LTT.ListItem AS INT) = P.PermissionableID
			AND CAST(LTT.ListItem AS INT) > 0
		)
		AND NOT EXISTS
			(
			SELECT 1
			FROM personupdate.PersonPermissionable PUPP
			WHERE PUPP.PermissionableLineage = P.PermissionableLineage
				AND PUPP.PersonUpdateID = @PersonUpdateID
			)

END
GO
--End procedure personupdate.SavePersonPermissionables

--Begin procedure workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =================================================================================================

CREATE PROCEDURE workflow.GetEntityWorkflowData

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		SELECT
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(
			SELECT MAX(WS2.WorkflowStepNumber) 
			FROM workflow.Workflow W2 
				JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID 
					AND W2.IsActive = W1.IsActive
					AND W2.EntityTypeCode = W1.EntityTypeCode 
			) AS WorkflowStepCount
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.EntityTypeCode = @EntityTypeCode
				AND W1.IsActive = 1
				AND WS1.WorkflowStepNumber = 1
				AND (
					@ProjectID = 0
					OR W1.ProjectID = @ProjectID
				)
		
		END
	--ENDIF

	ELSE
		BEGIN
	
			IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
				BEGIN
				
				SELECT TOP 1 
					D.WorkflowStepName,
					D.WorkflowStepNumber,
					D.WorkflowStepCount
				FROM 
					(
					SELECT
						EWSGP.WorkflowStepName,
						EWSGP.WorkflowStepNumber,
						workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
					FROM workflow.EntityWorkflowStepGroupPerson EWSGP
					WHERE EWSGP.EntityTypeCode = @EntityTypeCode
						AND EWSGP.EntityID = @EntityID
						AND EWSGP.IsComplete = 0

					UNION

					SELECT 
						EWSGPG.WorkflowStepName,
						EWSGPG.WorkflowStepNumber,
						workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
					FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
					WHERE EWSGPG.EntityTypeCode = @EntityTypeCode
						AND EWSGPG.EntityID = @EntityID
						AND EWSGPG.IsComplete = 0			
				) AS D
				ORDER BY 2

				END
			--ENDIF
			
			ELSE
				BEGIN

				SELECT
					'Approved' AS WorkflowStepName,
					workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
					workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
				
				END
			--ENDIF
			
		END
	--ENDIF

END
GO
--End procedure workflow.GetEntityWorkflowData

--Begin procedure workflow.GetEntityWorkflowPeople
EXEC utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.01.01
-- Description:	A procedure to return people assigned to an entityy's current workflow step
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
--
-- Author:			Jonathan Burnham
-- Create date:	2019.02.19
-- Description:	refactored to check against a 
-- ==========================================================================================
CREATE PROCEDURE workflow.GetEntityWorkflowPeople

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT,
@ProjectID INT = 0

AS
BEGIN

	DECLARE @tTable TABLE
		(
		WorkflowStepGroupName VARCHAR(1000), 
		WorkflowStepGroupPersonGroupName VARCHAR(1000), 
		PersonGroupID INT,
		IsFinancialApprovalRequired BIT DEFAULT 0,
		PersonID INT,
		FullName VARCHAR(1000),
		EmailAddress VARCHAR(1000),
		IsComplete BIT DEFAULT 0
		)

	IF @EntityID = 0
		BEGIN
		
		INSERT INTO @tTable 
			(WorkflowStepGroupName,WorkflowStepGroupPersonGroupName,PersonGroupID,IsFinancialApprovalRequired,PersonID,FullName,EmailAddress,IsComplete)
		SELECT
			WSG.WorkflowStepGroupName,
			'',
			0,
			WSG.IsFinancialApprovalRequired,
			WSGP.PersonID,
			person.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
				AND (
					@ProjectID = 0 
					OR W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
				)
			JOIN person.Person P ON P.PersonID = WSGP.PersonID

		UNION

		SELECT
			WSG.WorkflowStepGroupName,
			PG.PersonGroupName,
			PG.PersonGroupID,
			WSG.IsFinancialApprovalRequired,
			PGP.PersonID,
			person.FormatPersonnameByPersonID(PGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID
			JOIN person.PersonGroupPerson PGP ON PGP.PersonGroupID = PG.PersonGroupID
			JOIN person.Person P ON P.PersonID = PGP.PersonID
	
		END
	ELSE
		BEGIN
	
		INSERT INTO @tTable 
			(WorkflowStepGroupName,WorkflowStepGroupPersonGroupName,PersonGroupID,IsFinancialApprovalRequired,PersonID,FullName,EmailAddress,IsComplete)
		SELECT
			EWSGP.WorkflowStepGroupName, 
			'',
			0,
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.PersonID,
			person.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber

		UNION

		SELECT
			EWSGPG.WorkflowStepGroupName, 
			PG.PersonGroupName,
			PG.PersonGroupID,
			EWSGPG.IsFinancialApprovalRequired,
			PGP.PersonID,
			person.FormatPersonnameByPersonID(PGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGPG.IsComplete
		FROM workflow.EntityWorkflowStepGroupPersonGroup EWSGPG
			JOIN person.PersonGroup PG ON PG.PersonGroupID = EWSGPG.PersonGroupID
			JOIN person.PersonGroupPerson PGP ON PGP.PersonGroupID = PG.PersonGroupID
			JOIN person.Person P ON P.PersonID = PGP.PersonID
				AND EWSGPG.EntityTypeCode = @EntityTypeCode
				AND EWSGPG.EntityID = @EntityID
				AND EWSGPG.WorkflowStepNumber = @WorkflowStepNumber

		END
	--ENDIF

	SELECT 
		T.WorkflowStepGroupName,
		T.WorkflowStepGroupPersonGroupName,
		T.PersonGroupID,
		T.IsFinancialApprovalRequired,
		T.PersonID,
		T.FullName,
		T.EmailAddress,
		T.IsComplete
	FROM @tTable T
	ORDER BY T.WorkflowStepGroupName, T.WorkflowStepGroupPersonGroupName, T.FullName
	
END
GO
--End procedure workflow.GetEntityWorkflowPeople

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.ProjectID,
		dropdown.GetProjectNameByProjectID(W.ProjectID) AS ProjectName,
		W.WorkflowID,	
		W.WorkflowName,
		W.WorkflowCompleteStatusName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT 
		WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT 
		WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepStatusName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.IsFinancialApprovalRequired,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		person.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGPG.PersonGroupID,
		WSGPG.WorkflowStepGroupID,
		PG.PersonGroupName
	FROM workflow.WorkflowStepGroupPersonGroup WSGPG
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGPG.WorkflowStepGroupID
		JOIN person.PersonGroup PG ON PG.PersonGroupID = WSGPG.PersonGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- ==========================================================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50) = NULL,
@EntityID INT,
@ProjectID INT = 0

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

	INSERT INTO workflow.EntityWorkflowStepGroupPersonGroup
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonGroupID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSG.IsFinancialApprovalRequired,
		WSGPG.PersonGroupID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPersonGroup WSGPG ON WSGPG.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR W.EntityTypeSubCode = @EntityTypeSubCode)
			AND W.IsActive = 1
			AND (@ProjectID = 0 OR W.ProjectID = @ProjectID)

END
GO
--End procedure workflow.InitializeEntityWorkflow

--Begin procedure workflow.ResetEntityWorkflowStepGroupPerson
EXEC utility.DropObject 'workflow.ResetEntityWorkflowStepGroupPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.06
-- Description:	A stored procedure to update already crated workflows with the current personnel assignments
--
-- Author:			Jonathan Burnham
-- Create date:	2019.01.21
-- Description:	refactored to support the workflow.WorkflowStepGroupPersonGroup functionality
-- =========================================================================================================
CREATE PROCEDURE workflow.ResetEntityWorkflowStepGroupPerson

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tEntityWorkflowStepGroupPerson TABLE
		(
		EntityTypeCode VARCHAR(50),
		EntityID INT,
		WorkflowID INT,
		WorkflowStepID INT,
		WorkflowStepGroupID INT,
		WorkflowName VARCHAR(250),
		WorkflowStepNumber INT,
		WorkflowStepName VARCHAR(250),
		WorkflowStepGroupName VARCHAR(250),
		IsFinancialApprovalRequired BIT,
		PersonID INT,
		IsComplete BIT
		)	
		
	;
	WITH WSGP AS
		(
		SELECT
			W.WorkflowID,
			WS.WorkflowStepID, 
			WSG.WorkflowStepGroupID, 
			WSGP.PersonID
		FROM workflow.WorkflowStepGroupPerson WSGP 
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.WorkflowID = @WorkflowID
				AND (
					W.ProjectID IN (SELECT PP.ProjectID FROM person.PersonProject PP WHERE PP.PersonID = WSGP.PersonID)
					OR W.EntityTypeCode IN ('TrendReportAggregator','PersonUpdate')
				)
		)
	
	INSERT INTO @tEntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID, IsComplete)
	SELECT
		D.EntityTypeCode, 
		D.EntityID, 
		D.WorkflowID, 
		D.WorkflowStepID, 
		D.WorkflowStepGroupID, 
		D.WorkflowName, 
		D.WorkflowStepNumber, 
		D.WorkflowStepName, 
		D.WorkflowStepGroupName, 
		D.IsFinancialApprovalRequired,
		E.PersonID,
		D.IsComplete
	FROM
		(
		SELECT
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.WorkflowID = @WorkflowID
		GROUP BY
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsFinancialApprovalRequired,
			EWSGP.IsComplete
		) D
		CROSS APPLY
			(
			SELECT
				WSGP.PersonID
			FROM WSGP
			WHERE WSGP.WorkflowID = D.WorkflowID
				AND WSGP.WorkflowStepID = D.WorkflowStepID
				AND WSGP.WorkflowStepGroupID = D.WorkflowStepGroupID
			) E
	ORDER BY 
		D.EntityID,
		D.WorkflowID,
		D.WorkflowStepID,
		D.WorkflowStepGroupID, 
		D.IsComplete

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.WorkflowID = @WorkflowID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsFinancialApprovalRequired, PersonID, IsComplete)
	SELECT
		T.EntityTypeCode, 
		T.EntityID, 
		T.WorkflowID, 
		T.WorkflowStepID, 
		T.WorkflowStepGroupID, 
		T.WorkflowName, 
		T.WorkflowStepNumber, 
		T.WorkflowStepName, 
		T.WorkflowStepGroupName, 
		T.IsFinancialApprovalRequired,
		T.PersonID, 
		T.IsComplete
	FROM @tEntityWorkflowStepGroupPerson T

END
GO
--End procedure workflow.ResetEntityWorkflowStepGroupPerson
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--USE HermisCloud
GO

DELETE FROM eventlog.EventLog WHERE EntityTypeCode LIKE '%Feedback' OR EntityTypeCode = 'Application'
DELETE FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityTypeCode LIKE '%Feedback'
DELETE FROM workflow.EntityWorkflowStepGroupPersonGroup WHERE EntityTypeCode LIKE '%Feedback'

--Begin table core.EmailTemplate
DELETE ET 
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode LIKE '%Feedback%'
GO

DELETE ETF
FROM core.EmailTemplateField ETF
WHERE NOT EXISTS 
	(
	SELECT 1
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = ETF.EntityTypeCode
	)
GO

EXEC core.EmailTemplateAddUpdate 
	'Feedback', 
	'Reject', 
	'Sent when feedback has been rejected.', 
	'Feedback Has Been Disapproved', 
	'<p>Roster member feedback was disapproved:</p><br/><p><strong>Title: </strong>[[FeedbackNameLink]]</p><p><strong>Roster Member: </strong>[[CSGMemberPersonNameFormatted]]</p><p><strong>Dates: </strong>[[DateRangeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the feedback workflow. Please log in and click the link above to review.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[FeedbackNameLink]]":"Link back to feedback record."},{"[[DateRangeFormatted]]":"Formatted date range of project."},{"[[CSGMemberPersonNameFormatted]]":"Name of the CSG member"},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Feedback', 
	'Submit', 
	'Sent when feedback has been submitted for approval.', 
	'Feedback Was Submitted For Your Review', 
	'<p>Roster member feedback has been submitted for your review:</p><br/><p><strong>Title: </strong>[[FeedbackNameLink]]</p><p><strong>Roster Member: </strong>[[CSGMemberPersonNameFormatted]]</p><p><strong>Dates: </strong>[[DateRangeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the feedback workflow. Please log in and click the link above to review.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[FeedbackNameLink]]":"Link back to feedback record."},{"[[DateRangeFormatted]]":"Formatted date range of project."},{"[[CSGMemberPersonNameFormatted]]":"Name of the CSG member"},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Feedback', 
	'Release', 
	'Sent when feedback has been approved.', 
	'Feedback Has Been Approved', 
	'<p>Roster member feedback was approved:</p><br/><p><strong>Title: </strong>[[FeedbackNameLink]]</p><p><strong>Roster Member: </strong>[[CSGMemberPersonNameFormatted]]</p><p><strong>Dates: </strong>[[DateRangeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the feedback workflow. Please log in and click the link above to review.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[FeedbackNameLink]]":"Link back to feedback record."},{"[[DateRangeFormatted]]":"Formatted date range of project."},{"[[CSGMemberPersonNameFormatted]]":"Name of the CSG member"},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'SURosterTerminationFinal', 
	'Sent to notifiy the SU roster management team that a member is about to be permanently deleted from the SU roster', 
	'Data Deletion', 
	'<p>Data regarding the following Roster Member will be deleted on [[DeletionDateFormatted]].<br /><br />Roster member: [[RosterPersonNameFormatted]]<br /><br />Please do not reply to this email as it is generated automatically by the HERMIS system.<br /><br />HERMIS Support Team<br /><br />If you need support, please email [[FeedBackMailTo]].</p>',
	'[{"[[DeletionDateFormatted]]":"Roster member deletion date"},{"[[RosterPersonNameFormatted]]":"The name of the person being deleted"},{"[[FeedBackMailTo]]":"Support email address"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'SURosterTerminationInitial', 
	'Sent to notifiy a user that he has been terminated from the SU roster', 
	'CSG Roster - Profile Update', 
	'<p>Dear [[ToFullName]],<br /><br />We write to confirm that your membership of the CGS Roster has terminated by reason of [[TerminationReason]].<br /><br />We will continue to hold your data in accordance with legal requirements.&nbsp;&nbsp;You can contact us at [[SURosterContactEmailAddress]]if you would like to discuss this.<br /><br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br />The SU Recruitment Team<br />[[SURosterContactEmailAddress]]</p>',
	'[{"[[TerminationReason]]":"Roster termination reason"},{"[[SURosterContactEmailAddress]]":"SU contact email address"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vacancy', 
	'DecrementWorkflow', 
	'Sent when a vacancy submission has been rejected.', 
	'A Vacancy Has Been Disapproved', 
	'<p>A previously submitted vacancy has been disapproved</p><br/><p><strong>Name: </strong>[[VacancyNameLink]]</p><p><strong>Opening Date: </strong>[[OpenDateTimeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the vacancy workflow. Please log in and click the link above to review this vacancy.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[VacancyNameLink]]":"Link back to vacancy record."},{"[[OpenDateTimeFormatted]]":"Formatted date of proposed vacancy opening."},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vacancy', 
	'IncrementWorkflow', 
	'Sent when a vacancy has been submitted for approval.', 
	'A Vacancy Was Submitted For Your Review', 
	'<p>A vacancy has been submitted for your review:</p><br/><p><strong>Name: </strong>[[VacancyNameLink]]</p><p><strong>Opening Date: </strong>[[OpenDateTimeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the vacancy workflow. Please log in and click the link above to review this vacancy.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[VacancyNameLink]]":"Link back to vacancy record."},{"[[OpenDateTimeFormatted]]":"Formatted date of proposed vacancy opening."},{"[[Comments]]":"Workflow comments."}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vacancy', 
	'Release', 
	'Sent when a vacancy submission has been approved.', 
	'A Vacancy Has Been Approved', 
	'<p>A previously submitted vacancy has been approved</p><br/><p><strong>Name: </strong>[[VacancyNameLink]]</p><p><strong>Opening Date: </strong>[[OpenDateTimeFormatted]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><br/><p>You are receiving this email notification from the system because you have been assigned as a member of the vacancy workflow. Please log in and click the link above to review this vacancy.</p><br/><p>Please do not reply to this email as it is generated automatically by the system.<br /><br />Thank you.</p>',
	'[{"[[VacancyNameLink]]":"Link back to vacancy record."},{"[[OpenDateTimeFormatted]]":"Formatted date of proposed vacancy opening."},{"[[Comments]]":"Workflow comments."}]'
GO

UPDATE ET
SET ET.WorkflowActionCode = 
	CASE 
		WHEN ET.EntityTypeCode = 'Feedback'
		THEN 
			CASE
				WHEN ET.EmailTemplateCode = 'Reject'
				THEN 'DecrementWorkflow'
				WHEN ET.EmailTemplateCode = 'Submit'
				THEN 'IncrementWorkflow'
				ELSE ET.EmailTemplateCode
			END
		WHEN ET.EntityTypeCode = 'Vacancy'
		THEN ET.EmailTemplateCode
		ELSE ET.WorkflowActionCode
	END
FROM core.EmailTemplate ET
WHERE ET.EntityTypeCode IN ('Feedback', 'Vacancy')
GO
--End table core.EmailTemplate

--Begin table core.EntityType
DELETE ET 
FROM core.EntityType ET
WHERE ET.EntityTypeCode IN ('ExternalFeedback', 'InternalFeedback')
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Application', 
	@EntityTypeName = 'Application', 
	@EntityTypeNamePlural = 'Applications',
	@HasWorkflow = 0,
	@SchemaName = 'hrms', 
	@TableName = 'Application', 
	@PrimaryKeyFieldName = 'ApplicationID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Competency', 
	@EntityTypeName = 'Competency', 
	@EntityTypeNamePlural = 'Competencies',
	@HasWorkflow = 0,
	@SchemaName = 'hrms', 
	@TableName = 'Competency', 
	@PrimaryKeyFieldName = 'CompetencyID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Feedback', 
	@EntityTypeName = 'Feedback', 
	@EntityTypeNamePlural = 'Feedback',
	@HasWorkflow = 0,
	@SchemaName = 'person', 
	@TableName = 'Feedback', 
	@PrimaryKeyFieldName = 'FeedbackID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonUpdate', 
	@EntityTypeName = 'Person Update', 
	@EntityTypeNamePlural = 'PersonUpdates',
	@HasWorkflow = 1,
	@SchemaName = 'personupdate', 
	@TableName = 'PersonUpdate', 
	@PrimaryKeyFieldName = 'PersonUpdateID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Vacancy', 
	@EntityTypeName = 'Vacancy', 
	@EntityTypeNamePlural = 'Vacancies',
	@HasWorkflow = 1,
	@SchemaName = 'hrms', 
	@TableName = 'Vacancy', 
	@PrimaryKeyFieldName = 'VacancyID'
GO
--End table core.EntityTypeAddUpdate

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@BeforeMenuItemCode = 'ConsultantList',
	@NewMenuItemCode = 'Vacancy',
	@NewMenuItemLink = '/vacancy/list',
	@NewMenuItemText = 'Recruitment to Roster',
	@PermissionableLineageList = 'vacancy.list',
	@ParentMenuItemCode = 'HumanResources'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonGroup',
	@NewMenuItemCode = 'PersonUpdate',
	@NewMenuItemLink = '/personupdate/list',
	@NewMenuItemText = 'User Management',
	@PermissionableLineageList = 'personupdate.list',
	@ParentMenuItemCode = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonUpdate',
	@NewMenuItemCode = 'Competency',
	@NewMenuItemLink = '/competency/list',
	@NewMenuItemText = 'Competencies',
	@PermissionableLineageList = 'competency.list',
	@ParentMenuItemCode = 'Admin'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'Training',
	@NewMenuItemCode = 'FeedbackList',
	@NewMenuItemLink = '/feedback/list',
	@NewMenuItemText = 'Member Feedback',
	@ParentMenuItemCode = 'HumanResources',
	@PermissionableLineageList = 'Feedback.List'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'EventLogList',
	@NewMenuItemCode = 'PendingEmailList',
	@NewMenuItemLink = '/pendingemail/list',
	@NewMenuItemText = 'Mail Log',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'PendingEmail.List'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Competency'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'HumanResources'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'PersonUpdate'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Vacancy'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
DELETE FROM core.SystemSetup WHERE SystemSetupKey IN ('SURosterTerminationEmialAddressTo', 'HumanitarianRosterTerminationEmialAddressTo')
GO

EXEC core.SystemSetupAddUpdate 'ApplicantSiteURL', '', 'https://applicant.oceandisc.com'
EXEC core.SystemSetupAddUpdate 'SURosterContactEmailAddress', 'The email address list to use to contact te roster management team', 'jcole@skotkonung.com'
EXEC core.SystemSetupAddUpdate 'SURosterTerminationEmailAddressToFinal', 'The email address list used when the final roster member termination email is sent', 'jcole@skotkonung.com'
EXEC core.SystemSetupAddUpdate 'SURosterTerminationEmailAddressToInitial', 'The email address list used when the initial roster member termination email is sent', 'jcole@skotkonung.com'
GO
--End table core.SystemSetup

--Begin table dropdown.AgeRange
TRUNCATE TABLE dropdown.AgeRange
GO

EXEC utility.InsertIdentityValue 'dropdown.AgeRange', 'AgeRangeID', 0
GO

INSERT INTO dropdown.AgeRange 
	(AgeRangeName, AgeRangeCode, DisplayOrder)
VALUES
	('18-24', '18-24', 1),
	('25-34', '25-34', 2),
	('35-44', '35-44', 3),
	('45-54', '45-54', 4),
	('55-64', '55-64', 5),
	('65-74', '65-74', 6),
	('74+', '74+', 7)
GO
--End table dropdown.AgeRange

--Begin table dropdown.ApplicantSource
TRUNCATE TABLE dropdown.ApplicantSource
GO

EXEC utility.InsertIdentityValue 'dropdown.ApplicantSource', 'ApplicantSourceID', 0
GO

INSERT INTO dropdown.ApplicantSource 
	(ApplicantSourceName, ApplicantSourceCode, HasOther, DisplayOrder)
VALUES
	('Online Advertisement (state website below)', 'OnlineAd', 1, 1),
	('Print Advertisement (state publication name below)', 'PrintAd', 1, 2),
	('Recruitment Fair (state event name below)', 'JobFair', 1, 3),
	('Through a colleague or contact (state their organisation/place of work below)', 'Network', 1, 4),
	('I am a former member of the UK CSG', 'PriorMember', 0, 5),
	('Other (Please State)', 'Other', 1, 6)

GO
--End table dropdown.ApplicantSource

--Begin table dropdown.CompetencyType
TRUNCATE TABLE dropdown.CompetencyType
GO

EXEC utility.InsertIdentityValue 'dropdown.CompetencyType', 'CompetencyTypeID', 0
GO

INSERT INTO dropdown.CompetencyType 
	(CompetencyTypeName, CompetencyTypeCode, DisplayOrder)
VALUES
	('Competency', 'Competency', 1),
	('Technical Capability', 'TechnicalCapability', 2)
GO
--End table dropdown.CompetencyType

--Begin table dropdown.CSGMemberPerformanceRating
TRUNCATE TABLE dropdown.CSGMemberPerformanceRating
GO

EXEC utility.InsertIdentityValue 'dropdown.CSGMemberPerformanceRating', 'CSGMemberPerformanceRatingID', 0
GO

INSERT INTO dropdown.CSGMemberPerformanceRating 
	(CSGMemberPerformanceRatingName, CSGMemberPerformanceRatingCode, DisplayOrder)
VALUES
	('Very Poor', '1', 1),
	('Poor', '2', 2),
	('Fair', '3', 3),
	('Good', '4', 4),
	('Excellent', '5', 5)
GO
--End table dropdown.CSGMemberPerformanceRating

--Begin table dropdown.Ethnicity
TRUNCATE TABLE dropdown.Ethnicity
GO

EXEC utility.InsertIdentityValue 'dropdown.Ethnicity', 'EthnicityID', 0
GO

INSERT INTO dropdown.Ethnicity 
	(EthnicityName, EthnicityCategoryName, HasOther, EthnicityCategorDisplayOrder, EthnicityDisplayOrder)
VALUES
	('English', 'White', 0, 1, 1),
	('Welsh', 'White', 0, 1, 2),
	('Scottish', 'White', 0, 1, 3),
	('Northern Irish', 'White', 0, 1, 4),
	('British', 'White', 0, 1, 5),
	('Irish', 'White', 0, 1, 6),
	('Gypsy or Irish Traveller', 'White', 0, 1, 7),
	('Any other White background (please describe)', 'White', 1, 1, 8),
	('White and Black Caribbean', 'Mixed/Multiple Ethnic Groups', 0, 2, 9),
	('White and Black African', 'Mixed/Multiple Ethnic Groups', 0, 2, 10),
	('White and Asian', 'Mixed/Multiple Ethnic Groups', 0, 2, 11),
	('Any other Mixed/Multiple Ethnic background (please describe)', 'Mixed/Multiple Ethnic Groups', 1, 2, 12),
	('Indian', 'Asian/Asian British', 0, 3, 13),
	('Pakistani', 'Asian/Asian British', 0, 3, 14),
	('Bangladeshi', 'Asian/Asian British', 0, 3, 15),
	('Chinese', 'Asian/Asian British', 0, 3, 16),
	('Any other Asian/Asian British background (please describe)', 'Asian/Asian British', 1, 3, 17),
	('African', 'Black African / Caribbean / Black British', 0, 4, 18),
	('Caribbean', 'Black African / Caribbean / Black British', 0, 4, 19),
	('Any other Black African / Caribbean / Black British background (please describe)', 'Black African / Caribbean / Black British', 1, 4, 20),
	('Arab', 'Other Ethnic Groups', 0, 5, 21),
	('Any other Ethnic Group background (Please describe)', 'Other Ethnic Groups', 1, 5, 22)
GO
--End table dropdown.Ethnicity

--Begin table dropdown.FeedbackQuestion
TRUNCATE TABLE dropdown.FeedbackQuestion
GO

EXEC utility.InsertIdentityValue 'dropdown.FeedbackQuestion', 'FeedbackQuestionID', 0
GO

INSERT INTO dropdown.FeedbackQuestion 
	(FeedbackQuestionName, FeedbackQuestionCode, DisplayOrder, IsExternal, IsInternal)
VALUES
	('SU support, advice, or analysis was relevant to our needs', 'SUPerformanceRatingID1', 1, 1, 0),
	('SU support, advice, or analysis was high quality', 'SUPerformanceRatingID2', 2, 1, 0),
	('I would recommend SU to other HMG colleagues', 'SUPerformanceRatingID3', 3, 1, 0),
	('What was the result of having SU support, advice, or analysis? (Please provide examples)', 'Question4', 4, 1, 0),
	('What could SU do to improve its support to similar taskings in future?', 'Question5', 5, 1, 0),
	('CSG Member Performance Rating', 'CSGMemberPerformanceRatingID', 6, 0, 1),
	('Please explain why you gave the rating above and provide feedback on the CSG member''s performance:', 'Question7', 7, 0, 1)
GO
--End table dropdown.FeedbackQuestion

--Begin table dropdown.InterviewOutcome
TRUNCATE TABLE dropdown.InterviewOutcome
GO

EXEC utility.InsertIdentityValue 'dropdown.InterviewOutcome', 'InterviewOutcomeID', 0
GO

INSERT INTO dropdown.InterviewOutcome 
	(InterviewOutcomeName, InterviewOutcomeCode, DisplayOrder)
VALUES
	('Pass', 'Pass', 1),
	('Fail', 'Fail', 2)
GO
--End table dropdown.InterviewOutcome

--Begin table dropdown.PersonType
TRUNCATE TABLE dropdown.PersonType
GO

EXEC utility.InsertIdentityValue 'dropdown.PersonType', 'PersonTypeID', 0
GO

INSERT INTO dropdown.PersonType 
	(PersonTypeName, PersonTypeCode, DisplayOrder)
VALUES
	('Serving Civil Servant', 'CivilServant', 1),
	('Serving Police', 'Police', 2)
GO

UPDATE PT
SET 
	PT.DisplayOrder = 99,
	PT.PersonTypeCode = 'Other',
	PT.PersonTypeName = 'Other'
FROM dropdown.PersonType PT
WHERE PT.PersonTypeID = 0
GO
--End table dropdown.PersonType

--Begin table dropdown.SuitabilityRating
TRUNCATE TABLE dropdown.SuitabilityRating
GO

EXEC utility.InsertIdentityValue 'dropdown.SuitabilityRating', 'SuitabilityRatingID', 0
GO

INSERT INTO dropdown.SuitabilityRating 
	(SuitabilityRatingName, SuitabilityRatingCode, DisplayOrder)
VALUES
	('Competency Met', 'Met', 1),
	('Competency partialy met', 'PartialMet', 2),
	('Competency not met', 'NotMet', 3)
GO
--End table dropdown.SuitabilityRating

--Begin table dropdown.SUPerformanceRating
TRUNCATE TABLE dropdown.SUPerformanceRating
GO

EXEC utility.InsertIdentityValue 'dropdown.SUPerformanceRating', 'SUPerformanceRatingID', 0
GO

INSERT INTO dropdown.SUPerformanceRating 
	(SUPerformanceRatingName, SUPerformanceRatingCode, DisplayOrder)
VALUES
	('Strongly Disagree', '1', 1),
	('Disagree', '2', 2),
	('Neutral', '3', 3),
	('Agree', '4', 4),
	('Strongly Agree', '5', 5)
GO
--End table dropdown.SUPerformanceRating

--Begin table dropdown.PersonUpdateType
TRUNCATE TABLE dropdown.PersonUpdateType
GO

EXEC utility.InsertIdentityValue 'dropdown.PersonUpdateType', 'PersonUpdateTypeID', 0
GO

INSERT INTO dropdown.PersonUpdateType 
	(PersonUpdateTypeName, PersonUpdateTypeCode, DisplayOrder)
VALUES
	('New User', 'NEW', 1),
	('Modify User', 'EDIT', 2),
	('Delete User', 'DELETE', 3)

GO
--End table dropdown.PersonUpdateType

--Begin table dropdown.VacancyStatus
TRUNCATE TABLE dropdown.VacancyStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.VacancyStatus', 'VacancyStatusID', 0
GO

INSERT INTO dropdown.VacancyStatus 
	(VacancyStatusName, VacancyStatusCode, DisplayOrder)
VALUES
	('Preparing', 'Preparing', 1),
	('Open', 'Open', 2),
	('Paused', 'Paused', 3),
	('Closed', 'Closed', 4)
GO
--End table dropdown.VacancyStatus

--Begin table dropdown.VacancyType
TRUNCATE TABLE dropdown.VacancyType
GO

EXEC utility.InsertIdentityValue 'dropdown.VacancyType', 'VacancyTypeID', 0
GO

INSERT INTO dropdown.VacancyType 
	(VacancyTypeName, VacancyTypeCode, DisplayOrder)
VALUES
	('Roster Member Application', 'RosterMemberApp', 1)
GO
--End table dropdown.VacancyType

--Begin table permissionable.Permissionable
DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('ExternalFeedback', 'InternalFeedback')
	OR P.PermissionableLineage LIKE '%AddInternalFeedback'
	OR P.PermissionableLineage LIKE '%ShowEngagements'
	OR P.PermissionableLineage LIKE '%ShowPermissionables'
GO

DELETE T FROM core.MenuItemPermissionableLineage T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM permissionable.Permissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM permissionable.PermissionableTemplatePermissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM person.PersonPermissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
DELETE T FROM personupdate.PersonPermissionable T WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = T.PermissionableLineage)
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Competency', 
	@DESCRIPTION='Add and update competencies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Competency.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Competency', 
	@DESCRIPTION='View the list of competencies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Competency.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Competency', 
	@DESCRIPTION='View competencies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Competency.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='Add / edit a roster member feedback item', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='View the list of roster member feedback items', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='Add an roster member feedback item', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.List.AddFeedback', 
	@PERMISSIONCODE='AddFeedback';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Feedback', 
	@DESCRIPTION='View a roster member feedback item', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Feedback.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonUpdate', 
	@DESCRIPTION='Add / edit a PersonUpdate', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonUpdate.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PendingEmail', 
	@DESCRIPTION='View the sent and pending mail log', 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PendingEmail.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonUpdate', 
	@DESCRIPTION='View the list of PersonUpdates', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonUpdate.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonUpdate', 
	@DESCRIPTION='View a PersonUpdate', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonUpdate.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='Add / edit a Vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='View the list of Vacancies', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='Manage applications', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='ManageApplication', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.ManageApplication', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Vacancy', 
	@DESCRIPTION='View a Vacancy', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='Vacancy.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='VacancyApplication', 
	@DESCRIPTION='Add / edit an application', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='VacancyApplication.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='VacancyApplication', 
	@DESCRIPTION='View the list of applications', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='VacancyApplication.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='VacancyApplication', 
	@DESCRIPTION='View an application', 
	@DISPLAYORDER=0, 
	@ISGLOBAL=0, 
	@ISSUPERADMINISTRATOR=0, 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='VacancyApplication.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the account tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the activitystreams tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the address tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the application tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the clearances tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the engagement tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the expertise tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the permissions tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the personal tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the qualification tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab'; 
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the IR35 data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyIR35', @PERMISSIONCODE='ModifyIR35'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the operational training data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyOperationalTraining', @PERMISSIONCODE='ModifyOperationalTraining'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the firearms training data', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyFirearmsTraining', @PERMISSIONCODE='ModifyFirearmsTraining'; 
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the account tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the activitystreams tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the address tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the application tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the clearances tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the engagement tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the expertise tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the permissions tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the personal tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab'; 
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the qualification tab', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab'; 
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--Begin table workflow.Workflow
UPDATE W
SET W.EntityTypeSubCode = 'External'
FROM workflow.Workflow W
WHERE W.WorkflowName LIKE '%External%'
	AND W.EntityTypeCode IN ('ExternalFeedback', 'Feedback', 'InternalFeedback')
GO

UPDATE W
SET W.EntityTypeSubCode = 'Internal'
FROM workflow.Workflow W
WHERE W.WorkflowName LIKE '%Internal%'
	AND W.EntityTypeCode IN ('ExternalFeedback', 'Feedback', 'InternalFeedback')
GO

UPDATE W
SET W.EntityTypeCode = 'Feedback'
FROM workflow.Workflow W
WHERE W.EntityTypeCode IN ('ExternalFeedback', 'InternalFeedback')
GO
--End table workflow.Workflow
--End file Build File - 04 - Data.sql

--Begin file Build File - 05 - Lesson.sql
--USE HermisCloud
GO

EXEC utility.AddSchema 'lesson'

--Begin table dropdown.LessonCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonCategory
	(
	LessonCategoryID INT IDENTITY(0,1) NOT NULL,
	LessonCategoryCode VARCHAR(50),
	LessonCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonCategoryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonCategory', 'DisplayOrder,LessonCategoryName', 'LessonCategoryID'
GO
--End table dropdown.LessonCategory

--Begin procedure dropdown.GetLessonCategoryData
EXEC Utility.DropObject 'dropdown.GetLessonCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetLessonCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonCategoryID, 
		T.LessonCategoryCode,
		T.LessonCategoryName
	FROM dropdown.LessonCategory T
	WHERE (T.LessonCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonCategoryName, T.LessonCategoryID

END
GO
--End procedure dropdown.GetLessonCategoryData

--Begin table dropdown.LessonCategory
TRUNCATE TABLE dropdown.LessonCategory
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonCategory', 'LessonCategoryID', 0
GO

INSERT INTO dropdown.LessonCategory 
	(LessonCategoryName, LessonCategoryCode, DisplayOrder)
VALUES
GO
--End table dropdown.LessonCategory

--Begin table dropdown.LessonStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonStatus
	(
	LessonStatusID INT IDENTITY(0,1) NOT NULL,
	LessonStatusCode VARCHAR(50),
	LessonStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonStatus', 'DisplayOrder,LessonStatusName', 'LessonStatusID'
GO
--End table dropdown.LessonStatus

--Begin procedure dropdown.GetLessonStatusData
EXEC Utility.DropObject 'dropdown.GetLessonStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonStatus table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetLessonStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonStatusID, 
		T.LessonStatusCode,
		T.LessonStatusName
	FROM dropdown.LessonStatus T
	WHERE (T.LessonStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonStatusName, T.LessonStatusID

END
GO
--End procedure dropdown.GetLessonStatusData

--Begin table dropdown.LessonStatus
TRUNCATE TABLE dropdown.LessonStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonStatus', 'LessonStatusID', 0
GO

INSERT INTO dropdown.LessonStatus 
	(LessonStatusName, LessonStatusCode, DisplayOrder)
VALUES
GO
--End table dropdown.LessonStatus

--Begin table dropdown.LessonType
DECLARE @TableName VARCHAR(250) = 'dropdown.LessonType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LessonType
	(
	LessonTypeID INT IDENTITY(0,1) NOT NULL,
	LessonTypeCode VARCHAR(50),
	LessonTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_LessonType', 'DisplayOrder,LessonTypeName', 'LessonTypeID'
GO
--End table dropdown.LessonType

--Begin procedure dropdown.GetLessonTypeData
EXEC Utility.DropObject 'dropdown.GetLessonTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.02
-- Description:	A stored procedure to return data from the dropdown.LessonType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetLessonTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LessonTypeID, 
		T.LessonTypeCode,
		T.LessonTypeName
	FROM dropdown.LessonType T
	WHERE (T.LessonTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LessonTypeName, T.LessonTypeID

END
GO
--End procedure dropdown.GetLessonTypeData

--Begin table dropdown.LessonType
TRUNCATE TABLE dropdown.LessonType
GO

EXEC utility.InsertIdentityValue 'dropdown.LessonType', 'LessonTypeID', 0
GO

INSERT INTO dropdown.LessonType 
	(LessonTypeName, LessonTypeCode, DisplayOrder)
VALUES
	('Project', 'Project', 1),
	('Programme', 'Program', 2),
	('Project and Programme', 'ProjectProgram', 3)
GO
--End table dropdown.LessonType

--Begin table lesson.Lesson
DECLARE @TableName VARCHAR(250) = 'lesson.Lesson'

EXEC utility.DropObject @TableName

CREATE TABLE lesson.Lesson
	(
	LessonID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	ClientID INT,
	CreateDateTime DATETIME,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LessonID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Lesson', 'DisplayOrder,LessonName', 'LessonID'
GO
--End table lesson.Lesson

--End file Build File - 05 - Lesson.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'General', 'General', 1;
EXEC permissionable.SavePermissionableGroup 'Document', 'Documents', 2;
EXEC permissionable.SavePermissionableGroup 'ForceAsset', 'Organizations & Facilities', 3;
EXEC permissionable.SavePermissionableGroup 'Insight', 'Incidents & Reports', 4;
EXEC permissionable.SavePermissionableGroup 'Activity', 'Activity', 5;
EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 6;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='Edit About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='About', @DESCRIPTION='View About & Support', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='About.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='Add / edit a roster member feedback item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='View the list of roster member feedback items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='Add an roster member feedback item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.List.AddFeedback', @PERMISSIONCODE='AddFeedback';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Feedback', @DESCRIPTION='View a roster member feedback item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Feedback.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Grant user access to dashboard links', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanHaveDashboardLinks', @PERMISSIONCODE='CanHaveDashboardLinks';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User recieves information requests from users without the dashboard links permissionable', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Default.CanRecieveDashboardInformationRequests', @PERMISSIONCODE='CanRecieveDashboardInformationRequests';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View the actual error on the cf error page', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the CTM Travel Approval menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CTMTravelApproval', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.CTMTravelApproval', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the DeployAdviser menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='DeployAdviser', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.DeployAdviser', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Compliance Checker menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumComplianceChecker', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumComplianceChecker', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MenuItem', @DESCRIPTION='View the Palladium Staff Time Collection menu link', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PalladiumStaffTimeCollection', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='MenuItem.PalladiumStaffTimeCollection', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the firearms training data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyFirearmsTraining', @PERMISSIONCODE='ModifyFirearmsTraining';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the IR35 data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyIR35', @PERMISSIONCODE='ModifyIR35';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Modify the operational training data', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ModifyOperationalTraining', @PERMISSIONCODE='ModifyOperationalTraining';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the account tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the activitystreams tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the address tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the application tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the clearances tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the engagement tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the expertise tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the permissions tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the personal tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Edit the qualification tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.AddUpdate.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the medical clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='MedicalVettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.MedicalVettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send bulk emails from the medical clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='MedicalVettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.MedicalVettingList.BullkEmail', @PERMISSIONCODE='BullkEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the security clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.VettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Send bulk emails from the security clearance list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.VettingList.BullkEmail', @PERMISSIONCODE='BullkEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the account tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAccountTab', @PERMISSIONCODE='ShowAccountTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the activitystreams tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowActivitystreamsTab', @PERMISSIONCODE='ShowActivitystreamsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the address tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowAddressTab', @PERMISSIONCODE='ShowAddressTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the application tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowApplicationTab', @PERMISSIONCODE='ShowApplicationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the clearances tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowClearancesTab', @PERMISSIONCODE='ShowClearancesTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the engagement tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowEngagementTab', @PERMISSIONCODE='ShowEngagementTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the expertise tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowExpertiseTab', @PERMISSIONCODE='ShowExpertiseTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the permissions tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPermissionsTab', @PERMISSIONCODE='ShowPermissionsTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the personal tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowPersonalTab', @PERMISSIONCODE='ShowPersonalTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the qualification tab', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Person.View.ShowQualificationTab', @PERMISSIONCODE='ShowQualificationTab';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add / edit a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a completed request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Amend', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.Amend', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View the list of requests for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View a request for information', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ResponseDashboard', @DESCRIPTION='Add / edit a response dashboard', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ResponseDashboard.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ResponseDashboard', @DESCRIPTION='View list of response dashboards', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ResponseDashboard.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ResponseDashboard', @DESCRIPTION='View a response dashboard', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='ResponseDashboard.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='Add / edit a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View the list of territories for a project', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Territory', @DESCRIPTION='View a territory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='Territory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VacancyApplication', @DESCRIPTION='Add / edit an application', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='VacancyApplication.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VacancyApplication', @DESCRIPTION='View the list of applications', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='VacancyApplication.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='VacancyApplication', @DESCRIPTION='View an application', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='General', @PERMISSIONABLELINEAGE='VacancyApplication.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit a document in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - images in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Images', @PERMISSIONCODE='Images';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - media in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - other in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type response - reports in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.Reports', @PERMISSIONCODE='Reports';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Edits documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.AddUpdate.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Allows users to download documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentName', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentName', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type dfid-chase documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.DFID-ChaseDocuments', @PERMISSIONCODE='DFID-ChaseDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type hermis guides and information in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.HERMISGuidesandInformation', @PERMISSIONCODE='HERMISGuidesandInformation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - images in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Images', @PERMISSIONCODE='Images';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - media in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Media', @PERMISSIONCODE='Media';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - other in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Other', @PERMISSIONCODE='Other';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type response - reports in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.Reports', @PERMISSIONCODE='Reports';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type su documents in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.SUDocuments', @PERMISSIONCODE='SUDocuments';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type templates and tools in the library', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Document', @PERMISSIONABLELINEAGE='Document.View.TemplatesandTools', @PERMISSIONCODE='TemplatesandTools';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.AddUpdate.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of Facilities', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Local Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.LocalFacility', @PERMISSIONCODE='LocalFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Response Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.ResponseFacility', @PERMISSIONCODE='ResponseFacility';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View a Stock Location Facility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Asset.View.StockLocation', @PERMISSIONCODE='StockLocation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.AddUpdate.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View the list of Organisations', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View an Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Civil Society Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.CivilSociety', @PERMISSIONCODE='CivilSociety';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Government Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Government', @PERMISSIONCODE='Government';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Non State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.NonStateArmedGroups', @PERMISSIONCODE='NonStateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a Private Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.Private', @PERMISSIONCODE='Private';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a State Armed Groups Organisation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ForceAsset', @PERMISSIONABLELINEAGE='Force.View.StateArmedGroups', @PERMISSIONCODE='StateArmedGroups';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Add / edit a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='Allows access to response report exports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View the list of response reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ActivityReport', @DESCRIPTION='View a response report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ActivityReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Share an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.AddUpdate.ShareIncident', @PERMISSIONCODE='ShareIncident';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View the list of incident reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View an Early Alert incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.EarlyAlert', @PERMISSIONCODE='EarlyAlert';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Geo Event incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.GeoEvent', @PERMISSIONCODE='GeoEvent';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View a Security incident report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='Incident.View.Security', @PERMISSIONCODE='Security';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='Add / edit a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View the list of Report Updates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ReportUpdate', @DESCRIPTION='View a Report Update', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='ReportUpdate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View the list of spot reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View a spot report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Add / edit a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='Situational report pdf export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View the list of trend reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReport', @DESCRIPTION='View a situational report', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Add / edit a situational report aggregation', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='Situational report aggregator export', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Export', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.Export', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='TrendReportAggregator', @DESCRIPTION='View the list of aggregated situational reports', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Insight', @PERMISSIONABLELINEAGE='TrendReportAggregator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the list of contacts', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View a contact', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Contact.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View the course catalog', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View a course', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View the equipment catalogue list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View an equipment catalogue', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of consignments', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of dispatched items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDispatchedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDispatchedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the list of distributed items', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedItems', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.ListDistributedItems', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentConsignment', @DESCRIPTION='View the consignment contents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentConsignment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View the equipment inventory', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View an equipment inventory item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='Add / edit an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment inventory list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='InventoryList', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.InventoryList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View the equipment item list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentItem', @DESCRIPTION='View an equipment item', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentItem.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='Add / edit an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View the list of Equipment Movements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentMovement', @DESCRIPTION='View an Equipment Movement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentMovement.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='Add / edit an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View the equipment order list', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentOrder', @DESCRIPTION='View an equipment order', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='EquipmentOrder.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='Add / edit a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View the list of modules', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Module', @DESCRIPTION='View a module', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Activity', @PERMISSIONABLELINEAGE='Module.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Competency', @DESCRIPTION='Add and update competencies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Competency.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Competency', @DESCRIPTION='View the list of competencies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Competency.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Competency', @DESCRIPTION='View competencies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Competency.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Access to the export utility', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=1, @METHODNAME='GetDocumentByDocumentEntityCode', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentEntityCode', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Download system documents', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='GetDocumentByDocumentGUID', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Document.GetDocumentByDocumentGUID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View the list of email templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplate', @DESCRIPTION='View an email template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View the list of event log entries', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View an event log entry', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PendingEmail', @DESCRIPTION='View the sent and pending mail log', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PendingEmail.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the list of permissionable templates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View the contents of a permissionable template', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Export the list of consultants', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ConsultantList', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ConsultantList.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Error message for DA users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='DAView', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.DAView', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the Humanitarian roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.HumanitarianRoster', @PERMISSIONCODE='HumanitarianRoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View the list of users on the SU roster', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List.SURoster', @PERMISSIONCODE='SURoster';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.DAPerson', @PERMISSIONCODE='DAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View a non-DA user', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View.NonDAPerson', @PERMISSIONCODE='NonDAPerson';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonGroup', @DESCRIPTION='Add / edit a user group', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonGroup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonGroup', @DESCRIPTION='View the list of user groups', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonGroup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonGroup', @DESCRIPTION='View a user group', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonGroup.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonUpdate', @DESCRIPTION='Add / edit a PersonUpdate', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonUpdate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonUpdate', @DESCRIPTION='View the list of PersonUpdates', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonUpdate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PersonUpdate', @DESCRIPTION='View a PersonUpdate', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PersonUpdate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit an activity stream', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View the list of activity streams', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View an activity stream', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='Add / edit an activity stream sponsor', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='View the list of activity stream sponsors', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProjectSponsor', @DESCRIPTION='View an activity stream sponsor', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ProjectSponsor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SystemSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='SystemSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='Add / edit a Vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='View the list of Vacancies', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='Manage applications', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ManageApplication', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.ManageApplication', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='Save application management', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ManageApplicationSave', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.ManageApplicationSave', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vacancy', @DESCRIPTION='View a Vacancy', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Vacancy.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISACTIVE=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;

--Begin update document type permissions
EXEC document.UpdateDocumentPermissions
--End update document type permissions
--End table permissionable.Permissionable

--Begin table core.MenuItemPermissionableLineage
DELETE MIPL FROM core.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table core.MenuItemPermissionableLineage

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.10 - 2019.03.07 19.58.36')
GO
--End build tracking

