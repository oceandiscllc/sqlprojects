--Begin table core.EmailTemplate
EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'OperationalTrainingExpiration', 
	'Sent to notifiy a user that one a training or qualification is about to expire',
	'Training Validity is about to Expire',
	'<p>The following training validity will expire soon.<br /><br />Roster member: [[ToFullName]]<br />Training Course: [[OperationalTrainingCourseName]]<br />Expiry Date: [[ExpiryDateFormatted]]<br /><br />Please log in to view the training records<br /><br />Please do not reply to this email as it is generated automatically by the HERMIS system.<br /><br />HERMIS Support Team<br /><br />If you need support, please email [[FeedBackMailTo]].</p>',
	'[{"[[ToFullName]]":"Name of the email recipient"},{"[[OperationalTrainingCourseName]]":"Name of the expiring course"},{"[[ExpiryDateFormatted]]":"Expiration date"},{"[[FeedBackMailTo]]":"Support email address"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'ProfileReview', 
	'Sent to notifiy a user that a DeployAdviser profile review is coming due',
	'CSG Roster – Profile Update',
	'<p>Dear [[ToFullName]],<br /><br />Please sign in to your [[DeployAdviserLink]] account to update your profile.&nbsp;&nbsp;Please ensure you update your cv and your summary biography.&nbsp;&nbsp;An up to date cv and summary biography ensure that SU is fully aware of your experience and able to consider you for relevant tasks.<br />If the link above does not work, cut and paste the following URL into the address bar of your browser [[DeployAdviserURL]].<br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br /><br />The SU Recruitment Team</p>',
	'[{"[[ToFullName]]":"Name of the email recipient"},{"[[DeployAdviserLink]]":"Link to the DeployAdviser web site"},{"[[DeployAdviserURL]]":"URL to the DeployAdviser web site"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'SecurityClearanceExpiration', 
	'Sent to notifiy a user the mandatory review data for a security clearance is approaching', 
	'A Vetting is about to Expire', 
	'<p>A vetting will expire soon.<br /><br />Vetting Level: [[SecurityClearanceName]]<br />Expiry Date: [[MandatoryReviewDateFormatted]]<br /><br />Please log in to view the vetting records<br /><br />Please do not reply to this email as it is generated automatically by the HERMIS system.<br /><br />HERMIS Support Team<br /><br />If you need support, please email [[FeedBackMailTo]].</p>',
	'[{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[SecurityClearanceName]]":"Security clearance name"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'MedicalVetting', 
	'MedicalClearanceReview', 
	'Sent to notifiy a user that one or more medical reviews are required', 
	'Medical Clearance Review', 
	'<p>Dear [[ToFullName]],<br /><br />You are required to undergo a [[MedicalClearanceTypeName]] medical check.<br />Please find attached the relevant instructions.<br />Please arrange you check directly with the service provider: Medfit.<br />If you need assistance please contact [Palladium email].<br />Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br /><br />[[Comments]]<br /><br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br /><br />The Palladium HR Team</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[MedicalClearanceTypeName]]":"Medical Clearance Types"},{"[[Comments]]":"Sender comments"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'BPPSDiscretionaryClearanceReview', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'BPPSInitialClearanceReview', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO
--End table core.EmailTemplate

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'MedicalVetting', 
	@EntityTypeName = 'Medical Clearance', 
	@EntityTypeNamePlural = 'Medical Clearances',
	@SchemaName = 'person', 
	@TableName = 'PersonMedicalClearance', 
	@PrimaryKeyFieldName = 'PersonMedicalClearanceID'

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonGroup', 
	@EntityTypeName = 'User Group', 
	@EntityTypeNamePlural = 'User Groups',
	@SchemaName = 'person', 
	@TableName = 'PersonGroup', 
	@PrimaryKeyFieldName = 'PersonGroupID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Vetting', 
	@EntityTypeName = 'Security Clearance', 
	@EntityTypeNamePlural = 'Security Clearances',
	@SchemaName = 'person', 
	@TableName = 'PersonSecurityClearance', 
	@PrimaryKeyFieldName = 'PersonSecurityClearanceID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ConsultantList',
	@NewMenuItemCode = 'MedicalVettingList',
	@NewMenuItemLink = '/person/medicalvettinglist',
	@NewMenuItemText = 'Medical Clearances',
	@ParentMenuItemCode = 'HumanResources',
	@PermissionableLineageList = 'Person.MedicalVettingList'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'PersonGroup',
	@NewMenuItemLink = '/persongroup/list',
	@NewMenuItemText = 'User Groups',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'persongroup.list'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'VettingList',
	@NewMenuItemCode = 'VettingList',
	@NewMenuItemLink = '/person/vettinglist',
	@NewMenuItemText = 'Security Clearances',
	@ParentMenuItemCode = 'HumanResources',
	@PermissionableLineageList = 'Person.VettingList'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'HumanResources'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'ProfileReviewEmailDayCount', 'The number of days prior to the next review date at which the profile review email is triggered', '30'
EXEC core.SystemSetupAddUpdate 'OperationalTrainingExpirationEmailDayCount', 'The number of days prior to the expiration date at which the operational training expiration email is triggered', '30'
EXEC core.SystemSetupAddUpdate 'SecurityClearanceExpirationEmailDayCount', 'The number of days prior to the expiration date at which the security clearance expiration email is triggered', '30'
GO
--End table core.SystemSetup

--Begin table dropdown.ApprovalAuthorityLevel
TRUNCATE TABLE dropdown.ApprovalAuthorityLevel
GO

EXEC utility.InsertIdentityValue 'dropdown.ApprovalAuthorityLevel', 'ApprovalAuthorityLevelID', 0
GO

INSERT INTO dropdown.ApprovalAuthorityLevel 
	(ApprovalAuthorityLevelName, ApprovalAuthorityLevelCode, DisplayOrder)
VALUES
	('Creator', 'Creator', 1),
	('Reviewer', 'Reviewer', 2),
	('Approver', 'Approver', 3)
GO
--End table dropdown.ApprovalAuthorityLevel

--Begin table dropdown.MedicalClearanceProcessStep
TRUNCATE TABLE dropdown.MedicalClearanceProcessStep
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceProcessStep', 'MedicalClearanceProcessStepID', 0
GO

INSERT INTO dropdown.MedicalClearanceProcessStep 
	(MedicalClearanceProcessStepCode, MedicalClearanceProcessStepName, DisplayOrder) 
VALUES
	('Complete', 'Complete', 1),
	('InProcess', 'In Process', 2),
	('InPreparation', 'In Preparation', 3)
GO
--End table dropdown.MedicalClearanceProcessStep

--Begin table dropdown.MedicalClearanceStatus
TRUNCATE TABLE dropdown.MedicalClearanceStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceStatus', 'MedicalClearanceStatusID', 0
GO

INSERT INTO dropdown.MedicalClearanceStatus 
	(MedicalClearanceStatusCode, MedicalClearanceStatusName, DisplayOrder) 
VALUES
	('Granted', 'Granted', 2),
	('NotGranted', 'Not Granted', 3)
GO
--End table dropdown.MedicalClearanceStatus

--Begin table dropdown.MedicalClearanceType
TRUNCATE TABLE dropdown.MedicalClearanceType
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceType', 'MedicalClearanceTypeID', 0
GO

INSERT INTO dropdown.MedicalClearanceType 
	(MedicalClearanceTypeCode, MedicalClearanceTypeName, DisplayOrder) 
VALUES
	('Physical', 'Physical', 1),
	('Psychological', 'Psychological', 2)
GO
--End table dropdown.MedicalClearanceType

--Begin table dropdown.SecurityClearance
TRUNCATE TABLE dropdown.SecurityClearance
GO

EXEC utility.InsertIdentityValue 'dropdown.SecurityClearance', 'SecurityClearanceID', 0
GO

UPDATE SC
SET 
	SC.SecurityClearanceCode = 'NONE',
	SC.SecurityClearanceName = 'No Clearance',
	SC.DisplayOrder = 1
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceID = 0
GO

INSERT INTO dropdown.SecurityClearance 
	(SecurityClearanceCode, SecurityClearanceName, DisplayOrder) 
VALUES
	('BPSS', 'BPSS', 2),
	('DV', 'DV + EU Equivalent Certificate', 9),
	('DV', 'DV + NATO and EU Equivalent Certificates', 10),
	('DV', 'DV + NATO Equivalent Certificate', 8),
	('DV', 'DV', 4),
	('SC', 'SC + EU Equivalent Certificate', 6),
	('SC', 'SC + NATO and EU Equivalent Certificates', 7),
	('SC', 'SC + NATO Equivalent Certificate', 5),
	('SC', 'SC', 3)
GO
--End table dropdown.SecurityClearance

--Begin table dropdown.SecurityClearanceProcessStep
TRUNCATE TABLE dropdown.SecurityClearanceProcessStep
GO

EXEC utility.InsertIdentityValue 'dropdown.SecurityClearanceProcessStep', 'SecurityClearanceProcessStepID', 0
GO

INSERT INTO dropdown.SecurityClearanceProcessStep 
	(SecurityClearanceProcessStepCode, SecurityClearanceProcessStepName, DisplayOrder) 
VALUES
	('COMPLETE', 'Discretionary Review Process Complete - Granted', 7),
	('COMPLETE', 'Discretionary Review Process Complete - Ineligible', 8),
	('COMPLETE', 'Process Complete - Granted', 4),
	('COMPLETE', 'Process Complete - Not Granted', 5),
	('NONE', 'Application Not Submitted', 2),
	('PREP', 'Application in Preperation', 1),
	('PROCESS', 'Application in Process', 3),
	('REVIEW', 'Under Review', 6)
GO
--End table dropdown.SecurityClearanceProcessStep

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the medical clearance list', 
	@METHODNAME='MedicalVettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.MedicalVettingList', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Send bulk emails from the medical clearance list', 
	@METHODNAME='MedicalVettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.MedicalVettingList.BullkEmail', 
	@PERMISSIONCODE='BullkEmail';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the security clearance list', 
	@METHODNAME='VettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.VettingList', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Send bulk emails from the security clearance list', 
	@METHODNAME='VettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.VettingList.BullkEmail', 
	@PERMISSIONCODE='BullkEmail';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonGroup', 
	@DESCRIPTION='Add / edit a user group', 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonGroup.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonGroup', 
	@DESCRIPTION='View the list of user groups',
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonGroup.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonGroup', 
	@DESCRIPTION='View a user group', 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonGroup.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable
