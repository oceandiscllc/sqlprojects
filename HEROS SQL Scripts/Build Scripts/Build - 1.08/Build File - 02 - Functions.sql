--Begin function person.GetMedicalVettingStatusIcon
EXEC utility.DropObject 'person.GetMedicalVettingStatusIcon'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2018.12.24
-- Description:	A function to return a medical vetting status icon
-- ===============================================================

CREATE FUNCTION person.GetMedicalVettingStatusIcon
(
@PersonMedicalClearanceID INT,
@MedicalClearanceTypeID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100) = '<a class="btn btn-xs btn-icon btn-circle btn-dark m-r-10" title="None"></a>'

	IF @PersonMedicalClearanceID > 0
		BEGIN

		SELECT @cReturn = 
			CASE
				WHEN MCS.MedicalClearanceStatusCode = 'NotGranted'
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="' + MCS.MedicalClearanceStatusName + '"></a>'
				WHEN PMC.MandatoryReviewDate < getDate()
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="Expired"></a>'
				WHEN MCS.MedicalClearanceStatusCode = 'Granted' AND DATEDIFF(d, getDate(), PMC.MandatoryReviewDate) < 90
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-warning m-r-10" title="Review Required"></a>'
				ELSE '<a class="btn btn-xs btn-icon btn-circle btn-green m-r-10" title="Active"></a>'
			END
		FROM person.PersonMedicalClearance PMC
			JOIN dropdown.MedicalClearanceStatus MCS ON MCS.MedicalClearanceStatusID = PMC.MedicalClearanceStatusID
				AND PMC.PersonMedicalClearanceID = @PersonMedicalClearanceID
				AND PMC.MedicalClearanceTypeID = @MedicalClearanceTypeID

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function person.GetMedicalVettingStatusIcon

--Begin function person.GetVettingStatusIcon
EXEC utility.DropObject 'person.GetVettingStatusIcon'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2018.12.21
-- Description:	A function to return a vetting status icon
-- =======================================================

CREATE FUNCTION person.GetVettingStatusIcon
(
@PersonSecurityClearanceID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100) = '<a class="btn btn-xs btn-icon btn-circle btn-dark m-r-10" title="None"></a>'

	IF EXISTS (SELECT 1 FROM person.PersonSecurityClearance PSC WHERE PSC.PersonSecurityClearanceID = @PersonSecurityClearanceID AND PSC.ClearanceGrantedDate IS NOT NULL)
		BEGIN

		SELECT @cReturn = 
			CASE
				WHEN PSC.MandatoryReviewDate < getDate()
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="Lapsed"></a>'
				WHEN DATEDIFF(d, getDate(), PSC.MandatoryReviewDate) < 180
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-yellow m-r-10" title="Review Required"></a>'
				ELSE '<a class="btn btn-xs btn-icon btn-circle btn-green m-r-10" title="Active"></a>'
			END
		FROM person.PersonSecurityClearance PSC
		WHERE PSC.PersonSecurityClearanceID = @PersonSecurityClearanceID

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function person.GetVettingStatusIcon
