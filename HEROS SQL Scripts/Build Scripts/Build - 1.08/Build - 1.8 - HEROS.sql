-- File Name:	Build - 1.8 - HEROS.sql
-- Build Key:	Build - 1.8 - 2019.02.07 17.40.23

--USE HermisCloud
GO

-- ==============================================================================================================================
-- Tables:
--		core.PendingEmail
--		core.WorldClock
--		dropdown.ApprovalAuthorityLevel
--		dropdown.MedicalClearanceProcessStep
--		dropdown.MedicalClearanceStatus
--		dropdown.SecurityClearance
--		dropdown.SecurityClearanceProcessStep
--		person.PersonGroup
--		person.PersonGroupPerson
--		person.PersonMedicalClearance
--		person.PersonMedicalClearanceProcessStep
--		person.PersonSecurityClearance
--		person.PersonSecurityClearanceProcessStep
--
-- Triggers:
--		person.TR_PersonMedicalClearance ON person.PersonMedicalClearance
--		person.TR_PersonMedicalClearanceProcessStep ON person.PersonMedicalClearanceProcessStep
--		person.TR_PersonSecurityClearance ON person.PersonSecurityClearance
--		person.TR_PersonSecurityClearanceProcessStep ON person.PersonSecurityClearanceProcessStep
--
-- Functions:
--		person.GetMedicalVettingStatusIcon
--		person.GetVettingStatusIcon
--
-- Procedures:
--		core.CreatePendingEmail
--		core.DeletePendingEmailByPendingEmailID
--		core.EmailTemplateAddUpdate
--		core.GetEmailTemplateByEmailTemplateID
--		core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
--		core.GetPendingEmail
--		core.GetPendingPersonOperationalTrainingExpirationEmail
--		core.GetPendingPersonProfileReviewEmail
--		core.GetPendingPersonSecurityClearanceExpirationEmail
--		document.GetDocumentsByDocumentEntityCodeList
--		dropdown.GetApprovalAuthorityLevelData
--		dropdown.GetMedicalClearanceProcessStepData
--		dropdown.GetMedicalClearanceStatusData
--		dropdown.GetSecurityClearanceData
--		dropdown.GetSecurityClearanceProcessStepData
--		eventlog.LogEmailTemplateAction
--		eventlog.LogPersonGroupAction
--		person.GetMedicalVettingEmailData
--		person.GetPersonByPersonID
--		person.GetPersonGroupByPersonGroupID
--		person.GetPersonMedicalClearanceByPersonID
--		person.GetPersonMedicalClearanceProcessStepByPersonID
--		person.GetPersonSecurityClearanceByPersonID
--		person.GetPersonSecurityClearanceProcessStepByPersonID
--		person.GetVettingEmailData
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--Begin table core.PendingEmail
DECLARE @TableName VARCHAR(250) = 'core.PendingEmail'

EXEC utility.DropObject @TableName

CREATE TABLE core.PendingEmail
	(
	PendingEmailID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EmailTemplateCode VARCHAR(50),
	EntityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PendingEmailID'
GO
--End table core.PendingEmail

--Begin table core.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplate'

EXEC utility.AddColumn @TableName, 'EmailTemplateDescription', 'VARCHAR(500)'
GO
--End table core.EmailTemplate

--Begin table core.WorldClock
DECLARE @TableName VARCHAR(250) = 'core.WorldClock'

EXEC utility.DropObject @TableName

CREATE TABLE core.WorldClock
	(
	WorldClockID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	TimeZoneName VARCHAR(250),
	WorldClockName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'WorldClockID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorldClock', 'EntityTypeCode,EntityID,DisplayOrder,WorldClockName,WorldClockID'
GO
--End table core.WorldClock

--Begin table dropdown.ApprovalAuthorityLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.ApprovalAuthorityLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ApprovalAuthorityLevel
	(
	ApprovalAuthorityLevelID INT IDENTITY(0,1) NOT NULL,
	ApprovalAuthorityLevelCode VARCHAR(50),
	ApprovalAuthorityLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApprovalAuthorityLevelID'
GO
--End table dropdown.ApprovalAuthorityLevel

--Begin table dropdown.MedicalClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MedicalClearanceProcessStep
	(
	MedicalClearanceProcessStepID INT IDENTITY(0,1) NOT NULL,
	MedicalClearanceProcessStepCode VARCHAR(50) NULL,
	MedicalClearanceProcessStepName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MedicalClearanceProcessStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MedicalClearanceProcessStep', 'DisplayOrder,MedicalClearanceProcessStepName', 'MedicalClearanceProcessStepID'
GO
--End table dropdown.MedicalClearanceProcessStep

--Begin table dropdown.MedicalClearanceStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MedicalClearanceStatus
	(
	MedicalClearanceStatusID INT IDENTITY(0,1) NOT NULL,
	MedicalClearanceStatusCode VARCHAR(50) NULL,
	MedicalClearanceStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MedicalClearanceStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MedicalClearanceStatus', 'DisplayOrder,MedicalClearanceStatusName', 'MedicalClearanceStatusID'
GO
--End table dropdown.MedicalClearanceStatus

--Begin table dropdown.MedicalClearanceType
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceType'

EXEC utility.AddColumn @TableName, 'MedicalClearanceTypeCode', 'VARCHAR(50)'
GO
--End table dropdown.MedicalClearanceType

--Begin table dropdown.SecurityClearance
DECLARE @TableName VARCHAR(250) = 'dropdown.SecurityClearance'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.SecurityClearanceLevel'

CREATE TABLE dropdown.SecurityClearance
	(
	SecurityClearanceID INT IDENTITY(0,1) NOT NULL,
	SecurityClearanceCode VARCHAR(50) NULL,
	SecurityClearanceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SecurityClearanceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SecurityClearance', 'DisplayOrder,SecurityClearanceName', 'SecurityClearanceID'
GO
--End table dropdown.SecurityClearance

--Begin table dropdown.SecurityClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'dropdown.SecurityClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SecurityClearanceProcessStep
	(
	SecurityClearanceProcessStepID INT IDENTITY(0,1) NOT NULL,
	SecurityClearanceProcessStepCode VARCHAR(50) NULL,
	SecurityClearanceProcessStepName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SecurityClearanceProcessStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SecurityClearanceProcessStep', 'DisplayOrder,SecurityClearanceProcessStepName', 'SecurityClearanceProcessStepID'
GO
--End table dropdown.SecurityClearanceProcessStep

--Begin table person.PersonGroup
DECLARE @TableName VARCHAR(250) = 'person.PersonGroup'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonGroup
	(
	PersonGroupID INT IDENTITY(1,1) NOT NULL,
	PersonGroupName VARCHAR(250),
	PersonGroupDescription VARCHAR(MAX),
	ApprovalAuthorityLevelID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApprovalAuthorityLevelID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonGroupID'
GO
--End table person.PersonGroup

--Begin table person.PersonGroupPerson
DECLARE @TableName VARCHAR(250) = 'person.PersonGroupPerson'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonGroupPerson
	(
	PersonGroupPersonID INT IDENTITY(1,1) NOT NULL,
	PersonGroupID INT,
	PersonID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'PersonGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonGroupPerson', 'PersonGroupID,PersonID'
GO
--End table person.PersonGroupPerson

--Begin table person.PersonMedicalClearance
DECLARE @TableName VARCHAR(250) = 'person.PersonMedicalClearance'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonMedicalClearance
	(
	PersonMedicalClearanceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	MedicalClearanceTypeID INT,
	MedicalClearanceStatusID INT,
	ClearanceGrantedDate DATE,
	MandatoryReviewDate DATE,
	VettingAuthorityName VARCHAR(250),
	Limitations VARCHAR(250),
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MedicalClearanceTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonMedicalClearanceID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonMedicalClearance', 'PersonID,MedicalClearanceTypeID'
GO

EXEC utility.DropObject 'person.TR_PersonMedicalClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonMedicalClearance table
-- =========================================================================
CREATE TRIGGER person.TR_PersonMedicalClearance ON person.PersonMedicalClearance AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PMC
	SET PMC.IsActive = 0
	FROM person.PersonMedicalClearance PMC
		JOIN INSERTED I ON I.PersonID = PMC.PersonID
			AND I.PersonMedicalClearanceID <> PMC.PersonMedicalClearanceID
			AND I.MedicalClearanceTypeID = PMC.MedicalClearanceTypeID

GO
--End table person.PersonMedicalClearance

--Begin table person.PersonMedicalClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'person.PersonMedicalClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonMedicalClearanceProcessStep
	(
	PersonMedicalClearanceProcessStepID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	MedicalClearanceTypeID INT,
	MedicalClearanceProcessStepID INT,
	StatusDate DATE,
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MedicalClearanceTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MedicalClearanceProcessStepID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonMedicalClearanceProcessStepID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonMedicalClearanceProcessStep', 'PersonID,MedicalClearanceTypeID,MedicalClearanceProcessStepID'
GO

EXEC utility.DropObject 'person.TR_PersonMedicalClearanceProcessStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonMedicalClearanceProcessStep table
-- ====================================================================================
CREATE TRIGGER person.TR_PersonMedicalClearanceProcessStep ON person.PersonMedicalClearanceProcessStep AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PMCPS
	SET PMCPS.IsActive = 0
	FROM person.PersonMedicalClearanceProcessStep PMCPS
		JOIN INSERTED I ON I.PersonID = PMCPS.PersonID
			AND I.PersonMedicalClearanceProcessStepID <> PMCPS.PersonMedicalClearanceProcessStepID
			AND I.MedicalClearanceTypeID = PMCPS.MedicalClearanceTypeID

GO
--End table person.PersonMedicalClearanceProcessStep

--Begin table person.PersonSecurityClearance
DECLARE @TableName VARCHAR(250) = 'person.PersonSecurityClearance'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonSecurityClearance
	(
	PersonSecurityClearanceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	SecurityClearanceID INT,
	ClearanceGrantedDate DATE,
	DiscretionaryReviewDate DATE,
	MandatoryReviewDate DATE,
	VettingAuthorityName VARCHAR(250),
	SponsorName VARCHAR(250),
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonSecurityClearanceID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonSecurityClearance', 'PersonID,SecurityClearanceID'
GO

EXEC utility.DropObject 'person.TR_PersonSecurityClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonSecurityClearance table
-- =========================================================================
CREATE TRIGGER person.TR_PersonSecurityClearance ON person.PersonSecurityClearance AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PSC
	SET PSC.IsActive = 0
	FROM person.PersonSecurityClearance PSC
		JOIN INSERTED I ON I.PersonID = PSC.PersonID
			AND I.PersonSecurityClearanceID <> PSC.PersonSecurityClearanceID

GO
--End table person.PersonSecurityClearance

--Begin table person.PersonSecurityClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'person.PersonSecurityClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonSecurityClearanceProcessStep
	(
	PersonSecurityClearanceProcessStepID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	SecurityClearanceID INT,
	SecurityClearanceProcessStepID INT,
	StatusDate DATE,
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceProcessStepID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonSecurityClearanceProcessStepID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonSecurityClearanceProcessStep', 'PersonID,SecurityClearanceID,SecurityClearanceProcessStepID'
GO

EXEC utility.DropObject 'person.TR_PersonSecurityClearanceProcessStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonSecurityClearanceProcessStep table
-- ====================================================================================
CREATE TRIGGER person.TR_PersonSecurityClearanceProcessStep ON person.PersonSecurityClearanceProcessStep AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PSCPS
	SET PSCPS.IsActive = 0
	FROM person.PersonSecurityClearanceProcessStep PSCPS
		JOIN INSERTED I ON I.PersonID = PSCPS.PersonID
			AND I.PersonSecurityClearanceProcessStepID <> PSCPS.PersonSecurityClearanceProcessStepID

GO
--End table person.PersonSecurityClearanceProcessStep

--Begin table workflow.EntityWorkflowStepGroupPerson
EXEC Utility.AddColumn 'workflow.EntityWorkflowStepGroupPerson', 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.AddColumn 'workflow.EntityWorkflowStepGroupPerson', 'PersonGroupID', 'INT', '0'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.WorkflowStepGroupPerson
EXEC Utility.AddColumn 'workflow.WorkflowStepGroupPerson', 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.AddColumn 'workflow.WorkflowStepGroupPerson', 'PersonGroupID', 'INT', '0'
GO
--End table workflow.WorkflowStepGroupPerson
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
--Begin function person.GetMedicalVettingStatusIcon
EXEC utility.DropObject 'person.GetMedicalVettingStatusIcon'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2018.12.24
-- Description:	A function to return a medical vetting status icon
-- ===============================================================

CREATE FUNCTION person.GetMedicalVettingStatusIcon
(
@PersonMedicalClearanceID INT,
@MedicalClearanceTypeID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100) = '<a class="btn btn-xs btn-icon btn-circle btn-dark m-r-10" title="None"></a>'

	IF @PersonMedicalClearanceID > 0
		BEGIN

		SELECT @cReturn = 
			CASE
				WHEN MCS.MedicalClearanceStatusCode = 'NotGranted'
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="' + MCS.MedicalClearanceStatusName + '"></a>'
				WHEN PMC.MandatoryReviewDate < getDate()
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="Expired"></a>'
				WHEN MCS.MedicalClearanceStatusCode = 'Granted' AND DATEDIFF(d, getDate(), PMC.MandatoryReviewDate) < 90
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-warning m-r-10" title="Review Required"></a>'
				ELSE '<a class="btn btn-xs btn-icon btn-circle btn-green m-r-10" title="Active"></a>'
			END
		FROM person.PersonMedicalClearance PMC
			JOIN dropdown.MedicalClearanceStatus MCS ON MCS.MedicalClearanceStatusID = PMC.MedicalClearanceStatusID
				AND PMC.PersonMedicalClearanceID = @PersonMedicalClearanceID
				AND PMC.MedicalClearanceTypeID = @MedicalClearanceTypeID

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function person.GetMedicalVettingStatusIcon

--Begin function person.GetVettingStatusIcon
EXEC utility.DropObject 'person.GetVettingStatusIcon'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2018.12.21
-- Description:	A function to return a vetting status icon
-- =======================================================

CREATE FUNCTION person.GetVettingStatusIcon
(
@PersonSecurityClearanceID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100) = '<a class="btn btn-xs btn-icon btn-circle btn-dark m-r-10" title="None"></a>'

	IF EXISTS (SELECT 1 FROM person.PersonSecurityClearance PSC WHERE PSC.PersonSecurityClearanceID = @PersonSecurityClearanceID AND PSC.ClearanceGrantedDate IS NOT NULL)
		BEGIN

		SELECT @cReturn = 
			CASE
				WHEN PSC.MandatoryReviewDate < getDate()
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-danger m-r-10" title="Lapsed"></a>'
				WHEN DATEDIFF(d, getDate(), PSC.MandatoryReviewDate) < 180
				THEN '<a class="btn btn-xs btn-icon btn-circle btn-yellow m-r-10" title="Review Required"></a>'
				ELSE '<a class="btn btn-xs btn-icon btn-circle btn-green m-r-10" title="Active"></a>'
			END
		FROM person.PersonSecurityClearance PSC
		WHERE PSC.PersonSecurityClearanceID = @PersonSecurityClearanceID

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function person.GetVettingStatusIcon

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--Begin procedure core.CreatePendingEmail
EXEC utility.DropObject 'core.CreatePendingEmail'
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure populate the core.PendingEmail table
-- ====================================================================
CREATE PROCEDURE core.CreatePendingEmail

@TargetDate DATE = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SET @TargetDate = ISNULL(@TargetDate, getDate())

	INSERT INTO core.PendingEmail
		(EntityTypeCode, EmailTemplateCode, EntityID)
	SELECT
		'Person' AS EntityTypeCode,
		'ProfileReview' AS EmailTemplateCode,
		P.PersonID AS EntityID
	FROM person.Person P
	WHERE DATEDIFF(d, @TargetDate, P.NextReviewDate) = CAST(core.GetSystemSetupValueBySystemSetupKey('ProfileReviewEmailDayCount', '30') AS INT)

	UNION

	SELECT
		'Person' AS EntityTypeCode,
		'OperationalTrainingExpiration' AS EmailTemplateCode,
		POT.PersonOperationalTrainingID AS EntityID
	FROM person.PersonOperationalTraining POT
	WHERE DATEDIFF(d, @TargetDate, POT.ExpiryDate) = CAST(core.GetSystemSetupValueBySystemSetupKey('OperationalTrainingExpirationEmailDayCount', '30') AS INT)

	UNION

	SELECT
		'Vetting' AS EntityTypeCode,
		'SecurityClearanceExpiration' AS EmailTemplateCode,
		PSC.PersonSecurityClearanceID AS EntityID
	FROM person.PersonSecurityClearance PSC
	WHERE DATEDIFF(d, @TargetDate, PSC.MandatoryReviewDate) = CAST(core.GetSystemSetupValueBySystemSetupKey('OperationalTrainingExpirationEmailDayCount', '30') AS INT)

END
GO
--End procedure core.CreatePendingEmail

--Begin procedure core.DeletePendingEmailByPendingEmailID
EXEC utility.DropObject 'core.DeletePendingEmailByPendingEmailID'
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure delete data from the core.PendingEmail table
-- ============================================================================
CREATE PROCEDURE core.DeletePendingEmailByPendingEmailID

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.PendingEmailID = @PendingEmailID

END
GO
--End procedure core.DeletePendingEmailByPendingEmailID

--Begin procedure core.EmailTemplateAddUpdate
EXEC utility.DropObject 'core.EmailTemplateAddUpdate'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure to get add / updade data in the core.EmailTemplate table
-- ========================================================================================
CREATE PROCEDURE core.EmailTemplateAddUpdate

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50),
@EmailTemplateDescription VARCHAR(500),
@EmailSubject VARCHAR(500),
@EmailText VARCHAR(MAX),
@EmailTemplatFieldJSON VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cJSONData VARCHAR(MAX)
	DECLARE @cPlaceHolderDescription VARCHAR(100)
	DECLARE @cPlaceHolderText VARCHAR(50)

	IF EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.EmailTemplateCode = @EmailTemplateCode)
		BEGIN

		UPDATE ET
		SET
			ET.EmailTemplateDescription = @EmailTemplateDescription,
			ET.EmailSubject = @EmailSubject,
			ET.EmailText =  @EmailText
		FROM core.EmailTemplate ET
		WHERE ET.EntityTypeCode = @EntityTypeCode
			AND ET.EmailTemplateCode = @EmailTemplateCode

		END
 	ELSE
		BEGIN

		INSERT INTO core.EmailTemplate
			(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
		VALUES
			(@EntityTypeCode, @EmailTemplateCode, @EmailTemplateDescription, @EmailSubject, @EmailText)

		END
	--ENDIF

	IF @EmailTemplatFieldJSON IS NOT NULL
		BEGIN

		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT [Value] 
			FROM OPENJSON(@EmailTemplatFieldJSON)			
			ORDER BY 1
					
		OPEN oCursor
		FETCH oCursor INTO @cJSONData
		WHILE @@fetch_status = 0
			BEGIN

			INSERT INTO core.EmailTemplateField
				(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
			SELECT
				@EntityTypeCode,
				JD.[key],
				JD.[value]
			FROM OPENJSON(@cJSONData)	JD
			WHERE NOT EXISTS
				(
				SELECT 1 
				FROM core.EmailTemplateField ETF
				WHERE ETF.EntityTypeCode = @EntityTypeCode
					AND ETF.PlaceHolderText COLLATE DATABASE_DEFAULT = JD.[key] COLLATE DATABASE_DEFAULT
					AND ETF.PlaceHolderDescription COLLATE DATABASE_DEFAULT = JD.[value] COLLATE DATABASE_DEFAULT
				)

			FETCH oCursor INTO @cJSONData
					
			END
		--END WHILE
						
		CLOSE oCursor
		DEALLOCATE oCursor	

		END
	--ENDIF

END
GO
--End procedure core.EmailTemplateAddUpdate

--Begin procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	--EmailTemplateData
	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

	--EmailTemplateFieldData
	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

	--EmailTemplateGlobalData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS SiteURL,
		core.GetSystemSetupValueBySystemSetupKey('SystemName', '') AS SystemName

END
GO
--End procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateCode,
		ET.EmailTemplateDescription,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetPendingEmail
EXEC utility.DropObject 'core.GetPendingEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure get data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		core.GetSystemSetupValueBySystemSetupKey('FeedBackMailTo', '') AS FeedBackEmailAddress,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReplyEmailAddress,
		PE.EmailTemplateCode,
		PE.EntityTypeCode + PE.EmailTemplateCode AS PendingEmailCode,
		PE.EntityTypeCode
	FROM core.PendingEmail PE
	ORDER BY PE.EntityTypeCode + PE.EmailTemplateCode

END
GO
--End procedure core.GetPendingEmail

--Begin procedure core.GetPendingPersonOperationalTrainingExpirationEmail
EXEC utility.DropObject 'core.GetPendingPersonOperationalTrainingExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingPersonOperationalTrainingExpirationEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		OTC.OperationalTrainingCourseName,
		P.EmailAddress,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted
	FROM person.PersonOperationalTraining POT
		JOIN person.Person P ON P.PersonID = POT.PersonID
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'OperationalTrainingExpiration'
			AND PE.EntityID = POT.PersonOperationalTrainingID
	ORDER BY 1

END
GO
--End procedure core.GetPendingPersonOperationalTrainingExpirationEmail

--Begin procedure core.GetPendingPersonProfileReviewEmail
EXEC utility.DropObject 'core.GetPendingPersonProfileReviewEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingPersonProfileReviewEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted,
		'<a href="' + DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') + '">DeployAdviser</a>' AS DeployAdviserLink,
		DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS DeployAdviserURL
	FROM person.Person P
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'ProfileReview'
			AND PE.EntityID = P.PersonID
	ORDER BY 1

END
GO
--End procedure core.GetPendingPersonProfileReviewEmail

--Begin procedure core.GetPendingPersonSecurityClearanceExpirationEmail
EXEC utility.DropObject 'core.GetPendingPersonSecurityClearanceExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingPersonSecurityClearanceExpirationEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		core.FormatDate(PSC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		SC.SecurityClearanceName,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted
	FROM person.PersonSecurityClearance PSC
		JOIN person.Person P ON P.PersonID = PSC.PersonID
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'SecurityClearanceExpiration'
			AND PE.EntityID = PSC.PersonSecurityClearanceID
	ORDER BY 1

END
GO
--End procedure core.GetPendingPersonSecurityClearanceExpirationEmail

--Begin procedure document.GetDocumentsByDocumentEntityCodeList
EXEC Utility.DropObject 'document.GetDocumentsByDocumentEntityCodeList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data from the document.Document table
-- ===============================================================================
CREATE PROCEDURE document.GetDocumentsByDocumentEntityCodeList

@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentType + '/' + D.ContentSubtype AS MimeType,
		D.DocumentData,
		D.DocumentID,
		D.DocumentTitle
	FROM document.Document D
	WHERE EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
			JOIN core.ListToTable(@DocumentEntityCodeList, ',') LTT ON LTT.ListItem = DE.DocumentEntityCode
				AND DE.DocumentID = D.DocumentID
		)
	ORDER BY D.DocumentTitle, D.DocumentID

END
GO
--End procedure document.GetDocumentsByDocumentEntityCodeList

--Begin procedure dropdown.GetApprovalAuthorityLevelData
EXEC Utility.DropObject 'dropdown.GetApprovalAuthorityLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.12
-- Description:	A stored procedure to return data from the dropdown.ApprovalAuthorityLevel table
-- =============================================================================================

EXEC Utility.DropObject 'dropdown.GetApprovalAuthorityLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dropdown.GetApprovalAuthorityLevelData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ApprovalAuthorityLevelID,
		T.ApprovalAuthorityLevelCode,
		T.ApprovalAuthorityLevelName
	FROM dropdown.ApprovalAuthorityLevel T
	WHERE (T.ApprovalAuthorityLevelID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ApprovalAuthorityLevelName, T.ApprovalAuthorityLevelID

END
GO
--End table dropdown.ApprovalAuthorityLevel

--Begin procedure dropdown.GetMedicalClearanceStatusData
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceStatusData'
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceStatusLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.24
-- Description:	A stored procedure to return data from the dropdown.MedicalClearanceStatus table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetMedicalClearanceStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MedicalClearanceStatusID, 
		T.MedicalClearanceStatusCode,
		T.MedicalClearanceStatusName
	FROM dropdown.MedicalClearanceStatus T
	WHERE (T.MedicalClearanceStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MedicalClearanceStatusName, T.MedicalClearanceStatusID

END
GO
--End procedure dropdown.GetMedicalClearanceStatusData

--Begin procedure dropdown.GetMedicalClearanceProcessStepData
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceProcessStepData'
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceProcessStepLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.24
-- Description:	A stored procedure to return data from the dropdown.MedicalClearanceProcessStep table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetMedicalClearanceProcessStepData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MedicalClearanceProcessStepID, 
		T.MedicalClearanceProcessStepCode,
		T.MedicalClearanceProcessStepName
	FROM dropdown.MedicalClearanceProcessStep T
	WHERE (T.MedicalClearanceProcessStepID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MedicalClearanceProcessStepName, T.MedicalClearanceProcessStepID

END
GO
--End procedure dropdown.GetMedicalClearanceProcessStepData

--Begin procedure dropdown.GetSecurityClearanceData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceData'
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the dropdown.SecurityClearance table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SecurityClearanceID, 
		T.SecurityClearanceCode,
		T.SecurityClearanceName
	FROM dropdown.SecurityClearance T
	WHERE (T.SecurityClearanceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SecurityClearanceName, T.SecurityClearanceID

END
GO
--End procedure dropdown.GetSecurityClearanceData

--Begin procedure dropdown.GetSecurityClearanceProcessStepData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceProcessStepData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the dropdown.SecurityClearanceProcessStep table
-- ===================================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceProcessStepData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SecurityClearanceProcessStepID, 
		T.SecurityClearanceProcessStepCode,
		T.SecurityClearanceProcessStepName
	FROM dropdown.SecurityClearanceProcessStep T
	WHERE (T.SecurityClearanceProcessStepID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SecurityClearanceProcessStepName, T.SecurityClearanceProcessStepID

END
GO
--End procedure dropdown.GetSecurityClearanceProcessStepData

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EmailTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM core.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure eventlog.LogPersonGroupAction
EXEC utility.DropObject 'eventlog.LogPersonGroupAction'
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonGroupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonGroup'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPersonGroupPerson VARCHAR(MAX) = ''
		
		SELECT @cPersonGroupPerson = COALESCE(@cPersonGroupPerson, '') + D.PersonGroupPerson
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonGroupPerson'), ELEMENTS) AS PersonGroupPerson
			FROM person.PersonGroupPerson T 
			WHERE T.PersonGroupID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PersonGroupPerson>' + ISNULL(@cPersonGroupPerson, '') + '</PersonGroupPerson>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.PersonGroup T
		WHERE T.PersonGroupID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonGroupAction

--Begin procedure person.GetMedicalVettingEmailData
EXEC Utility.DropObject 'person.GetMedicalVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.16
-- Description:	A stored procedure to return data for outgoing medical vetting e-mail
-- ==================================================================================
CREATE PROCEDURE person.GetMedicalVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@MedicalClearanceTypeIDList VARCHAR(MAX),
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonMedicalClearanceProcessStep
		(PersonID, MedicalClearanceTypeID, MedicalClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		CAST(LTT2.ListItem AS INT),
		(SELECT MCPS.MedicalClearanceProcessStepID FROM dropdown.MedicalClearanceProcessStep MCPS WHERE MCPS.MedicalClearanceProcessStepCode = 'InPreparation'),
		getDate(),
		1,
		'Medical clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT1 ON CAST(LTT1.ListItem AS INT) = P.PersonID
		CROSS APPLY core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT2

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		(SELECT 
			REPLACE(STUFF((
				SELECT ',' + MedicalClearanceTypeName
				FROM dropdown.MedicalClearanceType MCT
					JOIN core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MCT.MedicalClearanceTypeID
				ORDER BY 1
				FOR XML PATH('')), 1, 1, ''), ',', ' and ')) AS MedicalClearanceTypeName
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL AS MedicalClearanceTypeName

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetMedicalVettingEmailData

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAPersonID,
		P.DefaultProjectID,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,		
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKWorkEligible,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		P.MiddleName,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR.PoliceRankID,
		PR.PoliceRankName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName,
		ISNULL((SELECT TOP 1 SC.SecurityClearanceName + ' - ' + core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID AND PSC.IsActive = 1 AND PSC.PersonID = P.PersonID), 'None') AS Vetting
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR ON PR.PoliceRankID = P.PoliceRankID
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
	WHERE P.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'PersonCorrespondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEXC
		JOIN dropdown.Country C ON C.CountryID = PEXC.CountryID
			AND PEXC.PersonID = @PersonID
	
	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertStatus
	SELECT
		ES.ExpertStatusID,
		ES.ExpertStatusName
	FROM person.PersonExpertStatus PES
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PES.ExpertStatusID
			AND ES.ExpertStatusID > 0
			AND PES.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonGroupByPersonGroupID
EXEC Utility.DropObject 'person.GetPersonGroupByPersonGroupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.13
-- Description:	A stored procedure to return data from the person.PersonGroup table
-- ================================================================================

CREATE PROCEDURE person.GetPersonGroupByPersonGroupID

@PersonGroupID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonGroup
	SELECT 
		PG.PersonGroupID,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
			SELECT COUNT( DISTINCT W.WorkflowID) 
			FROM workflow.WorkflowStepGroupPerson WSGP
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
				JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			WHERE W.IsActive = 1
			AND WSGP.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT( DISTINCT PersonID) FROM person.PersonGroupPerson PGP WHERE PGP.PersonGroupID = PG.PersonGroupID) AS UserCount,
		AAL.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName,
		PG.CreateDateTime,
		core.FormatDate(PG.CreateDateTime) AS CreateDateTimeFormatted
	FROM person.PersonGroup PG
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PG.PersonGroupID = @PersonGroupID

	--PersonGroupPerson
	SELECT
		PGP.PersonID,
		person.FormatPersonNameByPersonID(PGP.PersonID,'LASTFIRST') AS FullName,
		P.UserName,
		P.Organization,
		R.RoleName,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateTimeFormatted
	FROM person.PersonGroupPerson PGP
	JOIN person.Person P ON P.PersonID = PGP.PersonID
	JOIN dropdown.Role R ON R.RoleID = P.RoleID
	WHERE PGP.PersonGroupID = @PersonGroupID
	ORDER BY P.LastName

	--PersonGroupWorkflows
	SELECT W.WorkflowID, P.ProjectName, P.ProjectID, W.WorkflowName, ET.EntityTypeName, WS.WorkflowStepNumber, core.FormatDate(WSGP.CreateDateTime) AS CreateDateFormatted
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		JOIN dropdown.Project P ON P.ProjectID = W.ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = W.EntityTypeCode
	WHERE W.IsActive = 1
	AND P.IsActive = 1
	AND WSGP.PersonGroupID = @PersonGroupID

END
GO
--End procedure person.GetPersonGroupByPersonGroupID

--Begin procedure person.GetPersonMedicalClearanceByPersonID
EXEC Utility.DropObject 'person.GetPersonMedicalClearanceByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.07
-- Description:	A stored procedure to return data from the person.PersonMedicalClearance table
-- ===========================================================================================
CREATE PROCEDURE person.GetPersonMedicalClearanceByPersonID

@PersonID INT,
@MedicalClearanceTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PMC.ClearanceGrantedDate,
		core.FormatDate(PMC.ClearanceGrantedDate) AS ClearanceGrantedDateFormatted,
		PMC.Limitations,
		PMC.MandatoryReviewDate,
		core.FormatDate(PMC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		PMC.MedicalClearanceStatusID,
		PMC.MedicalClearanceTypeID,
		PMC.Notes,
		PMC.PersonMedicalClearanceID,
		PMC.VettingAuthorityName
	FROM person.PersonMedicalClearance PMC
		JOIN dropdown.MedicalClearanceType MCT ON MCT.MedicalClearanceTypeID = PMC.MedicalClearanceTypeID
			AND PMC.PersonID = @PersonID
			AND PMC.IsActive = 1
			AND MCT.MedicalClearanceTypeCode = @MedicalClearanceTypeCode

END
GO
--End procedure person.GetPersonMedicalClearanceByPersonID

--Begin procedure person.GetPersonMedicalClearanceProcessStepByPersonID
EXEC Utility.DropObject 'person.GetPersonMedicalClearanceProcessStepByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.08
-- Description:	A stored procedure to return data from the person.PersonMedicalClearanceProcessStep table
-- ======================================================================================================
CREATE PROCEDURE person.GetPersonMedicalClearanceProcessStepByPersonID

@PersonID INT,
@MedicalClearanceTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PMCPS.MedicalClearanceProcessStepID,
		PMCPS.MedicalClearanceTypeID,
		PMCPS.Notes,
		PMCPS.PersonMedicalClearanceProcessStepID,
		PMCPS.StatusDate,
		core.FormatDate(PMCPS.StatusDate) AS StatusDateFormatted
	FROM person.PersonMedicalClearanceProcessStep PMCPS
		JOIN dropdown.MedicalClearanceType MCT ON MCT.MedicalClearanceTypeID = PMCPS.MedicalClearanceTypeID
			AND PMCPS.PersonID = @PersonID
			AND PMCPS.IsActive = 1
			AND MCT.MedicalClearanceTypeCode = @MedicalClearanceTypeCode

END
GO
--End procedure person.GetPersonMedicalClearanceProcessStepByPersonID

--Begin procedure person.GetPersonSecurityClearanceByPersonID
EXEC Utility.DropObject 'person.GetPersonSecurityClearanceByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the person.PersonSecurityClearance table
-- ============================================================================================
CREATE PROCEDURE person.GetPersonSecurityClearanceByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PSC.ClearanceGrantedDate,
		core.FormatDate(PSC.ClearanceGrantedDate) AS ClearanceGrantedDateFormatted,
		PSC.DiscretionaryReviewDate,
		core.FormatDate(PSC.DiscretionaryReviewDate) AS DiscretionaryReviewDateFormatted,
		PSC.MandatoryReviewDate,
		core.FormatDate(PSC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		PSC.Notes,
		PSC.PersonSecurityClearanceID,
		PSC.SecurityClearanceID,
		PSC.SponsorName,
		PSC.VettingAuthorityName
	FROM person.PersonSecurityClearance PSC
	WHERE PSC.PersonID = @PersonID
		AND PSC.IsActive = 1

END
GO
--End procedure person.GetPersonSecurityClearanceByPersonID

--Begin procedure person.GetPersonSecurityClearanceProcessStepByPersonID
EXEC Utility.DropObject 'person.GetPersonSecurityClearanceProcessStepByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the person.PersonSecurityClearanceProcessStep table
-- =======================================================================================================
CREATE PROCEDURE person.GetPersonSecurityClearanceProcessStepByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PSCPS.Notes,
		PSCPS.PersonID,
		PSCPS.PersonSecurityClearanceProcessStepID,
		PSCPS.SecurityClearanceID,
		PSCPS.SecurityClearanceProcessStepID,
		PSCPS.StatusDate,
		core.FormatDate(PSCPS.StatusDate) AS StatusDateFormatted
	FROM person.PersonSecurityClearanceProcessStep PSCPS
	WHERE PSCPS.PersonID = @PersonID
		AND PSCPS.IsActive = 1

END
GO
--End procedure person.GetPersonSecurityClearanceProcessStepByPersonID

--Begin procedure person.GetVettingEmailData
EXEC Utility.DropObject 'person.GetVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data for outgoing vetting e-mail
-- ==========================================================================
CREATE PROCEDURE person.GetVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonSecurityClearanceProcessStep
		(PersonID, SecurityClearanceID, SecurityClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		(SELECT SCPS.SecurityClearanceID FROM dropdown.SecurityClearance SCPS WHERE SCPS.SecurityClearanceCode = 'BPSS'),
		(SELECT SCPS.SecurityClearanceProcessStepID FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = 'PREP'),
		getDate(),
		1,
		'Secrity clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.DiscretionaryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS DiscretionaryReviewDateFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS MandatoryReviewDateFormatted
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL,
		NULL

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetVettingEmailData

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--Begin table core.EmailTemplate
EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'OperationalTrainingExpiration', 
	'Sent to notifiy a user that one a training or qualification is about to expire',
	'Training Validity is about to Expire',
	'<p>The following training validity will expire soon.<br /><br />Roster member: [[ToFullName]]<br />Training Course: [[OperationalTrainingCourseName]]<br />Expiry Date: [[ExpiryDateFormatted]]<br /><br />Please log in to view the training records<br /><br />Please do not reply to this email as it is generated automatically by the HERMIS system.<br /><br />HERMIS Support Team<br /><br />If you need support, please email [[FeedBackMailTo]].</p>',
	'[{"[[ToFullName]]":"Name of the email recipient"},{"[[OperationalTrainingCourseName]]":"Name of the expiring course"},{"[[ExpiryDateFormatted]]":"Expiration date"},{"[[FeedBackMailTo]]":"Support email address"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'ProfileReview', 
	'Sent to notifiy a user that a DeployAdviser profile review is coming due',
	'CSG Roster â Profile Update',
	'<p>Dear [[ToFullName]],<br /><br />Please sign in to your [[DeployAdviserLink]] account to update your profile.&nbsp;&nbsp;Please ensure you update your cv and your summary biography.&nbsp;&nbsp;An up to date cv and summary biography ensure that SU is fully aware of your experience and able to consider you for relevant tasks.<br />If the link above does not work, cut and paste the following URL into the address bar of your browser [[DeployAdviserURL]].<br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br /><br />The SU Recruitment Team</p>',
	'[{"[[ToFullName]]":"Name of the email recipient"},{"[[DeployAdviserLink]]":"Link to the DeployAdviser web site"},{"[[DeployAdviserURL]]":"URL to the DeployAdviser web site"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Person', 
	'SecurityClearanceExpiration', 
	'Sent to notifiy a user the mandatory review data for a security clearance is approaching', 
	'A Vetting is about to Expire', 
	'<p>A vetting will expire soon.<br /><br />Vetting Level: [[SecurityClearanceName]]<br />Expiry Date: [[MandatoryReviewDateFormatted]]<br /><br />Please log in to view the vetting records<br /><br />Please do not reply to this email as it is generated automatically by the HERMIS system.<br /><br />HERMIS Support Team<br /><br />If you need support, please email [[FeedBackMailTo]].</p>',
	'[{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[SecurityClearanceName]]":"Security clearance name"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'MedicalVetting', 
	'MedicalClearanceReview', 
	'Sent to notifiy a user that one or more medical reviews are required', 
	'Medical Clearance Review', 
	'<p>Dear [[ToFullName]],<br /><br />You are required to undergo a [[MedicalClearanceTypeName]] medical check.<br />Please find attached the relevant instructions.<br />Please arrange you check directly with the service provider: Medfit.<br />If you need assistance please contact [Palladium email].<br />Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br /><br />[[Comments]]<br /><br />Please do not reply to this email as it is generated automatically.<br /><br />All the best,<br /><br />The Palladium HR Team</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[MedicalClearanceTypeName]]":"Medical Clearance Types"},{"[[Comments]]":"Sender comments"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'BPPSDiscretionaryClearanceReview', 
	'Sent to notifiy a user that the documents attached to the email for a discretionary review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Discretionary Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an discretionary review of your current security clearance.&nbsp;&nbsp;This discretionary review is required by [[DiscretionaryReviewDateFormatted]].&nbsp;&nbsp;The mandatory review date for this security clearance is [[MandatoryReviewDateFormatted]].Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[DiscretionaryReviewDateFormatted]]":"Discretionary review date"},{"[[FromFullName]]":"Name of the email sender"},{"[[MandatoryReviewDateFormatted]]":"Mandatory review date"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO

EXEC core.EmailTemplateAddUpdate 
	'Vetting', 
	'BPPSInitialClearanceReview', 
	'Sent to notifiy a user that the documents attached to the email for an initial review for a BPPS security clearance require completion', 
	'BPPS Security Clearance Initial Review', 
	'<p>Dear [[ToFullName]],<br /><br />The information requested on the forms attached to this email is part of an initial security clearance review.&nbsp;&nbsp;Please complete the attached forms and return them to [[ReturnAddress]] no later than [[AttachmentDueDateFormatted]].<br />[[Comments]]<br /><br />Many thanks<br />[[FromFullName]]</p>',
	'[{"[[AttachmentDueDateFormatted]]":"Due date of the attached documents"},{"[[Comments]]":"Sender comments"},{"[[FromFullName]]":"Name of the email sender"},{"[[ReturnAddress]]":"Return address for the attached documents"},{"[[ToFullName]]":"Name of the email recipient"}]'
GO
--End table core.EmailTemplate

--Begin table core.EntityType
EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'MedicalVetting', 
	@EntityTypeName = 'Medical Clearance', 
	@EntityTypeNamePlural = 'Medical Clearances',
	@SchemaName = 'person', 
	@TableName = 'PersonMedicalClearance', 
	@PrimaryKeyFieldName = 'PersonMedicalClearanceID'

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'PersonGroup', 
	@EntityTypeName = 'User Group', 
	@EntityTypeNamePlural = 'User Groups',
	@SchemaName = 'person', 
	@TableName = 'PersonGroup', 
	@PrimaryKeyFieldName = 'PersonGroupID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Vetting', 
	@EntityTypeName = 'Security Clearance', 
	@EntityTypeNamePlural = 'Security Clearances',
	@SchemaName = 'person', 
	@TableName = 'PersonSecurityClearance', 
	@PrimaryKeyFieldName = 'PersonSecurityClearanceID'
GO
--End table core.EntityType

--Begin table core.MenuItem
EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'ConsultantList',
	@NewMenuItemCode = 'MedicalVettingList',
	@NewMenuItemLink = '/person/medicalvettinglist',
	@NewMenuItemText = 'Medical Clearances',
	@ParentMenuItemCode = 'HumanResources',
	@PermissionableLineageList = 'Person.MedicalVettingList'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'PersonList',
	@NewMenuItemCode = 'PersonGroup',
	@NewMenuItemLink = '/persongroup/list',
	@NewMenuItemText = 'User Groups',
	@ParentMenuItemCode = 'Admin',
	@PermissionableLineageList = 'persongroup.list'
GO

EXEC core.MenuItemAddUpdate
	@AfterMenuItemCode = 'VettingList',
	@NewMenuItemCode = 'VettingList',
	@NewMenuItemLink = '/person/vettinglist',
	@NewMenuItemText = 'Security Clearances',
	@ParentMenuItemCode = 'HumanResources',
	@PermissionableLineageList = 'Person.VettingList'
GO

EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
EXEC core.UpdateParentPermissionableLineageByMenuItemCode 'HumanResources'
GO
--End table core.MenuItem

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'ProfileReviewEmailDayCount', 'The number of days prior to the next review date at which the profile review email is triggered', '30'
EXEC core.SystemSetupAddUpdate 'OperationalTrainingExpirationEmailDayCount', 'The number of days prior to the expiration date at which the operational training expiration email is triggered', '30'
EXEC core.SystemSetupAddUpdate 'SecurityClearanceExpirationEmailDayCount', 'The number of days prior to the expiration date at which the security clearance expiration email is triggered', '30'
GO
--End table core.SystemSetup

--Begin table dropdown.ApprovalAuthorityLevel
TRUNCATE TABLE dropdown.ApprovalAuthorityLevel
GO

EXEC utility.InsertIdentityValue 'dropdown.ApprovalAuthorityLevel', 'ApprovalAuthorityLevelID', 0
GO

INSERT INTO dropdown.ApprovalAuthorityLevel 
	(ApprovalAuthorityLevelName, ApprovalAuthorityLevelCode, DisplayOrder)
VALUES
	('Creator', 'Creator', 1),
	('Reviewer', 'Reviewer', 2),
	('Approver', 'Approver', 3)
GO
--End table dropdown.ApprovalAuthorityLevel

--Begin table dropdown.MedicalClearanceProcessStep
TRUNCATE TABLE dropdown.MedicalClearanceProcessStep
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceProcessStep', 'MedicalClearanceProcessStepID', 0
GO

INSERT INTO dropdown.MedicalClearanceProcessStep 
	(MedicalClearanceProcessStepCode, MedicalClearanceProcessStepName, DisplayOrder) 
VALUES
	('Complete', 'Complete', 1),
	('InProcess', 'In Process', 2),
	('InPreparation', 'In Preparation', 3)
GO
--End table dropdown.MedicalClearanceProcessStep

--Begin table dropdown.MedicalClearanceStatus
TRUNCATE TABLE dropdown.MedicalClearanceStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceStatus', 'MedicalClearanceStatusID', 0
GO

INSERT INTO dropdown.MedicalClearanceStatus 
	(MedicalClearanceStatusCode, MedicalClearanceStatusName, DisplayOrder) 
VALUES
	('Granted', 'Granted', 2),
	('NotGranted', 'Not Granted', 3)
GO
--End table dropdown.MedicalClearanceStatus

--Begin table dropdown.MedicalClearanceType
TRUNCATE TABLE dropdown.MedicalClearanceType
GO

EXEC utility.InsertIdentityValue 'dropdown.MedicalClearanceType', 'MedicalClearanceTypeID', 0
GO

INSERT INTO dropdown.MedicalClearanceType 
	(MedicalClearanceTypeCode, MedicalClearanceTypeName, DisplayOrder) 
VALUES
	('Physical', 'Physical', 1),
	('Psychological', 'Psychological', 2)
GO
--End table dropdown.MedicalClearanceType

--Begin table dropdown.SecurityClearance
TRUNCATE TABLE dropdown.SecurityClearance
GO

EXEC utility.InsertIdentityValue 'dropdown.SecurityClearance', 'SecurityClearanceID', 0
GO

UPDATE SC
SET 
	SC.SecurityClearanceCode = 'NONE',
	SC.SecurityClearanceName = 'No Clearance',
	SC.DisplayOrder = 1
FROM dropdown.SecurityClearance SC
WHERE SC.SecurityClearanceID = 0
GO

INSERT INTO dropdown.SecurityClearance 
	(SecurityClearanceCode, SecurityClearanceName, DisplayOrder) 
VALUES
	('BPSS', 'BPSS', 2),
	('DV', 'DV + EU Equivalent Certificate', 9),
	('DV', 'DV + NATO and EU Equivalent Certificates', 10),
	('DV', 'DV + NATO Equivalent Certificate', 8),
	('DV', 'DV', 4),
	('SC', 'SC + EU Equivalent Certificate', 6),
	('SC', 'SC + NATO and EU Equivalent Certificates', 7),
	('SC', 'SC + NATO Equivalent Certificate', 5),
	('SC', 'SC', 3)
GO
--End table dropdown.SecurityClearance

--Begin table dropdown.SecurityClearanceProcessStep
TRUNCATE TABLE dropdown.SecurityClearanceProcessStep
GO

EXEC utility.InsertIdentityValue 'dropdown.SecurityClearanceProcessStep', 'SecurityClearanceProcessStepID', 0
GO

INSERT INTO dropdown.SecurityClearanceProcessStep 
	(SecurityClearanceProcessStepCode, SecurityClearanceProcessStepName, DisplayOrder) 
VALUES
	('COMPLETE', 'Discretionary Review Process Complete - Granted', 7),
	('COMPLETE', 'Discretionary Review Process Complete - Ineligible', 8),
	('COMPLETE', 'Process Complete - Granted', 4),
	('COMPLETE', 'Process Complete - Not Granted', 5),
	('NONE', 'Application Not Submitted', 2),
	('PREP', 'Application in Preperation', 1),
	('PROCESS', 'Application in Process', 3),
	('REVIEW', 'Under Review', 6)
GO
--End table dropdown.SecurityClearanceProcessStep

--Begin table permissionable.Permissionable
EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the medical clearance list', 
	@METHODNAME='MedicalVettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.MedicalVettingList', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Send bulk emails from the medical clearance list', 
	@METHODNAME='MedicalVettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.MedicalVettingList.BullkEmail', 
	@PERMISSIONCODE='BullkEmail';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='View the security clearance list', 
	@METHODNAME='VettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.VettingList', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='Person', 
	@DESCRIPTION='Send bulk emails from the security clearance list', 
	@METHODNAME='VettingList', 
	@PERMISSIONABLEGROUPCODE='General', 
	@PERMISSIONABLELINEAGE='Person.VettingList.BullkEmail', 
	@PERMISSIONCODE='BullkEmail';
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonGroup', 
	@DESCRIPTION='Add / edit a user group', 
	@METHODNAME='AddUpdate', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonGroup.AddUpdate', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonGroup', 
	@DESCRIPTION='View the list of user groups',
	@METHODNAME='List', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonGroup.List', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.SavePermissionable 
	@CONTROLLERNAME='PersonGroup', 
	@DESCRIPTION='View a user group', 
	@METHODNAME='View', 
	@PERMISSIONABLEGROUPCODE='Administration', 
	@PERMISSIONABLELINEAGE='PersonGroup.View', 
	@PERMISSIONCODE=NULL;
GO

EXEC permissionable.UpdateSuperAdministratorPersonPermissionables
GO
--End table permissionable.Permissionable

--End file Build File - 04 - Data.sql

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin build tracking
INSERT INTO syslog.BuildLog (BuildKey) VALUES ('Build - 1.8 - 2019.02.07 17.40.23')
GO
--End build tracking

