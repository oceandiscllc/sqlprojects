--Begin table core.PendingEmail
DECLARE @TableName VARCHAR(250) = 'core.PendingEmail'

EXEC utility.DropObject @TableName

CREATE TABLE core.PendingEmail
	(
	PendingEmailID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EmailTemplateCode VARCHAR(50),
	EntityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PendingEmailID'
GO
--End table core.PendingEmail

--Begin table core.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'core.EmailTemplate'

EXEC utility.AddColumn @TableName, 'EmailTemplateDescription', 'VARCHAR(500)'
GO
--End table core.EmailTemplate

--Begin table core.WorldClock
DECLARE @TableName VARCHAR(250) = 'core.WorldClock'

EXEC utility.DropObject @TableName

CREATE TABLE core.WorldClock
	(
	WorldClockID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	TimeZoneName VARCHAR(250),
	WorldClockName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'WorldClockID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorldClock', 'EntityTypeCode,EntityID,DisplayOrder,WorldClockName,WorldClockID'
GO
--End table core.WorldClock

--Begin table dropdown.ApprovalAuthorityLevel
DECLARE @TableName VARCHAR(250) = 'dropdown.ApprovalAuthorityLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ApprovalAuthorityLevel
	(
	ApprovalAuthorityLevelID INT IDENTITY(0,1) NOT NULL,
	ApprovalAuthorityLevelCode VARCHAR(50),
	ApprovalAuthorityLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ApprovalAuthorityLevelID'
GO
--End table dropdown.ApprovalAuthorityLevel

--Begin table dropdown.MedicalClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MedicalClearanceProcessStep
	(
	MedicalClearanceProcessStepID INT IDENTITY(0,1) NOT NULL,
	MedicalClearanceProcessStepCode VARCHAR(50) NULL,
	MedicalClearanceProcessStepName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MedicalClearanceProcessStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MedicalClearanceProcessStep', 'DisplayOrder,MedicalClearanceProcessStepName', 'MedicalClearanceProcessStepID'
GO
--End table dropdown.MedicalClearanceProcessStep

--Begin table dropdown.MedicalClearanceStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MedicalClearanceStatus
	(
	MedicalClearanceStatusID INT IDENTITY(0,1) NOT NULL,
	MedicalClearanceStatusCode VARCHAR(50) NULL,
	MedicalClearanceStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MedicalClearanceStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MedicalClearanceStatus', 'DisplayOrder,MedicalClearanceStatusName', 'MedicalClearanceStatusID'
GO
--End table dropdown.MedicalClearanceStatus

--Begin table dropdown.MedicalClearanceType
DECLARE @TableName VARCHAR(250) = 'dropdown.MedicalClearanceType'

EXEC utility.AddColumn @TableName, 'MedicalClearanceTypeCode', 'VARCHAR(50)'
GO
--End table dropdown.MedicalClearanceType

--Begin table dropdown.SecurityClearance
DECLARE @TableName VARCHAR(250) = 'dropdown.SecurityClearance'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.SecurityClearanceLevel'

CREATE TABLE dropdown.SecurityClearance
	(
	SecurityClearanceID INT IDENTITY(0,1) NOT NULL,
	SecurityClearanceCode VARCHAR(50) NULL,
	SecurityClearanceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SecurityClearanceID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SecurityClearance', 'DisplayOrder,SecurityClearanceName', 'SecurityClearanceID'
GO
--End table dropdown.SecurityClearance

--Begin table dropdown.SecurityClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'dropdown.SecurityClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SecurityClearanceProcessStep
	(
	SecurityClearanceProcessStepID INT IDENTITY(0,1) NOT NULL,
	SecurityClearanceProcessStepCode VARCHAR(50) NULL,
	SecurityClearanceProcessStepName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'SecurityClearanceProcessStepID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_SecurityClearanceProcessStep', 'DisplayOrder,SecurityClearanceProcessStepName', 'SecurityClearanceProcessStepID'
GO
--End table dropdown.SecurityClearanceProcessStep

--Begin table person.PersonGroup
DECLARE @TableName VARCHAR(250) = 'person.PersonGroup'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonGroup
	(
	PersonGroupID INT IDENTITY(1,1) NOT NULL,
	PersonGroupName VARCHAR(250),
	PersonGroupDescription VARCHAR(MAX),
	ApprovalAuthorityLevelID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ApprovalAuthorityLevelID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonGroupID'
GO
--End table person.PersonGroup

--Begin table person.PersonGroupPerson
DECLARE @TableName VARCHAR(250) = 'person.PersonGroupPerson'

EXEC utility.DropObject @TableName
	
CREATE TABLE person.PersonGroupPerson
	(
	PersonGroupPersonID INT IDENTITY(1,1) NOT NULL,
	PersonGroupID INT,
	PersonID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'PersonGroupID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonGroupPerson', 'PersonGroupID,PersonID'
GO
--End table person.PersonGroupPerson

--Begin table person.PersonMedicalClearance
DECLARE @TableName VARCHAR(250) = 'person.PersonMedicalClearance'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonMedicalClearance
	(
	PersonMedicalClearanceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	MedicalClearanceTypeID INT,
	MedicalClearanceStatusID INT,
	ClearanceGrantedDate DATE,
	MandatoryReviewDate DATE,
	VettingAuthorityName VARCHAR(250),
	Limitations VARCHAR(250),
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MedicalClearanceTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonMedicalClearanceID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonMedicalClearance', 'PersonID,MedicalClearanceTypeID'
GO

EXEC utility.DropObject 'person.TR_PersonMedicalClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonMedicalClearance table
-- =========================================================================
CREATE TRIGGER person.TR_PersonMedicalClearance ON person.PersonMedicalClearance AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PMC
	SET PMC.IsActive = 0
	FROM person.PersonMedicalClearance PMC
		JOIN INSERTED I ON I.PersonID = PMC.PersonID
			AND I.PersonMedicalClearanceID <> PMC.PersonMedicalClearanceID
			AND I.MedicalClearanceTypeID = PMC.MedicalClearanceTypeID

GO
--End table person.PersonMedicalClearance

--Begin table person.PersonMedicalClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'person.PersonMedicalClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonMedicalClearanceProcessStep
	(
	PersonMedicalClearanceProcessStepID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	MedicalClearanceTypeID INT,
	MedicalClearanceProcessStepID INT,
	StatusDate DATE,
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MedicalClearanceTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MedicalClearanceProcessStepID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonMedicalClearanceProcessStepID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonMedicalClearanceProcessStep', 'PersonID,MedicalClearanceTypeID,MedicalClearanceProcessStepID'
GO

EXEC utility.DropObject 'person.TR_PersonMedicalClearanceProcessStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonMedicalClearanceProcessStep table
-- ====================================================================================
CREATE TRIGGER person.TR_PersonMedicalClearanceProcessStep ON person.PersonMedicalClearanceProcessStep AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PMCPS
	SET PMCPS.IsActive = 0
	FROM person.PersonMedicalClearanceProcessStep PMCPS
		JOIN INSERTED I ON I.PersonID = PMCPS.PersonID
			AND I.PersonMedicalClearanceProcessStepID <> PMCPS.PersonMedicalClearanceProcessStepID
			AND I.MedicalClearanceTypeID = PMCPS.MedicalClearanceTypeID

GO
--End table person.PersonMedicalClearanceProcessStep

--Begin table person.PersonSecurityClearance
DECLARE @TableName VARCHAR(250) = 'person.PersonSecurityClearance'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonSecurityClearance
	(
	PersonSecurityClearanceID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	SecurityClearanceID INT,
	ClearanceGrantedDate DATE,
	DiscretionaryReviewDate DATE,
	MandatoryReviewDate DATE,
	VettingAuthorityName VARCHAR(250),
	SponsorName VARCHAR(250),
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonSecurityClearanceID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonSecurityClearance', 'PersonID,SecurityClearanceID'
GO

EXEC utility.DropObject 'person.TR_PersonSecurityClearance'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonSecurityClearance table
-- =========================================================================
CREATE TRIGGER person.TR_PersonSecurityClearance ON person.PersonSecurityClearance AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PSC
	SET PSC.IsActive = 0
	FROM person.PersonSecurityClearance PSC
		JOIN INSERTED I ON I.PersonID = PSC.PersonID
			AND I.PersonSecurityClearanceID <> PSC.PersonSecurityClearanceID

GO
--End table person.PersonSecurityClearance

--Begin table person.PersonSecurityClearanceProcessStep
DECLARE @TableName VARCHAR(250) = 'person.PersonSecurityClearanceProcessStep'

EXEC utility.DropObject @TableName

CREATE TABLE person.PersonSecurityClearanceProcessStep
	(
	PersonSecurityClearanceProcessStepID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	SecurityClearanceID INT,
	SecurityClearanceProcessStepID INT,
	StatusDate DATE,
	Notes VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'SecurityClearanceProcessStepID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonSecurityClearanceProcessStepID'
EXEC utility.SetIndexClustered @TableName, 'IX_PersonSecurityClearanceProcessStep', 'PersonID,SecurityClearanceID,SecurityClearanceProcessStepID'
GO

EXEC utility.DropObject 'person.TR_PersonSecurityClearanceProcessStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.17
-- Description:	A trigger to update the person.PersonSecurityClearanceProcessStep table
-- ====================================================================================
CREATE TRIGGER person.TR_PersonSecurityClearanceProcessStep ON person.PersonSecurityClearanceProcessStep AFTER INSERT
AS
	SET ARITHABORT ON;

	UPDATE PSCPS
	SET PSCPS.IsActive = 0
	FROM person.PersonSecurityClearanceProcessStep PSCPS
		JOIN INSERTED I ON I.PersonID = PSCPS.PersonID
			AND I.PersonSecurityClearanceProcessStepID <> PSCPS.PersonSecurityClearanceProcessStepID

GO
--End table person.PersonSecurityClearanceProcessStep

--Begin table workflow.EntityWorkflowStepGroupPerson
EXEC Utility.AddColumn 'workflow.EntityWorkflowStepGroupPerson', 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.AddColumn 'workflow.EntityWorkflowStepGroupPerson', 'PersonGroupID', 'INT', '0'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.WorkflowStepGroupPerson
EXEC Utility.AddColumn 'workflow.WorkflowStepGroupPerson', 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.AddColumn 'workflow.WorkflowStepGroupPerson', 'PersonGroupID', 'INT', '0'
GO
--End table workflow.WorkflowStepGroupPerson