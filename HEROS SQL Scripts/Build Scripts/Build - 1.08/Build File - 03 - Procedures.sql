--Begin procedure core.CreatePendingEmail
EXEC utility.DropObject 'core.CreatePendingEmail'
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure populate the core.PendingEmail table
-- ====================================================================
CREATE PROCEDURE core.CreatePendingEmail

@TargetDate DATE = NULL

AS
BEGIN
	SET NOCOUNT ON;

	SET @TargetDate = ISNULL(@TargetDate, getDate())

	INSERT INTO core.PendingEmail
		(EntityTypeCode, EmailTemplateCode, EntityID)
	SELECT
		'Person' AS EntityTypeCode,
		'ProfileReview' AS EmailTemplateCode,
		P.PersonID AS EntityID
	FROM person.Person P
	WHERE DATEDIFF(d, @TargetDate, P.NextReviewDate) = CAST(core.GetSystemSetupValueBySystemSetupKey('ProfileReviewEmailDayCount', '30') AS INT)

	UNION

	SELECT
		'Person' AS EntityTypeCode,
		'OperationalTrainingExpiration' AS EmailTemplateCode,
		POT.PersonOperationalTrainingID AS EntityID
	FROM person.PersonOperationalTraining POT
	WHERE DATEDIFF(d, @TargetDate, POT.ExpiryDate) = CAST(core.GetSystemSetupValueBySystemSetupKey('OperationalTrainingExpirationEmailDayCount', '30') AS INT)

	UNION

	SELECT
		'Vetting' AS EntityTypeCode,
		'SecurityClearanceExpiration' AS EmailTemplateCode,
		PSC.PersonSecurityClearanceID AS EntityID
	FROM person.PersonSecurityClearance PSC
	WHERE DATEDIFF(d, @TargetDate, PSC.MandatoryReviewDate) = CAST(core.GetSystemSetupValueBySystemSetupKey('OperationalTrainingExpirationEmailDayCount', '30') AS INT)

END
GO
--End procedure core.CreatePendingEmail

--Begin procedure core.DeletePendingEmailByPendingEmailID
EXEC utility.DropObject 'core.DeletePendingEmailByPendingEmailID'
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure delete data from the core.PendingEmail table
-- ============================================================================
CREATE PROCEDURE core.DeletePendingEmailByPendingEmailID

@PendingEmailID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PE
	FROM core.PendingEmail PE
	WHERE PE.PendingEmailID = @PendingEmailID

END
GO
--End procedure core.DeletePendingEmailByPendingEmailID

--Begin procedure core.EmailTemplateAddUpdate
EXEC utility.DropObject 'core.EmailTemplateAddUpdate'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure to get add / updade data in the core.EmailTemplate table
-- ========================================================================================
CREATE PROCEDURE core.EmailTemplateAddUpdate

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50),
@EmailTemplateDescription VARCHAR(500),
@EmailSubject VARCHAR(500),
@EmailText VARCHAR(MAX),
@EmailTemplatFieldJSON VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cJSONData VARCHAR(MAX)
	DECLARE @cPlaceHolderDescription VARCHAR(100)
	DECLARE @cPlaceHolderText VARCHAR(50)

	IF EXISTS (SELECT 1 FROM core.EmailTemplate ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.EmailTemplateCode = @EmailTemplateCode)
		BEGIN

		UPDATE ET
		SET
			ET.EmailTemplateDescription = @EmailTemplateDescription,
			ET.EmailSubject = @EmailSubject,
			ET.EmailText =  @EmailText
		FROM core.EmailTemplate ET
		WHERE ET.EntityTypeCode = @EntityTypeCode
			AND ET.EmailTemplateCode = @EmailTemplateCode

		END
 	ELSE
		BEGIN

		INSERT INTO core.EmailTemplate
			(EntityTypeCode,EmailTemplateCode,EmailTemplateDescription,EmailSubject,EmailText)
		VALUES
			(@EntityTypeCode, @EmailTemplateCode, @EmailTemplateDescription, @EmailSubject, @EmailText)

		END
	--ENDIF

	IF @EmailTemplatFieldJSON IS NOT NULL
		BEGIN

		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT [Value] 
			FROM OPENJSON(@EmailTemplatFieldJSON)			
			ORDER BY 1
					
		OPEN oCursor
		FETCH oCursor INTO @cJSONData
		WHILE @@fetch_status = 0
			BEGIN

			INSERT INTO core.EmailTemplateField
				(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
			SELECT
				@EntityTypeCode,
				JD.[key],
				JD.[value]
			FROM OPENJSON(@cJSONData)	JD
			WHERE NOT EXISTS
				(
				SELECT 1 
				FROM core.EmailTemplateField ETF
				WHERE ETF.EntityTypeCode = @EntityTypeCode
					AND ETF.PlaceHolderText COLLATE DATABASE_DEFAULT = JD.[key] COLLATE DATABASE_DEFAULT
					AND ETF.PlaceHolderDescription COLLATE DATABASE_DEFAULT = JD.[value] COLLATE DATABASE_DEFAULT
				)

			FETCH oCursor INTO @cJSONData
					
			END
		--END WHILE
						
		CLOSE oCursor
		DEALLOCATE oCursor	

		END
	--ENDIF

END
GO
--End procedure core.EmailTemplateAddUpdate

--Begin procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode
EXEC utility.DropObject 'core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode'
GO

-- =============================================================================
-- Author:		Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the core.EmailTemplate table
-- =============================================================================
CREATE PROCEDURE core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

@EntityTypeCode VARCHAR(50),
@EmailTemplateCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	--EmailTemplateData
	SELECT 
		ET.EmailSubject,
		ET.EmailText
	FROM core.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.EmailTemplateCode = @EmailTemplateCode

	--EmailTemplateFieldData
	SELECT 
		ETF.PlaceHolderText
	FROM core.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode IN ('Any', @EntityTypeCode)
	ORDER BY ETF.PlaceHolderText

	--EmailTemplateGlobalData
	SELECT
		core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS SiteURL,
		core.GetSystemSetupValueBySystemSetupKey('SystemName', '') AS SystemName

END
GO
--End procedure core.GetEmailTemplateDataByEntityTypeCodeAndEmailTemplateCode

--Begin procedure core.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'core.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to data from the core.EmailTemplate table
-- =========================================================================
CREATE PROCEDURE core.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailSubject,
		ET.EmailTemplateCode,
		ET.EmailTemplateDescription,
		ET.EmailTemplateID, 
		ET.EmailText,
		ET.EntityTypeCode, 
		core.GetEntityTypeNameByEntityTypeCode(ET.EntityTypeCode) AS EntityTypeName
	FROM core.EmailTemplate ET
	WHERE ET.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM core.EmailTemplateField ETF
		JOIN core.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure core.GetEmailTemplateByEmailTemplateID

--Begin procedure core.GetPendingEmail
EXEC utility.DropObject 'core.GetPendingEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure get data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		core.GetSystemSetupValueBySystemSetupKey('FeedBackMailTo', '') AS FeedBackEmailAddress,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS NoReplyEmailAddress,
		PE.EmailTemplateCode,
		PE.EntityTypeCode + PE.EmailTemplateCode AS PendingEmailCode,
		PE.EntityTypeCode
	FROM core.PendingEmail PE
	ORDER BY PE.EntityTypeCode + PE.EmailTemplateCode

END
GO
--End procedure core.GetPendingEmail

--Begin procedure core.GetPendingPersonOperationalTrainingExpirationEmail
EXEC utility.DropObject 'core.GetPendingPersonOperationalTrainingExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingPersonOperationalTrainingExpirationEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		OTC.OperationalTrainingCourseName,
		P.EmailAddress,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted
	FROM person.PersonOperationalTraining POT
		JOIN person.Person P ON P.PersonID = POT.PersonID
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'OperationalTrainingExpiration'
			AND PE.EntityID = POT.PersonOperationalTrainingID
	ORDER BY 1

END
GO
--End procedure core.GetPendingPersonOperationalTrainingExpirationEmail

--Begin procedure core.GetPendingPersonProfileReviewEmail
EXEC utility.DropObject 'core.GetPendingPersonProfileReviewEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingPersonProfileReviewEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted,
		'<a href="' + DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') + '">DeployAdviser</a>' AS DeployAdviserLink,
		DeployAdviserCloud.core.GetSystemSetupValueBySystemSetupKey('SiteURL', '') AS DeployAdviserURL
	FROM person.Person P
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'ProfileReview'
			AND PE.EntityID = P.PersonID
	ORDER BY 1

END
GO
--End procedure core.GetPendingPersonProfileReviewEmail

--Begin procedure core.GetPendingPersonSecurityClearanceExpirationEmail
EXEC utility.DropObject 'core.GetPendingPersonSecurityClearanceExpirationEmail'
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.13
-- Description:	A stored procedure gat data from the core.PendingEmail table
-- =========================================================================
CREATE PROCEDURE core.GetPendingPersonSecurityClearanceExpirationEmail

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PE.PendingEmailID,
		P.EmailAddress,
		core.FormatDate(PSC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		SC.SecurityClearanceName,
		person.FormatPersonNameByPersonID(P.PersonID, 'Firstlast') AS PersonNameFormatted
	FROM person.PersonSecurityClearance PSC
		JOIN person.Person P ON P.PersonID = PSC.PersonID
		JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID
		JOIN core.PendingEmail PE ON PE.EntityTypeCode = 'Person'
			AND PE.EmailTemplateCode = 'SecurityClearanceExpiration'
			AND PE.EntityID = PSC.PersonSecurityClearanceID
	ORDER BY 1

END
GO
--End procedure core.GetPendingPersonSecurityClearanceExpirationEmail

--Begin procedure document.GetDocumentsByDocumentEntityCodeList
EXEC Utility.DropObject 'document.GetDocumentsByDocumentEntityCodeList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data from the document.Document table
-- ===============================================================================
CREATE PROCEDURE document.GetDocumentsByDocumentEntityCodeList

@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentType + '/' + D.ContentSubtype AS MimeType,
		D.DocumentData,
		D.DocumentID,
		D.DocumentTitle
	FROM document.Document D
	WHERE EXISTS
		(
		SELECT 1
		FROM document.DocumentEntity DE
			JOIN core.ListToTable(@DocumentEntityCodeList, ',') LTT ON LTT.ListItem = DE.DocumentEntityCode
				AND DE.DocumentID = D.DocumentID
		)
	ORDER BY D.DocumentTitle, D.DocumentID

END
GO
--End procedure document.GetDocumentsByDocumentEntityCodeList

--Begin procedure dropdown.GetApprovalAuthorityLevelData
EXEC Utility.DropObject 'dropdown.GetApprovalAuthorityLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.12
-- Description:	A stored procedure to return data from the dropdown.ApprovalAuthorityLevel table
-- =============================================================================================

EXEC Utility.DropObject 'dropdown.GetApprovalAuthorityLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dropdown.GetApprovalAuthorityLevelData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ApprovalAuthorityLevelID,
		T.ApprovalAuthorityLevelCode,
		T.ApprovalAuthorityLevelName
	FROM dropdown.ApprovalAuthorityLevel T
	WHERE (T.ApprovalAuthorityLevelID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ApprovalAuthorityLevelName, T.ApprovalAuthorityLevelID

END
GO
--End table dropdown.ApprovalAuthorityLevel

--Begin procedure dropdown.GetMedicalClearanceStatusData
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceStatusData'
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceStatusLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.24
-- Description:	A stored procedure to return data from the dropdown.MedicalClearanceStatus table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetMedicalClearanceStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MedicalClearanceStatusID, 
		T.MedicalClearanceStatusCode,
		T.MedicalClearanceStatusName
	FROM dropdown.MedicalClearanceStatus T
	WHERE (T.MedicalClearanceStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MedicalClearanceStatusName, T.MedicalClearanceStatusID

END
GO
--End procedure dropdown.GetMedicalClearanceStatusData

--Begin procedure dropdown.GetMedicalClearanceProcessStepData
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceProcessStepData'
EXEC Utility.DropObject 'dropdown.GetMedicalClearanceProcessStepLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.24
-- Description:	A stored procedure to return data from the dropdown.MedicalClearanceProcessStep table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetMedicalClearanceProcessStepData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MedicalClearanceProcessStepID, 
		T.MedicalClearanceProcessStepCode,
		T.MedicalClearanceProcessStepName
	FROM dropdown.MedicalClearanceProcessStep T
	WHERE (T.MedicalClearanceProcessStepID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MedicalClearanceProcessStepName, T.MedicalClearanceProcessStepID

END
GO
--End procedure dropdown.GetMedicalClearanceProcessStepData

--Begin procedure dropdown.GetSecurityClearanceData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceData'
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the dropdown.SecurityClearance table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SecurityClearanceID, 
		T.SecurityClearanceCode,
		T.SecurityClearanceName
	FROM dropdown.SecurityClearance T
	WHERE (T.SecurityClearanceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SecurityClearanceName, T.SecurityClearanceID

END
GO
--End procedure dropdown.GetSecurityClearanceData

--Begin procedure dropdown.GetSecurityClearanceProcessStepData
EXEC Utility.DropObject 'dropdown.GetSecurityClearanceProcessStepData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the dropdown.SecurityClearanceProcessStep table
-- ===================================================================================================
CREATE PROCEDURE dropdown.GetSecurityClearanceProcessStepData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SecurityClearanceProcessStepID, 
		T.SecurityClearanceProcessStepCode,
		T.SecurityClearanceProcessStepName
	FROM dropdown.SecurityClearanceProcessStep T
	WHERE (T.SecurityClearanceProcessStepID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SecurityClearanceProcessStepName, T.SecurityClearanceProcessStepID

END
GO
--End procedure dropdown.GetSecurityClearanceProcessStepData

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.09.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'EmailTemplate'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM core.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure eventlog.LogPersonGroupAction
EXEC utility.DropObject 'eventlog.LogPersonGroupAction'
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2019.01.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonGroupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'PersonGroup'
	
	IF @EventCode IN ('Create', 'Update')
		BEGIN

		DECLARE @cPersonGroupPerson VARCHAR(MAX) = ''
		
		SELECT @cPersonGroupPerson = COALESCE(@cPersonGroupPerson, '') + D.PersonGroupPerson
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonGroupPerson'), ELEMENTS) AS PersonGroupPerson
			FROM person.PersonGroupPerson T 
			WHERE T.PersonGroupID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PersonGroupPerson>' + ISNULL(@cPersonGroupPerson, '') + '</PersonGroupPerson>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.PersonGroup T
		WHERE T.PersonGroupID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonGroupAction

--Begin procedure person.GetMedicalVettingEmailData
EXEC Utility.DropObject 'person.GetMedicalVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.16
-- Description:	A stored procedure to return data for outgoing medical vetting e-mail
-- ==================================================================================
CREATE PROCEDURE person.GetMedicalVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@MedicalClearanceTypeIDList VARCHAR(MAX),
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonMedicalClearanceProcessStep
		(PersonID, MedicalClearanceTypeID, MedicalClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		CAST(LTT2.ListItem AS INT),
		(SELECT MCPS.MedicalClearanceProcessStepID FROM dropdown.MedicalClearanceProcessStep MCPS WHERE MCPS.MedicalClearanceProcessStepCode = 'InPreparation'),
		getDate(),
		1,
		'Medical clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT1 ON CAST(LTT1.ListItem AS INT) = P.PersonID
		CROSS APPLY core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT2

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		(SELECT 
			REPLACE(STUFF((
				SELECT ',' + MedicalClearanceTypeName
				FROM dropdown.MedicalClearanceType MCT
					JOIN core.ListToTable(@MedicalClearanceTypeIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = MCT.MedicalClearanceTypeID
				ORDER BY 1
				FOR XML PATH('')), 1, 1, ''), ',', ' and ')) AS MedicalClearanceTypeName
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL AS MedicalClearanceTypeName

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetMedicalVettingEmailData

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date:	2016.12.01
-- Description:	A stored procedure to return data from the person.Person table
-- ===========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		CCC.CountryCallingCode,
		CCC.CountryCallingCodeID,
		CCC2.CountryCallingCode AS HomePhoneCountryCallingCode,
		CCC2.CountryCallingCodeID AS HomePhoneCountryCallingCodeID,
		CCC3.CountryCallingCode AS WorkPhoneCountryCallingCode,
		CCC3.CountryCallingCodeID AS WorkPhoneCountryCallingCodeID,
		G.GenderID,
		G.GenderName,
		P.APIKey,
		P.APIKeyExpirationDateTime,
		P.BirthDate,
		core.FormatDate(P.BirthDate) AS BirthDateFormatted,
		P.CellPhone,
		P.ChestSize,
		P.Citizenship1ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship1ISOCountryCode2) AS Citizenship1CountryName,
		P.Citizenship2ISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.Citizenship2ISOCountryCode2) AS Citizenship2CountryName,
		P.CollarSize,
		P.CreateDateTime,
		core.FormatDateTime(P.CreateDateTime) AS CreateDateTimeFormatted,
		P.DAPersonID,
		P.DefaultProjectID,
		P.DFIDStaffNumber,
		P.EmailAddress,
		P.ExpertiseNotes,
		P.FCOStaffNumber,
		P.FirstName,
		P.HasAcceptedTerms,
		P.HeadSize,
		P.Height,
		P.HomePhone,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsPhoneVerified,		
		P.IsSuperAdministrator,
		P.IsUKEUNational,
		P.IsUKWorkEligible,
		P.JoinDate,
		core.FormatDate(P.JoinDate) AS JoinDateFormatted,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.LastReviewDate,
		core.FormatDate(P.LastReviewDate) AS LastReviewDateFormatted,
		P.MailAddress1,
		P.MailAddress2,
		P.MailAddress3,
		P.MailISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.MailISOCountryCode2) AS MailCountryName,
		P.MailMunicipality,
		P.MailPostalCode,
		P.MailRegion,
		P.ManagerPersonID,
		P.MiddleName,
		P.MiddleName,
		P.MobilePIN,
		P.NextReviewDate,
		core.FormatDate(P.NextReviewDate) AS NextReviewDateFormatted,
		P.NickName,		
		P.Organization,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		P.PlaceOfBirthISOCountryCode2,
		dropdown.GetCountryNameByISOCountryCode(P.PlaceOfBirthISOCountryCode2) AS PlaceOfBirthCountryName,
		P.PlaceOfBirthMunicipality,
		P.PreferredName,
		P.RegistrationCode,		
		P.SecondaryEmailAddress,
		P.Suffix,
		P.SummaryBiography,
		P.Title,
		P.Token,
		P.TokenCreateDateTime,
		P.UNDAC,
		P.UserName,
		P.WorkPhone,
		person.FormatPersonNameByPersonID(P.ManagerPersonID, 'FirstLast') AS ManagerPersonNameFormatted,
		PF.PoliceForceID,
		PF.PoliceForceName,
		PR.PoliceRankID,
		PR.PoliceRankName,
		R.RoleID,
		R.RoleName,
		RO.ReviewOutcomeID,
		RO.ReviewOutcomeName,
		ISNULL((SELECT TOP 1 SC.SecurityClearanceName + ' - ' + core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC JOIN dropdown.SecurityClearance SC ON SC.SecurityClearanceID = PSC.SecurityClearanceID AND PSC.IsActive = 1 AND PSC.PersonID = P.PersonID), 'None') AS Vetting
	FROM person.Person P
		JOIN dropdown.CountryCallingCode CCC ON CCC.CountryCallingCodeID = P.CountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = P.HomePhoneCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = P.WorkPhoneCountryCallingCodeID
		JOIN dropdown.Gender G ON G.GenderID = P.GenderID
		JOIN dropdown.PoliceForce PF ON PF.PoliceForceID = P.PoliceForceID
		JOIN dropdown.PoliceRank PR ON PR.PoliceRankID = P.PoliceRankID
		JOIN dropdown.ReviewOutcome RO ON RO.ReviewOutcomeID = P.ReviewOutcomeID
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
	WHERE P.PersonID = @PersonID

	--PersonCorrespondence
	SELECT
		newID() AS PersonCorrespondenceGUID,
		CS.CorrespondenceStatusID,
		CS.CorrespondenceStatusName,
		CT.CorrespondenceCategoryID,
		CT.CorrespondenceCategoryName,
		PC.CorrespondenceDate,
		core.FormatDate(PC.CorrespondenceDate) AS CorrespondenceDateFormatted,
		PC.CorrespondenceDescription,
		REPLACE(PC.CorrespondenceDescription, CHAR(13) + CHAR(10), '<br />') AS CorrespondenceDescriptionFormatted,
		PC.PersonCorrespondenceID,
		PC.CreatedByPersonID,
		person.FormatPersonNameByPersonID(PC.CreatedByPersonID, 'LastFirstTitle') AS CreatedByPersonNameFormatted,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'PersonCorrespondence'
				AND DE.EntityID = PC.PersonCorrespondenceID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonCorrespondence PC
		JOIN dropdown.CorrespondenceCategory CT ON CT.CorrespondenceCategoryID = PC.CorrespondenceCategoryID
		JOIN dropdown.CorrespondenceStatus CS ON CS.CorrespondenceStatusID = PC.CorrespondenceStatusID
			AND PC.PersonID = @PersonID
	ORDER BY PC.CorrespondenceDate DESC, PC.PersonCorrespondenceID

	--PersonDocument
	SELECT
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted,
		D.DocumentGUID,
		D.DocumentTitle,
		'<a class="m-r-10" href="javascript:deleteDocumentByDocumentGUID(''' + D.DocumentGUID + ''',''' + DE.EntityTypeCode + ''',''' + ISNULL(DE.EntityTypeSubCode, '') + ''')"><i class="fa fa-fw fa-times-circle"></i></a>' AS DeleteDocumentLink,
		'<a href="/document/getDocumentByDocumentGUID/DocumentGUID/' + D.DocumentGUID + '">' + D.DocumentTitle + '</a>' AS DownloadDocumentLink
	FROM document.DocumentEntity DE
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'Person'
			AND DE.EntityTypeSubCode = 'CV'
			AND DE.EntityID = @PersonID

	--PersonExperienceCountry
	SELECT
		C.CountryID,
		C.CountryName
	FROM person.PersonExperienceCountry PEXC
		JOIN dropdown.Country C ON C.CountryID = PEXC.CountryID
			AND PEXC.PersonID = @PersonID
	
	--PersonExpertCategory
	SELECT
		EC.ExpertCategoryID,
		EC.ExpertCategoryName
	FROM person.PersonExpertCategory PEC
		JOIN dropdown.ExpertCategory EC ON EC.ExpertCategoryID = PEC.ExpertCategoryID
			AND EC.ExpertCategoryID > 0
			AND PEC.PersonID = @PersonID

	--PersonExpertStatus
	SELECT
		ES.ExpertStatusID,
		ES.ExpertStatusName
	FROM person.PersonExpertStatus PES
		JOIN dropdown.ExpertStatus ES ON ES.ExpertStatusID = PES.ExpertStatusID
			AND ES.ExpertStatusID > 0
			AND PES.PersonID = @PersonID

	--PersonExpertType
	SELECT
		ET.ExpertTypeID,
		ET.ExpertTypeName
	FROM person.PersonExpertType PET
		JOIN dropdown.ExpertType ET ON ET.ExpertTypeID = PET.ExpertTypeID
			AND PET.PersonID = @PersonID

	--PersonFirearmsTraining
	SELECT
		newID() AS PersonFirearmsTrainingGUID,
		PFT.Course,
		PFT.CourseDate,
		core.FormatDate(PFT.CourseDate) AS CourseDateFormatted,
		PFT.ExpiryDate,
		core.FormatDate(PFT.ExpiryDate) AS ExpiryDateFormatted,
		PFT.PersonFirearmsTrainingID,
		PFT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'FirearmsTraining'
				AND DE.EntityID = PFT.PersonFirearmsTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonFirearmsTraining PFT
	WHERE PFT.PersonID = @PersonID
	ORDER BY PFT.ExpiryDate DESC, PFT.PersonFirearmsTrainingID

	--PersonLanguage
	SELECT
		newID() AS PersonLanguageGUID,
		L.ISOLanguageCode2,
		L.LanguageName,
		LP1.LanguageProficiencyName AS SpeakingLanguageProficiencyName,
		LP2.LanguageProficiencyName AS ReadingLanguageProficiencyName,
		LP3.LanguageProficiencyName AS WritingLanguageProficiencyName,
		PL.SpeakingLanguageProficiencyID,
		PL.ReadingLanguageProficiencyID,
		PL.WritingLanguageProficiencyID,
		PL.IsVerified
	FROM person.PersonLanguage PL
		JOIN dropdown.Language L ON L.ISOLanguageCode2 = PL.ISOLanguageCode2
		JOIN dropdown.LanguageProficiency LP1 ON LP1.LanguageProficiencyID = PL.SpeakingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP2 ON LP2.LanguageProficiencyID = PL.ReadingLanguageProficiencyID
		JOIN dropdown.LanguageProficiency LP3 ON LP3.LanguageProficiencyID = PL.WritingLanguageProficiencyID
			AND PL.PersonID = @PersonID
	ORDER BY L.LanguageName

	--PersonOperationalTraining
	SELECT
		newID() AS PersonOperationalTrainingGUID,
		OTC.OperationalTrainingCourseID,
		OTC.OperationalTrainingCourseName,
		POT.CourseDate,
		core.FormatDate(POT.CourseDate) AS CourseDateFormatted,
		POT.ExpiryDate,
		core.FormatDate(POT.ExpiryDate) AS ExpiryDateFormatted,
		POT.PersonOperationalTrainingID,
		POT.Provider,
		ISNULL((
		SELECT 
			D.DocumentTitle,
			DE.DocumentEntityCode
		FROM document.Document D
			JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
				AND DE.EntityTypeCode = 'Person'
				AND DE.EntityTypeSubCode = 'OperationalTraining'
				AND DE.EntityID = POT.PersonOperationalTrainingID
		FOR JSON PATH
		), '[]') AS UploadedFiles
	FROM person.PersonOperationalTraining POT
		JOIN dropdown.OperationalTrainingCourse OTC ON OTC.OperationalTrainingCourseID = POT.OperationalTrainingCourseID
			AND POT.PersonID = @PersonID
	ORDER BY POT.ExpiryDate DESC, POT.PersonOperationalTrainingID

	--PersonOrganisationalExperience
	SELECT
		OE.OrganisationalExperienceID,
		OE.OrganisationalExperienceName
	FROM person.PersonOrganisationalExperience POE
		JOIN dropdown.OrganisationalExperience OE ON OE.OrganisationalExperienceID = POE.OrganisationalExperienceID
			AND POE.PersonID = @PersonID

	--PersonPassport
	SELECT
		newID() AS PersonPassportGUID,
		PP.PassportExpirationDate,
		core.FormatDate(PP.PassportExpirationDate) AS PassportExpirationDateFormatted,
		PP.PassportIssuer,
		PP.PassportNumber,
		PP.PersonPassportID,
		PT.PassportTypeID,
		PT.PassportTypeName
	FROM person.PersonPassport PP
		JOIN dropdown.PassportType PT ON PT.PassportTypeID = PP.PassportTypeID
			AND PP.PersonID = @PersonID
	ORDER BY PP.PassportIssuer, PP.PassportExpirationDate, PP.PersonPassportID

	--PersonPasswordSecurity
	SELECT
		PPS.PasswordSecurityQuestionAnswer,
		PPS.PasswordSecurityQuestionID,
		PSQ.PasswordSecurityQuestionName
	FROM person.PersonPasswordSecurity PPS
		JOIN dropdown.PasswordSecurityQuestion PSQ ON PSQ.PasswordSecurityQuestionID = PPS.PasswordSecurityQuestionID
			AND PPS.PersonID = @PersonID

	-- PersonProject
	SELECT
		PP.ProjectID,
		P.ProjectName
	FROM person.PersonProject PP
		JOIN dropdown.Project P ON P.ProjectID = PP.ProjectID
			AND PP.PersonID = @PersonID
	ORDER BY P.ProjectName

	--PersonQualificationAcademic
	SELECT
		newID() AS PersonQualificationAcademicGUID,
		PQA.Degree,
		PQA.GraduationDate,
		core.FormatDate(PQA.GraduationDate) AS GraduationDateFormatted,
		PQA.Institution,
		PQA.PersonQualificationAcademicID,
		PQA.StartDate,
		core.FormatDate(PQA.StartDate) AS StartDateFormatted,
		PQA.SubjectArea
	FROM person.PersonQualificationAcademic PQA
	WHERE PQA.PersonID = @PersonID
	ORDER BY PQA.GraduationDate DESC, PQA.PersonQualificationAcademicID

	--PersonQualificationCertification
	SELECT
		newID() AS PersonQualificationCertificationGUID,
		PQC.CompletionDate,
		core.FormatDate(PQC.CompletionDate) AS CompletionDateFormatted,
		PQC.Course,
		PQC.ExpirationDate,
		core.FormatDate(PQC.ExpirationDate) AS ExpirationDateFormatted,
		PQC.PersonQualificationCertificationID,
		PQC.Provider
	FROM person.PersonQualificationCertification PQC
	WHERE PQC.PersonID = @PersonID
	ORDER BY PQC.ExpirationDate DESC, PQC.PersonQualificationCertificationID

	--PersonSecondaryExpertCategory
	SELECT
		SEC.ExpertCategoryID,
		SEC.ExpertCategoryName
	FROM person.PersonSecondaryExpertCategory PSEC
		JOIN dropdown.ExpertCategory SEC ON SEC.ExpertCategoryID = PSEC.ExpertCategoryID
			AND PSEC.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.GetPersonGroupByPersonGroupID
EXEC Utility.DropObject 'person.GetPersonGroupByPersonGroupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2019.01.13
-- Description:	A stored procedure to return data from the person.PersonGroup table
-- ================================================================================

CREATE PROCEDURE person.GetPersonGroupByPersonGroupID

@PersonGroupID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PersonGroup
	SELECT 
		PG.PersonGroupID,
		PG.PersonGroupName,
		PG.PersonGroupDescription,
		(
			SELECT COUNT( DISTINCT W.WorkflowID) 
			FROM workflow.WorkflowStepGroupPerson WSGP
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
				JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
				JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			WHERE W.IsActive = 1
			AND WSGP.PersonGroupID = PG.PersonGroupID
		) AS WorkflowCount,
		(SELECT COUNT( DISTINCT PersonID) FROM person.PersonGroupPerson PGP WHERE PGP.PersonGroupID = PG.PersonGroupID) AS UserCount,
		AAL.ApprovalAuthorityLevelID,
		AAL.ApprovalAuthorityLevelName,
		PG.CreateDateTime,
		core.FormatDate(PG.CreateDateTime) AS CreateDateTimeFormatted
	FROM person.PersonGroup PG
		JOIN dropdown.ApprovalAuthorityLevel AAL ON AAL.ApprovalAuthorityLevelID = PG.ApprovalAuthorityLevelID
			AND PG.PersonGroupID = @PersonGroupID

	--PersonGroupPerson
	SELECT
		PGP.PersonID,
		person.FormatPersonNameByPersonID(PGP.PersonID,'LASTFIRST') AS FullName,
		P.UserName,
		P.Organization,
		R.RoleName,
		core.FormatDate(PGP.CreateDateTime) AS CreateDateTimeFormatted
	FROM person.PersonGroupPerson PGP
	JOIN person.Person P ON P.PersonID = PGP.PersonID
	JOIN dropdown.Role R ON R.RoleID = P.RoleID
	WHERE PGP.PersonGroupID = @PersonGroupID
	ORDER BY P.LastName

	--PersonGroupWorkflows
	SELECT W.WorkflowID, P.ProjectName, P.ProjectID, W.WorkflowName, ET.EntityTypeName, WS.WorkflowStepNumber, core.FormatDate(WSGP.CreateDateTime) AS CreateDateFormatted
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		JOIN dropdown.Project P ON P.ProjectID = W.ProjectID
		JOIN core.EntityType ET ON ET.EntityTypeCode = W.EntityTypeCode
	WHERE W.IsActive = 1
	AND P.IsActive = 1
	AND WSGP.PersonGroupID = @PersonGroupID

END
GO
--End procedure person.GetPersonGroupByPersonGroupID

--Begin procedure person.GetPersonMedicalClearanceByPersonID
EXEC Utility.DropObject 'person.GetPersonMedicalClearanceByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.07
-- Description:	A stored procedure to return data from the person.PersonMedicalClearance table
-- ===========================================================================================
CREATE PROCEDURE person.GetPersonMedicalClearanceByPersonID

@PersonID INT,
@MedicalClearanceTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PMC.ClearanceGrantedDate,
		core.FormatDate(PMC.ClearanceGrantedDate) AS ClearanceGrantedDateFormatted,
		PMC.Limitations,
		PMC.MandatoryReviewDate,
		core.FormatDate(PMC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		PMC.MedicalClearanceStatusID,
		PMC.MedicalClearanceTypeID,
		PMC.Notes,
		PMC.PersonMedicalClearanceID,
		PMC.VettingAuthorityName
	FROM person.PersonMedicalClearance PMC
		JOIN dropdown.MedicalClearanceType MCT ON MCT.MedicalClearanceTypeID = PMC.MedicalClearanceTypeID
			AND PMC.PersonID = @PersonID
			AND PMC.IsActive = 1
			AND MCT.MedicalClearanceTypeCode = @MedicalClearanceTypeCode

END
GO
--End procedure person.GetPersonMedicalClearanceByPersonID

--Begin procedure person.GetPersonMedicalClearanceProcessStepByPersonID
EXEC Utility.DropObject 'person.GetPersonMedicalClearanceProcessStepByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.08
-- Description:	A stored procedure to return data from the person.PersonMedicalClearanceProcessStep table
-- ======================================================================================================
CREATE PROCEDURE person.GetPersonMedicalClearanceProcessStepByPersonID

@PersonID INT,
@MedicalClearanceTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PMCPS.MedicalClearanceProcessStepID,
		PMCPS.MedicalClearanceTypeID,
		PMCPS.Notes,
		PMCPS.PersonMedicalClearanceProcessStepID,
		PMCPS.StatusDate,
		core.FormatDate(PMCPS.StatusDate) AS StatusDateFormatted
	FROM person.PersonMedicalClearanceProcessStep PMCPS
		JOIN dropdown.MedicalClearanceType MCT ON MCT.MedicalClearanceTypeID = PMCPS.MedicalClearanceTypeID
			AND PMCPS.PersonID = @PersonID
			AND PMCPS.IsActive = 1
			AND MCT.MedicalClearanceTypeCode = @MedicalClearanceTypeCode

END
GO
--End procedure person.GetPersonMedicalClearanceProcessStepByPersonID

--Begin procedure person.GetPersonSecurityClearanceByPersonID
EXEC Utility.DropObject 'person.GetPersonSecurityClearanceByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the person.PersonSecurityClearance table
-- ============================================================================================
CREATE PROCEDURE person.GetPersonSecurityClearanceByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PSC.ClearanceGrantedDate,
		core.FormatDate(PSC.ClearanceGrantedDate) AS ClearanceGrantedDateFormatted,
		PSC.DiscretionaryReviewDate,
		core.FormatDate(PSC.DiscretionaryReviewDate) AS DiscretionaryReviewDateFormatted,
		PSC.MandatoryReviewDate,
		core.FormatDate(PSC.MandatoryReviewDate) AS MandatoryReviewDateFormatted,
		PSC.Notes,
		PSC.PersonSecurityClearanceID,
		PSC.SecurityClearanceID,
		PSC.SponsorName,
		PSC.VettingAuthorityName
	FROM person.PersonSecurityClearance PSC
	WHERE PSC.PersonID = @PersonID
		AND PSC.IsActive = 1

END
GO
--End procedure person.GetPersonSecurityClearanceByPersonID

--Begin procedure person.GetPersonSecurityClearanceProcessStepByPersonID
EXEC Utility.DropObject 'person.GetPersonSecurityClearanceProcessStepByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.15
-- Description:	A stored procedure to return data from the person.PersonSecurityClearanceProcessStep table
-- =======================================================================================================
CREATE PROCEDURE person.GetPersonSecurityClearanceProcessStepByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PSCPS.Notes,
		PSCPS.PersonID,
		PSCPS.PersonSecurityClearanceProcessStepID,
		PSCPS.SecurityClearanceID,
		PSCPS.SecurityClearanceProcessStepID,
		PSCPS.StatusDate,
		core.FormatDate(PSCPS.StatusDate) AS StatusDateFormatted
	FROM person.PersonSecurityClearanceProcessStep PSCPS
	WHERE PSCPS.PersonID = @PersonID
		AND PSCPS.IsActive = 1

END
GO
--End procedure person.GetPersonSecurityClearanceProcessStepByPersonID

--Begin procedure person.GetVettingEmailData
EXEC Utility.DropObject 'person.GetVettingEmailData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.22
-- Description:	A stored procedure to return data for outgoing vetting e-mail
-- ==========================================================================
CREATE PROCEDURE person.GetVettingEmailData

@PersonID INT,
@PersonIDList VARCHAR(MAX),
@Notes VARCHAR(MAX),
@DocumentEntityCodeList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO person.PersonSecurityClearanceProcessStep
		(PersonID, SecurityClearanceID, SecurityClearanceProcessStepID, StatusDate, IsActive, Notes)
	SELECT
		P.PersonID,
		(SELECT SCPS.SecurityClearanceID FROM dropdown.SecurityClearance SCPS WHERE SCPS.SecurityClearanceCode = 'BPSS'),
		(SELECT SCPS.SecurityClearanceProcessStepID FROM dropdown.SecurityClearanceProcessStep SCPS WHERE SCPS.SecurityClearanceProcessStepCode = 'PREP'),
		getDate(),
		1,
		'Secrity clearance review forms e-mailed by ' + person.FormatPersonNameByPersonID(@PersonID , 'FirstLast') + '.'  + @Notes 
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	SELECT
		'ToFullName' AS RecordCode,
		P.EmailAddress,
		person.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.DiscretionaryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS DiscretionaryReviewDateFormatted,
		ISNULL((SELECT TOP 1 core.FormatDate(PSC.MandatoryReviewDate) FROM person.PersonSecurityClearance PSC WHERE PSC.PersonID = P.PersonID AND PSC.IsActive = 1), '') AS MandatoryReviewDateFormatted
	FROM person.Person P
		JOIN core.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PersonID

	UNION

	SELECT
		'FromFullName' AS RecordCode,
		core.GetSystemSetupValueBySystemSetupKey('NoReply', '') AS EmailAddress,
		person.FormatPersonNameByPersonID(@PersonID, 'FirstLast') AS PersonNameFormatted,
		NULL,
		NULL

	ORDER BY 1

	EXEC document.GetDocumentsByDocumentEntityCodeList @DocumentEntityCodeList

END
GO
--End procedure person.GetVettingEmailData
