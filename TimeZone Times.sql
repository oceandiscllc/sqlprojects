select *,
(CAST(LEFT(TZI.current_utc_offset, 3) AS NUMERIC(18,2)) * 60) + (CAST(RIGHT(TZI.current_utc_offset, 2) AS NUMERIC(18,2)) * CASE WHEN LEFT(TZI.current_utc_offset, 1) = '-' THEN -1 ELSE 1 END)
,core.FormatDateTime(DATEADD(n, (CAST(LEFT(TZI.current_utc_offset, 3) AS NUMERIC(18,2)) * 60) + (CAST(RIGHT(TZI.current_utc_offset, 2) AS NUMERIC(18,2)) * CASE WHEN LEFT(TZI.current_utc_offset, 1) = '-' THEN -1 ELSE 1 END), SYSUTCDateTime()))
--, core.FormatDateTime(DATEADD(n, (CAST(LEFT(TZI.current_utc_offset, 3) AS NUMERIC(18,2)) * 60) + (CAST(RIGHT(TZI.current_utc_offset, 2) AS NUMERIC(18,2)) * CASE WHEN LEFT(TZI.current_utc_offset, 1) = '-' THEN -1 ELSE 1 END)), SYSUTCDateTime()))
from sys.time_zone_info TZI
