USE InternetDental

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

BEGIN TRANSACTION

	CREATE TABLE dbo.IDCartsNew
		(
		Cart_ID INT NOT NULL IDENTITY(1,1),
		User_ID INT,
		CartName VARCHAR(50),
		Status INT,
		DateAdded DateTime
		)

	SET IDENTITY_INSERT dbo.IDCartsNew ON

	INSERT INTO dbo.IDCartsNew
		(Cart_ID,User_ID,CartName,Status,DateAdded)
	SELECT	
		C.Cart_ID,
		C.User_ID,
		C.CartName,
		C.Status,
		C.DateAdded
	FROM dbo.IDCarts C
	ORDER BY C.Cart_ID, C.User_ID, C.CartName

	SET IDENTITY_INSERT dbo.IDCartsNew OFF

	DROP TABLE dbo.IDCarts

	EXEC sp_rename 'dbo.IDCartsNew', 'IDCarts'

	ALTER TABLE dbo.IDCarts ADD CONSTRAINT DF_IDCarts_DateAdded DEFAULT getDate() FOR DateAdded
	ALTER TABLE dbo.IDCarts ADD CONSTRAINT DF_IDCarts_Status DEFAULT 1 FOR Status
	ALTER TABLE dbo.IDCarts ADD CONSTRAINT DF_IDCarts_User_ID DEFAULT 0 FOR User_ID

	ALTER TABLE dbo.IDCarts ADD CONSTRAINT PK_IDCarts PRIMARY KEY NONCLUSTERED (Cart_ID)

	ALTER TABLE dbo.IDCarts ALTER COLUMN DateAdded DateTime NOT NULL
	ALTER TABLE dbo.IDCarts ALTER COLUMN Status INT NOT NULL
	ALTER TABLE dbo.IDCarts ALTER COLUMN User_ID INT NOT NULL

	CREATE CLUSTERED INDEX IX_IDCarts ON dbo.IDCarts (User_ID,CartName)

	CREATE TABLE dbo.IDCart_ItemsNew
		(
		Cart_Item_ID INT NOT NULL IDENTITY(1,1),
		Cart_ID INT,
		ItemCode VARCHAR(20),
		Quantity INT,
		DataAdded DateTime
		)

	SET IDENTITY_INSERT dbo.IDCart_ItemsNew ON

	INSERT INTO dbo.IDCart_ItemsNew
		(Cart_Item_ID,Cart_ID,ItemCode,Quantity,DataAdded)
	SELECT	
		CI.Cart_Item_ID,
		CI.Cart_ID,
		CI.ItemCode,
		CI.Quantity,
		CI.DataAdded
	FROM dbo.IDCart_Items CI
	ORDER BY CI.Cart_ID, CI.Cart_Item_ID

	SET IDENTITY_INSERT dbo.IDCart_ItemsNew OFF

	DROP TABLE dbo.IDCart_Items

	EXEC sp_rename 'dbo.IDCart_ItemsNew', 'IDCart_Items'

	ALTER TABLE dbo.IDCart_Items ADD CONSTRAINT DF_IDCart_Items_DataAdded DEFAULT getDate() FOR DataAdded
	ALTER TABLE dbo.IDCart_Items ADD CONSTRAINT DF_IDCart_Items_Cart_ID DEFAULT 0 FOR Cart_ID
	ALTER TABLE dbo.IDCart_Items ADD CONSTRAINT DF_IDCart_Items_Quantity DEFAULT 0 FOR Quantity

	ALTER TABLE dbo.IDCart_Items ADD CONSTRAINT PK_IDCart_Items PRIMARY KEY NONCLUSTERED (Cart_Item_ID)

	ALTER TABLE dbo.IDCart_Items ALTER COLUMN DataAdded DateTime NOT NULL
	ALTER TABLE dbo.IDCart_Items ALTER COLUMN Cart_ID INT NOT NULL
	ALTER TABLE dbo.IDCart_Items ALTER COLUMN Quantity INT NOT NULL

	CREATE CLUSTERED INDEX IX_IDCart_Items ON dbo.IDCart_Items (Cart_ID,ItemCode)

	CREATE TABLE dbo.IDOrdersNew
		(
		Order_ID INT IDENTITY(1,1),
		CardCode VARCHAR(15) NOT NULL,
		User_ID INT NOT NULL,
		Shipping_ID INT NOT NULL,
		Billing_ID INT NOT NULL,
		Status INT NOT NULL,
		CartSubTotal NUMERIC(18, 6),
		CartDiscount NUMERIC(18, 6),
		Shipping NUMERIC(18, 6),
		CartTotal NUMERIC(18, 6),
		TrackingNum VARCHAR(45),
		DocNum INT,
		DiscountLevel INT,
		DataAdded DATETIME,
		StatusMessage VARCHAR(100),
		SalesTax NUMERIC(18, 6),
		PONum VARCHAR(100),
		Comment VARCHAR(1000),
		DetailEOF VARCHAR(1),
		DetailLines INT,
		)

	SET IDENTITY_INSERT dbo.IDOrdersNew ON

	INSERT INTO dbo.IDOrdersNew
		(Order_ID,CardCode,User_ID,Shipping_ID,Billing_ID,Status,CartSubTotal,CartDiscount,Shipping,CartTotal,TrackingNum,DocNum,DiscountLevel,DataAdded,StatusMessage,SalesTax,PONum,Comment,DetailEOF,DetailLines)
	SELECT	
		O.Order_ID,
		O.CardCode,
		O.User_ID,
		O.Shipping_ID,
		O.Billing_ID,
		O.Status,
		O.CartSubTotal,
		O.CartDiscount,
		O.Shipping,
		O.CartTotal,
		O.TrackingNum,
		O.DocNum,
		O.DiscountLevel,
		O.DataAdded,
		O.StatusMessage,
		O.SalesTax,
		O.PONum,
		O.Comment,
		O.DetailEOF,
		O.DetailLines
	FROM dbo.IDOrders O
	ORDER BY O.Order_ID, O.User_ID

	SET IDENTITY_INSERT dbo.IDOrdersNew OFF

	DROP TABLE dbo.IDOrders

	EXEC sp_rename 'dbo.IDOrdersNew', 'IDOrders'

	ALTER TABLE dbo.IDOrders ADD CONSTRAINT PK_IDOrders PRIMARY KEY CLUSTERED (Order_ID,CardCode,User_ID,Shipping_ID,Billing_ID)

	IF @@ERROR <> 0
	 BEGIN

		ROLLBACK TRANSACTION
		RETURN
		
		END		
	--ENDIF

COMMIT TRANSACTION
GO