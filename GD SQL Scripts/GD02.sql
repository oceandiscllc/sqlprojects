USE [InternetDental]
GO

/****** Object:  UserDefinedFunction [dbo].[HighlightText]    Script Date: 07/20/2013 18:58:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HighlightText]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[HighlightText]
GO

USE [InternetDental]
GO

/****** Object:  UserDefinedFunction [dbo].[HighlightText]    Script Date: 07/20/2013 18:58:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2013.07.20
-- Description:	A function to wrap target text in a highlight span
-- ===============================================================

CREATE FUNCTION [dbo].[HighlightText]
(
@cTargetString VARCHAR(MAX),
@cSearchString VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

DECLARE @cSearchStringElement VARCHAR(MAX)
DECLARE @cSearchStringElementFormatted VARCHAR(MAX)

DECLARE @nCount INT = 0
DECLARE @nLength INT
DECLARE @nPosition INT

DECLARE @tTable TABLE (ASCIICode INT NOT NULL IDENTITY(1,1) PRIMARY KEY, SearchString VARCHAR(MAX))

SET @cSearchString = @cSearchString + ' '
SET @nPosition = CHARINDEX(' ', @cSearchString)

WHILE @nPosition > 0 AND @nCount < 32
	BEGIN
	
	SET @nCount = @nCount + 1
	SET @cSearchStringElement = LEFT(@cSearchString, @nPosition - 1)

	IF LEN(LTRIM(@cSearchStringElement)) > 0
		BEGIN
		
		IF NOT EXISTS (SELECT 1 FROM @tTable T WHERE T.SearchString = CAST(@cSearchStringElement AS VARCHAR(MAX)))
			BEGIN
			
			SET @cSearchStringElementFormatted = LTRIM(RTRIM(CAST(@cSearchStringElement AS VARCHAR(MAX))))
			INSERT INTO @tTable (SearchString) VALUES (@cSearchStringElementFormatted)
			SET @cTargetString = REPLACE(@cTargetString, @cSearchStringElementFormatted, CHAR(@nCount))
			
			END
		--ENDIF
		
		END
	--ENDIF
	
	SET @nLength = LEN(@cSearchStringElement)
	SET @cSearchString = RIGHT(@cSearchString, LEN(@cSearchString) - @nLength)
	SET @nPosition = CHARINDEX(' ', @cSearchString)

	END
--END WHILE

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT 
		T.ASCIICode,
		UPPER(T.SearchString)
	FROM @tTable T

OPEN oCursor
FETCH oCursor INTO @nCount, @cSearchString
WHILE @@fetch_status = 0
	BEGIN
	
	SET @cTargetString = REPLACE(@cTargetString, CHAR(@nCount), '<span class="cSearchString">' + @cSearchString + '</span>')
	
	FETCH oCursor INTO @nCount, @cSearchString

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	

RETURN @cTargetString
END
--End function dbo.HighlightText


GO


