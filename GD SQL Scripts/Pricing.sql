DECLARE @cDatabaseName varchar(50)
DECLARE @cSQL varchar(max)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DB.Name
	FROM sys.Views DB
	WHERE DB.Name LIKE 'ID_ItemPricing%'
	ORDER BY DB.Name

OPEN oCursor
FETCH oCursor INTO @cDatabaseName
WHILE @@fetch_status = 0
	BEGIN

	SET @cSQL = 'SELECT ''' + @cDatabaseName + ''' AS ViewName, * FROM dbo.' + @cDatabaseName + ' WHERE ItemCode = ''554376'''
	EXEC(@cSQL)

	FETCH oCursor into @cDatabaseName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor	
