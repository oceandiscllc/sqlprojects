USE [InternetDental]
GO

BEGIN TRANSACTION

DECLARE @nOrder_ID INT
DECLARE @tOutput TABLE (Order_ID INT)

INSERT [dbo].[IDOrders] 
	([CardCode], [User_ID], [Shipping_ID], [Billing_ID], [Status], [CartSubTotal], [CartDiscount], [Shipping], [CartTotal], [TrackingNum], [DocNum], [DiscountLevel], [DataAdded], [StatusMessage], [SalesTax], [PONum], [Comment], [DetailEOF], [DetailLines]) 
OUTPUT INSERTED.Order_ID INTO @tOutput
VALUES 
	(N'7777', 26, 1, 2, 1, CAST(323.810000 AS Numeric(18, 6)), CAST(6.630000 AS Numeric(18, 6)), CAST(5.500000 AS Numeric(18, 6)), CAST(329.310000 AS Numeric(18, 6)), NULL, NULL, 1, CAST(0x0000A244010BE74C AS DateTime), NULL, NULL, N'', NULL, N'Y', 5)

SELECT @nOrder_ID = Order_ID
FROM @tOutput

INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 1, N'130718', CAST(1.000000 AS Numeric(19, 6)), CAST(83.640000 AS Numeric(19, 6)), CAST(83.640000 AS Numeric(19, 6)), CAST(69.130000 AS Numeric(19, 6)), CAST(69.130000 AS Numeric(19, 6)), CAST(0x0000A244010BE74C AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 2, N'193555', CAST(2.000000 AS Numeric(19, 6)), CAST(11.780000 AS Numeric(19, 6)), CAST(23.560000 AS Numeric(19, 6)), CAST(7.280000 AS Numeric(19, 6)), CAST(14.560000 AS Numeric(19, 6)), CAST(0x0000A244010BE74C AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 3, N'194700', CAST(4.000000 AS Numeric(19, 6)), CAST(19.320000 AS Numeric(19, 6)), CAST(77.280000 AS Numeric(19, 6)), CAST(14.470000 AS Numeric(19, 6)), CAST(57.880000 AS Numeric(19, 6)), CAST(0x0000A244010BE74C AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 4, N'216976', CAST(2.000000 AS Numeric(19, 6)), CAST(55.990000 AS Numeric(19, 6)), CAST(111.980000 AS Numeric(19, 6)), CAST(46.020000 AS Numeric(19, 6)), CAST(92.040000 AS Numeric(19, 6)), CAST(0x0000A244010BE74C AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 5, N'217902', CAST(2.000000 AS Numeric(19, 6)), CAST(16.990000 AS Numeric(19, 6)), CAST(33.980000 AS Numeric(19, 6)), CAST(13.800000 AS Numeric(19, 6)), CAST(27.600000 AS Numeric(19, 6)), CAST(0x0000A244010BE74C AS DateTime), NULL, NULL)

COMMIT TRANSACTION
GO