UPDATE InternetDental.dbo.IDDyanmicPricingLevels
SET NumberOfLevels = 15
GO

IF NOT EXISTS (SELECT 1 FROM InternetDental.dbo.IDDynamicPricing DP WHERE DP.dynamicPriceLevel = 12)
	INSERT INTO InternetDental.dbo.IDDynamicPricing (dynamci_id,dynamicPriceLevel,dynamicPriceLimit,dateadded) VALUES (12,12,12500,getDate())
--ENDIF

IF NOT EXISTS (SELECT 1 FROM InternetDental.dbo.IDDynamicPricing DP WHERE DP.dynamicPriceLevel = 13)
	INSERT INTO InternetDental.dbo.IDDynamicPricing (dynamci_id,dynamicPriceLevel,dynamicPriceLimit,dateadded) VALUES (13,13,15000,getDate())
--ENDIF	

IF NOT EXISTS (SELECT 1 FROM InternetDental.dbo.IDDynamicPricing DP WHERE DP.dynamicPriceLevel = 14)
	INSERT INTO InternetDental.dbo.IDDynamicPricing (dynamci_id,dynamicPriceLevel,dynamicPriceLimit,dateadded) VALUES (14,14,17500,getDate())
--ENDIF	

IF NOT EXISTS (SELECT 1 FROM InternetDental.dbo.IDDynamicPricing DP WHERE DP.dynamicPriceLevel = 15)
	INSERT INTO InternetDental.dbo.IDDynamicPricing (dynamci_id,dynamicPriceLevel,dynamicPriceLimit,dateadded) VALUES (15,15,20000,getDate())
--ENDIF	
GO
