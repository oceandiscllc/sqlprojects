USE [InternetDental]
GO

BEGIN TRANSACTION

DECLARE @nOrder_ID INT
DECLARE @tOutput TABLE (Order_ID INT)

INSERT [dbo].[IDOrders] 
	([CardCode], [User_ID], [Shipping_ID], [Billing_ID], [Status], [CartSubTotal], [CartDiscount], [Shipping], [CartTotal], [TrackingNum], [DocNum], [DiscountLevel], [DataAdded], [StatusMessage], [SalesTax], [PONum], [Comment], [DetailEOF], [DetailLines]) 
OUTPUT INSERTED.Order_ID INTO @tOutput
VALUES 
	(N'7777', 26, 1, 2, 1, CAST(316.540000 AS Numeric(18, 6)), CAST(177.010000 AS Numeric(18, 6)), CAST(5.500000 AS Numeric(18, 6)), CAST(322.040000 AS Numeric(18, 6)), NULL, NULL, 1, CAST(0x0000A235011145DC AS DateTime), NULL, NULL, N'', NULL, N'Y', 9)

SELECT @nOrder_ID = Order_ID
FROM @tOutput

INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 1, N'042100', CAST(1.000000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 2, N'042120', CAST(1.000000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 3, N'042135', CAST(1.000000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 4, N'042137', CAST(1.000000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(10.500000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(8.030000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 5, N'070102', CAST(1.000000 AS Numeric(19, 6)), CAST(21.200000 AS Numeric(19, 6)), CAST(21.200000 AS Numeric(19, 6)), CAST(18.440000 AS Numeric(19, 6)), CAST(18.440000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 6, N'071828', CAST(1.000000 AS Numeric(19, 6)), CAST(84.310000 AS Numeric(19, 6)), CAST(84.310000 AS Numeric(19, 6)), CAST(75.190000 AS Numeric(19, 6)), CAST(75.190000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 7, N'196600', CAST(6.000000 AS Numeric(19, 6)), CAST(20.050000 AS Numeric(19, 6)), CAST(120.300000 AS Numeric(19, 6)), CAST(15.550000 AS Numeric(19, 6)), CAST(93.300000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 8, N'198292', CAST(1.000000 AS Numeric(19, 6)), CAST(33.690000 AS Numeric(19, 6)), CAST(33.690000 AS Numeric(19, 6)), CAST(28.380000 AS Numeric(19, 6)), CAST(28.380000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)
INSERT [dbo].[IDOrderItems] ([order_ID], [LineNum], [ItemCode], [quantity], [itemPrice], [itemTotal], [discountItemPrice], [discountItemTotal], [dateAdded], [RedeemInfo], [PromoOffer]) VALUES (@nOrder_ID, 9, N'220683', CAST(1.000000 AS Numeric(19, 6)), CAST(21.490000 AS Numeric(19, 6)), CAST(21.490000 AS Numeric(19, 6)), CAST(19.190000 AS Numeric(19, 6)), CAST(19.190000 AS Numeric(19, 6)), CAST(0x0000A235011145DC AS DateTime), NULL, NULL)

COMMIT TRANSACTION
GO