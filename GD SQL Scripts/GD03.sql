USE InternetDental
GO

IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID('dbo.VendorXref') AND SC.name = 'VendorID')
	ALTER TABLE dbo.VendorXref ADD VendorID INT
--ENDIF
GO

DECLARE @tTable TABLE (VendorID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, FriendlyName VARCHAR(100))

INSERT INTO @tTable
	(FriendlyName)
SELECT DISTINCT
	VX.FriendlyName
FROM dbo.VendorXref VX
ORDER BY VX.FriendlyName

UPDATE VX
SET VX.VendorID = T.VendorID
FROM dbo.VendorXref VX
	JOIN @tTable T ON T.FriendlyName = VX.FriendlyName
GO
  
ALTER TABLE dbo.VendorXref ALTER COLUMN VendorID INT NOT NULL
GO

  
