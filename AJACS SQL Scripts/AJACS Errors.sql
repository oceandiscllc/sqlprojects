DECLARE @dNow DATE = getDate()
DECLARE @nMonth INT = DATEPART(M, @dNow)
DECLARE @nYear INT = DATEPART(YYYY, @dNow)

DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))

SELECT
	COUNT(Error) AS ItemCount, 
	Error
FROM [AJACSUtility].[SysLog].[ApplicationErrorLog]
WHERE ErrorDateTime >= @dStartDate
	AND Error NOT LIKE '%twofactor%'
GROUP BY Error
order by 1 desc