USE JIRA
GO

DECLARE @nMonth INT = 04
DECLARE @nYear INT = 2016
DECLARE @dStartDate DATE = CAST(@nMonth AS VARCHAR(2)) + '/01/' + CAST(@nYear AS CHAR(4))

SELECT
	P.PName AS ProjectName,
	--JI.IssueNum,
	--JI.Summary,
	FV.FixVersion,
	SUM(D.TimeWorked)
FROM
	(
	SELECT 
		CAST(SUM(WL.TimeWorked) / 3600 AS NUMERIC(18,2)) AS TimeWorked,
		WL.IssueID
	FROM jira.dbo.WorkLog WL
	WHERE WL.StartDate >= @dStartDate
		--AND WL.StartDate < DATEADD(m, 4, @dStartDate)
	GROUP BY WL.IssueID
	) D
	JOIN dbo.jiraissue JI ON JI.ID = D.IssueID
	JOIN dbo.Project P ON P.ID = JI.Project
	OUTER APPLY
		(
		SELECT
			PV.VName AS FixVersion
		FROM dbo.NodeAssociation NA 
			JOIN dbo.projectversion PV ON PV.ID = NA.SINK_NODE_ID
				AND NA.ASSOCIATION_TYPE = 'IssueFixVersion'
				AND NA.SOURCE_NODE_ID = JI.ID
		) FV
	WHERE P.PName = 'Extractives'
	GROUP BY P.PName, FV.FixVersion
ORDER BY FV.FixVersion--, JI.IssueNum
