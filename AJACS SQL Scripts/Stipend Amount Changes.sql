USE AJACS
GO

SELECT S.* 
FROM [AJACS].[dropdown].[Stipend] S
WHERE S.StipendTypeCode = 'PoliceStipend'
	AND S.StipendCategory IN ('Non Commissioned Officer','Police')

UPDATE S
SET S.StipendAmount = S.StipendAmount + 100
FROM [AJACS].[dropdown].[Stipend] S
WHERE S.StipendTypeCode = 'PoliceStipend'
	AND S.StipendCategory IN ('Non Commissioned Officer','Police')
