USE AJACS
GO

TRUNCATE TABLE dbo.Incident
TRUNCATE TABLE dbo.IncidentCommunity
TRUNCATE TABLE dbo.IncidentProvince
GO

TRUNCATE TABLE dbo.KeyEvent
TRUNCATE TABLE dbo.KeyEventCommunity
TRUNCATE TABLE dbo.KeyEventEngagementCriteriaResult
TRUNCATE TABLE dbo.KeyEventProvince
TRUNCATE TABLE dbo.KeyEventSource
GO

TRUNCATE TABLE dbo.SpotReport
TRUNCATE TABLE dbo.SpotReportComment
TRUNCATE TABLE dbo.SpotReportCommunity
TRUNCATE TABLE dbo.SpotReportIncident
TRUNCATE TABLE dbo.SpotReportProvince
GO

TRUNCATE TABLE weeklyreport.Community
TRUNCATE TABLE weeklyreport.ProgramReport
TRUNCATE TABLE weeklyreport.Province
TRUNCATE TABLE weeklyreport.WeeklyReport
GO

DELETE 
FROM dbo.Person
WHERE UserName NOT IN 
	(
	'Celestine',
	'devdonor',
	'devuser',
	'director',
	'Donor',
	'DonorTest',
	'IanVM',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Officercontest15',
	'Rabaa',
	'Test2',
	'todd.pires'
	)
GO

UPDATE dbo.Contact
SET 
	Address1 = 'Address 1 For' + CAST(ContactID AS VARCHAR(10)),
	Address2 = 'Address 2 For' + CAST(ContactID AS VARCHAR(10)),
	Aliases = NULL,
	ArabicFirstName = NULL,
	ArabicLastName = NULL,
	ArabicMiddleName = NULL,
	ArabicName = NULL,
	CellPhoneNumber = 'CP 555-' + RIGHT('1212' + CAST(ContactID AS VARCHAR(10)), 4),
	City = 'City For' + CAST(ContactID AS VARCHAR(10)),
	DateOfBirth = DATEADD(month, ContactID, CAST('01/' + CAST((ContactID % 27) + 1 AS VARCHAR(10)) + '/1960' AS DATE)),
	EmailAddress1 = 'AjacsContact1-' + CAST(ContactID AS VARCHAR(10)) + '@oceandisc.com',
	EmailAddress2 = 'AjacsContact2-' + CAST(ContactID AS VARCHAR(10)) + '@oceandisc.com',
	EmployerName = 'Ajacs',
	FaceBookPageURL = CAST(ContactID AS VARCHAR(10)) + '@facebook.com',
	FaxNumber = 'FAX 555-' + RIGHT('1212' + CAST(ContactID AS VARCHAR(10)), 4),
	FirstName = 'FN ' + CAST(ContactID AS VARCHAR(10)),
	GovernmentIDNumber = 'ID ' + RIGHT('123987456258' + CAST(ContactID AS VARCHAR(10)), 12),
	LastName = 'LN ' + CAST(ContactID AS VARCHAR(10)),
	MiddleName = 'MN ' + CAST(ContactID AS VARCHAR(10)),
	MotherName = NULL,
	PassportNumber = 'PPT ' + RIGHT('123987456258' + CAST(ContactID AS VARCHAR(10)), 12),
	PhoneNumber = 'PH 555-' + RIGHT('1212' + CAST(ContactID AS VARCHAR(10)), 4),
	PlaceOfBirth = 'Place Of Birth ' + CAST(ContactID AS VARCHAR(10)),
	PostalCode = CAST(ContactID AS VARCHAR(10)),
	Profession = 'Profession for ' + CAST(ContactID AS VARCHAR(10)),
	SkypeUserName = 'AjacsSkypeContact-' + CAST(ContactID AS VARCHAR(10)),
	State = 'State For' + CAST(ContactID AS VARCHAR(10)),
	Title = 'Mr.'
GO

UPDATE dbo.TeamMember
SET
	FirstName = 'FN ' + CAST(TeamMemberID AS VARCHAR(10)),
	LastName = 'LN ' + CAST(TeamMemberID AS VARCHAR(10))
GO

DELETE EWS
FROM workflow.EntityWorkflowStep EWS
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWS.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode <> 'ConceptNote'
GO
