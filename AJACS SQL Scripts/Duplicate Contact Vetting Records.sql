DELETE D
--SELECT *
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY CV.ContactID, CV.VettingOutcomeID, CV.VettingDate, CV.ContactVettingTypeID ORDER BY CV.ContactID, CV.VettingOutcomeID, CV.VettingDate, CV.ContactVettingTypeID) AS RowIndex,
		CV.ContactID,
		CV.VettingOutcomeID, 
		CV.VettingDate, 
		CV.ContactVettingTypeID
	FROM dbo.ContactVetting CV
	) D
WHERE D.RowIndex > 1



