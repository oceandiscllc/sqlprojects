DELETE FROM dbo.ConceptNote WHERE ConceptNoteID = 33
GO

DELETE T FROM dbo.ConceptNoteBudget T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteClass T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteCommunity T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteContact T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteContactEquipment T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteCourse T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteEquipmentCatalog T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteIndicator T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteProvince T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
DELETE T FROM dbo.ConceptNoteTask T WHERE NOT EXISTS (SELECT 1 FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = T.ConceptNoteID)
GO