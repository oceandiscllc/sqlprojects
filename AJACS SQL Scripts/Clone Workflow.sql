IF (SELECT OBJECT_ID('tempdb.dbo.#tOutputWorkflow', 'u')) IS NOT NULL
	DROP TABLE #tOutputWorkflow
--ENDIF
IF (SELECT OBJECT_ID('tempdb.dbo.#tOutputWorkflowStep', 'u')) IS NOT NULL
	DROP TABLE #tOutputWorkflowStep
--ENDIF
IF (SELECT OBJECT_ID('tempdb.dbo.#tOutputWorkflowStepGroup', 'u')) IS NOT NULL
	DROP TABLE #tOutputWorkflowStepGroup
--ENDIF

DECLARE @cWorkflowName VARCHAR(50) = 'US Activity Workflow'
DECLARE @nSourceWorkflowID INT
DECLARE @nTargetWorkflowID INT

CREATE TABLE #tOutputWorkflow (WorkflowID INT NOT NULL PRIMARY KEY)
CREATE TABLE #tOutputWorkflowStep (TargetWorkflowStepID INT NOT NULL PRIMARY KEY, SourceWorkflowStepID INT NOT NULL DEFAULT 0)
CREATE TABLE #tOutputWorkflowStepGroup (TargetWorkflowStepGroupID INT NOT NULL PRIMARY KEY, SourceWorkflowStepGroupID INT NOT NULL)

SELECT TOP 1 @nSourceWorkflowID = W.WorkflowID
FROM workflow.Workflow W
WHERE W.EntityTypeCode = 'ConceptNote'
	AND W.IsActive = 1
ORDER BY W.WorkflowID

INSERT INTO workflow.Workflow
	(EntityTypeCode, WorkflowName, IsActive)
OUTPUT INSERTED.WorkflowID INTO #tOutputWorkflow
SELECT
	W.EntityTypeCode, 
	@cWorkflowName, 
	W.IsActive
FROM workflow.Workflow W
WHERE W.WorkflowID = @nSourceWorkflowID

SELECT @nTargetWorkflowID = O.WorkflowID
FROM #tOutputWorkflow O

;
WITH WSS AS 
	(
	SELECT 
		WS.WorkflowStepID,
		WS.WorkflowStepNumber,
		WS.WorkflowStepName
  FROM workflow.WorkflowStep WS
	WHERE WS.WorkflowID = @nSourceWorkflowID
	)

MERGE INTO workflow.WorkflowStep WST USING WSS ON 1 = 0
WHEN NOT MATCHED THEN
	INSERT 
		(WorkflowID, WorkflowStepNumber, WorkflowStepName)
	VALUES
		( 
		@nTargetWorkflowID,
		WSS.WorkflowStepNumber,
		WSS.WorkflowStepName
		)
	OUTPUT INSERTED.WorkflowStepID, WSS.WorkflowStepID
	INTO #tOutputWorkflowStep (TargetWorkflowStepID, SourceWorkflowStepID)
;

WITH WSGS AS 
	(
	SELECT 
		WSG.WorkflowStepGroupID,
		O.TargetWorkflowStepID,
		WSG.WorkflowStepGroupName
  FROM workflow.WorkflowStepGroup WSG
		JOIN #tOutputWorkflowStep O ON O.SourceWorkflowStepID = WSG.WorkflowStepID
	)

MERGE INTO workflow.WorkflowStepGroup WSGT USING WSGS ON 1 = 0
WHEN NOT MATCHED THEN
	INSERT 
		(WorkflowStepID, WorkflowStepGroupName)
	VALUES
		( 
		WSGS.TargetWorkflowStepID,
		WSGS.WorkflowStepGroupName
		)
	OUTPUT INSERTED.WorkflowStepGroupID, WSGS.WorkflowStepGroupID
	INTO #tOutputWorkflowStepGroup (TargetWorkflowStepGroupID, SourceWorkflowStepGroupID)
;

WITH WSGPS AS 
	(
	SELECT 
		WSGP.WorkflowStepGroupPersonID,
		O.TargetWorkflowStepGroupID,
		WSGP.PersonID
  FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN #tOutputWorkflowStepGroup O ON O.SourceWorkflowStepGroupID = WSGP.WorkflowStepGroupID
	)

MERGE INTO workflow.WorkflowStepGroupPerson WSGPT USING WSGPS ON 1 = 0
WHEN NOT MATCHED THEN
	INSERT 
		(WorkflowStepGroupID, PersonID)
	VALUES
		( 
		WSGPS.TargetWorkflowStepGroupID,
		WSGPS.PersonID
		)
;

DROP TABLE #tOutputWorkflow
DROP TABLE #tOutputWorkflowStep
DROP TABLE #tOutputWorkflowStepGroup
GO

/*
DELETE W
FROM workflow.Workflow W
WHERE W.WorkflowID > 21

DELETE WS
FROM workflow.WorkflowStep WS
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.Workflow W
	WHERE W.WorkflowID = WS.WorkflowID
	)

DELETE WSG
FROM workflow.WorkflowStepGroup WSG
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.WorkflowStep WS
	WHERE WS.WorkflowStepID = WSG.WorkflowStepID
	)

DELETE WSGP
FROM workflow.WorkflowStepGroupPerson WSGP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.WorkflowStepGroup WSG
	WHERE WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
	)
*/
