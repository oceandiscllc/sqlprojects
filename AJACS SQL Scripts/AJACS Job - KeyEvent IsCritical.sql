DECLARE @nKeyEventID INT
DECLARE @tOutput TABLE (KeyEventID INT)

UPDATE T
SET T.IsCritical = 0
OUTPUT INSERTED.KeyEventID INTO @tOutput
FROM dbo.KeyEvent T
WHERE T.IsCritical = 1
	AND DATEDIFF(d, T.KeyEventDate, getDate()) > CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey('KeyEventIsCriticalExpirationDays', '10') AS INT)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT O.KeyEventID
	FROM @tOutput O
	ORDER BY O.KeyEventID
	
OPEN oCursor
FETCH oCursor INTO @nKeyEventID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogKeyEventAction @nKeyEventID, 'update', 0, 'System job expired the IsCritical flag'

	FETCH oCursor INTO @nKeyEventID
	
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor	
GO