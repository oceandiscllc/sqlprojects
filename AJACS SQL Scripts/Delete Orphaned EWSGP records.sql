SELECT *
FROM workflow.EntityWorkflowStepGroupPerson EWSGP
WHERE EWSGP.EntityTypeCode = 'SpotReport'
	AND EWSGP.EntityID IN (184,197,204,206)
ORDER BY EWSGP.EntityID, EWSGP.WorkflowStepNumber, EWSGP.EntityWorkflowStepGroupPersonID

SELECT *
FROM dbo.SpotReport
WHERE SpotReportID IN (184,197,204,206)

DELETE EWSGP
--SELECT *
FROM workflow.EntityWorkflowStepGroupPerson EWSGP
WHERE EWSGP.EntityTypeCode = 'ConceptNote'
	AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.ConceptNote
		WHERE ConceptNoteID = EWSGP.EntityID
		)
