USE JIRA
GO

WITH PD AS
	(
	SELECT 
		D.PName AS ProjectName,
		U.Last_Name + ', ' + U.First_Name AS EmployeeName,
		D.AmountOwed,
		D.IsPartner
	FROM
		(
		SELECT
			P.PName,
			ODR.IsPartner,
			WL.Author,
			CAST(SUM(WL.TimeWorked / 3600) AS NUMERIC(18,2)) AS TimeWorked,
			CAST(SUM(WL.TimeWorked / 3600 * ODR.Rate) AS NUMERIC(18,2)) AS AmountOwed
		FROM JIRA.dbo.WorkLog WL
			JOIN JIRA.dbo.JIRAIssue JI ON JI.ID = WL.IssueID
			JOIN JIRA.dbo.Project P ON P.ID = JI.Project
				AND P.PKey = 'efe'
			JOIN JIRA.dbo.ODRate ODR ON ODR.Author = WL.Author
				AND ODR.EffectiveDate <= CAST(WL.StartDate AS DATE)
				AND 
					(
					ODR.ExpirationDate IS NULL 
						OR ODR.ExpirationDate >= CAST(WL.StartDate AS DATE)
					)
		GROUP BY P.PName, WL.Author, ODR.IsPartner
		) D
		JOIN JIRA.dbo.ODRate ODR ON ODR.Author = D.Author
			AND ODR.ExpirationDate IS NULL
		JOIN JIRA.dbo.CWD_User U ON U.User_Name = CASE WHEN ODR.Author = 'tpires' THEN 'todd.pires' ELSE ODR.Author END
		--ORDER BY D.PName, ODR.IsPartner, U.Last_Name, U.First_Name
	)

SELECT 
	PD.*
FROM PD

UNION

SELECT 
	'Total',
	'',
	SUM(PD.AmountOwed),
	0
FROM PD

ORDER BY 1, 2
