USE AJACS
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
	WHERE WS.ParentWorkflowStepID = 0
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = W.EntityTypeCode + '.AddUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

DELETE FROM permissionable.Permissionable WHERE IsWorkflow = 1

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, T.WorkflowStepID INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY DisplayIndex) AS DisplayOrder,
	1
FROM @tTable T
WHERE ParentWorkflowStepID = 0
ORDER BY DisplayIndex

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY PermissionableName) AS DisplayOrder,
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
ORDER BY DisplayIndex
