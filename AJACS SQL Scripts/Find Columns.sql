USE AJACS
GO

DECLARE @cColumnName2 VARCHAR(50) = 'EquipmentDistributionID'
DECLARE @cIDList VARCHAR(MAX) = '544'

SELECT 
	NULL AS SchemaName, 
	NULL AS TableName,
	NULL AS ColumnName,
	'DECLARE @cIDList VARCHAR(MAX) = ''' + @cIDList + '''' AS SelectData,
	'DECLARE @cIDList VARCHAR(MAX) = ''' + @cIDList + '''' AS DeleteData

UNION

SELECT
	S.Name AS SchemaName,
	T.Name AS TableName,
	C.Name AS ColumnName,
	'SELECT * FROM ' + S.Name + '.' + T.Name + ' T JOIN dbo.ListToTable(@cIDList, '','') LTT ON CAST(LTT.ListItem AS INT) = T.' + C.Name,
	'DELETE T FROM ' + S.Name + '.' + T.Name + ' T JOIN dbo.ListToTable(@cIDList, '','') LTT ON CAST(LTT.ListItem AS INT) = T.' + C.Name
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND 
			(
				C.Name LIKE @cColumnName2
			)
		--AND S.Name NOT IN ('Deprecated','Dropdown','Reporting','Staging')
ORDER BY 2, 3

/*
USE AJACS
GO

EXEC utility.AddSchema 'deprecated'
GO

EXEC utility.DropObject 'procurement.CommunityEquipmentInventory'
EXEC utility.DropObject 'procurement.CommunityEquipmentInventoryAudit'
EXEC utility.DropObject 'procurement.ProvinceEquipmentInventory'
EXEC utility.DropObject 'procurement.ProvinceEquipmentInventoryAudit'
EXEC utility.DropObject 'procurement.EquipmentCatalogImport'
GO

TRUNCATE TABLE procurement.DistributedInventory
TRUNCATE TABLE procurement.DistributedInventoryAudit
TRUNCATE TABLE procurement.EquipmentInventory
TRUNCATE TABLE procurement.LicenseEquipmentInventory
TRUNCATE TABLE procurement.EquipmentDistribution
TRUNCATE TABLE procurement.EquipmentDistributionPlan
GO

ALTER SCHEMA deprecated TRANSFER dbo.ConceptNoteContactEquipment
ALTER SCHEMA deprecated TRANSFER procurement.EquipmentAuditOutcome
ALTER SCHEMA deprecated TRANSFER procurement.EquipmentDistributionPlan
GO

UPDATE procurement.LicenseEquipmentCatalog
SET
	QuantityPending = 0,
	QuantityObligated = 0,
	QuantityPurchased = 0,
	QuantityDistributed = 0
GO


DECLARE @cColumnName2 VARCHAR(50) = 'ProjectID'

SELECT
	S.Name AS SchemaName,
	T.Name AS TableName,
	C.Name AS ColumnName,
	'UPDATE ' + S.Name + '.' + T.Name + ' SET ' + C.Name + ' = ' + C.Name + ' * 10'
FROM sys.Tables T
	JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
	JOIN sys.Columns C ON C.Object_ID = T.Object_ID
		AND C.Name = @cColumnName2
		AND S.Name <> 'dropdown'
ORDER BY 1, 2, 3
*/
