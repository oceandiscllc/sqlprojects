USE AJACS
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1
	JOIN dropdown.Role R ON R.RoleID = P1.RoleID
	CROSS JOIN permissionable.Permissionable P2
WHERE 
	(
	(R.RoleName = 'Third Party Monitoring' AND P2.PermissionableLineage IN ('Community.View.Information','Province.View.Information'))
		OR (R.RoleName = 'Donor' AND P2.PermissionableLineage IN ('Community.View.Information','Community.View.Analysis','Province.View.Information','Province.View.Analysis'))
		OR (R.RoleName NOT IN ('Donor', 'Third Party Monitoring') AND P2.PermissionableLineage IN ('Community.View.Information','Community.View.Analysis','Community.View.Implementation','Province.View.Information','Province.View.Analysis','Province.View.Implementation'))
	)
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.PersonPermissionable PP
		WHERE PP.PersonID = P1.PersonID
			AND PP.PermissionableLineage = P2.PermissionableLineage
		)

--91	Other										=546
--20	Donor										=80
--3		Third Party Monitoring  =6