SELECT EWS.*
FROM [AJACS].[workflow].[EntityWorkflowStep] ews
	JOIN [workflow].[WorkflowStep] ws on ws.[WorkflowStepID] = ews.[WorkflowStepID]
	JOIN [workflow].[Workflow] W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'ProgramReport'
		ORDER BY 2, 3

/*
UPDATE [workflow].[EntityWorkflowStep]
SET IsComplete = 0
WHERE [EntityWorkflowStepID] = 2153

UPDATE [weeklyreport].[ProgramReport] SET WorkflowStepNumber = 3 WHERE [ProgramReportID] = 14
*/