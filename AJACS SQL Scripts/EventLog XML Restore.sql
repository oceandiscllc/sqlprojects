DECLARE @tTable TABLE (XMLData XML)

INSERT INTO @tTable
	(XMLData)
SELECT CAST(C1 AS XML) AS XMLData
FROM OPENROWSET (BULK 'C:\www\CNBudget23.xml', SINGLE_BLOB) AS T1(C1)

SELECT
  ConceptNoteBudgets.value('(ConceptNoteBudgetID)[1]', 'int') AS ConceptNoteBudgetID,
  ConceptNoteBudgets.value('(ConceptNoteID)[1]', 'int') AS ConceptNoteID,
  ConceptNoteBudgets.value('(ItemName)[1]', 'varchar(100)') AS ItemName,
  ConceptNoteBudgets.value('(BudgetTypeID)[1]', 'int') AS BudgetTypeID,
  ConceptNoteBudgets.value('(Quantity)[1]', 'numeric(18,4)') AS Quantity,
  ConceptNoteBudgets.value('(UnitCost)[1]', 'numeric(18,2)') AS UnitCost,
  ConceptNoteBudgets.value('(ItemDescription)[1]', 'varchar(max)') AS ItemDescription,
  ConceptNoteBudgets.value('(SpentToDate)[1]', 'numeric(18,2)') AS SpentToDate,
  ConceptNoteBudgets.value('(UnitOfIssue)[1]', 'varchar(25)') AS UnitOfIssue,
  ConceptNoteBudgets.value('(BudgetSubTypeID)[1]', 'int') AS BudgetSubTypeID,
  ConceptNoteBudgets.value('(QuantityOfIssue)[1]', 'numeric(18,4)') AS QuantityOfIssue
FROM @tTable
CROSS APPLY XMLData.nodes('/ConceptNoteBudgets/ConceptNoteBudget') AS T(ConceptNoteBudgets)
ORDER BY ConceptNoteBudgets.value('(ConceptNoteBudgetID)[1]', 'int')
GO

/*
***** Use this *****
<?xml version="1.0" encoding="UTF-8"?>
<ConceptNoteBudgets>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14182</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Field Manager (TBD)</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>14.0000</Quantity>
		<UnitCost>90.00</UnitCost>
		<ItemDescription>Coordinates data collection and translation. One for each Province</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>persons</UnitOfIssue>
		<BudgetSubTypeID>3</BudgetSubTypeID>
		<QuantityOfIssue>2.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14183</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Translator</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>10.0000</Quantity>
		<UnitCost>95.00</UnitCost>
		<ItemDescription>Transcription and translation - 4 translators x 10 days LOE each</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>persons</UnitOfIssue>
		<BudgetSubTypeID>3</BudgetSubTypeID>
		<QuantityOfIssue>4.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14184</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Research Team Lodging - Debrief</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>4.0000</Quantity>
		<UnitCost>120.00</UnitCost>
		<ItemDescription>Lodging for 8 members of research team  to attend 2 day debrief  in Gaziantep.  2 nights x 4 double rooms</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>days</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14185</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Full Board Costs - both workshops</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>22.0000</Quantity>
		<UnitCost>20.00</UnitCost>
		<ItemDescription>Participant M&amp;IE and Dinner 14 WS1 and 8 WS2 participants x 2 days x $20 per day</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>workshops</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>2.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14186</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Ground transport Syria to home of record</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>8.0000</Quantity>
		<UnitCost>150.00</UnitCost>
		<ItemDescription>Ground transportation cost from Aleppo / Idlib to home of record is 150 each way x 8 field researchers not selected for debrief</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>RTN</UnitOfIssue>
		<BudgetSubTypeID>2</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14187</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Cellular data and service</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>22.0000</Quantity>
		<UnitCost>25.00</UnitCost>
		<ItemDescription>Service for all 22 staff members to use inside and outside Syria</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Piece</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14188</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>M&amp;E Advisor</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>18.0000</Quantity>
		<UnitCost>310.00</UnitCost>
		<ItemDescription>Oversee data collection, cleaning and entry into database</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Days</UnitOfIssue>
		<BudgetSubTypeID>3</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14189</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Local Ground transport in Syria</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>8.0000</Quantity>
		<UnitCost>40.00</UnitCost>
		<ItemDescription>8 researcher pairs x $40 to cover local transportation around each community during data collection</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>RTN</UnitOfIssue>
		<BudgetSubTypeID>2</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14190</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Accommodation - AJACS</ItemName>
		<BudgetTypeID>1</BudgetTypeID>
		<Quantity>2.0000</Quantity>
		<UnitCost>131.00</UnitCost>
		<ItemDescription>1 M&amp;E Manager, DTL, and 1 Support Staff x 2 nights</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>persons</UnitOfIssue>
		<BudgetSubTypeID>7</BudgetSubTypeID>
		<QuantityOfIssue>3.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14191</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Field Director (Razan al Sham)</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>7.0000</Quantity>
		<UnitCost>160.00</UnitCost>
		<ItemDescription>Providing  insight into each beneficiary community. Troubleshoot issues throughout implementation. 35% time over 20 days</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Days</UnitOfIssue>
		<BudgetSubTypeID>3</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14192</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Meeting Room</ItemName>
		<BudgetTypeID>1</BudgetTypeID>
		<Quantity>2.0000</Quantity>
		<UnitCost>600.00</UnitCost>
		<ItemDescription>Includes coffee and lunch breaks</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Rooms</UnitOfIssue>
		<BudgetSubTypeID>7</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14193</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Ground transport Gaz to Syria (from training)</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>16.0000</Quantity>
		<UnitCost>250.00</UnitCost>
		<ItemDescription>Ground transportation cost from the training in Gaziantep to Aleppo / Idlib is 250 one way x 16 field researchers.</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>One Way</UnitOfIssue>
		<BudgetSubTypeID>2</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14194</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Tax on Salaries</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>0.2000</Quantity>
		<UnitCost>15930.00</UnitCost>
		<ItemDescription>20% on paid salaries to contracted staff based in Turkey or Syria and paid through Turkey office</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Fee</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14195</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Interpretation Services</ItemName>
		<BudgetTypeID>1</BudgetTypeID>
		<Quantity>2.0000</Quantity>
		<UnitCost>500.00</UnitCost>
		<ItemDescription>2 interpreters for 2 days</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>persons</UnitOfIssue>
		<BudgetSubTypeID>7</BudgetSubTypeID>
		<QuantityOfIssue>2.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14196</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Supplies for Research Teams</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>8.0000</Quantity>
		<UnitCost>25.00</UnitCost>
		<ItemDescription>clipboards, binders, notebooks, pens, and folders. $25 per team x  8 teams.</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Piece</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14197</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Translation of course materials</ItemName>
		<BudgetTypeID>1</BudgetTypeID>
		<Quantity>1.0000</Quantity>
		<UnitCost>1000.00</UnitCost>
		<ItemDescription>Translation from English to Arabic</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Lump Sum</UnitOfIssue>
		<BudgetSubTypeID>7</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14198</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Skype Credit</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>5.0000</Quantity>
		<UnitCost>10.00</UnitCost>
		<ItemDescription>Skype credit for 5 staff:  Field Director, Field Coordinators (x2), Program Manager and M&amp;E Advisor</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Piece</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14199</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Training Materials</ItemName>
		<BudgetTypeID>1</BudgetTypeID>
		<Quantity>22.0000</Quantity>
		<UnitCost>7.50</UnitCost>
		<ItemDescription>Nametags, notebooks, whiteboard, flipcharts, etc.</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Lump Sum</UnitOfIssue>
		<BudgetSubTypeID>7</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14200</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Operations manager (Susanne Porzelt</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>5.0000</Quantity>
		<UnitCost>218.00</UnitCost>
		<ItemDescription>Backstopping daily operations 25% time over 20 days</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Days</UnitOfIssue>
		<BudgetSubTypeID>3</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14201</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>New SIM Cards</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>2.0000</Quantity>
		<UnitCost>25.00</UnitCost>
		<ItemDescription>SIM cards for 2 field researchers that don't have one</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Piece</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14202</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Travel from Syria to Debrief</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>250.0000</Quantity>
		<UnitCost>8.00</UnitCost>
		<ItemDescription>Ground transport - to and from Syria to Gaziantep. 250 one way x 8 participants</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>One way</UnitOfIssue>
		<BudgetSubTypeID>2</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14203</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Internet</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>1.0000</Quantity>
		<UnitCost>100.00</UnitCost>
		<ItemDescription>Internet service for office</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Piece</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14204</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Printing</ItemName>
		<BudgetTypeID>1</BudgetTypeID>
		<Quantity>22.0000</Quantity>
		<UnitCost>5.00</UnitCost>
		<ItemDescription>Printing of materials for each participant</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Lump Sum</UnitOfIssue>
		<BudgetSubTypeID>7</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14205</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Simultaneous translation equipment hire</ItemName>
		<BudgetTypeID>1</BudgetTypeID>
		<Quantity>1.0000</Quantity>
		<UnitCost>1000.00</UnitCost>
		<ItemDescription>Equipment parts + technician and transport for booth</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Lump Sum</UnitOfIssue>
		<BudgetSubTypeID>7</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14206</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Research Team Lodging - Initial Training</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>7.0000</Quantity>
		<UnitCost>120.00</UnitCost>
		<ItemDescription>Lodging for 14 Syria-based members of the research team  to attend a 2 day training in Gaziantep.  2 nights x 7 double rooms.</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Days</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>2.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14207</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Programs Manager (Joel Bombardier)</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>4.0000</Quantity>
		<UnitCost>190.00</UnitCost>
		<ItemDescription>Reviews  all deliverables and ensures full compliance with SOW and edit final reports - 4 days LOE</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Days</UnitOfIssue>
		<BudgetSubTypeID>3</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14208</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Printing Costs</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>8.0000</Quantity>
		<UnitCost>50.00</UnitCost>
		<ItemDescription>Printing/photocopying copies of materials.  $50 per team x 8 teams.</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Piece</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14209</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Field Researchers (TBD)</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>10.0000</Quantity>
		<UnitCost>70.00</UnitCost>
		<ItemDescription>Conducts interviews/collects data. 16 staff, two for each community.</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>persons</UnitOfIssue>
		<BudgetSubTypeID>3</BudgetSubTypeID>
		<QuantityOfIssue>16.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14210</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Travel from Syria to Initial Training</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>14.0000</Quantity>
		<UnitCost>250.00</UnitCost>
		<ItemDescription>Ground transport - to and from home in Syria to Gaziantep. 250 one way x 14 participants</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>One Way</UnitOfIssue>
		<BudgetSubTypeID>2</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14211</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Utilities</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>1.0000</Quantity>
		<UnitCost>250.00</UnitCost>
		<ItemDescription>Utilities include: water, electricity, heating, building services and cleaning services</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Month</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
	<ConceptNoteBudget>
		<ConceptNoteBudgetID>14212</ConceptNoteBudgetID>
		<ConceptNoteID>23</ConceptNoteID>
		<ItemName>Office Rent</ItemName>
		<BudgetTypeID>4</BudgetTypeID>
		<Quantity>1.0000</Quantity>
		<UnitCost>610.00</UnitCost>
		<ItemDescription>Office rent for Gaziantep office based on a one month breakout of annual contract</ItemDescription>
		<SpentToDate>0.00</SpentToDate>
		<UnitOfIssue>Month</UnitOfIssue>
		<BudgetSubTypeID>6</BudgetSubTypeID>
		<QuantityOfIssue>1.0000</QuantityOfIssue>
	</ConceptNoteBudget>
</ConceptNoteBudgets>
*/
