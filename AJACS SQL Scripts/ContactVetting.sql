					SELECT
						E.ContactID,
						E.USContactVettingID,
						OACV12.VettingDate AS USVettingDate,
						ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,
						VO1.VettingOutcomeName AS USVettingOutcomeName,
						E.UKContactVettingID,
						OACV22.VettingDate AS UKVettingDate,
						ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,
						VO2.VettingOutcomeName AS UKVettingOutcomeName
					FROM
						(
						SELECT
							D.ContactID,
							ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
							ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
						FROM	
							(
							SELECT 
								MAX(CV0.ContactVettingID) AS ContactVettingID,
								CV0.ContactID
							FROM dbo.ContactVetting CV0
								JOIN dbo.Contact C ON C.ContactID = CV0.ContactID
									AND C.IsActive = 1
									AND EXISTS
										(
										SELECT 1
										FROM dbo.ContactContactAffiliation CCA
											JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
												AND CA.ContactAffiliationName = 'Community Security Working Groups'
												AND CCA.ContactID = C.ContactID
										)
									AND EXISTS
										(
										SELECT 1
										FROM dbo.ContactContactType CCT
											JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
												AND CT.ContactTypeCode = 'Beneficiary'
												AND CCT.ContactID = C.ContactID
										)
							GROUP BY CV0.ContactID
							) D
							OUTER APPLY
								(
								SELECT
									MAX(CV11.ContactVettingID) AS ContactVettingID
								FROM dbo.ContactVetting CV11
								WHERE CV11.ContactID = D.ContactID
									AND CV11.ContactVettingTypeID = 1
								) OACV11
							OUTER APPLY
								(
								SELECT
									MAX(CV21.ContactVettingID) AS ContactVettingID
								FROM dbo.ContactVetting CV21
								WHERE CV21.ContactID = D.ContactID
									AND CV21.ContactVettingTypeID = 2
								) OACV21
						) E
						OUTER APPLY
							(
							SELECT
								CV12.VettingDate,
								CV12.VettingOutcomeID
							FROM dbo.ContactVetting CV12
							WHERE CV12.ContactVettingID = E.USContactVettingID
							) OACV12
						OUTER APPLY
							(
							SELECT
								CV22.VettingDate,
								CV22.VettingOutcomeID
							FROM dbo.ContactVetting CV22
							WHERE CV22.ContactVettingID = E.UKContactVettingID
							) OACV22
						JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = ISNULL(OACV12.VettingOutcomeID, 0)
						JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = ISNULL(OACV22.VettingOutcomeID, 0)
							AND ISNULL(OACV12.VettingOutcomeID, 0) = 0
		ORDER BY 5, 1

