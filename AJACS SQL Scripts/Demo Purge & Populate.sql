USE AJACS
GO

!! remeber to backup the person data !!

DECLARE @cSQLText VARCHAR(MAX)

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		'TRUNCATE TABLE ' + S.Name + '.' + T.Name AS SQLText
	FROM sys.Tables T
		JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID
			AND S.Name NOT IN ('dropdown','permissionable','utility','workflow')
			AND T.Name NOT IN ('DocumentMimeType','EntityType','MenuItem','MenuItemPermissionableLineage','Person')
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @cSQLText
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC (@cSQLText)

	FETCH oCursor INTO @cSQLText
	
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor	
GO

DELETE P 
FROM dbo.Person P 
WHERE P.UserName <> 'JCole'
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.Person P
	WHERE P.PersonID = PP.PersonID
	)
GO

TRUNCATE TABLE workflow.EntityWorkflowStep
GO

SET IDENTITY_INSERT dbo.Community ON 

GO
INSERT dbo.Community 
	(CommunityID, CommunityName, ProvinceID, CommunityGroupID, CommunitySubGroupID, PoliceStationNumber, PoliceStationStatusID, PolicePresenceCategoryID, PolicePresenceStatusID, ImpactDecisionID, StatusChangeID, Population, PopulationSource, CommunityEngagementStatusID, Summary, KeyPoints, Implications, RiskMitigation, ProgramNotes1, ProgramNotes2, ProgramNotes3, ProgramNotes4, ProgramNotes5, Latitude, Longitude) 
VALUES 
	(1, N'Sample Community 1.1', 8, 1, 1, 1, 3, 1, 2, 3, 3, 1500, N'FCO Estimate', 2, N'<p>Sample community 1 for demonstration purposes</p>', N'<p>Add detail as you wish</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(-1.277997000000000 AS Numeric(20, 15)), CAST(36.877539000000000 AS Numeric(20, 15))),
	(2, N'Sample Project 2', 8, 1, 2, 3, 2, 1, 3, 4, 3, 3000, N'SK Estimate', 3, N'<p>This is a sample project for demonstration purposes</p>', N'<p>Add details as necessary</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0.569092000000000 AS Numeric(20, 15)), CAST(40.715811000000000 AS Numeric(20, 15))),
	(3, N'Sample Project 1', 8, 2, 2, 5, 1, 1, 0, 4, 3, 800, N'SK Estimate', 3, N'<p>This is a sample project for demonstration purposes</p>', N'<p>Please add detail</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(2.326275000000000 AS Numeric(20, 15)), CAST(42.242910000000000 AS Numeric(20, 15))),
	(4, N'Sample Project 3', 8, 2, 2, 0, 0, 0, 0, 0, 0, 1300, N'SK Estimate', 3, N'<p>This is a sample project for demonstration purposes</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(3.868948000000000 AS Numeric(20, 15)), CAST(40.517815000000000 AS Numeric(20, 15))),
	(5, N'Sample Project 4', 8, 4, 2, 1, 3, 2, 1, 4, 3, 2000, N'SK Estimate', 4, N'<p>This is a sample project for demonstration purposes</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(-0.094173000000000 AS Numeric(20, 15)), CAST(42.671135000000000 AS Numeric(20, 15)))
GO

SET IDENTITY_INSERT dbo.Community OFF
GO

INSERT dbo.CommunityEngagementCriteriaResult 
	(CommunityID, EngagementCriteriaID, EngagementCriteriaStatusID) 
VALUES 
	(1, 1, 0),
	(1, 2, 0),
	(1, 3, 0),
	(1, 4, 0),
	(1, 5, 0),
	(2, 1, 0),
	(2, 2, 0),
	(2, 3, 0),
	(2, 4, 0),
	(2, 5, 0),
	(3, 1, 0),
	(3, 2, 0),
	(3, 3, 0),
	(3, 4, 0),
	(3, 5, 0),
	(4, 1, 0),
	(4, 2, 0),
	(4, 3, 0),
	(4, 4, 0),
	(4, 5, 0),
	(5, 1, 0),
	(5, 2, 0),
	(5, 3, 0),
	(5, 4, 0),
	(5, 5, 0)
GO

SET IDENTITY_INSERT dbo.Incident ON 
GO

INSERT dbo.Incident 
	(IncidentID, IncidentName, IncidentDate, IncidentTypeID, SourceReliabilityID, InformationValidityID, Summary, KeyPoints, Implications, RiskMitigation, Latitude, Longitude) 
VALUES 
	(1, N'Armed group mass execution', CAST(N'2015-05-12' AS Date), 4, 2, 1, NULL, NULL, NULL, NULL, CAST(1.752046223357981 AS Numeric(20, 15)), CAST(40.072631835937500 AS Numeric(20, 15))),
	(2, N'Sample Incident', CAST(N'2015-05-21' AS Date), 2, 1, 2, N'<p>This is a sample incident</p>', N'<p>The following sample issues have been identified</p><ul><li>Sample issue 1</li><li>Sample issue 2</li><li>Sample issue 3</li></ul>', N'<p><strong>The risk register will need updating </strong>to ensure that the risk to staff is reflected</p>', N'<p>All staff to log their movements with the office.</p>', CAST(-0.729472412211201 AS Numeric(20, 15)), CAST(42.213683724403380 AS Numeric(20, 15)))
GO

SET IDENTITY_INSERT dbo.Incident OFF
GO

INSERT dbo.IncidentCommunity 
	(IncidentID, CommunityID) 
VALUES 
	(2, 1)
GO

INSERT dbo.IncidentProvince 
	(IncidentID, ProvinceID) 
VALUES 
	(1, 8)
GO

SET IDENTITY_INSERT dbo.Province ON 
GO

INSERT dbo.Province 
	(ProvinceID, ProvinceName, ImpactDecisionID, StatusChangeID, Summary, Implications, RiskMitigation, KeyPoints, ProgramNotes1, ProgramNotes2) 
VALUES 
	(8, N'Sample Province 1', 3, 3, N'<p>This is a sample province for demonstration purposes</p>', NULL, NULL, N'<p>Please feel free to add detail</p>', NULL, NULL)
GO

SET IDENTITY_INSERT dbo.Province OFF
GO
