USE AJACS
GO

--DELETE D
SELECT *
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY EWSGP.EntityID, EWSGP.WorkflowID, EWSGP.WorkflowStepID, EWSGP.WorkflowStepGroupID, EWSGP.WorkflowStepNumber, EWSGP.PersonID ORDER BY EWSGP.EntityID, EWSGP.WorkflowID, EWSGP.WorkflowStepID, EWSGP.WorkflowStepGroupID, EWSGP.WorkflowStepNumber, EWSGP.PersonID) AS RowIndex,
		EWSGP.EntityID, 
		EWSGP.WorkflowID, 
		EWSGP.WorkflowStepID, 
		EWSGP.WorkflowStepGroupID, 
		EWSGP.WorkflowStepNumber,
		EWSGP.PersonID
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EXISTS
		(
		SELECT 1
		FROM workflow.Workflow W
		WHERE W.WorkflowID = EWSGP.WorkflowID
		)
	) D
WHERE D.RowIndex > 1



