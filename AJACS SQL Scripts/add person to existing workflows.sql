USE AJACS
GO

SELECT
	EntityTypeCode, 0 AS EntityID, W.WorkflowID, WS.WorkflowStepID, WSG.WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID
FROM [workflow].[Workflow] W
	JOIN [workflow].[WorkflowStep] WS ON WS.WorkflowID = W.WorkflowID
	JOIN [workflow].[WorkflowStepGroup] WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
	JOIN [workflow].[WorkflowStepGroupPerson] WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
		AND W.WorkflowID IN (21,22,23)
ORDER BY W.WorkflowID, WS.WorkflowStepID, WSG.WorkflowStepGroupID, WSGP.PersonID


INSERT INTO [workflow].[EntityWorkflowStepGroupPerson]
	(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, IsComplete, PersonID)
SELECT
	EWSGP.EntityTypeCode, EWSGP.EntityID, EWSGP.WorkflowID, EWSGP.WorkflowStepID, EWSGP.WorkflowStepGroupID, EWSGP.WorkflowName, EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepName, EWSGP.WorkflowStepGroupName, EWSGP.IsComplete, 109
FROM [workflow].[EntityWorkflowStepGroupPerson] EWSGP
	JOIN 
		(
		SELECT
			W.WorkflowID, WS.WorkflowStepID, WSG.WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID
		FROM [workflow].[Workflow] W
			JOIN [workflow].[WorkflowStep] WS ON WS.WorkflowID = W.WorkflowID
			JOIN [workflow].[WorkflowStepGroup] WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN [workflow].[WorkflowStepGroupPerson] WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.WorkflowID IN (21,22,23)
				AND WSGP.PersonID = 258
		) D ON D.WorkflowID = EWSGP.WorkflowID
			AND D.WorkflowStepName = EWSGP.WorkflowStepName
			AND D.WorkflowStepGroupName = EWSGP.WorkflowStepGroupName
GROUP BY EWSGP.EntityID, EWSGP.WorkflowStepName, EWSGP.WorkflowStepGroupName, EWSGP.EntityTypeCode, EWSGP.EntityID, EWSGP.WorkflowID, EWSGP.WorkflowStepID, EWSGP.WorkflowStepGroupID, EWSGP.WorkflowName, EWSGP.WorkflowStepNumber, EWSGP.IsComplete
ORDER BY EWSGP.WorkflowID, EWSGP.EntityID, EWSGP.WorkflowStepID, EWSGP.WorkflowStepGroupID


