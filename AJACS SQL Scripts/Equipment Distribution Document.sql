SELECT DE.*
FROM [AJACS].[document].[DocumentEntity] DE
	JOIN document.Document D ON D.DocumentID = DE.DocumentID
		AND D.DocumentName = '9905FA9A-2004-C4F4-F501F7246D43CAD5'

UPDATE DE
SET DE.EntityID = 432, DE.EntityTypeCode = 'EquipmentDistribution'
FROM [AJACS].[document].[DocumentEntity] DE
	JOIN document.Document D ON D.DocumentID = DE.DocumentID
		AND D.DocumentName = '9905FA9A-2004-C4F4-F501F7246D43CAD5'

