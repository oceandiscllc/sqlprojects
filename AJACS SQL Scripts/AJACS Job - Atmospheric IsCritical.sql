DECLARE @nAtmosphericID INT
DECLARE @tOutput TABLE (AtmosphericID INT)

UPDATE T
SET T.IsCritical = 0
OUTPUT INSERTED.AtmosphericID INTO @tOutput
FROM dbo.Atmospheric T
WHERE T.IsCritical = 1
	AND DATEDIFF(d, T.AtmosphericDate, getDate()) > CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey('AtmosphericIsCriticalExpirationDays', '10') AS INT)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT O.AtmosphericID
	FROM @tOutput O
	ORDER BY O.AtmosphericID
	
OPEN oCursor
FETCH oCursor INTO @nAtmosphericID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogAtmosphericAction @nAtmosphericID, 'update', 0, 'System job expired the IsCritical flag'

	FETCH oCursor INTO @nAtmosphericID
	
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor	
GO