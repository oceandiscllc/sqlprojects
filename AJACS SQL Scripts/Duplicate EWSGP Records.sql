USE AJACS
GO

--DELETE D
SELECT *
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY EWSGP.EntityID, EWSGP.WorkflowID, EWSGP.WorkflowStepID, EWSGP.WorkflowStepGroupID, EWSGP.WorkflowStepNumber, EWSGP.PersonID ORDER BY EWSGP.EntityID, EWSGP.WorkflowID, EWSGP.WorkflowStepID, EWSGP.WorkflowStepGroupID, EWSGP.WorkflowStepNumber, EWSGP.PersonID) AS RowIndex,
		EWSGP.EntityID, 
		EWSGP.WorkflowID, 
		EWSGP.WorkflowStepID, 
		EWSGP.WorkflowStepGroupID, 
		EWSGP.WorkflowStepNumber,
		EWSGP.PersonID
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EXISTS
		(
		SELECT 1
		FROM workflow.Workflow W
		WHERE W.WorkflowID = EWSGP.WorkflowID
		)
	) D
WHERE D.RowIndex > 1

/*
DELETE EWSGP
FROM workflow.EntityWorkflowStepGroupPerson EWSGP
WHERE EWSGP.EntityTypeCode = 'ConceptNote'
	AND EWSGP.EntityID IN 
	(
	12,32,34,35,36,37,38,40,41,42,43,44,48,50,53,56,59,64,65,66,69,70,71,72,73,74,82,83,85,86,87,92,95,97,102,108,110,111,114,115,116,117,118,121,123,124,125,126,131,132,133,134,137,138,140,142,144,145,146,147,148,149,
	13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,45,47,49,51,57,58,61,62,63,67,77,79,80,81,84,88,89,90,94,99,100,105,107,113,122,128,129,135,139,141,143
	)
GO

INSERT INTO workflow.EntityWorkflowStepGroupPerson
	(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
SELECT
	'ConceptNote',
	CN.ConceptNoteID,
	W.WorkflowID, 
	WS.WorkflowStepID, 
	WSG.WorkflowStepGroupID, 
	W.WorkflowName, 
	WS.WorkflowStepNumber, 
	WS.WorkflowStepName, 
	WSG.WorkflowStepGroupName, 
	WSGP.PersonID,
	0
FROM dbo.ConceptNote CN, workflow.Workflow W
	JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
	JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
	JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
		AND W.WorkflowID = 22
WHERE CN.ConceptNoteID IN (12,32,34,35,36,37,38,40,41,42,43,44,48,50,53,56,59,64,65,66,69,70,71,72,73,74,82,83,85,86,87,92,95,97,102,108,110,111,114,115,116,117,118,121,123,124,125,126,131,132,133,134,137,138,140,142,144,145,146,147,148,149)
GO

INSERT INTO workflow.EntityWorkflowStepGroupPerson
	(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
SELECT
	'ConceptNote',
	CN.ConceptNoteID,
	W.WorkflowID, 
	WS.WorkflowStepID, 
	WSG.WorkflowStepGroupID, 
	W.WorkflowName, 
	WS.WorkflowStepNumber, 
	WS.WorkflowStepName, 
	WSG.WorkflowStepGroupName, 
	WSGP.PersonID,
	0
FROM dbo.ConceptNote CN, workflow.Workflow W
	JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
	JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
	JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
		AND W.WorkflowID = 23
WHERE CN.ConceptNoteID IN (13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,45,47,49,51,57,58,61,62,63,67,77,79,80,81,84,88,89,90,94,99,100,105,107,113,122,128,129,135,139,141,143)
GO

UPDATE EWSGP
SET EWSGP.IsComplete = 
	CASE
		WHEN EWSGP.EntityID IN (82,97)
			AND EWSGP.WorkflowStepNumber < 3
		THEN 1
		WHEN EWSGP.EntityID IN (115,116,124,148,108)
			AND EWSGP.WorkflowStepNumber < 4
		THEN 1
		WHEN EWSGP.EntityID IN (132,133,56,117,118,137)
			AND EWSGP.WorkflowStepNumber < 5
		THEN 1
		WHEN EWSGP.EntityID IN (85,102,131,134,138,140,142,144,145,146,147,149)
			AND EWSGP.WorkflowStepNumber < 6
		THEN 1
		WHEN EWSGP.EntityID IN (40,71,72,37,43)
			AND EWSGP.WorkflowStepNumber < 7
		THEN 1
		ELSE 0
	END 

FROM workflow.EntityWorkflowStepGroupPerson EWSGP
WHERE EWSGP.EntityTypeCode = 'ConceptNote'
	AND EWSGP.EntityID IN (12,32,34,35,36,37,38,40,41,42,43,44,48,50,53,56,59,64,65,66,69,70,71,72,73,74,82,83,85,86,87,92,95,97,102,108,110,111,114,115,116,117,118,121,123,124,125,126,131,132,133,134,137,138,140,142,144,145,146,147,148,149)
GO

UPDATE EWSGP
SET EWSGP.IsComplete = 
	CASE
		WHEN EWSGP.EntityID IN (89)
			AND EWSGP.WorkflowStepNumber < 2
		THEN 1
		WHEN EWSGP.EntityID IN (26,27,28,29,99)
			AND EWSGP.WorkflowStepNumber < 3
		THEN 1
		WHEN EWSGP.EntityID IN (88)
			AND EWSGP.WorkflowStepNumber < 4
		THEN 1
		WHEN EWSGP.EntityID IN (81)
			AND EWSGP.WorkflowStepNumber < 5
		THEN 1
		WHEN EWSGP.EntityID IN (84,100)
			AND EWSGP.WorkflowStepNumber < 6
		THEN 1
		WHEN EWSGP.EntityID IN (107,135,139,141,143)
			AND EWSGP.WorkflowStepNumber < 7
		THEN 1
		WHEN EWSGP.EntityID IN (61,122,129)
			AND EWSGP.WorkflowStepNumber < 8
		THEN 1
		WHEN EWSGP.EntityID IN (18)
			AND EWSGP.WorkflowStepNumber < 9
		THEN 1
		ELSE 0
	END 

FROM workflow.EntityWorkflowStepGroupPerson EWSGP
WHERE EWSGP.EntityTypeCode = 'ConceptNote'
	AND EWSGP.EntityID IN (13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,45,47,49,51,57,58,61,62,63,67,77,79,80,81,84,88,89,90,94,99,100,105,107,113,122,128,129,135,139,141,143)
GO
*/
