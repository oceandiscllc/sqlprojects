USE AJACS
GO

DECLARE @cSchemaName VARCHAR(50) = 'dbo'
DECLARE @cTableName VARCHAR(50) = 'Contact'

SELECT
	S1.Name AS SchemaName,
	O1.Name AS TableName,
	C1.Name AS ColumnName,
	C1.Is_Nullable AS IsNullable,
	C1.Max_Length AS MaxLength,
	C1.Precision,
	T1.Name AS DataType
FROM sys.objects O1
	JOIN sys.Schemas S1 ON S1.schema_ID = O1.schema_ID
	JOIN sys.Columns C1 ON O1.Object_ID = C1.Object_ID
	JOIN sys.Types T1 ON C1.User_Type_ID = T1.User_Type_ID
		AND S1.Name = @cSchemaName
		AND O1.Name = @cTableName
		AND O1.Type = 'U'
		--AND T1.Name IN ('varbinary', 'geometry','geography')
ORDER BY 3

USE BLLC
GO

DECLARE @cSchemaName VARCHAR(50) = 'contact'
DECLARE @cTableName VARCHAR(50) = 'Contact'

SELECT
	S1.Name AS SchemaName,
	O1.Name AS TableName,
	C1.Name AS ColumnName,
	C1.Is_Nullable AS IsNullable,
	C1.Max_Length AS MaxLength,
	C1.Precision,
	T1.Name AS DataType
FROM sys.objects O1
	JOIN sys.Schemas S1 ON S1.schema_ID = O1.schema_ID
	JOIN sys.Columns C1 ON O1.Object_ID = C1.Object_ID
	JOIN sys.Types T1 ON C1.User_Type_ID = T1.User_Type_ID
		AND S1.Name = @cSchemaName
		AND O1.Name = @cTableName
		AND O1.Type = 'U'
		--AND T1.Name IN ('varbinary', 'geometry','geography')
ORDER BY 3
