USE AJACS
GO

select *
FROM dbo.ConceptNote CN
WHERE CN.ConceptNoteID IN (81)

/*
SELECT 
	CN.ConceptNoteID,
	CN.ConceptNoteTypeCode,
	CN.WorkflowStepNumber,
	CNS.ConceptNoteStatusID,
	CNS.ConceptNoteStatusName
FROM dbo.ConceptNote CN
	JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
WHERE CN.ConceptNoteID IN (81)

SELECT *
FROM workflow.EntityWorkflowStep EWS
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWS.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'ConceptNote'
		AND EWS.EntityID IN (81)
ORDER BY EWS.EntityID, EWS.WorkflowStepID
*/
/*
UPDATE CN
SET 
	CN.WorkflowStepNumber = 7,
	CN.COnceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
	CN.ConceptNoteTypeCode = 'Activity'

FROM dbo.ConceptNote CN
WHERE CN.ConceptNoteID IN (81)

UPDATE EWS
SET EWS.IsComplete = 
	CASE
		WHEN WS.WorkflowStepNumber < 8
		THEN 1
		ELSE 0
	END
FROM workflow.EntityWorkflowStep EWS
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWS.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'ConceptNote'
		AND EWS.EntityID IN (81)
*/
