SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	S.Name + '.' + O.Name
FROM sys.Objects O
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
		AND O.type = 'U' 
		AND O.Name Like '%Risk%'
ORDER BY S.Name, O.Name
