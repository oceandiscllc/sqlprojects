USE AJACS
GO

SELECT * FROM dbo.ContactImport 

UPDATE dbo.Contact 
SET ContactImportID = 0
GO

DELETE 
FROM dbo.ContactImport 
WHERE FirstName IS NULL
GO

DECLARE @cContactAffiliation VARCHAR(250) = 'FSP'
DECLARE @cImportNote VARCHAR(250) = 'BulkImport ' + CAST((YEAR(getDate()) * 100 + Month(getDate())) * 100 + DAY(getDate()) AS CHAR(8)) + ' - ' + @cContactAffiliation
DECLARE @tOutput TABLE (ContactID INT)

DELETE FROM dbo.Contact WHERE Notes = @cImportNote
DELETE CA FROM dbo.ContactContactAffiliation CA WHERE NOT EXISTS (SELECT 1 FROM dbo.Contact C WHERE C.ContactID = CA.ContactID)
DELETE CT FROM dbo.ContactContactType CT WHERE NOT EXISTS (SELECT 1 FROM dbo.Contact C WHERE C.ContactID = CT.ContactID)

INSERT INTO dbo.Contact
	(Address1,Address2,Aliases,ArabicFirstName,ArabicLastName,ArabicMiddleName,ArabicMotherName,CellPhoneNumber,CellPhoneNumberCountryCallingCodeID,CitizenshipCountryID1,CitizenshipCountryID2,City,CommunityID,AssetUnitID,ContactImportID,CountryID,DateOfBirth,DescriptionOfDuties,EmailAddress1,EmailAddress2,EmployerName,EmployerTypeID,FaceBookPageURL,FaxNumber,FaxNumberCountryCallingCodeID,FirstName,Gender,GovernmentIDNumber,GovernmentIDNumberCountryID,IsRegimeDefector,LastName,MiddleName,MotherName,PassportExpirationDate,PassportNumber,PhoneNumber,PhoneNumberCountryCallingCodeID,PlaceOfBirth,PlaceOfBirthCountryID,PostalCode,PreviousDuties,PreviousProfession,PreviousRankOrTitle,PreviousServiceEndDate,PreviousServiceStartDate,PreviousUnit,Profession,ProjectID,SARGMinistryAndUnit,SkypeUserName,StartDate,State,StipendID,Title,ArabicAddress,ArabicPlaceOfBirth,ArabicTitle,Notes)
OUTPUT INSERTED.ContactID INTO @tOutput
SELECT
	CI.Address1,
	CI.Address2,
	CI.Aliases,
	CI.ArabicName,
	CI.ArabicLastName,
	CI.ArabicMiddleName,
	CI.ArabicMotherName,
	CI.CellPhoneNumber,
	ISNULL((SELECT TOP 1 CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC WHERE CCC.CountryCallingCode = CI.CellPhoneNumberCountryCallingCode AND LEN(LTRIM(RTRIM(CI.CellPhoneNumberCountryCallingCode))) > 0), 0) AS CellPhoneNumberCountryCallingCodeID,
	ISNULL((SELECT C.CountryID FROM dropdown.Country C WHERE C.CountryName = CI.CitizenshipCountryID1), 0) AS CitizenshipCountryID1,
	ISNULL((SELECT C.CountryID FROM dropdown.Country C WHERE C.CountryName = CI.CitizenshipCountryID2), 0) AS CitizenshipCountryID2,
	CI.City,
	ISNULL((SELECT TOP 1 C.CommunityID FROM dbo.Community C WHERE C.CommunityName = CI.Community), 0) AS CommunityID,
	ISNULL((SELECT TOP 1 AU.AssetUnitID FROM asset.AssetUnit AU WHERE AU.AssetUnitName LIKE '%' + CI.CommunityAssetUnit + '%'), 0) AS AssetUnitID,
	CI.ContactImportID,
	ISNULL((SELECT C.CountryID FROM dropdown.Country C WHERE C.CountryName = CI.Country),0) AS CountryID,
	CASE WHEN ISDATE(CI.DateOfBirth) = 0 THEN NULL ELSE CI.DateOfBirth END AS DateOfBirth,
	CI.DescriptionOfDuties,
	CI.EmailAddress1,
	CI.EmailAddress2,
	CI.EmployerName,
	ISNULL((SELECT ET.EmployerTypeID FROM dropdown.EmployerType ET WHERE ET.EmployerTypeName = CI.EmployerType),0) AS EmployerTypeID,
	CI.FaceBookPageURL,
	CI.FaxNumber,
	ISNULL((SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC WHERE CCC.CountryCallingCode = CI.FaxNumberCountryCallingCode),0) AS FaxNumberCountryCallingCodeID,
	CI.FirstName,
	IIF (CI.Gender NOT IN ('M', 'Male'), 'Female', 'Male') AS Gender,
	CI.GovernmentIDNumber,
	ISNULL((SELECT C.CountryID FROM dropdown.Country C WHERE C.CountryName = CI.GovernmentIDNumberCountry),0) AS GovernmentIDNumberCountryID,
	IIF (CI.IsRegimeDefector = 'Yes', 1, 0),
	CI.LastName,
	CI.MiddleName,
	CI.MotherName,
	CASE WHEN ISDATE(CI.PassportExpirationDate) = 0 THEN NULL ELSE CI.PassportExpirationDate END AS PassportExpirationDate,
	CI.PassportNumber,
	CI.PhoneNumber,
	CASE WHEN ISNUMERIC(CI.PhoneNumberCountryCallingCode) = 1 THEN ISNULL((SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC WHERE CCC.CountryCallingCode = CI.PhoneNumberCountryCallingCode),0) ELSE 0 END AS PhoneNumberCountryCallingCodeID,
	CI.PlaceOfBirth,
	ISNULL((SELECT C.CountryID FROM dropdown.Country C WHERE C.CountryName = CI.PlaceOfBirthCountry), 0),
	CI.PostalCode,
	CI.PreviousDuties,
	CI.PreviousProfession,
	CI.PreviousRankOrTitle,
	CASE WHEN ISDATE(CI.PreviousServiceEndDate) = 0 THEN NULL ELSE CI.PreviousServiceEndDate END,
	CASE WHEN ISDATE(CI.PreviousServiceStartDate) = 0 THEN NULL ELSE CI.PreviousServiceStartDate END,
	CI.PreviousUnit,
	CI.Profession,
	ISNULL((SELECT P.ProjectID FROM dropdown.Project P WHERE P.ProjectName = CI.Project),0) AS ProjectID,
	CI.SARGMinistryAndUnit,
	CI.SkypeUserName,
	CASE WHEN ISDATE(CI.StartDate) = 0 THEN NULL ELSE CI.StartDate END,
	CI.State,
	ISNULL((SELECT S.StipendID FROM dropdown.Stipend S WHERE S.StipendName = CI.Stipend),0) AS StipendID,
	CI.Title,
	CI.ArabicAddress,
	CI.ArabicPlaceOfBirth,
	CI.ArabicTitle,
	@cImportNote
FROM dbo.ContactImport CI
WHERE CI.ContactAffiliation = @cContactAffiliation

IF EXISTS (SELECT * FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = @cContactAffiliation)
	BEGIN

	INSERT INTO dbo.ContactContactAffiliation 
		(ContactID, ContactAffiliationID)
	SELECT
		O.ContactID,
		(SELECT CA.ContactAffiliationID FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = @cContactAffiliation)
	FROM @tOutput O

	END
--ENDIF

IF @cContactAffiliation IN ('FSP', 'JusticeStipend')
	BEGIN

	INSERT INTO dbo.ContactContactType 
		(ContactID, ContactTypeID)
	SELECT
		O.ContactID,
		CASE
			WHEN @cContactAffiliation = 'FSP'
			THEN (SELECT CT.ContactTypeID FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Police Stipend')
			WHEN @cContactAffiliation = 'JusticeStipend'
			THEN (SELECT CT.ContactTypeID FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Justice Stipend')
			ELSE 0
		END
	FROM @tOutput O

	END
--ENDIF

	--INSERT INTO dbo.ContactContactType 
	--	(ContactID, ContactTypeID)
	--SELECT
	--	O.ContactID,
	--	(SELECT CT.ContactTypeID FROM dropdown.ContactType CT WHERE CT.ContactTypeCode = 'PartnersStakeholder')
	--FROM @tOutput O
