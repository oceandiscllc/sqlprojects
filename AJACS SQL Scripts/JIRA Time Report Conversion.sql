USE TODD
GO

DECLARE @cTicketsToExclude VARCHAR(MAX) = 'AJACS-587,AJACS-735,AJACS-736,AJACS-737,AJACS-662,AJACS-765,AJACS-766,AJACS-767,AJACS-768'

UPDATE TL
SET
	TL.Project = LEFT(TL.Issue, CHARINDEX('-', Issue) - 1),
	TL.HoursSpent = 
		CASE
			WHEN CHARINDEX('h', TimeSpent) = 0
			THEN 0
			ELSE CAST(LEFT(TimeSpent, CHARINDEX('h', TimeSpent) - 1) AS INT)
		END,
	TL.MinutesSpent = 
		CASE
			WHEN CHARINDEX('h', TimeSpent) = 0
			THEN CAST(REPLACE(TimeSpent, 'm', '') AS INT)
			ELSE CAST(REPLACE(STUFF(TimeSpent, 1, CHARINDEX('h', TimeSpent), ''), 'm', '') AS INT)
		END

FROM dbo.TimeLog TL

UPDATE TL
SET TL.TotalSpent = CAST(TL.HoursSpent AS NUMERIC(18,2)) + CAST(TL.MinutesSpent AS NUMERIC(18,2)) / 60
FROM dbo.TimeLog TL

UPDATE TL
SET TL.IsBillable = 0
FROM dbo.TimeLog TL
WHERE EXISTS
	(
	SELECT 1
	FROM dbo.ListToTable(@cTicketsToExclude, ',') LTT
	WHERE LTT.ListItem = TL.Issue
	)

SELECT * FROM TimeLog ORDER BY Issue

SELECT
	A.Project,
	A.BillableTime,
	B.TotalTime,
	B.TotalTime - A.BillableTime AS LostTime
FROM
		(
		SELECT 
			TL.Project,
			SUM(TL.TotalSpent) AS BillableTime
		FROM dbo.TimeLog TL
		WHERE TL.IsBillable = 1
		GROUP BY TL.Project
		) A JOIN
			(
			SELECT 
				TL.Project,
				SUM(TL.TotalSpent) AS TotalTime
			FROM dbo.TimeLog TL
			GROUP BY TL.Project
			) B ON B.Project = A.Project
ORDER BY A.Project