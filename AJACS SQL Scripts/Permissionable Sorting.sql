USE [AJACS]
GO

/*
select * from permissionable.Permissionable P order by 9
select * from permissionable.Permissionable P where P.PermissionableCode LIKE 'WorkflowStepID__' AND P.PermissionableLineage LIKE 'ConceptNote.AddUpdate%' order by P.PermissionableLineage
SELECT * FROM permissionable.DisplayGroup
SELECT * FROM permissionable.DisplayGroupPermissionable order by 2
GO
[procurement].[CommunityEquipmentInventoryAudit]
UPDATE permissionable.Permissionable SET MethodName = 'ListDistributedInventory' WHERE PermissionableID = 1538

insert into permissionable.Permissionable 
	(ControllerName, MethodName)
VALUES
	('EquipmentDistribution','SetDeliveryDate')

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO

*/

	DECLARE @nPadLength INT
	
	SELECT @nPadLength = LEN(CAST(COUNT(P.PermissionableID) AS VARCHAR(50)))
	FROM permissionable.Permissionable P
	
	;
	WITH HD (DisplayIndex,PermissionableID,PermissionableClass,ParentPermissionableID,DisplayGroupID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY DG.DisplayOrder, P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			'perm-' + CAST(P.PermissionableID AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			DG.DisplayGroupID,
			1
		FROM permissionable.Permissionable P
			JOIN permissionable.DisplayGroupPermissionable DGP ON DGP.PermissionableID = P.PermissionableID
			JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = DGP.DisplayGroupID
		WHERE P.ParentPermissionableID = 0
	
		UNION ALL
	
		SELECT
			CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			CAST(HD.PermissionableClass + ' perm-' + CAST(P.PermissionableID AS VARCHAR(10)) AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			HD.DisplayGroupID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM permissionable.Permissionable P
			JOIN HD ON HD.PermissionableID = P.ParentPermissionableID
		)
	
	SELECT
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentPermissionableID,
		HD1.PermissionableID,
		'group-' + CAST(DG.DisplayGroupID AS VARCHAR(10)) + ' ' + RTRIM(REPLACE(HD1.PermissionableClass, 'perm-' + CAST(HD1.PermissionableID AS VARCHAR(10)), '')) AS PermissionableClass,
		HD1.DisplayGroupID,
		DG.DisplayGroupName,
		P.PermissionableCode,
		P.PermissionableName,
		P.PermissionableLineage,
	
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentPermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasChildren,
		
		CASE
			WHEN 17 > 0 AND EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PP.PermissionableLineage AND PP.PersonID = 17 AND P.PermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
	
	FROM HD HD1
		JOIN permissionable.Permissionable P ON P.PermissionableID = HD1.PermissionableID
		JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = HD1.DisplayGroupID
	ORDER BY HD1.DisplayIndex
