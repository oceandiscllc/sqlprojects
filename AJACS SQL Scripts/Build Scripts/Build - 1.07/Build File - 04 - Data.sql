USE AJACS
GO

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.DropColumn @TableName, 'DisplayGroupID'
GO

DELETE FROM permissionable.Permissionable WHERE PermissionableLineage = 'Monitoring'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'RequestForInformation.Add')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder, IsGlobal, IsWorkflow)
	SELECT
		P.ParentPermissionableID, 
		'Add',
		'Add',
		'RequestForInformation.Add',
		P.DisplayOrder,
		P.IsGlobal,
		P.IsWorkflow
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'RequestForInformation.List'

	END
--ENDIF
GO

UPDATE P
SET 
	P.PermissionableCode = 'Update',
	P.PermissionableName = 'Edit',
	P.PermissionableLineage = 'RequestForInformation.Update'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'RequestForInformation.AddUpdate'
GO

UPDATE P
SET DisplayOrder = 
	CASE
		WHEN P.PermissionableLineage IN ('Objective.AddUpdate','Person.AddUpdate','RequestForInformation.Add')
		THEN 1
		WHEN P.PermissionableLineage IN ('Objective.View','Person.Delete','RequestForInformation.Update')
		THEN 2
		WHEN P.PermissionableLineage IN ('Objective.List','Person.View','RequestForInformation.View')
		THEN 3
		WHEN P.PermissionableLineage IN ('Person.List','RequestForInformation.List')
		THEN 4
	END		

FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN ('Objective.AddUpdate','Objective.List','Objective.View','Person.AddUpdate','Person.Delete','Person.List','Person.View','RequestForInformation.Add','RequestForInformation.List','RequestForInformation.Update','RequestForInformation.View')
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
UPDATE PP
SET PP.PermissionableLineage = 'RequestForInformation.Add'
FROM permissionable.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'RequestForInformation.AddUpdate'
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO
--End table permissionable.PersonPermissionable

--Begin Permissions Assignment
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (69,17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (69,17,26)

	END
--ENDIF
GO
--End Permissions Assignment



