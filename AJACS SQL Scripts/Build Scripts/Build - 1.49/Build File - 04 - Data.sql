USE AJACS
GO

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Contact.View.CSWGMember')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID,PermissionableCode,PermissionableName,DisplayOrder)
	SELECT
		P.PermissionableID,
		'CSWGMemberVetting',
		'View CSWG Member',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.View'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Contact.List.UpdateVetting')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID,PermissionableCode,PermissionableName,DisplayOrder)
	SELECT
		P.PermissionableID,
		'UpdateVetting',
		'Update Vetting',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.List'
	
	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveD',
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveD',
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
