USE AJACS
GO

--Begin procedure dbo.GetCommunities
EXEC Utility.DropObject 'dbo.GetCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.15
-- Description:	A stored procedure to get data from the dbo.Community table
--
-- Author:			Greg Yingling
-- Create Date: 2016.02.28
-- Description:	Added IsActive Consideration
-- ========================================================================
CREATE PROCEDURE dbo.GetCommunities

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID 
			AND C.IsActive = 1
	ORDER BY C.CommunityName

END
GO
--End procedure dbo.GetCommunities

--Begin dbo.GetDocumentByPhysicalFileName
EXEC Utility.DropObject 'dbo.GetDocumentByPhysicalFileName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dbo.Document table
-- ==========================================================================
CREATE PROCEDURE dbo.GetDocumentByPhysicalFileName

@PhysicalFileName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentSubType,
		D.ContentType,
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.PhysicalFileSize
	FROM dbo.Document D
	WHERE D.PhysicalFileName = @PhysicalFileName

END
GO
--End dbo.GetDocumentByPhysicalFileName

--Begin procedure dbo.GetEmailAddressesByPermissionableLineage
EXEC Utility.DropObject 'dbo.GetEmailAddressesByPermissionableLineage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to get data from the dbo.Person table
--
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	Added distinct and list support
-- =====================================================================
CREATE PROCEDURE dbo.GetEmailAddressesByPermissionableLineage

@PermissionableLineage VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		P.EmailAddress,
		P.PersonID
	FROM permissionable.PersonPermissionable PP
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ListToTable(@PermissionableLineage, ',') LTT
				WHERE LTT.ListItem = PP.PermissionableLineage
				)
			AND P.PersonID <> @PersonID
			AND P.CanReceiveEmail = 1
	ORDER BY P.EmailAddress
		
END
GO
--End procedure dbo.GetEmailAddressesByPermissionableLineage

--Begin procedure dbo.GetEmailAddressesByRoleName
EXEC Utility.DropObject 'dbo.GetEmailAddressesByRoleName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A stored procedure to get data from the dbo.Person table
-- =====================================================================
CREATE PROCEDURE dbo.GetEmailAddressesByRoleName

@RoleName VARCHAR(50),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.PersonID
	FROM dbo.Person P 
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND R.RoleName = @RoleName
			AND P.PersonID <> @PersonID
			AND P.CanReceiveEmail = 1
	ORDER BY P.EmailAddress
		
END
GO
--End procedure dbo.GetEmailAddressesByRoleName

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Added the IsAccountLockedOut bit
--
-- Author:			John Lyons
-- Create date:	2015.07.29
-- Description:	Added two factor support
-- ============================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.FirstName, 
		P.IsAccountLockedOut,
		P.LastName,
		P.Organization,
		P.PersonID,
		P.RoleID, 
		P.Title,
		P.UserName,
		R.RoleName,
		P.Phone, 
		P.CountryCallingCodeID,
		P.IsPhoneVerified,
		P.CanReceiveEmail
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.GetPersonEmailAddressesByRoleName
EXEC Utility.DropObject 'dbo.GetPersonEmailAddressesByRoleName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to return person email addresses from the dbo.Person table based on a role name
-- ===============================================================================================================
CREATE PROCEDURE dbo.GetPersonEmailAddressesByRoleName

@RoleName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND R.RoleName = @RoleName
			AND P.CanReceiveEmail = 1
	ORDER BY P.EmailAddress

END
GO
--End procedure dbo.GetPersonEmailAddressesByRoleName

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:		Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), 0) AS ThumbnailLength,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.WeeklyReportID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O
		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.WorkflowStepNumber,
		WR.SummaryMapZoom 
	FROM weeklyreport.WeeklyReport WR

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'WeeklyReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nWeeklyReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'WeeklyReport'
			AND WSWA.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @nWeeklyReportID)
	ORDER BY WSWA.DisplayOrder

	SELECT
		C.CommunityID,
		C.CommunityName,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID,
		ID.ImpactDecisionName,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		C2.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunity SMC WHERE SMC.CommunityID = C.CommunityID AND SMC.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM weeklyreport.Community C
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Community C2 ON C2.CommunityID = C.CommunityID
			 AND C.WeeklyReportID = @nWeeklyReportID

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
			AND CAT.CommunityAssetTypeID = 1
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor,
		'/assets/img/icons/' + REPLACE(ZT.HexColor, '#', '') + '.png' AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CAT.CommunityAssetTypeID = 2
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapIncident SMI WHERE SMI.IncidentID = I.IncidentID AND SMI.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (
				EXISTS	(
					SELECT 1
					FROM dbo.IncidentCommunity IC
						JOIN weeklyreport.Community C ON C.CommunityID = IC.CommunityID
							AND IC.IncidentID = I.IncidentID
							AND C.WeeklyReportID = @nWeeklyReportID
				)
				OR
				EXISTS (
					SELECT 1
					FROM dbo.IncidentProvince IP
						JOIN weeklyreport.Province P ON P.ProvinceID = IP.ProvinceID
							AND IP.IncidentID = I.IncidentID
							AND P.WeeklyReportID = @nWeeklyReportID
				)
			)		

	SELECT
		F.ForceID,
		F.ForceName,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor,
		'/assets/img/icons/' + REPLACE(AOT.HexColor, '#', '') + '.png' AS Icon,
		F.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapForce SMF WHERE SMF.ForceID = F.ForceID AND SMF.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND EXISTS
				(
					SELECT 1
					FROM force.ForceCommunity FC
						JOIN weeklyreport.Community C ON C.CommunityID = FC.CommunityID
							AND FC.ForceID = F.ForceID
							AND C.WeeklyReportID = @nWeeklyReportID
				)

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A stored procedure to people associated with the workflow steps on a communityprovinceengagement update
-- ====================================================================================================================
CREATE PROCEDURE workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT CPEU.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU WHERE CPEU.CommunityProvinceEngagementUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT CPEU.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU WHERE CPEU.CommunityProvinceEngagementUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'CommunityProvinceEngagementUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople

--Begin procedure workflow.GetConceptNoteWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetConceptNoteWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A stored procedure to people associated with the workflow steps on a concept note
-- ==============================================================================================
CREATE PROCEDURE workflow.GetConceptNoteWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'ConceptNote.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetConceptNoteWorkflowStepPeople

--Begin procedure workflow.GetEquipmentDistributionPlanWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetEquipmentDistributionPlanWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Kevin Ross
-- Create date:	2015.07.01
-- Description:	A stored procedure to return workflow data
-- =======================================================
CREATE PROCEDURE workflow.GetEquipmentDistributionPlanWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'EquipmentDistributionPlan'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'EquipmentDistributionPlan.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetEquipmentDistributionPlanWorkflowStepPeople

--Begin procedure workflow.GetFIFUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetFIFUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to people associated with the workflow steps on a fif update
-- ============================================================================================
CREATE PROCEDURE workflow.GetFIFUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT FU.WorkflowStepNumber FROM fifupdate.FIFUpdate FU WHERE FU.FIFUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT FU.WorkflowStepNumber FROM fifupdate.FIFUpdate FU WHERE FU.FIFUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'FIFUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetFIFUpdateWorkflowStepPeople

--Begin procedure workflow.GetJusticeUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetJusticeUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2016.02.13
-- Description:	A stored procedure to get the people in a justice update workflow step
-- ===================================================================================
CREATE PROCEDURE workflow.GetJusticeUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'JusticeUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT FU.WorkflowStepNumber FROM justiceupdate.JusticeUpdate FU WHERE FU.JusticeUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT FU.WorkflowStepNumber FROM justiceupdate.JusticeUpdate FU WHERE FU.JusticeUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'JusticeUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetJusticeUpdateWorkflowStepPeople

--Begin procedure workflow.GetPoliceEngagementUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetPoliceEngagementUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A stored procedure to people associated with the workflow steps on a policeengagement update
-- =========================================================================================================
CREATE PROCEDURE workflow.GetPoliceEngagementUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'PoliceEngagementUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT CPEU.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate CPEU WHERE CPEU.PoliceEngagementUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT CPEU.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate CPEU WHERE CPEU.PoliceEngagementUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'PoliceEngagementUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetPoliceEngagementUpdateWorkflowStepPeople

--Begin procedure workflow.GetProgramReportWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetProgramReportWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A stored procedure to people associated with the workflow steps on a Program report
-- ================================================================================================
CREATE PROCEDURE workflow.GetProgramReportWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ProgramReport'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT WR.WorkflowStepNumber FROM weeklyreport.ProgramReport WR WHERE WR.ProgramReportID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.ProgramReport WR WHERE WR.ProgramReportID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'ProgramReport.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetProgramReportWorkflowStepPeople

--Begin procedure workflow.GetRecommendationUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetRecommendationUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.25
-- Description:	A stored procedure to people associated with the workflow steps on a recommendation update
-- =======================================================================================================
CREATE PROCEDURE workflow.GetRecommendationUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'Recommendation.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetRecommendationUpdateWorkflowStepPeople

--Begin procedure workflow.GetRiskUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetRiskUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A stored procedure to people associated with the workflow steps on a risk update
-- =============================================================================================
CREATE PROCEDURE workflow.GetRiskUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'Risk.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetRiskUpdateWorkflowStepPeople

--Begin procedure workflow.GetSpotReportWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetSpotReportWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A stored procedure to people associated with the workflow steps on a spot report
-- =============================================================================================
CREATE PROCEDURE workflow.GetSpotReportWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'SpotReport.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetSpotReportWorkflowStepPeople

--Begin procedure workflow.GetWeeklyReportWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetWeeklyReportWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A stored procedure to people associated with the workflow steps on a Weekly report
-- =============================================================================================
CREATE PROCEDURE workflow.GetWeeklyReportWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0,
@CanReceiveEmail BIT = null

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'WeeklyReport.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND (@CanReceiveEmail IS NULL OR P.CanReceiveEmail = @CanReceiveEmail)
	ORDER BY FullName

END
GO
--End procedure workflow.GetWeeklyReportWorkflowStepPeople
