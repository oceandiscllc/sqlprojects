USE AJACS
GO

--Begin table dbo.Person
DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC utility.AddColumn @TableName, 'CanReceiveEmail', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'CanReceiveEmail', 'BIT', 1
GO
--End table dbo.Person

--Begin table dbo.SpotReport
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReport'

EXEC utility.AddColumn @TableName, 'SummaryMapZoom', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'SummaryMapZoom', 'INT', 0
GO
--End table dbo.SpotReport

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'QuantityDistributed', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'QuantityDistributed', 'INT', 0
GO
--End table procurement.EquipmentInventory

--Begin table weeklyreport.WeeklyReport
DECLARE @TableName VARCHAR(250) = 'weeklyreport.WeeklyReport'

EXEC utility.AddColumn @TableName, 'SummaryMapZoom', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'SummaryMapZoom', 'INT', 0
GO
--End table weeklyreport.WeeklyReport
