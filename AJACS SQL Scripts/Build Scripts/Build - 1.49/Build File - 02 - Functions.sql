USE AJACS
GO

--Begin function procurement.GetEquipmentDistributionEquipmentInventoryQuantityAvailable
EXEC utility.DropObject 'procurement.GetEquipmentDistributionEquipmentInventoryQuantityAvailable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.28
-- Description:	A function to get the quantity available for a EquipmentDistributionEquipmentInventory item
-- ========================================================================================================

CREATE FUNCTION procurement.GetEquipmentDistributionEquipmentInventoryQuantityAvailable
(
@Quantity INT,
@EquipmentInventoryID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0
	
	IF @Quantity = 0
		BEGIN

		SELECT @nReturn = EI.Quantity - 
											EI.QuantityDistributed - 
											(SELECT SUM(EDEI.Quantity) FROM procurement.EquipmentDistributionEquipmentInventory EDEI WHERE EDEI.EquipmentInventoryID = @EquipmentInventoryID)
		FROM procurement.EquipmentInventory EI
		WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
		END
	--ENDIF
	
	RETURN @nReturn
END
GO
--End function procurement.GetEquipmentDistributionEquipmentInventoryQuantityAvailable
