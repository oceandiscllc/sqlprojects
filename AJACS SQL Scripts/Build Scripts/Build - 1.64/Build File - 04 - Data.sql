USE AJACS
GO

EXEC utility.EntityTypeAddUpdate 'Vetting', 'Vetting', NULL
GO

UPDATE dbo.EmailTemplateField
SET DisplayOrder = 0
GO

EXEC Utility.EmailTemplateFieldAddUpdate 'ConceptNote', '[[ContactIDList]]', 'List of selected contact ID''s'
EXEC Utility.EmailTemplateFieldAddUpdate 'ConceptNote', '[[FirstName]]', 'Submitter First Name'
EXEC Utility.EmailTemplateFieldAddUpdate 'ConceptNote', '[[LastName]]', 'Submitter Last Name'
EXEC Utility.EmailTemplateFieldAddUpdate 'Vetting', '[[ContactIDList]]', 'List of selected contact ID''s'
EXEC Utility.EmailTemplateFieldAddUpdate 'Vetting', '[[FirstName]]', 'Submitter First Name'
EXEC Utility.EmailTemplateFieldAddUpdate 'Vetting', '[[LastName]]', 'Submitter Last Name'
GO

UPDATE dbo.EmailTemplateField
SET PlaceHolderText = REPLACE(PlaceHolderText, '[[', '[[Submitter')
WHERE PlaceHolderDescription LIKE 'Submitter%' 
	AND PlaceHolderText NOT LIKE '[[Submitter%'
GO

UPDATE dropdown.ImpactDecision
SET IsActive = 0
GO

UPDATE ID
SET 
	ID.ImpactDecisionName = 'No',
	ID.IsActive = 1
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName IN ('No Engagement', 'No')
GO

UPDATE ID
SET 
	ID.ImpactDecisionName = 'Under Review',
	ID.IsActive = 1
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName IN ('Engagement with Caution', 'Under Review')
GO

UPDATE ID
SET 
	ID.ImpactDecisionName = 'Yes',
	ID.IsActive = 1
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName IN ('Engage', 'Yes')
GO
