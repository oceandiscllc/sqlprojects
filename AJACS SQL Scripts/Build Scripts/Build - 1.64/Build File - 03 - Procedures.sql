USE AJACS
GO

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID,
		C.CommunityName,
		C.ImpactDecisionID,
		C.PolicePresenceCategoryID,
		C.Population,
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CR.ActivitiesOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.ActivityImplementationEndDate,
		dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
		CR.ActivityImplementationStartDate,
		dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
		CR.ApprovedActivityCount,
		CR.AreaManagerPersonID,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		CR.CommunityCode,
		CR.CommunityID,
		CR.CommunityRoundCivilDefenseCoverageID,
		CR.CommunityRoundGroupID,
		CR.CommunityRoundID,
		CR.CommunityRoundJusticeActivityID,
		CR.CommunityRoundOutput11StatusID,
		CR.CommunityRoundOutput12StatusID,
		CR.CommunityRoundOutput13StatusID,
		CR.CommunityRoundOutput14StatusID,
		CR.CommunityRoundPreAssessmentStatusID,
		CR.CommunityRoundRAPInfoID,
		CR.CommunityRoundRoundID,
		CR.CommunityRoundTamkeenID,
		CR.CSAP2PeerReviewMeetingDate,
		dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
		CR.CSAP2SubmittedDate,
		dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
		CR.CSAPBeginDevelopmentDate,
		dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
		CR.CSAPCommunityAnnouncementDate,
		dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
		CR.CSAPFinalizedDate,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		CR.CSAPProcessStartDate,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		CR.FemaleQuestionnairCount,
		CR.FieldFinanceOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerContactName,
		CR.FieldLogisticOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerContactName,
		CR.FieldOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficerContactName,
		CR.FinalSWOTDate,
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
		CR.InitialSCADate,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
		CR.IsActive,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.KickoffMeetingCount,
		CR.MOUDate,
		dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
		CR.NewCommunityEngagementCluster,
		CR.Preassessment,
		CR.Postassessment,
		CR.ProjectOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		CR.QuestionnairCount,
		CR.SCAFinalizedDate,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
		CR.SCAMeetingCount,
		CR.SensitizationDefineFSPRoleDate,
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
		CR.SensitizationDiscussCommMakeupDate,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
		CR.SensitizationMeetingMOUDate,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
		CR.ToRDate,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		FOHT.FieldOfficerHiredTypeID,
		FOHT.FieldOfficerHiredTypeName,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		CRES.CommunityRoundEngagementStatusID,
		CRES.CommunityRoundEngagementStatusName,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Male'  
		) AS CSWGMaleCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Female' 
		) AS CSWGFemaleCount
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.FieldOfficerHiredType FOHT ON FOHT.FieldOfficerHiredTypeID = CR.FieldOfficerHiredTypeID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
			AND CR.CommunityRoundID = @CommunityRoundID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityRound'
			AND DE.EntityID = @CommunityRoundID
			AND D.DocumentDescription IN ('RAP Report','TOR Document', 'Key Achievement', 'Finalized SCA', 'Finalized CSAP')
	ORDER BY D.DocumentDescription

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		C.EmployerName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,
		ISNULL(OACV12.VettingOutcomeName, 'Not Vetted') AS USVettingOutcomeName,
		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,
		ISNULL(OACV22.VettingOutcomeName, 'Not Vetted') AS UKVettingOutcomeName,
		CCSWGC.ContactCSWGClassificationID,
		CCSWGC.ContactCSWGClassificationName
	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = CV0.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C1.ContactID
					AND CRC.CommunityRoundID = @CommunityRoundID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT 
				CV12.VettingOutcomeID,
				VO1.VettingOutcomeName
			FROM dbo.ContactVetting CV12
			JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT 
				CV22.VettingOutcomeID,
				VO2.VettingOutcomeName
			FROM dbo.ContactVetting CV22
			JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
	ORDER BY 3, 1

	--Updates
	SELECT
		CRU.CommunityRoundUpdateID, 
		CRU.CommunityRoundID, 
		CRU.FOUpdates, 
		CRU.POUpdates, 
		CRU.NextStepsUpdates, 
		CRU.NextStepsProcess, 
		CRU.Risks, 
		CRU.UpdateDate,
		dbo.FormatDate(CRU.UpdateDate) AS UpdateDateFormatted
	FROM dbo.CommunityRoundUpdate CRU
	WHERE CRU.CommunityRoundID = @CommunityRoundID
	ORDER BY CRU.UpdateDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure permissionable.CheckEmailForCanRecieveEmail
EXEC utility.DropObject 'permissionable.CheckEmailForCanRecieveEmail'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:	Eric Jones
-- Create date: 2016.07.11
-- Description:	A stored procedure which takes an email and finds out if the person associated with that email can receive emails from the system.
-- ===========================================================================
CREATE PROCEDURE permissionable.CheckEmailForCanRecieveEmail

@PersonEmailAddress VARCHAR(320)

AS
BEGIN

	SELECT 1
		FROM permissionable.PersonPermissionable PP
	JOIN Person P on p.PersonID = PP.PersonID
		WHERE P.EmailAddress = @PersonEmailAddress
			AND PP.PermissionableLineage = 'Main.CanRecieveEmail.CanRecieveEmail'
		
END
GO
--End permissionable dbo.CheckEmailForCanRecieveEmail

--Begin procedure reporting.GetCommunityRoundList
EXEC utility.DropObject 'reporting.GetCommunityRoundList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:		Justin Branum
-- Create date:	2016.05.29
-- Description:	A stored procedure to get data from the dbo.CommunityRound table
-- =============================================================================

CREATE PROCEDURE reporting.GetCommunityRoundList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.communityName,
		C.Population,
		CES.CommunityEngagementStatusName,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.CommunityCode,
		CR.CommunityRoundID,
		CR.CommunityRoundRoundID,
		CR.FemaleQuestionnairCount,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficer,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC AS IsFieldOfficerIntroToFSPandLA,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.NewCommunityEngagementCluster,
		CR.PostAssessment,
		CR.PreAssessment,
		CR.QuestionnairCount,
		CR.SCAMeetingCount,
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRES.CommunityRoundEngagementStatusName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusNam,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		case when CR.CSAPFinalizedDate is null then 'No' else 'Yes' end as 'FinalizedCSAP', 
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted,
		case when CR.SCAFinalizedDate is null then 'No' else 'Yes' end as 'FinalizedSCA',
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		ID.ImpactDecisionName,
		P.provinceName,
		PPC.PolicePresenceCategoryName,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
			AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Male' 
		) AS CSWGMaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Female' 
		) AS CSWGFemaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.USVettingExpirationDate > GetDate()
		) AS CSWGRAMActiveCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.UKVettingExpirationDate  > GetDate()
		) AS CSWGUKActiveCount,
		dbo.FormatDate(
		(
		SELECT MAX(UpdateDate)
		FROM dbo.CommunityRoundUpdate 
		WHERE CommunityRoundID = CR.CommunityRoundID
		)) AS CurrentUpdateDateFormatted,
		(
		SELECT Top 1 
		FOUpdates +' <br>' + POUpdates+' <br>' + NextStepsUpdates +' <br>' +NextStepsProcess +' <br>' + Risks
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateData,
			dbo.FormatDate(
			(
				SELECT MAX(UpdateDate)
				FROM dbo.CommunityRoundUpdate 
				WHERE CommunityRoundID = CR.CommunityRoundID
				AND UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
			)) AS LastWeekUpdateDateFormatted
			,(SELECT Top 1 
				FOUpdates +' <br>' + POUpdates+' <br>' + NextStepsUpdates+' <br>' +NextStepsProcess+' <br>' + Risks
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateData,
		(
		SELECT Top 1 
		FOUpdates
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateFOUpdates,
		(
		SELECT Top 1 
		POUpdates
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdatePOUpdates,
		(
		SELECT Top 1 
		NextStepsUpdates +' <br>' +NextStepsProcess
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateNextStepsUpdates,
		(
		SELECT Top 1 
		Risks
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateRisk,
		(SELECT Top 1 
				FOUpdates
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateFOUpdates,
		(SELECT Top 1 
				POUpdates
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdatePOUpdates,
		(SELECT Top 1 
				NextStepsUpdates +' <br>' +NextStepsProcess
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateNextSteps,
		(SELECT Top 1 
				Risks
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateRisks
	FROM dbo.CommunityRound CR
		JOIN reporting.SearchResult RSR ON RSR.EntityID = CR.communityroundID
				AND RSR.EntityTypeCode = 'CommunityRound'
				AND RSR.PersonID = @PersonID
		JOIN dbo.community C ON C.communityID = CR.communityID
		JOIN dbo.province P ON P.provinceID = C.provinceID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityRoundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID	
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID

END
GO
--End procedure reporting.GetCommunityRoundList

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes
@PersonID INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	 (SELECT TOP 1 WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote') AS WorkflowType,
	 CNS.ConceptNoteStatusCode,
	 CN.WorkflowStepNumber,


		CN.ConceptNoteID,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,
		CN.TaskCode,
		
		CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
							ELSE 'Closedown'
						END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
							ELSE 'Closedown'
						END
				END

			ELSE
			'Bad WorkFlow'

		END AS Status,

		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1
			SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,

		CASE 
			WHEN 
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			
			
			 IN ('Cancelled','Development','Closed','Closedown')	
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			WHEN CN.TaskCode ='Closed'						
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			ELSE FORMAT((IsNull(OACNB.TotalBudget,0)+ IsNull(OACNA.ConceptNoteAmmendmentTotal,0) + isnull(OACNCE.ConceptNoteEquimentTotal, 0)), 'C', 'en-us') 
		END AS TotalBudget,
		
		CASE 
			WHEN 
			
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			
			 IN ('Cancelled','Development')	
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us') 
		END AS TotalSpent,
		
		CASE 
			WHEN 
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			 IN ('Closed','Cancelled','OnHold','Development','Closedown')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT(((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmmendmentTotal,0)+ isnull(OACNCE.ConceptNoteEquimentTotal, 0)) - (IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0))), 'C', 'en-us') 
		END AS TotalRemaining

	FROM dbo.ConceptNote CN
		JOIN Reporting.SearchResult SR ON SR.EntityID = CN.ConceptNoteID AND SR.EntityTypeCode='ConceptNote' AND SR.PersonID = @PersonID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB
		OUTER APPLY
			(
			SELECT
				SUM(CNF.drAmt) - SUM(CNF.CrAmt) as ConceptNoteFinanceTotal
			FROM dbo.ConceptNoteFinance CNF
			WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
			) OACNF
		OUTER APPLY
			(
			SELECT
				SUM(CNA.Cost) as ConceptNoteAmmendmentTotal
			FROM dbo.ConceptNoteAmmendment CNA
			WHERE CNA.ConceptNoteID = CN.ConceptNoteID
			) OACNA
		OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue) as ConceptNoteEquimentTotal
			FROM dbo.ConceptNoteEquipmentCatalog CNCE
				JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					AND CNCE.ConceptNoteID = CN.ConceptNoteID
			) OACNCE
			ORDER BY 9 DESC

END
GO
--End procedure reporting.GetConceptNotes

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
--
-- Author:			Justin Branum
-- Create date: 2016.01.21
-- Description: Added New columns regrading Previous Unit data for vetting export report.
-- ==================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
        dbo.getContactTypesByContactID(C1.ContactID) AS ContactTypeName,
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.City,
		C1.CommunityAssetID,
		C1.CommunityAssetUnitID,
		C1.CommunityID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', C1.CommunityID) AS CommunityName,
		C1.ContactID,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.DescriptionOfDuties,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsRegimeDefector,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.PassportNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.PreviousDuties,
		C1.PreviousProfession,
		C1.PreviousRankOrTitle,
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDate,
		dbo.FormatUSDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateUS,
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDate,
		dbo.FormatUSDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateUS,
		C1.PreviousUnit AS PreviousUnitLocation,
		C1.Profession,
		C1.ProvinceID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Province', C1.ProvinceID) AS ProvinceName,
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,
		dbo.FormatUSDate(C1.StartDate) AS JoinDateUSFormatted,
		C1.State,
		C1.Title,
		dbo.FormatDate(C1.UKVettingExpirationDate) AS UKVettingExpirationDateFormatted,
		dbo.FormatUSDate(C1.USVettingExpirationDate) AS USVettingExpirationDateFormatted,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		OACA.CommunityAssetName,
		OACA.TerritoryName,
		OAVO1.VettingOutcomeName AS USVettingOutcomeName,
		OAVO2.VettingOutcomeName AS UKVettingOutcomeName,
		P1.ProjectID,
		P1.ProjectName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		OUTER APPLY
			(
			SELECT 
				CA.CommunityAssetName,

				CASE
					WHEN CA.ProvinceID > 0
					THEN dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Province', CA.ProvinceID)
					WHEN CA.CommunityID > 0
					THEN dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', CA.CommunityID)
					ELSE ''
				END AS TerritoryName

			FROM dbo.CommunityAsset CA 
			WHERE CA.CommunityAssetID = C1.CommunityAssetID
			) OACA
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 1
			ORDER BY CV.ContactVettingID DESC
			) OAVO1
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 2
			ORDER BY CV.ContactVettingID DESC
			) OAVO2
	ORDER BY C1.ContactID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetJusticeCashHandoverReport
EXEC Utility.DropObject 'reporting.GetJusticeCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A stored procedure to data for the cash handover form
-- ==================================================================

CREATE PROCEDURE reporting.GetJusticeCashHandoverReport
@PersonID INT,	 
@PaymentMonth INT,	 
@PaymentYear INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@RunningCost INT,
		@nContactCount INT, 
		@nStipendAmountAuthorized NUMERIC(18, 2), 
		@cStipendName nvarchar(250), 
		@nCommunityAssetID INT, 
		@nCommunityAssetUnitID INT,
		@TotalCost INT = 0

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,

			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.CommunityAssetID > 0
				AND C.CommunityAssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.CommunityAssetID = @nCommunityAssetID AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,CommunityAssetID,CommunityAssetUnitID) VALUES (@PersonID,@nCommunityAssetID,@nCommunityAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
		SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
		SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
		SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
		SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
		SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.CommunityAssetID = @nCommunityAssetID
			AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	

	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID



	SELECT 

		@RunningCost =SUM(CAUE.ExpenseAmountAuthorized)
		--SUM(CAUE.ExpenseAmountAuthorized)
		--CAUE.ExpenseAmountAuthorized,
		--CAUE.CommunityAssetUnitID,
		--CA.CommunityAssetID


	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @PaymentMonth AND CAUE.PaymentYear =@PaymentYear




		SELECT
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END AS ProvinceName,
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetArabicProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetArabicProvinceNameByProvinceID(CA.ProvinceID)
		END AS ArabicProvinceName,
		dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst') AS FullName,
		DateName(month , DateAdd(month,@PaymentMonth, 0) - 1) + ' - ' + CAST(@PaymentYear AS CHAR(4)) as PaymentMonthYear,
		ISNULL(ISNULL(@TotalCost, 0), 0) + ISNULL(@RunningCost, 0) as CompleteCost,
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @PaymentMonth AND CAUE.PaymentYear =@PaymentYear


END
GO
--End procedure reporting.GetJusticeCashHandoverReport

--Begin procedure reporting.GetJusticeStipendActivityVariancePeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePeople

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		ISNULL(A.StipendNameCount, 0) AS AuthorizedStipendNameCount,
		ISNULL(B.StipendNameCount, 0) AS ReconciledStipendNameCount,
		ISNULL(B.StipendNameCount, 0) - ISNULL(A.StipendNameCount, 0) AS Variance
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' AND ProvinceID =@ProvinceID 
			 AND StipendAuthorizedDate IS NOT NULL
		GROUP BY CSP.StipendName, S.DisplayOrder
		) A
		LEFT JOIN
			(
			SELECT

			SUM(StipendCount)AS StipendNameCount,
			D.StipendName
			FROM
						(SELECT 
							CASE
							WHEN CSP.StipendPaidDate IS NULL THEN  0
							ELSE COUNT(	CSP.StipendName)
							END as StipendCount,
							CSP.StipendName
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.ProvinceID = @ProvinceID
						 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend'  
						AND StipendPaidDate IS NOT NULL
						GROUP BY CSP.StipendName,csp.StipendPaidDate) D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePeople

--Begin procedure reporting.GetOpsFundReport
EXEC Utility.DropObject 'reporting.GetOpsFundReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetOpsFundReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
	(
	SELECT
		CSP.ContactStipendPaymentID,
		CSP.StipendAmountAuthorized,
		CSP.CommunityID,

	CASE
		WHEN CSP.CommunityID > 0
		THEN [dbo].[GetProvinceIDByCommunityID](CSP.CommunityID)
		ELSE CSP.ProvinceID
	END AS ProvinceID
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID 
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	)

	SELECT 
		C.GovernmentIDNumber,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ProvinceName,
		(SELECT P.ArabicProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ArabicProvinceName,
		(SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS CommunityName,
		(SELECT C.ArabicCommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS ArabicCommunityName,
		CSP.StipendName,
		CA.CommunityAssetName,
		CAU.CommunityAssetUnitName,
		CSP.ContactID,
		dbo.FormatPersonNameByPersonID(@PersonID ,'LastFirst') AS AJACSRepresentative,
		dbo.FormatContactNameByContactID(CSP.ContactID, 'LastFirst') AS BeneficaryName,
		C.ArabicFirstName +' ' + C.ArabicMiddlename + ' ' + C.ArabicLastName  AS ArabicBeneficaryName,
		SD.StipendAmountAuthorized,
		dbo.FormatDate(StipendAuthorizedDate) AS StipendAuthorizedDateFormatted,
		DateName(month, DateAdd(month, CSP.PaymentMonth, 0 ) - 1) + ' - ' + CAST(PaymentYear AS VARCHAR(50)) AS PaymentMonthYear
	FROM dbo.ContactStipendPayment CSP
		JOIN SD ON SD.ContactStipendPaymentID = CSP.ContactStipendPaymentID
		JOIN dbo.Contact   C  ON C.contactid = csp.ContactID
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = C.CommunityAssetID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = C.CommunityAssetUnitID
	ORDER BY ProvinceName , CommunityName, StipendName, BeneficaryName
	
END
GO
--End procedure reporting.GetOpsFundReport

--Begin procedure Utility.EmailTemplateFieldAddUpdate
EXEC Utility.DropObject 'Utility.EmailTemplateFieldAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2016.07.17
-- Description:	A procedure do an UPSERT into the dbo.EmailTemplateField
-- =====================================================================

CREATE PROCEDURE Utility.EmailTemplateFieldAddUpdate

@EntityTypeCode VARCHAR(250), 
@PlaceHolderText VARCHAR(50), 
@PlaceHolderDescription VARCHAR(MAX), 
@DisplayOrder INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	MERGE dbo.EmailTemplateField T1
	USING (SELECT @EntityTypeCode AS EntityTypeCode, @PlaceHolderText AS PlaceHolderText, @PlaceHolderDescription AS PlaceHolderDescription, @DisplayOrder AS DisplayOrder) T2
		ON T2.EntityTypeCode = T1.EntityTypeCode
			AND T2.PlaceHolderText = T1.PlaceHolderText
	WHEN MATCHED THEN 
	UPDATE 
	SET 
		T1.PlaceHolderDescription = T2.PlaceHolderDescription,
		T1.DisplayOrder = T2.DisplayOrder
	WHEN NOT MATCHED THEN
	INSERT 
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		(
		T2.EntityTypeCode, 
		T2.PlaceHolderText, 
		T2.PlaceHolderDescription, 
		T2.DisplayOrder
		);

END
GO
--End Procedure Utility.EmailTemplateFieldAddUpdate