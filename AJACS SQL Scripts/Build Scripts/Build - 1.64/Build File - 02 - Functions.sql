USE AJACS
GO

--Begin function dbo.GetContactTypesByContactID
EXEC utility.DropObject 'dbo.GetContactTypesByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			John Lyons
-- Create date:	2015.04.28
-- Description:	A function to return contact types
-- ===============================================
CREATE FUNCTION dbo.GetContactTypesByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @ContactTypes VARCHAR(MAX) = ''
	
	SELECT @ContactTypes += CT.ContactTypeName + ', '
	FROM  dbo.ContactContactType CCT 
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID 
			AND CCT.ContactID = @ContactID

	IF LEN(RTRIM(@ContactTypes)) > 0 
		SET @ContactTypes = RTRIM(SUBSTRING(@ContactTypes, 0, LEN(@ContactTypes)))
	--ENDIF

	RETURN RTRIM(@ContactTypes)

END
GO
--End function dbo.GetContactTypesByContactID


