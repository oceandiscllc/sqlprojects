USE AJACS
GO

--Begin procedure dbo.GetCommunityAssetUnitExpenseNotes
EXEC Utility.DropObject 'dbo.GetCommunityAssetUnitExpenseNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.20
-- Description:	A stored procedure to get notes field from the CommunityAssetUnitExpense table
-- ===========================================================================================
CREATE PROCEDURE dbo.GetCommunityAssetUnitExpenseNotes

@CommunityAssetUnitExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CAUE.Notes
	FROM dbo.CommunityAssetUnitExpense CAUE
	WHERE CAUE.CommunityAssetUnitExpenseID = @CommunityAssetUnitExpenseID
	
END
GO
--End procedure dbo.GetCommunityAssetUnitExpenseNotes

--Begin procedure dbo.GetContactStipendPaymentComments
EXEC Utility.DropObject 'dbo.GetContactStipendPaymentComments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.20
-- Description:	A stored procedure to get comments field from the ContactStipendPayment table
-- ==========================================================================================
CREATE PROCEDURE dbo.GetContactStipendPaymentComments

@ContactStipendPaymentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CSP.Comments
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.ContactStipendPaymentID = @ContactStipendPaymentID
	
END
GO
--End procedure dbo.GetContactStipendPaymentComments

--Begin procedure dbo.SaveCommunityAssetUnitExpenseNotes
EXEC Utility.DropObject 'dbo.SaveCommunityAssetUnitExpenseNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.20
-- Description:	A stored procedure to update the comments field in the CommunityAssetUnitExpense table
-- ===================================================================================================
CREATE PROCEDURE dbo.SaveCommunityAssetUnitExpenseNotes

@CommunityAssetUnitExpenseID INT,
@Notes VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CAUE
	SET CAUE.Notes = @Notes
	FROM dbo.CommunityAssetUnitExpense CAUE
	WHERE CAUE.CommunityAssetUnitExpenseID = @CommunityAssetUnitExpenseID
	
END
GO
--End procedure dbo.SaveCommunityAssetUnitExpenseNotes

--Begin procedure dbo.SaveContactStipendPaymentComments
EXEC Utility.DropObject 'dbo.SaveContactStipendPaymentComments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.20
-- Description:	A stored procedure to update the comments field in the ContactStipendPayment table
-- ===============================================================================================
CREATE PROCEDURE dbo.SaveContactStipendPaymentComments

@ContactStipendPaymentID INT,
@Comments VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET CSP.Comments = @Comments
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.ContactStipendPaymentID = @ContactStipendPaymentID
	
END
GO
--End procedure dbo.SaveContactStipendPaymentComments

--Begin procedure eventlog.LogEquipmentInventoryAction
EXEC utility.DropObject 'eventlog.LogEquipmentInventoryAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Greg Yingling
-- Create date: 2015.06.25
-- Description:	Add audit support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentInventoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EquipmentInventory'), ELEMENTS
			)
		FROM procurement.EquipmentInventory T
		WHERE T.EquipmentInventoryID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentInventoryAction

--Begin procedure procurement.GetCommunityEquipmentInventory
EXEC Utility.DropObject 'dbo.GetCommunityEquipmentInventory'
EXEC Utility.DropObject 'procurement.GetCommunityEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.CommunityEquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2016.05.19
-- Description:	Refactored to support the redesigned inventory system
-- =========================================================================================
CREATE PROCEDURE procurement.GetCommunityEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DeliveredToEndUserDate) AS DeliveredToEndUserDateFormatted,
		dbo.FormatConceptNoteTitle(EI.ConceptNoteID) AS Title	
	FROM
		(
		SELECT
			DI.EquipmentInventoryID,
			SUM(DI.Quantity) AS Quantity,
			MAX(DI.DeliveredToEndUserDate) AS DeliveredToEndUserDate
		FROM
			(
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				CA.CommunityID,
				CA.ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'CommunityAsset'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				C.CommunityID,
				C.ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Contact'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Community'
					THEN F.TerritoryID
					ELSE 0
				END AS CommunityID,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Province'
					THEN F.TerritoryID
					ELSE 0
				END AS ProvinceID
	
			FROM procurement.DistributedInventory DI
				JOIN force.Force F ON F.ForceID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Force'
	
			) DI
		WHERE DI.CommunityID = @EntityID
		GROUP BY DI.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure procurement.GetCommunityEquipmentInventory

--Begin procedure procurement.GetProvinceEquipmentInventory
EXEC Utility.DropObject 'dbo.GetProvinceEquipmentInventory'
EXEC Utility.DropObject 'procurement.GetProvinceEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.ProvinceEquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2016.05.19
-- Description:	Refactored to support the redesigned inventory system
-- =========================================================================================
CREATE PROCEDURE procurement.GetProvinceEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DeliveredToEndUserDate) AS DeliveredToEndUserDateFormatted,
		dbo.FormatConceptNoteTitle(EI.ConceptNoteID) AS Title	
	FROM
		(
		SELECT
			DI.EquipmentInventoryID,
			SUM(DI.Quantity) AS Quantity,
			MAX(DI.DeliveredToEndUserDate) AS DeliveredToEndUserDate
		FROM
			(
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				CA.CommunityID,
				CA.ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'CommunityAsset'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				C.CommunityID,
				C.ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Contact'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Community'
					THEN F.TerritoryID
					ELSE 0
				END AS CommunityID,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Province'
					THEN F.TerritoryID
					ELSE 0
				END AS ProvinceID
	
			FROM procurement.DistributedInventory DI
				JOIN force.Force F ON F.ForceID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Force'
	
			) DI
		WHERE DI.ProvinceID = @EntityID
		GROUP BY DI.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure procurement.GetProvinceEquipmentInventory