USE AJACS
GO

--Begin procedure logicalframework.GetLogicalFrameworkOverviewData
EXEC Utility.DropObject 'logicalframework.GetLogicalFrameworkOverviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.12.12
-- Description:	A stored procedure to return data from the logicalframework.GetLogicalFrameworkOverviewData table
-- ==============================================================================================================
CREATE PROCEDURE logicalframework.GetLogicalFrameworkOverviewData

AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @OrderedObjectives TABLE (
		OrderedObjectiveId INT IDENTITY(100,1),
		ObjectiveID INT,
		ParentObjectiveID INT,
		ObjectiveName varchar(100),
		ObjectiveTypeID INT
	);

	DECLARE @GridData TABLE (
		ObjectiveID INT,
		ParentObjectiveID INT,
		ObjectiveName VARCHAR(100),
		TreeLevel INT,
		TreePath VARCHAR(50),
		TreeBasePath INT,
		ObjectiveTypeName VARCHAR(50),
		IndicatorID INT,
		IndicatorName NVARCHAR(500),
		IndicatorStatusName VARCHAR(250),
		IndicatorStatusColor VARCHAR(250),
		Q1Milestones INT,
		Q2Milestones INT,
		Q3Milestones INT,
		Q4Milestones INT,
		MilestoneTotal INT
	);

	INSERT INTO @OrderedObjectives (ObjectiveID,ParentObjectiveID,ObjectiveName,ObjectiveTypeID)
	SELECT O.ObjectiveID,O.ParentObjectiveID,O.ObjectiveName, O.ObjectiveTypeID
	FROM logicalframework.Objective O
	WHERE O.IsActive = 1
	ORDER BY O.ObjectiveName;

	WITH HD AS 
		(
		SELECT 
			OrderedObjectiveId, 
			ObjectiveID, 
			ParentObjectiveID, 
			ObjectiveName, 
			ObjectiveTypeID, 
			0 AS TreeLevel,
			CAST(OrderedObjectiveId AS VARCHAR(255)) AS TreePath
		FROM @OrderedObjectives T1
		WHERE ParentObjectiveID = 0

		UNION ALL

		SELECT 
			T2.OrderedObjectiveId, 
			T2.ObjectiveID, 
			T2.ParentObjectiveID, 
			T2.ObjectiveName, 
			T2.ObjectiveTypeID, 
			TreeLevel + 1, 
			CAST(TreePath + '.' + CAST(T2.OrderedObjectiveId AS VARCHAR(255)) AS VARCHAR(255)) AS TreePath
		FROM @OrderedObjectives T2
		INNER JOIN HD itms ON itms.ObjectiveID = T2.ParentObjectiveID
	)

	INSERT INTO @GridData
	SELECT 
		O.ObjectiveID, 
		O.ParentObjectiveID, 
		O.ObjectiveName, 
		O.TreeLevel, 
		O.TreePath,
		NULL,
		OT.ObjectiveTypeName,
		NULL AS IndicatorID, 
		NULL AS IndicatorName, 
		NULL AS IndicatorStatusName, 
		NULL AS IndicatorStatusColor,
		NULL AS Q1Milestones, 
		NULL AS Q2Milestones, 
		NULL AS Q3Milestones, 
		NULL AS Q4Milestones, 
		NULL AS MilestoneTotal
	FROM HD O
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND NOT EXISTS 
				(
				SELECT 'found' 
				FROM logicalframework.Indicator I 
				WHERE I.ObjectiveID = O.ObjectiveID AND 
					I.IsActive = 1
				)

	UNION

	SELECT 
		O.ObjectiveID, 
		O.ParentObjectiveID, 
		O.ObjectiveName, 
		O.TreeLevel, 
		O.TreePath, 
		NULL,
		OT.ObjectiveTypeName,
		I.IndicatorID, 
		I.IndicatorName, 
		DIS.IndicatorStatusName, 
		IIF(DIS.IndicatorStatusName='Off Track','E41A1C',IIF(DIS.IndicatorStatusName='On Track','4DAF4A',IIF(DIS.IndicatorStatusName='Progressing','FFFF33','A65628'))) AS IndicatorStatusColor,
		(
		SELECT IsNULL(SUM(IM.AchievedValue), 0)
		FROM logicalframework.Milestone M
			JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
				AND M.IsActive = 1
				AND IM.IndicatorID = I.IndicatorID
				AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ1Month')
		) AS Q1Milestones,
		(
		SELECT IsNULL(SUM(IM.AchievedValue), 0)
		FROM logicalframework.Milestone M
			JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
				AND M.IsActive = 1
				AND IM.IndicatorID = I.IndicatorID
				AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ2Month')
		) AS Q2Milestones,
		(
		SELECT IsNULL(SUM(IM.AchievedValue), 0)
		FROM logicalframework.Milestone M
			JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
				AND M.IsActive = 1
				AND IM.IndicatorID = I.IndicatorID
				AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ3Month')
		) AS Q3Milestones,
		(
		SELECT IsNULL(SUM(IM.AchievedValue), 0)
		FROM logicalframework.Milestone M
			JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
				AND M.IsActive = 1
				AND IM.IndicatorID = I.IndicatorID
				AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ4Month')
		) AS Q4Milestones,
		(
		SELECT IsNULL(SUM(IM.AchievedValue), 0)
		FROM logicalframework.Milestone M
			JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
				AND M.IsActive = 1
				AND IM.IndicatorID = I.IndicatorID
				AND (
					DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ1Month') OR 
					DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ2Month') OR 
					DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ3Month') OR 
					DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ4Month')
					)
		) AS MilestonesTotal
	FROM  HD O
		JOIN logicalframework.Indicator I ON I.ObjectiveID = O.ObjectiveID
		JOIN dropdown.IndicatorStatus DIS ON DIS.IndicatorStatusID = I.IndicatorStatusID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
	AND I.IsActive = 1
	ORDER BY O.TreePath;

	--Grid Data
	SELECT * 
	FROM @GridData;

	UPDATE D
	SET D.TreeBasePath = 
		CASE 
			WHEN CHARINDEX('.', D.TreePath) > 0
			THEN CAST(LEFT(D.TreePath, CHARINDEX('.', D.TreePath) - 1) AS INT)
			ELSE CAST(D.TreePath AS INT)
		END
	FROM @GridData D;

	WITH CD AS
		(
		SELECT 
			D.IndicatorStatusName, 
			COUNT(D.IndicatorStatusName) AS IndicatorStatusCount,
			D.IndicatorStatusColor,
			D.TreeBasePath
		FROM @GridData D
		WHERE D.IndicatorStatusName IS NOT NULL
		GROUP BY D.TreeBasePath, D.IndicatorStatusName, D.IndicatorStatusColor
		)

	--ChartData
	SELECT
		CD.IndicatorStatusName,
		CD.IndicatorStatusCount,
		CD.IndicatorStatusColor,
		CD.TreeBasePath,
		(SELECT TOP 1 D.ObjectiveName FROM @GridData D WHERE D.TreeBasePath = CD.TreeBasePath) AS ObjectiveName
	FROM CD

END
GO
--End procedure logicalframework.GetLogicalFrameworkOverviewData

--Begin procedure reporting.GetActivityAnalysis
EXEC Utility.DropObject 'reporting.GetActivityAnalysis'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================
-- Author:			Kevin Ross
-- Create date: 2017.07.20
-- Description:	A stored procedure to get GetActivityAnalysis data
-- ===============================================================
CREATE PROCEDURE reporting.GetActivityAnalysis

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		CN.ConceptNoteID,
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CAST(CAST((CNI.ActualQuantity / CAST(I.TargetValue AS DECIMAL(10, 2))) * 100 AS DECIMAL(5, 0)) AS varchar(5)) + ' %' AS ActivityTargetPercentage,
		CNI.TargetQuantity,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeName,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' : ' + dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS FullTitle,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ConceptNoteRefCode,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS Title,
		dbo.GetConceptNoteVersionNumberByConceptNoteID(CN.ConceptNoteID) AS VersionNumber,

		CASE
			WHEN DIS.IndicatorStatusName = 'On Track'
			THEN dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/4DAF4A.png'
			WHEN DIS.IndicatorStatusName = 'Off Track'
			THEN dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/E41A1C.png'
			WHEN DIS.IndicatorStatusName = 'Progressing'
			THEN dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/FFFF33.png'
			ELSE dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/A65628.png' 
		END AS IndicatorStatusIcon,

		I.IndicatorName,
		I.TargetValue AS OverallTarget,
		IT.IndicatorTypeName,
		O.ObjectiveName,
		(SELECT COUNT(RTI.IndicatorID) FROM logicalframework.Indicator RTI JOIN dbo.ConceptNoteIndicator RTCNI ON RTCNI.IndicatorID = RTI.IndicatorID JOIN dbo.ConceptNote RTCN ON RTCN.ConceptNoteID = RTCNI.ConceptNoteID JOIN dropdown.ConceptNoteType RTCNT ON RTCNT.ConceptNoteTypeID = RTCN.ConceptNoteTypeID) AS RecordsTotal
	FROM logicalframework.Indicator I
		JOIN Reporting.SearchResult SR ON SR.EntityID = I.IndicatorID 
			AND SR.EntityTypeCode='ActivityAnalysis' 
			AND SR.PersonID = @PersonID
		JOIN dbo.ConceptNoteIndicator CNI ON CNI.IndicatorID = SR.EntityID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode NOT IN ('Amended','Cancelled','OnHold')
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.IndicatorStatus DIS ON DIS.IndicatorStatusID = I.IndicatorStatusID
	ORDER BY dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' : ' + dbo.FormatConceptNoteTitle(CN.ConceptNoteID), CNT.ConceptNoteTypeName, I.IndicatorName, O.ObjectiveName

END
GO
--End procedure reporting.GetActivityAnalysis

--Begin procedure weeklyreport.ApproveWeeklyReport
EXEC Utility.DropObject 'weeklyreport.ApproveWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
--
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	Renamed from SubmitWeeklyReport to ApproveWeeklyReport
--
-- Author:			Todd Pires
-- Create date:	2015.05.16
-- Description:	Added the post-approval initialization call
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ======================================================================
CREATE PROCEDURE weeklyreport.ApproveWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @nWeeklyReportID INT
	DECLARE @tOutput1 TABLE (EntityTypeCode VARCHAR(50), EntityID INT)
	DECLARE @tOutput2 TABLE (WeeklyReportID INT)

	SELECT @nWeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput1
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @nWeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput1
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @nWeeklyReportID

	INSERT INTO @tOutput1 (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @nWeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O1.EntityTypeCode, O1.EntityID
		FROM @tOutput1 O1
		ORDER BY O1.EntityTypeCode, O1.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	TRUNCATE TABLE weeklyreport.Community
	TRUNCATE TABLE weeklyreport.Province
	TRUNCATE TABLE weeklyreport.SummaryMapAsset
	TRUNCATE TABLE weeklyreport.SummaryMapCommunity
	TRUNCATE TABLE weeklyreport.SummaryMapIncident
	
	DELETE FROM weeklyreport.WeeklyReport

	INSERT INTO weeklyreport.WeeklyReport 
		(WorkflowStepNumber) 
	OUTPUT INSERTED.WeeklyReportID INTO @tOutput2
	VALUES 
		(1)

	SELECT @nWeeklyReportID = O2.WeeklyReportID FROM @tOutput2 O2

	EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

END
GO
--End procedure weeklyreport.ApproveWeeklyReport