USE AJACS
GO

EXEC utility.DropObject 'dbo.CommunityAsset'
EXEC utility.DropObject 'dbo.CommunityAssetRisk'
EXEC utility.DropObject 'dbo.CommunityAssetUnit'
EXEC utility.DropObject 'dbo.CommunityAssetUnitExpense'
EXEC utility.DropObject 'dbo.GetCommunityAssetByCommunityAssetID'
EXEC utility.DropObject 'dbo.SaveCommunityAssetUnitExpenseNotes'
EXEC utility.DropObject 'dropdown.CommunityAssetStatus'
EXEC utility.DropObject 'dropdown.CommunityAssetType'
EXEC utility.DropObject 'dropdown.CommunityAssetUnitCostRate'
EXEC utility.DropObject 'dropdown.CommunityAssetUnitType'
EXEC utility.DropObject 'dropdown.GetCommunityAssetStatusData'
EXEC utility.DropObject 'dropdown.GetCommunityAssetTypeData'
EXEC utility.DropObject 'dropdown.GetCommunityAssetUnitCostRateData'
EXEC utility.DropObject 'dropdown.GetCommunityAssetUnitTypeData'
EXEC utility.DropObject 'reporting.CommunityAssetUnitCostRateTotal'
EXEC utility.DropObject 'weeklyreport.SummaryMapCommunityAsset'
GO

EXEC utility.DropColumn 'dbo.Contact', 'CommunityAssetID'
EXEC utility.DropColumn 'dbo.Contact', 'CommunityAssetUnitID'
GO

EXEC utility.DropColumn 'reporting.StipendPayment', 'CommunityAssetID'
EXEC utility.DropColumn 'reporting.StipendPayment', 'CommunityAssetUnitID'
GO
