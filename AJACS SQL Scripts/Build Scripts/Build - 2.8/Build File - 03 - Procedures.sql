USE AJACS
GO

--Begin procedure reporting.GetStipendPaymentReport
EXEC utility.DropObject 'reporting.GetStipendPaymentReport'
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2015.09.30
-- Description:	A stored procedure to add data to the reporting.StipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
--
-- Author:			Todd Pires
-- Create date:	2017.04.28
-- Description:	Refactored to support categories
-- =================================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT 

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)
	DECLARE	@PaymentMonth INT
	DECLARE	@PaymentYear INT
	DECLARE @ProvinceName NVarchar(max)

	DELETE SP
	FROM reporting.StipendPayment SP


	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendCategory,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendCategory,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Policemen] = SP.[Policemen] + CASE WHEN @cStipendName = 'Police' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Policemen Count] = SP.[Policemen Count] + CASE WHEN @cStipendName = 'Police' THEN @nContactCount ELSE 0 END,

			SP.[Non Commissioned Officer] = SP.[Non Commissioned Officer] + CASE WHEN @cStipendName = 'Non Commissioned Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Non Commissioned Officer Count] = SP.[Non Commissioned Officer Count] + CASE WHEN @cStipendName = 'Non Commissioned Officer' THEN @nContactCount ELSE 0 END,

			SP.[Commissioned Officer] = SP.[Commissioned Officer] + CASE WHEN @cStipendName = 'Commissioned Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Commissioned Officer Count] = SP.[Commissioned Officer Count] + CASE WHEN @cStipendName = 'Commissioned Officer' THEN @nContactCount ELSE 0 END,

			SP.[Commander] = SP.[Commander] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Commander Count] = SP.[Commander Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END
			--SP.[Command] = SP.[Command] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Command Count] = SP.[Command Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END,
			--SP.[General] = SP.[General] + CASE WHEN @cStipendName = 'General' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[General Count] = SP.[General Count] + CASE WHEN @cStipendName = 'General' THEN @nContactCount ELSE 0 END,
			--SP.[Colonel] = SP.[Colonel] + CASE WHEN @cStipendName = 'Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Colonel Count] = SP.[Colonel Count] + CASE WHEN @cStipendName = 'Colonel' THEN @nContactCount ELSE 0 END,
			--SP.[Colonel Doctor] = SP.[Colonel Doctor] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Colonel Doctor Count] = SP.[Colonel Doctor Count] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nContactCount ELSE 0 END,
			--SP.[Lieutenant Colonel] = SP.[Lieutenant Colonel] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Lieutenant Colonel Count] = SP.[Lieutenant Colonel Count] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nContactCount ELSE 0 END,
			--SP.[Major] = SP.[Major] + CASE WHEN @cStipendName = 'Major' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Major Count] = SP.[Major Count] + CASE WHEN @cStipendName = 'Major' THEN @nContactCount ELSE 0 END,
			--SP.[Captain] = SP.[Captain] + CASE WHEN @cStipendName = 'Captain' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Captain Count] = SP.[Captain Count] + CASE WHEN @cStipendName = 'Captain' THEN @nContactCount ELSE 0 END,
			--SP.[Captain Doctor] = SP.[Captain Doctor] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Captain Doctor Count] = SP.[Captain Doctor Count] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nContactCount ELSE 0 END,
			--SP.[First Lieutenant] = SP.[First Lieutenant] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[First Lieutenant Count] = SP.[First Lieutenant Count] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nContactCount ELSE 0 END,
			--SP.[Contracted Officer] = SP.[Contracted Officer] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Contracted Officer Count] = SP.[Contracted Officer Count] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nContactCount ELSE 0 END,
			--SP.[First Sergeant] = SP.[First Sergeant] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[First Sergeant Count] = SP.[First Sergeant Count] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nContactCount ELSE 0 END,
			--SP.[Sergeant] = SP.[Sergeant] + CASE WHEN @cStipendName = 'Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Sergeant Count] = SP.[Sergeant Count] + CASE WHEN @cStipendName = 'Sergeant' THEN @nContactCount ELSE 0 END,
			--SP.[First Adjutant] = SP.[First Adjutant] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[First Adjutant Count] = SP.[First Adjutant Count] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nContactCount ELSE 0 END,
			--SP.[Adjutant] = SP.[Adjutant] + CASE WHEN @cStipendName = 'Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Adjutant Count] = SP.[Adjutant Count] + CASE WHEN @cStipendName = 'Adjutant' THEN @nContactCount ELSE 0 END,
			--SP.[Policeman] = SP.[Policeman] + CASE WHEN @cStipendName = 'Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Policeman Count] = SP.[Policeman Count] + CASE WHEN @cStipendName = 'Policeman' THEN @nContactCount ELSE 0 END,
			--SP.[Contracted Policeman] = SP.[Contracted Policeman] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Contracted Policeman Count] = SP.[Contracted Policeman Count] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		@PaymentMonth = PaymentMonth, 
		@PaymentYear = PaymentYear  ,
		@ProvinceName = dbo.GetProvinceNameByProvinceID(CSP.ProvinceID)
		FROM dbo.ContactStipendPayment CSP 
			JOIN 
				(
				SELECT TOP 1 EntityID	
				FROM reporting.SearchResult SR 
				WHERE SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
				) D ON D.EntityID = CSP.ContactStipendPaymentID


	
	SELECT 
		SP.*,
		DateName(mm,DATEADD(mm,@PaymentMonth,0)) as PaymentMonth,
		@PaymentYear as PaymentYear,
		@ProvinceName as ProvinceName,
		AUCR.AssetUnitCostName AS [Running Costs],
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dropdown.AssetUnitCost AUCR ON AUCR.AssetUnitCostID = AU.AssetUnitCostID

END
GO
--End procedure reporting.GetStipendPaymentReport