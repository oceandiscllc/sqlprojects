USE AJACS
GO

--Begin table dropdown.ObjectiveType
DECLARE @TableName VARCHAR(250) = 'dropdown.ObjectiveType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ObjectiveType
	(
	ObjectiveTypeID INT IDENTITY(0,1) NOT NULL,
	ObjectiveTypeCode VARCHAR(50),
	ObjectiveTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ObjectiveTypeID'
EXEC utility.SetIndexNonClustered 'IX_ObjectiveTypeName', @TableName, 'DisplayOrder,ObjectiveTypeName', 'ObjectiveTypeID'
GO

SET IDENTITY_INSERT dropdown.ObjectiveType ON
GO

INSERT INTO dropdown.ObjectiveType (ObjectiveTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ObjectiveType OFF
GO

INSERT INTO dropdown.ObjectiveType 
	(ObjectiveTypeCode,ObjectiveTypeName,DisplayOrder)
VALUES
	('Impact', 'Impact', 1),
	('Outcome', 'Outcome', 2),
	('IntermediateOutcome', 'Intermediate Outcome', 3),
	('Output', 'Output', 4)
GO
--End table dropdown.ObjectiveType

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

ALTER TABLE logicalframework.Indicator ALTER COLUMN AchievedValue INT
ALTER TABLE logicalframework.Indicator ALTER COLUMN BaselineValue INT
ALTER TABLE logicalframework.Indicator ALTER COLUMN TargetValue INT

EXEC utility.SetDefaultConstraint @TableName, 'AchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'BaselineValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TargetValue', 'INT', 0
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

ALTER TABLE logicalframework.Indicator ALTER COLUMN AchievedValue INT
ALTER TABLE logicalframework.Indicator ALTER COLUMN TargetValue INT

EXEC utility.SetDefaultConstraint @TableName, 'AchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TargetValue', 'INT', 0
GO
--End table logicalframework.Milestone



