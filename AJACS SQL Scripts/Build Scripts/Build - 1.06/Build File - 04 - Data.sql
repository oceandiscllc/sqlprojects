USE AJACS
GO

--Begin table dbo.Menuitem
UPDATE MI
SET MI.MenuItemCode = REPLACE(MI.MenuItemCode, 'Add', 'List')
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('IndicatorAdd','MilestoneAdd','ObjectiveAdd')
GO

UPDATE MI
SET MI.MenuItemCode = 'ObjectiveOverview'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'MonitoringOverview'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveOverview', @NewMenuItemLink='/objective/list?ObjectiveTypeCode=IntermediateOutcome', @BeforeMenuItemCode='ObjectiveList'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveList', @NewMenuItemLink='/objective/list', @AfterMenuItemCode='ObjectiveOverview'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorList', @NewMenuItemLink='/indicator/list', @AfterMenuItemCode='ObjectiveList'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='MilestoneList', @NewMenuItemLink='/milestone/list', @AfterMenuItemCode='IndicatorList'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorTypeList', @AfterMenuItemCode='IndicatorList'
GO
--End table dbo.Menuitem
