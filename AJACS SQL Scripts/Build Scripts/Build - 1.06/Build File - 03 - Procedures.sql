USE AJACS
GO

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
-- ==============================================================
CREATE PROCEDURE dbo.GetDonorFeed

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		MI.Icon,
		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		OAWR.PhysicalFileName,
		D.EntityID,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
					AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE LEFT(DOC.DocumentTitle, 14) = 'AJACS-WR-' + RIGHT('0000' + CAST(D.EntityID AS VARCHAR(10)), 4)
			) OAWR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
		JOIN dbo.MenuItem MI ON MI.MenuItemCode = 
			CASE
				WHEN D.EntityTypeCode = 'WeeklyReport'
				THEN 'ManageWeeklyReports'
				ELSE D.EntityTypeCode
			END

			AND OAWR.PhysicalFileName IS NOT NULL
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
	ORDER BY D.UpdateDate DESC, D.EntityTypeCode, D.EntityID
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dropdown.GetIndicatorTypeData
EXEC Utility.DropObject 'dropdown.GetIndicatorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to return data from the dropdown.IndicatorType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetIndicatorTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IndicatorTypeID,
		T.IndicatorTypeName
	FROM dropdown.IndicatorType T
	WHERE (T.IndicatorTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IndicatorTypeName, T.IndicatorTypeID

END
GO
--End procedure dropdown.GetIndicatorTypeData

--Begin procedure eventlog.LogConceptNoteAction
EXEC Utility.DropObject 'eventlog.LogConceptNoteAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.04.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- =========================================================================================
CREATE PROCEDURE eventlog.LogConceptNoteAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cConceptNoteCommunities VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCommunities = COALESCE(@cConceptNoteCommunities, '') + D.ConceptNoteCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCommunity'), ELEMENTS) AS ConceptNoteCommunity
			FROM dbo.ConceptNoteCommunity T 
			WHERE T.ConceptNoteID = @EntityID
			) D
			
		DECLARE @cConceptNoteProvinces VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteProvinces = COALESCE(@cConceptNoteProvinces, '') + D.ConceptNoteProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteProvince'), ELEMENTS) AS ConceptNoteProvince
			FROM dbo.ConceptNoteProvince T 
			WHERE T.ConceptNoteID = @EntityID
			) D			
			
		DECLARE @cConceptNoteBudget VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteBudget = COALESCE(@cConceptNoteBudget, '') + D.ConceptNoteBudget
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteBudget'), ELEMENTS) AS ConceptNoteBudget
			FROM dbo.ConceptNoteBudget T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteClass VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteClass = COALESCE(@cConceptNoteClass, '') + D.ConceptNoteClass
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteClass'), ELEMENTS) AS ConceptNoteClass
			FROM dbo.ConceptNoteClass T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteContact VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteContact= COALESCE(@cConceptNoteContact, '') + D.ConceptNoteContact
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteContact'), ELEMENTS) AS ConceptNoteContact
			FROM dbo.ConceptNoteContact T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteCourse VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCourse= COALESCE(@cConceptNoteCourse, '') + D.ConceptNoteCourse
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCourse'), ELEMENTS) AS ConceptNoteCourse
			FROM dbo.ConceptNoteCourse T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteEquipmentCatalog  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteEquipmentCatalog = COALESCE(@cConceptNoteEquipmentCatalog , '') + D.ConceptNoteEquipmentCatalog 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteEquipmentCatalog'), ELEMENTS) AS ConceptNoteEquipmentCatalog 
			FROM dbo.ConceptNoteEquipmentCatalog  T 
			WHERE T.ConceptNoteID = @EntityID
			) D				
			
		DECLARE @cConceptNoteIndicator  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteIndicator = COALESCE(@cConceptNoteIndicator , '') + D.ConceptNoteIndicator 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteIndicator'), ELEMENTS) AS ConceptNoteIndicator 
			FROM dbo.ConceptNoteIndicator  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteTask  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteTask = COALESCE(@cConceptNoteTask , '') + D.ConceptNoteTask 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteTask'), ELEMENTS) AS ConceptNoteTask 
			FROM dbo.ConceptNoteTask  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ConceptNoteCommunities>' + ISNULL( @cConceptNoteCommunities , '') + '</ConceptNoteCommunities>') AS XML), 
			CAST(('<ConceptNoteProvinces>' + ISNULL( @cConceptNoteProvinces , '') + '</ConceptNoteProvinces>') AS XML), 
			CAST(('<ConceptNoteBudget>' + ISNULL( @cConceptNoteBudget , '') + '</ConceptNoteBudget>') AS XML), 
			CAST(('<ConceptNoteClass>' + ISNULL( @cConceptNoteClass , '') + '</ConceptNoteClass>') AS XML), 
			CAST(('<ConceptNoteContact>' + ISNULL( @cConceptNoteContact , '') + '</ConceptNoteContact>') AS XML), 
			CAST(('<ConceptNoteCourse>' + ISNULL( @cConceptNoteCourse , '') + '</ConceptNoteCourse>') AS XML), 
			CAST(('<ConceptNoteEquipmentCatalog>' + ISNULL( @cConceptNoteEquipmentCatalog  , '') + '</ConceptNoteEquipmentCatalog>') AS XML), 
			CAST(('<ConceptNoteIndicator>' + ISNULL( @cConceptNoteIndicator  , '') + '</ConceptNoteIndicator>') AS XML), 
			CAST(('<ConceptNoteTask>' + ISNULL( @cConceptNoteTask  , '') + '</ConceptNoteTask>') AS XML)
			FOR XML RAW('ConceptNote'), ELEMENTS
			)
		FROM dbo.ConceptNote T
		WHERE T.ConceptNoteID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogConceptNoteAction

--Begin procedure logicalframework.GetIndicatorByObjectiveID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the logicalframework.Indicator table
-- =====================================================================================
CREATE PROCEDURE logicalframework.GetIndicatorByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		I.AchievedDate,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormatted,
		I.AchievedValue,
		I.BaselineDate,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormatted,
		I.BaselineValue,
		I.IndicatorDescription,
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorSource,
		I.TargetDate,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormatted,
		I.TargetValue,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName
  FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
			AND I.ObjectiveID = @ObjectiveID
	ORDER BY I.IndicatorID
	
END
GO
--End procedure logicalframework.GetIndicatorByObjectiveID

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
--
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	Add 'short' formats
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorSource, 	
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 	
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetMilestoneByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetMilestoneByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		MilestoneName VARCHAR(100),
		TargetDateFormattedShort CHAR(6),
		PlannedValue INT,
		InprogressValue INT,
		AchievedValue INT,
		BaseLineValue INT
		)

	INSERT INTO @tTable
		(MilestoneName, TargetDateFormattedShort, PlannedValue, InprogressValue, AchievedValue, BaseLineValue)
	SELECT
		'Baseline',
		LEFT(DATENAME(month, I.BaseLineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaseLineDate) AS CHAR(4)), 2),
		0 AS PlannedValue,
		0 AS InprogressValue,
		0 AS AchievedValue,
		I.BaseLineValue
	FROM logicalframework.Indicator I
	WHERE I.IndicatorID = @IndicatorID

	INSERT INTO @tTable
		(MilestoneName, TargetDateFormattedShort, PlannedValue, InprogressValue, AchievedValue, BaseLineValue)
	SELECT
		M1.MilestoneName, 	
		LEFT(DATENAME(month, M1.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(M1.TargetDate) AS CHAR(4)), 2),

		CASE
			WHEN OT.ObjectiveTypeCode = 'IntermediateOutcome'
			THEN 0
			ELSE ISNULL(OAP.TargetQuantity, 0)
		END,

		CASE
			WHEN OT.ObjectiveTypeCode = 'IntermediateOutcome'
			THEN 0
			ELSE ISNULL(OAIP.TargetQuantity, 0)
		END,

		ISNULL(OAA.TargetQuantity, 0),
		M1.TargetValue
	FROM logicalframework.Milestone M1
		JOIN logicalframework.Indicator I ON I.IndicatorID = M1.IndicatorID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND M1.IndicatorID = @IndicatorID
		CROSS APPLY
			(
			SELECT
				SUM(CNI.TargetQuantity) AS TargetQuantity
			FROM dbo.ConceptNoteIndicator CNI
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
				JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
					AND CN.WorkflowStepNumber < 8
					AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','OnHold')
					AND CNI.IndicatorID = M1.IndicatorID
					AND EXISTS
						(
						SELECT 1
						FROM logicalframework.Milestone M2 
						WHERE M2.IndicatorID = CNI.IndicatorID
							AND CN.EndDate <= M2.TargetDate
						)
			) AS OAP
		CROSS APPLY
			(
			SELECT
				SUM(CNI.TargetQuantity) AS TargetQuantity
			FROM dbo.ConceptNoteIndicator CNI
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
				JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
					AND CN.WorkflowStepNumber IN (8,9)
					AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','OnHold')
					AND CNI.IndicatorID = M1.IndicatorID
					AND EXISTS
						(
						SELECT 1
						FROM logicalframework.Milestone M2 
						WHERE M2.IndicatorID = CNI.IndicatorID
							AND CN.EndDate <= M2.TargetDate
						)
			) AS OAIP
		CROSS APPLY
			(
			SELECT
				SUM(CNI.TargetQuantity) AS TargetQuantity
			FROM dbo.ConceptNoteIndicator CNI
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
				JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
					AND CN.WorkflowStepNumber > 9
					AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','OnHold')
					AND CNI.IndicatorID = M1.IndicatorID
					AND EXISTS
						(
						SELECT 1
						FROM logicalframework.Milestone M2 
						WHERE M2.IndicatorID = CNI.IndicatorID
							AND CN.EndDate <= M2.TargetDate
						)
			) AS OAA
	ORDER BY M1.TargetDate, 1

	INSERT INTO @tTable
		(MilestoneName, TargetDateFormattedShort, PlannedValue, InprogressValue, AchievedValue, BaseLineValue)
	SELECT
		'Total' AS MilestoneName, 	
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2),

		CASE
			WHEN OT.ObjectiveTypeCode = 'IntermediateOutcome'
			THEN 0
			ELSE ISNULL(OAP.TargetQuantity, 0)
		END,

		CASE
			WHEN OT.ObjectiveTypeCode = 'IntermediateOutcome'
			THEN 0
			ELSE ISNULL(OAIP.TargetQuantity, 0)
		END,

		ISNULL(OAA.TargetQuantity, 0),
		I.TargetValue
	FROM logicalframework.Indicator I
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND I.IndicatorID = @IndicatorID
		CROSS APPLY
			(
			SELECT
				SUM(CNI.TargetQuantity) AS TargetQuantity
			FROM dbo.ConceptNoteIndicator CNI
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
				JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
					AND CN.WorkflowStepNumber < 8
					AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','OnHold')
					AND CNI.IndicatorID = I.IndicatorID
			) AS OAP
		CROSS APPLY
			(
			SELECT
				SUM(CNI.TargetQuantity) AS TargetQuantity
			FROM dbo.ConceptNoteIndicator CNI
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
				JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
					AND CN.WorkflowStepNumber IN (8,9)
					AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','OnHold')
					AND CNI.IndicatorID = I.IndicatorID
			) AS OAIP
		CROSS APPLY
			(
			SELECT
				SUM(CNI.TargetQuantity) AS TargetQuantity
			FROM dbo.ConceptNoteIndicator CNI
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
				JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
					AND CN.WorkflowStepNumber > 9
					AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','OnHold')
					AND CNI.IndicatorID = I.IndicatorID
			) AS OAA

	SELECT
		T.MilestoneName, 
		T.TargetDateFormattedShort, 
		T.PlannedValue, 
		T.InprogressValue, 
		T.AchievedValue, 
		T.BaseLineValue
	FROM @tTable T
	ORDER BY T.RowIndex
	
END
GO
--End procedure logicalframework.GetMilestoneByIndicatorID

--Begin procedure procurement.GetPurchaseRequestByPurchaseRequestID
EXEC Utility.DropObject 'procurement.GetPurchaseRequestByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.01
-- Description:	A stored procedure to return data for a purchase request
-- =====================================================================
CREATE PROCEDURE procurement.GetPurchaseRequestByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT

	SELECT @nConceptNoteID = PR.ConceptNoteID
	FROM procurement.PurchaseRequest PR
	WHERE PR.PurchaseRequestID = @PurchaseRequestID

	SELECT
		PR.PurchaseRequestID,
		PR.ConceptNoteID,
		PR.RequestPersonID,
		PR.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(PR.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		PR.DeliveryDate,
		dbo.FormatDate(PR.DeliveryDate) AS DeliveryDateFormatted,
		PR.ReferenceCode,
		PR.Address,
		PR.AddressCountryID,
		PR.Notes,
		CUR.CurrencyID,
		CUR.CurrencyName,
		COU.CountryName,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' ' + CN.Title AS ConceptNoteTitle
	FROM procurement.PurchaseRequest PR
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = PR.CurrencyID
		JOIN dropdown.Country COU ON COU.CountryID = PR.AddressCountryID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = PR.ConceptNoteID
	WHERE PR.PurchaseRequestID = @PurchaseRequestID
	
	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.ConceptNoteBudgetID,
		CNB.ItemName,
		ISNULL(OAPRCNB.Quantity, CNB.Quantity) AS Quantity,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNB.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT PRCNB.Quantity 
			FROM procurement.PurchaseRequestConceptNoteBudget PRCNB 
			WHERE PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID 
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNB

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNEC.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT PRCNEC.Quantity 
			FROM procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC 
			WHERE PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID 
				AND PRCNEC.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNEC

	SELECT
		SC.SubContractorID,
		SC.SubContractorName
	FROM procurement.PurchaseRequestSubContractor PRSC
		JOIN dbo.SubContractor SC ON SC.SubContractorID = PRSC.SubContractorID
			AND PRSC.PurchaseRequestID = @PurchaseRequestID
END
GO
--End procedure procurement.GetPurchaseRequestByPurchaseRequestID

--Begin procedure reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.31
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID

@ConceptNoteID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNCE.Quantity,
		EC.ItemName,
		ISNULL(EI.SerialNumber, 'None') AS SerialNumber
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CNCE.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND CNCE.ConceptNoteID = @ConceptNoteID
			AND CNCE.ContactID = @ContactID
	ORDER BY EC.ItemName, EI.SerialNumber, CNCE.Quantity

END
GO
--End procedure reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID

--Begin procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.31
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT

		CASE
			WHEN CON.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CON.CommunityID)
			WHEN CON.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CON.ProvinceID)
			ELSE ''
		END AS Location,
			
		CON.ContactID,
		CON.EmployerName,
		CN.ConceptNoteID,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ConceptNoteReferenceCode,
		dbo.FormatContactNameByContactID(CON.ContactID, 'LastFirst') AS Fullname
	FROM
		(
		SELECT DISTINCT
			CNCE.ConceptNoteID,
			CNCE.ContactID
		FROM dbo.ConceptNoteContactEquipment CNCE
		WHERE CNCE.ConceptNoteID = @ConceptNoteID
		) D
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = D.ConceptNoteID
		JOIN dbo.Contact CON ON CON.ContactID = D.ContactID
	ORDER BY Fullname, D.ContactID

END
GO
--End procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,

		CASE
			WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed')
			THEN CNS.ConceptNoteStatusName
			ELSE
				CASE
					WHEN CN.WorkflowStepNumber < 6
					THEN 'Development'
					WHEN CN.WorkflowStepNumber = 6
					THEN 'Submitted for CSO Approval'
					ELSE 'Implementation'
				END
			END AS Status,

		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1
			SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		OACNB.TotalBudget,
		CN.SpentToDate AS TotalSpent,
		OACNB.TotalBudget - CN.SpentToDate AS TotalRemaining
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB

END
GO
--End procedure reporting.GetConceptNotes

--Begin procedure reporting.GetLicenseEquipment
EXEC Utility.DropObject 'reporting.GetLicenseEquipment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the business license report
-- ===========================================================================
CREATE PROCEDURE reporting.GetLicenseEquipment

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.EquipmentCatalogID,
		LEC.ReferenceCode,
		EC.ItemName,
		LEC.ECCN,
		L.LicenseNumber,
		dbo.FormatDate(L.StartDate) AS StartDateFormatted,
		dbo.FormatDate(L.EndDate) AS EndDateFormatted,
		LEC.QuantityAuthorized,
		QP.QuantityPurchased,
		ISNULL(QD.QuantityDistributed, 0) AS QuantityDistributed,
		QP.QuantityPurchased - ISNULL(QD.QuantityDistributed, 0) AS QuantityRemaining,
		CAST(LEC.BudgetLimit / LEC.QuantityAuthorized AS NUMERIC(18,2)) AS UnitCost,
		LEC.BudgetLimit,
		CAST(LEC.BudgetLimit - ISNULL((LEC.BudgetLimit / LEC.QuantityAuthorized * QD.QuantityDistributed), 0) AS NUMERIC(18,2)) AS BudgetRemaining,
		CAST(ISNULL(LEC.BudgetLimit / LEC.QuantityAuthorized * QD.QuantityDistributed, 0) AS INT) AS TotalDistributed
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = LEC.EquipmentCatalogID
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
		OUTER APPLY
			(
			SELECT SUM(EI.Quantity) AS QuantityPurchased
			FROM procurement.EquipmentInventory EI
			WHERE EI.EquipmentCatalogID = EC.EquipmentCatalogID
			) QP
		OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity) AS QuantityDistributed
			FROM dbo.ConceptNoteContactEquipment CNCE
				JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CNCE.EquipmentInventoryID
				JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
					AND EC.EquipmentCatalogID = LEC.EquipmentCatalogID
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
					AND CN.ConceptNoteContactEquipmentDistributionDate IS NOT NULL
			GROUP BY EC.EquipmentCatalogID
			) QD 

END
GO
--End procedure reporting.GetLicenseEquipment

--Begin procedure reporting.GetPurchaseRequestByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purches request reports
-- ==============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		PurchaseRequestID INT, 
		ConceptNoteID INT, 
		RequestPersonID INT, 
		PointOfContactPersonID INT, 
		PointOfContactPersonNameFormatted VARCHAR(250), 
		RequestPersonNameFormatted VARCHAR(250), 
		DeliveryDate DATE, 
		DeliveryDateFormatted VARCHAR(15),
		ReferenceCode VARCHAR(50), 
		Address VARCHAR(200), 
		AddressCountryID INT, 
		Notes VARCHAR(MAX), 
		CurrencyID INT, 
		CurrencyName VARCHAR(250), 
		CountryName VARCHAR(50), 
		ConceptNoteTitle VARCHAR(300) 
		)

	INSERT INTO @tTable
		(PurchaseRequestID,ConceptNoteID,RequestPersonID,PointOfContactPersonID,PointOfContactPersonNameFormatted,RequestPersonNameFormatted,DeliveryDate,DeliveryDateFormatted,ReferenceCode,Address,AddressCountryID,Notes,CurrencyID,CurrencyName,CountryName,ConceptNoteTitle)
	SELECT
		PR.PurchaseRequestID,
		PR.ConceptNoteID,
		PR.RequestPersonID,
		PR.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(PR.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		dbo.FormatPersonNameByPersonID(PR.RequestPersonID, 'LastFirst') AS RequestPersonNameFormatted,
		PR.DeliveryDate,
		dbo.FormatDate(PR.DeliveryDate) AS DeliveryDateFormatted,
		PR.ReferenceCode,
		PR.Address,
		PR.AddressCountryID,
		PR.Notes,
		CUR.CurrencyID,
		CUR.CurrencyName,
		COU.CountryName,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle
	FROM procurement.PurchaseRequest PR
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = PR.CurrencyID
		JOIN dropdown.Country COU ON COU.CountryID = PR.AddressCountryID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = PR.ConceptNoteID
	WHERE PR.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(PurchaseRequestID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		T.PurchaseRequestID,
		T.ConceptNoteID,
		T.RequestPersonID,
		T.PointOfContactPersonID,
		T.PointOfContactPersonNameFormatted,
		T.RequestPersonNameFormatted,
		T.DeliveryDate,
		T.DeliveryDateFormatted,
		T.ReferenceCode,
		T.Address,
		T.AddressCountryID,
		T.Notes,
		T.CurrencyID,
		T.CurrencyName,
		T.CountryName,
		T.ConceptNoteTitle
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestByPurchaseRequestID

--Begin procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestEquipmentByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purches request reports
-- ==============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ID INT, 
		BudgetTypeName Varchar(250),
		Quantity INT, 

		ItemName VARCHAR(250), 
		UnitCost NUMERIC(18,2), 
		UnitOfIssue VARCHAR(250), 
		UnitCostFormatted VARCHAR(50), 
		TotalCost NUMERIC(18,2), 
		TotalCostFormatted VARCHAR(50)
		)

	INSERT INTO @tTable
		(ID,BudgetTypeName,Quantity,ItemName,UnitCost,UnitOfIssue,UnitCostFormatted,TotalCost,TotalCostFormatted)
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID as ID,
		'Equipment' as BudgetTypeName,
		PRCNEC.Quantity,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
		JOIN procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC ON PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID
			AND PRCNEC.PurchaseRequestID = @PurchaseRequestID

	UNION

	SELECT			
		CNB.ConceptNoteBudgetID as ID,
		BT.BudgetTypeName,
		PRCNB.Quantity,
		CNB.ItemName,
		CNB.UnitCost,
		CNB.UnitOfIssue,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID	
		JOIN procurement.PurchaseRequestConceptNoteBudget PRCNB ON PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(ID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		T.ID,
		T.BudgetTypeName,
		T.Quantity,
		T.ItemName,
		T.UnitCost,
		T.UnitOfIssue,
		T.UnitCostFormatted,
		T.TotalCost,
		T.TotalCostFormatted	
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

--Begin procedure reporting.GetPurchaseRequestSubContractorByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestSubContractorByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purches request reports
-- ==============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestSubContractorByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (SubContractorName VARCHAR(100), Address VARCHAR(200), PrimaryContactPhone VARCHAR(20))

	INSERT INTO @tTable
		(SubContractorName, Address, PrimaryContactPhone)
	SELECT
		SC.SubContractorName, 
		SC.Address, 
		SC.PrimaryContactPhone
	FROM procurement.PurchaseRequestSubContractor PRSC
		JOIN dbo.SubContractor SC ON SC.SubContractorID = PRSC.SubContractorID
			AND PRSC.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(SubContractorName, Address, PrimaryContactPhone)
		VALUES
			(NULL, NULL, NULL)

		END
	--ENDIF

	SELECT
		T.SubContractorName, 
		T.Address, 
		T.PrimaryContactPhone
	FROM @tTable T
	ORDER BY T.SubContractorName

END
GO
--End procedure reporting.GetPurchaseRequestSubContractorByPurchaseRequestID

--Begin procedure reporting.GetSerialNumberTracker
EXEC Utility.DropObject 'reporting.GetSerialNumberTracker'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data from the procurement.EquipmentInventory table
-- =========================================================================================
CREATE PROCEDURE reporting.GetSerialNumberTracker

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		dbo.FormatConceptNoteReferenceCode(PEI.ConceptNoteID) AS ReferenceCode,
		dbo.FormatContactNameByContactID(CNCE.ContactID,'LastFirst') as RecipientNameFormatted,
		CON.Gender, 
		CON.EmployerName, 
		CON.City,
		CON.GovernmentIDNumber,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		PEC.ItemName,
		PEI.SerialNumber,
		PEI.IMEIMACAddress,
		PEI.Sim,
		PEI.BudgetCode,
		PEI.UnitCost,
		(SELECT ConceptNoteTypeName FROM dropdown.ConceptNoteType  CNT WHERE CN.ConceptNoteTypeID = CNT.ConceptNoteTypeID ) as SubType
	FROM procurement.EquipmentInventory   PEI
		JOIN procurement.EquipmentCatalog     PEC  ON PEI.EquipmentCatalogID    = PEC.EquipmentCatalogID
		JOIN dbo.ConceptNoteContactEquipment  CNCE ON CNCE.EquipmentInventoryID = PEI.EquipmentInventoryID
		JOIN dbo.Contact                      CON  ON CON.ContactID             = CNCE.ContactID
		JOIN dbo.ConceptNote                  CN   ON CN.ConceptNoteID          = PEI.ConceptNoteID
	ORDER BY PEI.ConceptNoteID DESC

END
GO
--End procedure reporting.GetSerialNumberTracker

--Begin procedure workflow.GetConceptNoteWorkflowData
EXEC Utility.DropObject 'workflow.GetConceptNoteWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A procedure to return workflow data from the dbo.ConceptNote table
--
-- Author:			Todd Pires
-- Create date:	2015.04.02
-- Description:	Tweaked the EventAction strings
-- ===============================================================================

CREATE PROCEDURE workflow.GetConceptNoteWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'ConceptNote'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Disapproved'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Advanced workflow'
			WHEN EL.EventCode = 'update'
			THEN 'Updated'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetConceptNoteWorkflowData