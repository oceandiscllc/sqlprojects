-- File Name:	Build-1.63 File 01 - AJACS.sql
-- Build Key:	Build-1.63 File 01 - AJACS - 2016.07.13 16.39.33

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.GetArabicProvinceNameByProvinceID
--		dbo.StripHTML
--
-- Procedures:
--		dbo.CloneConceptNote
--		dbo.GetCommunityAssetLocations
--		dbo.GetCommunityRoundByCommunityRoundID
--		dropdown.GetConceptNoteWorkflowData
--		reporting.GetCashHandoverReport
--		reporting.GetConceptNotes
--		reporting.GetJusticeCashHandoverReport
--		reporting.GetJusticeStipendActivitySummaryPayments
--		reporting.GetJusticeStipendActivitySummaryPeople
--		reporting.GetJusticeStipendActivityVariancePayments
--		reporting.GetJusticeStipendActivityVariancePeople
--		reporting.GetJusticeStipendPaymentReport
--		utility.AddColumn
--		utility.DropConstraintsAndIndexes
--		utility.SetDefaultConstraint
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.GetArabicProvinceNameByProvinceID
EXEC Utility.DropObject 'dbo.GetArabicProvinceNameByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A function to return an Arabic Province Name for a specific ProvinceID
-- ===================================================================================
CREATE FUNCTION dbo.GetArabicProvinceNameByProvinceID
(
@ProvinceID INT 
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cProvinceName NVARCHAR(250) = ''
	
	SELECT @cProvinceName =  P.ArabicProvinceName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID
	
	RETURN ISNULL(@cProvinceName, '')

END
GO
--End function dbo.GetArabicProvinceNameByProvinceID

--Begin function dbo.StripHTML
EXEC Utility.DropObject 'dbo.StripHTML'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A function to return an Arabic Province Name for a specific ProvinceID
-- ===================================================================================
CREATE FUNCTION dbo.StripHTML
(
@HTMLText VARCHAR(MAX)
)

RETURNS VARCHAR(MAX) AS
BEGIN
  DECLARE @Start INT
  DECLARE @End INT
  DECLARE @Length INT
  SET @Start = CHARINDEX('<',@HTMLText)
  SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
  SET @Length = (@End - @Start) + 1
  WHILE @Start > 0 AND @End > 0 AND @Length > 0
  BEGIN
      SET @HTMLText = STUFF(@HTMLText,@Start,@Length,'')
      SET @Start = CHARINDEX('<',@HTMLText)
      SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
      SET @Length = (@End - @Start) + 1
  END

  RETURN LTRIM(RTRIM(@HTMLText))

END
GO
--End function dbo.StripHTML

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT,
@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,
		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,
		'Clone of:  ' + C.Title,
		C.VettingRequirements,
		1
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@nConceptNoteID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.WorkflowID = @WorkflowID

	INSERT INTO	dbo.ConceptNoteAmmendment
		(ConceptNoteID, AmmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Ammendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Ammendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.GetCommunityAssetLocations
EXEC Utility.DropObject 'dbo.GetCommunityAssetLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	A stored procedure to get location data from the dbo.CommunityAsset table
--
-- Author:			Todd Pires
-- Create date:	2016.07.05
-- Description:	Bug fix to get community assets assigned to communities in a specified province
-- ============================================================================================
CREATE PROCEDURE dbo.GetCommunityAssetLocations
@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CA.Location.STAsText() AS Location,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND 
				(
				IsNull(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(CA.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR CA.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR CA.ProvinceID = @ProvinceID
					OR EXISTS
						(
						SELECT 1
						FROM dbo.Community C
						WHERE C.ProvinceID = @ProvinceID
							AND C.CommunityID = CA.CommunityID
						)
				)

END
GO
--End procedure dbo.GetCommunityAssetLocations

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID,
		C.CommunityName,
		C.ImpactDecisionID,
		C.PolicePresenceCategoryID,
		C.Population,
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CR.ActivitiesOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.ActivityImplementationEndDate,
		dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
		CR.ActivityImplementationStartDate,
		dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
		CR.ApprovedActivityCount,
		CR.AreaManagerPersonID,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		CR.CommunityCode,
		CR.CommunityID,
		CR.CommunityRoundCivilDefenseCoverageID,
		CR.CommunityRoundGroupID,
		CR.CommunityRoundID,
		CR.CommunityRoundJusticeActivityID,
		CR.CommunityRoundOutput11StatusID,
		CR.CommunityRoundOutput12StatusID,
		CR.CommunityRoundOutput13StatusID,
		CR.CommunityRoundOutput14StatusID,
		CR.CommunityRoundPreAssessmentStatusID,
		CR.CommunityRoundRAPInfoID,
		CR.CommunityRoundRoundID,
		CR.CommunityRoundTamkeenID,
		CR.CSAP2PeerReviewMeetingDate,
		dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
		CR.CSAP2SubmittedDate,
		dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
		CR.CSAPBeginDevelopmentDate,
		dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
		CR.CSAPCommunityAnnouncementDate,
		dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
		CR.CSAPFinalizedDate,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		CR.CSAPProcessStartDate,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		CR.FemaleQuestionnairCount,
		CR.FieldFinanceOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerContactName,
		CR.FieldLogisticOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerContactName,
		CR.FieldOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficerContactName,
		CR.FinalSWOTDate,
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
		CR.InitialSCADate,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
		CR.IsActive,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.KickoffMeetingCount,
		CR.MOUDate,
		dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
		CR.NewCommunityEngagementCluster,
		CR.Preassessment,
		CR.Postassessment,
		CR.ProjectOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		CR.QuestionnairCount,
		CR.SCAFinalizedDate,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
		CR.SCAMeetingCount,
		CR.SensitizationDefineFSPRoleDate,
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
		CR.SensitizationDiscussCommMakeupDate,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
		CR.SensitizationMeetingMOUDate,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
		CR.ToRDate,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		FOHT.FieldOfficerHiredTypeID,
		FOHT.FieldOfficerHiredTypeName,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		CRES.CommunityRoundEngagementStatusID,
		CRES.CommunityRoundEngagementStatusName,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Male'  
		) AS CSWGMaleCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Female' 
		) AS CSWGFemaleCount
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.FieldOfficerHiredType FOHT ON FOHT.FieldOfficerHiredTypeID = CR.FieldOfficerHiredTypeID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
			AND CR.CommunityRoundID = @CommunityRoundID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityRound'
			AND DE.EntityID = @CommunityRoundID
			AND D.DocumentDescription IN ('RAP Report','TOR Document')
	ORDER BY D.DocumentDescription

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		C.EmployerName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,
		ISNULL(OACV12.VettingOutcomeName, 'Not Vetted') AS USVettingOutcomeName,
		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,
		ISNULL(OACV22.VettingOutcomeName, 'Not Vetted') AS UKVettingOutcomeName,
		CCSWGC.ContactCSWGClassificationID,
		CCSWGC.ContactCSWGClassificationName
	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = CV0.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C1.ContactID
					AND CRC.CommunityRoundID = @CommunityRoundID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT 
				CV12.VettingOutcomeID,
				VO1.VettingOutcomeName
			FROM dbo.ContactVetting CV12
			JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT 
				CV22.VettingOutcomeID,
				VO2.VettingOutcomeName
			FROM dbo.ContactVetting CV22
			JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
	ORDER BY 3, 1

	--Updates
	SELECT
		CRU.CommunityRoundUpdateID, 
		CRU.CommunityRoundID, 
		CRU.FOUpdates, 
		CRU.POUpdates, 
		CRU.NextStepsUpdates, 
		CRU.NextStepsProcess, 
		CRU.Risks, 
		CRU.UpdateDate,
		dbo.FormatDate(CRU.UpdateDate) AS UpdateDateFormatted
	FROM dbo.CommunityRoundUpdate CRU
	WHERE CRU.CommunityRoundID = @CommunityRoundID
	ORDER BY CRU.UpdateDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure reporting.GetCashHandoverReport
EXEC Utility.DropObject 'reporting.GetCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2015.09.28
-- Description:	Added the ArabicProvinceName column
-- ==================================================================
CREATE PROCEDURE reporting.GetCashHandoverReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ArabicProvinceName NVARCHAR(250)
	DECLARE @FullName VARCHAR(200) = (SELECT dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst'))
	DECLARE @PaymentMonthYear VARCHAR(20)
	DECLARE @PaymentMonth INT
	DECLARE @PaymentYear  INT
	DECLARE @ProvinceName VARCHAR(250)
	DECLARE @RunningCost INT 
	DECLARE @TotalCost INT 
	DECLARE @TotalExpenseCost INT
	
	SELECT @RunningCost = SUM(E.RunningCost)
	FROM
		(
		SELECT
			(
			SELECT CAE.ExpenseAmountAuthorized
			FROM CommunityAsset CA
				JOIN CommunityAssetUnit CAU ON  CA.CommunityAssetID = CAU.CommunityAssetUnitID
				JOIN CommunityAssetUnitExpense CAE ON   CAE.CommunityAssetUnitID = CAU.CommunityAssetUnitID
					AND paymentmonth = @PaymentMonth  
					AND paymentyear = @PaymentYear	
			) AS RunningCost
		FROM
			(
			SELECT DISTINCT 
				CSP.CommunityID
			FROM dbo.ContactStipendPayment CSP
				JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
					AND SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
			) D
		) E
	
	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT 
		@PaymentMonth = PaymentMonth, 
		@PaymentYear = PaymentYear  
	FROM dbo.ContactStipendPayment CSP 
		JOIN 
			(
			SELECT TOP 1 EntityID	
			FROM reporting.SearchResult SR 
			WHERE SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
			) D ON  D.EntityID = CSP.ContactStipendPaymentID

	SELECT 
		@TotalExpenseCost = SUM(CAUE.ExpenseAmountAuthorized) 
	FROM dbo.CommunityAssetUnitExpense CAUE
	WHERE PaymentMonth = @PaymentMonth AND PaymentYear=@PaymentYear 

	SELECT TOP 1 
		@ArabicProvinceName = P.ArabicProvinceName,
		@PaymentMonthYear = DateName(month , DateAdd(month, CSP.PaymentMonth, 0) - 1) + ' - ' + CAST(CSP.PaymentYear AS CHAR(4)),
		@ProvinceName = P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT
		@ArabicProvinceName AS ArabicProvinceName,
		@FullName AS FullName,
		@PaymentMonthYear AS PaymentMonthYear,
		@ProvinceName AS ProvinceName,
		ISNULL(@TotalCost, 0) AS TotalCost,
		ISNULL(@TotalExpenseCost, 0) AS TotalExpenseCost,
		ISNULL(@TotalCost, 0) + ISNULL(@TotalExpenseCost, 0) as CompleteCost
END
GO
--End procedure reporting.GetCashHandoverReport

--Begin procedure dropdown.GetConceptNoteWorkflowData
EXEC Utility.DropObject 'dropdown.GetConceptNoteWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the workflow.Workflow table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetConceptNoteWorkflowData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.WorkflowID, 
		T.WorkflowName
	FROM workflow.Workflow T
	WHERE (T.WorkflowID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
		AND T.EntityTypeCode = 'ConceptNote'
	ORDER BY T.WorkflowName, T.WorkflowID

END
GO
--End  procedure dropdown.GetConceptNoteWorkflowData

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	 (SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote'),
	 CNS.ConceptNoteStatusCode,
	 CN.WorkflowStepNumber,


		CN.ConceptNoteID,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,
		CN.TaskCode,
		
		CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
							ELSE 'Closedown'
						END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
							ELSE 'Closedown'
						END
				END

			ELSE
			'Bad WorkFlow'

		END AS Status,

		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1
			SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,

		CASE 
			WHEN 
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			
			
			 IN ('Cancelled','Development')	
			THEN FORMAT(0, 'C', 'en-us') 
			WHEN CN.TaskCode ='Closed'						
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			ELSE FORMAT((IsNull(OACNB.TotalBudget,0)+ IsNull(OACNA.ConceptNoteAmmendmentTotal,0) + isnull(OACNCE.ConceptNoteEquimentTotal, 0)), 'C', 'en-us') 
		END AS TotalBudget,
		
		CASE 
			WHEN 
			
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			
			 IN ('Cancelled','Development')	
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us') 
		END AS TotalSpent,
		
		CASE 
			WHEN 
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			 IN ('Closed','Cancelled','OnHold','Development')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT(((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmmendmentTotal,0)+ isnull(OACNCE.ConceptNoteEquimentTotal, 0)) - (IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0))), 'C', 'en-us') 
		END AS TotalRemaining

	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB
		OUTER APPLY
			(
			SELECT
				SUM(CNF.drAmt) - SUM(CNF.CrAmt) as ConceptNoteFinanceTotal
			FROM dbo.ConceptNoteFinance CNF
			WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
			) OACNF
		OUTER APPLY
			(
			SELECT
				SUM(CNA.Cost) as ConceptNoteAmmendmentTotal
			FROM dbo.ConceptNoteAmmendment CNA
			WHERE CNA.ConceptNoteID = CN.ConceptNoteID
			) OACNA
		OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue) as ConceptNoteEquimentTotal
			FROM dbo.ConceptNoteEquipmentCatalog CNCE
				JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					AND CNCE.ConceptNoteID = CN.ConceptNoteID
			) OACNCE
			ORDER BY 9 DESC

END
GO
--End procedure reporting.GetConceptNotes

--Begin procedure reporting.GetJusticeCashHandoverReport
EXEC Utility.DropObject 'reporting.GetJusticeCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A stored procedure to data for the cash handover form
-- ==================================================================

CREATE PROCEDURE reporting.GetJusticeCashHandoverReport

@PersonID INT,	 
@PaymentMonth INT,	 
@PaymentYear INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RunningCost INT ,@nContactCount INT, @nStipendAmountAuthorized NUMERIC(18, 2), @cStipendName nvarchar(250), @nCommunityAssetID INT, @nCommunityAssetUnitID INT,@TotalCost INT = 0



	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,

			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.CommunityAssetID > 0
				AND C.CommunityAssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.CommunityAssetID = @nCommunityAssetID AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,CommunityAssetID,CommunityAssetUnitID) VALUES (@PersonID,@nCommunityAssetID,@nCommunityAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
		SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
		SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
		SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
		SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
		SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.CommunityAssetID = @nCommunityAssetID
			AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	

	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID



	SELECT 

		@RunningCost =SUM(CAUE.ExpenseAmountAuthorized)
		--SUM(CAUE.ExpenseAmountAuthorized)
		--CAUE.ExpenseAmountAuthorized,
		--CAUE.CommunityAssetUnitID,
		--CA.CommunityAssetID


	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @PaymentMonth AND CAUE.PaymentYear =@PaymentYear




		SELECT
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END AS ProvinceName,
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetArabicProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetArabicProvinceNameByProvinceID(CA.ProvinceID)
		END AS ArabicProvinceName,
		dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst') AS FullName,
		DateName(month , DateAdd(month,@PaymentMonth, 0) - 1) + ' - ' + CAST(@PaymentYear AS CHAR(4)) as PaymentMonthYear,
		ISNULL(ISNULL(@TotalCost, 0), 0) + ISNULL(@RunningCost, 0) as CompleteCost,
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @PaymentMonth AND CAUE.PaymentYear =@PaymentYear


END
GO
--End procedure reporting.GetJusticeCashHandoverReport

--Begin procedure reporting.GetJusticeStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPayments

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UPPER(LEFT(DateName( month , DateAdd( month , D.PaymentMonth , -1 ) ),3)) + ' - ' + CAST(D.PaymentYear as varchar(5)) AS YearMonthFormatted,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		SUM(D.StipendAmountPaid) AS StipendAmountPaid,
		SUM(D.StipendAmountAuthorized) AS StipendAmountAuthorized,
		(SELECT SUM(ExpenseAmountAuthorized)  FROM dbo.CommunityAssetUnitExpense CAUE 
					JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = CAUE.CommunityAssetUnitID
					JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = CAU.CommunityAssetID AND CAUE.ProvinceID =d.ProvinceID AND CAUE.PaymentYear= D.PaymentYear AND CAUE.PaymentMonth=D.PaymentMonth) as StipendExpenseAmount
	FROM
		(
		SELECT
			CSP.StipendAmountPaid,
			CSP.StipendAmountAuthorized,
			CSP.ContactID,
			CSP.ProvinceID,
			CSP.PaymentYear,
			CSP.PaymentMonth,
		
			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			cast(CSP.PaymentYear as varchar(50)) + cast(CSP.PaymentMonth as varchar(50)) AS YearMonth,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID 
		AND StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND StipendTypeCode ='JusticeStipend' 
		) D
	GROUP BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.provinceID
	ORDER BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.ProvinceID ASC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPayments

--Begin procedure reporting.GetJusticeStipendActivitySummaryPeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPeople

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(PVT.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(PVT.YearMonth, 4) AS YearMonthFormatted,
		(SELECT P.ProvinceID FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		*
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			YEAR(CSP.StipendPaidDate) * 100 + MONTH(CSP.StipendPaidDate) AS YearMonth
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
			AND CSP.StipendPaidDate IS NOT NULL
		GROUP BY CSP.StipendPaidDate, CSP.StipendName
		) AS D
	PIVOT
		(
		MAX(D.StipendNameCount)
		FOR D.StipendName IN
			(
			[Rank 1],
			[Rank 2],
			[Rank 3],
			[Rank 4],
			[Rank 5]
			)
		) AS PVT
	ORDER BY PVT.YearMonth DESC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPeople

--Begin procedure reporting.GetJusticeStipendActivityVariancePayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePayments

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendAmountAuthorizedTotal,
		B.StipendAmountPaidTotal,
		   B.StipendAmountPaidTotal - A.StipendAmountAuthorizedTotal  AS Variance
	FROM
		(
			SELECT 
		SUM(D.StipendAmountAuthorizedTotal) as StipendAmountAuthorizedTotal,
		D.StipendName,
		D.DisplayOrder
		FROM (
		SELECT 
			CSP.StipendAmountAuthorized,
			CASE 
			WHEN CSP.StipendAuthorizedDate IS NULL THEN 0 
			ELSE CSP.StipendAmountAuthorized
			END as StipendAmountAuthorizedTotal,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
				AND CSP.ProvinceID = @ProvinceID
				 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
				 ) D
		GROUP BY D.StipendName, D.DisplayOrder
		) A
		JOIN
			(
	SELECT 
		SUM(D.StipendAmountPaidTotal) as StipendAmountPaidTotal,
		D.StipendName
		FROM (
			SELECT 
			CASE 
			WHEN CSP.StipendPaidDate IS NULL THEN 0 
			ELSE CSP.StipendAmountPaid
			END as StipendAmountPaidTotal,
			CSP.StipendName

			FROM dbo.ContactStipendPayment CSP
			WHERE CSP.ProvinceID = @ProvinceID
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
			 )D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePayments

--Begin procedure reporting.GetJusticeStipendActivityVariancePeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePeople

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		ISNULL(A.StipendNameCount, 0) AS AuthorizedStipendNameCount,
		ISNULL(B.StipendNameCount, 0) AS ReconciledStipendNameCount,
		ISNULL(B.StipendNameCount, 0) - ISNULL(A.StipendNameCount, 0) AS Variance
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' AND ProvinceID =@ProvinceID 
			 AND StipendAuthorizedDate IS NOT NULL
		GROUP BY CSP.StipendName, S.DisplayOrder
		) A
		LEFT JOIN
			(
			SELECT

			SUM(StipendCount)AS StipendNameCount,
			D.StipendName
			FROM
						(SELECT 
							CASE
							WHEN CSP.StipendPaidDate IS NULL THEN  0
							ELSE COUNT(	CSP.StipendName)
							END as StipendCount,
							CSP.StipendName
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.ProvinceID = @ProvinceID
						 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend'  
						AND StipendPaidDate IS NOT NULL
						GROUP BY CSP.StipendName,csp.StipendPaidDate) D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePeople

--Begin procedure reporting.GetJusticeStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetJusticeStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendPaymentReport

@PersonID INT = 0, 
@Year int = 0, 
@Month int = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nCommunityAssetID INT	
	DECLARE @nCommunityAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,

			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.CommunityAssetID > 0
				AND C.CommunityAssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.CommunityAssetID = @nCommunityAssetID AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,CommunityAssetID,CommunityAssetUnitID) VALUES (@PersonID,@nCommunityAssetID,@nCommunityAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
		SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
		SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
		SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
		SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
		SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.CommunityAssetID = @nCommunityAssetID
			AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
			SP.[Total Count] ,
			SP.[Total Stipend],
			SP.[Rank 1],
			SP.[Rank 2],
			SP.[Rank 3],
			SP.[Rank 4],
			SP.[Rank 5],
			SP.[Rank 1 Count],
			SP.[Rank 2 Count],
			SP.[Rank 3 Count],
			SP.[Rank 4 Count],
			SP.[Rank 5 Count],
			sp.stipendpaymentid, 
			sp.personid,
			sp.communityassetid,
			sp.CommunityAssetunitID,
	CAUE.ExpenseAmountAuthorized AS [Running Costs],
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetCommunityNameByCommunityID(CA.CommunityID)
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center

	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @Month AND CAUE.PaymentYear =@Year

END
GO
--End procedure reporting.GetJusticeStipendPaymentReport

--Begin procedure utility.AddColumn
EXEC Utility.DropObject 'utility.AddColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD [' + @ColumnName + '] ' + @DataType
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.DropConstraintsAndIndexes
EXEC Utility.DropObject 'utility.DropConstraintsAndIndexes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropConstraintsAndIndexes

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + DC.Name + ']' AS SQL
		FROM sys.default_constraints DC
		WHERE DC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + FK.Name + ']' AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @TableName AS SQL
		FROM sys.indexes I
		WHERE I.object_ID = OBJECT_ID(@TableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor
END
GO
--End  procedure utility.DropConstraintsAndIndexes

--Begin procedure utility.SetDefaultConstraint
EXEC Utility.DropObject 'utility.SetDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date: 2013.10.12
-- Description:	A helper stored procedure for table upgrades
-- ===============================================================================================================================
CREATE PROCEDURE utility.SetDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX),
@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ''' + @Default + ''' WHERE [' + @ColumnName + '] IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ' + @Default + ' WHERE [' + @ColumnName + '] IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN [' + @ColumnName + '] ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + REPLACE(@ColumnName, ' ', '_')
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR [' + @ColumnName + ']'
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR [' + @ColumnName + ']'
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.SetDefaultConstraint


--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dropdown.Stipend
DECLARE @nRank250ID INT

SELECT TOP 1 @nRank250ID = S.StipendID
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'JusticeStipend'
	AND S.StipendAmount = 250
ORDER BY S.StipendID

UPDATE C
SET C.StipendID = @nRank250ID
FROM dbo.Contact C
	JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
		AND S.StipendTypeCode = 'JusticeStipend'
		AND S.StipendAmount = 250

DELETE S
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'JusticeStipend'
	AND S.StipendAmount = 250
	AND S.StipendID <> @nRank250ID

UPDATE S
SET S.StipendName = 
	CASE
		WHEN S.StipendAmount = 300
		THEN 'Rank 1'
		WHEN S.StipendAmount = 250
		THEN 'Rank 2'
		WHEN S.StipendAmount = 200
		THEN 'Rank 3'
		WHEN S.StipendAmount = 125
		THEN 'Rank 4'
		WHEN S.StipendAmount = 100
		THEN 'Rank 5'
	END
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'JusticeStipend'
GO
--End table dropdown.Stipend

--Begin table reporting.StipendPayment
DECLARE @TableName VARCHAR(250) = 'reporting.StipendPayment'

EXEC utility.DropConstraintsAndIndexes @TableName

EXEC utility.AddColumn @TableName, 'Rank 1', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 1 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 2', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 2 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 3', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 3 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 4', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 4 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 5', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 5 Count', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'Adjutant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Adjutant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain Doctor', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain Doctor Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel Doctor', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel Doctor Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Command', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Command Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Officer', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Officer Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Policeman', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Policeman Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Adjutant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Adjutant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Lieutenant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Lieutenant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Sergeant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Sergeant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'General', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'General Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Lieutenant Colonel', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Lieutenant Colonel Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Major', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Major Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Policeman', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Policeman Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 1', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 1 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 2', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 2 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 3', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 3 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 4', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 4 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 5', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 5 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Sergeant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Sergeant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Total Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Total Stipend', 'NUMERIC(18, 0)', 0

EXEC utility.SetIndexClustered @TableName, 'IX_StipendPayment', 'PersonID,StipendPaymentID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'StipendPaymentID'
GO
--End table reporting.StipendPayment


--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Community', 'Community', 0
GO
EXEC utility.SavePermissionableGroup 'CommunityRound', 'CommunityRound', 0
GO
EXEC utility.SavePermissionableGroup 'CommunityRoundActivity', 'CommunityRoundActivity', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contact', 0
GO
EXEC utility.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'Implementation', 'Implementation', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Insight & Understanding', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Province', 'Province', 0
GO
EXEC utility.SavePermissionableGroup 'RapidAssessments', 'Rapid Assessments', 0
GO
EXEC utility.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflows', 'Workflows', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Data Export', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'DataImport', 'Default', NULL, 'DataImport.Default', 'Data Import', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'AddUpdate', NULL, 'EmailTemplateAdministration.AddUpdate', 'Add / edit an email template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'List EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'View', NULL, 'EmailTemplateAdministration.View', 'View EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'List EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Exports', 'BusinessLicenseReport', NULL, 'Exports.BusinessLicenseReport', 'Business License Report Exports', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'CanRecieveEmail', 'CanRecieveEmail', 'Main.CanRecieveEmail.CanRecieveEmail', 'User Can Receive Email', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors SiteConfiguration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'Delete', NULL, 'Permissionable.Delete', 'Delete a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'List PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'Delete', NULL, 'Person.Delete', 'Delete a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'ViewPermissionables', NULL, 'Person.ViewPermissionables', 'View list of permissionables on view page', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'List the server setup keys', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Community', 'AddUpdate', NULL, 'Community.AddUpdate', 'Add / edit a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'List Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'View the analysis tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Export', 'Community.View.Export', 'Export Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'ExportEquipmentDistribution', 'Community.View.ExportEquipmentDistribution', 'Export Equipment Distributions Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'Implementation Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'View the information tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'AddUpdate', NULL, 'CommunityRound.AddUpdate', 'Add / edit a community round', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'List', NULL, 'CommunityRound.List', 'View the list of community rounds', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'View', NULL, 'CommunityRound.View', 'View a community round', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'AddUpdate', NULL, 'CommunityRoundActivity.AddUpdate', 'CommunityRoundActivity.AddUpdate', 0, 0, 'CommunityRoundActivity'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'List', NULL, 'CommunityRoundActivity.List', 'CommunityRoundActivity.List', 0, 0, 'CommunityRoundActivity'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'View', NULL, 'CommunityRoundActivity.View', 'CommunityRoundActivity.View', 0, 0, 'CommunityRoundActivity'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'CETeam', 'Contact.AddUpdate.CETeam', 'Add / edit contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'FieldStaff', 'Contact.AddUpdate.FieldStaff', 'Add / edit contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'IO4', 'Contact.AddUpdate.IO4', 'Add / edit contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeOther', 'Contact.AddUpdate.JusticeOther', 'Add / edit contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeStipend', 'Contact.AddUpdate.JusticeStipend', 'Add / edit contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersStakeholder', 'Contact.AddUpdate.PartnersStakeholder', 'Add / edit contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersSupplierVendor', 'Contact.AddUpdate.PartnersSupplierVendor', 'Add / edit contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceOther', 'Contact.AddUpdate.PoliceOther', 'Add / edit contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceStipend', 'Contact.AddUpdate.PoliceStipend', 'Add / edit contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'ProjectStaff', 'Contact.AddUpdate.ProjectStaff', 'Add / edit contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'SubContractors', 'Contact.AddUpdate.SubContractors', 'Add / edit contacts of type Sub-Contractor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'ImportPayees', NULL, 'Contact.ImportPayees', 'Allows import of payees in payment system', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', NULL, 'Contact.JusticePaymentList', 'Allows view of justice stipends payments', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'CETeam', 'Contact.List.CETeam', 'Include contacts of type CE Team in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ExportPayees', 'Contact.List.ExportPayees', 'Export payees from the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'FieldStaff', 'Contact.List.FieldStaff', 'Include contacts of type Field Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'IO4', 'Contact.List.IO4', 'Include contacts of type IO4 in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeOther', 'Contact.List.JusticeOther', 'Include contacts of type Justice Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeStipend', 'Contact.List.JusticeStipend', 'Include contacts of type Justice Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersStakeholder', 'Contact.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersSupplierVendor', 'Contact.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceOther', 'Contact.List.PoliceOther', 'Include contacts of type Police Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceStipend', 'Contact.List.PoliceStipend', 'Include contacts of type Police Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ProjectStaff', 'Contact.List.ProjectStaff', 'Include contacts of type Project Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'SubContractors', 'Contact.List.SubContractors', 'Include contacts of type Sub-Contractor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', NULL, 'Contact.PaymentList', 'View the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'CashHandOverExport', 'Contact.PaymentList.CashHandOverExport', 'Export the cash handover report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'OpFundsReport', 'Contact.PaymentList.OpFundsReport', 'Export the op funds report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendActivity', 'Contact.PaymentList.StipendActivity', 'Export the stipend activity report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendPaymentReport', 'Contact.PaymentList.StipendPaymentReport', 'Export the stipend payment report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'CETeam', 'Contact.View.CETeam', 'View contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'FieldStaff', 'Contact.View.FieldStaff', 'View contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'IO4', 'Contact.View.IO4', 'View contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeOther', 'Contact.View.JusticeOther', 'View contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeStipend', 'Contact.View.JusticeStipend', 'View contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersStakeholder', 'Contact.View.PartnersStakeholder', 'View contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersSupplierVendor', 'Contact.View.PartnersSupplierVendor', 'View contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceOther', 'Contact.View.PoliceOther', 'View contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceStipend', 'Contact.View.PoliceStipend', 'View contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'ProjectStaff', 'Contact.View.ProjectStaff', 'View contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'SubContractors', 'Contact.View.SubContractors', 'View contacts of type Sub-Contractors', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'VettingMoreInfo', 'Contact.View.VettingMoreInfo', 'View the more info button on the vetting history data table', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'CETeam', 'Vetting.List.CETeam', 'Include contacts of type CE Team in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'Export', 'Vetting.List.Export', 'Export the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'FieldStaff', 'Vetting.List.FieldStaff', 'Include contacts of type Field Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'IO4', 'Vetting.List.IO4', 'Include contacts of type IO4 in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeOther', 'Vetting.List.JusticeOther', 'Include contacts of type Justice Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeStipend', 'Vetting.List.JusticeStipend', 'Include contacts of type Justice Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersStakeholder', 'Vetting.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersSupplierVendor', 'Vetting.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceOther', 'Vetting.List.PoliceOther', 'Include contacts of type Police Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceStipend', 'Vetting.List.PoliceStipend', 'Include contacts of type Police Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'ProjectStaff', 'Vetting.List.ProjectStaff', 'Include contacts of type Project Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'SubContractors', 'Vetting.List.SubContractors', 'Include contacts of type Sub-Contractor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeChangeNotification', 'Vetting.List.VettingOutcomeChangeNotification', 'Recieve an email when a vetting outcome has changed for one or more contacts', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeConsider', 'Vetting.List.VettingOutcomeConsider', 'Assign a vetting outcome of "Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeDoNotConsider', 'Vetting.List.VettingOutcomeDoNotConsider', 'Assign a vetting outcome of "Do Not Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeInsufficientData', 'Vetting.List.VettingOutcomeInsufficientData', 'Assign a vetting outcome of "Insufficient Data"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeNotVetted', 'Vetting.List.VettingOutcomeNotVetted', 'Assign a vetting outcome of "Not Vetted"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomePendingInternalReview', 'Vetting.List.VettingOutcomePendingInternalReview', 'Assign a vetting outcome of "Pending Internal Review"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeSubmittedforVetting', 'Vetting.List.VettingOutcomeSubmittedforVetting', 'Assign a vetting outcome of "Submitted for Vetting"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUK', 'Vetting.List.VettingTypeUK', 'Update UK vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUS', 'Vetting.List.VettingTypeUS', 'Update US vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'Notification', 'ExpirationCountEmail', 'Vetting.Notification.ExpirationCountEmail', 'Recieve the monthly vetting expiration counts e-mail', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', NULL, 'Document.AddUpdate', '', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '004', 'Document.AddUpdate.004', 'Add / edit 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '100', 'Document.AddUpdate.100', 'Add / edit 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '101', 'Document.AddUpdate.101', 'Add / edit 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '102', 'Document.AddUpdate.102', 'Add / edit 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '103', 'Document.AddUpdate.103', 'Add / edit 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '104', 'Document.AddUpdate.104', 'Add / edit 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '105', 'Document.AddUpdate.105', 'Add / edit 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '106', 'Document.AddUpdate.106', 'Add / edit 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '107', 'Document.AddUpdate.107', 'Add / edit 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '108', 'Document.AddUpdate.108', 'Add / edit 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '109', 'Document.AddUpdate.109', 'Add / edit 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '301', 'Document.AddUpdate.301', 'Add / edit 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '302', 'Document.AddUpdate.302', 'Add / edit 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '303', 'Document.AddUpdate.303', 'Add / edit 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '304', 'Document.AddUpdate.304', 'Add / edit 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '306', 'Document.AddUpdate.306', 'Add / edit 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '500', 'Document.AddUpdate.500', 'Add / edit 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '501', 'Document.AddUpdate.501', 'Add / edit 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '502', 'Document.AddUpdate.502', 'Add / edit 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '503', 'Document.AddUpdate.503', 'Add / edit 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '504', 'Document.AddUpdate.504', 'Add / edit 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '505', 'Document.AddUpdate.505', 'Add / edit 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '506', 'Document.AddUpdate.506', 'Add / edit 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '507', 'Document.AddUpdate.507', 'Add / edit 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '508', 'Document.AddUpdate.508', 'Add / edit 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '509', 'Document.AddUpdate.509', 'Add / edit 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '510', 'Document.AddUpdate.510', 'Add / edit 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '511', 'Document.AddUpdate.511', 'Add / edit 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '512', 'Document.AddUpdate.512', 'Add / edit 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '513', 'Document.AddUpdate.513', 'Add / edit 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '514', 'Document.AddUpdate.514', 'Add / edit 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '515', 'Document.AddUpdate.515', 'Add / edit 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '516', 'Document.AddUpdate.516', 'Add / edit 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '517', 'Document.AddUpdate.517', 'Add / edit 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '518', 'Document.AddUpdate.518', 'Add / edit 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '519', 'Document.AddUpdate.519', 'Add / edit 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '520', 'Document.AddUpdate.520', 'Add / edit 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '521', 'Document.AddUpdate.521', 'Add / edit 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '600', 'Document.AddUpdate.600', 'Add / edit 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '601', 'Document.AddUpdate.601', 'Add / edit 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '602', 'Document.AddUpdate.602', 'Add / edit 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '603', 'Document.AddUpdate.603', 'Add / edit 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '604', 'Document.AddUpdate.604', 'Add / edit 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '605', 'Document.AddUpdate.605', 'Add / edit 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '700', 'Document.AddUpdate.700', 'Add / edit 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '701', 'Document.AddUpdate.701', 'Add / edit 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '702', 'Document.AddUpdate.702', 'Add / edit 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '801', 'Document.AddUpdate.801', 'Add / edit 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '802', 'Document.AddUpdate.802', 'Add / edit 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '803', 'Document.AddUpdate.803', 'Add / edit 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '804', 'Document.AddUpdate.804', 'Add / edit 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '805', 'Document.AddUpdate.805', 'Add / edit 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '806', 'Document.AddUpdate.806', 'Add / edit 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '807', 'Document.AddUpdate.807', 'Add / edit 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '808', 'Document.AddUpdate.808', 'Add / edit 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '900', 'Document.AddUpdate.900', 'Add / edit 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '901', 'Document.AddUpdate.901', 'Add / edit 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '902', 'Document.AddUpdate.902', 'Add / edit 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '903', 'Document.AddUpdate.903', 'Add / edit 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '904', 'Document.AddUpdate.904', 'Add / edit 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '905', 'Document.AddUpdate.905', 'Add / edit 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '906', 'Document.AddUpdate.906', 'Add / edit 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '907', 'Document.AddUpdate.907', 'Add / edit 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '908', 'Document.AddUpdate.908', 'Add / edit 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '909', 'Document.AddUpdate.909', 'Add / edit 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '910', 'Document.AddUpdate.910', 'Add / edit 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '004', 'Document.View.004', 'View 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '100', 'Document.View.100', 'View 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '101', 'Document.View.101', 'View 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '102', 'Document.View.102', 'View 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '103', 'Document.View.103', 'View 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '104', 'Document.View.104', 'View 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '105', 'Document.View.105', 'View 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '106', 'Document.View.106', 'View 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '107', 'Document.View.107', 'View 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '108', 'Document.View.108', 'View 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '109', 'Document.View.109', 'View 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '301', 'Document.View.301', 'View 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '302', 'Document.View.302', 'View 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '303', 'Document.View.303', 'View 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '304', 'Document.View.304', 'View 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '306', 'Document.View.306', 'View 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '500', 'Document.View.500', 'View 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '501', 'Document.View.501', 'View 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '502', 'Document.View.502', 'View 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '503', 'Document.View.503', 'View 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '504', 'Document.View.504', 'View 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '505', 'Document.View.505', 'View 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '506', 'Document.View.506', 'View 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '507', 'Document.View.507', 'View 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '508', 'Document.View.508', 'View 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '509', 'Document.View.509', 'View 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '510', 'Document.View.510', 'View 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '511', 'Document.View.511', 'View 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '512', 'Document.View.512', 'View 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '513', 'Document.View.513', 'View 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '514', 'Document.View.514', 'View 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '515', 'Document.View.515', 'View 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '516', 'Document.View.516', 'View 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '517', 'Document.View.517', 'View 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '518', 'Document.View.518', 'View 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '519', 'Document.View.519', 'View 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '520', 'Document.View.520', 'View 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '521', 'Document.View.521', 'View 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '600', 'Document.View.600', 'View 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '601', 'Document.View.601', 'View 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '602', 'Document.View.602', 'View 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '603', 'Document.View.603', 'View 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '604', 'Document.View.604', 'View 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '605', 'Document.View.605', 'View 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '700', 'Document.View.700', 'View 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '701', 'Document.View.701', 'View 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '702', 'Document.View.702', 'View 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '801', 'Document.View.801', 'View 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '802', 'Document.View.802', 'View 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '803', 'Document.View.803', 'View 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '804', 'Document.View.804', 'View 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '805', 'Document.View.805', 'View 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '806', 'Document.View.806', 'View 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '807', 'Document.View.807', 'View 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '808', 'Document.View.808', 'View 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '900', 'Document.View.900', 'View 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '901', 'Document.View.901', 'View 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '902', 'Document.View.902', 'View 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '903', 'Document.View.903', 'View 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '904', 'Document.View.904', 'View 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '905', 'Document.View.905', 'View 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '906', 'Document.View.906', 'View 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '907', 'Document.View.907', 'View 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '908', 'Document.View.908', 'View 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '909', 'Document.View.909', 'View 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '910', 'Document.View.910', 'View 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateDecision', NULL, 'DonorDecision.AddUpdateDecision', 'Add / edit a donor decision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateMeeting', NULL, 'DonorDecision.AddUpdateMeeting', 'Add / edit donor meetings & actions', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'List', NULL, 'DonorDecision.List', 'List Donor Decisions, Meetings & Actions DonorDecision', 1, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'View', NULL, 'DonorDecision.View', 'View DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'AddUpdate', NULL, 'EquipmentCatalog.AddUpdate', 'Add / edit the equipment catalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'List', NULL, 'EquipmentCatalog.List', 'List EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'View', NULL, 'EquipmentCatalog.View', 'View EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Audit', NULL, 'EquipmentDistribution.Audit', 'EquipmentDistribution.Audit', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'EquipmentDistribution.Create', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'FinalizeEquipmentDistribution', NULL, 'EquipmentDistribution.FinalizeEquipmentDistribution', 'EquipmentDistribution.FinalizeEquipmentDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistributedInventory', NULL, 'EquipmentDistribution.ListDistributedInventory', 'EquipmentDistribution.ListDistributedInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistribution', NULL, 'EquipmentDistribution.ListDistribution', 'EquipmentDistribution.ListDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListInventory', NULL, 'EquipmentDistribution.ListInventory', 'EquipmentDistribution.ListInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'SetDeliveryDate', NULL, 'EquipmentDistribution.SetDeliveryDate', 'EquipmentDistribution.SetDeliveryDate', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Transfer', NULL, 'EquipmentDistribution.Transfer', 'EquipmentDistribution.Transfer', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit the equipment inventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'List EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', 'Export', 'EquipmentInventory.List.Export', 'Export EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'Audit', NULL, 'EquipmentManagement.Audit', 'Audit Equipment EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'List', NULL, 'EquipmentManagement.List', 'List Equipment Locations EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'AddUpdate', NULL, 'CommunityAsset.AddUpdate', 'Add / edit a community asset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'List', NULL, 'CommunityAsset.List', 'List CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'View', NULL, 'CommunityAsset.View', 'View CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'AddUpdate', NULL, 'CommunityMemberSurvey.AddUpdate', 'Add / edit a community member survey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'View', NULL, 'CommunityMemberSurvey.View', 'View CommunityMemberSurvey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagement', 'AddUpdate', NULL, 'CommunityProvinceEngagement.AddUpdate', 'Add / edit a community province engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagementUpdate', 'Export', NULL, 'CommunityProvinceEngagementUpdate.Export', 'Export CommunityProvinceEngagementUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'FIFUpdate', 'View', NULL, 'FIFUpdate.View', 'View FIFUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Justice', 'AddUpdate', NULL, 'Justice.AddUpdate', 'Add / edit a justice report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'AddUpdate', NULL, 'KeyEvent.AddUpdate', 'Add / edit a key event', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'List', NULL, 'KeyEvent.List', 'List KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'View', NULL, 'KeyEvent.View', 'View KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'PoliceEngagement', 'AddUpdate', NULL, 'PoliceEngagement.AddUpdate', 'Add / edit a police engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'AddUpdate', NULL, 'Project.AddUpdate', 'Add / edit a project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'List', NULL, 'Project.List', 'List Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'View', NULL, 'Project.View', 'View Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'AddUpdate', NULL, 'Atmospheric.AddUpdate', 'Add / edit an atmospheric report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'List Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'AddUpdate', NULL, 'Finding.AddUpdate', 'Add / edit a finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'List Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'AddUpdate', NULL, 'Force.AddUpdate', 'Add / edit a force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'List Forces Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'AddUpdate', NULL, 'Incident.AddUpdate', 'Add / edit an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'List Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'AddUpdate', NULL, 'Recommendation.AddUpdate', 'Add / edit a recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'List Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', 'Export', 'Recommendation.List.Export', 'Export Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Add', NULL, 'RequestForInformation.Add', 'Add a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'List RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', 'Export', 'RequestForInformation.List.Export', 'Export RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', NULL, 'RequestForInformation.Update', 'Edit a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', 'Amend', 'RequestForInformation.Update.Amend', 'Amend a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', NULL, 'Risk.AddUpdate', 'Add / edit a risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'List Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', 'Export', 'Risk.List.Export', 'Export Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', NULL, 'SpotReport.AddUpdate', 'Add / edit a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', 'Amend', 'SpotReport.AddUpdate.Amend', 'Amend a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'List SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Approved', 'SpotReport.View.Approved', 'Approved SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Export', 'SpotReport.View.Export', 'Export SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'InWork', 'SpotReport.View.InWork', 'In Work SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Add / edit a survey question', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Add / edit a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Administer a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Export Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListQuestions', NULL, 'SurveyManagement.ListQuestions', 'Questions List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveyResponses', NULL, 'SurveyManagement.ListSurveyResponses', 'List Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Surveys List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'View Questions SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'View Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', 'Export', 'WeeklyReport.AddUpdate.Export', 'Export the weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'View', NULL, 'WeeklyReport.View', 'View the completed weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagement', 'Export', NULL, 'CommunityProvinceEngagement.Export', 'Export the engagement report', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'AddUpdate', NULL, 'Indicator.AddUpdate', 'Add / edit an indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'List', NULL, 'Indicator.List', 'List Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'View', NULL, 'Indicator.View', 'View Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'AddUpdate', NULL, 'IndicatorType.AddUpdate', 'Add / edit an indicatortype', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'List', NULL, 'IndicatorType.List', 'List IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'View', NULL, 'IndicatorType.View', 'View IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'AddUpdate', NULL, 'Milestone.AddUpdate', 'Add / edit a milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'List', NULL, 'Milestone.List', 'List Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'View', NULL, 'Milestone.View', 'View Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'AddUpdate', NULL, 'Objective.AddUpdate', 'Add / edit an objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', NULL, 'Objective.ChartList', 'M & E Overview Charts Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'List', NULL, 'Objective.List', 'List Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage Objectives & Indicators Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'Overview Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'View', NULL, 'Objective.View', 'View Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', NULL, 'ConceptNote.AddUpdate', 'Add / edit a concep nNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'ExportConceptNoteBudget', 'ConceptNote.AddUpdate.ExportConceptNoteBudget', 'Export Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'Finances', 'ConceptNote.AddUpdate.Finances', 'Add / edit activity finances', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', NULL, 'ConceptNote.List', 'List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', 'Export', 'ConceptNote.List.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', NULL, 'ConceptNote.VettingList', 'Vetting List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', 'ExportVetting', 'ConceptNote.VettingList.ExportVetting', 'Export Vetting ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', NULL, 'ConceptNote.View', 'View ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'Export', 'ConceptNote.View.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'ViewBudget', 'ConceptNote.View.ViewBudget', 'View Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'AddUpdate', NULL, 'ConceptNoteContactEquipment.AddUpdate', 'Add / edit equipment associated with a concept note', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'FinalizeEquipmentDistribution', NULL, 'ConceptNoteContactEquipment.FinalizeEquipmentDistribution', 'Finalize Equipment Distribution ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'List', NULL, 'ConceptNoteContactEquipment.List', 'List ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', NULL, 'ConceptNoteContactEquipment.View', 'View ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', 'Export', 'ConceptNoteContactEquipment.View.Export', 'Export ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'AddUpdate', NULL, 'License.AddUpdate', 'Add / edit a license', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'List', NULL, 'License.List', 'List License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'View', NULL, 'License.View', 'View License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'AddUpdate', NULL, 'LicenseEquipmentCatalog.AddUpdate', 'Add / edit the license equipment catalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', NULL, 'LicenseEquipmentCatalog.List', 'List LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', 'Export', 'LicenseEquipmentCatalog.List.Export', 'Export LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'View', NULL, 'LicenseEquipmentCatalog.View', 'View LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'AddUpdate', NULL, 'PurchaseRequest.AddUpdate', 'Add / edit a purchase request', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'List', NULL, 'PurchaseRequest.List', 'List PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', NULL, 'PurchaseRequest.View', 'View PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', 'Export', 'PurchaseRequest.View.Export', 'Export PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'AddUpdate', NULL, 'ProgramReport.AddUpdate', 'Add / edit a program report', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'List', NULL, 'ProgramReport.List', 'List ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', NULL, 'ProgramReport.View', 'View ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', 'Export', 'ProgramReport.View.Export', 'Export ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'Province', 'AddUpdate', NULL, 'Province.AddUpdate', 'Add / edit a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'List', NULL, 'Province.List', 'List Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', NULL, 'Province.View', 'View Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Analysis', 'Province.View.Analysis', 'View the analysis tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'ExportEquipmentDistribution', 'Province.View.ExportEquipmentDistribution', 'Export Equipment Distributions Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Implementation', 'Province.View.Implementation', 'Implementation Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Information', 'Province.View.Information', 'View the information tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'DailyReport', 'AddUpdate', NULL, 'DailyReport.AddUpdate', 'Add / edit a daily report', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'List', NULL, 'DailyReport.List', 'List DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'View', NULL, 'DailyReport.View', 'View DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'AddUpdate', NULL, 'FocusGroupSurvey.AddUpdate', 'Add / edit a focus group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'View', NULL, 'FocusGroupSurvey.View', 'View FocusGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'AddUpdate', NULL, 'KeyInformantSurvey.AddUpdate', 'Add / edit a key informant survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'View', NULL, 'KeyInformantSurvey.View', 'View KeyInformantSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', NULL, 'RAPData.List', 'List RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', 'Export', 'RAPData.List.Export', 'Export RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RapidPerceptionSurvey', 'View', NULL, 'RapidPerceptionSurvey.View', 'View RapidPerceptionSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'AddUpdate', NULL, 'StakeholderGroupSurvey.AddUpdate', 'Add / edit a stakeholder group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'View', NULL, 'StakeholderGroupSurvey.View', 'View StakeholderGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'AddUpdate', NULL, 'StationCommanderSurvey.AddUpdate', 'Add / edit a station commander survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'View', NULL, 'StationCommanderSurvey.View', 'View StationCommanderSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'AddUpdate', NULL, 'Team.AddUpdate', 'Add / edit a team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'List', NULL, 'Team.List', 'List Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'View', NULL, 'Team.View', 'View Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'SubContractor', 'AddUpdate', NULL, 'SubContractor.AddUpdate', 'Add / edit a sub-contractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'List', NULL, 'SubContractor.List', 'List SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'View', NULL, 'SubContractor.View', 'View SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / edit a class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'List Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / edit a course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'List Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the list of workflows', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflows'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP FROM permissionable.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table permissionable.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.63 File 01 - AJACS - 2016.07.13 16.39.33')
GO
--End build tracking

