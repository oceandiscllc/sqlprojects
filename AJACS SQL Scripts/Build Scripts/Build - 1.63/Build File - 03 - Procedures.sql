USE AJACS
GO

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT,
@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,
		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,
		'Clone of:  ' + C.Title,
		C.VettingRequirements,
		1
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@nConceptNoteID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.WorkflowID = @WorkflowID

	INSERT INTO	dbo.ConceptNoteAmmendment
		(ConceptNoteID, AmmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Ammendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Ammendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.GetCommunityAssetLocations
EXEC Utility.DropObject 'dbo.GetCommunityAssetLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	A stored procedure to get location data from the dbo.CommunityAsset table
--
-- Author:			Todd Pires
-- Create date:	2016.07.05
-- Description:	Bug fix to get community assets assigned to communities in a specified province
-- ============================================================================================
CREATE PROCEDURE dbo.GetCommunityAssetLocations
@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CA.Location.STAsText() AS Location,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND 
				(
				IsNull(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(CA.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR CA.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR CA.ProvinceID = @ProvinceID
					OR EXISTS
						(
						SELECT 1
						FROM dbo.Community C
						WHERE C.ProvinceID = @ProvinceID
							AND C.CommunityID = CA.CommunityID
						)
				)

END
GO
--End procedure dbo.GetCommunityAssetLocations

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID,
		C.CommunityName,
		C.ImpactDecisionID,
		C.PolicePresenceCategoryID,
		C.Population,
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CR.ActivitiesOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.ActivityImplementationEndDate,
		dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
		CR.ActivityImplementationStartDate,
		dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
		CR.ApprovedActivityCount,
		CR.AreaManagerPersonID,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		CR.CommunityCode,
		CR.CommunityID,
		CR.CommunityRoundCivilDefenseCoverageID,
		CR.CommunityRoundGroupID,
		CR.CommunityRoundID,
		CR.CommunityRoundJusticeActivityID,
		CR.CommunityRoundOutput11StatusID,
		CR.CommunityRoundOutput12StatusID,
		CR.CommunityRoundOutput13StatusID,
		CR.CommunityRoundOutput14StatusID,
		CR.CommunityRoundPreAssessmentStatusID,
		CR.CommunityRoundRAPInfoID,
		CR.CommunityRoundRoundID,
		CR.CommunityRoundTamkeenID,
		CR.CSAP2PeerReviewMeetingDate,
		dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
		CR.CSAP2SubmittedDate,
		dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
		CR.CSAPBeginDevelopmentDate,
		dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
		CR.CSAPCommunityAnnouncementDate,
		dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
		CR.CSAPFinalizedDate,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		CR.CSAPProcessStartDate,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		CR.FemaleQuestionnairCount,
		CR.FieldFinanceOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerContactName,
		CR.FieldLogisticOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerContactName,
		CR.FieldOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficerContactName,
		CR.FinalSWOTDate,
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
		CR.InitialSCADate,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
		CR.IsActive,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.KickoffMeetingCount,
		CR.MOUDate,
		dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
		CR.NewCommunityEngagementCluster,
		CR.Preassessment,
		CR.Postassessment,
		CR.ProjectOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		CR.QuestionnairCount,
		CR.SCAFinalizedDate,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
		CR.SCAMeetingCount,
		CR.SensitizationDefineFSPRoleDate,
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
		CR.SensitizationDiscussCommMakeupDate,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
		CR.SensitizationMeetingMOUDate,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
		CR.ToRDate,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		FOHT.FieldOfficerHiredTypeID,
		FOHT.FieldOfficerHiredTypeName,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		CRES.CommunityRoundEngagementStatusID,
		CRES.CommunityRoundEngagementStatusName,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Male'  
		) AS CSWGMaleCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Female' 
		) AS CSWGFemaleCount
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.FieldOfficerHiredType FOHT ON FOHT.FieldOfficerHiredTypeID = CR.FieldOfficerHiredTypeID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
			AND CR.CommunityRoundID = @CommunityRoundID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityRound'
			AND DE.EntityID = @CommunityRoundID
			AND D.DocumentDescription IN ('RAP Report','TOR Document')
	ORDER BY D.DocumentDescription

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		C.EmployerName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,
		ISNULL(OACV12.VettingOutcomeName, 'Not Vetted') AS USVettingOutcomeName,
		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,
		ISNULL(OACV22.VettingOutcomeName, 'Not Vetted') AS UKVettingOutcomeName,
		CCSWGC.ContactCSWGClassificationID,
		CCSWGC.ContactCSWGClassificationName
	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = CV0.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C1.ContactID
					AND CRC.CommunityRoundID = @CommunityRoundID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT 
				CV12.VettingOutcomeID,
				VO1.VettingOutcomeName
			FROM dbo.ContactVetting CV12
			JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT 
				CV22.VettingOutcomeID,
				VO2.VettingOutcomeName
			FROM dbo.ContactVetting CV22
			JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
	ORDER BY 3, 1

	--Updates
	SELECT
		CRU.CommunityRoundUpdateID, 
		CRU.CommunityRoundID, 
		CRU.FOUpdates, 
		CRU.POUpdates, 
		CRU.NextStepsUpdates, 
		CRU.NextStepsProcess, 
		CRU.Risks, 
		CRU.UpdateDate,
		dbo.FormatDate(CRU.UpdateDate) AS UpdateDateFormatted
	FROM dbo.CommunityRoundUpdate CRU
	WHERE CRU.CommunityRoundID = @CommunityRoundID
	ORDER BY CRU.UpdateDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure reporting.GetCashHandoverReport
EXEC Utility.DropObject 'reporting.GetCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2015.09.28
-- Description:	Added the ArabicProvinceName column
-- ==================================================================
CREATE PROCEDURE reporting.GetCashHandoverReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ArabicProvinceName NVARCHAR(250)
	DECLARE @FullName VARCHAR(200) = (SELECT dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst'))
	DECLARE @PaymentMonthYear VARCHAR(20)
	DECLARE @PaymentMonth INT
	DECLARE @PaymentYear  INT
	DECLARE @ProvinceName VARCHAR(250)
	DECLARE @RunningCost INT 
	DECLARE @TotalCost INT 
	DECLARE @TotalExpenseCost INT
	
	SELECT @RunningCost = SUM(E.RunningCost)
	FROM
		(
		SELECT
			(
			SELECT CAE.ExpenseAmountAuthorized
			FROM CommunityAsset CA
				JOIN CommunityAssetUnit CAU ON  CA.CommunityAssetID = CAU.CommunityAssetUnitID
				JOIN CommunityAssetUnitExpense CAE ON   CAE.CommunityAssetUnitID = CAU.CommunityAssetUnitID
					AND paymentmonth = @PaymentMonth  
					AND paymentyear = @PaymentYear	
			) AS RunningCost
		FROM
			(
			SELECT DISTINCT 
				CSP.CommunityID
			FROM dbo.ContactStipendPayment CSP
				JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
					AND SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
			) D
		) E
	
	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT 
		@PaymentMonth = PaymentMonth, 
		@PaymentYear = PaymentYear  
	FROM dbo.ContactStipendPayment CSP 
		JOIN 
			(
			SELECT TOP 1 EntityID	
			FROM reporting.SearchResult SR 
			WHERE SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
			) D ON  D.EntityID = CSP.ContactStipendPaymentID

	SELECT 
		@TotalExpenseCost = SUM(CAUE.ExpenseAmountAuthorized) 
	FROM dbo.CommunityAssetUnitExpense CAUE
	WHERE PaymentMonth = @PaymentMonth AND PaymentYear=@PaymentYear 

	SELECT TOP 1 
		@ArabicProvinceName = P.ArabicProvinceName,
		@PaymentMonthYear = DateName(month , DateAdd(month, CSP.PaymentMonth, 0) - 1) + ' - ' + CAST(CSP.PaymentYear AS CHAR(4)),
		@ProvinceName = P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT
		@ArabicProvinceName AS ArabicProvinceName,
		@FullName AS FullName,
		@PaymentMonthYear AS PaymentMonthYear,
		@ProvinceName AS ProvinceName,
		ISNULL(@TotalCost, 0) AS TotalCost,
		ISNULL(@TotalExpenseCost, 0) AS TotalExpenseCost,
		ISNULL(@TotalCost, 0) + ISNULL(@TotalExpenseCost, 0) as CompleteCost
END
GO
--End procedure reporting.GetCashHandoverReport

--Begin procedure dropdown.GetConceptNoteWorkflowData
EXEC Utility.DropObject 'dropdown.GetConceptNoteWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the workflow.Workflow table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetConceptNoteWorkflowData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.WorkflowID, 
		T.WorkflowName
	FROM workflow.Workflow T
	WHERE (T.WorkflowID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
		AND T.EntityTypeCode = 'ConceptNote'
	ORDER BY T.WorkflowName, T.WorkflowID

END
GO
--End  procedure dropdown.GetConceptNoteWorkflowData

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	 (SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote'),
	 CNS.ConceptNoteStatusCode,
	 CN.WorkflowStepNumber,


		CN.ConceptNoteID,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,
		CN.TaskCode,
		
		CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
							ELSE 'Closedown'
						END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
							ELSE 'Closedown'
						END
				END

			ELSE
			'Bad WorkFlow'

		END AS Status,

		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1
			SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,

		CASE 
			WHEN 
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			
			
			 IN ('Cancelled','Development')	
			THEN FORMAT(0, 'C', 'en-us') 
			WHEN CN.TaskCode ='Closed'						
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			ELSE FORMAT((IsNull(OACNB.TotalBudget,0)+ IsNull(OACNA.ConceptNoteAmmendmentTotal,0) + isnull(OACNCE.ConceptNoteEquimentTotal, 0)), 'C', 'en-us') 
		END AS TotalBudget,
		
		CASE 
			WHEN 
			
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			
			 IN ('Cancelled','Development')	
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us') 
		END AS TotalSpent,
		
		CASE 
			WHEN 
			(CASE ltrim(rtrim((SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')))
			WHEN 'US Activity Workflow'	THEN 
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 7	THEN 'Development'
						WHEN CN.WorkflowStepNumber = 7	THEN 'Implementation'
						ELSE 'Closedown'
					END
				END
					

			WHEN 'UK Activity Workflow' THEN  
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < 6		THEN 'Development'
						WHEN CN.WorkflowStepNumber = 6		THEN 'Implementation'
					ELSE 'Closedown'
					END
				END

			ELSE
			'Bad WorkFlow'

		END)
			 IN ('Closed','Cancelled','OnHold','Development')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT(((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmmendmentTotal,0)+ isnull(OACNCE.ConceptNoteEquimentTotal, 0)) - (IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0))), 'C', 'en-us') 
		END AS TotalRemaining

	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB
		OUTER APPLY
			(
			SELECT
				SUM(CNF.drAmt) - SUM(CNF.CrAmt) as ConceptNoteFinanceTotal
			FROM dbo.ConceptNoteFinance CNF
			WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
			) OACNF
		OUTER APPLY
			(
			SELECT
				SUM(CNA.Cost) as ConceptNoteAmmendmentTotal
			FROM dbo.ConceptNoteAmmendment CNA
			WHERE CNA.ConceptNoteID = CN.ConceptNoteID
			) OACNA
		OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue) as ConceptNoteEquimentTotal
			FROM dbo.ConceptNoteEquipmentCatalog CNCE
				JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					AND CNCE.ConceptNoteID = CN.ConceptNoteID
			) OACNCE
			ORDER BY 9 DESC

END
GO
--End procedure reporting.GetConceptNotes

--Begin procedure reporting.GetJusticeCashHandoverReport
EXEC Utility.DropObject 'reporting.GetJusticeCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A stored procedure to data for the cash handover form
-- ==================================================================

CREATE PROCEDURE reporting.GetJusticeCashHandoverReport

@PersonID INT,	 
@PaymentMonth INT,	 
@PaymentYear INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RunningCost INT ,@nContactCount INT, @nStipendAmountAuthorized NUMERIC(18, 2), @cStipendName nvarchar(250), @nCommunityAssetID INT, @nCommunityAssetUnitID INT,@TotalCost INT = 0



	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,

			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.CommunityAssetID > 0
				AND C.CommunityAssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.CommunityAssetID = @nCommunityAssetID AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,CommunityAssetID,CommunityAssetUnitID) VALUES (@PersonID,@nCommunityAssetID,@nCommunityAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
		SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
		SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
		SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
		SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
		SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.CommunityAssetID = @nCommunityAssetID
			AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	

	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID



	SELECT 

		@RunningCost =SUM(CAUE.ExpenseAmountAuthorized)
		--SUM(CAUE.ExpenseAmountAuthorized)
		--CAUE.ExpenseAmountAuthorized,
		--CAUE.CommunityAssetUnitID,
		--CA.CommunityAssetID


	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @PaymentMonth AND CAUE.PaymentYear =@PaymentYear




		SELECT
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END AS ProvinceName,
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetArabicProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetArabicProvinceNameByProvinceID(CA.ProvinceID)
		END AS ArabicProvinceName,
		dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst') AS FullName,
		DateName(month , DateAdd(month,@PaymentMonth, 0) - 1) + ' - ' + CAST(@PaymentYear AS CHAR(4)) as PaymentMonthYear,
		ISNULL(ISNULL(@TotalCost, 0), 0) + ISNULL(@RunningCost, 0) as CompleteCost,
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(CA.CommunityID))
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @PaymentMonth AND CAUE.PaymentYear =@PaymentYear


END
GO
--End procedure reporting.GetJusticeCashHandoverReport

--Begin procedure reporting.GetJusticeStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPayments

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UPPER(LEFT(DateName( month , DateAdd( month , D.PaymentMonth , -1 ) ),3)) + ' - ' + CAST(D.PaymentYear as varchar(5)) AS YearMonthFormatted,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		SUM(D.StipendAmountPaid) AS StipendAmountPaid,
		SUM(D.StipendAmountAuthorized) AS StipendAmountAuthorized,
		(SELECT SUM(ExpenseAmountAuthorized)  FROM dbo.CommunityAssetUnitExpense CAUE 
					JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = CAUE.CommunityAssetUnitID
					JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = CAU.CommunityAssetID AND CAUE.ProvinceID =d.ProvinceID AND CAUE.PaymentYear= D.PaymentYear AND CAUE.PaymentMonth=D.PaymentMonth) as StipendExpenseAmount
	FROM
		(
		SELECT
			CSP.StipendAmountPaid,
			CSP.StipendAmountAuthorized,
			CSP.ContactID,
			CSP.ProvinceID,
			CSP.PaymentYear,
			CSP.PaymentMonth,
		
			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			cast(CSP.PaymentYear as varchar(50)) + cast(CSP.PaymentMonth as varchar(50)) AS YearMonth,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID 
		AND StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND StipendTypeCode ='JusticeStipend' 
		) D
	GROUP BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.provinceID
	ORDER BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.ProvinceID ASC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPayments

--Begin procedure reporting.GetJusticeStipendActivitySummaryPeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPeople

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(PVT.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(PVT.YearMonth, 4) AS YearMonthFormatted,
		(SELECT P.ProvinceID FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		*
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			YEAR(CSP.StipendPaidDate) * 100 + MONTH(CSP.StipendPaidDate) AS YearMonth
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
			AND CSP.StipendPaidDate IS NOT NULL
		GROUP BY CSP.StipendPaidDate, CSP.StipendName
		) AS D
	PIVOT
		(
		MAX(D.StipendNameCount)
		FOR D.StipendName IN
			(
			[Rank 1],
			[Rank 2],
			[Rank 3],
			[Rank 4],
			[Rank 5]
			)
		) AS PVT
	ORDER BY PVT.YearMonth DESC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPeople

--Begin procedure reporting.GetJusticeStipendActivityVariancePayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePayments

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendAmountAuthorizedTotal,
		B.StipendAmountPaidTotal,
		   B.StipendAmountPaidTotal - A.StipendAmountAuthorizedTotal  AS Variance
	FROM
		(
			SELECT 
		SUM(D.StipendAmountAuthorizedTotal) as StipendAmountAuthorizedTotal,
		D.StipendName,
		D.DisplayOrder
		FROM (
		SELECT 
			CSP.StipendAmountAuthorized,
			CASE 
			WHEN CSP.StipendAuthorizedDate IS NULL THEN 0 
			ELSE CSP.StipendAmountAuthorized
			END as StipendAmountAuthorizedTotal,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
				AND CSP.ProvinceID = @ProvinceID
				 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
				 ) D
		GROUP BY D.StipendName, D.DisplayOrder
		) A
		JOIN
			(
	SELECT 
		SUM(D.StipendAmountPaidTotal) as StipendAmountPaidTotal,
		D.StipendName
		FROM (
			SELECT 
			CASE 
			WHEN CSP.StipendPaidDate IS NULL THEN 0 
			ELSE CSP.StipendAmountPaid
			END as StipendAmountPaidTotal,
			CSP.StipendName

			FROM dbo.ContactStipendPayment CSP
			WHERE CSP.ProvinceID = @ProvinceID
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
			 )D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePayments

--Begin procedure reporting.GetJusticeStipendActivityVariancePeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePeople

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		ISNULL(A.StipendNameCount, 0) AS AuthorizedStipendNameCount,
		ISNULL(B.StipendNameCount, 0) AS ReconciledStipendNameCount,
		ISNULL(B.StipendNameCount, 0) - ISNULL(A.StipendNameCount, 0) AS Variance
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' AND ProvinceID =@ProvinceID 
			 AND StipendAuthorizedDate IS NOT NULL
		GROUP BY CSP.StipendName, S.DisplayOrder
		) A
		LEFT JOIN
			(
			SELECT

			SUM(StipendCount)AS StipendNameCount,
			D.StipendName
			FROM
						(SELECT 
							CASE
							WHEN CSP.StipendPaidDate IS NULL THEN  0
							ELSE COUNT(	CSP.StipendName)
							END as StipendCount,
							CSP.StipendName
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.ProvinceID = @ProvinceID
						 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend'  
						AND StipendPaidDate IS NOT NULL
						GROUP BY CSP.StipendName,csp.StipendPaidDate) D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePeople

--Begin procedure reporting.GetJusticeStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetJusticeStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendPaymentReport

@PersonID INT = 0, 
@Year int = 0, 
@Month int = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nCommunityAssetID INT	
	DECLARE @nCommunityAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,

			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.CommunityAssetID > 0
				AND C.CommunityAssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.CommunityAssetID = @nCommunityAssetID AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,CommunityAssetID,CommunityAssetUnitID) VALUES (@PersonID,@nCommunityAssetID,@nCommunityAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
		SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
		SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
		SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
		SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
		SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
		SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.CommunityAssetID = @nCommunityAssetID
			AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
			SP.[Total Count] ,
			SP.[Total Stipend],
			SP.[Rank 1],
			SP.[Rank 2],
			SP.[Rank 3],
			SP.[Rank 4],
			SP.[Rank 5],
			SP.[Rank 1 Count],
			SP.[Rank 2 Count],
			SP.[Rank 3 Count],
			SP.[Rank 4 Count],
			SP.[Rank 5 Count],
			sp.stipendpaymentid, 
			sp.personid,
			sp.communityassetid,
			sp.CommunityAssetunitID,
	CAUE.ExpenseAmountAuthorized AS [Running Costs],
		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetCommunityNameByCommunityID(CA.CommunityID)
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center

	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dbo.CommunityAssetUnitExpense CAUE ON CAUE.CommunityAssetUnitID = CAU.CommunityAssetUnitID AND CAUE.PaymentMonth  = @Month AND CAUE.PaymentYear =@Year

END
GO
--End procedure reporting.GetJusticeStipendPaymentReport

--Begin procedure utility.AddColumn
EXEC Utility.DropObject 'utility.AddColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD [' + @ColumnName + '] ' + @DataType
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.DropConstraintsAndIndexes
EXEC Utility.DropObject 'utility.DropConstraintsAndIndexes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropConstraintsAndIndexes

@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + DC.Name + ']' AS SQL
		FROM sys.default_constraints DC
		WHERE DC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + KC.Name + ']' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + FK.Name + ']' AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @TableName AS SQL
		FROM sys.indexes I
		WHERE I.object_ID = OBJECT_ID(@TableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor
END
GO
--End  procedure utility.DropConstraintsAndIndexes

--Begin procedure utility.SetDefaultConstraint
EXEC Utility.DropObject 'utility.SetDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date: 2013.10.12
-- Description:	A helper stored procedure for table upgrades
-- ===============================================================================================================================
CREATE PROCEDURE utility.SetDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX),
@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ''' + @Default + ''' WHERE [' + @ColumnName + '] IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ' + @Default + ' WHERE [' + @ColumnName + '] IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN [' + @ColumnName + '] ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + REPLACE(@ColumnName, ' ', '_')
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR [' + @ColumnName + ']'
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR [' + @ColumnName + ']'
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.SetDefaultConstraint

