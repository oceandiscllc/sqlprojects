USE AJACS
GO

--Begin table dropdown.Stipend
DECLARE @nRank250ID INT

SELECT TOP 1 @nRank250ID = S.StipendID
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'JusticeStipend'
	AND S.StipendAmount = 250
ORDER BY S.StipendID

UPDATE C
SET C.StipendID = @nRank250ID
FROM dbo.Contact C
	JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
		AND S.StipendTypeCode = 'JusticeStipend'
		AND S.StipendAmount = 250

DELETE S
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'JusticeStipend'
	AND S.StipendAmount = 250
	AND S.StipendID <> @nRank250ID

UPDATE S
SET S.StipendName = 
	CASE
		WHEN S.StipendAmount = 300
		THEN 'Rank 1'
		WHEN S.StipendAmount = 250
		THEN 'Rank 2'
		WHEN S.StipendAmount = 200
		THEN 'Rank 3'
		WHEN S.StipendAmount = 125
		THEN 'Rank 4'
		WHEN S.StipendAmount = 100
		THEN 'Rank 5'
	END
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'JusticeStipend'
GO
--End table dropdown.Stipend

--Begin table reporting.StipendPayment
DECLARE @TableName VARCHAR(250) = 'reporting.StipendPayment'

EXEC utility.DropConstraintsAndIndexes @TableName

EXEC utility.AddColumn @TableName, 'Rank 1', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 1 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 2', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 2 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 3', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 3 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 4', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 4 Count', 'INT'
EXEC utility.AddColumn @TableName, 'Rank 5', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Rank 5 Count', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'Adjutant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Adjutant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain Doctor', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Captain Doctor Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel Doctor', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Colonel Doctor Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Command', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Command Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Officer', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Officer Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Policeman', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Contracted Policeman Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Adjutant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Adjutant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Lieutenant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Lieutenant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Sergeant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'First Sergeant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'General', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'General Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Lieutenant Colonel', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Lieutenant Colonel Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Major', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Major Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Policeman', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Policeman Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 1', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 1 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 2', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 2 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 3', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 3 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 4', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 4 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 5', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 5 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Sergeant', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Sergeant Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Total Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Total Stipend', 'NUMERIC(18, 0)', 0

EXEC utility.SetIndexClustered @TableName, 'IX_StipendPayment', 'PersonID,StipendPaymentID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'StipendPaymentID'
GO
--End table reporting.StipendPayment

