USE AJACS
GO

--Begin function dbo.GetArabicProvinceNameByProvinceID
EXEC Utility.DropObject 'dbo.GetArabicProvinceNameByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A function to return an Arabic Province Name for a specific ProvinceID
-- ===================================================================================
CREATE FUNCTION dbo.GetArabicProvinceNameByProvinceID
(
@ProvinceID INT 
)

RETURNS NVARCHAR(250)

AS
BEGIN

	DECLARE @cProvinceName NVARCHAR(250) = ''
	
	SELECT @cProvinceName =  P.ArabicProvinceName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID
	
	RETURN ISNULL(@cProvinceName, '')

END
GO
--End function dbo.GetArabicProvinceNameByProvinceID

--Begin function dbo.StripHTML
EXEC Utility.DropObject 'dbo.StripHTML'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A function to return an Arabic Province Name for a specific ProvinceID
-- ===================================================================================
CREATE FUNCTION dbo.StripHTML
(
@HTMLText VARCHAR(MAX)
)

RETURNS VARCHAR(MAX) AS
BEGIN
  DECLARE @Start INT
  DECLARE @End INT
  DECLARE @Length INT
  SET @Start = CHARINDEX('<',@HTMLText)
  SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
  SET @Length = (@End - @Start) + 1
  WHILE @Start > 0 AND @End > 0 AND @Length > 0
  BEGIN
      SET @HTMLText = STUFF(@HTMLText,@Start,@Length,'')
      SET @Start = CHARINDEX('<',@HTMLText)
      SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
      SET @Length = (@End - @Start) + 1
  END

  RETURN LTRIM(RTRIM(@HTMLText))

END
GO
--End function dbo.StripHTML
