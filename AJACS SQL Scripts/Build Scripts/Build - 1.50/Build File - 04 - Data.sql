USE AJACS
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'Document')
	BEGIN
	
	--Begin table dbo.EmailTemplate
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode,WorkflowActionCode,EmailText)
	VALUES
		('Document','DocumentShare','<p>Dear [[FirstName]],<br /><br />Please find attached to this email a document from the AJACS KMS, [[DocumentTitle]] that we believe may be of interest to you.</p><p>Comment</p><p>Best Regards <br />[[SubmittingUser]]<br />AJACS Team</p>')
	--End table dbo.EmailTemplate

	--Begin table dbo.EmailTemplateField
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('Document','[[FirstName]]','First Name',1),
		('Document','[[DocumentTitle]]','DocumentTitle',2),
		('Document','[[Comment]]','Comment',3),
		('Document','[[SubmittingUser]]','Submitting User',4)	
	--End  table dbo.EmailTemplateField

	END
--ENDIF
GO

--Begin table dbo.EntityType
UPDATE dbo.EntityType
SET HasWorkflow = 1
WHERE EntityTypeCode IN 
	(
	'CommunityProvinceEngagementUpdate',
	'ConceptNote',
	'EquipmentDistributionPlan',
	'FIFUpdate',
	'JusticeUpdate',
	'PoliceEngagementUpdate',
	'ProgramReport',
	'RecommendationUpdate',
	'RiskUpdate',
	'SpotReport',
	'WeeklyReport'
	)
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentDistributionList', @NewMenuItemLink='/equipmentdistribution/listdistribution', @NewMenuItemText='Equipment Distributions', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentInventoryList', @PermissionableLineageList='EquipmentDistribution.ListDistribution'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DistributedEquipmentList', @NewMenuItemLink='/equipmentdistribution/listdistributedinventory', @NewMenuItemText='Distributed Equipment', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentDistributionList', @PermissionableLineageList='EquipmentDistribution.ListDistributedInventory'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO

DELETE 
FROM dbo.MenuItem
WHERE MenuItemCode IN ('EquipmentDistributionPlanList','EquipmentManagement','ConceptNoteContactEquipmentList')
GO

DELETE MILP
FROM dbo.MenuItemPermissionableLineage MILP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.MenuItem MI
	WHERE MI.MenuItemID = MILP.MenuItemID
	)
	OR NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = MILP.PermissionableLineage
	)
GO
--End table dbo.MenuItem

--Begin table dropdown.AuditOutcome
UPDATE dropdown.AuditOutcome
SET IsActive = 0
WHERE AuditOutcomeCode = 'Transfer'
GO
--End table dropdown.AuditOutcome

--Begin table permissionable.PersonPermissionable
UPDATE P
SET P.IsSuperAdministrator = 1
FROM dbo.Person P 
WHERE P.UserName IN 
	(
	'bgreen',
	'christopher.crouch',
	'DaveD',
	'DaveR',
	'eric.jones',
	'gyingling',
	'IanVM',
	'jburnham',
	'JCole',
	'jlyons',
	'justin',
	'kevin',
	'Nameer',
	'Rabaa',
	'todd.pires',
	'usamah'
	)
GO
--End table permissionable.PersonPermissionable

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
