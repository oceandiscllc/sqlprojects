USE AJACS
GO

--Begin procedure utility.AddSchema
EXEC utility.DropObject 'utility.AddSchema'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2015.10.06
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddSchema

@SchemaName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @SchemaName)
		BEGIN
	
		DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA ' + LOWER(@SchemaName)
	
		EXEC (@cSQL)
	
		END
	--ENDIF

END
GO
--End procedure utility.AddSchema

--Begin schema syslog
EXEC utility.AddSchema 'syslog'
GO
--End schema syslog
