USE AJACS
GO

--Begin function dbo.FormatConceptNoteReferenceCode
EXEC utility.DropObject 'dbo.FormatConceptNoteReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- =========================================================================

CREATE FUNCTION dbo.FormatConceptNoteReferenceCode
(
@ConceptNoteID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(100) = (SELECT C.ActivityCode FROM dbo.ConceptNote C WHERE C.ConceptNoteID = @ConceptNoteID)
	DECLARE @cSystemName VARCHAR(50)

	IF @cReturn IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'
				
		SET @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)

		END
	--ENDIF

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteReferenceCode

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
--
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	Added ISNULL support
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cReturn VARCHAR(300)
	DECLARE @cSystemName NVARCHAR(100) = (SELECT C.ActivityCode FROM dbo.ConceptNote C WHERE C.ConceptNoteID = @ConceptNoteID)
	
	IF @cSystemName IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'

		END
	--ENDIF
	
	SELECT @cReturn = @cSystemName + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteTitle

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@EquipmentDistribbtionID INT,
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @tSource TABLE (CommunityID INT, ProvinceID INT)

	INSERT INTO @tSource
		(CommunityID, ProvinceID)
	SELECT 
		C.CommunityID,

		CASE C.ProvinceID
			WHEN 0  
			THEN dbo.GetProvinceIDByCommunityID (C.CommunityID)
			ELSE C.ProvinceID 
		END AS ProvinceID
				
	FROM procurement.DistributedInventory DI
		JOIN Contact C ON C.Contactid = DI.EndUserEntityID 
			AND DI.EquipmentDistributionID = @EquipmentDistribbtionID 
			AND EndUserEntityTypeCode = 'Contact'

	UNION 

	SELECT 

		CASE TerritoryTypeCode
			WHEN 'Province' 
			THEN 0
			ELSE TerritoryID 
		END AS CommunityID,

		CASE TerritoryTypeCode
			WHEN 'Community' 
			THEN dbo.GetProvinceIDByCommunityID (TerritoryID)
			ELSE TerritoryID 
		END AS ProvinceID 

	FROM procurement.DistributedInventory DI
		JOIN Force.Force F ON F.ForceID = DI.EndUserEntityID 
			AND DI.EquipmentDistributionID = @EquipmentDistribbtionID 
			AND EndUserEntityTypeCode = 'Force'

	UNION

	SELECT 
		CA.CommunityID,

		CASE CA.ProvinceID
			WHEN 0  
			THEN dbo.GetProvinceIDByCommunityID (CA.CommunityID)
			ELSE CA.ProvinceID 
		END AS ProvinceID
				
	FROM procurement.DistributedInventory DI
		JOIN CommunityAsset CA ON CA.CommunityAssetID = DI.EndUserEntityID 
			AND DI.EquipmentDistributionID = @EquipmentDistribbtionID 
			AND EndUserEntityTypeCode = 'CommunityAsset'

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(ISNULL(CT.Latitude, 0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(CT.Longitude,0) AS VARCHAR(MAX))
	FROM @tSource C1
		JOIN Community CT ON c1.CommunityID = ct.CommunityID  AND CT.ProvinceID = @ProvinceID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CT.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / LEN('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan

--Begin function dbo.GetEventNameByEventCode
EXEC utility.DropObject 'dbo.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
--
-- Author:			Todd Pires
-- Create date:	2015.04.30
-- Description:	Added failedlogin support
-- ====================================================================
CREATE FUNCTION dbo.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'finalize'
			THEN 'Finalize'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'read'
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			ELSE @EventCode + ' not found'
		END

	RETURN @EventCodeName

END
GO
--End function dbo.GetEventNameByEventCode

--Begin function dbo.HasPermission
EXEC utility.DropObject 'dbo.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION dbo.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP WHERE PP.PermissionableLineage = @PermissionableLineage AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function dbo.HasPermission

--Begin permissionable.HasFileAccess
EXEC Utility.DropObject 'permissionable.HasFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date: 2015.07.18
-- Description:	A stored procedure to get check access to a file
-- =============================================================
CREATE PROCEDURE permissionable.HasFileAccess

@PersonID INT,
@PhysicalFileName VARCHAR(50)

AS
BEGIN

	SELECT 1
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		AND 
			(
			PP.PermissionableLineage = 
				(
				SELECT 'Document.View.' + ISNULL(DT.DocumentTypePermissionCode, '000')
				FROM dbo.Document D
					JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
						AND D.PhysicalFileName = @PhysicalFileName
				)
				OR
					( 
					(SELECT D.DocumentTypeID FROM dbo.Document D WHERE D.PhysicalFileName = @PhysicalFileName) = 0
					)
			)

END
GO
--End procedure permissionable.HasFileAccess

--Begin function procurement.FormatEndUserByEntityTypCodeAndEntityID
EXEC utility.DropObject 'procurement.FormatEndUserByEntityTypCodeAndEntityID'
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.03.05
-- Description:	A function a formatted end user name for a distributed inventory item
-- ==================================================================================
CREATE FUNCTION procurement.FormatEndUserByEntityTypCodeAndEntityID
(
@EntityTypCode VARCHAR(50),
@EntityID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(300)

	IF @EntityTypCode = 'CommunityAsset'
		SELECT @cReturn = ISNULL(CA.CommunityAssetName, '') + ' (Asset)' FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = @EntityID
	ELSE IF @EntityTypCode = 'Force'
		SELECT @cReturn = ISNULL(F.ForceName, '') + ' (Force)' FROM force.Force F WHERE F.ForceID = @EntityID
	ELSE IF @EntityTypCode = 'Contact'
		SELECT @cReturn = dbo.FormatContactNameByContactID(@EntityID, 'LastFirst')
	--ENDIF
	
	RETURN @cReturn

END
GO
--End function procurement.FormatEndUserByEntityTypCodeAndEntityID

--Begin function procurement.GetDistributedInventoryQuantityAvailable
EXEC utility.DropObject 'procurement.GetEquipmentDistributionEquipmentInventoryQuantityAvailable'
EXEC utility.DropObject 'procurement.GetDistributedInventoryQuantityAvailable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.28
-- Description:	A function to get the quantity available for a DistributedInventory item
-- =====================================================================================

CREATE FUNCTION procurement.GetDistributedInventoryQuantityAvailable
(
@EquipmentInventoryID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0

	SELECT @nReturn = EI.Quantity - 
										EI.QuantityDistributed - 
										(SELECT SUM(DI.Quantity) FROM procurement.DistributedInventory DI JOIN procurement.EquipmentDistribution ED ON ED.EquipmentDistributionID = DI.EquipmentDistributionID AND ED.IsActive = 1 AND DI.EquipmentInventoryID = @EquipmentInventoryID)
	FROM procurement.EquipmentInventory EI
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
	
	RETURN @nReturn
END
GO
--End function procurement.GetDistributedInventoryQuantityAvailable

--Begin function procurement.GetLastEquipmentAuditDate
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditDate'
GO

-- =================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last auditdate for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
--
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	Refactored to work with the procurement.DistributedInventoryAudit table
-- =================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditDate
(
@DistributedInventoryID INT
)

RETURNS DATE

AS
BEGIN

	DECLARE @dLastAuditDate DATE
	
	SELECT TOP 1 @dLastAuditDate = DIA.AuditDate
	FROM procurement.DistributedInventoryAudit DIA
	WHERE DIA.DistributedInventoryID = @DistributedInventoryID
	ORDER BY DIA.DistributedInventoryAuditID DESC
	
	RETURN @dLastAuditDate

END
GO
--End function procurement.GetLastEquipmentAuditDate

--Begin function procurement.GetLastEquipmentAuditOutcome
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditOutcome'
GO

-- ====================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last Audit Outcome for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
--
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	Refactored to work with the procurement.DistributedInventoryAudit table
-- ====================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditOutcome
(
@DistributedInventoryID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cLastAuditOutcomeName VARCHAR(50)
	
	SELECT TOP 1 @cLastAuditOutcomeName = AO.AuditOutcomeName
	FROM procurement.DistributedInventoryAudit DIA
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = DIA.AuditOutcomeID
			AND DIA.DistributedInventoryID = @DistributedInventoryID
	ORDER BY DIA.AuditDate DESC

	IF @cLastAuditOutcomeName IS NULL
		BEGIN
		
		SELECT @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM dropdown.AuditOutcome AO
		WHERE AO.AuditOutcomeID = 0

		END
	--ENDIF
	
	RETURN @cLastAuditOutcomeName

END
GO
--End function procurement.GetLastEquipmentAuditOutcome

--Begin function procurement.GetLicensedQuantityAvailable
EXEC utility.DropObject 'procurement.GetLicensedQuantityAvailable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return the quantity left on valid licenses for an equipmentcatalogid
-- ===============================================================================================
CREATE FUNCTION procurement.GetLicensedQuantityAvailable
(
@EquipmentCatalogID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nQuantityLicensed INT
	DECLARE @nQuantityExpended INT

	SELECT @nQuantityLicensed = SUM(LEC.QuantityAuthorized)
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND L.EndDate >= getDate()
			AND LEC.EquipmentCatalogID = @EquipmentCatalogID

	SELECT @nQuantityExpended = SUM(LEI.Quantity)
	FROM procurement.LicenseEquipmentInventory LEI
		JOIN procurement.License L ON L.LicenseID = LEI.LicenseID
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = LEI.EquipmentInventoryID
			AND L.EndDate >= getDate()
			AND EI.EquipmentCatalogID = @EquipmentCatalogID


	RETURN ISNULL(@nQuantityLicensed, 0) - ISNULL(@nQuantityExpended, 0)

END
GO
--End function procurement.GetLicensedQuantityAvailable

--Begin function procurement.GetLicensedQuantityAvailableByLicenseID
EXEC utility.DropObject 'procurement.GetLicensedQuantityAvailableByLicenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return the quantity left on valid licenses for an equipmentcatalogid and a licenseid
-- ===============================================================================================================
CREATE FUNCTION procurement.GetLicensedQuantityAvailableByLicenseID
(
@EquipmentCatalogID INT,
@LicenseID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nQuantityLicensed INT
	DECLARE @nQuantityExpended INT

	SELECT @nQuantityLicensed = SUM(LEC.QuantityAuthorized)
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND L.LicenseID = @LicenseID
			AND L.EndDate >= getDate()
			AND LEC.EquipmentCatalogID = @EquipmentCatalogID

	SELECT @nQuantityExpended = SUM(LEI.Quantity)
	FROM procurement.LicenseEquipmentInventory LEI
		JOIN procurement.License L ON L.LicenseID = LEI.LicenseID
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = LEI.EquipmentInventoryID
			AND L.LicenseID = @LicenseID
			AND L.EndDate >= getDate()
			AND EI.EquipmentCatalogID = @EquipmentCatalogID


	RETURN ISNULL(@nQuantityLicensed, 0) - ISNULL(@nQuantityExpended, 0)

END
GO
--End function procurement.GetLicensedQuantityAvailableByLicenseID

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
-- =================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0

	IF @EntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE
		BEGIN
	
		DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.CanHaveListView
EXEC utility.DropObject 'workflow.CanHaveListView'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has List and View accesss to a specific EntityTypeCode and EntityID
-- =====================================================================================================================

CREATE FUNCTION workflow.CanHaveListView
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveListView BIT = 0
	DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	IF @nWorkflowStepNumber > @nWorkflowStepCount 
		AND EXISTS
			(
			SELECT 1
			FROM permissionable.PersonPermissionable PP
			WHERE PP.PersonID = @PersonID
				AND PP.PermissionableLineage IN (@EntityTypeCode + '.List', @EntityTypeCode + '.View')
			)
		SET @nCanHaveListView = 1
	ELSE IF EXISTS 
		(
		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.PersonID = @PersonID
		)
		SET @nCanHaveListView = 1
	--ENDIF
	
	RETURN @nCanHaveListView
END
GO
--End function workflow.CanHaveListView

--Begin function workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowData
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepCount INT,
	CanRejectAfterFinalApproval BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(SELECT MAX(WS2.WorkflowStepNumber) FROM workflow.Workflow W2 JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID AND W2.IsActive = 1 AND W2.EntityTypeCode = W1.EntityTypeCode) AS WorkflowStepCount,
			ET.CanRejectAfterFinalApproval
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.IsActive = 1
				AND W1.EntityTypeCode = @EntityTypeCode
				AND WS1.WorkflowStepNumber = 1
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = W1.EntityTypeCode
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
			BEGIN
			
			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT
				'Approved',
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM dbo.EntityType ET 
			WHERE ET.EntityTypeCode = @EntityTypeCode
			
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData

--Begin function workflow.GetEntityWorkflowPeople
EXEC utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return people assigned to an entityy's current workflow step
-- =======================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowPeople
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepGroupName VARCHAR(250),
	PersonID INT,
	FullName VARCHAR(250),
	EmailAddress VARCHAR(320),
	IsComplete BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepGroupName, PersonID, FullName, EmailAddress, IsComplete)
		SELECT
			WSG.WorkflowStepGroupName,
			WSGP.PersonID,
			dbo.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN dbo.Person P ON P.PersonID = WSGP.PersonID
		ORDER BY 1, 2
	
		END
	ELSE
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepGroupName, PersonID, FullName, EmailAddress, IsComplete)
		SELECT
			EWSGP.WorkflowStepGroupName, 
			EWSGP.PersonID,
			dbo.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN dbo.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber
		ORDER BY 1, 2
	
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowPeople

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT = ISNULL((SELECT MAX(EWSGP.WorkflowStepNumber) FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID), 0)
	
	RETURN @nWorkflowStepCount
END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = (SELECT TOP 1 EWSGP.WorkflowStepNumber FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID AND IsComplete = 0 ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID)

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber
END
GO
--End function workflow.GetWorkflowStepNumber
