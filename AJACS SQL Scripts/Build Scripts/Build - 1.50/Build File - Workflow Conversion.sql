﻿USE AJACS
GO

TRUNCATE TABLE workflow.Workflow
GO

TRUNCATE TABLE workflow.WorkflowStep
GO

SET IDENTITY_INSERT workflow.Workflow ON 
GO

IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', '')) = 'Dev'
	BEGIN

	INSERT INTO workflow.Workflow 
		(WorkflowID, EntityTypeCode, WorkflowStepCount, WorkflowName, IsActive) 
	VALUES 
		(1, 'ConceptNote', 10, 'Old Concept Note', 0),
		(2, 'SpotReport', 3, 'Old Spot Report', 0),
		(3, 'WeeklyReport', 3, 'Old Weekly Report', 0),
		(4, 'ProgramReport', 3, 'Old Program Report', 0),
		(6, 'RecommendationUpdate', 3, 'Old Recommendation Update', 0),
		(7, 'RiskUpdate', 3, 'Old Risk Update', 0),
		(8, 'CommunityProvinceEngagementUpdate', 3, 'Old Community Province Engagement Update', 0),
		(9, 'PoliceEngagementUpdate', 3, 'Old Police Engagement Update', 0),
		(10, 'FIFUpdate', 3, 'Old FIF Update', 0),
		(11, 'JusticeUpdate', 3, 'Old Justice Update', 0)

	END
--ENDIF
GO

SET IDENTITY_INSERT workflow.Workflow OFF
GO

SET IDENTITY_INSERT workflow.WorkflowStep ON 
GO

IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', '')) = 'Dev'
	BEGIN

	INSERT INTO workflow.WorkflowStep 
		(WorkflowStepID, ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName) 
	VALUES 
		(24, 0, 1, 1, 'Concept Note Draft', NULL),
		(25, 0, 1, 2, 'Concept Note Edit', NULL),
		(26, 0, 1, 3, 'Concept Note Approval', NULL),
		(27, 0, 1, 4, 'Activity Sheet Draft', NULL),
		(29, 27, 1, 4, 'Activity Sheet Draft', NULL),
		(28, 27, 1, 4, 'M&E Review I', NULL),
		(30, 0, 1, 5, 'Activity Sheet Final Review', NULL),
		(31, 0, 1, 6, 'Activity Sheet Approval', NULL),
		(32, 0, 1, 7, 'Activity Sheet Donor Approval', NULL),
		(33, 0, 1, 8, 'Activity Implementation', NULL),
		(34, 0, 1, 9, 'Activity Closedown', NULL),
		(43, 34, 1, 9, 'Activity Implementer Reporting', NULL),
		(45, 34, 1, 9, 'Activity Manager Review and Approval', NULL),
		(44, 34, 1, 9, 'M&E Review II', NULL),
		(53, 0, 1, 10, 'Finalize', NULL),
		(15, 0, 2, 1, 'Draft', 'In Draft'),
		(16, 0, 2, 2, 'Review', 'Under Review'),
		(17, 0, 2, 3, 'Approve', 'Pending Approval'),
		(18, 0, 3, 1, 'Draft', 'In Draft'),
		(19, 0, 3, 2, 'Review', 'Under Review'),
		(20, 0, 3, 3, 'Approve', 'Pending Approval'),
		(21, 0, 4, 1, 'Draft', 'In Draft'),
		(22, 0, 4, 2, 'Review', 'Under Review'),
		(23, 0, 4, 3, 'Approve', 'Pending Approval'),
		(37, 0, 6, 1, 'Draft', 'In Draft'),
		(38, 0, 6, 2, 'Review', 'Under Review'),
		(39, 0, 6, 3, 'Approve', 'Pending Approval'),
		(40, 0, 7, 1, 'Draft', 'In Draft'),
		(41, 0, 7, 2, 'Review', 'Under Review'),
		(42, 0, 7, 3, 'Approve', 'Pending Approval'),
		(47, 0, 8, 1, 'Draft', 'In Draft'),
		(48, 0, 8, 2, 'Review', 'Under Review'),
		(49, 0, 8, 3, 'Approve', 'Pending Approval'),
		(50, 0, 9, 1, 'Draft', 'In Draft'),
		(51, 0, 9, 2, 'Review', 'Under Review'),
		(52, 0, 9, 3, 'Approve', 'Pending Approval'),
		(54, 0, 10, 1, 'Draft', 'In Draft'),
		(55, 0, 10, 2, 'Review', 'Under Review'),
		(56, 0, 10, 3, 'Approve', 'Pending Approval'),
		(57, 0, 11, 1, 'Draft', 'In Draft'),
		(58, 0, 11, 2, 'Review', 'Under Review'),
		(59, 0, 11, 3, 'Approve', 'Pending Approval')

	END
--ENDIF
GO

SET IDENTITY_INSERT workflow.WorkflowStep OFF
GO

SET IDENTITY_INSERT workflow.Workflow ON 
GO

IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', '')) = 'Prod'
	BEGIN

	INSERT INTO workflow.Workflow 
		(WorkflowID, EntityTypeCode, WorkflowStepCount, WorkflowName, IsActive) 
	VALUES 
		(1, 'ConceptNote', 10, 'Old Concept Note', 0),
		(2, 'SpotReport', 3, 'Old Spot Report', 0),
		(3, 'WeeklyReport', 3, 'Old Weekly Report', 0),
		(4, 'ProgramReport', 3, 'Old Program Report', 0),
		(5, 'EquipmentDistributionPlan', 2, 'Old EquipmentDistribution Plan', 0),
		(6, 'RecommendationUpdate', 3, 'Old Recommendation Update', 0),
		(7, 'RiskUpdate', 3, 'Old Risk Update', 0),
		(8, 'CommunityProvinceEngagementUpdate', 3, 'Old Community Province Engagement Update', 0),
		(9, 'PoliceEngagementUpdate', 3, 'Old Police Engagement Update', 0),
		(10, 'FIFUpdate', 3, 'Old FIF Update', 0),
		(11, 'JusticeUpdate', 3, 'Old Justice Update', 0)

	END
--ENDIF
GO

SET IDENTITY_INSERT workflow.Workflow OFF
GO

SET IDENTITY_INSERT workflow.WorkflowStep ON 
GO

IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', '')) = 'Prod'
	BEGIN

	INSERT workflow.WorkflowStep 
		(WorkflowStepID, ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName) 
	VALUES 
		(24, 0, 1, 1, 'Concept Note Draft', NULL),
		(25, 0, 1, 2, 'Concept Note Edit', NULL),
		(26, 0, 1, 3, 'Concept Note Approval', NULL),
		(27, 0, 1, 4, 'Activity Sheet Draft', NULL),
		(29, 27, 1, 4, 'Activity Sheet Draft', NULL),
		(28, 27, 1, 4, 'M&E Review I', NULL),
		(30, 0, 1, 5, 'Activity Sheet Final Review', NULL),
		(31, 0, 1, 6, 'Activity Sheet Approval', NULL),
		(32, 0, 1, 7, 'Activity Sheet Donor Approval', NULL),
		(33, 0, 1, 8, 'Activity Implementation', NULL),
		(34, 0, 1, 9, 'Activity Closedown', NULL),
		(43, 34, 1, 9, 'Activity Implementer Reporting', NULL),
		(45, 34, 1, 9, 'Activity Manager Review and Approval', NULL),
		(44, 34, 1, 9, 'M&E Review II', NULL),
		(53, 0, 1, 10, 'Finalize', NULL),
		(15, 0, 2, 1, 'Draft', 'In Draft'),
		(16, 0, 2, 2, 'Review', 'Under Review'),
		(17, 0, 2, 3, 'Approve', 'Pending Approval'),
		(18, 0, 3, 1, 'Draft', 'In Draft'),
		(19, 0, 3, 2, 'Review', 'Under Review'),
		(20, 0, 3, 3, 'Approve', 'Pending Approval'),
		(21, 0, 4, 1, 'Draft', 'In Draft'),
		(22, 0, 4, 2, 'Review', 'Under Review'),
		(23, 0, 4, 3, 'Approve', 'Pending Approval'),
		(35, 0, 5, 1, 'Draft', 'In Draft'),
		(36, 0, 5, 2, 'Approval', 'Pending Approval'),
		(37, 0, 6, 1, 'Draft', 'In Draft'),
		(38, 0, 6, 2, 'Review', 'Under Review'),
		(39, 0, 6, 3, 'Approve', 'Pending Approval'),
		(40, 0, 7, 1, 'Draft', 'In Draft'),
		(41, 0, 7, 2, 'Review', 'Under Review'),
		(42, 0, 7, 3, 'Approve', 'Pending Approval'),
		(47, 0, 8, 1, 'Draft', 'In Draft'),
		(48, 0, 8, 2, 'Review', 'Under Review'),
		(49, 0, 8, 3, 'Approve', 'Pending Approval'),
		(50, 0, 9, 1, 'Draft', 'In Draft'),
		(51, 0, 9, 2, 'Review', 'Under Review'),
		(52, 0, 9, 3, 'Approve', 'Pending Approval'),
		(54, 0, 10, 1, 'Draft', 'In Draft'),
		(55, 0, 10, 2, 'Review', 'Under Review'),
		(56, 0, 10, 3, 'Approve', 'Pending Approval'),
		(57, 0, 11, 1, 'Draft', 'In Draft'),
		(58, 0, 11, 2, 'Review', 'Under Review'),
		(59, 0, 11, 3, 'Approve', 'Pending Approval')

	END
--ENDIF
GO

SET IDENTITY_INSERT workflow.WorkflowStep OFF
GO

EXEC utility.AddColumn 'workflow.WorkflowStepGroup', 'LegacyWorkflowStepID', 'INT'
EXEC utility.AddColumn 'workflow.WorkflowStepGroup', 'PermissionableWorkflowStepID', 'INT'

EXEC utility.SetDefaultConstraint 'workflow.WorkflowStepGroup', 'LegacyWorkflowStepID', 'INT', 0
EXEC utility.SetDefaultConstraint 'workflow.WorkflowStepGroup', 'PermissionableWorkflowStepID', 'INT', 0
GO

DECLARE @nLastWorkflowID INT
DECLARE @tWorkflow TABLE (WorkflowID INT NOT NULL PRIMARY KEY, EntityTypeCode VARCHAR(50))
DECLARE @tWorkflowStep TABLE (WorkflowStepNumber INT NOT NULL PRIMARY KEY, WorkflowStepName VARCHAR(50))

INSERT INTO @tWorkflowStep
	(WorkflowStepNumber,WorkflowStepName)
VALUES
	(1,'Draft'),
	(2,'Review'),
	(3,'Approve')

DELETE W
FROM workflow.Workflow W
WHERE W.EntityTypeCode LIKE '%xxx'
	OR W.EntityTypeCode LIKE '%New'
	OR W.EntityTypeCode = 'EquipmentDistributionPlan'

DELETE WS
FROM workflow.WorkflowStep WS
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.Workflow W
	WHERE W.WorkflowID = WS.WorkflowID
	)

TRUNCATE TABLE workflow.WorkflowStepGroup
TRUNCATE TABLE workflow.WorkflowStepGroupPerson

UPDATE workflow.WorkflowStepGroup SET PermissionableWorkflowStepID = 0

--Begin table workflow.Workflow
INSERT INTO workflow.Workflow 
	(WorkflowName,EntityTypeCode,IsActive)
OUTPUT INSERTED.WorkflowID, INSERTED.EntityTypeCode INTO @tWorkflow
VALUES
	('Community Province Engagement Update New', 'CommunityProvinceEngagementUpdate New', 0),
	('FIF Update New', 'FIFUpdate New', 0),
	('Justice Update New', 'JusticeUpdate New', 0),
	('Police Engagement Update New', 'PoliceEngagementUpdate New', 0),
	('Program Report New', 'ProgramReport New', 0),
	('Recommendation Update New', 'RecommendationUpdate New', 0),
	('Risk Update New', 'RiskUpdate New', 0),
	('Spot Report New', 'SpotReport New', 0),
	('Weekly Report New', 'WeeklyReport New', 0)
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
INSERT INTO workflow.WorkflowStep 
	(WorkflowID,WorkflowStepNumber,WorkflowStepName)
SELECT
	W.WorkflowID,
	WS.WorkflowStepNumber,
	WS.WorkflowStepName
FROM @tWorkflow W, @tWorkflowStep WS
---End table workflow.WorkflowStep

--Start table workflow.WorkflowStepGroup
INSERT INTO workflow.WorkflowStepGroup
	(WorkflowStepID,WorkflowStepGroupName,LegacyWorkflowStepID,PermissionableWorkflowStepID)
SELECT
	(
	SELECT
		WS1.WorkflowStepID 
	FROM workflow.WorkflowStep WS1
	WHERE WS1.WorkflowID = W.WorkflowID
		AND WS1.WorkflowStepName = WS.WorkflowStepName
	),

	WS.WorkflowStepName + ' Group',

	ISNULL(
		(
		SELECT 
			WS2.WorkflowStepID 
		FROM workflow.WorkflowStep WS2
			JOIN workflow.Workflow W1 ON W1.WorkflowID = WS2.WorkflowID
				AND W1.EntityTypeCode = REPLACE(W.EntityTypeCode, ' New', '')
				AND WS2.WorkflowStepNumber = WS.WorkflowStepNumber
		), 0),

	ISNULL(
		(
		SELECT 
			CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
		FROM permissionable.Permissionable P
		WHERE P.PermissionableName = WS.WorkflowStepName
			AND 
				(
				P.PermissionableLineage LIKE REPLACE(W.EntityTypeCode, ' New', '') + '.AddUpdate.WorkflowStepID__'
					OR (P.PermissionableLineage LIKE 'CommunityProvinceEngagementUpdate.WorkflowStepID%' AND REPLACE(W.EntityTypeCode, ' New', '') = 'CommunityProvinceEngagementUpdate')
					OR (P.PermissionableLineage LIKE 'FIFUpdate.WorkflowStepID%' AND REPLACE(W.EntityTypeCode, ' New', '') = 'FIFUpdate')
					OR (P.PermissionableLineage LIKE 'JusticeUpdate.WorkflowStepID%' AND REPLACE(W.EntityTypeCode, ' New', '') = 'JusticeUpdate')
					OR (P.PermissionableLineage LIKE 'PoliceEngagementUpdate.WorkflowStepID%' AND REPLACE(W.EntityTypeCode, ' New', '') = 'PoliceEngagementUpdate')
					OR (P.PermissionableLineage LIKE 'Recommendation.AddUpdate.WorkflowStepID%' AND REPLACE(W.EntityTypeCode, ' New', '') = 'RecommendationUpdate')
					OR (P.PermissionableLineage LIKE 'Risk.AddUpdate.WorkflowStepID%' AND REPLACE(W.EntityTypeCode, ' New', '') = 'RiskUpdate')
				)
		), 0)
FROM @tWorkflow W, @tWorkflowStep WS
GO
--End table workflow.WorkflowStepGroup

DECLARE @nOriginalWorkflowID INT
DECLARE @nParentPermissionableID INT
DECLARE @nWorkflowID INT
DECLARE @tWorkflow TABLE (WorkflowID INT NOT NULL PRIMARY KEY)

SELECT @nOriginalWorkflowID = W.WorkflowID
FROM workflow.Workflow W
WHERE W.EntityTypeCode = 'ConceptNote'

SELECT @nParentPermissionableID = P.PermissionableID
FROM permissionable.Permissionable P 
WHERE P.PermissionableLineage = 'ConceptNote.AddUpdate'

--Begin table workflow.Workflow
INSERT INTO workflow.Workflow 
	(WorkflowName,EntityTypeCode,IsActive)
OUTPUT INSERTED.WorkflowID INTO @tWorkflow
VALUES
	('Concept Note New', 'ConceptNote New', 0)

SELECT @nWorkflowID = WorkflowID FROM @tWorkflow

--Begin table workflow.WorkflowStep
INSERT INTO workflow.WorkflowStep 
	(WorkflowID,WorkflowStepName,WorkflowStepNumber)
VALUES
	(@nWorkflowID, 'Concept Note Draft', 1),
	(@nWorkflowID, 'Concept Note Edit', 2),
	(@nWorkflowID, 'Concept Note Approval', 3),
	(@nWorkflowID, 'Activity Sheet Draft', 4),
	(@nWorkflowID, 'Activity Sheet Final Review', 5),
	(@nWorkflowID, 'Activity Sheet Approval', 6),
	(@nWorkflowID, 'Activity Sheet Donor Approval', 7),
	(@nWorkflowID, 'Activity Implementation', 8),
	(@nWorkflowID, 'Activity Closedown', 9),
	(@nWorkflowID, 'Finalize', 10)
--End table workflow.WorkflowStep

--Start table workflow.WorkflowStepGroup
INSERT INTO workflow.WorkflowStepGroup
	(WorkflowStepID,WorkflowStepGroupName,PermissionableWorkflowStepID,LegacyWorkflowStepID)
SELECT
	WS.WorkflowStepID,
	WS.WorkflowStepName + ' Group',
	ISNULL((SELECT CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT) FROM permissionable.Permissionable P WHERE P.ParentPermissionableID = @nParentPermissionableID 
		AND P.PermissionableName = WS.WorkflowStepName), 0),
	(SELECT WS1.WorkflowStepID FROM workflow.WorkflowStep WS1 WHERE WS1.WorkflowID = @nOriginalWorkflowID 
		AND WS1.WorkflowStepName = WS.WorkflowStepName AND WS1.ParentWorkflowStepID = 0)
FROM workflow.WorkflowStep WS
WHERE WS.WorkflowID = @nWorkflowID
	AND WS.WorkflowStepNumber IN (1,2,3,5,6,7,8,10)
	
INSERT INTO workflow.WorkflowStepGroup
	(WorkflowStepID,WorkflowStepGroupName,PermissionableWorkflowStepID,LegacyWorkflowStepID)
SELECT
	CASE
		WHEN WS.WorkflowStepName IN ('Activity Sheet Draft', 'M&E Review I')
		THEN (SELECT WS1.WorkflowStepID FROM workflow.WorkflowStep WS1 WHERE WS1.WorkflowID = @nWorkflowID 
			AND WS1.WorkflowStepNumber = 4)
		ELSE (SELECT WS1.WorkflowStepID FROM workflow.WorkflowStep WS1 WHERE WS1.WorkflowID = @nWorkflowID 
			AND WS1.WorkflowStepNumber = 9)
	END, 

	WS.WorkflowStepName + ' Group',
	ISNULL((SELECT CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT) FROM permissionable.Permissionable P WHERE P.ParentPermissionableID != @nParentPermissionableID 
		AND P.PermissionableName = WS.WorkflowStepName), 0),
	(SELECT WS1.WorkflowStepID FROM workflow.WorkflowStep WS1 WHERE WS1.WorkflowID = WS.WorkflowID AND WS1.WorkflowStepName = WS.WorkflowStepName AND WS1.ParentWorkflowStepID = WS.ParentWorkflowStepID)
FROM workflow.WorkflowStep WS
WHERE WS.WorkflowID = @nOriginalWorkflowID
	AND WS.WorkflowStepName IN ('Activity Sheet Draft', 'M&E Review I', 'Activity Implementer Reporting', 'Activity Manager Review and Approval', 'M&E Review II')
	AND WS.ParentWorkflowStepID != 0
GO

--Start table workflow.WorkflowStepGroupPerson
INSERT INTO workflow.WorkflowStepGroupPerson
	(WorkflowStepGroupID, PersonID)
SELECT
	WSG.WorkflowStepGroupID,
	CAP.PersonID
FROM workflow.WorkflowStepGroup WSG
	CROSS APPLY
		(
		SELECT
			PP.PersonID
		FROM permissionable.PersonPermissionable PP
			JOIN permissionable.Permissionable P ON P.PermissionableLineage = PP.PermissionableLineage
				AND P.PermissionableCode = 'WorkflowStepID' + CAST(WSG.PermissionableWorkflowStepID AS VARCHAR(10))
				AND EXISTS
					(
					SELECT 1
					FROM dbo.Person P 
					WHERE P.PersonID = PP.personID
					)
		) CAP
ORDER BY WSG.WorkflowStepGroupID, CAP.PersonID
--End table workflow.WorkflowStepGroupPerson

UPDATE W
SET 
	W.IsActive = 
	CASE
		WHEN W.WorkflowName IS NULL
		THEN 0
		ELSE 1
	END,

	W.EntityTypeCode = REPLACE(W.EntityTypeCode, ' New', '')
FROM workflow.Workflow W
GO

UPDATE W
SET W.EntityTypeCode = W.EntityTypeCode + ' Old'
FROM workflow.Workflow W
WHERE W.WorkflowName IS NULL
GO

TRUNCATE TABLE workflow.EntityWorkflowStepGroupPerson
GO

DECLARE @cEntityTypeCode VARCHAR(50)
DECLARE @nEntityID INT
DECLARE @nWorkflowStepNumber INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		'CommunityProvinceEngagementUpdate' AS EntityTypeCode,
		T.CommunityProvinceEngagementUpdateID AS EntityID,
		T.WorkflowStepNumber
	FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T

	UNION

	SELECT
		'ConceptNote' AS EntityTypeCode,
		T.ConceptNoteID AS EntityID,
		T.WorkflowStepNumber
	FROM dbo.ConceptNote T

	UNION

	SELECT 
		'FIFUpdate' AS EntityTypeCode,
		T.FIFUpdateID AS EntityID,
		T.WorkflowStepNumber
	FROM fifupdate.FIFUpdate T

	UNION

	SELECT 
		'JusticeUpdate' AS EntityTypeCode,
		T.JusticeUpdateID AS EntityID,
		T.WorkflowStepNumber
	FROM justiceupdate.JusticeUpdate T

	UNION

	SELECT 
		'PoliceEngagementUpdate' AS EntityTypeCode,
		T.PoliceEngagementUpdateID AS EntityID,
		T.WorkflowStepNumber
	FROM policeengagementupdate.PoliceEngagementUpdate T

	UNION

	SELECT
		'ProgramReport' AS EntityTypeCode,
		T.ProgramReportID AS EntityID,
		T.WorkflowStepNumber
	FROM weeklyreport.ProgramReport T

	UNION

	SELECT 
		'RecommendationUpdate' AS EntityTypeCode,
		T.RecommendationUpdateID AS EntityID,
		T.WorkflowStepNumber
	FROM recommendationupdate.RecommendationUpdate T

	UNION

	SELECT 
		'RiskUpdate' AS EntityTypeCode,
		T.RiskUpdateID AS EntityID,
		T.WorkflowStepNumber
	FROM riskupdate.RiskUpdate T

	UNION

	SELECT
		'SpotReport' AS EntityTypeCode,
		T.SpotReportID AS EntityID,
		T.WorkflowStepNumber
	FROM dbo.SpotReport T

	UNION

	SELECT 
		'WeeklyReport' AS EntityTypeCode,
		T.WeeklyReportID AS EntityID,
		T.WorkflowStepNumber
	FROM weeklyreport.WeeklyReport T

	ORDER BY EntityTypeCode, EntityID
	
OPEN oCursor
FETCH oCursor INTO @cEntityTypeCode, @nEntityID, @nWorkflowStepNumber
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC workflow.InitializeEntityWorkflow @cEntityTypeCode, @nEntityID

	UPDATE EWSGP
	SET EWSGP.IsComplete = 1
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @cEntityTypeCode
		AND EWSGP.EntityID = @nEntityID
		AND EWSGP.WorkflowStepNumber < @nWorkflowStepNumber
			
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID, @nWorkflowStepNumber
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

DECLARE @tTable TABLE (EntityID INT, WorkflowStepID INT)

;
WITH D AS
	(
	SELECT
		EWS.EntityWorkflowStepID,
		EWS.EntityID,
		EWS.WorkflowStepID,
		EWS.IsComplete,
		WS.WorkflowStepNumber
	FROM workflow.EntityWorkflowStep EWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWS.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNote CN
				WHERE CN.WorkflowStepNumber IN (4,9)
					AND CN.COnceptNoteID = EWS.EntityID
				)
			AND EXISTS
				(
				SELECT WS1.WorkflowStepID
				FROM workflow.WorkflowStep WS1
				WHERE WS1.ParentWorkflowStepID != 0
					AND WS1.WorkflowID = W.WorkflowID
					AND WS1.WorkflowStepID = WS.WorkflowStepID
				)
	)

INSERT INTO @tTable
	(EntityID,WorkflowStepID)
SELECT
	E.EntityID,
	E.WorkflowStepID
FROM
	(
	SELECT
		D.EntityID,
		D.WorkflowStepID,
		D.IsComplete,
		D.WorkflowStepNumber
	FROM D
	WHERE EXISTS
		(
		SELECT 1
		FROM D D1
		WHERE D1.EntityID = D.EntityID
			AND D1.WorkflowStepNumber = 4
			AND D1.IsComplete = 0
		)
		AND EXISTS
			(
			SELECT 1
			FROM D D2
			WHERE D2.EntityID = D.EntityID
				AND D2.WorkflowStepNumber = 4
				AND D2.IsComplete = 1
			)
		AND D.WorkflowStepNumber = 4

	UNION ALL

	SELECT
		D.EntityID,
		D.WorkflowStepID,
		D.IsComplete,
		D.WorkflowStepNumber
	FROM D
	WHERE EXISTS
		(
		SELECT 1
		FROM D D1
		WHERE D1.EntityID = D.EntityID
			AND D1.WorkflowStepNumber = 9
			AND D1.IsComplete = 0
		)
		AND EXISTS
			(
			SELECT 1
			FROM D D2
			WHERE D2.EntityID = D.EntityID
				AND D2.WorkflowStepNumber = 9
				AND D2.IsComplete = 1
			)
		AND D.WorkflowStepNumber = 9
	) E
WHERE E.IsComplete = 1

UPDATE EWSGP
SET EWSGP.IsComplete = 1
FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	JOIN @tTable T ON T.EntityID = EWSGP.EntityID
		AND EWSGP.EntityTypeCode = 'ConceptNote'
	JOIN workflow.WorkflowStepGroup WSG ON WSG.LegacyWorkflowStepID = T.WorkflowStepID
		AND WSG.WorkflowStepGroupID = EWSGP.WorkflowStepGroupID
GO
