USE AJACS
GO

--Begin procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.11
-- Description:	A stored procedure to get data from the communityprovinceengagementupdate.CommunityProvinceEngagementUpdate table
-- ==============================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CommunityProvinceEngagementUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (CommunityProvinceEngagementUpdateID INT)

		INSERT INTO communityprovinceengagementupdate.CommunityProvinceEngagementUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.CommunityProvinceEngagementUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @CommunityProvinceEngagementUpdateID = O.CommunityProvinceEngagementUpdateID FROM @tOutput O
		
		EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='CommunityProvinceEngagementUpdate', @EntityID=@CommunityProvinceEngagementUpdateID

		END
	ELSE
		SELECT @CommunityProvinceEngagementUpdateID = CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('CommunityProvinceEngagementUpdate', @CommunityProvinceEngagementUpdateID)
	
	--CommunityProvinceEngagement
	SELECT
		CPEU.CommunityProvinceEngagementUpdateID, 
		CPEU.WorkflowStepNumber 
	FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			JOIN communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ON CPEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CPEU.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('CommunityProvinceEngagementUpdate', @CommunityProvinceEngagementUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('CommunityProvinceEngagementUpdate', @CommunityProvinceEngagementUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Community Province Engagement'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Community Province Engagement'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Community Province Engagement'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Community Province Engagement'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
		AND EL.EntityID = @CommunityProvinceEngagementUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.CommunityAssetID,		
		C1.CommunityAssetUnitID,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,		
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,		
		C1.CommunityID,		
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate(
			(SELECT CreateDateTime 
			FROM eventlog.EventLog 
			WHERE eventcode = 'create'
  			AND entitytypecode = 'contact'
  			AND entityid = C1.ContactID)
		)  AS InitialEntryDateFormatted,
		C1.IsActive,		
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.ProvinceID,		
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(
			SELECT CommunitySubGroupName
			FROM dropdown.CommunitySubGroup
			WHERE CommunitySubGroupID = 
				(
				SELECT CommunitySubGroupID 
				FROM dbo.Community
				WHERE CommunityID = C1.CommunityID
				)
		) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
	WHERE CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		CNC.USVettingDate,
		CNC.UKVettingDate,
		dbo.FormatDate(CNC.USVettingDate) AS USVettingDateFormatted,
		dbo.FormatDate(CNC.UKVettingDate) AS UKVettingDateFormatted,
		VO1.VettingOutcomeID AS USVettingOutcomeID,
		VO1.VettingOutcomeName AS USVettingOutcomeName,
		VO2.VettingOutcomeID AS UKVettingOutcomeID,
		VO2.VettingOutcomeName AS UKVettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CNC.USVettingOutcomeID
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CNC.UKVettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

		SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.27
-- Description:	Bug fixes
--
-- Author:			Todd Pires
-- Create Date: 2015.09.27
-- Description:	Added Recommendations
--
-- Author:			Todd Pires
-- Create Date: 2016.02.11
-- Description:	Added Program Report
--
-- Author:			Todd Pires
-- Create Date: 2016.03.22
-- Description:	Implemented support for the new workflow system
-- ==============================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN 'fa fa-fw fa-lightbulb-o'
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN 'fa fa-fw fa-question-circle'
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN 'fa fa-fw fa-calendar'
		END AS Icon,

		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN OAR.RecommendationName
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		CASE
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN OAWR.PhysicalFileName
			ELSE NULL
		END AS PhysicalFileName,

		D.EntityID,
		D.UpdateDate,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('Recommendation','RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'Recommendation' AND permissionable.HasPermission('Recommendation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				R.RecommendationName,
				R.IsActive
			FROM recommendation.Recommendation R
			WHERE R.RecommendationID = D.EntityID
					AND D.EntityTypeCode = 'Recommendation'
			) OAR
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle,
				workflow.GetWorkflowStepNumber(D.EntityTypeCode, D.EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(D.EntityTypeCode, D.EntityID) AS WorkflowStepCount
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
			AND
				(
				D.EntityTypeCode <> 'Recommendation'
					OR OAR.IsActive = 1
				)
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
			AND
				(
				D.EntityTypeCode <> 'SpotReport'
					OR OASR.WorkflowStepNumber > OASR.WorkflowStepCount
				)
			AND
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)

	UNION
	
	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		'fa-newspaper-o' AS Icon,
		ET.EntityTypeName,
		D.DocumentTitle AS Title,
		D.PhysicalFileName,
		0 AS EntityID,
		D.DocumentDate AS UpdateDate,
		dbo.FormatDate(D.DocumentDate) AS UpdateDateFormatted
	FROM dbo.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DT.DocumentTypeCode = 'ProgramReport'
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = DT.DocumentTypeCode
			AND D.DocumentDate >= DATEADD(d, -14, getDate())
			AND permissionable.HasPermission('ProgramReport.View', @PersonID) = 1

	ORDER BY 8 DESC, 1, 7
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:		Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), 0) AS ThumbnailLength,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('SpotReport', @SpotReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('SpotReport', @SpotReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @SpotReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END 
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure eventlog.LogEquipmentDistributionAction
EXEC utility.DropObject 'eventlog.LogEquipmentDistributionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.10.30
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentDistributionAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentDistribution',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDistributedInventory VARCHAR(MAX) 
	
		SELECT 
			@cDistributedInventory = COALESCE(@cDistributedInventory, '') + D.DistributedInventory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DistributedInventory'), ELEMENTS) AS DistributedInventory
			FROM procurement.DistributedInventory T 
			WHERE T.EquipmentDistributionID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentDistribution',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<DistributedInventory>' + ISNULL(@cDistributedInventory, '') + '</DistributedInventory>') AS XML)
			FOR XML RAW('EquipmentDistribution'), ELEMENTS
			)
		FROM procurement.EquipmentDistribution T
		WHERE T.EquipmentDistributionID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentDistributionAction

--Begin procedure eventlog.LogPermissionableAction
EXEC utility.DropObject 'eventlog.LogPermissionableAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Permissionable',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('Permissionable'), ELEMENTS
			)
		FROM permissionable.Permissionable T 
		WHERE T.PermissionableID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableAction

--Begin procedure dropdown.GetControllerData
EXEC Utility.DropObject 'dropdown.GetControllerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetControllerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.ControllerName
	FROM permissionable.Permissionable T
	ORDER BY T.ControllerName

END
GO
--End procedure dropdown.GetControllerData

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.20
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@IncludeZero BIT = 0,
@PersonID INT = 0,
@HasAddUpdate BIT = 0,
@HasView BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DG.DocumentGroupID,
		DG.DocumentGroupName,
		DT.DocumentTypeID,
		DT.DocumentTypePermissionCode,
		DT.DocumentTypeName
	FROM dropdown.DocumentType DT
		JOIN dropdown.DocumentGroup DG ON DG.DocumentGroupID = DT.DocumentGroupID
			AND DT.IsActive = 1
			AND (DT.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PersonPermissionable PP
				WHERE PP.PersonID = @PersonID
					AND 
						(
						@HasAddUpdate = 1 AND PP.PermissionableLineage = 'Document.AddUpdate.' + DT.DocumentTypePermissionCode
							OR @HasView = 1 AND PP.PermissionableLineage = 'Document.View.' + DT.DocumentTypePermissionCode
						)
				)
	ORDER BY DG.DisplayOrder, DT.DocumentTypeName

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetMethodData
EXEC Utility.DropObject 'dropdown.GetMethodData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.06
-- Description:	A stored procedure to return data from the permissionable.Permissionable table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetMethodData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.MethodName
	FROM permissionable.Permissionable T
	ORDER BY T.MethodName

END
GO
--End procedure dropdown.GetMethodData

--Begin procedure fifupdate.GetFIFUpdate
EXEC Utility.DropObject 'fifupdate.GetFIFUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to get data from the fifupdate.FIFUpdate table
-- ==============================================================================
CREATE PROCEDURE fifupdate.GetFIFUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FIFUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM fifupdate.FIFUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (FIFUpdateID INT)

		INSERT INTO fifupdate.FIFUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.FIFUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @FIFUpdateID = O.FIFUpdateID FROM @tOutput O
		
		EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='FIFUpdate', @EntityID=@FIFUpdateID

		END
	ELSE
		SELECT @FIFUpdateID = FU.FIFUpdateID FROM fifupdate.FIFUpdate FU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('FIFUpdate', @FIFUpdateID)

	--FIF
	SELECT
		FU.FIFUpdateID, 
		FU.WorkflowStepNumber 
	FROM fifupdate.FIFUpdate FU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
	ORDER BY C2.CommunityName, C1.CommunityID
	
	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	ORDER BY P2.ProvinceName, P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'
			JOIN fifupdate.FIFUpdate FU ON FU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND FU.FIFUpdateID = @FIFUpdateID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('FIFUpdate', @FIFUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('FIFUpdate', @FIFUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created FIF Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected FIF Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved FIF Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated FIF Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'FIFUpdate'
		AND EL.EntityID = @FIFUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure fifupdate.GetFIFUpdate¿

--Begin procedure justiceupdate.GetJusticeUpdate
EXEC Utility.DropObject 'justiceupdate.GetJusticeUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get data for a justice update batch
-- ======================================================================
CREATE PROCEDURE justiceupdate.GetJusticeUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @JusticeUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM justiceupdate.JusticeUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (JusticeUpdateID INT)

		INSERT INTO justiceupdate.JusticeUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.JusticeUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @JusticeUpdateID = O.JusticeUpdateID FROM @tOutput O
		
		EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='JusticeUpdate', @EntityID=@JusticeUpdateID

		END
	ELSE
		SELECT @JusticeUpdateID = FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('JusticeUpdate', @JusticeUpdateID)

	--Justice
	SELECT
		FU.JusticeUpdateID, 
		FU.WorkflowStepNumber 
	FROM justiceupdate.JusticeUpdate FU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDATETIME(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM justiceupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
	ORDER BY C2.CommunityName, C1.CommunityID
	
	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDATETIME(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM justiceupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	ORDER BY P2.ProvinceName, P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'JusticeUpdate'
			JOIN justiceupdate.JusticeUpdate JU ON JU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND JU.JusticeUpdateID = @JusticeUpdateID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('JusticeUpdate', @JusticeUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('JusticeUpdate', @JusticeUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Justice Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Justice Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Justice Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Justice Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'JusticeUpdate'
		AND EL.EntityID = @JusticeUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure justiceupdate.GetJusticeUpdate

--Begin procedure permissionable.AddPersonPermissionable
EXEC Utility.DropObject 'permissionable.AddPersonPermissionable'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to add data to the permissionable.PersonPermissionable table
-- ============================================================================================
CREATE PROCEDURE permissionable.AddPersonPermissionable

@PersonID INT, 
@PermissionableLineage VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	INSERT INTO permissionable.PersonPermissionable
		(PersonID, PermissionableLineage)
	VALUES
		(@PersonID, @PermissionableLineage)

END
GO
--End procedure permissionable.AddPersonPermissionable

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC Utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Brandon Green
-- Create date:	2015.10.02
-- Description:	A stored procedure to data from the permissionable.Permissionable table
-- ====================================================================================
CREATE PROCEDURE permissionable.GetPermissionableByPermissionableID

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PermissionableID,
		P.ControllerName,
		P.MethodName,
		P.PermissionCode,
		P.PermissionableGroupID,
		P.Description,
		P.IsGlobal,
		P.DisplayOrder
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID
		
END
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionableGroups
EXEC Utility.DropObject 'permissionable.GetPermissionableGroups'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to get data from the permissionable.PermissionableGroup table
-- =============================================================================================
CREATE PROCEDURE permissionable.GetPermissionableGroups

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PG.PermissionableGroupID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		PG.DisplayOrder,
		(SELECT COUNT(P.PermissionableGroupID) FROM permissionable.Permissionable P WHERE P.PermissionableGroupID = PG.PermissionableGroupID) AS ItemCount
	FROM permissionable.PermissionableGroup PG
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, PG.PermissionableGroupID
		
END
GO
--End procedure permissionable.GetPermissionableGroups

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.09
-- Description:	A stored procedure to get data from the permissionable.Permissionable table
-- ========================================================================================
CREATE PROCEDURE permissionable.GetPermissionables

@PersonID INT = 0,
@PermissionableTemplateID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ControllerName,
		P.Description,
		P.DisplayOrder,
		P.IsGlobal,
		P.MethodName,
		P.PermissionableLineage,
		P.PermissionCode,
		P.PermissionableID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP WHERE PP.PersonID = @PersonID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			WHEN @PermissionableTemplateID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID AND PTP.PermissionableID = P.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
						
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage
		
END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure permissionable.GetPersonPermissionables
EXEC Utility.DropObject 'permissionable.GetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to get data from the permissionable.PersonPermissionable table
-- ==============================================================================================
CREATE PROCEDURE permissionable.GetPersonPermissionables

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PP.PermissionableLineage
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
	ORDER BY PP.PermissionableLineage
		
END
GO
--End procedure permissionable.GetPersonPermissionables

--Begin permissionable.HasFileAccess
EXEC Utility.DropObject 'permissionable.HasFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date: 2015.07.18
-- Description:	A stored procedure to get check access to a file
-- =============================================================
CREATE PROCEDURE permissionable.HasFileAccess

@PersonID INT,
@PhysicalFileName VARCHAR(50)

AS
BEGIN

	SELECT 1
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		AND 
			(
			PP.PermissionableLineage = 
				(
				SELECT 'Document.View.' + ISNULL(DT.DocumentTypePermissionCode, '000')
				FROM dbo.Document D
					JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
						AND D.PhysicalFileName = @PhysicalFileName
				)
				OR
					( 
					(SELECT D.DocumentTypeID FROM dbo.Document D WHERE D.PhysicalFileName = @PhysicalFileName) = 0
					)
			)

END
GO
--End procedure permissionable.HasFileAccess

--Begin procedure permissionable.SavePersonPermissionables
EXEC Utility.DropObject 'permissionable.SavePersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.02
-- Description:	A stored procedure to add data to the permissionable.PersonPermissionable table
-- ====================================================================================
CREATE PROCEDURE permissionable.SavePersonPermissionables

@PersonID INT, 
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
		
	DELETE PP
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	UNION

	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE EXISTS
		(
		SELECT 1 
		FROM dbo.ListToTable(@PermissionableIDList, ',') LTT 
		WHERE CAST(LTT.ListItem AS INT) = P.PermissionableID
			AND CAST(LTT.ListItem AS INT) > 0
		)
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.PersonPermissionable PP
			WHERE PP.PermissionableLineage = P.PermissionableLineage
				AND PP.PersonID = @PersonID
			)

END
GO
--End procedure permissionable.SavePersonPermissionables

--Begin procedure policeengagementupdate.GetPoliceEngagementUpdate
EXEC Utility.DropObject 'policeengagementupdate.GetPoliceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.11
-- Description:	A stored procedure to get data from the policeengagementupdate.PoliceEngagementUpdate table
-- ========================================================================================================
CREATE PROCEDURE policeengagementupdate.GetPoliceEngagementUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PoliceEngagementUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM policeengagementupdate.PoliceEngagementUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (PoliceEngagementUpdateID INT)

		INSERT INTO policeengagementupdate.PoliceEngagementUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.PoliceEngagementUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @PoliceEngagementUpdateID = O.PoliceEngagementUpdateID FROM @tOutput O
		
		EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='PoliceEngagementUpdate', @EntityID=@PoliceEngagementUpdateID

		END
	ELSE
		SELECT @PoliceEngagementUpdateID = PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('PoliceEngagementUpdate', @PoliceEngagementUpdateID)
	
	--PoliceEngagement
	SELECT
		PEU.PoliceEngagementUpdateID, 
		PEU.WorkflowStepNumber 
	FROM policeengagementupdate.PoliceEngagementUpdate PEU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'PoliceEngagementUpdate'
			JOIN policeengagementupdate.PoliceEngagementUpdate PEU ON PEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND PEU.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('PoliceEngagementUpdate', @PoliceEngagementUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('PoliceEngagementUpdate', @PoliceEngagementUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Police Engagement Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Police Engagement Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Police Engagement Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Police Engagement Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'PoliceEngagementUpdate'
		AND EL.EntityID = @PoliceEngagementUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure policeengagementupdate.GetPoliceEngagementUpdate

--Begin procedure procurement.GetEquipmentDistributionByEquipmentDistributionID
EXEC Utility.DropObject 'procurement.GetEquipmentDistributionByEquipmentDistributionID'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.03.08
-- Description:	A stored procedure to data from the procurement.EquipmentDistribution table
-- ========================================================================================
CREATE PROCEDURE procurement.GetEquipmentDistributionByEquipmentDistributionID

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ED.Aim,
		ED.Annexes,
		ED.CurrentSituation,
		ED.DeliveredToRecipientDate,
		dbo.FormatDate(ED.DeliveredToRecipientDate) AS DeliveredToRecipientDateFormatted,
		ED.Distribution,
		ED.EquipmentDistributionName,
		ED.Errata,
		ED.ErrataTitle,
		ED.ExportRoute,
		ED.IsActive,
		ED.OperationalResponsibility,
		ED.OperationalSecurity,
		ED.Phase1,
		ED.Phase2,
		ED.Phase3,
		ED.Phase4,
		ED.Phase5,
		ED.PlanOutline,
		ED.RecipientContactID,
		dbo.FormatContactNameByContactID(ED.RecipientContactID, 'LastFirst') AS RecipientContactNameFormatted,
		ED.Summary
	FROM procurement.EquipmentDistribution ED
	WHERE ED.EquipmentDistributionID = @EquipmentDistributionID
		
END
GO
--End procedure procurement.GetEquipmentDistributionByEquipmentDistributionID

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
--
-- Author:			Greg Yingling
-- Update date:	2015.05.26
-- Description:	Added the Audit Outcome, Audit Date, and Audit Evidence Call
--
-- Author:			Todd Pires
-- Update date:	2015.11.29
-- Description:	Refactored the audit recordset
--
-- Author:			Todd Pires
-- Update date:	2016.03.05
-- Description:	Removed the locations recordset, Refactored the audit recordset
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID,
		D.Title,
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentDeliveredToImplementerDate,
		dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted,
		EI.EquipmentInventoryID,
		EI.EquipmentOrderDate,
		dbo.FormatDate(EI.EquipmentOrderDate) AS EquipmentOrderDateFormatted,
		EI.EquipmentRemovalDate,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReasonID,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EI.EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		EI.EquipmentStatusID,
		EI.EquipmentUsageGB,
		EI.EquipmentUsageMinutes,
		EI.ExpirationDate,
		dbo.FormatDate(EI.ExpirationDate) AS ExpirationDateFormatted,
		EI.IMEIMACAddress,
		EI.InServiceDate,
		dbo.FormatDate(EI.InServiceDate) AS InServiceDateFormatted,
		EI.LicenseKey,
		EI.PONumber,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.UnitCost,
		ER.EquipmentRemovalReasonName,
		ES.EquipmentStatusName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		DIA.AuditQuantity, 
		DIA.AuditDate,
		dbo.FormatDate(DIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(DIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = DIA.DocumentID)
			THEN '<a class="btn btn-info" href="/servefile/getFile/GUID/' + (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = DIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN DIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(' + CAST(DIA.DistributedInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.DistributedInventoryAudit DIA
		JOIN procurement.DistributedInventory DI ON DI.DistributedInventoryID = DIA.DistributedInventoryID
			AND DI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = DIA.AuditOutcomeID
	ORDER BY 2 DESC

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.LicenseEquipmentCatalog table
-- ==========================================================================================
CREATE PROCEDURE procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID

@LicenseEquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentCatalogID,
		EC.ItemName,
		L.LicenseID,
		L.LicenseNumber,
		LEC.BudgetLimit,
		LEC.ECCN,
		LEC.LicenseEquipmentCatalogID,
		LEC.LicenseItemDescription,
		LEC.ReferenceCode,
		LEC.QuantityAuthorized,
		LEC.QuantityAuthorized - procurement.GetLicensedQuantityAvailableByLicenseID(EC.EquipmentCatalogID, L.LicenseID) AS QuantityExpended,
		procurement.GetLicensedQuantityAvailableByLicenseID(EC.EquipmentCatalogID, L.LicenseID) AS QuantityRemaining,
		LEC.ItemPageNumber,
		LEC.ItemLineNumber
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = LEC.EquipmentCatalogID
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND LEC.LicenseEquipmentCatalogID = @LicenseEquipmentCatalogID
		
END
GO
--End procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID

--Begin procedure recommendationupdate.GetRecommendationUpdate
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the recommendationupdate.RecommendationUpdate table
-- ====================================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRecommendationUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM recommendationupdate.RecommendationUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RecommendationUpdateID INT)

		INSERT INTO recommendationupdate.RecommendationUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RecommendationUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @nRecommendationUpdateID = O.RecommendationUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRecommendationUpdateAction @EntityID=@nRecommendationUpdateID, @EventCode='Create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='RecommendationUpdate', @EntityID=@nRecommendationUpdateID

		END
	ELSE
		SELECT @nRecommendationUpdateID = RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('RecommendationUpdate', @nRecommendationUpdateID)
	
	SELECT
		RU.RecommendationUpdateID, 
		RU.WorkflowStepNumber 
	FROM recommendationupdate.RecommendationUpdate RU

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
			JOIN recommendationupdate.RecommendationUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RecommendationUpdateID = @nRecommendationUpdateID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('RecommendationUpdate', @nRecommendationUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('RecommendationUpdate', @nRecommendationUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Recommendation Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Recommendation Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Recommendation Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Recommendation Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'RecommendationUpdate'
		AND EL.EntityID = @nRecommendationUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure recommendationupdate.GetRecommendationUpdate

--Begin procedure reporting.IndicatorReport
EXEC Utility.DropObject 'reporting.IndicatorReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure to get CommunityEngagementReport data FROM the eventlog table
-- =============================================================================================
CREATE PROCEDURE reporting.IndicatorReport

 @StartDate Date , @EndDate Date

AS
BEGIN
	



SELECT DISTINCT D.ReportType, D.SubreportType,D.EntityLocationName, D.EntityName, D.Author, D.Notes, D.Value, D.Updatedate, D.EntityID, D.EntityLocationID
FROM
(

SELECT

'Police Engagement' as ReportType,
'Community Indicator' as SubReportType,
C.CommunityName as EntityLocationName,
E.CommunityID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.CommunityProvinceEngagementNotes as Notes,
E.PoliceEngagementRiskValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') as PoliceEngagementRiskValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityIndicators/CommunityIndicator') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID

UNION ALL


SELECT

'Police Engagement' as ReportType,
'Province Indicator' as SubReportType,
P.ProvinceName as EntityLocationName,
E.ProvinceID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.CommunityProvinceEngagementNotes as Notes,
E.PoliceEngagementRiskValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') as PoliceEngagementRiskValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceIndicators/ProvinceIndicator') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID


UNION ALL

SELECT

'Community Engagement' as ReportType,
'Community Indicator' as SubReportType,
C.CommunityName as EntityLocationName,
E.CommunityID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.CommunityProvinceEngagementNotes as Notes, 
E.CommunityProvinceEngagementAchievedValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('CommunityProvinceEngagementAchievedValue[1]', 'nvarchar(max)') as CommunityProvinceEngagementAchievedValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityIndicators/CommunityIndicator') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID



UNION ALL




SELECT

'Community Engagement' as ReportType,
'Province Indicator' as SubReportType,
P.ProvinceName as EntityLocationName,
E.ProvinceID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.CommunityProvinceEngagementNotes as Notes, 
E.CommunityProvinceEngagementAchievedValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('CommunityProvinceEngagementAchievedValue[1]', 'nvarchar(max)') as CommunityProvinceEngagementAchievedValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceIndicators/ProvinceIndicator') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID
) D


UNION ALL


SELECT

'Justice Achieved Indicator' as ReportType,
'Community Indicator' as SubReportType,
C.CommunityName as EntityLocationName,
E.CommunityID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.JusticeNotes as Notes,
E.JusticeAchievedValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('JusticeAchievedValue[1]', 'nvarchar(max)') as JusticeAchievedValue,
P.value('JusticeNotes[1]', 'nvarchar(max)') AS JusticeNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityIndicators/CommunityIndicator') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID

UNION ALL


SELECT

'Justice  Achieved Indicator' as ReportType,
'Province Indicator' as SubReportType,
P.ProvinceName as EntityLocationName,
E.ProvinceID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.JusticeNotes as Notes,
E.JusticeAchievedValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('JusticeAchievedValue[1]', 'nvarchar(max)') as JusticeAchievedValue,
P.value('JusticeNotes[1]', 'nvarchar(max)') AS JusticeNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceIndicators/ProvinceIndicator') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID


UNION ALL


SELECT

'FIF Achieved Indicator' as ReportType,
'Community Indicator' as SubReportType,
C.CommunityName as EntityLocationName,
E.CommunityID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.FIFNotes as Notes,
E.FIFAchievedValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('FIFAchievedValue[1]', 'nvarchar(max)') as FIFAchievedValue,
P.value('FIFNotes[1]', 'nvarchar(max)') AS FIFNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityIndicators/CommunityIndicator') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID

UNION ALL


SELECT

'FIF Achieved Indicator' as ReportType,
'Province Indicator' as SubReportType,
P.ProvinceName as EntityLocationName,
E.ProvinceID as EntityLocationID,
I.IndicatorName as EntityName,
I.IndicatorID as EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
E.FIFNotes as Notes,
E.FIFAchievedValue as Value,
dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

FROM
(
SELECT
P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('FIFAchievedValue[1]', 'nvarchar(max)') as FIFAchievedValue,
P.value('FIFNotes[1]', 'nvarchar(max)') AS FIFNotes,  
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceIndicators/ProvinceIndicator') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID
ORDER BY D.ReportType ,D.SubReportType , D.EntityLocationName, D.EntityName, D.Author , D.UpdateDate

END
GO
--End procedure reporting.IndicatorReport

--Begin procedure reporting.RiskReport
EXEC Utility.DropObject 'reporting.RiskReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure to get CommunityEngagementReport data from the eventlog table
-- =============================================================================================
CREATE PROCEDURE reporting.RiskReport

 @StartDate Date , @EndDate Date

AS
BEGIN
	

		SELECT DISTINCT D.ReportType, D.SubreportType,D.EntityLocationName, D.EntityName, D.Author, D.Notes, D.Value, D.Updatedate, D.EntityID, D.EntityLocationID
		FROM
		(



		SELECT 
		 'Police Engagement' as ReportType,
		  'Community Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.PoliceEngagementRiskNotes as Notes,
		E.PoliceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
		 P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E

		JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'Police Engagement' as ReportType,
		  'Province Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.PoliceEngagementRiskNotes as Notes,
		E.PoliceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
		 P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID

		Union ALL

		SELECT 
		 'Community Engagement' as ReportType,
		  'Province Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.CommunityProvinceEngagementNotes as Notes,
		E.CommunityProvinceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes,
		 P.value('CommunityProvinceEngagementRiskValue[1]', 'nvarchar(max)') AS CommunityProvinceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID
		UNION ALL

		SELECT

		 'Community Engagement' as ReportType,
		 'Community Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.CommunityProvinceEngagementNotes as Notes, 
		E.CommunityProvinceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author , 
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM
		(

		SELECT
		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes,
		 P.value('CommunityProvinceEngagementRiskValue[1]', 'nvarchar(max)') AS CommunityProvinceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime
		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E
		JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID
		) D

		UNION ALL

		SELECT 
		 'FIF Engagement' as ReportType,
		  'Community FIF Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.FIFEngagementRiskNotes as Notes,
		E.FIFEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('FIFNotes[1]', 'nvarchar(max)') AS FIFEngagementRiskNotes,
		 P.value('FIFRiskValue[1]', 'nvarchar(max)') AS FIFEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E

		JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'FIF Engagement' as ReportType,
		  'Province FIF Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.FIFRiskNotes as Notes,
		E.FIFRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('FIFNotes[1]', 'nvarchar(max)') AS FIFRiskNotes,
		 P.value('FIFRiskValue[1]', 'nvarchar(max)') AS FIFRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'Justice Engagement' as ReportType,
		  'Community Justice Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.JusticeRiskNotes as Notes,
		E.JusticeRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('JusticeNotes[1]', 'nvarchar(max)') AS JusticeRiskNotes,
		 P.value('JusticeRiskValue[1]', 'nvarchar(max)') AS JusticeRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E

		JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'Justice Engagement' as ReportType,
		  'Province Justice Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.JusticeRiskNotes as Notes,
		E.JusticeRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('JusticeNotes[1]', 'nvarchar(max)') AS JusticeRiskNotes,
		 P.value('JusticeRiskValue[1]', 'nvarchar(max)') AS JusticeRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID

		ORDER BY D.ReportType ,D.SubReportType , D.EntityLocationName, D.EntityName, D.Author , D.UpdateDate

END
GO
--End procedure reporting.RiskReport

--Begin procedure riskupdate.GetRiskUpdate
EXEC Utility.DropObject 'riskupdate.GetRiskUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.08
-- Description:	A stored procedure to get data from the riskupdate.RiskUpdate table
-- ================================================================================
CREATE PROCEDURE riskupdate.GetRiskUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRiskUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM riskupdate.RiskUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RiskUpdateID INT)

		INSERT INTO riskupdate.RiskUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RiskUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @nRiskUpdateID = O.RiskUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRiskUpdateAction @EntityID=@nRiskUpdateID, @EventCode='Create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='RiskUpdate', @EntityID=@nRiskUpdateID

		END
	ELSE
		SELECT @nRiskUpdateID = RU.RiskUpdateID FROM riskupdate.RiskUpdate RU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('RiskUpdate', @nRiskUpdateID)
	
	SELECT
		RU.RiskUpdateID, 
		RU.WorkflowStepNumber 
	FROM riskupdate.RiskUpdate RU

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
			JOIN riskupdate.RiskUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RiskUpdateID = @nRiskUpdateID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('RiskUpdate', @nRiskUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('RiskUpdate', @nRiskUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Risk Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Risk Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Risk Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Risk Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'RiskUpdate'
		AND EL.EntityID = @nRiskUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure riskupdate.GetRiskUpdate

--Begin procedure utility.SavePermissionable
EXEC utility.DropObject 'utility.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE utility.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@Description VARCHAR(MAX), 
@IsGlobal BIT = 0, 
@DisplayOrder INT = 0,
@PermissionableGroupCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description, 
			P.IsGlobal = @IsGlobal, 
			P.DisplayOrder = @DisplayOrder
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsGlobal, DisplayOrder) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@IsGlobal, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure utility.SavePermissionable

--Begin procedure utility.SavePermissionableGroup
EXEC utility.DropObject 'utility.SavePermissionableGroup'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to save data to the permissionable.PermissionableGroup table
-- ============================================================================================
CREATE PROCEDURE utility.SavePermissionableGroup

@PermissionableGroupCode VARCHAR(50),
@PermissionableGroupName VARCHAR(250),
@DisplayOrder INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode)
		BEGIN
		
		UPDATE PG
		SET 
			PG.PermissionableGroupName = @PermissionableGroupName,
			PG.DisplayOrder = @DisplayOrder
		FROM permissionable.PermissionableGroup PG
		WHERE PG.PermissionableGroupCode = @PermissionableGroupCode
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.PermissionableGroup 
			(PermissionableGroupCode, PermissionableGroupName, DisplayOrder) 
		VALUES 
			(
			@PermissionableGroupCode, 
			@PermissionableGroupName, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure utility.SavePermissionableGroup

--Begin procedure utility.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'utility.UpdateSuperAdministratorPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to update PersonPermissionable data for Super Admins
-- ====================================================================================
CREATE PROCEDURE utility.UpdateSuperAdministratorPersonPermissionables

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM permissionable.PersonPermissionable PP
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND P.IsSuperAdministrator = 1
	
	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1
		CROSS JOIN permissionable.Permissionable P2
	WHERE P1.IsSuperAdministrator = 1

END	
GO
--End procedure utility.UpdateSuperAdministratorPersonPermissionables

--Begin procedure weeklyreport.GetProgramReportByProgramReportID
EXEC Utility.DropObject 'weeklyreport.GetProgramReportByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.11
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.13
-- Description:	Added ResearchPrevious, ResearchProjected, removed Remarks
-- =================================================================================
CREATE PROCEDURE weeklyreport.GetProgramReportByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ProgramReport', @ProgramReportID)

	SELECT
		PR.EngagementPrevious,
		PR.EngagementProjected,
		PR.JusticePrevious,
		PR.JusticeProjected,
		PR.MonitoringPrevious,
		PR.MonitoringProjected,
		PR.OperationsPrevious,
		PR.OperationsProjected,
		PR.OverallPrevious,
		PR.OverallProjected,
		PR.PolicingPrevious,
		PR.PolicingProjected,
		dbo.FormatProgramReportReferenceCode(PR.ProgramReportID) AS ProgramReportReferenceCode,
		PR.ProgramReportEndDate,
		dbo.FormatDate(PR.ProgramReportEndDate) AS ProgramReportEndDateFormatted,
		PR.ProgramReportID,
		PR.ProgramReportName,
		PR.ProgramReportStartDate,
		dbo.FormatDate(PR.ProgramReportStartDate) AS ProgramReportStartDateFormatted,
		PR.ResearchPrevious,
		PR.ResearchProjected,
		PR.StructuresPrevious,
		PR.StructuresProjected,
		PR.WorkflowStepNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('ProgramReport') AS EntityTypeName
	FROM weeklyreport.ProgramReport PR
	WHERE PR.ProgramReportID = @ProgramReportID

	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'ProgramReport'
			AND DE.EntityID = @ProgramReportID
	ORDER BY D.DocumentDescription

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ProgramReport'
			JOIN weeklyreport.ProgramReport PR ON PR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND PR.ProgramReportID = @ProgramReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ProgramReport', @ProgramReportID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ProgramReport', @ProgramReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
				CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Program Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Program Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Program Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Program Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ProgramReport'
		AND EL.EntityID = @ProgramReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END
GO
--End procedure weeklyreport.GetProgramReportByProgramReportID

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O

		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('WeeklyReport', @nWeeklyReportID)
	
	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.WorkflowStepNumber,
		WR.SummaryMapZoom 
	FROM weeklyreport.WeeklyReport WR

		SELECT
		C.CommunityID,
		C.CommunityName,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID,
		ID.ImpactDecisionName,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		C2.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunity SMC WHERE SMC.CommunityID = C.CommunityID AND SMC.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM weeklyreport.Community C
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Community C2 ON C2.CommunityID = C.CommunityID
			 AND C.WeeklyReportID = @nWeeklyReportID

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
			AND CAT.CommunityAssetTypeID = 1
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor,
		'/assets/img/icons/' + REPLACE(ZT.HexColor, '#', '') + '.png' AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CAT.CommunityAssetTypeID = 2
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapIncident SMI WHERE SMI.IncidentID = I.IncidentID AND SMI.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (
				EXISTS	(
					SELECT 1
					FROM dbo.IncidentCommunity IC
						JOIN weeklyreport.Community C ON C.CommunityID = IC.CommunityID
							AND IC.IncidentID = I.IncidentID
							AND C.WeeklyReportID = @nWeeklyReportID
				)
				OR
				EXISTS (
					SELECT 1
					FROM dbo.IncidentProvince IP
						JOIN weeklyreport.Province P ON P.ProvinceID = IP.ProvinceID
							AND IP.IncidentID = I.IncidentID
							AND P.WeeklyReportID = @nWeeklyReportID
				)
			)		

	SELECT
		F.ForceID,
		F.ForceName,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor,
		'/assets/img/icons/' + REPLACE(AOT.HexColor, '#', '') + '.png' AS Icon,
		F.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapForce SMF WHERE SMF.ForceID = F.ForceID AND SMF.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND EXISTS
				(
					SELECT 1
					FROM force.ForceCommunity FC
						JOIN weeklyreport.Community C ON C.CommunityID = FC.CommunityID
							AND FC.ForceID = F.ForceID
							AND C.WeeklyReportID = @nWeeklyReportID
				)

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WR.WeeklyReportID = @nWeeklyReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('WeeklyReport', @nWeeklyReportID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('WeeklyReport', @nWeeklyReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Weekly Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Weekly Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Weekly Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Weekly Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'WeeklyReport'
		AND EL.EntityID = @nWeeklyReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure workflow.DecrementWorkflow
EXEC Utility.DropObject 'workflow.DecrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.10.28
-- Description:	A stored procedure to Decrement a workflow
-- =======================================================
CREATE PROCEDURE workflow.DecrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	UPDATE EWSGP
	SET EWSGP.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	SELECT
		@nWorkflowStepNumber AS OldWorkflowStepNumber,
		workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
		workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount

END
GO
--End procedure workflow.DecrementWorkflow

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
-- ============================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		dbo.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
	ORDER BY 5, WSGP.PersonID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.IncrementWorkflow
EXEC Utility.DropObject 'workflow.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.10.28
-- Description:	A stored procedure to increment a workflow
-- =======================================================
CREATE PROCEDURE workflow.IncrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @nWorkflowStepGroupID INT
	
	SELECT TOP 1
		@nWorkflowStepNumber = EWSGP.WorkflowStepNumber,
		@nWorkflowStepGroupID = EWSGP.WorkflowStepGroupID	
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.PersonID = @PersonID
		AND EWSGP.IsComplete = 0
		AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID
	
	IF @nWorkflowStepNumber > 0 AND @nWorkflowStepGroupID > 0
		BEGIN
	
		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGP.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	ELSE
		BEGIN
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			0 AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	--ENDIF

END
GO
--End procedure workflow.IncrementWorkflow

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1

END
GO
--End procedure workflow.InitializeEntityWorkflow
