USE AJACS
GO

--Begin table dbo.MenuItem
DECLARE @TableName VARCHAR(250) = 'dbo.MenuItem'

EXEC utility.AddColumn @TableName, 'EntityTypeCode', 'VARCHAR(50)'
GO

UPDATE dbo.MenuItem
SET MenuItemCode = 'ProgramReport'
WHERE MenuItemCode = 'ProgramReportList'
GO

UPDATE dbo.MenuItem
SET EntityTypeCode = MenuItemCode
WHERE MenuItemCode IN ('CommunityProvenceEngagementUpdate','ConceptNote','FIFUpdate','JusticeUpdate','PoliceEngagementUpdate','ProgramReport','RecommendationUpdate','RiskUpdate','SpotReport','WeeklyReport')
GO
--End table dbo.MenuItem

--Begin table dbo.Person
DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC utility.AddColumn @TableName, 'IsSuperAdministrator', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
GO
--End table dbo.Person

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
GO

EXEC utility.DropObject 'permissionable.TR_Permissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A trigger to update the permissionable.Permissionable table
-- ========================================================================
CREATE TRIGGER permissionable.TR_Permissionable ON permissionable.Permissionable FOR INSERT, UPDATE
AS
SET ARITHABORT ON

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE @cPermissionableLineage1 VARCHAR(355)
	DECLARE @cPermissionableLineage2 VARCHAR(355)
	DECLARE @nPermissionableID INT
	
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT I.PermissionableID
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPermissionableID
	WHILE @@fetch_status = 0
		BEGIN

		SELECT @cPermissionableLineage1 = P.PermissionableLineage
		FROM permissionable.Permissionable P
		WHERE P.PermissionableID = @nPermissionableID

		UPDATE P
		SET P.PermissionableLineage = 			
			P.ControllerName 
				+ '.' 
				+ P.MethodName
				+ CASE
						WHEN P.PermissionCode IS NOT NULL
						THEN '.' + P.PermissionCode
						ELSE ''
					END

		FROM permissionable.Permissionable P
		WHERE P.PermissionableID = @nPermissionableID

		SELECT @cPermissionableLineage2 = P.PermissionableLineage
		FROM permissionable.Permissionable P
		WHERE P.PermissionableID = @nPermissionableID

		UPDATE MIPL
		SET MIPL.PermissionableLineage = @cPermissionableLineage2
		FROM dbo.MenuItemPermissionableLineage MIPL
		WHERE MIPL.PermissionableLineage = @cPermissionableLineage1

		UPDATE PP
		SET PP.PermissionableLineage = @cPermissionableLineage2
		FROM permissionable.PersonPermissionable PP
		WHERE PP.PermissionableLineage = @cPermissionableLineage1
		
		FETCH oCursor INTO @nPermissionableID
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table permissionable.Permissionable

--Begin table procurement.DistributedInventory
DECLARE @TableName VARCHAR(250) = 'procurement.DistributedInventory'

EXEC utility.DropObject 'procurement.DistributedEquipment'
EXEC utility.DropObject 'procurement.EquipmentDistributionEquipmentInventory'
EXEC utility.DropObject @TableName

CREATE TABLE procurement.DistributedInventory
	(
	DistributedInventoryID INT IDENTITY(1,1) NOT NULL,
	EquipmentDistributionID INT,
	EquipmentInventoryID INT,
	Quantity INT,
	DeliveredToEndUserDate DATE,
	EndUserEntityTypeCode VARCHAR(50),
	EndUserEntityID INT,
	IsLost BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EndUserEntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentDistributionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsLost', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DistributedInventoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_DistributedInventory', 'EquipmentDistributionID,EquipmentInventoryID'
GO
--End table procurement.DistributedInventory

--Begin table procurement.DistributedInventoryAudit
DECLARE @TableName VARCHAR(250) = 'procurement.DistributedInventoryAudit'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.DistributedInventoryAudit
	(
	DistributedInventoryAuditID INT IDENTITY(1,1) NOT NULL,
	DistributedInventoryID INT,
	DocumentID INT,
	AuditOutcomeID INT,
	AuditQuantity INT,
	AuditDate DATE,
	PersonID INT,
	AuditNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuditDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AuditQuantity', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DistributedInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DistributedInventoryAuditID'
EXEC utility.SetIndexClustered @TableName, 'IX_DistributedInventoryAudit', 'DistributedInventoryID,AuditDate DESC'
GO
--End table procurement.DistributedInventoryAudit

--Begin table procurement.EquipmentDistribution
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistribution'

EXEC utility.AddColumn @TableName, 'Aim', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Annexes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CurrentSituation', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Distribution', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Errata', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'ErrataTitle', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'ExportRoute', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'OperationalResponsibility', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'OperationalSecurity', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Phase1', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Phase2', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Phase3', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Phase4', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Phase5', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PlanOutline', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Summary', 'VARCHAR(MAX)'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
GO
--End table procurement.EquipmentDistribution

--Begin table procurement.EquipmentInventory
EXEC utility.AddColumn 'procurement.EquipmentInventory', 'PONumber', 'VARCHAR(25)'
GO
--End table procurement.EquipmentInventory

--Begin table procurement.LicenseEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.LicenseEquipmentInventory'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.LicenseEquipmentInventory
	(
	LicenseEquipmentInventoryID INT IDENTITY(1,1) NOT NULL,
	LicenseID INT,
	EquipmentInventoryID INT,
	Quantity INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'LicenseID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LicenseEquipmentInventoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_LicenseEquipmentInventory', 'LicenseID,EquipmentInventoryID'
GO
--End table procurement.LicenseEquipmentInventory

--Begin Synonyms
EXEC Utility.DropObject 'syslog.ApplicationErrorLog'
GO
EXEC Utility.DropObject 'syslog.DuoWebTwoFactorLog'
GO

CREATE SYNONYM syslog.ApplicationErrorLog FOR AJACSUtility.syslog.ApplicationErrorLog
GO
CREATE SYNONYM syslog.DuoWebTwoFactorLog FOR AJACSUtility.syslog.DuoWebTwoFactorLog
GO
--End Synonyms
