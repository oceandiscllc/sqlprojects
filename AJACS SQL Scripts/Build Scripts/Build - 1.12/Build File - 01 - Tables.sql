USE AJACS
GO

--Begin table dbo.Class
DECLARE @TableName VARCHAR(250) = 'dbo.Class'

EXEC utility.AddColumn @TableName, 'QualityAssuranceFeedback', 'VARCHAR(MAX)'

EXEC utility.DropColumn @TableName, 'ExternalCapacity'
GO
--End table dbo.Class

--Begin table dbo.ContactContactType
DECLARE @TableName VARCHAR(250) = 'dbo.ContactContactType'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactContactType
	(
	ContactContactTypeID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	ContactTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactContactTypeID'
EXEC utility.SetIndexClustered 'IX_ContactContactType', @TableName, 'ContactID,ContactTypeID'
GO
--End table dbo.ContactContactType

--Begin table dropdown.ContactType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactType
	(
	ContactTypeID INT IDENTITY(0,1) NOT NULL,
	ContactTypeCode VARCHAR(50),
	ContactTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactTypeID'
EXEC utility.SetIndexNonClustered 'IX_ContactTypeName', @TableName, 'DisplayOrder,ContactTypeName','ContactTypeID'
GO

SET IDENTITY_INSERT dropdown.ContactType ON
GO

INSERT INTO dropdown.ContactType (ContactTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ContactType OFF
GO

INSERT INTO dropdown.ContactType 
	(ContactTypeCode,ContactTypeName,DisplayOrder)
VALUES
	('Beneficiary','Beneficiary',1),
	('StaffPartner','Staff / Partner',2),
	('Stipend','Stipend',3)
GO 
--End table dropdown.ContactType

--Begin table dropdown.EquipmentRemovalReason
DECLARE @TableName VARCHAR(250) = 'dropdown.EquipmentRemovalReason'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EquipmentRemovalReason
	(
	EquipmentRemovalReasonID INT IDENTITY(0,1) NOT NULL,
	EquipmentRemovalReasonName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentRemovalReasonID'
EXEC utility.SetIndexNonClustered 'IX_EquipmentRemovalReasonName', @TableName, 'DisplayOrder,EquipmentRemovalReasonName','EquipmentRemovalReasonID'
GO

SET IDENTITY_INSERT dropdown.EquipmentRemovalReason ON
GO

INSERT INTO dropdown.EquipmentRemovalReason (EquipmentRemovalReasonID) VALUES (0)

SET IDENTITY_INSERT dropdown.EquipmentRemovalReason OFF
GO

INSERT INTO dropdown.EquipmentRemovalReason 
	(EquipmentRemovalReasonName,DisplayOrder)
VALUES
	('Damaged',1),
	('Stolen',2),
	('Lost',3),
	('Written Off',4)
GO 
--End table dropdown.EquipmentRemovalReason

--Begin table dropdown.EquipmentStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.EquipmentStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EquipmentStatus
	(
	EquipmentStatusID INT IDENTITY(0,1) NOT NULL,
	EquipmentStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentStatusID'
EXEC utility.SetIndexNonClustered 'IX_EquipmentStatusName', @TableName, 'DisplayOrder,EquipmentStatusName','EquipmentStatusID'
GO

SET IDENTITY_INSERT dropdown.EquipmentStatus ON
GO

INSERT INTO dropdown.EquipmentStatus (EquipmentStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.EquipmentStatus OFF
GO

INSERT INTO dropdown.EquipmentStatus 
	(EquipmentStatusName,DisplayOrder)
VALUES
	('In Stock',1),
	('In Service',2),
	('Removed',3)
GO 
--End table dropdown.EquipmentStatus

--Begin table dropdown.VettingOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.VettingOutcome'

EXEC utility.AddColumn @TableName, 'HexColor', 'VARCHAR(7)'
GO
--End table dropdown.VettingOutcome

--Begin table procurement.EquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentCatalog'

EXEC utility.AddColumn @TableName, 'Impact', 'INT'
EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'
EXEC utility.AddColumn @TableName, 'MitigationNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Risk', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'Impact', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'Risk', 'INT', 0
GO
--End table procurement.EquipmentCatalog

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'EquipmentRemovalDate', 'DATE'
EXEC utility.AddColumn @TableName, 'EquipmentRemovalReasonID', 'INT'
EXEC utility.AddColumn @TableName, 'EquipmentRemovalReporterPersonID', 'INT'
EXEC utility.AddColumn @TableName, 'EquipmentStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentRemovalReasonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentRemovalReporterPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentStatusID', 'INT', 0
GO
--End table procurement.EquipmentInventory

--Begin table weeklyreport.ProgramReport
DECLARE @TableName VARCHAR(250) = 'weeklyreport.ProgramReport'

EXEC utility.AddColumn @TableName, 'ResearchPrevious', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'ResearchProjected', 'VARCHAR(MAX)'

EXEC utility.DropColumn @TableName, 'Remarks'
GO
--End table weeklyreport.ProgramReport
