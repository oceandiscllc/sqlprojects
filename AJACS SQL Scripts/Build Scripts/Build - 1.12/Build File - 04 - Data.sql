USE AJACS
GO

--Begin table dbo.EntityType
UPDATE dbo.EntityType
SET EntityTypeName = 'Bi-Weekly Program Report'
WHERE EntityTypeName = 'Weekly Program Report'
GO
--End table dbo.EntityType

--Begin table dropdown.EngagementCriteriaStatus
UPDATE dropdown.EngagementCriteriaStatus
SET IsActive = 0
WHERE EngagementCriteriaStatusName LIKE 'Provisional%'
GO
--End table dropdown.EngagementCriteriaStatus

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'TrainingCourseDocument')
	BEGIN
	
	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode, DocumentTypeName, DisplayOrder, IsActive)
	VALUES 
		('TrainingCourseDocument','Training Course Document',11,0),
		('QualityAssuranceFeedbackDocument','Quality Assurance Feedback Document',12,0),
		('ParticipantsDocument','Participants Document',13,0),
		('Trainer1Document','Trainer 1 Document',14,0),
		('Trainer2Document','Trainer 2 Document',15,0)

	END
--ENDIF
GO
--End table dropdown.DocumentType

--Begin table dropdown.VettingOutcome
UPDATE dropdown.VettingOutcome
SET VettingOutcomeName = 'Not Vetted'
WHERE VettingOutcomeID = 0
GO

UPDATE dropdown.VettingOutcome SET HexColor = '#999999' WHERE VettingOutcomeName = 'Not Vetted'
UPDATE dropdown.VettingOutcome SET HexColor = '#FFFF00' WHERE VettingOutcomeName = 'Pending'
UPDATE dropdown.VettingOutcome SET HexColor = '#336600' WHERE VettingOutcomeName = 'Consider'
UPDATE dropdown.VettingOutcome SET HexColor = '#FF0000' WHERE VettingOutcomeName = 'Do Not Consider'
GO
--End table dropdown.VettingOutcome
