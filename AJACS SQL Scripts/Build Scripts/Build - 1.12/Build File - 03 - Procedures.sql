USE AJACS
GO

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName,
		CNC.ConceptNoteID
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = @ClassID
	
	SELECT

		CASE
			WHEN CO.CommunityID > 0
			THEN (SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CO.CommunityID)
			WHEN CO.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CO.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'Class'
			AND DE.EntityID = @ClassID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
-- ==================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
		
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetCourseByCourseID
EXEC Utility.DropObject 'dbo.GetCourseByCourseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Kevin Ross
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the dbo.Course table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added call for Course Document information
-- =================================================================
CREATE PROCEDURE dbo.GetCourseByCourseID

@CourseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ActivityCode,
		C.CourseID,
		C.CourseName,
		C.CourseTypeID,
		C.Curriculum,
		C.LearnerProfileTypeID,
		C.ProgramTypeID,
		C.SponsorName,
		C.Syllabus,
		CT.CourseTypeID,
		CT.CourseTypeName,
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Course') AS EntityTypeName
	FROM dbo.Course C
		JOIN dropdown.CourseType CT ON CT.CourseTypeID = C.CourseTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
	WHERE C.CourseID = @CourseID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'Course'
			AND DE.EntityID = @CourseID
	ORDER BY DT.DocumentTypeName
		
END
GO
--End procedure dbo.GetCourseByCourseID

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.18
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID, 
		T.ContactTypeName
	FROM dropdown.ContactType T
	WHERE (T.ContactTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A stored procedure to get data from the dbo.Document table
--
-- Author:			Todd Pires
-- Create date:	2015.05.21
-- Description:	Modified to support weekly reports
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.ContentSubtype,
		D.ContentType,
		0 AS DocumentEntityID
	FROM dbo.Document D
	WHERE 
		(@EntityTypeCode = 'ProgramReport' AND D.DocumentTitle LIKE '%' + dbo.FormatProgramReportReferenceCode(@EntityID)) OR
		(@EntityTypeCode = 'WeeklyReport' AND D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(@EntityID))

	UNION					
					
	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.ContentSubtype,
		D.ContentType,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID

	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure dropdown.GetEquipmentRemovalReasonData
EXEC Utility.DropObject 'dropdown.GetEquipmentRemovalReasonData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Greg Yingling
-- Create date:	2015.05.19
-- Description:	A stored procedure to return data from the dropdown.EquipmentRemovalReason table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetEquipmentRemovalReasonData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentRemovalReasonID, 
		T.EquipmentRemovalReasonName
	FROM dropdown.EquipmentRemovalReason T
	WHERE (T.EquipmentRemovalReasonID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EquipmentRemovalReasonName, T.EquipmentRemovalReasonID

END
GO
--End procedure dropdown.GetEquipmentRemovalReasonData

--Begin procedure dropdown.GetEquipmentStatusData
EXEC Utility.DropObject 'dropdown.GetEquipmentStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Greg Yingling
-- Create date:	2015.05.19
-- Description:	A stored procedure to return data from the dropdown.EquipmentStatus table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetEquipmentStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentStatusID, 
		T.EquipmentStatusName
	FROM dropdown.EquipmentStatus T
	WHERE (T.EquipmentStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EquipmentStatusName, T.EquipmentStatusID

END
GO
--End procedure dropdown.GetEquipmentStatusData

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Todd Pires
-- Create date: 2015.05.19
-- Description:	Added ContactType support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cContactContactAffiliations VARCHAR(MAX) 
	
		SELECT 
			@cContactContactAffiliations = COALESCE(@cContactContactAffiliations, '') + D.ContactContactAffiliation 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactContactAffiliation'), ELEMENTS) AS ContactContactAffiliation
			FROM dbo.ContactContactAffiliation T 
			WHERE T.ContactID = @EntityID
			) D

		DECLARE @cContactContactTypes VARCHAR(MAX) 
	
		SELECT 
			@cContactContactTypes = COALESCE(@cContactContactTypes, '') + D.ContactContactType 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactContactType'), ELEMENTS) AS ContactContactType
			FROM dbo.ContactContactType T 
			WHERE T.ContactID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ContactContactAffiliations>' + ISNULL(@cContactContactAffiliations, '') + '</ContactContactAffiliations>') AS XML),
			CAST(('<ContactContactTypes>' + ISNULL(@cContactContactTypes, '') + '</ContactContactTypes>') AS XML)
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM dbo.Contact T
		WHERE T.ContactID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.EquipmentCatalogID, 
		EC.IsCommon,
		EC.ItemName, 
		EC.Notes, 
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.UnitOfIssue, 
		ISNULL(EC.Risk, 1) AS Risk,
		ISNULL(EC.Impact, 1) AS Impact,
		EC.MitigationNotes, 
		ECC.EquipmentCatalogCategoryID, 
		ECC.EquipmentCatalogCategoryName, 
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentCatalog') AS EntityTypeName
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.EquipmentCatalogCategory ECC ON ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
		
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================

-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.IMEIMACAddress,
		EI.UnitCost,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.EquipmentStatusID,
		ES.EquipmentStatusName,
		EI.EquipmentRemovalReasonID,
		ER.EquipmentRemovalReasonName,
		EI.EquipmentRemovalDate,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'EquipmentInventory'
			AND DE.EntityID = @EquipmentInventoryID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure reporting.GetProgramReportByProgramReportID
EXEC Utility.DropObject 'reporting.GetProgramReportByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date:	2015.04.24
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.13
-- Description:	Added ResearchPrevious, ResearchProjected, removed Remarks
-- =================================================================================
CREATE PROCEDURE reporting.GetProgramReportByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PR.EngagementPrevious,
		PR.EngagementProjected,
		PR.JusticePrevious,
		PR.JusticeProjected,
		PR.MonitoringPrevious,
		PR.MonitoringProjected,
		PR.OperationsPrevious,
		PR.OperationsProjected,
		PR.OverallPrevious,
		PR.OverallProjected,
		PR.PolicingPrevious,
		PR.PolicingProjected,
		dbo.FormatProgramReportReferenceCode(PR.ProgramReportID) AS ProgramReportReferenceCode,
		PR.ProgramReportEndDate,
		dbo.FormatDate(PR.ProgramReportEndDate) AS ProgramReportEndDateFormatted,
		PR.ProgramReportID,
		PR.ProgramReportName,
		PR.ProgramReportStartDate,
		dbo.FormatDate(PR.ProgramReportStartDate) AS ProgramReportStartDateFormatted,
		PR.ResearchPrevious,
		PR.ResearchProjected,
		PR.StructuresPrevious,
		PR.StructuresProjected,
		PR.WorkflowStepNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('ProgramReport') AS EntityTypeName
	FROM weeklyreport.ProgramReport PR
	WHERE PR.ProgramReportID = @ProgramReportID

END
GO
--End procedure reporting.GetProgramReportByProgramReportID

--Begin procedure weeklyreport.GetProgramReportByProgramReportID
EXEC Utility.DropObject 'weeklyreport.GetProgramReportByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.11
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.13
-- Description:	Added ResearchPrevious, ResearchProjected, removed Remarks
-- =================================================================================
CREATE PROCEDURE weeklyreport.GetProgramReportByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PR.EngagementPrevious,
		PR.EngagementProjected,
		PR.JusticePrevious,
		PR.JusticeProjected,
		PR.MonitoringPrevious,
		PR.MonitoringProjected,
		PR.OperationsPrevious,
		PR.OperationsProjected,
		PR.OverallPrevious,
		PR.OverallProjected,
		PR.PolicingPrevious,
		PR.PolicingProjected,
		dbo.FormatProgramReportReferenceCode(PR.ProgramReportID) AS ProgramReportReferenceCode,
		PR.ProgramReportEndDate,
		dbo.FormatDate(PR.ProgramReportEndDate) AS ProgramReportEndDateFormatted,
		PR.ProgramReportID,
		PR.ProgramReportName,
		PR.ProgramReportStartDate,
		dbo.FormatDate(PR.ProgramReportStartDate) AS ProgramReportStartDateFormatted,
		PR.ResearchPrevious,
		PR.ResearchProjected,
		PR.StructuresPrevious,
		PR.StructuresProjected,
		PR.WorkflowStepNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('ProgramReport') AS EntityTypeName
	FROM weeklyreport.ProgramReport PR
	WHERE PR.ProgramReportID = @ProgramReportID

	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'ProgramReport'
			AND DE.EntityID = @ProgramReportID
	ORDER BY D.DocumentDescription

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ProgramReport'
			JOIN weeklyreport.ProgramReport PR ON PR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND PR.ProgramReportID = @ProgramReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ProgramReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ProgramReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ProgramReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT PR.WorkflowStepNumber FROM weeklyreport.ProgramReport PR WHERE PR.ProgramReportID = @ProgramReportID) > 0
					THEN (SELECT PR.WorkflowStepNumber FROM weeklyreport.ProgramReport PR WHERE PR.ProgramReportID = @ProgramReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure weeklyreport.GetProgramReportByProgramReportID
