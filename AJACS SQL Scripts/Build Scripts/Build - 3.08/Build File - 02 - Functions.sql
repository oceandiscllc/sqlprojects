USE AJACS
GO

--Begin function procurement.GetDistributedInventoryQuantityAvailable
EXEC utility.DropObject 'procurement.GetDistributedInventoryQuantityAvailable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.28
-- Description:	A function to get the quantity available for a DistributedInventory item
--
-- Author:			Todd Pires
-- Create date:	2016.10.24
-- Description:	Bug fix
--
-- Author:			Todd Pires
-- Create date:	2016.12.21
-- Description:	Bug fix
--
-- Author:			Jonathan Burnham
-- Create date:	2018.03.20
-- Description:	Bug fix
-- =====================================================================================

CREATE FUNCTION procurement.GetDistributedInventoryQuantityAvailable
(
@EquipmentDistributionID INT,
@EquipmentInventoryID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0

	SELECT @nReturn = 
		EI.Quantity - 
		EI.QuantityDistributed - 
			(
			SELECT ISNULL(SUM(DI.Quantity), 0)
			FROM procurement.DistributedInventory DI 
			JOIN procurement.EquipmentDistribution ED ON ED.EquipmentDistributionID = DI.EquipmentDistributionID 
				AND DI.EquipmentDistributionID = @EquipmentDistributionID
				AND DI.EquipmentInventoryID = @EquipmentInventoryID
				AND ED.IsActive = 1
			)
	FROM procurement.EquipmentInventory EI
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
	
	RETURN @nReturn
END
GO
--End function procurement.GetDistributedInventoryQuantityAvailable