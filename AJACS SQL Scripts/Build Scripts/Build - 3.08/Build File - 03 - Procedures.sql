USE AJACS
GO

--Begin procedure reporting.GetActivityAnalysis
EXEC Utility.DropObject 'reporting.GetActivityAnalysis'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================
-- Author:			Kevin Ross
-- Create date: 2017.07.20
-- Description:	A stored procedure to get GetActivityAnalysis data
-- ===============================================================
CREATE PROCEDURE reporting.GetActivityAnalysis

@PersonID INT,
@ConceptNoteID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		CN.ConceptNoteID,
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CAST(CAST((CNI.ActualQuantity / CAST(I.TargetValue AS DECIMAL(10, 2))) * 100 AS DECIMAL(5, 0)) AS varchar(5)) + ' %' AS ActivityTargetPercentage,
		CNI.TargetQuantity,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeName,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' : ' + dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS FullTitle,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ConceptNoteRefCode,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS Title,
		dbo.GetConceptNoteVersionNumberByConceptNoteID(CN.ConceptNoteID) AS VersionNumber,

		CASE
			WHEN DIS.IndicatorStatusName = 'On Track'
			THEN dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/4DAF4A.png'
			WHEN DIS.IndicatorStatusName = 'Off Track'
			THEN dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/E41A1C.png'
			WHEN DIS.IndicatorStatusName = 'Progressing'
			THEN dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/FFFF33.png'
			ELSE dbo.GetServerSetupValueByServerSetupKey('SiteURL','') +'/assets/img/icons/A65628.png' 
		END AS IndicatorStatusIcon,

		I.IndicatorName,
		I.TargetValue AS OverallTarget,
		IT.IndicatorTypeName,
		O.ObjectiveName,
		(SELECT COUNT(RTI.IndicatorID) FROM logicalframework.Indicator RTI JOIN dbo.ConceptNoteIndicator RTCNI ON RTCNI.IndicatorID = RTI.IndicatorID JOIN dbo.ConceptNote RTCN ON RTCN.ConceptNoteID = RTCNI.ConceptNoteID JOIN dropdown.ConceptNoteType RTCNT ON RTCNT.ConceptNoteTypeID = RTCN.ConceptNoteTypeID) AS RecordsTotal
	FROM logicalframework.Indicator I
		JOIN Reporting.SearchResult SR ON SR.EntityID = I.IndicatorID 
			AND SR.EntityTypeCode='ActivityAnalysis' 
			AND SR.PersonID = @PersonID
		JOIN dbo.ConceptNoteIndicator CNI ON CNI.IndicatorID = SR.EntityID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode NOT IN ('Amended','Cancelled','OnHold')
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.IndicatorStatus DIS ON DIS.IndicatorStatusID = I.IndicatorStatusID
			AND (@ConceptNoteID = 0 OR CN.ConceptNoteID = @ConceptNoteID)
	ORDER BY dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' : ' + dbo.FormatConceptNoteTitle(CN.ConceptNoteID), CNT.ConceptNoteTypeName, I.IndicatorName, O.ObjectiveName

END
GO
--End procedure reporting.GetActivityAnalysis