USE AJACS
GO

--Begin table dbo.KeyEvent (old table name:  atmospherics)
DECLARE @TableName VARCHAR(250) = 'dbo.KeyEvent'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.KeyEvent
	(
	KeyEventID INT IDENTITY(1,1) NOT NULL,
	KeyEventTypeID INT,
	KeyEventTypeDescription NVARCHAR(50),
	KeyEventCategoryID INT,
	KeyEventDate DATE,
	KeyEventReportedDate DATE,
	ConfidenceLevelID INT,
	Information NVARCHAR(MAX),
	Recommendation NVARCHAR(MAX),
	IsCritical BIT,
	HasPoliceInvolvement BIT,
	PoliceInvolvementNotes NVARCHAR(MAX),
	Implications NVARCHAR(MAX),
	RiskMitigation NVARCHAR(MAX),
	Resourcing NVARCHAR(MAX),
	ResourcingStatus NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConfidenceLevelID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'HasPoliceInvolvement', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsCritical', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KeyEventCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KeyEventReportedDate', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'KeyEventTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'KeyEventID'
GO

SET IDENTITY_INSERT dbo.KeyEvent ON
GO

INSERT INTO dbo.KeyEvent
	(KeyEventID, KeyEventTypeID, KeyEventTypeDescription, KeyEventCategoryID, KeyEventDate, KeyEventReportedDate, ConfidenceLevelID, Information, Recommendation, IsCritical, HasPoliceInvolvement, PoliceInvolvementNotes)
SELECT
	A.atmospheric_id, 
	ISNULL((SELECT T.KeyEventTypeID FROM dropdown.KeyEventType T WHERE T.KeyEventTypeName = A.atmospheric_type), 0),
	A.type_description, 
	ISNULL((SELECT T.KeyEventCategoryID FROM dropdown.KeyEventCategory T WHERE T.KeyEventCategoryName = A.atmospheric_category), 0),
	A.reported_date, 
	A.entry_date, 
	ISNULL((SELECT T.ConfidenceLevelID FROM dropdown.ConfidenceLevel T WHERE T.ConfidenceLevelName = A.report_confidence), 0),
	A.narative_information, 
	A.recommendation, 

	CASE 
		WHEN A.critical = 'YES' 
		THEN 1 
		ELSE 0 
	END,

	CASE 
		WHEN A.fsp_involvement = 'YES' 
		THEN 1 
		ELSE 0 
	END,

	A.fsp_notes
FROM amnuna_db.dbo.atmospherics A
GO

SET IDENTITY_INSERT dbo.KeyEvent OFF
GO
--End table dbo.KeyEvent

--Begin table dbo.KeyEventCommunity (old table name:  community_atmospheric)
DECLARE @TableName VARCHAR(250) = 'dbo.KeyEventCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.KeyEventCommunity
	(
	KeyEventCommunityID INT IDENTITY(1,1) NOT NULL,
	KeyEventID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KeyEventID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KeyEventCommunityID'
EXEC utility.SetIndexClustered 'IX_KeyEventCommunity', @TableName, 'KeyEventID,CommunityID'
GO

INSERT INTO dbo.KeyEventCommunity
	(KeyEventID,CommunityID)
SELECT
	KE.KeyEventID,
	CA.Community_ID
FROM amnuna_db.dbo.community_atmospheric CA
	JOIN dbo.KeyEvent KE ON KE.KeyEventID = CA.atmospheric_id
GO
--End table dbo.KeyEventCommunity

--Begin table dbo.KeyEventEngagementCriteriaResult (old table name:  atmospheric_criteria_engagement)
DECLARE @TableName VARCHAR(250) = 'dbo.KeyEventEngagementCriteriaResult'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.KeyEventEngagementCriteriaResult
	(
	KeyEventEngagementCriteriaResultID INT IDENTITY(1,1) NOT NULL,
	KeyEventID INT,
	EngagementCriteriaID INT,
	EngagementCriteriaChoiceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'KeyEventID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EngagementCriteriaID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EngagementCriteriaChoiceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KeyEventEngagementCriteriaResultID'
EXEC utility.SetIndexClustered 'IX_KeyEventEngagementCriteriaResult', @TableName, 'KeyEventID,EngagementCriteriaID,EngagementCriteriaChoiceID'
GO

INSERT INTO dbo.KeyEventEngagementCriteriaResult
	(KeyEventID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.KeyEventID,
	ISNULL((SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Security Requirements'), 0),
	ISNULL((SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.Security), 0)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.KeyEvent KE ON KE.KeyEventID = ACE.atmospheric_id
GO

INSERT INTO dbo.KeyEventEngagementCriteriaResult
	(KeyEventID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.KeyEventID,
	ISNULL((SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Donor Restrictions'), 0),
	ISNULL((SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.DonorRedLines), 0)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.KeyEvent KE ON KE.KeyEventID = ACE.atmospheric_id
GO

INSERT INTO dbo.KeyEventEngagementCriteriaResult
	(KeyEventID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.KeyEventID,
	ISNULL((SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Political/Power Dynamics'), 0),
	ISNULL((SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.PoliticalPowerDynamics), 0)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.KeyEvent KE ON KE.KeyEventID = ACE.atmospheric_id
GO

INSERT INTO dbo.KeyEventEngagementCriteriaResult
	(KeyEventID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.KeyEventID,
	ISNULL((SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Stakeholder Groups'), 0),
	ISNULL((SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.CommunityGroups), 0)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.KeyEvent KE ON KE.KeyEventID = ACE.atmospheric_id
GO

INSERT INTO dbo.KeyEventEngagementCriteriaResult
	(KeyEventID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.KeyEventID,
	ISNULL((SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'FSP Operational Presence'), 0),
	ISNULL((SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.FSPOperational), 0)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.KeyEvent KE ON KE.KeyEventID = ACE.atmospheric_id
GO
--End table dbo.KeyEventEngagementCriteriaResult

--Begin table dbo.KeyEventProvince (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dbo.KeyEventProvince'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.KeyEventProvince
	(
	KeyEventProvinceID INT IDENTITY(1,1) NOT NULL,
	KeyEventID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'KeyEventID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KeyEventProvinceID'
EXEC utility.SetIndexClustered 'IX_KeyEventProvince', @TableName, 'KeyEventID,ProvinceID'
GO
--End table dbo.KeyEventProvince

--Begin table dbo.KeyEventSource (old table name:  sources)
DECLARE @TableName VARCHAR(250) = 'dbo.KeyEventSource'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.KeyEventSource
	(
	KeyEventSourceID INT IDENTITY(1,1) NOT NULL,
	KeyEventID INT,
	SourceName NVARCHAR(250),
	SourceAttribution NVARCHAR(MAX),
	SourceSummary NVARCHAR(MAX),
	SourceDate DATE,
	SourceTypeID INT,
	SourceCategoryID INT,
	ConfidenceLevelID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConfidenceLevelID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KeyEventID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SourceCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SourceTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'KeyEventSourceID'
EXEC utility.SetIndexClustered 'IX_KeyEventSource', @TableName, 'KeyEventID,KeyEventSourceID'
GO

INSERT INTO dbo.KeyEventSource
	(KeyEventID, SourceName, SourceAttribution, SourceSummary, SourceDate, SourceTypeID, SourceCategoryID, ConfidenceLevelID)
SELECT
	S.atmospheric_id,
	'Source',
	S.Atribution,
	S.Source_Description,
	S.Source_Date,
	ISNULL((SELECT T.SourceTypeID FROM dropdown.SourceType T WHERE T.SourceTypeName = S.Type), 0),
	ISNULL((SELECT T.SourceCategoryID FROM dropdown.SourceCategory T WHERE T.SourceCategoryName = S.Location), 0),
	ISNULL((SELECT T.ConfidenceLevelID FROM dropdown.ConfidenceLevel T WHERE T.ConfidenceLevelName = S.Confidence), 0)
FROM amnuna_db.dbo.sources S
	JOIN dbo.KeyEvent KE ON KE.KeyEventID = S.atmospheric_id
GO
--End table dbo.KeyEventSource
