USE AJACS
GO

--Begin table dropdown.AtmosphericType (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.AtmosphericType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AtmosphericType
	(
	AtmosphericTypeID INT IDENTITY(0,1) NOT NULL,
	AtmosphericTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AtmosphericTypeID'
EXEC utility.SetIndexNonClustered 'IX_AtmosphericTypeName', @TableName, 'DisplayOrder,AtmosphericTypeName', 'AtmosphericTypeID'
GO

SET IDENTITY_INSERT dropdown.AtmosphericType ON
GO

INSERT INTO dropdown.AtmosphericType (AtmosphericTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.AtmosphericType OFF
GO

INSERT INTO dropdown.AtmosphericType 
	(AtmosphericTypeName,DisplayOrder)
VALUES
	('General Security', 1),
	('Humanitarian Conditions', 2),
	('Local Institutions', 3),
	('Local Stakeholders', 4),
	('Police', 5)
GO
--End table dropdown.AtmosphericType

--Begin table dropdown.BudgetType (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.BudgetType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.BudgetType
	(
	BudgetTypeID INT IDENTITY(0,1) NOT NULL,
	BudgetTypeName VARCHAR(100),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'BudgetTypeID'
EXEC utility.SetIndexNonClustered 'IX_BudgetTypeName', @TableName, 'DisplayOrder,BudgetTypeName', 'BudgetTypeID'
GO

SET IDENTITY_INSERT dropdown.BudgetType ON
GO

INSERT INTO dropdown.BudgetType (BudgetTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.BudgetType OFF
GO

INSERT INTO dropdown.BudgetType 
	(BudgetTypeName,DisplayOrder)
VALUES
	('Other Direct Costs (Training)', 1),
	('Travel, per diem and Accommodation', 2),
	('Short term Technical Advisors (Police, RSD and TNA)', 3),
	('Contractual', 4),
	('Equipment', 5)
GO
--End table dropdown.BudgetType

--Begin table dropdown.CommunityEngagementStatus (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityEngagementStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityEngagementStatus
	(
	CommunityEngagementStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityEngagementStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityEngagementStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityEngagementStatusName', @TableName, 'DisplayOrder,CommunityEngagementStatusName', 'CommunityEngagementStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityEngagementStatus ON
GO

INSERT INTO dropdown.CommunityEngagementStatus (CommunityEngagementStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityEngagementStatus OFF
GO

INSERT INTO dropdown.CommunityEngagementStatus 
	(CommunityEngagementStatusName,DisplayOrder) 
VALUES
	('Not Yet Engaged', 1),
	('Engagement Started', 2),
	('Engaged - No Issues', 3),
	('Engaged - Minor Issues', 4),
	('Engaged - Major Issues', 5),
	('Engagement Suspected', 6),
	('Engagement Stopped', 7),
	('RAP Phase 2', 8),
	('RAP Phase 3', 9)
GO
--End table dropdown.CommunityEngagementStatus

--Begin table dropdown.CommunityGroup (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityGroup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityGroup
	(
	CommunityGroupID INT IDENTITY(0,1) NOT NULL,
	CommunityGroupName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityGroupID'
EXEC utility.SetIndexNonClustered 'IX_CommunityGroupName', @TableName, 'DisplayOrder,CommunityGroupName', 'CommunityGroupID'
GO

SET IDENTITY_INSERT dropdown.CommunityGroup ON
GO

INSERT INTO dropdown.CommunityGroup (CommunityGroupID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityGroup OFF
GO

INSERT INTO dropdown.CommunityGroup 
	(CommunityGroupName,DisplayOrder) 
VALUES
	('Group 1', 1),
	('Group 2', 2),
	('Group 3', 3),
	('Group 4', 4)
GO
--End table dropdown.CommunityGroup

--Begin table dropdown.CommunitySubGroup (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunitySubGroup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunitySubGroup
	(
	CommunitySubGroupID INT IDENTITY(0,1) NOT NULL,
	CommunitySubGroupName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunitySubGroupID'
EXEC utility.SetIndexNonClustered 'IX_CommunitySubGroupName', @TableName, 'DisplayOrder,CommunitySubGroupName', 'CommunitySubGroupID'
GO

SET IDENTITY_INSERT dropdown.CommunitySubGroup ON
GO

INSERT INTO dropdown.CommunitySubGroup (CommunitySubGroupID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunitySubGroup OFF
GO

INSERT INTO dropdown.CommunitySubGroup 
	(CommunitySubGroupName,DisplayOrder) 
VALUES
	('Phase 1', 1),
	('Phase 2', 2),
	('Phase 3', 3),
	('Phase 4', 4)
GO
--End table dropdown.CommunitySubGroup

--Begin table dropdown.ConfidenceLevel (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.ConfidenceLevel'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConfidenceLevel
	(
	ConfidenceLevelID INT IDENTITY(0,1) NOT NULL,
	ConfidenceLevelName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConfidenceLevelID'
EXEC utility.SetIndexNonClustered 'IX_ConfidenceLevelName', @TableName, 'DisplayOrder,ConfidenceLevelName', 'ConfidenceLevelID'
GO

SET IDENTITY_INSERT dropdown.ConfidenceLevel ON
GO

INSERT INTO dropdown.ConfidenceLevel (ConfidenceLevelID) VALUES (0)

SET IDENTITY_INSERT dropdown.ConfidenceLevel OFF
GO

INSERT INTO dropdown.ConfidenceLevel 
	(ConfidenceLevelName,DisplayOrder)
VALUES
	('High', 2),
	('Low', 4),
	('Medium', 3),
	('Not Known', 5),
	('Very High', 1)
GO
--End table dropdown.ConfidenceLevel

--Begin table dropdown.CourseType (old table name:  coursetype)
DECLARE @TableName VARCHAR(250) = 'dropdown.CourseType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CourseType
	(
	CourseTypeID INT IDENTITY(0,1) NOT NULL,
	CourseTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CourseTypeID'
EXEC utility.SetIndexNonClustered 'IX_CourseTypeName', @TableName, 'DisplayOrder,CourseTypeName', 'CourseTypeID'
GO

SET IDENTITY_INSERT dropdown.CourseType ON
GO

INSERT INTO dropdown.CourseType 
	(CourseTypeID,CourseTypeName,DisplayOrder) 
SELECT
	CT.CourseTypeID,
	CT.CourseTypeName,
	CT.DisplayOrder
FROM amnuna_db.dbo.coursetype CT
GO

SET IDENTITY_INSERT dropdown.CourseType OFF
GO
--End table dropdown.CourseType

--Begin table dropdown.DailyReportType (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.DailyReportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DailyReportType
	(
	DailyReportTypeID INT IDENTITY(0,1) NOT NULL,
	DailyReportTypeName VARCHAR(50),
	HasCommunities BIT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'HasCommunities', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DailyReportTypeID'
EXEC utility.SetIndexNonClustered 'IX_DailyReportTypeName', @TableName, 'DisplayOrder,DailyReportTypeName', 'DailyReportTypeID'
GO

SET IDENTITY_INSERT dropdown.DailyReportType ON
GO

INSERT INTO dropdown.DailyReportType (DailyReportTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.DailyReportType OFF
GO

INSERT INTO dropdown.DailyReportType 
	(DailyReportTypeName,DisplayOrder,HasCommunities)
VALUES
	('Morning Check-in', 1, 0),
	('End of Day', 2, 1)
GO
--End table dropdown.DailyReportType

--Begin table dropdown.DateFilter (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.DateFilter'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DateFilter
	(
	DateFilterID INT IDENTITY(1,1) NOT NULL,
	DateFilterName VARCHAR(50),
	DateNumber INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DateFilterID'
EXEC utility.SetIndexNonClustered 'IX_DateFilterName', @TableName, 'DisplayOrder,DateFilterName', 'DateFilterID'
GO

SET IDENTITY_INSERT dropdown.DateFilter ON
GO

INSERT INTO dropdown.DateFilter (DateFilterID) VALUES (0)

SET IDENTITY_INSERT dropdown.DateFilter OFF
GO

INSERT INTO dropdown.DateFilter 
	(DateFilterName,DateNumber,DisplayOrder)
VALUES
	('Today', 1, 1),
	('Last Week', 7, 2),
	('Last 2 Weeks', 14, 3),
	('Last 30 days', 30, 4),
	('Last 90 days', 90, 5)
GO
--End table dropdown.DateFilter

--Begin table dropdown.DocumentType (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentType
	(
	DocumentTypeID INT IDENTITY(0,1) NOT NULL,
	DocumentTypeCode VARCHAR(50),
	DocumentTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentTypeID'
EXEC utility.SetIndexNonClustered 'IX_DocumentTypeName', @TableName, 'DisplayOrder,DocumentTypeName', 'DocumentTypeID'
GO

SET IDENTITY_INSERT dropdown.DocumentType ON
GO

INSERT INTO dropdown.DocumentType (DocumentTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.DocumentType OFF
GO

INSERT INTO dropdown.DocumentType 
	(DocumentTypeCode,DocumentTypeName,DisplayOrder)
VALUES
	('SpotReport', 'Spot Report', 1),
	('WeeklyReport', 'Weekly Report', 4),
	('ProvincialWeeklyReport', 'Provincial Weekly Information Report', 2),
	('SyriaWeeklyReport', 'Syria Weekly Information Report', 3),
	('OtherDocument', 'Other Document', 5)
GO
--End table dropdown.DocumentType

--Begin table dropdown.EngagementCriteria (old table name:  communityengagementcriteria)
DECLARE @TableName VARCHAR(250) = 'dropdown.EngagementCriteria'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EngagementCriteria
	(
	EngagementCriteriaID INT IDENTITY(1,1) NOT NULL,
	EngagementCriteriaName VARCHAR(50),
	EngagementCriteriaDescription VARCHAR(500),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EngagementCriteriaID'
EXEC utility.SetIndexNonClustered 'IX_EngagementCriteriaName', @TableName, 'DisplayOrder,EngagementCriteriaName', 'EngagementCriteriaID'
GO

SET IDENTITY_INSERT dropdown.EngagementCriteria ON
GO

INSERT INTO dropdown.EngagementCriteria 
	(EngagementCriteriaID,EngagementCriteriaName,EngagementCriteriaDescription,DisplayOrder) 
SELECT
	CEC.CommunityEngagementCriteriaID,
	CEC.CommunityEngagementCriteriaName,
	CEC.CommunityEngagementCriteriaDescription,
	CEC.DisplayOrder
FROM amnuna_db.dbo.communityengagementcriteria CEC
GO

SET IDENTITY_INSERT dropdown.EngagementCriteria OFF
GO
--End table dropdown.EngagementCriteria

--Begin table dropdown.EngagementCriteriaChoice (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.EngagementCriteriaChoice'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EngagementCriteriaChoice
	(
	EngagementCriteriaChoiceID INT IDENTITY(1,1) NOT NULL,
	EngagementCriteriaChoiceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EngagementCriteriaChoiceID'
EXEC utility.SetIndexNonClustered 'IX_EngagementCriteriaChoiceName', @TableName, 'DisplayOrder,EngagementCriteriaChoiceName', 'EngagementCriteriaChoiceID'
GO

INSERT INTO dropdown.EngagementCriteriaChoice 
	(EngagementCriteriaChoiceName,DisplayOrder)
VALUES
	('Positive', 1),
	('Neutral', 2),
	('Negative', 3)
GO
--End table dropdown.EngagementCriteriaChoice

--Begin table dropdown.EngagementCriteriaStatus (old table name:  communityengagementcriteriastatus)
DECLARE @TableName VARCHAR(250) = 'dropdown.EngagementCriteriaStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EngagementCriteriaStatus
	(
	EngagementCriteriaStatusID INT IDENTITY(0,1) NOT NULL,
	EngagementCriteriaStatusName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EngagementCriteriaStatusID'
EXEC utility.SetIndexNonClustered 'IX_EngagementCriteriaStatusName', @TableName, 'DisplayOrder,EngagementCriteriaStatusName', 'EngagementCriteriaStatusID'
GO

SET IDENTITY_INSERT dropdown.EngagementCriteriaStatus ON
GO

INSERT INTO dropdown.EngagementCriteriaStatus 
	(EngagementCriteriaStatusID,EngagementCriteriaStatusName,HexColor,DisplayOrder)
SELECT
	CECS.CommunityEngagementCriteriaStatusID,
	CECS.CommunityEngagementCriteriaStatusName,
	CECS.HexColor,
	CECS.DisplayOrder
FROM amnuna_db.dbo.communityengagementcriteriastatus CECS
GO

SET IDENTITY_INSERT dropdown.EngagementCriteriaStatus OFF
GO
--End table dropdown.EngagementCriteriaStatus

--Begin table dropdown.EquipmentCatalogCategory (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.EquipmentCatalogCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EquipmentCatalogCategory
	(
	EquipmentCatalogCategoryID INT IDENTITY(0,1) NOT NULL,
	EquipmentCatalogCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentCatalogCategoryID'
EXEC utility.SetIndexNonClustered 'IX_EquipmentCatalogCategoryName', @TableName, 'DisplayOrder,EquipmentCatalogCategoryName', 'EquipmentCatalogCategoryID'
GO

SET IDENTITY_INSERT dropdown.EquipmentCatalogCategory ON
GO

INSERT INTO dropdown.EquipmentCatalogCategory (EquipmentCatalogCategoryID) VALUES (0)

SET IDENTITY_INSERT dropdown.EquipmentCatalogCategory OFF
GO

INSERT INTO dropdown.EquipmentCatalogCategory 
	(EquipmentCatalogCategoryName,DisplayOrder)
VALUES
	('Electronics',1),
	('General',2), 
	('Medical',3),
	('Police Equipment',4),
	('Satellite Communications',5),
	('Training Materials',6)
GO
--End table dropdown.EquipmentCatalogCategory

--Begin table dropdown.ImpactDecision (old table name:  impactdecision)
DECLARE @TableName VARCHAR(250) = 'dropdown.ImpactDecision'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ImpactDecision
	(
	ImpactDecisionID INT IDENTITY(0,1) NOT NULL,
	ImpactDecisionName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImpactDecisionID'
EXEC utility.SetIndexNonClustered 'IX_ImpactDecisionName', @TableName, 'DisplayOrder,ImpactDecisionName', 'ImpactDecisionID'
GO

SET IDENTITY_INSERT dropdown.ImpactDecision ON
GO

INSERT INTO dropdown.ImpactDecision 
	(ImpactDecisionID,ImpactDecisionName,HexColor,DisplayOrder)
SELECT
	ID.ImpactDecisionID,
	ID.ImpactDecisionName,
	ID.HexColor,
	ID.DisplayOrder
FROM amnuna_db.dbo.impactdecision ID
GO

SET IDENTITY_INSERT dropdown.ImpactDecision OFF
GO

UPDATE ID1
SET ID1.HexColor = (SELECT ID2.HexColor FROM dropdown.ImpactDecision ID2 WHERE ID2.ImpactDecisionID = 1)
FROM dropdown.ImpactDecision ID1 WHERE ID1.ImpactDecisionID = 0
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.KeyEventCategory (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.KeyEventCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.KeyEventCategory
	(
	KeyEventCategoryID INT IDENTITY(0,1) NOT NULL,
	KeyEventCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'KeyEventCategoryID'
EXEC utility.SetIndexNonClustered 'IX_KeyEventCategoryName', @TableName, 'DisplayOrder,KeyEventCategoryName', 'KeyEventCategoryID'
GO

SET IDENTITY_INSERT dropdown.KeyEventCategory ON
GO

INSERT INTO dropdown.KeyEventCategory (KeyEventCategoryID) VALUES (0)

SET IDENTITY_INSERT dropdown.KeyEventCategory OFF
GO

INSERT INTO dropdown.KeyEventCategory 
	(KeyEventCategoryName,DisplayOrder)
VALUES
	('Atmospheric', 1),
	('Reframed Atmospheric', 2)
GO
--End table dropdown.KeyEventCategory

--Begin table dropdown.KeyEventGroup (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.KeyEventGroup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.KeyEventGroup
	(
	KeyEventGroupID INT IDENTITY(1,1) NOT NULL,
	KeyEventGroupName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'KeyEventGroupID'
EXEC utility.SetIndexNonClustered 'IX_KeyEventGroupName', @TableName, 'DisplayOrder,KeyEventGroupName', 'KeyEventGroupID'
GO

INSERT INTO dropdown.KeyEventGroup 
	(KeyEventGroupName,DisplayOrder)
VALUES
	('Security', 1),
	('Political', 2),
	('Population', 3),
	('Others', 4)
GO
--End table dropdown.KeyEventGroup

--Begin table dropdown.KeyEventType (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.KeyEventType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.KeyEventType
	(
	KeyEventTypeID INT IDENTITY(0,1) NOT NULL,
	KeyEventTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'KeyEventTypeID'
EXEC utility.SetIndexNonClustered 'IX_KeyEventTypeName', @TableName, 'DisplayOrder,KeyEventTypeName', 'KeyEventTypeID'
GO

SET IDENTITY_INSERT dropdown.KeyEventType ON
GO

INSERT INTO dropdown.KeyEventType (KeyEventTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.KeyEventType OFF
GO

INSERT INTO dropdown.KeyEventType 
	(KeyEventTypeName)
VALUES
	('Armed Actors'),
	('Changes'),
	('Conflict'),
	('Crime'),
	('Dynamics'),
	('Events'),
	('Gender Based'),
	('Humanitarian Issue'),
	('Movements'),
	('Other'),
	('Service Delivery Issue')
GO
--End table dropdown.KeyEventType

--Begin table dropdown.KeyEventGroupKeyEventType (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.KeyEventGroupKeyEventType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.KeyEventGroupKeyEventType
	(
	KeyEventGroupKeyEventTypeID INT IDENTITY(1,1) NOT NULL,
	KeyEventGroupID INT,
	KeyEventTypeID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KeyEventGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KeyEventTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'KeyEventGroupID,DisplayOrder,KeyEventTypeID'
GO

INSERT INTO dropdown.KeyEventGroupKeyEventType 
	(KeyEventGroupID,KeyEventTypeID,DisplayOrder)
SELECT
	KEG.KeyEventGroupID,
	KET.KeyEventTypeID,
	ROW_NUMBER() OVER (ORDER BY KET.KeyEventTypeName)
FROM dropdown.KeyEventGroup KEG, dropdown.KeyEventType KET
WHERE KEG.KeyEventGroupName = 'Security'
	AND KET.KeyEventTypeName IN ('Armed Actors','Conflict','Crime','Gender Based')
GO

INSERT INTO dropdown.KeyEventGroupKeyEventType 
	(KeyEventGroupID,KeyEventTypeID,DisplayOrder)
SELECT
	KEG.KeyEventGroupID,
	KET.KeyEventTypeID,
	ROW_NUMBER() OVER (ORDER BY KET.KeyEventTypeName)
FROM dropdown.KeyEventGroup KEG, dropdown.KeyEventType KET
WHERE KEG.KeyEventGroupName = 'Political'
	AND KET.KeyEventTypeName IN ('Changes','Dynamics','Events')
GO

INSERT INTO dropdown.KeyEventGroupKeyEventType 
	(KeyEventGroupID,KeyEventTypeID,DisplayOrder)
SELECT
	KEG.KeyEventGroupID,
	KET.KeyEventTypeID,
	ROW_NUMBER() OVER (ORDER BY KET.KeyEventTypeName)
FROM dropdown.KeyEventGroup KEG, dropdown.KeyEventType KET
WHERE KEG.KeyEventGroupName = 'Population'
	AND KET.KeyEventTypeName IN ('Humanitarian Issue','Movements','Service Delivery Issue')
GO

INSERT INTO dropdown.KeyEventGroupKeyEventType 
	(KeyEventGroupID,KeyEventTypeID,DisplayOrder)
SELECT
	KEG.KeyEventGroupID,
	KET.KeyEventTypeID,
	ROW_NUMBER() OVER (ORDER BY KET.KeyEventTypeName)
FROM dropdown.KeyEventGroup KEG, dropdown.KeyEventType KET
WHERE KEG.KeyEventGroupName = 'Others'
	AND KET.KeyEventTypeName IN ('Other')
GO
--End table dropdown.KeyEventGroupKeyEventType

--Begin table dropdown.LearnerProfileType (old table name:  learnerprofiletype)
DECLARE @TableName VARCHAR(250) = 'dropdown.LearnerProfileType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LearnerProfileType
	(
	LearnerProfileTypeID INT IDENTITY(0,1) NOT NULL,
	LearnerProfileTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'LearnerProfileTypeID'
EXEC utility.SetIndexNonClustered 'IX_LearnerProfileTypeName', @TableName, 'DisplayOrder,LearnerProfileTypeName', 'LearnerProfileTypeID'
GO

SET IDENTITY_INSERT dropdown.LearnerProfileType ON
GO

INSERT INTO dropdown.LearnerProfileType 
	(LearnerProfileTypeID,LearnerProfileTypeName,DisplayOrder)
SELECT
	L.LearnerProfileTypeID,
	L.LearnerProfileTypeName,
	L.DisplayOrder
FROM amnuna_db.dbo.learnerprofiletype L
GO

SET IDENTITY_INSERT dropdown.LearnerProfileType OFF
GO
--End table dropdown.LearnerProfileType

--Begin table dropdown.PolicePresenceCategory (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.PolicePresenceCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PolicePresenceCategory
	(
	PolicePresenceCategoryID INT IDENTITY(0,1) NOT NULL,
	PolicePresenceCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PolicePresenceCategoryID'
EXEC utility.SetIndexNonClustered 'IX_PolicePresenceCategoryName', @TableName, 'DisplayOrder,PolicePresenceCategoryName', 'PolicePresenceCategoryID'
GO

SET IDENTITY_INSERT dropdown.PolicePresenceCategory ON
GO

INSERT INTO dropdown.PolicePresenceCategory (PolicePresenceCategoryID) VALUES (0)

SET IDENTITY_INSERT dropdown.PolicePresenceCategory OFF
GO

INSERT INTO dropdown.PolicePresenceCategory 
	(PolicePresenceCategoryName,DisplayOrder) 
VALUES
	('Yes', 1),
	('No', 2),
	('Unknown', 3)
GO
--End table dropdown.PolicePresenceCategory

--Begin table dropdown.PolicePresenceStatus (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.PolicePresenceStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PolicePresenceStatus
	(
	PolicePresenceStatusID INT IDENTITY(0,1) NOT NULL,
	PolicePresenceStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PolicePresenceStatusID'
EXEC utility.SetIndexNonClustered 'IX_PolicePresenceStatusName', @TableName, 'DisplayOrder,PolicePresenceStatusName', 'PolicePresenceStatusID'
GO

SET IDENTITY_INSERT dropdown.PolicePresenceStatus ON
GO

INSERT INTO dropdown.PolicePresenceStatus (PolicePresenceStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.PolicePresenceStatus OFF
GO

INSERT INTO dropdown.PolicePresenceStatus 
	(PolicePresenceStatusName,DisplayOrder) 
VALUES
	('No Presence', 1),
	('Limited Presence', 2),
	('Some Presence', 3),
	('Fully Functional', 4),
	('Unknown', 5)
GO
--End table dropdown.PolicePresenceStatus

--Begin table dropdown.PoliceStationStatus (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.PoliceStationStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PoliceStationStatus
	(
	PoliceStationStatusID INT IDENTITY(0,1) NOT NULL,
	PoliceStationStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PoliceStationStatusID'
EXEC utility.SetIndexNonClustered 'IX_PoliceStationStatusName', @TableName, 'DisplayOrder,PoliceStationStatusName', 'PoliceStationStatusID'
GO

SET IDENTITY_INSERT dropdown.PoliceStationStatus ON
GO

INSERT INTO dropdown.PoliceStationStatus (PoliceStationStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.PoliceStationStatus OFF
GO

INSERT INTO dropdown.PoliceStationStatus 
	(PoliceStationStatusName,DisplayOrder) 
VALUES
	('Fully in Use', 1),
	('Partially in Use', 2),
	('Building Viable', 3),
	('Non-Viable - Structureal Issues', 4),
	('Non Viable - Utilities not present', 5),
	('No Police Station', 6),
	('Unknown', 7)
GO
--End table dropdown.PoliceStationStatus

--Begin table dropdown.ProgramType (old table name:  programtype)
DECLARE @TableName VARCHAR(250) = 'dropdown.ProgramType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProgramType
	(
	ProgramTypeID INT IDENTITY(0,1) NOT NULL,
	ProgramTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProgramTypeID'
EXEC utility.SetIndexNonClustered 'IX_ProgramTypeName', @TableName, 'DisplayOrder,ProgramTypeName', 'ProgramTypeID'
GO

SET IDENTITY_INSERT dropdown.ProgramType ON
GO

INSERT INTO dropdown.ProgramType 
	(ProgramTypeID,ProgramTypeName,DisplayOrder)
SELECT
	P.ProgramTypeID,
	P.ProgramTypeName,
	P.DisplayOrder
FROM amnuna_db.dbo.programtype P
GO

SET IDENTITY_INSERT dropdown.ProgramType OFF
GO
--End table dropdown.ProgramType

--Begin table dropdown.Role (old table name:  role)
DECLARE @TableName VARCHAR(250) = 'dropdown.Role'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Role
	(
	RoleID INT IDENTITY(0,1) NOT NULL,
	RoleName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RoleID'
EXEC utility.SetIndexNonClustered 'IX_RoleName', @TableName, 'DisplayOrder,RoleName', 'RoleID'
GO

SET IDENTITY_INSERT dropdown.Role ON
GO

INSERT INTO dropdown.Role 
	(RoleID,RoleName,DisplayOrder) 
SELECT
	R.RoleID,
	R.RoleName,
	R.DisplayOrder
FROM amnuna_db.dbo.role R
GO

SET IDENTITY_INSERT dropdown.Role OFF
GO
--End table dropdown.Role

--Begin table dropdown.SourceCategory (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.SourceCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SourceCategory
	(
	SourceCategoryID INT IDENTITY(0,1) NOT NULL,
	SourceCategoryName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SourceCategoryID'
EXEC utility.SetIndexNonClustered 'IX_SourceCategoryName', @TableName, 'DisplayOrder,SourceCategoryName', 'SourceCategoryID'
GO

SET IDENTITY_INSERT dropdown.SourceCategory ON
GO

INSERT INTO dropdown.SourceCategory (SourceCategoryID) VALUES (0)

SET IDENTITY_INSERT dropdown.SourceCategory OFF
GO

INSERT INTO dropdown.SourceCategory 
	(SourceCategoryName,DisplayOrder) 
VALUES
	('Grand Source', 1),
	('In Country Report', 2),
	('Opposition Media', 3),
	('Other', 11),
	('Out of Country Report', 4),
	('Photograph', 5),
	('Pro Regime Media', 6),
	('Social Media', 7),
	('Social Media Group Outlet', 8),
	('Video', 9),
	('Website', 10)
GO
--End table dropdown.SourceCategory

--Begin table dropdown.SourceType (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.SourceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SourceType
	(
	SourceTypeID INT IDENTITY(0,1) NOT NULL,
	SourceTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SourceTypeID'
EXEC utility.SetIndexNonClustered 'IX_SourceTypeName', @TableName, 'DisplayOrder,SourceTypeName', 'SourceTypeID'
GO

SET IDENTITY_INSERT dropdown.SourceType ON
GO

INSERT INTO dropdown.SourceType (SourceTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.SourceType OFF
GO

INSERT INTO dropdown.SourceType 
	(SourceTypeName,DisplayOrder) 
VALUES
	('Primary', 1),
	('Secondary', 2),
	('Tertiary', 3)
GO
--End table dropdown.SourceType

--Begin table dropdown.StatusChange (old table name:  statuschange)
DECLARE @TableName VARCHAR(250) = 'dropdown.StatusChange'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.StatusChange
	(
	StatusChangeID INT IDENTITY(0,1) NOT NULL,
	StatusChangeName VARCHAR(50),
	HexColor VARCHAR(7),
	Direction VARCHAR(6),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'StatusChangeID'
EXEC utility.SetIndexNonClustered 'IX_StatusChangeName', @TableName, 'DisplayOrder,StatusChangeName', 'StatusChangeID'
GO

SET IDENTITY_INSERT dropdown.StatusChange ON
GO

INSERT INTO dropdown.StatusChange 
	(StatusChangeID,StatusChangeName,HexColor,DisplayOrder)
SELECT
	SC.StatusChangeID,
	SC.StatusChangeName,
	SC.HexColor,
	SC.DisplayOrder
FROM amnuna_db.dbo.statuschange SC
GO

SET IDENTITY_INSERT dropdown.StatusChange OFF
GO

INSERT INTO dropdown.StatusChange 
	(StatusChangeName,HexColor,DisplayOrder)
VALUES
	('Continued No Engagement', '#663333', 5)
GO

UPDATE dropdown.StatusChange SET Direction = 'circle' WHERE StatusChangeID IN (0, 4)
UPDATE dropdown.StatusChange SET Direction = 'up' WHERE StatusChangeID = 1
UPDATE dropdown.StatusChange SET Direction = 'down' WHERE StatusChangeID = 2
UPDATE dropdown.StatusChange SET Direction = 'left' WHERE StatusChangeID IN (3, 5)
GO
--End table dropdown.StatusChange

--Begin table dropdown.StudentOutcomeType (old table name:  studentoutcometype)
DECLARE @TableName VARCHAR(250) = 'dropdown.StudentOutcomeType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.StudentOutcomeType
	(
	StudentOutcomeTypeID INT IDENTITY(0,1) NOT NULL,
	StudentOutcomeTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'StudentOutcomeTypeID'
EXEC utility.SetIndexNonClustered 'IX_StudentOutcomeTypeName', @TableName, 'DisplayOrder,StudentOutcomeTypeName', 'StudentOutcomeTypeID'
GO

SET IDENTITY_INSERT dropdown.StudentOutcomeType ON
GO

INSERT INTO dropdown.StudentOutcomeType 
	(StudentOutcomeTypeID,StudentOutcomeTypeName,DisplayOrder) 
SELECT
	SOT.StudentOutcomeTypeID,
	SOT.StudentOutcomeTypeName,
	SOT.DisplayOrder
FROM amnuna_db.dbo.studentoutcometype SOT
GO

SET IDENTITY_INSERT dropdown.StudentOutcomeType OFF
GO
--End table dropdown.StudentOutcomeType

--Begin table dropdown.VettingStatus (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.VettingStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VettingStatus
	(
	VettingStatusID INT IDENTITY(0,1) NOT NULL,
	VettingStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VettingStatusID'
EXEC utility.SetIndexNonClustered 'IX_VettingStatusName', @TableName, 'DisplayOrder,VettingStatusName', 'VettingStatusID'
GO

SET IDENTITY_INSERT dropdown.VettingStatus ON
GO

INSERT INTO dropdown.VettingStatus (VettingStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.VettingStatus OFF
GO

INSERT INTO dropdown.VettingStatus 
	(VettingStatusName,DisplayOrder) 
VALUES
	('Not Vetted', 1),
	('Vetted - Pass', 2),
	('Vetted - Fail', 3)
GO
--End table dropdown.VettingStatus
