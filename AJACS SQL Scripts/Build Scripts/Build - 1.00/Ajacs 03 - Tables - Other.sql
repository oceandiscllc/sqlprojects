USE AJACS
GO

--Begin table dbo.Community (old table name:  community, community_location)
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Community
	(
	CommunityID INT IDENTITY(1,1) NOT NULL,
	CommunityName NVARCHAR(250), 
	ProvinceID INT, 
	CommunityGroupID INT, 
	CommunitySubGroupID INT, 
	PoliceStationNumber INT, 
	PoliceStationStatusID INT, --status_policestation
	PolicePresenceCategoryID INT, --present_representatives_FSPolice
	PolicePresenceStatusID INT, --status_present_FSPolice
	ImpactDecisionID INT, 
	StatusChangeID INT, 
	Population INT, 
	PopulationSource NVARCHAR(250),
	CommunityEngagementStatusID INT, --status_community
	Summary NVARCHAR(MAX), --Notes
	KeyPoints NVARCHAR(MAX),
	Implications NVARCHAR(MAX), 
	RiskMitigation NVARCHAR(MAX), 
	ProgramNotes1 NVARCHAR(MAX), --history_police
	ProgramNotes2 NVARCHAR(MAX), --history_community
	ProgramNotes3 NVARCHAR(MAX), --history_capacitybuilding
	ProgramNotes4 NVARCHAR(MAX), --history_training
	ProgramNotes5 NVARCHAR(MAX), --history_others
	Latitude NUMERIC(20,15),
	Longitude NUMERIC(20,15)
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'CommunityEngagementStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunitySubGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ImpactDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Latitude', 'NUMERIC(20,15)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Longitude', 'NUMERIC(20,15)', 0
EXEC utility.SetDefaultConstraint @TableName, 'PolicePresenceCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PolicePresenceStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceStationNumber', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceStationStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Population', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StatusChangeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
EXEC utility.SetIndexNonClustered 'IX_Community', @TableName, 'CommunityName', 'ProvinceID'
GO

SET IDENTITY_INSERT dbo.Community ON
GO

INSERT INTO dbo.Community
	(CommunityID, CommunityName, ProvinceID, CommunityGroupID, CommunitySubGroupID, PoliceStationNumber, PoliceStationStatusID, PolicePresenceCategoryID, PolicePresenceStatusID, ImpactDecisionID, StatusChangeID, Population, PopulationSource, CommunityEngagementStatusID, Summary, KeyPoints, Implications, RiskMitigation, ProgramNotes1, ProgramNotes2, ProgramNotes3, ProgramNotes4, ProgramNotes5)
SELECT
	C.community_id, 
	C.community_name, 
	C.province, 
	ISNULL((SELECT T.CommunityGroupID FROM dropdown.CommunityGroup T WHERE T.CommunityGroupName = C.group_type), 0),
	ISNULL((SELECT T.CommunitySubGroupID FROM dropdown.CommunitySubGroup T WHERE T.CommunitySubGroupName = C.subgroup_type), 0),
	C.PoliceStationNumber, 
	ISNULL((SELECT T.PoliceStationStatusID FROM dropdown.PoliceStationStatus T WHERE T.PoliceStationStatusName = C.status_policestation), 0),
	ISNULL((SELECT T.PolicePresenceCategoryID FROM dropdown.PolicePresenceCategory T WHERE T.PolicePresenceCategoryName = C.present_representatives_FSPolice), 0),
	ISNULL((SELECT T.PolicePresenceStatusID FROM dropdown.PolicePresenceStatus T WHERE T.PolicePresenceStatusName = C.status_present_FSPolice), 0),
	C.ImpactDecisionID, 
	C.StatusChangeID, 
	C.Population, 
	C.estimate_source, 
	ISNULL((SELECT T.CommunityEngagementStatusID FROM dropdown.CommunityEngagementStatus T WHERE T.CommunityEngagementStatusName = C.status_community), 0),
	C.notes, 
	C.KeyPoints,
	C.Implications, 
	C.RiskMitigation, 
	C.history_police, 
	C.history_community, 
	C.history_capacitybuilding, 
	C.history_training, 
	C.history_others
FROM amnuna_db.dbo.Community C
GO

SET IDENTITY_INSERT dbo.Community OFF
GO

UPDATE C
SET
	C.Latitude = CAST(CL.Latitude AS NUMERIC(20,15)),
	C.Longitude = CAST(CL.Longitude AS NUMERIC(20,15))
FROM dbo.Community C
	JOIN amnuna_db.dbo.community_location CL ON CL.Community_ID = C.CommunityID
GO
--End table dbo.Community

--Begin table dbo.CommunityEngagementCriteriaResult (old table name:  communityengagementcriteriaresults)
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityEngagementCriteriaResult'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityEngagementCriteriaResult
	(
	CommunityEngagementCriteriaResultID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	EngagementCriteriaID INT,
	EngagementCriteriaStatusID INT,
	Notes NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EngagementCriteriaID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EngagementCriteriaStatusID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityEngagementCriteriaResultID'
EXEC utility.SetIndexClustered 'IX_CommunityEngagementCriteriaResult', @TableName, 'CommunityID,EngagementCriteriaID,EngagementCriteriaStatusID'
GO

INSERT INTO dbo.CommunityEngagementCriteriaResult
	(CommunityID,EngagementCriteriaID,EngagementCriteriaStatusID,Notes)
SELECT
	CECR.Community_ID,
	CECR.CommunityEngagementCriteriaID,
	CECR.CommunityEngagementCriteriaStatusID,
	CECR.CommunityEngagementCriteriaNotes
FROM amnuna_db.dbo.communityengagementcriteriaresults CECR
GO
--End table dbo.CommunityEngagementCriteriaResult

--Begin table dbo.Equipment (old table name:  equipment)
DECLARE @TableName VARCHAR(250) = 'dbo.Equipment'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Equipment
	(
	EquipmentID INT IDENTITY(1,1) NOT NULL,
	BudgetCode VARCHAR(15),
	Supplier VARCHAR(100),
	ItemCost NUMERIC(18,2),
	ItemName VARCHAR(100),
	Quantity INT,
	ItemDescription VARCHAR(500),
	SerialNumber VARCHAR(50),
	SIM VARCHAR(10),
	IMEIMACAddress VARCHAR(20),
	IssueDate DATE,
	FinalBeneficiaryGroup VARCHAR(100),
	BeneficiarySignatory VARCHAR(100),
	Location VARCHAR(250),
	SpecificLocation VARCHAR(250),
	Comments NVARCHAR(MAX) NULL
	) 

EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentID'
GO

SET IDENTITY_INSERT dbo.Equipment ON
GO

INSERT INTO dbo.Equipment
	(EquipmentID, BudgetCode, Supplier, ItemCost, ItemName, Quantity, ItemDescription, SerialNumber, SIM, IMEIMACAddress, IssueDate, FinalBeneficiaryGroup, BeneficiarySignatory, Location, SpecificLocation, Comments)
SELECT
	E.EquipmentID, 
	E.BudgetCode, 
	E.Supplier, 
	E.ItemCost, 
	E.ItemName, 
	E.Quantity, 
	E.ItemDescription, 
	E.SerialNumber, 
	E.SIM, 
	E.IMEIMACAddress, 
	E.IssueDate, 
	E.FinalBeneficiaryGroup, 
	E.BeneficiarySignatory, 
	E.Location, 
	E.SpecificLocation, 
	E.Comments
FROM amnuna_db.dbo.equipment E
GO

SET IDENTITY_INSERT dbo.Equipment OFF
GO
--End table dbo.Equipment

--Begin table dbo.EntityType (old table name:  entitytype)
DECLARE @TableName VARCHAR(250) = 'dbo.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeGroupCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeAbbreviation VARCHAR(5)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO

INSERT INTO dbo.EntityType 
	(EntityTypeGroupCode,EntityTypeCode,EntityTypeName,EntityTypeAbbreviation) 
VALUES 
	('RAPData', 'CommunityMemberSurvey', 'Community Member Survey', 'CMS'),
	('RAPData', 'FocusGroupSurvey', 'Focus Group Questionnaire', 'FGQ'),
	('RAPData', 'KeyInformantSurvey', 'Key Informant Interview', 'KII'),
	('RAPData', 'RapidPerceptionSurvey', 'Rapid Perception Survey', 'RPS'),
	('RAPData', 'StakeholderGroupSurvey', 'Stakeholder Group Questionnaire', 'SGQ'),
	('RAPData', 'StationCommanderSurvey', 'FSP Station Commander Survey', 'SCS'),
	('Training', 'Class', 'Class', NULL),
	('Training', 'Course','Course', NULL),
	(NULL, 'Atmospheric', 'General Atmospheric', 'GA'),
	(NULL, 'Contact', 'Contact', 'CON'),
	(NULL, 'KeyEvent', 'Key Event', 'KE'),
	('Procurement', 'ConceptNote', 'Concept Note', 'CN'),
	('Procurement', 'EquipmentCatalog', 'Equipment Catalog', 'EC'),
	('Procurement', 'EquipmentInventory', 'Equipment Inventory', 'EI'),
	('Procurement', 'License', 'License', 'LIC'),
	('Procurement', 'LicenseEquipmentCatalog', 'License Equipment Catalog', 'LEC')
GO

INSERT INTO dbo.EntityType 
	(EntityTypeGroupCode,EntityTypeCode,EntityTypeName) 
SELECT
	'Documents', 
	DT.DocumentTypeCode,
	DT.DocumentTypeName
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeID > 0
GO
--End table dbo.EntityType

--Begin table dbo.Person (old table name:  users)
DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Person
	(
	PersonID INT IDENTITY(1,1) NOT NULL,
	FirstName NVARCHAR(100),
	LastName NVARCHAR(100),
	Title NVARCHAR(50),
	UserName NVARCHAR(250),
	Organization NVARCHAR(250),
	RoleID INT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	Token VARCHAR(36),
	TokenCreateDateTime DATETIME,
	EmailAddress VARCHAR(320),
	IsLegacyPassword BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'RoleID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsLegacyPassword', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO

SET IDENTITY_INSERT dbo.Person ON
GO

INSERT INTO dbo.Person
	(PersonID, UserName, RoleID, Password, PasswordSalt, InvalidLoginAttempts, Token, TokenCreateDateTime, EmailAddress)
SELECT
	U.ID, 
	U.UserName, 
	U.RoleID, 
	U.Password, 
	U.PasswordSalt, 
	U.InvalidLoginAttempts, 
	U.Token, 
	U.TokenIssueDate, 
	U.EmailAddress
FROM amnuna_db.dbo.Users U
GO

SET IDENTITY_INSERT dbo.Person OFF
GO
--End table dbo.Person

--Begin table dbo.Province (old table name:  province)
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Province
	(
	ProvinceID INT IDENTITY(1,1) NOT NULL,
	ProvinceName NVARCHAR(250), 
	ImpactDecisionID INT, 
	StatusChangeID INT, 
	Summary NVARCHAR(MAX), --Notes
	Implications NVARCHAR(MAX), 
	RiskMitigation NVARCHAR(MAX), 
	KeyPoints NVARCHAR(MAX), 
	ProgramNotes1 NVARCHAR(MAX), --history_police
	ProgramNotes2 NVARCHAR(MAX) --history_community
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'ImpactDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StatusChangeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
EXEC utility.SetIndexNonClustered 'IX_Province', @TableName, 'ProvinceName', 'ProvinceID'
GO

SET IDENTITY_INSERT dbo.Province ON
GO

INSERT INTO dbo.Province
	(ProvinceID, ProvinceName, ImpactDecisionID, StatusChangeID, Summary, Implications, RiskMitigation, KeyPoints, ProgramNotes1, ProgramNotes2)
SELECT
	C.province_id, 
	C.province_name, 
	C.ImpactDecisionID, 
	C.StatusChangeID, 
	C.notes, 
	C.Implications, 
	C.RiskMitigation, 
	C.KeyPoints, 
	C.history_police, 
	C.history_community
FROM amnuna_db.dbo.Province C
GO

SET IDENTITY_INSERT dbo.Province OFF
GO
--End table dbo.Province

--Begin table dbo.RequestForInformation (old table name:  requestforinformation)
DECLARE @TableName VARCHAR(250) = 'dbo.RequestForInformation'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.RequestForInformation
	(
  RequestForInformationID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	Location VARCHAR(250),
	Incident VARCHAR(500),
	IncidentDate DATE,
	KnownDetails VARCHAR(MAX),
	InformationRequested VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	) 

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', 0	

EXEC utility.SetPrimaryKeyClustered @TableName, 'RequestForInformationID'

SET IDENTITY_INSERT dbo.RequestForInformation ON
GO

INSERT INTO dbo.RequestForInformation
	(RequestForInformationID, CommunityID, Location, Incident, IncidentDate, KnownDetails, InformationRequested, CreateDateTime, CreatePersonID)
SELECT
	SCS.RequestForInformationID, 
	SCS.Community_ID, 
	SCS.Location, 
	SCS.Incident, 
	SCS.IncidentDate, 
	SCS.KnownDetails, 
	SCS.InformationRequested,
	SCS.CreateDateTime,
	SCS.CreateUserID
FROM amnuna_db.dbo.requestforinformation SCS
GO

SET IDENTITY_INSERT dbo.RequestForInformation OFF
GO
--End table dbo.RequestForInformation