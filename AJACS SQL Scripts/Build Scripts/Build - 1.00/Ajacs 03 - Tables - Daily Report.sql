USE AJACS
GO

--Begin table dbo.DailyReport (old table name:  end_of_day_report, morning_checkin)
DECLARE @TableName VARCHAR(250) = 'dbo.DailyReport'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.DailyReport
	(
	DailyReportID INT IDENTITY(1,1) NOT NULL,
	DailyReportTypeID INT,
	TeamID INT,
	ReportDate DATE,
	PersonID INT,
	IsDutyOfCareComplete BIT,
	Notes NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DailyReportTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsDutyOfCareComplete', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TeamID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DailyReportID'
EXEC utility.SetIndexClustered 'IX_DailyReport', @TableName, 'DailyReportTypeID,ReportDate,PersonID'
GO

SET IDENTITY_INSERT dbo.DailyReport ON
GO

INSERT INTO dbo.DailyReport
	(DailyReportID, DailyReportTypeID, TeamID, ReportDate, PersonID, IsDutyOfCareComplete, Notes)
SELECT
	D.endday_id, 
	(SELECT DRT.DailyReportTypeID FROM dropdown.DailyReportType DRT WHERE DRT.DailyReportTypeName = 'End of Day'), 
	D.team_id, 
	D.endday_date,
	ISNULL((SELECT P.PersonID FROM dbo.Person P WHERE P.UserName = D.endday_user), 0),
	
	CASE
		WHEN D.dutyofcare = 'YES'
		THEN 1
		ELSE 0
	END,
	
	D.notes 
FROM amnuna_db.dbo.end_of_day_report D
GO

SET IDENTITY_INSERT dbo.DailyReport OFF
GO
--End table dbo.DailyReport

--Begin table dbo.DailyReportCommunity (old table name:  endday_community_report)
DECLARE @TableName VARCHAR(250) = 'dbo.DailyReportCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.DailyReportCommunity
	(
	DailyReportCommunityID INT IDENTITY(1,1) NOT NULL,
	DailyReportID INT,
	CommunityID INT,
	Notes NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DailyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DailyReportCommunityID'
EXEC utility.SetIndexClustered 'IX_DailyReportCommunity', @TableName, 'DailyReportID,CommunityID'
GO

INSERT INTO dbo.DailyReportCommunity
	(DailyReportID,CommunityID,Notes)
SELECT
	DC.endday_id,
	DC.community_id,
	DC.notes
FROM amnuna_db.dbo.endday_community_report DC
	JOIN dbo.DailyReport DR ON DR.DailyReportID = DC.endday_id
GO

INSERT INTO dbo.DailyReport
	(DailyReportTypeID, TeamID, ReportDate, PersonID, IsDutyOfCareComplete, Notes)
SELECT
	(SELECT DRT.DailyReportTypeID FROM dropdown.DailyReportType DRT WHERE DRT.DailyReportTypeName = 'Morning Check-in'), 
	D.team_id, 
	D.checkin_date, 
	ISNULL((SELECT P.PersonID FROM dbo.Person P WHERE P.UserName = D.checkin_user), 0),
	
	CASE
		WHEN D.dutyofcare = 'YES'
		THEN 1
		ELSE 0
	END,
	
	D.notes 
FROM amnuna_db.dbo.morning_checkin D
GO
--End table dbo.DailyReportCommunity

--Begin table dbo.Team (old table name:  team)
DECLARE @TableName VARCHAR(250) = 'dbo.Team'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Team
	(
	TeamID INT IDENTITY(1,1) NOT NULL,
	TeamName NVARCHAR(250)
	)
	
EXEC utility.SetPrimaryKeyClustered @TableName, 'TeamID'
EXEC utility.SetIndexNonClustered 'IX_Team', @TableName, 'TeamName'
GO

SET IDENTITY_INSERT dbo.Team ON
GO

INSERT INTO dbo.Team
	(TeamID, TeamName)
SELECT
	T.Team_ID, 
	T.Team_Name
FROM amnuna_db.dbo.Team T
GO

SET IDENTITY_INSERT dbo.Team OFF
GO
--End table dbo.Team

--Begin table dbo.TeamMember (old table name:  team_members)
DECLARE @TableName VARCHAR(250) = 'dbo.TeamMember'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.TeamMember
	(
	TeamMemberID INT IDENTITY(1,1) NOT NULL,
	TeamID INT,
	FirstName NVARCHAR(200),
	LastName NVARCHAR(200)
	)

EXEC utility.SetDefaultConstraint @TableName, 'TeamID', 'INT', 0
	
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'TeamMemberID'
EXEC utility.SetIndexClustered 'IX_TeamMember', @TableName, 'TeamID, LastName, FirstName'
GO

INSERT INTO dbo.TeamMember
	(TeamID, FirstName, LastName)
SELECT
	T.Team_ID, 
	T.FirstName,
	T.SurName
FROM amnuna_db.dbo.team_members T
GO
--End table dbo.TeamMember