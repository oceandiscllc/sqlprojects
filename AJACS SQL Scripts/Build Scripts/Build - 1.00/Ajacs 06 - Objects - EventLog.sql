USE AJACS
GO

--Begin Tables
--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'eventlog.EventLog'

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC Utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC Utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC Utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC Utility.SetPrimaryKeyNonclustered @TableName, 'EventLogID'
EXEC Utility.SetIndexClustered 'IX_EventLog', @TableName, 'EntityTypeCode, EntityID, EventCode, EventLogID'
GO
--End table eventlog.EventLog
--End Tables

--Begin Functions
--Begin function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A function to return document data for a specific entity record
-- ============================================================================

CREATE FUNCTION eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cDocuments VARCHAR(MAX) = '<Documents></Documents>'
	
	IF EXISTS (SELECT 1 FROM dbo.DocumentEntity T WHERE T.EntityTypeCode = @EntityTypeCode AND T.EntityID = @EntityID)
		BEGIN

		SELECT 
			@cDocuments = COALESCE(@cDocuments, '') + D.Document 
		FROM
			(
			SELECT
				(SELECT T.DocumentID FOR XML RAW('Documents'), ELEMENTS) AS Document
			FROM dbo.DocumentEntity T 
			WHERE T.EntityTypeCode = @EntityTypeCode
				AND T.EntityID = @EntityID
			) D
	
		IF @cDocuments IS NULL OR LEN(LTRIM(@cDocuments)) = 0
			SET @cDocuments = '<Documents></Documents>'
		--ENDIF

		END
	--ENDIF
		
	RETURN @cDocuments

END
GO
--End function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID

--Begin function eventlog.GetEntityCreateDateTimeByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'eventlog.GetEntityCreateDateTimeByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A function to return the createdatetime of a specific event log record
-- ===================================================================================

CREATE FUNCTION eventlog.GetEntityCreateDateTimeByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS DATETIME

AS
BEGIN

	DECLARE @dCreateDateTime DATETIME

	SELECT @dCreateDateTime = EL.CreateDateTime
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = @EntityTypeCode
		AND EL.EntityID = @EntityID
		AND EL.EventCode = 'create'
		
	RETURN @dCreateDateTime

END
GO
--End function eventlog.GetEntityCreateDateTimeByEntityTypeCodeAndEntityID
--End Functions

--Begin Procedures
--Begin procedure eventlog.LogAtmosphericAction
EXEC utility.DropObject 'eventlog.LogAtmosphericAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAtmosphericAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Atmospheric',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cAtmosphericCommunities VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericCommunities = COALESCE(@cAtmosphericCommunities, '') + D.AtmosphericCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericCommunity'), ELEMENTS) AS AtmosphericCommunity
			FROM dbo.AtmosphericCommunity T 
			WHERE T.AtmosphericID = @EntityID
			) D

		DECLARE @cAtmosphericEngagementCriteriaResults VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericEngagementCriteriaResults = COALESCE(@cAtmosphericEngagementCriteriaResults, '') + D.AtmosphericEngagementCriteriaResult 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericEngagementCriteriaResult'), ELEMENTS) AS AtmosphericEngagementCriteriaResult
			FROM dbo.AtmosphericEngagementCriteriaResult T 
			WHERE T.AtmosphericID = @EntityID
			) D

		DECLARE @cAtmosphericSources VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericSources = COALESCE(@cAtmosphericSources, '') + D.AtmosphericSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericSource'), ELEMENTS) AS AtmosphericSource
			FROM dbo.AtmosphericSource T 
			WHERE T.AtmosphericID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Atmospheric',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<AtmosphericCommunities>' + ISNULL(@cAtmosphericCommunities, '') + '</AtmosphericCommunities>') AS XML),
			CAST(('<AtmosphericEngagementCriteriaResults>' + ISNULL(@cAtmosphericEngagementCriteriaResults, '') + '</AtmosphericEngagementCriteriaResults>') AS XML),
			CAST(('<AtmosphericSources>' + ISNULL(@cAtmosphericSources, '') + '</AtmosphericSources>') AS XML)
			FOR XML RAW('Atmospheric'), ELEMENTS
			)
		FROM dbo.Atmospheric T
		WHERE T.AtmosphericID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAtmosphericAction

--Begin procedure eventlog.LogClassAction
EXEC utility.DropObject 'eventlog.LogClassAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClassAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Class',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cClassStudents VARCHAR(MAX) 
	
		SELECT 
			@cClassStudents = COALESCE(@cClassStudents, '') + D.ClassStudent 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClassStudent'), ELEMENTS) AS ClassStudent
			FROM dbo.ClassStudent T 
			WHERE T.ClassID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Class',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<ClassStudents>' + ISNULL(@cClassStudents, '') + '</ClassStudents>') AS XML)
			FOR XML RAW('Class'), ELEMENTS
			)
		FROM dbo.Class T
		WHERE T.ClassID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClassAction

--Begin procedure eventlog.LogCommunityAction
EXEC utility.DropObject 'eventlog.LogCommunityAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Community',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cCommunityEngagementCriteriaResults VARCHAR(MAX) 
	
		SELECT 
			@cCommunityEngagementCriteriaResults = COALESCE(@cCommunityEngagementCriteriaResults, '') + D.CommunityEngagementCriteriaResult 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityEngagementCriteriaResult'), ELEMENTS) AS CommunityEngagementCriteriaResult
			FROM dbo.CommunityEngagementCriteriaResult T 
			WHERE T.CommunityID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<CommunityEngagementCriteriaResults>' + ISNULL(@cCommunityEngagementCriteriaResults, '') + '</CommunityEngagementCriteriaResults>') AS XML)
			FOR XML RAW('Community'), ELEMENTS
			)
		FROM dbo.Community T
		WHERE T.CommunityID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAction

--Begin procedure eventlog.LogCommunityMemberSurveyAction
EXEC utility.DropObject 'eventlog.LogCommunityMemberSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityMemberSurveyAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityMemberSurvey',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('CommunityMemberSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityMemberSurvey',
			@EntityID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('CommunityMemberSurvey'), ELEMENTS
			)
		FROM dbo.CommunityMemberSurvey T
		WHERE T.CommunityMemberSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityMemberSurveyAction

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cContactVettings VARCHAR(MAX) 
	
		SELECT 
			@cContactVettings = COALESCE(@cContactVettings, '') + D.ContactVetting 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactVetting'), ELEMENTS) AS ContactVetting
			FROM dbo.ContactVetting T 
			WHERE T.ContactID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<ContactVettings>' + ISNULL(@cContactVettings, '') + '</ContactVettings>') AS XML)
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM dbo.Contact T
		WHERE T.ContactID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogCourseAction
EXEC utility.DropObject 'eventlog.LogCourseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCourseAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Course',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Course',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('Course'), ELEMENTS
			)
		FROM dbo.Course T
		WHERE T.CourseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCourseAction

--Begin procedure eventlog.LogDailyReportAction
EXEC utility.DropObject 'eventlog.LogDailyReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDailyReportAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'DailyReport',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDailyReportCommunities VARCHAR(MAX) 
	
		SELECT 
			@cDailyReportCommunities = COALESCE(@cDailyReportCommunities, '') + D.DailyReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DailyReportCommunity'), ELEMENTS) AS DailyReportCommunity
			FROM dbo.DailyReportCommunity T 
			WHERE T.DailyReportID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'DailyReport',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<DailyReportCommunities>' + ISNULL(@cDailyReportCommunities, '') + '</DailyReportCommunities>') AS XML)
			FOR XML RAW('DailyReport'), ELEMENTS
			)
		FROM dbo.DailyReport T
		WHERE T.DailyReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDailyReportAction

--Begin procedure eventlog.LogDocumentAction
EXEC utility.DropObject 'eventlog.LogDocumentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDocumentAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Document',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocumentProvinces VARCHAR(MAX) 
	
		SELECT 
			@cDocumentProvinces = COALESCE(@cDocumentProvinces, '') + D.DocumentProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DocumentProvince'), ELEMENTS) AS DocumentProvince
			FROM dbo.DocumentEntity T 
			WHERE T.DocumentID = @EntityID
				AND T.EntityTypeCode = 'Province'
			) D

		DECLARE @cDocumentCommunities VARCHAR(MAX) 
	
		SELECT 
			@cDocumentCommunities = COALESCE(@cDocumentCommunities, '') + D.DocumentCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DocumentCommunity'), ELEMENTS) AS DocumentCommunity
			FROM dbo.DocumentEntity T 
			WHERE T.DocumentID = @EntityID
				AND T.EntityTypeCode = 'Community'
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			(
			SELECT T.*,
			CAST(('<DocumentProvinces>' + ISNULL(@cDocumentProvinces, '') + '</DocumentProvinces>') AS XML),
			CAST(('<DocumentCommunities>' + ISNULL(@cDocumentCommunities, '') + '</DocumentCommunities>') AS XML)
			FOR XML RAW('Document'), ELEMENTS
			)
		FROM dbo.Document T
		WHERE T.DocumentID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDocumentAction

--Begin procedure eventlog.LogEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogEquipmentCatalogAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentCatalogAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('EquipmentCatalog'), ELEMENTS
			)
		FROM procurement.EquipmentCatalog T
		WHERE T.EquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentCatalogAction

--Begin procedure eventlog.LogFocusGroupSurveyAction
EXEC utility.DropObject 'eventlog.LogFocusGroupSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFocusGroupSurveyAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'FocusGroupSurvey',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('CommunityMemberSurvey', @EntityID)
		DECLARE @cFocusGroupSurveyParticipants VARCHAR(MAX) 
	
		SELECT 
			@cFocusGroupSurveyParticipants = COALESCE(@cFocusGroupSurveyParticipants, '') + D.FocusGroupSurveyParticipant 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FocusGroupSurveyParticipant'), ELEMENTS) AS FocusGroupSurveyParticipant
			FROM dbo.FocusGroupSurveyParticipant T 
			WHERE T.FocusGroupSurveyID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'FocusGroupSurvey',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<FocusGroupSurveyParticipants>' + ISNULL(@cFocusGroupSurveyParticipants, '') + '</FocusGroupSurveyParticipants>') AS XML),
			CAST(@cDocuments AS XML)
			FOR XML RAW('FocusGroupSurvey'), ELEMENTS
			)
		FROM dbo.FocusGroupSurvey T
		WHERE T.FocusGroupSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFocusGroupSurveyAction

--Begin procedure eventlog.LogKeyEventAction
EXEC utility.DropObject 'eventlog.LogKeyEventAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogKeyEventAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'KeyEvent',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cKeyEventProvinces VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventProvinces = COALESCE(@cKeyEventProvinces, '') + D.KeyEventProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventProvince'), ELEMENTS) AS KeyEventProvince
			FROM dbo.KeyEventProvince T 
			WHERE T.KeyEventID = @EntityID
			) D

		DECLARE @cKeyEventCommunities VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventCommunities = COALESCE(@cKeyEventCommunities, '') + D.KeyEventCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventCommunity'), ELEMENTS) AS KeyEventCommunity
			FROM dbo.KeyEventCommunity T 
			WHERE T.KeyEventID = @EntityID
			) D

		DECLARE @cKeyEventEngagementCriteriaResults VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventEngagementCriteriaResults = COALESCE(@cKeyEventEngagementCriteriaResults, '') + D.KeyEventEngagementCriteriaResult 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventEngagementCriteriaResult'), ELEMENTS) AS KeyEventEngagementCriteriaResult
			FROM dbo.KeyEventEngagementCriteriaResult T 
			WHERE T.KeyEventID = @EntityID
			) D

		DECLARE @cKeyEventSources VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventSources = COALESCE(@cKeyEventSources, '') + D.KeyEventSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventSource'), ELEMENTS) AS KeyEventSource
			FROM dbo.KeyEventSource T 
			WHERE T.KeyEventID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'KeyEvent',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<KeyEventProvinces>' + ISNULL(@cKeyEventProvinces, '') + '</KeyEventProvinces>') AS XML),
			CAST(('<KeyEventCommunities>' + ISNULL(@cKeyEventCommunities, '') + '</KeyEventCommunities>') AS XML),
			CAST(('<KeyEventEngagementCriteriaResults>' + ISNULL(@cKeyEventEngagementCriteriaResults, '') + '</KeyEventEngagementCriteriaResults>') AS XML),
			CAST(('<KeyEventSources>' + ISNULL(@cKeyEventSources, '') + '</KeyEventSources>') AS XML)
			FOR XML RAW('KeyEvent'), ELEMENTS
			)
		FROM dbo.KeyEvent T
		WHERE T.KeyEventID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogKeyEventAction

--Begin procedure eventlog.LogKeyInformantSurveyAction
EXEC utility.DropObject 'eventlog.LogKeyInformantSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogKeyInformantSurveyAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'KeyInformantSurvey',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('KeyInformantSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'KeyInformantSurvey',
			@EntityID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('KeyInformantSurvey'), ELEMENTS
			)
		FROM dbo.KeyInformantSurvey T
		WHERE T.KeyInformantSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogKeyInformantSurveyAction

--Begin procedure eventlog.LogLicenseAction
EXEC utility.DropObject 'eventlog.LogLicenseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLicenseAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'License',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('License', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'License',
			@EntityID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('License'), ELEMENTS
			)
		FROM procurement.License T
		WHERE T.LicenseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLicenseAction

--Begin procedure eventlog.LogLicenseEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogLicenseEquipmentCatalogAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLicenseEquipmentCatalogAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'LicenseEquipmentCatalog',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'LicenseEquipmentCatalog',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('LicenseEquipmentCatalog'), ELEMENTS
			)
		FROM procurement.LicenseEquipmentCatalog T
		WHERE T.LicenseEquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLicenseEquipmentCatalogAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'ResetPassword'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Person',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cPersonMenuItems VARCHAR(MAX) 
	
		SELECT 
			@cPersonMenuItems = COALESCE(@cPersonMenuItems, '') + D.PersonmenuItem 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonmenuItem'), ELEMENTS) AS PersonmenuItem
			FROM dbo.PersonmenuItem T 
			WHERE T.PersonID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<PersonMenuItems>' + ISNULL(@cPersonMenuItems, '') + '</PersonMenuItems>') AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM dbo.Person T
		WHERE T.PersonID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogProvinceAction
EXEC utility.DropObject 'eventlog.LogProvinceAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProvinceAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Province',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Province',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('Province'), ELEMENTS
			)
		FROM dbo.Province T
		WHERE T.ProvinceID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProvinceAction

--Begin procedure eventlog.LogRapidPerceptionSurveyAction
EXEC utility.DropObject 'eventlog.LogRapidPerceptionSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRapidPerceptionSurveyAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'RapidPerceptionSurvey',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('RapidPerceptionSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'RapidPerceptionSurvey',
			@EntityID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('RapidPerceptionSurvey'), ELEMENTS
			)
		FROM dbo.RapidPerceptionSurvey T
		WHERE T.RapidPerceptionSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRapidPerceptionSurveyAction

--Begin procedure eventlog.LogStakeholderGroupSurveyAction
EXEC utility.DropObject 'eventlog.LogStakeholderGroupSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogStakeholderGroupSurveyAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'StakeholderGroupSurvey',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('StakeholderGroupSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'StakeholderGroupSurvey',
			@EntityID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('StakeholderGroupSurvey'), ELEMENTS
			)
		FROM dbo.StakeholderGroupSurvey T
		WHERE T.StakeholderGroupSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogStakeholderGroupSurveyAction

--Begin procedure eventlog.LogStationCommanderSurveyAction
EXEC utility.DropObject 'eventlog.LogStationCommanderSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogStationCommanderSurveyAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'StationCommanderSurvey',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('StationCommanderSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'StationCommanderSurvey',
			@EntityID,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('StationCommanderSurvey'), ELEMENTS
			)
		FROM dbo.StationCommanderSurvey T
		WHERE T.StationCommanderSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogStationCommanderSurveyAction

--Begin procedure eventlog.LogTeamAction
EXEC utility.DropObject 'eventlog.LogTeamAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTeamAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Team',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cTeamMembers VARCHAR(MAX) 
	
		SELECT 
			@cTeamMembers = COALESCE(@cTeamMembers, '') + D.TeamMember 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TeamMember'), ELEMENTS) AS TeamMember
			FROM dbo.TeamMember T 
			WHERE T.TeamID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Team',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<TeamMembers>' + ISNULL(@cTeamMembers, '') + '</TeamMembers>') AS XML)
			FOR XML RAW('Team'), ELEMENTS
			)
		FROM dbo.Team T
		WHERE T.TeamID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTeamAction

--Begin procedure eventlog.LogWeeklyReportAction
EXEC utility.DropObject 'eventlog.LogWeeklyReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWeeklyReportAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'WeeklyReport',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cWeeklyReportCommunities VARCHAR(MAX) 
		DECLARE @cWeeklyReportProvinces VARCHAR(MAX) 
	
		SELECT 
			@cWeeklyReportCommunities = COALESCE(@cWeeklyReportCommunities, '') + D.WeeklyReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WeeklyReportCommunity'), ELEMENTS) AS WeeklyReportCommunity
			FROM weeklyreport.Community T 
			WHERE T.WeeklyReportID = @EntityID
			) D
	
		SELECT 
			@cWeeklyReportProvinces = COALESCE(@cWeeklyReportProvinces, '') + D.WeeklyReportProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WeeklyReportProvince'), ELEMENTS) AS WeeklyReportProvince
			FROM weeklyreport.Province T 
			WHERE T.WeeklyReportID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'WeeklyReport',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<WeeklyReportProvinces>' + ISNULL(@cWeeklyReportProvinces, '') + '</WeeklyReportProvinces>') AS XML),
			CAST(('<WeeklyReportCommunities>' + ISNULL(@cWeeklyReportCommunities, '') + '</WeeklyReportCommunities>') AS XML)
			FOR XML RAW('WeeklyReport'), ELEMENTS
			)
		FROM weeklyreport.WeeklyReport T
		WHERE T.WeeklyReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWeeklyReportAction
--End Procedures