USE AJACSUtility
GO
--Begin Tables
--Begin table dbo.AJACSFileType (old table name: none)
IF NOT EXISTS (SELECT 1 FROM sys.tables T WHERE T.Name = 'AJACSFileType')
	BEGIN

	CREATE TABLE dbo.AJACSFileType
		(
		AJACSFileTypeID INT IDENTITY(1,1) NOT NULL,
		Extension VARCHAR(10),
		MimeType VARCHAR(250)
		CONSTRAINT PK_AJACSFileType PRIMARY KEY CLUSTERED 
			(
			AJACSFileTypeID ASC
			)
		)

	END
--ENDIF
GO

TRUNCATE TABLE dbo.AJACSFileType
GO

INSERT dbo.AJACSFileType 
	(Extension, MimeType) 
VALUES 
	('7z','application/x-7z-compressed'),
	('avi','video/avi'),
	('bmp','image/x-ms-bmp'),
	('csv','text/comma-separated-values'),
	('doc','application/msword'),
	('docm','application/vnd.ms-word.document.macroEnabled.12'),
	('docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	('dotm','application/vnd.ms-word.template.macroEnabled.12'),
	('flv','video/x-flv'),
	('fodg','application/octet-stream'),
	('fodp','application/octet-stream'),
	('fods','application/octet-stream'),
	('fodt','application/octet-stream'),
	('gif','image/gif'),
	('jpg','image/jpeg'),
	('mam','application/octet-stream'),
	('mht','message/rfc822'),
	('mkv','video/x-matroska'),
	('mov','video/quicktime'),
	('mp3','audio/mpeg3'),
	('mp4','video/mp4'),
	('mpeg','video/mpeg'),
	('mpg','video/mpeg'),
	('mpp','application/vnd.ms-project'),
	('msg','application/vnd.ms-outlook'),
	('odg','application/vnd.oasis.opendocument.graphics'),
	('odp','application/vnd.oasis.opendocument.presentatio'),
	('ods','application/vnd.oasis.opendocument.spreadsheet'),
	('odt','application/vnd.oasis.opendocument.text'),
	('ogg','audio/ogg'),
	('pdf','application/pdf'),
	('png','image/png'),
	('potm','application/vnd.ms-powerpoint.template.macroEnabled.12'),
	('ppam','application/vnd.ms-powerpoint.addin.macroEnabled.12'),
	('ppsm','application/vnd.ms-powerpoint.slideshow.macroEnabled.12'),
	('ppsx','application/vnd.openxmlformats-officedocument.presentationml.slideshow'),
	('ppt','application/vnd.ms-powerpoint'),
	('pptm','application/vnd.ms-powerpoint.presentation.macroEnabled.12'),
	('pptx','application/vnd.openxmlformats-officedocument.presentationml.presentatio'),
	('rtf','application/rtf'),
	('sldm','application/vnd.ms-powerpoint.slide.macroEnabled.12'),
	('tiff','image/tiff'),
	('tsv','text/tab-separated-values'),
	('txt','text/plai'),
	('wav','audio/wave'),
	('wma','audio/x-ms-wma'),
	('wmv','video/x-ms-wmv'),
	('wps','application/vnd.ms-works'),
	('xfdl','application/vnd.xfdl'),
	('xlam','application/vnd.ms-excel.addin.macroEnabled.12'),
	('xlm','application/excel'),
	('xls','application/vnd.ms-excel'),
	('xlsm','application/vnd.ms-excel.sheet.macroEnabled.12'),
	('xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
	('xltm','application/vnd.ms-excel.template.macroEnabled.12'),
	('xml','application/xml'),
	('xps','application/vnd.ms-xpsdocument'),
	('zip','application/zip')
GO
--End table dbo.AJACSFileType

--Begin table dbo.ServerSetup (old table name: none)
IF NOT EXISTS (SELECT 1 FROM sys.tables T WHERE T.Name = 'ServerSetup')
	BEGIN

	CREATE TABLE dbo.ServerSetup
		(
		ServerSetupID INT IDENTITY(1,1) NOT NULL,
		ServerSetupKey VARCHAR(250) NULL,
		ServerSetupValue VARCHAR(max) NULL,
		CreateDate DATETIME NOT NULL CONSTRAINT DF_ServerSetup_CreateDate DEFAULT getdate(),
		CONSTRAINT PK_ServerSetup PRIMARY KEY CLUSTERED 
			(
			ServerSetupID ASC
			)
		)

	END
--ENDIF
GO

TRUNCATE TABLE dbo.ServerSetup
GO

INSERT dbo.ServerSetup 
	(ServerSetupKey, ServerSetupValue) 
VALUES 
	('DatabaseServerDomainName', 'gamma.pridedata.com'),
	('DatabaseServerIPV4Address', '10.10.1.102'),
	('ConceptNoteBackgroundText', 'The Access to Justice and Community Security (AJACS) project aims to enable people in opposition-controlled Syria to experience increasingly effective accountable and transparent security and justice services, which are delivered on a progressively formalised basis by civilian-led institutions; in increasingly resilient partnership with communities. In order to do this, the programme will work with both communities and formal/informal S&J service providers (in particular the Free Syrian Police or FSP) to develop their capacity to design and deliver services that respond to the needs of the population.  The programme seeks to promote local ownership and leadership of the decision-making processes which guide the delivery of resources, including those supplied by AJACS.'),
	('InvalidLoginLimit', '3'),
	('NetworkName', 'Development'),
	('NoReply', 'noreply@ajacs.oceandisc.com'),
	('PasswordDuration', '90'),
	('SiteURL', 'http://JLLIS.com'),
	('SolrServerIPV4Address', 'localhost'),
	('SSRSDomain', 'gamma.pridedata.com'),
	('SSRSPassword', 'Sch00n3rs'),
	('SSRSReportServerPath', 'ReportServer'),
	('SSRSUserName', 'ssrsuser')
GO
--End table dbo.ServerSetup
--End Tables