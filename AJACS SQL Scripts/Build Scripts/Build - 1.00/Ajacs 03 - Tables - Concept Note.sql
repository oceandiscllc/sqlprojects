USE AJACS
GO

--Begin table dbo.ConceptNote (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNote
	(
	ConceptNoteID INT IDENTITY(1,1) NOT NULL,
	Title VARCHAR(250),
	SubmissionDate DATE,
	ConceptNoteGroupID INT,
	ConceptNoteTypeID INT,
	PointOfContactPersonID1 INT,
	PointOfContactPersonID2 INT,
	Background VARCHAR(MAX),
	Objectives VARCHAR(MAX),
	SoleSourceJustification VARCHAR(MAX),
	BeneficiaryDetails VARCHAR(MAX),
	MonitoringEvaluation VARCHAR(MAX),
	ActivityCode VARCHAR(50),
	OtherDeliverable VARCHAR(MAX),
	FundedBy VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PointOfContactPersonID1', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PointOfContactPersonID2', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteID'
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteActivity (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteActivity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteActivity
	(
	ConceptNoteActivityID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	ActivityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteActivityID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteActivity', @TableName, 'ConceptNoteID,ActivityID'
GO
--End table dbo.ConceptNoteActivity

--Begin table dbo.ConceptNoteBudget (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteBudget'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteBudget
	(
	ConceptNoteBudgetID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	BudgetTypeID INT,
	Cost NUMERIC(18,2),
	Notes VARCHAR(500)
	)

EXEC utility.SetDefaultConstraint @TableName, 'BudgetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteBudgetID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteBudget', @TableName, 'ConceptNoteID,ConceptNoteBudgetID'
GO
--End table dbo.ConceptNoteBudget

--conceptnotecourse
--	CourseName VARCHAR(200),
--	Curriculum VARCHAR(MAX),
--	CourseTypeID INT,
--	LearnerProfileTypeID INT,
--	ProgramTypeID INT,
--	SponsorName VARCHAR(250),
--	Syllabus VARCHAR(MAX),

--also
--conceptnotecommunity need ActionType - training or activity
--conceptnoteprovince
--conceptnotetask
--	name, start, end, notes


--Begin table dbo.ConceptNoteEquipmentCatalog (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteEquipmentCatalog'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteEquipmentCatalog
	(
	ConceptNoteEquipmentCatalogID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	EquipmentCatalogID INT,
	Quantity INT	
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentCatalogID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteEquipmentCatalogID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteEquipmentCatalog', @TableName, 'ConceptNoteID,EquipmentCatalogID'
GO
--End table dbo.ConceptNoteEquipmentCatalog