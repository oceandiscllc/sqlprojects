USE AJACS
GO

--Begin table dbo.Atmospheric (old table name:  general_assessment)
DECLARE @TableName VARCHAR(250) = 'dbo.Atmospheric'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Atmospheric
	(
	AtmosphericID INT IDENTITY(1,1) NOT NULL,
	AtmosphericTypeID INT,
	AtmosphericDate DATE,
	AtmosphericReportedDate DATE,
	ConfidenceLevelID INT,
	Information NVARCHAR(MAX),
	Recommendation NVARCHAR(MAX),
	IsCritical BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AtmosphericTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConfidenceLevelID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsCritical', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AtmosphericReportedDate', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'AtmosphericID'
GO

SET IDENTITY_INSERT dbo.Atmospheric ON
GO

INSERT INTO dbo.Atmospheric
	(AtmosphericID, AtmosphericTypeID, AtmosphericDate, AtmosphericReportedDate, ConfidenceLevelID, Information, Recommendation, IsCritical)
SELECT
	A.general_assessment_id, 
	ISNULL((SELECT T.AtmosphericTypeID FROM dropdown.AtmosphericType T WHERE T.AtmosphericTypeName = A.general_assessment_type), 0),
	A.reported_date, 
	A.entry_date, 
	ISNULL((SELECT T.ConfidenceLevelID FROM dropdown.ConfidenceLevel T WHERE T.ConfidenceLevelName = A.report_confidence), 0),
	A.narative_information, 
	A.recommendation, 

	CASE 
		WHEN A.critical = 'YES' 
		THEN 1 
		ELSE 0 
	END

FROM amnuna_db.dbo.general_assessment A
GO

SET IDENTITY_INSERT dbo.Atmospheric OFF
GO
--End table dbo.Atmospheric

--Begin table dbo.AtmosphericCommunity (old table name:  general_assessment_community)
DECLARE @TableName VARCHAR(250) = 'dbo.AtmosphericCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.AtmosphericCommunity
	(
	AtmosphericCommunityID INT IDENTITY(1,1) NOT NULL,
	AtmosphericID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AtmosphericID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AtmosphericCommunityID'
EXEC utility.SetIndexClustered 'IX_AtmosphericCommunity', @TableName, 'AtmosphericID,CommunityID'
GO

INSERT INTO dbo.AtmosphericCommunity
	(AtmosphericID,CommunityID)
SELECT
	KE.AtmosphericID,
	CA.Community_ID
FROM amnuna_db.dbo.general_assessment_community CA
	JOIN dbo.Atmospheric KE ON KE.AtmosphericID = CA.general_assessment_id
GO
--End table dbo.AtmosphericCommunity

--Begin table dbo.AtmosphericEngagementCriteriaResult (old table name:  general_assessment_criteria_engagement)
DECLARE @TableName VARCHAR(250) = 'dbo.AtmosphericEngagementCriteriaResult'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.AtmosphericEngagementCriteriaResult
	(
	AtmosphericEngagementCriteriaResultID INT IDENTITY(1,1) NOT NULL,
	AtmosphericID INT,
	EngagementCriteriaID INT,
	EngagementCriteriaChoiceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AtmosphericID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EngagementCriteriaID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EngagementCriteriaChoiceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AtmosphericEngagementCriteriaResultID'
EXEC utility.SetIndexClustered 'IX_AtmosphericEngagementCriteriaResult', @TableName, 'AtmosphericID,EngagementCriteriaID,EngagementCriteriaChoiceID'
GO

INSERT INTO dbo.AtmosphericEngagementCriteriaResult
	(AtmosphericID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.AtmosphericID,
	ISNULL((SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Security Requirements'), 0),
	ISNULL((SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.Security), 0)
FROM amnuna_db.dbo.general_assessment_criteria_engagement ACE
	JOIN dbo.Atmospheric KE ON KE.AtmosphericID = ACE.general_assessment_id
GO

INSERT INTO dbo.AtmosphericEngagementCriteriaResult
	(AtmosphericID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.AtmosphericID,
	(SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Donor Restrictions'),
	(SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.DonorRedLines)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.Atmospheric KE ON KE.AtmosphericID = ACE.atmospheric_id
GO

INSERT INTO dbo.AtmosphericEngagementCriteriaResult
	(AtmosphericID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.AtmosphericID,
	(SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Political/Power Dynamics'),
	(SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.PoliticalPowerDynamics)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.Atmospheric KE ON KE.AtmosphericID = ACE.atmospheric_id
GO

INSERT INTO dbo.AtmosphericEngagementCriteriaResult
	(AtmosphericID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.AtmosphericID,
	(SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'Stakeholder Groups'),
	(SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.CommunityGroups)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.Atmospheric KE ON KE.AtmosphericID = ACE.atmospheric_id
GO

INSERT INTO dbo.AtmosphericEngagementCriteriaResult
	(AtmosphericID,EngagementCriteriaID,EngagementCriteriaChoiceID)
SELECT
	KE.AtmosphericID,
	(SELECT T.EngagementCriteriaID FROM dropdown.EngagementCriteria T WHERE T.EngagementCriteriaName = 'FSP Operational Presence'),
	(SELECT T.EngagementCriteriaChoiceID FROM dropdown.EngagementCriteriaChoice T WHERE T.EngagementCriteriaChoiceName = ACE.FSPOperational)
FROM amnuna_db.dbo.atmospheric_criteria_engagement ACE
	JOIN dbo.Atmospheric KE ON KE.AtmosphericID = ACE.atmospheric_id
GO
--End table dbo.AtmosphericEngagementCriteriaResult

--Begin table dbo.AtmosphericSource (old table name:  general_assessment_sources)
DECLARE @TableName VARCHAR(250) = 'dbo.AtmosphericSource'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.AtmosphericSource
	(
	AtmosphericSourceID INT IDENTITY(1,1) NOT NULL,
	AtmosphericID INT,
	SourceName NVARCHAR(250),
	SourceAttribution NVARCHAR(MAX),
	SourceSummary NVARCHAR(MAX),
	SourceDate DATE,
	SourceTypeID INT,
	SourceCategoryID INT,
	ConfidenceLevelID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConfidenceLevelID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AtmosphericID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SourceCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SourceTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AtmosphericSourceID'
EXEC utility.SetIndexClustered 'IX_AtmosphericSource', @TableName, 'AtmosphericID,AtmosphericSourceID'
GO

INSERT INTO dbo.AtmosphericSource
	(AtmosphericID, SourceName, SourceAttribution, SourceSummary, SourceDate, SourceTypeID, SourceCategoryID, ConfidenceLevelID)
SELECT
	S.general_assessment_id,
	'Source',
	S.Atribution,
	S.Source_Description,
	S.Source_Date,
	ISNULL((SELECT T.SourceTypeID FROM dropdown.SourceType T WHERE T.SourceTypeName = S.Type), 0),
	ISNULL((SELECT T.SourceCategoryID FROM dropdown.SourceCategory T WHERE T.SourceCategoryName = S.Location), 0),
	ISNULL((SELECT T.ConfidenceLevelID FROM dropdown.ConfidenceLevel T WHERE T.ConfidenceLevelName = S.Confidence), 0)
FROM amnuna_db.dbo.general_assessment_sources S
	JOIN dbo.Atmospheric KE ON KE.AtmosphericID = S.general_assessment_id
GO
--End table dbo.AtmosphericSource
