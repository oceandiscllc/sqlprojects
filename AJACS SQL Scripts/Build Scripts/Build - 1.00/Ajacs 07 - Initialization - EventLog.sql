USE AJACS
GO

--Begin dbo.Community eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.Community_ID,
		C.CreateUserID
	FROM amnuna_db.dbo.Community C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogCommunityAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.Community T ON T.Community_ID = EL.EntityID
		AND EL.EntityTypeCode = 'Community'
		AND EL.EventCode = 'create'
GO
--End dbo.Community eventlog updates

--Begin dbo.CommunityMemberSurvey eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.CommunityMemberSurveyID,
		C.CreateUserID
	FROM amnuna_db.dbo.CommunityMemberSurvey C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogCommunityMemberSurveyAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.CommunityMemberSurvey T ON T.CommunityMemberSurveyID = EL.EntityID
		AND EL.EntityTypeCode = 'CommunityMemberSurvey'
		AND EL.EventCode = 'create'
GO
--End dbo.CommunityMemberSurvey event logupdates

--Begin dbo.Document eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.DocumentID,
		C.CreateUserID
	FROM amnuna_db.dbo.Document C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogDocumentAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.Document T ON T.DocumentID = EL.EntityID
		AND EL.EntityTypeCode = 'Document'
		AND EL.EventCode = 'create'
GO
--End dbo.Document event logupdates

--Begin dbo.FocusGroupSurvey eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.FocusGroupSurveyID,
		C.CreateUserID
	FROM amnuna_db.dbo.FocusGroupSurvey C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogFocusGroupSurveyAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.FocusGroupSurvey T ON T.FocusGroupSurveyID = EL.EntityID
		AND EL.EntityTypeCode = 'FocusGroupSurvey'
		AND EL.EventCode = 'create'
GO
--End dbo.FocusGroupSurvey event logupdates

--Begin dbo.KeyInformantSurvey eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.InformantSurveyID,
		C.CreateUserID
	FROM amnuna_db.dbo.InformantSurvey C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogKeyInformantSurveyAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.InformantSurvey T ON T.InformantSurveyID = EL.EntityID
		AND EL.EntityTypeCode = 'KeyInformantSurvey'
		AND EL.EventCode = 'create'
GO
--End dbo.KeyInformantSurvey event logupdates

--Begin dbo.RapidPerceptionSurvey eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.PerceptionSurveyID,
		C.CreateUserID
	FROM amnuna_db.dbo.PerceptionSurvey C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogRapidPerceptionSurveyAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.PerceptionSurvey T ON T.PerceptionSurveyID = EL.EntityID
		AND EL.EntityTypeCode = 'RapidPerceptionSurvey'
		AND EL.EventCode = 'create'
GO
--End dbo.RapidPerceptionSurvey event logupdates

--Begin dbo.StakeholderGroupSurvey eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.StakeholderSurveyID,
		C.CreateUserID
	FROM amnuna_db.dbo.StakeholderSurvey C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogStakeholderGroupSurveyAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.StakeholderSurvey T ON T.StakeholderSurveyID = EL.EntityID
		AND EL.EntityTypeCode = 'StakeholderGroupSurvey'
		AND EL.EventCode = 'create'
GO
--End dbo.StakeholderGroupSurvey event logupdates

--Begin dbo.StationCommanderSurvey eventlog updates
DECLARE @nCreatePersonID INT
DECLARE @nEntityID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
	SELECT 
		C.StationCommanderSurveyID,
		C.CreateUserID
	FROM amnuna_db.dbo.StationCommanderSurvey C
	
OPEN oCursor
FETCH oCursor INTO @nEntityID, @nCreatePersonID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogStationCommanderSurveyAction @nEntityID, 'create', @nCreatePersonID
	FETCH oCursor INTO @nEntityID, @nCreatePersonID
	
	END
--END WHILE
	
CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE EL
SET EL.CreateDateTime = T.CreateDateTime
FROM eventlog.EventLog EL
	JOIN amnuna_db.dbo.StationCommanderSurvey T ON T.StationCommanderSurveyID = EL.EntityID
		AND EL.EntityTypeCode = 'StationCommanderSurvey'
		AND EL.EventCode = 'create'
GO
--End dbo.StationCommanderSurvey event logupdates