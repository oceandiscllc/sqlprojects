USE AJACS
GO

--Begin Tables
--Begin table weeklyreport.Community (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'weeklyreport.Community'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.Community
	(
	CommunityID INT,
	CommunityName VARCHAR(250),
	ProvinceID INT,
	WeeklyReportID INT,
	CommunityEngagementStatusID INT,
	ImpactDecisionID INT, 
	StatusChangeID INT, 
	Summary NVARCHAR(MAX),
	KeyPoints NVARCHAR(MAX),
	Implications NVARCHAR(MAX), 
	RiskMitigation NVARCHAR(MAX),
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ImpactDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StatusChangeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table weeklyreport.Community

--Begin table weeklyreport.Province (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'weeklyreport.Province'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.Province
	(
	ProvinceID INT,
	ProvinceName VARCHAR(250),
	WeeklyReportID INT,
	ImpactDecisionID INT, 
	StatusChangeID INT, 
	Summary NVARCHAR(MAX),
	Implications NVARCHAR(MAX), 
	RiskMitigation NVARCHAR(MAX), 
	KeyPoints NVARCHAR(MAX),
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'ImpactDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StatusChangeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table weeklyreport.Province

--Begin table weeklyreport.WeeklyReport (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'weeklyreport.WeeklyReport'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.WeeklyReport
	(
	WeeklyReportID INT IDENTITY(1,1),
	WeeklyReportStatusCode VARCHAR(50)
	)
	
EXEC utility.SetPrimaryKeyClustered @TableName, 'WeeklyReportID'
GO
--End table weeklyreport.WeeklyReport
--End Tables

--Begin procedures
--Begin procedure weeklyreport.DeleteWeeklyReportCommunity
EXEC Utility.DropObject 'weeklyreport.DeleteWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to remove a community from the weekly report
-- ============================================================================
CREATE PROCEDURE weeklyreport.DeleteWeeklyReportCommunity

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE C
	FROM weeklyreport.Community C
	WHERE C.CommunityID = @CommunityID

END
GO
--End procedure weeklyreport.DeleteWeeklyReportCommunity

--Begin procedure weeklyreport.DeleteWeeklyReportProvince
EXEC Utility.DropObject 'weeklyreport.DeleteWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to remove a Province from the weekly report
-- ===========================================================================
CREATE PROCEDURE weeklyreport.DeleteWeeklyReportProvince

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE P
	FROM weeklyreport.Province P
	WHERE P.ProvinceID = @ProvinceID

END
GO
--End procedure weeklyreport.DeleteWeeklyReportProvince

--Begin procedure weeklyreport.GetWeeklyReportCommunity
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get community data for the weekly report
-- ===========================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReportCommunity

@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID, 
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID, 
		C.StatusChangeID, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		CES.CommunityEngagementStatusName,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM dbo.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
			AND C.CommunityID = @CommunityID
	
	SELECT
		C.CommunityID, 
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID, 
		C.StatusChangeID, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		CES.CommunityEngagementStatusName,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
			AND C.CommunityID = @CommunityID

END
GO
--End procedure weeklyreport.GetWeeklyReportCommunity

--Begin procedure weeklyreport.GetWeeklyReportProvince
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get Province data for the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReportProvince

@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceID, 
		P.ImpactDecisionID, 
		P.StatusChangeID, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM dbo.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

	SELECT
		P.ProvinceID, 
		P.ImpactDecisionID, 
		P.StatusChangeID, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

END
GO
--End procedure weeklyreport.GetWeeklyReportProvince

--Begin procedure weeklyreport.PopulateWeeklyReportCommunities
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add community data to the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportCommunities

@CommunityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Community
		(CommunityID, CommunityName, ProvinceID, CommunityEngagementStatusID, ImpactDecisionID, StatusChangeID, Summary, KeyPoints, Implications, RiskMitigation)
	SELECT
		C.CommunityID,
		C.CommunityName,
		C.ProvinceID,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID, 
		C.StatusChangeID, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation
	FROM dbo.Community C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Community WRC
				WHERE WRC.CommunityID = C.CommunityID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportCommunities

--Begin procedure weeklyreport.PopulateWeeklyReportProvinces
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportProvinces'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add Province data to the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportProvinces

@ProvinceIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Province
		(ProvinceID, ProvinceName, ImpactDecisionID, StatusChangeID, Summary, KeyPoints, Implications, RiskMitigation)
	SELECT
		P.ProvinceID,
		P.ProvinceName,
		P.ImpactDecisionID, 
		P.StatusChangeID, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation
	FROM dbo.Province P
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Province WRP
				WHERE WRP.ProvinceID = P.ProvinceID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportProvinces

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		INSERT INTO weeklyreport.WeeklyReport 
			(WeeklyReportStatusCode) 
		VALUES 
			('Draft')
			
		END
	--ENDIF
	
	SELECT
		WR.WeeklyReportID, 
		WR.WeeklyReportStatusCode 
	FROM weeklyreport.WeeklyReport WR

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure weeklyreport.SubmitWeeklyReport
EXEC Utility.DropObject 'weeklyreport.SubmitWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
-- ======================================================================
CREATE PROCEDURE weeklyreport.SubmitWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @WeeklyReportID INT
	DECLARE @tOutput TABLE (EntityTypeCode VARCHAR(50), EntityID INT)

	SELECT @WeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @WeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @WeeklyReportID

	INSERT INTO @tOutput (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @WeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.EntityTypeCode, O.EntityID
		FROM @tOutput O
		ORDER BY O.EntityTypeCode, O.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	DELETE T FROM weeklyreport.Community T WHERE T.WeeklyReportID = @WeeklyReportID
	DELETE T FROM weeklyreport.Province T WHERE T.WeeklyReportID = @WeeklyReportID
	DELETE T FROM weeklyreport.WeeklyReport T WHERE T.WeeklyReportID = @WeeklyReportID

END
GO
--End procedure weeklyreport.SubmitWeeklyReport

--Begin procedure weeklyreport.SubmitWeeklyReportForApproval
EXEC Utility.DropObject 'weeklyreport.SubmitWeeklyReportForApproval'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
-- ======================================================================
CREATE PROCEDURE weeklyreport.SubmitWeeklyReportForApproval

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	DECLARE @tOutput TABLE (WeeklyReportID INT)

	INSERT INTO weeklyreport.WeeklyReport
		(WeeklyReportStatusCode)
	OUTPUT INSERTED.WeeklyReportID INTO @tOutput
	VALUES
		('PendingApproval')

	SELECT @nWeeklyReportID = O.WeeklyReportID 
	FROM @tOutput O

	UPDATE weeklyreport.Community
	SET WeeklyReportID = @nWeeklyReportID

	UPDATE weeklyreport.Province
	SET WeeklyReportID = @nWeeklyReportID
	
	SELECT O.WeeklyReportID 
	FROM @tOutput O

END
GO
--End procedure weeklyreport.SubmitWeeklyReportForApproval
--End procedures