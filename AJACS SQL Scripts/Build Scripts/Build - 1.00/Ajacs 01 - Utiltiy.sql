USE AJACS
GO

--Begin Schemas
--Begin schema dropdown
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'dropdown')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA dropdown'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema dropdown

--Begin schema eventlog
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'eventlog')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA eventlog'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema eventlog

--Begin schema procurement
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'procurement')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA procurement'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema procurement

--Begin schema reporting
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'reporting')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA reporting'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema reporting

--Begin schema utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'utility')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema utility

--Begin schema weeklyreport
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'weeklyreport')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA weeklyreport'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema weeklyreport

--Begin schema workflow
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'workflow')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA workflow'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema workflow
--End Schemas

--Begin dependencies
--Begin procedure utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date: 2012.09.24
-- Description:	A stored procedure to drop a function, stored procedcure or table from the database
--
-- Author:			Todd Pires
-- Modify date: 2013.05.20
-- Description:	Added support for Synonyms, optimized the code
--
-- Author:			Todd Pires
-- Modify date: 2013.08.02
-- Description:	Added support for Schemas & Primary Keys
--
-- Author:			Todd Pires
-- Modify date: 2013.10.21
-- Description:	Added support for Triggers
--
-- Author:			Todd Pires
-- Modify date: 2014.05.07
-- Description:	Added support for Types
-- ================================================================================================
CREATE PROCEDURE utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)
DECLARE @cType VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @cType = O.Type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @cType IS NOT NULL
	BEGIN
	
	IF @cType IN ('D', 'PK')
		BEGIN
		
		SELECT
			@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @cSQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('P','PC')
		BEGIN
		
		SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'SN'
		BEGIN
		
		SET @cSQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'TR'
		BEGIN
		
		SET @cSQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'U'
		BEGIN
		
		SET @cSQL = 'DROP TABLE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'V'
		BEGIN
		
		SET @cSQL = 'DROP VIEW ' + @ObjectName
		EXEC (@cSQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@cSQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP TYPE ' + @ObjectName
	EXEC (@cSQL)

	END
--ENDIF
		
END	
GO
--End procedure utility.DropObject
--End dependencies

--Begin Procedures
--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250),
	@DataType VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.DropColumn
EXEC utility.DropObject 'utility.DropColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2013.08.02
-- Description:	A helper stored procedure for table upgrades
--
-- Author:		Todd Pires
-- Create date: 2014.04.15
-- Description:	Add a check for statistics
-- =========================================================
CREATE PROCEDURE utility.DropColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.foreign_keys	FK JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id JOIN sys.objects O ON O.object_id = FK.parent_object_id JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = FKC.parent_object_id AND C.Column_ID = FKC.parent_Column_ID AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName) 
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name
		FROM sys.foreign_keys FK 
			JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id 
			JOIN sys.objects O ON O.object_id = FK.parent_object_id 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = FKC.parent_object_id 
				AND C.Column_ID = FKC.parent_Column_ID 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			'DROP STATISTICS ' + @TableName + '.' + S1.Name + ''
		FROM sys.stats S1 
			JOIN sys.tables T1 ON T1.Object_ID = S1.Object_ID
			JOIN sys.schemas S2 ON S2.schema_ID = T1.schema_ID 
			JOIN sys.stats_columns SC ON SC.stats_id = S1.stats_id 
				AND T1.Object_ID = SC.Object_ID
			JOIN sys.columns C ON C.column_id = SC.column_id 
				AND T1.Object_ID = C.Object_ID
			JOIN sys.types T2 ON T2.system_type_id = C.system_type_id
				AND S1.user_created = 1
				AND S2.Name + '.' + T1.Name = @TableName 
				AND C.Name = @ColumnName
	
	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @ColumnName
		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.DropColumn

--Begin procedure utility.DropConstraintsAndIndexes
EXEC utility.DropObject 'utility.DropConstraintsAndIndexes'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropConstraintsAndIndexes
	@TableName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name AS SQL
		FROM sys.default_constraints DC
		WHERE DC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + KC.Name + '' AS SQL
		FROM sys.key_constraints KC
		WHERE KC.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name AS SQL
		FROM sys.foreign_keys FK
		WHERE FK.parent_object_ID = OBJECT_ID(@TableName)

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 'DROP INDEX ' + I.Name + ' ON ' + @TableName AS SQL
		FROM sys.indexes I
		WHERE I.object_ID = OBJECT_ID(@TableName) 
			AND I.Is_Primary_Key = 0

	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE

	CLOSE oCursor
	DEALLOCATE oCursor

END
GO
--End procedure utility.DropConstraintsAndIndexes

--Begin procedure utility.DropIndex
EXEC utility.DropObject 'utility.DropIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2013.04.19
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropIndex
	@TableName VARCHAR(250),
	@IndexName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Object_ID = OBJECT_ID(@TableName) AND I.Name = @IndexName)
		BEGIN

		SET @cSQL = 'DROP INDEX ' + @IndexName + ' ON ' + @TableName
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.DropIndex

--Begin procedure utility.SetDefaultConstraint
EXEC utility.DropObject 'utility.SetDefaultConstraint'
GO

-- ===============================================================================================================================
-- Author:			Todd Pires
-- Create date: 2013.10.12
-- Description:	A helper stored procedure for table upgrades
-- ===============================================================================================================================
CREATE PROCEDURE utility.SetDefaultConstraint
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250),
	@DataType VARCHAR(250),
	@Default VARCHAR(MAX),
	@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
		SET @cSQL = 'UPDATE ' + @TableName + ' SET ' + @ColumnName + ' = ''' + @Default + ''' WHERE ' + @ColumnName + ' IS NULL'
	ELSE
		SET @cSQL = 'UPDATE ' + @TableName + ' SET ' + @ColumnName + ' = ' + @Default + ' WHERE ' + @ColumnName + ' IS NULL'
	--ENDIF

	EXEC (@cSQL)

	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN ' + @ColumnName + ' ' + @DataType + ' NOT NULL'
	EXEC (@cSQL)

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + @ColumnName
	
	IF @OverWriteExistingConstraint = 1
		BEGIN	

		SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		IF @cSQL IS NOT NULL
			EXECUTE (@cSQL)
		--ENDIF
		
		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN	

		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR ' + @ColumnName
		ELSE
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR ' + @ColumnName
	--ENDIF
	
		EXECUTE (@cSQL)

		END
	--ENDIF

	END
GO
--End procedure utility.SetDefaultConstraint

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
--
-- Author:		Todd Pires
-- Modify date: 2013.10.10
-- Description:	Added INCLUDE support
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX),
	@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered

--Begin procedure utility.SetPrimaryKeyClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyClustered
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyClustered

--Begin procedure utility.SetPrimaryKeyNonClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyNonClustered'
GO

-- =========================================================
-- Author:		Todd Pires
-- Create date: 2010.12.08
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyNonClustered
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyNonClustered
--End Procedures

--Begin Tables
--Begin table utility.BuildLog  (old table name: none)
DECLARE @TableName VARCHAR(250) = 'utility.BuildLog'

EXEC utility.DropObject @TableName

CREATE TABLE utility.BuildLog
	(
	BuildLogID INT IDENTITY(1,1) NOT NULL,
	BuildKey VARCHAR(100) NULL,
	CreateDateTime DATETIME NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BuildLogID'
GO
--End table utility.BuildLog
--End Tables