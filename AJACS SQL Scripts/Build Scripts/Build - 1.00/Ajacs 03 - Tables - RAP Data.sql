USE AJACS
GO

--Begin table dbo.CommunityMemberSurvey (old table name:  communitymembersurvey)
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityMemberSurvey'

EXEC utility.DropObject @TableName

CREATE TABLE CommunityMemberSurvey 
	(
  CommunityMemberSurveyID INT IDENTITY(1,1) NOT NULL,
  CommunityID INT,
  SubjectAge INT,
  SubjectGender CHAR(1),
  SubjectPosition NVARCHAR(250),
  InterviewDateTime DATETIME,
  InterviewerName1 NVARCHAR(500),
  InterviewerName2 NVARCHAR(500),
  Question01 INT,
  Question02 INT,
  Question03 INT,
  Question04 INT,
  Question05 INT,
  Question06 INT,
  Question07 INT,
  Question08 INT,
  Question09 NVARCHAR(MAX),
  Question10 INT,
  Question11 INT,
  Question12 INT,
  Question13 INT,
  Question14 INT,
  Question15 INT,
  Question16 INT,
  Question17 INT,
  Question18 INT,
  Question19 INT,
  Question20 NVARCHAR(500),
  Question21 INT,
  Question22 INT,
  Question23 INT,
  Question24 INT,
  Question25 INT,
  Question26 INT,
  Question27 NVARCHAR(500),
  Question28 INT,
  Question29 INT,
  Question30 INT,
  Question31 INT,
  Question32 INT,
  Question33 INT,
  Question34 INT,
  Question35 NVARCHAR(MAX),
  Question36 INT,
  Question37 INT,
  Question38 INT,
  Question39 INT,
  Question40 INT,
  Question41 INT,
  Question42 INT,
  Question43 INT,
  Question44 INT,
  Question45 NVARCHAR(500),
  Question46 INT,
  Question47 INT,
  Question48 INT,
  Question49 INT,
  Question50 INT,
  Question51 INT,
  Question52 INT,
  Question53 NVARCHAR(MAX),
  Question54 INT,
  Question55 INT,
  Question56 INT,
  Question57 INT,
  Question58 INT,
  Question59 INT,
  Question60 NVARCHAR(500),
  Question61 INT,
  Question62 INT,
  Question63 NVARCHAR(MAX),
  Question64 INT,
  Question65 NVARCHAR(MAX),
  Question66 NVARCHAR(MAX),
  Question67 INT,
  Question68 NVARCHAR(MAX),
  Question69 INT,
  Question70 NVARCHAR(MAX),
  Question71 NVARCHAR(500),
  ReferenceCode VARCHAR(6)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'Question01', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question02', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question03', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question04', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question05', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question06', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question07', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question08', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question10', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question11', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question12', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question13', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question14', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question15', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question16', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question17', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question18', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question19', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question21', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question22', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question23', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question24', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question25', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question26', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question28', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question29', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question30', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question31', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question32', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question33', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question34', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question36', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question37', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question38', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question39', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question40', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question41', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question42', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question43', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question44', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question46', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question47', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question48', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question49', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question50', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question51', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question52', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question54', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question55', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question56', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question57', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question58', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question59', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question61', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question62', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question64', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question67', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question69', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SubjectAge', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityMemberSurveyID'
GO

SET IDENTITY_INSERT dbo.CommunityMemberSurvey ON
GO

INSERT INTO dbo.CommunityMemberSurvey
	(CommunityMemberSurveyID, CommunityID, SubjectAge, SubjectGender, SubjectPosition, InterviewDateTime, InterviewerName1, InterviewerName2, 
		Question01, Question02, Question03, Question04, Question05, Question06, Question07, Question08, Question09, Question10, Question11, Question12, Question13, Question14, Question15, 
		Question16, Question17, Question18, Question19, Question20, Question21, Question22, Question23, Question24, Question25, Question26, Question27, Question28, Question29, Question30, 
		Question31, Question32, Question33, Question34, Question35, Question36, Question37, Question38, Question39, Question40, Question41, Question42, Question43, Question44, Question45, 
		Question46, Question47, Question48, Question49, Question50, Question51, Question52, Question53, Question54, Question55, Question56, Question57, Question58, Question59, Question60, 
		Question61, Question62, Question63, Question64, Question65, Question66, Question67, Question68, Question69, Question70, Question71, ReferenceCode)
SELECT 
	CMS.CommunityMemberSurveyID,
	CMS.Community_ID,
	CMS.SubjectAge,
	CMS.SubjectGender,
	CMS.SubjectPosition,
	CMS.InterviewDateTime,
	CMS.InterviewerName1,
	CMS.InterviewerName2,
	CMS.Question01,
	CMS.Question02,
	CMS.Question03,
	CMS.Question04,
	CMS.Question05,
	CMS.Question06,
	CMS.Question07,
	CMS.Question08,
	CMS.Question09,
	CMS.Question10,
	CMS.Question11,
	CMS.Question12,
	CMS.Question13,
	CMS.Question14,
	CMS.Question15,
	CMS.Question16,
	CMS.Question17,
	CMS.Question18,
	CMS.Question19,
	CMS.Question20,
	CMS.Question21,
	CMS.Question22,
	CMS.Question23,
	CMS.Question24,
	CMS.Question25,
	CMS.Question26,
	CMS.Question27,
	CMS.Question28,
	CMS.Question29,
	CMS.Question30,
	CMS.Question31,
	CMS.Question32,
	CMS.Question33,
	CMS.Question34,
	CMS.Question35,
	CMS.Question36,
	CMS.Question37,
	CMS.Question38,
	CMS.Question39,
	CMS.Question40,
	CMS.Question41,
	CMS.Question42,
	CMS.Question43,
	CMS.Question44,
	CMS.Question45,
	CMS.Question46,
	CMS.Question47,
	CMS.Question48,
	CMS.Question49,
	CMS.Question50,
	CMS.Question51,
	CMS.Question52,
	CMS.Question53,
	CMS.Question54,
	CMS.Question55,
	CMS.Question56,
	CMS.Question57,
	CMS.Question58,
	CMS.Question59,
	CMS.Question60,
	CMS.Question61,
	CMS.Question62,
	CMS.Question63,
	CMS.Question64,
	CMS.Question65,
	CMS.Question66,
	CMS.Question67,
	CMS.Question68,
	ISNULL(CMS.Question69, 0),
	CMS.Question70,
	CMS.Question71,
	CMS.ReferenceCode
FROM amnuna_db.dbo.communitymembersurvey CMS
GO

SET IDENTITY_INSERT dbo.CommunityMemberSurvey OFF
GO
--End table dbo.CommunityMemberSurvey

--Begin table dbo.FocusGroupSurvey (old table name:  focusgroupsurvey)
DECLARE @TableName VARCHAR(250) = 'dbo.FocusGroupSurvey'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.FocusGroupSurvey
	(
	FocusGroupSurveyID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	InterviewDateTime DATETIME,
	InterviewerName1 NVARCHAR(500),
	InterviewerName2 NVARCHAR(500),
	Notes NVARCHAR(MAX),
	ReferenceCode VARCHAR(6)
	) 

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0	

EXEC utility.SetPrimaryKeyClustered @TableName, 'FocusGroupSurveyID'

SET IDENTITY_INSERT dbo.FocusGroupSurvey ON
GO

INSERT INTO dbo.FocusGroupSurvey
	(FocusGroupSurveyID, CommunityID, InterviewDateTime, InterviewerName1, InterviewerName2, Notes, ReferenceCode)
SELECT
	SCS.FocusGroupSurveyID, 
	SCS.Community_ID, 
	SCS.InterviewDateTime, 
	SCS.InterviewerName1, 
	SCS.InterviewerName2, 
	SCS.Notes,
	SCS.ReferenceCode
FROM amnuna_db.dbo.focusgroupsurvey SCS
GO

SET IDENTITY_INSERT dbo.FocusGroupSurvey OFF
GO
--End table dbo.FocusGroupSurvey

--Begin table dbo.FocusGroupSurveyParticipant (old table name:  focusgroupsurveyparticipant)
DECLARE @TableName VARCHAR(250) = 'dbo.FocusGroupSurveyParticipant'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.FocusGroupSurveyParticipant
	(
  FocusGroupSurveyParticipantID INT IDENTITY(1,1) NOT NULL,
	FocusGroupSurveyID INT,
	SubjectAge INT,
	SubjectGender CHAR(1),
	SubjectPosition NVARCHAR(500) DEFAULT NULL,
  SubjectNotes NVARCHAR(MAX)
	) 

EXEC utility.SetDefaultConstraint @TableName, 'FocusGroupSurveyID', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'SubjectAge', 'INT', 0	

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FocusGroupSurveyParticipantID'
EXEC utility.SetIndexClustered 'IX_FocusGroupSurveyParticipant', @TableName, 'FocusGroupSurveyID,FocusGroupSurveyParticipantID'

INSERT INTO dbo.FocusGroupSurveyParticipant
	(FocusGroupSurveyID, SubjectAge, SubjectGender, SubjectPosition, SubjectNotes)
SELECT
	SCS.FocusGroupSurveyID, 
	SCS.SubjectAge, 
	SCS.SubjectGender, 
	SCS.SubjectPosition, 
	SCS.SubjectNotes
FROM amnuna_db.dbo.focusgroupsurveyparticipant SCS
GO
--End table dbo.FocusGroupSurveyParticipant

--Begin table dbo.KeyInformantSurvey (old table name:  informantsurvey)
DECLARE @TableName VARCHAR(250) = 'dbo.KeyInformantSurvey'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.KeyInformantSurvey
	(
  KeyInformantSurveyID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
  SubjectName NVARCHAR(500),
	SubjectGender CHAR(1),
  SubjectPosition NVARCHAR(500),	
  InterviewDateTime DATETIME,
  InterviewerName1 NVARCHAR(500),
  InterviewerName2 NVARCHAR(500),
	Question01 NVARCHAR(MAX),
	Question02 NVARCHAR(MAX),
	Question03 NVARCHAR(MAX),
	Question04 NVARCHAR(MAX),
	Question05 NVARCHAR(MAX),
	Question06 NVARCHAR(MAX),
	Question07 NVARCHAR(MAX),
	Question08 NVARCHAR(MAX),
	Question09 NVARCHAR(MAX),
	Question10 NVARCHAR(MAX),
	Question11 NVARCHAR(MAX),
	Question12 NVARCHAR(MAX),
	Question13 NVARCHAR(MAX),
	Question14 NVARCHAR(MAX),
	Question15 NVARCHAR(MAX),
	Question16 NVARCHAR(MAX),
	Question17 NVARCHAR(MAX),
	ReferenceCode VARCHAR(6)
	) 

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0	

EXEC utility.SetPrimaryKeyClustered @TableName, 'KeyInformantSurveyID'

SET IDENTITY_INSERT dbo.KeyInformantSurvey ON
GO

INSERT INTO dbo.KeyInformantSurvey
	(KeyInformantSurveyID, CommunityID, SubjectName, SubjectGender, SubjectPosition, InterviewDateTime, InterviewerName1, InterviewerName2, Question01, Question02, Question03, Question04, Question05, Question06, Question07, Question08, Question09, Question10, Question11, Question12, Question13, Question14, Question15, Question16, Question17, ReferenceCode)
SELECT
	SCS.InformantSurveyID, 
	SCS.Community_ID, 
	SCS.SubjectName,
	SCS.SubjectGender, 
	SCS.SubjectPosition,
	SCS.InterviewDateTime, 
	SCS.InterviewerName1, 
	SCS.InterviewerName2, 
	SCS.Question01, 
	SCS.Question02, 
	SCS.Question03, 
	SCS.Question04, 
	SCS.Question05, 
	SCS.Question06, 
	SCS.Question07, 
	SCS.Question08, 
	SCS.Question09, 
	SCS.Question10, 
	SCS.Question11, 
	SCS.Question12, 
	SCS.Question13, 
	SCS.Question14, 
	SCS.Question15, 
	SCS.Question16, 
	SCS.Question17,
	SCS.ReferenceCode
FROM amnuna_db.dbo.informantsurvey SCS
GO

SET IDENTITY_INSERT dbo.KeyInformantSurvey OFF
GO
--End table dbo.KeyInformantSurvey

--Begin table dbo.RapidPerceptionSurvey (old table name:  perceptionsurvey)
DECLARE @TableName VARCHAR(250) = 'dbo.RapidPerceptionSurvey'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.RapidPerceptionSurvey
	(
	RapidPerceptionSurveyID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	SubjectName NVARCHAR(500),
	SubjectAge INT,
	SubjectGender CHAR(1),
	SubjectPosition NVARCHAR(500),	
	InterviewDateTime DATETIME,
	InterviewerName1 NVARCHAR(500),
	InterviewerName2 NVARCHAR(500),
	InvitedToFocusGroup BIT,
	Question01 INT,
	Question02 INT,
	Question03 INT,
	Question04 INT,
	Question05 INT,
	Question06 INT,
	Question07 INT,
	Question08 INT,
	Question09 INT,
	Question10 INT,
	Question11 INT,
	Question12 INT,
	Question13 INT,
	Question14 INT,
	Question15 INT,
	Question16 INT,
	Question17 INT,
	Question18 INT,
	Question19 INT,
	Question20 INT,
	Question21 INT,
	Question22 INT,
	Question23 NVARCHAR(MAX),
	Question24 INT,
	Question25 INT,
	Question26 INT,
	Question27 INT,
	Question28 INT,
	Question29 INT,
	Question30 NVARCHAR(MAX),
	Question31 INT,
	Question32 INT,
	Question33 NVARCHAR(MAX),
	ReferenceCode VARCHAR(6)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'InvitedToFocusGroup', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question01', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question02', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question03', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question04', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question05', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question06', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question07', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question08', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question09', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question10', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question11', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question12', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question13', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question14', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question15', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question16', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question17', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question18', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question19', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question20', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question21', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question22', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question24', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question25', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question26', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question27', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question28', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question29', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question31', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Question32', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RapidPerceptionSurveyID'

SET IDENTITY_INSERT dbo.RapidPerceptionSurvey ON
GO

INSERT INTO dbo.RapidPerceptionSurvey
	(RapidPerceptionSurveyID, CommunityID, SubjectName, SubjectAge, SubjectGender, SubjectPosition, InterviewDateTime, InterviewerName1, InterviewerName2, InvitedToFocusGroup, Question01, Question02, Question03, Question04, Question05, Question06, Question07, Question08, Question09, Question10, Question11, Question12, Question13, Question14, Question15, Question16, Question17, Question18, Question19, Question20, Question21, Question22, Question23, Question24, Question25, Question26, Question27, Question28, Question29, Question30, Question31, Question32, Question33, ReferenceCode)
SELECT
	SCS.PerceptionSurveyID, 
	SCS.Community_ID, 
	SCS.SubjectName,
	SCS.SubjectAge,
	SCS.SubjectGender, 
	SCS.SubjectPosition,
	SCS.InterviewDateTime, 
	SCS.InterviewerName1, 
	SCS.InterviewerName2, 
	SCS.InvitedToFocusGroup,
	SCS.Question01, 
	SCS.Question02, 
	SCS.Question03, 
	SCS.Question04, 
	SCS.Question05, 
	SCS.Question06, 
	SCS.Question07, 
	SCS.Question08, 
	SCS.Question09, 
	SCS.Question10, 
	SCS.Question11, 
	SCS.Question12, 
	SCS.Question13, 
	SCS.Question14, 
	SCS.Question15, 
	SCS.Question16, 
	SCS.Question17,
	SCS.Question18, 
	SCS.Question19, 
	SCS.Question20, 
	SCS.Question21, 
	SCS.Question22, 
	SCS.Question23, 
	SCS.Question24, 
	SCS.Question25, 
	SCS.Question26, 
	SCS.Question27,
	SCS.Question28, 
	SCS.Question29, 
	SCS.Question30, 
	SCS.Question31, 
	SCS.Question32, 
	SCS.Question33,
	SCS.ReferenceCode
FROM amnuna_db.dbo.perceptionsurvey SCS
GO

SET IDENTITY_INSERT dbo.RapidPerceptionSurvey OFF
GO
--End table dbo.RapidPerceptionSurvey

--Begin table dbo.StakeholderGroupSurvey (old table name:  stakeholdersurvey)
DECLARE @TableName VARCHAR(250) = 'dbo.StakeholderGroupSurvey'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.StakeholderGroupSurvey
	(
  StakeholderGroupSurveyID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
  SubjectName NVARCHAR(500),
  InterviewDateTime DATETIME,
  InterviewerName1 NVARCHAR(500),
  InterviewerName2 NVARCHAR(500),
  Question01 NVARCHAR(MAX),
  Question02 NVARCHAR(MAX),
  Question03 NVARCHAR(MAX),
  Question04 NVARCHAR(MAX),
  Question05 NVARCHAR(MAX),
  Question06 NVARCHAR(MAX),
  Question07 NVARCHAR(MAX),
  Question08 NVARCHAR(MAX),
  Question09 NVARCHAR(MAX),
  Question10 NVARCHAR(MAX),
  Question11 NVARCHAR(MAX),
  Question12 NVARCHAR(MAX),
  Question13 NVARCHAR(MAX),
  Question14 NVARCHAR(MAX),
  Question15 NVARCHAR(MAX),
  Question16 NVARCHAR(MAX),
  Question17 NVARCHAR(MAX),
  Question18 NVARCHAR(MAX),
	ReferenceCode VARCHAR(6)
	) 

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0	

EXEC utility.SetPrimaryKeyClustered @TableName, 'StakeholderGroupSurveyID'

SET IDENTITY_INSERT dbo.StakeholderGroupSurvey ON
GO

INSERT INTO dbo.StakeholderGroupSurvey
	(StakeholderGroupSurveyID, CommunityID, SubjectName, InterviewDateTime, InterviewerName1, InterviewerName2, Question01, Question02, Question03, Question04, Question05, Question06, Question07, Question08, Question09, Question10, Question11, Question12, Question13, Question14, Question15, Question16, Question18, Question17, ReferenceCode)
SELECT
	SCS.StakeholderSurveyID, 
	SCS.Community_ID, 
	SCS.SubjectName, 
	SCS.InterviewDateTime, 
	SCS.InterviewerName1, 
	SCS.InterviewerName2, 
	SCS.Question01, 
	SCS.Question02, 
	SCS.Question03, 
	SCS.Question04, 
	SCS.Question05, 
	SCS.Question06, 
	SCS.Question07, 
	SCS.Question08, 
	SCS.Question09, 
	SCS.Question10, 
	SCS.Question11, 
	SCS.Question12, 
	SCS.Question13, 
	SCS.Question14, 
	SCS.Question15, 
	SCS.Question16, 
	SCS.Question17, 
	SCS.Question18,
	SCS.ReferenceCode
FROM amnuna_db.dbo.stakeholdersurvey SCS
GO

SET IDENTITY_INSERT dbo.StakeholderGroupSurvey OFF
GO
--End table dbo.StakeholderGroupSurvey

--Begin table dbo.StationCommanderSurvey (old table name:  stationcommandersurvey)
DECLARE @TableName VARCHAR(250) = 'dbo.StationCommanderSurvey'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.StationCommanderSurvey
	(
  StationCommanderSurveyID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
  SubjectName NVARCHAR(500),
  InterviewDateTime DATETIME,
  InterviewerName1 NVARCHAR(500),
  InterviewerName2 NVARCHAR(500),
  Question01 NVARCHAR(MAX),
  Question02 NVARCHAR(MAX),
  Question03 NVARCHAR(MAX),
  Question04 INT,
  Question05 NVARCHAR(MAX),
  Question06 NVARCHAR(MAX),
  Question07 INT,
  Question08 NVARCHAR(MAX),
  Question09 INT,
  Question10 NVARCHAR(MAX),
  Question11 NVARCHAR(MAX),
  Question12 INT,
  Question13 NVARCHAR(MAX),
  Question14 INT,
  Question15 NVARCHAR(MAX),
  Question16 NVARCHAR(MAX),
  Question17 NVARCHAR(MAX),
  Question18 NVARCHAR(MAX),
	ReferenceCode VARCHAR(6)
	) 

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'Question04', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'Question07', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'Question09', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'Question12', 'INT', 0	
EXEC utility.SetDefaultConstraint @TableName, 'Question14', 'INT', 0	

EXEC utility.SetPrimaryKeyClustered @TableName, 'StationCommanderSurveyID'

SET IDENTITY_INSERT dbo.StationCommanderSurvey ON
GO

INSERT INTO dbo.StationCommanderSurvey
	(StationCommanderSurveyID, CommunityID, SubjectName, InterviewDateTime, InterviewerName1, InterviewerName2, Question01, Question02, Question03, Question04, Question05, Question06, Question07, Question08, Question09, Question10, Question11, Question12, Question13, Question14, Question15, Question16, Question17, Question18, ReferenceCode)
SELECT
	SCS.StationCommanderSurveyID, 
	SCS.Community_ID, 
	SCS.SubjectName, 
	SCS.InterviewDateTime, 
	SCS.InterviewerName1, 
	SCS.InterviewerName2, 
	SCS.Question01, 
	SCS.Question02, 
	SCS.Question03, 
	SCS.Question04, 
	SCS.Question05, 
	SCS.Question06, 
	SCS.Question07, 
	SCS.Question08, 
	SCS.Question09, 
	SCS.Question10, 
	SCS.Question11, 
	SCS.Question12, 
	SCS.Question13, 
	SCS.Question14, 
	SCS.Question15, 
	SCS.Question16, 
	SCS.Question17,
	SCS.Question18,
	SCS.ReferenceCode
FROM amnuna_db.dbo.stationcommandersurvey SCS
GO

SET IDENTITY_INSERT dbo.StationCommanderSurvey OFF
GO
--End table dbo.StationCommanderSurvey
