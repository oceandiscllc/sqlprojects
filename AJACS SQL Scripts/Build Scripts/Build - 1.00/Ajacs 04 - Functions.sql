USE AJACS
GO
--Begin functions
--Begin function dbo.FormatDate
EXEC utility.DropObject 'dbo.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION dbo.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 113)

END
GO
--End function dbo.FormatDate

--Begin function dbo.FormatDateTime
EXEC utility.DropObject 'dbo.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION dbo.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(17)

AS
BEGIN

	RETURN CONVERT(CHAR(17), @DateTimeData, 113)

END
GO
--End function dbo.FormatDateTime

--Begin function dbo.FormatPersonName
EXEC utility.DropObject 'dbo.FormatPersonName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.16
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION dbo.FormatPersonName
(
@FirstName VARCHAR(100),
@LastName VARCHAR(100),
@Title VARCHAR(50),
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250) = ''
	
	SET @FirstName = ISNULL(@FirstName, '')
	SET @LastName = ISNULL(@LastName, '')
	SET @Title = ISNULL(@Title, '')
	
	IF @Format = 'FirstLast' OR @Format = 'TitleFirstLast'
		BEGIN
			
		SET @cRetVal = @FirstName + ' ' + @LastName
	
		IF @Format = 'TitleFirstLast' AND LEN(RTRIM(@Title)) > 0
			BEGIN
				
			SET @cRetVal = @Title + ' ' + RTRIM(LTRIM(@cRetVal))
	
			END
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format = 'LastFirst' OR @Format = 'LastFirstTitle'
		BEGIN
			
		IF LEN(RTRIM(@LastName)) > 0
			BEGIN
				
			SET @cRetVal = @LastName + ', '
	
			END
		--ENDIF
				
		SET @cRetVal = @cRetVal + @FirstName + ' '
	
		IF @Format = 'LastFirstTitle' AND LEN(RTRIM(@Title)) > 0
			BEGIN
				
			SET @cRetVal = @cRetVal + @Title
	
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatPersonName

--Begin function dbo.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'dbo.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION dbo.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM dbo.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function dbo.GetEntityTypeNameByEntityTypeCode

--Begin function dbo.GetFileSize
EXEC utility.DropObject 'dbo.GetFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create Date:	2013.05.15
-- Description:	A function to return the size of a file with units based on a file size in bytes
-- =============================================================================================

CREATE FUNCTION dbo.GetFileSize
(
@PhysicalFileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cFileSize VARCHAR(50)

SELECT @cFileSize = 

	CASE
		WHEN @PhysicalFileSize < 1000
		THEN CAST(@PhysicalFileSize as VARCHAR(50)) + ' b'
		WHEN @PhysicalFileSize < 1000000
		THEN CAST(ROUND((@PhysicalFileSize/1000), 2) AS VARCHAR(50)) + ' kb'
		WHEN @PhysicalFileSize < 1000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
		WHEN @PhysicalFileSize < 1000000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
		ELSE 'More than 1000 gb'
	END

RETURN ISNULL(@cFileSize, '0 b')
END
GO
--End function dbo.GetFileSize

--Begin function dbo.GetPersonNameByPersonID
EXEC utility.DropObject 'dbo.GetPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A function to return the name of a person in a specified format from a PersonID
-- ============================================================================================

CREATE FUNCTION dbo.GetPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cTitle VARCHAR(50)
	DECLARE @cRetVal VARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @PersonID IS NOT NULL AND @PersonID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(P.FirstName, ''),
			@cLastName = ISNULL(P.LastName, ''),
			@cTitle = ISNULL(P.Title, '')
		FROM dbo.Person P
		WHERE P.PersonID = @PersonID
	
		IF @Format = 'FirstLast' OR @Format = 'TitleFirstLast'
			BEGIN
			
			SET @cRetVal = @cFirstName + ' ' + @cLastName
	
			IF @Format = 'TitleFirstLast' AND LEN(RTRIM(@cTitle)) > 0
				BEGIN
				
				SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))
	
				END
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstTitle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				BEGIN
				
				SET @cRetVal = @cLastName + ', '
	
				END
			--ENDIF
				
			SET @cRetVal = @cRetVal + @cFirstName + ' '
	
			IF @Format = 'LastFirstTitle' AND LEN(RTRIM(@cTitle)) > 0
				BEGIN
				
				SET @cRetVal = @cRetVal + @cTitle
	
				END
			--ENDIF
			
			END
		--ENDIF
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.GetPersonNameByPersonID

--Begin function dbo.GetReferenceCodeByEntityTypeCode
EXEC utility.DropObject 'dbo.GetReferenceCodeByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a formatted reference code
-- ============================================================

CREATE FUNCTION dbo.GetReferenceCodeByEntityTypeCode
(
@EntityTypeCode VARCHAR(50),
@CommunityID INT,
@EntityID INT,
@ReferenceCode VARCHAR(6)
)

RETURNS VARCHAR(25)

AS
BEGIN

	DECLARE @cReturn VARCHAR(25)

	SELECT @cReturn = ET.EntityTypeAbbreviation + '-' + CAST(@CommunityID AS VARCHAR(10)) + '-' + CAST(@EntityID AS VARCHAR(10)) + '-' + @ReferenceCode
	FROM dbo.EntityType ET 
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.GetReferenceCodeByEntityTypeCode

--Begin function dbo.GetServerSetupValueByServerSetupKey
EXEC utility.DropObject 'dbo.GetServerSetupValueByServerSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a ServerSetupValue from a ServerSetupKey from the AJACSUtility.dbo.ServerSetup table
-- ======================================================================================================================

CREATE FUNCTION dbo.GetServerSetupValueByServerSetupKey
(
@ServerSetupKey VARCHAR(250),
@DefaultServerSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cServerSetupValue VARCHAR(MAX)
	
	SELECT @cServerSetupValue = SS.ServerSetupValue 
	FROM AJACSUtility.dbo.ServerSetup SS 
	WHERE SS.ServerSetupKey = @ServerSetupKey
	
	RETURN ISNULL(@cServerSetupValue, @DefaultServerSetupValue)

END
GO
--End function dbo.GetServerSetupValueByServerSetupKey

--Begin function dbo.ListToTable
EXEC utility.DropObject 'dbo.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
--
-- Update date:	2012.06.07
-- Description:	Added the ListItemID column
--
-- Update date:	2014.06.04
-- Description:	Added MakeDistinct support
--
-- Update date:	2014.09.19
-- Description:	Refactored to use CTE's, dropped MakeDistinct support
-- ========================================================================

CREATE FUNCTION dbo.ListToTable
(
@List VARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT, ListItem VARCHAR(MAX))

AS
BEGIN

	SET @List += @Delimiter
	
	;
	WITH 
		ByteCount(ByteIndex) AS 
			(
			SELECT 1 
	
			UNION ALL 
	
			SELECT BC.ByteIndex + 1 
			FROM ByteCount BC 
			WHERE BC.ByteIndex < DATALENGTH(@List)
			)
	
		,StartIndex(StartIndex) AS 
			(
			SELECT 1 
	
			UNION ALL 
	
			SELECT BC.ByteIndex + 1 
			FROM ByteCount BC 
			WHERE SUBSTRING(@List, BC.ByteIndex, 1) = @Delimiter
			)
	
		,ItemLength(StartIndex, ItemLength) AS 
			(
			SELECT 
				SI.StartIndex, 
	
				CASE
					WHEN CHARINDEX(@Delimiter, @List, SI.StartIndex) = 0
					THEN CHARINDEX(@Delimiter, REVERSE(@List), 0) - 1
					ELSE CHARINDEX(@Delimiter, @List, SI.StartIndex) - SI.StartIndex
				END
	
			FROM StartIndex SI
			)
	
	INSERT INTO @tTable
		(ListItemID, ListItem)
	SELECT
		ROW_NUMBER() OVER(ORDER BY IL.StartIndex),
		SUBSTRING(@List, IL.StartIndex, IL.ItemLength)
	FROM ItemLength IL
	WHERE LEN(SUBSTRING(@List, IL.StartIndex, IL.ItemLength)) > 0
	OPTION (MAXRECURSION 0)
	
	RETURN

END
GO
--End function dbo.ListToTable
--End functions