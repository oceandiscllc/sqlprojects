USE AJACS
GO

--Begin Tables
--Begin table procurement.EquipmentCatalog (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentCatalog'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentCatalog
	(
	EquipmentCatalogID INT IDENTITY(1,1) NOT NULL,
	ItemName VARCHAR(250),
	EquipmentCatalogCategoryID INT,
	UnitOfIssue VARCHAR(25),
	UnitCost NUMERIC(18,2),
	Notes VARCHAR(MAX),
	IsCommon BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentCatalogCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsCommon', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UnitCost', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentCatalogID'
EXEC utility.SetIndexClustered 'IX_EquipmentCatalog', @TableName, 'ItemName,EquipmentCatalogCategoryID'
GO
--End table procurement.EquipmentCatalog

--Begin table procurement.License (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'procurement.License'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.License
	(
	LicenseID INT IDENTITY(1,1) NOT NULL,
	LicenseNumber VARCHAR(10),
	StartDate DATE,
	EndDate DATE
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'LicenseID'
GO
--End table procurement.License

--Begin table procurement.LicenseEquipmentCatalog (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'procurement.LicenseEquipmentCatalog'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.LicenseEquipmentCatalog
	(
	LicenseEquipmentCatalogID INT IDENTITY(1,1) NOT NULL,
	LicenseID INT,
	EquipmentCatalogID INT,
	LicenseItemDescription VARCHAR(1000),
	ECCN VARCHAR(10),
	QuantityAuthorized INT,
	QuantityPending INT,
	QuantityObligated INT,
	QuantityPurchased INT,
	QuantityDistributed INT,
	BudgetLimit	NUMERIC(18,2),
	ReferenceCode VARCHAR(6)
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'BudgetLimit', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentCatalogID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'LicenseID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuantityAuthorized', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuantityDistributed', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuantityObligated', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuantityPending', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuantityPurchased', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'LicenseEquipmentCatalogID'
EXEC utility.SetIndexNonClustered 'IX_LicenseEquipmentCatalog', @TableName, 'LicenseID,EquipmentCatalogID'
GO
--End table procurement.LicenseEquipmentCatalog
--End Tables

--Begin Procedures
--Begin procedure dropdown.GetEquipmentCatalogCategoryData
EXEC Utility.DropObject 'dropdown.GetEquipmentCatalogCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to return data from the dropdown.EquipmentCatalogCategory table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetEquipmentCatalogCategoryData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EquipmentCatalogCategoryID, 
		T.EquipmentCatalogCategoryName
	FROM dropdown.EquipmentCatalogCategory T
	WHERE (T.EquipmentCatalogCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EquipmentCatalogCategoryName, T.EquipmentCatalogCategoryID

END
GO
--End procedure dropdown.GetEquipmentCatalogCategoryData

--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.EquipmentCatalogID, 
		EC.IsCommon,
		EC.ItemName, 
		EC.Notes, 
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.UnitOfIssue, 
		ECC.EquipmentCatalogCategoryID, 
		ECC.EquipmentCatalogCategoryName, 
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentCatalog') AS EntityTypeName
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.EquipmentCatalogCategory ECC ON ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
		
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID

--Begin procedure procurement.GetLicenseByLicenseID
EXEC Utility.DropObject 'procurement.GetLicenseByLicenseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the procurement.License table
-- ==========================================================================
CREATE PROCEDURE procurement.GetLicenseByLicenseID

@LicenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		L.EndDate,
		dbo.FormatDate(L.EndDate) AS EndDateFormatted,
		L.LicenseID,
		L.LicenseNumber,
		L.StartDate,
		dbo.FormatDate(L.StartDate) AS StartDateFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('License') AS EntityTypeName
	FROM procurement.License L
	WHERE L.LicenseID = @LicenseID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'License'
			AND DE.EntityID = @LicenseID
		
END
GO
--End procedure procurement.GetLicenseByLicenseID

--Begin procedure procurement.GetLicenses
EXEC Utility.DropObject 'procurement.GetLicenses'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.License table
-- ==========================================================================
CREATE PROCEDURE procurement.GetLicenses

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		L.LicenseID,
		L.LicenseNumber
	FROM procurement.License L
	ORDER BY L.LicenseNumber
		
END
GO
--End procedure procurement.GetLicenses

--Begin procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.LicenseEquipmentCatalog table
-- ==========================================================================================
CREATE PROCEDURE procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID

@LicenseEquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentCatalogID,
		EC.ItemName,
		L.LicenseID,
		L.LicenseNumber,
		LEC.BudgetLimit,
		LEC.ECCN,
		LEC.LicenseEquipmentCatalogID,
		LEC.LicenseItemDescription,
		LEC.ReferenceCode,
		LEC.QuantityAuthorized,
		LEC.QuantityDistributed,
		LEC.QuantityObligated,
		LEC.QuantityPending,
		LEC.QuantityPurchased,
		dbo.GetEntityTypeNameByEntityTypeCode('LicensedEquipmentCatalog') AS EntityTypeName
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = LEC.EquipmentCatalogID
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND LEC.LicenseEquipmentCatalogID = @LicenseEquipmentCatalogID
		
END
GO
--End procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID
--End Procedures
