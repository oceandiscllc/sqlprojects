USE AJACS
GO

--Begin Tables
--Begin table workflow.EntityWorkflowPerson
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowPerson'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.EntityWorkflowPerson
	(
	EntityWorkflowPersonID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	WorkflowStepNumber INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EntityWorkflowPersonID'
EXEC utility.SetIndexClustered 'IX_EntityWorkflowPerson', @TableName, 'EntityTypeCode,EntityID,WorkflowStepNumber,PersonID'
GO
--End table workflow.EntityWorkflowPerson

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.Workflow
	(
	WorkflowID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'WorkflowID'
EXEC utility.SetIndexClustered 'IX_Workflow', @TableName, 'EntityTypeCode'
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowPerson
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowPerson'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowPerson
	(
	WorkflowPersonID INT IDENTITY(1,1) NOT NULL,
	WorkflowID INT,
	WorkflowStepNumber INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'WorkflowPersonID'
EXEC utility.SetIndexClustered 'IX_WorkflowPerson', @TableName, 'WorkflowID,WorkflowStepNumber,PersonID'
GO
--End table workflow.WorkflowPerson
--End Tables

/*
--Begin Procedures
--Begin procedure workflow.AddWorkflowPersons
EXEC utility.DropObject 'workflow.AddWorkflowPersons'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to add data to the workflow.WorkflowPerson table based on a WorkflowID, WorkflowStepNumber and PersonIDList
-- ===========================================================================================================================================
CREATE PROCEDURE workflow.AddWorkflowPersons
@WorkflowID INT = 0,
@WorkflowStepNumber INT = 0,
@PersonIDList VARCHAR(MAX) = '',
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO workflow.WorkflowPerson
		(WorkflowID, WorkflowStepNumber, PersonID)
	SELECT
		@WorkflowID,
		@WorkflowStepNumber,
		LTT.ListItem
	FROM utility.ListToTable(@PersonIDList, ',', 0) LTT 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM workflow.WorkflowPerson WP
		WHERE WP.WorkflowID = @WorkflowID
			AND WP.WorkflowStepNumber = @WorkflowStepNumber
			AND WP.PersonID = LTT.ListItem
		)

END
GO
--End procedure workflow.AddWorkflowPersons

--Begin procedure workflow.AssignWorkflowToContract
EXEC utility.DropObject 'workflow.AssignWorkflowToContract'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to add data to the workflow.EntityWorkflowPerson table based on an EntityTypeCode and an EntityID
-- =================================================================================================================================
CREATE PROCEDURE workflow.AssignWorkflowToContract
@Comments VARCHAR(MAX) = NULL,
@EntityTypeCode VARCHAR(50) = NULL,
@EntityID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ContractWorkflowPersons VARCHAR(MAX) 

	UPDATE C
	SET 
		C.WorkflowID = W.WorkflowID,
		C.WorkflowStatusID = (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WS.WorkflowStatusCode = 'PendingSubmission'),
		C.WorkflowStepNumber = 1
	FROM workflow.Contract C
		JOIN workflow.Workflow W ON W.OrganizationID = C.OriginatingOrganizationID
			AND C.ContractID = @ContractID

	EXEC workflow.AddEventLogContractEntry @Comments = @Comments, @ContractID = @ContractID, @EventCode = 'AssignWorkflow', @PersonID = @PersonID

	DELETE CWP
	FROM workflow.ContractWorkflowPerson CWP
	WHERE CWP.ContractID = @ContractID
	
	INSERT INTO workflow.ContractWorkflowPerson
		(ContractID, WorkflowStepNumber, PersonID)
	SELECT
		C.ContractID,
		WP.WorkflowStepNumber,
		WP.PersonID
	FROM workflow.WorkflowPerson WP
		JOIN workflow.Contract C ON C.WorkflowID = WP.WorkflowID
			AND C.ContractID = @ContractID

	SELECT 
		@ContractWorkflowPersons = COALESCE(@ContractWorkflowPersons, '') + D.ContractWorkflowPerson 
	FROM
		(
		SELECT
			(SELECT CWP.* FOR XML RAW('ContractWorkflowPerson'), ELEMENTS) AS ContractWorkflowPerson
		FROM workflow.ContractWorkflowPerson CWP
		WHERE CWP.ContractID = @ContractID
		) D

	INSERT INTO workflow.EventLog
		(EntityID,EntityTypeCode,EventCode,EventData,PersonID)
	VALUES
		(
		@ContractID,
		'Contract',
		'AssignWorkflowPersons',
		CAST(('<ContractWorkflowPersons>' + @ContractWorkflowPersons + '</ContractWorkflowPersons>') AS XML),
		@PersonID
		)

END
GO
--End procedure workflow.AssignWorkflowToContract

--Begin procedure workflow.DeleteWorkflowPersonByWorkflowPersonID
EXEC utility.DropObject 'workflow.DeleteWorkflowPersonByWorkflowPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to delete data from the workflow.WorkflowPerson table based on an WorkflowPersonID
-- =============================================================================================================
CREATE PROCEDURE workflow.DeleteWorkflowPersonByWorkflowPersonID
@WorkflowPersonID INT = 0,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE
	FROM workflow.WorkflowPerson
	WHERE WorkflowPersonID = @WorkflowPersonID

END
GO
--End procedure workflow.DeleteWorkflowPersonByWorkflowPersonID

--Begin procedure workflow.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to get data from the workflow.ContractWorkflowPerson table based on a ContractID and a WorkflowStepNumber
-- ====================================================================================================================================
CREATE PROCEDURE workflow.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber
@ContractID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CWP.ContractID,
		CWP.WorkflowStepNumber,
		O.OrganizationName,
		P.EmailAddress,
		P.PersonID,
		workflow.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.Phone
	FROM workflow.ContractWorkflowPerson CWP
		JOIN workflow.Person P ON P.PersonID = CWP.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND CWP.ContractID = @ContractID
			AND CWP.WorkflowStepNumber = @WorkflowStepNumber
	ORDER BY CWP.WorkflowStepNumber, 6, P.PersonID
		
END
GO
--End procedure workflow.GetContractWorkflowPersonsByContractIDAndWorkflowStepNumber

--Begin procedure workflow.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to get data from the workflow.WorkflowPerson table based on a WorkflowID and a WorkflowStepNumber
-- ============================================================================================================================
CREATE PROCEDURE workflow.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber
@WorkflowID INT = 0,
@WorkflowStepNumber INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		@WorkflowID AS WorkflowID,
		@WorkflowStepNumber AS WorkflowStepNumber,
		O.OrganizationName,
		P.EmailAddress,
		P.PersonID,
		workflow.GetPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.Phone
	FROM workflow.WorkflowPerson WP
		JOIN workflow.Person P ON P.PersonID = WP.PersonID
		JOIN Dropdown.Organization O ON O.OrganizationID = P.OrganizationID
			AND WP.WorkflowID = @WorkflowID
			AND WP.WorkflowStepNumber = @WorkflowStepNumber
	ORDER BY 2, 6, P.PersonID
		
END
GO
--End procedure workflow.GetWorkflowPersonsByWorkflowIDAndWorkflowStepNumber

--Begin procedure workflow.GetWorkflowStepNumberByContractID
EXEC utility.DropObject 'workflow.GetWorkflowStepNumberByContractID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to validate a workflow step number for a contract
-- =================================================================================
CREATE PROCEDURE workflow.GetWorkflowStepNumberByContractID
@ContractID INT = 0,

@ReturnValue INT OUTPUT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT @ReturnValue = C.WorkflowStepNumber
	FROM workflow.Contract C
	WHERE C.ContractID = @ContractID
	
END
GO
--End procedure workflow.GetWorkflowStepNumberByContractID

--Begin procedure workflow.GetWorkflowStepsByWorkflowID
EXEC utility.DropObject 'workflow.GetWorkflowStepsByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to get data from the workflow.WorkflowPerson table based on a WorkflowID
-- ===================================================================================================
CREATE PROCEDURE workflow.GetWorkflowStepsByWorkflowID
@WorkflowID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		RowIndex INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		WorkflowStepPersonCount INT NOT NULL DEFAULT 0,
		WorkflowID INT NOT NULL DEFAULT 0,
		WorkflowStepNumber INT NOT NULL DEFAULT 0,
		IsFirst BIT NOT NULL DEFAULT 0,
		IsLast BIT NOT NULL DEFAULT 0
		)
	
	IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowPerson WP WHERE WP.WorkflowID = @WorkflowID)
		BEGIN

		INSERT INTO @tTable
			(WorkflowStepPersonCount,WorkflowID,WorkflowStepNumber)
		VALUES
			(0,@WorkflowID,1),
			(0,@WorkflowID,2)
		
		END
	ELSE
		BEGIN
		
		INSERT INTO @tTable
			(WorkflowStepPersonCount,WorkflowID,WorkflowStepNumber)
		SELECT
			COUNT(WP.WorkflowPersonID),
			WP.WorkflowID,
			WP.WorkflowStepNumber
		FROM workflow.WorkflowPerson WP
		WHERE WP.WorkflowID = @WorkflowID
		GROUP BY 
			WP.WorkflowID,
			WP.WorkflowStepNumber
		ORDER BY WP.WorkflowStepNumber

		END
	--ENDIF

	UPDATE @tTable 
	SET 
		IsFirst = 
			CASE 
				WHEN RowIndex = 1
				THEN 1
				ELSE 0
			END,
		
		IsLast = 
			CASE 
				WHEN RowIndex = (SELECT MAX(T.RowIndex) FROM @tTable T)
				THEN 1
				ELSE 0
			END

	SELECT
		T.WorkflowStepPersonCount,
		T.WorkflowID,
		T.RowIndex AS WorkflowStepNumber,
		(SELECT COUNT(T.RowIndex) FROM @tTable T) AS WorkflowStepCount,
		T.IsFirst,
		T.IsLast
	FROM @tTable T
	ORDER BY RowIndex
		
END
GO
--End procedure workflow.GetWorkflowStepsByWorkflowID

--Begin procedure workflow.GetWorkflowsByOrganizationID
EXEC utility.DropObject 'workflow.GetWorkflowsByOrganizationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to get data from the workflow.Workflow table based on an OrganizationID
-- ==================================================================================================
CREATE PROCEDURE workflow.GetWorkflowsByOrganizationID
@OrganizationID INT = 0,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tWorkflow AS AccessControl.Workflow
	
	INSERT INTO @tWorkflow (WorkflowID) SELECT W.WorkflowID FROM workflow.Workflow W WHERE W.OrganizationID = @OrganizationID

	SELECT
		W.WorkflowID,
		W.OrganizationID,
		O.OrganizationName,
		UR.CanHaveDelete,
		UR.CanHaveEdit
	FROM workflow.Workflow W
		JOIN AccessControl.GetWorkflowRecordsByPersonID(@PersonID, @tWorkflow) UR ON UR.WorkflowID = W.WorkflowID
		JOIN Dropdown.Organization O ON O.OrganizationID = W.OrganizationID

END
GO
--End procedure workflow.GetWorkflowsByOrganizationID

--Begin procedure workflow.IncrementWorkflow
EXEC utility.DropObject 'workflow.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to move a contract record to the next step in its workflow
-- ==========================================================================================
CREATE PROCEDURE workflow.IncrementWorkflow
@Comments VARCHAR(MAX),
@ContractID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentWorkflowStepNumber INT
	DECLARE @FinalWorkflowStepNumber INT
	
	SELECT
		@CurrentWorkflowStepNumber = C.WorkflowStepNumber,
		@FinalWorkflowStepNumber = (SELECT MAX(CWP.WorkflowStepNumber) FROM workflow.ContractWorkflowPerson CWP WHERE CWP.ContractID = C.ContractID)
	FROM workflow.Contract C
		JOIN Dropdown.WorkflowStatus WS ON WS.WorkflowStatusID = C.WorkflowStatusID
			AND C.ContractID = @ContractID

	UPDATE C
	SET 
		C.WorkflowStatusID = 
			CASE 
				WHEN @CurrentWorkflowStepNumber < @FinalWorkflowStepNumber
				THEN (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WorkflowStatusCode = 'PendingApproval')
				ELSE (SELECT WS.WorkflowStatusID FROM Dropdown.WorkflowStatus WS WHERE WorkflowStatusCode = 'Approved')
			END,

		C.WorkflowStepNumber = @CurrentWorkflowStepNumber + 1
	FROM workflow.Contract C
	WHERE C.ContractID = @ContractID

	EXEC workflow.AddEventLogContractEntry @Comments = @Comments, @ContractID = @ContractID, @EventCode = 'IncrementWorkflow', @PersonID = @PersonID

	SELECT
		@CurrentWorkflowStepNumber + 1 AS CurrentWorkflowStepNumber,
		@FinalWorkflowStepNumber AS FinalWorkflowStepNumber
	
END
GO
--End procedure workflow.IncrementWorkflow

--Begin procedure workflow.SaveWorkflowPerson
EXEC utility.DropObject 'workflow.SaveWorkflowPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to save data to the workflow.WorkflowPerson table
-- ============================================================================
CREATE PROCEDURE workflow.SaveWorkflowPerson
@WorkflowID INT,
@WorkflowStepPersonData VARCHAR(MAX) = NULL,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tWorkflowStepPerson TABLE 
		(
		WorkflowStepNumber INT,
		PersonID INT
		)

	INSERT INTO @tWorkflowStepPerson
		(WorkflowStepNumber,PersonID)
	SELECT
		CAST(utility.ListGetAt(LTT.ListItem, 1, '_') AS INT),
		CAST(utility.ListGetAt(LTT.ListItem, 2, '_') AS INT)
	FROM utility.ListToTable(@WorkflowStepPersonData, ',', 1) LTT 

	DELETE WP
	FROM workflow.WorkflowPerson WP
	WHERE WP.WorkflowID = @WorkflowID
		AND NOT EXISTS
			(
			SELECT 1
			FROM @tWorkflowStepPerson WSP
			WHERE WSP.WorkflowStepNumber = WP.WorkflowStepNumber
				AND WSP.PersonID = WP.PersonID
			)

	INSERT INTO workflow.WorkflowPerson
		(WorkflowID,WorkflowStepNumber,PersonID)
	SELECT
		@WorkflowID,
		WSP.WorkflowStepNumber,
		WSP.PersonID
	FROM @tWorkflowStepPerson WSP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM workflow.WorkflowPerson WP
		WHERE WP.WorkflowID = @WorkflowID
			AND WP.WorkflowStepNumber = WSP.WorkflowStepNumber
			AND WP.PersonID = WSP.PersonID
		)

	EXEC workflow.AddEventLogWorkflowEntry @WorkflowID = @WorkflowID, @EventCode = 'Update', @PersonID = @PersonID
		
END
GO
--End procedure workflow.SaveWorkflowPerson

--Begin procedure Dropdown.GetWorkflowStatusByWorkflowStatusCode
EXEC utility.DropObject 'Dropdown.GetWorkflowStatusByWorkflowStatusCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.02.14
-- Description:	A stored procedure to return data from the Dropdown.WorkflowStatus table based on a WorkflowStatusCode
-- ===================================================================================================================
CREATE PROCEDURE Dropdown.GetWorkflowStatusByWorkflowStatusCode
@WorkflowStatusCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		WS.WorkflowStatusID,
		WS.WorkflowStatusName
	FROM Dropdown.WorkflowStatus WS
	WHERE WS.WorkflowStatusCode = @WorkflowStatusCode

END
GO
--End procedure Dropdown.GetWorkflowStatusByWorkflowStatusCode
--End Procedures
*/