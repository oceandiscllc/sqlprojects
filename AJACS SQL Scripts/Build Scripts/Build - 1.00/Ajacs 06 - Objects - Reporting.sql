USE AJACS
GO

--Begin procedure reporting.GetWeeklyReport
EXEC Utility.DropObject 'reporting.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
-- =================================================================
CREATE PROCEDURE reporting.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionName,
		ID.HexColor
	FROM
		(
		SELECT
			C.ImpactDecisionID
		FROM weeklyreport.Community C
	
		UNION
		
		SELECT
			P.ImpactDecisionID
		FROM weeklyreport.Province P
		) D
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = D.ImpactDecisionID
	ORDER BY ID.DisplayOrder, ID.ImpactDecisionName, ID.ImpactDecisionID


	SELECT
		SC.StatusChangeName,
		SC.HexColor
	FROM
		(
		SELECT
			C.StatusChangeID
		FROM weeklyreport.Community C
	
		UNION
		
		SELECT
			P.StatusChangeID
		FROM weeklyreport.Province P
		) D
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = D.StatusChangeID
	ORDER BY SC.DisplayOrder, SC.StatusChangeName, SC.StatusChangeID

	SELECT
		P.ProvinceName, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
	ORDER BY P.ProvinceName, P.ProvinceID
				
	SELECT
		C.CommunityName, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		CES.CommunityEngagementStatusName,
		ID.ImpactDecisionName,
		ID.HexColor AS ImpactDecisionHexColor,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor,
		
		CASE
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID IN (2,3)
			THEN 'Watch'
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID = 4
			THEN 'Engage'
			WHEN C.StatusChangeID = 1
			THEN 'Alert'
			ELSE ''
		END AS CommunityReportStatusName
			
	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure reporting.GetWeeklyReport