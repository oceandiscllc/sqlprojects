USE AJACS
GO

--Begin Tables
--Begin table dbo.Contact (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Contact
	(
	ContactID INT IDENTITY(1,1) NOT NULL,
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	ArabicName NVARCHAR(500),
	Address1 VARCHAR(100),
	Address2 VARCHAR(100), 
	City VARCHAR(100), 
	State VARCHAR(100), 
	PostalCode VARCHAR(10),
	CountryID INT,
	PhoneNumber VARCHAR(20),
	CellPhoneNumber VARCHAR(20),
	FaxNumber VARCHAR(20),
	EmailAddress1 VARCHAR(320),
	EmailAddress2 VARCHAR(320),
	SkypeUserName VARCHAR(50),
	PlaceOfBirth VARCHAR(100), 
	DateOfBirth DATE,
	Gender VARCHAR(10),
	Aliases NVARCHAR(MAX),
	CitizenshipCountryID1 INT,
	CitizenshipCountryID2 INT,
	GovernmentIDNumber VARCHAR(20), 
	GovernmentIDNumberCountryID INT,
	PassportNumber VARCHAR(20), 
	EmployerName VARCHAR(50),
	ProjectID INT,
	Title VARCHAR(50),
	CommunityID INT,
	ProvinceID INT,
	Profession VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CitizenshipCountryID1', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CitizenshipCountryID2', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'GovernmentIDNumberCountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactID'
GO
--End table dbo.Contact

--Begin table dbo.ContactVetting (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dbo.ContactVetting'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactVetting
	(
	ContactVettingID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	VettingOutcomeID INT,
	VettingDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'VettingOutcomeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactVettingID'
EXEC utility.SetIndexClustered 'IX_ContactVetting', @TableName, 'ContactID,VettingDate,VettingOutcomeID'
GO
--End table dbo.Contact

--Begin table dropdown.Country (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0,1) NOT NULL,
	CountryName VARCHAR(50),
	ISOCountryCode CHAR(3), 
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CountryID'
EXEC utility.SetIndexNonClustered 'IX_CountryName', @TableName, 'DisplayOrder,CountryName', 'CountryID'
GO

SET IDENTITY_INSERT dropdown.Country ON
GO

INSERT INTO dropdown.Country (CountryID) VALUES (0)

SET IDENTITY_INSERT dropdown.Country OFF
GO

INSERT INTO dropdown.Country 
	(ISOCountryCode, CountryName, DisplayOrder, IsActive) 
VALUES 
	('AFG', 'Afghanistan',100, 1),
	('ALA', 'Åland Islands',100, 1),
	('ALB', 'Albania',100, 1),
	('DZA', 'Algeria',100, 1),
	('ASM', 'American Samoa',100, 1),
	('AND', 'Andorra',100, 1),
	('AGO', 'Angola',100, 1),
	('AIA', 'Anguilla',100, 1),
	('ATG', 'Antigua and Barbuda',100, 1),
	('ARG', 'Argentina',100, 1),
	('ARM', 'Armenia',100, 1),
	('ABW', 'Aruba',100, 1),
	('AUS', 'Australia',100, 1),
	('AUT', 'Austria',100, 1),
	('AZE', 'Azerbaijan',100, 1),
	('BHS', 'Bahamas',100, 1),
	('BHR', 'Bahrain',100, 1),
	('BGD', 'Bangladesh',100, 1),
	('BRB', 'Barbados',100, 1),
	('BLR', 'Belarus',100, 1),
	('BEL', 'Belgium',100, 1),
	('BLZ', 'Belize',100, 1),
	('BEN', 'Benin',100, 1),
	('BMU', 'Bermuda',100, 1),
	('BTN', 'Bhutan',100, 1),
	('BOL', 'Bolivia',100, 1),
	('BES', 'Bonaire, Sint Eustatius and Saba',100, 1),
	('BIH', 'Bosnia and Herzegovina',100, 1),
	('BWA', 'Botswana',100, 1),
	('BVT', 'Bouvet Island',100, 1),
	('BRA', 'Brazil',100, 1),
	('IOT', 'British Indian Ocean Territory',100, 1),
	('BRN', 'Brunei Darussalam',100, 1),
	('BGR', 'Bulgaria',100, 1),
	('BFA', 'Burkina Faso',100, 1),
	('BDI', 'Burundi',100, 1),
	('KHM', 'Cambodia',100, 1),
	('CMR', 'Cameroon',100, 1),
	('CAN', 'Canada',100, 1),
	('CPV', 'Cape Verde',100, 1),
	('CYM', 'Cayman Islands',100, 1),
	('CAF', 'Central African Republic',100, 1),
	('TCD', 'Chad',100, 1),
	('CHL', 'Chile',100, 1),
	('CHN', 'China',100, 1),
	('CXR', 'Christmas Island',100, 1),
	('CCK', 'Cocos (Keeling) Islands',100, 1),
	('COL', 'Colombia',100, 1),
	('COM', 'Comoros',100, 1),
	('COG', 'Congo',100, 1),
	('COD', 'Congo',100, 1),
	('COK', 'Cook Islands',100, 1),
	('CRI', 'Costa Rica',100, 1),
	('CIV', 'Côte D''Ivoire',100, 1),
	('HRV', 'Croatia',100, 1),
	('CUB', 'Cuba',100, 1),
	('CUW', 'Curaçao',100, 1),
	('CYP', 'Cyprus',100, 1),
	('CZE', 'Czech Republic',100, 1),
	('DNK', 'Denmark',100, 1),
	('DJI', 'Djibouti',100, 1),
	('DMA', 'Dominica',100, 1),
	('DOM', 'Dominican Republic',100, 1),
	('ECU', 'Ecuador',100, 1),
	('EGY', 'Egypt',100, 1),
	('SLV', 'El Salvador',100, 1),
	('GNQ', 'Equatorial Guinea',100, 1),
	('ERI', 'Eritrea',100, 1),
	('EST', 'Estonia',100, 1),
	('ETH', 'Ethiopia',100, 1),
	('FLK', 'Falkland Islands (Malvinas)',100, 1),
	('FRO', 'Faroe Islands',100, 1),
	('FJI', 'Fiji',100, 1),
	('FIN', 'Finland',100, 1),
	('FRA', 'France',100, 1),
	('GUF', 'French Guiana',100, 1),
	('PYF', 'French Polynesia',100, 1),
	('ATF', 'French Southern Territories',100, 1),
	('GAB', 'Gabon',100, 1),
	('GMB', 'Gambia',100, 1),
	('GEO', 'Georgia',100, 1),
	('DEU', 'Germany',100, 1),
	('GHA', 'Ghana',100, 1),
	('GIB', 'Gibraltar',100, 1),
	('GRC', 'Greece',100, 1),
	('GRL', 'Greenland',100, 1),
	('GRD', 'Grenada',100, 1),
	('GLP', 'Guadeloupe',100, 1),
	('GUM', 'Guam',100, 1),
	('GTM', 'Guatemala',100, 1),
	('GGY', 'Guernsey',100, 1),
	('GIN', 'Guinea',100, 1),
	('GNB', 'Guinea-Bissau',100, 1),
	('GUY', 'Guyana',100, 1),
	('HTI', 'Haiti',100, 1),
	('HMD', 'Heard Island and McDonald Islands',100, 1),
	('VAT', 'Holy See (Vatican City State)',100, 1),
	('HND', 'Honduras',100, 1),
	('HKG', 'Hong Kong',100, 1),
	('HUN', 'Hungary',100, 1),
	('ISL', 'Iceland',100, 1),
	('IND', 'India',100, 1),
	('IDN', 'Indonesia',100, 1),
	('IRN', 'Iran',100, 1),
	('IRQ', 'Iraq',100, 1),
	('IRL', 'Ireland',100, 1),
	('IMN', 'Isle of Man',100, 1),
	('ISR', 'Israel',100, 1),
	('ITA', 'Italy',100, 1),
	('JAM', 'Jamaica',100, 1),
	('JPN', 'Japan',100, 1),
	('JEY', 'Jersey',100, 1),
	('JOR', 'Jordan',100, 1),
	('KAZ', 'Kazakhstan',100, 1),
	('KEN', 'Kenya',100, 1),
	('KIR', 'Kiribati',100, 1),
	('PRK', 'Korea (North)',100, 1),
	('KOR', 'Korea (South)',100, 1),
	('KWT', 'Kuwait',100, 1),
	('KGZ', 'Kyrgyzstan',100, 1),
	('LAO', 'Laos',100, 1),
	('LVA', 'Latvia',100, 1),
	('LBN', 'Lebanon',100, 1),
	('LSO', 'Lesotho',100, 1),
	('LBR', 'Liberia',100, 1),
	('LBY', 'Libya',100, 1),
	('LIE', 'Liechtenstein',100, 1),
	('LTU', 'Lithuania',100, 1),
	('LUX', 'Luxembourg',100, 1),
	('MAC', 'Macao',100, 1),
	('MKD', 'Macedonia',100, 1),
	('MDG', 'Madagascar',100, 1),
	('MWI', 'Malawi',100, 1),
	('MYS', 'Malaysia',100, 1),
	('MDV', 'Maldives',100, 1),
	('MLI', 'Mali',100, 1),
	('MLT', 'Malta',100, 1),
	('MHL', 'Marshall Islands',100, 1),
	('MTQ', 'Martinique',100, 1),
	('MRT', 'Mauritania',100, 1),
	('MUS', 'Mauritius',100, 1),
	('MYT', 'Mayotte',100, 1),
	('MEX', 'Mexico',100, 1),
	('FSM', 'Micronesia',100, 1),
	('MDA', 'Moldova',100, 1),
	('MCO', 'Monaco',100, 1),
	('MNG', 'Mongolia',100, 1),
	('MNE', 'Montenegro',100, 1),
	('MSR', 'Montserrat',100, 1),
	('MAR', 'Morocco',100, 1),
	('MOZ', 'Mozambique',100, 1),
	('MMR', 'Myanmar',100, 1),
	('NAM', 'Namibia',100, 1),
	('NRU', 'Nauru',100, 1),
	('NPL', 'Nepal',100, 1),
	('NLD', 'Netherlands',100, 1),
	('NCL', 'New Caledonia',100, 1),
	('NZL', 'New Zealand',100, 1),
	('NIC', 'Nicaragua',100, 1),
	('NER', 'Niger',100, 1),
	('NGA', 'Nigeria',100, 1),
	('NIU', 'Niue',100, 1),
	('NFK', 'Norfolk Island',100, 1),
	('MNP', 'Northern Mariana Islands',100, 1),
	('NOR', 'Norway',100, 1),
	('OMN', 'Oman',100, 1),
	('PAK', 'Pakistan',100, 1),
	('PLW', 'Palau',100, 1),
	('PAN', 'Panama',100, 1),
	('PNG', 'Papua New Guinea',100, 1),
	('PRY', 'Paraguay',100, 1),
	('PER', 'Peru',100, 1),
	('PHL', 'Philippines',100, 1),
	('PCN', 'Pitcairn',100, 1),
	('POL', 'Poland',100, 1),
	('PRT', 'Portugal',100, 1),
	('PRI', 'Puerto Rico',100, 1),
	('QAT', 'Qatar',100, 1),
	('REU', 'Réunion',100, 1),
	('ROU', 'Romania',100, 1),
	('RUS', 'Russian Federation',100, 1),
	('RWA', 'Rwanda',100, 1),
	('BLM', 'Saint Barthélemy',100, 1),
	('SHN', 'Saint Helena, Ascension and Tristan Da Cunha',100, 1),
	('KNA', 'Saint Kitts And Nevis',100, 1),
	('LCA', 'Saint Lucia',100, 1),
	('MAF', 'Saint Martin (French Part)',100, 1),
	('SPM', 'Saint Pierre And Miquelon',100, 1),
	('VCT', 'Saint Vincent And The Grenadines',100, 1),
	('WSM', 'Samoa',100, 1),
	('SMR', 'San Marino',100, 1),
	('STP', 'Sao Tome and Principe',100, 1),
	('SAU', 'Saudi Arabia',100, 1),
	('SEN', 'Senegal',100, 1),
	('SRB', 'Serbia',100, 1),
	('SYC', 'Seychelles',100, 1),
	('SLE', 'Sierra Leone',100, 1),
	('SGP', 'Singapore',100, 1),
	('SXM', 'Sint Maarten (Dutch part)',100, 1),
	('SVK', 'Slovakia',100, 1),
	('SVN', 'Slovenia',100, 1),
	('SLB', 'Solomon Islands',100, 1),
	('SOM', 'Somalia',100, 1),
	('ZAF', 'South Africa',100, 1),
	('SSD', 'South Sudan',100, 1),
	('ESP', 'Spain',100, 1),
	('LKA', 'Sri Lanka',100, 1),
	('SDN', 'Sudan',100, 1),
	('SUR', 'Suriname',100, 1),
	('SJM', 'Svalbard And Jan Mayen',100, 1),
	('SWZ', 'Swaziland',100, 1),
	('SWE', 'Sweden',100, 1),
	('CHE', 'Switzerland',100, 1),
	('SYR', 'Syrian Arab Republic',1, 1),
	('TWN', 'Taiwan',100, 1),
	('TJK', 'Tajikistan',100, 1),
	('TZA', 'Tanzania',100, 1),
	('THA', 'Thailand',100, 1),
	('TLS', 'Timor-Leste',100, 1),
	('TGO', 'Togo',100, 1),
	('TKL', 'Tokelau',100, 1),
	('TON', 'Tonga',100, 1),
	('TTO', 'Trinidad and Tobago',100, 1),
	('TUN', 'Tunisia',100, 1),
	('TUR', 'Turkey',2, 1),
	('TKM', 'Turkmenistan',100, 1),
	('TCA', 'Turks and Caicos Islands',100, 1),
	('TUV', 'Tuvalu',100, 1),
	('UGA', 'Uganda',100, 1),
	('UKR', 'Ukraine',100, 1),
	('ARE', 'United Arab Emirates',100, 1),
	('GBR', 'United Kingdom',3, 1),
	('USA', 'United States',4, 1),
	('UMI', 'United States Minor Outlying Islands',100, 1),
	('URY', 'Uruguay',100, 1),
	('UZB', 'Uzbekistan',100, 1),
	('VUT', 'Vanuatu',100, 1),
	('VEN', 'Venezuela',100, 1),
	('VNM', 'Viet Nam',100, 1),
	('VGB', 'Virgin Islands, British',100, 1),
	('VIR', 'Virgin Islands, U.S.',100, 1),
	('WLF', 'Wallis and Futuna',100, 1),
	('ESH', 'Western Sahara',100, 1),
	('YEM', 'Yemen',100, 1),
	('ZMB', 'Zambia',100, 1),
	('ZWE', 'Zimbabwe',100, 1)
GO
--End table dropdown.Country

--Begin table dropdown.Project (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.Project'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Project
	(
	ProjectID INT IDENTITY(0,1) NOT NULL,
	ProjectName VARCHAR(50),
	ISOProjectCode CHAR(3), 
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID'
EXEC utility.SetIndexNonClustered 'IX_ProjectName', @TableName, 'DisplayOrder,ProjectName', 'ProjectID'
GO

SET IDENTITY_INSERT dropdown.Project ON
GO

INSERT INTO dropdown.Project (ProjectID) VALUES (0)

SET IDENTITY_INSERT dropdown.Project OFF
GO

INSERT INTO dropdown.Project 
	(ProjectName, DisplayOrder) 
VALUES 
	('AJACS', 1),
	('AMOS', 2),
	('SSP', 3),
	('TAMKEEN', 4)
GO
--End table dropdown.Project

--Begin table dropdown.VettingOutcome (old table name:  none)
DECLARE @TableName VARCHAR(250) = 'dropdown.VettingOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.VettingOutcome
	(
	VettingOutcomeID INT IDENTITY(0,1) NOT NULL,
	VettingOutcomeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'VettingOutcomeID'
EXEC utility.SetIndexNonClustered 'IX_VettingOutcomeName', @TableName, 'DisplayOrder,VettingOutcomeName', 'VettingOutcomeID'
GO

SET IDENTITY_INSERT dropdown.VettingOutcome ON
GO

INSERT INTO dropdown.VettingOutcome (VettingOutcomeID) VALUES (0)

SET IDENTITY_INSERT dropdown.VettingOutcome OFF
GO

INSERT INTO dropdown.VettingOutcome 
	(VettingOutcomeName, DisplayOrder) 
VALUES 
	('Pending', 1),
	('Consider', 2),
	('Do Not Consider', 3)
GO
--End table dropdown.VettingOutcome
--End Tables

--Begin Procedures
--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
-- ==================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.ArabicName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceID FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID,
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID
		
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID, 
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetProjectData
EXEC Utility.DropObject 'dropdown.GetProjectData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.24
-- Description:	A stored procedure to return data from the dropdown.Project table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetProjectData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectID, 
		T.ProjectName
	FROM dropdown.Project T
	WHERE (T.ProjectID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectName, T.ProjectID

END
GO
--End procedure dropdown.GetProjectData

--Begin procedure dropdown.GetVettingOutcomeData
EXEC Utility.DropObject 'dropdown.GetVettingOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.24
-- Description:	A stored procedure to return data from the dropdown.VettingOutcome table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetVettingOutcomeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VettingOutcomeID, 
		T.VettingOutcomeName
	FROM dropdown.VettingOutcome T
	WHERE (T.VettingOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VettingOutcomeName, T.VettingOutcomeID

END
GO
--End procedure dropdown.GetVettingOutcomeData
--End Procedures
