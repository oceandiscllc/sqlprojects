USE AJACS
GO

--Begin procedure dbo.GetAtmosphericByAtmosphericID
EXEC Utility.DropObject 'dbo.GetAtmosphericByAtmosphericID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.18
-- Description:	A stored procedure to data from the dbo.Atmospheric table
-- ======================================================================
CREATE PROCEDURE dbo.GetAtmosphericByAtmosphericID

@AtmosphericID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		A.AtmosphericDate, 
		dbo.FormatDate(A.AtmosphericDate) AS AtmosphericDateFormatted, 
		A.AtmosphericID, 
		A.AtmosphericReportedDate, 
		dbo.FormatDate(A.AtmosphericReportedDate) AS AtmosphericReportedDateFormatted, 
		A.AtmosphericTypeID, 
		A.ConfidenceLevelID, 
		A.Information, 
		A.IsCritical, 
		A.Recommendation, 
		AT.AtmosphericTypeName,
		CL.ConfidenceLevelName,
		dbo.GetEntityTypeNameByEntityTypeCode('Atmospheric') AS EntityTypeName
	FROM dbo.Atmospheric A
		JOIN dropdown.AtmosphericType AT ON AT.AtmosphericTypeID = A.AtmosphericTypeID
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = A.ConfidenceLevelID
			AND A.AtmosphericID = @AtmosphericID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.AtmosphericCommunity AC
		JOIN dbo.Community C ON C.CommunityID = AC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND AC.AtmosphericID = @AtmosphericID
	ORDER BY C.CommunityName, C.CommunityID
	
	SELECT
		AECR.EngagementCriteriaID, 
		AECR.EngagementCriteriaChoiceID
	FROM dbo.AtmosphericEngagementCriteriaResult AECR
	WHERE AECR.AtmosphericID = @AtmosphericID

	SELECT
		CL.ConfidenceLevelName,
		ATS.ConfidenceLevelID,
		ATS.AtmosphericSourceID, 
		ATS.SourceAttribution, 
		ATS.SourceCategoryID, 
		ATS.SourceDate, 
		ATS.SourceName, 
		ATS.SourceSummary, 
		ATS.SourceTypeID, 
		SC.SourceCategoryName,
		ST.SourceTypeName
	FROM dbo.AtmosphericSource ATS
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = ATS.ConfidenceLevelID
		JOIN dropdown.SourceCategory SC ON SC.SourceCategoryID = ATS.SourceCategoryID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = ATS.SourceTypeID
			AND ATS.AtmosphericID = @AtmosphericID
	ORDER BY ATS.SourceName, ATS.AtmosphericSourceID
	
	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		ATS.AtmosphericSourceID
	FROM dbo.AtmosphericSource ATS
		JOIN dbo.DocumentEntity DE ON DE.EntityTypeCode = 'AtmosphericSource'
			AND DE.EntityID = ATS.AtmosphericSourceID
			AND ATS.AtmosphericID = @AtmosphericID
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
	
END
GO
--End procedure dbo.GetAtmosphericByAtmosphericID

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
-- ====================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.ExternalCapacity,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CM.CommunityID,
		CM.CommunityName,
		CO.CourseID,
		CO.CourseName
	FROM dbo.Class CL
		JOIN dbo.Community CM ON CM.CommunityID = CL.CommunityID
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
	
	SELECT
		CM.CommunityID,
		CM.CommunityName,
		dbo.FormatDate(S.DateOfBirth) AS DateOfBirthFormatted,
		S.FullName,
		S.StudentID
	FROM dbo.ClassStudent CS
		JOIN dbo.Class CL ON CL.ClassID = CS.ClassID
		JOIN dbo.Student S ON S.StudentID = CS.StudentID
		JOIN dbo.Community CM ON CM.CommunityID = S.CommunityID
			AND CL.ClassID = @ClassID
		
END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetCommunities
EXEC Utility.DropObject 'dbo.GetCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.15
-- Description:	A stored procedure to get data from the dbo.Community table
-- ========================================================================
CREATE PROCEDURE dbo.GetCommunities

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
	ORDER BY P.ProvinceName, C.CommunityName

END
GO
--End procedure dbo.GetCommunities

--Begin procedure dbo.GetCommunityByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Community table
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID, 
		C.CommunityGroupID, 
		C.CommunityID, 
		C.CommunityName, 
		C.CommunitySubGroupID, 
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyPoints, 
		C.Latitude, 
		C.Longitude,
		C.PolicePresenceCategoryID, 
		C.PolicePresenceStatusID, 
		C.PoliceStationNumber, 
		C.PoliceStationStatusID, 
		C.Population, 
		C.PopulationSource, 
		C.ProgramNotes1, 
		C.ProgramNotes2, 
		C.ProgramNotes3, 
		C.ProgramNotes4, 
		C.ProgramNotes5, 
		C.ProvinceID,
		C.RiskMitigation, 
		C.StatusChangeID, 
		C.Summary, 
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CSG.CommunitySubGroupName,
		ID.ImpactDecisionName,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		PPS.PolicePresenceStatusName,
		PSS.PoliceStationStatusName,
		SC.StatusChangeName
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.PolicePresenceStatus PPS ON PPS.PolicePresenceStatusID = C.PolicePresenceStatusID
		JOIN dropdown.PoliceStationStatus PSS ON PSS.PoliceStationStatusID = C.PoliceStationStatusID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
			AND C.CommunityID = @CommunityID

	SELECT
		CECR.CommunityID, 
		CECR.EngagementCriteriaID, 
		CECR.EngagementCriteriaStatusID, 
		CECR.Notes,
		ECS.EngagementCriteriaStatusName,
		ECS.HexColor
	FROM dbo.CommunityEngagementCriteriaResult CECR
		JOIN dropdown.EngagementCriteriaStatus ECS ON ECS.EngagementCriteriaStatusID = CECR.EngagementCriteriaStatusID
			AND CECR.CommunityID = @CommunityID
	ORDER BY CECR.EngagementCriteriaID
	
END
GO
--End procedure dbo.GetCommunityByCommunityID

--Begin procedure dbo.GetCommunityLocations
EXEC Utility.DropObject 'dbo.GetCommunityLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to get location data from the dbo.Community table
-- =================================================================================
CREATE PROCEDURE dbo.GetCommunityLocations
@CommunityID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityID,
		C.CommunityName,
		C.Latitude,
		C.Longitude,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.ImpactDecisionName, 
		ID.ImpactDecisionID
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.ImpactDecision ID on ID.ImpactDecisionID = C.ImpactDecisionID
			AND 
				(
				@CommunityID = 0
					OR C.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR C.ProvinceID = @ProvinceID
				)
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure dbo.GetCommunityLocations

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
-- ======================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CN.ConceptNoteID,
		CN.Background,
		CN.Objectives,
		CN.ActivityCode,
		CN.CourseName,
		CN.Curriculum,
		CN.SponsorName,
		CN.Syllabus,
		CN.OtherDeliverable,
		CT.CourseTypeID,
		CT.CourseTypeName,
		LPT.LearnerProfileTypeID,
		LPT.LearnerProfileTypeName,
		PT.ProgramTypeID,
		PT.ProgramTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName
	FROM dbo.ConceptNote C
		JOIN dropdown.ConceptNoteType CT ON CT.ConceptNoteTypeID = C.ConceptNoteTypeID
		JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
		JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.ConceptNoteBudgetID,
		CNB.Cost,
		CNB.Notes
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		CNEC.Quantity * EC.UnitCost AS TotalCost
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN dbo.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetCourseByCourseID
EXEC Utility.DropObject 'dbo.GetCourseByCourseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Kevin Ross
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the dbo.Course table
-- =================================================================
CREATE PROCEDURE dbo.GetCourseByCourseID

@CourseID INT

AS
BEGIN
	SET NOCOUNT ON;

		SELECT 
			C.ActivityCode,
			C.CourseID,
			C.CourseName,
			C.CourseTypeID,
			C.Curriculum,
			C.LearnerProfileTypeID,
			C.ProgramTypeID,
			C.SponsorName,
			C.Syllabus,
			CT.CourseTypeID,
			CT.CourseTypeName,
			LPT.LearnerProfileTypeID,
			LPT.LearnerProfileTypeName,
			PT.ProgramTypeID,
			PT.ProgramTypeName,
			dbo.GetEntityTypeNameByEntityTypeCode('Course') AS EntityTypeName
		FROM dbo.Course C
			JOIN dropdown.CourseType CT ON CT.CourseTypeID = C.CourseTypeID
			JOIN dropdown.LearnerProfileType LPT ON LPT.LearnerProfileTypeID = C.LearnerProfileTypeID
			JOIN dropdown.ProgramType PT ON PT.ProgramTypeID = C.ProgramTypeID
		WHERE C.CourseID = @CourseID
		
END
GO
--End procedure dbo.GetCourseByCourseID

--Begin procedure dbo.GetCourses
EXEC Utility.DropObject 'dbo.GetCourses'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.17
-- Description:	A stored procedure to get data from the dbo.Course table
-- =====================================================================
CREATE PROCEDURE dbo.GetCourses

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CourseID,
		C.CourseName
	FROM dbo.Course C 
	ORDER BY C.CourseName, C.CourseID

END
GO
--End procedure dbo.GetCourses

--Begin procedure dbo.GetDailyReportByDailyReportID
EXEC Utility.DropObject 'dbo.GetDailyReportByDailyReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the dbo.DailyReport table
-- ======================================================================
CREATE PROCEDURE dbo.GetDailyReportByDailyReportID

@DailyReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DR.DailyReportID,
		DR.IsDutyOfCareComplete, 
		DR.Notes,
		DR.PersonID,
		dbo.GetPersonNameByPersonID(DR.PersonID, 'LastFirstTitle') AS FullName,
		DR.ReportDate,
		dbo.FormatDate(DR.ReportDate) AS ReportDateFormatted,
		(SELECT COUNT(DRC.DailyReportCommunityID) FROM dbo.DailyReportCommunity DRC JOIN dbo.Community C ON C.CommunityID = DRC.CommunityID AND DRC.DailyReportID = DR.DailyReportID) AS CommunityCount,
		DRT.DailyReportTypeID,
		DRT.DailyReportTypeName, 
		DRT.HasCommunities, 
		T.TeamID, 
		T.TeamName
	FROM dbo.DailyReport DR
		JOIN dropdown.DailyReportType DRT ON DRT.DailyReportTypeID = DR.DailyReportTypeID
		JOIN dbo.Team T ON T.TeamID = DR.TeamID
			AND DR.DailyReportID = @DailyReportID

	SELECT
		C.CommunityID, 
		C.CommunityName,
		DRC.Notes
	FROM dbo.DailyReportCommunity DRC
		JOIN dbo.Community C ON C.CommunityID = DRC.CommunityID
			AND DRC.DailyReportID = @DailyReportID
	ORDER BY C.CommunityName
	
END
GO
--End procedure dbo.GetDailyReportByDailyReportID

--Begin procedure dbo.GetDashboardItemCounts
EXEC Utility.DropObject 'dbo.GetDashboardItemCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.19
-- Description:	A stored procedure to get item count data for the dashboard
-- ========================================================================
CREATE PROCEDURE dbo.GetDashboardItemCounts

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		(SELECT COUNT(A.AtmosphericID) FROM dbo.Atmospheric A) AS AtmosphericCount,
		(SELECT COUNT(C.CommunityID) FROM dbo.Community C JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID) AS CommunityCount,
		(SELECT COUNT(KE.KeyEventID) FROM dbo.KeyEvent KE) AS KeyEventCount,
		(SELECT COUNT(KE.KeyEventID) FROM dbo.KeyEvent KE WHERE KE.IsCritical = 1) AS CriticalKeyEventCount

END
GO
--End procedure dbo.GetDashboardItemCounts

--Begin procedure dbo.GetDocumentByPhysicalFileName
EXEC Utility.DropObject 'dbo.GetDocumentByPhysicalFileName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dbo.Document table
-- ==========================================================================
CREATE PROCEDURE dbo.GetDocumentByPhysicalFileName

@PhysicalFileName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.DocumentID, 
		D.DocumentName
	FROM dbo.Document D
	WHERE D.PhysicalFileName = @PhysicalFileName

END
GO
--End procedure dbo.GetDocumentByPhysicalFileName

--Begin procedure dbo.GetEquipmentByEquipmentID
EXEC Utility.DropObject 'dbo.GetEquipmentByEquipmentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A stored procedure to data from the dbo.Equipment table
-- ====================================================================
CREATE PROCEDURE dbo.GetEquipmentByEquipmentID

@EquipmentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		E.BeneficiarySignatory,
		E.BudgetCode,
		E.Comments,
		E.EquipmentID,
		E.FinalBeneficiaryGroup,
		E.IMEIMACAddress,
		E.IssueDate,
		dbo.FormatDate(E.IssueDate) AS IssueDateFormatted,
		E.ItemCost,
		E.ItemDescription,
		E.ItemName,
		E.Location,
		E.Quantity,
		E.SerialNumber,
		E.SIM,
		E.SpecificLocation,
		E.Supplier
	FROM dbo.Equipment E
	WHERE E.EquipmentID = @EquipmentID

END
GO
--End procedure dbo.GetEquipmentByEquipmentID

--Begin procedure dbo.GetEquipmentFilters
EXEC Utility.DropObject 'dbo.GetEquipmentFilters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A stored procedure to return data from the dbo.Equipment table
-- ===========================================================================
CREATE PROCEDURE dbo.GetEquipmentFilters

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CASE
			WHEN D.FinalBeneficiaryGroup IS NULL
			THEN 'No Final Beneficiary Group Listed'
			ELSE D.FinalBeneficiaryGroup
		END AS FinalBeneficiaryGroup

	FROM (SELECT DISTINCT E.FinalBeneficiaryGroup FROM dbo.Equipment E) D
	ORDER BY D.FinalBeneficiaryGroup

	SELECT 
		CASE
			WHEN D.ItemName IS NULL
			THEN 'No Item Listed'
			ELSE D.ItemName
		END AS ItemName

	FROM (SELECT DISTINCT E.ItemName FROM dbo.Equipment E) D
	ORDER BY D.ItemName

	SELECT 
		CASE
			WHEN D.Location IS NULL
			THEN 'No Location Listed'
			ELSE D.Location
		END AS Location

	FROM (SELECT DISTINCT E.Location FROM dbo.Equipment E) D
	ORDER BY D.Location

END
GO
--End procedure dbo.GetEquipmentFilters

--Begin procedure dbo.GetEntityTypeDataByEntityTypeGroupCode
EXEC Utility.DropObject 'dbo.GetEntityTypeDataByEntityTypeGroupCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dbo.EntityType table
-- =================================================================================
CREATE PROCEDURE dbo.GetEntityTypeDataByEntityTypeGroupCode

@EntityTypeGroupCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EntityTypeID,
		EntityTypeCode,
		EntityTypeName,
		EntityTypeAbbreviation
	FROM dbo.EntityType ET
	WHERE ET.EntityTypeGroupCode = @EntityTypeGroupCode
	ORDER BY EntityTypeName, EntityTypeCode, EntityTypeID

END
GO
--End procedure dbo.GetEntityTypeDataByEntityTypeGroupCode

--Begin procedure dbo.GetKeyEventByKeyEventID
EXEC Utility.DropObject 'dbo.GetKeyEventByKeyEventID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.18
-- Description:	A stored procedure to data from the dbo.KeyEvent table
-- ===================================================================
CREATE PROCEDURE dbo.GetKeyEventByKeyEventID

@KeyEventID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CL.ConfidenceLevelName,
		KE.ConfidenceLevelID, 
		KE.HasPoliceInvolvement, 
		KE.Implications, 
		KE.Information, 
		KE.IsCritical, 
		KE.KeyEventCategoryID, 
		KE.KeyEventDate, 
		dbo.FormatDate(KE.KeyEventDate) AS KeyEventDateFormatted, 
		KE.KeyEventID, 
		KE.KeyEventReportedDate, 
		dbo.FormatDate(KE.KeyEventReportedDate) AS KeyEventReportedDateFormatted, 
		KE.KeyEventTypeDescription, 
		KE.KeyEventTypeID, 
		KE.PoliceInvolvementNotes, 
		KE.Recommendation, 
		KE.Resourcing, 
		KE.ResourcingStatus,
		KE.RiskMitigation, 
		KEC.KeyEventCategoryName,
		KET.KeyEventTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('KeyEvent') AS EntityTypeName
	FROM dbo.KeyEvent KE
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = KE.ConfidenceLevelID
		JOIN dropdown.KeyEventCategory KEC ON KEC.KeyEventCategoryID = KE.KeyEventCategoryID
		JOIN dropdown.KeyEventType KET ON KET.KeyEventTypeID = KE.KeyEventTypeID
			AND KE.KeyEventID = @KeyEventID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.KeyEventCommunity KEC
		JOIN dbo.Community C ON C.CommunityID = KEC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND KEC.KeyEventID = @KeyEventID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		KEECR.EngagementCriteriaID, 
		KEECR.EngagementCriteriaChoiceID
	FROM dbo.KeyEventEngagementCriteriaResult KEECR
	WHERE KEECR.KeyEventID = @KeyEventID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.KeyEventProvince KEP
		JOIN dbo.Province P ON P.ProvinceID = KEP.ProvinceID
			AND KEP.KeyEventID = @KeyEventID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		CL.ConfidenceLevelName,
		KES.ConfidenceLevelID,
		KES.KeyEventSourceID, 
		KES.SourceAttribution, 
		KES.SourceCategoryID, 
		KES.SourceDate, 
		dbo.FormatDate(KES.SourceDate) AS SourceDateFormatted, 
		KES.SourceName, 
		KES.SourceSummary, 
		KES.SourceTypeID, 
		SC.SourceCategoryName,
		ST.SourceTypeName
	FROM dbo.KeyEventSource KES
		JOIN dropdown.ConfidenceLevel CL ON CL.ConfidenceLevelID = KES.ConfidenceLevelID
		JOIN dropdown.SourceCategory SC ON SC.SourceCategoryID = KES.SourceCategoryID
		JOIN dropdown.SourceType ST ON ST.SourceTypeID = KES.SourceTypeID
			AND KES.KeyEventID = @KeyEventID
	ORDER BY KES.SourceName, KES.KeyEventSourceID

	SELECT
		D.DocumentName,
		D.PhysicalFileName
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON D.DocumentID = DE.DocumentID
			AND DE.EntityTypeCode = 'KeyEvent'
			AND DE.EntityID = @KeyEventID

END
GO
--End procedure dbo.GetKeyEventByKeyEventID

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return data from the dbo.Person table based on a PersonID
-- ============================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.FirstName, 
		P.LastName,
		P.Organization,
		P.PersonID,
		P.RoleID, 
		P.Title,
		P.UserName,
		R.RoleName
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.GetPersonEmailAddressesByRoleName
EXEC Utility.DropObject 'dbo.GetPersonEmailAddressesByRoleName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to return person email addresses from the dbo.Person table based on a role name
-- ===============================================================================================================
CREATE PROCEDURE dbo.GetPersonEmailAddressesByRoleName

@RoleName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND R.RoleName = @RoleName
	ORDER BY P.EmailAddress

END
GO
--End procedure dbo.GetPersonEmailAddressesByRoleName

--Begin procedure dbo.GetProvinceByProvinceID
EXEC Utility.DropObject 'dbo.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.14
-- Description:	A stored procedure to data from the dbo.Province table
-- ===================================================================
CREATE PROCEDURE dbo.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ImpactDecisionID,
		P.Implications,
		P.KeyPoints,
		P.ProgramNotes1,
		P.ProgramNotes2,
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID,
		P.Summary,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

	SELECT
		CES.CommunityEngagementStatusName,
		ISNULL(D.CommunityCount, 0) AS CommunityCount
	FROM dropdown.CommunityEngagementStatus CES
	OUTER APPLY
		(
		SELECT
			COUNT(C.CommunityID) AS CommunityCount,
			C.CommunityEngagementStatusID
		FROM dbo.Community C
		WHERE C.CommunityEngagementStatusID = CES.CommunityEngagementStatusID
			AND C.ProvinceID = @ProvinceID
		GROUP BY C.CommunityEngagementStatusID
		) D 
	WHERE CES.CommunityEngagementStatusID > 0
	ORDER BY CES.DisplayOrder, CES.CommunityEngagementStatusName, D.CommunityCount
	
END
GO
--End procedure dbo.GetProvinceByProvinceID

--Begin procedure dbo.GetProvinces
EXEC Utility.DropObject 'dbo.GetProvinces'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dbo.Province table
-- ==========================================================================
CREATE PROCEDURE dbo.GetProvinces

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
	ORDER BY P.ProvinceName, P.ProvinceID

END
GO
--End procedure dbo.GetProvinces

--Begin procedure dbo.GetTeamByTeamID
EXEC Utility.DropObject 'dbo.GetTeamByTeamID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return Team data
-- ===============================================================
CREATE PROCEDURE dbo.GetTeamByTeamID

@TeamID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TeamID,
		T.TeamName
	FROM dbo.Team T
	WHERE T.TeamID = @TeamID

	SELECT
		TM.TeamMemberID,
		TM.TeamID,
		TM.FirstName, 
		TM.LastName
	FROM dbo.TeamMember TM
	WHERE TM.TeamID = @TeamID
	ORDER BY TM.LastName, TM.FirstName, TM.TeamID

END
GO
--End procedure dbo.GetTeamByTeamID

--Begin procedure dbo.GetTeams
EXEC Utility.DropObject 'dbo.GetTeams'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.16
-- Description:	A stored procedure to get data from the dbo.Team table
-- ===================================================================
CREATE PROCEDURE dbo.GetTeams

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TeamID,
		T.TeamName
	FROM dbo.Team T
	ORDER BY T.TeamName, T.TeamID

END
GO
--End procedure dbo.GetTeams

--Begin procedure dbo.ValidateCommunityName
EXEC Utility.DropObject 'dbo.ValidateCommunityName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.17
-- Description:	A stored procedure to get data from the dbo.Community table
-- ========================================================================
CREATE PROCEDURE dbo.ValidateCommunityName

@CommunityName NVARCHAR(250),
@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.CommunityID) AS CommunityCount
	FROM dbo.Community C
	WHERE C.CommunityName = @CommunityName
		AND C.CommunityID <> @CommunityID

END
GO
--End procedure dbo.ValidateCommunityName