USE AJACS
GO

--Begin table dbo.Class (old table name:  class)
DECLARE @TableName VARCHAR(250) = 'dbo.Class'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Class
	(
	ClassID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	CourseID INT,
	StartDate DATE,
	EndDate DATE,
	Location VARCHAR(250),
	ClassPointOfContact VARCHAR(500),
	Instructor1 VARCHAR(100),
	Instructor2 VARCHAR(100),
	Instructor1Comments VARCHAR(MAX),
	Instructor2Comments VARCHAR(MAX),
	StudentFeedbackSummary VARCHAR(MAX),
	Seats INT,
	ExternalCapacity INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CourseID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ExternalCapacity', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Seats', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClassID'
GO

SET IDENTITY_INSERT dbo.Class ON
GO

INSERT INTO dbo.Class
	(ClassID, CommunityID, CourseID, StartDate, EndDate, Location, ClassPointOfContact, Instructor1, Instructor2, Instructor1Comments, Instructor2Comments, StudentFeedbackSummary, Seats, ExternalCapacity)
SELECT
	C.ClassID, 
	C.Community_ID, 
	C.CourseID, 
	C.StartDate, 
	C.EndDate, 
	C.Location, 
	C.ClassPointOfContact, 
	C.Instructor1, 
	C.Instructor2, 
	C.Instructor1Comments, 
	C.Instructor2Comments, 
	C.StudentFeedbackSummary, 
	C.Seats, 
	C.ExternalCapacity
FROM amnuna_db.dbo.class C
GO

SET IDENTITY_INSERT dbo.Class OFF
GO
--End table dbo.Class

--Begin table dbo.ClassStudent (old table name:  classstudent)
DECLARE @TableName VARCHAR(250) = 'dbo.ClassStudent'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ClassStudent
	(
	ClassStudentID INT IDENTITY(1,1) NOT NULL,
	ClassID INT,
	StudentID INT,
	StudentOutcomeTypeID INT,
	Comments VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StudentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StudentOutcomeTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClassStudentID'
EXEC utility.SetIndexClustered 'IX_ClassStudent', @TableName, 'ClassID,StudentID'
GO

INSERT INTO dbo.ClassStudent
	(ClassID, StudentID, StudentOutcomeTypeID, Comments)
SELECT
	CS.ClassID, 
	CS.StudentID, 
	CS.StudentOutcomeTypeID, 
	CS.Comments
FROM amnuna_db.dbo.classstudent CS
GO
--End table dbo.ClassStudent

--Begin table dbo.Course (old table name:  course)
DECLARE @TableName VARCHAR(250) = 'dbo.Course'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Course
	(
	CourseID INT IDENTITY(1,1) NOT NULL,
	ActivityCode VARCHAR(50),
	CourseName VARCHAR(200),
	Curriculum VARCHAR(MAX),
	CourseTypeID INT,
	LearnerProfileTypeID INT,
	ProgramTypeID INT,
	SponsorName VARCHAR(250),
	Syllabus VARCHAR(MAX)
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'CourseTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'LearnerProfileTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProgramTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CourseID'
EXEC utility.SetIndexNonClustered 'IX_Course', @TableName, 'CourseName'
GO

SET IDENTITY_INSERT dbo.Course ON
GO

INSERT INTO dbo.Course
	(CourseID, ActivityCode, CourseName, Curriculum, CourseTypeID, LearnerProfileTypeID, ProgramTypeID, SponsorName, Syllabus)
SELECT
	C.CourseID, 
	C.ActivityCode, 
	C.CourseName, 
	C.CourseDescription, 
	C.CourseTypeID, 
	C.LearnerProfileTypeID, 
	C.ProgramTypeID, 
	C.SponsorName, 
	C.Notes	
FROM amnuna_db.dbo.course C
GO

SET IDENTITY_INSERT dbo.Course OFF
GO
--End table dbo.Course

--Begin table dbo.Student (old table name:  student)
DECLARE @TableName VARCHAR(250) = 'dbo.Student'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Student
	(
	StudentID INT IDENTITY(1,1) NOT NULL,
	FullName VARCHAR(200),
	Address1 VARCHAR(200),
	City VARCHAR(200),
	Province VARCHAR(200),
	CellPhone VARCHAR(25),
	DateOfBirth DATE,
	GovernmentIDNumber VARCHAR(25),
	CurrentEmployer VARCHAR(200),
	ProjectTitle VARCHAR(50), 
	Rank VARCHAR(50),
	ApprovalStatus VARCHAR(50),
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'StudentID'
GO

SET IDENTITY_INSERT dbo.Student ON
GO

INSERT INTO dbo.Student
	(StudentID, FullName, Address1, City, Province, CellPhone, DateOfBirth, GovernmentIDNumber, CurrentEmployer, ProjectTitle, Rank, ApprovalStatus, CommunityID)
SELECT
	S.StudentID, 
	S.FullName, 
	S.Address1, 
	S.City, 
	S.Province, 
	S.CellPhone, 
	S.DateOfBirth, 
	S.GovernmentIDNumber, 
	S.CurrentEmployer, 
	S.ProjectTitle, 
	S.Rank, 
	S.ApprovalStatus, 
	S.Community_ID
FROM amnuna_db.dbo.student S
GO

SET IDENTITY_INSERT dbo.Student OFF
GO
--End table dbo.Student
