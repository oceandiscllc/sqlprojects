USE AJACS
GO

--Begin procedure dropdown.GetAtmosphericTypeData
EXEC Utility.DropObject 'dropdown.GetAtmosphericTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.AtmosphericType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetAtmosphericTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AtmosphericTypeID, 
		T.AtmosphericTypeName
	FROM dropdown.AtmosphericType T
	WHERE (T.AtmosphericTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AtmosphericTypeName, T.AtmosphericTypeID

END
GO
--End procedure dropdown.GetAtmosphericTypeData

--Begin procedure dropdown.GetBudgetTypeData
EXEC Utility.DropObject 'dropdown.GetBudgetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to return data from the dropdown.BudgetType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetBudgetTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.BudgetTypeID, 
		T.BudgetTypeName
	FROM dropdown.BudgetType T
	WHERE (T.BudgetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.BudgetTypeName, T.BudgetTypeID

END
GO
--End procedure dropdown.GetBudgetTypeData

--Begin procedure dropdown.GetCommunityEngagementStatusData
EXEC Utility.DropObject 'dropdown.GetCommunityEngagementStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.CommunityEngagementStatus table
-- ================================================================================================
CREATE PROCEDURE dropdown.GetCommunityEngagementStatusData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityEngagementStatusID, 
		T.CommunityEngagementStatusName
	FROM dropdown.CommunityEngagementStatus T
	WHERE (T.CommunityEngagementStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityEngagementStatusName, T.CommunityEngagementStatusID

END
GO
--End procedure dropdown.GetCommunityEngagementStatusData

--Begin procedure dropdown.GetCommunityGroupData
EXEC Utility.DropObject 'dropdown.GetCommunityGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.CommunityGroup table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetCommunityGroupData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityGroupID, 
		T.CommunityGroupName
	FROM dropdown.CommunityGroup T
	WHERE (T.CommunityGroupID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityGroupName, T.CommunityGroupID

END
GO
--End procedure dropdown.GetCommunityGroupData

--Begin procedure dropdown.GetCommunitySubGroupData
EXEC Utility.DropObject 'dropdown.GetCommunitySubGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.CommunitySubGroup table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetCommunitySubGroupData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunitySubGroupID, 
		T.CommunitySubGroupName
	FROM dropdown.CommunitySubGroup T
	WHERE (T.CommunitySubGroupID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunitySubGroupName, T.CommunitySubGroupID

END
GO
--End procedure dropdown.GetCommunitySubGroupData

--Begin procedure dropdown.GetConfidenceLevelData
EXEC Utility.DropObject 'dropdown.GetConfidenceLevelData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.ConfidenceLevel table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConfidenceLevelData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConfidenceLevelID, 
		T.ConfidenceLevelName
	FROM dropdown.ConfidenceLevel T
	WHERE (T.ConfidenceLevelID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConfidenceLevelName, T.ConfidenceLevelID

END
GO
--End procedure dropdown.GetConfidenceLevelData

--Begin procedure dropdown.GetCourseTypeData
EXEC Utility.DropObject 'dropdown.GetCourseTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.CourseType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetCourseTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CourseTypeID, 
		T.CourseTypeName
	FROM dropdown.CourseType T
	WHERE (T.CourseTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CourseTypeName, T.CourseTypeID

END
GO
--End procedure dropdown.GetCourseTypeData

--Begin procedure dropdown.GetDailyReportTypeData
EXEC Utility.DropObject 'dropdown.GetDailyReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.DailyReportType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetDailyReportTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DailyReportTypeID, 
		T.DailyReportTypeName,
		T.HasCommunities
	FROM dropdown.DailyReportType T
	WHERE (T.DailyReportTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DailyReportTypeName, T.DailyReportTypeID

END
GO
--End procedure dropdown.GetDailyReportTypeData

--Begin procedure dropdown.GetDateFilterData
EXEC Utility.DropObject 'dropdown.GetDateFilterData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.DateFilter table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetDateFilterData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DateFilterID, 
		T.DateFilterName
	FROM dropdown.DateFilter T
	WHERE (T.DateFilterID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DateFilterName, T.DateFilterID

END
GO
--End procedure dropdown.GetDateFilterData

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.20
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DocumentTypeID, 
		T.DocumentTypeName
	FROM dropdown.DocumentType T
	WHERE (T.DocumentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DocumentTypeName, T.DocumentTypeID

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetEngagementCriteriaData
EXEC Utility.DropObject 'dropdown.GetEngagementCriteriaData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.EngagementCriteria table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetEngagementCriteriaData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EngagementCriteriaID, 
		T.EngagementCriteriaName,
		T.EngagementCriteriaDescription
	FROM dropdown.EngagementCriteria T
	WHERE (T.EngagementCriteriaID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EngagementCriteriaName, T.EngagementCriteriaID

END
GO
--End procedure dropdown.GetEngagementCriteriaData

--Begin procedure dropdown.GetEngagementCriteriaChoiceData
EXEC Utility.DropObject 'dropdown.GetEngagementCriteriaChoiceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.EngagementCriteriaChoice table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetEngagementCriteriaChoiceData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EngagementCriteriaChoiceID, 
		T.EngagementCriteriaChoiceName
	FROM dropdown.EngagementCriteriaChoice T
	WHERE (T.EngagementCriteriaChoiceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EngagementCriteriaChoiceName, T.EngagementCriteriaChoiceID

END
GO
--End procedure dropdown.GetEngagementCriteriaChoiceData

--Begin procedure dropdown.GetEngagementCriteriaStatusData
EXEC Utility.DropObject 'dropdown.GetEngagementCriteriaStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.EngagementCriteriaStatus table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetEngagementCriteriaStatusData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EngagementCriteriaStatusID, 
		T.EngagementCriteriaStatusName,
		T.HexColor
	FROM dropdown.EngagementCriteriaStatus T
	WHERE (T.EngagementCriteriaStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EngagementCriteriaStatusName, T.EngagementCriteriaStatusID

END
GO
--End procedure dropdown.GetEngagementCriteriaStatusData

--Begin procedure dropdown.GetImpactDecisionData
EXEC Utility.DropObject 'dropdown.GetImpactDecisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.ImpactDecision table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetImpactDecisionData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImpactDecisionID, 
		T.ImpactDecisionName,
		T.HexColor
	FROM dropdown.ImpactDecision T
	WHERE (T.ImpactDecisionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImpactDecisionName, T.ImpactDecisionID

END
GO
--End procedure dropdown.GetImpactDecisionData

--Begin procedure dropdown.GetKeyEventCategoryData
EXEC Utility.DropObject 'dropdown.GetKeyEventCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.KeyEventCategory table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetKeyEventCategoryData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.KeyEventCategoryID, 
		T.KeyEventCategoryName
	FROM dropdown.KeyEventCategory T
	WHERE (T.KeyEventCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.KeyEventCategoryName, T.KeyEventCategoryID

END
GO
--End procedure dropdown.GetKeyEventCategoryData

--Begin procedure dropdown.GetKeyEventTypeData
EXEC Utility.DropObject 'dropdown.GetKeyEventTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.KeyEventType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetKeyEventTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		KEG.KeyEventGroupID,
		KEG.KeyEventGroupName,
		KET.KeyEventTypeID,
		KET.KeyEventTypeName
	FROM dropdown.KeyEventGroupKeyEventType A
		JOIN dropdown.KeyEventGroup KEG ON KEG.KeyEventGroupID = A.KeyEventGroupID
		JOIN dropdown.KeyEventType KET ON KET.KeyEventTypeID = A.KeyEventTypeID
	ORDER BY KEG.DisplayOrder, A.DisplayOrder

END
GO
--End procedure dropdown.GetKeyEventTypeData

--Begin procedure dropdown.GetLearnerProfileTypeData
EXEC Utility.DropObject 'dropdown.GetLearnerProfileTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.LearnerProfileType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetLearnerProfileTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LearnerProfileTypeID, 
		T.LearnerProfileTypeName
	FROM dropdown.LearnerProfileType T
	WHERE (T.LearnerProfileTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LearnerProfileTypeName, T.LearnerProfileTypeID

END
GO
--End procedure dropdown.GetLearnerProfileTypeData

--Begin procedure dropdown.GetPolicePresenceCategoryData
EXEC Utility.DropObject 'dropdown.GetPolicePresenceCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.PolicePresenceCategory table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetPolicePresenceCategoryData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PolicePresenceCategoryID, 
		T.PolicePresenceCategoryName
	FROM dropdown.PolicePresenceCategory T
	WHERE (T.PolicePresenceCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PolicePresenceCategoryName, T.PolicePresenceCategoryID

END
GO
--End procedure dropdown.GetPolicePresenceCategoryData

--Begin procedure dropdown.GetPolicePresenceStatusData
EXEC Utility.DropObject 'dropdown.GetPolicePresenceStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.PolicePresenceStatus table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetPolicePresenceStatusData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PolicePresenceStatusID, 
		T.PolicePresenceStatusName
	FROM dropdown.PolicePresenceStatus T
	WHERE (T.PolicePresenceStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PolicePresenceStatusName, T.PolicePresenceStatusID

END
GO
--End procedure dropdown.GetPolicePresenceStatusData

--Begin procedure dropdown.GetPoliceStationStatusData
EXEC Utility.DropObject 'dropdown.GetPoliceStationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.PoliceStationStatus table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetPoliceStationStatusData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PoliceStationStatusID, 
		T.PoliceStationStatusName
	FROM dropdown.PoliceStationStatus T
	WHERE (T.PoliceStationStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PoliceStationStatusName, T.PoliceStationStatusID

END
GO
--End procedure dropdown.GetPoliceStationStatusData

--Begin procedure dropdown.GetProgramTypeData
EXEC Utility.DropObject 'dropdown.GetProgramTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.ProgramType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetProgramTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProgramTypeID, 
		T.ProgramTypeName
	FROM dropdown.ProgramType T
	WHERE (T.ProgramTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProgramTypeName, T.ProgramTypeID

END
GO
--End procedure dropdown.GetProgramTypeData

--Begin procedure dropdown.GetRoleData
EXEC Utility.DropObject 'dropdown.GetRoleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.Role table
-- ===========================================================================
CREATE PROCEDURE dropdown.GetRoleData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RoleID, 
		T.RoleName
	FROM dropdown.Role T
	WHERE (T.RoleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RoleName, T.RoleID

END
GO
--End procedure dropdown.GetRoleData

--Begin procedure dropdown.GetSourceCategoryData
EXEC Utility.DropObject 'dropdown.GetSourceCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.SourceCategory table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetSourceCategoryData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceCategoryID, 
		T.SourceCategoryName
	FROM dropdown.SourceCategory T
	WHERE (T.SourceCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceCategoryName, T.SourceCategoryID

END
GO
--End procedure dropdown.GetSourceCategoryData

--Begin procedure dropdown.GetSourceTypeData
EXEC Utility.DropObject 'dropdown.GetSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.SourceType table
-- =================================================================================
CREATE PROCEDURE dropdown.GetSourceTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceTypeID, 
		T.SourceTypeName
	FROM dropdown.SourceType T
	WHERE (T.SourceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceTypeName, T.SourceTypeID

END
GO
--End procedure dropdown.GetSourceTypeData

--Begin procedure dropdown.GetStatusChangeData
EXEC Utility.DropObject 'dropdown.GetStatusChangeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.StatusChange table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetStatusChangeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusChangeID, 
		T.StatusChangeName,
		T.HexColor
	FROM dropdown.StatusChange T
	WHERE (T.StatusChangeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusChangeName, T.StatusChangeID

END
GO
--End procedure dropdown.GetStatusChangeData

--Begin procedure dropdown.GetStudentOutcomeTypeData
EXEC Utility.DropObject 'dropdown.GetStudentOutcomeTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.StudentOutcomeType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetStudentOutcomeTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StudentOutcomeTypeID, 
		T.StudentOutcomeTypeName
	FROM dropdown.StudentOutcomeType T
	WHERE (T.StudentOutcomeTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StudentOutcomeTypeName, T.StudentOutcomeTypeID

END
GO
--End procedure dropdown.GetStudentOutcomeTypeData