USE AJACS
GO

--Begin procedure dbo.GetCommunityMemberSurveyByCommunityMemberSurveyID
EXEC Utility.DropObject 'dbo.GetCommunityMemberSurveyByCommunityMemberSurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return CommunityMemberSurvey data
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityMemberSurveyByCommunityMemberSurveyID

@CommunityMemberSurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		CMS.CommunityMemberSurveyID,
		CMS.InterviewDateTime,
		dbo.FormatDateTime(CMS.InterviewDateTime) AS InterviewDateTimeFormatted,
		CMS.InterviewerName1,
		CMS.InterviewerName2,
		CMS.Question01,
		CMS.Question02,
		CMS.Question03,
		CMS.Question04,
		CMS.Question05,
		CMS.Question06,
		CMS.Question07,
		CMS.Question08,
		CMS.Question09,
		CMS.Question10,
		CMS.Question11,
		CMS.Question12,
		CMS.Question13,
		CMS.Question14,
		CMS.Question15,
		CMS.Question16,
		CMS.Question17,
		CMS.Question18,
		CMS.Question19,
		CMS.Question20,
		CMS.Question21,
		CMS.Question22,
		CMS.Question23,
		CMS.Question24,
		CMS.Question25,
		CMS.Question26,
		CMS.Question27,
		CMS.Question28,
		CMS.Question29,
		CMS.Question30,
		CMS.Question31,
		CMS.Question32,
		CMS.Question33,
		CMS.Question34,
		CMS.Question35,
		CMS.Question36,
		CMS.Question37,
		CMS.Question38,
		CMS.Question39,
		CMS.Question40,
		CMS.Question41,
		CMS.Question42,
		CMS.Question43,
		CMS.Question44,
		CMS.Question45,
		CMS.Question46,
		CMS.Question47,
		CMS.Question48,
		CMS.Question49,
		CMS.Question50,
		CMS.Question51,
		CMS.Question52,
		CMS.Question53,
		CMS.Question54,
		CMS.Question55,
		CMS.Question56,
		CMS.Question57,
		CMS.Question58,
		CMS.Question59,
		CMS.Question60,
		CMS.Question61,
		CMS.Question62,
		CMS.Question63,
		CMS.Question64,
		CMS.Question65,
		CMS.Question66,
		CMS.Question67,
		CMS.Question68,
		CMS.Question69,
		CMS.Question70,
		CMS.Question71,
		CMS.ReferenceCode,
		dbo.GetReferenceCodeByEntityTypeCode('CommunityMemberSurvey', CMS.CommunityID, CMS.CommunityMemberSurveyID, CMS.ReferenceCode) AS ReferenceCodeFormatted,
		CMS.SubjectAge,
		CMS.SubjectGender,
		CMS.SubjectPosition,
		dbo.GetEntityTypeNameByEntityTypeCode('CommunityMemberSurvey') AS EntityTypeName
	FROM dbo.CommunityMemberSurvey CMS
		JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID
			AND CMS.CommunityMemberSurveyID = @CommunityMemberSurveyID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityMemberSurvey'
			AND DE.EntityID = @CommunityMemberSurveyID

END
GO
--End procedure dbo.GetCommunityMemberSurveyByCommunityMemberSurveyID

--Begin procedure dbo.GetFocusGroupSurveyByFocusGroupSurveyID
EXEC Utility.DropObject 'dbo.GetFocusGroupSurveyByFocusGroupSurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return FocusGroupSurvey data
-- ===============================================================
CREATE PROCEDURE dbo.GetFocusGroupSurveyByFocusGroupSurveyID

@FocusGroupSurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		FGS.FocusGroupSurveyID,
		FGS.InterviewDateTime,
		dbo.FormatDateTime(FGS.InterviewDateTime) AS InterviewDateTimeFormatted,
		FGS.InterviewerName1,
		FGS.InterviewerName2,
		FGS.Notes,
		FGS.ReferenceCode,
		dbo.GetReferenceCodeByEntityTypeCode('FocusGroupSurvey', FGS.CommunityID, FGS.FocusGroupSurveyID, FGS.ReferenceCode) AS ReferenceCodeFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('FocusGroupSurvey') AS EntityTypeName
	FROM dbo.FocusGroupSurvey FGS
		JOIN dbo.Community C ON C.CommunityID = FGS.CommunityID
			AND FGS.FocusGroupSurveyID = @FocusGroupSurveyID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'FocusGroupSurvey'
			AND DE.EntityID = @FocusGroupSurveyID

	SELECT
		FGSP.FocusGroupSurveyID,
		FGSP.FocusGroupSurveyParticipantID,
		FGSP.SubjectAge,
		FGSP.SubjectGender,
		FGSP.SubjectPosition,
		FGSP.SubjectNotes
	FROM dbo.FocusGroupSurveyParticipant FGSP
	WHERE FGSP.FocusGroupSurveyID = @FocusGroupSurveyID
	ORDER BY FGSP.FocusGroupSurveyParticipantID

END
GO
--End procedure dbo.GetFocusGroupSurveyByFocusGroupSurveyID

--Begin procedure dbo.GetKeyInformantSurveyByKeyInformantSurveyID
EXEC Utility.DropObject 'dbo.GetKeyInformantSurveyByKeyInformantSurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return KeyInformantSurvey data
-- =================================================================
CREATE PROCEDURE dbo.GetKeyInformantSurveyByKeyInformantSurveyID

@KeyInformantSurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		KIS.KeyInformantSurveyID,
		KIS.InterviewDateTime,
		dbo.FormatDateTime(KIS.InterviewDateTime) AS InterviewDateTimeFormatted,
		KIS.InterviewerName1,
		KIS.InterviewerName2,
		KIS.Question01,
		KIS.Question02,
		KIS.Question03,
		KIS.Question04,
		KIS.Question05,
		KIS.Question06,
		KIS.Question07,
		KIS.Question08,
		KIS.Question09,
		KIS.Question10,
		KIS.Question11,
		KIS.Question12,
		KIS.Question13,
		KIS.Question14,
		KIS.Question15,
		KIS.Question16,
		KIS.Question17,
		KIS.ReferenceCode,
		dbo.GetReferenceCodeByEntityTypeCode('KeyInformantSurvey', KIS.CommunityID, KIS.KeyInformantSurveyID, KIS.ReferenceCode) AS ReferenceCodeFormatted,
		KIS.SubjectGender,
		KIS.SubjectName,
		KIS.SubjectPosition,
		dbo.GetEntityTypeNameByEntityTypeCode('KeyInformantSurvey') AS EntityTypeName
	FROM dbo.KeyInformantSurvey KIS
		JOIN dbo.Community C ON C.CommunityID = KIS.CommunityID
			AND KIS.KeyInformantSurveyID = @KeyInformantSurveyID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'KeyInformantSurvey'
			AND DE.EntityID = @KeyInformantSurveyID

END
GO
--End procedure dbo.GetKeyInformantSurveyByKeyInformantSurveyID

--Begin procedure dbo.GetRapidPerceptionSurveyByRapidPerceptionSurveyID
EXEC Utility.DropObject 'dbo.GetRapidPerceptionSurveyByRapidPerceptionSurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return RapidPerceptionSurvey data
-- ====================================================================
CREATE PROCEDURE dbo.GetRapidPerceptionSurveyByRapidPerceptionSurveyID

@RapidPerceptionSurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		RPS.InterviewDateTime,
		dbo.FormatDateTime(RPS.InterviewDateTime) AS InterviewDateTimeFormatted,
		RPS.InterviewerName1,
		RPS.InterviewerName2,
		RPS.InvitedToFocusGroup,
		RPS.Question01,
		RPS.Question02,
		RPS.Question03,
		RPS.Question04,
		RPS.Question05,
		RPS.Question06,
		RPS.Question07,
		RPS.Question08,
		RPS.Question09,
		RPS.Question10,
		RPS.Question11,
		RPS.Question12,
		RPS.Question13,
		RPS.Question14,
		RPS.Question15,
		RPS.Question16,
		RPS.Question17,
		RPS.Question18,
		RPS.Question19,
		RPS.Question20,
		RPS.Question21,
		RPS.Question22,
		RPS.Question23,
		RPS.Question24,
		RPS.Question25,
		RPS.Question26,
		RPS.Question27,
		RPS.Question28,
		RPS.Question29,
		RPS.Question30,
		RPS.Question31,
		RPS.Question32,
		RPS.Question33,
		RPS.RapidPerceptionSurveyID,
		RPS.ReferenceCode,
		dbo.GetReferenceCodeByEntityTypeCode('RapidPerceptionSurvey', RPS.CommunityID, RPS.RapidPerceptionSurveyID, RPS.ReferenceCode) AS ReferenceCodeFormatted,
		RPS.SubjectAge,
		RPS.SubjectGender,
		RPS.SubjectName,
		RPS.SubjectPosition,
		dbo.GetEntityTypeNameByEntityTypeCode('RapidPerceptionSurvey') AS EntityTypeName
	FROM dbo.RapidPerceptionSurvey RPS
		JOIN dbo.Community C ON C.CommunityID = RPS.CommunityID
			AND RPS.RapidPerceptionSurveyID = @RapidPerceptionSurveyID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'RapidPerceptionSurvey'
			AND DE.EntityID = @RapidPerceptionSurveyID

END
GO
--End procedure dbo.GetRapidPerceptionSurveyByRapidPerceptionSurveyID

--Begin procedure dbo.GetStakeholderGroupSurveyByStakeholderGroupSurveyID
EXEC Utility.DropObject 'dbo.GetStakeholderGroupSurveyByStakeholderGroupSurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return StakeholderGroupSurvey data
-- =====================================================================
CREATE PROCEDURE dbo.GetStakeholderGroupSurveyByStakeholderGroupSurveyID

@StakeholderGroupSurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		SGS.CommunityID,
		SGS.InterviewDateTime,
		dbo.FormatDateTime(SGS.InterviewDateTime) AS InterviewDateTimeFormatted,
		SGS.InterviewerName1,
		SGS.InterviewerName2,
		SGS.Question01,
		SGS.Question02,
		SGS.Question03,
		SGS.Question04,
		SGS.Question05,
		SGS.Question06,
		SGS.Question07,
		SGS.Question08,
		SGS.Question09,
		SGS.Question10,
		SGS.Question11,
		SGS.Question12,
		SGS.Question13,
		SGS.Question14,
		SGS.Question15,
		SGS.Question16,
		SGS.Question17,
		SGS.Question18,
		SGS.ReferenceCode,
		dbo.GetReferenceCodeByEntityTypeCode('StakeholderGroupSurvey', SGS.CommunityID, SGS.StakeholderGroupSurveyID, SGS.ReferenceCode) AS ReferenceCodeFormatted,
		SGS.StakeholderGroupSurveyID,
		SGS.SubjectName,
		dbo.GetEntityTypeNameByEntityTypeCode('StakeholderGroupSurvey') AS EntityTypeName
	FROM dbo.StakeholderGroupSurvey SGS
		JOIN dbo.Community C ON C.CommunityID = SGS.CommunityID
			AND SGS.StakeholderGroupSurveyID = @StakeholderGroupSurveyID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'StakeholderGroupSurvey'
			AND DE.EntityID = @StakeholderGroupSurveyID

END
GO
--End procedure dbo.GetStakeholderGroupSurveyByStakeholderGroupSurveyID

--Begin procedure dbo.GetStationCommanderSurveyByStationCommanderSurveyID
EXEC Utility.DropObject 'dbo.GetStationCommanderSurveyByStationCommanderSurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return StationCommanderSurvey data
-- =====================================================================
CREATE PROCEDURE dbo.GetStationCommanderSurveyByStationCommanderSurveyID

@StationCommanderSurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		SCS.InterviewDateTime,
		dbo.FormatDateTime(SCS.InterviewDateTime) AS InterviewDateTimeFormatted,
		SCS.InterviewerName1,
		SCS.InterviewerName2,
		SCS.Question01,
		SCS.Question02,
		SCS.Question03,
		SCS.Question04,
		SCS.Question05,
		SCS.Question06,
		SCS.Question07,
		SCS.Question08,
		SCS.Question09,
		SCS.Question10,
		SCS.Question11,
		SCS.Question12,
		SCS.Question13,
		SCS.Question14,
		SCS.Question15,
		SCS.Question16,
		SCS.Question17,
		SCS.Question18,
		SCS.ReferenceCode,
		dbo.GetReferenceCodeByEntityTypeCode('StationCommanderSurvey', SCS.CommunityID, SCS.StationCommanderSurveyID, SCS.ReferenceCode) AS ReferenceCodeFormatted,
		SCS.StationCommanderSurveyID,
		SCS.SubjectName,
		dbo.GetEntityTypeNameByEntityTypeCode('StationCommanderSurvey') AS EntityTypeName
	FROM dbo.StationCommanderSurvey SCS
		JOIN dbo.Community C ON C.CommunityID = SCS.CommunityID
			AND SCS.StationCommanderSurveyID = @StationCommanderSurveyID

	SELECT
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'StationCommanderSurvey'
			AND DE.EntityID = @StationCommanderSurveyID

END
GO
--End procedure dbo.GetStationCommanderSurveyByStationCommanderSurveyID
