USE AJACS
GO

SET IDENTITY_INSERT procurement.EquipmentCatalog ON 
GO

INSERT procurement.EquipmentCatalog 
	(EquipmentCatalogID, ItemName, EquipmentCatalogCategoryID, UnitOfIssue, UnitCost, Notes, IsCommon) 
VALUES 
	(1, 'Laptops with Arabic Keyboards (Lenovo Thinkpad Edge) with Software, such as Windows loaded', 0, 'pc', CAST(1300.00 AS Numeric(18, 2)), NULL, 1),
	(10, 'Video Camera - Camcorder Sony/Cannon HDR ', 0, 'pc', CAST(413.00 AS Numeric(18, 2)), 'must buy 32 GB memory card', 1),
	(11, 'Camera - Samsung Digital Photography Camera ', 0, 'pc', CAST(236.00 AS Numeric(18, 2)), 'must buy memory card', 1),
	(12, 'Camera Professional - DSLR', 0, 'pc', CAST(767.00 AS Numeric(18, 2)), 'must buy memory card', 1),
	(13, 'Memory Card 16 GB', 0, 'pc', CAST(23.60 AS Numeric(18, 2)), NULL, 1),
	(14, 'Memory Card 32 GB', 0, 'pc', CAST(35.40 AS Numeric(18, 2)), NULL, 1),
	(15, 'Projector - Epson', 0, 'pc', CAST(649.00 AS Numeric(18, 2)), NULL, 1),
	(16, 'Digital Recorder - SONY', 0, 'pc', CAST(76.70 AS Numeric(18, 2)), NULL, 1),
	(17, 'Printer - HP Portable Multifunction printer, or similar', 0, 'pc', CAST(359.90 AS Numeric(18, 2)), NULL, 1),
	(18, 'Ink cartridges for Multifunction printer', 0, 'pc', CAST(94.40 AS Numeric(18, 2)), 'typically buy 3 cartridges per printer', 1),
	(19, 'Laser Printer - Cannon', 0, 'pc', CAST(354.00 AS Numeric(18, 2)), NULL, 1),
	(2, 'Laptop Cases', 0, 'pc', CAST(47.20 AS Numeric(18, 2)), NULL, 1),
	(20, 'Ink Cartridges for laser printer', 0, 'pc', CAST(100.30 AS Numeric(18, 2)), 'typically buy 3 cartridges per printer', 1),
	(21, 'Field Medical Trauma Kits (Large)', 0, 'pc', CAST(1534.00 AS Numeric(18, 2)), NULL, 1),
	(22, 'Personal Medical Kit', 0, 'pc', CAST(251.34 AS Numeric(18, 2)), NULL, 1),
	(23, 'CPR Training Maniquin', 0, 'pc', CAST(472.00 AS Numeric(18, 2)), NULL, 1),
	(24, 'Alcohol Swab', 0, 'pc', CAST(0.06 AS Numeric(18, 2)), NULL, 1),
	(25, 'Fake Blood', 0, 'pc', CAST(0.09 AS Numeric(18, 2)), NULL, 1),
	(26, 'Tooway Satellite Internet System (cables, mounting pipe, activation)', 0, 'System', CAST(637.20 AS Numeric(18, 2)), 'you must select a service package and purchase a r', 1),
	(27, 'Tooway Satellite Internet Service 60 GB "Silver" Package', 0, '6 months', CAST(1195.32 AS Numeric(18, 2)), 'standard package', 1),
	(28, 'Tooway Satellite Internet Service (100 GB) ', 0, '6 months', CAST(2937.06 AS Numeric(18, 2)), NULL, 1),
	(29, 'TP Link Router', 0, 'pc', CAST(46.02 AS Numeric(18, 2)), NULL, 1),
	(3, 'Backpack - Black', 0, 'pc', CAST(59.00 AS Numeric(18, 2)), NULL, 1),
	(30, 'iDirect Satellite Internet Modem', 0, 'pc', CAST(1711.00 AS Numeric(18, 2)), 'These satellite internet systems are really only u', 1),
	(31, 'iDirect antenna', 0, 'pc', CAST(590.00 AS Numeric(18, 2)), 'the antenna is optional - if you don''t get the ant', 1),
	(32, 'iDirect Service - 6 Months 1024 Kbps download / 512 Kbps Upload (1:10) ', 0, '6 Months', CAST(6840.00 AS Numeric(18, 2)), 'you must also purchase a router', 1),
	(33, 'iDirect Horns', 0, 'pc', CAST(283.20 AS Numeric(18, 2)), NULL, 1),
	(34, 'Inmarsat IsatPhone Pro Satellite+ 500 units prepaid', 0, 'pc', CAST(1635.48 AS Numeric(18, 2)), NULL, 1),
	(35, 'Inmarsat IsatPhone Pro Satellite', 0, 'pc', CAST(975.86 AS Numeric(18, 2)), NULL, 1),
	(36, 'Inmarsat 500 Units Prepaid Card (valid for one year)', 0, 'pc', CAST(659.62 AS Numeric(18, 2)), NULL, 1),
	(37, 'Iridium Extreme 9575 SatPhone + 200 units credit', 0, 'pc', CAST(2530.00 AS Numeric(18, 2)), NULL, 1),
	(38, 'Iridium Extreme 9575 SatPhone', 0, 'pc', CAST(1870.30 AS Numeric(18, 2)), NULL, 1),
	(39, 'Iridium 200 Unit Prepaid Card (valid for one year)', 0, 'pc', CAST(659.62 AS Numeric(18, 2)), NULL, 1),
	(4, 'Samsung Galaxy SIII', 0, 'pc', CAST(289.10 AS Numeric(18, 2)), NULL, 1),
	(40, 'Generator - Diesel 2.3 Kva (23 Kg)', 0, 'pc', CAST(495.60 AS Numeric(18, 2)), NULL, 1),
	(41, 'Generators - Petrol  5.5 Kva (80 Kg)', 0, 'pc', CAST(804.54 AS Numeric(18, 2)), NULL, 1),
	(42, 'Solar Power Generator - Portable Ecoboxx ', 0, 'pc', CAST(300.00 AS Numeric(18, 2)), NULL, 1),
	(43, 'Solar Power Generation System (160W )', 0, 'pc', CAST(1872.66 AS Numeric(18, 2)), NULL, 1),
	(5, 'Samsung Galaxy S4 Mini', 0, 'pc', CAST(436.60 AS Numeric(18, 2)), NULL, 1),
	(6, 'Flash Drive - Kingston 32Gb USB ', 0, 'pc', CAST(23.60 AS Numeric(18, 2)), NULL, 1),
	(7, 'Secure Flash Drive', 0, 'pc', CAST(59.00 AS Numeric(18, 2)), NULL, 1),
	(8, 'Iron Key Flash Drive', 0, 'pc', CAST(225.38 AS Numeric(18, 2)), NULL, 1),
	(9, 'External Hard Drive - 1 TB Digital External HD (2.5) USB 2.0-3.0 ', 0, 'pc', CAST(118.00 AS Numeric(18, 2)), NULL, 1)
GO

SET IDENTITY_INSERT procurement.EquipmentCatalog OFF
GO

SET IDENTITY_INSERT procurement.License ON 
GO
INSERT procurement.License (LicenseID, LicenseNumber, StartDate, EndDate) VALUES (1, 'D528821', CAST(N'2014-04-01' AS Date), CAST(N'2016-03-31' AS Date))
INSERT procurement.License (LicenseID, LicenseNumber, StartDate, EndDate) VALUES (2, 'D1001935', CAST(N'2015-01-07' AS Date), CAST(N'2017-01-31' AS Date))
GO

SET IDENTITY_INSERT procurement.License OFF
GO

INSERT INTO procurement.LicenseEquipmentCatalog 
	(LicenseItemDescription,ECCN,QuantityAuthorized,QuantityDistributed,BudgetLimit)
VALUES
	('CD and MP3 Player','EAR99',1000,0,350000),
	('Package of Recordable CDs','EAR99',5000,0,150000),
	('Speakers','EAR99',1000,0,200000),
	('4 track digital recorder','EAR99',5000,0,1750000),
	('USB Mixer, 3 Band','5A991',1000,0,1500000),
	('Dual Channel Compressor','5A991',1000,0,200000),
	('FM Radio Tranmitter, 2.5-5KW','5A991',1000,0,2900000),
	('Headphone splitters','EAR99',1000,0,100000),
	('Headphones','EAR99',1000,0,30000),
	('Studio Headphones, Sennheiser HD380Pro or LIK','EAR99',1000,0,300000),
	('Angle Poise Microphone','EAR99',1000,0,80000),
	('Diaphragm Condenser Microphone','EAR99',1000,0,250000),
	('Dynamic Vocal Microphone','EAR99',1000,0,80000),
	('Microphone Stand','EAR99',1000,0,50000),
	('Camcorder, Sony HDR CX','EAR99',10000,0,7080000),
	('Digital Photo Camera 5x Optical','EAR99',10000,15,3090000),
	('Portable Projector','EAR99',5000,0,3000000),
	('Nikon Camera D3100 18-55mm or like item such as Cannon','EAR99',10000,15,10440000),
	('Hidden Button, Pen or Pin Camera','5A980',1000,0,50000),
	('Semi Professional Video Camera','EAR99',5000,0,37500000),
	('Wireless Microphone for Professional Video Camera','EAR99',5000,0,3700000),
	('Waterproof Backpacks','EAR99',5000,0,750000),
	('Laptop 15 Inch (Thinkpad Edge)','5A992',50000,30,57500000),
	('MacBook PRO 13 Inch','5A992',5000,0,11000000),
	('MacBook PRO 15 Inch','5A992',5000,0,13000000),
	('External Hard Drive no Encryption','EAR99',10000,15,1300000),
	('32GB USB Memory no Encryption','EAR99',50000,0,1500000),
	('Laptop/Notebook Bags (17inch)','EAR99',10000,0,400000),
	('TP Link Routers','5A992',50000,0,2300000),
	('Iron Key 32 GB USB','5A992',10000,0,1910000),
	('Android Tablet, Samsung Note 10.1','5A992',5000,0,4000000),
	('LCD Desktop Monitors','EAR99',1000,0,250000),
	('FX Series Computer','5A992',1000,0,1722000),
	('Laser Printers - Cannon laser printer  i-sensys MF4410 or Like ','EAR99',5000,15,1000000),
	('Ink Cartridges for Laser Printer ','EAR99',1000,15,85000),
	('Industrial Printer, Cannon Image Runner C7260I or like','EAR99',5000,0,550000),
	('Microsoft Office','5D992',60000,0,18000000),
	('Video Editing Software Adobe','5D992',1000,0,1800000),
	('BGAN HUGHES 9201, 492 KBS','5A992',5000,0,26930000),
	('Inmarsat Sat Phone Pro + service','5A992',10000,0,15530000),
	('Portable Radio 16 Channels','5A991',100000,0,20600000),
	('Samsung Galaxy Smartphone','5A992',50000,55,13500000),
	('Satellite Internet System (including Modem,Antenna, and service) or Like Item (iDirect, Astra or Tooway)','5A992',10000,0,94140000),
	('Solar Power Radio (hand crank)','5A991',100000,0,9000000),
	('iDirect Horn Antenna System','EAR99',10000,0,2830000),
	('Iridium Satphone','5A992',5000,0,14000000),
	('Advanced Smartphones (Galaxy S3)','5A992',50000,0,30000000),
	('Radio Repeater ','5A991',5000,0,16520000),
	('UHF/VHF Transceiver','5A991',5000,0,22500000),
	('Linked Repeaters','5A991',5000,0,24780000),
	('Medical Trauma Kits','EAR99',25000,0,22500000),
	('Personal Trauma Kit','EAR99',50000,0,15000000),
	('Generator 11 OHP Single Cylinder','2A994',10000,0,16000000),
	('Generator 9 OHP Single Cylinder','2A994',10000,0,10000000),
	('Portable Generator 4000 WATT 7OHP','2A994',10000,0,22000000),
	('Solar Power Generation System','EAR99',10000,0,20000000),
	('Portable Solar Charger','EAR99',5000,0,800000),
	('Rechargable NIMH Batteries','EAR99',1000,0,50000),
	('NIMH Batteries','EAR99',250,0,3000),
	('Combo packs of batteries','EAR99',5000,0,40000),
	('Uninterruptable power supply','EAR99',1000,0,500000),
	('Li-ion Batteries','EAR99',5000,0,2365000),
	('Rapid Battery Charger (for Icoms)','EAR99',5000,0,200000)
GO

UPDATE procurement.LicenseEquipmentCatalog SET EquipmentCatalogID = LicenseEquipmentCatalogID, LicenseID = (LicenseEquipmentCatalogID % 2) + 1
GO