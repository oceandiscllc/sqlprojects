USE AJACS
GO

--Begin table dbo.MenuItem (old table name:  menuitem)
DECLARE @TableName VARCHAR(250) = 'dbo.MenuItem'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.MenuItem
	(
	MenuItemID INT IDENTITY(1,1) NOT NULL,
	ParentMenuItemID INT,
	MenuItemCode VARCHAR(50),
	MenuItemText VARCHAR(250),
	MenuItemLink VARCHAR(500),
	DisplayOrder INT,
	Icon VARCHAR(50),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'ParentMenuItemID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'MenuItemID'
GO

INSERT INTO dbo.MenuItem 
	(MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder,Icon) 
VALUES 
	('Dashboard','Dashboard','/main',1,'fa fa-fw fa-dashboard'),
	('Province','Provinces','/province/list',2,'fa fa-fw fa-flag-o'),
	('Community','Communities','/community/list',3,'fa fa-fw fa-building'),
	('Atmospheric','General Atmospherics','/atmospheric/list',4,'fa fa-fw fa-cloud'),
	('KeyEvent','Key Events','/keyevent/list',5,'fa fa-fw fa-calendar'),
	('RAPDelivery','RAP Delivery',NULL,6,'fa fa-fw fa-paper-plane-o'),
	('RAPData','RAP Data',NULL,8,'fa fa-fw fa-bar-chart'),
	('Training','Training',NULL,9,'fa fa-fw fa-university'),
	('Document','Documents','/document/list',10,'fa fa-fw fa-tasks'),
	('Admin','Admin',NULL,11,'fa fa-fw fa-cogs'),
	('ConceptNote','Concept Notes','/conceptnote/list',12,'fa fa-fw fa-pencil-square-o'),
	('Procurement','Procurement',NULL,13,'fa fa-fw fa-table'),
	('ActivityManagement','Activity Management',NULL,14,'fa fa-fw fa-line-chart')
GO

INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'DailyReportList','View Daily Reports','/dailyreport/list',2 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPDelivery'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'TeamList','View Teams','/team/list',2 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPDelivery'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'RAPDataList','View RAP Data','/rapdata/list',1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPData'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'CommunityMemberSurveyAdd','Add Community Member Survey','/communitymembersurvey/addupdate/id/0',2 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPData'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'FocusGroupSurveyAdd','Add Focus Group Questionnaire','/focusgroupsurvey/addupdate/id/0',3 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPData'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'StationCommanderSurveyAdd','Add FSP Station Commander Survey','/stationcommandersurvey/addupdate/id/0',4 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPData'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'KeyInformantSurveyAdd','Add Key Informant Interview','/keyinformantsurvey/addupdate/id/0',5 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPData'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'StakeholderGroupSurveyAdd','Add Stakeholder Group Questionnaire','/stakeholdergroupsurvey/addupdate/id/0',6 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPData'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'CourseList','View Course Catalog','/course/list',3 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Training'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'ClassList','View Class Schedules','/class/list',4 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Training'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'ContactList','Contacts','/contact/list',1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Admin'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'PersonList','Users','/person/list',2 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Admin'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'WeeklyReport','Weekly Reports','/weeklyreport/addupdate',4 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Admin'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'LicenseList','Licenses','/license/list',1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Procurement'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'EquipmentCatalogList','Equipment Catalog','/equipmentcatalog/list',2 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Procurement'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'EquipmentInventoryList','Equipment Inventory','/equipment/list',3 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Procurement'
INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'LicenseEquipmentCatalogList','Licensed Equipment','/licenseequipmentcatalog/list',4 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Procurement'

--INSERT INTO dbo.MenuItem (ParentMenuItemID,MenuItemCode,MenuItemText,MenuItemLink,DisplayOrder) SELECT MI.MenuItemID, 'DocumentAdd','Upload Documents','/document/addupdate/id/0',3 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Document'
GO
--End table dbo.MenuItem

--Begin table dbo.PersonMenuItem (old table name:  usermenuitem)
DECLARE @TableName VARCHAR(250) = 'dbo.PersonMenuItem'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.PersonMenuItem
	(
	PersonMenuItemID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	MenuItemID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonMenuItemID'
EXEC utility.SetIndexClustered 'IX_PersonMenuItem', @TableName, 'PersonID,MenuItemID'
GO

INSERT INTO dbo.PersonMenuItem
	(PersonID, MenuItemID)
SELECT
	P.PersonID,
	MI.MenuItemID
FROM dbo.MenuItem MI, dbo.Person P
GO
--End table dbo.PersonMenuItem
--End Tables