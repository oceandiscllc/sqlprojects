USE AJACS
GO

--Begin table dbo.ConceptNoteBudget
ALTER TABLE AJACS.dbo.ConceptNoteBudget DROP CONSTRAINT DF_ConceptNoteBudget_Quantity;
ALTER TABLE AJACS.dbo.ConceptNoteBudget ALTER COLUMN Quantity NUMERIC(18,2);
ALTER TABLE AJACS.dbo.ConceptNoteBudget ADD CONSTRAINT DF_ConceptNoteBudget_Quantity DEFAULT 0 for Quantity
GO
--End table dbo.ConceptNoteBudget

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
GO
--End table dbo.Contact

--Begin table dbo.ContactVetting
DECLARE @TableName VARCHAR(250) = 'dbo.ContactVetting'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactVetting
	(
	ContactVettingID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	VettingOutcomeID INT,
	VettingDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'VettingOutcomeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactVettingID'
EXEC utility.SetIndexClustered 'IX_ContactVetting', @TableName, 'ContactID,VettingOutcomeID,VettingDate'
GO
--End table dbo.ContactVetting