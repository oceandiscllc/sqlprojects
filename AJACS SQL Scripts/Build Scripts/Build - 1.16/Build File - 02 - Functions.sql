USE AJACS
GO

--Begin function dbo.FormatContactNameByContactID
EXEC utility.DropObject 'dbo.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return the name of a contact in a specified format from a ContactID
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Add middle name support
-- ==============================================================================================

CREATE FUNCTION dbo.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName NVARCHAR(200)
	DECLARE @cMiddleName NVARCHAR(200)
	DECLARE @cLastName NVARCHAR(200)
	DECLARE @cTitle NVARCHAR(100)
	DECLARE @cRetVal VARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cMiddleName = ISNULL(C.MiddleName, ''),
			@cLastName = ISNULL(C.LastName, ''),
			@cTitle = ISNULL(C.Title, '')
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
	
		IF @Format = 'FirstLast' OR @Format = 'FirstMiddleLast' OR @Format = 'TitleFirstLast' OR @Format = 'TitleFirstMiddleLast'
			BEGIN
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal = @cFirstName + ' '
			--ENDIF

			IF (@Format = 'FirstMiddleLast' OR @Format = 'TitleFirstMiddleLast') AND LEN(RTRIM(@cMiddleName)) > 0
				SET @cRetVal += @cMiddleName + ' '
			--ENDIF
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal += @cLastName + ' '
			--ENDIF
			
			IF (@Format = 'TitleFirstLast' OR @Format = 'TitleFirstMiddleLast') AND LEN(RTRIM(@cTitle)) > 0
				SET @cRetVal = @cTitle + ' ' + @cRetVal
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstMiddle' OR @Format = 'LastFirstMiddleTitle' OR @Format = 'LastFirstTitle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				SET @cRetVal = @cLastName + ', '
			--ENDIF
			
			IF LEN(RTRIM(@cFirstName)) > 0
				SET @cRetVal += @cFirstName + ' '
			--ENDIF
	
			IF (@Format = 'LastFirstMiddle' OR @Format = 'LastFirstMiddleTitle') AND LEN(RTRIM(@cMiddleName)) > 0
				BEGIN
				
				SET @cRetVal += @cMiddleName + ' '
				IF @Format = 'LastFirstMiddleTitle' AND LEN(RTRIM(@cTitle)) > 0
					SET @cRetVal += @cTitle
				--ENDIF

				END
			--ENDIF
	
			IF @Format = 'LastFirstTitle' AND LEN(RTRIM(@cTitle)) > 0
				SET @cRetVal += @cTitle
			--ENDIF
			
			END
		--ENDIF

		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatContactNameByContactID

--Begin function eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	A function to return the last update date from the eventlogEventLog table for a specific EntityTypeCode and EntityID
-- =================================================================================================================================

CREATE FUNCTION eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT 
)

RETURNS VARCHAR(20)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(20)
	
	SELECT TOP 1
		@cRetVal = dbo.FormatDateTime(EL.CreateDateTime)
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = @EntityTypeCode
		AND EL.EntityID = @EntityID
		AND EL.EventCode = 'Update'
	ORDER BY EL.EventLogID DESC
	
	RETURN ISNULL(@cRetVal, '')

END
GO
--End function eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID