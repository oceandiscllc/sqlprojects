USE AJACS
GO

--Begin table dbo.ContactContactType
INSERT INTO dbo.ContactContactType
	(ContactID, ContactTypeID)
SELECT
	C.ContactID,
	(SELECT CT.ContactTypeID FROM dropdown.ContactType CT WHERE CT.ContactTypeCode = 'Beneficiary')
FROM dbo.Contact C
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.ContactContactType CCT
	WHERE CCT.ContactID = C.ContactID
	)
GO
--End table dbo.ContactContactType

--Begin table dbo.MenuItem
UPDATE MI
SET 
	MI.MenuItemCode = 'Contacts',
	MI.MenuItemText = 'Contacts'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'Beneficiaries'
GO

UPDATE MI
SET MI.MenuItemCode = 'BeneficiaryList'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'ContactList'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='BeneficiaryList', @NewMenuItemText='Beneficiaries', @NewMenuItemLink='/contact/list?ContactTypeCode=Beneficiary', @ParentMenuItemCode='Contacts', @PermissionableLineageList='Contact.List.Beneficiary'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerList', @NewMenuItemText='Staff &amp; Partners', @NewMenuItemLink='/contact/list?ContactTypeCode=StaffPartner', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='BeneficiaryList', @PermissionableLineageList='Contact.List.StaffPartner'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerVettingList', @NewMenuItemText='Staff &amp; Partner Vetting', @NewMenuItemLink='/contact/list?ContactTypeCode=StaffPartner&IsVetting=1', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='StaffPartnerList', @PermissionableLineageList='Contact.List.StaffPartnerVetting'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StipendList', @NewMenuItemText='Stipends', @NewMenuItemLink='/contact/list?ContactTypeCode=Stipend', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='StaffPartnerList', @PermissionableLineageList='Contact.List.Stipend'
GO
--End table dbo.MenuItem

--Begin table dropdown.RequestForInformationStatus
IF NOT EXISTS (SELECT 1 FROM dropdown.RequestForInformationStatus RFIS WHERE RFIS.RequestForInformationStatusCode = 'NoActionMandated')
	BEGIN
	
	INSERT INTO dropdown.RequestForInformationStatus
		(RequestForInformationStatusCode, RequestForInformationStatusName, DisplayOrder)
	VALUES
		('NoActionMandated', 'No Action Mandated', 4)
	
	END
--ENDIF
GO
--End table dropdown.RequestForInformationStatus

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Beneficiary',
	'Beneficiary',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.AddUpdate.Beneficiary'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StaffPartner',
	'Staff &amp; Partner',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.AddUpdate.StaffPartner'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Beneficiary',
	'Beneficiary',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.View.Beneficiary'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StaffPartner',
	'Staff &amp; Partner',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.View.StaffPartner'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Beneficiary',
	'Beneficiary',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.Beneficiary'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StaffPartner',
	'Staff &amp; Partner',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.StaffPartner'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StaffPartnerVetting',
	'Staff &amp; Partner Vetting',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.StaffPartnerVetting'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Stipend',
	'Stipend',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.AddUpdate.Stipend'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Stipend',
	'Stipend',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.View.Stipend'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Stipend',
	'Stipend',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.Stipend'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'IsActive',
	'Active / InActive',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.IsActive'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportVetting',
	'Export Vetting',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List.StaffPartnerVetting'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.StaffPartnerVetting.ExportVetting'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (17,26)

	END
--ENDIF
GO
--End table permissionable.PersonPermissionable

