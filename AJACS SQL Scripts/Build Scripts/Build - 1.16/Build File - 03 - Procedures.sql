USE AJACS
GO

--Begin procedure dbo.GetCommunityByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Community table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID, 
		C.CommunityGroupID, 
		C.CommunityID, 
		C.CommunityName, 
		C.CommunitySubGroupID, 
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyPoints, 
		C.Latitude, 
		C.Longitude,
		C.PolicePresenceCategoryID, 
		C.PolicePresenceStatusID, 
		C.PoliceStationNumber, 
		C.PoliceStationStatusID, 
		C.Population, 
		C.PopulationSource, 
		C.ProgramNotes1, 
		C.ProgramNotes2, 
		C.ProgramNotes3, 
		C.ProgramNotes4, 
		C.ProgramNotes5, 
		C.ProvinceID,
		C.RiskMitigation, 
		C.StatusChangeID, 
		C.Summary, 
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CSG.CommunitySubGroupName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Community', @CommunityID) AS UpdateDateFormatted,
		ID.ImpactDecisionName,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		PPS.PolicePresenceStatusName,
		PSS.PoliceStationStatusName,
		SC.StatusChangeName
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.PolicePresenceStatus PPS ON PPS.PolicePresenceStatusID = C.PolicePresenceStatusID
		JOIN dropdown.PoliceStationStatus PSS ON PSS.PoliceStationStatusID = C.PoliceStationStatusID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
			AND C.CommunityID = @CommunityID

	SELECT
		CECR.CommunityID, 
		CECR.EngagementCriteriaID, 
		CECR.EngagementCriteriaStatusID, 
		CECR.Notes,
		ECS.EngagementCriteriaStatusName,
		ECS.HexColor
	FROM dbo.CommunityEngagementCriteriaResult CECR
		JOIN dropdown.EngagementCriteriaStatus ECS ON ECS.EngagementCriteriaStatusID = CECR.EngagementCriteriaStatusID
			AND CECR.CommunityID = @CommunityID
	ORDER BY CECR.EngagementCriteriaID
	
END
GO
--End procedure dbo.GetCommunityByCommunityID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.LastName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-circle.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A stored procedure to get data from the dbo.Document table
--
-- Author:			Todd Pires
-- Create date:	2015.05.21
-- Description:	Modified to support weekly reports
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.ContentSubtype,
		D.ContentType
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID

	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure dbo.GetProvinceByProvinceID
EXEC Utility.DropObject 'dbo.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.14
-- Description:	A stored procedure to data from the dbo.Province table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
-- ===================================================================
CREATE PROCEDURE dbo.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ID.ImpactDecisionName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Province', @ProvinceID) AS UpdateDateFormatted,
		P.ImpactDecisionID,
		P.Implications,
		P.KeyPoints,
		P.ProgramNotes1,
		P.ProgramNotes2,
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID,
		P.Summary,
		SC.StatusChangeName
	FROM dbo.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

	SELECT
		CES.CommunityEngagementStatusName,
		ISNULL(D.CommunityCount, 0) AS CommunityCount
	FROM dropdown.CommunityEngagementStatus CES
	OUTER APPLY
		(
		SELECT
			COUNT(C.CommunityID) AS CommunityCount,
			C.CommunityEngagementStatusID
		FROM dbo.Community C
		WHERE C.CommunityEngagementStatusID = CES.CommunityEngagementStatusID
			AND C.ProvinceID = @ProvinceID
		GROUP BY C.CommunityEngagementStatusID
		) D 
	WHERE CES.CommunityEngagementStatusID > 0
	ORDER BY CES.DisplayOrder, CES.CommunityEngagementStatusName, D.CommunityCount
	
END
GO
--End procedure dbo.GetProvinceByProvinceID

--Begin procedure dropdown.GetSpotReportStatusData
EXEC Utility.DropObject 'dropdown.GetSpotReportStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	A stored procedure to return data from the workflow.WorkFlowStep table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSpotReportStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		WS.WorkFlowStepNumber,
		WS.WorkFlowStatusName
	FROM workflow.WorkFlowStep WS
		JOIN workflow.WorkFlow W ON W.WorkFlowID = WS.WorkFlowID
			AND W.EntityTypeCode = 'SpotReport'
	
	UNION
	
	SELECT
		W.WorkFlowStepCount + 1 AS WorkFlowStepNumber,
		'Approved' AS WorkFlowStatusName
	FROM workflow.WorkFlow W
	WHERE W.EntityTypeCode = 'SpotReport'
	
	ORDER BY 1, 2

END
GO
--End procedure dropdown.GetSpotReportStatusData

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Todd Pires
-- Create date: 2015.05.19
-- Description:	Added ContactType support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cContactContactAffiliations VARCHAR(MAX) 
	
		SELECT 
			@cContactContactAffiliations = COALESCE(@cContactContactAffiliations, '') + D.ContactContactAffiliation 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactContactAffiliation'), ELEMENTS) AS ContactContactAffiliation
			FROM dbo.ContactContactAffiliation T 
			WHERE T.ContactID = @EntityID
			) D

		DECLARE @cContactContactTypes VARCHAR(MAX) 
	
		SELECT 
			@cContactContactTypes = COALESCE(@cContactContactTypes, '') + D.ContactContactType 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactContactType'), ELEMENTS) AS ContactContactType
			FROM dbo.ContactContactType T 
			WHERE T.ContactID = @EntityID
			) D

		DECLARE @cContactContactVettings VARCHAR(MAX) 
	
		SELECT 
			@cContactContactVettings = COALESCE(@cContactContactVettings, '') + D.ContactContactVetting 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactContactVetting'), ELEMENTS) AS ContactContactVetting
			FROM dbo.ContactContactVetting T 
			WHERE T.ContactID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ContactContactAffiliations>' + ISNULL(@cContactContactAffiliations, '') + '</ContactContactAffiliations>') AS XML),
			CAST(('<ContactContactTypes>' + ISNULL(@cContactContactTypes, '') + '</ContactContactTypes>') AS XML),
			CAST(('<ContactContactVettings>' + ISNULL(@cContactContactVettings, '') + '</ContactContactVettings>') AS XML)
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM dbo.Contact T
		WHERE T.ContactID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
-- ==================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceID FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,	
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.FaceBookpageURL
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'ConceptNoteContact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		
END
GO
--End procedure reporting.GetContact