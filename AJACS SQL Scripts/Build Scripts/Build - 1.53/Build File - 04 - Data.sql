USE AJACS
GO

--Begin table dropdown.ContactType
UPDATE CT
SET CT.ContactTypeName = 'Sub-Contractor'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'SubContractors'
GO

UPDATE CT
SET CT.ContactTypeName = 'Stipend'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'PoliceStipend'
GO
--End table dropdown.ContactType
