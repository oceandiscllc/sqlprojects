USE AJACS
GO

--Begin procedure procurement.GetEquipmentDistributionByEquipmentDistributionID
EXEC Utility.DropObject 'procurement.GetEquipmentDistributionByEquipmentDistributionID'
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.03.08
-- Description:	A stored procedure to data from the procurement.EquipmentDistribution table
--
-- Author:			Todd Pires
-- Create date:	2016.03.08
-- Description:	Added document support
-- ========================================================================================
CREATE PROCEDURE procurement.GetEquipmentDistributionByEquipmentDistributionID

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ED.Aim,
		ED.Annexes,
		ED.CurrentSituation,
		ED.DeliveredToRecipientDate,
		dbo.FormatDate(ED.DeliveredToRecipientDate) AS DeliveredToRecipientDateFormatted,
		ED.Distribution,
		ED.EquipmentDistributionName,
		ED.Errata,
		ED.ErrataTitle,
		ED.ExportRoute,
		ED.IsActive,
		ED.OperationalResponsibility,
		ED.OperationalSecurity,
		ED.Phase1,
		ED.Phase2,
		ED.Phase3,
		ED.Phase4,
		ED.Phase5,
		ED.PlanOutline,
		ED.RecipientContactID,
		dbo.FormatContactNameByContactID(ED.RecipientContactID, 'LastFirst') AS RecipientContactNameFormatted,
		ED.Summary
	FROM procurement.EquipmentDistribution ED
	WHERE ED.EquipmentDistributionID = @EquipmentDistributionID

	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'EquipmentDistribution'
			AND DE.EntityID = @EquipmentDistributionID
	ORDER BY D.DocumentDescription
			
END
GO
--End procedure procurement.GetEquipmentDistributionByEquipmentDistributionID

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
--
-- Author:			Justin Branum
-- Create date: 2016.01.21
-- Description: Added New columns regrading Previous Unit data for vetting export report.
-- ==================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.City,
		C1.CommunityAssetID,
		C1.CommunityAssetUnitID,
		C1.CommunityID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', C1.CommunityID) AS CommunityName,
		C1.ContactID,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.DescriptionOfDuties,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsRegimeDefector,
		C1.LastName,
		C1.MiddleName,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.PassportNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.PreviousDuties,
		C1.PreviousProfession,
		C1.PreviousRankOrTitle,
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDate,
		dbo.FormatUSDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateUS,
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDate,
		dbo.FormatUSDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateUS,
		C1.PreviousUnit AS PreviousUnitLocation,
		C1.Profession,
		C1.ProvinceID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Province', C1.ProvinceID) AS ProvinceName,
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,
		dbo.FormatUSDate(C1.StartDate) AS JoinDateUSFormatted,
		C1.State,
		C1.Title,
		dbo.FormatDate(C1.UKVettingExpirationDate) AS UKVettingExpirationDateFormatted,
		dbo.FormatUSDate(C1.USVettingExpirationDate) AS USVettingExpirationDateFormatted,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		OACA.CommunityAssetName,
		OACA.TerritoryName,
		OAVO1.VettingOutcomeName AS USVettingOutcomeName,
		OAVO2.VettingOutcomeName AS UKVettingOutcomeName,
		P1.ProjectID,
		P1.ProjectName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		OUTER APPLY
			(
			SELECT 
				CA.CommunityAssetName,

				CASE
					WHEN CA.ProvinceID > 0
					THEN dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Province', CA.ProvinceID)
					WHEN CA.CommunityID > 0
					THEN dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', CA.CommunityID)
					ELSE ''
				END AS TerritoryName

			FROM dbo.CommunityAsset CA 
			WHERE CA.CommunityAssetID = C1.CommunityAssetID
			) OACA
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 1
			ORDER BY CV.ContactVettingID DESC
			) OAVO1
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 2
			ORDER BY CV.ContactVettingID DESC
			) OAVO2
	ORDER BY C1.ContactID
		
END
GO
--End procedure reporting.GetContact