USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'ActualOutput', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'IsEquipmentHandoverComplete', 'BIT'
EXEC utility.AddColumn @TableName, 'IsFinalPaymentMade', 'BIT'
EXEC utility.AddColumn @TableName, 'Summary', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'IsEquipmentHandoverComplete', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFinalPaymentMade', 'BIT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteContactEquipment
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContactEquipment'

EXEC utility.DropConstraintsAndIndexes @TableName

EXEC utility.DropColumn @TableName, 'EquipmentCatalogID'

EXEC utility.AddColumn @TableName, 'EquipmentInventoryID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteContactEquipmentID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteContactEquipment', @TableName, 'ConceptNoteID,ContactID,EquipmentInventoryID'
GO
--End table dbo.ConceptNoteContactEquipment

--Begin table dbo.ConceptNoteIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteIndicator'

EXEC utility.AddColumn @TableName, 'ActualQuantity', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ActualQuantity', 'INT', 0
GO
--End table dbo.ConceptNoteIndicator

--Begin table procurement.EquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentCatalog'

IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Dev')) = 'Prod'
	BEGIN

	EXEC utility.DropObject @TableName

	CREATE TABLE procurement.EquipmentCatalog
		(
		EquipmentCatalogID INT IDENTITY(1,1) NOT NULL,
		ItemName VARCHAR(250),
		ItemDescription VARCHAR(500),
		EquipmentCatalogCategoryID INT,
		UnitOfIssue VARCHAR(25),
		UnitCost NUMERIC(18,2),
		Notes VARCHAR(MAX),
		IsCommon BIT
		)

	EXEC utility.SetDefaultConstraint @TableName, 'EquipmentCatalogCategoryID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'IsCommon', 'BIT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'UnitCost', 'NUMERIC(18,2)', 0

	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentCatalogID'
	EXEC utility.SetIndexClustered 'IX_EquipmentCatalog', @TableName, 'ItemName,EquipmentCatalogCategoryID'

	END
--ENDIF
GO
--End table procurement.EquipmentCatalog

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.DropColumn @TableName, 'BeneficiarySignatory'
EXEC utility.DropColumn @TableName, 'FinalBeneficiaryGroup'
EXEC utility.DropColumn @TableName, 'IssueDate'
EXEC utility.DropColumn @TableName, 'ItemDescription'
EXEC utility.DropColumn @TableName, 'ItemName'
EXEC utility.DropColumn @TableName, 'Location'
EXEC utility.DropColumn @TableName, 'SpecificLocation'

EXEC utility.AddColumn @TableName, 'EquipmentCatalogID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentCatalogID', 'INT', 0
GO
--End table procurement.EquipmentInventory
