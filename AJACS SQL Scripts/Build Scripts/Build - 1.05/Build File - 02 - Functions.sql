USE AJACS
GO

--Begin function dbo.FormatContactNameByContactID
EXEC utility.DropObject 'dbo.FormatContactNameByContactID'
EXEC utility.DropObject 'dbo.FormatPersonNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A function to return the name of a contact in a specified format from a ContactID
-- ==============================================================================================

CREATE FUNCTION dbo.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cTitle VARCHAR(50)
	DECLARE @cRetVal VARCHAR(250)
	
	SET @cRetVal = ''
	
	IF @ContactID IS NOT NULL AND @ContactID > 0
		BEGIN
		
		SELECT
			@cFirstName = ISNULL(C.FirstName, ''),
			@cLastName = ISNULL(C.LastName, ''),
			@cTitle = ISNULL(C.Title, '')
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
	
		IF @Format = 'FirstLast' OR @Format = 'TitleFirstLast'
			BEGIN
			
			SET @cRetVal = @cFirstName + ' ' + @cLastName
	
			IF @Format = 'TitleFirstLast' AND LEN(RTRIM(@cTitle)) > 0
				BEGIN
				
				SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))
	
				END
			--ENDIF
			
			END
		--ENDIF
			
		IF @Format = 'LastFirst' OR @Format = 'LastFirstTitle'
			BEGIN
			
			IF LEN(RTRIM(@cLastName)) > 0
				BEGIN
				
				SET @cRetVal = @cLastName + ', '
	
				END
			--ENDIF
				
			SET @cRetVal = @cRetVal + @cFirstName + ' '
	
			IF @Format = 'LastFirstTitle' AND LEN(RTRIM(@cTitle)) > 0
				BEGIN
				
				SET @cRetVal = @cRetVal + @cTitle
	
				END
			--ENDIF
			
			END
		--ENDIF
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatContactNameByContactID
