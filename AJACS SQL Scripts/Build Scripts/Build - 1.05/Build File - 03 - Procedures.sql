USE AJACS
GO

--Begin procedure dbo.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteEquipmentCatalog table
-- ==========================================================================================
CREATE PROCEDURE dbo.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteBudgetByConceptNoteID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
-- ======================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.Summary,
		CN.Title,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CourseID,
		C.CourseName
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Community COM on COM.CommunityID = C.CommunityID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A stored procedure to get data from the dbo.ConceptNoteContactEquipment table
-- ==========================================================================================
CREATE PROCEDURE dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(D.ConceptNoteID) AS Title
	FROM
		(
		SELECT
			SUM(CNCE.Quantity) AS Quantity,
			CNCE.EquipmentInventoryID,
			CN.ConceptNoteID
		FROM dbo.ConceptNoteContactEquipment CNCE
			JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
			JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
				AND CN.ConceptNoteContactEquipmentDistributionDate IS NOT NULL
				AND 
					(
						(@EntityTypeCode = 'Community' AND C.CommunityID = @EntityID)
							OR (@EntityTypeCode = 'Province' AND C.ProvinceID = @EntityID)
					)
		GROUP BY CN.ConceptNoteID, CNCE.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = D.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID

--Begin procedure dbo.GetConceptNoteEquipmentByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteEquipmentCatalog table
-- ==========================================================================================
CREATE PROCEDURE dbo.GetConceptNoteEquipmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteEquipmentByConceptNoteID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
-- ==============================================================
CREATE PROCEDURE dbo.GetDonorFeed

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		MI.Icon,
		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE 'AJACS-WR-A' + RIGHT('0000' + CAST(D.EntityID AS VARCHAR(10)), 4) 
		END AS Title,

		D.EntityID,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
					AND D.EntityTypeCode = 'SpotReport'
			) OASR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
		JOIN [dbo].[MenuItem] MI ON MI.[MenuItemCode] = 
			CASE
				WHEN D.EntityTypeCode = 'WeeklyReport'
				THEN 'ManageWeeklyReports'
				ELSE D.EntityTypeCode
			END

			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
	ORDER BY D.UpdateDate DESC, D.EntityTypeCode, D.EntityID
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure logicalframework.GetIndicatorByObjectiveID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the logicalframework.Indicator table
-- =====================================================================================
CREATE PROCEDURE logicalframework.GetIndicatorByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		I.AchievedValue,
		I.BaselineDate,
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		I.BaselineValue,
		I.IndicatorDescription,
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorSource,
		I.TargetDate,
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		I.TargetValue,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName
  FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
			AND I.ObjectiveID = @ObjectiveID
	ORDER BY I.IndicatorID
	
END
GO
--End procedure logicalframework.GetIndicatorByObjectiveID

--Begin procedure logicalframework.GetMilestoneByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetMilestoneByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		M.AchievedDate,
		dbo.FormatDate(M.AchievedDate) AS AchievedDateFormatted,
		M.AchievedValue, 	
		M.MilestoneID, 	
		M.MilestoneName, 	
		M.TargetDate, 	
		dbo.FormatDate(M.TargetDate) AS TargetDateFormatted,
		M.TargetValue, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Milestone') AS EntityTypeName
	FROM logicalframework.Milestone M
	WHERE M.IndicatorID = @IndicatorID
	ORDER BY M.MilestoneName, M.MilestoneID
	
END
GO
--End procedure logicalframework.GetMilestoneByIndicatorID

--Begin procedure logicalframework.GetObjectiveByParentObjectiveID
EXEC Utility.DropObject 'logicalframework.GetObjectiveByParentObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.30
-- Description:	A stored procedure to return Objective data
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectiveByParentObjectiveID

@ParentObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM logicalframework.Objective O
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
	WHERE O.ParentObjectiveID = @ParentObjectiveID
	ORDER BY O.ObjectiveName, O.ObjectiveID

END
GO
--End procedure logicalframework.GetObjectiveByParentObjectiveID

--Begin procedure procurement.GetConceptNoteContactEquipmentByConceptNoteID
EXEC Utility.DropObject 'procurement.GetConceptNoteContactEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to get data from the procurement.ConceptNoteContactEquipment table
-- ==================================================================================================
CREATE PROCEDURE procurement.GetConceptNoteContactEquipmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatPersonName(C.FirstName, C.LastName, NULL, 'LastFirst') AS FullName,
		CNCE.Quantity,
		EC.ItemName,
		EI.SerialNumber
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CNCE.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND CNCE.ConceptNoteID = @ConceptNoteID
	ORDER BY FullName, ItemName
	
END
GO
--End procedure procurement.GetConceptNoteContactEquipmentByConceptNoteID

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
-- =====================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.IMEIMACAddress,
		EI.UnitCost,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure procurement.GetEquipmentInventoryFilters
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryFilters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A stored procedure to return data from the procurement.EquipmentInventory table
-- ====================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryFilters

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CASE
			WHEN D.ItemName IS NULL
			THEN 'No Item Listed'
			ELSE D.ItemName
		END AS ItemName

	FROM (SELECT DISTINCT EC.ItemName FROM procurement.EquipmentCatalog EC) D
	ORDER BY D.ItemName

END
GO
--End procedure procurement.GetEquipmentInventoryFilters

--Begin procedure reporting.GetConceptNoteActivityLocationsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteActivityLocationsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteActivityLocationsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cResult VARCHAR(MAX) = ' '
	
	SELECT @cResult = @cResult + CommunityName + N' ,'
	FROM dbo.ConceptNoteCommunity CNC
		JOIN dbo.Community P ON P.CommunityID = CNC.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT @cResult = @cResult + ProvinceName + N' ,'
	FROM dbo.ConceptNoteProvince CNP
		JOIN dbo.Province P ON P.ProvinceID = CNP.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
	
	IF LEN(RTRIM(@cResult)) = 0 
		SELECT @cResult AS ActivityLocations
	ELSE
		SELECT SUBSTRING(@cResult, 0, LEN(@cResult) -1) AS ActivityLocations
	--ENDIF

END
GO
--End procedure reporting.GetConceptNoteActivityLocationsByConceptNoteID

--Begin procedure reporting.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'reporting.GeConceptNoteBudgetByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CNBD AS
		(
		SELECT			
			BT.BudgetTypeName,
			CNB.ItemName,
			CNB.Quantity,
			CNB.UnitCost,
			CNB.Quantity * CNB.UnitCost AS TotalCost
		FROM dbo.ConceptNoteBudget CNB
			JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
				AND CNB.ConceptNoteID = @ConceptNoteID
				
		UNION ALL
		
		SELECT
			'Equipment' AS BudgetTypeName,
			EC.ItemName, 
			CNEC.Quantity,
			EC.UnitCost,
			CNEC.Quantity * EC.UnitCost AS TotalCost
		 FROM dbo.ConceptNoteEquipmentCatalog CNEC
			 JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID 
				AND CNEC.ConceptNoteID = @ConceptNoteID
		)

	SELECT 
		CNBD.BudgetTypeName,
		CNBD.ItemName, 
		CNBD.Quantity,
		CNBD.UnitCost,
		CNBD.TotalCost,
		(SELECT SUM(CNBD.TotalCost) FROM CNBD) AS GrandTotalCost 
	FROM CNBD
	ORDER BY CNBD.BudgetTypeName, CNBD.ItemName

END
GO
--End procedure reporting.GetConceptNoteBudgetByConceptNoteID

--Begin procedure reporting.GetConceptNoteClassByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteClassesByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteClassByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			John Lyons
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data for the Concept Note report
-- ============================================================================
CREATE PROCEDURE reporting.GetConceptNoteClassByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CLS.ClassID,
		CLS.EndDate,
		dbo.FormatDate(CLS.EndDate) AS EndDateFormatted,
		CLS.Location,
		CLS.StartDate,
		dbo.FormatDate(CLS.StartDate) AS StartDateFormatted,
		CRS.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class CLS ON CLS.ClassID = CNC.ClassID
		JOIN dbo.Community COM on COM.CommunityID = CLS.CommunityID
		JOIN dbo.Course CRS ON CRS.CourseID = CLS.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY CRS.CourseName

END
GO
--End procedure reporting.GetConceptNoteClassByConceptNoteID

--Begin procedure reporting.GetConceptNoteClassLocationsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteClassLocationsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteClassLocationsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cResult VARCHAR(MAX) = ' '
	
	SELECT @cResult = @cResult + Location + N' ,'
	FROM dbo.ConceptNoteClass CC
		JOIN dbo.Class C ON C.ClassID = CC.ClassID
			AND CC.ConceptNoteID = @ConceptNoteID
	
	IF LEN(RTRIM(@cResult)) = 0 
		SELECT @cResult AS Locations
	ELSE
		SELECT SUBSTRING(@cResult, 0, LEN(@cResult) -1) AS Locations
	--ENDIF

END
GO
--End procedure reporting.GetConceptNoteClassLocationsByConceptNoteID

--Begin procedure reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.31
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID

@ConceptNoteID INT,
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNCE.Quantity,
		EC.ItemName,
		ISNULL(EI.SerialNumber, 'None') AS SerialNumber
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CNCE.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND CNCE.ConceptNoteID = @ConceptNoteID
			AND CNCE.ContactID = @ContactID
	ORDER BY EC.ItemName, EI.SerialNumber, CNCE.Quantity

END
GO
--End procedure reporting.GetConceptNoteContactEquipmentByConceptNoteIDAndContactID

--Begin procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.31
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE
			WHEN CON.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CON.CommunityID)
			WHEN CON.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CON.ProvinceID)
			ELSE ''
		END AS Location,
			
		CON.EmployerName,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(CNCE.ConceptNoteID) AS ConceptNoteTitle,
		dbo.FormatConceptNoteReferenceCode(CNCE.ConceptNoteID) AS ConceptNoteReferenceCode,
		dbo.FormatContactNameByContactID(CNCE.ContactID, 'LastFirst') AS Fullname
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
		JOIN dbo.Contact CON ON CON.ContactID = CNCE.ContactID
			AND CNCE.ConceptNoteID = @ConceptNoteID
	ORDER BY Fullname, CNCE.ContactID

END
GO
--End procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

--Begin procedure reporting.GetConceptNoteCourseByConceptNoteID
EXEC Utility.DropObject 'reporting.GeConceptNoteCourseByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteCourseByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteCoursesByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			John Lyons
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data for a concept note
-- ==============================================================
CREATE PROCEDURE reporting.GetConceptNoteCourseByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CourseID,
		C.CourseName,
		C.Syllabus,
		C.Curriculum
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

END
GO
--End procedure reporting.GetConceptNoteCourseByConceptNoteID

--Begin procedure reporting.GetConceptNoteDemographicsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteDemographicsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteDemographicsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1), AgeRange VARCHAR(50))
	
	INSERT INTO @tTable
		(AgeRange)
	VALUES
		('Men (18+)'),
		('Women (18+)'),
		('Youth (Boys < 18)'),
		('Youth (Girls < 18)'), 
		('TOTAL')
	
	;
	WITH CD AS
		(
		SELECT
			COUNT(D.DisplayOrder) AS ContactCount,
			D.DisplayOrder
		FROM
			(
			SELECT
	
				CASE
					WHEN DATEDIFF(year, C.DateOfBirth, getDate()) > 18 AND Gender = 'Male'
					THEN 1
					WHEN DATEDIFF(year, C.DateOfBirth, getDate()) > 18 AND Gender = 'Female'
					THEN 2
					WHEN DATEDIFF(year, C.DateOfBirth, getDate()) <= 18 AND Gender = 'Male'
					THEN 3
					WHEN DATEDIFF(year, C.DateOfBirth, getDate()) <= 18 AND Gender = 'Female'
					THEN 4
				END AS DisplayOrder
	
			FROM dbo.Contact C
				JOIN dbo.ConceptNoteContact CNC ON CNC.ContactID = C.ContactID
					AND CNC.ConceptNoteID = @ConceptNoteID
			) D
		GROUP BY D.DisplayOrder
	
		UNION
	
		SELECT
			COUNT(CNC.ConceptNoteContactID),
			5
		FROM dbo.ConceptNoteContact CNC 
		WHERE CNC.ConceptNoteID = @ConceptNoteID
		)
	
	SELECT 
		T.AgeRange,
		CD.ContactCount,
		T.DisplayOrder
	FROM @tTable T
		JOIN CD ON CD.DisplayOrder = T.DisplayOrder
	ORDER BY T.DisplayOrder asc

END
GO
--End procedure reporting.GetConceptNoteDemographicsByConceptNoteID

--Begin procedure reporting.GetConceptNoteIndicatorByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteIndicatorByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteIndicatorByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		I.AchievedValue, 	
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorName + '<br>' + I.IndicatorDescription as FullLabel,	
		I.IndicatorSource, 	
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 	
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName,
		CNI.Comments
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
			AND CNI.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = I.IndicatorID
	ORDER BY I.IndicatorName, I.IndicatorID

END
GO
--End procedure reporting.GetConceptNoteIndicatorByConceptNoteID

--Begin procedure reporting.GetConceptNotePOCByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNotePOCByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNotePOCByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatPersonNameByPersonID(P.PersonID, 'LastFirstTitle') AS FullName,
		P.EmailAddress
	FROM dbo.Person P
	WHERE EXISTS
		(
		SELECT 1
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID
			AND 
				(
				CN.PointOfContactPersonID1 = P.PersonID
					OR CN.PointOfContactPersonID2 = P.PersonID
				)
		)
	ORDER BY FullName

END
GO
--End procedure reporting.GetConceptNotePOCByConceptNoteID

--Begin procedure reporting.GetConceptNoteTasksByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteTasksByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteTasksByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.ConceptNoteTaskName,
		CNT1.ConceptNoteTaskDescription,
		ISNULL(D.SubContractorName, 'none') AS SubContractorName,
		ISNULL(E.ConceptNoteTaskName, 'none') AS ParentConceptNoteTaskName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
			(
			SELECT
				SC.SubContractorID,
				SC.SubContractorName
			FROM dbo.SubContractor SC
			WHERE SC.SubContractorID = CNT1.SubContractorID
			) D 
		OUTER APPLY
			(
			SELECT
				CNT2.ConceptNoteTaskName
			FROM dbo.ConceptNoteTask CNT2
			WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			) E
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.Startdate, CNT1.EndDate, CNT1.ConceptNoteTaskName

END
GO
--End procedure reporting.GetConceptNoteTasksByConceptNoteID

--Begin procedure reporting.GetLicenseEquipmentReportByDateRange
EXEC Utility.DropObject 'reporting.GetLicenseEquipmentReportByDateRange'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date:	2015.03.28
-- Description:	A stored procedure to get license equipment data
-- =============================================================
CREATE PROCEDURE reporting.GetLicenseEquipmentReportByDateRange

@StartDate DATE,
@EndDate DATE

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ConceptNoteReferenceCode,
		LEC.ReferenceCode,
		EC.ItemName,
		LEC.ECCN,
		L.LicenseNumber,
		CNCE.Quantity,
		EI.UnitCost,
		EI.UnitCost * CNCE.Quantity AS TotalCost,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatContactNameByContactID(CNCE.ContactID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CNCE.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN procurement.LicenseEquipmentCatalog LEC ON LEC.EquipmentCatalogID = EC.EquipmentCatalogID
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND CN.ConceptNoteContactEquipmentDistributionDate BETWEEN @StartDate AND @EndDate
	ORDER BY 1, 9, 10

END
GO
--End procedure reporting.GetLicenseEquipmentReportByDateRange

