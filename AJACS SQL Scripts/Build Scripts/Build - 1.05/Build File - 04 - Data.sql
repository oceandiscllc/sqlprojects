USE AJACS
GO

--Begin table dropdown.DocumentType
UPDATE dropdown.DocumentType
SET DocumentTypeName = 'Weekly Atmospheric Report'
WHERE DocumentTypeCode = 'WeeklyReport'
GO
--End table dropdown.DocumentType

--Begin table workflow.WorkflowStep
UPDATE workflow.WorkflowStep
SET WorkflowStepName = 'Activity Completion'
WHERE WorkflowStepNumber = 9
GO
--End table workflow.WorkflowStep

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'MonitoringFinalReport')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode,DocumentTypeName,IsActive)
	VALUES
		('MonitoringFinalReport', 'Monitoring & Evaluation Final Report', 0),
		('VendorFinalReport', 'Final Vendor/Resource Partner Report', 0)

	END
--ENDIF
--End table dropdown.DocumentType

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Monitoring')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Monitoring','Monitoring & Evaluation', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Exports')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(PermissionableCode, PermissionableName, DisplayOrder)
	VALUES
		('Exports','Exports', 0)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'BusinessLicenseReport',
		'Business License Report',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'Exports'

	END
--ENDIF
GO

DELETE P FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'MonitoringOverview'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Monitoring')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(PermissionableCode, PermissionableName, DisplayOrder)
	VALUES
		('Monitoring','Monitoring & Evaluation Overview', 0)

	END
--ENDIF
GO
--End table permissionable.Permissionable
	
--Begin table permissionable.DisplayGroupPermissionable
UPDATE DGP
SET DGP.DisplayGroupID = (SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Monitoring')
FROM permissionable.DisplayGroupPermissionable DGP
	JOIN permissionable.Permissionable P ON P.PermissionableID = DGP.PermissionableID
		AND P.PermissionableCode IN ('Indicator','IndicatorType','Milestone','Objective')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'ObservationReport'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Exports')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

DELETE DGP FROM permissionable.DisplayGroupPermissionable DGP JOIN permissionable.Permissionable P ON P.PermissionableID = DGP.PermissionableID AND P.PermissionableCode = 'MonitoringOverview'
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Monitoring'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'Monitoring'
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table dbo.Menuitem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='MonitoringOverview', @NewMenuItemLink='/monitoring/overview', @NewMenuItemText='Overview', @ParentMenuItemCode='LogicalFramework', @BeforeMenuItemCode='ObjectiveAdd', @PermissionableLineageList='Monitoring'
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.MenuItemCode = 'Monitoring'
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'MonitoringOverview'),
	MIPL.PermissionableLineage
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.ParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'LogicalFramework')
GO

--Begin Permissions Assignment
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (69,17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (69,17,26)

	END
--ENDIF
GO
--End Permissions Assignment