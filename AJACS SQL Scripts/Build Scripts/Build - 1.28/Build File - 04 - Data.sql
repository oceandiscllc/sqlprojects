USE AJACS
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveOverview', @NewMenuItemLink='/objective/chartlist', @BeforeMenuItemCode='ObjectiveList', @PermissionableLineageList='Objective.ChartList'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveManage', @NewMenuItemLink='/objective/manage', @NewMenuItemText='Manage', @ParentMenuItemCode='LogicalFramework', @BeforeMenuItemCode='ObjectiveList', @PermissionableLineageList='Objective.AddUpdate'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
--End table dbo.MenuItem

--Begin table dropdown.EquipmentStatus
IF NOT EXISTS (SELECT 1 FROM dropdown.EquipmentStatus ES WHERE ES.EquipmentStatusName = 'Consumable')
	BEGIN
	
	UPDATE ES
	SET ES.DisplayOrder = 2
	FROM dropdown.EquipmentStatus ES
	WHERE ES.EquipmentStatusName  = 'In Service'
	
	UPDATE ES
	SET ES.DisplayOrder = 3
	FROM dropdown.EquipmentStatus ES
	WHERE ES.EquipmentStatusName  = 'In Stock'

	UPDATE ES
	SET ES.DisplayOrder = 4
	FROM dropdown.EquipmentStatus ES
	WHERE ES.EquipmentStatusName  = 'Removed'
	
	INSERT INTO dropdown.EquipmentStatus
		(EquipmentStatusName, DisplayOrder)
	VALUES
		('Consumable', 1)
		
	END
--ENDIF
GO
--End table dropdown.EquipmentStatus

--Begin table logicalframework.Objective
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Dev')) = 'Prod'
	BEGIN

	UPDATE O
	SET O.IsActive = 0
	FROM logicalframework.Objective O
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND OT.ObjectiveTypeCode = 'IntermediateOutcome'

	END
--ENDIF
GO
--End table logicalframework.Objective

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ChartList',
	'M & E Overview Charts',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Objective'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Objective.ChartList'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN permissionable.Permissionable P ON P.PermissionableLineage = PP.PermissionableLineage
		AND PP.PermissionableLineage = 'Objective.AddUpdate'
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P.PersonID,
	'Objective.AddUpdate'
FROM dbo.Person P
WHERE P.UserName IN 
	(
	'CarmenG',
	'Celestine',
	'JCole',
	'Rabaa'
	)
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
