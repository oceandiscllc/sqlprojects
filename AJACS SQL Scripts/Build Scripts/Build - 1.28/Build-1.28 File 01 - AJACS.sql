-- File Name:	Build-1.28 File 01 - AJACS.sql
-- Build Key:	Build-1.28 File 01 - AJACS - 2015.09.09 20.13.49

USE AJACS
GO

-- ==============================================================================================================================
-- Procedures:
--		dbo.GetContactByContactID
--		dbo.GetDailyReportByDailyReportID
--		dbo.ValidateLogin
--		eventlog.LogRecommendationAction
--		logicalframework.GetIndicatorByIndicatorID
--		logicalframework.GetIndicatorByObjectiveID
--		logicalframework.GetIntermediateOutcomeChartData
--		logicalframework.GetMilestoneByIndicatorID
--		logicalframework.GetMilestoneByMilestoneID
--		logicalframework.GetMilestoneDataByIndicatorID
--		project.GetProjectByProjectID
--		recommendation.GetRecommendationByRecommendationID
--		recommendation.GetRecommendationStatusUpdateByRecommendationStatusUpdateID
--		recommendationupdate.ApproveRecommendationUpdateRecommendations
--		recommendationupdate.DeleteRecommendationUpdateRecommendation
--		recommendationupdate.GetRecommendationUpdateByRecommendationID
--		survey.GetSurveyResponseBySurveyResponseID
--
-- Tables:
--		recommendation.RecommendationStatusUpdate
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'CommunityAssetID', 'INT'
EXEC utility.AddColumn @TableName, 'CommunityAssetUnitID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitID', 'INT', 0
GO
--End table dbo.Contact

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.DropColumn @TableName, 'ActualValue'
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.DropColumn @TableName, 'AchievedDate'
EXEC utility.DropColumn @TableName, 'ActualDate'
EXEC utility.DropColumn @TableName, 'ActualValue'
EXEC utility.DropColumn @TableName, 'InProgressDate'
EXEC utility.DropColumn @TableName, 'PlannedDate'
GO
--End table logicalframework.Milestone

--Begin table recommendation.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendation.Recommendation'

EXEC utility.DropColumn @TableName, 'RecommendationStatusUpdateDescription'
EXEC utility.DropColumn @TableName, 'StatusUpdateDescription'
GO
--End table recommendation.Recommendation

--Begin table recommendation.RecommendationStatusUpdate
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationStatusUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationStatusUpdate
	(
	RecommendationStatusUpdateID INT IDENTITY(1,1),
	RecommendationID INT,
	PersonID INT,
	RecommendationStatusUpdateDateTime DATETIME,
	StatusUpdateDescription VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationStatusUpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationStatusUpdateID'
GO
--End table recommendation.RecommendationStatusUpdate

--Begin table recommendationupdate.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.Recommendation'

EXEC utility.DropColumn @TableName, 'RecommendationStatusUpdateDescription'
EXEC utility.AddColumn @TableName, 'StatusUpdateDescription', 'VARCHAR(MAX)'
GO
--End table recommendationupdate.Recommendation

--Begin table recommendationupdate.RecommendationStatusUpdate
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationStatusUpdate'

EXEC utility.DropObject @TableName
--End table recommendationupdate.RecommendationStatusUpdate

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.GetPersonNameByPersonID
EXEC Utility.DropObject 'dbo.GetPersonNameByPersonID'
GO
--End function dbo.GetPersonNameByPersonID
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.ArabicMotherName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PlaceOfBirthCountryID,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C1.CommunityAssetID,
		C1.CommunityAssetUnitID,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		(SELECT C7.CommunityAssetName FROM dbo.CommunityAsset C7 WHERE C7.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,
		(SELECT C8.CommunityAssetUnitName FROM dbo.CommunityAssetUnit C8 WHERE C8.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetDailyReportByDailyReportID
EXEC Utility.DropObject 'dbo.GetDailyReportByDailyReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.16
-- Description:	A stored procedure to data from the dbo.DailyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.08.31
-- Description:	Implemented the FormatPersonNameByPersonID function
-- ======================================================================
CREATE PROCEDURE dbo.GetDailyReportByDailyReportID

@DailyReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DR.DailyReportID,
		DR.IsDutyOfCareComplete, 
		DR.Notes,
		DR.PersonID,
		dbo.FormatPersonNameByPersonID(DR.PersonID, 'LastFirstTitle') AS FullName,
		DR.ReportDate,
		dbo.FormatDate(DR.ReportDate) AS ReportDateFormatted,
		(SELECT COUNT(DRC.DailyReportCommunityID) FROM dbo.DailyReportCommunity DRC JOIN dbo.Community C ON C.CommunityID = DRC.CommunityID AND DRC.DailyReportID = DR.DailyReportID) AS CommunityCount,
		DRT.DailyReportTypeID,
		DRT.DailyReportTypeName, 
		DRT.HasCommunities, 
		T.TeamID, 
		T.TeamName
	FROM dbo.DailyReport DR
		JOIN dropdown.DailyReportType DRT ON DRT.DailyReportTypeID = DR.DailyReportTypeID
		JOIN dbo.Team T ON T.TeamID = DR.TeamID
			AND DR.DailyReportID = @DailyReportID

	SELECT
		C.CommunityID, 
		C.CommunityName,
		DRC.Notes
	FROM dbo.DailyReportCommunity DRC
		JOIN dbo.Community C ON C.CommunityID = DRC.CommunityID
			AND DRC.DailyReportID = @DailyReportID
	ORDER BY C.CommunityName
	
END
GO
--End procedure dbo.GetDailyReportByDailyReportID

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Adding password expiration support
--
-- Author:			Todd Pires
-- Create date:	2015.08.31
-- Description:	Implemented the FormatPersonNameByPersonID function
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cPhone =P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin

--Begin procedure eventlog.LogRecommendationAction
EXEC utility.DropObject 'eventlog.LogRecommendationAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRecommendationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRecommendationCommunities VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationCommunities = COALESCE(@cRecommendationCommunities, '') + D.RecommendationCommunities 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationCommunities'), ELEMENTS) AS RecommendationCommunities
			FROM recommendation.RecommendationCommunity T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationComponents VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationComponents = COALESCE(@cRecommendationComponents, '') + D.RecommendationComponents 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationComponents'), ELEMENTS) AS RecommendationComponents
			FROM recommendation.RecommendationComponent T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationIndicators VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationIndicators = COALESCE(@cRecommendationIndicators, '') + D.RecommendationIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationIndicators'), ELEMENTS) AS RecommendationIndicators
			FROM recommendation.RecommendationIndicator T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationProvinces VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationProvinces = COALESCE(@cRecommendationProvinces, '') + D.RecommendationProvinces 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationProvinces'), ELEMENTS) AS RecommendationProvinces
			FROM recommendation.RecommendationProvince T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationRisks VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationRisks = COALESCE(@cRecommendationRisks, '') + D.RecommendationRisks 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationRisks'), ELEMENTS) AS RecommendationRisks
			FROM recommendation.RecommendationRisk T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationStatusUpdates VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationStatusUpdates = COALESCE(@cRecommendationStatusUpdates, '') + D.RecommendationStatusUpdates 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationStatusUpdates'), ELEMENTS) AS RecommendationStatusUpdates
			FROM recommendation.RecommendationStatusUpdate T 
			WHERE T.RecommendationID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RecommendationCommunities>' + ISNULL(@cRecommendationCommunities, '') + '</RecommendationCommunities>') AS XML),
			CAST(('<RecommendationComponents>' + ISNULL(@cRecommendationComponents, '') + '</RecommendationComponents>') AS XML),
			CAST(('<RecommendationIndicators>' + ISNULL(@cRecommendationIndicators, '') + '</RecommendationIndicators>') AS XML),
			CAST(('<RecommendationProvinces>' + ISNULL(@cRecommendationProvinces, '') + '</RecommendationProvinces>') AS XML),
			CAST(('<RecommendationRisks>' + ISNULL(@cRecommendationRisks, '') + '</RecommendationRisks>') AS XML),
			CAST(('<RecommendationStatusUpdates>' + ISNULL(@cRecommendationStatusUpdates, '') + '</RecommendationStatusUpdates>') AS XML)
			FOR XML RAW('Recommendation'), ELEMENTS
			)
		FROM recommendation.Recommendation T
		WHERE T.RecommendationID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRecommendationAction

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
--
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	Add 'short' formats
--
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	Removed the ActualValue column
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetIndicatorByObjectiveID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the logicalframework.Indicator table
--
-- Author:			Todd Pires
-- Create Date: 2015.08.27
-- Description:	Added additional fields
--
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	Removed the ActualValue column
-- =====================================================================================
CREATE PROCEDURE logicalframework.GetIndicatorByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		I.AchievedDate,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormatted,
		I.AchievedValue,
		I.ActualDate,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormatted,
		I.BaselineDate,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormatted,
		I.BaselineValue,
		I.IndicatorDescription,
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorNumber,
		I.IndicatorSource,
		I.InProgressDate,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormatted,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormatted,
		I.PlannedValue,
		I.StatusUpdateDescription,
		I.TargetDate,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormatted,
		I.TargetValue,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName
  FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
			AND I.ObjectiveID = @ObjectiveID
			AND I.IsActive = 1
	ORDER BY I.IndicatorID
	
END
GO
--End procedure logicalframework.GetIndicatorByObjectiveID

--Begin procedure logicalframework.GetIntermediateOutcomeChartData
EXEC Utility.DropObject 'logicalframework.GetIntermediateOutcomeChartData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	A stored procedure to return intermediate outcome objective data
-- =============================================================================
CREATE PROCEDURE logicalframework.GetIntermediateOutcomeChartData

AS
BEGIN
	SET NOCOUNT ON;

	WITH Total AS
		(
		SELECT
			I.AchievedValue,
			I.IndicatorID,
			I.IndicatorName,
			I.InprogressValue,
			I.PlannedValue,
			I.TargetValue,
			I.PlannedValue + I.InprogressValue + I.AchievedValue AS TotalValue,
			LFS.LogicalFrameworkStatusName,
			O2.ObjectiveID,
			O2.ObjectiveName,
			O2.StatusUpdateDescription
		FROM logicalframework.Indicator I
			JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
			JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
				AND O2.IsActive = 1
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O2.LogicalFrameworkStatusID
			JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
				AND OT.ObjectiveTypeCode = 'Output'
				AND I.IsActive = 1
		)
	
	SELECT
		T.IndicatorID,
		T.IndicatorName,
		T.LogicalFrameworkStatusName,
		T.ObjectiveID,
		T.ObjectiveName,
		T.StatusUpdateDescription,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS AchievedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS InprogressValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST((CAST(T.TargetValue AS NUMERIC(18,2)) - CAST(T.AchievedValue AS NUMERIC(18,2)) - CAST(T.InprogressValue AS NUMERIC(18,2)) - CAST(T.PlannedValue AS NUMERIC(18,2))) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS IntendedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS PlannedValuePercent

	FROM Total T
	ORDER BY T.ObjectiveName, T.ObjectiveID, T.IndicatorName, T.IndicatorID

END
GO
--End procedure logicalframework.GetIntermediateOutcomeChartData

--Begin procedure logicalframework.GetMilestoneByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetMilestoneByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
--
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	Completely refactored
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH Total AS
		(
		SELECT
			I.AchievedValue,
			3 AS DisplayOrder,
			I.InprogressValue,
			'Total' AS MilestoneName,
			I.PlannedValue,
			I.TargetDate,
			LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
			I.TargetValue,
			I.PlannedValue + I.InprogressValue + I.AchievedValue AS TotalValue
		FROM logicalframework.Indicator I
		WHERE I.IndicatorID = @IndicatorID
		)
		
	SELECT
		I.BaseLineValue AS AchievedValue,
		1 AS DisplayOrder,
		0 AS InprogressValue,
		'Baseline' AS MilestoneName,
		0 AS PlannedValue,
		I.BaseLineDate AS TargetDate,
		LEFT(DATENAME(month, I.BaseLineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaseLineDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		0 AS TargetValue,
		0 AS AchievedValuePercent,
		0 AS InprogressValuePercent,
		0 AS IntendedValuePercent,
		0 AS PlannedValuePercent
	FROM logicalframework.Indicator I
	WHERE I.IndicatorID = @IndicatorID
	
	UNION
	
	SELECT 
		M.AchievedValue,
		2 AS DisplayOrder,
		M.InProgressValue,
		M.MilestoneName,
		M.PlannedValue,
		M.TargetDate,
		LEFT(DATENAME(month, M.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(M.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		M.TargetValue,
		0 AS AchievedValuePercent,
		0 AS InprogressValuePercent,
		0 AS IntendedValuePercent,
		0 AS PlannedValuePercent
	FROM logicalframework.Milestone M
	WHERE M.IndicatorID = @IndicatorID
	
	UNION
	
	SELECT
		T.AchievedValue,
		T.DisplayOrder,
		T.InprogressValue,
		T.MilestoneName,
		T.PlannedValue,
		T.TargetDate,
		T.TargetDateFormattedShort,
		T.TargetValue,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS AchievedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS InprogressValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST((CAST(T.TargetValue AS NUMERIC(18,2)) - CAST(T.AchievedValue AS NUMERIC(18,2)) - CAST(T.InprogressValue AS NUMERIC(18,2)) - CAST(T.PlannedValue AS NUMERIC(18,2))) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE 0
		END AS IntendedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
			ELSE CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS PlannedValuePercent

	FROM Total T
	
	ORDER BY 2, 6
	
END
GO
--End procedure logicalframework.GetMilestoneByIndicatorID

--Begin procedure logicalframework.GetMilestoneByMilestoneID
EXEC Utility.DropObject 'logicalframework.GetMilestoneByMilestoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByMilestoneID

@MilestoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IndicatorID, 	
		I.IndicatorName, 	
		M.AchievedValue, 	
		M.MilestoneID, 	
		M.MilestoneName, 	
		M.TargetDate, 	
		dbo.FormatDate(M.TargetDate) AS TargetDateFormatted,
		M.TargetValue, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Milestone') AS EntityTypeName
	FROM logicalframework.Milestone M
		JOIN logicalframework.Indicator I ON I.IndicatorID = M.IndicatorID
			AND M.MilestoneID = @MilestoneID

END
GO
--End procedure logicalframework.GetMilestoneByMilestoneID

--Begin procedure logicalframework.GetMilestoneDataByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetMilestoneDataByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Todd Pires
-- Create date:	2015.09.04
-- Description:	A stored procedure to return Milestone data
-- ============================================================
CREATE PROCEDURE logicalframework.GetMilestoneDataByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		M.AchievedValue, 
		M.IndicatorID, 
		M.InProgressValue, 
		M.IsActive, 
		M.MilestoneID, 
		M.MilestoneName, 
		M.PlannedValue,
		M.TargetDate, 
		dbo.FormatDate(M.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, M.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(M.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		M.TargetValue
	FROM logicalframework.MileStone M
	WHERE M.IndicatorID = @IndicatorID
	
END
GO
--End procedure logicalframework.GetMilestoneDataByIndicatorID

--Begin procedure project.GetProjectByProjectID
EXEC Utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.07
-- Description:	A stored procedure to get data from the project.Project table
--
-- Author:			Todd Pires
-- Create date: 2015.08.24
-- Description:	Added the ProjectCommunityValueHTML and ProjectProvinceValueHTML fields
-- ====================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProjectID, 
		P.ProjectName, 
		P.ProjectDescription, 
		P.ProjectSummary, 
		P.ProjectPartner, 
		P.ProjectValue, 
		P.ConceptNoteID,
		dbo.FormatConceptNoteTitle(P.ConceptNoteID) AS ConceptNoteTitle,
		PS.ProjectStatusID,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND P.ProjectID = @ProjectID

	SELECT
		C.CommunityID,
		C.CommunityName,
		PC.ProjectCommunityValue,
		'<input type="number" id="CommunityValue' + CAST(C.CommunityID AS VARCHAR(10)) + '" name="CommunityValue' + CAST(C.CommunityID AS VARCHAR(10)) + '" class="form-control project-value" value="' + CAST(ISNULL(PC.ProjectCommunityValue, 0) AS VARCHAR(10)) + '">' AS ProjectCommunityValueHTML
	FROM project.ProjectCommunity PC
		JOIN dbo.Community C ON C.CommunityID = PC.CommunityID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusName,
		FT.FindingTypeName
	FROM project.ProjectFinding PF
		JOIN finding.Finding F ON F.FindingID = PF.FindingID
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND PF.ProjectID = @ProjectID
	ORDER BY F.FindingName, F.FindingID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM project.ProjectIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.ProjectID = @ProjectID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName,
		PP.ProjectProvinceValue,
		'<input type="number" id="ProvinceValue' + CAST(P.ProvinceID AS VARCHAR(10)) + '" name="ProvinceValue' + CAST(P.ProvinceID AS VARCHAR(10)) + '" class="form-control project-value" value="' + CAST(ISNULL(PP.ProjectProvinceValue, 0) AS VARCHAR(10)) + '">' AS ProjectProvinceValueHTML
	FROM project.ProjectProvince PP
		JOIN dbo.Province P ON P.ProvinceID = PP.ProvinceID
			AND PP.ProjectID = @ProjectID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF((
				SELECT 
				 ', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
				 JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = PR.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList									
	FROM project.ProjectRecommendation PR
		JOIN recommendation.Recommendation R ON R.RecommendationID = PR.RecommendationID
			AND PR.ProjectID = @ProjectID
	ORDER BY R.RecommendationName, R.RecommendationID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure recommendation.GetRecommendationByRecommendationID
EXEC Utility.DropObject 'recommendation.GetRecommendationByRecommendationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to get data from the recommendation.Recommendation table
-- ========================================================================================
CREATE PROCEDURE recommendation.GetRecommendationByRecommendationID

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendation.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.Community C ON C.CommunityID = RC.CommunityID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		C.ComponentID,
		C.ComponentName
	FROM recommendation.RecommendationComponent RC
		JOIN dropdown.Component C ON C.ComponentID = RC.ComponentID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.ComponentName, C.ComponentID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM recommendation.RecommendationIndicator RI
		JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND RI.RecommendationID = @RecommendationID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.Province P ON P.ProvinceID = RP.ProvinceID
			AND RP.RecommendationID = @RecommendationID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RT.RiskTypeName
	FROM recommendation.RecommendationRisk RR
		JOIN dbo.Risk R ON R.RiskID = RR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND RR.RecommendationID = @RecommendationID
	ORDER BY R.RiskName, R.RiskID

	SELECT TOP 1
		RSU.StatusUpdateDescription
	FROM recommendation.RecommendationStatusUpdate RSU
	WHERE RSU.RecommendationID = @RecommendationID
	ORDER BY RSU.RecommendationStatusUpdateDateTime DESC

	SELECT 
		'<a class="btn btn-info" onclick="viewRecommendationStatusUpdate(' + CAST(RSU.RecommendationStatusUpdateID AS VARCHAR(10)) + ')">View</a>' AS ActionButton,
		dbo.FormatPersonNameByPersonID(RSU.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(RSU.RecommendationStatusUpdateDateTime) AS RecommendationStatusUpdateDateTimeFormatted
	FROM recommendation.RecommendationStatusUpdate RSU
	WHERE RSU.RecommendationID = @RecommendationID
	ORDER BY RSU.RecommendationStatusUpdateDateTime DESC

END
GO
--End procedure recommendation.GetRecommendationByRecommendationID

--Begin procedure recommendation.GetRecommendationStatusUpdateByRecommendationID
EXEC Utility.DropObject 'recommendation.GetRecommendationStatusUpdateByRecommendationID'
GO
--End procedure recommendation.GetRecommendationByRecommendationID

--Begin procedure recommendation.GetRecommendationStatusUpdateByRecommendationStatusUpdateID
EXEC Utility.DropObject 'recommendation.GetRecommendationStatusUpdateByRecommendationStatusUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to get data from the recommendation.GetRecommendationStatusUpdate table
-- =======================================================================================================
CREATE PROCEDURE recommendation.GetRecommendationStatusUpdateByRecommendationStatusUpdateID

@RecommendationStatusUpdateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RSU.StatusUpdateDescription,
		dbo.FormatPersonNameByPersonID(RSU.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(RSU.RecommendationStatusUpdateDateTime) AS RecommendationStatusUpdateDateTimeFormatted
	FROM recommendation.RecommendationStatusUpdate RSU
	WHERE RSU.RecommendationStatusUpdateID = @RecommendationStatusUpdateID

END
GO
--End procedure recommendation.GetRecommendationStatusUpdateByRecommendationStatusUpdateID

--Begin procedure recommendationupdate.ApproveRecommendationUpdateRecommendations
EXEC Utility.DropObject 'recommendationupdate.ApproveRecommendationUpdateRecommendations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to submit recommendations in a recommendation update for approval
-- =================================================================================================
CREATE PROCEDURE recommendationupdate.ApproveRecommendationUpdateRecommendations

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	
	DECLARE @RecommendationID INT
	DECLARE @RecommendationUpdateID INT = (SELECT TOP 1 RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU ORDER BY RU.RecommendationUpdateID DESC)
	DECLARE @tOutput TABLE (RecommendationID INT)

	UPDATE R
	SET
		R.IsActive = RUR.IsActive,
		R.IsResearchElement = RUR.IsResearchElement,
		R.RecommendationDescription = RUR.RecommendationDescription,
		R.RecommendationName = RUR.RecommendationName
	OUTPUT INSERTED.RecommendationID INTO @tOutput
	FROM recommendation.Recommendation R
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = R.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationCommunity T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationCommunity
		(RecommendationID, CommunityID)
	SELECT
		T.RecommendationID, 
		T.CommunityID
	FROM recommendationupdate.RecommendationCommunity T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationComponent T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationComponent
		(RecommendationID, ComponentID)
	SELECT
		T.RecommendationID, 
		T.ComponentID
	FROM recommendationupdate.RecommendationComponent T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationIndicator T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationIndicator
		(RecommendationID, IndicatorID)
	SELECT
		T.RecommendationID, 
		T.IndicatorID
	FROM recommendationupdate.RecommendationIndicator T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationProvince T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationProvince
		(RecommendationID, ProvinceID)
	SELECT
		T.RecommendationID, 
		T.ProvinceID
	FROM recommendationupdate.RecommendationProvince T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationRisk T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationRisk
		(RecommendationID, RiskID)
	SELECT
		T.RecommendationID, 
		T.RiskID
	FROM recommendationupdate.RecommendationRisk T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationStatusUpdate
		(RecommendationID, PersonID, StatusUpdateDescription)
	SELECT
		T.RecommendationID, 
		@PersonID, 
		T.StatusUpdateDescription
	FROM recommendationupdate.Recommendation T
	WHERE T.RecommendationUpdateID = @RecommendationUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.RecommendationID
		FROM @tOutput O
		ORDER BY O.RecommendationID
	
	OPEN oCursor
	FETCH oCursor INTO @RecommendationID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'read', @PersonID, NULL
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @RecommendationID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'update', @PersonID, NULL
	
	DELETE FROM recommendationupdate.RecommendationUpdate

	TRUNCATE TABLE recommendationupdate.Recommendation
	TRUNCATE TABLE recommendationupdate.RecommendationCommunity
	TRUNCATE TABLE recommendationupdate.RecommendationComponent
	TRUNCATE TABLE recommendationupdate.RecommendationIndicator
	TRUNCATE TABLE recommendationupdate.RecommendationProvince
	TRUNCATE TABLE recommendationupdate.RecommendationRisk

END
GO
--End procedure recommendationupdate.ApproveRecommendationUpdateRecommendations

--Begin procedure recommendationupdate.DeleteRecommendationUpdateRecommendation
EXEC Utility.DropObject 'recommendationupdate.DeleteRecommendationUpdateRecommendation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to remove a recommendation from the recommendation update
--
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	Added one to many table support
-- =========================================================================================
CREATE PROCEDURE recommendationupdate.DeleteRecommendationUpdateRecommendation

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM recommendationupdate.Recommendation T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationCommunity T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationComponent T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationIndicator T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationProvince T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationRisk T
	WHERE T.RecommendationID = @RecommendationID

END
GO
--End procedure recommendationupdate.DeleteRecommendationUpdateRecommendation

--Begin procedure recommendationupdate.GetRecommendationUpdateByRecommendationID
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdateByRecommendationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.26
-- Description:	A stored procedure to get data from the recommendationupdate.Recommendation table
-- ==============================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdateByRecommendationID

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.StatusUpdateDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendationupdate.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM recommendationupdate.RecommendationCommunity RC
		JOIN dbo.Community C ON C.CommunityID = RC.CommunityID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		C.ComponentID,
		C.ComponentName
	FROM recommendationupdate.RecommendationComponent RC
		JOIN dropdown.Component C ON C.ComponentID = RC.ComponentID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.ComponentName, C.ComponentID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM recommendationupdate.RecommendationIndicator RI
		JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND RI.RecommendationID = @RecommendationID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM recommendationupdate.RecommendationProvince RP
		JOIN dbo.Province P ON P.ProvinceID = RP.ProvinceID
			AND RP.RecommendationID = @RecommendationID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RT.RiskTypeName
	FROM recommendationupdate.RecommendationRisk RR
		JOIN dbo.Risk R ON R.RiskID = RR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND RR.RecommendationID = @RecommendationID
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure recommendationupdate.GetRecommendationUpdateByRecommendationID

--Begin survey.GetSurveyResponseBySurveyResponseID
EXEC Utility.DropObject 'survey.GetSurveyResponseBySurveyResponseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.03
-- Description:	A stored procedure to get survey a response from the database
--
-- Author:			Todd Pires
-- Create date:	2015.08.31
-- Description:	Implemented the FormatPersonNameByPersonID function
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyResponseBySurveyResponseID

@SurveyResponseID INT

AS
BEGIN

	SELECT
		L.LanguageName,
		SR.LanguageCode,
		SR.SurveyID,
		SR.SurveyResponseID,
		dbo.FormatDateTime(SR.ResponseDateTime) AS ResponseDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(SR.PersonID, 'LastFirst') AS FullName
	FROM survey.SurveyResponse SR
		JOIN dropdown.Language L ON L.LanguageCode = SR.LanguageCode
			AND SR.SurveyResponseID = @SurveyResponseID
	
END
GO
--End procedure survey.GetSurveyResponseBySurveyResponseID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveOverview', @NewMenuItemLink='/objective/chartlist', @BeforeMenuItemCode='ObjectiveList', @PermissionableLineageList='Objective.ChartList'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveManage', @NewMenuItemLink='/objective/manage', @NewMenuItemText='Manage', @ParentMenuItemCode='LogicalFramework', @BeforeMenuItemCode='ObjectiveList', @PermissionableLineageList='Objective.AddUpdate'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
--End table dbo.MenuItem

--Begin table dropdown.EquipmentStatus
IF NOT EXISTS (SELECT 1 FROM dropdown.EquipmentStatus ES WHERE ES.EquipmentStatusName = 'Consumable')
	BEGIN
	
	UPDATE ES
	SET ES.DisplayOrder = 2
	FROM dropdown.EquipmentStatus ES
	WHERE ES.EquipmentStatusName  = 'In Service'
	
	UPDATE ES
	SET ES.DisplayOrder = 3
	FROM dropdown.EquipmentStatus ES
	WHERE ES.EquipmentStatusName  = 'In Stock'

	UPDATE ES
	SET ES.DisplayOrder = 4
	FROM dropdown.EquipmentStatus ES
	WHERE ES.EquipmentStatusName  = 'Removed'
	
	INSERT INTO dropdown.EquipmentStatus
		(EquipmentStatusName, DisplayOrder)
	VALUES
		('Consumable', 1)
		
	END
--ENDIF
GO
--End table dropdown.EquipmentStatus

--Begin table logicalframework.Objective
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Dev')) = 'Prod'
	BEGIN

	UPDATE O
	SET O.IsActive = 0
	FROM logicalframework.Objective O
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND OT.ObjectiveTypeCode = 'IntermediateOutcome'

	END
--ENDIF
GO
--End table logicalframework.Objective

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ChartList',
	'M & E Overview Charts',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Objective'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Objective.ChartList'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN permissionable.Permissionable P ON P.PermissionableLineage = PP.PermissionableLineage
		AND PP.PermissionableLineage = 'Objective.AddUpdate'
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P.PersonID,
	'Objective.AddUpdate'
FROM dbo.Person P
WHERE P.UserName IN 
	(
	'CarmenG',
	'Celestine',
	'JCole',
	'Rabaa'
	)
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.28 File 01 - AJACS - 2015.09.09 20.13.49')
GO
--End build tracking

