USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'CommunityAssetID', 'INT'
EXEC utility.AddColumn @TableName, 'CommunityAssetUnitID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitID', 'INT', 0
GO
--End table dbo.Contact

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.DropColumn @TableName, 'ActualValue'
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.DropColumn @TableName, 'AchievedDate'
EXEC utility.DropColumn @TableName, 'ActualDate'
EXEC utility.DropColumn @TableName, 'ActualValue'
EXEC utility.DropColumn @TableName, 'InProgressDate'
EXEC utility.DropColumn @TableName, 'PlannedDate'
GO
--End table logicalframework.Milestone

--Begin table recommendation.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendation.Recommendation'

EXEC utility.DropColumn @TableName, 'RecommendationStatusUpdateDescription'
EXEC utility.DropColumn @TableName, 'StatusUpdateDescription'
GO
--End table recommendation.Recommendation

--Begin table recommendation.RecommendationStatusUpdate
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationStatusUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationStatusUpdate
	(
	RecommendationStatusUpdateID INT IDENTITY(1,1),
	RecommendationID INT,
	PersonID INT,
	RecommendationStatusUpdateDateTime DATETIME,
	StatusUpdateDescription VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationStatusUpdateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationStatusUpdateID'
GO
--End table recommendation.RecommendationStatusUpdate

--Begin table recommendationupdate.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.Recommendation'

EXEC utility.DropColumn @TableName, 'RecommendationStatusUpdateDescription'
EXEC utility.AddColumn @TableName, 'StatusUpdateDescription', 'VARCHAR(MAX)'
GO
--End table recommendationupdate.Recommendation

--Begin table recommendationupdate.RecommendationStatusUpdate
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationStatusUpdate'

EXEC utility.DropObject @TableName
--End table recommendationupdate.RecommendationStatusUpdate
