USE AJACS
GO

--Begin function dbo.ValidatePassword
EXEC utility.DropObject 'dbo.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create date:	2015.08.01
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION dbo.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = Password,
		@cPasswordSalt = PasswordSalt
	FROM dbo.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.ValidatePassword

--Begin function survey.GetAdministerButtonHTMLBySurveyID
EXEC utility.DropObject 'survey.GetAdministerButtonHTMLBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.01
-- Description:	A function to return an html string for a survey Administer button
-- ===============================================================================

CREATE FUNCTION survey.GetAdministerButtonHTMLBySurveyID
(
@SurveyID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cHTML VARCHAR(MAX) = ''

	SELECT @cHTML = 
		STUFF((SELECT '<br><a href="/surveymanagement/administersurvey/id/' + CAST(SL.SurveyID AS VARCHAR(10)) + '?LanguageCode=' + SL.LanguageCode + '">' + L.LanguageName + '</a>' 
		FROM survey.SurveyLanguage SL 
			JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
		WHERE SL.SurveyID = @SurveyID
		ORDER BY SL.LanguageCode 
		FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'), 1, 4, '')

	RETURN ISNULL(@cHTML, '')

END
GO
--End function survey.GetAdministerButtonHTMLBySurveyID

--Begin function survey.GetViewButtonHTMLBySurveyID
EXEC utility.DropObject 'survey.GetViewButtonHTMLBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.01
-- Description:	A function to return an html string for a survey view button
-- =========================================================================

CREATE FUNCTION survey.GetViewButtonHTMLBySurveyID
(
@SurveyID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cHTML VARCHAR(MAX) = ''

	SELECT @cHTML = 
		STUFF((SELECT '<br><a href="/surveymanagement/viewsurvey/id/' + CAST(SL.SurveyID AS VARCHAR(10)) + '?LanguageCode=' + SL.LanguageCode + '">' + L.LanguageName + '</a>' 
		FROM survey.SurveyLanguage SL 
			JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
		WHERE SL.SurveyID = @SurveyID
		ORDER BY SL.LanguageCode 
		FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'), 1, 4, '')

	RETURN ISNULL(@cHTML, '')

END
GO
--End function survey.GetViewButtonHTMLBySurveyID

--Begin function utility.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'utility.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date:	2015.08.02
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION utility.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM dbo.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function utility.GetDescendantMenuItemsByMenuItemCode


