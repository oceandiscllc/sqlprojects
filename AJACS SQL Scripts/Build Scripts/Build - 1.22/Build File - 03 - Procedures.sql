USE AJACS
GO

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage = MIPL.PermissionableLineage
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage = MIPL.PermissionableLineage
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Added the IsAccountLockedOut bit
--
-- Author:			John Lyons
-- Create date:	2015.07.29
-- Description:	Added two factor support
-- ============================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.FirstName, 
		P.IsAccountLockedOut,
		P.LastName,
		P.Organization,
		P.PersonID,
		P.RoleID, 
		P.Title,
		P.UserName,
		R.RoleName,
		P.Phone, 
		P.CountryCallingCodeID,
		P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.GetRiskByRiskID
EXEC Utility.DropObject 'dbo.GetRiskByRiskID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===========================================================================
CREATE PROCEDURE dbo.GetRiskByRiskID

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName
	FROM dbo.Risk R
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND R.RiskID = @RiskID

END
GO
--End procedure dbo.GetRiskByRiskID

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Adding password expiration support
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT


	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cPhone =P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin

--Begin procedure eventlog.LogConceptNoteAction
EXEC utility.DropObject 'eventlog.LogConceptNoteAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogConceptNoteAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cConceptNoteBudget VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteBudget = COALESCE(@cConceptNoteBudget, '') + D.ConceptNoteBudget
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteBudget'), ELEMENTS) AS ConceptNoteBudget
			FROM dbo.ConceptNoteBudget T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteClass VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteClass = COALESCE(@cConceptNoteClass, '') + D.ConceptNoteClass
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteClass'), ELEMENTS) AS ConceptNoteClass
			FROM dbo.ConceptNoteClass T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteCommunities VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCommunities = COALESCE(@cConceptNoteCommunities, '') + D.ConceptNoteCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCommunity'), ELEMENTS) AS ConceptNoteCommunity
			FROM dbo.ConceptNoteCommunity T 
			WHERE T.ConceptNoteID = @EntityID
			) D
			
		DECLARE @cConceptNoteContact VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteContact= COALESCE(@cConceptNoteContact, '') + D.ConceptNoteContact
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteContact'), ELEMENTS) AS ConceptNoteContact
			FROM dbo.ConceptNoteContact T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteCourse VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCourse= COALESCE(@cConceptNoteCourse, '') + D.ConceptNoteCourse
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCourse'), ELEMENTS) AS ConceptNoteCourse
			FROM dbo.ConceptNoteCourse T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteEquipmentCatalog  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteEquipmentCatalog = COALESCE(@cConceptNoteEquipmentCatalog , '') + D.ConceptNoteEquipmentCatalog 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteEquipmentCatalog'), ELEMENTS) AS ConceptNoteEquipmentCatalog 
			FROM dbo.ConceptNoteEquipmentCatalog  T 
			WHERE T.ConceptNoteID = @EntityID
			) D				
			
		DECLARE @cConceptNoteIndicator  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteIndicator = COALESCE(@cConceptNoteIndicator , '') + D.ConceptNoteIndicator 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteIndicator'), ELEMENTS) AS ConceptNoteIndicator 
			FROM dbo.ConceptNoteIndicator  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteProvinces VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteProvinces = COALESCE(@cConceptNoteProvinces, '') + D.ConceptNoteProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteProvince'), ELEMENTS) AS ConceptNoteProvince
			FROM dbo.ConceptNoteProvince T 
			WHERE T.ConceptNoteID = @EntityID
			) D			

		DECLARE @cConceptNoteTask  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteTask = COALESCE(@cConceptNoteTask , '') + D.ConceptNoteTask 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteTask'), ELEMENTS) AS ConceptNoteTask 
			FROM dbo.ConceptNoteTask  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteRisk VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteRisk = COALESCE(@cConceptNoteRisk, '') + D.ConceptNoteRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteRisk'), ELEMENTS) AS ConceptNoteRisk
			FROM dbo.ConceptNoteRisk T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ConceptNoteBudgets>' + ISNULL(@cConceptNoteBudget , '') + '</ConceptNoteBudgets>') AS XML), 
			CAST(('<ConceptNoteClasses>' + ISNULL(@cConceptNoteClass , '') + '</ConceptNoteClasses>') AS XML), 
			CAST(('<ConceptNoteCommunities>' + ISNULL(@cConceptNoteCommunities , '') + '</ConceptNoteCommunities>') AS XML), 
			CAST(('<ConceptNoteContacts>' + ISNULL(@cConceptNoteContact , '') + '</ConceptNoteContacts>') AS XML), 
			CAST(('<ConceptNoteCourses>' + ISNULL(@cConceptNoteCourse , '') + '</ConceptNoteCourses>') AS XML), 
			CAST(('<ConceptNoteEquipmentCatalogs>' + ISNULL(@cConceptNoteEquipmentCatalog  , '') + '</ConceptNoteEquipmentCatalogs>') AS XML), 
			CAST(('<ConceptNoteIndicators>' + ISNULL(@cConceptNoteIndicator  , '') + '</ConceptNoteIndicators>') AS XML), 
			CAST(('<ConceptNoteProvinces>' + ISNULL(@cConceptNoteProvinces , '') + '</ConceptNoteProvinces>') AS XML), 
			CAST(('<ConceptNoteTasks>' + ISNULL(@cConceptNoteTask  , '') + '</ConceptNoteTasks>') AS XML),
			CAST(('<ConceptNoteRisks>' + ISNULL(@cConceptNoteRisk  , '') + '</ConceptNoteRisks>') AS XML)
			FOR XML RAW('ConceptNote'), ELEMENTS
			)
		FROM dbo.ConceptNote T
		WHERE T.ConceptNoteID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogConceptNoteAction

--Begin procedure eventlog.LogRiskAction
EXEC utility.DropObject 'eventlog.LogRiskAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRiskAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Risk',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Risk',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Risk'), ELEMENTS
			)
		FROM dbo.Risk T
		WHERE T.RiskID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRiskAction

--Begin procedure eventlog.LogSurveyAction
EXEC utility.DropObject 'eventlog.LogSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Survey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSurveyLabels NVARCHAR(MAX) 
	
		SELECT 
			@cSurveyLabels = COALESCE(@cSurveyLabels, '') + D.SurveyLabels 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyLabels'), ELEMENTS) AS SurveyLabels
			FROM survey.SurveyLabel T 
			WHERE T.SurveyID = @EntityID
			) D

		DECLARE @cSurveyLanguages VARCHAR(MAX) 
	
		SELECT 
			@cSurveyLanguages = COALESCE(@cSurveyLanguages, '') + D.SurveyLanguages 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyLanguages'), ELEMENTS) AS SurveyLanguages
			FROM survey.SurveyLanguage T 
			WHERE T.SurveyID = @EntityID
			) D

		DECLARE @cSurveySurveyQuestions NVARCHAR(MAX) 
	
		SELECT 
			@cSurveySurveyQuestions = COALESCE(@cSurveySurveyQuestions, '') + D.SurveySurveyQuestions 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveySurveyQuestions'), ELEMENTS) AS SurveySurveyQuestions
			FROM survey.SurveySurveyQuestion T 
			WHERE T.SurveyID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Survey',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<SurveyLabels>' + ISNULL(@cSurveyLabels, '') + '</SurveyLabels>') AS XML),
			CAST(('<SurveyLanguages>' + ISNULL(@cSurveyLanguages, '') + '</SurveyLanguages>') AS XML),
			CAST(('<SurveySurveyQuestions>' + ISNULL(@cSurveySurveyQuestions, '') + '</SurveySurveyQuestions>') AS XML)
			FOR XML RAW('Survey'), ELEMENTS
			)
		FROM survey.Survey T
		WHERE T.SurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSurveyAction

--Begin procedure eventlog.LogSurveyQuestionAction
EXEC utility.DropObject 'eventlog.LogSurveyQuestionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSurveyQuestionAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SurveyQuestion',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSurveyQuestionLabels NVARCHAR(MAX) 
	
		SELECT 
			@cSurveyQuestionLabels = COALESCE(@cSurveyQuestionLabels, '') + D.SurveyQuestionLabels 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyQuestionLabels'), ELEMENTS) AS SurveyQuestionLabels
			FROM survey.SurveyQuestionLabel T 
			WHERE T.SurveyQuestionID = @EntityID
			) D

		DECLARE @cSurveyQuestionLanguages VARCHAR(MAX) 
	
		SELECT 
			@cSurveyQuestionLanguages = COALESCE(@cSurveyQuestionLanguages, '') + D.SurveyQuestionLanguages 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyQuestionLanguages'), ELEMENTS) AS SurveyQuestionLanguages
			FROM survey.SurveyQuestionLanguage T 
			WHERE T.SurveyQuestionID = @EntityID
			) D

		DECLARE @cSurveyQuestionResponseChoices NVARCHAR(MAX) 
	
		SELECT 
			@cSurveyQuestionResponseChoices = COALESCE(@cSurveyQuestionResponseChoices, '') + D.SurveyQuestionResponseChoices 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyQuestionResponseChoices'), ELEMENTS) AS SurveyQuestionResponseChoices
			FROM survey.SurveyQuestionResponseChoice T 
			WHERE T.SurveyQuestionID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SurveyQuestion',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<SurveyQuestionLabels>' + ISNULL(@cSurveyQuestionLabels, '') + '</SurveyQuestionLabels>') AS XML),
			CAST(('<SurveyQuestionLanguages>' + ISNULL(@cSurveyQuestionLanguages, '') + '</SurveyQuestionLanguages>') AS XML),
			CAST(('<SurveyQuestionResponseChoices>' + ISNULL(@cSurveyQuestionResponseChoices, '') + '</SurveyQuestionResponseChoices>') AS XML)
			FOR XML RAW('Survey'), ELEMENTS
			)
		FROM survey.SurveyQuestion T
		WHERE T.SurveyQuestionID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSurveyQuestionAction

--Begin procedure reporting.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.06
-- Description:	A stored procedure to get data for the Concept Note Budget Export
-- ==============================================================================
CREATE PROCEDURE reporting.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CNBD AS
		(
		SELECT
			BT.BudgetTypeName,
			BST.BudgetSubTypeName,
			CNB.ConceptNoteBudgetID AS EntityID,
			CNB.ItemName,
			CNB.ItemDescription,
			CNB.UnitCost,
			FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNB.Quantity,
			FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue as TotalCost, 
			CNB.UnitOfIssue,
			CNB.QuantityOfIssue
		FROM dbo.ConceptNoteBudget CNB
				JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
				JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
					AND CNB.conceptnoteid = @ConceptNoteID

		UNION ALL

		SELECT
			'Equipment' as BudgetTypeName,
			'' as BudgetSubTypeName,
			CNEC.ConceptNoteEquipmentCatalogID AS EntityID,
			EC.ItemName,
			EC.ItemDescription,
			EC.UnitCost,
			FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNEC.Quantity,
			FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
			EC.UnitOfIssue,
			EC.QuantityOfIssue
		FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = @ConceptNoteID
		)

	SELECT
		BTS.BudgetTypeSequence,
		BSS.BudgetSubTypeSequence,
		ROW_NUMBER() OVER (PARTITION BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName ORDER BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName, CNBD.ItemName, CNBD.Quantity * CNBD.UnitCost, CNBD.EntityID) AS ItemSequence,
		CNBD.BudgetTypeName, 
		CNBD.BudgetSubTypeName,
		CNBD.ItemName,
		CNBD.ItemDescription,
		CNBD.UnitCostFormatted,
		CNBD.Quantity,
		CNBD.TotalCostFormatted,
		CNBD.TotalCost,
		CNBD.UnitOfIssue,
		CNBD.QuantityOfIssue
	FROM CNBD
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (ORDER BY BTN.BudgetTypeName) AS BudgetTypeSequence,
				BTN.BudgetTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName 
				FROM CNBD
				) BTN
			) BTS ON BTS.BudgetTypeName = CNBD.BudgetTypeName
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY BSN.BudgetTypeName ORDER BY BSN.BudgetTypeName, BSN.BudgetSubTypeName) AS BudgetSubTypeSequence,
				BSN.BudgetTypeName,
				BSN.BudgetSubTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName,
					CNBD.BudgetSubTypeName
				FROM CNBD
				) BSN
			) BSS ON BSS.BudgetTypeName = CNBD.BudgetTypeName
				AND BSS.BudgetSubTypeName = CNBD.BudgetSubTypeName

END
GO
--End procedure reporting.GetConceptNoteBudgetByConceptNoteID

--Begin procedure reporting.GetConceptNoteRiskByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteRiskByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetConceptNoteRiskByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName
	FROM ConceptNoteRisk CNR
		JOIN Risk R ON R.RiskID = CNR.RiskID AND CNR.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID

END
GO
--End procedure reporting.GetConceptNoteRiskByConceptNoteID

--Begin procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestEquipmentByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purchase request reports
--
-- Author:			Brandon Green
-- Create date:	2015.07.27
-- Description:	Added the row index
-- ===============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ID INT, 
		BudgetTypeName Varchar(250),
		Quantity NUMERIC(18,4),
		ItemName VARCHAR(250), 
		UnitCost NUMERIC(18,2), 
		UnitOfIssue VARCHAR(250), 
		UnitCostFormatted VARCHAR(50), 
		TotalCost NUMERIC(18,2), 
		TotalCostFormatted VARCHAR(50),
		QuantityOfIssue NUMERIC(18,4)
		)

	INSERT INTO @tTable
		(ID,BudgetTypeName,Quantity,ItemName,UnitCost,UnitOfIssue,UnitCostFormatted,TotalCost,TotalCostFormatted,QuantityOfIssue)
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID as ID,
		'Equipment' as BudgetTypeName,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.ItemName,
		EC.UnitCost,
		CAST(EC.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.QuantityOfIssue
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
		JOIN procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC ON PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID
			AND PRCNEC.PurchaseRequestID = @PurchaseRequestID

	UNION

	SELECT			
		CNB.ConceptNoteBudgetID as ID,
		BT.BudgetTypeName,
		ISNULL(PRCNB.Quantity,CNB.Quantity) AS Quantity,
		CNB.ItemName,
		CNB.UnitCost,
		CAST(CNB.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID	
		JOIN procurement.PurchaseRequestConceptNoteBudget PRCNB ON PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(ID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		ROW_NUMBER() OVER (ORDER BY T.ID) AS RowNumber,
		T.ID,
		T.BudgetTypeName,
		T.Quantity,
		T.ItemName,
		T.UnitCost,
		T.UnitOfIssue,
		T.UnitCostFormatted,
		T.TotalCost,
		T.TotalCostFormatted,
		T.QuantityOfIssue
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

--Begin procedure reporting.GetRiskReportByPersonID
EXEC Utility.DropObject 'reporting.GetRiskReportByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.08.01
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetRiskReportByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName
	FROM reporting.SearchResult SR
		JOIN Risk R ON R.RiskID = SR.EntityID  
			AND SR.PersonID = @PersonID  
			AND SR.EntityTypeCode='risk'
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID

END
GO
--End procedure reporting.GetRiskReportByPersonID

--Begin survey.GetSurveyBySurveyID
EXEC Utility.DropObject 'survey.GetSurveyBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.15
-- Description:	A stored procedure to get a survey from the database
-- =================================================================
CREATE PROCEDURE survey.GetSurveyBySurveyID

@SurveyID INT,
@LanguageCode CHAR(2)

AS
BEGIN

	SELECT 
		S.IsActive,
		S.SurveyID
	FROM survey.Survey S
	WHERE S.SurveyID = @SurveyID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SL.SurveyName, 
		SL.SurveyDescription
	FROM survey.SurveyLabel SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyLanguage SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		SQ.SurveyQuestionID,
		(SELECT STUFF((SELECT ', ' + SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID ORDER BY SQL.LanguageCode FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'),1,2,'')) AS LanguageCodes,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionText,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionDescription,
	
		SQT.SurveyQuestionResponseTypeCode,
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveySurveyQuestion SSQ
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQ.SurveyID = @SurveyID
			AND 
				(
				@LanguageCode = ''
					OR EXISTS
						(
						SELECT 1
						FROM survey.SurveyQuestionLanguage SQL
						WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID
							AND SQL.LanguageCode = @LanguageCode 
						)
				)
	
END
GO
--End procedure survey.GetSurveyBySurveyID

--Begin survey.GetSurveyQuestionBySurveyQuestionID
EXEC Utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.13
-- Description:	A stored procedure to get a survey question from the database
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionBySurveyQuestionID

@SurveyQuestionID INT

AS
BEGIN

	SELECT 
		SQ.IsActive,
		SQ.SurveyQuestionID, 
		SQT.SurveyQuestionResponseTypeID,
		SQT.SurveyQuestionResponseTypeCode, 
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveyQuestion SQ
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SQ.SurveyQuestionID = @SurveyQuestionID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SQL.SurveyQuestionText, 
		SQL.SurveyQuestionDescription
	FROM survey.SurveyQuestionLabel SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyQuestionLanguage SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

END
GO
--End procedure survey.GetSurveyQuestionBySurveyQuestionID

--Begin survey.GetSurveyQuestionResponseChoices
EXEC Utility.DropObject 'survey.GetSurveyQuestionResponseChoices'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.01
-- Description:	A stored procedure to get survey question response choices from the database
-- =========================================================================================
CREATE PROCEDURE survey.GetSurveyQuestionResponseChoices

@SurveyQuestionCode VARCHAR(50),
@SurveyQuestionID INT,
@LanguageCode CHAR(2)

AS
BEGIN

	SELECT 
		SQRC.SurveyQuestionResponseChoiceText,
		SQRC.SurveyQuestionResponseChoiceValue
	FROM survey.SurveyQuestionResponseChoice SQRC
	WHERE 
		(
		@LanguageCode = ''
			OR SQRC.LanguageCode = @LanguageCode
		)
		AND 
			(
			(@SurveyQuestionCode != '' AND SQRC.SurveyQuestionCode = @SurveyQuestionCode)
				OR (@SurveyQuestionID > 0 AND SQRC.SurveyQuestionID = @SurveyQuestionID)
			)
	ORDER BY SQRC.DisplayOrder, SQRC.SurveyQuestionResponseChoiceID

END
GO
--End procedure survey.GetSurveyQuestionResponseChoices

--Begin utility.UpdateParentPermissionableLineageByMenuItemCode
EXEC Utility.DropObject 'utility.UpdateParentPermissionableLineageByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to update the menuitem permissionable linage records of a parent from its descendants
-- =====================================================================================================================
CREATE PROCEDURE utility.UpdateParentPermissionableLineageByMenuItemCode

@MenuItemCode VARCHAR(50)

AS
BEGIN

	DELETE MIPL
	FROM dbo.MenuItemPermissionableLineage MIPL
		JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
			AND MI.MenuItemCode = @MenuItemCode
	
	INSERT INTO dbo.MenuItemPermissionableLineage
		(MenuItemID,PermissionableLineage)
	SELECT 
		(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode),
		MIPL1.PermissionableLineage
	FROM dbo.MenuItemPermissionableLineage MIPL1
		JOIN utility.GetDescendantMenuItemsByMenuItemCode(@MenuItemCode) MI ON MI.MenuItemID = MIPL1.MenuItemID
			AND NOT EXISTS
				(
				SELECT 1
				FROM dbo.MenuItemPermissionableLineage MIPL2
				WHERE MIPL2.MenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode)
					AND MIPL2.PermissionableLineage = MIPL1.PermissionableLineage
				)

END
GO
--End procedure utility.UpdateParentPermissionableLineageByMenuItemCode