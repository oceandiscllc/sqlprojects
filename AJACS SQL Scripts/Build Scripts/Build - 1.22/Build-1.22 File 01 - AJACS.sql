-- File Name:	Build-1.22 File 01 - AJACS.sql
-- Build Key:	Build-1.22 File 01 - AJACS - 2015.08.02 19.46.16

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.ValidatePassword
--		survey.GetAdministerButtonHTMLBySurveyID
--		survey.GetViewButtonHTMLBySurveyID
--		utility.GetDescendantMenuItemsByMenuItemCode
--
-- Procedures:
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetMenuItemsByPersonID
--		dbo.GetPersonByPersonID
--		dbo.GetRiskByRiskID
--		dbo.ValidateLogin
--		eventlog.LogConceptNoteAction
--		eventlog.LogRiskAction
--		eventlog.LogSurveyAction
--		eventlog.LogSurveyQuestionAction
--		reporting.GetConceptNoteBudgetByConceptNoteID
--		reporting.GetConceptNoteRiskByConceptNoteID
--		reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
--		reporting.GetRiskReportByPersonID
--		survey.GetSurveyBySurveyID
--		survey.GetSurveyQuestionBySurveyQuestionID
--		survey.GetSurveyQuestionResponseChoices
--		utility.UpdateParentPermissionableLineageByMenuItemCode
--
-- Tables:
--		dropdown.CountryCallingCode
--		survey.Survey
--		survey.SurveyLabel
--		survey.SurveyQuestionLabel
--		survey.SurveyQuestionResponseChoice
--		survey.SurveyResponse
--		survey.SurveySurveyQuestionResponse
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Person
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Person') AND SC.Name = 'PhoneISOCountryDataID')
	EXEC sp_RENAME 'dbo.Person.PhoneISOCountryDataID', 'CountryCallingCodeID', 'COLUMN';
--ENDIF
GO

DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC utility.AddColumn @TableName, 'CountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'IsPhoneVerified', 'BIT'
EXEC utility.AddColumn @TableName, 'Phone', 'VARCHAR(50)'

EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsPhoneVerified', 'BIT', 0
GO
--End table dbo.Person

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.AddColumn @TableName, 'ISOCountryCode2', 'CHAR(2)'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dropdown.Country') AND SC.Name = 'ISOCountryCode')
	EXEC sp_RENAME 'dropdown.Country.ISOCountryCode', 'ISOCountryCode3', 'COLUMN';
--ENDIF
GO

UPDATE C SET C.ISOCountryCode2 = 'AW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ABW'
UPDATE C SET C.ISOCountryCode2 = 'AF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AFG'
UPDATE C SET C.ISOCountryCode2 = 'AO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AGO'
UPDATE C SET C.ISOCountryCode2 = 'AI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AIA'
UPDATE C SET C.ISOCountryCode2 = 'AX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALA'
UPDATE C SET C.ISOCountryCode2 = 'AL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALB'
UPDATE C SET C.ISOCountryCode2 = 'AD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AND'
UPDATE C SET C.ISOCountryCode2 = 'AE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARE'
UPDATE C SET C.ISOCountryCode2 = 'AR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARG'
UPDATE C SET C.ISOCountryCode2 = 'AM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARM'
UPDATE C SET C.ISOCountryCode2 = 'AS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ASM'
UPDATE C SET C.ISOCountryCode2 = 'AQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATA'
UPDATE C SET C.ISOCountryCode2 = 'TF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATF'
UPDATE C SET C.ISOCountryCode2 = 'AG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATG'
UPDATE C SET C.ISOCountryCode2 = 'AU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUS'
UPDATE C SET C.ISOCountryCode2 = 'AT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUT'
UPDATE C SET C.ISOCountryCode2 = 'AZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AZE'
UPDATE C SET C.ISOCountryCode2 = 'BI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BDI'
UPDATE C SET C.ISOCountryCode2 = 'BE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEL'
UPDATE C SET C.ISOCountryCode2 = 'BJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEN'
UPDATE C SET C.ISOCountryCode2 = 'BF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BFA'
UPDATE C SET C.ISOCountryCode2 = 'BD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGD'
UPDATE C SET C.ISOCountryCode2 = 'BG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGR'
UPDATE C SET C.ISOCountryCode2 = 'BH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHR'
UPDATE C SET C.ISOCountryCode2 = 'BS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHS'
UPDATE C SET C.ISOCountryCode2 = 'BA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BIH'
UPDATE C SET C.ISOCountryCode2 = 'BL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLM'
UPDATE C SET C.ISOCountryCode2 = 'BY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLR'
UPDATE C SET C.ISOCountryCode2 = 'BZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLZ'
UPDATE C SET C.ISOCountryCode2 = 'BM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BMU'
UPDATE C SET C.ISOCountryCode2 = 'BO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BOL'
UPDATE C SET C.ISOCountryCode2 = 'BR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRA'
UPDATE C SET C.ISOCountryCode2 = 'BB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRB'
UPDATE C SET C.ISOCountryCode2 = 'BN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRN'
UPDATE C SET C.ISOCountryCode2 = 'BT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BTN'
UPDATE C SET C.ISOCountryCode2 = 'BV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BVT'
UPDATE C SET C.ISOCountryCode2 = 'BW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BWA'
UPDATE C SET C.ISOCountryCode2 = 'CF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAF'
UPDATE C SET C.ISOCountryCode2 = 'CA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAN'
UPDATE C SET C.ISOCountryCode2 = 'CC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CCK'
UPDATE C SET C.ISOCountryCode2 = 'CH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHE'
UPDATE C SET C.ISOCountryCode2 = 'CL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHL'
UPDATE C SET C.ISOCountryCode2 = 'CN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHN'
UPDATE C SET C.ISOCountryCode2 = 'CI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CIV'
UPDATE C SET C.ISOCountryCode2 = 'CM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CMR'
UPDATE C SET C.ISOCountryCode2 = 'CD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COD'
UPDATE C SET C.ISOCountryCode2 = 'CG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COG'
UPDATE C SET C.ISOCountryCode2 = 'CK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COK'
UPDATE C SET C.ISOCountryCode2 = 'CO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COL'
UPDATE C SET C.ISOCountryCode2 = 'KM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COM'
UPDATE C SET C.ISOCountryCode2 = 'CV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CPV'
UPDATE C SET C.ISOCountryCode2 = 'CR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CRI'
UPDATE C SET C.ISOCountryCode2 = 'CU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUB'
UPDATE C SET C.ISOCountryCode2 = 'CW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUW'
UPDATE C SET C.ISOCountryCode2 = 'CX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CXR'
UPDATE C SET C.ISOCountryCode2 = 'KY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYM'
UPDATE C SET C.ISOCountryCode2 = 'CY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYP'
UPDATE C SET C.ISOCountryCode2 = 'CZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CZE'
UPDATE C SET C.ISOCountryCode2 = 'DE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DEU'
UPDATE C SET C.ISOCountryCode2 = 'DJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DJI'
UPDATE C SET C.ISOCountryCode2 = 'DM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DMA'
UPDATE C SET C.ISOCountryCode2 = 'DK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DNK'
UPDATE C SET C.ISOCountryCode2 = 'DO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'
UPDATE C SET C.ISOCountryCode2 = 'DZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DZA'
UPDATE C SET C.ISOCountryCode2 = 'EC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ECU'
UPDATE C SET C.ISOCountryCode2 = 'EG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EGY'
UPDATE C SET C.ISOCountryCode2 = 'ER' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ERI'
UPDATE C SET C.ISOCountryCode2 = 'EH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESH'
UPDATE C SET C.ISOCountryCode2 = 'ES' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESP'
UPDATE C SET C.ISOCountryCode2 = 'EE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EST'
UPDATE C SET C.ISOCountryCode2 = 'ET' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ETH'
UPDATE C SET C.ISOCountryCode2 = 'FI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FIN'
UPDATE C SET C.ISOCountryCode2 = 'FJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FJI'
UPDATE C SET C.ISOCountryCode2 = 'FK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FLK'
UPDATE C SET C.ISOCountryCode2 = 'FR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRA'
UPDATE C SET C.ISOCountryCode2 = 'FO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRO'
UPDATE C SET C.ISOCountryCode2 = 'FM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FSM'
UPDATE C SET C.ISOCountryCode2 = 'GA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GAB'
UPDATE C SET C.ISOCountryCode2 = 'GB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GBR'
UPDATE C SET C.ISOCountryCode2 = 'GE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GEO'
UPDATE C SET C.ISOCountryCode2 = 'GG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GGY'
UPDATE C SET C.ISOCountryCode2 = 'GH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GHA'
UPDATE C SET C.ISOCountryCode2 = 'GI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIB'
UPDATE C SET C.ISOCountryCode2 = 'GN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIN'
UPDATE C SET C.ISOCountryCode2 = 'GP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GLP'
UPDATE C SET C.ISOCountryCode2 = 'GM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GMB'
UPDATE C SET C.ISOCountryCode2 = 'GW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNB'
UPDATE C SET C.ISOCountryCode2 = 'GQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNQ'
UPDATE C SET C.ISOCountryCode2 = 'GR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRC'
UPDATE C SET C.ISOCountryCode2 = 'GD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRD'
UPDATE C SET C.ISOCountryCode2 = 'GL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRL'
UPDATE C SET C.ISOCountryCode2 = 'GT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GTM'
UPDATE C SET C.ISOCountryCode2 = 'GF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUF'
UPDATE C SET C.ISOCountryCode2 = 'GU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUM'
UPDATE C SET C.ISOCountryCode2 = 'GY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUY'
UPDATE C SET C.ISOCountryCode2 = 'HK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HKG'
UPDATE C SET C.ISOCountryCode2 = 'HM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HMD'
UPDATE C SET C.ISOCountryCode2 = 'HN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HND'
UPDATE C SET C.ISOCountryCode2 = 'HR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HRV'
UPDATE C SET C.ISOCountryCode2 = 'HT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HTI'
UPDATE C SET C.ISOCountryCode2 = 'HU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HUN'
UPDATE C SET C.ISOCountryCode2 = 'ID' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IDN'
UPDATE C SET C.ISOCountryCode2 = 'IM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IMN'
UPDATE C SET C.ISOCountryCode2 = 'IN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IND'
UPDATE C SET C.ISOCountryCode2 = 'IO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IOT'
UPDATE C SET C.ISOCountryCode2 = 'IE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRL'
UPDATE C SET C.ISOCountryCode2 = 'IR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRN'
UPDATE C SET C.ISOCountryCode2 = 'IQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRQ'
UPDATE C SET C.ISOCountryCode2 = 'IS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISL'
UPDATE C SET C.ISOCountryCode2 = 'IL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISR'
UPDATE C SET C.ISOCountryCode2 = 'IT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ITA'
UPDATE C SET C.ISOCountryCode2 = 'JM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JAM'
UPDATE C SET C.ISOCountryCode2 = 'JE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JEY'
UPDATE C SET C.ISOCountryCode2 = 'JO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JOR'
UPDATE C SET C.ISOCountryCode2 = 'JP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JPN'
UPDATE C SET C.ISOCountryCode2 = 'KZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KAZ'
UPDATE C SET C.ISOCountryCode2 = 'KE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KEN'
UPDATE C SET C.ISOCountryCode2 = 'KG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KGZ'
UPDATE C SET C.ISOCountryCode2 = 'KH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KHM'
UPDATE C SET C.ISOCountryCode2 = 'KI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KIR'
UPDATE C SET C.ISOCountryCode2 = 'KN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KNA'
UPDATE C SET C.ISOCountryCode2 = 'KR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOR'
UPDATE C SET C.ISOCountryCode2 = 'XK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOS'
UPDATE C SET C.ISOCountryCode2 = 'KW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KWT'
UPDATE C SET C.ISOCountryCode2 = 'LA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LAO'
UPDATE C SET C.ISOCountryCode2 = 'LB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBN'
UPDATE C SET C.ISOCountryCode2 = 'LR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBR'
UPDATE C SET C.ISOCountryCode2 = 'LY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBY'
UPDATE C SET C.ISOCountryCode2 = 'LC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LCA'
UPDATE C SET C.ISOCountryCode2 = 'LI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LIE'
UPDATE C SET C.ISOCountryCode2 = 'LK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LKA'
UPDATE C SET C.ISOCountryCode2 = 'LS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LSO'
UPDATE C SET C.ISOCountryCode2 = 'LT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LTU'
UPDATE C SET C.ISOCountryCode2 = 'LU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LUX'
UPDATE C SET C.ISOCountryCode2 = 'LV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LVA'
UPDATE C SET C.ISOCountryCode2 = 'MO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAC'
UPDATE C SET C.ISOCountryCode2 = 'MF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAF'
UPDATE C SET C.ISOCountryCode2 = 'MA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAR'
UPDATE C SET C.ISOCountryCode2 = 'MC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MCO'
UPDATE C SET C.ISOCountryCode2 = 'MD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDA'
UPDATE C SET C.ISOCountryCode2 = 'MG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDG'
UPDATE C SET C.ISOCountryCode2 = 'MV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDV'
UPDATE C SET C.ISOCountryCode2 = 'MX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MEX'
UPDATE C SET C.ISOCountryCode2 = 'MH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MHL'
UPDATE C SET C.ISOCountryCode2 = 'MK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MKD'
UPDATE C SET C.ISOCountryCode2 = 'ML' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLI'
UPDATE C SET C.ISOCountryCode2 = 'MT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLT'
UPDATE C SET C.ISOCountryCode2 = 'MM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MMR'
UPDATE C SET C.ISOCountryCode2 = 'ME' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNE'
UPDATE C SET C.ISOCountryCode2 = 'MN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNG'
UPDATE C SET C.ISOCountryCode2 = 'MP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNP'
UPDATE C SET C.ISOCountryCode2 = 'MZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MOZ'
UPDATE C SET C.ISOCountryCode2 = 'MR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MRT'
UPDATE C SET C.ISOCountryCode2 = 'MS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MSR'
UPDATE C SET C.ISOCountryCode2 = 'MQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MTQ'
UPDATE C SET C.ISOCountryCode2 = 'MU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MUS'
UPDATE C SET C.ISOCountryCode2 = 'MW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MWI'
UPDATE C SET C.ISOCountryCode2 = 'MY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYS'
UPDATE C SET C.ISOCountryCode2 = 'YT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYT'
UPDATE C SET C.ISOCountryCode2 = 'NA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NAM'
UPDATE C SET C.ISOCountryCode2 = 'NC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NCL'
UPDATE C SET C.ISOCountryCode2 = 'NE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NER'
UPDATE C SET C.ISOCountryCode2 = 'NF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NFK'
UPDATE C SET C.ISOCountryCode2 = 'NG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NGA'
UPDATE C SET C.ISOCountryCode2 = 'NI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIC'
UPDATE C SET C.ISOCountryCode2 = 'NU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIU'
UPDATE C SET C.ISOCountryCode2 = 'NL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NLD'
UPDATE C SET C.ISOCountryCode2 = 'NO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NOR'
UPDATE C SET C.ISOCountryCode2 = 'NP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NPL'
UPDATE C SET C.ISOCountryCode2 = 'NR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NRU'
UPDATE C SET C.ISOCountryCode2 = 'NZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NZL'
UPDATE C SET C.ISOCountryCode2 = 'OM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'OMN'
UPDATE C SET C.ISOCountryCode2 = 'PK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAK'
UPDATE C SET C.ISOCountryCode2 = 'PA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAN'
UPDATE C SET C.ISOCountryCode2 = 'PN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PCN'
UPDATE C SET C.ISOCountryCode2 = 'PE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PER'
UPDATE C SET C.ISOCountryCode2 = 'PH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PHL'
UPDATE C SET C.ISOCountryCode2 = 'PW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PLW'
UPDATE C SET C.ISOCountryCode2 = 'PG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PNG'
UPDATE C SET C.ISOCountryCode2 = 'PL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'POL'
UPDATE C SET C.ISOCountryCode2 = 'PR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'
UPDATE C SET C.ISOCountryCode2 = 'KP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRK'
UPDATE C SET C.ISOCountryCode2 = 'PT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRT'
UPDATE C SET C.ISOCountryCode2 = 'PY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRY'
UPDATE C SET C.ISOCountryCode2 = 'PS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PSE'
UPDATE C SET C.ISOCountryCode2 = 'PF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PYF'
UPDATE C SET C.ISOCountryCode2 = 'QA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'QAT'
UPDATE C SET C.ISOCountryCode2 = 'RE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'REU'
UPDATE C SET C.ISOCountryCode2 = 'RO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ROU'
UPDATE C SET C.ISOCountryCode2 = 'RU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RUS'
UPDATE C SET C.ISOCountryCode2 = 'RW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RWA'
UPDATE C SET C.ISOCountryCode2 = 'SA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SAU'
UPDATE C SET C.ISOCountryCode2 = 'SD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SDN'
UPDATE C SET C.ISOCountryCode2 = 'SN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SEN'
UPDATE C SET C.ISOCountryCode2 = 'SG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGP'
UPDATE C SET C.ISOCountryCode2 = 'GS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGS'
UPDATE C SET C.ISOCountryCode2 = 'SJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SJM'
UPDATE C SET C.ISOCountryCode2 = 'SB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLB'
UPDATE C SET C.ISOCountryCode2 = 'SL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLE'
UPDATE C SET C.ISOCountryCode2 = 'SV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLV'
UPDATE C SET C.ISOCountryCode2 = 'SM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SMR'
UPDATE C SET C.ISOCountryCode2 = 'SO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SOM'
UPDATE C SET C.ISOCountryCode2 = 'PM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SPM'
UPDATE C SET C.ISOCountryCode2 = 'RS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SRB'
UPDATE C SET C.ISOCountryCode2 = 'SS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SSD'
UPDATE C SET C.ISOCountryCode2 = 'ST' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'STP'
UPDATE C SET C.ISOCountryCode2 = 'SR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SUR'
UPDATE C SET C.ISOCountryCode2 = 'SK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVK'
UPDATE C SET C.ISOCountryCode2 = 'SI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVN'
UPDATE C SET C.ISOCountryCode2 = 'SE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWE'
UPDATE C SET C.ISOCountryCode2 = 'SZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWZ'
UPDATE C SET C.ISOCountryCode2 = 'SX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SXM'
UPDATE C SET C.ISOCountryCode2 = 'SC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYC'
UPDATE C SET C.ISOCountryCode2 = 'SY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYR'
UPDATE C SET C.ISOCountryCode2 = 'TC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCA'
UPDATE C SET C.ISOCountryCode2 = 'TD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCD'
UPDATE C SET C.ISOCountryCode2 = 'TG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TGO'
UPDATE C SET C.ISOCountryCode2 = 'TH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'THA'
UPDATE C SET C.ISOCountryCode2 = 'TJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TJK'
UPDATE C SET C.ISOCountryCode2 = 'TK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKL'
UPDATE C SET C.ISOCountryCode2 = 'TM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKM'
UPDATE C SET C.ISOCountryCode2 = 'TL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TLS'
UPDATE C SET C.ISOCountryCode2 = 'TO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TON'
UPDATE C SET C.ISOCountryCode2 = 'TT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TTO'
UPDATE C SET C.ISOCountryCode2 = 'TN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUN'
UPDATE C SET C.ISOCountryCode2 = 'TR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUR'
UPDATE C SET C.ISOCountryCode2 = 'TV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUV'
UPDATE C SET C.ISOCountryCode2 = 'TW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TWN'
UPDATE C SET C.ISOCountryCode2 = 'TZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TZA'
UPDATE C SET C.ISOCountryCode2 = 'UG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UGA'
UPDATE C SET C.ISOCountryCode2 = 'UA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UKR'
UPDATE C SET C.ISOCountryCode2 = 'UM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UMI'
UPDATE C SET C.ISOCountryCode2 = 'UY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'URY'
UPDATE C SET C.ISOCountryCode2 = 'US' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'USA'
UPDATE C SET C.ISOCountryCode2 = 'UZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UZB'
UPDATE C SET C.ISOCountryCode2 = 'VA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'
UPDATE C SET C.ISOCountryCode2 = 'VC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VCT'
UPDATE C SET C.ISOCountryCode2 = 'VE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VEN'
UPDATE C SET C.ISOCountryCode2 = 'VG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VGB'
UPDATE C SET C.ISOCountryCode2 = 'VI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VIR'
UPDATE C SET C.ISOCountryCode2 = 'VN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VNM'
UPDATE C SET C.ISOCountryCode2 = 'VU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VUT'
UPDATE C SET C.ISOCountryCode2 = 'WF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WLF'
UPDATE C SET C.ISOCountryCode2 = 'WS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WSM'
UPDATE C SET C.ISOCountryCode2 = 'YE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'YEM'
UPDATE C SET C.ISOCountryCode2 = 'ZA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZAF'
UPDATE C SET C.ISOCountryCode2 = 'ZM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZMB'
UPDATE C SET C.ISOCountryCode2 = 'ZW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZWE'
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
DECLARE @TableName VARCHAR(250) = 'dropdown.CountryCallingCode'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CountryCallingCode
	(
	CountryCallingCodeID INT IDENTITY(1,1) NOT NULL,
	CountryID INT,
	CountryCallingCode INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCode', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryCallingCodeID'
EXEC utility.SetIndexClustered 'IX_CountryCallingCode', @TableName, 'CountryID,CountryCallingCode'
GO

INSERT INTO dropdown.CountryCallingCode 
	(CountryID,CountryCallingCode) 
VALUES 
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ABW'),297),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AFG'),93),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AGO'),244),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AIA'),1264),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALA'),358),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALB'),355),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AND'),376),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARE'),971),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARG'),54),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARM'),374),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ASM'),1684),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATA'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATF'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATG'),1268),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUS'),61),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUT'),43),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AZE'),994),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BDI'),257),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEL'),32),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEN'),229),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BFA'),226),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGD'),880),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGR'),359),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHR'),973),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHS'),1242),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BIH'),387),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLM'),590),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLR'),375),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLZ'),501),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BMU'),1441),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BOL'),591),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRA'),55),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRB'),1246),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRN'),673),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BTN'),975),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BVT'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BWA'),267),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAF'),236),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAN'),1),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CCK'),61),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHE'),41),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHL'),56),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHN'),86),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CIV'),225),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CMR'),237),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COD'),243),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COG'),242),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COK'),682),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COL'),57),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COM'),269),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CPV'),238),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CRI'),506),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUB'),53),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUW'),5999),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CXR'),61),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYM'),1345),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYP'),357),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CZE'),420),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DEU'),49),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DJI'),253),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DMA'),1767),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DNK'),45),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),180),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),918),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),291),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),849),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DZA'),213),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ECU'),593),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EGY'),20),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ERI'),291),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESH'),212),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESP'),34),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EST'),372),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ETH'),251),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FIN'),358),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FJI'),679),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FLK'),500),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRA'),33),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRO'),298),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FSM'),691),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GAB'),241),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GBR'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GEO'),995),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GGY'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GHA'),233),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIB'),350),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIN'),224),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GLP'),590),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GMB'),220),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNB'),245),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNQ'),240),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRC'),30),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRD'),1473),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRL'),299),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GTM'),502),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUF'),594),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUM'),1671),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUY'),592),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HKG'),852),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HMD'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HND'),504),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HRV'),385),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HTI'),509),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HUN'),36),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IDN'),62),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IMN'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IND'),91),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IOT'),246),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRL'),353),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRN'),98),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRQ'),964),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISL'),354),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISR'),972),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ITA'),39),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JAM'),1876),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JEY'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JOR'),962),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JPN'),81),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KAZ'),76),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KAZ'),77),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KEN'),254),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KGZ'),996),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KHM'),855),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KIR'),686),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KNA'),1869),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOR'),82),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOS'),383),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KWT'),965),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LAO'),856),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBN'),961),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBR'),231),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBY'),218),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LCA'),1758),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LIE'),423),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LKA'),94),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LSO'),266),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LTU'),370),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LUX'),352),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LVA'),371),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAC'),853),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAF'),590),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAR'),212),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MCO'),377),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDA'),373),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDG'),261),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDV'),960),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MEX'),52),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MHL'),692),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MKD'),389),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLI'),223),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLT'),356),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MMR'),95),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNE'),382),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNG'),976),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNP'),1670),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MOZ'),258),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MRT'),222),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MSR'),1664),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MTQ'),596),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MUS'),230),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MWI'),265),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYS'),60),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYT'),262),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NAM'),264),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NCL'),687),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NER'),227),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NFK'),672),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NGA'),234),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIC'),505),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIU'),683),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NLD'),31),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NOR'),47),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NPL'),977),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NRU'),674),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NZL'),64),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'OMN'),968),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAK'),92),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAN'),507),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PCN'),64),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PER'),51),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PHL'),63),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PLW'),680),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PNG'),675),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'POL'),48),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'),17),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'),871),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'),939),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRK'),850),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRT'),351),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRY'),595),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PSE'),970),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PYF'),689),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'QAT'),974),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'REU'),262),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ROU'),40),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RUS'),7),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RWA'),250),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SAU'),966),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SDN'),249),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SEN'),221),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGP'),65),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGS'),500),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SJM'),4779),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLB'),677),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLE'),232),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLV'),503),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SMR'),378),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SOM'),252),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SPM'),508),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SRB'),381),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SSD'),211),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'STP'),239),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SUR'),597),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVK'),421),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVN'),386),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWE'),46),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWZ'),268),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SXM'),1721),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYC'),248),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYR'),963),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCA'),1649),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCD'),235),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TGO'),228),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'THA'),66),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TJK'),992),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKL'),690),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKM'),993),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TLS'),670),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TON'),676),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TTO'),1868),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUN'),216),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUR'),90),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUV'),688),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TWN'),886),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TZA'),255),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UGA'),256),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UKR'),380),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UMI'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'URY'),598),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'USA'),1),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UZB'),998),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),3),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),906),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),698),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),379),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VCT'),1784),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VEN'),58),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VGB'),1284),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VIR'),1340),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VNM'),84),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VUT'),678),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WLF'),681),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WSM'),685),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'YEM'),967),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZAF'),27),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZMB'),260),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZWE'),263)
GO
--End table dropdown.CountryCallingCode

--Begin table survey.Survey
DECLARE @TableName VARCHAR(250) = 'survey.Survey'

EXEC utility.DropObject @TableName

CREATE TABLE survey.Survey
	(
	SurveyID INT IDENTITY(1,1) NOT NULL,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyID'
GO
--End table survey.Survey

--Begin table survey.SurveyLabel
DECLARE @TableName VARCHAR(250) = 'survey.SurveyLabel'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyLabel
	(
	SurveyLabelID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	LanguageCode CHAR(2),
	SurveyName NVARCHAR(500),
	SurveyDescription NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyLabelID'
EXEC utility.SetIndexClustered 'IX_SurveyLabel', @TableName, 'SurveyID,LanguageCode'
GO
--End table survey.SurveyLabel

--Begin table survey.SurveyQuestionLabel
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionLabel'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionLabel
	(
	SurveyQuestionLabelID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	SurveyQuestionText NVARCHAR(500),
	SurveyQuestionDescription NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionLabelID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionLabel', @TableName, 'SurveyQuestionID,LanguageCode'
GO
--End table survey.SurveyQuestionLabel

--Begin table survey.SurveyQuestionResponseChoice
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionResponseChoice'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionResponseChoice
	(
	SurveyQuestionResponseChoiceID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionCode VARCHAR(50),
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	SurveyQuestionResponseChoiceValue VARCHAR(50),
	SurveyQuestionResponseChoiceText NVARCHAR(500),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionResponseChoiceID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionResponseChoice', @TableName, 'SurveyQuestionID,LanguageCode,DisplayOrder'
GO

INSERT INTO survey.SurveyQuestionResponseChoice
	(SurveyQuestionCode,LanguageCode,SurveyQuestionResponseChoiceValue,SurveyQuestionResponseChoiceText,DisplayOrder)
VALUES
	('YesNo','AR','1',N'�?ع�?',1),
	('YesNo','AR','0',N'�?ا',2),
	('YesNo','EN','1','Yes',1),
	('YesNo','EN','0','No',2)
GO
--End table survey.SurveyQuestionResponseChoice

--Begin table survey.SurveyResponse
DECLARE @TableName VARCHAR(250) = 'survey.SurveyResponse'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyResponse
	(
	SurveyResponseID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	LanguageCode CHAR(2),
	ResponseDateTime DATETIME,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ResponseDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyResponseID'
EXEC utility.SetIndexClustered 'IX_SurveySurveyQuestionResponse', @TableName, 'SurveyID,LanguageCode'
GO
--End table survey.SurveySurveyQuestionResponse

--Begin table survey.SurveySurveyQuestionResponse
DECLARE @TableName VARCHAR(250) = 'survey.SurveySurveyQuestionResponse'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveySurveyQuestionResponse
	(
	SurveySurveyQuestionResponseID INT IDENTITY(1,1) NOT NULL,
	SurveyResponseID INT,
	SurveyQuestionID INT,
	SurveyQuestionResponse NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyResponseID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveySurveyQuestionResponseID'
EXEC utility.SetIndexClustered 'IX_SurveySurveyQuestionResponse', @TableName, 'SurveyResponseID,SurveyQuestionID'
GO
--End table survey.SurveySurveyQuestionResponse


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.ValidatePassword
EXEC utility.DropObject 'dbo.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create date:	2015.08.01
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION dbo.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = Password,
		@cPasswordSalt = PasswordSalt
	FROM dbo.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

		WHILE (@nI < 65536)
			BEGIN

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
			SET @nI = @nI + 1

			END
		--END WHILE

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.ValidatePassword

--Begin function survey.GetAdministerButtonHTMLBySurveyID
EXEC utility.DropObject 'survey.GetAdministerButtonHTMLBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.01
-- Description:	A function to return an html string for a survey Administer button
-- ===============================================================================

CREATE FUNCTION survey.GetAdministerButtonHTMLBySurveyID
(
@SurveyID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cHTML VARCHAR(MAX) = ''

	SELECT @cHTML = 
		STUFF((SELECT '<br><a href="/surveymanagement/administersurvey/id/' + CAST(SL.SurveyID AS VARCHAR(10)) + '?LanguageCode=' + SL.LanguageCode + '">' + L.LanguageName + '</a>' 
		FROM survey.SurveyLanguage SL 
			JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
		WHERE SL.SurveyID = @SurveyID
		ORDER BY SL.LanguageCode 
		FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'), 1, 4, '')

	RETURN ISNULL(@cHTML, '')

END
GO
--End function survey.GetAdministerButtonHTMLBySurveyID

--Begin function survey.GetViewButtonHTMLBySurveyID
EXEC utility.DropObject 'survey.GetViewButtonHTMLBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.01
-- Description:	A function to return an html string for a survey view button
-- =========================================================================

CREATE FUNCTION survey.GetViewButtonHTMLBySurveyID
(
@SurveyID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cHTML VARCHAR(MAX) = ''

	SELECT @cHTML = 
		STUFF((SELECT '<br><a href="/surveymanagement/viewsurvey/id/' + CAST(SL.SurveyID AS VARCHAR(10)) + '?LanguageCode=' + SL.LanguageCode + '">' + L.LanguageName + '</a>' 
		FROM survey.SurveyLanguage SL 
			JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
		WHERE SL.SurveyID = @SurveyID
		ORDER BY SL.LanguageCode 
		FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'), 1, 4, '')

	RETURN ISNULL(@cHTML, '')

END
GO
--End function survey.GetViewButtonHTMLBySurveyID

--Begin function utility.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'utility.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date:	2015.08.02
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION utility.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM dbo.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function utility.GetDescendantMenuItemsByMenuItemCode



--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage = MIPL.PermissionableLineage
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage = MIPL.PermissionableLineage
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Added the IsAccountLockedOut bit
--
-- Author:			John Lyons
-- Create date:	2015.07.29
-- Description:	Added two factor support
-- ============================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.FirstName, 
		P.IsAccountLockedOut,
		P.LastName,
		P.Organization,
		P.PersonID,
		P.RoleID, 
		P.Title,
		P.UserName,
		R.RoleName,
		P.Phone, 
		P.CountryCallingCodeID,
		P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.GetRiskByRiskID
EXEC Utility.DropObject 'dbo.GetRiskByRiskID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===========================================================================
CREATE PROCEDURE dbo.GetRiskByRiskID

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName
	FROM dbo.Risk R
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND R.RiskID = @RiskID

END
GO
--End procedure dbo.GetRiskByRiskID

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Adding password expiration support
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT


	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cPhone =P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin

--Begin procedure eventlog.LogConceptNoteAction
EXEC utility.DropObject 'eventlog.LogConceptNoteAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogConceptNoteAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cConceptNoteBudget VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteBudget = COALESCE(@cConceptNoteBudget, '') + D.ConceptNoteBudget
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteBudget'), ELEMENTS) AS ConceptNoteBudget
			FROM dbo.ConceptNoteBudget T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteClass VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteClass = COALESCE(@cConceptNoteClass, '') + D.ConceptNoteClass
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteClass'), ELEMENTS) AS ConceptNoteClass
			FROM dbo.ConceptNoteClass T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteCommunities VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCommunities = COALESCE(@cConceptNoteCommunities, '') + D.ConceptNoteCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCommunity'), ELEMENTS) AS ConceptNoteCommunity
			FROM dbo.ConceptNoteCommunity T 
			WHERE T.ConceptNoteID = @EntityID
			) D
			
		DECLARE @cConceptNoteContact VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteContact= COALESCE(@cConceptNoteContact, '') + D.ConceptNoteContact
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteContact'), ELEMENTS) AS ConceptNoteContact
			FROM dbo.ConceptNoteContact T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteCourse VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCourse= COALESCE(@cConceptNoteCourse, '') + D.ConceptNoteCourse
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCourse'), ELEMENTS) AS ConceptNoteCourse
			FROM dbo.ConceptNoteCourse T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteEquipmentCatalog  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteEquipmentCatalog = COALESCE(@cConceptNoteEquipmentCatalog , '') + D.ConceptNoteEquipmentCatalog 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteEquipmentCatalog'), ELEMENTS) AS ConceptNoteEquipmentCatalog 
			FROM dbo.ConceptNoteEquipmentCatalog  T 
			WHERE T.ConceptNoteID = @EntityID
			) D				
			
		DECLARE @cConceptNoteIndicator  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteIndicator = COALESCE(@cConceptNoteIndicator , '') + D.ConceptNoteIndicator 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteIndicator'), ELEMENTS) AS ConceptNoteIndicator 
			FROM dbo.ConceptNoteIndicator  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteProvinces VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteProvinces = COALESCE(@cConceptNoteProvinces, '') + D.ConceptNoteProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteProvince'), ELEMENTS) AS ConceptNoteProvince
			FROM dbo.ConceptNoteProvince T 
			WHERE T.ConceptNoteID = @EntityID
			) D			

		DECLARE @cConceptNoteTask  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteTask = COALESCE(@cConceptNoteTask , '') + D.ConceptNoteTask 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteTask'), ELEMENTS) AS ConceptNoteTask 
			FROM dbo.ConceptNoteTask  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteRisk VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteRisk = COALESCE(@cConceptNoteRisk, '') + D.ConceptNoteRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteRisk'), ELEMENTS) AS ConceptNoteRisk
			FROM dbo.ConceptNoteRisk T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ConceptNoteBudgets>' + ISNULL(@cConceptNoteBudget , '') + '</ConceptNoteBudgets>') AS XML), 
			CAST(('<ConceptNoteClasses>' + ISNULL(@cConceptNoteClass , '') + '</ConceptNoteClasses>') AS XML), 
			CAST(('<ConceptNoteCommunities>' + ISNULL(@cConceptNoteCommunities , '') + '</ConceptNoteCommunities>') AS XML), 
			CAST(('<ConceptNoteContacts>' + ISNULL(@cConceptNoteContact , '') + '</ConceptNoteContacts>') AS XML), 
			CAST(('<ConceptNoteCourses>' + ISNULL(@cConceptNoteCourse , '') + '</ConceptNoteCourses>') AS XML), 
			CAST(('<ConceptNoteEquipmentCatalogs>' + ISNULL(@cConceptNoteEquipmentCatalog  , '') + '</ConceptNoteEquipmentCatalogs>') AS XML), 
			CAST(('<ConceptNoteIndicators>' + ISNULL(@cConceptNoteIndicator  , '') + '</ConceptNoteIndicators>') AS XML), 
			CAST(('<ConceptNoteProvinces>' + ISNULL(@cConceptNoteProvinces , '') + '</ConceptNoteProvinces>') AS XML), 
			CAST(('<ConceptNoteTasks>' + ISNULL(@cConceptNoteTask  , '') + '</ConceptNoteTasks>') AS XML),
			CAST(('<ConceptNoteRisks>' + ISNULL(@cConceptNoteRisk  , '') + '</ConceptNoteRisks>') AS XML)
			FOR XML RAW('ConceptNote'), ELEMENTS
			)
		FROM dbo.ConceptNote T
		WHERE T.ConceptNoteID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogConceptNoteAction

--Begin procedure eventlog.LogRiskAction
EXEC utility.DropObject 'eventlog.LogRiskAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRiskAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Risk',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Risk',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Risk'), ELEMENTS
			)
		FROM dbo.Risk T
		WHERE T.RiskID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRiskAction

--Begin procedure eventlog.LogSurveyAction
EXEC utility.DropObject 'eventlog.LogSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Survey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSurveyLabels NVARCHAR(MAX) 
	
		SELECT 
			@cSurveyLabels = COALESCE(@cSurveyLabels, '') + D.SurveyLabels 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyLabels'), ELEMENTS) AS SurveyLabels
			FROM survey.SurveyLabel T 
			WHERE T.SurveyID = @EntityID
			) D

		DECLARE @cSurveyLanguages VARCHAR(MAX) 
	
		SELECT 
			@cSurveyLanguages = COALESCE(@cSurveyLanguages, '') + D.SurveyLanguages 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyLanguages'), ELEMENTS) AS SurveyLanguages
			FROM survey.SurveyLanguage T 
			WHERE T.SurveyID = @EntityID
			) D

		DECLARE @cSurveySurveyQuestions NVARCHAR(MAX) 
	
		SELECT 
			@cSurveySurveyQuestions = COALESCE(@cSurveySurveyQuestions, '') + D.SurveySurveyQuestions 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveySurveyQuestions'), ELEMENTS) AS SurveySurveyQuestions
			FROM survey.SurveySurveyQuestion T 
			WHERE T.SurveyID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Survey',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<SurveyLabels>' + ISNULL(@cSurveyLabels, '') + '</SurveyLabels>') AS XML),
			CAST(('<SurveyLanguages>' + ISNULL(@cSurveyLanguages, '') + '</SurveyLanguages>') AS XML),
			CAST(('<SurveySurveyQuestions>' + ISNULL(@cSurveySurveyQuestions, '') + '</SurveySurveyQuestions>') AS XML)
			FOR XML RAW('Survey'), ELEMENTS
			)
		FROM survey.Survey T
		WHERE T.SurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSurveyAction

--Begin procedure eventlog.LogSurveyQuestionAction
EXEC utility.DropObject 'eventlog.LogSurveyQuestionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSurveyQuestionAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SurveyQuestion',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSurveyQuestionLabels NVARCHAR(MAX) 
	
		SELECT 
			@cSurveyQuestionLabels = COALESCE(@cSurveyQuestionLabels, '') + D.SurveyQuestionLabels 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyQuestionLabels'), ELEMENTS) AS SurveyQuestionLabels
			FROM survey.SurveyQuestionLabel T 
			WHERE T.SurveyQuestionID = @EntityID
			) D

		DECLARE @cSurveyQuestionLanguages VARCHAR(MAX) 
	
		SELECT 
			@cSurveyQuestionLanguages = COALESCE(@cSurveyQuestionLanguages, '') + D.SurveyQuestionLanguages 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyQuestionLanguages'), ELEMENTS) AS SurveyQuestionLanguages
			FROM survey.SurveyQuestionLanguage T 
			WHERE T.SurveyQuestionID = @EntityID
			) D

		DECLARE @cSurveyQuestionResponseChoices NVARCHAR(MAX) 
	
		SELECT 
			@cSurveyQuestionResponseChoices = COALESCE(@cSurveyQuestionResponseChoices, '') + D.SurveyQuestionResponseChoices 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveyQuestionResponseChoices'), ELEMENTS) AS SurveyQuestionResponseChoices
			FROM survey.SurveyQuestionResponseChoice T 
			WHERE T.SurveyQuestionID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SurveyQuestion',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<SurveyQuestionLabels>' + ISNULL(@cSurveyQuestionLabels, '') + '</SurveyQuestionLabels>') AS XML),
			CAST(('<SurveyQuestionLanguages>' + ISNULL(@cSurveyQuestionLanguages, '') + '</SurveyQuestionLanguages>') AS XML),
			CAST(('<SurveyQuestionResponseChoices>' + ISNULL(@cSurveyQuestionResponseChoices, '') + '</SurveyQuestionResponseChoices>') AS XML)
			FOR XML RAW('Survey'), ELEMENTS
			)
		FROM survey.SurveyQuestion T
		WHERE T.SurveyQuestionID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSurveyQuestionAction

--Begin procedure reporting.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.06
-- Description:	A stored procedure to get data for the Concept Note Budget Export
-- ==============================================================================
CREATE PROCEDURE reporting.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CNBD AS
		(
		SELECT
			BT.BudgetTypeName,
			BST.BudgetSubTypeName,
			CNB.ConceptNoteBudgetID AS EntityID,
			CNB.ItemName,
			CNB.ItemDescription,
			CNB.UnitCost,
			FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNB.Quantity,
			FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue as TotalCost, 
			CNB.UnitOfIssue,
			CNB.QuantityOfIssue
		FROM dbo.ConceptNoteBudget CNB
				JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
				JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
					AND CNB.conceptnoteid = @ConceptNoteID

		UNION ALL

		SELECT
			'Equipment' as BudgetTypeName,
			'' as BudgetSubTypeName,
			CNEC.ConceptNoteEquipmentCatalogID AS EntityID,
			EC.ItemName,
			EC.ItemDescription,
			EC.UnitCost,
			FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNEC.Quantity,
			FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
			EC.UnitOfIssue,
			EC.QuantityOfIssue
		FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = @ConceptNoteID
		)

	SELECT
		BTS.BudgetTypeSequence,
		BSS.BudgetSubTypeSequence,
		ROW_NUMBER() OVER (PARTITION BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName ORDER BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName, CNBD.ItemName, CNBD.Quantity * CNBD.UnitCost, CNBD.EntityID) AS ItemSequence,
		CNBD.BudgetTypeName, 
		CNBD.BudgetSubTypeName,
		CNBD.ItemName,
		CNBD.ItemDescription,
		CNBD.UnitCostFormatted,
		CNBD.Quantity,
		CNBD.TotalCostFormatted,
		CNBD.TotalCost,
		CNBD.UnitOfIssue,
		CNBD.QuantityOfIssue
	FROM CNBD
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (ORDER BY BTN.BudgetTypeName) AS BudgetTypeSequence,
				BTN.BudgetTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName 
				FROM CNBD
				) BTN
			) BTS ON BTS.BudgetTypeName = CNBD.BudgetTypeName
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY BSN.BudgetTypeName ORDER BY BSN.BudgetTypeName, BSN.BudgetSubTypeName) AS BudgetSubTypeSequence,
				BSN.BudgetTypeName,
				BSN.BudgetSubTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName,
					CNBD.BudgetSubTypeName
				FROM CNBD
				) BSN
			) BSS ON BSS.BudgetTypeName = CNBD.BudgetTypeName
				AND BSS.BudgetSubTypeName = CNBD.BudgetSubTypeName

END
GO
--End procedure reporting.GetConceptNoteBudgetByConceptNoteID

--Begin procedure reporting.GetConceptNoteRiskByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteRiskByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetConceptNoteRiskByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName
	FROM ConceptNoteRisk CNR
		JOIN Risk R ON R.RiskID = CNR.RiskID AND CNR.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID

END
GO
--End procedure reporting.GetConceptNoteRiskByConceptNoteID

--Begin procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestEquipmentByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purchase request reports
--
-- Author:			Brandon Green
-- Create date:	2015.07.27
-- Description:	Added the row index
-- ===============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ID INT, 
		BudgetTypeName Varchar(250),
		Quantity NUMERIC(18,4),
		ItemName VARCHAR(250), 
		UnitCost NUMERIC(18,2), 
		UnitOfIssue VARCHAR(250), 
		UnitCostFormatted VARCHAR(50), 
		TotalCost NUMERIC(18,2), 
		TotalCostFormatted VARCHAR(50),
		QuantityOfIssue NUMERIC(18,4)
		)

	INSERT INTO @tTable
		(ID,BudgetTypeName,Quantity,ItemName,UnitCost,UnitOfIssue,UnitCostFormatted,TotalCost,TotalCostFormatted,QuantityOfIssue)
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID as ID,
		'Equipment' as BudgetTypeName,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.ItemName,
		EC.UnitCost,
		CAST(EC.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.QuantityOfIssue
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
		JOIN procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC ON PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID
			AND PRCNEC.PurchaseRequestID = @PurchaseRequestID

	UNION

	SELECT			
		CNB.ConceptNoteBudgetID as ID,
		BT.BudgetTypeName,
		ISNULL(PRCNB.Quantity,CNB.Quantity) AS Quantity,
		CNB.ItemName,
		CNB.UnitCost,
		CAST(CNB.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID	
		JOIN procurement.PurchaseRequestConceptNoteBudget PRCNB ON PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(ID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		ROW_NUMBER() OVER (ORDER BY T.ID) AS RowNumber,
		T.ID,
		T.BudgetTypeName,
		T.Quantity,
		T.ItemName,
		T.UnitCost,
		T.UnitOfIssue,
		T.UnitCostFormatted,
		T.TotalCost,
		T.TotalCostFormatted,
		T.QuantityOfIssue
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

--Begin procedure reporting.GetRiskReportByPersonID
EXEC Utility.DropObject 'reporting.GetRiskReportByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.08.01
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetRiskReportByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName
	FROM reporting.SearchResult SR
		JOIN Risk R ON R.RiskID = SR.EntityID  
			AND SR.PersonID = @PersonID  
			AND SR.EntityTypeCode='risk'
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID

END
GO
--End procedure reporting.GetRiskReportByPersonID

--Begin survey.GetSurveyBySurveyID
EXEC Utility.DropObject 'survey.GetSurveyBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.15
-- Description:	A stored procedure to get a survey from the database
-- =================================================================
CREATE PROCEDURE survey.GetSurveyBySurveyID

@SurveyID INT,
@LanguageCode CHAR(2)

AS
BEGIN

	SELECT 
		S.IsActive,
		S.SurveyID
	FROM survey.Survey S
	WHERE S.SurveyID = @SurveyID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SL.SurveyName, 
		SL.SurveyDescription
	FROM survey.SurveyLabel SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyLanguage SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		SQ.SurveyQuestionID,
		(SELECT STUFF((SELECT ', ' + SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID ORDER BY SQL.LanguageCode FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'),1,2,'')) AS LanguageCodes,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionText,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionDescription,
	
		SQT.SurveyQuestionResponseTypeCode,
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveySurveyQuestion SSQ
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQ.SurveyID = @SurveyID
			AND 
				(
				@LanguageCode = ''
					OR EXISTS
						(
						SELECT 1
						FROM survey.SurveyQuestionLanguage SQL
						WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID
							AND SQL.LanguageCode = @LanguageCode 
						)
				)
	
END
GO
--End procedure survey.GetSurveyBySurveyID

--Begin survey.GetSurveyQuestionBySurveyQuestionID
EXEC Utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.13
-- Description:	A stored procedure to get a survey question from the database
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionBySurveyQuestionID

@SurveyQuestionID INT

AS
BEGIN

	SELECT 
		SQ.IsActive,
		SQ.SurveyQuestionID, 
		SQT.SurveyQuestionResponseTypeID,
		SQT.SurveyQuestionResponseTypeCode, 
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveyQuestion SQ
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SQ.SurveyQuestionID = @SurveyQuestionID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SQL.SurveyQuestionText, 
		SQL.SurveyQuestionDescription
	FROM survey.SurveyQuestionLabel SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyQuestionLanguage SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

END
GO
--End procedure survey.GetSurveyQuestionBySurveyQuestionID

--Begin survey.GetSurveyQuestionResponseChoices
EXEC Utility.DropObject 'survey.GetSurveyQuestionResponseChoices'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.01
-- Description:	A stored procedure to get survey question response choices from the database
-- =========================================================================================
CREATE PROCEDURE survey.GetSurveyQuestionResponseChoices

@SurveyQuestionCode VARCHAR(50),
@SurveyQuestionID INT,
@LanguageCode CHAR(2)

AS
BEGIN

	SELECT 
		SQRC.SurveyQuestionResponseChoiceText,
		SQRC.SurveyQuestionResponseChoiceValue
	FROM survey.SurveyQuestionResponseChoice SQRC
	WHERE 
		(
		@LanguageCode = ''
			OR SQRC.LanguageCode = @LanguageCode
		)
		AND 
			(
			(@SurveyQuestionCode != '' AND SQRC.SurveyQuestionCode = @SurveyQuestionCode)
				OR (@SurveyQuestionID > 0 AND SQRC.SurveyQuestionID = @SurveyQuestionID)
			)
	ORDER BY SQRC.DisplayOrder, SQRC.SurveyQuestionResponseChoiceID

END
GO
--End procedure survey.GetSurveyQuestionResponseChoices

--Begin utility.UpdateParentPermissionableLineageByMenuItemCode
EXEC Utility.DropObject 'utility.UpdateParentPermissionableLineageByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to update the menuitem permissionable linage records of a parent from its descendants
-- =====================================================================================================================
CREATE PROCEDURE utility.UpdateParentPermissionableLineageByMenuItemCode

@MenuItemCode VARCHAR(50)

AS
BEGIN

	DELETE MIPL
	FROM dbo.MenuItemPermissionableLineage MIPL
		JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
			AND MI.MenuItemCode = @MenuItemCode
	
	INSERT INTO dbo.MenuItemPermissionableLineage
		(MenuItemID,PermissionableLineage)
	SELECT 
		(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode),
		MIPL1.PermissionableLineage
	FROM dbo.MenuItemPermissionableLineage MIPL1
		JOIN utility.GetDescendantMenuItemsByMenuItemCode(@MenuItemCode) MI ON MI.MenuItemID = MIPL1.MenuItemID
			AND NOT EXISTS
				(
				SELECT 1
				FROM dbo.MenuItemPermissionableLineage MIPL2
				WHERE MIPL2.MenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @MenuItemCode)
					AND MIPL2.PermissionableLineage = MIPL1.PermissionableLineage
				)

END
GO
--End procedure utility.UpdateParentPermissionableLineageByMenuItemCode
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Survey')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('Survey', 'Survey')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'SurveyQuestion')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('SurveyQuestion', 'Survey Question')

	END
--ENDIF
GO
--End table dbo.EntityType

/*
--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Document', @AfterMenuItemCode='Community'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RequestForInformation', @AfterMenuItemCode='Document'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Research', @NewMenuItemText='Research', @AfterMenuItemCode='RequestForInformation', @Icon='fa fa-fw fa-balance-scale'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Atmospheric', @ParentMenuItemCode='Research', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @NewMenuItemText='Weekly Atmospheric Report', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='IncidentReportList', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SpotReport', @ParentMenuItemCode='Research', @AfterMenuItemCode='Atmospheric', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Surveys', @ParentMenuItemCode='Research', @AfterMenuItemCode='SpotReport', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ManageWeeklyReports', @AfterMenuItemCode='RAPData', @NewMenuItemText='Implementation'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskManagement', @AfterMenuItemCode='ManageWeeklyReports'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Equipment', @AfterMenuItemCode='Training'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Contacts', @AfterMenuItemCode='Equipment'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Stipends', @AfterMenuItemCode='Contacts'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='OpperationsSupport', @NewMenuItemText='Operations and Implementation Support', @AfterMenuItemCode='Stipends', @Icon='fa fa-fw fa-compass'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Procurement', @AfterMenuItemCode='OpperationsSupport'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Activity', @AfterMenuItemCode='Procurement'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LogicalFramework', @AfterMenuItemCode='Activity'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Admin', @AfterMenuItemCode='LogicalFramework'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Atmospheric'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Surveys'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPDelivery'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPData'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Training'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Equipment'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Contacts'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Stipends'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Procurement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Activity'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem
*/

--Begin table dbo.Person
UPDATE P
SET P.PasswordExpirationDateTime = DATEADD(d, 30, getDate())
FROM dbo.Person P
WHERE P.PasswordExpirationDateTime IS NULL
GO
--End table dbo.Person

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.22 File 01 - AJACS - 2015.08.02 19.46.16')
GO
--End build tracking

