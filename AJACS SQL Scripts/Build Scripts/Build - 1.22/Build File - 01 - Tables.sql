USE AJACS
GO

--Begin table dbo.Person
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Person') AND SC.Name = 'PhoneISOCountryDataID')
	EXEC sp_RENAME 'dbo.Person.PhoneISOCountryDataID', 'CountryCallingCodeID', 'COLUMN';
--ENDIF
GO

DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC utility.AddColumn @TableName, 'CountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'IsPhoneVerified', 'BIT'
EXEC utility.AddColumn @TableName, 'Phone', 'VARCHAR(50)'

EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsPhoneVerified', 'BIT', 0
GO
--End table dbo.Person

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.AddColumn @TableName, 'ISOCountryCode2', 'CHAR(2)'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dropdown.Country') AND SC.Name = 'ISOCountryCode')
	EXEC sp_RENAME 'dropdown.Country.ISOCountryCode', 'ISOCountryCode3', 'COLUMN';
--ENDIF
GO

UPDATE C SET C.ISOCountryCode2 = 'AW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ABW'
UPDATE C SET C.ISOCountryCode2 = 'AF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AFG'
UPDATE C SET C.ISOCountryCode2 = 'AO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AGO'
UPDATE C SET C.ISOCountryCode2 = 'AI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AIA'
UPDATE C SET C.ISOCountryCode2 = 'AX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALA'
UPDATE C SET C.ISOCountryCode2 = 'AL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALB'
UPDATE C SET C.ISOCountryCode2 = 'AD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AND'
UPDATE C SET C.ISOCountryCode2 = 'AE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARE'
UPDATE C SET C.ISOCountryCode2 = 'AR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARG'
UPDATE C SET C.ISOCountryCode2 = 'AM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARM'
UPDATE C SET C.ISOCountryCode2 = 'AS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ASM'
UPDATE C SET C.ISOCountryCode2 = 'AQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATA'
UPDATE C SET C.ISOCountryCode2 = 'TF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATF'
UPDATE C SET C.ISOCountryCode2 = 'AG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATG'
UPDATE C SET C.ISOCountryCode2 = 'AU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUS'
UPDATE C SET C.ISOCountryCode2 = 'AT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUT'
UPDATE C SET C.ISOCountryCode2 = 'AZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AZE'
UPDATE C SET C.ISOCountryCode2 = 'BI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BDI'
UPDATE C SET C.ISOCountryCode2 = 'BE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEL'
UPDATE C SET C.ISOCountryCode2 = 'BJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEN'
UPDATE C SET C.ISOCountryCode2 = 'BF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BFA'
UPDATE C SET C.ISOCountryCode2 = 'BD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGD'
UPDATE C SET C.ISOCountryCode2 = 'BG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGR'
UPDATE C SET C.ISOCountryCode2 = 'BH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHR'
UPDATE C SET C.ISOCountryCode2 = 'BS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHS'
UPDATE C SET C.ISOCountryCode2 = 'BA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BIH'
UPDATE C SET C.ISOCountryCode2 = 'BL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLM'
UPDATE C SET C.ISOCountryCode2 = 'BY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLR'
UPDATE C SET C.ISOCountryCode2 = 'BZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLZ'
UPDATE C SET C.ISOCountryCode2 = 'BM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BMU'
UPDATE C SET C.ISOCountryCode2 = 'BO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BOL'
UPDATE C SET C.ISOCountryCode2 = 'BR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRA'
UPDATE C SET C.ISOCountryCode2 = 'BB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRB'
UPDATE C SET C.ISOCountryCode2 = 'BN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRN'
UPDATE C SET C.ISOCountryCode2 = 'BT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BTN'
UPDATE C SET C.ISOCountryCode2 = 'BV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BVT'
UPDATE C SET C.ISOCountryCode2 = 'BW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BWA'
UPDATE C SET C.ISOCountryCode2 = 'CF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAF'
UPDATE C SET C.ISOCountryCode2 = 'CA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAN'
UPDATE C SET C.ISOCountryCode2 = 'CC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CCK'
UPDATE C SET C.ISOCountryCode2 = 'CH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHE'
UPDATE C SET C.ISOCountryCode2 = 'CL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHL'
UPDATE C SET C.ISOCountryCode2 = 'CN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHN'
UPDATE C SET C.ISOCountryCode2 = 'CI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CIV'
UPDATE C SET C.ISOCountryCode2 = 'CM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CMR'
UPDATE C SET C.ISOCountryCode2 = 'CD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COD'
UPDATE C SET C.ISOCountryCode2 = 'CG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COG'
UPDATE C SET C.ISOCountryCode2 = 'CK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COK'
UPDATE C SET C.ISOCountryCode2 = 'CO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COL'
UPDATE C SET C.ISOCountryCode2 = 'KM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COM'
UPDATE C SET C.ISOCountryCode2 = 'CV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CPV'
UPDATE C SET C.ISOCountryCode2 = 'CR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CRI'
UPDATE C SET C.ISOCountryCode2 = 'CU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUB'
UPDATE C SET C.ISOCountryCode2 = 'CW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUW'
UPDATE C SET C.ISOCountryCode2 = 'CX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CXR'
UPDATE C SET C.ISOCountryCode2 = 'KY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYM'
UPDATE C SET C.ISOCountryCode2 = 'CY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYP'
UPDATE C SET C.ISOCountryCode2 = 'CZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CZE'
UPDATE C SET C.ISOCountryCode2 = 'DE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DEU'
UPDATE C SET C.ISOCountryCode2 = 'DJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DJI'
UPDATE C SET C.ISOCountryCode2 = 'DM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DMA'
UPDATE C SET C.ISOCountryCode2 = 'DK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DNK'
UPDATE C SET C.ISOCountryCode2 = 'DO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'
UPDATE C SET C.ISOCountryCode2 = 'DZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DZA'
UPDATE C SET C.ISOCountryCode2 = 'EC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ECU'
UPDATE C SET C.ISOCountryCode2 = 'EG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EGY'
UPDATE C SET C.ISOCountryCode2 = 'ER' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ERI'
UPDATE C SET C.ISOCountryCode2 = 'EH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESH'
UPDATE C SET C.ISOCountryCode2 = 'ES' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESP'
UPDATE C SET C.ISOCountryCode2 = 'EE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EST'
UPDATE C SET C.ISOCountryCode2 = 'ET' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ETH'
UPDATE C SET C.ISOCountryCode2 = 'FI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FIN'
UPDATE C SET C.ISOCountryCode2 = 'FJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FJI'
UPDATE C SET C.ISOCountryCode2 = 'FK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FLK'
UPDATE C SET C.ISOCountryCode2 = 'FR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRA'
UPDATE C SET C.ISOCountryCode2 = 'FO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRO'
UPDATE C SET C.ISOCountryCode2 = 'FM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FSM'
UPDATE C SET C.ISOCountryCode2 = 'GA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GAB'
UPDATE C SET C.ISOCountryCode2 = 'GB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GBR'
UPDATE C SET C.ISOCountryCode2 = 'GE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GEO'
UPDATE C SET C.ISOCountryCode2 = 'GG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GGY'
UPDATE C SET C.ISOCountryCode2 = 'GH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GHA'
UPDATE C SET C.ISOCountryCode2 = 'GI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIB'
UPDATE C SET C.ISOCountryCode2 = 'GN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIN'
UPDATE C SET C.ISOCountryCode2 = 'GP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GLP'
UPDATE C SET C.ISOCountryCode2 = 'GM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GMB'
UPDATE C SET C.ISOCountryCode2 = 'GW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNB'
UPDATE C SET C.ISOCountryCode2 = 'GQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNQ'
UPDATE C SET C.ISOCountryCode2 = 'GR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRC'
UPDATE C SET C.ISOCountryCode2 = 'GD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRD'
UPDATE C SET C.ISOCountryCode2 = 'GL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRL'
UPDATE C SET C.ISOCountryCode2 = 'GT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GTM'
UPDATE C SET C.ISOCountryCode2 = 'GF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUF'
UPDATE C SET C.ISOCountryCode2 = 'GU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUM'
UPDATE C SET C.ISOCountryCode2 = 'GY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUY'
UPDATE C SET C.ISOCountryCode2 = 'HK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HKG'
UPDATE C SET C.ISOCountryCode2 = 'HM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HMD'
UPDATE C SET C.ISOCountryCode2 = 'HN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HND'
UPDATE C SET C.ISOCountryCode2 = 'HR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HRV'
UPDATE C SET C.ISOCountryCode2 = 'HT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HTI'
UPDATE C SET C.ISOCountryCode2 = 'HU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HUN'
UPDATE C SET C.ISOCountryCode2 = 'ID' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IDN'
UPDATE C SET C.ISOCountryCode2 = 'IM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IMN'
UPDATE C SET C.ISOCountryCode2 = 'IN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IND'
UPDATE C SET C.ISOCountryCode2 = 'IO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IOT'
UPDATE C SET C.ISOCountryCode2 = 'IE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRL'
UPDATE C SET C.ISOCountryCode2 = 'IR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRN'
UPDATE C SET C.ISOCountryCode2 = 'IQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRQ'
UPDATE C SET C.ISOCountryCode2 = 'IS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISL'
UPDATE C SET C.ISOCountryCode2 = 'IL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISR'
UPDATE C SET C.ISOCountryCode2 = 'IT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ITA'
UPDATE C SET C.ISOCountryCode2 = 'JM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JAM'
UPDATE C SET C.ISOCountryCode2 = 'JE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JEY'
UPDATE C SET C.ISOCountryCode2 = 'JO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JOR'
UPDATE C SET C.ISOCountryCode2 = 'JP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JPN'
UPDATE C SET C.ISOCountryCode2 = 'KZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KAZ'
UPDATE C SET C.ISOCountryCode2 = 'KE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KEN'
UPDATE C SET C.ISOCountryCode2 = 'KG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KGZ'
UPDATE C SET C.ISOCountryCode2 = 'KH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KHM'
UPDATE C SET C.ISOCountryCode2 = 'KI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KIR'
UPDATE C SET C.ISOCountryCode2 = 'KN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KNA'
UPDATE C SET C.ISOCountryCode2 = 'KR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOR'
UPDATE C SET C.ISOCountryCode2 = 'XK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOS'
UPDATE C SET C.ISOCountryCode2 = 'KW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KWT'
UPDATE C SET C.ISOCountryCode2 = 'LA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LAO'
UPDATE C SET C.ISOCountryCode2 = 'LB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBN'
UPDATE C SET C.ISOCountryCode2 = 'LR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBR'
UPDATE C SET C.ISOCountryCode2 = 'LY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBY'
UPDATE C SET C.ISOCountryCode2 = 'LC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LCA'
UPDATE C SET C.ISOCountryCode2 = 'LI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LIE'
UPDATE C SET C.ISOCountryCode2 = 'LK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LKA'
UPDATE C SET C.ISOCountryCode2 = 'LS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LSO'
UPDATE C SET C.ISOCountryCode2 = 'LT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LTU'
UPDATE C SET C.ISOCountryCode2 = 'LU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LUX'
UPDATE C SET C.ISOCountryCode2 = 'LV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LVA'
UPDATE C SET C.ISOCountryCode2 = 'MO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAC'
UPDATE C SET C.ISOCountryCode2 = 'MF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAF'
UPDATE C SET C.ISOCountryCode2 = 'MA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAR'
UPDATE C SET C.ISOCountryCode2 = 'MC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MCO'
UPDATE C SET C.ISOCountryCode2 = 'MD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDA'
UPDATE C SET C.ISOCountryCode2 = 'MG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDG'
UPDATE C SET C.ISOCountryCode2 = 'MV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDV'
UPDATE C SET C.ISOCountryCode2 = 'MX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MEX'
UPDATE C SET C.ISOCountryCode2 = 'MH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MHL'
UPDATE C SET C.ISOCountryCode2 = 'MK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MKD'
UPDATE C SET C.ISOCountryCode2 = 'ML' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLI'
UPDATE C SET C.ISOCountryCode2 = 'MT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLT'
UPDATE C SET C.ISOCountryCode2 = 'MM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MMR'
UPDATE C SET C.ISOCountryCode2 = 'ME' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNE'
UPDATE C SET C.ISOCountryCode2 = 'MN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNG'
UPDATE C SET C.ISOCountryCode2 = 'MP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNP'
UPDATE C SET C.ISOCountryCode2 = 'MZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MOZ'
UPDATE C SET C.ISOCountryCode2 = 'MR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MRT'
UPDATE C SET C.ISOCountryCode2 = 'MS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MSR'
UPDATE C SET C.ISOCountryCode2 = 'MQ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MTQ'
UPDATE C SET C.ISOCountryCode2 = 'MU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MUS'
UPDATE C SET C.ISOCountryCode2 = 'MW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MWI'
UPDATE C SET C.ISOCountryCode2 = 'MY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYS'
UPDATE C SET C.ISOCountryCode2 = 'YT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYT'
UPDATE C SET C.ISOCountryCode2 = 'NA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NAM'
UPDATE C SET C.ISOCountryCode2 = 'NC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NCL'
UPDATE C SET C.ISOCountryCode2 = 'NE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NER'
UPDATE C SET C.ISOCountryCode2 = 'NF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NFK'
UPDATE C SET C.ISOCountryCode2 = 'NG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NGA'
UPDATE C SET C.ISOCountryCode2 = 'NI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIC'
UPDATE C SET C.ISOCountryCode2 = 'NU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIU'
UPDATE C SET C.ISOCountryCode2 = 'NL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NLD'
UPDATE C SET C.ISOCountryCode2 = 'NO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NOR'
UPDATE C SET C.ISOCountryCode2 = 'NP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NPL'
UPDATE C SET C.ISOCountryCode2 = 'NR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NRU'
UPDATE C SET C.ISOCountryCode2 = 'NZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NZL'
UPDATE C SET C.ISOCountryCode2 = 'OM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'OMN'
UPDATE C SET C.ISOCountryCode2 = 'PK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAK'
UPDATE C SET C.ISOCountryCode2 = 'PA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAN'
UPDATE C SET C.ISOCountryCode2 = 'PN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PCN'
UPDATE C SET C.ISOCountryCode2 = 'PE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PER'
UPDATE C SET C.ISOCountryCode2 = 'PH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PHL'
UPDATE C SET C.ISOCountryCode2 = 'PW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PLW'
UPDATE C SET C.ISOCountryCode2 = 'PG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PNG'
UPDATE C SET C.ISOCountryCode2 = 'PL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'POL'
UPDATE C SET C.ISOCountryCode2 = 'PR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'
UPDATE C SET C.ISOCountryCode2 = 'KP' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRK'
UPDATE C SET C.ISOCountryCode2 = 'PT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRT'
UPDATE C SET C.ISOCountryCode2 = 'PY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRY'
UPDATE C SET C.ISOCountryCode2 = 'PS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PSE'
UPDATE C SET C.ISOCountryCode2 = 'PF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PYF'
UPDATE C SET C.ISOCountryCode2 = 'QA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'QAT'
UPDATE C SET C.ISOCountryCode2 = 'RE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'REU'
UPDATE C SET C.ISOCountryCode2 = 'RO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ROU'
UPDATE C SET C.ISOCountryCode2 = 'RU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RUS'
UPDATE C SET C.ISOCountryCode2 = 'RW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RWA'
UPDATE C SET C.ISOCountryCode2 = 'SA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SAU'
UPDATE C SET C.ISOCountryCode2 = 'SD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SDN'
UPDATE C SET C.ISOCountryCode2 = 'SN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SEN'
UPDATE C SET C.ISOCountryCode2 = 'SG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGP'
UPDATE C SET C.ISOCountryCode2 = 'GS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGS'
UPDATE C SET C.ISOCountryCode2 = 'SJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SJM'
UPDATE C SET C.ISOCountryCode2 = 'SB' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLB'
UPDATE C SET C.ISOCountryCode2 = 'SL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLE'
UPDATE C SET C.ISOCountryCode2 = 'SV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLV'
UPDATE C SET C.ISOCountryCode2 = 'SM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SMR'
UPDATE C SET C.ISOCountryCode2 = 'SO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SOM'
UPDATE C SET C.ISOCountryCode2 = 'PM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SPM'
UPDATE C SET C.ISOCountryCode2 = 'RS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SRB'
UPDATE C SET C.ISOCountryCode2 = 'SS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SSD'
UPDATE C SET C.ISOCountryCode2 = 'ST' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'STP'
UPDATE C SET C.ISOCountryCode2 = 'SR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SUR'
UPDATE C SET C.ISOCountryCode2 = 'SK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVK'
UPDATE C SET C.ISOCountryCode2 = 'SI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVN'
UPDATE C SET C.ISOCountryCode2 = 'SE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWE'
UPDATE C SET C.ISOCountryCode2 = 'SZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWZ'
UPDATE C SET C.ISOCountryCode2 = 'SX' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SXM'
UPDATE C SET C.ISOCountryCode2 = 'SC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYC'
UPDATE C SET C.ISOCountryCode2 = 'SY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYR'
UPDATE C SET C.ISOCountryCode2 = 'TC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCA'
UPDATE C SET C.ISOCountryCode2 = 'TD' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCD'
UPDATE C SET C.ISOCountryCode2 = 'TG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TGO'
UPDATE C SET C.ISOCountryCode2 = 'TH' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'THA'
UPDATE C SET C.ISOCountryCode2 = 'TJ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TJK'
UPDATE C SET C.ISOCountryCode2 = 'TK' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKL'
UPDATE C SET C.ISOCountryCode2 = 'TM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKM'
UPDATE C SET C.ISOCountryCode2 = 'TL' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TLS'
UPDATE C SET C.ISOCountryCode2 = 'TO' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TON'
UPDATE C SET C.ISOCountryCode2 = 'TT' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TTO'
UPDATE C SET C.ISOCountryCode2 = 'TN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUN'
UPDATE C SET C.ISOCountryCode2 = 'TR' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUR'
UPDATE C SET C.ISOCountryCode2 = 'TV' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUV'
UPDATE C SET C.ISOCountryCode2 = 'TW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TWN'
UPDATE C SET C.ISOCountryCode2 = 'TZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TZA'
UPDATE C SET C.ISOCountryCode2 = 'UG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UGA'
UPDATE C SET C.ISOCountryCode2 = 'UA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UKR'
UPDATE C SET C.ISOCountryCode2 = 'UM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UMI'
UPDATE C SET C.ISOCountryCode2 = 'UY' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'URY'
UPDATE C SET C.ISOCountryCode2 = 'US' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'USA'
UPDATE C SET C.ISOCountryCode2 = 'UZ' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UZB'
UPDATE C SET C.ISOCountryCode2 = 'VA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'
UPDATE C SET C.ISOCountryCode2 = 'VC' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VCT'
UPDATE C SET C.ISOCountryCode2 = 'VE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VEN'
UPDATE C SET C.ISOCountryCode2 = 'VG' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VGB'
UPDATE C SET C.ISOCountryCode2 = 'VI' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VIR'
UPDATE C SET C.ISOCountryCode2 = 'VN' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VNM'
UPDATE C SET C.ISOCountryCode2 = 'VU' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VUT'
UPDATE C SET C.ISOCountryCode2 = 'WF' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WLF'
UPDATE C SET C.ISOCountryCode2 = 'WS' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WSM'
UPDATE C SET C.ISOCountryCode2 = 'YE' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'YEM'
UPDATE C SET C.ISOCountryCode2 = 'ZA' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZAF'
UPDATE C SET C.ISOCountryCode2 = 'ZM' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZMB'
UPDATE C SET C.ISOCountryCode2 = 'ZW' FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZWE'
GO
--End table dropdown.Country

--Begin table dropdown.CountryCallingCode
DECLARE @TableName VARCHAR(250) = 'dropdown.CountryCallingCode'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CountryCallingCode
	(
	CountryCallingCodeID INT IDENTITY(1,1) NOT NULL,
	CountryID INT,
	CountryCallingCode INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'CountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CountryCallingCode', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CountryCallingCodeID'
EXEC utility.SetIndexClustered 'IX_CountryCallingCode', @TableName, 'CountryID,CountryCallingCode'
GO

INSERT INTO dropdown.CountryCallingCode 
	(CountryID,CountryCallingCode) 
VALUES 
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ABW'),297),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AFG'),93),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AGO'),244),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AIA'),1264),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALA'),358),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ALB'),355),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AND'),376),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARE'),971),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARG'),54),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ARM'),374),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ASM'),1684),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATA'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATF'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ATG'),1268),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUS'),61),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AUT'),43),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'AZE'),994),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BDI'),257),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEL'),32),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BEN'),229),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BFA'),226),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGD'),880),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BGR'),359),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHR'),973),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BHS'),1242),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BIH'),387),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLM'),590),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLR'),375),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BLZ'),501),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BMU'),1441),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BOL'),591),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRA'),55),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRB'),1246),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BRN'),673),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BTN'),975),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BVT'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'BWA'),267),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAF'),236),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CAN'),1),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CCK'),61),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHE'),41),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHL'),56),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CHN'),86),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CIV'),225),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CMR'),237),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COD'),243),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COG'),242),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COK'),682),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COL'),57),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'COM'),269),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CPV'),238),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CRI'),506),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUB'),53),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CUW'),5999),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CXR'),61),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYM'),1345),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CYP'),357),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'CZE'),420),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DEU'),49),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DJI'),253),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DMA'),1767),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DNK'),45),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),180),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),918),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),291),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DOM'),849),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'DZA'),213),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ECU'),593),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EGY'),20),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ERI'),291),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESH'),212),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ESP'),34),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'EST'),372),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ETH'),251),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FIN'),358),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FJI'),679),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FLK'),500),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRA'),33),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FRO'),298),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'FSM'),691),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GAB'),241),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GBR'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GEO'),995),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GGY'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GHA'),233),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIB'),350),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GIN'),224),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GLP'),590),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GMB'),220),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNB'),245),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GNQ'),240),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRC'),30),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRD'),1473),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GRL'),299),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GTM'),502),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUF'),594),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUM'),1671),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'GUY'),592),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HKG'),852),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HMD'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HND'),504),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HRV'),385),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HTI'),509),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'HUN'),36),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IDN'),62),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IMN'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IND'),91),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IOT'),246),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRL'),353),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRN'),98),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'IRQ'),964),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISL'),354),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ISR'),972),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ITA'),39),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JAM'),1876),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JEY'),44),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JOR'),962),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'JPN'),81),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KAZ'),76),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KAZ'),77),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KEN'),254),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KGZ'),996),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KHM'),855),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KIR'),686),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KNA'),1869),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOR'),82),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KOS'),383),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'KWT'),965),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LAO'),856),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBN'),961),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBR'),231),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LBY'),218),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LCA'),1758),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LIE'),423),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LKA'),94),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LSO'),266),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LTU'),370),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LUX'),352),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'LVA'),371),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAC'),853),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAF'),590),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MAR'),212),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MCO'),377),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDA'),373),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDG'),261),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MDV'),960),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MEX'),52),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MHL'),692),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MKD'),389),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLI'),223),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MLT'),356),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MMR'),95),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNE'),382),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNG'),976),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MNP'),1670),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MOZ'),258),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MRT'),222),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MSR'),1664),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MTQ'),596),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MUS'),230),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MWI'),265),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYS'),60),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'MYT'),262),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NAM'),264),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NCL'),687),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NER'),227),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NFK'),672),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NGA'),234),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIC'),505),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NIU'),683),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NLD'),31),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NOR'),47),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NPL'),977),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NRU'),674),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'NZL'),64),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'OMN'),968),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAK'),92),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PAN'),507),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PCN'),64),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PER'),51),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PHL'),63),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PLW'),680),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PNG'),675),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'POL'),48),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'),17),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'),871),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRI'),939),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRK'),850),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRT'),351),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PRY'),595),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PSE'),970),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'PYF'),689),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'QAT'),974),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'REU'),262),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ROU'),40),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RUS'),7),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'RWA'),250),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SAU'),966),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SDN'),249),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SEN'),221),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGP'),65),
	--((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SGS'),500),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SJM'),4779),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLB'),677),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLE'),232),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SLV'),503),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SMR'),378),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SOM'),252),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SPM'),508),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SRB'),381),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SSD'),211),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'STP'),239),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SUR'),597),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVK'),421),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SVN'),386),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWE'),46),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SWZ'),268),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SXM'),1721),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYC'),248),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'SYR'),963),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCA'),1649),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TCD'),235),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TGO'),228),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'THA'),66),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TJK'),992),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKL'),690),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TKM'),993),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TLS'),670),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TON'),676),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TTO'),1868),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUN'),216),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUR'),90),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TUV'),688),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TWN'),886),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'TZA'),255),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UGA'),256),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UKR'),380),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UMI'),0),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'URY'),598),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'USA'),1),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'UZB'),998),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),3),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),906),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),698),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VAT'),379),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VCT'),1784),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VEN'),58),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VGB'),1284),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VIR'),1340),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VNM'),84),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'VUT'),678),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WLF'),681),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'WSM'),685),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'YEM'),967),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZAF'),27),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZMB'),260),
	((SELECT C.CountryID FROM dropdown.Country C WHERE C.ISOCountryCode3 = 'ZWE'),263)
GO
--End table dropdown.CountryCallingCode

--Begin table survey.Survey
DECLARE @TableName VARCHAR(250) = 'survey.Survey'

EXEC utility.DropObject @TableName

CREATE TABLE survey.Survey
	(
	SurveyID INT IDENTITY(1,1) NOT NULL,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyID'
GO
--End table survey.Survey

--Begin table survey.SurveyLabel
DECLARE @TableName VARCHAR(250) = 'survey.SurveyLabel'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyLabel
	(
	SurveyLabelID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	LanguageCode CHAR(2),
	SurveyName NVARCHAR(500),
	SurveyDescription NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyLabelID'
EXEC utility.SetIndexClustered 'IX_SurveyLabel', @TableName, 'SurveyID,LanguageCode'
GO
--End table survey.SurveyLabel

--Begin table survey.SurveyQuestionLabel
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionLabel'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionLabel
	(
	SurveyQuestionLabelID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	SurveyQuestionText NVARCHAR(500),
	SurveyQuestionDescription NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionLabelID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionLabel', @TableName, 'SurveyQuestionID,LanguageCode'
GO
--End table survey.SurveyQuestionLabel

--Begin table survey.SurveyQuestionResponseChoice
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionResponseChoice'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionResponseChoice
	(
	SurveyQuestionResponseChoiceID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionCode VARCHAR(50),
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	SurveyQuestionResponseChoiceValue VARCHAR(50),
	SurveyQuestionResponseChoiceText NVARCHAR(500),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionResponseChoiceID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionResponseChoice', @TableName, 'SurveyQuestionID,LanguageCode,DisplayOrder'
GO

INSERT INTO survey.SurveyQuestionResponseChoice
	(SurveyQuestionCode,LanguageCode,SurveyQuestionResponseChoiceValue,SurveyQuestionResponseChoiceText,DisplayOrder)
VALUES
	('YesNo','AR','1',N'نعم',1),
	('YesNo','AR','0',N'لا',2),
	('YesNo','EN','1','Yes',1),
	('YesNo','EN','0','No',2)
GO
--End table survey.SurveyQuestionResponseChoice

--Begin table survey.SurveyResponse
DECLARE @TableName VARCHAR(250) = 'survey.SurveyResponse'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyResponse
	(
	SurveyResponseID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	LanguageCode CHAR(2),
	ResponseDateTime DATETIME,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ResponseDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyResponseID'
EXEC utility.SetIndexClustered 'IX_SurveySurveyQuestionResponse', @TableName, 'SurveyID,LanguageCode'
GO
--End table survey.SurveySurveyQuestionResponse

--Begin table survey.SurveySurveyQuestionResponse
DECLARE @TableName VARCHAR(250) = 'survey.SurveySurveyQuestionResponse'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveySurveyQuestionResponse
	(
	SurveySurveyQuestionResponseID INT IDENTITY(1,1) NOT NULL,
	SurveyResponseID INT,
	SurveyQuestionID INT,
	SurveyQuestionResponse NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyResponseID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveySurveyQuestionResponseID'
EXEC utility.SetIndexClustered 'IX_SurveySurveyQuestionResponse', @TableName, 'SurveyResponseID,SurveyQuestionID'
GO
--End table survey.SurveySurveyQuestionResponse

