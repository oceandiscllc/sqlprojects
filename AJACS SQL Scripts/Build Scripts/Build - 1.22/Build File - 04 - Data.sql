USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Survey')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('Survey', 'Survey')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'SurveyQuestion')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('SurveyQuestion', 'Survey Question')

	END
--ENDIF
GO
--End table dbo.EntityType

/*
--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Document', @AfterMenuItemCode='Community'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RequestForInformation', @AfterMenuItemCode='Document'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Research', @NewMenuItemText='Research', @AfterMenuItemCode='RequestForInformation', @Icon='fa fa-fw fa-balance-scale'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Atmospheric', @ParentMenuItemCode='Research', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @NewMenuItemText='Weekly Atmospheric Report', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='IncidentReportList', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SpotReport', @ParentMenuItemCode='Research', @AfterMenuItemCode='Atmospheric', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Surveys', @ParentMenuItemCode='Research', @AfterMenuItemCode='SpotReport', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ManageWeeklyReports', @AfterMenuItemCode='RAPData', @NewMenuItemText='Implementation'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskManagement', @AfterMenuItemCode='ManageWeeklyReports'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Equipment', @AfterMenuItemCode='Training'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Contacts', @AfterMenuItemCode='Equipment'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Stipends', @AfterMenuItemCode='Contacts'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='OpperationsSupport', @NewMenuItemText='Operations and Implementation Support', @AfterMenuItemCode='Stipends', @Icon='fa fa-fw fa-compass'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Procurement', @AfterMenuItemCode='OpperationsSupport'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Activity', @AfterMenuItemCode='Procurement'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LogicalFramework', @AfterMenuItemCode='Activity'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Admin', @AfterMenuItemCode='LogicalFramework'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Atmospheric'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Surveys'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPDelivery'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPData'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Training'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Equipment'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Contacts'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Stipends'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Procurement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Activity'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem
*/

--Begin table dbo.Person
UPDATE P
SET P.PasswordExpirationDateTime = DATEADD(d, 30, getDate())
FROM dbo.Person P
WHERE P.PasswordExpirationDateTime IS NULL
GO
--End table dbo.Person

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
