USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'ConceptNote' AND ET.WorkflowActionCode = 'Amended')
	BEGIN

	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	VALUES
		('ConceptNote', 'Amended', '<p>An AJACS [[ConceptNoteTypeName]] has been Amended</p><p><strong>[[ConceptNoteTypeName]]: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Activity Management workflow. Please click the link above to review this [[ConceptNoteTypeName]].</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p><p>&nbsp;</p>');

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dropdown.ContactType
IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeCode = 'ProjectStaffCreative')
	BEGIN

	INSERT INTO dropdown.ContactType
		(ContactTypeCode, ContactTypeName)
	VALUES
		('ProjectStaffCreative', 'Project Staff - Creative')

	END
--ENDIF
GO

UPDATE CT
SET
	CT.ContactTypeCode = 'ProjectStaffASI',
	CT.ContactTypeName = 'Project Staff - ASI'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'ProjectStaff'
GO
--End table dropdown.ContactType
