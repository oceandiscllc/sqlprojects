USE AJACS
GO

--Begin procedure dbo.GetCommunityFeed
EXEC Utility.DropObject 'dbo.GetCommunityFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetCommunityFeed

@PersonID INT = 0,
@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID = @CommunityID
					AND IC.IncidentID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID = @CommunityID
					AND SRC.SpotReportID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Community Engagement Permitted Change' AS EntityTypeName,			
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS PhysicalFileName,
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
			AND CIDH.CommunityID = @CommunityID

	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetCommunityFeed

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
--
-- Author:			Eric Jones
-- Create date:	2016.11.20
-- Description:	Added IsCommunityAssociationRequired field
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.AssetUnitID,		
		(SELECT AU.AssetUnitName FROM asset.AssetUnit AU WHERE AU.AssetUnitID = C1.AssetUnitID) AS AssetUnitName,		
		(SELECT A.AssetName FROM asset.Asset A JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID AND AU.AssetUnitID = C1.AssetUnitID) AS AssetName,
		dbo.GetCommunityNameByCommunityID(asset.GetCommunityIDByAssetUnitID(C1.AssetUnitID)) AS AssetCommunityName,
		C1.CommunityID,		
		dbo.GetCommunityNameByCommunityID(C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate((SELECT EL.CreateDateTime FROM eventlog.EventLog EL WHERE EL.EventCode = 'create' AND EL.EntityTypeCode = 'Contact' AND EL.EntityID = C1.ContactID)) AS InitialEntryDateFormatted,
		C1.IsActive,
		C1.IsCommunityAssociationRequired,	
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(SELECT CSG.CommunitySubGroupName FROM dropdown.CommunitySubGroup CSG JOIN dbo.Community C ON C. CommunitySubGroupID = CSG.CommunitySubGroupID AND C.CommunityID = C1.CommunityID) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID, 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,

		CASE
			WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
			AND CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
	--Get Document By Contact ID
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID
	ORDER BY D.DocumentDescription

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetDashboardItemCounts
EXEC Utility.DropObject 'dbo.GetDashboardItemCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.19
-- Description:	A stored procedure to get item count data for the dashboard
--
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	Added Spot Reports
--
-- Author:			Todd Pires
-- Create date:	2016.09.30
-- Description:	Refactored
--
-- Author:			Todd Pires
-- Create date:	2016.12/17
-- Description:	Refactored
-- ========================================================================
CREATE PROCEDURE dbo.GetDashboardItemCounts

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkplanAllocationTotal NUMERIC(18,2)
	DECLARE @nWorkplanCommittedTotal NUMERIC(18,2)
	DECLARE @nWorkplanDisbursedTotal NUMERIC(18,2)
	DECLARE @nPendingCommitmentTotal NUMERIC(18,2)
	DECLARE @nActivityCountTotal INT

	DECLARE @tTable1 TABLE
		(
		ConceptNoteTypeName VARCHAR(250),
		WorkplanAllocation NUMERIC(18,2) DEFAULT 0,
		WorkplanCommitted NUMERIC(18,2) DEFAULT 0,
		WorkplanDisbursed NUMERIC(18,2) DEFAULT 0,
		PendingCommitment NUMERIC(18,2) DEFAULT 0,
		ActivityCount INT DEFAULT 0
		)

	DECLARE @tTable2 TABLE (ItemCount INT  DEFAULT 0, ItemName VARCHAR(250))

	--Component Engagement Counts
	SELECT 
		PVT.Component,
		ISNULL(PVT.No, 0) AS No,
		ISNULL(PVT.Pending, 0) AS Pending,
		ISNULL(PVT.Yes, 0) AS Yes
	FROM
		(
		SELECT
			'CommunityEngagement' AS Component,
			COUNT(C.CommunityComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.CommunityComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		UNION

		SELECT
			'JusticeEngagement' AS Component,
			COUNT(C.JusticeComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.JusticeComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		UNION

		SELECT
			'PoliceEngagement' AS Component,
			COUNT(C.PoliceComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.PoliceComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		) AS D
	PIVOT
		(
		MAX(D.ItemCount)
		FOR D.ItemName IN
			(
			No,Pending,Yes
			)
	) AS PVT
	ORDER BY PVT.Component

	--Funds Data
	INSERT INTO @tTable1 (ConceptNoteTypeName) SELECT CNT.ConceptNoteTypeName FROM dropdown.ConceptNoteType CNT WHERE CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
	INSERT INTO @tTable1 (ConceptNoteTypeName) VALUES ('Other')
	
	--WorkplanAllocation
	UPDATE T1
	SET T1.WorkplanAllocation = E.WorkplanAllocation
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.CurrentUSDAllocation) AS WorkplanAllocation,
				ConceptNoteTypeName
			FROM
				(
				SELECT 
					WPA.CurrentUSDAllocation,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM workplan.WorkplanActivity WPA
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	SELECT @nWorkplanAllocationTotal = SUM(T1.WorkplanAllocation)
	FROM @tTable1 T1

	--WorkplanCommitted
	UPDATE T1
	SET T1.WorkplanCommitted = T1.WorkplanCommitted + E.ConceptNoteBudgetTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteBudgetTotal) AS ConceptNoteBudgetTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS ConceptNoteBudgetTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteBudget CNB
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNB.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.WorkplanCommitted = T1.WorkplanCommitted + E.ConceptNoteEquimentTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteEquimentTotal) AS ConceptNoteEquimentTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue AS ConceptNoteEquimentTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	SELECT @nWorkplanCommittedTotal = SUM(T1.WorkplanCommitted)
	FROM @tTable1 T1

	--WorkplanDisbursed
	UPDATE T1
	SET T1.WorkplanDisbursed = T1.WorkplanDisbursed + ISNULL(E.ConceptNoteFinanceTotal, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteFinanceTotal) AS ConceptNoteFinanceTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNF.drAmt - CNF.CrAmt AS ConceptNoteFinanceTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteFinance CNF
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.WorkplanDisbursed = T1.WorkplanDisbursed + ISNULL(E.ActualTotalAmount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ActualTotalAmount) AS ActualTotalAmount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ActualTotalAmount,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNote CN
						JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
							AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	SELECT @nWorkplanDisbursedTotal = SUM(T1.WorkplanDisbursed)
	FROM @tTable1 T1

	--PendingCommitment
	UPDATE T1
	SET T1.PendingCommitment = T1.PendingCommitment + E.ConceptNoteBudgetTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteBudgetTotal) AS ConceptNoteBudgetTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS ConceptNoteBudgetTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteBudget CNB
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNB.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Development')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.PendingCommitment = T1.PendingCommitment + E.ConceptNoteEquimentTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteEquimentTotal) AS ConceptNoteEquimentTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue AS ConceptNoteEquimentTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Development')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	SELECT @nPendingCommitmentTotal = SUM(T1.PendingCommitment)
	FROM @tTable1 T1

	--ActivityCount
	UPDATE T1
	SET T1.ActivityCount = ISNULL(E.ActivityCount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				COUNT(D.ConceptNoteID) AS ActivityCount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ConceptNoteID,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName
				FROM dbo.ConceptNote CN
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	SELECT @nActivityCountTotal = SUM(T1.ActivityCount)
	FROM @tTable1 T1

	SELECT
		T1.ConceptNoteTypeName,
		T1.WorkplanAllocation,

		CASE
			WHEN @nWorkplanAllocationTotal = 0
			THEN 0
			ELSE CAST(T1.WorkplanAllocation / @nWorkplanAllocationTotal * 100 AS NUMERIC(18,2))
		END AS WorkplanAllocationPercent,

		T1.WorkplanCommitted,

		CASE
			WHEN T1.WorkplanAllocation = 0
			THEN 0
			ELSE CAST(T1.WorkplanCommitted / T1.WorkplanAllocation * 100 AS NUMERIC(18,2))
		END AS WorkplanCommittedPercent,

		T1.WorkplanDisbursed,

		CASE
			WHEN T1.WorkplanAllocation = 0
			THEN 0
			ELSE CAST(T1.WorkplanDisbursed / T1.WorkplanAllocation * 100 AS NUMERIC(18,2))
		END AS WorkplanDisbursedPercent,

		T1.WorkplanDisbursed AS CommittedDisbursed,

		CASE
			WHEN T1.WorkplanCommitted = 0
			THEN 0
			ELSE CAST(T1.WorkplanDisbursed / T1.WorkplanCommitted * 100 AS NUMERIC(18,2))
		END AS CommittedDisbursedPercent,

		T1.WorkplanAllocation - T1.WorkplanCommitted AS WorkplanUncommitted,

		CASE
			WHEN T1.WorkplanAllocation = 0
			THEN 0
			ELSE CAST((T1.WorkplanAllocation - T1.WorkplanCommitted) / T1.WorkplanAllocation * 100 AS NUMERIC(18,2))
		END AS WorkplanUncommittedPercent,

		T1.PendingCommitment,

		CASE
			WHEN @nPendingCommitmentTotal = 0
			THEN 0
			ELSE CAST(T1.PendingCommitment / @nPendingCommitmentTotal * 100 AS NUMERIC(18,2))
		END AS PendingCommitmentPercent,

		T1.ActivityCount,

		CASE
			WHEN T1.ActivityCount = 0
			THEN 0
			ELSE CAST(CAST(T1.ActivityCount AS NUMERIC(18,2)) / CAST(@nActivityCountTotal AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
		END AS ActivityCountPercent

	FROM @tTable1 T1

	UNION

	SELECT
		'Total',
		@nWorkplanAllocationTotal,
		0,
		@nWorkplanCommittedTotal,
		0,
		@nWorkplanDisbursedTotal,
		0,
		@nWorkplanDisbursedTotal,
		0,
		@nWorkplanAllocationTotal - @nWorkplanCommittedTotal,
		0,
		@nPendingCommitmentTotal,
		0,
		@nActivityCountTotal,
		0

	--Overall Engagement Counts
	INSERT INTO @tTable2 (ItemName) SELECT CS.ComponentStatusName FROM dropdown.ComponentStatus CS WHERE CS.ComponentStatusID > 0 AND CS.IsActive = 1

	;
	WITH OEC AS
		(
		SELECT
			COUNT(D.ComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM
			(
			SELECT 
				C.CommunityID,
				(
				SELECT MIN(T.ComponentStatusID)
				FROM 
					(
					VALUES (CommunityComponentStatusID),(JusticeComponentStatusID),(PoliceComponentStatusID)
					) AS T(ComponentStatusID)
				) AS ComponentStatusID
			FROM dbo.Community C
			WHERE C.IsActive = 1
			) D
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = D.ComponentStatusID
				AND CS.ComponentStatusID > 0
		GROUP BY D.ComponentStatusID, CS.ComponentStatusName
		)

	UPDATE T2
	SET T2.ItemCount = OEC.ItemCount
	FROM @tTable2 T2
		JOIN OEC ON OEC.ItemName = T2.ItemName

	SELECT 
		T2.ItemName,
		T2.ItemCount
	FROM @tTable2 T2
	ORDER BY T2.ItemName

	--Permitted Engagement Counts
	DELETE FROM @tTable2
	INSERT INTO @tTable2 (ItemName) SELECT ID.ImpactDecisionName FROM dropdown.ImpactDecision ID WHERE ID.ImpactDecisionID > 0 AND ID.IsActive = 1

	;
	WITH PEC AS
		(
		SELECT
			COUNT(ID.ImpactDecisionID) AS ItemCount,
			ID.ImpactDecisionName AS ItemName
		FROM dbo.Community C
			JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
				AND C.IsActive = 1
				AND ID.IsActive = 1
		GROUP BY ID.ImpactDecisionID, ID.ImpactDecisionName
		)

	UPDATE T2
	SET T2.ItemCount = PEC.ItemCount
	FROM @tTable2 T2
		JOIN PEC ON PEC.ItemName = T2.ItemName

	SELECT 
		T2.ItemName,
		T2.ItemCount
	FROM @tTable2 T2
	ORDER BY T2.ItemName

END
GO
--End procedure dbo.GetDashboardItemCounts

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.27
-- Description:	Bug fixes
--
-- Author:			Todd Pires
-- Create Date: 2015.09.27
-- Description:	Added Recommendations
--
-- Author:			Todd Pires
-- Create Date: 2016.02.11
-- Description:	Added Program Report
--
-- Author:			Todd Pires
-- Create Date: 2016.03.22
-- Description:	Implemented support for the new workflow system
--
-- Author:			Brandon Green
-- Create Date: 2016.08.15
-- Description:	Implemented support incident reports, atmospheric reports and ARAP docs
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
-- ====================================================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN 'fa fa-fw fa-lightbulb-o'
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN 'fa fa-fw fa-question-circle'
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN 'fa fa-fw fa-calendar'
			WHEN ET.EntityTypeCode = 'Incident'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'Document'
			THEN 'fa fa-fw fa-file'
		END AS Icon,

		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN OAR.RecommendationName
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			WHEN D.EntityTypeCode = 'Incident'
			THEN OAI.IncidentName
			WHEN D.EntityTypeCode = 'Document'
			THEN OAD.DocumentTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		CASE
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN OAWR.PhysicalFileName
			WHEN ET.EntityTypeCode = 'Document'
			THEN OAD.PhysicalFileName
			ELSE NULL
		END AS PhysicalFileName,

		D.EntityID,
		D.UpdateDate,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('Recommendation','RequestForInformation','SpotReport','WeeklyReport','Incident','Document')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'Recommendation' AND permissionable.HasPermission('Recommendation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'Document')
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				R.RecommendationName,
				R.IsActive
			FROM recommendation.Recommendation R
			WHERE R.RecommendationID = D.EntityID
					AND D.EntityTypeCode = 'Recommendation'
			) OAR
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle,
				workflow.GetWorkflowStepNumber(D.EntityTypeCode, D.EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(D.EntityTypeCode, D.EntityID) AS WorkflowStepCount
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		OUTER APPLY
			(
			SELECT
				INC.IncidentName
			FROM dbo.Incident INC
			WHERE INC.IncidentID = D.EntityID
				AND D.EntityTypeCode = 'Incident'
			) OAI
		OUTER APPLY
			(
			SELECT
				CASE
					WHEN permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
					THEN DOC.DocumentTitle
					ELSE NULL
				END AS DocumentTitle,

				CASE
					WHEN permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
					THEN DOC.PhysicalFileName
					ELSE NULL
				END AS PhysicalFileName
			FROM dbo.Document DOC
			JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = DOC.DocumentTypeID
				AND DOC.DocumentTypeID > 0 
				AND DT.IsActive = 1
			WHERE DOC.DocumentID = D.EntityID
				AND DOC.DisplayInCommunityNewsFeed = 1 
				AND D.EntityTypeCode = 'Document'
			) OAD
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
			AND
				(
				D.EntityTypeCode <> 'Recommendation'
					OR OAR.IsActive = 1
				)
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
			AND
				(
				D.EntityTypeCode <> 'SpotReport'
					OR OASR.WorkflowStepNumber > OASR.WorkflowStepCount
				)
			AND
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)
			AND
				(
				D.EntityTypeCode <> 'Incident'
					OR OAI.IncidentName IS NOT NULL
				)
			AND
				(
				D.EntityTypeCode <> 'Document'
					OR OAD.DocumentTitle IS NOT NULL
				)

	UNION
	
	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		'fa-newspaper-o' AS Icon,
		ET.EntityTypeName,
		D.DocumentTitle AS Title,
		D.PhysicalFileName,
		0 AS EntityID,
		D.DocumentDate AS UpdateDate,
		dbo.FormatDate(D.DocumentDate) AS UpdateDateFormatted
	FROM dbo.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DT.DocumentTypeCode = 'ProgramReport'
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = DT.DocumentTypeCode
			AND D.DocumentDate >= DATEADD(d, -14, getDate())
			AND permissionable.HasPermission('ProgramReport.View', @PersonID) = 1

	UNION

	SELECT
		'EngagementPermitted' AS EntityTypeCode,
		LOWER('community') AS Controller,
		'fa fa-fw fa-bolt' AS Icon,
		'Community Engagement Permitted Change' AS EntityTypeName,			
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title,
		NULL AS PhysicalFileName,
		CIDH.CommunityID AS EntityID,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID

	ORDER BY 8 DESC, 1, 7
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dbo.GetProvinceFeed
EXEC Utility.DropObject 'dbo.GetProvinceFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.25
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetProvinceFeed

@PersonID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)
					AND IC.IncidentID = ELD.EntityID

				UNION

				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.ProvinceID = @ProvinceID
					AND IP.IncidentID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)
					AND SRC.SpotReportID = ELD.EntityID

				UNION

				SELECT 1
				FROM dbo.SpotReportProvince SRP
				WHERE SRP.ProvinceID = @ProvinceID
					AND SRP.SpotReportID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Community Engagement Permitted Change' AS EntityTypeName,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS PhysicalFileName,
		C.CommunityName + 'Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
	WHERE P.ProvinceID = @ProvinceID


	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetProvinceFeed

--Begin procedure dropdown.GetWorkplanActivityData
EXEC Utility.DropObject 'dropdown.GetWorkplanActivityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.12.17
-- Description:	A stored procedure to return data from the workplan.WorkplanActivity table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetWorkplanActivityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.WorkplanActivityID, 
		T.WorkplanActivityName
	FROM workplan.WorkplanActivity T
		JOIN workplan.Workplan WP ON WP.WorkplanID = T.WorkplanID
			AND WP.IsActive = 1
	ORDER BY T.WorkplanActivityName, T.WorkplanActivityID
	
END
GO
--End procedure dropdown.GetWorkplanActivityData

--Begin procedure force.GetForceByEventLogID
EXEC Utility.DropObject 'force.GetForceByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.29
-- Description:	A stored procedure to force data from the eventlog.EventLog table
-- Notes:				Changes here must ALSO be made to force.GetForceByForceID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
--
-- Author:			Eric Jones
-- Create date:	2016.12.07
-- Description:	implemented eventlog data to populate revision history table
--
-- Author:			Eric Jones
-- Create date:	2016.12.15
-- Description:	added eventlog id to query result set.
-- ==============================================================================
CREATE PROCEDURE force.GetForceByEventLogID

@EventLogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Force.value('CommanderFullName[1]', 'VARCHAR(250)') AS CommanderFullName,
		Force.value('Comments[1]', 'VARCHAR(250)') AS Comments,
		Force.value('DeputyCommanderFullName[1]', 'VARCHAR(250)') AS DeputyCommanderFullName,
		Force.value('ForceDescription[1]', 'VARCHAR(250)') AS ForceDescription,
		Force.value('ForceID[1]', 'INT') AS ForceID,
		Force.value('ForceName[1]', 'VARCHAR(250)') AS ForceName,
		Force.value('History[1]', 'VARCHAR(250)') AS History,
		Force.value('Location[1]', 'VARCHAR(MAX)') AS Location,
		Force.value('Notes[1]', 'VARCHAR(250)') AS Notes,
		Force.value('TerritoryID[1]', 'INT') AS TerritoryID,
		Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)'), Force.value('TerritoryID[1]', 'INT')) AS TerritoryName,
		Force.value('WebLinks[1]', 'VARCHAR(250)') AS WebLinks,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		AOO.AreaOfOperationTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force') AS T(Force)
		JOIN dropdown.AreaOfOperationType AOO ON AOO.AreaOfOperationTypeID = Force.value('AreaOfOperationTypeID[1]', 'INT')
			AND EL.EventLogID = @EventLogID

	SELECT
		Force.value('(CommunityID)[1]', 'INT') AS CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceCommunities/ForceCommunity') AS T(Force)
			JOIN dbo.Community C ON C.CommunityID = Force.value('(CommunityID)[1]', 'INT')
			JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
				AND EL.EventLogID = @EventLogID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceEquipmentResourceProviders/ForceEquipmentResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceFinancialResourceProviders/ForceFinancialResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(RiskID)[1]', 'INT') AS RiskID,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceRisks/ForceRisk') AS T(Force)
			JOIN dbo.Risk R ON R.RiskID = Force.value('(RiskID)[1]', 'INT')
			JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
				AND EL.EventLogID = @EventLogID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		Force.value('(ForceUnitID)[1]', 'INT') AS ForceUnitID,
		Force.value('CommanderFullName[1]', 'VARCHAR(250)') AS CommanderFullName,
		Force.value('DeputyCommanderFullName[1]', 'VARCHAR(250)') AS DeputyCommanderFullName,
		Force.value('(TerritoryID)[1]', 'INT') AS TerritoryID,
		Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)'), Force.value('(TerritoryID)[1]', 'INT')) AS TerritoryName,
		Force.value('(UnitName)[1]', 'VARCHAR(250)') AS UnitName,
		Force.value('(UnitTypeID)[1]', 'INT') AS UnitTypeID,
		UT.UnitTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceUnits/ForceUnit') AS T(Force)
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = Force.value('(UnitTypeID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY Force.value('(UnitName)[1]', 'VARCHAR(250)'), Force.value('(ForceUnitID)[1]', 'INT')
	
	SELECT 
		EL.eventLogID, 
		EL.eventcode, 
		EL.eventdata, 
		dbo.FormatDateTime(EL.createdatetime) AS createDateFormatted, 
		dbo.FormatPersonName(P.firstname, P.lastname,'','LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
		JOIN person P on p.personid = el.personid
	WHERE EL.EntityTypeCode = 'force' AND EL.eventcode != 'read' AND EL.entityid = (SELECT EntityID FROM eventlog.EventLog WHERE EventLogID = @EventLogID)
	
END
GO
--End procedure force.GetForceByEventLogID

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- Notes:				Changes here must ALSO be made to force.GetForceByEventLogID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
--
-- Author:			Eric Jones
-- Create date:	2016.12.07
-- Description:	implemented eventlog data to populate revision history table
--
-- Author:			Eric Jones
-- Create date:	2016.12.15
-- Description:	added eventlog id to query result set.
-- =========================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderContactID, 
		FU.DeputyCommanderContactID,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
	SELECT 
		EL.eventLogID,
		EL.eventcode, 
		EL.eventdata, 
		dbo.FormatDateTime(EL.createdatetime) AS createDateFormatted, 
		dbo.FormatPersonName(P.firstname, P.lastname,'','LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
		JOIN person P on p.personid = el.personid
	WHERE EL.EntityTypeCode = 'force' AND EL.entityid = @ForceID AND EL.eventcode != 'read' 
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure workplan.GetWorkplanActivityConceptNotes
EXEC Utility.DropObject 'workplan.GetWorkplanActivityConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.21
-- Description:	A stored procedure to get concept note data based on a WorkplanActivityID
-- ======================================================================================
CREATE PROCEDURE workplan.GetWorkplanActivityConceptNotes

@nPersonID INT,
@WorkplanActivityID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH BD AS
		(
		SELECT
			B.ConceptNoteID,
			FORMAT(SUM(B.SpentToDate), 'C', 'en-us') AS SpentToDateFormatted,
			FORMAT(SUM(B.TotalCost), 'C', 'en-us') AS TotalCostFormatted
		FROM
			(
			SELECT
				CNEC.ConceptNoteID,
				CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS SpentToDate,
				CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost
			FROM dbo.ConceptNoteEquipmentCatalog CNEC
				JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNEC.ConceptNoteID
					AND CN.WorkplanActivityID = @WorkplanActivityID

			UNION

			SELECT
				CNB.ConceptNoteID,
				CNB.SpentToDate,
				CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost
			FROM dbo.ConceptNoteBudget CNB
				JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNB.ConceptNoteID
					AND CN.WorkplanActivityID = @WorkplanActivityID
			) B
		GROUP BY B.ConceptNoteID
		)

	SELECT 
		CN.ConceptNoteID,
		CN.ActivityCode, 
		CN.Title, 
		CNS.ConceptNoteStatusName,
		BD.SpentToDateFormatted,
		BD.TotalCostFormatted,
		permissionable.HasPermission('ConceptNote.View', @nPersonID) AS HasPermission
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN BD ON BD.ConceptNoteID = CN.ConceptNoteID
			AND CN.WorkplanActivityID = @WorkplanActivityID
	
END
GO
--End procedure workplan.GetWorkplanActivityConceptNotes

--Begin procedure workplan.GetWorkplanByWorkplanID
EXEC Utility.DropObject 'workplan.GetWorkplanByWorkplanID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.21
-- Description:	A stored procedure to get data from the workplan.Workplan table
--
-- Author:			Todd Pires
-- Create date: 2016.12.17
-- Description:	Added activity counts to the Activity Performance result set
-- ============================================================================
CREATE PROCEDURE workplan.GetWorkplanByWorkplanID

@WorkplanID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		WP.EndDate, 
		dbo.FormatDate(WP.EndDate) AS EndDateFormatted,
		WP.IsActive, 
		WP.IsForDashboard, 
		WP.StartDate,
		dbo.FormatDate(WP.StartDate) AS StartDateFormatted,
		WP.UKContractNumber, 
		WP.USContractNumber, 
		WP.USDToGBPExchangeRate,
		WP.WorkplanID, 
		WP.WorkplanName
	FROM workplan.Workplan WP
	WHERE	WP.WorkplanID = @WorkplanID

	SELECT
		CNT.ConceptNoteTypeName,
		FS1.FundingSourceName AS CurrentFundingSourceName,
		FS2.FundingSourceName AS OriginalFundingSourceName,
		FORMAT(WPA.CurrentUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS CurrentGBPAllocation,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		WPA.Description,
		WPA.Notes,
		FORMAT(WPA.OriginalUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS OriginalGBPAllocation,
		FORMAT(WPA.OriginalUSDAllocation, 'C', 'en-us') AS OriginalUSDAllocation,
		WPA.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM workplan.WorkplanActivity WPA
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		JOIN dropdown.FundingSource FS1 ON FS1.FundingSourceID = WPA.CurrentFundingSourceID
		JOIN dropdown.FundingSource FS2 ON FS2.FundingSourceID = WPA.OriginalFundingSourceID
			AND WPA.WorkplanID = @WorkplanID

	SELECT
		D.WorkplanActivityID,
		WPA.WorkplanActivityName,
		CNT.ConceptNoteTypeName,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		FORMAT(SUM(D.Planned), 'C', 'en-us') AS Planned,
		FORMAT(SUM(D.Disbursed), 'C', 'en-us') AS Disbursed,
		FORMAT(SUM(D.CommittedBalance), 'C', 'en-us') AS CommittedBalance,
		(
		SELECT COUNT(CN1.ConceptNoteID) 
		FROM dbo.ConceptNote CN1 
			JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN1.ConceptNoteStatusID
				AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','Closed','OnHold')
				AND CN1.WorkplanActivityID = D.WorkplanActivityID
		) AS ConceptNoteCount
	FROM
		(
		SELECT 
			WPA.WorkplanActivityID,

			CASE
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) = 'Development'
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal
				ELSE 0
			END AS Planned,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				THEN CN.ActualTotalAmount + OACNF.ConceptNoteFinanceTotal
				ELSE 0
			END AS Disbursed,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal - CN.ActualTotalAmount - OACNF.ConceptNoteFinanceTotal
				ELSE 0
			END AS CommittedBalance

		FROM dbo.ConceptNote CN
			JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
				AND WPA.WorkplanID = @WorkplanID
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue), 0) AS TotalBudget
				FROM dbo.ConceptNoteBudget CNB
				WHERE CNB.ConceptNoteID = CN.ConceptNoteID
				) OACNB
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0) AS ConceptNoteEquimentTotal
				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
						AND CNCE.ConceptNoteID = CN.ConceptNoteID
				) OACNCE
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNF.drAmt) - SUM(CNF.CrAmt), 0) AS ConceptNoteFinanceTotal
				FROM dbo.ConceptNoteFinance CNF
				WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
				) OACNF
		) D
	JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = D.WorkplanActivityID
	JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
	GROUP BY D.WorkplanActivityID, WPA.WorkplanActivityName, WPA.CurrentUSDAllocation, CNT.ConceptNoteTypeName
	ORDER BY WPA.WorkplanActivityName

END
GO
--End procedure workplan.GetWorkplanByWorkplanID
