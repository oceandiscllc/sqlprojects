USE AJACS
GO

--Begin procedure dbo.GetCommunityFeed
EXEC Utility.DropObject 'dbo.GetCommunityFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- ==================================================================
CREATE PROCEDURE dbo.GetCommunityFeed

@PersonID INT = 0,
@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN document.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID = @CommunityID
					AND IC.IncidentID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS DocumentName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID = @CommunityID
					AND SRC.SpotReportID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Community Engagement Permitted Change' AS EntityTypeName,			
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS DocumentName,
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
			AND CIDH.CommunityID = @CommunityID
			AND CIDH.CreateDateTime >= DATEADD(d, -14, getDate())

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD   
		JOIN document.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
			AND ELD.EntityTypeCode = 'WeeklyReport'

	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetCommunityFeed

--Begin procedure dbo.GetProvinceFeed
EXEC Utility.DropObject 'dbo.GetProvinceFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.25
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetProvinceFeed

@PersonID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN Document.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN Document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)
					AND IC.IncidentID = ELD.EntityID

				UNION

				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.ProvinceID = @ProvinceID
					AND IP.IncidentID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)
					AND SRC.SpotReportID = ELD.EntityID

				UNION

				SELECT 1
				FROM dbo.SpotReportProvince SRP
				WHERE SRP.ProvinceID = @ProvinceID
					AND SRP.SpotReportID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Community Engagement Permitted Change' AS EntityTypeName,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS DocumentName,
		C.CommunityName + 'Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM dbo.CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND P.ProvinceID = @ProvinceID
			AND CIDH.CreateDateTime >= DATEADD(d, -14, getDate())

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD   
		JOIN document.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
			AND ELD.EntityTypeCode = 'WeeklyReport'

	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetProvinceFeed