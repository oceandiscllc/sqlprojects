USE AJACS
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.AuditOutcome AO WHERE AO.AuditOutcomeName = 'Verified - Consumed')
	INSERT INTO dropdown.AuditOutcome (AuditOutcomeName) VALUES ('Verified - Consumed')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Notarial Staff')
	INSERT INTO dropdown.ContactAffiliation (ContactAffiliationName) VALUES ('Notarial Staff')
--ENDIF
IF NOT EXISTS (SELECT 1 FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Registry Staff')
	INSERT INTO dropdown.ContactAffiliation (ContactAffiliationName) VALUES ('Registry Staff')
--ENDIF
IF NOT EXISTS (SELECT 1 FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'ADR Staff')
	INSERT INTO dropdown.ContactAffiliation (ContactAffiliationName) VALUES ('ADR Staff')
--ENDIF
GO
