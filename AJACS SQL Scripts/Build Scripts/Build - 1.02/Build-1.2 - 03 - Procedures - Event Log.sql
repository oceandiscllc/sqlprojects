USE AJACS
GO

--Begin procedure eventlog.LogAtmosphericAction
EXEC utility.DropObject 'eventlog.LogAtmosphericAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAtmosphericAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Atmospheric',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cAtmosphericCommunities VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericCommunities = COALESCE(@cAtmosphericCommunities, '') + D.AtmosphericCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericCommunity'), ELEMENTS) AS AtmosphericCommunity
			FROM dbo.AtmosphericCommunity T 
			WHERE T.AtmosphericID = @EntityID
			) D

		DECLARE @cAtmosphericEngagementCriteriaResults VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericEngagementCriteriaResults = COALESCE(@cAtmosphericEngagementCriteriaResults, '') + D.AtmosphericEngagementCriteriaResult 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericEngagementCriteriaResult'), ELEMENTS) AS AtmosphericEngagementCriteriaResult
			FROM dbo.AtmosphericEngagementCriteriaResult T 
			WHERE T.AtmosphericID = @EntityID
			) D

		DECLARE @cAtmosphericSources VARCHAR(MAX) 
	
		SELECT 
			@cAtmosphericSources = COALESCE(@cAtmosphericSources, '') + D.AtmosphericSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AtmosphericSource'), ELEMENTS) AS AtmosphericSource
			FROM dbo.AtmosphericSource T 
			WHERE T.AtmosphericID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Atmospheric',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<AtmosphericCommunities>' + ISNULL(@cAtmosphericCommunities, '') + '</AtmosphericCommunities>') AS XML),
			CAST(('<AtmosphericEngagementCriteriaResults>' + ISNULL(@cAtmosphericEngagementCriteriaResults, '') + '</AtmosphericEngagementCriteriaResults>') AS XML),
			CAST(('<AtmosphericSources>' + ISNULL(@cAtmosphericSources, '') + '</AtmosphericSources>') AS XML)
			FOR XML RAW('Atmospheric'), ELEMENTS
			)
		FROM dbo.Atmospheric T
		WHERE T.AtmosphericID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAtmosphericAction

--Begin procedure eventlog.LogClassAction
EXEC utility.DropObject 'eventlog.LogClassAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClassAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Class',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cClassContacts VARCHAR(MAX) 
	
		SELECT 
			@cClassContacts = COALESCE(@cClassContacts, '') + D.ClassContact 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ClassContact'), ELEMENTS) AS ClassContact
			FROM dbo.ClassContact T 
			WHERE T.ClassID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog

			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Class',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ClassContacts>' + ISNULL(@cClassContacts, '') + '</ClassContacts>') AS XML)
			FOR XML RAW('Class'), ELEMENTS
			)
		FROM dbo.Class T
		WHERE T.ClassID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClassAction

--Begin procedure eventlog.LogCommunityAction
EXEC utility.DropObject 'eventlog.LogCommunityAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cCommunityEngagementCriteriaResults VARCHAR(MAX) 
	
		SELECT 
			@cCommunityEngagementCriteriaResults = COALESCE(@cCommunityEngagementCriteriaResults, '') + D.CommunityEngagementCriteriaResult 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityEngagementCriteriaResult'), ELEMENTS) AS CommunityEngagementCriteriaResult
			FROM dbo.CommunityEngagementCriteriaResult T 
			WHERE T.CommunityID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<CommunityEngagementCriteriaResults>' + ISNULL(@cCommunityEngagementCriteriaResults, '') + '</CommunityEngagementCriteriaResults>') AS XML)
			FOR XML RAW('Community'), ELEMENTS
			)
		FROM dbo.Community T
		WHERE T.CommunityID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAction

--Begin procedure eventlog.LogCommunityMemberSurveyAction
EXEC utility.DropObject 'eventlog.LogCommunityMemberSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityMemberSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityMemberSurvey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('CommunityMemberSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityMemberSurvey',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('CommunityMemberSurvey'), ELEMENTS
			)
		FROM dbo.CommunityMemberSurvey T
		WHERE T.CommunityMemberSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityMemberSurveyAction

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cContactContactAffiliations VARCHAR(MAX) 
	
		SELECT 
			@cContactContactAffiliations = COALESCE(@cContactContactAffiliations, '') + D.ContactContactAffiliation 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ContactContactAffiliation'), ELEMENTS) AS ContactContactAffiliation
			FROM dbo.ContactContactAffiliation T 
			WHERE T.ContactID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ContactContactAffiliations>' + ISNULL(@cContactContactAffiliations, '') + '</ContactContactAffiliations>') AS XML)
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM dbo.Contact T
		WHERE T.ContactID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogCourseAction
EXEC utility.DropObject 'eventlog.LogCourseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCourseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Course',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Course',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Course'), ELEMENTS
			)
		FROM dbo.Course T
		WHERE T.CourseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCourseAction

--Begin procedure eventlog.LogDailyReportAction
EXEC utility.DropObject 'eventlog.LogDailyReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDailyReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'DailyReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDailyReportCommunities VARCHAR(MAX) 
	
		SELECT 
			@cDailyReportCommunities = COALESCE(@cDailyReportCommunities, '') + D.DailyReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DailyReportCommunity'), ELEMENTS) AS DailyReportCommunity
			FROM dbo.DailyReportCommunity T 
			WHERE T.DailyReportID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'DailyReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<DailyReportCommunities>' + ISNULL(@cDailyReportCommunities, '') + '</DailyReportCommunities>') AS XML)
			FOR XML RAW('DailyReport'), ELEMENTS
			)
		FROM dbo.DailyReport T
		WHERE T.DailyReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDailyReportAction

--Begin procedure eventlog.LogDocumentAction
EXEC utility.DropObject 'eventlog.LogDocumentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDocumentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocumentEntities VARCHAR(MAX) 
	
		SELECT 
			@cDocumentEntities = COALESCE(@cDocumentEntities, '') + D.DocumentEntity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DocumentEntity'), ELEMENTS) AS DocumentEntity
			FROM dbo.DocumentEntity T 
			WHERE T.DocumentID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<DocumentEntities>' + ISNULL(@cDocumentEntities, '') + '</DocumentEntities>') AS XML)
			FOR XML RAW('Document'), ELEMENTS
			)
		FROM dbo.Document T
		WHERE T.DocumentID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDocumentAction

--Begin procedure eventlog.LogEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogEquipmentCatalogAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentCatalogAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EquipmentCatalog'), ELEMENTS
			)
		FROM procurement.EquipmentCatalog T
		WHERE T.EquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentCatalogAction

--Begin procedure eventlog.LogEquipmentInventoryAction
EXEC utility.DropObject 'eventlog.LogEquipmentInventoryAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentInventoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EquipmentInventory'), ELEMENTS
			)
		FROM procurement.EquipmentInventory T
		WHERE T.EquipmentInventoryID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentInventoryAction

--Begin procedure eventlog.LogFocusGroupSurveyAction
EXEC utility.DropObject 'eventlog.LogFocusGroupSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFocusGroupSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'FocusGroupSurvey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('CommunityMemberSurvey', @EntityID)
		DECLARE @cFocusGroupSurveyParticipants VARCHAR(MAX) 
	
		SELECT 
			@cFocusGroupSurveyParticipants = COALESCE(@cFocusGroupSurveyParticipants, '') + D.FocusGroupSurveyParticipant 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FocusGroupSurveyParticipant'), ELEMENTS) AS FocusGroupSurveyParticipant
			FROM dbo.FocusGroupSurveyParticipant T 
			WHERE T.FocusGroupSurveyID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'FocusGroupSurvey',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<FocusGroupSurveyParticipants>' + ISNULL(@cFocusGroupSurveyParticipants, '') + '</FocusGroupSurveyParticipants>') AS XML),
			CAST(@cDocuments AS XML)
			FOR XML RAW('FocusGroupSurvey'), ELEMENTS
			)
		FROM dbo.FocusGroupSurvey T
		WHERE T.FocusGroupSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFocusGroupSurveyAction

--Begin procedure eventlog.LogKeyEventAction
EXEC utility.DropObject 'eventlog.LogKeyEventAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogKeyEventAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'KeyEvent',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cKeyEventProvinces VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventProvinces = COALESCE(@cKeyEventProvinces, '') + D.KeyEventProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventProvince'), ELEMENTS) AS KeyEventProvince
			FROM dbo.KeyEventProvince T 
			WHERE T.KeyEventID = @EntityID
			) D

		DECLARE @cKeyEventCommunities VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventCommunities = COALESCE(@cKeyEventCommunities, '') + D.KeyEventCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventCommunity'), ELEMENTS) AS KeyEventCommunity
			FROM dbo.KeyEventCommunity T 
			WHERE T.KeyEventID = @EntityID
			) D

		DECLARE @cKeyEventEngagementCriteriaResults VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventEngagementCriteriaResults = COALESCE(@cKeyEventEngagementCriteriaResults, '') + D.KeyEventEngagementCriteriaResult 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventEngagementCriteriaResult'), ELEMENTS) AS KeyEventEngagementCriteriaResult
			FROM dbo.KeyEventEngagementCriteriaResult T 
			WHERE T.KeyEventID = @EntityID
			) D

		DECLARE @cKeyEventSources VARCHAR(MAX) 
	
		SELECT 
			@cKeyEventSources = COALESCE(@cKeyEventSources, '') + D.KeyEventSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('KeyEventSource'), ELEMENTS) AS KeyEventSource
			FROM dbo.KeyEventSource T 
			WHERE T.KeyEventID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'KeyEvent',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<KeyEventProvinces>' + ISNULL(@cKeyEventProvinces, '') + '</KeyEventProvinces>') AS XML),
			CAST(('<KeyEventCommunities>' + ISNULL(@cKeyEventCommunities, '') + '</KeyEventCommunities>') AS XML),
			CAST(('<KeyEventEngagementCriteriaResults>' + ISNULL(@cKeyEventEngagementCriteriaResults, '') + '</KeyEventEngagementCriteriaResults>') AS XML),
			CAST(('<KeyEventSources>' + ISNULL(@cKeyEventSources, '') + '</KeyEventSources>') AS XML)
			FOR XML RAW('KeyEvent'), ELEMENTS
			)
		FROM dbo.KeyEvent T
		WHERE T.KeyEventID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogKeyEventAction

--Begin procedure eventlog.LogKeyInformantSurveyAction
EXEC utility.DropObject 'eventlog.LogKeyInformantSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogKeyInformantSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'KeyInformantSurvey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('KeyInformantSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'KeyInformantSurvey',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('KeyInformantSurvey'), ELEMENTS
			)
		FROM dbo.KeyInformantSurvey T
		WHERE T.KeyInformantSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogKeyInformantSurveyAction

--Begin procedure eventlog.LogLicenseAction
EXEC utility.DropObject 'eventlog.LogLicenseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLicenseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'License',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('License', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'License',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('License'), ELEMENTS
			)
		FROM procurement.License T
		WHERE T.LicenseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLicenseAction

--Begin procedure eventlog.LogLicenseEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogLicenseEquipmentCatalogAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLicenseEquipmentCatalogAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'LicenseEquipmentCatalog',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'LicenseEquipmentCatalog',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('LicenseEquipmentCatalog'), ELEMENTS
			)
		FROM procurement.LicenseEquipmentCatalog T
		WHERE T.LicenseEquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLicenseEquipmentCatalogAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Person',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cPersonPermissionables VARCHAR(MAX) 
	
		SELECT 
			@cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPermissionable'), ELEMENTS) AS PersonPermissionable
			FROM permissionable.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<PersonPermissionables>' + ISNULL(@cPersonPermissionables, '') + '</PersonPermissionables>') AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM dbo.Person T
		WHERE T.PersonID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogProvinceAction
EXEC utility.DropObject 'eventlog.LogProvinceAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProvinceAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Province',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Province',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Province'), ELEMENTS
			)
		FROM dbo.Province T
		WHERE T.ProvinceID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProvinceAction

--Begin procedure eventlog.LogRapidPerceptionSurveyAction
EXEC utility.DropObject 'eventlog.LogRapidPerceptionSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRapidPerceptionSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'RapidPerceptionSurvey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('RapidPerceptionSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'RapidPerceptionSurvey',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('RapidPerceptionSurvey'), ELEMENTS
			)
		FROM dbo.RapidPerceptionSurvey T
		WHERE T.RapidPerceptionSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRapidPerceptionSurveyAction

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSpotReportComments VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportComments = COALESCE(@cSpotReportComments, '') + D.SpotReportComment 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportComment'), ELEMENTS) AS SpotReportComment
			FROM dbo.SpotReportComment T 
			WHERE T.SpotReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<SpotReportComments>' + ISNULL(@cSpotReportComments, '') + '</SpotReportComments>') AS XML)
			FOR XML RAW('SpotReport'), ELEMENTS
			)
		FROM dbo.SpotReport T
		WHERE T.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure eventlog.LogStakeholderGroupSurveyAction
EXEC utility.DropObject 'eventlog.LogStakeholderGroupSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogStakeholderGroupSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'StakeholderGroupSurvey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('StakeholderGroupSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'StakeholderGroupSurvey',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('StakeholderGroupSurvey'), ELEMENTS
			)
		FROM dbo.StakeholderGroupSurvey T
		WHERE T.StakeholderGroupSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogStakeholderGroupSurveyAction

--Begin procedure eventlog.LogStationCommanderSurveyAction
EXEC utility.DropObject 'eventlog.LogStationCommanderSurveyAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogStationCommanderSurveyAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'StationCommanderSurvey',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('StationCommanderSurvey', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'StationCommanderSurvey',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('StationCommanderSurvey'), ELEMENTS
			)
		FROM dbo.StationCommanderSurvey T
		WHERE T.StationCommanderSurveyID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogStationCommanderSurveyAction

--Begin procedure eventlog.LogSubContractorAction
EXEC utility.DropObject 'eventlog.LogSubContractorAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.16
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSubContractorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('SubContractor', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('SubContractor'), ELEMENTS
			)
		FROM procurement.SubContractor T
		WHERE T.SubContractorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSubContractorAction

--Begin procedure eventlog.LogTeamAction
EXEC utility.DropObject 'eventlog.LogTeamAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogTeamAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Team',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cTeamMembers VARCHAR(MAX) 
	
		SELECT 
			@cTeamMembers = COALESCE(@cTeamMembers, '') + D.TeamMember 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('TeamMember'), ELEMENTS) AS TeamMember
			FROM dbo.TeamMember T 
			WHERE T.TeamID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Team',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<TeamMembers>' + ISNULL(@cTeamMembers, '') + '</TeamMembers>') AS XML)
			FOR XML RAW('Team'), ELEMENTS
			)
		FROM dbo.Team T
		WHERE T.TeamID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogTeamAction

--Begin procedure eventlog.LogWeeklyReportAction
EXEC utility.DropObject 'eventlog.LogWeeklyReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWeeklyReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'WeeklyReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cWeeklyReportCommunities VARCHAR(MAX) 
		DECLARE @cWeeklyReportProvinces VARCHAR(MAX) 
	
		SELECT 
			@cWeeklyReportCommunities = COALESCE(@cWeeklyReportCommunities, '') + D.WeeklyReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WeeklyReportCommunity'), ELEMENTS) AS WeeklyReportCommunity
			FROM weeklyreport.Community T 
			WHERE T.WeeklyReportID = @EntityID
			) D
	
		SELECT 
			@cWeeklyReportProvinces = COALESCE(@cWeeklyReportProvinces, '') + D.WeeklyReportProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WeeklyReportProvince'), ELEMENTS) AS WeeklyReportProvince
			FROM weeklyreport.Province T 
			WHERE T.WeeklyReportID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'WeeklyReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<WeeklyReportProvinces>' + ISNULL(@cWeeklyReportProvinces, '') + '</WeeklyReportProvinces>') AS XML),
			CAST(('<WeeklyReportCommunities>' + ISNULL(@cWeeklyReportCommunities, '') + '</WeeklyReportCommunities>') AS XML)
			FOR XML RAW('WeeklyReport'), ELEMENTS
			)
		FROM weeklyreport.WeeklyReport T
		WHERE T.WeeklyReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWeeklyReportAction
