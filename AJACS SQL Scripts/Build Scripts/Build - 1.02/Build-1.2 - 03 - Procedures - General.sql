USE AJACS
GO

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
-- ======================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.Title,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		'AJACS-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CourseID,
		C.CourseName
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Community COM on COM.CommunityID = C.CommunityID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT.ConceptNoteTaskID,
		CNT.ConceptNoteTaskName,
		CNT.ConceptNoteTaskDescription,
		CNT.ParentConceptNoteTaskID,
		CNT.StartDate,
		CNT.EndDate,
		CNT.IsComplete,
		CNT1.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT.ParentConceptNoteTaskID
				) CNT1

	WHERE CNT.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT.ConceptNoteTaskName, CNT.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
-- ==================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceID FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID
		
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetDashboardItemCounts
EXEC Utility.DropObject 'dbo.GetDashboardItemCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.19
-- Description:	A stored procedure to get item count data for the dashboard
--
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	Added Spot Reports
-- ========================================================================
CREATE PROCEDURE dbo.GetDashboardItemCounts

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		(SELECT COUNT(A.AtmosphericID) FROM dbo.Atmospheric A) AS AtmosphericCount,
		(SELECT COUNT(C.CommunityID) FROM dbo.Community C JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID) AS CommunityCount,
		(SELECT COUNT(KE.KeyEventID) FROM dbo.KeyEvent KE) AS KeyEventCount,
		(SELECT COUNT(KE.KeyEventID) FROM dbo.KeyEvent KE WHERE KE.IsCritical = 1) AS CriticalKeyEventCount,
		(SELECT COUNT(SR.SpotReportID) FROM dbo.SpotReport SR) AS SpotReportCount,
		(SELECT COUNT(SR.SpotReportID) FROM dbo.SpotReport SR WHERE SR.IsCritical = 1) AS CriticalSpotReportCount

END
GO
--End procedure dbo.GetDashboardItemCounts

--Begin procedure dbo.GetEmailAddressesByPermissionableLineage
EXEC Utility.DropObject 'dbo.GetEmailAddressesByPermissionableLineage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the dbo.Person table
-- =================================================================
CREATE PROCEDURE dbo.GetEmailAddressesByPermissionableLineage

@PermissionableLineage VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.PersonID
	FROM permissionable.PersonPermissionable PP
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND PP.PermissionableLineage = @PermissionableLineage
			AND P.PersonID <> @PersonID
	ORDER BY P.EmailAddress
		
END
GO
--End procedure dbo.GetEmailAddressesByPermissionableLineage

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		0,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				MI.PermissionableLineage IS NULL 
					OR EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE PP.PermissionableLineage = MI.PermissionableLineage
								AND PP.PersonID = @PersonID
						)
				)			
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetRequestForInformationByRequestForInformationID
EXEC Utility.DropObject 'dbo.GetRequestForInformationByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
-- ====================================================================================
CREATE PROCEDURE dbo.GetRequestForInformationByRequestForInformationID

@RequestForInformationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C2.CommunityID,
		C2.CommunityName,
		RFI.CompletedDate,
		dbo.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		RFI.IncidentDate,
		dbo.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.InformationRequested,
		RFI.InProgressDate,
		dbo.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.KnownDetails,
		RFI.Location,
		RFI.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonFullname,
		RFI.RequestDate,
		dbo.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationID,
		RFI.RequestForInformationTitle,
		RFI.RequestPersonID,
		dbo.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonFullname,
		RFIRT.RequestForInformationResultTypeID,
		RFIRT.RequestForInformationResultTypeCode,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusID,
		RFIS.RequestForInformationStatusCode,
		RFIS.RequestForInformationStatusName,
		dbo.GetEntityTypeNameByEntityTypeCode('RequestForInformation') AS EntityTypeName
	FROM dbo.RequestForInformation RFI
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFI.RequestForInformationID = @RequestForInformationID
		OUTER APPLY
				(
				SELECT
					C1.CommunityID,
					C1.CommunityName
				FROM dbo.Community C1
				WHERE C1.CommunityID = RFI.CommunityID
				) C2

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'RequestForInformation'
			AND DE.EntityID = @RequestForInformationID

END
GO
--End procedure dbo.GetRequestForInformationByRequestForInformationID

--Begin procedure dbo.GetServerSetupValuesByServerSetupKey
EXEC Utility.DropObject 'dbo.GetServerSetupValuesByServerSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the AJACSUtility.dbo.ServerSetup table
-- =======================================================================================

CREATE PROCEDURE dbo.GetServerSetupValuesByServerSetupKey

@ServerSetupKey VARCHAR(250)


AS
BEGIN
	
	SELECT SS.ServerSetupValue 
	FROM AJACSUtility.dbo.ServerSetup SS 
	WHERE SS.ServerSetupKey = @ServerSetupKey
	ORDER BY SS.ServerSetupID

END
GO
--End procedure dbo.GetServerSetupValuesByServerSetupKey

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Add the workflow step reqult set
-- =====================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName,
		C.Icon AS CurrentCommunityIcon,
		C.ImpactDecisionName AS CurrentCommunityImpactDecisionName,
		C.StatusChangeName AS CurrentCommunityStatusChangeName,
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		P.Icon AS CurrentProvinceIcon,
		P.ProvinceName,
		P.ImpactDecisionName AS CurrentProvinceImpactDecisionName,
		P.StatusChangeName AS CurrentProvinceStatusChangeName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunityID,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProvinceID,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		'AJACS-SR-A' + RIGHT('0000' + CAST(SR.SpotReportID AS VARCHAR(10)), 4) AS ReferenceCode,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		OUTER APPLY
				(
				SELECT
					C1.CommunityName,
					ID1.ImpactDecisionName,
					SC1.StatusChangeName,
					dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID1.HexColor, '#', '') + '-' + SC1.Direction + '.png' AS Icon
				FROM dbo.Community C1
					JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = C1.ImpactDecisionID
					JOIN dropdown.StatusChange SC1 ON SC1.StatusChangeID = C1.StatusChangeID
						AND C1.CommunityID = SR.CommunityID
				) C
		OUTER APPLY
				(
				SELECT
					P1.ProvinceName,
					ID2.ImpactDecisionName,
					SC2.StatusChangeName,
					dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID2.HexColor, '#', '') + '-' + SC2.Direction + '.png' AS Icon
				FROM dbo.Province P1
					JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = P1.ImpactDecisionID
					JOIN dropdown.StatusChange SC2 ON SC2.StatusChangeID = P1.StatusChangeID
						AND P1.ProvinceID = SR.ProvinceID
				) P

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dbo.GetSubContractorBySubContractorID
EXEC Utility.DropObject 'dbo.GetSubContractorBySubContractorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	A stored procedure to data from the dbo.SubContractor table
-- ========================================================================
CREATE PROCEDURE dbo.GetSubContractorBySubContractorID

@SubContractorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.CountryID AS AddressCountryID,
		C1.CountryName AS AddressCountryName,
		C2.CountryID AS RegistrationCountryID,
		C2.CountryName AS RegistrationCountryName,
		SC.Address,
		SC.PrimaryContactEmailAddress,
		SC.PrimaryContactName,
		SC.PrimaryContactPhone,
		SC.RegistrationNumber,
		SC.SubContractorID,
		SC.SubContractorName,
		SC.TaxNumber,
		SCBT.SubContractorBusinessTypeID,
		SCBT.SubContractorBusinessTypeName,
		SCRT.SubContractorRelationshipTypeID,
		SCRT.SubContractorRelationshipTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('SubContractor') AS EntityTypeName
	FROM dbo.SubContractor SC
		JOIN dropdown.Country C1 ON C1.CountryID = SC.AddressCountryID
		JOIN dropdown.Country C2 ON C2.CountryID = SC.RegistrationCountryID
		JOIN dropdown.SubContractorBusinessType SCBT ON SCBT.SubContractorBusinessTypeID = SC.SubContractorBusinessTypeID
		JOIN dropdown.SubContractorRelationshipType SCRT ON SCRT.SubContractorRelationshipTypeID = SC.SubContractorRelationshipTypeID
		
END
GO
--End procedure dbo.GetSubContractorBySubContractorID

--Begin procedure dbo.GetSubContractors
EXEC Utility.DropObject 'dbo.GetSubContractors'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A stored procedure to get data from the dbo.SubContractor table
-- ============================================================================
CREATE PROCEDURE dbo.GetSubContractors

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		SC.SubContractorID,
		SC.SubContractorName,
		SCRT.SubContractorRelationshipTypeID,
		SCRT.SubContractorRelationshipTypeName
	FROM dbo.SubContractor SC 
		JOIN dropdown.SubContractorRelationshipType SCRT ON SCRT.SubContractorRelationshipTypeID = SC.SubContractorRelationshipTypeID
	ORDER BY SCRT.DisplayOrder, SCRT.SubContractorRelationshipTypeName, SC.SubContractorName, SC.SubContractorID

END
GO
--End procedure dbo.GetSubContractors

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50)
		)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
				THEN 1
				ELSE 0
			END,

		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsValidPassword,IsValidUserName,FullName,RoleName) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)
		
			SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin

--Begin procedure dbo.ValidateProvinceName
EXEC Utility.DropObject 'dbo.ValidateProvinceName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.Province table
-- =======================================================================
CREATE PROCEDURE dbo.ValidateProvinceName

@ProvinceName NVARCHAR(250),
@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.ProvinceID) AS ProvinceCount
	FROM dbo.Province P
	WHERE P.ProvinceName = @ProvinceName
		AND P.ProvinceID <> @ProvinceID

END
GO
--End procedure dbo.ValidateProvinceName

--Begin procedure permissionable.GetPermissionables
EXEC Utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date: 2015.03.08
-- Description:	A stored procedure to get system permissionables
-- =============================================================
CREATE PROCEDURE permissionable.GetPermissionables

@PersonID INT

AS
BEGIN

	DECLARE @nPadLength INT
	
	SELECT @nPadLength = LEN(CAST(COUNT(P.PermissionableID) AS VARCHAR(50)))
	FROM permissionable.Permissionable P
	
	;
	WITH HD (DisplayIndex,PermissionableID,PermissionableClass,ParentPermissionableID,DisplayGroupID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY DG.DisplayOrder, P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			'perm-' + CAST(P.PermissionableID AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			DG.DisplayGroupID,
			1
		FROM permissionable.Permissionable P
			JOIN permissionable.DisplayGroupPermissionable DGP ON DGP.PermissionableID = P.PermissionableID
			JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = DGP.DisplayGroupID
		WHERE P.ParentPermissionableID = 0
	
		UNION ALL
	
		SELECT
			CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			CAST(HD.PermissionableClass + ' perm-' + CAST(P.PermissionableID AS VARCHAR(10)) AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			HD.DisplayGroupID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM permissionable.Permissionable P
			JOIN HD ON HD.PermissionableID = P.ParentPermissionableID
		)
	
	SELECT
		HD1.NodeLevel,
		HD1.ParentPermissionableID,
		HD1.PermissionableID,
		'group-' + CAST(DG.DisplayGroupID AS VARCHAR(10)) + ' ' + RTRIM(REPLACE(HD1.PermissionableClass, 'perm-' + CAST(HD1.PermissionableID AS VARCHAR(10)), '')) AS PermissionableClass,
		HD1.DisplayGroupID,
		DG.DisplayGroupName,
		P.PermissionableName,
	
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentPermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasChildren,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PP.PermissionableLineage AND PP.PersonID = @PersonID AND P.PermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
	
	FROM HD HD1
		JOIN permissionable.Permissionable P ON P.PermissionableID = HD1.PermissionableID
		JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = HD1.DisplayGroupID
	ORDER BY HD1.DisplayIndex

END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'dbo.GetEquipmentyByEquipmentID'
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
-- =====================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EI.BeneficiarySignatory,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.FinalBeneficiaryGroup,
		EI.IMEIMACAddress,
		EI.IssueDate,
		dbo.FormatDate(EI.IssueDate) AS IssueDateFormatted,
		EI.UnitCost,
		EI.ItemDescription,
		EI.ItemName,
		EI.Location,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.SpecificLocation,
		EI.Supplier,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure procurement.GetEquipmentInventoryFilters
EXEC Utility.DropObject 'dbo.GetEquipmentFilters'
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryFilters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A stored procedure to return data from the procurement.EquipmentInventory table
-- ====================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryFilters

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CASE
			WHEN D.FinalBeneficiaryGroup IS NULL
			THEN 'No Final Beneficiary Group Listed'
			ELSE D.FinalBeneficiaryGroup
		END AS FinalBeneficiaryGroup

	FROM (SELECT DISTINCT EI.FinalBeneficiaryGroup FROM procurement.EquipmentInventory EI) D
	ORDER BY D.FinalBeneficiaryGroup

	SELECT 
		CASE
			WHEN D.ItemName IS NULL
			THEN 'No Item Listed'
			ELSE D.ItemName
		END AS ItemName

	FROM (SELECT DISTINCT EI.ItemName FROM procurement.EquipmentInventory EI) D
	ORDER BY D.ItemName

	SELECT 
		CASE
			WHEN D.Location IS NULL
			THEN 'No Location Listed'
			ELSE D.Location
		END AS Location

	FROM (SELECT DISTINCT EI.Location FROM procurement.EquipmentInventory EI) D
	ORDER BY D.Location

END
GO
--End procedure procurement.GetEquipmentInventoryFilters

--Begin procedure utility.MenuItemAddUpdate
EXEC Utility.DropObject 'utility.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to add / update a menu item
-- ===============================================================
CREATE PROCEDURE utility.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineage VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM dbo.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM dbo.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM dbo.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO dbo.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,PermissionableLineage,Icon,IsActive)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@PermissionableLineage,
				@Icon,
				@IsActive
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE dbo.MenuItem SET IsActive = @IsActive WHERE MenuItemID = @nNewMenuItemID
			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM dbo.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF
			IF @NewMenuItemLink IS NOT NULL
				UPDATE dbo.MenuItem SET MenuItemLink = @NewMenuItemLink WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			IF @NewMenuItemText IS NOT NULL
				UPDATE dbo.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE dbo.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			IF @PermissionableLineage IS NOT NULL
				UPDATE dbo.MenuItem SET PermissionableLineage = @PermissionableLineage WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
	
			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM dbo.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM dbo.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure utility.MenuItemAddUpdate

--Begin procedure weeklyreport.ApproveWeeklyReport
EXEC Utility.DropObject 'weeklyreport.ApproveWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
--
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	Renamed from SubmitWeeklyReport to ApproveWeeklyReport
-- ======================================================================
CREATE PROCEDURE weeklyreport.ApproveWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @WeeklyReportID INT
	DECLARE @tOutput TABLE (EntityTypeCode VARCHAR(50), EntityID INT)

	SELECT @WeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @WeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @WeeklyReportID

	INSERT INTO @tOutput (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @WeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.EntityTypeCode, O.EntityID
		FROM @tOutput O
		ORDER BY O.EntityTypeCode, O.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	TRUNCATE TABLE weeklyreport.Community
	TRUNCATE TABLE weeklyreport.Province
	DELETE FROM weeklyreport.WeeklyReport

END
GO
--End procedure weeklyreport.ApproveWeeklyReport

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.WeeklyReportID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O
		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	SELECT
		WR.WeeklyReportID, 
		WR.WorkflowStepNumber 
	FROM weeklyreport.WeeklyReport WR

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'WeeklyReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nWeeklyReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'WeeklyReport'
			AND WSWA.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @nWeeklyReportID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure clean up
EXEC Utility.DropObject 'reporting.GetWeeklyReportImpactDecision'
EXEC Utility.DropObject 'reporting.GetWeeklyReportStatusChange'
EXEC Utility.DropObject 'weeklyreport.SubmitWeeklyReport'
EXEC Utility.DropObject 'weeklyreport.SubmitWeeklyReportForApproval'
GO
--End procedure clean up
