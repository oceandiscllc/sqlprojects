USE AJACS
GO

--Begin table dropdown.ConceptNoteStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ConceptNoteStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConceptNoteStatus
	(
	ConceptNoteStatusID INT IDENTITY(0,1) NOT NULL,
	ConceptNoteStatusCode VARCHAR(50),
	ConceptNoteStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteStatusID'
EXEC utility.SetIndexNonClustered 'IX_ConceptNoteStatusName', @TableName, 'DisplayOrder,ConceptNoteStatusName', 'ConceptNoteStatusID'
GO

SET IDENTITY_INSERT dropdown.ConceptNoteStatus ON
GO

INSERT INTO dropdown.ConceptNoteStatus (ConceptNoteStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.ConceptNoteStatus OFF
GO

INSERT INTO dropdown.ConceptNoteStatus 
	(ConceptNoteStatusCode,ConceptNoteStatusName,DisplayOrder)
VALUES
	('Active', 'Active', 1),
	('OnHold', 'On Hold', 2),
	('Cancelled', 'Cancelled', 3),
	('Closed', 'Closed', 4)
GO
--End table dropdown.ConceptNoteStatus

--Begin table dropdown.ConceptNoteType
DECLARE @TableName VARCHAR(250) = 'dropdown.ConceptNoteType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConceptNoteType
	(
	ConceptNoteTypeID INT IDENTITY(0,1) NOT NULL,
	ConceptNoteTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteTypeID'
EXEC utility.SetIndexNonClustered 'IX_ConceptNoteTypeName', @TableName, 'DisplayOrder,ConceptNoteTypeName', 'ConceptNoteTypeID'
GO

SET IDENTITY_INSERT dropdown.ConceptNoteType ON
GO

INSERT INTO dropdown.ConceptNoteType (ConceptNoteTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ConceptNoteType OFF
GO

INSERT INTO dropdown.ConceptNoteType 
	(ConceptNoteTypeName,DisplayOrder)
VALUES
	('Community Engagement', 1),
	('Material Support', 2),
	('Meetings', 3),
	('Programme Development', 4),
	('Training', 5)
GO
--End table dropdown.ConceptNoteType

--Begin table dropdown.ContactAffiliation
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactAffiliation'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactAffiliation
	(
	ContactAffiliationID INT IDENTITY(0,1) NOT NULL,
	ContactAffiliationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactAffiliationID'
EXEC utility.SetIndexNonClustered 'IX_ContactAffiliationName', @TableName, 'DisplayOrder,ContactAffiliationName', 'ContactAffiliationID'
GO

SET IDENTITY_INSERT dropdown.ContactAffiliation ON
GO

INSERT INTO dropdown.ContactAffiliation (ContactAffiliationID) VALUES (0)

SET IDENTITY_INSERT dropdown.ContactAffiliation OFF
GO

INSERT INTO dropdown.ContactAffiliation 
	(ContactAffiliationName,DisplayOrder)
VALUES
	('Community', 1),
	('Community Security Working Groups', 2),
	('FSP', 3)
GO
--End table dropdown.ContactAffiliation

--Begin table dropdown.Currency
DECLARE @TableName VARCHAR(250) = 'dropdown.Currency'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Currency
	(
	CurrencyID INT IDENTITY(0,1) NOT NULL,
	ISOCurrencyCode CHAR(3),
	CurrencyName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 100
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CurrencyID'
EXEC utility.SetIndexClustered 'IX_Currency', @TableName, 'DisplayOrder,CurrencyName,ISOCurrencyCode'
GO

SET IDENTITY_INSERT dropdown.Currency ON
GO

INSERT INTO dropdown.Currency (CurrencyID) VALUES (0)

SET IDENTITY_INSERT dropdown.Currency OFF
GO

INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('AED','UAE Dirham',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('AFN','Afghani Afghani',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ALL','Albanian Lek',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('AMD','Armenian Dram',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ANG','Netherlands Antillean Guilder',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('AOA','Angolan Kwanza',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ARS','Argentine Peso',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('AUD','Australian Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('AWG','Aruban Florin',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('AZN','Azerbaijanian Manat',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BAM','Bosnia and Herzegovina Convertible Mark',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BBD','Barbados Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BDT','Bangladeshi Taka',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BGN','Bulgarian Lev',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BHD','Bahraini Dinar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BIF','Burundi Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BMD','Bermudian Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BND','Brunei Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BOB','Bolivian Boliviano',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BRL','Brazilian Real',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BSD','Bahamian Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BTN','Bhutanise Ngultrum',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BWP','Botswanan Pula',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BYR','Belarussian Ruble',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('BZD','Belize Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CAD','Canadian Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CDF','Congolais Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CHF','Swiss Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CLP','Chilean Peso',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CNY','Chinese Yuan Renminbi',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('COP','Colombian Peso',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CRC','Costa Rican Colon',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CUP','Cuban Peso',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CVE','Cape Verde Escudo',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('CZK','Czech Koruna',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('DJF','Djibouti Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('DKK','Danish Krone',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('DOP','Dominican Peso',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('DZD','Algerian Dinar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('EGP','Egyptian Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ERN','Eritrean Nakfa',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ETB','Ethiopian Birr',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('EUR','Euro',3)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('FJD','Fiji Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('FKP','Falkland Islands Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GBP','UK Pound Sterling',2)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GEL','Geogrian Lari',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GHS','Ghanan Cedi',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GIP','Gibraltar Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GMD','Gambian Dalasi',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GNF','Guinea Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GTQ','Guatemalian Quetzal',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('GYD','Guyana Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('HKD','Hong Kong Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('HNL','Honduran Lempira',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('HRK','Croatian Kuna',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('HTG','Hatian Gourde',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('HUF','Hungarian Forint',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('IDR','Indonesian Rupiah',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ILS','Israeli New Shekel',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('INR','Indian Rupee',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('IQD','Iraqi Dinar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('IRR','Iranian Rial',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ISK','Iceland Krona',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('JMD','Jamaican Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('JOD','Jordanian Dinar',5)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('JPY','Japanese Yen',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KES','Kenyan Shilling',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KGS','Kyrgyzstan Som',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KHR','Cambodian Riel',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KMF','Comoro Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KPW','North Korean Won',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KRW','South Korean Won',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KWD','Kuwaiti Dinar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KYD','Cayman Islands Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('KZT','Kazakhstan Tenge',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LAK','Lao Kip',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LBP','Lebanese Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LKR','Sri Lanka Rupee',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LRD','Liberian Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LSL','Lesotho Loti',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LTL','Lithuanian Litas',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LVL','Latvian Lats',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('LYD','Libyan Dinar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MAD','Moroccan Dirham',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MDL','Moldovan Leu',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MGA','Malagasy Ariary',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MKD','Macedonian Denar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MMK','Myanmar Kyat',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MNT','Mongolian Tugrik',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MOP','Macaoan Pataca',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MRO','Maurutanian Ouguiya',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MUR','Mauritius Rupee',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MVR','Maldiven Rufiyaa',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MWK','Malawian Kwacha',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MXN','Mexican Peso',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MYR','Malaysian Ringgit',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('MZN','Mozambique Metical',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('NAD','Namibia Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('NGN','Nigerian Naira',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('NIO','Nicaraguan Cordoba Oro',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('NOK','Norwegian Krone',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('NPR','Nepalese Rupee',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('NZD','New Zealand Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('OMR','Omani Rial',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('PAB','Panamean Balboa',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('PEN','Puruvian Nuevo Sol',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('PGK','Papa New Guinean Kina',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('PHP','Philippine Peso',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('PKR','Pakistan Rupee',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('PLN','Polish Zloty',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('PYG','Paraguay Guarani',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('QAR','Qatari Rial',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('RON','New Romanian Leu',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('RSD','Serbian Dinar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('RUB','Russian Ruble',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('RWF','Rwandan Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SAR','Saudi Riyal',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SBD','Solomon Islands Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SCR','Seychelles Rupee',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SDG','Sudanese Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SEK','Swedish Krona',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SGD','Singapore Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SHP','Saint Helena Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SLL','Sierra Leone Leone',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SOS','Somali Shilling',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SRD','Surinam Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SSP','South Sudanese Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('STD','Sao Tome and Principe Dobra',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SVC','El Salvador Colon',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SYP','Syrian Pound',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('SZL','Swaziland Lilangeni',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('THB','Thai Baht',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TJS','Tajikistan Somoni',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TMT','Turkmenistan New Manat',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TND','Tunisian Dinar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TOP','Tongan Pa’anga',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TRY','Turkish Lira',4)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TTD','Trinidad and Tobago Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TWD','New Taiwan Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('TZS','Tanzanian Shilling',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('UAH','Ukranian Hryvnia',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('UGX','Uganda Shilling',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('USD','US Dollar',1)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('UYU','Uragyan Peso Uruguayo',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('UZS','Uzbekistan Sum',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('VEF','Venezuelan Bolivar Fuerte',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('VND','Vietnamise Dong',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('VUV','Vanuatun Vatu',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('WST','Samoan Tala',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('XAF','CFA Franc BEAC',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('XCD','East Caribbean Dollar',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('XOF','CFA Franc BCEAO',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('XPF','CFP Franc',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('YER','Yemeni Rial',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ZAR','South African Rand',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ZMW','New Zambian Kwacha',100)
INSERT INTO dropdown.Currency (ISOCurrencyCode, CurrencyName, DisplayOrder) VALUES ('ZWL','Zimbabwe Dollar',100)
GO
--End table dropdown.Currency

--Begin table dropdown.FundingSource
DECLARE @TableName VARCHAR(250) = 'dropdown.FundingSource'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FundingSource
	(
	FundingSourceID INT IDENTITY(0,1) NOT NULL,
	FundingSourceName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FundingSourceID'
EXEC utility.SetIndexNonClustered 'IX_FundingSourceName', @TableName, 'DisplayOrder,FundingSourceName', 'FundingSourceID'
GO

SET IDENTITY_INSERT dropdown.FundingSource ON
GO

INSERT INTO dropdown.FundingSource (FundingSourceID) VALUES (0)

SET IDENTITY_INSERT dropdown.FundingSource OFF
GO

INSERT INTO dropdown.FundingSource 
	(FundingSourceName,DisplayOrder)
VALUES
	('CSO', 1),
	('Secretariat', 2),
	('Joint', 3)
GO
--End table dropdown.FundingSource

--Begin table dropdown.Implementer
DECLARE @TableName VARCHAR(250) = 'dropdown.Implementer'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Implementer
	(
	ImplementerID INT IDENTITY(0,1) NOT NULL,
	ImplementerName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ImplementerID'
EXEC utility.SetIndexNonClustered 'IX_ImplementerName', @TableName, 'DisplayOrder,ImplementerName', 'ImplementerID'
GO

SET IDENTITY_INSERT dropdown.Implementer ON
GO

INSERT INTO dropdown.Implementer (ImplementerID) VALUES (0)

SET IDENTITY_INSERT dropdown.Implementer OFF
GO

INSERT INTO dropdown.Implementer 
	(ImplementerName,DisplayOrder)
VALUES
	('ASI', 1),
	('CAI', 2),
	('Joint', 3)
GO
--End table dropdown.Implementer

--Begin table dropdown.SubContractorRelationshipType
DECLARE @TableName VARCHAR(250) = 'dropdown.SubContractorRelationshipType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SubContractorRelationshipType
	(
	SubContractorRelationshipTypeID INT IDENTITY(0,1) NOT NULL,
	SubContractorRelationshipTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SubContractorRelationshipTypeID'
EXEC utility.SetIndexNonClustered 'IX_SubContractorRelationshipTypeName', @TableName, 'DisplayOrder,SubContractorRelationshipTypeName', 'SubContractorRelationshipTypeID'
GO

SET IDENTITY_INSERT dropdown.SubContractorRelationshipType ON
GO

INSERT INTO dropdown.SubContractorRelationshipType (SubContractorRelationshipTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.SubContractorRelationshipType OFF
GO

INSERT INTO dropdown.SubContractorRelationshipType 
	(SubContractorRelationshipTypeName,DisplayOrder)
VALUES
	('Resource Partner', 1),
	('Vendor', 2)
GO
--End table dropdown.SubContractorRelationshipType

--Begin table dropdown.SubContractorBusinessType
DECLARE @TableName VARCHAR(250) = 'dropdown.SubContractorBusinessType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SubContractorBusinessType
	(
	SubContractorBusinessTypeID INT IDENTITY(0,1) NOT NULL,
	SubContractorBusinessTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SubContractorBusinessTypeID'
EXEC utility.SetIndexNonClustered 'IX_SubContractorBusinessTypeName', @TableName, 'DisplayOrder,SubContractorBusinessTypeName', 'SubContractorBusinessTypeID'
GO

SET IDENTITY_INSERT dropdown.SubContractorBusinessType ON
GO

INSERT INTO dropdown.SubContractorBusinessType (SubContractorBusinessTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.SubContractorBusinessType OFF
GO

INSERT INTO dropdown.SubContractorBusinessType 
	(SubContractorBusinessTypeName,DisplayOrder)
VALUES
	('Company', 1),
	('Charity', 2),
	('Sole Trader', 3),
	('Partnership', 4),
	('Other', 5)
GO
--End table dropdown.SubContractorBusinessType

--Begin table dropdown.RequestForInformationResultType
DECLARE @TableName VARCHAR(250) = 'dropdown.RequestForInformationResultType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RequestForInformationResultType
	(
	RequestForInformationResultTypeID INT IDENTITY(0,1) NOT NULL,
	RequestForInformationResultTypeCode VARCHAR(50),
	RequestForInformationResultTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RequestForInformationResultTypeID'
EXEC utility.SetIndexNonClustered 'IX_RequestForInformationResultTypeName', @TableName, 'DisplayOrder,RequestForInformationResultTypeName', 'RequestForInformationResultTypeID'
GO

SET IDENTITY_INSERT dropdown.RequestForInformationResultType ON
GO

INSERT INTO dropdown.RequestForInformationResultType (RequestForInformationResultTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.RequestForInformationResultType OFF
GO

INSERT INTO dropdown.RequestForInformationResultType 
	(RequestForInformationResultTypeCode,RequestForInformationResultTypeName,DisplayOrder)
VALUES
	('RFIResponse', 'RFI Response', 1),
	('SpotReport', 'Spot Report', 2),
	('Assessment', 'Critical Assessment', 3),
	('Atmospheric', 'Additional Atmospheric', 4)
GO
--End table dropdown.RequestForInformationResultType


--Begin table dropdown.RequestForInformationStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.RequestForInformationStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RequestForInformationStatus
	(
	RequestForInformationStatusID INT IDENTITY(0,1) NOT NULL,
	RequestForInformationStatusCode VARCHAR(50),
	RequestForInformationStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RequestForInformationStatusID'
EXEC utility.SetIndexNonClustered 'IX_RequestForInformationStatusName', @TableName, 'DisplayOrder,RequestForInformationStatusName', 'RequestForInformationStatusID'
GO

SET IDENTITY_INSERT dropdown.RequestForInformationStatus ON
GO

INSERT INTO dropdown.RequestForInformationStatus (RequestForInformationStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.RequestForInformationStatus OFF
GO

INSERT INTO dropdown.RequestForInformationStatus 
	(RequestForInformationStatusCode,RequestForInformationStatusName,DisplayOrder)
VALUES
	('New', 'New', 1),
	('InProgress', 'In Progress', 2),
	('Completed', 'Completed', 3)
GO
--End table dropdown.RequestForInformationStatus

