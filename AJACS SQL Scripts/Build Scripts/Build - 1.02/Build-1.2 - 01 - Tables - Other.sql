USE AJACS
GO

--Begin table dbo.ContactContactAffiliation
DECLARE @TableName VARCHAR(250) = 'dbo.ContactContactAffiliation'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactContactAffiliation
	(
	ContactContactAffiliationID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	ContactAffiliationID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactAffiliationID', 'INT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactContactAffiliationID'
EXEC utility.SetIndexClustered 'IX_ContactContactAffiliation', @TableName, 'ContactID,ContactAffiliationID'
GO
--End table dbo.ContactContactAffiliation

--Begin table dbo.ConceptNote
DECLARE @cTableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC Utility.DropColumn @cTableName, 'FundedBy'
EXEC Utility.DropColumn @cTableName, 'IsActivitySheet'

EXEC Utility.AddColumn @cTableName, 'CanManageContacts', 'BIT'
EXEC Utility.AddColumn @cTableName, 'ConceptNoteStatusID', 'INT'
EXEC Utility.AddColumn @cTableName, 'ConceptNoteTypeCode', 'VARCHAR(50)'
EXEC Utility.AddColumn @cTableName, 'ConceptNoteTypeID', 'INT'
EXEC Utility.AddColumn @cTableName, 'CurrencyID', 'INT'
EXEC Utility.AddColumn @cTableName, 'EndDate', 'DATE'
EXEC Utility.AddColumn @cTableName, 'FundingSourceID', 'INT'
EXEC Utility.AddColumn @cTableName, 'ImplementerID', 'INT'
EXEC Utility.AddColumn @cTableName, 'PlanNotes', 'VARCHAR(MAX)'
EXEC Utility.AddColumn @cTableName, 'StartDate', 'DATE'
EXEC Utility.AddColumn @cTableName, 'WorkflowStepNumber', 'INT'

EXEC Utility.SetDefaultConstraint @cTableName, 'CanManageContacts', 'BIT', 1
EXEC Utility.SetDefaultConstraint @cTableName, 'ConceptNoteStatusID', 'INT', 0
EXEC Utility.SetDefaultConstraint @cTableName, 'ConceptNoteTypeID', 'INT', 0
EXEC Utility.SetDefaultConstraint @cTableName, 'CurrencyID', 'INT', 0
EXEC Utility.SetDefaultConstraint @cTableName, 'FundingSourceID', 'INT', 0
EXEC Utility.SetDefaultConstraint @cTableName, 'ImplementerID', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepNumber', 'INT', 1
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteBudget
DECLARE @cTableName VARCHAR(250) = 'dbo.ConceptNoteBudget'

EXEC Utility.AddColumn @cTableName, 'Ammendments', 'VARCHAR(MAX)'
EXEC Utility.AddColumn @cTableName, 'ItemDescription', 'VARCHAR(MAX)'
EXEC Utility.AddColumn @cTableName, 'NotesToFile', 'VARCHAR(MAX)'
EXEC Utility.AddColumn @cTableName, 'SpentToDate', 'NUMERIC(18,2)'
EXEC Utility.AddColumn @cTableName, 'UnitOfIssue', 'VARCHAR(25)'

EXEC Utility.SetDefaultConstraint @cTableName, 'SpentToDate', 'NUMERIC(18,2)', 0
GO
--End table dbo.ConceptNoteBudget

--Begin table dbo.ConceptNoteClass
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteClass'

EXEC utility.DropObject 'dbo.ClassVetting'
EXEC utility.DropObject 'dropdown.VettingStatus'
EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteClass
	(
	ConceptNoteClassID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	ClassID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteClassID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteClass', @TableName, 'ConceptNoteID,ClassID'
GO
--End table dbo.ConceptNoteClass

--Begin table dbo.ConceptNoteCommunity
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteCommunity
	(
	ConceptNoteCommunityID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteCommunityID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteCommunity', @TableName, 'ConceptNoteID,CommunityID'
GO
--End table dbo.ConceptNoteCommunity

--Begin table dbo.ConceptNoteContact
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContact'

EXEC utility.DropObject 'dbo.ContactVetting'
EXEC utility.DropObject 'dropdown.VettingStatus'
EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteContact
	(
	ConceptNoteContactID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	ContactID INT,
	VettingOutcomeID INT,
	VettingDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'VettingOutcomeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteContactID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteContact', @TableName, 'ConceptNoteID,ContactID'
GO
--End table dbo.ConceptNoteContact

--Begin table dbo.ConceptNoteIndicator
DECLARE @cTableName VARCHAR(250) = 'dbo.ConceptNoteIndicator'

EXEC Utility.AddColumn @cTableName, 'TargetQuantity', 'INT'
EXEC Utility.AddColumn @cTableName, 'Comments', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @cTableName, 'TargetQuantity', 'INT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteProvince
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteProvince'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteProvince
	(
	ConceptNoteProvinceID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteProvinceID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteProvince', @TableName, 'ConceptNoteID,ProvinceID'
GO
--End table dbo.ConceptNoteProvince

--Begin table dbo.ConceptNoteTask
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteTask'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteTask
	(
	ConceptNoteTaskID INT IDENTITY(1,1) NOT NULL,
	ParentConceptNoteTaskID INT,
	ConceptNoteID INT,
	SubContractorID INT,
	ConceptNoteTaskName VARCHAR(100),
	ConceptNoteTaskDescription VARCHAR(MAX),
	StartDate DATE,
	EndDate DATE,
	IsComplete BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentConceptNoteTaskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SubContractorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteTaskID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteTask', @TableName, 'ConceptNoteID,ParentConceptNoteTaskID,ConceptNoteTaskName,ConceptNoteTaskID'
GO
--End table dbo.ConceptNoteTask

--Begin table dbo.RequestForInformation
DECLARE @TableName VARCHAR(250) = 'dbo.RequestForInformation'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.RequestForInformation
	(
  RequestForInformationID INT IDENTITY(1,1) NOT NULL,
	RequestForInformationStatusID INT,
	RequestForInformationResultTypeID INT,
	RequestPersonID INT,
	PointOfContactPersonID INT,
	CommunityID INT,
	Location VARCHAR(250),
	RequestForInformationTitle VARCHAR(100),
	KnownDetails VARCHAR(MAX),
	InformationRequested VARCHAR(MAX),
	IncidentDate DATE,
	RequestDate DATE,
	InProgressDate DATE,
	CompletedDate DATE
	) 

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PointOfContactPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RequestForInformationResultTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RequestForInformationStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RequestDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'RequestPersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RequestForInformationID'
GO
--End table dbo.RequestForInformation

--Begin table dbo.SpotReportComment
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReportComment'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SpotReportComment
	(
	SpotReportCommentID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	Comments VARCHAR(MAX),
	CreatePersonID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportCommentID'
EXEC utility.SetIndexClustered 'IX_SpotReportComment', @TableName, 'SpotReportID,SpotReportCommentID'
GO
--End table dbo.SpotReportComment

--Begin table dbo.SubContractor
DECLARE @TableName VARCHAR(250) = 'dbo.SubContractor'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SubContractor
	(
	SubContractorID INT IDENTITY(1,1) NOT NULL,
	SubContractorName VARCHAR(100),
	SubContractorBusinessTypeID INT,
	SubContractorRelationshipTypeID INT,
	Address VARCHAR(200),
	AddressCountryID INT,
	RegistrationNumber VARCHAR(50),
	RegistrationCountryID INT,
	TaxNumber VARCHAR(50),
	PrimaryContactName VARCHAR(100),
	PrimaryContactPhone VARCHAR(20),
	PrimaryContactEmailAddress VARCHAR(320)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AddressCountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegistrationCountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SubContractorBusinessTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SubContractorRelationshipTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SubContractorID'
EXEC utility.SetIndexClustered 'IX_SubContractor', @TableName, 'SubContractorName,SubContractorID'
GO
--End table dbo.SubContractor

--Begin table eventlog.EventLog
DECLARE @cTableName VARCHAR(250) = 'eventlog.EventLog'

EXEC Utility.AddColumn @cTableName, 'Comments', 'VARCHAR(MAX)'
GO
--End table eventlog.EventLog

--Begin table permissionable.Permissionable
DECLARE @cTableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC Utility.AddColumn @cTableName, 'IsGlobal', 'BIT'
EXEC Utility.AddColumn @cTableName, 'IsWorkflow', 'BIT'

EXEC utility.SetDefaultConstraint @cTableName, 'IsGlobal', 'BIT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'IsWorkflow', 'BIT', 0
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DECLARE @cTableName VARCHAR(250) = 'permissionable.PersonPermissionable'

EXEC utility.DropIndex @cTableName, 'IX_PersonPermissionable'
EXEC Utility.DropColumn @cTableName, 'PermissionableID'
EXEC Utility.AddColumn @cTableName, 'PermissionableLineage', 'VARCHAR(MAX)'

EXEC utility.SetIndexClustered 'IX_PersonPermissionable', @cTableName, 'PersonID'
GO
--End table permissionable.PersonPermissionable

--Begin table procurement.EquipmentInventory
DECLARE @cTableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC Utility.AddColumn @cTableName, 'ConceptNoteID', 'INT'
EXEC Utility.SetDefaultConstraint @cTableName, 'ConceptNoteID', 'INT', 0
GO
--End table procurement.EquipmentInventory

--Begin table procurement.PurchaseOrder
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseOrder'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseOrder
	(
	PurchaseOrderID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	RequestPersonID INT,
	PointOfContactPersonID INT,
	CurrencyID INT,
	DeliveryDate DATE,
	ReferenceCode VARCHAR(50),
	Address VARCHAR(200),
	AddressCountryID INT,
	Notes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AddressCountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CurrencyID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PointOfContactPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RequestPersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PurchaseOrderID'
GO
--End table procurement.PurchaseOrder

--Begin table procurement.PurchaseOrderConceptNoteBudget
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseOrderConceptNoteBudget'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseOrderConceptNoteBudget
	(
	PurchaseOrderConceptNoteBudgetID INT IDENTITY(1,1) NOT NULL,
	PurchaseOrderID INT,
	ConceptNoteBudgetID INT,
	Quantity INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteBudgetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PurchaseOrderID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PurchaseOrderConceptNoteBudgetID'
EXEC utility.SetIndexClustered 'IX_PurchaseOrderConceptNoteBudget', @TableName, 'PurchaseOrderID,ConceptNoteBudgetID'
GO
--End table procurement.PurchaseOrderConceptNoteBudget

--Begin table procurement.PurchaseOrderConceptNoteEquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseOrderConceptNoteEquipmentCatalog'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseOrderConceptNoteEquipmentCatalog
	(
	PurchaseOrderConceptNoteEquipmentCatalogID INT IDENTITY(1,1) NOT NULL,
	PurchaseOrderID INT,
	ConceptNoteEquipmentCatalogID INT,
	Quantity INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteEquipmentCatalogID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PurchaseOrderID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PurchaseOrderConceptNoteEquipmentCatalogID'
EXEC utility.SetIndexClustered 'IX_PurchaseOrderConceptNoteEquipmentCatalog', @TableName, 'PurchaseOrderID,ConceptNoteEquipmentCatalogID'
GO
--End table procurement.PurchaseOrderConceptNoteEquipmentCatalog

--Begin table procurement.PurchaseOrderSubContractor
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseOrderSubContractor'

EXEC utility.DropObject 'dbo.PurchaseOrderSubContractor'
EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseOrderSubContractor
	(
	PurchaseOrderSubContractorID INT IDENTITY(1,1) NOT NULL,
	PurchaseOrderID INT,
	SubContractorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PurchaseOrderID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SubContractorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PurchaseOrderSubContractorID'
EXEC utility.SetIndexClustered 'IX_PurchaseOrderSubContractor', @TableName, 'PurchaseOrderID,SubContractorID'
GO
--End table procurement.PurchaseOrderSubContractor

--Begin table reporting.SearchResult
DECLARE @TableName VARCHAR(250) = 'reporting.SearchResult'

EXEC utility.DropObject @TableName

CREATE TABLE reporting.SearchResult
	(
	SearchResultID BIGINT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SearchResultID'
EXEC utility.SetIndexClustered 'IX_SearchResult', @TableName, 'EntityTypeCode,EntityID,PersonID'
GO
--End table reporting.SearchResult

--Begin table weeklyreport.WeeklyReport
EXEC utility.DropColumn 'weeklyreport.WeeklyReport', 'WeeklyReportStatusCode'
GO

EXEC utility.SetDefaultConstraint 'weeklyreport.WeeklyReport', 'WorkflowStepNumber', 'INT', 1, 1
GO
--End weeklyreport.WeeklyReport

--Begin table workflow.Workflow
DECLARE @cTableName VARCHAR(250) = 'workflow.Workflow'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.Workflow
	(
	WorkflowID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	WorkflowStepCount INT
	)

EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'WorkflowID'
EXEC utility.SetIndexClustered 'IX_Workflow', @cTableName, 'EntityTypeCode,WorkflowID'
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowAction
DECLARE @cTableName VARCHAR(250) = 'workflow.WorkflowAction'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.WorkflowAction
	(
	WorkflowActionID INT IDENTITY(1,1) NOT NULL,
	WorkflowActionCode VARCHAR(50),
	WorkflowActionName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'WorkflowActionID'
EXEC utility.SetIndexClustered 'IX_Action', @cTableName, 'WorkflowActionName,WorkflowActionCode,WorkflowActionID'
GO

INSERT INTO workflow.WorkflowAction
	(WorkflowActionCode,WorkflowActionName)
VALUES
	('Cancel','Cancel'),
	('DecrementWorkflow','Reject'),
	('Hold','Hold'),
	('IncrementWorkflow','Approve'),
	('IncrementWorkflow','Close'),
	('IncrementWorkflow','Submit For Approval'),
	('IncrementWorkflowAndCreateActivity','Approve'),
	('IncrementWorkflowAndCreateActivitySheet','Approve'),
	('IncrementWorkflowAndUploadFile','Upload File & Approve')
GO
--End table workflow.WorkflowAction

--Begin table workflow.WorkflowStep
DECLARE @cTableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.WorkflowStep
	(
	WorkflowStepID INT IDENTITY(1,1) NOT NULL,
	ParentWorkflowStepID INT,
	WorkflowID INT,
	WorkflowStepNumber INT,
	WorkflowStepName VARCHAR(50)
	)

EXEC utility.SetDefaultConstraint @cTableName, 'ParentWorkflowStepID', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowID', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'WorkflowStepID'
EXEC utility.SetIndexClustered 'IX_WorkflowStep', @cTableName, 'WorkflowID,WorkflowStepNumber,WorkflowStepName'
GO
--End table workflow.WorkflowStep

--Begin table workflow.WorkflowStepWorkflowAction
DECLARE @cTableName VARCHAR(250) = 'workflow.WorkflowStepWorkflowAction'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.WorkflowStepWorkflowAction
	(
	WorkflowStepWorkflowActionID INT IDENTITY(1,1) NOT NULL,
	WorkflowID INT,
	WorkflowStepNumber INT,
	WorkflowActionID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowActionID', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowID', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'WorkflowStepWorkflowActionID'
EXEC utility.SetIndexClustered 'IX_WorkflowStepWorkflowAction', @cTableName, 'WorkflowID,WorkflowStepNumber,WorkflowActionID'
GO
--End table workflow.WorkflowStepWorkflowAction

--Begin table workflow.EntityWorkflowStep
DECLARE @cTableName VARCHAR(250) = 'workflow.EntityWorkflowStep'

EXEC utility.DropObject @cTableName

CREATE TABLE workflow.EntityWorkflowStep
	(
	EntityWorkflowStepID INT IDENTITY(1,1) NOT NULL,
	EntityID INT,
	WorkflowStepID INT,
	IsComplete BIT
	)

EXEC utility.SetDefaultConstraint @cTableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'IsComplete', 'BIT', 0
EXEC utility.SetDefaultConstraint @cTableName, 'WorkflowStepID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @cTableName, 'EntityWorkflowStepID'
EXEC utility.SetIndexClustered 'IX_EntityWorkflowStep', @cTableName, 'EntityID,WorkflowStepID'
GO
--End table workflow.EntityWorkflowStep

--Begin table cleanup
EXEC utility.DropObject 'dbo.ContactVetting'
EXEC utility.DropObject 'dbo.Student'
EXEC utility.DropObject 'dbo.PersonMenuItem'
EXEC utility.DropObject 'workflow.EntityWorkflowPerson'
EXEC utility.DropObject 'workflow.WorkflowPerson'
GO

IF EXISTS (SELECT 1 FROM sys.objects O JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID WHERE O.type IN ('U') AND S.Name = 'dbo' AND O.Name = 'Equipment')
	ALTER SCHEMA procurement TRANSFER dbo.Equipment
--ENDIF
GO
IF EXISTS (SELECT 1 FROM sys.objects O JOIN sys.Schemas S ON S.Schema_ID = O.Schema_ID WHERE O.type IN ('U') AND S.Name = 'procurement' AND O.Name = 'Equipment')
	EXEC sp_rename 'procurement.Equipment', 'EquipmentInventory'
--ENDIF
GO
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('procurement.EquipmentInventory') AND SC.Name = 'ItemCost')
	EXEC sp_rename 'procurement.EquipmentInventory.ItemCost', 'UnitCost', 'COLUMN'
--ENDIF
GO
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('procurement.EquipmentInventory') AND SC.Name = 'EquipmentID')
	EXEC sp_rename 'procurement.EquipmentInventory.EquipmentID', 'EquipmentInventoryID', 'COLUMN'
--ENDIF
GO
--End table cleanup
