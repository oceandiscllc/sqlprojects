USE [msdb]
GO

DECLARE @xJobID UNIQUEIDENTIFIER

SELECT @xJobID = Job_ID
FROM dbo.sysjobs_view JV 
WHERE JV.Name = 'AJACS dbo.Atmospheric IsCritical Expiration'

IF @xJobID IS NOT NULL
	EXEC msdb.dbo.sp_delete_job @job_id=@xJobID, @delete_unused_schedule=1
--ENDIF
GO

BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'AJACS dbo.Atmospheric IsCritical Expiration', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'A script to expire out the IsCritical flag in dbo.Atmospheric', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Execution]    Script Date: 3/19/2015 8:26:52 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Execution', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @nAtmosphericID INT
DECLARE @tOutput TABLE (AtmosphericID INT)

UPDATE T
SET T.IsCritical = 0
OUTPUT INSERTED.AtmosphericID INTO @tOutput
FROM dbo.Atmospheric T
WHERE T.IsCritical = 1
	AND DATEDIFF(d, T.AtmosphericDate, getDate()) > CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey(''AtmosphericIsCriticalExpirationDays'', ''10'') AS INT)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT O.AtmosphericID
	FROM @tOutput O
	ORDER BY O.AtmosphericID
	
OPEN oCursor
FETCH oCursor INTO @nAtmosphericID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogAtmosphericAction @nAtmosphericID, ''update'', 0, ''System job expired the IsCritical flag''

	FETCH oCursor INTO @nAtmosphericID
	
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor	
GO', 
		@database_name=N'AJACS', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'AJACS IsCritical Flag Expiration', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20150319, 
		@active_end_date=99991231, 
		@active_start_time=10000, 
		@active_end_time=235959, 
		@schedule_uid=N'92630fb0-e1d1-4773-a2f6-b6debb3140f6'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

USE [msdb]
GO

DECLARE @xJobID UNIQUEIDENTIFIER

SELECT @xJobID = Job_ID
FROM dbo.sysjobs_view JV 
WHERE JV.Name = 'AJACS dbo.KeyEvent IsCritical Expiration'

IF @xJobID IS NOT NULL
	EXEC msdb.dbo.sp_delete_job @job_id=@xJobID, @delete_unused_schedule=1
--ENDIF
GO

BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'AJACS dbo.KeyEvent IsCritical Expiration', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'A script to expire out the IsCritical flag in dbo.KeyEvent', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Execution]    Script Date: 3/19/2015 8:35:50 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Execution', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @nKeyEventID INT
DECLARE @tOutput TABLE (KeyEventID INT)

UPDATE T
SET T.IsCritical = 0
OUTPUT INSERTED.KeyEventID INTO @tOutput
FROM dbo.KeyEvent T
WHERE T.IsCritical = 1
	AND DATEDIFF(d, T.KeyEventDate, getDate()) > CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey(''KeyEventIsCriticalExpirationDays'', ''10'') AS INT)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT O.KeyEventID
	FROM @tOutput O
	ORDER BY O.KeyEventID
	
OPEN oCursor
FETCH oCursor INTO @nKeyEventID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogKeyEventAction @nKeyEventID, ''update'', 0, ''System job expired the IsCritical flag''

	FETCH oCursor INTO @nKeyEventID
	
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor	
GO', 
		@database_name=N'AJACS', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'AJACS IsCritical Flag Expiration', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20150319, 
		@active_end_date=99991231, 
		@active_start_time=10000, 
		@active_end_time=235959, 
		@schedule_uid=N'92630fb0-e1d1-4773-a2f6-b6debb3140f6'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

USE [msdb]
GO

DECLARE @xJobID UNIQUEIDENTIFIER

SELECT @xJobID = Job_ID
FROM dbo.sysjobs_view JV 
WHERE JV.Name = 'AJACS dbo.SpotReport IsCritical Expiration'

IF @xJobID IS NOT NULL
	EXEC msdb.dbo.sp_delete_job @job_id=@xJobID, @delete_unused_schedule=1
--ENDIF
GO

BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'AJACS dbo.SpotReport IsCritical Expiration', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'A script to expire out the IsCritical flag in dbo.SpotReport', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Execution]    Script Date: 3/19/2015 8:39:58 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Execution', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @nSpotReportID INT
DECLARE @tOutput TABLE (SpotReportID INT)

UPDATE T
SET T.IsCritical = 0
OUTPUT INSERTED.SpotReportID INTO @tOutput
FROM dbo.SpotReport T
WHERE T.IsCritical = 1
	AND DATEDIFF(d, T.SpotReportDate, getDate()) > CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey(''SpotReportIsCriticalExpirationDays'', ''10'') AS INT)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT O.SpotReportID
	FROM @tOutput O
	ORDER BY O.SpotReportID
	
OPEN oCursor
FETCH oCursor INTO @nSpotReportID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogSpotReportAction @nSpotReportID, ''update'', 0, ''System job expired the IsCritical flag''

	FETCH oCursor INTO @nSpotReportID
	
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor	
GO', 
		@database_name=N'AJACS', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'AJACS IsCritical Flag Expiration', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20150319, 
		@active_end_date=99991231, 
		@active_start_time=10000, 
		@active_end_time=235959, 
		@schedule_uid=N'92630fb0-e1d1-4773-a2f6-b6debb3140f6'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO