USE AJACS
GO

--Begin procedure dropdown.GetConceptNoteStatusData
EXEC Utility.DropObject 'dropdown.GetConceptNoteStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	A stored procedure to return data from the dropdown.ConceptNoteStatus table
-- ========================================================================================
CREATE PROCEDURE dropdown.GetConceptNoteStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConceptNoteStatusID, 
		T.ConceptNoteStatusCode,
		T.ConceptNoteStatusName
	FROM dropdown.ConceptNoteStatus T
	WHERE (T.ConceptNoteStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConceptNoteStatusName, T.ConceptNoteStatusID

END
GO
--End procedure dropdown.GetConceptNoteStatusData

--Begin procedure dropdown.GetConceptNoteTypeData
EXEC Utility.DropObject 'dropdown.GetConceptNoteTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	A stored procedure to return data from the dropdown.ConceptNoteType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConceptNoteTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConceptNoteTypeID, 
		T.ConceptNoteTypeName
	FROM dropdown.ConceptNoteType T
	WHERE (T.ConceptNoteTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConceptNoteTypeName, T.ConceptNoteTypeID

END
GO
--End procedure dropdown.GetConceptNoteTypeData

--Begin procedure dropdown.GetContactAffiliationData
EXEC Utility.DropObject 'dropdown.GetContactAffiliationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	A stored procedure to return data from the dropdown.ContactAffiliation table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetContactAffiliationData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactAffiliationID,
		T.ContactAffiliationName
	FROM dropdown.ContactAffiliation T
	WHERE (T.ContactAffiliationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactAffiliationName, T.ContactAffiliationID

END
GO
--End procedure dropdown.GetContactAffiliationData

--Begin procedure dropdown.GetCurrencyData
EXEC Utility.DropObject 'dropdown.GetCurrencyData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A stored procedure to return data from the dropdown.Currency table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetCurrencyData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CurrencyID,
		T.ISOCurrencyCode,
		T.CurrencyName
	FROM dropdown.Currency T
	WHERE (T.CurrencyID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CurrencyName, T.CurrencyID

END
GO
--End procedure dropdown.GetCurrencyData

--Begin procedure dropdown.GetFundingSourceData
EXEC Utility.DropObject 'dropdown.GetFundingSourceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	A stored procedure to return data from the dropdown.FundingSource table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFundingSourceData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FundingSourceID, 
		T.FundingSourceName
	FROM dropdown.FundingSource T
	WHERE (T.FundingSourceID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FundingSourceName, T.FundingSourceID

END
GO
--End procedure dropdown.GetFundingSourceData

--Begin procedure dropdown.GetImplementerData
EXEC Utility.DropObject 'dropdown.GetImplementerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	A stored procedure to return data from the dropdown.Implementer table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetImplementerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ImplementerID, 
		T.ImplementerName
	FROM dropdown.Implementer T
	WHERE (T.ImplementerID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ImplementerName, T.ImplementerID

END
GO
--End procedure dropdown.GetImplementerData

--Begin procedure dropdown.GetSubContractorBusinessTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorBusinessTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A stored procedure to return data from the dropdown.SubContractorBusinessType table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorBusinessTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorBusinessTypeID, 
		T.SubContractorBusinessTypeName
	FROM dropdown.SubContractorBusinessType T
	WHERE (T.SubContractorBusinessTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorBusinessTypeName, T.SubContractorBusinessTypeID

END
GO
--End procedure dropdown.GetSubContractorBusinessTypeData

--Begin procedure dropdown.GetSubContractorRelationshipTypeData
EXEC Utility.DropObject 'dropdown.GetSubContractorRelationshipTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A stored procedure to return data from the dropdown.SubContractorRelationshipType table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetSubContractorRelationshipTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SubContractorRelationshipTypeID, 
		T.SubContractorRelationshipTypeName
	FROM dropdown.SubContractorRelationshipType T
	WHERE (T.SubContractorRelationshipTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SubContractorRelationshipTypeName, T.SubContractorRelationshipTypeID

END
GO
--End procedure dropdown.GetSubContractorRelationshipTypeData

--Begin procedure dropdown.GetRequestForInformationResultTypeData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationResultTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationResultType table
-- ======================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationResultTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationResultTypeID, 
		T.RequestForInformationResultTypeCode,
		T.RequestForInformationResultTypeName
	FROM dropdown.RequestForInformationResultType T
	WHERE (T.RequestForInformationResultTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationResultTypeName, T.RequestForInformationResultTypeID

END
GO
--End procedure dropdown.GetRequestForInformationResultTypeData

--Begin procedure dropdown.GetRequestForInformationStatusData
EXEC Utility.DropObject 'dropdown.GetRequestForInformationStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to return data from the dropdown.RequestForInformationStatus table
-- ==================================================================================================
CREATE PROCEDURE dropdown.GetRequestForInformationStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RequestForInformationStatusID, 
		T.RequestForInformationStatusCode,
		T.RequestForInformationStatusName
	FROM dropdown.RequestForInformationStatus T
	WHERE (T.RequestForInformationStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RequestForInformationStatusName, T.RequestForInformationStatusID

END
GO
--End procedure dropdown.GetRequestForInformationStatusData

