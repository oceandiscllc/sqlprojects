USE AJACSUtility
GO

--Begin table dbo.ServerSetup
DELETE SS
FROM dbo.ServerSetup SS
WHERE SS.ServerSetupKey IN ('AtmosphericIsCriticalExpirationDays','HostName','KeyEventIsCriticalExpirationDays','RFIMailTo','SpotReportIsCriticalExpirationDays')

IF (SELECT AJACS.dbo.GetServerSetupValueByServerSetupKey('Environment', '')) = 'Prod'
	BEGIN
	
	INSERT INTO dbo.ServerSetup
		(ServerSetupKey, ServerSetupValue)
	VALUES
		('RFIMailTo', 'celestinep@ajacscoms.com'),
		('RFIMailTo', 'victoriaj@ajacscoms.com'),
		('RFIMailTo', 'elizabethm@ajacscoms.com'),
		('HostName', 'WINDOWS-472HA4B'),
		('AtmosphericIsCriticalExpirationDays', '10'),
		('KeyEventIsCriticalExpirationDays', '10'),
		('SpotReportIsCriticalExpirationDays', '10')

	END
ELSE
	BEGIN
	
	INSERT INTO dbo.ServerSetup
		(ServerSetupKey, ServerSetupValue)
	VALUES
		('RFIMailTo', AJACS.dbo.GetServerSetupValueByServerSetupKey('NoReply', '')),
		('HostName', 'WIN-9FC0AS6CIG5'),
		('AtmosphericIsCriticalExpirationDays', '10'),
		('KeyEventIsCriticalExpirationDays', '10'),
		('SpotReportIsCriticalExpirationDays', '10')
		
	END
--ENDIF
GO
--End table dbo.ServerSetup

USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'RequestForInformation')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('RequestForInformation', 'Request For Information')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'SubContractor')
	INSERT INTO dbo.EntityType (EntityTypeGroupCode,EntityTypeCode,EntityTypeName) VALUES ('ConceptNote', 'SubContractor', 'Sub Contractor')
--ENDIF
GO

UPDATE dbo.EntityType
SET EntityTypeGroupCode = 'ConceptNote'
WHERE EntityTypeCode IN ('ConceptNote','Contact')
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE MI
SET MI.MenuItemCode = 'SpotReport'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'KeyEvent'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Atmospheric', @NewMenuItemText='Atmospherics'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SpotReport', @NewMenuItemText='Spot Reports', @NewMenuItemLink='/spotreport/list', @Icon='fa fa-fw fa-bolt'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPData', @NewMenuItemText='Rapid Assessments'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPDelivery', @ParentMenuItemCode='RAPData', @AfterMenuItemCode='StakeholderGroupSurveyAdd', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @ParentMenuItemCode='', @AfterMenuItemCode='RAPData', @Icon='fa fa-fw fa-calendar'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Document', @NewMenuItemText='Document Library', @AfterMenuItemCode='WeeklyReport', @Icon='fa fa-fw fa-folder-open'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Procurement', @AfterMenuItemCode='Training'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PurchaseOrderAdd', @ParentMenuItemCode='Procurement', @NewMenuItemText='Purchase Orders', @NewMenuItemLink='/purchaseorder/list', @AfterMenuItemCode='PurchaseOrderList'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Beneficiaries', @NewMenuItemText='Beneficiaries', @ParentMenuItemCode='', @AfterMenuItemCode='Procurement', @Icon='fa fa-fw fa-users'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ContactList', @NewMenuItemText='Contacts', @ParentMenuItemCode='Beneficiaries', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SubContractorList', @NewMenuItemText='Sub Contractors', @ParentMenuItemCode='Beneficiaries', @AfterMenuItemCode='ContactList', @NewMenuItemLink='/subcontractor/list', @PermissionableLineage='SubContractor.List'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNote', @NewMenuItemText='Activity Management', @AfterMenuItemCode='Beneficiaries', @IsActive=0
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LogicalFramework', @AfterMenuItemCode='ConceptNote', @NewMenuItemText='Monitoring & Evaluation', @Icon='fa fa-fw fa-heartbeat', @IsActive=0
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveAdd', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Objectives', @NewMenuItemLink='/objective/addupdate/id/0'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorAdd', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Indicators', @NewMenuItemLink='/indicator/addupdate/id/0', @AfterMenuItemCode='ObjectiveAdd'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorTypeList', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Indicator Types', @NewMenuItemLink='/indicatortype/list', @AfterMenuItemCode='IndicatorAdd'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='MilestoneAdd', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Milestones', @NewMenuItemLink='/milestone/addupdate/id/0', @AfterMenuItemCode='IndicatorTypeList'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RequestForInformation', @AfterMenuItemCode='LogicalFramework', @NewMenuItemText='Requests For Information', @NewMenuItemLink='/requestforinformation/list', @Icon='fa fa-fw fa-question-circle', @PermissionableLineage='RequestForInformation.List'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Admin', @AfterMenuItemCode='RequestForInformation'
GO
EXEC utility.MenuItemAddUpdate @DeleteMenuItemCode='ActivityManagement'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentInventoryList', @NewMenuItemLink='/equipmentinventory/list'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @NewMenuItemText='Create Weekly Report'
GO
--End table dbo.MenuItem

--Begin table dropdown.DocumentType
INSERT INTO dropdown.DocumentType
	(DocumentTypeCode, DocumentTypeName)
SELECT
	RFIRT.RequestForInformationResultTypeCode,
	RFIRT.RequestForInformationResultTypeName
FROM dropdown.RequestForInformationResultType RFIRT
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dropdown.DocumentType DT
	WHERE DT.DocumentTypeCode = RFIRT.RequestForInformationResultTypeCode
	)
	AND RFIRT.RequestForInformationResultTypeID > 0
GO

DECLARE @tTable TABLE (DisplayOrder INT NOT NULL PRIMARY KEY IDENTITY(1,1), DocumentTypeCode VARCHAR(50))

INSERT INTO @tTable
	(DocumentTypeCode)
SELECT
	DT.DocumentTypeCode
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeID > 0
ORDER BY DocumentTypeName

UPDATE DT
SET DT.DisplayOrder = T.DisplayOrder
FROM dropdown.DocumentType DT
	JOIN @tTable T ON T.DocumentTypeCode = DT.DocumentTypeCode
GO
--End table dropdown.DocumentType

--Begin table permissionable.DisplayGroup
TRUNCATE TABLE permissionable.DisplayGroup
GO

INSERT INTO permissionable.DisplayGroup
	(DisplayGroupCode,DisplayGroupName,DisplayOrder)
VALUES
	('CommunityProvince', 'Communities & Provinces', 1),
	('RAPData', 'RAP Data', 2),
	('RAPDelivery', 'RAP Delivery', 3),
	('Training', 'Training', 4),
	('Procurement', 'Procurement', 5),
	('ConceptNote', 'Concept Note', 6),
	('ObservationReport', 'Observations & Reports', 7),
	('Administrative', 'Administrative', 8)
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

INSERT INTO permissionable.Permissionable
	(PermissionableCode, PermissionableName, DisplayOrder)
VALUES
	('ActivityManagement','Activity Management', 0),
	('Atmospheric','General Atmospherics', 0),
	('Class','Class Schedules', 2),
	('Community','Communities', 0),
	('CommunityMemberSurvey','Community Member Survey', 2),
	('ConceptNote','Concept Notes', 0),
	('Contact','Contacts', 0),
	('Course','Course Catalog', 1),
	('DailyReport','Daily Reports', 0),
	('Document','Documents', 0),
	('EquipmentCatalog','Equipment Catalog', 0),
	('EquipmentInventory','Equipment Inventory', 0),
	('FocusGroupSurvey','Focus Group Questionnaire', 3),
	('Indicator','Indicators', 0),
	('IndicatorType','Indicator Types', 0),
	('KeyEvent','Key Events', 0),
	('KeyInformantSurvey','Key Informant Interview', 4),
	('LicenseEquipmentCatalog','Licensed Equipment', 0),
	('License','Licenses', 0),
	('Milestone','Milestones', 0),
	('Objective','Objectives', 0),
	('Person','Users', 0),
	('Province','Provinces', 0),
	('PurchaseOrder','Purchase Orders', 0),
	('RAPData','View RAP Data', 1),
	('RapidPerceptionSurvey','Rapid Perception Survey', 5),
	('RequestForInformation', 'Requests For Information', 0),
	('SpotReport','Spot Reports', 0),
	('StakeholderGroupSurvey','Stakeholder Group Questionnaire', 6),
	('StationCommanderSurvey','FSP Station Commander Survey', 7),
	('SubContractor','Sub Contractors', 0),
	('Team','Teams', 0),
	('WeeklyReport','Weekly Report', 0)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	1
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN 
	(
	'Atmospheric',
	'Class',
	'Community',
	'CommunityMemberSurvey',
	'ConceptNote',
	'Contact',
	'Course',
	'DailyReport',
	'Document',
	'EquipmentCatalog',
	'EquipmentInventory',
	'FocusGroupSurvey',
	'Indicator',
	'IndicatorType',
	'KeyEvent',
	'KeyInformantSurvey',
	'LicenseEquipmentCatalog',
	'License',
	'Milestone',
	'Objective',
	'Person',
	'Province',
	'PurchaseOrder',
	'RequestForInformation',
	'SpotReport',
	'StakeholderGroupSurvey',
	'StationCommanderSurvey',
	'SubContractor',
	'Team',
	'WeeklyReport'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'View',
	'View',
	2
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN 
	(
	'Atmospheric',
	'Class',
	'Community',
	'CommunityMemberSurvey',
	'ConceptNote',
	'Contact',
	'Course',
	'DailyReport',
	'Document',
	'EquipmentCatalog',
	'EquipmentInventory',
	'FocusGroupSurvey',
	'Indicator',
	'IndicatorType',
	'KeyEvent',
	'KeyInformantSurvey',
	'LicenseEquipmentCatalog',
	'License',
	'Milestone',
	'Objective',
	'Person',
	'Province',
	'PurchaseOrder',
	'RapidPerceptionSurvey',
	'RequestForInformation',
	'SpotReport',
	'StakeholderGroupSurvey',
	'StationCommanderSurvey',
	'SubContractor',
	'Team',
	'WeeklyReport'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'List',
	'List',
	3
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN 
	(
	'Atmospheric',
	'Class',
	'Community',
	'ConceptNote',
	'Contact',
	'Course',
	'DailyReport',
	'Document',
	'EquipmentCatalog',
	'EquipmentInventory',
	'IndicatorType',
	'KeyEvent',
	'LicenseEquipmentCatalog',
	'License',
	'Person',
	'Province',
	'PurchaseOrder',
	'RAPData',
	'RequestForInformation',
	'SpotReport',
	'SubContractor',
	'Team',
	'WeeklyReport'
	)
GO

UPDATE permissionable.Permissionable
SET DisplayOrder = DisplayOrder + 1
WHERE PermissionableLineage IN ('Person.View','Person.List')
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'Delete',
	'Delete',
	2
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN 
	(
	'Person'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'Approved',
	'Approved',
	1
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'SpotReport.View'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'InWork',
	'In Work',
	2
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'SpotReport.View'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'Comment',
	'Comment',
	1
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'SpotReport.View.Approved'
	)
GO

INSERT INTO permissionable.Permissionable
	(PermissionableCode, PermissionableName, IsGlobal)
VALUES 
	('DataExport','Data Export',1),
	('Login','Login',1),
	('Main','Main',1),
	('Remote','Remote',1)
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
TRUNCATE TABLE permissionable.DisplayGroupPermissionable
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityProvince'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Community','Province')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'RAPData'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('CommunityMemberSurvey','FocusGroupSurvey','KeyInformantSurvey','RAPData','RapidPerceptionSurvey','StakeholderGroupSurvey','StationCommanderSurvey')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'RAPDelivery'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('DailyReport','Team')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Training'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Class','Course')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Procurement'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('EquipmentCatalog','EquipmentInventory','LicenseEquipmentCatalog','License','PurchaseOrder')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'ConceptNote'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('ActivityManagement','ConceptNote','Indicator','IndicatorType','Milestone','Objective')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'ObservationReport'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Atmospheric','Document','KeyEvent','SpotReport','WeeklyReport')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Administrative'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Contact','Person','RequestForInformation','SubContractor')
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table dbo.MenuItem (Permissionables)
UPDATE MI
SET MI.PermissionableLineage = P.PermissionableLineage
FROM dbo.MenuItem MI
	JOIN permissionable.Permissionable P ON P.PermissionableLineage = REPLACE(REPLACE(STUFF(MI.MenuItemLink, 1, 1, ''), '/', '.'), '.id.0', '')
GO

UPDATE MI
SET MI.PermissionableLineage = 'EquipmentInventory.List'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'EquipmentInventoryList'
GO
--End table dbo.MenuItem

--Begin table workflow.Workflow
TRUNCATE TABLE workflow.Workflow
GO

INSERT INTO workflow.Workflow
	(EntityTypeCode,WorkflowStepCount)
VALUES
	('ConceptNote',9),
	('SpotReport',3),
	('WeeklyReport',3)
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
TRUNCATE TABLE workflow.WorkflowStep
GO

DECLARE @nWorkflowID INT

SELECT @nWorkflowID = W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'ConceptNote'

INSERT INTO workflow.WorkflowStep
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
VALUES
	(0, @nWorkflowID, 1, 'Concept Note Draft'),
	(0, @nWorkflowID, 2, 'Concept Note Edit'),
	(0, @nWorkflowID, 3, 'Concept Note Internal Approval'),
	(0, @nWorkflowID, 4, 'Activity Sheet Draft'),
	(0, @nWorkflowID, 5, 'Activity Sheet Edit'),
	(0, @nWorkflowID, 6, 'Activity Sheet Internal Approval'),
	(0, @nWorkflowID, 7, 'Activity Pending'),
	(0, @nWorkflowID, 8, 'Activity In Progress'),
	(0, @nWorkflowID, 9, 'Activity Completed')

INSERT INTO workflow.WorkflowStep
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
SELECT
	WS.WorkflowStepID,
	WS.WorkflowID,
	WS.WorkflowStepNumber,
	'M&E'
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = Ws.WorkflowID
		AND W.EntityTypeCode = 'ConceptNote'
		AND WS.WorkflowStepName IN ('Concept Note Edit', 'Activity Sheet Edit')

INSERT INTO workflow.WorkflowStep
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
SELECT
	WS.WorkflowStepID,
	WS.WorkflowID,
	WS.WorkflowStepNumber,
	'Procurement'
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = Ws.WorkflowID
		AND W.EntityTypeCode = 'ConceptNote'
		AND WS.WorkflowStepName IN ('Concept Note Edit', 'Activity Sheet Edit')

INSERT INTO workflow.WorkflowStep
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
SELECT
	WS.WorkflowStepID,
	WS.WorkflowID,
	WS.WorkflowStepNumber,
	'Finance'
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = Ws.WorkflowID
		AND W.EntityTypeCode = 'ConceptNote'
		AND WS.WorkflowStepName = 'Activity Sheet Edit'
GO

DECLARE @nWorkflowID INT

SELECT @nWorkflowID = W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'SpotReport'

INSERT INTO workflow.WorkflowStep
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
VALUES
	(0, @nWorkflowID, 1, 'Draft'),
	(0, @nWorkflowID, 2, 'Review'),
	(0, @nWorkflowID, 3, 'Approve')

SELECT @nWorkflowID = W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'WeeklyReport'

INSERT INTO workflow.WorkflowStep
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
VALUES
	(0, @nWorkflowID, 1, 'Draft'),
	(0, @nWorkflowID, 2, 'Review'),
	(0, @nWorkflowID, 3, 'Approve')
GO
--End table workflow.WorkflowStep

--Begin table workflow.WorkflowStepWorkflowAction
TRUNCATE TABLE workflow.WorkflowStepWorkflowAction
GO

--Add the 'Submit For Approval' action to all
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	W.WorkflowID,
	D.WorkflowStepNumber,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionName = 'Submit For Approval'),
	1
FROM workflow.Workflow W
	JOIN 
		(
		SELECT DISTINCT
			WS.WorkflowID,
			WS.WorkflowStepNumber
		FROM workflow.WorkflowStep WS
		WHERE WS.WorkflowStepNumber IN (1,2,4,5,7,8)
		) D ON D.WorkflowID = W.WorkflowID
GO		

--Add the 'Reject' action to all
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	W.WorkflowID,
	D.WorkflowStepNumber,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionName = 'Reject'),
	
	CASE
		WHEN D.WorkflowStepNumber = 2
		THEN 2
		WHEN D.WorkflowStepNumber = 3 AND (SELECT W.EntityTypeCode FROM workflow.Workflow W WHERE W.WorkflowID = D.WorkflowID) IN ('SpotReport','Weeklyreport')
		THEN 2
		ELSE 4
	END

FROM workflow.Workflow W
	JOIN 
		(
		SELECT DISTINCT
			WS.WorkflowID,
			WS.WorkflowStepNumber
		FROM workflow.WorkflowStep WS
		WHERE WS.WorkflowStepNumber IN (2,3,5,6,8,9)
		) D ON D.WorkflowID = W.WorkflowID
GO		

--Add the 'Approve' action to ConceptNote step 3
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	W.WorkflowID,
	3,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'IncrementWorkflowAndCreateActivitySheet'),
	1
FROM workflow.Workflow W
WHERE W.EntityTypeCode = 'ConceptNote'
GO		

--Add the 'Approve' action to SpotReport step 3
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	WS.WorkflowID,
	WS.WorkflowStepNumber,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'IncrementWorkflow' AND WA.WorkflowActionName = 'Approve'),
	1
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND WS.WorkflowStepNumber = 3
		AND W.EntityTypeCode = 'SpotReport'
GO		

--Add the 'Approve' action to WeeklyReport step 3
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	WS.WorkflowID,
	WS.WorkflowStepNumber,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'IncrementWorkflowAndUploadFile'),
	1
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND WS.WorkflowStepNumber = 3
		AND W.EntityTypeCode = 'WeeklyReport'
GO

--Add the 'Cancel' action to ConceptNote steps 3, 6 & 9
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	W.WorkflowID,
	D.WorkflowStepNumber,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'Cancel'),
	2
FROM workflow.Workflow W
	JOIN 
		(
		SELECT DISTINCT
			WS.WorkflowID,
			WS.WorkflowStepNumber
		FROM workflow.WorkflowStep WS
		WHERE WS.WorkflowStepNumber IN (3,6,9)
		) D ON D.WorkflowID = W.WorkflowID
	AND W.EntityTypeCode = 'ConceptNote'
GO		

--Add the 'Hold' action to ConceptNote steps 3, 6 & 9
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	W.WorkflowID,
	D.WorkflowStepNumber,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'Hold'),
	3
FROM workflow.Workflow W
	JOIN 
		(
		SELECT DISTINCT
			WS.WorkflowID,
			WS.WorkflowStepNumber
		FROM workflow.WorkflowStep WS
		WHERE WS.WorkflowStepNumber IN (3,6,9)
		) D ON D.WorkflowID = W.WorkflowID
	AND W.EntityTypeCode = 'ConceptNote'
GO		

--Add the 'Approve' action to ConceptNote step 6
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	W.WorkflowID,
	6,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'IncrementWorkflowAndCreateActivity'),
	1
FROM workflow.Workflow W
WHERE W.EntityTypeCode = 'ConceptNote'
GO		

--Add the 'Approve' action to ConceptNote step 9
INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	W.WorkflowID,
	9,
	(SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'IncrementWorkflow' AND WA.WorkflowActionName = 'Approve'),
	1
FROM workflow.Workflow W
WHERE W.EntityTypeCode = 'ConceptNote'
GO		
--End table workflow.WorkflowStepWorkflowAction

--Begin table permissionable.Permissionable (Workflow)
DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
	WHERE WS.ParentWorkflowStepID = 0
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = W.EntityTypeCode + '.AddUpdate'),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

DELETE FROM permissionable.Permissionable WHERE IsWorkflow = 1

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, CAST(REPLACE(INSERTED.PermissionableCode, 'WorkflowStepID', '') AS INT) INTO @tOutput
SELECT
	T.ParentPermissionableID,
	'WorkflowStepID' + CAST(T.WorkflowStepID AS VARCHAR(10)),
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY DisplayIndex) AS DisplayOrder,
	1
FROM @tTable T
WHERE ParentWorkflowStepID = 0
ORDER BY DisplayIndex

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	'WorkflowStepID' + CAST(T.WorkflowStepID AS VARCHAR(10)),
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY PermissionableName) AS DisplayOrder,
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
ORDER BY DisplayIndex
GO
--End table permissionable.Permissionable

--Begin table workflow.EntityWorkflowStep
TRUNCATE TABLE workflow.EntityWorkflowStep
GO

INSERT INTO workflow.EntityWorkflowStep
	(EntityID, WorkflowStepID)
SELECT
	SR.SpotReportID,
	WS.WorkflowStepID
FROM dbo.SpotReport SR, workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'SpotReport'
GO

INSERT INTO workflow.EntityWorkflowStep
	(EntityID, WorkflowStepID)
SELECT
	WR.WeeklyReportID,
	WS.WorkflowStepID
FROM weeklyreport.WeeklyReport WR, workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'WeeklyReport'
GO
--End table workflow.EntityWorkflowStep

--Begin table permissionable.PersonPermissionable
TRUNCATE TABLE permissionable.PersonPermissionable
GO

INSERT INTO permissionable.PersonPermissionable 
	(PersonID, PermissionableLineage) 
VALUES 
	(4,'ActivityManagement'),
	(4,'Atmospheric'),
	(4,'Atmospheric.AddUpdate'),
	(4,'Atmospheric.List'),
	(4,'Atmospheric.View'),
	(4,'Class'),
	(4,'Class.AddUpdate'),
	(4,'Class.List'),
	(4,'Class.View'),
	(4,'Community'),
	(4,'Community.AddUpdate'),
	(4,'Community.List'),
	(4,'Community.View'),
	(4,'CommunityMemberSurvey'),
	(4,'CommunityMemberSurvey.AddUpdate'),
	(4,'CommunityMemberSurvey.View'),
	(4,'ConceptNote'),
	(4,'ConceptNote.AddUpdate'),
	(4,'ConceptNote.List'),
	(4,'ConceptNote.View'),
	(4,'Contact'),
	(4,'Contact.AddUpdate'),
	(4,'Contact.List'),
	(4,'Contact.View'),
	(4,'Course'),
	(4,'Course.AddUpdate'),
	(4,'Course.List'),
	(4,'Course.View'),
	(4,'DailyReport'),
	(4,'DailyReport.AddUpdate'),
	(4,'DailyReport.List'),
	(4,'DailyReport.View'),
	(4,'Document'),
	(4,'Document.AddUpdate'),
	(4,'Document.List'),
	(4,'Document.View'),
	(4,'EquipmentCatalog'),
	(4,'EquipmentCatalog.AddUpdate'),
	(4,'EquipmentCatalog.List'),
	(4,'EquipmentCatalog.View'),
	(4,'EquipmentInventory'),
	(4,'EquipmentInventory.AddUpdate'),
	(4,'EquipmentInventory.List'),
	(4,'EquipmentInventory.View'),
	(4,'FocusGroupSurvey'),
	(4,'FocusGroupSurvey.AddUpdate'),
	(4,'FocusGroupSurvey.View'),
	(4,'Indicator'),
	(4,'Indicator.AddUpdate'),
	(4,'Indicator.View'),
	(4,'IndicatorType'),
	(4,'IndicatorType.AddUpdate'),
	(4,'IndicatorType.List'),
	(4,'IndicatorType.View'),
	(4,'KeyEvent'),
	(4,'KeyEvent.AddUpdate'),
	(4,'KeyEvent.List'),
	(4,'KeyEvent.View'),
	(4,'KeyInformantSurvey'),
	(4,'KeyInformantSurvey.AddUpdate'),
	(4,'KeyInformantSurvey.View'),
	(4,'License'),
	(4,'License.AddUpdate'),
	(4,'License.List'),
	(4,'License.View'),
	(4,'LicenseEquipmentCatalog'),
	(4,'LicenseEquipmentCatalog.AddUpdate'),
	(4,'LicenseEquipmentCatalog.List'),
	(4,'LicenseEquipmentCatalog.View'),
	(4,'Milestone'),
	(4,'Milestone.AddUpdate'),
	(4,'Milestone.View'),
	(4,'Objective'),
	(4,'Objective.AddUpdate'),
	(4,'Objective.View'),
	(4,'Person'),
	(4,'Person.AddUpdate'),
	(4,'Person.List'),
	(4,'Person.View'),
	(4,'Province'),
	(4,'Province.AddUpdate'),
	(4,'Province.List'),
	(4,'Province.View'),
	(4,'RAPData'),
	(4,'RAPData.List'),
	(4,'RapidPerceptionSurvey'),
	(4,'RapidPerceptionSurvey.View'),
	(4,'SpotReport'),
	(4,'SpotReport.AddUpdate'),
	(4,'SpotReport.List'),
	(4,'SpotReport.View'),
	(4,'StakeholderGroupSurvey'),
	(4,'StakeholderGroupSurvey.AddUpdate'),
	(4,'StakeholderGroupSurvey.View'),
	(4,'StationCommanderSurvey'),
	(4,'StationCommanderSurvey.AddUpdate'),
	(4,'StationCommanderSurvey.View'),
	(4,'Team'),
	(4,'Team.AddUpdate'),
	(4,'Team.List'),
	(4,'Team.View'),
	(4,'WeeklyReport'),
	(4,'WeeklyReport.AddUpdate'),
	(4,'WeeklyReport.List'),
	(4,'WeeklyReport.View'),
	(5,'Atmospheric'),
	(5,'Atmospheric.List'),
	(5,'Atmospheric.View'),
	(5,'Community'),
	(5,'Community.List'),
	(5,'Community.View'),
	(5,'KeyEvent'),
	(5,'KeyEvent.List'),
	(5,'KeyEvent.View'),
	(5,'Province'),
	(5,'Province.List'),
	(5,'Province.View'),
	(5,'SpotReport'),
	(5,'SpotReport.List'),
	(5,'SpotReport.View'),
	(5,'WeeklyReport'),
	(5,'WeeklyReport.List'),
	(5,'WeeklyReport.View'),
	(7,'ActivityManagement'),
	(7,'Atmospheric'),
	(7,'Atmospheric.AddUpdate'),
	(7,'Atmospheric.List'),
	(7,'Atmospheric.View'),
	(7,'Class'),
	(7,'Class.AddUpdate'),
	(7,'Class.List'),
	(7,'Class.View'),
	(7,'Community'),
	(7,'Community.AddUpdate'),
	(7,'Community.List'),
	(7,'Community.View'),
	(7,'CommunityMemberSurvey'),
	(7,'CommunityMemberSurvey.AddUpdate'),
	(7,'CommunityMemberSurvey.View'),
	(7,'ConceptNote'),
	(7,'ConceptNote.AddUpdate'),
	(7,'ConceptNote.List'),
	(7,'ConceptNote.View'),
	(7,'Contact'),
	(7,'Contact.AddUpdate'),
	(7,'Contact.List'),
	(7,'Contact.View'),
	(7,'Course'),
	(7,'Course.AddUpdate'),
	(7,'Course.List'),
	(7,'Course.View'),

	(7,'DailyReport'),
	(7,'DailyReport.AddUpdate'),
	(7,'DailyReport.List'),
	(7,'DailyReport.View'),
	(7,'Document'),
	(7,'Document.AddUpdate'),
	(7,'Document.List'),
	(7,'Document.View'),
	(7,'EquipmentCatalog'),
	(7,'EquipmentCatalog.AddUpdate'),
	(7,'EquipmentCatalog.List'),
	(7,'EquipmentCatalog.View'),
	(7,'EquipmentInventory'),
	(7,'EquipmentInventory.AddUpdate'),
	(7,'EquipmentInventory.List'),
	(7,'EquipmentInventory.View'),
	(7,'FocusGroupSurvey'),
	(7,'FocusGroupSurvey.AddUpdate'),
	(7,'FocusGroupSurvey.View'),
	(7,'Indicator'),
	(7,'Indicator.AddUpdate'),
	(7,'Indicator.View'),
	(7,'IndicatorType'),
	(7,'IndicatorType.AddUpdate'),
	(7,'IndicatorType.List'),
	(7,'IndicatorType.View'),
	(7,'KeyEvent'),
	(7,'KeyEvent.AddUpdate'),
	(7,'KeyEvent.List'),
	(7,'KeyEvent.View'),
	(7,'KeyInformantSurvey'),
	(7,'KeyInformantSurvey.AddUpdate'),
	(7,'KeyInformantSurvey.View'),
	(7,'License'),
	(7,'License.AddUpdate'),
	(7,'License.List'),
	(7,'License.View'),
	(7,'LicenseEquipmentCatalog'),
	(7,'LicenseEquipmentCatalog.AddUpdate'),
	(7,'LicenseEquipmentCatalog.List'),
	(7,'LicenseEquipmentCatalog.View'),
	(7,'Milestone'),
	(7,'Milestone.AddUpdate'),
	(7,'Milestone.View'),
	(7,'Objective'),
	(7,'Objective.AddUpdate'),
	(7,'Objective.View'),
	(7,'Person'),
	(7,'Person.AddUpdate'),
	(7,'Person.List'),
	(7,'Person.View'),
	(7,'Province'),
	(7,'Province.AddUpdate'),
	(7,'Province.List'),
	(7,'Province.View'),
	(7,'RAPData'),
	(7,'RAPData.List'),
	(7,'RapidPerceptionSurvey'),
	(7,'RapidPerceptionSurvey.View'),
	(7,'SpotReport'),
	(7,'SpotReport.AddUpdate'),
	(7,'SpotReport.List'),
	(7,'SpotReport.View'),
	(7,'StakeholderGroupSurvey'),
	(7,'StakeholderGroupSurvey.AddUpdate'),
	(7,'StakeholderGroupSurvey.View'),
	(7,'StationCommanderSurvey'),
	(7,'StationCommanderSurvey.AddUpdate'),
	(7,'StationCommanderSurvey.View'),
	(7,'Team'),
	(7,'Team.AddUpdate'),
	(7,'Team.List'),
	(7,'Team.View'),
	(7,'WeeklyReport'),
	(7,'WeeklyReport.AddUpdate'),
	(7,'WeeklyReport.List'),
	(7,'WeeklyReport.View'),
	(8,'CommunityMemberSurvey.View'),
	(8,'FocusGroupSurvey.View'),
	(8,'KeyInformantSurvey.View'),
	(8,'RapidPerceptionSurvey.View'),
	(8,'StakeholderGroupSurvey.View'),
	(8,'StationCommanderSurvey.View'),
	(9,'ActivityManagement'),
	(9,'Atmospheric'),
	(9,'Atmospheric.AddUpdate'),
	(9,'Atmospheric.List'),
	(9,'Atmospheric.View'),
	(9,'Class'),
	(9,'Class.AddUpdate'),
	(9,'Class.List'),
	(9,'Class.View'),
	(9,'Community'),
	(9,'Community.AddUpdate'),
	(9,'Community.List'),
	(9,'Community.View'),
	(9,'CommunityMemberSurvey'),
	(9,'CommunityMemberSurvey.AddUpdate'),
	(9,'CommunityMemberSurvey.View'),
	(9,'ConceptNote'),
	(9,'ConceptNote.AddUpdate'),
	(9,'ConceptNote.List'),
	(9,'ConceptNote.View'),
	(9,'Contact'),
	(9,'Contact.AddUpdate'),
	(9,'Contact.List'),
	(9,'Contact.View'),
	(9,'Course'),
	(9,'Course.AddUpdate'),
	(9,'Course.List'),
	(9,'Course.View'),
	(9,'DailyReport'),
	(9,'DailyReport.AddUpdate'),
	(9,'DailyReport.List'),
	(9,'DailyReport.View'),
	(9,'Document'),
	(9,'Document.AddUpdate'),
	(9,'Document.List'),
	(9,'Document.View'),
	(9,'EquipmentCatalog'),
	(9,'EquipmentCatalog.AddUpdate'),
	(9,'EquipmentCatalog.List'),
	(9,'EquipmentCatalog.View'),
	(9,'EquipmentInventory'),
	(9,'EquipmentInventory.AddUpdate'),
	(9,'EquipmentInventory.List'),
	(9,'EquipmentInventory.View'),
	(9,'FocusGroupSurvey'),
	(9,'FocusGroupSurvey.AddUpdate'),
	(9,'FocusGroupSurvey.View'),
	(9,'Indicator'),
	(9,'Indicator.AddUpdate'),
	(9,'Indicator.View'),
	(9,'IndicatorType'),
	(9,'IndicatorType.AddUpdate'),
	(9,'IndicatorType.List'),
	(9,'IndicatorType.View'),
	(9,'KeyEvent'),
	(9,'KeyEvent.AddUpdate'),
	(9,'KeyEvent.List'),
	(9,'KeyEvent.View'),
	(9,'KeyInformantSurvey'),
	(9,'KeyInformantSurvey.AddUpdate'),
	(9,'KeyInformantSurvey.View'),
	(9,'License'),
	(9,'License.AddUpdate'),
	(9,'License.List'),
	(9,'License.View'),
	(9,'LicenseEquipmentCatalog'),
	(9,'LicenseEquipmentCatalog.AddUpdate'),
	(9,'LicenseEquipmentCatalog.List'),
	(9,'LicenseEquipmentCatalog.View'),
	(9,'Milestone'),
	(9,'Milestone.AddUpdate'),
	(9,'Milestone.View'),
	(9,'Objective'),
	(9,'Objective.AddUpdate'),
	(9,'Objective.View'),
	(9,'Person'),
	(9,'Person.AddUpdate'),
	(9,'Person.List'),
	(9,'Person.View'),
	(9,'Province'),
	(9,'Province.AddUpdate'),
	(9,'Province.List'),
	(9,'Province.View'),
	(9,'RAPData'),
	(9,'RAPData.List'),
	(9,'RapidPerceptionSurvey'),
	(9,'RapidPerceptionSurvey.View'),
	(9,'SpotReport'),
	(9,'SpotReport.AddUpdate'),
	(9,'SpotReport.List'),
	(9,'SpotReport.View'),
	(9,'StakeholderGroupSurvey'),
	(9,'StakeholderGroupSurvey.AddUpdate'),
	(9,'StakeholderGroupSurvey.View'),
	(9,'StationCommanderSurvey'),
	(9,'StationCommanderSurvey.AddUpdate'),
	(9,'StationCommanderSurvey.View'),
	(9,'Team'),
	(9,'Team.AddUpdate'),
	(9,'Team.List'),
	(9,'Team.View'),
	(9,'WeeklyReport'),
	(9,'WeeklyReport.AddUpdate'),
	(9,'WeeklyReport.List'),
	(9,'WeeklyReport.View'),
	(11,'CommunityMemberSurvey'),
	(11,'CommunityMemberSurvey.AddUpdate'),
	(11,'CommunityMemberSurvey.View'),
	(11,'FocusGroupSurvey'),
	(11,'FocusGroupSurvey.AddUpdate'),
	(11,'FocusGroupSurvey.View'),
	(11,'KeyInformantSurvey'),
	(11,'KeyInformantSurvey.AddUpdate'),
	(11,'KeyInformantSurvey.View'),
	(11,'RAPData'),
	(11,'RAPData.List'),
	(11,'RapidPerceptionSurvey'),
	(11,'RapidPerceptionSurvey.View'),
	(11,'StakeholderGroupSurvey'),
	(11,'StakeholderGroupSurvey.AddUpdate'),
	(11,'StakeholderGroupSurvey.View'),
	(11,'StationCommanderSurvey'),
	(11,'StationCommanderSurvey.AddUpdate'),
	(11,'StationCommanderSurvey.View'),
	(12,'CommunityMemberSurvey'),
	(12,'CommunityMemberSurvey.View'),
	(12,'FocusGroupSurvey'),
	(12,'FocusGroupSurvey.View'),
	(12,'KeyInformantSurvey'),
	(12,'KeyInformantSurvey.View'),
	(12,'RAPData'),
	(12,'RapidPerceptionSurvey'),
	(12,'RapidPerceptionSurvey.View'),
	(12,'StakeholderGroupSurvey'),
	(12,'StakeholderGroupSurvey.View'),
	(12,'StationCommanderSurvey'),
	(12,'StationCommanderSurvey.View'),
	(13,'CommunityMemberSurvey'),
	(13,'CommunityMemberSurvey.AddUpdate'),
	(13,'CommunityMemberSurvey.View'),
	(13,'FocusGroupSurvey'),
	(13,'FocusGroupSurvey.AddUpdate'),
	(13,'FocusGroupSurvey.View'),
	(13,'KeyInformantSurvey'),
	(13,'KeyInformantSurvey.AddUpdate'),
	(13,'KeyInformantSurvey.View'),
	(13,'RapidPerceptionSurvey'),
	(13,'RapidPerceptionSurvey.View'),
	(13,'StakeholderGroupSurvey'),
	(13,'StakeholderGroupSurvey.AddUpdate'),
	(13,'StakeholderGroupSurvey.View'),
	(13,'StationCommanderSurvey'),
	(13,'StationCommanderSurvey.AddUpdate'),
	(13,'StationCommanderSurvey.View'),
	(14,'CommunityMemberSurvey'),
	(14,'CommunityMemberSurvey.AddUpdate'),
	(14,'CommunityMemberSurvey.View'),
	(14,'FocusGroupSurvey'),
	(14,'FocusGroupSurvey.AddUpdate'),
	(14,'FocusGroupSurvey.View'),
	(14,'KeyInformantSurvey'),
	(14,'KeyInformantSurvey.AddUpdate'),
	(14,'KeyInformantSurvey.View'),
	(14,'RAPData'),
	(14,'RAPData.List'),
	(14,'RapidPerceptionSurvey'),
	(14,'RapidPerceptionSurvey.View'),
	(14,'StakeholderGroupSurvey'),
	(14,'StakeholderGroupSurvey.AddUpdate'),
	(14,'StakeholderGroupSurvey.View'),
	(14,'StationCommanderSurvey'),
	(14,'StationCommanderSurvey.AddUpdate'),
	(14,'StationCommanderSurvey.View'),
	(16,'Atmospheric'),
	(16,'Atmospheric.List'),
	(16,'Atmospheric.View'),
	(16,'CommunityMemberSurvey.View'),
	(16,'Document'),
	(16,'Document.AddUpdate'),
	(16,'Document.List'),
	(16,'Document.View'),
	(16,'FocusGroupSurvey.View'),
	(16,'KeyEvent'),
	(16,'KeyEvent.List'),
	(16,'KeyEvent.View'),
	(16,'KeyInformantSurvey.View'),
	(16,'RapidPerceptionSurvey.View'),
	(16,'SpotReport'),
	(16,'SpotReport.List'),
	(16,'SpotReport.View'),
	(16,'StakeholderGroupSurvey.View'),
	(16,'StationCommanderSurvey.View'),
	(16,'WeeklyReport'),
	(16,'WeeklyReport.List'),
	(16,'WeeklyReport.View'),
	(17,'ActivityManagement'),
	(17,'Atmospheric'),
	(17,'Class'),
	(17,'Community'),
	(17,'CommunityMemberSurvey'),
	(17,'ConceptNote'),
	(17,'Contact'),
	(17,'Course'),
	(17,'DailyReport'),
	(17,'Document'),
	(17,'EquipmentCatalog'),
	(17,'EquipmentInventory'),
	(17,'FocusGroupSurvey'),
	(17,'Indicator'),
	(17,'IndicatorType'),
	(17,'KeyEvent'),
	(17,'KeyInformantSurvey'),
	(17,'LicenseEquipmentCatalog'),
	(17,'License'),
	(17,'Milestone'),
	(17,'Objective'),
	(17,'Person'),
	(17,'Province'),
	(17,'RAPData'),
	(17,'RapidPerceptionSurvey'),
	(17,'SpotReport'),
	(17,'StakeholderGroupSurvey'),
	(17,'StationCommanderSurvey'),
	(17,'Team'),
	(17,'WeeklyReport'),
	(17,'Atmospheric.AddUpdate'),
	(17,'Class.AddUpdate'),
	(17,'Community.AddUpdate'),
	(17,'CommunityMemberSurvey.AddUpdate'),
	(17,'ConceptNote.AddUpdate'),
	(17,'Contact.AddUpdate'),
	(17,'Course.AddUpdate'),
	(17,'DailyReport.AddUpdate'),
	(17,'Document.AddUpdate'),
	(17,'EquipmentCatalog.AddUpdate'),
	(17,'EquipmentInventory.AddUpdate'),
	(17,'FocusGroupSurvey.AddUpdate'),
	(17,'Indicator.AddUpdate'),
	(17,'IndicatorType.AddUpdate'),
	(17,'KeyEvent.AddUpdate'),
	(17,'KeyInformantSurvey.AddUpdate'),
	(17,'LicenseEquipmentCatalog.AddUpdate'),
	(17,'License.AddUpdate'),
	(17,'Milestone.AddUpdate'),
	(17,'Objective.AddUpdate'),
	(17,'Person.AddUpdate'),
	(17,'Province.AddUpdate'),
	(17,'SpotReport.AddUpdate'),
	(17,'StakeholderGroupSurvey.AddUpdate'),
	(17,'StationCommanderSurvey.AddUpdate'),
	(17,'Team.AddUpdate'),
	(17,'WeeklyReport.AddUpdate'),
	(17,'Atmospheric.View'),
	(17,'Class.View'),
	(17,'Community.View'),
	(17,'CommunityMemberSurvey.View'),
	(17,'ConceptNote.View'),
	(17,'Contact.View'),
	(17,'Course.View'),
	(17,'DailyReport.View'),
	(17,'Document.View'),
	(17,'EquipmentCatalog.View'),
	(17,'EquipmentInventory.View'),
	(17,'FocusGroupSurvey.View'),
	(17,'Indicator.View'),
	(17,'IndicatorType.View'),
	(17,'KeyEvent.View'),
	(17,'KeyInformantSurvey.View'),
	(17,'LicenseEquipmentCatalog.View'),
	(17,'License.View'),
	(17,'Milestone.View'),
	(17,'Objective.View'),
	(17,'Person.View'),
	(17,'Province.View'),
	(17,'RapidPerceptionSurvey.View'),
	(17,'SpotReport.View'),
	(17,'StakeholderGroupSurvey.View'),
	(17,'StationCommanderSurvey.View'),
	(17,'Team.View'),
	(17,'WeeklyReport.View'),
	(17,'Atmospheric.List'),
	(17,'Class.List'),
	(17,'Community.List'),
	(17,'ConceptNote.List'),
	(17,'Contact.List'),
	(17,'Course.List'),
	(17,'DailyReport.List'),
	(17,'Document.List'),
	(17,'EquipmentCatalog.List'),
	(17,'EquipmentInventory.List'),
	(17,'IndicatorType.List'),
	(17,'KeyEvent.List'),
	(17,'LicenseEquipmentCatalog.List'),
	(17,'License.List'),
	(17,'Person.List'),
	(17,'Province.List'),
	(17,'RAPData.List'),
	(17,'SpotReport.List'),
	(17,'Team.List'),
	(17,'WeeklyReport.List'),
	(17,'Person.Delete'),
	(17,'DataExport'),
	(17,'Login'),
	(17,'Main'),
	(17,'Remote'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID1'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID2'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID3'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID4'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID5'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID6'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID7'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID8'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID9'),
	(17,'SpotReport.AddUpdate.WorkflowStepID15'),
	(17,'SpotReport.AddUpdate.WorkflowStepID16'),
	(17,'SpotReport.AddUpdate.WorkflowStepID17'),
	(17,'WeeklyReport.AddUpdate.WorkflowStepID18'),
	(17,'WeeklyReport.AddUpdate.WorkflowStepID19'),
	(17,'WeeklyReport.AddUpdate.WorkflowStepID20'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID2.WorkflowStepID10'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID2.WorkflowStepID12'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID5.WorkflowStepID14'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID5.WorkflowStepID11'),
	(17,'ConceptNote.AddUpdate.WorkflowStepID5.WorkflowStepID13'),
	(19,'CommunityMemberSurvey'),
	(19,'CommunityMemberSurvey.AddUpdate'),
	(19,'CommunityMemberSurvey.View'),
	(19,'FocusGroupSurvey'),
	(19,'FocusGroupSurvey.AddUpdate'),
	(19,'FocusGroupSurvey.View'),
	(19,'KeyInformantSurvey'),
	(19,'KeyInformantSurvey.View'),
	(19,'RAPData'),
	(19,'RAPData.List'),
	(19,'RapidPerceptionSurvey'),
	(19,'StakeholderGroupSurvey'),
	(19,'StakeholderGroupSurvey.View'),
	(19,'StationCommanderSurvey'),
	(19,'StationCommanderSurvey.View'),
	(21,'CommunityMemberSurvey.View'),
	(21,'FocusGroupSurvey.View'),
	(21,'KeyInformantSurvey'),
	(21,'KeyInformantSurvey.View'),
	(21,'RapidPerceptionSurvey.View'),
	(21,'StakeholderGroupSurvey'),
	(21,'StakeholderGroupSurvey.View'),
	(21,'StationCommanderSurvey.View'),
	(22,'CommunityMemberSurvey'),
	(22,'CommunityMemberSurvey.AddUpdate'),
	(22,'CommunityMemberSurvey.View'),
	(22,'FocusGroupSurvey'),
	(22,'FocusGroupSurvey.AddUpdate'),
	(22,'FocusGroupSurvey.View'),
	(22,'KeyInformantSurvey'),
	(22,'KeyInformantSurvey.AddUpdate'),
	(22,'KeyInformantSurvey.View'),
	(22,'RapidPerceptionSurvey'),
	(22,'RapidPerceptionSurvey.View'),
	(22,'StakeholderGroupSurvey'),
	(22,'StakeholderGroupSurvey.AddUpdate'),
	(22,'StakeholderGroupSurvey.View'),
	(22,'StationCommanderSurvey'),
	(22,'StationCommanderSurvey.AddUpdate'),
	(22,'StationCommanderSurvey.View'),
	(23,'Atmospheric'),
	(23,'Atmospheric.List'),
	(23,'Atmospheric.View'),
	(23,'Community'),
	(23,'Community.List'),
	(23,'Community.View'),
	(23,'CommunityMemberSurvey.View'),
	(23,'Document'),
	(23,'Document.AddUpdate'),
	(23,'Document.List'),
	(23,'Document.View'),
	(23,'FocusGroupSurvey.View'),
	(23,'KeyEvent'),
	(23,'KeyEvent.List'),
	(23,'KeyEvent.View'),
	(23,'KeyInformantSurvey.View'),
	(23,'Province'),
	(23,'Province.List'),
	(23,'Province.View'),
	(23,'RapidPerceptionSurvey.View'),
	(23,'SpotReport'),
	(23,'SpotReport.List'),
	(23,'SpotReport.View'),
	(23,'StakeholderGroupSurvey.View'),
	(23,'StationCommanderSurvey.View'),
	(23,'WeeklyReport'),
	(23,'WeeklyReport.List'),
	(23,'WeeklyReport.View'),
	(24,'CommunityMemberSurvey'),
	(24,'CommunityMemberSurvey.AddUpdate'),
	(24,'CommunityMemberSurvey.View'),
	(24,'FocusGroupSurvey'),
	(24,'FocusGroupSurvey.AddUpdate'),
	(24,'FocusGroupSurvey.View'),
	(24,'KeyInformantSurvey'),
	(24,'KeyInformantSurvey.AddUpdate'),
	(24,'KeyInformantSurvey.View'),
	(24,'RapidPerceptionSurvey'),
	(24,'RapidPerceptionSurvey.View'),
	(24,'StakeholderGroupSurvey'),
	(24,'StakeholderGroupSurvey.AddUpdate'),
	(24,'StakeholderGroupSurvey.View'),
	(24,'StationCommanderSurvey'),
	(24,'StationCommanderSurvey.AddUpdate'),
	(24,'StationCommanderSurvey.View'),
	(26,'ActivityManagement'),
	(26,'Atmospheric'),
	(26,'Class'),
	(26,'Community'),
	(26,'CommunityMemberSurvey'),
	(26,'ConceptNote'),
	(26,'Contact'),
	(26,'Course'),
	(26,'DailyReport'),
	(26,'Document'),
	(26,'EquipmentCatalog'),
	(26,'EquipmentInventory'),
	(26,'FocusGroupSurvey'),
	(26,'Indicator'),
	(26,'IndicatorType'),
	(26,'KeyEvent'),
	(26,'KeyInformantSurvey'),
	(26,'LicenseEquipmentCatalog'),
	(26,'License'),
	(26,'Milestone'),
	(26,'Objective'),
	(26,'Person'),
	(26,'Province'),
	(26,'RAPData'),
	(26,'RapidPerceptionSurvey'),
	(26,'SpotReport'),
	(26,'StakeholderGroupSurvey'),
	(26,'StationCommanderSurvey'),
	(26,'Team'),
	(26,'WeeklyReport'),
	(26,'Atmospheric.AddUpdate'),
	(26,'Class.AddUpdate'),
	(26,'Community.AddUpdate'),
	(26,'CommunityMemberSurvey.AddUpdate'),
	(26,'ConceptNote.AddUpdate'),
	(26,'Contact.AddUpdate'),
	(26,'Course.AddUpdate'),
	(26,'DailyReport.AddUpdate'),
	(26,'Document.AddUpdate'),
	(26,'EquipmentCatalog.AddUpdate'),
	(26,'EquipmentInventory.AddUpdate'),
	(26,'FocusGroupSurvey.AddUpdate'),
	(26,'Indicator.AddUpdate'),
	(26,'IndicatorType.AddUpdate'),
	(26,'KeyEvent.AddUpdate'),
	(26,'KeyInformantSurvey.AddUpdate'),
	(26,'LicenseEquipmentCatalog.AddUpdate'),
	(26,'License.AddUpdate'),
	(26,'Milestone.AddUpdate'),
	(26,'Objective.AddUpdate'),
	(26,'Person.AddUpdate'),
	(26,'Province.AddUpdate'),
	(26,'SpotReport.AddUpdate'),
	(26,'StakeholderGroupSurvey.AddUpdate'),
	(26,'StationCommanderSurvey.AddUpdate'),
	(26,'Team.AddUpdate'),
	(26,'WeeklyReport.AddUpdate'),
	(26,'Atmospheric.View'),
	(26,'Class.View'),
	(26,'Community.View'),
	(26,'CommunityMemberSurvey.View'),
	(26,'ConceptNote.View'),
	(26,'Contact.View'),
	(26,'Course.View'),
	(26,'DailyReport.View'),
	(26,'Document.View'),
	(26,'EquipmentCatalog.View'),
	(26,'EquipmentInventory.View'),
	(26,'FocusGroupSurvey.View'),
	(26,'Indicator.View'),
	(26,'IndicatorType.View'),
	(26,'KeyEvent.View'),
	(26,'KeyInformantSurvey.View'),
	(26,'LicenseEquipmentCatalog.View'),
	(26,'License.View'),
	(26,'Milestone.View'),
	(26,'Objective.View'),
	(26,'Person.View'),
	(26,'Province.View'),
	(26,'RapidPerceptionSurvey.View'),
	(26,'SpotReport.View'),
	(26,'StakeholderGroupSurvey.View'),
	(26,'StationCommanderSurvey.View'),
	(26,'Team.View'),
	(26,'WeeklyReport.View'),
	(26,'Atmospheric.List'),
	(26,'Class.List'),
	(26,'Community.List'),
	(26,'ConceptNote.List'),
	(26,'Contact.List'),
	(26,'Course.List'),
	(26,'DailyReport.List'),
	(26,'Document.List'),
	(26,'EquipmentCatalog.List'),
	(26,'EquipmentInventory.List'),
	(26,'IndicatorType.List'),
	(26,'KeyEvent.List'),
	(26,'LicenseEquipmentCatalog.List'),
	(26,'License.List'),
	(26,'Person.List'),
	(26,'Province.List'),
	(26,'RAPData.List'),
	(26,'SpotReport.List'),
	(26,'Team.List'),
	(26,'WeeklyReport.List'),
	(26,'Person.Delete'),
	(26,'DataExport'),
	(26,'Login'),
	(26,'Main'),
	(26,'Remote'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID1'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID2'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID3'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID4'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID5'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID6'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID7'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID8'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID9'),
	(26,'SpotReport.AddUpdate.WorkflowStepID15'),
	(26,'SpotReport.AddUpdate.WorkflowStepID16'),
	(26,'SpotReport.AddUpdate.WorkflowStepID17'),
	(26,'WeeklyReport.AddUpdate.WorkflowStepID18'),
	(26,'WeeklyReport.AddUpdate.WorkflowStepID19'),
	(26,'WeeklyReport.AddUpdate.WorkflowStepID20'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID2.WorkflowStepID10'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID2.WorkflowStepID12'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID5.WorkflowStepID14'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID5.WorkflowStepID11'),
	(26,'ConceptNote.AddUpdate.WorkflowStepID5.WorkflowStepID13'),
	(27,'Community'),
	(27,'Community.AddUpdate'),
	(27,'Community.List'),
	(27,'Province'),
	(27,'Province.List'),
	(27,'Province.View'),
	(28,'Community'),
	(28,'Community.AddUpdate'),
	(28,'Community.List'),
	(28,'Province'),
	(28,'Province.List'),
	(28,'Province.View'),
	(29,'Community'),
	(29,'Community.AddUpdate'),
	(29,'Community.List'),
	(29,'Province'),
	(29,'Province.List'),
	(29,'Province.View'),
	(30,'Community'),
	(30,'Community.AddUpdate'),
	(30,'Community.List'),
	(30,'Province'),
	(30,'Province.List'),
	(30,'Province.View'),
	(31,'Community'),
	(31,'Community.AddUpdate'),
	(31,'Community.List'),
	(31,'Province'),
	(31,'Province.List'),
	(31,'Province.View'),
	(32,'Community'),
	(32,'Community.AddUpdate'),
	(32,'Community.List'),
	(32,'Province'),
	(32,'Province.List'),
	(32,'Province.View'),
	(33,'Community'),
	(33,'Community.AddUpdate'),
	(33,'Community.List'),
	(33,'Province'),
	(33,'Province.List'),
	(33,'Province.View'),
	(34,'Community'),
	(34,'Community.AddUpdate'),
	(34,'Community.List'),
	(34,'Province'),
	(34,'Province.List'),
	(34,'Province.View'),
	(35,'Community'),
	(35,'Community.AddUpdate'),
	(35,'Community.List'),
	(35,'Province'),
	(35,'Province.List'),
	(35,'Province.View'),
	(36,'Community'),
	(36,'Community.AddUpdate'),
	(36,'Community.List'),
	(36,'Province'),
	(36,'Province.List'),
	(36,'Province.View'),
	(38,'Atmospheric'),
	(38,'Atmospheric.List'),
	(38,'Atmospheric.View'),
	(38,'CommunityMemberSurvey.View'),
	(38,'Document'),
	(38,'Document.AddUpdate'),
	(38,'Document.List'),
	(38,'Document.View'),
	(38,'FocusGroupSurvey.View'),
	(38,'KeyEvent'),
	(38,'KeyEvent.List'),
	(38,'KeyEvent.View'),
	(38,'KeyInformantSurvey.View'),
	(38,'RapidPerceptionSurvey.View'),
	(38,'SpotReport'),
	(38,'SpotReport.List'),
	(38,'SpotReport.View'),
	(38,'StakeholderGroupSurvey.View'),
	(38,'StationCommanderSurvey.View'),
	(38,'WeeklyReport'),
	(38,'WeeklyReport.List'),
	(38,'WeeklyReport.View'),
	(39,'Atmospheric'),
	(39,'Atmospheric.List'),
	(39,'Atmospheric.View'),
	(39,'Community'),
	(39,'Community.List'),
	(39,'Community.View'),
	(39,'CommunityMemberSurvey.View'),
	(39,'Document'),
	(39,'Document.AddUpdate'),
	(39,'Document.List'),
	(39,'Document.View'),
	(39,'FocusGroupSurvey.View'),
	(39,'KeyEvent'),
	(39,'KeyEvent.List'),
	(39,'KeyEvent.View'),
	(39,'KeyInformantSurvey.View'),
	(39,'Province'),
	(39,'Province.List'),
	(39,'Province.View'),
	(39,'RapidPerceptionSurvey.View'),
	(39,'SpotReport'),
	(39,'SpotReport.List'),
	(39,'SpotReport.View'),
	(39,'StakeholderGroupSurvey.View'),
	(39,'StationCommanderSurvey.View'),
	(39,'WeeklyReport'),
	(39,'WeeklyReport.List'),
	(39,'WeeklyReport.View'),
	(40,'Atmospheric'),
	(40,'Atmospheric.List'),
	(40,'Atmospheric.View'),
	(40,'Community'),
	(40,'Community.List'),
	(40,'Community.View'),
	(40,'CommunityMemberSurvey.View'),
	(40,'Document'),
	(40,'Document.AddUpdate'),
	(40,'Document.List'),
	(40,'Document.View'),
	(40,'FocusGroupSurvey.View'),
	(40,'KeyEvent'),
	(40,'KeyEvent.List'),
	(40,'KeyEvent.View'),
	(40,'KeyInformantSurvey.View'),
	(40,'Province'),
	(40,'Province.List'),
	(40,'Province.View'),
	(40,'RapidPerceptionSurvey.View'),
	(40,'SpotReport'),
	(40,'SpotReport.List'),
	(40,'SpotReport.View'),
	(40,'StakeholderGroupSurvey.View'),
	(40,'StationCommanderSurvey.View'),
	(40,'WeeklyReport'),
	(40,'WeeklyReport.List'),
	(40,'WeeklyReport.View'),
	(42,'Community'),
	(42,'Community.AddUpdate'),
	(42,'Community.List'),
	(42,'Province'),
	(42,'Province.List'),
	(42,'Province.View'),
	(45,'Community'),
	(45,'Community.List'),
	(45,'Community.View'),
	(45,'Province'),
	(45,'Province.List'),
	(45,'Province.View'),
	(46,'Atmospheric'),
	(46,'Atmospheric.List'),
	(46,'Atmospheric.View'),
	(46,'Community'),
	(46,'Community.List'),
	(46,'Community.View'),
	(46,'KeyEvent'),
	(46,'KeyEvent.List'),
	(46,'KeyEvent.View'),
	(46,'Province'),
	(46,'Province.List'),
	(46,'Province.View'),
	(46,'SpotReport'),
	(46,'SpotReport.List'),
	(46,'SpotReport.View'),
	(46,'WeeklyReport'),
	(46,'WeeklyReport.List'),
	(46,'WeeklyReport.View'),
	(47,'Atmospheric'),
	(47,'Atmospheric.AddUpdate'),
	(47,'Atmospheric.List'),
	(47,'Atmospheric.View'),
	(47,'CommunityMemberSurvey.View'),
	(47,'Document'),
	(47,'Document.AddUpdate'),
	(47,'Document.List'),
	(47,'Document.View'),
	(47,'FocusGroupSurvey.View'),
	(47,'KeyEvent'),
	(47,'KeyEvent.AddUpdate'),
	(47,'KeyEvent.List'),
	(47,'KeyEvent.View'),
	(47,'KeyInformantSurvey.View'),
	(47,'RapidPerceptionSurvey.View'),
	(47,'SpotReport'),
	(47,'SpotReport.AddUpdate'),
	(47,'SpotReport.List'),
	(47,'SpotReport.View'),
	(47,'StakeholderGroupSurvey.View'),
	(47,'StationCommanderSurvey.View'),
	(47,'WeeklyReport'),
	(47,'WeeklyReport.AddUpdate'),
	(47,'WeeklyReport.List'),
	(47,'WeeklyReport.View')
GO

INSERT INTO permissionable.PersonPermissionable 
	(PersonID, PermissionableLineage) 
VALUES 
	(48,'ActivityManagement'),
	(48,'Atmospheric'),
	(48,'Atmospheric.List'),
	(48,'Atmospheric.View'),
	(48,'Class.List'),
	(48,'Class.View'),
	(48,'Community'),
	(48,'Community.List'),
	(48,'Community.View'),
	(48,'CommunityMemberSurvey.View'),
	(48,'ConceptNote'),
	(48,'ConceptNote.AddUpdate'),
	(48,'ConceptNote.List'),
	(48,'ConceptNote.View'),
	(48,'Document'),
	(48,'Document.AddUpdate'),
	(48,'Document.List'),
	(48,'Document.View'),
	(48,'EquipmentCatalog'),
	(48,'EquipmentCatalog.List'),
	(48,'EquipmentCatalog.View'),
	(48,'EquipmentInventory'),
	(48,'EquipmentInventory.List'),
	(48,'EquipmentInventory.View'),
	(48,'FocusGroupSurvey.View'),
	(48,'Indicator'),
	(48,'Indicator.AddUpdate'),
	(48,'Indicator.View'),
	(48,'IndicatorType'),
	(48,'IndicatorType.AddUpdate'),
	(48,'IndicatorType.List'),
	(48,'IndicatorType.View'),
	(48,'KeyEvent'),
	(48,'KeyEvent.List'),
	(48,'KeyEvent.View'),
	(48,'KeyInformantSurvey.View'),
	(48,'LicenseEquipmentCatalog'),
	(48,'LicenseEquipmentCatalog.List'),
	(48,'LicenseEquipmentCatalog.View'),
	(48,'Milestone'),
	(48,'Milestone.AddUpdate'),
	(48,'Milestone.View'),
	(48,'Objective'),
	(48,'Objective.AddUpdate'),
	(48,'Objective.View'),
	(48,'Province'),
	(48,'Province.List'),
	(48,'Province.View'),
	(48,'RapidPerceptionSurvey.View'),
	(48,'SpotReport'),
	(48,'SpotReport.List'),
	(48,'SpotReport.View'),
	(48,'StakeholderGroupSurvey.View'),
	(48,'StationCommanderSurvey.View'),
	(48,'WeeklyReport'),
	(48,'WeeklyReport.List'),
	(48,'WeeklyReport.View'),
	(49,'Atmospheric'),
	(49,'Atmospheric.List'),
	(49,'Atmospheric.View'),
	(49,'Community'),
	(49,'Community.List'),
	(49,'Community.View'),
	(49,'CommunityMemberSurvey.View'),
	(49,'Document'),
	(49,'Document.AddUpdate'),
	(49,'Document.List'),
	(49,'Document.View'),
	(49,'FocusGroupSurvey.View'),
	(49,'KeyEvent'),
	(49,'KeyEvent.List'),
	(49,'KeyEvent.View'),
	(49,'KeyInformantSurvey.View'),
	(49,'Province'),
	(49,'Province.List'),
	(49,'Province.View'),
	(49,'RapidPerceptionSurvey.View'),
	(49,'SpotReport'),
	(49,'SpotReport.List'),
	(49,'SpotReport.View'),
	(49,'StakeholderGroupSurvey.View'),
	(49,'StationCommanderSurvey.View'),
	(49,'WeeklyReport'),
	(49,'WeeklyReport.List'),
	(49,'WeeklyReport.View'),
	(50,'Atmospheric'),
	(50,'Atmospheric.List'),
	(50,'Atmospheric.View'),
	(50,'Community'),
	(50,'Community.List'),
	(50,'Community.View'),
	(50,'CommunityMemberSurvey.View'),
	(50,'Document'),
	(50,'Document.AddUpdate'),
	(50,'Document.List'),
	(50,'Document.View'),
	(50,'FocusGroupSurvey.View'),
	(50,'KeyEvent'),
	(50,'KeyEvent.List'),
	(50,'KeyEvent.View'),
	(50,'KeyInformantSurvey.View'),
	(50,'Province'),
	(50,'Province.List'),
	(50,'Province.View'),
	(50,'RapidPerceptionSurvey.View'),
	(50,'SpotReport'),
	(50,'SpotReport.List'),
	(50,'SpotReport.View'),
	(50,'StakeholderGroupSurvey.View'),
	(50,'StationCommanderSurvey.View'),
	(50,'WeeklyReport'),
	(50,'WeeklyReport.List'),
	(50,'WeeklyReport.View'),
	(51,'Atmospheric'),
	(51,'Atmospheric.List'),
	(51,'Atmospheric.View'),
	(51,'CommunityMemberSurvey.View'),
	(51,'Document'),
	(51,'Document.AddUpdate'),
	(51,'Document.List'),
	(51,'Document.View'),
	(51,'FocusGroupSurvey.View'),
	(51,'KeyEvent'),
	(51,'KeyEvent.List'),
	(51,'KeyEvent.View'),
	(51,'KeyInformantSurvey.View'),
	(51,'RapidPerceptionSurvey.View'),
	(51,'SpotReport'),
	(51,'SpotReport.List'),
	(51,'SpotReport.View'),
	(51,'StakeholderGroupSurvey.View'),
	(51,'StationCommanderSurvey.View'),
	(51,'WeeklyReport'),
	(51,'WeeklyReport.List'),
	(51,'WeeklyReport.View'),
	(52,'Community'),
	(52,'Community.AddUpdate'),
	(52,'Community.List'),
	(52,'Province'),
	(52,'Province.List'),
	(52,'Province.View'),
	(56,'ActivityManagement'),
	(56,'Atmospheric'),
	(56,'Atmospheric.AddUpdate'),
	(56,'Atmospheric.List'),
	(56,'Atmospheric.View'),
	(56,'Class'),
	(56,'Class.AddUpdate'),
	(56,'Class.List'),
	(56,'Class.View'),
	(56,'Community'),
	(56,'Community.AddUpdate'),
	(56,'Community.List'),
	(56,'Community.View'),
	(56,'CommunityMemberSurvey'),
	(56,'CommunityMemberSurvey.AddUpdate'),
	(56,'CommunityMemberSurvey.View'),
	(56,'ConceptNote'),
	(56,'ConceptNote.AddUpdate'),
	(56,'ConceptNote.List'),
	(56,'ConceptNote.View'),
	(56,'Contact'),
	(56,'Contact.AddUpdate'),
	(56,'Contact.List'),
	(56,'Contact.View'),
	(56,'Course'),
	(56,'Course.AddUpdate'),
	(56,'Course.List'),
	(56,'Course.View'),
	(56,'DailyReport'),
	(56,'DailyReport.AddUpdate'),
	(56,'DailyReport.List'),
	(56,'DailyReport.View'),
	(56,'Document'),
	(56,'Document.AddUpdate'),
	(56,'Document.List'),
	(56,'Document.View'),
	(56,'EquipmentCatalog'),
	(56,'EquipmentCatalog.AddUpdate'),
	(56,'EquipmentCatalog.List'),
	(56,'EquipmentCatalog.View'),
	(56,'EquipmentInventory'),
	(56,'EquipmentInventory.AddUpdate'),
	(56,'EquipmentInventory.List'),
	(56,'EquipmentInventory.View'),
	(56,'FocusGroupSurvey'),
	(56,'FocusGroupSurvey.AddUpdate'),
	(56,'FocusGroupSurvey.View'),
	(56,'Indicator'),
	(56,'Indicator.AddUpdate'),
	(56,'Indicator.View'),
	(56,'IndicatorType'),
	(56,'IndicatorType.AddUpdate'),
	(56,'IndicatorType.List'),
	(56,'IndicatorType.View'),
	(56,'KeyEvent'),
	(56,'KeyEvent.AddUpdate'),
	(56,'KeyEvent.List'),
	(56,'KeyEvent.View'),
	(56,'KeyInformantSurvey'),
	(56,'KeyInformantSurvey.AddUpdate'),
	(56,'KeyInformantSurvey.View'),
	(56,'License'),
	(56,'License.AddUpdate'),
	(56,'License.List'),
	(56,'License.View'),
	(56,'LicenseEquipmentCatalog'),
	(56,'LicenseEquipmentCatalog.AddUpdate'),
	(56,'LicenseEquipmentCatalog.List'),
	(56,'LicenseEquipmentCatalog.View'),
	(56,'Milestone'),
	(56,'Milestone.AddUpdate'),
	(56,'Milestone.View'),
	(56,'Objective'),
	(56,'Objective.AddUpdate'),
	(56,'Objective.View'),
	(56,'Person'),
	(56,'Person.AddUpdate'),
	(56,'Person.List'),
	(56,'Person.View'),
	(56,'Province'),
	(56,'Province.AddUpdate'),
	(56,'Province.List'),
	(56,'Province.View'),
	(56,'RAPData'),
	(56,'RAPData.List'),
	(56,'RapidPerceptionSurvey'),
	(56,'RapidPerceptionSurvey.View'),
	(56,'SpotReport'),
	(56,'SpotReport.AddUpdate'),
	(56,'SpotReport.List'),
	(56,'SpotReport.View'),
	(56,'StakeholderGroupSurvey'),
	(56,'StakeholderGroupSurvey.AddUpdate'),
	(56,'StakeholderGroupSurvey.View'),
	(56,'StationCommanderSurvey'),
	(56,'StationCommanderSurvey.AddUpdate'),
	(56,'StationCommanderSurvey.View'),
	(56,'Team'),
	(56,'Team.AddUpdate'),
	(56,'Team.List'),
	(56,'Team.View'),
	(56,'WeeklyReport'),
	(56,'WeeklyReport.AddUpdate'),
	(56,'WeeklyReport.List'),
	(56,'WeeklyReport.View'),
	(57,'CommunityMemberSurvey'),
	(57,'CommunityMemberSurvey.AddUpdate'),
	(57,'CommunityMemberSurvey.View'),
	(57,'FocusGroupSurvey'),
	(57,'FocusGroupSurvey.AddUpdate'),
	(57,'FocusGroupSurvey.View'),
	(57,'KeyInformantSurvey'),
	(57,'KeyInformantSurvey.AddUpdate'),
	(57,'KeyInformantSurvey.View'),
	(57,'RapidPerceptionSurvey'),
	(57,'RapidPerceptionSurvey.View'),
	(57,'StakeholderGroupSurvey'),
	(57,'StakeholderGroupSurvey.AddUpdate'),
	(57,'StakeholderGroupSurvey.View'),
	(57,'StationCommanderSurvey'),
	(57,'StationCommanderSurvey.AddUpdate'),
	(57,'StationCommanderSurvey.View'),
	(58,'CommunityMemberSurvey'),
	(58,'CommunityMemberSurvey.AddUpdate'),
	(58,'CommunityMemberSurvey.View'),
	(58,'FocusGroupSurvey'),
	(58,'FocusGroupSurvey.AddUpdate'),
	(58,'FocusGroupSurvey.View'),
	(58,'KeyInformantSurvey'),
	(58,'KeyInformantSurvey.AddUpdate'),
	(58,'KeyInformantSurvey.View'),
	(58,'RAPData'),
	(58,'RAPData.List'),
	(58,'RapidPerceptionSurvey'),
	(58,'RapidPerceptionSurvey.View'),
	(58,'StakeholderGroupSurvey'),
	(58,'StakeholderGroupSurvey.AddUpdate'),
	(58,'StakeholderGroupSurvey.View'),
	(58,'StationCommanderSurvey'),
	(58,'StationCommanderSurvey.AddUpdate'),
	(58,'StationCommanderSurvey.View'),
	(59,'CommunityMemberSurvey'),
	(59,'CommunityMemberSurvey.AddUpdate'),
	(59,'CommunityMemberSurvey.View'),
	(59,'FocusGroupSurvey'),
	(59,'FocusGroupSurvey.AddUpdate'),
	(59,'FocusGroupSurvey.View'),
	(59,'KeyInformantSurvey'),
	(59,'KeyInformantSurvey.AddUpdate'),
	(59,'KeyInformantSurvey.View'),
	(59,'RapidPerceptionSurvey'),
	(59,'RapidPerceptionSurvey.View'),
	(59,'StakeholderGroupSurvey'),
	(59,'StakeholderGroupSurvey.AddUpdate'),
	(59,'StakeholderGroupSurvey.View'),
	(59,'StationCommanderSurvey'),
	(59,'StationCommanderSurvey.AddUpdate'),
	(59,'StationCommanderSurvey.View'),
	(62,'Atmospheric'),
	(62,'Atmospheric.AddUpdate'),
	(62,'Atmospheric.List'),
	(62,'Atmospheric.View'),
	(62,'Community'),
	(62,'Community.List'),
	(62,'Community.View'),
	(62,'CommunityMemberSurvey.View'),
	(62,'Document'),
	(62,'Document.AddUpdate'),
	(62,'Document.List'),
	(62,'Document.View'),
	(62,'FocusGroupSurvey.View'),
	(62,'KeyEvent'),

	(62,'KeyEvent.AddUpdate'),
	(62,'KeyEvent.List'),
	(62,'KeyEvent.View'),
	(62,'KeyInformantSurvey.View'),
	(62,'Province'),
	(62,'Province.List'),
	(62,'Province.View'),
	(62,'RapidPerceptionSurvey.View'),
	(62,'SpotReport'),
	(62,'SpotReport.AddUpdate'),
	(62,'SpotReport.List'),
	(62,'SpotReport.View'),
	(62,'StakeholderGroupSurvey.View'),
	(62,'StationCommanderSurvey.View'),
	(62,'WeeklyReport'),
	(62,'WeeklyReport.AddUpdate'),
	(62,'WeeklyReport.List'),
	(62,'WeeklyReport.View'),
	(63,'Atmospheric'),
	(63,'Atmospheric.List'),
	(63,'Atmospheric.View'),
	(63,'Community'),
	(63,'Community.List'),
	(63,'Community.View'),
	(63,'CommunityMemberSurvey.View'),
	(63,'Document'),
	(63,'Document.AddUpdate'),
	(63,'Document.List'),
	(63,'Document.View'),
	(63,'FocusGroupSurvey.View'),
	(63,'KeyEvent'),
	(63,'KeyEvent.List'),
	(63,'KeyEvent.View'),
	(63,'KeyInformantSurvey.View'),
	(63,'Province'),
	(63,'Province.List'),
	(63,'Province.View'),
	(63,'RapidPerceptionSurvey.View'),
	(63,'SpotReport'),
	(63,'SpotReport.List'),
	(63,'SpotReport.View'),
	(63,'StakeholderGroupSurvey.View'),
	(63,'StationCommanderSurvey.View'),
	(63,'WeeklyReport'),
	(63,'WeeklyReport.List'),
	(63,'WeeklyReport.View'),
	(64,'ActivityManagement'),
	(64,'Atmospheric'),
	(64,'Atmospheric.List'),
	(64,'Atmospheric.View'),
	(64,'Class.List'),
	(64,'Class.View'),
	(64,'Community'),
	(64,'Community.AddUpdate'),
	(64,'Community.List'),
	(64,'Community.View'),
	(64,'CommunityMemberSurvey'),
	(64,'CommunityMemberSurvey.AddUpdate'),
	(64,'CommunityMemberSurvey.View'),
	(64,'ConceptNote'),
	(64,'ConceptNote.AddUpdate'),
	(64,'ConceptNote.List'),
	(64,'ConceptNote.View'),
	(64,'Document'),
	(64,'Document.AddUpdate'),
	(64,'Document.List'),
	(64,'Document.View'),
	(64,'EquipmentCatalog'),
	(64,'EquipmentCatalog.List'),
	(64,'EquipmentCatalog.View'),
	(64,'EquipmentInventory'),
	(64,'EquipmentInventory.List'),
	(64,'EquipmentInventory.View'),
	(64,'FocusGroupSurvey'),
	(64,'FocusGroupSurvey.AddUpdate'),
	(64,'FocusGroupSurvey.View'),
	(64,'Indicator'),
	(64,'Indicator.AddUpdate'),
	(64,'Indicator.View'),
	(64,'IndicatorType'),
	(64,'IndicatorType.AddUpdate'),
	(64,'IndicatorType.List'),
	(64,'IndicatorType.View'),
	(64,'KeyEvent'),
	(64,'KeyEvent.List'),
	(64,'KeyEvent.View'),
	(64,'KeyInformantSurvey'),
	(64,'KeyInformantSurvey.AddUpdate'),
	(64,'KeyInformantSurvey.View'),
	(64,'LicenseEquipmentCatalog'),
	(64,'LicenseEquipmentCatalog.List'),
	(64,'LicenseEquipmentCatalog.View'),
	(64,'Milestone'),
	(64,'Milestone.AddUpdate'),
	(64,'Milestone.View'),
	(64,'Objective'),
	(64,'Objective.AddUpdate'),
	(64,'Objective.View'),
	(64,'Province'),
	(64,'Province.AddUpdate'),
	(64,'Province.List'),
	(64,'Province.View'),
	(64,'RAPData'),
	(64,'RAPData.List'),
	(64,'RapidPerceptionSurvey'),
	(64,'RapidPerceptionSurvey.View'),
	(64,'SpotReport'),
	(64,'SpotReport.List'),
	(64,'SpotReport.View'),
	(64,'StakeholderGroupSurvey'),
	(64,'StakeholderGroupSurvey.AddUpdate'),
	(64,'StakeholderGroupSurvey.View'),
	(64,'StationCommanderSurvey'),
	(64,'StationCommanderSurvey.AddUpdate'),
	(64,'StationCommanderSurvey.View'),
	(64,'WeeklyReport'),
	(64,'WeeklyReport.List'),
	(64,'WeeklyReport.View'),
	(66,'Atmospheric'),
	(66,'Atmospheric.List'),
	(66,'Atmospheric.View'),
	(66,'Community'),
	(66,'Community.View'),
	(66,'CommunityMemberSurvey.View'),
	(66,'FocusGroupSurvey.View'),
	(66,'KeyEvent'),
	(66,'KeyEvent.List'),
	(66,'KeyEvent.View'),
	(66,'KeyInformantSurvey.View'),
	(66,'Province'),
	(66,'Province.List'),
	(66,'Province.View'),
	(66,'RAPData'),
	(66,'RapidPerceptionSurvey.View'),
	(66,'SpotReport'),
	(66,'SpotReport.List'),
	(66,'SpotReport.View'),
	(66,'StakeholderGroupSurvey.View'),
	(66,'StationCommanderSurvey.View'),
	(66,'WeeklyReport'),
	(66,'WeeklyReport.List'),
	(66,'WeeklyReport.View')
GO

IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (69,17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (69,17,26)

	END
--ENDIF
GO
--End table permissionable.PersonPermissionable
