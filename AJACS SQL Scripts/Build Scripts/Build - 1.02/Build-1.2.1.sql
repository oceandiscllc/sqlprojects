USE AJACS
GO

--Begin table dbo.MenuItemPermissionableLineage
DECLARE @TableName VARCHAR(250) = 'dbo.MenuItemPermissionableLineage'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.MenuItemPermissionableLineage
	(
	MenuItemPermissionableLineageID INT IDENTITY(1,1) NOT NULL,
	MenuItemID INT,
	PermissionableLineage VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'MenuItemID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuItemPermissionableLineageID'
EXEC utility.SetIndexClustered 'IX_MenuItemPermissionable', @TableName, 'MenuItemID'
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	MI.MenuItemID,
	MI.PermissionableLineage
FROM dbo.MenuItem MI
WHERE MI.MenuItemLink IS NOT NULL
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPData'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'RAPData.List',
	'CommunityMemberSurvey.AddUpdate',
	'FocusGroupSurvey.AddUpdate',
	'StationCommanderSurvey.AddUpdate',
	'KeyInformantSurvey.AddUpdate',
	'StakeholderGroupSurvey.AddUpdate',
	'DailyReport.List',
	'Team.List'
	)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'RAPDelivery'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'DailyReport.List',
	'Team.List'
	)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Training'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'Course.List',
	'Class.List'
	)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Procurement'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'License.List',
	'EquipmentCatalog.List',
	'EquipmentInventory.List',
	'LicenseEquipmentCatalog.List',
	'PurchaseOrder.List'
	)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Beneficiaries'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'Contact.List',
	'SubContractor.List'
	)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'LogicalFramework'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'Objective.AddUpdate',
	'Indicator.AddUpdate',
	'IndicatorType.List',
	'Milestone.AddUpdate'
	)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Admin'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'Person.List'
	)
GO
--End table dbo.MenuItemPermissionableLineage

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		0,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PersonPermissionable PP
				WHERE EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
						AND PP.PermissionableLineage = MIPL.PermissionableLineage
					)
					AND PP.PersonID = @PersonID
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50)
		)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
				THEN 1
				ELSE 0
			END,

		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsValidPassword,IsValidUserName,FullName,RoleName) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin