USE AJACS
GO

--Begin procedure workflow.CanIncrementConceptNoteWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementConceptNoteWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementConceptNoteWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementConceptNoteWorkflow

--Begin procedure workflow.CanIncrementSpotReportWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementSpotReportWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementSpotReportWorkflow

@EntityID INT


AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementSpotReportWorkflow

--Begin procedure workflow.CanIncrementWeeklyReportWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementWeeklyReportWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementWeeklyReportWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WR.WeeklyReportID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementWeeklyReportWorkflow

--Begin procedure workflow.GetConceptNoteWorkflowData
EXEC Utility.DropObject 'workflow.GetConceptNoteWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.15
-- Description:	A procedure to return workflow data from the dbo.ConceptNote table
-- ===============================================================================

CREATE PROCEDURE workflow.GetConceptNoteWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'ConceptNote'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetConceptNoteWorkflowData

--Begin procedure workflow.GetSpotReportWorkflowData
EXEC Utility.DropObject 'workflow.GetSpotReportWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A procedure to return workflow data from the dbo.SpotReport table
-- ==============================================================================

CREATE PROCEDURE workflow.GetSpotReportWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'SpotReport'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetSpotReportWorkflowData

--Begin procedure workflow.GetWeeklyReportWorkflowData
EXEC Utility.DropObject 'workflow.GetWeeklyReportWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A procedure to return workflow data from the weeklyreport.WeeklyReport table
-- =========================================================================================

CREATE PROCEDURE workflow.GetWeeklyReportWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WR.WeeklyReportID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'WeeklyReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'WeeklyReport'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Weekly Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Weekly Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Weekly Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Weekly Report'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'WeeklyReport'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetWeeklyReportWorkflowData

--Begin procedure workflow.GetWorkflowStepIDsByEntityTypeCodeAndWorkflowStepNumber
EXEC Utility.DropObject 'workflow.GetWorkflowStepIDsByEntityTypeCodeAndWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A procedure to return workflow step IDs based on an entity type code and an work flow step number
-- ==============================================================================================================

CREATE PROCEDURE workflow.GetWorkflowStepIDsByEntityTypeCodeAndWorkflowStepNumber

@EntityTypeCode VARCHAR(50), 
@WorkflowStepNumber INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = @WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		@EntityTypeCode + '.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		HD.WorkflowStepID
	FROM HD
	WHERE HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)

END
GO
--End procedure workflow.GetWorkflowStepIDsByEntityTypeCodeAndWorkflowStepNumber

--Begin procedure workflow.SetEntityWorkflowStepInComplete
EXEC Utility.DropObject 'workflow.SetEntityWorkflowStepInComplete'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.11
-- Description:	A procedure to reset the IsCOmplete bit in the workflow.EntityWorkflowStep table
-- =============================================================================================

CREATE PROCEDURE workflow.SetEntityWorkflowStepInComplete

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT

AS
BEGIN

	UPDATE EWS
	SET EWS.IsComplete = 0
	FROM workflow.EntityWorkflowStep EWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = EWS.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = @EntityTypeCode
			AND WS.WorkflowStepNumber = @WorkflowStepNumber
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.SetEntityWorkflowStepInComplete

