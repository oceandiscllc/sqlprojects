USE AJACS
GO

--Begin function dbo.FormatPersonNameByPersonID
EXEC utility.DropObject 'dbo.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.11
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION dbo.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @FirstName VARCHAR(25)
	DECLARE @LastName VARCHAR(25)
	DECLARE @cRetVal VARCHAR(250)
	DECLARE @Title VARCHAR(50)
	
	SET @cRetVal = ''
	
	SELECT
		@FirstName = ISNULL(P.FirstName, ''),
		@LastName = ISNULL(P.LastName, ''),
		@Title = ISNULL(P.Title, '')
	FROM dbo.Person P
	WHERE P.PersonID = @PersonID
	
	IF @Format = 'FirstLast' OR @Format = 'TitleFirstLast'
		BEGIN
			
		SET @cRetVal = @FirstName + ' ' + @LastName
	
		IF @Format = 'TitleFirstLast' AND LEN(RTRIM(@Title)) > 0
			BEGIN
				
			SET @cRetVal = @Title + ' ' + RTRIM(LTRIM(@cRetVal))
	
			END
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format = 'LastFirst' OR @Format = 'LastFirstTitle'
		BEGIN
			
		IF LEN(RTRIM(@LastName)) > 0
			BEGIN
				
			SET @cRetVal = @LastName + ', '
	
			END
		--ENDIF
				
		SET @cRetVal = @cRetVal + @FirstName + ' '
	
		IF @Format = 'LastFirstTitle' AND LEN(RTRIM(@Title)) > 0
			BEGIN
				
			SET @cRetVal = @cRetVal + @Title
	
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatPersonNameByPersonID

--Begin function dbo.ListToTable
EXEC utility.DropObject 'dbo.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
--
-- Author:			Todd Pires
-- Update date:	2012.06.07
-- Description:	Added the ListItemID column
--
-- Author:			Todd Pires
-- Update date:	2014.06.04
-- Description:	Added MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use CTE's, dropped MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use XML - based on a script by Kshitij Satpute from SQLServerCentral.com
-- ===================================================================================================

CREATE FUNCTION dbo.ListToTable
(
@List VARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'VARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function dbo.ListToTable

--Begin function reporting.IsDraftReport
EXEC utility.DropObject 'reporting.IsDraftReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A function to determine if a report is in draft status
-- ===================================================================

CREATE FUNCTION reporting.IsDraftReport
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nIsDraft BIT = 1
	
	IF @EntityTypeCode = 'SpotReport'
		BEGIN
		
		SELECT @nIsDraft = CASE WHEN W.WorkflowStepCount - (SELECT T.WorkflowStepNumber FROM dbo.SpotReport T WHERE T.SpotReportID = @EntityID) >= 0 THEN 1 ELSE 0 END 
		FROM workflow.Workflow W 
		WHERE W.EntityTypeCode = @EntityTypeCode
	
		END
	ELSE IF @EntityTypeCode = 'WeeklyReport'
		BEGIN
		
		SELECT @nIsDraft = CASE WHEN W.WorkflowStepCount - (SELECT T.WorkflowStepNumber FROM weeklyreport.WeeklyReport T WHERE T.WeeklyReportID = @EntityID) >= 0 THEN 1 ELSE 0 END 
		FROM workflow.Workflow W 
		WHERE W.EntityTypeCode = @EntityTypeCode
	
		END
	--ENDIF
	
	RETURN @nIsDraft

END
GO
--End function reporting.IsDraftReport

--Begin function permissionable.HasPermission
EXEC utility.DropObject 'permissionable.HasPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.10
-- Description:	A function to determine if a PeronID has a permission
-- ==================================================================

CREATE FUNCTION permissionable.HasPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP WHERE PP.PermissionableLineage = @PermissionableLineage AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function permissionable.HasPermission
