USE AJACS
GO

--Begin procedure reporting.GeConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'reporting.GeConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GeConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CNBD AS
		(
		SELECT			
			BT.BudgetTypeName,
			CNB.ItemName,
			CNB.Quantity,
			CNB.UnitCost,
			CNB.Quantity * CNB.UnitCost AS TotalCost
		FROM dbo.ConceptNoteBudget CNB
			JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
				AND CNB.ConceptNoteID = @ConceptNoteID
				
		UNION ALL
		
		SELECT
			'Equipment' AS BudgetTypeName,
			EC.ItemName, 
			CNEC.Quantity,
			EC.UnitCost,
			CNEC.Quantity * EC.UnitCost AS TotalCost
		 FROM dbo.ConceptNoteEquipmentCatalog CNEC
			 JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID 
				AND CNEC.ConceptNoteID = @ConceptNoteID
		)

	SELECT 
		CNBD.BudgetTypeName,
		CNBD.ItemName, 
		CNBD.Quantity,
		CNBD.UnitCost,
		CNBD.TotalCost,
		(SELECT SUM(CNBD.TotalCost) FROM CNBD) AS GrandTotalCost 
	FROM CNBD
	ORDER BY CNBD.BudgetTypeName, CNBD.ItemName

END
GO
--End procedure reporting.GeConceptNoteBudgetByConceptNoteID

--Begin procedure reporting.GeConceptNoteByConceptNoteID
EXEC Utility.DropObject 'reporting.GeConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			John Lyons
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data for the Concept Note report
-- ============================================================================
CREATE PROCEDURE reporting.GeConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.Background, 
		CN.ConceptNoteID,
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.Title,
		'AJACS-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4) AS ReferenceCode
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

END
GO
--End procedure reporting.GeConceptNoteByConceptNoteID

--Begin procedure reporting.GeConceptNoteCourseByConceptNoteID
EXEC Utility.DropObject 'reporting.GeConceptNoteCourseByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			John Lyons
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data for the Concept Note report
-- ============================================================================
CREATE PROCEDURE reporting.GeConceptNoteCourseByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CourseName
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

END
GO
--End procedure reporting.GeConceptNoteByConceptNoteID

--Begin procedure reporting.GeConceptNoteIndicatorByConceptNoteID
EXEC Utility.DropObject 'reporting.GeConceptNoteIndicatorByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			John Lyons
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data for the Concept Note report
-- ============================================================================
CREATE PROCEDURE reporting.GeConceptNoteIndicatorByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IndicatorName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
			AND CNI.ConceptNoteID = @ConceptNoteID
	ORDER BY I.IndicatorName, I.IndicatorID

END
GO
--End procedure reporting.GeConceptNoteIndicatorByConceptNoteID

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
-- ==================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		RTRIM(C1.LastName + ',' + C1.FirstName + ' ' + LEFT(ISNULL(C1.MiddleName, ' '), 1)) AS fullName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceID FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetRequestForInformation
EXEC Utility.DropObject 'reporting.GetRequestForInformation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
-- ====================================================================================
CREATE PROCEDURE reporting.GetRequestForInformation

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ISNULL(C2.CommunityName, RFI.Location) AS LocationName,
		dbo.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		dbo.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.InformationRequested,
		dbo.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.KnownDetails,
		dbo.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonFullname,
		dbo.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationTitle,
		dbo.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonFullname,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusName
	FROM dbo.RequestForInformation RFI
		JOIN reporting.SearchResult RSR ON RSR.EntityID = RFI.RequestForInformationID
			AND RSR.EntityTypeCode = 'RequestForInformation'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
		OUTER APPLY
				(
				SELECT
					C1.CommunityID,
					C1.CommunityName
				FROM dbo.Community C1
				WHERE C1.CommunityID = RFI.CommunityID
				) C2

END
GO
--End procedure reporting.GetRequestForInformation

--Begin procedure reporting.GetWeeklyReportCommunity
EXEC Utility.DropObject 'reporting.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportCommunity

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		C.WeeklyReportID,	
		reporting.IsDraftReport('WeeklyReport', C.WeeklyReportID) AS IsDraft,
		'AJACS-WR-A' + RIGHT('0000' + CAST(C.WeeklyReportID AS VARCHAR(10)), 4) AS ReferenceCode,
		CES.CommunityEngagementStatusName,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,
		
		CASE
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID IN (2,3)
			THEN 'Watch'
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID = 4
			THEN 'Engage'
			ELSE 'Alert'			
		END AS CommunityReportStatusName
			
	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure reporting.GetWeeklyReportCommunity

--Begin procedure reporting.GetWeeklyReportProvince
EXEC Utility.DropObject 'reporting.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportProvince

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceName, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		P.WeeklyReportID,
		reporting.IsDraftReport('WeeklyReport', P.WeeklyReportID) AS IsDraft,
	  'AJACS-WR-A' + RIGHT('0000' + CAST(P.WeeklyReportID AS VARCHAR(10)), 4) AS ReferenceCode,
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
	ORDER BY P.ProvinceName, P.ProvinceID

END
GO
--End procedure reporting.GetWeeklyReportProvince

