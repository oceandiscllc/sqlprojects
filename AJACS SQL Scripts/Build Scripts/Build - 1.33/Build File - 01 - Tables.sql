USE AJACS
GO

--Begin table dropdown.RiskStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.RiskStatus'

EXEC utility.AddColumn @TableName, 'IsForDonor', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsForDonor', 'BIT', 0
GO

UPDATE RS
SET RS.IsForDonor = 1
FROM dropdown.RiskStatus RS
WHERE RS.RiskStatusName = 'Live-External'
GO
--End table dropdown.RiskStatus
