USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@ConceptNoteID INT,
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(CT.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(CT.Longitude AS VARCHAR(MAX))
	FROM dbo.ConceptNote CN
	JOIN dbo.ConceptNoteContactEquipment CNCE ON CNCE.ConceptNoteID = CN.ConceptNoteID
		AND CN.ConceptNoteID = @ConceptNoteID
	JOIN dbo.Contact C1 ON C1.ContactID = CNCE.ContactID
	JOIN Community CT ON c1.CommunityID = ct.CommunityID  AND CT.ProvinceID = @ProvinceID
	JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CT.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / LEN('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan