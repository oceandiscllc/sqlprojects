USE AJACS
GO

--Begin table dropdown.CommunitySubGroup
UPDATE CSG
SET CSG.CommunitySubGroupName = REPLACE(CSG.CommunitySubGroupName, 'Phase', 'Wave')
FROM dropdown.CommunitySubGroup CSG
GO
--End table dropdown.CommunitySubGroup

--Begin table dropdown.ImpactDecision
IF NOT EXISTS (SELECT 1 FROM dropdown.ImpactDecision ID WHERE ID.ImpactDecisionName = 'Partial Disengagement')
	BEGIN
	
	UPDATE ID
	SET ID.DisplayOrder = ID.DisplayOrder + 1
	FROM dropdown.ImpactDecision ID
	WHERE ID.DisplayOrder > 1
	
	INSERT INTO dropdown.ImpactDecision
		(ImpactDecisionName, HexColor, DisplayOrder)
	VALUES
		('Partial Disengagement', '#E92805', 2)

	END
--ENDIF
GO
--End table dropdown.ImpactDecision

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PoliceEngagementUpdate', @NewMenuItemLink='/policeengagement/addupdate', @NewMenuItemText='Police Engagement', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='CommunityProvenceEngagementUpdate', @PermissionableLineageList='PoliceEngagementUpdate.WorkflowStepID%'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
