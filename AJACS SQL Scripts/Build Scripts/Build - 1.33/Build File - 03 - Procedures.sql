USE AJACS
GO

--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Community table
-- ====================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)
	
	INSERT INTO communityprovinceengagementupdate.Community
		(CommunityID, CommunityProvinceEngagementUpdateID, CommunityEngagementOutput1, CommunityEngagementOutput2, CommunityEngagementOutput3, CommunityEngagementOutput4, CAPAgreedDate, LastNeedsAssessmentDate, TORMOUStatusID, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@CommunityProvinceEngagementUpdateID,
		C1.CommunityEngagementOutput1, 
		C1.CommunityEngagementOutput2, 
		C1.CommunityEngagementOutput3, 
		C1.CommunityEngagementOutput4, 
		C1.CAPAgreedDate, 
		C1.LastNeedsAssessmentDate, 
		C1.TORMOUStatusID,
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM communityprovinceengagementupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO communityprovinceengagementupdate.CommunityContact
		(CommunityProvinceEngagementUpdateID, CommunityID, ContactID)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		C.CommunityID,
		C.ContactID
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND C.IsActive = 1
			AND C.IsValid = 1
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
						AND CCT.ContactID = C.ContactID
				)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactAffiliation CCA
					JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
						AND CA.ContactAffiliationName = 'Community Security Working Groups'
						AND CCA.ContactID = C.ContactID
				)

	INSERT INTO communityprovinceengagementupdate.CommunityIndicator
		(CommunityProvinceEngagementUpdateID, CommunityID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.CommunityProvinceEngagementAchievedValue, 0),
		OACI.CommunityProvinceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityProvinceEngagementAchievedValue,
				CI.CommunityProvinceEngagementNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO communityprovinceengagementupdate.CommunityProject
		(CommunityProvinceEngagementUpdateID, CommunityID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		PC.CommunityID, 
		PC.ProjectID, 
		PC.CommunityProvinceEngagementNotes
	FROM project.ProjectCommunity PC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PC.CommunityID
	
	INSERT INTO communityprovinceengagementupdate.CommunityRecommendation
		(CommunityProvinceEngagementUpdateID, CommunityID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RC.CommunityID, 
		RC.RecommendationID, 
		RC.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
	
	INSERT INTO communityprovinceengagementupdate.CommunityRisk
		(CommunityProvinceEngagementUpdateID, CommunityID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		CR.CommunityID, 
		CR.RiskID, 
		CR.CommunityProvinceEngagementRiskValue, 
		CR.CommunityProvinceEngagementNotes
	FROM dbo.CommunityRisk CR
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CR.CommunityID

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'CommunityProvinceEngagementCommunity', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Province table
-- ===================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)

	INSERT INTO communityprovinceengagementupdate.Province
		(ProvinceID, CommunityProvinceEngagementUpdateID, CommunityEngagementOutput1, CommunityEngagementOutput2, CommunityEngagementOutput3, CommunityEngagementOutput4, CAPAgreedDate, LastNeedsAssessmentDate, TORMOUStatusID, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@CommunityProvinceEngagementUpdateID,
		P1.CommunityEngagementOutput1, 
		P1.CommunityEngagementOutput2, 
		P1.CommunityEngagementOutput3, 
		P1.CommunityEngagementOutput4, 
		P1.CAPAgreedDate, 
		P1.LastNeedsAssessmentDate, 
		P1.TORMOUStatusID,
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM communityprovinceengagementupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO communityprovinceengagementupdate.ProvinceContact
		(CommunityProvinceEngagementUpdateID, ProvinceID, ContactID)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		C.ProvinceID,
		C.ContactID
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ProvinceID
			AND C.IsActive = 1
			AND C.IsValid = 1
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
						AND CCT.ContactID = C.ContactID
				)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactAffiliation CCA
					JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
						AND CA.ContactAffiliationName = 'Community Security Working Groups'
						AND CCA.ContactID = C.ContactID
				)

	INSERT INTO communityprovinceengagementupdate.ProvinceIndicator
		(CommunityProvinceEngagementUpdateID, ProvinceID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.CommunityProvinceEngagementAchievedValue, 0),
		OAPI.CommunityProvinceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.CommunityProvinceEngagementAchievedValue,
				PRI.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI

	INSERT INTO communityprovinceengagementupdate.ProvinceProject
		(CommunityProvinceEngagementUpdateID, ProvinceID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		PP.ProvinceID, 
		PP.ProjectID, 
		PP.CommunityProvinceEngagementNotes
	FROM project.ProjectProvince PP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.ProvinceID
	
	INSERT INTO communityprovinceengagementupdate.ProvinceRecommendation
		(CommunityProvinceEngagementUpdateID, ProvinceID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RP.ProvinceID, 
		RP.RecommendationID, 
		RP.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
	
	INSERT INTO communityprovinceengagementupdate.ProvinceRisk
		(CommunityProvinceEngagementUpdateID, ProvinceID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		PR.ProvinceID, 
		PR.RiskID, 
		PR.CommunityProvinceEngagementRiskValue, 
		PR.CommunityProvinceEngagementNotes
	FROM dbo.ProvinceRisk PR
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PR.ProvinceID

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'CommunityProvinceEngagementProvince', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

--Begin procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A procedure to approve a Community/Province Engagement Update
-- ==========================================================================
CREATE PROCEDURE communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nCommunityProvinceEngagementUpdateID INT = ISNULL((SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC), 0)
	DECLARE @nContactAffiliationID INT = ISNULL((SELECT CA.ContactAffiliationID FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Community Security Working Groups'), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.CAPAgreedDate = CPEU.CAPAgreedDate,
		P.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		P.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		P.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN communityprovinceengagementupdate.Province CPEU ON CPEU.ProvinceID = P.ProvinceID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputProvince O ON O.ProvinceID = C.ProvinceID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		PC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.ProvinceContact PC

	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.CommunityProvinceEngagementAchievedValue, 
		PRI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceIndicator PRI

	DELETE PC
	FROM project.ProjectProvince PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO project.ProjectProvince
		(ProvinceID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.ProvinceID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceProject CP

	DELETE RP
	FROM recommendation.RecommendationProvince RP
		JOIN @tOutputProvince O ON O.ProvinceID = RP.ProvinceID

	INSERT INTO recommendation.RecommendationProvince
		(ProvinceID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RecommendationID,
		PR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceRecommendation PR

	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.PoliceEngagementRiskValue, 
		PR.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Province'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementProvince'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')

	UPDATE C
	SET
		C.CAPAgreedDate = CPEU.CAPAgreedDate,
		C.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		C.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		C.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN communityprovinceengagementupdate.Community CPEU ON CPEU.CommunityID = C.CommunityID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputCommunity O ON O.CommunityID = C.CommunityID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		CC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.CommunityContact CC

	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.CommunityProvinceEngagementAchievedValue, 
		CI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityIndicator CI

	DELETE PC
	FROM project.ProjectCommunity PC
		JOIN @tOutputCommunity O ON O.CommunityID = PC.CommunityID
	
	INSERT INTO project.ProjectCommunity
		(CommunityID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.CommunityID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityProject CP

	DELETE RC
	FROM recommendation.RecommendationCommunity RC
		JOIN @tOutputCommunity O ON O.CommunityID = RC.CommunityID
	
	INSERT INTO recommendation.RecommendationCommunity
		(CommunityID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RecommendationID,
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRecommendation CR

	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.CommunityProvinceEngagementRiskValue, 
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Community'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementCommunity'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	
	DELETE FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

	TRUNCATE TABLE communityprovinceengagementupdate.Province
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceContact
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceProject
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRisk

	TRUNCATE TABLE communityprovinceengagementupdate.Community
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityContact
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityProject
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRisk

END

GO
--End procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

--Begin procedure communityprovinceengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dbo.Community and communityprovinceengagementupdate.Community tables
-- ============================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CAPAgreedDate,
		dbo.FormatDate(C.CAPAgreedDate) AS CAPAgreedDateFormatted,
		C.CommunityID,
		C.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4,
		C.CommunityName AS EntityName,
		C.LastNeedsAssessmentDate,
		dbo.FormatDate(C.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM dbo.Community C
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = C.TORMOUStatusID
			AND C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CAPAgreedDate,
		dbo.FormatDate(C1.CAPAgreedDate) AS CAPAgreedDateFormatted,
		C1.CommunityID,
		C1.CommunityEngagementOutput1,
		C1.CommunityEngagementOutput2,
		C1.CommunityEngagementOutput3,
		C1.CommunityEngagementOutput4,
		C1.LastNeedsAssessmentDate,
		dbo.FormatDate(C1.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		C2.CommunityName AS EntityName,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM communityprovinceengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = C1.TORMOUStatusID
			AND C1.CommunityID = @CommunityID

	--EntityContactCurrent
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
	WHERE C.IsActive = 1
		AND C.IsValid = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
					AND CCT.ContactID = C.ContactID
			)
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactAffiliation CCA
				JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
					AND CA.ContactAffiliationName = 'Community Security Working Groups'
					AND CCA.ContactID = C.ContactID
			)
		AND C.CommunityID = @CommunityID
	ORDER BY 2

	--EntityContactUpdate
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN communityprovinceengagementupdate.CommunityContact CC ON CC.ContactID = C.ContactID
			AND CC.CommunityID = @CommunityID
	ORDER BY 2

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityFinding
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
			AND F.IsActive = 1
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingCommunity FC
				WHERE FC.FindingID = F.FindingID
					AND FC.CommunityID = @CommunityID
				)
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingIndicator FI
					JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
						AND FI.FindingID = F.FindingID
						AND I.IsActive = 1
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO2'
				)
	ORDER BY F.FindingName, F.FindingID		

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.CommunityProvinceEngagementAchievedValue,
		OACI.CommunityProvinceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.CommunityProvinceEngagementAchievedValue, 
				CI.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(ISNULL(OACP.ProjectCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		OUTER APPLY
			(
			SELECT
				PC.ProjectCommunityID
			FROM project.ProjectCommunity PC
			WHERE PC.ProjectID = P.ProjectID
				AND PC.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		OACP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(P.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		OUTER APPLY
			(
			SELECT
				CP.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityProject CP
			WHERE CP.ProjectID = P.ProjectID
				AND CP.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(ISNULL(OACR.RecommendationCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				RC.RecommendationCommunityID
			FROM recommendation.RecommendationCommunity RC
			WHERE RC.RecommendationID = R.RecommendationID
				AND RC.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.CommunityProvinceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.CommunityProvinceEngagementRiskValue,
		OACR.CommunityProvinceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementRiskValue, 
				CR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure communityprovinceengagementupdate.GetProvinceByProvinceID

--Begin procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.11
-- Description:	A stored procedure to get data from the communityprovinceengagementupdate.CommunityProvinceEngagementUpdate table
-- ==============================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CommunityProvinceEngagementUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (CommunityProvinceEngagementUpdateID INT)

		INSERT INTO communityprovinceengagementupdate.CommunityProvinceEngagementUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.CommunityProvinceEngagementUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.CommunityProvinceEngagementUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'

		SELECT @CommunityProvinceEngagementUpdateID = O.CommunityProvinceEngagementUpdateID FROM @tOutput O
		
		EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='create', @PersonID = @PersonID

		END
	ELSE
		SELECT @CommunityProvinceEngagementUpdateID = CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU
	--ENDIF
	
	--CommunityProvinceEngagement
	SELECT
		CPEU.CommunityProvinceEngagementUpdateID, 
		CPEU.WorkflowStepNumber 
	FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			JOIN communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ON CPEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowStatus
	SELECT
		'CommunityProvinceEngagementUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @CommunityProvinceEngagementUpdateID

	--WorkflowStepWorkflowAction
	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT CPEU.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU WHERE CPEU.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

--Begin procedure communityprovinceengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dbo.Province table and communityprovinceengagementupdate.Province tables
-- ================================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CAPAgreedDate,
		dbo.FormatDate(P.CAPAgreedDate) AS CAPAgreedDateFormatted,
		P.ProvinceID,
		P.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4,
		P.ProvinceName AS EntityName,
		P.LastNeedsAssessmentDate,
		dbo.FormatDate(P.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM dbo.Province P
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = P.TORMOUStatusID
			AND P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CAPAgreedDate,
		dbo.FormatDate(P1.CAPAgreedDate) AS CAPAgreedDateFormatted,
		P1.ProvinceID,
		P1.CommunityEngagementOutput1,
		P1.CommunityEngagementOutput2,
		P1.CommunityEngagementOutput3,
		P1.CommunityEngagementOutput4,
		P1.LastNeedsAssessmentDate,
		dbo.FormatDate(P1.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		P2.ProvinceName AS EntityName,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM communityprovinceengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = P1.TORMOUStatusID
			AND P1.ProvinceID = @ProvinceID

	--EntityContactCurrent
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
	WHERE C.IsActive = 1
		AND C.IsValid = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
					AND CCT.ContactID = C.ContactID
			)
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactAffiliation CCA
				JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
					AND CA.ContactAffiliationName = 'Community Security Working Groups'
					AND CCA.ContactID = C.ContactID
			)
		AND C.ProvinceID = @ProvinceID
	ORDER BY 2

	--EntityContactUpdate
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN communityprovinceengagementupdate.ProvinceContact PC ON PC.ContactID = C.ContactID
			AND PC.ProvinceID = @ProvinceID
	ORDER BY 2

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityFinding
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
			AND F.IsActive = 1
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingProvince FP
				WHERE FP.FindingID = F.FindingID
					AND FP.ProvinceID = @ProvinceID
				)
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingIndicator FI
					JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
						AND FI.FindingID = F.FindingID
						AND I.IsActive = 1
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO2'
				)
	ORDER BY F.FindingName, F.FindingID		

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.CommunityProvinceEngagementAchievedValue,
		OAPI.CommunityProvinceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.CommunityProvinceEngagementAchievedValue, 
				PRI.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(OAPP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		OUTER APPLY
			(
			SELECT
				PP.ProjectProvinceID
			FROM project.ProjectProvince PP
			WHERE PP.ProjectID = P.ProjectID
				AND PP.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		OAPP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(P.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP1 ON PP1.ProjectID = P.ProjectID
			AND PP1.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		OUTER APPLY
			(
			SELECT
				PP2.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceProject PP2
			WHERE PP2.ProjectID = P.ProjectID
				AND PP2.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(ISNULL(OAPR.RecommendationProvinceID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				RP.RecommendationProvinceID
			FROM recommendation.RecommendationProvince RP
			WHERE RP.RecommendationID = R.RecommendationID
				AND RP.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.CommunityProvinceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.CommunityProvinceEngagementRiskValue,
		OAPR.CommunityProvinceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure communityprovinceengagementupdate.GetProvinceByProvinceID

--Begin procedure dbo.CheckDonorRiskAccess
EXEC Utility.DropObject 'dbo.CheckDonorRiskAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.12
-- Description:	A stored procedure to check person acccess to a specific risk
-- ==========================================================================
CREATE PROCEDURE dbo.CheckDonorRiskAccess

@PersonID INT,
@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;
				
	SELECT R.RiskID
	FROM dbo.Risk R
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
			AND R.RiskID = @RiskID
			AND 
				(
				RS.IsForDonor = 1
					OR NOT EXISTS
						(
						SELECT 1
						FROM dbo.Person P
							JOIN dropdown.Role R ON R.RoleID = P.RoleID
								AND P.PersonID = @PersonID
								AND R.RoleName = 'Donor'
						)
				)

END
GO
--End procedure dbo.CheckDonorRiskAccess

--Begin procedure dbo.GetCommunityMemberSurveyChartDataByProvinceID
EXEC Utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.06
-- Description:	A stored procedure to get data from the dbo.CommunityMemberSurvey table
-- ====================================================================================
CREATE PROCEDURE dbo.GetCommunityMemberSurveyChartDataByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Concern
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountC AS Most,
		D.QuestionCountB AS Neutral,
		D.QuestionCountA AS Least
	FROM
		(
		SELECT
			6 AS Question,
			'Robbery' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question54 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question54 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question54 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			5 AS Question,
			'Sexual assault/violence' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question55 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question55 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question55 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			4 AS Question,
			'Murder' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question56 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question56 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question56 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			3 AS Question,
			'Kidnapping' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question57 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question57 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question57 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			2 AS Question,
			'Vandalism' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question58 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question58 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question58 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			1 AS Question,
			'Assault' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question59 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question59 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question59 IN (4,5)) AS QuestionCountC
		) D
	ORDER BY D.Question
	
	--Confidence
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountC AS Most,
		D.QuestionCountB AS Neutral,
		D.QuestionCountA AS Least
	FROM
		(
		SELECT
			5 AS Question,
			'Police' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question22 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question22 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question22 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			4 AS Question,
			'Armed Groups' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question23 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question23 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question23 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			3 AS Question,
			'Formal Courts' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question24 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question24 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question24 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			2 AS Question,
			'Community leaders' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question25 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question25 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question25 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			1 AS Question,
			'Community Based Organizations' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question26 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question26 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question26 IN (4,5)) AS QuestionCountC
		) D
	ORDER BY D.Question
	
	--FSPPerception
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountYes AS Yes,
		D.QuestionCountNo AS No
	FROM
		(
		SELECT
			1 AS Question,
			'Are the police are responsive to your concerns' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question62 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question62 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			2 AS Question,
			'Do the police serve your interests' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question64 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question64 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			3 AS Question,
			'Are you able to voice your concerns' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question67 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question67 = 2) AS QuestionCountNo
		) D
	ORDER BY D.Question
	
	--Services
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountYes AS Yes,
		D.QuestionCountNo AS No
	FROM
		(
		SELECT
			12 AS Question,
			'Formal courts' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question01 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question01 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			11 AS Question,
			'Legal support for low income populations' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question02 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question02 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			10 AS Question,
			'Victim support' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question03 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question03 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			9 AS Question,
			'Family services' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question04 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question04 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			8 AS Question,
			'Dispute mediation (property)' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question05 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question05 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			7 AS Question,
			'Dispute mediation (land)' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question06 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question06 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			6 AS Question, 
			'Dispute mediation (business)' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question07 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question07 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			5 AS Question, 
			'Dispute mediation (other)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question08 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question08 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			4 AS Question, 
			'Public records services (birth/death certificates)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question10 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question10 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			3 AS Question, 
			'Public records services (land/property)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question11 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question11 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			2 AS Question, 
			'Public records services (marriage)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question12 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question12 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			1 AS Question, 
			'Public records services (other)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question13 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID AND C.ProvinceID = @ProvinceID AND CMS.Question13 = 2) AS QuestionCountNo
		) D
	ORDER BY D.Question

END
GO
--End procedure dbo.GetCommunityMemberSurveyChartDataByProvinceID

--Begin procedure policeengagementupdate.AddPoliceEngagementCommunity
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Community table
-- =========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Community
		(CommunityID, PoliceEngagementUpdateID, UpdatePersonID, CapacityAssessmentDate, PPPDate, PoliceEngagementOutput1, MaterialSupportStatus)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID,
		C1.CapacityAssessmentDate, 
		C1.PPPDate, 
		C1.PoliceEngagementOutput1, 
		C1.MaterialSupportStatus
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM policeengagementupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO policeengagementupdate.CommunityClass
		(PoliceEngagementUpdateID, CommunityID, ClassID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OACC.PoliceEngagementNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
					JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNC2.CommunityID
						AND CNC2.ConceptNoteID = CNC1.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('PEO2','PEO3')
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = CAST(LTT.ListItem AS INT)
				) OACC

	INSERT INTO policeengagementupdate.CommunityIndicator
		(PoliceEngagementUpdateID, CommunityID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.PoliceEngagementAchievedValue, 0),
		OACI.PoliceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.PoliceEngagementAchievedValue,
				CI.PoliceEngagementNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI
	
	INSERT INTO policeengagementupdate.CommunityRecommendation
		(PoliceEngagementUpdateID, CommunityID, RecommendationID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RC.CommunityID, 
		RC.RecommendationID, 
		RC.PoliceEngagementNotes
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
	
	INSERT INTO policeengagementupdate.CommunityRisk
		(PoliceEngagementUpdateID, CommunityID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CR.CommunityID, 
		CR.RiskID, 
		CR.CommunityProvinceEngagementRiskValue, 
		CR.CommunityProvinceEngagementNotes
	FROM dbo.CommunityRisk CR
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CR.CommunityID

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'PoliceEngagementCommunity', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementCommunity

--Begin procedure policeengagementupdate.AddPoliceEngagementProvince
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Province table
-- ========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Province
		(ProvinceID, PoliceEngagementUpdateID, UpdatePersonID, CapacityAssessmentDate, PPPDate, PoliceEngagementOutput1, MaterialSupportStatus)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID,
		P1.CapacityAssessmentDate, 
		P1.PPPDate, 
		P1.PoliceEngagementOutput1, 
		P1.MaterialSupportStatus
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM policeengagementupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO policeengagementupdate.ProvinceClass
		(PoliceEngagementUpdateID, ProvinceID, ClassID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OAPC.PoliceEngagementNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
					JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNP.ProvinceID
						AND CNP.ConceptNoteID = CNC.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('PEO2','PEO3')
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = CAST(LTT.ListItem AS INT)
				) OAPC

	INSERT INTO policeengagementupdate.ProvinceIndicator
		(PoliceEngagementUpdateID, ProvinceID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.PoliceEngagementAchievedValue, 0),
		OAPI.PoliceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.PoliceEngagementAchievedValue,
				PRI.PoliceEngagementNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO policeengagementupdate.ProvinceRecommendation
		(PoliceEngagementUpdateID, ProvinceID, RecommendationID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RP.ProvinceID, 
		RP.RecommendationID, 
		RP.PoliceEngagementNotes
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
	
	INSERT INTO policeengagementupdate.ProvinceRisk
		(PoliceEngagementUpdateID, ProvinceID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		PR.ProvinceID, 
		PR.RiskID, 
		PR.PoliceEngagementRiskValue, 
		PR.PoliceEngagementNotes
	FROM dbo.ProvinceRisk PR
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PR.ProvinceID

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'PoliceEngagementProvince', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementProvince

--Begin procedure policeengagementupdate.ApprovePoliceEngagementUpdate
EXEC Utility.DropObject 'policeengagementupdate.ApprovePoliceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2015.09.24
-- Description:	A procedure to approve a Police Engagement Update
-- ==============================================================
CREATE PROCEDURE policeengagementupdate.ApprovePoliceEngagementUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nPoliceEngagementUpdateID INT = ISNULL((SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogPoliceEngagementAction @nPoliceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogPoliceEngagementAction @nPoliceEngagementUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.CapacityAssessmentDate = PEU.CapacityAssessmentDate, 
		P.PoliceEngagementOutput1 = PEU.PoliceEngagementOutput1,
		P.PPPDate = PEU.PPPDate
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN policeengagementupdate.Province PEU ON PEU.ProvinceID = P.ProvinceID
			AND PEU.PoliceEngagementUpdateID = @nPoliceEngagementUpdateID

	DELETE PC
	FROM dbo.ProvinceClass PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO dbo.ProvinceClass
		(ProvinceID, ClassID, PoliceEngagementNotes)
	SELECT
		PC.ProvinceID,
		PC.ClassID,
		PC.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceClass PC

	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.PoliceEngagementAchievedValue, 
		PRI.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceIndicator PRI

	DELETE RP
	FROM recommendation.RecommendationProvince RP
		JOIN @tOutputProvince O ON O.ProvinceID = RP.ProvinceID
	
	INSERT INTO recommendation.RecommendationProvince
		(ProvinceID, RecommendationID, PoliceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RecommendationID,
		PR.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceRecommendation PR

	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.PoliceEngagementRiskValue, 
		PR.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Province'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	UPDATE C
	SET
		C.CapacityAssessmentDate = PEU.CapacityAssessmentDate, 
		C.PoliceEngagementOutput1 = PEU.PoliceEngagementOutput1,
		C.PPPDate = PEU.PPPDate
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN policeengagementupdate.Community PEU ON PEU.CommunityID = C.CommunityID
			AND PEU.PoliceEngagementUpdateID = @nPoliceEngagementUpdateID

	DELETE CC
	FROM dbo.CommunityClass CC
		JOIN @tOutputCommunity O ON O.CommunityID = CC.CommunityID
	
	INSERT INTO dbo.CommunityClass
		(CommunityID, ClassID, PoliceEngagementNotes)
	SELECT
		CC.CommunityID,
		CC.ClassID,
		CC.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityClass CC

	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.PoliceEngagementAchievedValue, 
		CI.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityIndicator CI

	DELETE RC
	FROM recommendation.RecommendationCommunity RC
		JOIN @tOutputCommunity O ON O.CommunityID = RC.CommunityID
	
	INSERT INTO recommendation.RecommendationCommunity
		(CommunityID, RecommendationID, PoliceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RecommendationID,
		CR.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityRecommendation CR

	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.PoliceEngagementRiskValue, 
		CR.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Community'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	
	DELETE FROM policeengagementupdate.PoliceEngagementUpdate

	TRUNCATE TABLE policeengagementupdate.Province
	TRUNCATE TABLE policeengagementupdate.ProvinceClass
	TRUNCATE TABLE policeengagementupdate.ProvinceIndicator
	TRUNCATE TABLE policeengagementupdate.ProvinceRecommendation
	TRUNCATE TABLE policeengagementupdate.ProvinceRisk

	TRUNCATE TABLE policeengagementupdate.Community
	TRUNCATE TABLE policeengagementupdate.CommunityClass
	TRUNCATE TABLE policeengagementupdate.CommunityIndicator
	TRUNCATE TABLE policeengagementupdate.CommunityRecommendation
	TRUNCATE TABLE policeengagementupdate.CommunityRisk

END

GO
--End procedure policeengagementupdate.ApprovePoliceEngagementUpdate

--Begin procedure policeengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Community and policeengagementupdate.Community tables
-- =================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CapacityAssessmentDate,
		dbo.FormatDate(C.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.MaterialSupportStatus,
		C.PoliceEngagementOutput1,
		C.PPPDate,
		dbo.FormatDate(C.PPPDate) AS PPPDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CapacityAssessmentDate,
		dbo.FormatDate(C1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C1.MaterialSupportStatus,
		C1.PoliceEngagementOutput1,
		C1.PPPDate,
		dbo.FormatDate(C1.PPPDate) AS PPPDateFormatted,
		C2.CommunityID,
		C2.CommunityName AS EntityName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.CommunityID = @CommunityID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.PoliceEngagementAchievedValue,
		OACI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.PoliceEngagementAchievedValue, 
				CI.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(ISNULL(OACR.RecommendationCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RC.RecommendationCommunityID
			FROM recommendation.RecommendationCommunity RC
			WHERE RC.RecommendationID = R.RecommendationID
				AND RC.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.PoliceEngagementRiskValue,
		OACR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID,
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.CommunityID = @CommunityID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.CommunityID = @CommunityID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.CommunityID = @CommunityID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetCommunityByCommunityID

--Begin procedure policeengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Province table and policeengagementupdate.Province tables
-- =====================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CapacityAssessmentDate,
		dbo.FormatDate(P.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P.MaterialSupportStatus,
		P.PoliceEngagementOutput1,
		P.PPPDate,
		dbo.FormatDate(P.PPPDate) AS PPPDateFormatted,
		P.ProvinceID,
		P.ProvinceName AS EntityName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CapacityAssessmentDate,
		dbo.FormatDate(P1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P1.MaterialSupportStatus,
		P1.PoliceEngagementOutput1,
		P1.PPPDate,
		dbo.FormatDate(P1.PPPDate) AS PPPDateFormatted,
		P2.ProvinceID,
		P2.ProvinceName AS EntityName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
			AND P1.ProvinceID = @ProvinceID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.PoliceEngagementAchievedValue,
		OAPI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.PoliceEngagementAchievedValue, 
				PRI.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(ISNULL(OAPR.RecommendationProvinceID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RP.RecommendationProvinceID
			FROM recommendation.RecommendationProvince RP
			WHERE RP.RecommendationID = R.RecommendationID
				AND RP.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.PoliceEngagementRiskValue,
		OAPR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID,
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.ProvinceID = @ProvinceID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.ProvinceID = @ProvinceID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.ProvinceID = @ProvinceID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetProvinceByProvinceID

--Begin procedure reporting.EDPProvincesAndCommunitiesByConceptNoteID
EXEC Utility.DropObject 'reporting.EDPProvincesAndCommunitiesByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to get reporting data
-- =====================================================
CREATE PROCEDURE reporting.EDPProvincesAndCommunitiesByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCommunityList VARCHAR(MAX) = ''
	DECLARE @nProvinceID INT
	DECLARE @tTable1 TABLE (CommunityID INT, ProvinceID INT)
	DECLARE @tTable2 TABLE (ProvinceID INT, CommunityNameList VARCHAR(MAX))

	INSERT INTO @tTable1
		(CommunityID, ProvinceID)
	SELECT DISTINCT
		C1.CommunityID,

		CASE
			WHEN C1.CommunityID > 0
			THEN dbo.GetProvinceIDByCommunityID(C1.CommunityID)
			ELSE C1.ProvinceID
		END AS ProvinceID
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteContactEquipment CNCE ON CNCE.ConceptNoteID = CN.ConceptNoteID
			AND CN.ConceptNoteID = 111
		JOIN dbo.Contact C1 ON C1.ContactID = CNCE.ContactID

	INSERT INTO @tTable2
		(ProvinceID)
	SELECT DISTINCT
		T1.ProvinceID
	FROM @tTable1 T1

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT T1.ProvinceID
		FROM @tTable1 T1
		ORDER BY T1.ProvinceID
		
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		SET @cCommunityList = NULL
	
		SELECT @cCommunityList = COALESCE(@cCommunityList + ', ', '') + dbo.GetCommunityNameByCommunityID(T1.CommunityID)
		FROM @tTable1 T1
		WHERE T1.ProvinceID = @nProvinceID
			AND T1.CommunityID > 0

		UPDATE T2
		SET T2.CommunityNameList = @cCommunityList
		FROM @tTable2 T2
		WHERE T2.ProvinceID = @nProvinceID

		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	SELECT
		P.ProvinceName,
		T2.CommunityNameList,
		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan(@ConceptNoteID, T2.ProvinceID) AS MapLink
	FROM @tTable2 T2
		JOIN dbo.Province P ON P.ProvinceID = T2.ProvinceID

END
GO
--End procedure reporting.EDPProvincesAndCommunitiesByConceptNoteID