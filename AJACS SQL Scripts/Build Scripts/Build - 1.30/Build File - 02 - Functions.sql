USE AJACS
GO

--Begin function eventlog.GetCommunityXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Community data for a specific PoliceEngagementUpdate record
-- =============================================================================================

CREATE FUNCTION eventlog.GetCommunityXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM policeengagementupdate.Community T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityClass data for a specific PoliceEngagementUpdate record
-- ===============================================================================================================

CREATE FUNCTION eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityClasses VARCHAR(MAX) = ''
	
	SELECT @cCommunityClasses = COALESCE(@cCommunityClasses, '') + D.CommunityClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityClass'), ELEMENTS) AS CommunityClass
		FROM policeengagementupdate.CommunityClass T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityClasses>' + ISNULL(@cCommunityClasses, '') + '</CommunityClasses>'

END
GO
--End function eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID
EXEC utility.DropObject 'eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.25
-- Description:	A function to return CommunityEngagementCriteriaResult data for a specific Community record
-- ========================================================================================================

CREATE FUNCTION eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityEngagementCriteriaResults VARCHAR(MAX) = ''
	
	SELECT @cCommunityEngagementCriteriaResults = COALESCE(@cCommunityEngagementCriteriaResults, '') + D.CommunityEngagementCriteriaResult
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityEngagementCriteriaResult'), ELEMENTS) AS CommunityEngagementCriteriaResult
		FROM dbo.CommunityEngagementCriteriaResult T 
		WHERE T.CommunityID = @CommunityID
		) D

	RETURN '<CommunityEngagementCriteriaResults>' + ISNULL(@cCommunityEngagementCriteriaResults, '') + '</CommunityEngagementCriteriaResults>'

END
GO
--End function eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID

--Begin function eventlog.GetCommunityIndicatorXMLByCommunityID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.25
-- Description:	A function to return CommunityIndicator data for a specific Community record
-- =========================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByCommunityID
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM dbo.CommunityIndicator T 
		WHERE T.CommunityID = @CommunityID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByCommunityID

--Begin function eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityIndicator data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM policeengagementupdate.CommunityIndicator T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRecommendation data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRecommendations VARCHAR(MAX) = ''
	
	SELECT @cCommunityRecommendations = COALESCE(@cCommunityRecommendations, '') + D.CommunityRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRecommendation'), ELEMENTS) AS CommunityRecommendation
		FROM policeengagementupdate.CommunityRecommendation T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityRecommendations>' + ISNULL(@cCommunityRecommendations, '') + '</CommunityRecommendations>'

END
GO
--End function eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityRiskXMLByCommunityID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.25
-- Description:	A function to return CommunityRisk data for a specific Community record
-- ====================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByCommunityID
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM dbo.CommunityRisk T 
		WHERE T.CommunityID = @CommunityID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByCommunityID

--Begin function eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRisk data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM policeengagementupdate.CommunityRisk T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Province data for a specific PoliceEngagementUpdate record
-- =======================================================================================================

CREATE FUNCTION eventlog.GetProvinceXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM policeengagementupdate.Province T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceClass data for a specific PoliceEngagementUpdate record
-- ==============================================================================================================

CREATE FUNCTION eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceClasses VARCHAR(MAX) = ''
	
	SELECT @cProvinceClasses = COALESCE(@cProvinceClasses, '') + D.ProvinceClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceClass'), ELEMENTS) AS ProvinceClass
		FROM policeengagementupdate.ProvinceClass T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceClasses>' + ISNULL(@cProvinceClasses, '') + '</ProvinceClasses>'

END
GO
--End function eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceIndicator data for a specific PoliceEngagementUpdate record
-- ================================================================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM policeengagementupdate.ProvinceIndicator T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'

END
GO
--End function eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRecommendation data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRecommendations VARCHAR(MAX) = ''
	
	SELECT @cProvinceRecommendations = COALESCE(@cProvinceRecommendations, '') + D.ProvinceRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRecommendation'), ELEMENTS) AS ProvinceRecommendation
		FROM policeengagementupdate.ProvinceRecommendation T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceRecommendations>' + ISNULL(@cProvinceRecommendations, '') + '</ProvinceRecommendations>'

END
GO
--End function eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRisk data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM policeengagementupdate.ProvinceRisk T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'

END
GO
--End function eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID

