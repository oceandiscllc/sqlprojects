-- File Name:	Build-1.30 File 01 - AJACS.sql
-- Build Key:	Build-1.30 File 01 - AJACS - 2015.09.28 20.18.56

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID
--		eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID
--		eventlog.GetCommunityIndicatorXMLByCommunityID
--		eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID
--		eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID
--		eventlog.GetCommunityRiskXMLByCommunityID
--		eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID
--		eventlog.GetCommunityXMLByPoliceEngagementUpdateID
--		eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID
--		eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID
--		eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID
--		eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID
--		eventlog.GetProvinceXMLByPoliceEngagementUpdateID
--
-- Procedures:
--		communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
--		dbo.GetClassCommunityNotes
--		dbo.GetClassProvinceNotes
--		dbo.GetCommunityByCommunityID
--		dbo.GetIndicatorCommunityNotes
--		dbo.GetIndicatorProvinceNotes
--		dbo.GetProvinceByProvinceID
--		eventlog.LogCommunityAction
--		eventlog.LogCommunityAssetAction
--		eventlog.LogIncidentAction
--		eventlog.LogPoliceEngagementAction
--		policeengagementupdate.ApprovePoliceEngagementUpdate
--		policeengagementupdate.DeletePoliceEngagementCommunity
--		policeengagementupdate.DeletePoliceEngagementProvince
--		policeengagementupdate.GetCommunityByCommunityID
--		policeengagementupdate.GetProvinceByProvinceID
--		reporting.GetCashHandoverReport
--		reporting.GetWeeklyReportCommunity
--		reporting.GetWeeklyReportProvince
--
-- Tables:
--		dbo.CommunityClass
--		dbo.ProvinceClass
--		policeengagementupdate.CommunityClass
--		policeengagementupdate.ProvinceClass
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'ArabicCommunityName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table dbo.Community

--Begin table dbo.CommunityClass
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityClass'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityClass
	(
	CommunityClassID INT IDENTITY(1,1),
	CommunityID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityClassID'
EXEC utility.SetIndexClustered 'IX_CommunityClass', @TableName, 'CommunityID,ClassID'
GO
--End table dbo.CommunityClass

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'ArabicProvinceName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table dbo.Province

--Begin table dbo.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceClass'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceClass
	(
	ProvinceClassID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceClassID'
EXEC utility.SetIndexClustered 'IX_ProvinceClass', @TableName, 'ProvinceID,ClassID'
GO
--End table dbo.ProvinceClass

--Begin table dropdown.CommunityAssetUnitType
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetUnitType'

EXEC utility.AddColumn @TableName, 'CommunityAssetUnitTypeCode', 'VARCHAR(50)'
GO

UPDATE dropdown.CommunityAssetUnitType
SET CommunityAssetUnitTypeCode = CommunityAssetUnitTypeName
GO
--End table dropdown.CommunityAssetUnitType

--Begin table policeengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Community'

EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Community

--Begin table policeengagementupdate.CommunityClass
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityClass'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityClass
	(
	CommunityClassID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityClassID'
EXEC utility.SetIndexClustered 'IX_CommunityClass', @TableName, 'CommunityID,ClassID'
GO
--End table policeengagementupdate.CommunityClass

--Begin table policeengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Province'

EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Province

--Begin table policeengagementupdate.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceClass'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceClass
	(
	ProvinceClassID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceClassID'
EXEC utility.SetIndexClustered 'IX_ProvinceClass', @TableName, 'ProvinceID,ClassID'
GO
--End table policeengagementupdate.ProvinceClass


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function eventlog.GetCommunityXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Community data for a specific PoliceEngagementUpdate record
-- =============================================================================================

CREATE FUNCTION eventlog.GetCommunityXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM policeengagementupdate.Community T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityClass data for a specific PoliceEngagementUpdate record
-- ===============================================================================================================

CREATE FUNCTION eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityClasses VARCHAR(MAX) = ''
	
	SELECT @cCommunityClasses = COALESCE(@cCommunityClasses, '') + D.CommunityClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityClass'), ELEMENTS) AS CommunityClass
		FROM policeengagementupdate.CommunityClass T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityClasses>' + ISNULL(@cCommunityClasses, '') + '</CommunityClasses>'

END
GO
--End function eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID
EXEC utility.DropObject 'eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.25
-- Description:	A function to return CommunityEngagementCriteriaResult data for a specific Community record
-- ========================================================================================================

CREATE FUNCTION eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityEngagementCriteriaResults VARCHAR(MAX) = ''
	
	SELECT @cCommunityEngagementCriteriaResults = COALESCE(@cCommunityEngagementCriteriaResults, '') + D.CommunityEngagementCriteriaResult
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityEngagementCriteriaResult'), ELEMENTS) AS CommunityEngagementCriteriaResult
		FROM dbo.CommunityEngagementCriteriaResult T 
		WHERE T.CommunityID = @CommunityID
		) D

	RETURN '<CommunityEngagementCriteriaResults>' + ISNULL(@cCommunityEngagementCriteriaResults, '') + '</CommunityEngagementCriteriaResults>'

END
GO
--End function eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID

--Begin function eventlog.GetCommunityIndicatorXMLByCommunityID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.25
-- Description:	A function to return CommunityIndicator data for a specific Community record
-- =========================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByCommunityID
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM dbo.CommunityIndicator T 
		WHERE T.CommunityID = @CommunityID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByCommunityID

--Begin function eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityIndicator data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM policeengagementupdate.CommunityIndicator T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRecommendation data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRecommendations VARCHAR(MAX) = ''
	
	SELECT @cCommunityRecommendations = COALESCE(@cCommunityRecommendations, '') + D.CommunityRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRecommendation'), ELEMENTS) AS CommunityRecommendation
		FROM policeengagementupdate.CommunityRecommendation T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityRecommendations>' + ISNULL(@cCommunityRecommendations, '') + '</CommunityRecommendations>'

END
GO
--End function eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetCommunityRiskXMLByCommunityID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.25
-- Description:	A function to return CommunityRisk data for a specific Community record
-- ====================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByCommunityID
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM dbo.CommunityRisk T 
		WHERE T.CommunityID = @CommunityID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByCommunityID

--Begin function eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRisk data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM policeengagementupdate.CommunityRisk T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Province data for a specific PoliceEngagementUpdate record
-- =======================================================================================================

CREATE FUNCTION eventlog.GetProvinceXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM policeengagementupdate.Province T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceClass data for a specific PoliceEngagementUpdate record
-- ==============================================================================================================

CREATE FUNCTION eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceClasses VARCHAR(MAX) = ''
	
	SELECT @cProvinceClasses = COALESCE(@cProvinceClasses, '') + D.ProvinceClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceClass'), ELEMENTS) AS ProvinceClass
		FROM policeengagementupdate.ProvinceClass T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceClasses>' + ISNULL(@cProvinceClasses, '') + '</ProvinceClasses>'

END
GO
--End function eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceIndicator data for a specific PoliceEngagementUpdate record
-- ================================================================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM policeengagementupdate.ProvinceIndicator T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'

END
GO
--End function eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRecommendation data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRecommendations VARCHAR(MAX) = ''
	
	SELECT @cProvinceRecommendations = COALESCE(@cProvinceRecommendations, '') + D.ProvinceRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRecommendation'), ELEMENTS) AS ProvinceRecommendation
		FROM policeengagementupdate.ProvinceRecommendation T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceRecommendations>' + ISNULL(@cProvinceRecommendations, '') + '</ProvinceRecommendations>'

END
GO
--End function eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID

--Begin function eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRisk data for a specific PoliceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID
(
@PoliceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM policeengagementupdate.ProvinceRisk T 
		WHERE T.PoliceEngagementUpdateID = @PoliceEngagementUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'

END
GO
--End function eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A procedure to approve a Community/Province Engagement Update
-- ==========================================================================
CREATE PROCEDURE communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nCommunityProvinceEngagementUpdateID INT = ISNULL((SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC), 0)
	DECLARE @nContactAffiliationID INT = ISNULL((SELECT CA.ContactAffiliationID FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Community Security Working Groups'), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	UPDATE P
	SET
		P.CAPAgreedDate = CPEU.CAPAgreedDate,
		P.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		P.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		P.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN communityprovinceengagementupdate.Province CPEU ON CPEU.ProvinceID = P.ProvinceID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputProvince O ON O.ProvinceID = C.ProvinceID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode = 'Beneficiary'
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		PC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.ProvinceContact PC

	DELETE FC
	FROM finding.FindingProvince FC
		JOIN @tOutputProvince O ON O.ProvinceID = FC.ProvinceID
	
	INSERT INTO finding.FindingProvince
		(ProvinceID, FindingID)
	SELECT
		PF.ProvinceID,
		PF.FindingID
	FROM communityprovinceengagementupdate.ProvinceFinding PF

	DELETE CI
	FROM dbo.ProvinceIndicator CI
		JOIN @tOutputProvince O ON O.ProvinceID = CI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		CI.ProvinceID,
		CI.IndicatorID,
		CI.CommunityProvinceEngagementAchievedValue, 
		CI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceIndicator CI

	DELETE PC
	FROM project.ProjectProvince PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO project.ProjectProvince
		(ProvinceID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.ProvinceID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceProject CP

	DELETE RC
	FROM recommendation.RecommendationProvince RC
		JOIN @tOutputProvince O ON O.ProvinceID = RC.ProvinceID
	
	INSERT INTO recommendation.RecommendationProvince
		(ProvinceID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		CR.ProvinceID,
		CR.RecommendationID,
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceRecommendation CR

	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		CR.ProvinceID,
		CR.RiskID,
		CR.CommunityProvinceEngagementRiskValue, 
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	DELETE FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

	TRUNCATE TABLE communityprovinceengagementupdate.Province
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceContact
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceFinding
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceProject
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRisk

	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'update', @PersonID, NULL

	UPDATE C
	SET
		C.CAPAgreedDate = CPEU.CAPAgreedDate,
		C.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		C.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		C.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN communityprovinceengagementupdate.Community CPEU ON CPEU.CommunityID = C.CommunityID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputCommunity O ON O.CommunityID = C.CommunityID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode = 'Beneficiary'
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		CC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.CommunityContact CC

	DELETE FC
	FROM finding.FindingCommunity FC
		JOIN @tOutputCommunity O ON O.CommunityID = FC.CommunityID
	
	INSERT INTO finding.FindingCommunity
		(CommunityID, FindingID)
	SELECT
		CF.CommunityID,
		CF.FindingID
	FROM communityprovinceengagementupdate.CommunityFinding CF

	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.CommunityProvinceEngagementAchievedValue, 
		CI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityIndicator CI

	DELETE PC
	FROM project.ProjectCommunity PC
		JOIN @tOutputCommunity O ON O.CommunityID = PC.CommunityID
	
	INSERT INTO project.ProjectCommunity
		(CommunityID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.CommunityID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityProject CP

	DELETE RC
	FROM recommendation.RecommendationCommunity RC
		JOIN @tOutputCommunity O ON O.CommunityID = RC.CommunityID
	
	INSERT INTO recommendation.RecommendationCommunity
		(CommunityID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RecommendationID,
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRecommendation CR

	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.CommunityProvinceEngagementRiskValue, 
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	DELETE FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

	TRUNCATE TABLE communityprovinceengagementupdate.Community
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityContact
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityFinding
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityProject
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRisk

	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'update', @PersonID, NULL

END

GO
--End procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

--Begin procedure dbo.GetClassCommunityNotes
EXEC Utility.DropObject 'dbo.GetClassCommunityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.CommunityClass table
-- ====================================================================================
CREATE PROCEDURE dbo.GetClassCommunityNotes

@CommunityClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CC.PoliceEngagementNotes
	FROM dbo.CommunityClass CC
	WHERE CC.CommunityClassID = @CommunityClassID
	
END
GO
--End procedure dbo.GetClassCommunityNotes

--Begin procedure dbo.GetClassProvinceNotes
EXEC Utility.DropObject 'dbo.GetClassProvinceNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.ProvinceClass table
-- ===============================================================================
CREATE PROCEDURE dbo.GetClassProvinceNotes

@ProvinceClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PC.PoliceEngagementNotes
	FROM dbo.ProvinceClass PC
	WHERE PC.ProvinceClassID = @ProvinceClassID
	
END
GO
--End procedure dbo.GetClassProvinceNotes

--Begin procedure dbo.GetCommunityByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Community table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
--
-- Author:			John Lyons
-- Create date:	2015.09.28
-- Description:	Added the ArabicCommunityName column
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ArabicCommunityName, 
		C.CommunityEngagementStatusID, 
		C.CommunityGroupID, 
		C.CommunityID, 
		C.CommunityName, 
		C.CommunitySubGroupID, 
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyPoints, 
		C.Latitude, 
		C.Longitude,
		C.PolicePresenceCategoryID, 
		C.PolicePresenceStatusID, 
		C.PoliceStationNumber, 
		C.PoliceStationStatusID, 
		C.Population, 
		C.PopulationSource, 
		C.ProgramNotes1, 
		C.ProgramNotes2, 
		C.ProgramNotes3, 
		C.ProgramNotes4, 
		C.ProgramNotes5, 
		C.ProvinceID,
		C.RiskMitigation, 
		C.StatusChangeID, 
		C.Summary, 
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CSG.CommunitySubGroupName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Community', @CommunityID) AS UpdateDateFormatted,
		ID.ImpactDecisionName,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		PPS.PolicePresenceStatusName,
		PSS.PoliceStationStatusName,
		SC.StatusChangeName
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.PolicePresenceStatus PPS ON PPS.PolicePresenceStatusID = C.PolicePresenceStatusID
		JOIN dropdown.PoliceStationStatus PSS ON PSS.PoliceStationStatusID = C.PoliceStationStatusID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
			AND C.CommunityID = @CommunityID

	SELECT
		CECR.CommunityID, 
		CECR.EngagementCriteriaID, 
		CECR.EngagementCriteriaStatusID, 
		CECR.Notes,
		ECS.EngagementCriteriaStatusName,
		ECS.HexColor
	FROM dbo.CommunityEngagementCriteriaResult CECR
		JOIN dropdown.EngagementCriteriaStatus ECS ON ECS.EngagementCriteriaStatusID = CECR.EngagementCriteriaStatusID
			AND CECR.CommunityID = @CommunityID
	ORDER BY CECR.EngagementCriteriaID
	
END
GO
--End procedure dbo.GetCommunityByCommunityID

--Begin procedure dbo.GetIndicatorCommunityNotes
EXEC Utility.DropObject 'dbo.GetIndicatorCommunityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.CommunityIndicator table
-- ====================================================================================
CREATE PROCEDURE dbo.GetIndicatorCommunityNotes

@CommunityIndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CI.CommunityProvinceEngagementAchievedValue,
		CI.CommunityProvinceEngagementNotes,
		CI.PoliceEngagementAchievedValue,
		CI.PoliceEngagementNotes
	FROM dbo.CommunityIndicator CI
	WHERE CI.CommunityIndicatorID = @CommunityIndicatorID
	
END
GO
--End procedure dbo.GetIndicatorCommunityNotes

--Begin procedure dbo.GetIndicatorProvinceNotes
EXEC Utility.DropObject 'dbo.GetIndicatorProvinceNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.ProvinceIndicator table
-- ===================================================================================
CREATE PROCEDURE dbo.GetIndicatorProvinceNotes

@ProvinceIndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PRI.CommunityProvinceEngagementAchievedValue,
		PRI.CommunityProvinceEngagementNotes,
		PRI.PoliceEngagementAchievedValue,
		PRI.PoliceEngagementNotes
	FROM dbo.ProvinceIndicator PRI
	WHERE PRI.ProvinceIndicatorID = @ProvinceIndicatorID
	
END
GO
--End procedure dbo.GetIndicatorProvinceNotes

--Begin procedure dbo.GetProvinceByProvinceID
EXEC Utility.DropObject 'dbo.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.14
-- Description:	A stored procedure to data from the dbo.Province table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
-- ===================================================================
CREATE PROCEDURE dbo.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ID.ImpactDecisionName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Province', @ProvinceID) AS UpdateDateFormatted,
		P.ArabicProvinceName,
		P.ImpactDecisionID,
		P.Implications,
		P.KeyPoints,
		P.ProgramNotes1,
		P.ProgramNotes2,
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID,
		P.Summary,
		SC.StatusChangeName
	FROM dbo.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

	SELECT
		CES.CommunityEngagementStatusName,
		ISNULL(D.CommunityCount, 0) AS CommunityCount
	FROM dropdown.CommunityEngagementStatus CES
	OUTER APPLY
		(
		SELECT
			COUNT(C.CommunityID) AS CommunityCount,
			C.CommunityEngagementStatusID
		FROM dbo.Community C
		WHERE C.CommunityEngagementStatusID = CES.CommunityEngagementStatusID
			AND C.ProvinceID = @ProvinceID
		GROUP BY C.CommunityEngagementStatusID
		) D 
	WHERE CES.CommunityEngagementStatusID > 0
	ORDER BY CES.DisplayOrder, CES.CommunityEngagementStatusName, D.CommunityCount
	
END
GO
--End procedure dbo.GetProvinceByProvinceID

--Begin procedure eventlog.LogCommunityAction
EXEC utility.DropObject 'eventlog.LogCommunityAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:			Todd Pires
-- Create date: 2015.09.25
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytActionTable
		--ENDIF
		
		SELECT *
		INTO #LogCommuniytActionTable
		FROM dbo.Community C
		WHERE C.CommunityID = @EntityID
		
		ALTER TABLE #LogCommuniytActionTable DROP COLUMN Location
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(C.Location AS VARCHAR(MAX)),
			(SELECT CAST(eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID(T.CommunityID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByCommunityID(T.CommunityID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByCommunityID(T.CommunityID) AS XML))
			FOR XML RAW('Community'), ELEMENTS
			)
		FROM #LogCommuniytActionTable T
			JOIN dbo.Community C ON C.CommunityID = T.CommunityID

		DROP TABLE #LogCommuniytActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAction

--Begin procedure eventlog.LogCommunityAssetAction
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.09.25
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytActionTable
		--ENDIF
		
		SELECT *
		INTO #LogCommuniytAssetActionTable
		FROM dbo.CommunityAsset CA
		WHERE CA.CommunityAssetID = @EntityID
		
		ALTER TABLE #LogCommuniytActionTable DROP COLUMN Location

		DECLARE @cCommunityAssetCommunities NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetCommunities = COALESCE(@cCommunityAssetCommunities, '') + D.CommunityAssetCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetCommunity'), ELEMENTS) AS CommunityAssetCommunity
			FROM dbo.CommunityAssetCommunity T 
			WHERE T.CommunityAssetID = @EntityID
			) D
				
		DECLARE @cCommunityAssetProvinces NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetProvinces = COALESCE(@cCommunityAssetProvinces, '') + D.CommunityAssetProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetProvince'), ELEMENTS) AS CommunityAssetProvince
			FROM dbo.CommunityAssetProvince T 
			WHERE T.CommunityAssetID = @EntityID
			) D			

		DECLARE @cCommunityAssetRisks NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetRisks = COALESCE(@cCommunityAssetRisks, '') + D.CommunityAssetRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetRisk'), ELEMENTS) AS CommunityAssetRisk
			FROM dbo.CommunityAssetRisk T 
			WHERE T.CommunityAssetID = @EntityID
			) D	

		DECLARE @cCommunityAssetUnits NVARCHAR(MAX)

		SELECT 
			@cCommunityAssetUnits = COALESCE(@cCommunityAssetUnits, '') + D.CommunityAssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetUnit'), ELEMENTS) AS CommunityAssetUnit
			FROM dbo.CommunityAssetUnit T 
			WHERE T.CommunityAssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(CA.Location AS VARCHAR(MAX)),
			CAST(('<CommunityAssetCommunities>' + ISNULL(@cCommunityAssetCommunities , '') + '</CommunityAssetCommunities>') AS XML), 
			CAST(('<CommunityAssetProvinces>' + ISNULL(@cCommunityAssetProvinces , '') + '</CommunityAssetProvinces>') AS XML),
			CAST(('<CommunityAssetRisks>' + ISNULL(@cCommunityAssetRisks  , '') + '</CommunityAssetRisks>') AS XML),
			CAST(('<CommunityAssetUnits>' + ISNULL(@cCommunityAssetUnits  , '') + '</CommunityAssetUnits>') AS XML)
			FOR XML RAW('CommunityAsset'), ELEMENTS
			)
		FROM #LogCommuniytAssetActionTable T
			JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = T.CommunityAssetID

		DROP TABLE #LogCommuniytAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAssetAction

--Begin procedure eventlog.LogIncidentAction
EXEC utility.DropObject 'eventlog.LogIncidentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.04.18
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIncidentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogIncidentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogIncidentActionTable
		--ENDIF
		
		SELECT *
		INTO #LogIncidentActionTable
		FROM dbo.Incident I
		WHERE I.IncidentID = @EntityID
		
		ALTER TABLE #LogIncidentActionTable DROP COLUMN Location

		DECLARE @cIncidentCommunities VARCHAR(MAX) 
	
		SELECT 
			@cIncidentCommunities = COALESCE(@cIncidentCommunities, '') + D.IncidentCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentCommunity'), ELEMENTS) AS IncidentCommunity
			FROM dbo.IncidentCommunity T 
			WHERE T.IncidentID = @EntityID
			) D

		DECLARE @cIncidentProvinces VARCHAR(MAX) 
	
		SELECT 
			@cIncidentProvinces = COALESCE(@cIncidentProvinces, '') + D.IncidentProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentProvince'), ELEMENTS) AS IncidentProvince
			FROM dbo.IncidentProvince T 
			WHERE T.IncidentID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments,
			(
			SELECT
			T.*, 
			CAST(I.Location AS VARCHAR(MAX)),
			CAST(('<IncidentCommunities>' + ISNULL(@cIncidentCommunities, '') + '</IncidentCommunities>') AS XML),
			CAST(('<IncidentProvinces>' + ISNULL(@cIncidentProvinces, '') + '</IncidentProvinces>') AS XML)
			FOR XML RAW('Incident'), ELEMENTS
			)

		FROM #LogIncidentActionTable T
			JOIN dbo.Incident I ON I.IncidentID = T.IncidentID

		DROP TABLE #LogIncidentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIncidentAction

--Begin procedure eventlog.LogPoliceEngagementAction
EXEC utility.DropObject 'eventlog.LogPoliceEngagementAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPoliceEngagementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML))
			FOR XML RAW('PoliceEngagementUpdate'), ELEMENTS
			)
		FROM communityprovinceengagementupdate.PoliceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PoliceEngagementUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPoliceEngagementAction

--Begin procedure policeengagementupdate.ApprovePoliceEngagementUpdate
EXEC Utility.DropObject 'policeengagementupdate.ApprovePoliceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create date:	2015.09.24
-- Description:	A procedure to approve a Police Engagement Update
-- ==============================================================
CREATE PROCEDURE policeengagementupdate.ApprovePoliceEngagementUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nPoliceEngagementUpdateID INT = ISNULL((SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	UPDATE P
	SET
		P.CapacityAssessmentDate = PEU.CapacityAssessmentDate, 
		P.PoliceEngagementOutput1 = PEU.PoliceEngagementOutput1,
		P.PPPDate = PEU.PPPDate
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN policeengagementupdate.Province PEU ON PEU.ProvinceID = P.ProvinceID
			AND PEU.PoliceEngagementUpdateID = @nPoliceEngagementUpdateID

	DELETE PC
	FROM dbo.ProvinceClass PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO dbo.ProvinceClass
		(ProvinceID, ClassID, PoliceEngagementNotes)
	SELECT
		PC.ProvinceID,
		PC.ClassID,
		PC.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceClass PC

	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.PoliceEngagementAchievedValue, 
		PRI.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceIndicator PRI

	DELETE RP
	FROM recommendation.RecommendationProvince RP
		JOIN @tOutputProvince O ON O.ProvinceID = RP.ProvinceID
	
	INSERT INTO recommendation.RecommendationProvince
		(ProvinceID, RecommendationID, PoliceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RecommendationID,
		PR.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceRecommendation PR

	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.PoliceEngagementRiskValue, 
		PR.PoliceEngagementNotes
	FROM policeengagementupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	DELETE FROM policeengagementupdate.PoliceEngagementUpdate

	TRUNCATE TABLE policeengagementupdate.Province
	TRUNCATE TABLE policeengagementupdate.ProvinceClass
	TRUNCATE TABLE policeengagementupdate.ProvinceIndicator
	TRUNCATE TABLE policeengagementupdate.ProvinceRecommendation
	TRUNCATE TABLE policeengagementupdate.ProvinceRisk

	EXEC eventlog.LogPoliceEngagementAction @nPoliceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogPoliceEngagementAction @nPoliceEngagementUpdateID, 'update', @PersonID, NULL

	UPDATE C
	SET
		C.CapacityAssessmentDate = PEU.CapacityAssessmentDate, 
		C.PoliceEngagementOutput1 = PEU.PoliceEngagementOutput1,
		C.PPPDate = PEU.PPPDate
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community P
		JOIN policeengagementupdate.Community PEU ON PEU.CommunityID = P.CommunityID
			AND PEU.PoliceEngagementUpdateID = @nPoliceEngagementUpdateID

	DELETE CC
	FROM dbo.CommunityClass PC
		JOIN @tOutputCommunity O ON O.CommunityID = PC.CommunityID
	
	INSERT INTO dbo.CommunityClass
		(CommunityID, ClassID, PoliceEngagementNotes)
	SELECT
		PC.CommunityID,
		PC.ClassID,
		PC.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityClass PC

	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.PoliceEngagementAchievedValue, 
		CI.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityIndicator CI

	DELETE RC
	FROM recommendation.RecommendationCommunity RC
		JOIN @tOutputCommunity O ON O.CommunityID = RC.CommunityID
	
	INSERT INTO recommendation.RecommendationCommunity
		(CommunityID, RecommendationID, PoliceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RecommendationID,
		CR.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityRecommendation CR

	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.PoliceEngagementRiskValue, 
		CR.PoliceEngagementNotes
	FROM policeengagementupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	DELETE FROM policeengagementupdate.PoliceEngagementUpdate

	TRUNCATE TABLE policeengagementupdate.Community
	TRUNCATE TABLE policeengagementupdate.CommunityClass
	TRUNCATE TABLE policeengagementupdate.CommunityIndicator
	TRUNCATE TABLE policeengagementupdate.CommunityRecommendation
	TRUNCATE TABLE policeengagementupdate.CommunityRisk

	EXEC eventlog.LogPoliceEngagementAction @nPoliceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogPoliceEngagementAction @nPoliceEngagementUpdateID, 'update', @PersonID, NULL

END

GO
--End procedure policeengagementupdate.ApprovePoliceEngagementUpdate

--Begin procedure policeengagementupdate.DeletePoliceEngagementCommunity
EXEC Utility.DropObject 'policeengagementupdate.DeletePoliceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.24
-- Description:	A stored procedure to delete data from the policeengagementupdate.Community table
-- ==============================================================================================
CREATE PROCEDURE policeengagementupdate.DeletePoliceEngagementCommunity

@CommunityID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)

	DELETE T
	FROM policeengagementupdate.Community T
	WHERE T.CommunityID = @CommunityID
	
	DELETE T
	FROM policeengagementupdate.CommunityClass T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM policeengagementupdate.CommunityIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM policeengagementupdate.CommunityRecommendation T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM policeengagementupdate.CommunityRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
		
	DELETE D
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
	
	DELETE DE
	FROM dbo.DocumentEntity DE 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.Document D
		WHERE D.DocumentID = DE.DocumentID
		)

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.DeletePoliceEngagementCommunity

--Begin procedure policeengagementupdate.DeletePoliceEngagementProvince
EXEC Utility.DropObject 'policeengagementupdate.DeletePoliceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.24
-- Description:	A stored procedure to delete data from the policeengagementupdate.Province table
-- =============================================================================================
CREATE PROCEDURE policeengagementupdate.DeletePoliceEngagementProvince

@ProvinceID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	DELETE T
	FROM policeengagementupdate.Province T
	WHERE T.ProvinceID = @ProvinceID
	
	DELETE T
	FROM policeengagementupdate.ProvinceClass T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM policeengagementupdate.ProvinceIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM policeengagementupdate.ProvinceRecommendation T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM policeengagementupdate.ProvinceRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
		
	DELETE D
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
	
	DELETE DE
	FROM dbo.DocumentEntity DE 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.Document D
		WHERE D.DocumentID = DE.DocumentID
		)

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.DeletePoliceEngagementProvince

--Begin procedure policeengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Community and policeengagementupdate.Community tables
-- =================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CapacityAssessmentDate,
		dbo.FormatDate(C.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.MaterialSupportStatus,
		C.PoliceEngagementOutput1,
		C.PPPDate,
		dbo.FormatDate(C.PPPDate) AS PPPDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CapacityAssessmentDate,
		dbo.FormatDate(C1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C1.MaterialSupportStatus,
		C1.PoliceEngagementOutput1,
		C1.PPPDate,
		dbo.FormatDate(C1.PPPDate) AS PPPDateFormatted,
		C2.CommunityID,
		C2.CommunityName AS EntityName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.CommunityID = @CommunityID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''Class'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.PoliceEngagementAchievedValue,
		OACI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.PoliceEngagementAchievedValue, 
				CI.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(RC.RecommendationCommunityID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.PoliceEngagementRiskValue,
		OACR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''Class'', ' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID,
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.CommunityID = @CommunityID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.CommunityID = @CommunityID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.CommunityID = @CommunityID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetCommunityByCommunityID

--Begin procedure policeengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Province table and policeengagementupdate.Province tables
-- =====================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CapacityAssessmentDate,
		dbo.FormatDate(P.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P.MaterialSupportStatus,
		P.PoliceEngagementOutput1,
		P.PPPDate,
		dbo.FormatDate(P.PPPDate) AS PPPDateFormatted,
		P.ProvinceID,
		P.ProvinceName AS EntityName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CapacityAssessmentDate,
		dbo.FormatDate(P1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P1.MaterialSupportStatus,
		P1.PoliceEngagementOutput1,
		P1.PPPDate,
		dbo.FormatDate(P1.PPPDate) AS PPPDateFormatted,
		P2.ProvinceID,
		P2.ProvinceName AS EntityName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
			AND P1.ProvinceID = @ProvinceID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''Class'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.PoliceEngagementAchievedValue,
		OAPI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.PoliceEngagementAchievedValue, 
				PRI.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(RP.RecommendationProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.PoliceEngagementRiskValue,
		OAPR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID,
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.ProvinceID = @ProvinceID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.ProvinceID = @ProvinceID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.ProvinceID = @ProvinceID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetProvinceByProvinceID


--Begin procedure reporting.GetCashHandoverReport
EXEC Utility.DropObject 'reporting.GetCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2015.09.28
-- Description:	Added the ArabicProvinceName column
-- ==================================================================
CREATE PROCEDURE reporting.GetCashHandoverReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ArabicProvinceName NVARCHAR(250)
	DECLARE @FullName VARCHAR(200) = (SELECT dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst'))
	DECLARE @PaymentMonthYear VARCHAR(20)
	DECLARE @ProvinceName VARCHAR(250)
	DECLARE @RunningCost INT 
	DECLARE @TotalCost INT 
	
	SELECT @RunningCost = SUM(E.RunningCost)
	FROM
		(
		SELECT
			CASE
				WHEN D.CommunityID = 0
				THEN CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('RunningCostProvince', 0)) AS INT)
				ELSE CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('RunningCostCommunity', 0)) AS INT)
			END AS RunningCost
		FROM
			(
			SELECT DISTINCT 
				CSP.CommunityID
			FROM dbo.ContactStipendPayment CSP
				JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
					AND SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
			) D
		) E
	
	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT TOP 1 
		@ArabicProvinceName = P.ArabicProvinceName,
		@PaymentMonthYear = DateName(month , DateAdd(month, CSP.PaymentMonth, 0) - 1) + ' - ' + CAST(CSP.PaymentYear AS CHAR(4)),
		@ProvinceName = P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT
		@ArabicProvinceName AS ArabicProvinceName,
		@FullName AS FullName,
		@PaymentMonthYear AS PaymentMonthYear,
		@ProvinceName AS ProvinceName,
		ISNULL(@TotalCost, 0) AS TotalCost
		
END
GO
--End procedure reporting.GetCashHandoverReport

--Begin procedure reporting.GetWeeklyReportCommunity
EXEC Utility.DropObject 'reporting.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.03.06
-- Description:	Changed the CommunityReportStatusName logic
--
-- Author:			Todd Pires
-- Update date:	2015.05.06
-- Description:	Repointed the reference code at the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Update date:	2015.09.24
-- Description:	Repointed the icons at /i/
-- ================================================================================
CREATE PROCEDURE reporting.GetWeeklyReportCommunity

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		C.WeeklyReportID,	
		reporting.IsDraftReport('WeeklyReport', C.WeeklyReportID) AS IsDraft,
		CES.CommunityEngagementStatusName,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/i/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,

		CASE
			WHEN C.ImpactDecisionID IN (0,1,2,3,5)
			THEN 'Watch'
			WHEN C.ImpactDecisionID IN (4) 
			THEN 'Engage'
			ELSE 'Alert'			
		END AS CommunityReportStatusName	

	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure reporting.GetWeeklyReportCommunity

--Begin procedure reporting.GetWeeklyReportProvince
EXEC Utility.DropObject 'reporting.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.04.27
-- Description:	Added the dbo.FormatProvinceIncidentsForGoogleMap call
--
-- Author:			Todd Pires
-- Update date:	2015.05.06
-- Description:	Repointed the reference code at the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Update date:	2015.09.24
-- Description:	Repointed the icons at /i/
-- ================================================================================
CREATE PROCEDURE reporting.GetWeeklyReportProvince

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceName, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		P.WeeklyReportID,
		reporting.IsDraftReport('WeeklyReport', P.WeeklyReportID) AS IsDraft,
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/i/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,
		dbo.FormatStaticGoogleMapForWeeklyReport(P.ProvinceID) AS MapImage
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
	ORDER BY P.ProvinceName, P.ProvinceID

END
GO
--End procedure reporting.GetWeeklyReportProvince
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.30 File 01 - AJACS - 2015.09.28 20.18.56')
GO
--End build tracking

