USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'ArabicCommunityName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table dbo.Community

--Begin table dbo.CommunityClass
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityClass'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityClass
	(
	CommunityClassID INT IDENTITY(1,1),
	CommunityID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityClassID'
EXEC utility.SetIndexClustered 'IX_CommunityClass', @TableName, 'CommunityID,ClassID'
GO
--End table dbo.CommunityClass

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'ArabicProvinceName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table dbo.Province

--Begin table dbo.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceClass'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceClass
	(
	ProvinceClassID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceClassID'
EXEC utility.SetIndexClustered 'IX_ProvinceClass', @TableName, 'ProvinceID,ClassID'
GO
--End table dbo.ProvinceClass

--Begin table dropdown.CommunityAssetUnitType
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetUnitType'

EXEC utility.AddColumn @TableName, 'CommunityAssetUnitTypeCode', 'VARCHAR(50)'
GO

UPDATE dropdown.CommunityAssetUnitType
SET CommunityAssetUnitTypeCode = CommunityAssetUnitTypeName
GO
--End table dropdown.CommunityAssetUnitType

--Begin table policeengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Community'

EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Community

--Begin table policeengagementupdate.CommunityClass
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityClass'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityClass
	(
	CommunityClassID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityClassID'
EXEC utility.SetIndexClustered 'IX_CommunityClass', @TableName, 'CommunityID,ClassID'
GO
--End table policeengagementupdate.CommunityClass

--Begin table policeengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Province'

EXEC utility.AddColumn @TableName, 'MaterialSupportStatus', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput1', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Province

--Begin table policeengagementupdate.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceClass'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceClass
	(
	ProvinceClassID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	ClassID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceClassID'
EXEC utility.SetIndexClustered 'IX_ProvinceClass', @TableName, 'ProvinceID,ClassID'
GO
--End table policeengagementupdate.ProvinceClass

