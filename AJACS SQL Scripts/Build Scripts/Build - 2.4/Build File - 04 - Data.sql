USE AJACS
GO

--Begin table dropdown.Stipend
UPDATE S
SET S.StipendCategory = 
	CASE
		WHEN StipendTypeCode = 'PoliceStipend' AND StipendAmount = 400
		THEN 'Command'
		WHEN StipendTypeCode = 'PoliceStipend' AND StipendAmount = 300
		THEN 'Commissioned Officer'
		WHEN StipendTypeCode = 'PoliceStipend' AND StipendAmount = 150
		THEN 'Non Commissioned Officer'
		WHEN StipendTypeCode = 'PoliceStipend' AND StipendAmount = 100
		THEN 'Police'
		ELSE StipendCategory
	END

FROM dropdown.Stipend S	
--End table dropdown.Stipend
