USE AJACS
GO

--Begin function dbo.GetLocationByContactID
EXEC utility.DropObject 'dbo.GetLocationByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2016.12.31
-- Description:	A function to get the location of a contact
-- ========================================================

CREATE FUNCTION dbo.GetLocationByContactID
(
@ContactID INT
)

RETURNS NVARCHAR(550)

AS
BEGIN

	DECLARE @cLocation NVARCHAR(550) = ''
	DECLARE @nAssetUnitID INT = 0
	DECLARE @nCommunityID INT = 0

	SELECT
		@nAssetUnitID = C.AssetUnitID,
		@nCommunityID = C.CommunityID
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF @nAssetUnitID > 0
		SELECT @cLocation = AU.AssetUnitName + ' (Department)' FROM asset.AssetUnit AU WHERE AU.AssetUnitID = @nAssetUnitID
	ELSE IF @nCommunityID > 0
		SELECT @cLocation = C.CommunityName + ' (Community)' FROM dbo.Community C WHERE C.CommunityID = @nCommunityID
	--ENDIF
	
	RETURN @cLocation
END
GO
--End function dbo.GetLocationByContactID

--Begin function dbo.IsAssetUnitCommunityStipendEligible
EXEC utility.DropObject 'dbo.IsAssetUnitCommunityStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if a community associated with an asset unit (via an asset) is stipend eligible
-- ====================================================================================================================

CREATE FUNCTION dbo.IsAssetUnitCommunityStipendEligible
(
@AssetUnitID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 0
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Community C ON C.ImpactDecisionID = ID.ImpactDecisionID
		JOIN asset.Asset A ON A.CommunityID = C.CommunityID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
			AND AU.AssetUnitID = @AssetUnitID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsAssetUnitCommunityStipendEligible

--Begin function dbo.IsCommunityStipendEligible
EXEC utility.DropObject 'dbo.IsCommunityStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to determine if a community is stipend eligible
-- =======================================================================

CREATE FUNCTION dbo.IsCommunityStipendEligible
(
@CommunityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 0
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Community C ON C.ImpactDecisionID = ID.ImpactDecisionID
			AND C.CommunityID = @CommunityID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsCommunityStipendEligible

--Begin function procurement.GetDistributedInventoryQuantityAvailable
EXEC utility.DropObject 'procurement.GetDistributedInventoryQuantityAvailable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.28
-- Description:	A function to get the quantity available for a DistributedInventory item
--
-- Author:			Todd Pires
-- Create date:	2016.10.24
-- Description:	Bug fix
--
-- Author:			Todd Pires
-- Create date:	2016.12.21
-- Description:	Bug fix
-- =====================================================================================

CREATE FUNCTION procurement.GetDistributedInventoryQuantityAvailable
(
@EquipmentDistributionID INT,
@EquipmentInventoryID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0

	SELECT @nReturn = EI.Quantity - 
										EI.QuantityDistributed - 
										--(
										--SELECT ISNULL(SUM(DI.Quantity), 0)
										--FROM procurement.DistributedInventory DI 
										--	JOIN procurement.EquipmentDistribution ED ON ED.EquipmentDistributionID = DI.EquipmentDistributionID 
										--		AND ED.IsActive = 0
										--		AND DI.EquipmentInventoryID = @EquipmentInventoryID
										--) - 
										(
										SELECT ISNULL(SUM(DI.Quantity), 0)
										FROM procurement.DistributedInventory DI 
										WHERE DI.EquipmentDistributionID = @EquipmentDistributionID
											AND DI.EquipmentInventoryID = @EquipmentInventoryID
										)
	FROM procurement.EquipmentInventory EI
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
	
	RETURN @nReturn
END
GO
--End function procurement.GetDistributedInventoryQuantityAvailable