-- File Name:	Build-1.42 File 01 - AJACS.sql
-- Build Key:	Build-1.42 File 01 - AJACS - 2015.12.18 21.00.26

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		procurement.GetDistributedEquipmentInventoryCount
--		procurement.GetEquipmentVarianceCount
--		procurement.GetObligatedEquipmentInventoryCount
--		procurement.GetPendingEquipmentInventoryCount
--		procurement.GetPurchasedEquipmentInventoryCount
--
-- Procedures:
--		dbo.GetContactByContactID
--		eventlog.LogPermissionableTemplateAction
--		eventlog.LogServerSetupAction
--		procurement.GetEquipmentCatalogByEquipmentCatalogID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table procurement.LicenseEquipmentCatalog 
DECLARE @TableName VARCHAR(250) = 'procurement.LicenseEquipmentCatalog '

EXEC utility.DropColumn @TableName, 'Distributed'
EXEC utility.DropColumn @TableName, 'Pending'
EXEC utility.DropColumn @TableName, 'Purchased'
EXEC utility.DropColumn @TableName, 'Obligated'
GO
--End table procurement.LicenseEquipmentCatalog
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function procurement.GetDistributedEquipmentInventoryCount
EXEC utility.DropObject 'procurement.GetDistributedEquipmentInventoryCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create Date:	2015.12.16
-- Description:	A function to return the count of distributed Equipment Inventory Items
-- ====================================================================================

CREATE FUNCTION procurement.GetDistributedEquipmentInventoryCount
(
@EquipmentCatalogID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCount INT

	SELECT @nCount = 
		(
			(
			SELECT 
				COUNT(CEI.CommunityEquipmentInventoryID) 
			FROM procurement.CommunityEquipmentInventory CEI 
				JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CEI.EquipmentInventoryID 
					AND EI.EquipmentCatalogID = @EquipmentCatalogID
			)
			+	
			(
			SELECT 
				COUNT(PEI.ProvinceEquipmentInventoryID) 
			FROM procurement.ProvinceEquipmentInventory PEI 
				JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = PEI.EquipmentInventoryID 
					AND EI.EquipmentCatalogID = @EquipmentCatalogID
			) 
		)
	
	RETURN ISNULL(@nCount, 0)
	
END
GO
--End function procurement.GetDistributedEquipmentInventoryCount

--Begin function procurement.GetEquipmentVarianceCount
EXEC utility.DropObject 'procurement.GetEquipmentVarianceCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2015.12.16
-- Description:	A function to return the count of the difference between the License Quantity Authorized and count of Inventory Items distributed / purchased / obligated / pending
-- ================================================================================================================================================================================

CREATE FUNCTION procurement.GetEquipmentVarianceCount
(
@EquipmentCatalogID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCount INT

	SELECT @nCount = 
		(
			(
			SELECT
				SUM(LEC.QuantityAuthorized)
			FROM procurement.LicenseEquipmentCatalog LEC
			WHERE LEC.EquipmentCatalogID = @EquipmentCatalogID			
			)
			- 
			(
			SELECT procurement.GetPendingEquipmentInventoryCount(@EquipmentCatalogID)
			)
			- 
			(
			SELECT procurement.GetObligatedEquipmentInventoryCount(@EquipmentCatalogID)
			)
			- 
			(
			SELECT procurement.GetPurchasedEquipmentInventoryCount(@EquipmentCatalogID)
			)
			- 
			(
			SELECT procurement.GetDistributedEquipmentInventoryCount(@EquipmentCatalogID)
			)
		)
	
	RETURN ISNULL(@nCount, 0)
	
END
GO
--End function procurement.GetEquipmentVarianceCount

--Begin function procurement.GetObligatedEquipmentInventoryCount
EXEC utility.DropObject 'procurement.GetObligatedEquipmentInventoryCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create Date:	2015.12.16
-- Description:	A function to return the count of Obligated Equipment Inventory Items
-- ==================================================================================

CREATE FUNCTION procurement.GetObligatedEquipmentInventoryCount
(
@EquipmentCatalogID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCount INT

	SELECT @nCount = 
		(
			(
			SELECT 
				SUM(PRCNEC.Quantity) 
			FROM procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC
				JOIN dbo.ConceptNoteEquipmentCatalog CNEC ON CNEC.ConceptNoteEquipmentCatalogID = PRCNEC.ConceptNoteEquipmentCatalogID
					AND CNEC.EquipmentCatalogID = @EquipmentCatalogID
			)
			- 
			(
			SELECT procurement.GetPurchasedEquipmentInventoryCount(@EquipmentCatalogID)
			)
			- 
			(
			SELECT procurement.GetDistributedEquipmentInventoryCount(@EquipmentCatalogID)
			)
		)
	
	RETURN ISNULL(@nCount, 0)
	
END
GO
--End function procurement.GetObligatedEquipmentInventoryCount

--Begin function procurement.GetPendingEquipmentInventoryCount
EXEC utility.DropObject 'procurement.GetPendingEquipmentInventoryCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create Date:	2015.12.16
-- Description:	A function to return the count of Pending Equipment Inventory Items
-- ================================================================================

CREATE FUNCTION procurement.GetPendingEquipmentInventoryCount
(
@EquipmentCatalogID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCount INT

	SELECT @nCount = 
		(
			(
			SELECT 
				SUM(CNEI.Quantity) 
			FROM dbo.ConceptNoteEquipmentCatalog CNEI
			WHERE CNEI.EquipmentCatalogID = @EquipmentCatalogID
			)
			- 
			(
			SELECT procurement.GetObligatedEquipmentInventoryCount(@EquipmentCatalogID)
			)
			- 
			(
			SELECT procurement.GetPurchasedEquipmentInventoryCount(@EquipmentCatalogID)
			)
			- 
			(
			SELECT procurement.GetDistributedEquipmentInventoryCount(@EquipmentCatalogID)
			)
		)
	
	RETURN ISNULL(@nCount, 0)
	
END
GO
--End function procurement.GetPendingEquipmentInventoryCount

--Begin function procurement.GetPurchasedEquipmentInventoryCount
EXEC utility.DropObject 'procurement.GetPurchasedEquipmentInventoryCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create Date:	2015.12.16
-- Description:	A function to return the count of Purchased Equipment Inventory Items
-- ==================================================================================

CREATE FUNCTION procurement.GetPurchasedEquipmentInventoryCount
(
@EquipmentCatalogID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCount INT

	SELECT @nCount = 
		(
			(
			SELECT 
				COUNT(EI.EquipmentInventoryID) 
			FROM procurement.EquipmentInventory EI 
			WHERE EI.EquipmentCatalogID = @EquipmentCatalogID
			)
			- 
			(
			SELECT procurement.GetDistributedEquipmentInventoryCount(@EquipmentCatalogID)
			)
		)
	
	RETURN ISNULL(@nCount, 0)
	
END
GO
--End function procurement.GetPurchasedEquipmentInventoryCount


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.ArabicMotherName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C1.CommunityAssetID,
		C1.CommunityAssetUnitID,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		C7.CountryID AS PlaceOfBirthCountryID,
		C7.CountryName AS PlaceOfBirthCountryName,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID
	ORDER BY CNC.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure eventlog.LogPermissionableTemplateAction
EXEC utility.DropObject 'eventlog.LogPermissionableTemplateAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPermissionableTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PermissionableTemplate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PermissionableTemplate',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('PermissionableTemplate'), ELEMENTS
			)
		FROM permissionable.PermissionableTemplate T 
		WHERE T.PermissionableTemplateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPermissionableTemplateAction

--Begin procedure eventlog.LogServerSetupAction
EXEC utility.DropObject 'eventlog.LogServerSetupAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogServerSetupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('ServerSetup'), ELEMENTS
			)
		FROM dbo.ServerSetup T 
		WHERE T.ServerSetupID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogServerSetupAction

--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.17
-- Description:	Added Risk, Impact, and Mitigation Notes fields
--
-- Author:			Todd Pires
-- Update date:	2015.12.15
-- Description:	Added LicenseEquipmentCatalog recordset
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.EquipmentCatalogID, 
		ISNULL(EC.Impact, 1) AS Impact,
		EC.IsCommon,
		EC.ItemMake,
		EC.ItemModel,
		EC.ItemName, 
		EC.MitigationNotes, 
		EC.Notes, 
		EC.QuantityOfIssue,
		ISNULL(EC.Risk, 1) AS Risk,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.UnitOfIssue, 
		ECC.EquipmentCatalogCategoryID, 
		ECC.EquipmentCatalogCategoryName, 
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentCatalog') AS EntityTypeName
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.EquipmentCatalogCategory ECC ON ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
	
	SELECT
		L.LicenseNumber,
		dbo.FormatDate(L.StartDate) AS StartDateFormatted,
		dbo.FormatDate(L.EndDate) AS EndDateFormatted
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND LEC.EquipmentCatalogID = @EquipmentCatalogID
	ORDER BY L.StartDate, L.EndDate, L.LicenseNumber
	
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'FIF', 'Formalized Institutional Frameworks'
EXEC utility.EntityTypeAddUpdate 'Login', 'Login'
EXEC utility.EntityTypeAddUpdate 'ServerSetup', 'Server Setup Key'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='FIF', @NewMenuItemLink='/fif/addupdate', @NewMenuItemText='Formalized Institutional Frameworks', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='PoliceEngagementUpdate', @PermissionableLineageList='FIF.WorkflowStepID%'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
--End table dbo.MenuItem

--Begin table dropdown.Component
IF NOT EXISTS (SELECT 1 FROM dropdown.Component C WHERE C.ComponentAbbreviation = 'RAP')
	BEGIN
	
	INSERT INTO dropdown.Component
		(ComponentAbbreviation, ComponentName,DisplayOrder)
	VALUES
		('RAP', 'Rapid Assessment Program', (SELECT MAX(C.DisplayOrder) + 1 FROM dropdown.Component C))
		
	END
--ENDIF
GO
--End table dropdown.Component

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'FIF')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('FIF',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'FIF'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'FIF'),
		WSWA.WorkflowStepNumber,
		WSWA.WorkflowActionID,
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
	
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'FIF')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('FIF','Formalized Institutional Frameworks', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'FIF'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('FIF')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'FIF')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'FIF','Formalized Institutional Frameworks')
		
	END
--ENDIF
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'FIF'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'FIF'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'FIF'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.42 File 01 - AJACS - 2015.12.18 21.00.26')
GO
--End build tracking

