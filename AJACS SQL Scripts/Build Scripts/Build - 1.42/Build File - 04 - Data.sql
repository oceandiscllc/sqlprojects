USE AJACS
GO

--Begin table dbo.EntityType
--End table dbo.EntityType

--Begin table dbo.MenuItem
--End table dbo.MenuItem

--Begin table dropdown.Component
DELETE C
FROM dropdown.Component C
WHERE C.ComponentAbbreviation = 'RAP'
GO
--End table dropdown.Component

--Begin table dropdown.ConceptNoteType
IF NOT EXISTS (SELECT 1 FROM dropdown.ConceptNoteType C WHERE C.ConceptNoteTypeName = 'Rapid Assessment Program')
	BEGIN
	
	INSERT INTO dropdown.ConceptNoteType
		(ConceptNoteTypeName,DisplayOrder)
	VALUES
		('Rapid Assessment Program', (SELECT MAX(C.DisplayOrder) + 1 FROM dropdown.ConceptNoteType C))
		
	END
--ENDIF
GO
--End table dropdown.ConceptNoteType

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
