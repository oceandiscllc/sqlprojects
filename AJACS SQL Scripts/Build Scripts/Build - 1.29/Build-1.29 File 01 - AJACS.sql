-- File Name:	Build-1.29 File 01 - AJACS.sql
-- Build Key:	Build-1.29 File 01 - AJACS - 2015.09.22 19.23.50

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID
--		eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID
--
-- Procedures:
--		communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity
--		communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince
--		communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
--		communityprovinceengagementupdate.DeleteCommunityProvinceEngagementCommunity
--		communityprovinceengagementupdate.DeleteCommunityProvinceEngagementProvince
--		communityprovinceengagementupdate.GetCommunityByCommunityID
--		communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate
--		communityprovinceengagementupdate.GetProvinceByProvinceID
--		dbo.GetCommunityAssetLocations
--		dbo.GetCommunityLocations
--		dbo.GetContactByContactID
--		dbo.GetIncidentLocations
--		dbo.GetIndicatorCommunityNotes
--		dbo.GetIndicatorProvinceNotes
--		dbo.GetMenuItemsByPersonID
--		dbo.GetRiskCommunityNotes
--		dbo.GetRiskDescription
--		dbo.GetRiskProvinceNotes
--		dropdown.GetComponentReportingAssociationData
--		dropdown.GetTORMOUStatusData
--		eventlog.LogCommunityProvinceEngagementAction
--		eventlog.LogPoliceEngagementAction
--		logicalframework.GetIntermediateOutcomeChartData
--		logicalframework.GetObjectiveByObjectiveID
--		policeengagementupdate.AddPoliceEngagementCommunity
--		policeengagementupdate.AddPoliceEngagementProvince
--		policeengagementupdate.GetCommunityByCommunityID
--		policeengagementupdate.GetPoliceEngagementUpdate
--		policeengagementupdate.GetProvinceByProvinceID
--		project.GetProjectCommunityNotes
--		project.GetProjectProvinceNotes
--		recommendation.GetRecommendationCommunityNotes
--		recommendation.GetRecommendationProvinceNotes
--		utility.EntityTypeAddUpdate
--		workflow.CanIncrementCommunityProvinceEngagementUpdateWorkflow
--		workflow.CanIncrementPoliceEngagementUpdateWorkflow
--		workflow.GetCommunityProvinceEngagementUpdateWorkflowData
--		workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople
--		workflow.GetPoliceEngagementUpdateWorkflowData
--		workflow.GetPoliceEngagementUpdateWorkflowStepPeople
--
-- Schemas:
--		communityprovinceengagementupdate
--		policeengagementupdate
--
-- Tables:
--		communityprovinceengagementupdate.Community
--		communityprovinceengagementupdate.CommunityContact
--		communityprovinceengagementupdate.CommunityFinding
--		communityprovinceengagementupdate.CommunityIndicator
--		communityprovinceengagementupdate.CommunityProject
--		communityprovinceengagementupdate.CommunityProvinceEngagementUpdate
--		communityprovinceengagementupdate.CommunityRecommendation
--		communityprovinceengagementupdate.CommunityRisk
--		communityprovinceengagementupdate.Province
--		communityprovinceengagementupdate.ProvinceContact
--		communityprovinceengagementupdate.ProvinceFinding
--		communityprovinceengagementupdate.ProvinceIndicator
--		communityprovinceengagementupdate.ProvinceProject
--		communityprovinceengagementupdate.ProvinceRecommendation
--		communityprovinceengagementupdate.ProvinceRisk
--		dbo.CommunityIndicator
--		dbo.CommunityRisk
--		dbo.ProvinceIndicator
--		dbo.ProvinceRisk
--		dropdown.ComponentReportingAssociation
--		dropdown.TORMOUStatus
--		policeengagementupdate.Community
--		policeengagementupdate.CommunityIndicator
--		policeengagementupdate.CommunityRecommendation
--		policeengagementupdate.CommunityRisk
--		policeengagementupdate.PoliceEngagementUpdate
--		policeengagementupdate.Province
--		policeengagementupdate.ProvinceIndicator
--		policeengagementupdate.ProvinceRecommendation
--		policeengagementupdate.ProvinceRisk
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin schema communityprovinceengagementupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'communityprovinceengagementupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA communityprovinceengagementupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema communityprovinceengagementupdate

--Begin schema policeengagementupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'policeengagementupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA policeengagementupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema policeengagementupdate

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table communityprovinceengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.Community
	(
	CommunityID INT,
	CommunityProvinceEngagementUpdateID INT,
	CommunityEngagementOutput1 VARCHAR(MAX),
	CommunityEngagementOutput2 VARCHAR(MAX),
	CommunityEngagementOutput3 VARCHAR(MAX),
	CommunityEngagementOutput4 VARCHAR(MAX),
	CAPAgreedDate DATE,
	LastNeedsAssessmentDate DATE,
	TORMOUStatusID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table communityprovinceengagementupdate.Community

--Begin table communityprovinceengagementupdate.CommunityContact
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityContact'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityContact
	(
	CommunityContactID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityContactID'
EXEC utility.SetIndexClustered 'IX_CommunityContact', @TableName, 'CommunityID,ContactID'
GO
--End table communityprovinceengagementupdate.CommunityContact

--Begin table communityprovinceengagementupdate.CommunityFinding
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityFinding'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityFinding
	(
	CommunityFindingID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	FindingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityFindingID'
EXEC utility.SetIndexClustered 'IX_CommunityFinding', @TableName, 'CommunityID,FindingID'
GO
--End table communityprovinceengagementupdate.CommunityFinding

--Begin table communityprovinceengagementupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table communityprovinceengagementupdate.CommunityIndicator

--Begin table communityprovinceengagementupdate.CommunityProject
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityProject'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityProject
	(
	CommunityProjectID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	ProjectID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityProjectID'
EXEC utility.SetIndexClustered 'IX_CommunityProject', @TableName, 'CommunityID,ProjectID'
GO
--End table communityprovinceengagementupdate.CommunityProject

--Begin table communityprovinceengagementupdate.CommunityProvinceEngagementUpdate
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityProvinceEngagementUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityProvinceEngagementUpdate
	(
	CommunityProvinceEngagementUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityProvinceEngagementUpdateID'
GO
--End table communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

--Begin table communityprovinceengagementupdate.CommunityRecommendation
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityRecommendation
	(
	CommunityRecommendationID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	RecommendationID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRecommendationID'
EXEC utility.SetIndexClustered 'IX_CommunityRecommendation', @TableName, 'CommunityID,RecommendationID'
GO
--End table communityprovinceengagementupdate.CommunityRecommendation

--Begin table communityprovinceengagementupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table communityprovinceengagementupdate.CommunityRisk

--Begin table communityprovinceengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.Province
	(
	ProvinceID INT,
	CommunityProvinceEngagementUpdateID INT,
	CommunityEngagementOutput1 VARCHAR(MAX),
	CommunityEngagementOutput2 VARCHAR(MAX),
	CommunityEngagementOutput3 VARCHAR(MAX),
	CommunityEngagementOutput4 VARCHAR(MAX),
	CAPAgreedDate DATE,
	LastNeedsAssessmentDate DATE,
	TORMOUStatusID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table communityprovinceengagementupdate.Province

--Begin table communityprovinceengagementupdate.ProvinceContact
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceContact'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceContact
	(
	ProvinceContactID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceContactID'
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetIndexClustered 'IX_ProvinceContact', @TableName, 'ProvinceID,ContactID'
GO
--End table communityprovinceengagementupdate.ProvinceContact

--Begin table communityprovinceengagementupdate.ProvinceFinding
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceFinding'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceFinding
	(
	ProvinceFindingID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	FindingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceFindingID'
EXEC utility.SetIndexClustered 'IX_ProvinceFinding', @TableName, 'ProvinceID,FindingID'
GO
--End table communityprovinceengagementupdate.ProvinceFinding

--Begin table communityprovinceengagementupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table communityprovinceengagementupdate.ProvinceIndicator

--Begin table communityprovinceengagementupdate.ProvinceProject
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceProject'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceProject
	(
	ProvinceProjectID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	ProjectID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceProjectID'
EXEC utility.SetIndexClustered 'IX_ProvinceProject', @TableName, 'ProvinceID,ProjectID'
GO
--End table communityprovinceengagementupdate.ProvinceProject

--Begin table communityprovinceengagementupdate.ProvinceRecommendation
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceRecommendation
	(
	ProvinceRecommendationID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	RecommendationID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRecommendationID'
EXEC utility.SetIndexClustered 'IX_ProvinceRecommendation', @TableName, 'ProvinceID,RecommendationID'
GO
--End table communityprovinceengagementupdate.ProvinceRecommendation

--Begin table communityprovinceengagementupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table communityprovinceengagementupdate.ProvinceRisk

--Begin table dbo.Community
DECLARE @ObjectName VARCHAR(250) = 'dbo.TR_CommunityLocation'
EXEC utility.DropObject @ObjectName
GO

DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'ArabicCommunityName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'CapacityAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CAPAgreedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput1', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput2', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput3', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput4', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'LastNeedsAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'Location', 'GEOMETRY'
EXEC utility.AddColumn @TableName, 'PPPDate', 'DATE'
EXEC utility.AddColumn @TableName, 'TORMOUStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
GO

UPDATE dbo.Community
SET Location = GEOMETRY::STPointFromText('POINT(' + CAST(Longitude AS VARCHAR(20)) + ' ' + CAST(Latitude AS VARCHAR(20)) + ')', 4326)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER dbo.TR_CommunityLocation ON dbo.Community AFTER INSERT, UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE C
	SET C.Location = GEOMETRY::STPointFromText('POINT(' + CAST(inserted.Longitude AS VARCHAR(20)) + ' ' + CAST(inserted.Latitude AS VARCHAR(20)) + ')', 4326)
	FROM dbo.Community C
		JOIN INSERTED ON INSERTED.CommunityID = C.CommunityID

END
GO
--End table dbo.Community

--Begin table dbo.CommunityAsset
EXEC utility.AddColumn 'dbo.CommunityAsset', 'tmpLocation', 'GEOMETRY'
GO

UPDATE dbo.CommunityAsset
SET tmpLocation = GEOMETRY::STGeomFromText(Location.STAsText(), 4326)
GO

EXEC utility.DropColumn 'dbo.CommunityAsset', 'Location'
GO

EXEC sp_RENAME 'CommunityAsset.tmpLocation' , 'Location', 'COLUMN'
GO
--End table dbo.CommunityAsset

--Begin table dbo.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1),
	CommunityID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table dbo.CommunityIndicator

--Begin table dbo.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1),
	CommunityID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table dbo.CommunityRisk

--Begin table dbo.Incident
DECLARE @ObjectName VARCHAR(250) = 'dbo.TR_IncidentLocation'
EXEC utility.DropObject @ObjectName
GO

EXEC utility.AddColumn 'dbo.Incident', 'Location', 'GEOMETRY'
GO

UPDATE dbo.Incident
SET Location = GEOMETRY::STPointFromText('POINT(' + CAST(Longitude AS VARCHAR(20)) + ' ' + CAST(Latitude AS VARCHAR(20)) + ')', 4326)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER dbo.TR_IncidentLocation ON dbo.Incident AFTER INSERT, UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE I
	SET I.Location = GEOMETRY::STPointFromText('POINT(' + CAST(inserted.Longitude AS VARCHAR(20)) + ' ' + CAST(inserted.Latitude AS VARCHAR(20)) + ')', 4326)
	FROM dbo.Incident I
		JOIN INSERTED ON INSERTED.IncidentID = I.IncidentID

END
GO
--End table dbo.Incident

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'ArabicProvinceName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'CapacityAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CAPAgreedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput1', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput2', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput3', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput4', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'LastNeedsAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PPPDate', 'DATE'
EXEC utility.AddColumn @TableName, 'TORMOUStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
GO
--End table dbo.Province

--Begin table dbo.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1),
	ProvinceID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table dbo.ProvinceIndicator

--Begin table dbo.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1),
	ProvinceID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table dbo.ProvinceRisk

--Begin table dropdown.ComponentReportingAssociation
DECLARE @TableName VARCHAR(250) = 'dropdown.ComponentReportingAssociation'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ComponentReportingAssociation
	(
	ComponentReportingAssociationID INT IDENTITY(0,1) NOT NULL,
	ComponentReportingAssociationCode VARCHAR(50),
	ComponentReportingAssociationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ComponentReportingAssociationID'
EXEC utility.SetIndexNonClustered 'IX_LogicalFrameworkStatus', @TableName, 'DisplayOrder,ComponentReportingAssociationName', 'ComponentReportingAssociationID'
GO

SET IDENTITY_INSERT dropdown.ComponentReportingAssociation ON
GO

INSERT INTO dropdown.ComponentReportingAssociation (ComponentReportingAssociationID) VALUES (0)

SET IDENTITY_INSERT dropdown.ComponentReportingAssociation OFF
GO

INSERT INTO dropdown.ComponentReportingAssociation 
	(ComponentReportingAssociationCode,ComponentReportingAssociationName,DisplayOrder)
VALUES
	('CEO1', 'Community Engagement Output 1', 1),
	('CEO2', 'Community Engagement Output 2', 2),
	('CEO3', 'Community Engagement Output 3', 3),
	('CEO4', 'Community Engagement Output 4', 4),
	('PEO1', 'Police Engagement Output 1', 5),
	('PEO2', 'Police Engagement Output 2', 6),
	('PEO3', 'Police Engagement Output 3', 7),
	('PEO4', 'Police Engagement Output 4', 8),
	('JO1', 'Justice Output 1', 9),
	('JO2', 'Justice Output 2', 10),
	('JO3', 'Justice Output 3', 11),
	('JO4', 'Justice Output 4', 12),
	('FIFO1', 'FIF Output 1', 13),
	('FIFO2', 'FIF Output 2', 14),
	('FIFO3', 'FIF Output 3', 15),
	('FIFO4', 'FIF Output 4', 16)
GO
--End table dropdown.ComponentReportingAssociation

--Begin table dropdown.TORMOUStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.TORMOUStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TORMOUStatus
	(
	TORMOUStatusID INT IDENTITY(0,1) NOT NULL,
	TORMOUStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'TORMOUStatusID'
EXEC utility.SetIndexNonClustered 'IX_LogicalFrameworkStatus', @TableName, 'DisplayOrder,TORMOUStatusName', 'TORMOUStatusID'
GO

SET IDENTITY_INSERT dropdown.TORMOUStatus ON
GO

INSERT INTO dropdown.TORMOUStatus (TORMOUStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.TORMOUStatus OFF
GO

INSERT INTO dropdown.TORMOUStatus 
	(TORMOUStatusName,DisplayOrder)
VALUES
	('Planned', 1),
	('In Progress', 2),
	('Achieved', 3)
GO
--End table dropdown.TORMOUStatus

--Begin table dropdown.ZoneType
UPDATE dropdown.ZoneType 
SET HexColor = UPPER(HexColor)
GO
--End table dropdown.ZoneType

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.AddColumn @TableName, 'ComponentReportingAssociationID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ComponentReportingAssociationID', 'INT', 0
GO
--End table logicalframework.Objective

--Begin table policeengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.Community
	(
	CommunityID INT,
	PoliceEngagementUpdateID INT,
	CapacityAssessmentDate DATE,
	PPPDate DATE,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table policeengagementupdate.Community

--Begin table policeengagementupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table policeengagementupdate.CommunityIndicator

--Begin table policeengagementupdate.CommunityRecommendation
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityRecommendation
	(
	CommunityRecommendationID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	RecommendationID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRecommendationID'
EXEC utility.SetIndexClustered 'IX_CommunityRecommendation', @TableName, 'CommunityID,RecommendationID'
GO
--End table policeengagementupdate.CommunityRecommendation

--Begin table policeengagementupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	RiskID INT,
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table policeengagementupdate.CommunityRisk

--Begin table policeengagementupdate.PoliceEngagementUpdate
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.PoliceEngagementUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.PoliceEngagementUpdate
	(
	PoliceEngagementUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PoliceEngagementUpdateID'
GO
--End table policeengagementupdate.PoliceEngagementUpdate

--Begin table policeengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.Province
	(
	ProvinceID INT,
	PoliceEngagementUpdateID INT,
	CapacityAssessmentDate DATE,
	PPPDate DATE,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table policeengagementupdate.Province

--Begin table policeengagementupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table policeengagementupdate.ProvinceIndicator

--Begin table policeengagementupdate.ProvinceRecommendation
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceRecommendation
	(
	ProvinceRecommendationID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	RecommendationID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRecommendationID'
EXEC utility.SetIndexClustered 'IX_ProvinceRecommendation', @TableName, 'ProvinceID,RecommendationID'
GO
--End table policeengagementupdate.ProvinceRecommendation

--Begin table policeengagementupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table policeengagementupdate.ProvinceRisk

--Begin table project.ProjectCommunity
DECLARE @TableName VARCHAR(250) = 'project.ProjectCommunity'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table project.ProjectCommunity

--Begin table project.ProjectProvince
DECLARE @TableName VARCHAR(250) = 'project.ProjectProvince'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table project.ProjectProvince

--Begin table recommendation.RecommendationCommunity
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationCommunity'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table recommendation.RecommendationCommunity

--Begin table recommendation.RecommendationProvince
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationProvince'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table recommendation.RecommendationProvince

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Community data for a specific CommunityProvinceEngagementUpdate record
-- ========================================================================================================

CREATE FUNCTION eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM communityprovinceengagementupdate.Community T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityContact data for a specific CommunityProvinceEngagementUpdate record
-- ===============================================================================================================

CREATE FUNCTION eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityContacts VARCHAR(MAX) = ''
	
	SELECT @cCommunityContacts = COALESCE(@cCommunityContacts, '') + D.CommunityContact
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityContact'), ELEMENTS) AS CommunityContact
		FROM communityprovinceengagementupdate.CommunityContact T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityContacts>' + ISNULL(@cCommunityContacts, '') + '</CommunityContacts>'

END
GO
--End function eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityFinding data for a specific CommunityProvinceEngagementUpdate record
-- ===============================================================================================================

CREATE FUNCTION eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityFindings VARCHAR(MAX) = ''
	
	SELECT @cCommunityFindings = COALESCE(@cCommunityFindings, '') + D.CommunityFinding
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityFinding'), ELEMENTS) AS CommunityFinding
		FROM communityprovinceengagementupdate.CommunityFinding T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityFindings>' + ISNULL(@cCommunityFindings, '') + '</CommunityFindings>'

END
GO
--End function eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityIndicator data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM communityprovinceengagementupdate.CommunityIndicator T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityProject data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityProjects VARCHAR(MAX) = ''
	
	SELECT @cCommunityProjects = COALESCE(@cCommunityProjects, '') + D.CommunityProject
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityProject'), ELEMENTS) AS CommunityProject
		FROM communityprovinceengagementupdate.CommunityProject T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityProjects>' + ISNULL(@cCommunityProjects, '') + '</CommunityProjects>'

END
GO
--End function eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRecommendation data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRecommendations VARCHAR(MAX) = ''
	
	SELECT @cCommunityRecommendations = COALESCE(@cCommunityRecommendations, '') + D.CommunityRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRecommendation'), ELEMENTS) AS CommunityRecommendation
		FROM communityprovinceengagementupdate.CommunityRecommendation T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityRecommendations>' + ISNULL(@cCommunityRecommendations, '') + '</CommunityRecommendations>'

END
GO
--End function eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRisk data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM communityprovinceengagementupdate.CommunityRisk T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Province data for a specific CommunityProvinceEngagementUpdate record
-- =======================================================================================================

CREATE FUNCTION eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM communityprovinceengagementupdate.Province T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceContact data for a specific CommunityProvinceEngagementUpdate record
-- ==============================================================================================================

CREATE FUNCTION eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceContacts VARCHAR(MAX) = ''
	
	SELECT @cProvinceContacts = COALESCE(@cProvinceContacts, '') + D.ProvinceContact
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceContact'), ELEMENTS) AS ProvinceContact
		FROM communityprovinceengagementupdate.ProvinceContact T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceContacts>' + ISNULL(@cProvinceContacts, '') + '</ProvinceContacts>'

END
GO
--End function eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceFinding data for a specific CommunityProvinceEngagementUpdate record
-- ==============================================================================================================

CREATE FUNCTION eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceFindings VARCHAR(MAX) = ''
	
	SELECT @cProvinceFindings = COALESCE(@cProvinceFindings, '') + D.ProvinceFinding
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceFinding'), ELEMENTS) AS ProvinceFinding
		FROM communityprovinceengagementupdate.ProvinceFinding T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceFindings>' + ISNULL(@cProvinceFindings, '') + '</ProvinceFindings>'

END
GO
--End function eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceIndicator data for a specific CommunityProvinceEngagementUpdate record
-- ================================================================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM communityprovinceengagementupdate.ProvinceIndicator T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'

END
GO
--End function eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceProject data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceProjects VARCHAR(MAX) = ''
	
	SELECT @cProvinceProjects = COALESCE(@cProvinceProjects, '') + D.ProvinceProject
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceProject'), ELEMENTS) AS ProvinceProject
		FROM communityprovinceengagementupdate.ProvinceProject T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceProjects>' + ISNULL(@cProvinceProjects, '') + '</ProvinceProjects>'

END
GO
--End function eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRecommendation data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRecommendations VARCHAR(MAX) = ''
	
	SELECT @cProvinceRecommendations = COALESCE(@cProvinceRecommendations, '') + D.ProvinceRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRecommendation'), ELEMENTS) AS ProvinceRecommendation
		FROM communityprovinceengagementupdate.ProvinceRecommendation T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceRecommendations>' + ISNULL(@cProvinceRecommendations, '') + '</ProvinceRecommendations>'

END
GO
--End function eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRisk data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM communityprovinceengagementupdate.ProvinceRisk T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'

END
GO
--End function eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Community table
-- ====================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)
	
	INSERT INTO communityprovinceengagementupdate.Community
		(CommunityID, CommunityProvinceEngagementUpdateID, UpdatePersonID)
	SELECT
		CAST(LTT.ListItem AS INT),
		@CommunityProvinceEngagementUpdateID,
		@PersonID
	FROM dbo.ListToTable(@CommunityIDList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Community C
		WHERE C.CommunityID = CAST(LTT.ListItem AS INT)
		)

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Province table
-- ===================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)
	
	INSERT INTO communityprovinceengagementupdate.Province
		(ProvinceID, CommunityProvinceEngagementUpdateID, UpdatePersonID)
	SELECT
		CAST(LTT.ListItem AS INT),
		(SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC),
		@PersonID
	FROM dbo.ListToTable(@ProvinceIDList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Province P
		WHERE P.ProvinceID = CAST(LTT.ListItem AS INT)
		)

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

--Begin procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A procedure to approve a Community/Province Engagement Update
-- ==========================================================================
CREATE PROCEDURE communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRANSACTION
	
	DECLARE @nCommunityID INT
	DECLARE @nCommunityProvinceEngagementUpdateID INT = ISNULL((SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdateID CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC), 0)
	DECLARE @nContactAffiliationID INT = ISNULL((SELECT CA.ContactAffiliationID FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Community Security Working Groups'), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	UPDATE P
	SET
		P.CAPAgreedDate = CPEU.CAPAgreedDate,
		P.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		P.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		P.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN communityprovinceengagementupdate.Province CPEU ON CPEU.ProvinceID = P.ProvinceID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputProvince O ON O.ProvinceID = C.ProvinceID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode = 'Beneficiary'
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		PC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.ProvinceContact PC

	DELETE FC
	FROM finding.FindingProvince FC
		JOIN @tOutputProvince O ON O.ProvinceID = FC.ProvinceID
	
	INSERT INTO finding.FindingProvince
		(ProvinceID, FindingID)
	SELECT
		PF.ProvinceID,
		PF.FindingID
	FROM communityprovinceengagementupdate.ProvinceFinding PF

	DELETE CI
	FROM dbo.ProvinceIndicator CI
		JOIN @tOutputProvince O ON O.ProvinceID = CI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID)
	SELECT
		CI.ProvinceID,
		CI.IndicatorID
	FROM communityprovinceengagementupdate.ProvinceIndicator CI

	DELETE PC
	FROM project.ProjectProvince PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO project.ProjectProvince
		(ProvinceID, ProjectID)
	SELECT
		CP.ProvinceID,
		CP.ProjectID
	FROM communityprovinceengagementupdate.ProvinceProject CP

	DELETE RC
	FROM recommendation.RecommendationProvince RC
		JOIN @tOutputProvince O ON O.ProvinceID = RC.ProvinceID
	
	INSERT INTO recommendation.RecommendationProvince
		(ProvinceID, RecommendationID)
	SELECT
		CR.ProvinceID,
		CR.RecommendationID
	FROM communityprovinceengagementupdate.ProvinceRecommendation CR

	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID)
	SELECT
		CR.ProvinceID,
		CR.RiskID
	FROM communityprovinceengagementupdate.ProvinceRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	DELETE FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

	TRUNCATE TABLE communityprovinceengagementupdate.Province
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceContact
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceFinding
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceProject
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRisk

	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'update', @PersonID, NULL

	UPDATE C
	SET
		C.CAPAgreedDate = CPEU.CAPAgreedDate,
		C.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		C.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		C.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN communityprovinceengagementupdate.Community CPEU ON CPEU.CommunityID = C.CommunityID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputCommunity O ON O.CommunityID = C.CommunityID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode = 'Beneficiary'
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		CC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.CommunityContact CC

	DELETE FC
	FROM finding.FindingCommunity FC
		JOIN @tOutputCommunity O ON O.CommunityID = FC.CommunityID
	
	INSERT INTO finding.FindingCommunity
		(CommunityID, FindingID)
	SELECT
		CF.CommunityID,
		CF.FindingID
	FROM communityprovinceengagementupdate.CommunityFinding CF

	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID)
	SELECT
		CI.CommunityID,
		CI.IndicatorID
	FROM communityprovinceengagementupdate.CommunityIndicator CI

	DELETE PC
	FROM project.ProjectCommunity PC
		JOIN @tOutputCommunity O ON O.CommunityID = PC.CommunityID
	
	INSERT INTO project.ProjectCommunity
		(CommunityID, ProjectID)
	SELECT
		CP.CommunityID,
		CP.ProjectID
	FROM communityprovinceengagementupdate.CommunityProject CP

	DELETE RC
	FROM recommendation.RecommendationCommunity RC
		JOIN @tOutputCommunity O ON O.CommunityID = RC.CommunityID
	
	INSERT INTO recommendation.RecommendationCommunity
		(CommunityID, RecommendationID)
	SELECT
		CR.CommunityID,
		CR.RecommendationID
	FROM communityprovinceengagementupdate.CommunityRecommendation CR

	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID)
	SELECT
		CR.CommunityID,
		CR.RiskID
	FROM communityprovinceengagementupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	DELETE FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

	TRUNCATE TABLE communityprovinceengagementupdate.Community
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityContact
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityFinding
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityProject
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRisk

	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'update', @PersonID, NULL

	COMMIT TRANSACTION

END

GO
--End procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

--Begin procedure communityprovinceengagementupdate.DeleteCommunityProvinceEngagementCommunity
EXEC Utility.DropObject 'communityprovinceengagementupdate.DeleteCommunityProvinceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to delete data from the communityprovinceengagementupdate.Community table
-- =========================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.DeleteCommunityProvinceEngagementCommunity

@CommunityID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)

	DELETE T
	FROM communityprovinceengagementupdate.Community T
	WHERE T.CommunityID = @CommunityID
	
	DELETE T
	FROM communityprovinceengagementupdate.CommunityContact T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.CommunityFinding T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.CommunityIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.CommunityProject T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.CommunityRecommendation T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.CommunityRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
		
	DELETE D
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementCommunity'
			AND DE.EntityID = @CommunityID
	
	DELETE DE
	FROM dbo.DocumentEntity DE 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.Document D
		WHERE D.DocumentID = DE.DocumentID
		)

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.DeleteCommunityProvinceEngagementCommunity

--Begin procedure communityprovinceengagementupdate.DeleteCommunityProvinceEngagementProvince
EXEC Utility.DropObject 'communityprovinceengagementupdate.DeleteCommunityProvinceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to delete data from the communityprovinceengagementupdate.Province table
-- ========================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.DeleteCommunityProvinceEngagementProvince

@ProvinceID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)
	
	DELETE T
	FROM communityprovinceengagementupdate.Province T
	WHERE T.ProvinceID = @ProvinceID
	
	DELETE T
	FROM communityprovinceengagementupdate.ProvinceContact T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.ProvinceFinding T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.ProvinceIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.ProvinceProject T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.ProvinceRecommendation T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM communityprovinceengagementupdate.ProvinceRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM communityprovinceengagementupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
		
	DELETE D
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementProvince'
			AND DE.EntityID = @ProvinceID
	
	DELETE DE
	FROM dbo.DocumentEntity DE 
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.Document D
		WHERE D.DocumentID = DE.DocumentID
		)

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.DeleteCommunityProvinceEngagementProvince

--Begin procedure communityprovinceengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dbo.Community and communityprovinceengagementupdate.Community tables
-- ============================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CAPAgreedDate,
		dbo.FormatDate(C.CAPAgreedDate) AS CAPAgreedDateFormatted,
		C.CommunityID,
		C.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4,
		C.CommunityName AS EntityName,
		C.LastNeedsAssessmentDate,
		dbo.FormatDate(C.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM dbo.Community C
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = C.TORMOUStatusID
			AND C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CAPAgreedDate,
		dbo.FormatDate(C1.CAPAgreedDate) AS CAPAgreedDateFormatted,
		C1.CommunityID,
		C1.CommunityEngagementOutput1,
		C1.CommunityEngagementOutput2,
		C1.CommunityEngagementOutput3,
		C1.CommunityEngagementOutput4,
		C1.LastNeedsAssessmentDate,
		dbo.FormatDate(C1.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		C2.CommunityName AS EntityName,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM communityprovinceengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = C1.TORMOUStatusID
			AND C1.CommunityID = @CommunityID

	--EntityContactCurrent
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
	WHERE C.IsActive = 1
		AND C.IsValid = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode = 'Beneficiary'
					AND CCT.ContactID = C.ContactID
			)
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactAffiliation CCA
				JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
					AND CA.ContactAffiliationName = 'Community Security Working Groups'
					AND CCA.ContactID = C.ContactID
			)
		AND C.CommunityID = @CommunityID
	ORDER BY 2

	--EntityContactUpdate
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN communityprovinceengagementupdate.CommunityContact CC ON CC.ContactID = C.ContactID
			AND CC.CommunityID = @CommunityID
	ORDER BY 2

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityFindingCurrent
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingCommunity FC
				WHERE FC.FindingID = F.FindingID
					AND FC.CommunityID = @CommunityID
				)
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingIndicator FI
					JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
						AND FI.FindingID = F.FindingID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO2'
				)
	ORDER BY F.FindingName, F.FindingID		

	--EntityFindingUpdate
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
		JOIN communityprovinceengagementupdate.CommunityFinding CF ON CF.FindingID = F.FindingID
			AND CF.CommunityID = @CommunityID
	ORDER BY F.FindingName, F.FindingID		

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.CommunityProvinceEngagementAchievedValue,
		OACI.CommunityProvinceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.CommunityProvinceEngagementAchievedValue, 
				CI.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PC.CommunityProvinceEngagementNotes,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(PC.ProjectCommunityID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		CP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(CP.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN communityprovinceengagementupdate.CommunityProject CP ON CP.ProjectID = P.ProjectID
			AND CP.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(RC.RecommendationCommunityID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.CommunityProvinceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM communityprovinceengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.CommunityProvinceEngagementRiskValue,
		OACR.CommunityProvinceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementRiskValue, 
				CR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure communityprovinceengagementupdate.GetCommunityByCommunityID

--Begin procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.11
-- Description:	A stored procedure to get data from the communityprovinceengagementupdate.CommunityProvinceEngagementUpdate table
-- ==============================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CommunityProvinceEngagementUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (CommunityProvinceEngagementUpdateID INT)

		INSERT INTO communityprovinceengagementupdate.CommunityProvinceEngagementUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.CommunityProvinceEngagementUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.CommunityProvinceEngagementUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'

		SELECT @CommunityProvinceEngagementUpdateID = O.CommunityProvinceEngagementUpdateID FROM @tOutput O
		
		EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='create', @PersonID = @PersonID

		END
	ELSE
		SELECT @CommunityProvinceEngagementUpdateID = CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU
	--ENDIF
	
	--CommunityProvinceEngagement
	SELECT
		CPEU.CommunityProvinceEngagementUpdateID, 
		CPEU.WorkflowStepNumber 
	FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			JOIN communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ON CPEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowStatus
	SELECT
		'CommunityProvinceEngagementUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @CommunityProvinceEngagementUpdateID

	--WorkflowStepWorkflowAction
	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT CPEU.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU WHERE CPEU.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

--Begin procedure communityprovinceengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dbo.Province table and communityprovinceengagementupdate.Province tables
-- ================================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CAPAgreedDate,
		dbo.FormatDate(P.CAPAgreedDate) AS CAPAgreedDateFormatted,
		P.ProvinceID,
		P.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4,
		P.ProvinceName AS EntityName,
		P.LastNeedsAssessmentDate,
		dbo.FormatDate(P.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM dbo.Province P
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = P.TORMOUStatusID
			AND P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CAPAgreedDate,
		dbo.FormatDate(P1.CAPAgreedDate) AS CAPAgreedDateFormatted,
		P1.ProvinceID,
		P1.CommunityEngagementOutput1,
		P1.CommunityEngagementOutput2,
		P1.CommunityEngagementOutput3,
		P1.CommunityEngagementOutput4,
		P1.LastNeedsAssessmentDate,
		dbo.FormatDate(P1.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		P2.ProvinceName AS EntityName,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM communityprovinceengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = P1.TORMOUStatusID
			AND P1.ProvinceID = @ProvinceID

	--EntityContactCurrent
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
	WHERE C.IsActive = 1
		AND C.IsValid = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode = 'Beneficiary'
					AND CCT.ContactID = C.ContactID
			)
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactAffiliation CCA
				JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
					AND CA.ContactAffiliationName = 'Community Security Working Groups'
					AND CCA.ContactID = C.ContactID
			)
		AND C.ProvinceID = @ProvinceID
	ORDER BY 2

	--EntityContactUpdate
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN communityprovinceengagementupdate.ProvinceContact PC ON PC.ContactID = C.ContactID
			AND PC.ProvinceID = @ProvinceID
	ORDER BY 2

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityFindingCurrent
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingProvince FP
				WHERE FP.FindingID = F.FindingID
					AND FP.ProvinceID = @ProvinceID
				)
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingIndicator FI
					JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
						AND FI.FindingID = F.FindingID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO2'
				)
	ORDER BY F.FindingName, F.FindingID		

	--EntityFindingUpdate
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
		JOIN communityprovinceengagementupdate.ProvinceFinding PF ON PF.FindingID = F.FindingID
			AND PF.ProvinceID = @ProvinceID
	ORDER BY F.FindingName, F.FindingID		

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.CommunityProvinceEngagementAchievedValue,
		OAPI.CommunityProvinceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.CommunityProvinceEngagementAchievedValue, 
				PRI.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	
	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PP.CommunityProvinceEngagementNotes,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(PP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		PP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(PP.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN communityprovinceengagementupdate.ProvinceProject PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(RP.RecommendationProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.CommunityProvinceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(OAPR.ProvinceRiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM communityprovinceengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.CommunityProvinceEngagementRiskValue,
		OAPR.CommunityProvinceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure communityprovinceengagementupdate.GetProvinceByProvinceID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.ArabicMotherName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C1.CommunityAssetID,
		C1.CommunityAssetUnitID,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		C7.CountryID AS PlaceOfBirthCountryID,
		C7.CountryName AS PlaceOfBirthCountryName,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetCommunityAssetLocations
EXEC Utility.DropObject 'dbo.GetCommunityAssetLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	A stored procedure to get location data from the dbo.CommunityAsset table
-- ======================================================================================
CREATE PROCEDURE dbo.GetCommunityAssetLocations
@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CA.Location.STAsText() AS Location,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND 
				(
				IsNull(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(CA.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR CA.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR CA.ProvinceID = @ProvinceID
				)

END
GO
--End procedure dbo.GetCommunityAssetLocations

--Begin procedure dbo.GetCommunityLocations
EXEC Utility.DropObject 'dbo.GetCommunityLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to get location data from the dbo.Community table
--
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	Refactored
-- =================================================================================
CREATE PROCEDURE dbo.GetCommunityLocations
@CommunityID INT = 0,
@ProvinceID INT = 0,
@Boundary VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT 
		C.CommunityID,
		C.CommunityName,
		CAST(C.Latitude AS NUMERIC(13,8)) AS Latitude,
		CAST(C.Longitude AS NUMERIC(13,8)) AS Longitude,
		C.Population,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		ID.ImpactDecisionName, 
		ID.ImpactDecisionID
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.ImpactDecision ID on ID.ImpactDecisionID = C.ImpactDecisionID
			AND 
				(
				@CommunityID = 0
					OR C.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR C.ProvinceID = @ProvinceID
				)
			AND 
				(
				IsNull(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(C.Location) = 1
				)
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure dbo.GetCommunityLocations

--Begin procedure dbo.GetIncidentLocations
EXEC Utility.DropObject 'dbo.GetIncidentLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.23
-- Description:	A stored procedure to data from the dbo.Incident table
--
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	Refactored
-- ===================================================================
CREATE PROCEDURE dbo.GetIncidentLocations

@StartDate DATE,
@EndDate DATE,
@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT
		I.IncidentID,
		I.IncidentName,
		CAST(I.Latitude AS NUMERIC(13,8)) AS Latitude,
		CAST(I.Longitude AS NUMERIC(13,8)) AS Longitude,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND I.IncidentDate BETWEEN @StartDate AND @EndDate
			AND 
				(
				IsNull(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(I.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR EXISTS
					(
					SELECT 1
					FROM dbo.Community C
						JOIN dbo.IncidentCommunity IC ON IC.CommunityID = C.CommunityID
							AND C.CommunityID = @CommunityID
							AND IC.IncidentID = I.IncidentID
					)
				)
			AND 
				(
				@ProvinceID = 0
					OR EXISTS
					(
					SELECT 1
					FROM dbo.Province P
						JOIN dbo.IncidentProvince IP ON IP.ProvinceID = P.ProvinceID
							AND P.ProvinceID = @ProvinceID
							AND IP.IncidentID = I.IncidentID
					)
				)

END
GO
--End procedure dbo.GetIncidentLocations

--Begin procedure dbo.GetIndicatorCommunityNotes
EXEC Utility.DropObject 'dbo.GetIndicatorCommunityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.CommunityIndicator table
-- ====================================================================================
CREATE PROCEDURE dbo.GetIndicatorCommunityNotes

@CommunityIndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CI.CommunityProvinceEngagementAchievedValue,
		CI.CommunityProvinceEngagementNotes
	FROM dbo.CommunityIndicator CI
	WHERE CI.CommunityIndicatorID = @CommunityIndicatorID
	
END
GO
--End procedure dbo.GetIndicatorCommunityNotes

--Begin procedure dbo.GetIndicatorProvinceNotes
EXEC Utility.DropObject 'dbo.GetIndicatorProvinceNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.ProvinceIndicator table
-- ===================================================================================
CREATE PROCEDURE dbo.GetIndicatorProvinceNotes

@ProvinceIndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PRI.CommunityProvinceEngagementAchievedValue,
		PRI.CommunityProvinceEngagementNotes
	FROM dbo.ProvinceIndicator PRI
	WHERE PRI.ProvinceIndicatorID = @ProvinceIndicatorID
	
END
GO
--End procedure dbo.GetIndicatorProvinceNotes

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetRiskCommunityNotes
EXEC Utility.DropObject 'dbo.GetRiskCommunityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.CommunityRisk table
-- ===============================================================================
CREATE PROCEDURE dbo.GetRiskCommunityNotes

@CommunityRiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CR.CommunityProvinceEngagementRiskValue,
		CR.CommunityProvinceEngagementNotes,
		R.RiskDescription
	FROM dbo.CommunityRisk CR
		JOIN dbo.Risk R ON R.RiskID = CR.RiskID
			AND CR.CommunityRiskID = @CommunityRiskID
	
END
GO
--End procedure dbo.GetRiskCommunityNotes

--Begin procedure dbo.GetRiskDescription
EXEC Utility.DropObject 'dbo.GetRiskDescription'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.Risk table
-- ======================================================================
CREATE PROCEDURE dbo.GetRiskDescription

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		R.RiskDescription
	FROM dbo.Risk R
	WHERE R.RiskID = @RiskID
	
END
GO
--End procedure dbo.GetRiskDescription

--Begin procedure dbo.GetRiskProvinceNotes
EXEC Utility.DropObject 'dbo.GetRiskProvinceNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.19
-- Description:	A stored procedure to return data from the dbo.ProvinceRisk table
-- ==============================================================================
CREATE PROCEDURE dbo.GetRiskProvinceNotes

@ProvinceRiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PR.CommunityProvinceEngagementRiskValue,
		PR.CommunityProvinceEngagementNotes,
		R.RiskDescription
	FROM dbo.ProvinceRisk PR
		JOIN dbo.Risk R ON R.RiskID = PR.RiskID
			AND PR.ProvinceRiskID = @ProvinceRiskID
	
END
GO
--End procedure dbo.GetRiskProvinceNotes

--Begin procedure dropdown.GetComponentReportingAssociationData
EXEC Utility.DropObject 'dropdown.GetComponentReportingAssociationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dropdown.ComponentReportingAssociation table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetComponentReportingAssociationData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ComponentReportingAssociationID,
		T.ComponentReportingAssociationCode,
		T.ComponentReportingAssociationName
	FROM dropdown.ComponentReportingAssociation T
	WHERE (T.ComponentReportingAssociationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ComponentReportingAssociationName, T.ComponentReportingAssociationID

END
GO
--End procedure dropdown.GetComponentReportingAssociationData

--Begin procedure dropdown.GetTORMOUStatusData
EXEC Utility.DropObject 'dropdown.GetTORMOUStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dropdown.TORMOUStatus table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetTORMOUStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.TORMOUStatusID,
		T.TORMOUStatusName
	FROM dropdown.TORMOUStatus T
	WHERE (T.TORMOUStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.TORMOUStatusName, T.TORMOUStatusID

END
GO
--End procedure dropdown.GetTORMOUStatusData

--Begin procedure eventlog.LogCommunityProvinceEngagementAction
EXEC utility.DropObject 'eventlog.LogCommunityProvinceEngagementAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityProvinceEngagementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityProvinceEngagementUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityProvinceEngagementUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML))
			FOR XML RAW('CommunityProvinceEngagementUpdate'), ELEMENTS
			)
		FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.CommunityProvinceEngagementUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityProvinceEngagementAction

--Begin procedure eventlog.LogPoliceEngagementAction
EXEC utility.DropObject 'eventlog.LogPoliceEngagementAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPoliceEngagementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		select 1
/*
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityContactXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityFindingXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityProjectXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceContactXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceFindingXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceProjectXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML))
			FOR XML RAW('PoliceEngagementUpdate'), ELEMENTS
			)
		FROM communityprovinceengagementupdate.PoliceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PoliceEngagementUpdateID
*/

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPoliceEngagementAction

--Begin procedure logicalframework.GetIntermediateOutcomeChartData
EXEC Utility.DropObject 'logicalframework.GetIntermediateOutcomeChartData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	A stored procedure to return intermediate outcome objective data
-- =============================================================================
CREATE PROCEDURE logicalframework.GetIntermediateOutcomeChartData

AS
BEGIN
	SET NOCOUNT ON;

	WITH Total AS
		(
		SELECT
			I.AchievedValue,
			I.IndicatorID,
			I.IndicatorName,
			I.InprogressValue,
			I.PlannedValue,
			I.TargetValue,
			I.PlannedValue + I.InprogressValue + I.AchievedValue AS TotalValue,
			LFS.LogicalFrameworkStatusName,
			O2.ObjectiveID,
			O2.ObjectiveName,
			O2.StatusUpdateDescription
		FROM logicalframework.Indicator I
			JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
			JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
				AND O2.IsActive = 1
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O2.LogicalFrameworkStatusID
			JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
				AND OT.ObjectiveTypeCode = 'Output'
				AND I.IsActive = 1
		)
	
	SELECT
		T.IndicatorID,
		T.IndicatorName,
		T.LogicalFrameworkStatusName,
		T.ObjectiveID,
		T.ObjectiveName,
		T.StatusUpdateDescription,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 
				CASE
					WHEN T.TotalValue > 0
					THEN CAST(CAST(T.AchievedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
		END AS AchievedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 
				CASE
					WHEN T.TotalValue > 0
					THEN CAST(CAST(T.InprogressValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
		END AS InprogressValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST((CAST(T.TargetValue AS NUMERIC(18,2)) - CAST(T.AchievedValue AS NUMERIC(18,2)) - CAST(T.InprogressValue AS NUMERIC(18,2)) - CAST(T.PlannedValue AS NUMERIC(18,2))) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 0
		END AS IntendedValuePercent,
		
		CASE
			WHEN T.TargetValue >= T.TotalValue
			THEN 
				CASE
					WHEN T.TargetValue > 0
					THEN CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TargetValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
			ELSE 
				CASE
					WHEN T.TotalValue > 0
					THEN CAST(CAST(T.PlannedValue AS NUMERIC(18,2)) / CAST(T.TotalValue AS NUMERIC(18,2)) * 100 AS NUMERIC(18,2))
					ELSE 0
				END
		END AS PlannedValuePercent

	FROM Total T
	ORDER BY T.ObjectiveName, T.ObjectiveID, T.IndicatorName, T.IndicatorID

END
GO
--End procedure logicalframework.GetIntermediateOutcomeChartData

--Begin procedure logicalframework.GetObjectiveByObjectiveID
EXEC Utility.DropObject 'logicalframework.GetObjectiveByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	A stored procedure to return Objective data
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Added fields
--
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	Added the ComponentReportingAssociationID
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectiveByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CRA.ComponentReportingAssociationCode,
		CRA.ComponentReportingAssociationID,
		CRA.ComponentReportingAssociationName,
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O1.IsActive,
		O1.ObjectiveDescription,
		O1.ObjectiveID, 
		O1.ObjectiveName, 
		O1.ParentObjectiveID, 

		CASE
			WHEN O1.ParentObjectiveID = 0
			THEN NULL
			ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
		END AS ParentObjectiveName,
		
		O1.StatusUpdateDescription,
		OT.ObjectiveTypeID, 
		OT.ObjectiveTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
	FROM logicalframework.Objective O1
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O1.ComponentReportingAssociationID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
			AND O1.ObjectiveID = @ObjectiveID

END
GO
--End procedure logicalframework.GetObjectiveByObjectiveID

--Begin procedure policeengagementupdate.AddPoliceEngagementCommunity
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Community table
-- =========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Community
		(CommunityID, PoliceEngagementUpdateID, UpdatePersonID)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID
	FROM dbo.ListToTable(@CommunityIDList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Community C
		WHERE C.CommunityID = CAST(LTT.ListItem AS INT)
		)

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementCommunity

--Begin procedure policeengagementupdate.AddPoliceEngagementProvince
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Province table
-- ========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Province
		(ProvinceID, PoliceEngagementUpdateID, UpdatePersonID)
	SELECT
		CAST(LTT.ListItem AS INT),
		(SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC),
		@PersonID
	FROM dbo.ListToTable(@ProvinceIDList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM policeengagementupdate.Province P
		WHERE P.ProvinceID = CAST(LTT.ListItem AS INT)
		)

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementProvince

--Begin procedure policeengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Community and policeengagementupdate.Community tables
-- =================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CapacityAssessmentDate,
		dbo.FormatDate(C.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C.CommunityID,
		C.PPPDate,
		dbo.FormatDate(C.PPPDate) AS PPPDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CapacityAssessmentDate,
		dbo.FormatDate(C1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C1.PPPDate,
		dbo.FormatDate(C1.PPPDate) AS PPPDateFormatted,
		C2.CommunityID,
		C2.CommunityName AS EntityName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.CommunityID = @CommunityID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.PoliceEngagementAchievedValue,
		OACI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.PoliceEngagementAchievedValue, 
				CI.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(RC.RecommendationCommunityID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.PoliceEngagementRiskValue,
		OACR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure policeengagementupdate.GetCommunityByCommunityID

--Begin procedure policeengagementupdate.GetPoliceEngagementUpdate
EXEC Utility.DropObject 'policeengagementupdate.GetPoliceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.11
-- Description:	A stored procedure to get data from the policeengagementupdate.PoliceEngagementUpdate table
-- ========================================================================================================
CREATE PROCEDURE policeengagementupdate.GetPoliceEngagementUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PoliceEngagementUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM policeengagementupdate.PoliceEngagementUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (PoliceEngagementUpdateID INT)

		INSERT INTO policeengagementupdate.PoliceEngagementUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.PoliceEngagementUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.PoliceEngagementUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'PoliceEngagementUpdate'

		SELECT @PoliceEngagementUpdateID = O.PoliceEngagementUpdateID FROM @tOutput O
		
		EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='create', @PersonID = @PersonID

		END
	ELSE
		SELECT @PoliceEngagementUpdateID = PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU
	--ENDIF
	
	--PoliceEngagement
	SELECT
		PEU.PoliceEngagementUpdateID, 
		PEU.WorkflowStepNumber 
	FROM policeengagementupdate.PoliceEngagementUpdate PEU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'PoliceEngagementUpdate'
			JOIN policeengagementupdate.PoliceEngagementUpdate PEU ON PEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowStatus
	SELECT
		'PoliceEngagementUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @PoliceEngagementUpdateID

	--WorkflowStepWorkflowAction
	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'PoliceEngagementUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT PEU.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate PEU WHERE PEU.PoliceEngagementUpdateID = @PoliceEngagementUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure policeengagementupdate.GetPoliceEngagementUpdate

--Begin procedure policeengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Province table and policeengagementupdate.Province tables
-- =====================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CapacityAssessmentDate,
		dbo.FormatDate(P.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P.ProvinceID,
		P.PPPDate,
		dbo.FormatDate(P.PPPDate) AS PPPDateFormatted
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CapacityAssessmentDate,
		dbo.FormatDate(P1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P1.PPPDate,
		dbo.FormatDate(P1.PPPDate) AS PPPDateFormatted,
		P2.ProvinceID,
		P2.ProvinceName AS EntityName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
			AND P1.ProvinceID = @ProvinceID


	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.PoliceEngagementAchievedValue,
		OAPI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.PoliceEngagementAchievedValue, 
				PRI.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(RP.RecommendationProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(OAPR.ProvinceRiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.PoliceEngagementRiskValue,
		OAPR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure policeengagementupdate.GetProvinceByProvinceID

--Begin procedure project.GetProjectCommunityNotes
EXEC Utility.DropObject 'project.GetProjectCommunityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.17
-- Description:	A stored procedure to return data from the project.ProjectCommunity table
-- ======================================================================================
CREATE PROCEDURE project.GetProjectCommunityNotes

@ProjectCommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RC.CommunityProvinceEngagementNotes
	FROM project.ProjectCommunity RC
	WHERE RC.ProjectCommunityID = @ProjectCommunityID
	
END
GO
--End procedure project.GetProjectCommunityNotes

--Begin procedure project.GetProjectProvinceNotes
EXEC Utility.DropObject 'project.GetProjectProvinceNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.17
-- Description:	A stored procedure to return data from the project.ProjectProvince table
-- =====================================================================================
CREATE PROCEDURE project.GetProjectProvinceNotes

@ProjectProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RP.CommunityProvinceEngagementNotes
	FROM project.ProjectProvince RP
	WHERE RP.ProjectProvinceID = @ProjectProvinceID
	
END
GO
--End procedure project.GetProjectProvinceNotes

--Begin procedure recommendation.GetRecommendationCommunityNotes
EXEC Utility.DropObject 'recommendation.GetRecommendationCommunityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.15
-- Description:	A stored procedure to return data from the recommendation.RecommendationCommunity table
-- ====================================================================================================
CREATE PROCEDURE recommendation.GetRecommendationCommunityNotes

@RecommendationCommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RC.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationCommunity RC
	WHERE RC.RecommendationCommunityID = @RecommendationCommunityID
	
END
GO
--End procedure recommendation.GetRecommendationCommunityNotes

--Begin procedure recommendation.GetRecommendationProvinceNotes
EXEC Utility.DropObject 'recommendation.GetRecommendationProvinceNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.15
-- Description:	A stored procedure to return data from the recommendation.RecommendationProvince table
-- ===================================================================================================
CREATE PROCEDURE recommendation.GetRecommendationProvinceNotes

@RecommendationProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RP.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationProvince RP
	WHERE RP.RecommendationProvinceID = @RecommendationProvinceID
	
END
GO
--End procedure recommendation.GetRecommendationProvinceNotes

--Begin procedure utility.EntityTypeAddUpdate
EXEC Utility.DropObject 'utility.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.08.30
-- Description:	A stored procedure to add / update a record in the dbo.EntityType table
-- ====================================================================================
CREATE PROCEDURE utility.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeGroupCode VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode)
		INSERT INTO dbo.EntityType (EntityTypeGroupCode, EntityTypeCode, EntityTypeName) VALUES (@EntityTypeGroupCode, @EntityTypeCode, @EntityTypeName)
	ELSE
		UPDATE dbo.EntityType SET EntityTypeGroupCode = @EntityTypeGroupCode, EntityTypeName = @EntityTypeName WHERE EntityTypeCode = @EntityTypeCode
	--ENDIF

END
GO
--End procedure utility.EntityTypeAddUpdate

--Begin procedure workflow.CanIncrementCommunityProvinceEngagementUpdateWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementCommunityProvinceEngagementUpdateWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementCommunityProvinceEngagementUpdateWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			JOIN CommunityProvinceEngagementupdate.CommunityProvinceEngagementUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.CommunityProvinceEngagementUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementCommunityProvinceEngagementUpdateWorkflow

--Begin procedure workflow.CanIncrementPoliceEngagementUpdateWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementPoliceEngagementUpdateWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementPoliceEngagementUpdateWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'PoliceEngagementUpdate'
			JOIN PoliceEngagementupdate.PoliceEngagementUpdate PEU ON PEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND PEU.PoliceEngagementUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementPoliceEngagementUpdateWorkflow

--Begin procedure workflow.GetCommunityProvinceEngagementUpdateWorkflowData
EXEC Utility.DropObject 'workflow.GetCommunityProvinceEngagementUpdateWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.25
-- Description:	A procedure to return workflow data from the CommunityProvinceEngagementupdate.CommunityProvinceEngagementUpdate table
-- =========================================================================================================

CREATE PROCEDURE workflow.GetCommunityProvinceEngagementUpdateWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			JOIN CommunityProvinceEngagementupdate.CommunityProvinceEngagementUpdate CPEU ON CPEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CPEU.CommunityProvinceEngagementUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'CommunityProvinceEngagementUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT RU.WorkflowStepNumber FROM CommunityProvinceEngagementupdate.CommunityProvinceEngagementUpdate RU WHERE RU.CommunityProvinceEngagementUpdateID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Community/Province Engagement Updates'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Community/Province Engagement Updates'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Community/Province Engagement Updates'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Community/Province Engagement Updates'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetCommunityProvinceEngagementUpdateWorkflowData

--Begin procedure workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A stored procedure to people associated with the workflow steps on a communityprovinceengagement update
-- ====================================================================================================================
CREATE PROCEDURE workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT CPEU.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU WHERE CPEU.CommunityProvinceEngagementUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT CPEU.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU WHERE CPEU.CommunityProvinceEngagementUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'CommunityProvinceEngagementUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople

--Begin procedure workflow.GetPoliceEngagementUpdateWorkflowData
EXEC Utility.DropObject 'workflow.GetPoliceEngagementUpdateWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.25
-- Description:	A procedure to return workflow data from the policeengagementupdate.PoliceEngagementUpdate table
-- =============================================================================================================

CREATE PROCEDURE workflow.GetPoliceEngagementUpdateWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'PoliceEngagementUpdate'
			JOIN PoliceEngagementupdate.PoliceEngagementUpdate CPEU ON CPEU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CPEU.PoliceEngagementUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'PoliceEngagementUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT PEU.WorkflowStepNumber FROM PoliceEngagementupdate.PoliceEngagementUpdate PEU WHERE PEU.PoliceEngagementUpdateID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'PoliceEngagementUpdate'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Police Engagement Updates'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Police Engagement Updates'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Police Engagement Updates'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Police Engagement Updates'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'PoliceEngagementUpdate'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetPoliceEngagementUpdateWorkflowData

--Begin procedure workflow.GetPoliceEngagementUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetPoliceEngagementUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A stored procedure to people associated with the workflow steps on a policeengagement update
-- =========================================================================================================
CREATE PROCEDURE workflow.GetPoliceEngagementUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'PoliceEngagementUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT CPEU.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate CPEU WHERE CPEU.PoliceEngagementUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT CPEU.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate CPEU WHERE CPEU.PoliceEngagementUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'PoliceEngagementUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetPoliceEngagementUpdateWorkflowStepPeople

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'CommunityProvinceEngagementUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	SELECT
		'CommunityProvinceEngagementUpdate',
		ET.WorkflowActionCode,
		
		CASE
			WHEN ET.WorkflowActionCode = 'DecrementWorkflow'
			THEN '<p>A previously submitted community/province engagement update has been disapproved:</p><p>&nbsp;</p><p><strong>Community/Province Engagement Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the community/province engagement update workflow. Please click the link above to review the updated community/province engagement.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'IncrementWorkflow'
			THEN '<p>An AJACS community/province engagement update has been submitted for your review:</p><p>&nbsp;</p><p><strong>Community/Province Engagement Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the community/province engagement update workflow. Please click the link above to review the updated community/province engagement.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'Release'
			THEN '<p>An AJACS community/province engagement update has been released:</p><p>&nbsp;</p><p><strong>Community/Province Engagement Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the community/province engagement update workflow. Please click the link above to review the updated community/province engagement.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
		END
	
	FROM dbo.EmailTemplate ET
	WHERE ET.EntityTypeCode = 'RiskUpdate'

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'CommunityProvinceEngagementUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('CommunityProvinceEngagementUpdate', '[[Link]]', 'Link', 1),
		('CommunityProvinceEngagementUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'CommunityProvinceEngagementCommunity', 'Community Province Engagement Community'
GO
EXEC utility.EntityTypeAddUpdate 'CommunityProvinceEngagementProvince', 'Community Province Engagement Province'
GO
EXEC utility.EntityTypeAddUpdate 'CommunityProvinceEngagementUpdate', 'Community Province Engagement Update'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityProvenceEngagementUpdate', @NewMenuItemLink='/communityprovinceengagement/addupdate', @NewMenuItemText='Community / Province Engagement', @ParentMenuItemCode='Implementation', @BeforeMenuItemCode='RiskManagement', @PermissionableLineageList='CommunityProvinceEngagementUpdate.WorkflowStepID%'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PoliceEngagementUpdate', @NewMenuItemLink='/policeengagement/addupdate', @NewMenuItemText='Police Engagement', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='CommunityProvenceEngagementUpdate', @PermissionableLineageList='PoliceEngagementUpdate.WorkflowStepID%'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveOverview', @NewMenuItemLink='/objective/chartlist', @BeforeMenuItemCode='ObjectiveList', @PermissionableLineageList='Objective.ChartList'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveManage', @NewMenuItemLink='/objective/manage', @NewMenuItemText='Manage', @ParentMenuItemCode='LogicalFramework', @BeforeMenuItemCode='ObjectiveList', @PermissionableLineageList='Objective.Manage'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
--End table dbo.MenuItem

--Begin table eventlog.EventLog
UPDATE eventlog.EventLog 
SET Comments = NULL 
WHERE comments = ''
GO
--End table eventlog.EventLog

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'CommunityProvinceEngagementUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('CommunityProvinceEngagementUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'CommunityProvinceEngagementUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'RecommendationUpdate'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'CommunityProvinceEngagementUpdate'),
		WSWA.WorkflowStepNumber,
		WSWA.WorkflowActionID,
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'RiskUpdate'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'PoliceEngagementUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('PoliceEngagementUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'PoliceEngagementUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'RiskUpdate'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'PoliceEngagementUpdate'),
		WSWA.WorkflowStepNumber,
		WSWA.WorkflowActionID,
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'RiskUpdate'
	
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityProvinceEngagementUpdate')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('CommunityProvinceEngagementUpdate','Community/Province Engagement', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'PoliceEngagementUpdate')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('PoliceEngagementUpdate','Police Engagement', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityProvinceEngagementUpdate'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('CommunityProvinceEngagementUpdate')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'PoliceEngagementUpdate'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('PoliceEngagementUpdate')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Manage',
	'Manage Objectives & Indicators',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Objective'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Objective.Manage'
		)
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CommunityProvinceEngagementUpdate')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'CommunityProvinceEngagementUpdate','Community/Province Engagement')
		
	END
--ENDIF
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CommunityProvinceEngagementUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO


IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'PoliceEngagementUpdate')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'PoliceEngagementUpdate','Police Engagement')
		
	END
--ENDIF
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'PoliceEngagementUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'PoliceEngagementUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'PoliceEngagementUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.29 File 01 - AJACS - 2015.09.22 19.23.50')
GO
--End build tracking

