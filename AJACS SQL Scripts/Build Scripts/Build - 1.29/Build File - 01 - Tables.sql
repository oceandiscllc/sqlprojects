USE AJACS
GO

--Begin table communityprovinceengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.Community
	(
	CommunityID INT,
	CommunityProvinceEngagementUpdateID INT,
	CommunityEngagementOutput1 VARCHAR(MAX),
	CommunityEngagementOutput2 VARCHAR(MAX),
	CommunityEngagementOutput3 VARCHAR(MAX),
	CommunityEngagementOutput4 VARCHAR(MAX),
	CAPAgreedDate DATE,
	LastNeedsAssessmentDate DATE,
	TORMOUStatusID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table communityprovinceengagementupdate.Community

--Begin table communityprovinceengagementupdate.CommunityContact
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityContact'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityContact
	(
	CommunityContactID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityContactID'
EXEC utility.SetIndexClustered 'IX_CommunityContact', @TableName, 'CommunityID,ContactID'
GO
--End table communityprovinceengagementupdate.CommunityContact

--Begin table communityprovinceengagementupdate.CommunityFinding
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityFinding'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityFinding
	(
	CommunityFindingID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	FindingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityFindingID'
EXEC utility.SetIndexClustered 'IX_CommunityFinding', @TableName, 'CommunityID,FindingID'
GO
--End table communityprovinceengagementupdate.CommunityFinding

--Begin table communityprovinceengagementupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table communityprovinceengagementupdate.CommunityIndicator

--Begin table communityprovinceengagementupdate.CommunityProject
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityProject'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityProject
	(
	CommunityProjectID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	ProjectID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityProjectID'
EXEC utility.SetIndexClustered 'IX_CommunityProject', @TableName, 'CommunityID,ProjectID'
GO
--End table communityprovinceengagementupdate.CommunityProject

--Begin table communityprovinceengagementupdate.CommunityProvinceEngagementUpdate
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityProvinceEngagementUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityProvinceEngagementUpdate
	(
	CommunityProvinceEngagementUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityProvinceEngagementUpdateID'
GO
--End table communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

--Begin table communityprovinceengagementupdate.CommunityRecommendation
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityRecommendation
	(
	CommunityRecommendationID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	RecommendationID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRecommendationID'
EXEC utility.SetIndexClustered 'IX_CommunityRecommendation', @TableName, 'CommunityID,RecommendationID'
GO
--End table communityprovinceengagementupdate.CommunityRecommendation

--Begin table communityprovinceengagementupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	CommunityID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table communityprovinceengagementupdate.CommunityRisk

--Begin table communityprovinceengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.Province
	(
	ProvinceID INT,
	CommunityProvinceEngagementUpdateID INT,
	CommunityEngagementOutput1 VARCHAR(MAX),
	CommunityEngagementOutput2 VARCHAR(MAX),
	CommunityEngagementOutput3 VARCHAR(MAX),
	CommunityEngagementOutput4 VARCHAR(MAX),
	CAPAgreedDate DATE,
	LastNeedsAssessmentDate DATE,
	TORMOUStatusID INT,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table communityprovinceengagementupdate.Province

--Begin table communityprovinceengagementupdate.ProvinceContact
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceContact'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceContact
	(
	ProvinceContactID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceContactID'
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetIndexClustered 'IX_ProvinceContact', @TableName, 'ProvinceID,ContactID'
GO
--End table communityprovinceengagementupdate.ProvinceContact

--Begin table communityprovinceengagementupdate.ProvinceFinding
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceFinding'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceFinding
	(
	ProvinceFindingID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	FindingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceFindingID'
EXEC utility.SetIndexClustered 'IX_ProvinceFinding', @TableName, 'ProvinceID,FindingID'
GO
--End table communityprovinceengagementupdate.ProvinceFinding

--Begin table communityprovinceengagementupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table communityprovinceengagementupdate.ProvinceIndicator

--Begin table communityprovinceengagementupdate.ProvinceProject
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceProject'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceProject
	(
	ProvinceProjectID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	ProjectID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceProjectID'
EXEC utility.SetIndexClustered 'IX_ProvinceProject', @TableName, 'ProvinceID,ProjectID'
GO
--End table communityprovinceengagementupdate.ProvinceProject

--Begin table communityprovinceengagementupdate.ProvinceRecommendation
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceRecommendation
	(
	ProvinceRecommendationID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	RecommendationID INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRecommendationID'
EXEC utility.SetIndexClustered 'IX_ProvinceRecommendation', @TableName, 'ProvinceID,RecommendationID'
GO
--End table communityprovinceengagementupdate.ProvinceRecommendation

--Begin table communityprovinceengagementupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'communityprovinceengagementupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE communityprovinceengagementupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1),
	CommunityProvinceEngagementUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table communityprovinceengagementupdate.ProvinceRisk

--Begin table dbo.Community
DECLARE @ObjectName VARCHAR(250) = 'dbo.TR_CommunityLocation'
EXEC utility.DropObject @ObjectName
GO

DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'ArabicCommunityName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'CapacityAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CAPAgreedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput1', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput2', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput3', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput4', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'LastNeedsAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'Location', 'GEOMETRY'
EXEC utility.AddColumn @TableName, 'PPPDate', 'DATE'
EXEC utility.AddColumn @TableName, 'TORMOUStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
GO

UPDATE dbo.Community
SET Location = GEOMETRY::STPointFromText('POINT(' + CAST(Longitude AS VARCHAR(20)) + ' ' + CAST(Latitude AS VARCHAR(20)) + ')', 4326)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER dbo.TR_CommunityLocation ON dbo.Community AFTER INSERT, UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE C
	SET C.Location = GEOMETRY::STPointFromText('POINT(' + CAST(inserted.Longitude AS VARCHAR(20)) + ' ' + CAST(inserted.Latitude AS VARCHAR(20)) + ')', 4326)
	FROM dbo.Community C
		JOIN INSERTED ON INSERTED.CommunityID = C.CommunityID

END
GO
--End table dbo.Community

--Begin table dbo.CommunityAsset
EXEC utility.AddColumn 'dbo.CommunityAsset', 'tmpLocation', 'GEOMETRY'
GO

UPDATE dbo.CommunityAsset
SET tmpLocation = GEOMETRY::STGeomFromText(Location.STAsText(), 4326)
GO

EXEC utility.DropColumn 'dbo.CommunityAsset', 'Location'
GO

EXEC sp_RENAME 'CommunityAsset.tmpLocation' , 'Location', 'COLUMN'
GO
--End table dbo.CommunityAsset

--Begin table dbo.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1),
	CommunityID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table dbo.CommunityIndicator

--Begin table dbo.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1),
	CommunityID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table dbo.CommunityRisk

--Begin table dbo.Incident
DECLARE @ObjectName VARCHAR(250) = 'dbo.TR_IncidentLocation'
EXEC utility.DropObject @ObjectName
GO

EXEC utility.AddColumn 'dbo.Incident', 'Location', 'GEOMETRY'
GO

UPDATE dbo.Incident
SET Location = GEOMETRY::STPointFromText('POINT(' + CAST(Longitude AS VARCHAR(20)) + ' ' + CAST(Latitude AS VARCHAR(20)) + ')', 4326)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER dbo.TR_IncidentLocation ON dbo.Incident AFTER INSERT, UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE I
	SET I.Location = GEOMETRY::STPointFromText('POINT(' + CAST(inserted.Longitude AS VARCHAR(20)) + ' ' + CAST(inserted.Latitude AS VARCHAR(20)) + ')', 4326)
	FROM dbo.Incident I
		JOIN INSERTED ON INSERTED.IncidentID = I.IncidentID

END
GO
--End table dbo.Incident

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'ArabicProvinceName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'CapacityAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CAPAgreedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput1', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput2', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput3', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CommunityEngagementOutput4', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'LastNeedsAssessmentDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PPPDate', 'DATE'
EXEC utility.AddColumn @TableName, 'TORMOUStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TORMOUStatusID', 'INT', 0
GO
--End table dbo.Province

--Begin table dbo.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1),
	ProvinceID INT,
	IndicatorID INT,
	CommunityProvinceEngagementAchievedValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table dbo.ProvinceIndicator

--Begin table dbo.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1),
	ProvinceID INT,
	RiskID INT,
	CommunityProvinceEngagementRiskValue INT,
	CommunityProvinceEngagementNotes VARCHAR(MAX),
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityProvinceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table dbo.ProvinceRisk

--Begin table dropdown.ComponentReportingAssociation
DECLARE @TableName VARCHAR(250) = 'dropdown.ComponentReportingAssociation'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ComponentReportingAssociation
	(
	ComponentReportingAssociationID INT IDENTITY(0,1) NOT NULL,
	ComponentReportingAssociationCode VARCHAR(50),
	ComponentReportingAssociationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ComponentReportingAssociationID'
EXEC utility.SetIndexNonClustered 'IX_LogicalFrameworkStatus', @TableName, 'DisplayOrder,ComponentReportingAssociationName', 'ComponentReportingAssociationID'
GO

SET IDENTITY_INSERT dropdown.ComponentReportingAssociation ON
GO

INSERT INTO dropdown.ComponentReportingAssociation (ComponentReportingAssociationID) VALUES (0)

SET IDENTITY_INSERT dropdown.ComponentReportingAssociation OFF
GO

INSERT INTO dropdown.ComponentReportingAssociation 
	(ComponentReportingAssociationCode,ComponentReportingAssociationName,DisplayOrder)
VALUES
	('CEO1', 'Community Engagement Output 1', 1),
	('CEO2', 'Community Engagement Output 2', 2),
	('CEO3', 'Community Engagement Output 3', 3),
	('CEO4', 'Community Engagement Output 4', 4),
	('PEO1', 'Police Engagement Output 1', 5),
	('PEO2', 'Police Engagement Output 2', 6),
	('PEO3', 'Police Engagement Output 3', 7),
	('PEO4', 'Police Engagement Output 4', 8),
	('JO1', 'Justice Output 1', 9),
	('JO2', 'Justice Output 2', 10),
	('JO3', 'Justice Output 3', 11),
	('JO4', 'Justice Output 4', 12),
	('FIFO1', 'FIF Output 1', 13),
	('FIFO2', 'FIF Output 2', 14),
	('FIFO3', 'FIF Output 3', 15),
	('FIFO4', 'FIF Output 4', 16)
GO
--End table dropdown.ComponentReportingAssociation

--Begin table dropdown.TORMOUStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.TORMOUStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.TORMOUStatus
	(
	TORMOUStatusID INT IDENTITY(0,1) NOT NULL,
	TORMOUStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'TORMOUStatusID'
EXEC utility.SetIndexNonClustered 'IX_LogicalFrameworkStatus', @TableName, 'DisplayOrder,TORMOUStatusName', 'TORMOUStatusID'
GO

SET IDENTITY_INSERT dropdown.TORMOUStatus ON
GO

INSERT INTO dropdown.TORMOUStatus (TORMOUStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.TORMOUStatus OFF
GO

INSERT INTO dropdown.TORMOUStatus 
	(TORMOUStatusName,DisplayOrder)
VALUES
	('Planned', 1),
	('In Progress', 2),
	('Achieved', 3)
GO
--End table dropdown.TORMOUStatus

--Begin table dropdown.ZoneType
UPDATE dropdown.ZoneType 
SET HexColor = UPPER(HexColor)
GO
--End table dropdown.ZoneType

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.AddColumn @TableName, 'ComponentReportingAssociationID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ComponentReportingAssociationID', 'INT', 0
GO
--End table logicalframework.Objective

--Begin table policeengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.Community
	(
	CommunityID INT,
	PoliceEngagementUpdateID INT,
	CapacityAssessmentDate DATE,
	PPPDate DATE,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table policeengagementupdate.Community

--Begin table policeengagementupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table policeengagementupdate.CommunityIndicator

--Begin table policeengagementupdate.CommunityRecommendation
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityRecommendation
	(
	CommunityRecommendationID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	RecommendationID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRecommendationID'
EXEC utility.SetIndexClustered 'IX_CommunityRecommendation', @TableName, 'CommunityID,RecommendationID'
GO
--End table policeengagementupdate.CommunityRecommendation

--Begin table policeengagementupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	CommunityID INT,
	RiskID INT,
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table policeengagementupdate.CommunityRisk

--Begin table policeengagementupdate.PoliceEngagementUpdate
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.PoliceEngagementUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.PoliceEngagementUpdate
	(
	PoliceEngagementUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PoliceEngagementUpdateID'
GO
--End table policeengagementupdate.PoliceEngagementUpdate

--Begin table policeengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.Province
	(
	ProvinceID INT,
	PoliceEngagementUpdateID INT,
	CapacityAssessmentDate DATE,
	PPPDate DATE,
	UpdateDateTime DATETIME,
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table policeengagementupdate.Province

--Begin table policeengagementupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	PoliceEngagementAchievedValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table policeengagementupdate.ProvinceIndicator

--Begin table policeengagementupdate.ProvinceRecommendation
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceRecommendation
	(
	ProvinceRecommendationID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	RecommendationID INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRecommendationID'
EXEC utility.SetIndexClustered 'IX_ProvinceRecommendation', @TableName, 'ProvinceID,RecommendationID'
GO
--End table policeengagementupdate.ProvinceRecommendation

--Begin table policeengagementupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE policeengagementupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1),
	PoliceEngagementUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	PoliceEngagementRiskValue INT,
	PoliceEngagementNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceEngagementUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table policeengagementupdate.ProvinceRisk

--Begin table project.ProjectCommunity
DECLARE @TableName VARCHAR(250) = 'project.ProjectCommunity'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table project.ProjectCommunity

--Begin table project.ProjectProvince
DECLARE @TableName VARCHAR(250) = 'project.ProjectProvince'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table project.ProjectProvince

--Begin table recommendation.RecommendationCommunity
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationCommunity'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table recommendation.RecommendationCommunity

--Begin table recommendation.RecommendationProvince
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationProvince'

EXEC utility.AddColumn @TableName, 'CommunityProvinceEngagementNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PoliceEngagementNotes', 'VARCHAR(MAX)'
GO
--End table recommendation.RecommendationProvince
