USE AJACS
GO

--Begin schema communityprovinceengagementupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'communityprovinceengagementupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA communityprovinceengagementupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema communityprovinceengagementupdate

--Begin schema policeengagementupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'policeengagementupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA policeengagementupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema policeengagementupdate
