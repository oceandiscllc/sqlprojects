USE AJACS
GO

--Begin function eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Community data for a specific CommunityProvinceEngagementUpdate record
-- ========================================================================================================

CREATE FUNCTION eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM communityprovinceengagementupdate.Community T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityContact data for a specific CommunityProvinceEngagementUpdate record
-- ===============================================================================================================

CREATE FUNCTION eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityContacts VARCHAR(MAX) = ''
	
	SELECT @cCommunityContacts = COALESCE(@cCommunityContacts, '') + D.CommunityContact
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityContact'), ELEMENTS) AS CommunityContact
		FROM communityprovinceengagementupdate.CommunityContact T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityContacts>' + ISNULL(@cCommunityContacts, '') + '</CommunityContacts>'

END
GO
--End function eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityFinding data for a specific CommunityProvinceEngagementUpdate record
-- ===============================================================================================================

CREATE FUNCTION eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityFindings VARCHAR(MAX) = ''
	
	SELECT @cCommunityFindings = COALESCE(@cCommunityFindings, '') + D.CommunityFinding
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityFinding'), ELEMENTS) AS CommunityFinding
		FROM communityprovinceengagementupdate.CommunityFinding T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityFindings>' + ISNULL(@cCommunityFindings, '') + '</CommunityFindings>'

END
GO
--End function eventlog.GetCommunityFindingXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityIndicator data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM communityprovinceengagementupdate.CommunityIndicator T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityProject data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityProjects VARCHAR(MAX) = ''
	
	SELECT @cCommunityProjects = COALESCE(@cCommunityProjects, '') + D.CommunityProject
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityProject'), ELEMENTS) AS CommunityProject
		FROM communityprovinceengagementupdate.CommunityProject T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityProjects>' + ISNULL(@cCommunityProjects, '') + '</CommunityProjects>'

END
GO
--End function eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRecommendation data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRecommendations VARCHAR(MAX) = ''
	
	SELECT @cCommunityRecommendations = COALESCE(@cCommunityRecommendations, '') + D.CommunityRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRecommendation'), ELEMENTS) AS CommunityRecommendation
		FROM communityprovinceengagementupdate.CommunityRecommendation T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityRecommendations>' + ISNULL(@cCommunityRecommendations, '') + '</CommunityRecommendations>'

END
GO
--End function eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return CommunityRisk data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM communityprovinceengagementupdate.CommunityRisk T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return Province data for a specific CommunityProvinceEngagementUpdate record
-- =======================================================================================================

CREATE FUNCTION eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM communityprovinceengagementupdate.Province T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceContact data for a specific CommunityProvinceEngagementUpdate record
-- ==============================================================================================================

CREATE FUNCTION eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceContacts VARCHAR(MAX) = ''
	
	SELECT @cProvinceContacts = COALESCE(@cProvinceContacts, '') + D.ProvinceContact
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceContact'), ELEMENTS) AS ProvinceContact
		FROM communityprovinceengagementupdate.ProvinceContact T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceContacts>' + ISNULL(@cProvinceContacts, '') + '</ProvinceContacts>'

END
GO
--End function eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceFinding data for a specific CommunityProvinceEngagementUpdate record
-- ==============================================================================================================

CREATE FUNCTION eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceFindings VARCHAR(MAX) = ''
	
	SELECT @cProvinceFindings = COALESCE(@cProvinceFindings, '') + D.ProvinceFinding
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceFinding'), ELEMENTS) AS ProvinceFinding
		FROM communityprovinceengagementupdate.ProvinceFinding T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceFindings>' + ISNULL(@cProvinceFindings, '') + '</ProvinceFindings>'

END
GO
--End function eventlog.GetProvinceFindingXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceIndicator data for a specific CommunityProvinceEngagementUpdate record
-- ================================================================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM communityprovinceengagementupdate.ProvinceIndicator T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'

END
GO
--End function eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceProject data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceProjects VARCHAR(MAX) = ''
	
	SELECT @cProvinceProjects = COALESCE(@cProvinceProjects, '') + D.ProvinceProject
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceProject'), ELEMENTS) AS ProvinceProject
		FROM communityprovinceengagementupdate.ProvinceProject T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceProjects>' + ISNULL(@cProvinceProjects, '') + '</ProvinceProjects>'

END
GO
--End function eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRecommendation data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRecommendations VARCHAR(MAX) = ''
	
	SELECT @cProvinceRecommendations = COALESCE(@cProvinceRecommendations, '') + D.ProvinceRecommendation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRecommendation'), ELEMENTS) AS ProvinceRecommendation
		FROM communityprovinceengagementupdate.ProvinceRecommendation T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceRecommendations>' + ISNULL(@cProvinceRecommendations, '') + '</ProvinceRecommendations>'

END
GO
--End function eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID

--Begin function eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A function to return ProvinceRisk data for a specific CommunityProvinceEngagementUpdate record
-- =================================================================================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID
(
@CommunityProvinceEngagementUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM communityprovinceengagementupdate.ProvinceRisk T 
		WHERE T.CommunityProvinceEngagementUpdateID = @CommunityProvinceEngagementUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'

END
GO
--End function eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID

