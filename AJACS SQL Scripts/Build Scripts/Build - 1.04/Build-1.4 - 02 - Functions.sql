USE AJACS
GO

--Begin function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A function to return document data for a specific entity record
--
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	Fixed a nodes bug
-- ============================================================================

CREATE FUNCTION eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cDocuments VARCHAR(MAX) = ''
	
	IF EXISTS (SELECT 1 FROM dbo.DocumentEntity T WHERE T.EntityTypeCode = @EntityTypeCode AND T.EntityID = @EntityID)
		BEGIN

		SELECT 
			@cDocuments = COALESCE(@cDocuments, '') + D.Document 
		FROM
			(
			SELECT
				(SELECT T.DocumentID FOR XML RAW('Documents'), ELEMENTS) AS Document
			FROM dbo.DocumentEntity T 
			WHERE T.EntityTypeCode = @EntityTypeCode
				AND T.EntityID = @EntityID
			) D
	
		IF @cDocuments IS NULL OR LEN(LTRIM(@cDocuments)) = 0
			SET @cDocuments = '<Documents></Documents>'
		--ENDIF

		END
	--ENDIF
		
	RETURN @cDocuments

END
GO
--End function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID