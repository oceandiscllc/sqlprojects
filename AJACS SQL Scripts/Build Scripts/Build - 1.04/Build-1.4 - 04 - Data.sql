USE AJACS
GO

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'SiteConfiguration')
	BEGIN
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('SiteConfiguration','Site Configuration', 9)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'SiteConfiguration')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(PermissionableCode, PermissionableName, DisplayOrder)
	VALUES
		('SiteConfiguration','Site Configuration', 0)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'ViewCFErrors',
		'View ColdFusion Errors',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'SiteConfiguration'

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'ManageServerSetupKeys',
		'Manage Server Setup Keys',
		2
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'SiteConfiguration'

	END
--ENDIF
GO
--End table permissionable.Permissionable
	
--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'SiteConfiguration'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('SiteConfiguration')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin Permissions Assignment
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (69,17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (69,17,26)

	END
--ENDIF
GO
--End Permissions Assignment