USE AJACS
GO

--Begin table dbo.MenuItem
UPDATE MI
SET MI.MenuItemLink = '/conceptnote/list'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'Activity'
GO

DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ConceptNote','ConceptNoteContactVettingList')
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.MenuItem MI
	WHERE MI.MenuItemID = MIPL.MenuItemID
	)
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.MenuItemCode IN ('BeneficiaryList','StaffPartnerList','StaffPartnerVettingList','CSWGList','ContactList','VettingList')
GO

DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('StaffPartnerList','CSWGList')
GO

UPDATE MI
SET
	MI.MenuItemCode = 'ContactList',
	MI.MenuItemLink = '/contact/list',
	MI.MenuItemText = 'Contacts'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'BeneficiaryList'
GO

UPDATE MI
SET
	MI.MenuItemCode = 'VettingList',
	MI.MenuItemLink = '/vetting/list',
	MI.MenuItemText = 'Vetting'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'StaffPartnerVettingList'
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID, PermissionableLineage)
SELECT 
	MI.MenuItemID,
	REPLACE(MI.MenuItemCode, 'List', '.List')
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ContactList','VettingList')
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Implementation'
GO
--End table dbo.MenuItem

--Begin table dropdown.ContactType
UPDATE CCT
SET CCT.ContactTypeID = (SELECT CT1.ContactTypeID FROM dropdown.ContactType CT1 WHERE CT1.ContactTypeName = 'Commander')
FROM dbo.ContactContactType CCT
	JOIN dropdown.ContactType CT2 ON CT2.ContactTypeID = CCT.ContactTypeID
		AND CT2.ContactTypeName = 'Deputy'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'CETeam',
	CT.ContactTypeName = 'CE Team'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'Beneficiary'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'PoliceStipend',
	CT.ContactTypeName = 'Police Stipend'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'Stipend'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'ProjectStaff',
	CT.ContactTypeName = 'Project Staff'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'StaffPartner'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'PartnersStakeholder',
	CT.ContactTypeName = 'Partners: Stakeholder'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeName = 'Commander'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'PartnersSupplierVendor',
	CT.ContactTypeName = 'Partners: Supplier/Vendor'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeName = 'Deputy'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Police Other')
	BEGIN
	
	INSERT INTO dropdown.ContactType
		(ContactTypeCode, ContactTypeName)
	VALUES
		('FieldStaff', 'Field Staff'),
		('IO4', 'IO4'),
		('JusticeOther', 'Justice Other'),
		('JusticeStipend', 'Justice Stipend'),
		('PoliceOther', 'Police Other'),
		('SubContractors', 'Sub-Contractors')

	END
--ENDIF
GO

UPDATE CT
SET CT.DisplayOrder = 
	CASE
		WHEN CT.ContactTypeName = 'CE Team' THEN 1
		WHEN CT.ContactTypeName = 'Police Stipend' THEN 2
		WHEN CT.ContactTypeName = 'Police Other' THEN 3
		WHEN CT.ContactTypeName = 'Justice Stipend' THEN 4
		WHEN CT.ContactTypeName = 'Justice Other' THEN 5
		WHEN CT.ContactTypeName = 'IO4' THEN 6
		WHEN CT.ContactTypeName = 'Partners: Supplier/Vendor' THEN 7
		WHEN CT.ContactTypeName = 'Partners: Stakeholder' THEN 8
		WHEN CT.ContactTypeName = 'Project Staff' THEN 9
		WHEN CT.ContactTypeName = 'Sub-Contractors' THEN 10
		WHEN CT.ContactTypeName = 'Field Staff' THEN 11
		ELSE CT.DisplayOrder
	END
FROM dropdown.ContactType CT
GO
--End table dropdown.ContactType

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Contact',
	'AddUpdate',
	CT.ContactTypeCode,
	'Add / edit contacts of type ' + CT.ContactTypeName,
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.AddUpdate.' + CT.ContactTypeCode
		)
GO

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Contact',
	'List',
	CT.ContactTypeCode,
	'Include contacts of type ' + CT.ContactTypeName + ' in the contact list',
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.List.' + CT.ContactTypeCode
		)
GO

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Contact',
	'View',
	CT.ContactTypeCode,
	'View contacts of type ' + CT.ContactTypeName,
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.View.' + CT.ContactTypeCode
		)
GO

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Vetting',
	'List',
	CT.ContactTypeCode,
	'Include contacts of type ' + CT.ContactTypeName + ' in the vetting list',
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Vetting.List.' + CT.ContactTypeCode
		)
GO
--End table permissionable.Permissionable















