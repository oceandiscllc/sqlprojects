-- File Name:	Build-1.51 File 01 - AJACS.sql
-- Build Key:	Build-1.51 File 01 - AJACS - 2016.04.01 01.33.04

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatActivityReferenceCode
--
-- Procedures:
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetConceptNoteFinanceTaskIDs
--		dbo.GetContactByContactID
--		dbo.GetMenuItemsByPersonID
--		permissionable.DeletePermissionable
--		permissionable.GetPermissionableByPermissionableID
--		reporting.RiskReport
--		utility.SavePermissionable
--		utility.SavePermissionableGroup
--		utility.UpdateSuperAdministratorPersonPermissionables
--
-- Tables:
--		dbo.ConceptNoteFinance
--		integration.CreativeFinancial
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'ConceptNoteFinanceTaskID', 'NVARCHAR(100)'
GO
--End table dbo.ConceptNote

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'UKVettingExpirationDate', 'DATE'
EXEC utility.AddColumn @TableName, 'USVettingExpirationDate', 'DATE'
GO

UPDATE C
SET 
	C.UKVettingExpirationDate = NULL,
	C.USVettingExpirationDate = NULL
FROM dbo.Contact C
GO

UPDATE C
SET C.UKVettingExpirationDate = DATEADD(m, 6, E.VettingDate)
FROM dbo.Contact C
	JOIN 
		(
		SELECT
			D.ContactID,
			D.VettingDate
		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (PARTITION BY CV.ContactID, CV.ContactVettingTypeID ORDER BY CV.ContactID, CV.VettingDate DESC) AS RowIndex,
				CV.ContactID,
				CV.VettingDate
			FROM AJACS.dbo.ContactVetting CV
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CVT.ContactVettingTypeName = 'UK'
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND VO.VettingOutcomeName = 'Consider'
			) D 
		WHERE D.RowIndex = 1
		) E ON E.ContactID = C.ContactID
GO

UPDATE C
SET C.USVettingExpirationDate = DATEADD(m, 6, E.VettingDate)
FROM dbo.Contact C
	JOIN 
		(
		SELECT
			D.ContactID,
			D.VettingDate
		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (PARTITION BY CV.ContactID, CV.ContactVettingTypeID ORDER BY CV.ContactID, CV.VettingDate DESC) AS RowIndex,
				CV.ContactID,
				CV.VettingDate
			FROM AJACS.dbo.ContactVetting CV
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CVT.ContactVettingTypeName = 'US'
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND VO.VettingOutcomeName = 'Consider'
			) D 
		WHERE D.RowIndex = 1
		) E ON E.ContactID = C.ContactID
GO
--End table dbo.Contact

--Begin table dbo.ContactVetting
DECLARE @TableName VARCHAR(250) = 'dbo.ContactVetting'

EXEC utility.AddColumn @TableName, 'Notes', 'VARCHAR(MAX)'
GO
--End table dbo.ContactVetting

--Begin table dbo.ConceptNoteFinance
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteFinance'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteFinance
	(
	ConceptNoteFinanceID INT IDENTITY(1,1) NOT NULL,
	TransactionID NVARCHAR(100) NOT NULL,
	CpnyID NVARCHAR(100),
	PerPost NVARCHAR(100),
	Acct NVARCHAR(100),
	Sub NVARCHAR(100),
	ProjectID NVARCHAR(100),
	TaskID NVARCHAR(100),
	BatNbr NVARCHAR(100),
	RefNbr NVARCHAR(100),
	Module NVARCHAR(100),
	LineNbr NVARCHAR(100),
	VendID NVARCHAR(100),
	DRAmt DECIMAL(32, 16) NOT NULL,
	CRAmt DECIMAL(32, 16) NOT NULL,
	CuryDRAmt DECIMAL(32, 16) NOT NULL,
	CuryCrAmt DECIMAL(32, 16) NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'DRAmt', 'DECIMAL(32, 16)', 0
EXEC utility.SetDefaultConstraint @TableName, 'CRAmt', 'DECIMAL(32, 16)', 0
EXEC utility.SetDefaultConstraint @TableName, 'CuryDRAmt', 'DECIMAL(32, 16)', 0
EXEC utility.SetDefaultConstraint @TableName, 'CuryCrAmt', 'DECIMAL(32, 16)', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteFinanceID'
GO
--End table dbo.ConceptNoteFinance

--Begin table dropdown.ContactType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactType'

EXEC utility.AddColumn @TableName, 'IsStipend', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsStipend', 'BIT', 0
GO

UPDATE CT
SET CT.IsStipend = 1
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode IN ('JusticeStipend','PoliceStipend')
GO
--End table dropdown.ContactType

--Begin table integration.CreativeFinancial
DECLARE @TableName VARCHAR(250) = 'integration.CreativeFinancial'

EXEC utility.DropObject @TableName

CREATE TABLE integration.CreativeFinancial
	(
	FinanceID INT IDENTITY(1,1) NOT NULL,
	TransactionID NVARCHAR(100),
	CpnyID NVARCHAR(100),
	PerPost NVARCHAR(100),
	Acct NVARCHAR(100),
	Sub NVARCHAR(100),
	ProjectID NVARCHAR(100),
	TaskID NVARCHAR(100),
	BatNbr NVARCHAR(100),
	RefNbr NVARCHAR(100),
	Module NVARCHAR(100),
	LineNbr NVARCHAR(100),
	VendID NVARCHAR(100),
	DRAmt NVARCHAR(100),
	CRAmt NVARCHAR(100),
	CuryDRAmt NVARCHAR(100),
	CuryCrAmt NVARCHAR(100)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'FinanceID'
GO
--End table integration.CreativeFinancial

--Begin table permissionable.xxx
EXEC utility.DropObject 'permissionable.DisplayGroup'
EXEC utility.DropObject 'permissionable.DisplayGroupPermissionable'
EXEC utility.DropObject 'permissionable.Permissionable2'
GO

DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.DropColumn @TableName, 'IsWorkflow'
EXEC utility.DropColumn @TableName, 'ParentPermissionableID'
EXEC utility.DropColumn @TableName, 'PermissionableCode'
EXEC utility.DropColumn @TableName, 'PermissionableName'
GO

DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.AddColumn @TableName, 'PermissionableLineage', 'VARCHAR(MAX)'
GO

UPDATE PTP
SET PTP.PermissionableLineage = P.PermissionableLineage
FROM permissionable.PermissionableTemplatePermissionable PTP
	JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.xxx


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatActivityReferenceCode
EXEC utility.DropObject 'dbo.FormatActivityReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- =========================================================================

CREATE FUNCTION dbo.FormatActivityReferenceCode
(
@ActivityID INT
)

RETURNS NVARCHAR(100)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(100) = (SELECT A.TaskCode FROM Activity.Activity A WHERE A.ActivityID = @ActivityID)
	DECLARE @cSystemName VARCHAR(50)

	IF @cReturn IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'
				
		SET @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ActivityID AS VARCHAR(10)), 4)

		END
	--ENDIF

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatActivityReferenceCode

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.USVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.USVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.UKVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.UKVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID
			FROM dbo.ContactVetting CV12
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID
			FROM dbo.ContactVetting CV22
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1
				
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetConceptNoteFinanceTaskIDs
EXEC Utility.DropObject 'dbo.GetConceptNoteFinanceTaskIDs'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2016.03.31
-- Description:	A stored procedure to data from the dbo.ConceptNoteFinance table
-- =============================================================================
CREATE PROCEDURE dbo.GetConceptNoteFinanceTaskIDs

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT CNF.TaskID
	FROM dbo.ConceptNoteFinance CNF
	ORDER BY CNF.TaskID
	
END
GO
--End procedure dbo.GetConceptNoteFinanceTaskIDs

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.CommunityAssetID,		
		C1.CommunityAssetUnitID,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,		
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,		
		C1.CommunityID,		
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate(
			(SELECT CreateDateTime 
			FROM eventlog.EventLog 
			WHERE eventcode = 'create'
  			AND entitytypecode = 'contact'
  			AND entityid = C1.ContactID)
		)  AS InitialEntryDateFormatted,
		C1.IsActive,		
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.ProvinceID,		
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(
			SELECT CommunitySubGroupName
			FROM dropdown.CommunitySubGroup
			WHERE CommunitySubGroupID = 
				(
				SELECT CommunitySubGroupID 
				FROM dbo.Community
				WHERE CommunityID = C1.CommunityID
				)
		) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID, 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
	WHERE CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
	--Get Document By Contact ID
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID
	ORDER BY D.DocumentDescription

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR NOT EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
					OR NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure permissionable.DeletePermissionable
EXEC Utility.DropObject 'permissionable.DeletePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.3.15
-- Description:	A stored procedure to delete data from the permissionable.Permissionable and person.PersonPermissionable tables
-- ============================================================================================================================
CREATE PROCEDURE permissionable.DeletePermissionable

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPermissionableLineage VARCHAR(MAX)

	SELECT
		@cPermissionableLineage = P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE P
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID

	DELETE MIPL
	FROM dbo.MenuItemPermissionableLineage MIPL
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = MIPL.PermissionableLineage
		)

	DELETE PP
	FROM permissionable.PersonPermissionable PP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PP.PermissionableLineage
		)

	DELETE PTP
	FROM permissionable.PermissionableTemplatePermissionable PTP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = PTP.PermissionableLineage
		)

	SELECT @cPermissionableLineage AS PermissionableLineage
		
END
GO
--End procedure permissionable.DeletePermissionable

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC Utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Brandon Green
-- Create date:	2015.10.02
-- Description:	A stored procedure to data from the permissionable.Permissionable table
-- ====================================================================================
CREATE PROCEDURE permissionable.GetPermissionableByPermissionableID

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PermissionableID,
		P.ControllerName,
		P.MethodName,
		P.PermissionCode,
		P.PermissionableGroupID,
		P.Description,
		P.IsActive,
		P.IsGlobal,
		P.DisplayOrder
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID
		
END
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure reporting.RiskReport
EXEC Utility.DropObject 'reporting.RiskReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure to get CommunityEngagementReport data from the eventlog table
-- =============================================================================================
CREATE PROCEDURE reporting.RiskReport

 @StartDate Date , @EndDate Date

AS
BEGIN
	

		SELECT DISTINCT D.ReportType, D.SubreportType,D.EntityLocationName, D.EntityName, D.Author, D.Notes, D.Value, D.Updatedate, D.EntityID, D.EntityLocationID
		FROM
		(



		SELECT 
		 'Police Engagement' as ReportType,
		  'Community Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.PoliceEngagementRiskNotes as Notes,
		E.PoliceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
		 P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E

		JOIN Community C ON C.CommunityID = E.CommunityID 
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'Police Engagement' as ReportType,
		  'Province Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.PoliceEngagementRiskNotes as Notes,
		E.PoliceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
		 P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID
		JOIN Risk R ON R.RiskID = E.RiskID

		Union ALL

		SELECT 
		 'Community Engagement' as ReportType,
		  'Province Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.CommunityProvinceEngagementNotes as Notes,
		E.CommunityProvinceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes,
		 P.value('CommunityProvinceEngagementRiskValue[1]', 'nvarchar(max)') AS CommunityProvinceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID 
		JOIN Risk R ON R.RiskID = E.RiskID
		UNION ALL

		SELECT

		 'Community Engagement' as ReportType,
		 'Community Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.CommunityProvinceEngagementNotes as Notes, 
		E.CommunityProvinceEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author , 
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM
		(

		SELECT
		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes,
		 P.value('CommunityProvinceEngagementRiskValue[1]', 'nvarchar(max)') AS CommunityProvinceEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime
		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E
		JOIN Community C ON C.CommunityID = E.CommunityID 
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL

		SELECT 
		 'FIF Engagement' as ReportType,
		  'Community FIF Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.FIFEngagementRiskNotes as Notes,
		E.FIFEngagementRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('FIFNotes[1]', 'nvarchar(max)') AS FIFEngagementRiskNotes,
		 P.value('FIFRiskValue[1]', 'nvarchar(max)') AS FIFEngagementRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E

		JOIN Community C ON C.CommunityID = E.CommunityID 
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'FIF Engagement' as ReportType,
		  'Province FIF Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.FIFRiskNotes as Notes,
		E.FIFRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('FIFNotes[1]', 'nvarchar(max)') AS FIFRiskNotes,
		 P.value('FIFRiskValue[1]', 'nvarchar(max)') AS FIFRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID 
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'Justice Engagement' as ReportType,
		  'Community Justice Risk' as SubReportType,
		C.CommunityName as EntityLocationName,
		E.CommunityID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.JusticeRiskNotes as Notes,
		E.JusticeRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('JusticeNotes[1]', 'nvarchar(max)') AS JusticeRiskNotes,
		 P.value('JusticeRiskValue[1]', 'nvarchar(max)') AS JusticeRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
		) E

		JOIN Community C ON C.CommunityID = E.CommunityID 
		JOIN Risk R ON R.RiskID = E.RiskID


		UNION ALL


		SELECT 
		 'Justice Engagement' as ReportType,
		  'Province Justice Risk' as SubReportType,
		P.ProvinceName as EntityLocationName,
		E.ProvinceID as EntityLocationID,
		r.RiskName as EntityName , 
		r.RiskID as EntityID , 
		E.JusticeRiskNotes as Notes,
		E.JusticeRiskValue as Value,
		dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') as Author ,  
		dbo.FormatDateTime( E.CreateDateTime) as UpdateDate

		FROM 
		(
		SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('JusticeNotes[1]', 'nvarchar(max)') AS JusticeRiskNotes,
		 P.value('JusticeRiskValue[1]', 'nvarchar(max)') AS JusticeRiskValue,
		 E.PersonID , 
		 E.CreateDateTime

		FROM eventlog.EventLog E
		CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
		) E

		JOIN Province P ON P.ProvinceID = E.ProvinceID 
		JOIN Risk R ON R.RiskID = E.RiskID

		) D

		ORDER BY D.ReportType ,D.SubReportType , D.EntityLocationName, D.EntityName, D.Author , D.UpdateDate

END
GO
--End procedure reporting.RiskReport

--Begin procedure utility.SavePermissionable
EXEC utility.DropObject 'utility.SavePermissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to save data to the permissionable.Permissionable table
-- =======================================================================================
CREATE PROCEDURE utility.SavePermissionable

@ControllerName VARCHAR(50),
@MethodName VARCHAR(250),
@PermissionCode VARCHAR(250) = NULL,
@PermissionableLineage VARCHAR(MAX), 
@Description VARCHAR(MAX), 
@IsGlobal BIT = 0, 
@DisplayOrder INT = 0,
@PermissionableGroupCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = @PermissionableLineage)
		BEGIN
		
		UPDATE P 
		SET 
			P.PermissionableGroupID = ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			P.Description = @Description, 
			P.IsGlobal = @IsGlobal, 
			P.DisplayOrder = @DisplayOrder
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = @PermissionableLineage
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.Permissionable 
			(ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsGlobal, DisplayOrder) 
		VALUES 
			(
			@ControllerName, 
			@MethodName, 
			@PermissionCode,
			ISNULL((SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode), 0),
			@Description, 
			@IsGlobal, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure utility.SavePermissionable

--Begin procedure utility.SavePermissionableGroup
EXEC utility.DropObject 'utility.SavePermissionableGroup'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to save data to the permissionable.PermissionableGroup table
-- ============================================================================================
CREATE PROCEDURE utility.SavePermissionableGroup

@PermissionableGroupCode VARCHAR(50),
@PermissionableGroupName VARCHAR(250),
@DisplayOrder INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupCode = @PermissionableGroupCode)
		BEGIN
		
		UPDATE PG
		SET 
			PG.PermissionableGroupName = @PermissionableGroupName,
			PG.DisplayOrder = @DisplayOrder
		FROM permissionable.PermissionableGroup PG
		WHERE PG.PermissionableGroupCode = @PermissionableGroupCode
		
		END
	ELSE
		BEGIN
		
		INSERT INTO permissionable.PermissionableGroup 
			(PermissionableGroupCode, PermissionableGroupName, DisplayOrder) 
		VALUES 
			(
			@PermissionableGroupCode, 
			@PermissionableGroupName, 
			@DisplayOrder
			)
			
		END
	--ENDIF

END	
GO
--End procedure utility.SavePermissionableGroup

--Begin procedure utility.UpdateSuperAdministratorPersonPermissionables
EXEC utility.DropObject 'utility.UpdateSuperAdministratorPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to update PersonPermissionable data for Super Admins
-- ====================================================================================
CREATE PROCEDURE utility.UpdateSuperAdministratorPersonPermissionables

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM permissionable.PersonPermissionable PP
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND P.IsSuperAdministrator = 1
	
	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1
		CROSS JOIN permissionable.Permissionable P2
	WHERE P1.IsSuperAdministrator = 1

END	
GO
--End procedure utility.UpdateSuperAdministratorPersonPermissionables

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.MenuItem
UPDATE MI
SET MI.MenuItemLink = '/conceptnote/list'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'Activity'
GO

DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ConceptNote','ConceptNoteContactVettingList')
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
WHERE NOT EXISTS
	(
	SELECT 1
	FROM dbo.MenuItem MI
	WHERE MI.MenuItemID = MIPL.MenuItemID
	)
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.MenuItemCode IN ('BeneficiaryList','StaffPartnerList','StaffPartnerVettingList','CSWGList','ContactList','VettingList')
GO

DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('StaffPartnerList','CSWGList')
GO

UPDATE MI
SET
	MI.MenuItemCode = 'ContactList',
	MI.MenuItemLink = '/contact/list',
	MI.MenuItemText = 'Contacts'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'BeneficiaryList'
GO

UPDATE MI
SET
	MI.MenuItemCode = 'VettingList',
	MI.MenuItemLink = '/vetting/list',
	MI.MenuItemText = 'Vetting'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'StaffPartnerVettingList'
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID, PermissionableLineage)
SELECT 
	MI.MenuItemID,
	REPLACE(MI.MenuItemCode, 'List', '.List')
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('ContactList','VettingList')
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Implementation'
GO
--End table dbo.MenuItem

--Begin table dropdown.ContactType
UPDATE CCT
SET CCT.ContactTypeID = (SELECT CT1.ContactTypeID FROM dropdown.ContactType CT1 WHERE CT1.ContactTypeName = 'Commander')
FROM dbo.ContactContactType CCT
	JOIN dropdown.ContactType CT2 ON CT2.ContactTypeID = CCT.ContactTypeID
		AND CT2.ContactTypeName = 'Deputy'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'CETeam',
	CT.ContactTypeName = 'CE Team'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'Beneficiary'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'PoliceStipend',
	CT.ContactTypeName = 'Police Stipend'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'Stipend'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'ProjectStaff',
	CT.ContactTypeName = 'Project Staff'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'StaffPartner'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'PartnersStakeholder',
	CT.ContactTypeName = 'Partners: Stakeholder'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeName = 'Commander'
GO

UPDATE CT
SET 
	CT.ContactTypeCode = 'PartnersSupplierVendor',
	CT.ContactTypeName = 'Partners: Supplier/Vendor'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeName = 'Deputy'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Police Other')
	BEGIN
	
	INSERT INTO dropdown.ContactType
		(ContactTypeCode, ContactTypeName)
	VALUES
		('FieldStaff', 'Field Staff'),
		('IO4', 'IO4'),
		('JusticeOther', 'Justice Other'),
		('JusticeStipend', 'Justice Stipend'),
		('PoliceOther', 'Police Other'),
		('SubContractors', 'Sub-Contractors')

	END
--ENDIF
GO

UPDATE CT
SET CT.DisplayOrder = 
	CASE
		WHEN CT.ContactTypeName = 'CE Team' THEN 1
		WHEN CT.ContactTypeName = 'Police Stipend' THEN 2
		WHEN CT.ContactTypeName = 'Police Other' THEN 3
		WHEN CT.ContactTypeName = 'Justice Stipend' THEN 4
		WHEN CT.ContactTypeName = 'Justice Other' THEN 5
		WHEN CT.ContactTypeName = 'IO4' THEN 6
		WHEN CT.ContactTypeName = 'Partners: Supplier/Vendor' THEN 7
		WHEN CT.ContactTypeName = 'Partners: Stakeholder' THEN 8
		WHEN CT.ContactTypeName = 'Project Staff' THEN 9
		WHEN CT.ContactTypeName = 'Sub-Contractors' THEN 10
		WHEN CT.ContactTypeName = 'Field Staff' THEN 11
		ELSE CT.DisplayOrder
	END
FROM dropdown.ContactType CT
GO
--End table dropdown.ContactType

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Contact',
	'AddUpdate',
	CT.ContactTypeCode,
	'Add / edit contacts of type ' + CT.ContactTypeName,
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.AddUpdate.' + CT.ContactTypeCode
		)
GO

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Contact',
	'List',
	CT.ContactTypeCode,
	'Include contacts of type ' + CT.ContactTypeName + ' in the contact list',
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.List.' + CT.ContactTypeCode
		)
GO

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Contact',
	'View',
	CT.ContactTypeCode,
	'View contacts of type ' + CT.ContactTypeName,
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.View.' + CT.ContactTypeCode
		)
GO

INSERT INTO permissionable.Permissionable
	(ControllerName, MethodName, PermissionCode, Description, PermissionableGroupID)
SELECT
	'Vetting',
	'List',
	CT.ContactTypeCode,
	'Include contacts of type ' + CT.ContactTypeName + ' in the vetting list',
	(SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Contact')
FROM dropdown.ContactType CT 
WHERE CT.ContactTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Vetting.List.' + CT.ContactTypeCode
		)
GO
--End table permissionable.Permissionable
















--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Community', 'Community', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contact', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'Implementation', 'Implementation', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Insight & Understanding', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Province', 'Province', 0
GO
EXEC utility.SavePermissionableGroup 'RapidAssessments', 'Rapid Assessments', 0
GO
EXEC utility.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflows', 'Workflows', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Data Export', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'DataImport', 'Default', NULL, 'DataImport.Default', 'Data Import', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'AddUpdate', NULL, 'EmailTemplateAdministration.AddUpdate', 'Add / Edit EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'List EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'View', NULL, 'EmailTemplateAdministration.View', 'View EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'List EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Exports', 'BusinessLicenseReport', NULL, 'Exports.BusinessLicenseReport', 'Business License Report Exports', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors SiteConfiguration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'Delete', NULL, 'Permissionable.Delete', 'Delete a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / Edit PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'List PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / Edit Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'Delete', NULL, 'Person.Delete', 'Delete Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / Edit ServerSetup', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'List ServerSetup', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Community', 'AddUpdate', NULL, 'Community.AddUpdate', 'Add / Edit Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'List Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'Analysis Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Export', 'Community.View.Export', 'Export Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'ExportEquipmentDistribution', 'Community.View.ExportEquipmentDistribution', 'Export Equipment Distributions Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'Implementation Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'Information Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'Beneficiary', 'Contact.AddUpdate.Beneficiary', 'Contact.AddUpdate.Beneficiary', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'CETeam', 'Contact.AddUpdate.CETeam', 'Add / edit contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'CSWGMember', 'Contact.AddUpdate.CSWGMember', 'Contact.AddUpdate.CSWGMember', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'FieldStaff', 'Contact.AddUpdate.FieldStaff', 'Add / edit contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'IO4', 'Contact.AddUpdate.IO4', 'Add / edit contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeOther', 'Contact.AddUpdate.JusticeOther', 'Add / edit contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeStipend', 'Contact.AddUpdate.JusticeStipend', 'Add / edit contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersStakeholder', 'Contact.AddUpdate.PartnersStakeholder', 'Add / edit contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersSupplierVendor', 'Contact.AddUpdate.PartnersSupplierVendor', 'Add / edit contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceOther', 'Contact.AddUpdate.PoliceOther', 'Add / edit contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceStipend', 'Contact.AddUpdate.PoliceStipend', 'Add / edit contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'ProjectStaff', 'Contact.AddUpdate.ProjectStaff', 'Add / edit contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'StaffPartner', 'Contact.AddUpdate.StaffPartner', 'Contact.AddUpdate.StaffPartner', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'Stipend', 'Contact.AddUpdate.Stipend', 'Contact.AddUpdate.Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'SubContractors', 'Contact.AddUpdate.SubContractors', 'Add / edit contacts of type Sub-Contractors', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'ImportPayees', NULL, 'Contact.ImportPayees', 'Contact.ImportPayees', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'Beneficiary', 'Contact.List.Beneficiary', 'Contact.List.Beneficiary', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'CETeam', 'Contact.List.CETeam', 'Include contacts of type CE Team in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'CSWGMemberVetting', 'Contact.List.CSWGMemberVetting', 'Contact.List.CSWGMemberVetting', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ExportPayees', 'Contact.List.ExportPayees', 'Contact.List.ExportPayees', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'FieldStaff', 'Contact.List.FieldStaff', 'Include contacts of type Field Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'IO4', 'Contact.List.IO4', 'Include contacts of type IO4 in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'IsActive', 'Contact.List.IsActive', 'Contact.List.IsActive', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeOther', 'Contact.List.JusticeOther', 'Include contacts of type Justice Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeStipend', 'Contact.List.JusticeStipend', 'Include contacts of type Justice Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersStakeholder', 'Contact.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersSupplierVendor', 'Contact.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceOther', 'Contact.List.PoliceOther', 'Include contacts of type Police Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceStipend', 'Contact.List.PoliceStipend', 'Include contacts of type Police Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ProjectStaff', 'Contact.List.ProjectStaff', 'Include contacts of type Project Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'StaffPartner', 'Contact.List.StaffPartner', 'Contact.List.StaffPartner', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'StaffPartnerVetting', 'Contact.List.StaffPartnerVetting', 'Contact.List.StaffPartnerVetting', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'Stipend', 'Contact.List.Stipend', 'Contact.List.Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'SubContractors', 'Contact.List.SubContractors', 'Include contacts of type Sub-Contractors in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'UpdateVetting', 'Contact.List.UpdateVetting', 'Contact.List.UpdateVetting', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', NULL, 'Contact.PaymentList', 'Contact.PaymentList', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'CashHandOverExport', 'Contact.PaymentList.CashHandOverExport', 'Contact.PaymentList.CashHandOverExport', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'OpFundsReport', 'Contact.PaymentList.OpFundsReport', 'Contact.PaymentList.OpFundsReport', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendActivity', 'Contact.PaymentList.StipendActivity', 'Contact.PaymentList.StipendActivity', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendPaymentReport', 'Contact.PaymentList.StipendPaymentReport', 'Contact.PaymentList.StipendPaymentReport', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'CETeam', 'Contact.View.CETeam', 'View contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'FieldStaff', 'Contact.View.FieldStaff', 'View contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'IO4', 'Contact.View.IO4', 'View contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeOther', 'Contact.View.JusticeOther', 'View contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeStipend', 'Contact.View.JusticeStipend', 'View contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersStakeholder', 'Contact.View.PartnersStakeholder', 'View contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersSupplierVendor', 'Contact.View.PartnersSupplierVendor', 'View contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceOther', 'Contact.View.PoliceOther', 'View contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceStipend', 'Contact.View.PoliceStipend', 'View contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'ProjectStaff', 'Contact.View.ProjectStaff', 'View contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'SubContractors', 'Contact.View.SubContractors', 'View contacts of type Sub-Contractors', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'VettingMoreInfo', 'Contact.View.VettingMoreInfo', 'View the more info button on the vetting history data table', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'CETeam', 'Vetting.List.CETeam', 'Include contacts of type CE Team in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'FieldStaff', 'Vetting.List.FieldStaff', 'Include contacts of type Field Staff in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'IO4', 'Vetting.List.IO4', 'Include contacts of type IO4 in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeOther', 'Vetting.List.JusticeOther', 'Include contacts of type Justice Other in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeStipend', 'Vetting.List.JusticeStipend', 'Include contacts of type Justice Stipend in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersStakeholder', 'Vetting.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersSupplierVendor', 'Vetting.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceOther', 'Vetting.List.PoliceOther', 'Include contacts of type Police Other in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceStipend', 'Vetting.List.PoliceStipend', 'Include contacts of type Police Stipend in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'ProjectStaff', 'Vetting.List.ProjectStaff', 'Include contacts of type Project Staff in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'SubContractors', 'Vetting.List.SubContractors', 'Include contacts of type Sub-Contractors in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', NULL, 'Document.AddUpdate', '', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '004', 'Document.AddUpdate.004', 'Add / Edit 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '100', 'Document.AddUpdate.100', 'Add / Edit 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '101', 'Document.AddUpdate.101', 'Add / Edit 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '102', 'Document.AddUpdate.102', 'Add / Edit 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '103', 'Document.AddUpdate.103', 'Add / Edit 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '104', 'Document.AddUpdate.104', 'Add / Edit 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '105', 'Document.AddUpdate.105', 'Add / Edit 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '106', 'Document.AddUpdate.106', 'Add / Edit 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '107', 'Document.AddUpdate.107', 'Add / Edit 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '108', 'Document.AddUpdate.108', 'Add / Edit 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '109', 'Document.AddUpdate.109', 'Add / Edit 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '301', 'Document.AddUpdate.301', 'Add / Edit 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '302', 'Document.AddUpdate.302', 'Add / Edit 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '303', 'Document.AddUpdate.303', 'Add / Edit 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '304', 'Document.AddUpdate.304', 'Add / Edit 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '306', 'Document.AddUpdate.306', 'Add / Edit 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '500', 'Document.AddUpdate.500', 'Add / Edit 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '501', 'Document.AddUpdate.501', 'Add / Edit 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '502', 'Document.AddUpdate.502', 'Add / Edit 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '503', 'Document.AddUpdate.503', 'Add / Edit 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '504', 'Document.AddUpdate.504', 'Add / Edit 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '505', 'Document.AddUpdate.505', 'Add / Edit 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '506', 'Document.AddUpdate.506', 'Add / Edit 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '507', 'Document.AddUpdate.507', 'Add / Edit 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '508', 'Document.AddUpdate.508', 'Add / Edit 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '509', 'Document.AddUpdate.509', 'Add / Edit 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '510', 'Document.AddUpdate.510', 'Add / Edit 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '511', 'Document.AddUpdate.511', 'Add / Edit 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '512', 'Document.AddUpdate.512', 'Add / Edit 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '513', 'Document.AddUpdate.513', 'Add / Edit 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '514', 'Document.AddUpdate.514', 'Add / Edit 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '515', 'Document.AddUpdate.515', 'Add / Edit 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '516', 'Document.AddUpdate.516', 'Add / Edit 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '517', 'Document.AddUpdate.517', 'Add / Edit 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '518', 'Document.AddUpdate.518', 'Add / Edit 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '519', 'Document.AddUpdate.519', 'Add / Edit 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '520', 'Document.AddUpdate.520', 'Add / Edit 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '521', 'Document.AddUpdate.521', 'Add / Edit 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '600', 'Document.AddUpdate.600', 'Add / Edit 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '601', 'Document.AddUpdate.601', 'Add / Edit 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '602', 'Document.AddUpdate.602', 'Add / Edit 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '603', 'Document.AddUpdate.603', 'Add / Edit 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '604', 'Document.AddUpdate.604', 'Add / Edit 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '605', 'Document.AddUpdate.605', 'Add / Edit 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '700', 'Document.AddUpdate.700', 'Add / Edit 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '701', 'Document.AddUpdate.701', 'Add / Edit 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '702', 'Document.AddUpdate.702', 'Add / Edit 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '801', 'Document.AddUpdate.801', 'Add / Edit 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '802', 'Document.AddUpdate.802', 'Add / Edit 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '803', 'Document.AddUpdate.803', 'Add / Edit 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '804', 'Document.AddUpdate.804', 'Add / Edit 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '805', 'Document.AddUpdate.805', 'Add / Edit 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '806', 'Document.AddUpdate.806', 'Add / Edit 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '807', 'Document.AddUpdate.807', 'Add / Edit 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '808', 'Document.AddUpdate.808', 'Add / Edit 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '900', 'Document.AddUpdate.900', 'Add / Edit 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '901', 'Document.AddUpdate.901', 'Add / Edit 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '902', 'Document.AddUpdate.902', 'Add / Edit 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '903', 'Document.AddUpdate.903', 'Add / Edit 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '904', 'Document.AddUpdate.904', 'Add / Edit 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '905', 'Document.AddUpdate.905', 'Add / Edit 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '906', 'Document.AddUpdate.906', 'Add / Edit 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '907', 'Document.AddUpdate.907', 'Add / Edit 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '908', 'Document.AddUpdate.908', 'Add / Edit 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '909', 'Document.AddUpdate.909', 'Add / Edit 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '910', 'Document.AddUpdate.910', 'Add / Edit 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '004', 'Document.View.004', 'View 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '100', 'Document.View.100', 'View 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '101', 'Document.View.101', 'View 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '102', 'Document.View.102', 'View 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '103', 'Document.View.103', 'View 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '104', 'Document.View.104', 'View 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '105', 'Document.View.105', 'View 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '106', 'Document.View.106', 'View 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '107', 'Document.View.107', 'View 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '108', 'Document.View.108', 'View 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '109', 'Document.View.109', 'View 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '301', 'Document.View.301', 'View 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '302', 'Document.View.302', 'View 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '303', 'Document.View.303', 'View 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '304', 'Document.View.304', 'View 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '306', 'Document.View.306', 'View 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '500', 'Document.View.500', 'View 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '501', 'Document.View.501', 'View 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '502', 'Document.View.502', 'View 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '503', 'Document.View.503', 'View 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '504', 'Document.View.504', 'View 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '505', 'Document.View.505', 'View 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '506', 'Document.View.506', 'View 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '507', 'Document.View.507', 'View 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '508', 'Document.View.508', 'View 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '509', 'Document.View.509', 'View 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '510', 'Document.View.510', 'View 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '511', 'Document.View.511', 'View 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '512', 'Document.View.512', 'View 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '513', 'Document.View.513', 'View 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '514', 'Document.View.514', 'View 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '515', 'Document.View.515', 'View 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '516', 'Document.View.516', 'View 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '517', 'Document.View.517', 'View 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '518', 'Document.View.518', 'View 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '519', 'Document.View.519', 'View 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '520', 'Document.View.520', 'View 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '521', 'Document.View.521', 'View 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '600', 'Document.View.600', 'View 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '601', 'Document.View.601', 'View 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '602', 'Document.View.602', 'View 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '603', 'Document.View.603', 'View 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '604', 'Document.View.604', 'View 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '605', 'Document.View.605', 'View 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '700', 'Document.View.700', 'View 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '701', 'Document.View.701', 'View 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '702', 'Document.View.702', 'View 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '801', 'Document.View.801', 'View 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '802', 'Document.View.802', 'View 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '803', 'Document.View.803', 'View 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '804', 'Document.View.804', 'View 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '805', 'Document.View.805', 'View 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '806', 'Document.View.806', 'View 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '807', 'Document.View.807', 'View 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '808', 'Document.View.808', 'View 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '900', 'Document.View.900', 'View 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '901', 'Document.View.901', 'View 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '902', 'Document.View.902', 'View 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '903', 'Document.View.903', 'View 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '904', 'Document.View.904', 'View 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '905', 'Document.View.905', 'View 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '906', 'Document.View.906', 'View 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '907', 'Document.View.907', 'View 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '908', 'Document.View.908', 'View 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '909', 'Document.View.909', 'View 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '910', 'Document.View.910', 'View 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateDecision', NULL, 'DonorDecision.AddUpdateDecision', 'Add / Edit Donor Decisions DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateMeeting', NULL, 'DonorDecision.AddUpdateMeeting', 'Add / Edit Donor Meetings & Actions DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'List', NULL, 'DonorDecision.List', 'List Donor Decisions, Meetings & Actions DonorDecision', 1, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'View', NULL, 'DonorDecision.View', 'View DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'AddUpdate', NULL, 'EquipmentCatalog.AddUpdate', 'Add / Edit EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'List', NULL, 'EquipmentCatalog.List', 'List EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'View', NULL, 'EquipmentCatalog.View', 'View EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Audit', NULL, 'EquipmentDistribution.Audit', 'EquipmentDistribution.Audit', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'EquipmentDistribution.Create', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'FinalizeEquipmentDistribution', NULL, 'EquipmentDistribution.FinalizeEquipmentDistribution', 'EquipmentDistribution.FinalizeEquipmentDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistributedInventory', NULL, 'EquipmentDistribution.ListDistributedInventory', 'EquipmentDistribution.ListDistributedInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistribution', NULL, 'EquipmentDistribution.ListDistribution', 'EquipmentDistribution.ListDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListInventory', NULL, 'EquipmentDistribution.ListInventory', 'EquipmentDistribution.ListInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'SetDeliveryDate', NULL, 'EquipmentDistribution.SetDeliveryDate', 'EquipmentDistribution.SetDeliveryDate', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Transfer', NULL, 'EquipmentDistribution.Transfer', 'EquipmentDistribution.Transfer', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'AddUpdate', NULL, 'EquipmentDistributionPlan.AddUpdate', 'Add / Edit EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'List', NULL, 'EquipmentDistributionPlan.List', 'List EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', NULL, 'EquipmentDistributionPlan.View', 'View EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', 'EquipmentDistributionPlan', 'EquipmentDistributionPlan.View.EquipmentDistributionPlan', 'Export Equipment Distribution Plan EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', 'EquipmentHandoverSheet', 'EquipmentDistributionPlan.View.EquipmentHandoverSheet', 'Export Equipment Handover Sheet EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / Edit EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'List EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', 'Export', 'EquipmentInventory.List.Export', 'Export EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'Audit', NULL, 'EquipmentManagement.Audit', 'Audit Equipment EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'List', NULL, 'EquipmentManagement.List', 'List Equipment Locations EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'AddUpdate', NULL, 'CommunityAsset.AddUpdate', 'Add / Edit CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'List', NULL, 'CommunityAsset.List', 'List CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'View', NULL, 'CommunityAsset.View', 'View CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'AddUpdate', NULL, 'CommunityMemberSurvey.AddUpdate', 'Add / Edit CommunityMemberSurvey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'View', NULL, 'CommunityMemberSurvey.View', 'View CommunityMemberSurvey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagement', 'AddUpdate', NULL, 'CommunityProvinceEngagement.AddUpdate', 'Add / Edit a Community Province Engagement Report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagementUpdate', 'Export', NULL, 'CommunityProvinceEngagementUpdate.Export', 'Export CommunityProvinceEngagementUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'FIF', 'AddUpdate', NULL, 'FIF.AddUpdate', 'Add/Edit', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'FIFUpdate', 'View', NULL, 'FIFUpdate.View', 'View FIFUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Justice', 'AddUpdate', NULL, 'Justice.AddUpdate', 'Add / Edit a Justice Report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'AddUpdate', NULL, 'KeyEvent.AddUpdate', 'Add / Edit KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'List', NULL, 'KeyEvent.List', 'List KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'View', NULL, 'KeyEvent.View', 'View KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'PoliceEngagement', 'AddUpdate', NULL, 'PoliceEngagement.AddUpdate', 'Add / Edit a Police Engagement Report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'AddUpdate', NULL, 'Project.AddUpdate', 'Add / Edit Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'List', NULL, 'Project.List', 'List Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'View', NULL, 'Project.View', 'View Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'AddUpdate', NULL, 'Atmospheric.AddUpdate', 'Add / Edit Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'List Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'AddUpdate', NULL, 'Finding.AddUpdate', 'Add / Edit Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'List Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'AddUpdate', NULL, 'Force.AddUpdate', 'Add / edit a force Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'List Forces Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'AddUpdate', NULL, 'Incident.AddUpdate', 'Add / Edit Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'List Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'AddUpdate', NULL, 'Recommendation.AddUpdate', 'Add / Edit Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'List Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', 'Export', 'Recommendation.List.Export', 'Export Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Add', NULL, 'RequestForInformation.Add', 'Add RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'List RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', 'Export', 'RequestForInformation.List.Export', 'Export RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', NULL, 'RequestForInformation.Update', 'Edit RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', 'Amend', 'RequestForInformation.Update.Amend', 'Amend RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', NULL, 'Risk.AddUpdate', 'Add / Edit Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'List Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', 'Export', 'Risk.List.Export', 'Export Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', NULL, 'SpotReport.AddUpdate', 'Add / Edit SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', 'Amend', 'SpotReport.AddUpdate.Amend', 'Amend SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'List SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Approved', 'SpotReport.View.Approved', 'Approved SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Export', 'SpotReport.View.Export', 'Export SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'InWork', 'SpotReport.View.InWork', 'In Work SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Add / Edit Questions SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Add / Edit Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Administer Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Export Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListQuestions', NULL, 'SurveyManagement.ListQuestions', 'Questions List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveyResponses', NULL, 'SurveyManagement.ListSurveyResponses', 'List Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Surveys List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'View Questions SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'View Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', NULL, 'WeeklyReport.AddUpdate', 'Add / Edit WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', 'Export', 'WeeklyReport.AddUpdate.Export', 'Export WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'List', NULL, 'WeeklyReport.List', 'List WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'View', NULL, 'WeeklyReport.View', 'View WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Indicator', 'AddUpdate', NULL, 'Indicator.AddUpdate', 'Add / Edit Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'List', NULL, 'Indicator.List', 'List Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'View', NULL, 'Indicator.View', 'View Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'AddUpdate', NULL, 'IndicatorType.AddUpdate', 'Add / Edit IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'List', NULL, 'IndicatorType.List', 'List IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'View', NULL, 'IndicatorType.View', 'View IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'AddUpdate', NULL, 'Milestone.AddUpdate', 'Add / Edit Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'List', NULL, 'Milestone.List', 'List Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'View', NULL, 'Milestone.View', 'View Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Monitoring', 'Overview', NULL, 'Monitoring.Overview', 'Add/Edit', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'AddUpdate', NULL, 'Objective.AddUpdate', 'Add / Edit Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', NULL, 'Objective.ChartList', 'M & E Overview Charts Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'List', NULL, 'Objective.List', 'List Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage Objectives & Indicators Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'Overview Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'View', NULL, 'Objective.View', 'View Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', NULL, 'ConceptNote.AddUpdate', 'Add / Edit ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'ExportConceptNoteBudget', 'ConceptNote.AddUpdate.ExportConceptNoteBudget', 'Export Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'Finances', 'ConceptNote.AddUpdate.Finances', 'Add / Edit Activity Finances', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', NULL, 'ConceptNote.List', 'List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', 'Export', 'ConceptNote.List.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', NULL, 'ConceptNote.VettingList', 'Vetting List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', 'ExportVetting', 'ConceptNote.VettingList.ExportVetting', 'Export Vetting ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', NULL, 'ConceptNote.View', 'View ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'Export', 'ConceptNote.View.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'ViewBudget', 'ConceptNote.View.ViewBudget', 'View Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'AddUpdate', NULL, 'ConceptNoteContactEquipment.AddUpdate', 'Add / Edit ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'FinalizeEquipmentDistribution', NULL, 'ConceptNoteContactEquipment.FinalizeEquipmentDistribution', 'Finalize Equipment Distribution ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'List', NULL, 'ConceptNoteContactEquipment.List', 'List ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', NULL, 'ConceptNoteContactEquipment.View', 'View ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', 'Export', 'ConceptNoteContactEquipment.View.Export', 'Export ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'AddUpdate', NULL, 'License.AddUpdate', 'Add / Edit License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'List', NULL, 'License.List', 'List License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'View', NULL, 'License.View', 'View License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'AddUpdate', NULL, 'LicenseEquipmentCatalog.AddUpdate', 'Add / Edit LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', NULL, 'LicenseEquipmentCatalog.List', 'List LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', 'Export', 'LicenseEquipmentCatalog.List.Export', 'Export LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'View', NULL, 'LicenseEquipmentCatalog.View', 'View LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'AddUpdate', NULL, 'PurchaseRequest.AddUpdate', 'Add / Edit PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'List', NULL, 'PurchaseRequest.List', 'List PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', NULL, 'PurchaseRequest.View', 'View PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', 'Export', 'PurchaseRequest.View.Export', 'Export PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'AddUpdate', NULL, 'ProgramReport.AddUpdate', 'Add / Edit ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'List', NULL, 'ProgramReport.List', 'List ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', NULL, 'ProgramReport.View', 'View ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', 'Export', 'ProgramReport.View.Export', 'Export ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'Province', 'AddUpdate', NULL, 'Province.AddUpdate', 'Add / Edit Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'List', NULL, 'Province.List', 'List Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', NULL, 'Province.View', 'View Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Analysis', 'Province.View.Analysis', 'Analysis Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'ExportEquipmentDistribution', 'Province.View.ExportEquipmentDistribution', 'Export Equipment Distributions Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Implementation', 'Province.View.Implementation', 'Implementation Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Information', 'Province.View.Information', 'Information Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'DailyReport', 'AddUpdate', NULL, 'DailyReport.AddUpdate', 'Add / Edit DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'List', NULL, 'DailyReport.List', 'List DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'View', NULL, 'DailyReport.View', 'View DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'AddUpdate', NULL, 'FocusGroupSurvey.AddUpdate', 'Add / Edit FocusGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'View', NULL, 'FocusGroupSurvey.View', 'View FocusGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'AddUpdate', NULL, 'KeyInformantSurvey.AddUpdate', 'Add / Edit KeyInformantSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'View', NULL, 'KeyInformantSurvey.View', 'View KeyInformantSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', NULL, 'RAPData.List', 'List RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', 'Export', 'RAPData.List.Export', 'Export RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RapidPerceptionSurvey', 'View', NULL, 'RapidPerceptionSurvey.View', 'View RapidPerceptionSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'AddUpdate', NULL, 'StakeholderGroupSurvey.AddUpdate', 'Add / Edit StakeholderGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'View', NULL, 'StakeholderGroupSurvey.View', 'View StakeholderGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'AddUpdate', NULL, 'StationCommanderSurvey.AddUpdate', 'Add / Edit StationCommanderSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'View', NULL, 'StationCommanderSurvey.View', 'View StationCommanderSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'AddUpdate', NULL, 'Team.AddUpdate', 'Add / Edit Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'List', NULL, 'Team.List', 'List Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'View', NULL, 'Team.View', 'View Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'SubContractor', 'AddUpdate', NULL, 'SubContractor.AddUpdate', 'Add / Edit SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'List', NULL, 'SubContractor.List', 'List SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'View', NULL, 'SubContractor.View', 'View SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / Edit Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'List Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / Edit Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'List Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / Edit a workflow', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the list of workflows', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflows'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.51 File 01 - AJACS - 2016.04.01 01.33.04')
GO
--End build tracking

