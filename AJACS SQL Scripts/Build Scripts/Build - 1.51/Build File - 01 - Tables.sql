USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'ConceptNoteFinanceTaskID', 'NVARCHAR(100)'
GO
--End table dbo.ConceptNote

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'UKVettingExpirationDate', 'DATE'
EXEC utility.AddColumn @TableName, 'USVettingExpirationDate', 'DATE'
GO

UPDATE C
SET 
	C.UKVettingExpirationDate = NULL,
	C.USVettingExpirationDate = NULL
FROM dbo.Contact C
GO

UPDATE C
SET C.UKVettingExpirationDate = DATEADD(m, 6, E.VettingDate)
FROM dbo.Contact C
	JOIN 
		(
		SELECT
			D.ContactID,
			D.VettingDate
		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (PARTITION BY CV.ContactID, CV.ContactVettingTypeID ORDER BY CV.ContactID, CV.VettingDate DESC) AS RowIndex,
				CV.ContactID,
				CV.VettingDate
			FROM AJACS.dbo.ContactVetting CV
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CVT.ContactVettingTypeName = 'UK'
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND VO.VettingOutcomeName = 'Consider'
			) D 
		WHERE D.RowIndex = 1
		) E ON E.ContactID = C.ContactID
GO

UPDATE C
SET C.USVettingExpirationDate = DATEADD(m, 6, E.VettingDate)
FROM dbo.Contact C
	JOIN 
		(
		SELECT
			D.ContactID,
			D.VettingDate
		FROM
			(
			SELECT 
				ROW_NUMBER() OVER (PARTITION BY CV.ContactID, CV.ContactVettingTypeID ORDER BY CV.ContactID, CV.VettingDate DESC) AS RowIndex,
				CV.ContactID,
				CV.VettingDate
			FROM AJACS.dbo.ContactVetting CV
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CVT.ContactVettingTypeName = 'US'
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND VO.VettingOutcomeName = 'Consider'
			) D 
		WHERE D.RowIndex = 1
		) E ON E.ContactID = C.ContactID
GO
--End table dbo.Contact

--Begin table dbo.ContactVetting
DECLARE @TableName VARCHAR(250) = 'dbo.ContactVetting'

EXEC utility.AddColumn @TableName, 'Notes', 'VARCHAR(MAX)'
GO
--End table dbo.ContactVetting

--Begin table dbo.ConceptNoteFinance
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteFinance'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteFinance
	(
	ConceptNoteFinanceID INT IDENTITY(1,1) NOT NULL,
	TransactionID NVARCHAR(100) NOT NULL,
	CpnyID NVARCHAR(100),
	PerPost NVARCHAR(100),
	Acct NVARCHAR(100),
	Sub NVARCHAR(100),
	ProjectID NVARCHAR(100),
	TaskID NVARCHAR(100),
	BatNbr NVARCHAR(100),
	RefNbr NVARCHAR(100),
	Module NVARCHAR(100),
	LineNbr NVARCHAR(100),
	VendID NVARCHAR(100),
	DRAmt DECIMAL(32, 16) NOT NULL,
	CRAmt DECIMAL(32, 16) NOT NULL,
	CuryDRAmt DECIMAL(32, 16) NOT NULL,
	CuryCrAmt DECIMAL(32, 16) NOT NULL
	)

EXEC utility.SetDefaultConstraint @TableName, 'DRAmt', 'DECIMAL(32, 16)', 0
EXEC utility.SetDefaultConstraint @TableName, 'CRAmt', 'DECIMAL(32, 16)', 0
EXEC utility.SetDefaultConstraint @TableName, 'CuryDRAmt', 'DECIMAL(32, 16)', 0
EXEC utility.SetDefaultConstraint @TableName, 'CuryCrAmt', 'DECIMAL(32, 16)', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteFinanceID'
GO
--End table dbo.ConceptNoteFinance

--Begin table dropdown.ContactType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactType'

EXEC utility.AddColumn @TableName, 'IsStipend', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsStipend', 'BIT', 0
GO

UPDATE CT
SET CT.IsStipend = 1
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode IN ('JusticeStipend','PoliceStipend')
GO
--End table dropdown.ContactType

--Begin table integration.CreativeFinancial
DECLARE @TableName VARCHAR(250) = 'integration.CreativeFinancial'

EXEC utility.DropObject @TableName

CREATE TABLE integration.CreativeFinancial
	(
	FinanceID INT IDENTITY(1,1) NOT NULL,
	TransactionID NVARCHAR(100),
	CpnyID NVARCHAR(100),
	PerPost NVARCHAR(100),
	Acct NVARCHAR(100),
	Sub NVARCHAR(100),
	ProjectID NVARCHAR(100),
	TaskID NVARCHAR(100),
	BatNbr NVARCHAR(100),
	RefNbr NVARCHAR(100),
	Module NVARCHAR(100),
	LineNbr NVARCHAR(100),
	VendID NVARCHAR(100),
	DRAmt NVARCHAR(100),
	CRAmt NVARCHAR(100),
	CuryDRAmt NVARCHAR(100),
	CuryCrAmt NVARCHAR(100)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'FinanceID'
GO
--End table integration.CreativeFinancial

--Begin table permissionable.xxx
EXEC utility.DropObject 'permissionable.DisplayGroup'
EXEC utility.DropObject 'permissionable.DisplayGroupPermissionable'
EXEC utility.DropObject 'permissionable.Permissionable2'
GO

DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.DropColumn @TableName, 'IsWorkflow'
EXEC utility.DropColumn @TableName, 'ParentPermissionableID'
EXEC utility.DropColumn @TableName, 'PermissionableCode'
EXEC utility.DropColumn @TableName, 'PermissionableName'
GO

DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.AddColumn @TableName, 'PermissionableLineage', 'VARCHAR(MAX)'
GO

UPDATE PTP
SET PTP.PermissionableLineage = P.PermissionableLineage
FROM permissionable.PermissionableTemplatePermissionable PTP
	JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.xxx

