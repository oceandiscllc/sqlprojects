USE AJACS
GO

--Begin function dbo.FormatActivityReferenceCode
EXEC utility.DropObject 'dbo.FormatActivityReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- =========================================================================

CREATE FUNCTION dbo.FormatActivityReferenceCode
(
@ActivityID INT
)

RETURNS NVARCHAR(100)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(100) = (SELECT A.TaskCode FROM Activity.Activity A WHERE A.ActivityID = @ActivityID)
	DECLARE @cSystemName VARCHAR(50)

	IF @cReturn IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'
				
		SET @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ActivityID AS VARCHAR(10)), 4)

		END
	--ENDIF

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatActivityReferenceCode
