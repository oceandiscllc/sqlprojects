USE AJACS
GO

--Begin table dropdown.AssetType 
IF NOT EXISTS(SELECT 1 FROM dropdown.AssetType AT WHERE AT.AssetTypeName = 'Police Other' AND AT.AssetTypeCategory = 'Police')
	BEGIN

	INSERT INTO dropdown.AssetType
		(AssetTypeName, Icon, DisplayOrder, AssetTypeCategory)
	VALUES
		('Police Other', 'asset-other.png', 0, 'Police'),
		('Prison', 'asset-court.png', 0, 'Police')

	END
--ENDIF
GO
--End table dropdown.AssetType 

--Begin table dropdown.ContactType 
UPDATE dropdown.ContactType 
SET IsActive = 0 
WHERE ContactTypeCode = 'FieldStaff' 
	AND ContactTypeName = 'Field Staff'
GO
--End table dropdown.ContactType 
