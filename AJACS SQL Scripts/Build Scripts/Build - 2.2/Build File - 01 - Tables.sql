USE AJACS
GO

-- ************************************************************************************************** --
--																																																		--
-- NOTE:  This build requires that the scheduled job AJACS dbo.Contact IsEquipmentEligible be created --
--																																																		--
-- ************************************************************************************************** --

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'IsCommunityAssociationRequired', 'BIT'
EXEC utility.AddColumn @TableName, 'IsEUEquipmentTransferEligible', 'BIT'
EXEC utility.AddColumn @TableName, 'IsUSEquipmentTransferEligible', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsCommunityAssociationRequired', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsEUEquipmentTransferEligible', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsUSEquipmentTransferEligible', 'BIT', '0'
GO
--End table dbo.Contact
