USE AJACS
GO

--Begin procedure dbo.GetCommunityByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Community table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
--
-- Author:			John Lyons
-- Create date:	2015.09.28
-- Description:	Added the ArabicCommunityName column
--
-- Author:			Johnathan Burnham
-- Create date:	2016.09.25
-- Description:	Refactored
--
-- Author:			Todd Pires
-- Create date:	2016.11.28
-- Description:	Bug fix
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nCEProjectsCount INT
	DECLARE @nCESpendValue NUMERIC(18,2)
	DECLARE @nJusticeAssetsCount INT
	DECLARE @nJusticeSpendValue NUMERIC(18,2)
	DECLARE @nJusticeStipendaryCount INT
	DECLARE @nPoliceAssetsCount INT
	DECLARE @nPoliceSpendValue NUMERIC(18,2)
	DECLARE @nPoliceStipendaryCount INT

	DECLARE @tTable TABLE (ComponentCode VARCHAR(50), ConceptNoteID INT, BudgetTotal NUMERIC(18,2))

	SELECT 
		C.ArabicCommunityName, 
		C.CommunityEngagementStatusID, 
		C.CommunityGroupID, 
		C.CommunityID, 
		C.CommunityName, 
		C.CommunitySubGroupID, 
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyPoints, 
		C.Latitude, 
		C.Longitude,
		C.PolicePresenceCategoryID, 
		C.PolicePresenceStatusID, 
		C.PoliceStationNumber, 
		C.PoliceStationStatusID, 
		C.Population, 
		C.PopulationSource, 
		C.ProgramNotes1, 
		C.ProgramNotes2, 
		C.ProgramNotes3, 
		C.ProgramNotes4, 
		C.ProgramNotes5, 
		C.ProvinceID,
		C.RiskMitigation, 
		C.StatusChangeID, 
		C.Summary, 
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CSG.CommunitySubGroupName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Community', @CommunityID) AS UpdateDateFormatted,
		ID.ImpactDecisionName,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		PPS.PolicePresenceStatusName,
		PSS.PoliceStationStatusName,
		SC.StatusChangeName,
		C.CommunityComponentStatusID,
		C.JusticeComponentStatusID,
		C.PoliceComponentStatusID,
		CS1.ComponentStatusName AS CommunityComponentStatusName,
		CS2.ComponentStatusName AS JusticeComponentStatusName,
		CS3.ComponentStatusName AS PoliceComponentStatusName,
		ISNULL(CS1.HexColor,'#000') AS CommunityComponentStatusHex,
		ISNULL(CS2.HexColor,'#000') AS JusticeComponentStatusHex,
		ISNULL(CS3.HexColor,'#000') AS PoliceComponentStatusHex
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.PolicePresenceStatus PPS ON PPS.PolicePresenceStatusID = C.PolicePresenceStatusID
		JOIN dropdown.PoliceStationStatus PSS ON PSS.PoliceStationStatusID = C.PoliceStationStatusID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
		JOIN dropdown.ComponentStatus CS1 ON CS1.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus CS2 ON CS2.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus CS3 ON CS3.ComponentStatusID = C.PoliceComponentStatusID
			AND C.CommunityID = @CommunityID

	SELECT @nJusticeStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'JusticeStipend' 
			) 
			AND C.IsActive = 1
	
	SELECT @nPoliceStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'PoliceStipend' 
			) 
			AND C.IsActive = 1

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'AccesstoJustice',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'AccesstoJustice'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'PoliceDevelopment',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'PoliceDevelopment'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'CommunityEngagement',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'CommunityEngagement'

	SELECT @nCEProjectsCount = COUNT(P.ProjectID)
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
		JOIN dropdown.Component CT ON P.ComponentID = CT.ComponentID
			AND PC.CommunityID = @CommunityID
			AND CT.ComponentAbbreviation = 'CE'
	
	SELECT @nJusticeAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Justice'
	
	SELECT @nPoliceAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Police'

	SELECT @nCESpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'CommunityEngagement'

	SELECT @nJusticeSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'AccesstoJustice'

	SELECT @nPoliceSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'PoliceDevelopment'

	SELECT 
		@nCEProjectsCount AS CEProjectsCount,
		@nJusticeAssetsCount AS JusticeAssetsCount,
		@nJusticeStipendaryCount AS JusticeStipendaryCount,
		@nPoliceAssetsCount AS PoliceAssetsCount,
		@nPoliceStipendaryCount AS PoliceStipendaryCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'AccesstoJustice') AS ActiveJusticeActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'CommunityEngagement') AS ActiveCEActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'PoliceDevelopment') AS ActivePoliceActivitySheetCount,
		FORMAT(@nCESpendValue, 'C', 'en-us') AS CESpendValueFormatted,
		FORMAT(@nJusticeSpendValue, 'C', 'en-us') AS JusticeSpendValueFormatted,
		FORMAT(@nPoliceSpendValue, 'C', 'en-us') AS PoliceSpendValueFormatted

END
GO
--End procedure dbo.GetCommunityByCommunityID

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
--
-- Author:			Eric Jones
-- Create date:	2016.11.20
-- Description:	Added IsCommunityAssociationRequired field
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.AssetUnitID,		
		(SELECT AU.AssetUnitName FROM asset.AssetUnit AU WHERE AU.AssetUnitID = C1.AssetUnitID) AS AssetUnitName,		
		(SELECT A.AssetName FROM asset.Asset A JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID AND AU.AssetUnitID = C1.AssetUnitID) AS AssetName,
		C1.CommunityID,		
		dbo.GetCommunityNameByCommunityID(C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate((SELECT EL.CreateDateTime FROM eventlog.EventLog EL WHERE EL.EventCode = 'create' AND EL.EntityTypeCode = 'Contact' AND EL.EntityID = C1.ContactID)) AS InitialEntryDateFormatted,
		C1.IsActive,
		C1.IsCommunityAssociationRequired,	
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(SELECT CSG.CommunitySubGroupName FROM dropdown.CommunitySubGroup CSG JOIN dbo.Community C ON C. CommunitySubGroupID = CSG.CommunitySubGroupID AND C.CommunityID = C1.CommunityID) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID, 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,

		CASE
			WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
			AND CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
	--Get Document By Contact ID
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID
	ORDER BY D.DocumentDescription

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure utility.UpdateEquipmentTransferEligible
EXEC Utility.DropObject 'utility.UpdateEquipmentTransferEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update the IsEUEquipmentTransferEligible and IsUSEquipmentTransferEligible in the dbo.Contact table
-- ======================================================================================================================================
CREATE PROCEDURE utility.UpdateEquipmentTransferEligible

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE C
	SET 
		C.IsEUEquipmentTransferEligible = 
			CASE
				WHEN C.UKVettingExpirationDate IS NOT NULL
					AND C.UKVettingExpirationDate >= getDate()
					AND 
						(
						(C.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C.CommunityID) = 1)
							OR (C.AssetUnitID > 0 AND dbo.IsAssetUnitCommunityStipendEligible(C.AssetUnitID) = 1)
						)
					AND EXISTS
						(
						SELECT 1
						FROM
							(
							SELECT 
								ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
								CV.ContactID
							FROM dbo.ContactVetting CV
								JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
									AND CVT.ContactVettingTypeCode = 'US'
								JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
									AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
							) D
						WHERE D.RowIndex = 1
							AND D.ContactID = C.ContactID
						)
				THEN 1
				ELSE 0
			END,

		C.IsUSEquipmentTransferEligible = 
			CASE
				WHEN C.USVettingExpirationDate IS NOT NULL
					AND C.USVettingExpirationDate >= getDate()
					AND 
						(
						(C.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C.CommunityID) = 1)
							OR (C.AssetUnitID > 0 AND dbo.IsAssetUnitCommunityStipendEligible(C.AssetUnitID) = 1)
						)
				THEN 1
				ELSE 0
			END

	FROM dbo.Contact C

END
GO
--End procedure utility.UpdateEquipmentTransferEligible