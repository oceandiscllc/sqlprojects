-- File Name:	Build-2.2 File 01 - AJACS.sql
-- Build Key:	Build-2.2 File 01 - AJACS - 2016.12.06 12.13.20

USE [AJACS]
GO

-- ==============================================================================================================================
-- Functions:
--		asset.GetAssetUnitEquipmentEligibility
--		asset.GetAssetUnitEquipmentEligibilityNotes
--		dbo.GetContactEquipmentEligibility
--		dbo.GetContactEquipmentEligibilityNotes
--		dbo.GetContactStipendEligibility
--		dbo.GetContactStipendEligibilityNotes
--		dbo.IsAssetUnitEquipmentEligible
--
-- Procedures:
--		dbo.GetCommunityByCommunityID
--		dbo.GetContactByContactID
--		utility.UpdateEquipmentTransferEligible
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

-- ************************************************************************************************** --
--																																																		--
-- NOTE:  This build requires that the scheduled job AJACS dbo.Contact IsEquipmentEligible be created --
--																																																		--
-- ************************************************************************************************** --

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'IsCommunityAssociationRequired', 'BIT'
EXEC utility.AddColumn @TableName, 'IsEUEquipmentTransferEligible', 'BIT'
EXEC utility.AddColumn @TableName, 'IsUSEquipmentTransferEligible', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsCommunityAssociationRequired', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsEUEquipmentTransferEligible', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsUSEquipmentTransferEligible', 'BIT', '0'
GO
--End table dbo.Contact

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function asset.GetAssetUnitEquipmentEligibility
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit Equipment eligibility data
-- ===================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibility
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C ON C.ContactID = AU.CommanderContactID
			AND AU.AssetUnitID = @AssetUnitID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function asset.GetAssetUnitEquipmentEligibility

--Begin function asset.GetAssetUnitEquipmentEligibilityNotes
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit equipment eligibility data
-- ===================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibilityNotes
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL
	DECLARE @cFundingSourceCode VARCHAR(50)

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not equipment elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C ON C.ContactID = AU.CommanderContactID
			AND AU.AssetUnitID = @AssetUnitID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equipment Eligible'
	ELSE
		SET @cReturn = 'Equipment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function asset.GetAssetUnitEquipmentEligibilityNotes

--Begin function dbo.GetContactEquipmentEligibility
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibility
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		CASE
			WHEN C.CommunityID > 0
			THEN dbo.IsCommunityStipendEligible(C.CommunityID)
			ELSE 
				(
				SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
				FROM asset.Asset A
					JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
						AND AU.AssetUnitID = C.AssetUnitID
				)
			END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 2
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibility

--Begin function dbo.GetContactEquipmentEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityNotes
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @cReturn =
		CASE
			WHEN C.CommunityID > 0
			THEN 
				CASE
					WHEN dbo.IsCommunityStipendEligible(C.CommunityID) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			WHEN C.AssetUnitID > 0
			THEN
				CASE
					WHEN 
						(
						SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
						FROM asset.Asset A
							JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
								AND AU.AssetUnitID = C.AssetUnitID
						) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equpiment Eligible'
	ELSE
		SET @cReturn = 'Equpiment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactEquipmentEligibilityNotes

--Begin function dbo.GetContactStipendEligibility
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibility
(
@ContactID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C2 ON C2.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = P.StipendFundingSourceID
			AND C1.ContactID = @ContactID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibility

--Begin function dbo.GetContactStipendEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotes
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C2 ON C2.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = P.StipendFundingSourceID
			AND C1.ContactID = @ContactID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotes

--Begin function dbo.IsAssetUnitEquipmentEligible
EXEC utility.DropObject 'dbo.IsAssetUnitEquipmentEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if an asset unit is equipment eligible
-- ===========================================================================

CREATE FUNCTION dbo.IsAssetUnitEquipmentEligible
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C1 ON C1.CommunityID = A.CommunityID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND AU.AssetUnitID = @AssetUnitID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF
	
	RETURN @bReturn

END
GO
--End function dbo.IsAssetUnitEquipmentEligible
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetCommunityByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Community table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
--
-- Author:			John Lyons
-- Create date:	2015.09.28
-- Description:	Added the ArabicCommunityName column
--
-- Author:			Johnathan Burnham
-- Create date:	2016.09.25
-- Description:	Refactored
--
-- Author:			Todd Pires
-- Create date:	2016.11.28
-- Description:	Bug fix
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nCEProjectsCount INT
	DECLARE @nCESpendValue NUMERIC(18,2)
	DECLARE @nJusticeAssetsCount INT
	DECLARE @nJusticeSpendValue NUMERIC(18,2)
	DECLARE @nJusticeStipendaryCount INT
	DECLARE @nPoliceAssetsCount INT
	DECLARE @nPoliceSpendValue NUMERIC(18,2)
	DECLARE @nPoliceStipendaryCount INT

	DECLARE @tTable TABLE (ComponentCode VARCHAR(50), ConceptNoteID INT, BudgetTotal NUMERIC(18,2))

	SELECT 
		C.ArabicCommunityName, 
		C.CommunityEngagementStatusID, 
		C.CommunityGroupID, 
		C.CommunityID, 
		C.CommunityName, 
		C.CommunitySubGroupID, 
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyPoints, 
		C.Latitude, 
		C.Longitude,
		C.PolicePresenceCategoryID, 
		C.PolicePresenceStatusID, 
		C.PoliceStationNumber, 
		C.PoliceStationStatusID, 
		C.Population, 
		C.PopulationSource, 
		C.ProgramNotes1, 
		C.ProgramNotes2, 
		C.ProgramNotes3, 
		C.ProgramNotes4, 
		C.ProgramNotes5, 
		C.ProvinceID,
		C.RiskMitigation, 
		C.StatusChangeID, 
		C.Summary, 
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CSG.CommunitySubGroupName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Community', @CommunityID) AS UpdateDateFormatted,
		ID.ImpactDecisionName,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		PPS.PolicePresenceStatusName,
		PSS.PoliceStationStatusName,
		SC.StatusChangeName,
		C.CommunityComponentStatusID,
		C.JusticeComponentStatusID,
		C.PoliceComponentStatusID,
		CS1.ComponentStatusName AS CommunityComponentStatusName,
		CS2.ComponentStatusName AS JusticeComponentStatusName,
		CS3.ComponentStatusName AS PoliceComponentStatusName,
		ISNULL(CS1.HexColor,'#000') AS CommunityComponentStatusHex,
		ISNULL(CS2.HexColor,'#000') AS JusticeComponentStatusHex,
		ISNULL(CS3.HexColor,'#000') AS PoliceComponentStatusHex
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.PolicePresenceStatus PPS ON PPS.PolicePresenceStatusID = C.PolicePresenceStatusID
		JOIN dropdown.PoliceStationStatus PSS ON PSS.PoliceStationStatusID = C.PoliceStationStatusID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
		JOIN dropdown.ComponentStatus CS1 ON CS1.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus CS2 ON CS2.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus CS3 ON CS3.ComponentStatusID = C.PoliceComponentStatusID
			AND C.CommunityID = @CommunityID

	SELECT @nJusticeStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'JusticeStipend' 
			) 
			AND C.IsActive = 1
	
	SELECT @nPoliceStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'PoliceStipend' 
			) 
			AND C.IsActive = 1

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'AccesstoJustice',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'AccesstoJustice'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'PoliceDevelopment',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'PoliceDevelopment'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'CommunityEngagement',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'CommunityEngagement'

	SELECT @nCEProjectsCount = COUNT(P.ProjectID)
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
		JOIN dropdown.Component CT ON P.ComponentID = CT.ComponentID
			AND PC.CommunityID = @CommunityID
			AND CT.ComponentAbbreviation = 'CE'
	
	SELECT @nJusticeAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Justice'
	
	SELECT @nPoliceAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Police'

	SELECT @nCESpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'CommunityEngagement'

	SELECT @nJusticeSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'AccesstoJustice'

	SELECT @nPoliceSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'PoliceDevelopment'

	SELECT 
		@nCEProjectsCount AS CEProjectsCount,
		@nJusticeAssetsCount AS JusticeAssetsCount,
		@nJusticeStipendaryCount AS JusticeStipendaryCount,
		@nPoliceAssetsCount AS PoliceAssetsCount,
		@nPoliceStipendaryCount AS PoliceStipendaryCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'AccesstoJustice') AS ActiveJusticeActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'CommunityEngagement') AS ActiveCEActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'PoliceDevelopment') AS ActivePoliceActivitySheetCount,
		FORMAT(@nCESpendValue, 'C', 'en-us') AS CESpendValueFormatted,
		FORMAT(@nJusticeSpendValue, 'C', 'en-us') AS JusticeSpendValueFormatted,
		FORMAT(@nPoliceSpendValue, 'C', 'en-us') AS PoliceSpendValueFormatted

END
GO
--End procedure dbo.GetCommunityByCommunityID

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
--
-- Author:			Eric Jones
-- Create date:	2016.11.20
-- Description:	Added IsCommunityAssociationRequired field
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.AssetUnitID,		
		(SELECT AU.AssetUnitName FROM asset.AssetUnit AU WHERE AU.AssetUnitID = C1.AssetUnitID) AS AssetUnitName,		
		(SELECT A.AssetName FROM asset.Asset A JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID AND AU.AssetUnitID = C1.AssetUnitID) AS AssetName,
		C1.CommunityID,		
		dbo.GetCommunityNameByCommunityID(C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate((SELECT EL.CreateDateTime FROM eventlog.EventLog EL WHERE EL.EventCode = 'create' AND EL.EntityTypeCode = 'Contact' AND EL.EntityID = C1.ContactID)) AS InitialEntryDateFormatted,
		C1.IsActive,
		C1.IsCommunityAssociationRequired,	
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(SELECT CSG.CommunitySubGroupName FROM dropdown.CommunitySubGroup CSG JOIN dbo.Community C ON C. CommunitySubGroupID = CSG.CommunitySubGroupID AND C.CommunityID = C1.CommunityID) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID, 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,

		CASE
			WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
			AND CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
	--Get Document By Contact ID
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID
	ORDER BY D.DocumentDescription

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure utility.UpdateEquipmentTransferEligible
EXEC Utility.DropObject 'utility.UpdateEquipmentTransferEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update the IsEUEquipmentTransferEligible and IsUSEquipmentTransferEligible in the dbo.Contact table
-- ======================================================================================================================================
CREATE PROCEDURE utility.UpdateEquipmentTransferEligible

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE C
	SET 
		C.IsEUEquipmentTransferEligible = 
			CASE
				WHEN C.UKVettingExpirationDate IS NOT NULL
					AND C.UKVettingExpirationDate >= getDate()
					AND 
						(
						(C.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C.CommunityID) = 1)
							OR (C.AssetUnitID > 0 AND dbo.IsAssetUnitCommunityStipendEligible(C.AssetUnitID) = 1)
						)
					AND EXISTS
						(
						SELECT 1
						FROM
							(
							SELECT 
								ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
								CV.ContactID
							FROM dbo.ContactVetting CV
								JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
									AND CVT.ContactVettingTypeCode = 'US'
								JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
									AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
							) D
						WHERE D.RowIndex = 1
							AND D.ContactID = C.ContactID
						)
				THEN 1
				ELSE 0
			END,

		C.IsUSEquipmentTransferEligible = 
			CASE
				WHEN C.USVettingExpirationDate IS NOT NULL
					AND C.USVettingExpirationDate >= getDate()
					AND 
						(
						(C.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C.CommunityID) = 1)
							OR (C.AssetUnitID > 0 AND dbo.IsAssetUnitCommunityStipendEligible(C.AssetUnitID) = 1)
						)
				THEN 1
				ELSE 0
			END

	FROM dbo.Contact C

END
GO
--End procedure utility.UpdateEquipmentTransferEligible
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dropdown.AssetType 
IF NOT EXISTS(SELECT 1 FROM dropdown.AssetType AT WHERE AT.AssetTypeName = 'Police Other' AND AT.AssetTypeCategory = 'Police')
	BEGIN

	INSERT INTO dropdown.AssetType
		(AssetTypeName, Icon, DisplayOrder, AssetTypeCategory)
	VALUES
		('Police Other', 'asset-other.png', 0, 'Police'),
		('Prison', 'asset-court.png', 0, 'Police')

	END
--ENDIF
GO
--End table dropdown.AssetType 

--Begin table dropdown.ContactType 
UPDATE dropdown.ContactType 
SET IsActive = 0 
WHERE ContactTypeCode = 'FieldStaff' 
	AND ContactTypeName = 'Field Staff'
GO
--End table dropdown.ContactType 

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Community', 'Community', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contact', 0
GO
EXEC utility.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'Implementation', 'Implementation', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Province', 'Province', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Research', 0
GO
EXEC utility.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflows', 'Workflows', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='List EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='View EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='List EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Exports', @DESCRIPTION='Business License Report Exports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='BusinessLicenseReport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Exports.BusinessLicenseReport', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User Can Receive Email', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CanRecieveEmail', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.CanRecieveEmail.CanRecieveEmail', @PERMISSIONCODE='CanRecieveEmail'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View ColdFusion Errors SiteConfiguration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='List PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='List Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View list of permissionables on view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Add / edit a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Recieve email for Update to Impact Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate.ImpactUpdateEmail', @PERMISSIONCODE='ImpactUpdateEmail'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='List Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the analysis tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Analysis', @PERMISSIONCODE='Analysis'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Equipment Distributions Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Implementation Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Implementation', @PERMISSIONCODE='Implementation'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the information tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Information', @PERMISSIONCODE='Information'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add contacts to a stipend payment list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddContactStipendPaymentContacts', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddContactStipendPaymentContacts', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.CETeam', @PERMISSIONCODE='CETeam'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.FieldStaff', @PERMISSIONCODE='FieldStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.IO4', @PERMISSIONCODE='IO4'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeOther', @PERMISSIONCODE='JusticeOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeStipend', @PERMISSIONCODE='JusticeStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceOther', @PERMISSIONCODE='PoliceOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceStipend', @PERMISSIONCODE='PoliceStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Project Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ProjectStaff', @PERMISSIONCODE='ProjectStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Sub-Contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.SubContractors', @PERMISSIONCODE='SubContractors'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows view of justice stipends payments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the cash hand over report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the op funds report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend activity report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Close out the stipend justice & police payment process', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows import of payees in payment system', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.AddContactStipendPaymentContacts', @PERMISSIONCODE='AddContactStipendPaymentContacts'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type CE Team in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.CETeam', @PERMISSIONCODE='CETeam'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export payees from the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ExportPayees', @PERMISSIONCODE='ExportPayees'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Field Staff in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.FieldStaff', @PERMISSIONCODE='FieldStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type IO4 in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.IO4', @PERMISSIONCODE='IO4'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeOther', @PERMISSIONCODE='JusticeOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='A bypass to allow users not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceOther', @PERMISSIONCODE='PoliceOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Project Staff in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ProjectStaff', @PERMISSIONCODE='ProjectStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Sub-Contractor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.SubContractors', @PERMISSIONCODE='SubContractors'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the cash handover report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the op funds report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend activity report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend payment report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend payment report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ReconcileContactStipendPayment', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.ReconcileContactStipendPayment', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.CETeam', @PERMISSIONCODE='CETeam'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.FieldStaff', @PERMISSIONCODE='FieldStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.IO4', @PERMISSIONCODE='IO4'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeOther', @PERMISSIONCODE='JusticeOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeStipend', @PERMISSIONCODE='JusticeStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceOther', @PERMISSIONCODE='PoliceOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceStipend', @PERMISSIONCODE='PoliceStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Project Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.ProjectStaff', @PERMISSIONCODE='ProjectStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Sub-Contractors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.SubContractors', @PERMISSIONCODE='SubContractors'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the more info button on the vetting history data table', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.VettingMoreInfo', @PERMISSIONCODE='VettingMoreInfo'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type CE Team in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.CETeam', @PERMISSIONCODE='CETeam'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Field Staff in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.FieldStaff', @PERMISSIONCODE='FieldStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type IO4 in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.IO4', @PERMISSIONCODE='IO4'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeOther', @PERMISSIONCODE='JusticeOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceOther', @PERMISSIONCODE='PoliceOther'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Project Staff in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ProjectStaff', @PERMISSIONCODE='ProjectStaff'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Sub-Contractor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.SubContractors', @PERMISSIONCODE='SubContractors'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Recieve an email when a vetting outcome has changed for one or more contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeChangeNotification', @PERMISSIONCODE='VettingOutcomeChangeNotification'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeConsider', @PERMISSIONCODE='VettingOutcomeConsider'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Do Not Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeDoNotConsider', @PERMISSIONCODE='VettingOutcomeDoNotConsider'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Insufficient Data"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeInsufficientData', @PERMISSIONCODE='VettingOutcomeInsufficientData'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Not Vetted"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeNotVetted', @PERMISSIONCODE='VettingOutcomeNotVetted'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Pending Internal Review"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomePendingInternalReview', @PERMISSIONCODE='VettingOutcomePendingInternalReview'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Submitted for Vetting"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeSubmittedforVetting', @PERMISSIONCODE='VettingOutcomeSubmittedforVetting'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update UK vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUK', @PERMISSIONCODE='VettingTypeUK'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update US vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUS', @PERMISSIONCODE='VettingTypeUS'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Recieve the monthly vetting expiration counts e-mail', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Notification', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.Notification.ExpirationCountEmail', @PERMISSIONCODE='ExpirationCountEmail'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.004', @PERMISSIONCODE='004'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.100', @PERMISSIONCODE='100'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.101', @PERMISSIONCODE='101'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.102', @PERMISSIONCODE='102'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.103', @PERMISSIONCODE='103'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.104', @PERMISSIONCODE='104'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.105', @PERMISSIONCODE='105'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.106', @PERMISSIONCODE='106'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.107', @PERMISSIONCODE='107'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.108', @PERMISSIONCODE='108'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.109', @PERMISSIONCODE='109'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.301', @PERMISSIONCODE='301'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.302', @PERMISSIONCODE='302'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.303', @PERMISSIONCODE='303'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.304', @PERMISSIONCODE='304'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.306', @PERMISSIONCODE='306'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.500', @PERMISSIONCODE='500'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.501', @PERMISSIONCODE='501'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.502', @PERMISSIONCODE='502'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.503', @PERMISSIONCODE='503'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.504', @PERMISSIONCODE='504'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.505', @PERMISSIONCODE='505'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.506', @PERMISSIONCODE='506'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.507', @PERMISSIONCODE='507'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.508', @PERMISSIONCODE='508'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.509', @PERMISSIONCODE='509'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.510', @PERMISSIONCODE='510'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.511', @PERMISSIONCODE='511'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.512', @PERMISSIONCODE='512'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.513', @PERMISSIONCODE='513'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.514', @PERMISSIONCODE='514'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.515', @PERMISSIONCODE='515'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.516', @PERMISSIONCODE='516'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.517', @PERMISSIONCODE='517'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.518', @PERMISSIONCODE='518'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.519', @PERMISSIONCODE='519'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.520', @PERMISSIONCODE='520'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.521', @PERMISSIONCODE='521'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.600', @PERMISSIONCODE='600'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.601', @PERMISSIONCODE='601'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.602', @PERMISSIONCODE='602'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.603', @PERMISSIONCODE='603'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.604', @PERMISSIONCODE='604'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.605', @PERMISSIONCODE='605'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.700', @PERMISSIONCODE='700'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.701', @PERMISSIONCODE='701'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.702', @PERMISSIONCODE='702'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.801', @PERMISSIONCODE='801'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.802', @PERMISSIONCODE='802'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.803', @PERMISSIONCODE='803'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.804', @PERMISSIONCODE='804'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.805', @PERMISSIONCODE='805'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.806', @PERMISSIONCODE='806'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.807', @PERMISSIONCODE='807'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.808', @PERMISSIONCODE='808'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.900', @PERMISSIONCODE='900'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.901', @PERMISSIONCODE='901'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.902', @PERMISSIONCODE='902'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.903', @PERMISSIONCODE='903'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.904', @PERMISSIONCODE='904'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.905', @PERMISSIONCODE='905'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.906', @PERMISSIONCODE='906'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.907', @PERMISSIONCODE='907'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.908', @PERMISSIONCODE='908'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.909', @PERMISSIONCODE='909'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.910', @PERMISSIONCODE='910'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.004', @PERMISSIONCODE='004'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.100', @PERMISSIONCODE='100'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.101', @PERMISSIONCODE='101'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.102', @PERMISSIONCODE='102'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.103', @PERMISSIONCODE='103'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.104', @PERMISSIONCODE='104'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.105', @PERMISSIONCODE='105'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.106', @PERMISSIONCODE='106'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.107', @PERMISSIONCODE='107'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.108', @PERMISSIONCODE='108'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.109', @PERMISSIONCODE='109'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.301', @PERMISSIONCODE='301'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.302', @PERMISSIONCODE='302'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.303', @PERMISSIONCODE='303'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.304', @PERMISSIONCODE='304'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.306', @PERMISSIONCODE='306'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.500', @PERMISSIONCODE='500'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.501', @PERMISSIONCODE='501'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.502', @PERMISSIONCODE='502'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.503', @PERMISSIONCODE='503'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.504', @PERMISSIONCODE='504'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.505', @PERMISSIONCODE='505'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.506', @PERMISSIONCODE='506'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.507', @PERMISSIONCODE='507'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.508', @PERMISSIONCODE='508'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.509', @PERMISSIONCODE='509'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.510', @PERMISSIONCODE='510'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.511', @PERMISSIONCODE='511'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.512', @PERMISSIONCODE='512'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.513', @PERMISSIONCODE='513'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.514', @PERMISSIONCODE='514'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.515', @PERMISSIONCODE='515'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.516', @PERMISSIONCODE='516'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.517', @PERMISSIONCODE='517'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.518', @PERMISSIONCODE='518'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.519', @PERMISSIONCODE='519'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.520', @PERMISSIONCODE='520'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.521', @PERMISSIONCODE='521'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.600', @PERMISSIONCODE='600'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.601', @PERMISSIONCODE='601'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.602', @PERMISSIONCODE='602'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.603', @PERMISSIONCODE='603'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.604', @PERMISSIONCODE='604'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.605', @PERMISSIONCODE='605'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.700', @PERMISSIONCODE='700'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.701', @PERMISSIONCODE='701'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.702', @PERMISSIONCODE='702'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.801', @PERMISSIONCODE='801'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.802', @PERMISSIONCODE='802'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.803', @PERMISSIONCODE='803'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.804', @PERMISSIONCODE='804'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.805', @PERMISSIONCODE='805'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.806', @PERMISSIONCODE='806'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.807', @PERMISSIONCODE='807'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.808', @PERMISSIONCODE='808'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.900', @PERMISSIONCODE='900'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.901', @PERMISSIONCODE='901'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.902', @PERMISSIONCODE='902'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.903', @PERMISSIONCODE='903'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.904', @PERMISSIONCODE='904'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.905', @PERMISSIONCODE='905'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.906', @PERMISSIONCODE='906'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.907', @PERMISSIONCODE='907'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.908', @PERMISSIONCODE='908'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.909', @PERMISSIONCODE='909'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.910', @PERMISSIONCODE='910'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit a donor decision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateDecision', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateDecision', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit donor meetings & actions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateMeeting', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateMeeting', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='List Donor Decisions, Meetings & Actions DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='View DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit the equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='List EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Add or update an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Audit', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Audit', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Create an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Create', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Create', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Delete an active equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Delete', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.FinalizeEquipmentDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListDistributedInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistribution', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListInventory', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.SetDeliveryDate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SetDeliveryDate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.SetDeliveryDate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Transfer', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Transfer', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Transfer', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit the equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='List EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Export EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='Audit Equipment EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.Audit', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='List Equipment Locations EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='Add / edit a community asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='List CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='View CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='Add / edit a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View the list of community rounds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.AddUpdate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.View', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='List Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='Add / edit an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='List Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='Add / edit an indicatortype', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='List IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='View IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='Add / edit a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='List Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Add / edit an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='M & E Overview Charts Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='ChartList', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.ChartList', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='List Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Manage Objectives & Indicators Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Manage', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Manage', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Overview Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Overview', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit a concep nNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Budget ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.ExportConceptNoteBudget', @PERMISSIONCODE='ExportConceptNoteBudget'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit activity finances', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Finances', @PERMISSIONCODE='Finances'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk pane a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Risk', @PERMISSIONCODE='Risk'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Vetting List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Vetting ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList.ExportVetting', @PERMISSIONCODE='ExportVetting'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Access to Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetAccesstoJustice', @PERMISSIONCODE='BudgetAccesstoJustice'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Communication', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunication', @PERMISSIONCODE='BudgetCommunication'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunityEngagement', @PERMISSIONCODE='BudgetCommunityEngagement'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Integrated Legitimate Structures', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetIntegratedLegitimateStructures', @PERMISSIONCODE='BudgetIntegratedLegitimateStructures'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetM&E', @PERMISSIONCODE='BudgetM&E'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type MER', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetMER', @PERMISSIONCODE='BudgetMER'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Police Development', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetPoliceDevelopment', @PERMISSIONCODE='BudgetPoliceDevelopment'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type RAP', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetRAP', @PERMISSIONCODE='BudgetRAP'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetResearch', @PERMISSIONCODE='BudgetResearch'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk panel in a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Risk', @PERMISSIONCODE='Risk'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Add / edit equipment associated with a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Finalize Equipment Distribution ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='List ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='View ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Export ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='Add / edit a license', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='List License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='View License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Add / edit the license equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='List LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Export LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='View LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Add / edit a purchase request', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='List PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='View PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Export PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Add or update a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Delete a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.Delete', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='List workplans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='View a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Add / edit a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='List ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Export ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Add / edit a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='List Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the analysis tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Analysis', @PERMISSIONCODE='Analysis'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Export Equipment Distributions Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Implementation Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Implementation', @PERMISSIONCODE='Implementation'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the information tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Information', @PERMISSIONCODE='Information'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='AssetUnit', @DESCRIPTION='A bypass to allow asset units not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='AssetUnit.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='Add / edit an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='List Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='Add / edit a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='List Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='List Forces Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a force Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='List Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Add / edit an media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View the list of media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Export selected media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View a media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Add / edit a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='List Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Export Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Add', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Add', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='List RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Export RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Amend a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update.Amend', @PERMISSIONCODE='Amend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Add / edit a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Export Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='List Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Amend a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate.Amend', @PERMISSIONCODE='Amend'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='List SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Approved SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Approved', @PERMISSIONCODE='Approved'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Export SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='In Work SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.InWork', @PERMISSIONCODE='InWork'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateQuestion', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Administer a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AdministerSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Export Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ExportSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ExportSurveyResponses', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Questions List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListQuestions', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListQuestions', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='List Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveyResponses', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Surveys List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveys', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Questions SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewQuestion', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Surveys SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewSurvey', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Weekly Report Add / Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Export the weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate.Export', @PERMISSIONCODE='Export'
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='View the completed weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='Add / edit a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View the list of zones', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='Add / edit a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='List SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='View SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Add / edit a class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='List Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='List Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL
GO
EXEC utility.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP FROM permissionable.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table permissionable.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-2.2 File 01 - AJACS - 2016.12.06 12.13.20')
GO
--End build tracking

