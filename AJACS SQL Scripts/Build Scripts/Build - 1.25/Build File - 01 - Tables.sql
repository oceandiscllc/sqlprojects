USE AJACS
GO

--Begin table dbo.Bookmark
DECLARE @TableName VARCHAR(250) = 'dbo.Bookmark'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Bookmark
	(
	BookmarkID INT IDENTITY(1,1),
	BookmarkName NVARCHAR(250),
	PersonID INT,
	DisplayOrder INT,
	PageURL VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookmarkID'
EXEC utility.SetIndexClustered 'IX_Bookmark', @TableName, 'PersonID,DisplayOrder,BookmarkName,BookmarkID'
GO
--Begin table dbo.Bookmark

--Begin table dbo.ConceptNote
ALTER TABLE dbo.ConceptNote ALTER COLUMN ActivityCode NVARCHAR(100)
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteTask
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteTask'

EXEC utility.AddColumn @TableName, 'SourceConceptNoteTaskID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'SourceConceptNoteTaskID', 'INT', 0
GO
--End table reporting.SearchResult

--Begin table dropdown.LogicalFrameworkStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LogicalFrameworkStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LogicalFrameworkStatus
	(
	LogicalFrameworkStatusID INT IDENTITY(0,1) NOT NULL,
	LogicalFrameworkStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'LogicalFrameworkStatusID'
EXEC utility.SetIndexNonClustered 'IX_LogicalFrameworkStatus', @TableName, 'DisplayOrder,LogicalFrameworkStatusName', 'LogicalFrameworkStatusID'
GO

SET IDENTITY_INSERT dropdown.LogicalFrameworkStatus ON
GO

INSERT INTO dropdown.LogicalFrameworkStatus (LogicalFrameworkStatusID, DisplayOrder) VALUES (0, 1)

SET IDENTITY_INSERT dropdown.LogicalFrameworkStatus OFF
GO

INSERT INTO dropdown.LogicalFrameworkStatus 
	(LogicalFrameworkStatusName,DisplayOrder)
VALUES
	('Not Started', 1),
	('Cancelled', 2),
	('Off Track', 3),
	('On Track', 4),
	('Completed', 5),
	('Closed', 6)	
GO
--End table dropdown.LogicalFrameworkStatus

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.DropIndex @TableName, 'IX_Indicator'

ALTER TABLE logicalframework.Indicator ALTER COLUMN IndicatorName NVARCHAR(500)

EXEC utility.AddColumn @TableName, 'ActualDate', 'DATE'
EXEC utility.AddColumn @TableName, 'ActualValue', 'INT'
EXEC utility.AddColumn @TableName, 'IndicatorNumber', 'NVARCHAR(50)'
EXEC utility.AddColumn @TableName, 'InProgressDate', 'DATE'
EXEC utility.AddColumn @TableName, 'InProgressValue', 'INT'
EXEC utility.AddColumn @TableName, 'LogicalFrameworkStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'PlannedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PlannedValue', 'INT'
EXEC utility.AddColumn @TableName, 'StatusUpdateDescription', 'NVARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'ActualValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', 0

EXEC utility.SetIndexClustered 'IX_Indicator', @TableName, 'ObjectiveID,IndicatorNumber,IndicatorName'
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.AddColumn @TableName, 'ActualDate', 'DATE'
EXEC utility.AddColumn @TableName, 'ActualValue', 'INT'
EXEC utility.AddColumn @TableName, 'InProgressDate', 'DATE'
EXEC utility.AddColumn @TableName, 'InProgressValue', 'INT'
EXEC utility.AddColumn @TableName, 'PlannedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PlannedValue', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ActualValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', 0
GO
--End table logicalframework.Milestone

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.AddColumn @TableName, 'LogicalFrameworkStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'StatusUpdateDescription', 'NVARCHAR(MAX)'
GO
--End table logicalframework.Objective

--Begin table recommendationupdate.RecommendationCommunity
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationCommunity
	(
	RecommendationCommunityID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationCommunityID'
EXEC utility.SetIndexClustered 'IX_RecommendationCommunity', @TableName, 'RecommendationID,CommunityID'
GO
--End table recommendationupdate.RecommendationCommunity

--Begin table recommendationupdate.RecommendationComponent
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationComponent'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationComponent
	(
	RecommendationComponentID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ComponentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationComponentID'
EXEC utility.SetIndexClustered 'IX_RecommendationComponent', @TableName, 'RecommendationID,ComponentID'
GO
--End table recommendationupdate.RecommendationComponent

--Begin table recommendationupdate.RecommendationIndicator
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationIndicator
	(
	RecommendationIndicatorID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationIndicatorID'
EXEC utility.SetIndexClustered 'IX_RecommendationIndicator', @TableName, 'RecommendationID,IndicatorID'
GO
--End table recommendationupdate.RecommendationIndicator

--Begin table recommendationupdate.RecommendationProvince
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationProvince'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationProvince
	(
	RecommendationProvinceID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationProvinceID'
EXEC utility.SetIndexClustered 'IX_RecommendationProvince', @TableName, 'RecommendationID,ProvinceID'
GO
--End table recommendationupdate.RecommendationProvince

--Begin table recommendationupdate.RecommendationRisk
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationRisk'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationRisk
	(
	RecommendationRiskID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationRiskID'
EXEC utility.SetIndexClustered 'IX_RecommendationRisk', @TableName, 'RecommendationID,RiskID'
GO
--End table recommendationupdate.RecommendationRisk

--Begin table reporting.SearchResult
DECLARE @TableName VARCHAR(250) = 'reporting.SearchResult'

EXEC utility.AddColumn @TableName, 'EntityTypeGroupCode', 'VARCHAR(50)'
GO
--End table reporting.SearchResult

