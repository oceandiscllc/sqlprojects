-- File Name:	Build-1.25 File 01 - AJACS.sql
-- Build Key:	Build-1.25 File 01 - AJACS - 2015.08.28 19.50.43

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatConceptNoteReferenceCode
--		dbo.FormatConceptNoteTitle
--
-- Procedures:
--		dbo.CloneConceptNote
--		dbo.GetBookmarkByPersonID
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.ReorderBookmarkDown
--		dbo.ReorderBookmarkUp
--		dropdown.GetLogicalFrameworkStatusData
--		eventlog.LogDataExportAction
--		logicalframework.GetIndicatorByIndicatorID
--		logicalframework.GetIndicatorByObjectiveID
--		procurement.GetConceptNoteContactEquipmentByConceptNoteID
--		project.GetProjectByProjectID
--		recommendationupdate.ApproveRecommendationUpdateRecommendations
--		recommendationupdate.DeleteRecommendationUpdateRecommendation
--		recommendationupdate.GetRecommendationUpdateByRecommendationID
--		recommendationupdate.PopulateRecommendationUpdateRecommendations
--		reporting.GetCommunityMemberSurvey
--		reporting.GetFocusGroupSurvey
--		reporting.GetKeyInformantSurvey
--		reporting.GetStakeholderGroupSurvey
--		reporting.GetStationCommanderSurvey
--		workflow.CanIncrementRecommendationUpdateWorkflow
--		workflow.GetRecommendationUpdateWorkflowData
--		workflow.GetRecommendationUpdateWorkflowStepPeople
--
-- Tables:
--		dbo.Bookmark
--		dropdown.LogicalFrameworkStatus
--		recommendationupdate.RecommendationCommunity
--		recommendationupdate.RecommendationComponent
--		recommendationupdate.RecommendationIndicator
--		recommendationupdate.RecommendationProvince
--		recommendationupdate.RecommendationRisk
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Bookmark
DECLARE @TableName VARCHAR(250) = 'dbo.Bookmark'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Bookmark
	(
	BookmarkID INT IDENTITY(1,1),
	BookmarkName NVARCHAR(250),
	PersonID INT,
	DisplayOrder INT,
	PageURL VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'BookmarkID'
EXEC utility.SetIndexClustered 'IX_Bookmark', @TableName, 'PersonID,DisplayOrder,BookmarkName,BookmarkID'
GO
--Begin table dbo.Bookmark

--Begin table dbo.ConceptNote
ALTER TABLE dbo.ConceptNote ALTER COLUMN ActivityCode NVARCHAR(100)
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteTask
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteTask'

EXEC utility.AddColumn @TableName, 'SourceConceptNoteTaskID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'SourceConceptNoteTaskID', 'INT', 0
GO
--End table reporting.SearchResult

--Begin table dropdown.LogicalFrameworkStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LogicalFrameworkStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.LogicalFrameworkStatus
	(
	LogicalFrameworkStatusID INT IDENTITY(0,1) NOT NULL,
	LogicalFrameworkStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'LogicalFrameworkStatusID'
EXEC utility.SetIndexNonClustered 'IX_LogicalFrameworkStatus', @TableName, 'DisplayOrder,LogicalFrameworkStatusName', 'LogicalFrameworkStatusID'
GO

SET IDENTITY_INSERT dropdown.LogicalFrameworkStatus ON
GO

INSERT INTO dropdown.LogicalFrameworkStatus (LogicalFrameworkStatusID, DisplayOrder) VALUES (0, 1)

SET IDENTITY_INSERT dropdown.LogicalFrameworkStatus OFF
GO

INSERT INTO dropdown.LogicalFrameworkStatus 
	(LogicalFrameworkStatusName,DisplayOrder)
VALUES
	('Not Started', 1),
	('Cancelled', 2),
	('Off Track', 3),
	('On Track', 4),
	('Completed', 5),
	('Closed', 6)	
GO
--End table dropdown.LogicalFrameworkStatus

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.DropIndex @TableName, 'IX_Indicator'

ALTER TABLE logicalframework.Indicator ALTER COLUMN IndicatorName NVARCHAR(500)

EXEC utility.AddColumn @TableName, 'ActualDate', 'DATE'
EXEC utility.AddColumn @TableName, 'ActualValue', 'INT'
EXEC utility.AddColumn @TableName, 'IndicatorNumber', 'NVARCHAR(50)'
EXEC utility.AddColumn @TableName, 'InProgressDate', 'DATE'
EXEC utility.AddColumn @TableName, 'InProgressValue', 'INT'
EXEC utility.AddColumn @TableName, 'LogicalFrameworkStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'PlannedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PlannedValue', 'INT'
EXEC utility.AddColumn @TableName, 'StatusUpdateDescription', 'NVARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'ActualValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', 0

EXEC utility.SetIndexClustered 'IX_Indicator', @TableName, 'ObjectiveID,IndicatorNumber,IndicatorName'
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.AddColumn @TableName, 'ActualDate', 'DATE'
EXEC utility.AddColumn @TableName, 'ActualValue', 'INT'
EXEC utility.AddColumn @TableName, 'InProgressDate', 'DATE'
EXEC utility.AddColumn @TableName, 'InProgressValue', 'INT'
EXEC utility.AddColumn @TableName, 'PlannedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PlannedValue', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ActualValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', 0
GO
--End table logicalframework.Milestone

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.AddColumn @TableName, 'LogicalFrameworkStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'StatusUpdateDescription', 'NVARCHAR(MAX)'
GO
--End table logicalframework.Objective

--Begin table recommendationupdate.RecommendationCommunity
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationCommunity
	(
	RecommendationCommunityID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationCommunityID'
EXEC utility.SetIndexClustered 'IX_RecommendationCommunity', @TableName, 'RecommendationID,CommunityID'
GO
--End table recommendationupdate.RecommendationCommunity

--Begin table recommendationupdate.RecommendationComponent
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationComponent'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationComponent
	(
	RecommendationComponentID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ComponentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationComponentID'
EXEC utility.SetIndexClustered 'IX_RecommendationComponent', @TableName, 'RecommendationID,ComponentID'
GO
--End table recommendationupdate.RecommendationComponent

--Begin table recommendationupdate.RecommendationIndicator
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationIndicator
	(
	RecommendationIndicatorID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationIndicatorID'
EXEC utility.SetIndexClustered 'IX_RecommendationIndicator', @TableName, 'RecommendationID,IndicatorID'
GO
--End table recommendationupdate.RecommendationIndicator

--Begin table recommendationupdate.RecommendationProvince
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationProvince'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationProvince
	(
	RecommendationProvinceID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationProvinceID'
EXEC utility.SetIndexClustered 'IX_RecommendationProvince', @TableName, 'RecommendationID,ProvinceID'
GO
--End table recommendationupdate.RecommendationProvince

--Begin table recommendationupdate.RecommendationRisk
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationRisk'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationRisk
	(
	RecommendationRiskID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationRiskID'
EXEC utility.SetIndexClustered 'IX_RecommendationRisk', @TableName, 'RecommendationID,RiskID'
GO
--End table recommendationupdate.RecommendationRisk

--Begin table reporting.SearchResult
DECLARE @TableName VARCHAR(250) = 'reporting.SearchResult'

EXEC utility.AddColumn @TableName, 'EntityTypeGroupCode', 'VARCHAR(50)'
GO
--End table reporting.SearchResult


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatConceptNoteReferenceCode
EXEC utility.DropObject 'dbo.FormatConceptNoteReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- =========================================================================

CREATE FUNCTION dbo.FormatConceptNoteReferenceCode
(
@ConceptNoteID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(100) = (SELECT C.ActivityCode FROM dbo.ConceptNote C WHERE C.ConceptNoteID = @ConceptNoteID)
	DECLARE @cSystemName VARCHAR(50)

	IF @cReturn IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue
		FROM AJACSUtility.dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'
				
		SET @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)

		END
	--ENDIF

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteReferenceCode

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
--
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	Added ISNULL support
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cReturn VARCHAR(300)
	DECLARE @cSystemName NVARCHAR(100) = (SELECT C.ActivityCode FROM dbo.ConceptNote C WHERE C.ConceptNoteID = @ConceptNoteID)
	
	IF @cSystemName IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)
		FROM AJACSUtility.dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'

		END
	--ENDIF
	
	SELECT @cReturn = @cSystemName + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteTitle
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,ActualTotalAmount,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,Remarks,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,UpdateDate,UpdateNoteDocumentID,UpdateTypeID,VettingRequirements,WorkflowStepNumber)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,
		C.ActualTotalAmount,
		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.Remarks,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,
		'Clone of:  ' + C.Title,
		C.UpdateDate,
		C.UpdateNoteDocumentID,
		C.UpdateTypeID,
		C.VettingRequirements,
		1
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStep
		(EntityID, WorkflowStepID)
	SELECT
		@nConceptNoteID,
		WS.WorkflowStepID
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'

	INSERT INTO	dbo.ConceptNoteAmmendment
		(ConceptNoteID, AmmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Ammendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Ammendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.GetBookmarkByPersonID
EXEC Utility.DropObject 'dbo.GetBookmarkByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to get data from the dbo.ReorderBookmark table
-- ==============================================================================
CREATE PROCEDURE dbo.GetBookmarkByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		B.BookmarkID, 
		B.BookmarkName, 
		B.DisplayOrder, 
		B.PageURL,
		B.PersonID
	FROM dbo.Bookmark B
	WHERE B.PersonID = @PersonID
	ORDER BY B.DisplayOrder, B.BookmarkName, B.BookmarkID

END
GO
--End procedure dbo.GetBookmarkByPersonID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.ReorderBookmarkUp
EXEC Utility.DropObject 'dbo.ReorderBookmarkUp'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to reorder data in the dbo.ReorderBookmark table
-- ================================================================================
CREATE PROCEDURE dbo.ReorderBookmarkUp

@BookmarkID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nOldDisplayOrder INT
	DECLARE @nPersonID INT
	DECLARE @tTable TABLE (DisplayOrder INT NOT NULL PRIMARY KEY IDENTITY(1,1), BookmarkID INT NOT NULL DEFAULT 0)

	SELECT 
		@nOldDisplayOrder = B.DisplayOrder,
		@nPersonID = B.PersonID
	FROM dbo.Bookmark B
	WHERE B.BookmarkID = @BookmarkID

	IF @nOldDisplayOrder = 1
		BEGIN

		INSERT INTO @tTable 
			(BookmarkID) 
		SELECT 
			B.BookmarkID 
		FROM dbo.Bookmark B
		WHERE B.PersonID = @nPersonID
			AND B.BookmarkID <> @BookmarkID
		ORDER BY B.DisplayOrder, B.BookmarkID 

		INSERT INTO @tTable 
			(BookmarkID)
		VALUES
			(@BookmarkID)

		END
	ELSE
		BEGIN

		INSERT INTO @tTable 
			(BookmarkID) 
		SELECT 
			B.BookmarkID 
		FROM dbo.Bookmark B
		WHERE B.PersonID = @nPersonID
			AND B.DisplayOrder < @nOldDisplayOrder - 1
		ORDER BY B.DisplayOrder, B.BookmarkID 

		INSERT INTO @tTable 
			(BookmarkID)
		VALUES
			(@BookmarkID)

		INSERT INTO @tTable 
			(BookmarkID) 
		SELECT 
			B.BookmarkID 
		FROM dbo.Bookmark B
		WHERE B.PersonID = @nPersonID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tTable T
				WHERE T.BookmarkID = B.BookmarkID
				)
		ORDER BY B.DisplayOrder, B.BookmarkID 

		END
	--ENDIF

	UPDATE B
	SET B.DisplayOrder = T.DisplayOrder
	FROM dbo.Bookmark B
		JOIN @tTable T ON T.BookmarkID = B.BookmarkID

	SELECT 
		B.BookmarkID, 
		B.BookmarkName, 
		B.DisplayOrder, 
		B.PageURL
	FROM dbo.Bookmark B
	WHERE B.PersonID = @nPersonID
	ORDER BY B.DisplayOrder

END
GO
--End procedure dbo.ReorderBookmarkUp

--Begin procedure dbo.ReorderBookmarkDown
EXEC Utility.DropObject 'dbo.ReorderBookmarkDown'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to reorder data in the dbo.ReorderBookmark table
-- ================================================================================
CREATE PROCEDURE dbo.ReorderBookmarkDown

@BookmarkID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nMaxDisplayOrder INT
	DECLARE @nOldDisplayOrder INT
	DECLARE @nPersonID INT
	DECLARE @tTable TABLE (DisplayOrder INT NOT NULL PRIMARY KEY IDENTITY(1,1), BookmarkID INT NOT NULL DEFAULT 0)

	SELECT 
		@nMaxDisplayOrder = (SELECT COUNT(B1.BookmarkID) FROM dbo.Bookmark B1 JOIN dbo.Bookmark B2 ON B2.PersonID = B1.PersonID AND B1.BookmarkID = @BookmarkID),
		@nOldDisplayOrder = B3.DisplayOrder,
		@nPersonID = B3.PersonID
	FROM dbo.Bookmark B3
	WHERE B3.BookmarkID = @BookmarkID

IF @nOldDisplayOrder = @nMaxDisplayOrder
		BEGIN

		INSERT INTO @tTable 
			(BookmarkID)
		VALUES
			(@BookmarkID)

		INSERT INTO @tTable 
			(BookmarkID) 
		SELECT 
			B.BookmarkID 
		FROM dbo.Bookmark B
		WHERE B.PersonID = @nPersonID
			AND B.BookmarkID <> @BookmarkID
		ORDER BY B.DisplayOrder, B.BookmarkID 

		END
	ELSE
		BEGIN

		INSERT INTO @tTable 
			(BookmarkID) 
		SELECT 
			B.BookmarkID 
		FROM dbo.Bookmark B
		WHERE B.PersonID = @nPersonID
			AND B.DisplayOrder <= @nOldDisplayOrder + 1
		ORDER BY B.DisplayOrder, B.BookmarkID 

		INSERT INTO @tTable 
			(BookmarkID)
		VALUES
			(@BookmarkID)

		INSERT INTO @tTable 
			(BookmarkID) 
		SELECT 
			B.BookmarkID 
		FROM dbo.Bookmark B
		WHERE B.PersonID = @nPersonID
			AND NOT EXISTS
				(
				SELECT 1
				FROM @tTable T
				WHERE T.BookmarkID = B.BookmarkID
				)
		ORDER BY B.DisplayOrder, B.BookmarkID 

		END
	--ENDIF

	UPDATE B
	SET B.DisplayOrder = T.DisplayOrder
	FROM dbo.Bookmark B
		JOIN @tTable T ON T.BookmarkID = B.BookmarkID

	SELECT 
		B.BookmarkID, 
		B.BookmarkName, 
		B.DisplayOrder, 
		B.PageURL
	FROM dbo.Bookmark B
	WHERE B.PersonID = @nPersonID
	ORDER BY B.DisplayOrder

END
GO
--End procedure dbo.ReorderBookmarkDown

--Begin procedure dropdown.GetLogicalFrameworkStatusData
EXEC Utility.DropObject 'dropdown.GetLogicalFrameworkStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	A stored procedure to return data from the dropdown.LogicalFrameworkStatus table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetLogicalFrameworkStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LogicalFrameworkStatusID,
		T.LogicalFrameworkStatusName
	FROM dropdown.LogicalFrameworkStatus T
	WHERE (T.LogicalFrameworkStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LogicalFrameworkStatusName, T.LogicalFrameworkStatusID

END
GO
--End procedure dropdown.GetLogicalFrameworkStatusData

--Begin procedure eventlog.LogDataExportAction
EXEC utility.DropObject 'eventlog.LogDataExportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.25
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDataExportAction

@PersonID INT,
@DataExportActionXML VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EventData)
	VALUES
		(
		@PersonID,
		'create',
		'Export',
		@DataExportActionXML
		)

END
GO
--End procedure eventlog.LogDataExportAction

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
--
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	Add 'short' formats
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.ActualValue,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetIndicatorByObjectiveID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the logicalframework.Indicator table
--
-- Author:			Todd Pires
-- Create Date: 2015.08.27
-- Description:	Added additional fields
-- =====================================================================================
CREATE PROCEDURE logicalframework.GetIndicatorByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		I.AchievedDate,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormatted,
		I.AchievedValue,
		I.ActualDate,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormatted,
		I.ActualValue,
		I.BaselineDate,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormatted,
		I.BaselineValue,
		I.IndicatorDescription,
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorNumber,
		I.IndicatorSource,
		I.InProgressDate,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormatted,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormatted,
		I.PlannedValue,
		I.StatusUpdateDescription,
		I.TargetDate,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormatted,
		I.TargetValue,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName
  FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
			AND I.ObjectiveID = @ObjectiveID
	ORDER BY I.IndicatorID
	
END
GO
--End procedure logicalframework.GetIndicatorByObjectiveID

--Begin procedure procurement.GetConceptNoteContactEquipmentByConceptNoteID
EXEC Utility.DropObject 'procurement.GetConceptNoteContactEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to get data from the procurement.ConceptNoteContactEquipment table
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================================
CREATE PROCEDURE procurement.GetConceptNoteContactEquipmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		CNCE.Quantity,
		EC.ItemName,
		EI.SerialNumber
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CNCE.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND CNCE.ConceptNoteID = @ConceptNoteID
	ORDER BY FullName, ItemName
	
END
GO
--End procedure procurement.GetConceptNoteContactEquipmentByConceptNoteID

--Begin procedure project.GetProjectByProjectID
EXEC Utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.07
-- Description:	A stored procedure to get data from the project.Project table
--
-- Author:			Todd Pires
-- Create date: 2015.08.24
-- Description:	Added the ProjectCommunityValueHTML and ProjectProvinceValueHTML fields
-- ====================================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProjectID, 
		P.ProjectName, 
		P.ProjectDescription, 
		P.ProjectSummary, 
		P.ProjectPartner, 
		P.ProjectValue, 
		P.ConceptNoteID,
		dbo.FormatConceptNoteTitle(P.ConceptNoteID) AS ConceptNoteTitle,
		PS.ProjectStatusID,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND P.ProjectID = @ProjectID

	SELECT
		C.CommunityID,
		C.CommunityName,
		PC.ProjectCommunityValue,
		'<input type="number" id="CommunityValue' + CAST(C.CommunityID AS VARCHAR(10)) + '" name="CommunityValue' + CAST(C.CommunityID AS VARCHAR(10)) + '" class="form-control project-value" value="' + CAST(ISNULL(PC.ProjectCommunityValue, 0) AS VARCHAR(10)) + '">' AS ProjectCommunityValueHTML
	FROM project.ProjectCommunity PC
		JOIN dbo.Community C ON C.CommunityID = PC.CommunityID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusName,
		FT.FindingTypeName
	FROM project.ProjectFinding PF
		JOIN finding.Finding F ON F.FindingID = PF.FindingID
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND PF.ProjectID = @ProjectID
	ORDER BY F.FindingName, F.FindingID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM project.ProjectIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.ProjectID = @ProjectID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName,
		PP.ProjectProvinceValue,
		'<input type="number" id="ProvinceValue' + CAST(P.ProvinceID AS VARCHAR(10)) + '" name="ProvinceValue' + CAST(P.ProvinceID AS VARCHAR(10)) + '" class="form-control project-value" value="' + CAST(ISNULL(PP.ProjectProvinceValue, 0) AS VARCHAR(10)) + '">' AS ProjectProvinceValueHTML
	FROM project.ProjectProvince PP
		JOIN dbo.Province P ON P.ProvinceID = PP.ProvinceID
			AND PP.ProjectID = @ProjectID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RecommendationID,
		R.RecommendationName
	FROM project.ProjectRecommendation FR
		JOIN recommendation.Recommendation R ON R.RecommendationID = FR.RecommendationID
			AND FR.ProjectID = @ProjectID
	ORDER BY R.RecommendationName, R.RecommendationID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure recommendationupdate.ApproveRecommendationUpdateRecommendations
EXEC Utility.DropObject 'recommendationupdate.ApproveRecommendationUpdateRecommendations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to submit recommendations in a recommendation update for approval
-- =================================================================================================
CREATE PROCEDURE recommendationupdate.ApproveRecommendationUpdateRecommendations

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	
	DECLARE @RecommendationID INT
	DECLARE @RecommendationUpdateID INT = (SELECT TOP 1 RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU ORDER BY RU.RecommendationUpdateID DESC)
	DECLARE @tOutput TABLE (RecommendationID INT)

	UPDATE R
	SET
		R.IsActive = RUR.IsActive,
		R.IsResearchElement = RUR.IsResearchElement,
		R.RecommendationDescription = RUR.RecommendationDescription,
		R.RecommendationName = RUR.RecommendationName
	OUTPUT INSERTED.RecommendationID INTO @tOutput
	FROM recommendation.Recommendation R
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = R.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationCommunity T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationCommunity
		(RecommendationID, CommunityID)
	SELECT
		T.RecommendationID, 
		T.CommunityID
	FROM recommendationupdate.RecommendationCommunity T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationComponent T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationComponent
		(RecommendationID, ComponentID)
	SELECT
		T.RecommendationID, 
		T.ComponentID
	FROM recommendationupdate.RecommendationComponent T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationIndicator T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationIndicator
		(RecommendationID, IndicatorID)
	SELECT
		T.RecommendationID, 
		T.IndicatorID
	FROM recommendationupdate.RecommendationIndicator T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationProvince T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationProvince
		(RecommendationID, ProvinceID)
	SELECT
		T.RecommendationID, 
		T.ProvinceID
	FROM recommendationupdate.RecommendationProvince T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DELETE T
	FROM recommendation.RecommendationRisk T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	INSERT INTO recommendation.RecommendationRisk
		(RecommendationID, RiskID)
	SELECT
		T.RecommendationID, 
		T.RiskID
	FROM recommendationupdate.RecommendationRisk T
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = T.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.RecommendationID
		FROM @tOutput O
		ORDER BY O.RecommendationID
	
	OPEN oCursor
	FETCH oCursor INTO @RecommendationID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'read', @PersonID, NULL
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @RecommendationID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'update', @PersonID, NULL
	
	TRUNCATE TABLE recommendationupdate.Recommendation
	DELETE FROM recommendationupdate.RecommendationUpdate

END
GO
--End procedure recommendationupdate.ApproveRecommendationUpdateRecommendations

--Begin procedure recommendationupdate.DeleteRecommendationUpdateRecommendation
EXEC Utility.DropObject 'recommendationupdate.DeleteRecommendationUpdateRecommendation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to remove a recommendation from the recommendation update
--
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	Added one to many table support
-- =========================================================================================
CREATE PROCEDURE recommendationupdate.DeleteRecommendationUpdateRecommendation

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM recommendationupdate.Recommendation T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationCommunity T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationComponent T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationIndicator T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationProvince T
	WHERE T.RecommendationID = @RecommendationID

	DELETE T
	FROM recommendationupdate.RecommendationRisk T
	WHERE T.RecommendationID = @RecommendationID

END
GO
--End procedure recommendationupdate.DeleteRecommendationUpdateRecommendation

--Begin procedure recommendationupdate.GetRecommendationUpdateByRecommendationID
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdateByRecommendationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.26
-- Description:	A stored procedure to get data from the recommendationupdate.Recommendation table
-- ==============================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdateByRecommendationID

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendationupdate.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM recommendationupdate.RecommendationCommunity RC
		JOIN dbo.Community C ON C.CommunityID = RC.CommunityID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		C.ComponentID,
		C.ComponentName
	FROM recommendationupdate.RecommendationComponent RC
		JOIN dropdown.Component C ON C.ComponentID = RC.ComponentID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.ComponentName, C.ComponentID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM recommendationupdate.RecommendationIndicator RI
		JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND RI.RecommendationID = @RecommendationID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM recommendationupdate.RecommendationProvince RP
		JOIN dbo.Province P ON P.ProvinceID = RP.ProvinceID
			AND RP.RecommendationID = @RecommendationID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RT.RiskTypeName
	FROM recommendationupdate.RecommendationRisk RR
		JOIN dbo.Risk R ON R.RiskID = RR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND RR.RecommendationID = @RecommendationID
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure recommendationupdate.GetRecommendationUpdateByRecommendationID

--Begin procedure recommendationupdate.PopulateRecommendationUpdateRecommendations
EXEC Utility.DropObject 'recommendationupdate.PopulateRecommendationUpdateRecommendations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.26
-- Description:	A stored procedure to add recommendation data to the recommendation update
-- =======================================================================================
CREATE PROCEDURE recommendationupdate.PopulateRecommendationUpdateRecommendations

@RecommendationIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO recommendationupdate.Recommendation
		(RecommendationID, RecommendationUpdateID, RecommendationName, RecommendationDescription, IsResearchElement, IsActive)
	SELECT
		R.RecommendationID, 
		(SELECT TOP 1 RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU ORDER BY RU.RecommendationUpdateID DESC),
		R.RecommendationName, 
		R.RecommendationDescription, 
		R.IsResearchElement, 
		R.IsActive
	FROM recommendation.Recommendation R
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = R.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.Recommendation RUR
				WHERE RUR.RecommendationID = R.RecommendationID
				)

	INSERT INTO recommendationupdate.RecommendationCommunity
		(RecommendationID, CommunityID)
	SELECT
		RC.RecommendationID,
		RC.CommunityID
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.RecommendationCommunity RUC
				WHERE RUC.RecommendationID = RC.RecommendationID
				)

	INSERT INTO recommendationupdate.RecommendationComponent
		(RecommendationID, ComponentID)
	SELECT
		RC.RecommendationID,
		RC.ComponentID
	FROM recommendation.RecommendationComponent RC
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.RecommendationComponent RUC
				WHERE RUC.RecommendationID = RC.RecommendationID
				)

	INSERT INTO recommendationupdate.RecommendationIndicator
		(RecommendationID, IndicatorID)
	SELECT
		RI.RecommendationID,
		RI.IndicatorID
	FROM recommendation.RecommendationIndicator RI
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RI.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.RecommendationIndicator RUI
				WHERE RUI.RecommendationID = RI.RecommendationID
				)

	INSERT INTO recommendationupdate.RecommendationProvince
		(RecommendationID, ProvinceID)
	SELECT
		RP.RecommendationID,
		RP.ProvinceID
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.RecommendationProvince RUP
				WHERE RUP.RecommendationID = RP.RecommendationID
				)

	INSERT INTO recommendationupdate.RecommendationRisk
		(RecommendationID, RiskID)
	SELECT
		RR.RecommendationID,
		RR.RiskID
	FROM recommendation.RecommendationRisk RR
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RR.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.RecommendationRisk RUR
				WHERE RUR.RecommendationID = RR.RecommendationID
				)

END
GO
--End procedure recommendationupdate.PopulateRecommendationUpdateRecommendations

--Begin procedure reporting.GetCommunityMemberSurvey
EXEC Utility.DropObject 'reporting.GetCommunityMemberSurvey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.25
-- Description:	A stored procedure to get data FROM the dbo.CommunityMemberSurvey table
-- ====================================================================================
CREATE PROCEDURE reporting.GetCommunityMemberSurvey

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityName, 
		CMS.*
	FROM dbo.CommunityMemberSurvey CMS
		JOIN dbo.Community C ON C.CommunityID = CMS.CommunityID
		JOIN reporting.SearchResult SR ON SR.PersonID = @PersonID
			AND SR.EntityTypeGroupCode = 'RAPData'
			AND SR.EntityTypeCode = 'CommunityMemberSurvey'
			AND SR.EntityID = CMS.CommunityMemberSurveyID
	ORDER BY C.CommunityName, CMS.CommunityMemberSurveyID

END
GO
--End procedure reporting.GetCommunityMemberSurvey

--Begin procedure reporting.GetFocusGroupSurvey
EXEC Utility.DropObject 'reporting.GetFocusGroupSurvey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.25
-- Description:	A stored procedure to get data FROM the dbo.FocusGroupSurvey table
-- ===============================================================================
CREATE PROCEDURE reporting.GetFocusGroupSurvey

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityName, 
		FGS.*
	FROM dbo.FocusGroupSurvey FGS
		JOIN dbo.Community C ON C.CommunityID = FGS.CommunityID
		JOIN reporting.SearchResult SR ON SR.PersonID = @PersonID
			AND SR.EntityTypeGroupCode = 'RAPData'
			AND SR.EntityTypeCode = 'FocusGroupSurvey'
			AND SR.EntityID = FGS.FocusGroupSurveyID
	ORDER BY C.CommunityName, FGS.FocusGroupSurveyID

END
GO
--End procedure reporting.GetFocusGroupSurvey

--Begin procedure reporting.GetKeyInformantSurvey
EXEC Utility.DropObject 'reporting.GetKeyInformantSurvey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.25
-- Description:	A stored procedure to get data FROM the dbo.KeyInformantSurvey table
-- =================================================================================
CREATE PROCEDURE reporting.GetKeyInformantSurvey

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityName, 
		KIS.*
	FROM dbo.KeyInformantSurvey KIS
		JOIN dbo.Community C ON C.CommunityID = KIS.CommunityID
		JOIN reporting.SearchResult SR ON SR.PersonID = @PersonID
			AND SR.EntityTypeGroupCode = 'RAPData'
			AND SR.EntityTypeCode = 'KeyInformantSurvey'
			AND SR.EntityID = KIS.KeyInformantSurveyID
	ORDER BY C.CommunityName, KIS.KeyInformantSurveyID

END
GO
--End procedure reporting.GetKeyInformantSurvey

--Begin procedure reporting.GetStakeholderGroupSurvey
EXEC Utility.DropObject 'reporting.GetStakeholderGroupSurvey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.25
-- Description:	A stored procedure to get data FROM the dbo.StakeholderGroupSurvey table
-- =================================================================================
CREATE PROCEDURE reporting.GetStakeholderGroupSurvey

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityName, 
		SGS.*
	FROM dbo.StakeholderGroupSurvey SGS
		JOIN dbo.Community C ON C.CommunityID = SGS.CommunityID
		JOIN reporting.SearchResult SR ON SR.PersonID = @PersonID
			AND SR.EntityTypeGroupCode = 'RAPData'
			AND SR.EntityTypeCode = 'StakeholderGroupSurvey'
			AND SR.EntityID = SGS.StakeholderGroupSurveyID
	ORDER BY C.CommunityName, SGS.StakeholderGroupSurveyID

END
GO
--End procedure reporting.GetStakeholderGroupSurvey

--Begin procedure reporting.GetStationCommanderSurvey
EXEC Utility.DropObject 'reporting.GetStationCommanderSurvey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.25
-- Description:	A stored procedure to get data FROM the dbo.StationCommanderSurvey table
-- =====================================================================================
CREATE PROCEDURE reporting.GetStationCommanderSurvey

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityName, 
		SCS.*
	FROM dbo.StationCommanderSurvey SCS
		JOIN dbo.Community C ON C.CommunityID = SCS.CommunityID
		JOIN reporting.SearchResult SR ON SR.PersonID = @PersonID
			AND SR.EntityTypeGroupCode = 'RAPData'
			AND SR.EntityTypeCode = 'StationCommanderSurvey'
			AND SR.EntityID = SCS.StationCommanderSurveyID
	ORDER BY C.CommunityName, SCS.StationCommanderSurveyID

END
GO
--End procedure reporting.GetStationCommanderSurvey

--Begin procedure workflow.CanIncrementRecommendationUpdateWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementRecommendationUpdateWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementRecommendationUpdateWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
			JOIN recommendationupdate.RecommendationUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RecommendationUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementRecommendationUpdateWorkflow

--Begin procedure workflow.GetRecommendationUpdateWorkflowData
EXEC Utility.DropObject 'workflow.GetRecommendationUpdateWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.25
-- Description:	A procedure to return workflow data from the recommendationupdate.RecommendationUpdate table
-- =========================================================================================================

CREATE PROCEDURE workflow.GetRecommendationUpdateWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
			JOIN recommendationupdate.RecommendationUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RecommendationUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'Risk.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'RecommendationUpdate'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Risk Updates'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Risk Updates'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Risk Updates'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Risk Updates'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'RecommendationUpdate'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetRecommendationUpdateWorkflowData

--Begin procedure workflow.GetRecommendationUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetRecommendationUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.25
-- Description:	A stored procedure to people associated with the workflow steps on a recommendation update
-- =======================================================================================================
CREATE PROCEDURE workflow.GetRecommendationUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'Risk.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetRecommendationUpdateWorkflowStepPeople
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Export')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Export', 'Export')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'RAPData')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('RAPData', 'RAP Data')
--ENDIF
GO
--End table dbo.EntityType

--Begin table dropdown.FundingSource
IF NOT EXISTS (SELECT 1 FROM dropdown.FundingSource FS WHERE FS.FundingSourceName = 'US - NEA')
	BEGIN
	
	INSERT INTO dropdown.FundingSource 
		(FundingSourceName, DisplayOrder, IsActive)
	VALUES
		('US - NEA', 2, 1),
		('US - OPS', 3, 1)
		
	END
--ENDIF
GO

UPDATE dropdown.FundingSource 
SET 
	FundingSourceName = 'US - CSO',
	DisplayOrder = 1
WHERE FundingSourceName = 'US';
GO

UPDATE dropdown.FundingSource 
SET IsActive = 0
WHERE FundingSourceName = 'Joint';
GO

UPDATE dropdown.FundingSource
SET DisplayOrder = 4
WHERE FundingSourceName = 'EU';
GO
--End table dropdown.FundingSource

--Begin table permissionable.Permissionable
UPDATE P1
SET 
	P1.PermissionableCode = 'Export',
	P1.PermissionableName = 'Export',
	P1.ParentPermissionableID = (SELECT P2.PermissionableID FROM permissionable.Permissionable P2 WHERE P2.PermissionableLineage = 'RAPData.List')
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'DownloadRAPData'
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportEquipmentDistributions',
	'Export Equipment Distributions',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Community.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Community.View.ExportEquipmentDistributions'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportConceptNoteBudget',
	'Export Budget',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.AddUpdate.ExportConceptNoteBudget'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ViewBudget',
	'View Budget',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.View.ViewBudget'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNoteContactEquipment.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNoteContactEquipment.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportPayees',
	'Export Payees',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.ExportPayees'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'CashHandOverExport',
	'Export Cash Handover Sheet',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.CashHandOverExport'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'OpFundsReport',
	'Export Cash Handover Sheet',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.OpFundsReport'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StipendActivity',
	'Export Cash Handover Sheet',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.StipendActivity'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StipendPaymentReport',
	'Export Stipend Payment Summary',
	4
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.StipendPaymentReport'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'EquipmentDistributionPlan',
	'Export Equipment Distribution Plan',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.View.EquipmentDistributionPlan'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'EquipmentHandoverSheet',
	'Export Equipment Handover Sheet',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.View.EquipmentHandoverSheet'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentInventory.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentInventory.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'LicenseEquipmentCatalog.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'LicenseEquipmentCatalog.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ProgramReport.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ProgramReport.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportEquipmentDistribution',
	'Export Equipment Distribution',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Province.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Province.View.ExportEquipmentDistribution'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PurchaseRequest.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PurchaseRequest.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Recommendation.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Recommendation.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'RequestForInformation.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'RequestForInformation.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Risk.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Risk.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'SpotReport.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'SpotReport.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportSurveyResponses',
	'Export Survey Responses',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'SurveyManagement'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'SurveyManagement.ExportSurveyResponses'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'WeeklyReport.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'WeeklyReport.AddUpdate.Export'
		)
GO
--End table permissionable.Permissionable

--Begin new conceptnote workflow steps
DECLARE @ParentWorkflowStepID INT
DECLARE @WorkflowID INT

SELECT @ParentWorkflowStepID = WorkflowStepID
FROM workflow.WorkflowStep 
WHERE WorkflowStepName = 'Activity Closedown';

SELECT @WorkflowID = WorkflowID
FROM workflow.Workflow
WHERE EntityTypeCode = 'ConceptNote';

INSERT INTO workflow.WorkflowStep (ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
VALUES
	(@ParentWorkflowStepID, @WorkflowID, 9, 'Activity Implementer Reporting'),
	(@ParentWorkflowStepID, @WorkflowID, 9, 'M&E Review'),
	(@ParentWorkflowStepID, @WorkflowID, 9, 'Activity Manager Review and Approval'),
	(@ParentWorkflowStepID, @WorkflowID, 9, 'Finalize');

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'ConceptNote'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'ConceptNote.AddUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableName = 'Activity Closedown'), 
	T.PermissionableCode, 
	T.PermissionableName, 
	CASE T.PermissionableName
		WHEN 'Activity Implementer Reporting' 
		THEN 1
		WHEN 'M&E Review' 
		THEN 2
		WHEN 'Activity Manager Review and Approval' 
		THEN 3
		WHEN 'Finalize' 
		THEN 4
	END,
	1
FROM @tTable T
	JOIN (SELECT WS.WorkflowStepID FROM workflow.WorkflowStep WS WHERE WS.WorkflowStepName = 'Activity Closedown') x ON x.WorkflowStepID = T.ParentWorkflowStepID
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO
--End new conceptnote workflow steps

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.25 File 01 - AJACS - 2015.08.28 19.50.43')
GO
--End build tracking

