USE AJACS
GO

--Begin function dbo.FormatConceptNoteReferenceCode
EXEC utility.DropObject 'dbo.FormatConceptNoteReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- =========================================================================

CREATE FUNCTION dbo.FormatConceptNoteReferenceCode
(
@ConceptNoteID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(100) = (SELECT C.ActivityCode FROM dbo.ConceptNote C WHERE C.ConceptNoteID = @ConceptNoteID)
	DECLARE @cSystemName VARCHAR(50)

	IF @cReturn IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue
		FROM AJACSUtility.dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'
				
		SET @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)

		END
	--ENDIF

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteReferenceCode

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
--
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	Added ISNULL support
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2015.08.27
-- Description:	Implemented the ActivityCode field
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cReturn VARCHAR(300)
	DECLARE @cSystemName NVARCHAR(100) = (SELECT C.ActivityCode FROM dbo.ConceptNote C WHERE C.ConceptNoteID = @ConceptNoteID)
	
	IF @cSystemName IS NULL
		BEGIN
		
		SELECT @cSystemName = SS.ServerSetupValue + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)
		FROM AJACSUtility.dbo.ServerSetup SS
		WHERE SS.ServerSetupKey = 'SystemName'

		END
	--ENDIF
	
	SELECT @cReturn = @cSystemName + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteTitle