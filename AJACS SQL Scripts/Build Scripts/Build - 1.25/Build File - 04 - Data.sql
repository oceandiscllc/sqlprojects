USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Export')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Export', 'Export')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'RAPData')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('RAPData', 'RAP Data')
--ENDIF
GO
--End table dbo.EntityType

--Begin table dropdown.FundingSource
IF NOT EXISTS (SELECT 1 FROM dropdown.FundingSource FS WHERE FS.FundingSourceName = 'US - NEA')
	BEGIN
	
	INSERT INTO dropdown.FundingSource 
		(FundingSourceName, DisplayOrder, IsActive)
	VALUES
		('US - NEA', 2, 1),
		('US - OPS', 3, 1)
		
	END
--ENDIF
GO

UPDATE dropdown.FundingSource 
SET 
	FundingSourceName = 'US - CSO',
	DisplayOrder = 1
WHERE FundingSourceName = 'US';
GO

UPDATE dropdown.FundingSource 
SET IsActive = 0
WHERE FundingSourceName = 'Joint';
GO

UPDATE dropdown.FundingSource
SET DisplayOrder = 4
WHERE FundingSourceName = 'EU';
GO
--End table dropdown.FundingSource

--Begin table permissionable.Permissionable
UPDATE P1
SET 
	P1.PermissionableCode = 'Export',
	P1.PermissionableName = 'Export',
	P1.ParentPermissionableID = (SELECT P2.PermissionableID FROM permissionable.Permissionable P2 WHERE P2.PermissionableLineage = 'RAPData.List')
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'DownloadRAPData'
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportEquipmentDistributions',
	'Export Equipment Distributions',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Community.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Community.View.ExportEquipmentDistributions'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportConceptNoteBudget',
	'Export Budget',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.AddUpdate.ExportConceptNoteBudget'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ViewBudget',
	'View Budget',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.View.ViewBudget'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNoteContactEquipment.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNoteContactEquipment.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportPayees',
	'Export Payees',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.ExportPayees'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'CashHandOverExport',
	'Export Cash Handover Sheet',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.CashHandOverExport'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'OpFundsReport',
	'Export Cash Handover Sheet',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.OpFundsReport'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StipendActivity',
	'Export Cash Handover Sheet',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.StipendActivity'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'StipendPaymentReport',
	'Export Stipend Payment Summary',
	4
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.PaymentList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.PaymentList.StipendPaymentReport'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'EquipmentDistributionPlan',
	'Export Equipment Distribution Plan',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.View.EquipmentDistributionPlan'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'EquipmentHandoverSheet',
	'Export Equipment Handover Sheet',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.View.EquipmentHandoverSheet'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentInventory.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentInventory.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'LicenseEquipmentCatalog.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'LicenseEquipmentCatalog.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ProgramReport.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ProgramReport.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportEquipmentDistribution',
	'Export Equipment Distribution',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Province.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Province.View.ExportEquipmentDistribution'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PurchaseRequest.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PurchaseRequest.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Recommendation.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Recommendation.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'RequestForInformation.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'RequestForInformation.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Risk.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Risk.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'SpotReport.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'SpotReport.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportSurveyResponses',
	'Export Survey Responses',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'SurveyManagement'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'SurveyManagement.ExportSurveyResponses'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'WeeklyReport.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'WeeklyReport.AddUpdate.Export'
		)
GO
--End table permissionable.Permissionable

--Begin new conceptnote workflow steps
DECLARE @ParentWorkflowStepID INT
DECLARE @WorkflowID INT

SELECT @ParentWorkflowStepID = WorkflowStepID
FROM workflow.WorkflowStep 
WHERE WorkflowStepName = 'Activity Closedown';

SELECT @WorkflowID = WorkflowID
FROM workflow.Workflow
WHERE EntityTypeCode = 'ConceptNote';

INSERT INTO workflow.WorkflowStep (ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
VALUES
	(@ParentWorkflowStepID, @WorkflowID, 9, 'Activity Implementer Reporting'),
	(@ParentWorkflowStepID, @WorkflowID, 9, 'M&E Review'),
	(@ParentWorkflowStepID, @WorkflowID, 9, 'Activity Manager Review and Approval'),
	(@ParentWorkflowStepID, @WorkflowID, 9, 'Finalize');

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'ConceptNote'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'ConceptNote.AddUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableName = 'Activity Closedown'), 
	T.PermissionableCode, 
	T.PermissionableName, 
	CASE T.PermissionableName
		WHEN 'Activity Implementer Reporting' 
		THEN 1
		WHEN 'M&E Review' 
		THEN 2
		WHEN 'Activity Manager Review and Approval' 
		THEN 3
		WHEN 'Finalize' 
		THEN 4
	END,
	1
FROM @tTable T
	JOIN (SELECT WS.WorkflowStepID FROM workflow.WorkflowStep WS WHERE WS.WorkflowStepName = 'Activity Closedown') x ON x.WorkflowStepID = T.ParentWorkflowStepID
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO
--End new conceptnote workflow steps

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
