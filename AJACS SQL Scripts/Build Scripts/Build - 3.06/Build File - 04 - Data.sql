USE AJACS
GO

--Begin table dropdown.IndicatorStatus
TRUNCATE TABLE dropdown.IndicatorStatus
GO

SET IDENTITY_INSERT dropdown.IndicatorStatus ON
GO
INSERT INTO dropdown.IndicatorStatus (IndicatorStatusID) VALUES (CAST(0 AS VARCHAR(10)))
GO
SET IDENTITY_INSERT dropdown.IndicatorStatus OFF;
GO

INSERT INTO dropdown.IndicatorStatus 
	(IndicatorStatusName)
VALUES
	('On Track'),
	('Progressing'),
	('Off Track')
GO
--End table dropdown.IndicatorStatus

UPDATE logicalframework.Indicator SET IndicatorStatusID = 1 WHERE IndicatorStatusID IS NULL
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='ObjectiveOverview', 
	@NewMenuItemLink='/logicalframework/overview', 
	@NewMenuItemText='Overview', 
	@BeforeMenuItemCode='LogicalFrameworkUpdate', 
	@PermissionableLineageList='LogicalFramework.Overview'
GO

EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ1Month', @ServerSetupValue = '12'
GO
EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ2Month', @ServerSetupValue = '3'
GO
EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ3Month', @ServerSetupValue = '7'
GO
EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ4Month', @ServerSetupValue = '9'
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Export Class Lists', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LogicalFramework', @DESCRIPTION='View M&E Overview', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='LogicalFramework.Overview', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
