-- File Name:	Build - 3.06 - AJACS.sql
-- Build Key:	Build - 3.06 - 2018.01.18 18.01.51

USE AJACS
GO

-- ==============================================================================================================================
-- Tables:
--		dbo.ClassTrainer
--		dropdown.IndicatorStatus
--
-- Procedures:
--		asset.GetAssetPaymentHistoryByAssetID
--		dbo.GetClassByClassID
--		dbo.GetConceptNotePaymentHistoryByConceptNoteID
--		dbo.TransferContacts
--		dropdown.GetIndicatorStatusData
--		logicalframework.GetIndicatorByIndicatorID
--		logicalframework.GetLogicalFrameworkOverviewData
--		reporting.GetClasses
--		weeklyreport.PopulateWeeklyReportCommunities
--		weeklyreport.PopulateWeeklyReportProvinces
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.ClassTrainer
DECLARE @TableName VARCHAR(250) = 'dbo.ClassTrainer'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ClassTrainer
	(
	ClassTrainerID INT IDENTITY(1,1) NOT NULL,
	ClassID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClassTrainerID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ClassTrainer', 'ClassID,ContactID'
GO
--End table dbo.ClassTrainer

--Begin table dropdown.IndicatorStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IndicatorStatus
	(
	IndicatorStatusID INT IDENTITY(0,1) NOT NULL,
	IndicatorStatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IndicatorStatus', 'DisplayOrder,IndicatorStatusName', 'IndicatorStatusID'
GO
--End table dropdown.IndicatorStatus

--Begin table logicalframework.Indicator
EXEC Utility.DropColumn 'logicalframework.Indicator', 'DataCollectionPlanSource'
EXEC Utility.DropColumn 'logicalframework.Indicator', 'DataCollectionPlanFrequency'
EXEC Utility.DropColumn 'logicalframework.Indicator', 'StatusDescription'
GO

EXEC Utility.AddColumn 'logicalframework.Indicator', 'DataCollectionPlanSource', 'VARCHAR(MAX)'
EXEC Utility.AddColumn 'logicalframework.Indicator', 'DataCollectionPlanFrequency', 'VARCHAR(MAX)'
EXEC Utility.AddColumn 'logicalframework.Indicator', 'StatusDescription', 'VARCHAR(MAX)'
EXEC Utility.AddColumn 'logicalframework.Indicator', 'IndicatorStatusID', 'INT', '0'
GO
--End table logicalframework.Indicator

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure asset.GetAssetPaymentHistoryByAssetID
EXEC Utility.DropObject 'asset.GetAssetPaymentHistoryByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.05
-- Description:	A stored procedure to get payment history data for an asset
-- ========================================================================
CREATE PROCEDURE asset.GetAssetPaymentHistoryByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE
		(
		ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		PaymentYear INT DEFAULT 0,
		PaymentMonth INT DEFAULT 0,
		AssetID INT DEFAULT 0,
		StipendAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		StipendAmountPaid NUMERIC(18,2) DEFAULT 0,
		PayeeCount INT DEFAULT 0,
		ExpenseAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		ExpenseAmountPaid NUMERIC(18,2) DEFAULT 0,
		AssetDepartmentCount INT DEFAULT 0
		)

	INSERT INTO @tTable1
		(PaymentYear, PaymentMonth, StipendAmountAuthorized, StipendAmountPaid, AssetID, PayeeCount)
	SELECT
		CSP.PaymentYear,
		CSP.PaymentMonth,
		IIF (CSP.StipendAuthorizedDate IS NOT NULL, SUM(CSP.StipendAmountAuthorized), 0) AS StipendAmountAuthorized,
		IIF (CSP.StipendPaidDate IS NOT NULL, SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaid,
		A.AssetID,
		COUNT(CSP.ContactID) AS PayeeCount
	FROM dbo.ContactStipendPayment CSP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = CSP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
	GROUP BY A.AssetID, A.AssetName, CSP.PaymentYear, CSP.PaymentMonth, CSP.StipendPaidDate, CSP.StipendAuthorizedDate

	MERGE @tTable1 T1
	USING
		(
		SELECT
			AUE.PaymentYear,
			AUE.PaymentMonth,
			IIF (AUE.ExpenseAuthorizedDate IS NOT NULL, SUM(AUE.ExpenseAmountAuthorized), 0) AS ExpenseAmountAuthorized,
			IIF (AUE.ExpensePaidDate IS NOT NULL, SUM(AUE.ExpenseAmountPaid), 0) AS ExpenseAmountPaid,
			A.AssetID,
			COUNT(AUE.AssetUnitID) AS AssetDepartmentCount
		FROM asset.AssetUnitExpense AUE
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
				AND A.AssetID = @AssetID
		GROUP BY A.AssetID, A.AssetName, AUE.PaymentYear, AUE.PaymentMonth, AUE.ExpenseAuthorizedDate, AUE.ExpensePaidDate
		) T3 ON T3.PaymentYear = T1.PaymentYear 
			AND T3.PaymentMonth = T1.PaymentMonth
			AND T3.AssetID = T1.AssetID 
	WHEN NOT MATCHED THEN
	INSERT
		(PaymentYear, PaymentMonth, ExpenseAmountAuthorized, ExpenseAmountPaid, AssetID, AssetDepartmentCount)
	VALUES
		(
		T3.PaymentYear, 
		T3.PaymentMonth, 
		T3.ExpenseAmountAuthorized, 
		T3.ExpenseAmountPaid, 
		T3.AssetID, 
		T3.AssetDepartmentCount
		)
	WHEN MATCHED THEN
	UPDATE 
	SET 
		T1.ExpenseAmountAuthorized = T3.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid = T3.ExpenseAmountPaid, 
		T1.AssetDepartmentCount = T3.AssetDepartmentCount
	;

	SELECT 
		T1.PaymentYear,
		RIGHT('00' + CAST(T1.PaymentMonth AS VARCHAR(2)), 2) AS PaymentMonthFormatted,
		T1.StipendAmountAuthorized,
		T1.StipendAmountPaid,
		T1.PayeeCount,
		T1.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid, 
		T1.AssetDepartmentCount
	FROM @tTable1 T1
		JOIN asset.Asset A ON A.AssetID = T1.AssetID
	ORDER BY T1.PaymentYear DESC, T1.PaymentMonth DESC, A.AssetName

END
GO
--End procedure asset.GetAssetPaymentHistoryByAssetID

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
--
-- Author:			Todd Pires
-- Update date:	2015.05.28
-- Description:	Added vetting data
--
-- Author:			Todd Pires
-- Update date:	2016.09.09
-- Description:	Pointed to the dbo.ContactVetting table
--
-- Author:			Jonathan Burnham
-- Update date:	2017.12.31
-- Description:	Adding Trainers
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Class
	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
	
	--ClassContact
	SELECT
		dbo.GetContactCommunityByContactID(CO.ContactID) AS ContactLocation,
		CO.ContactID,
		CNC2.VettingIcon,
		CNC2.VettingOutcomeName,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT TOP 1

				CASE
					WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
					THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
					ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
				END AS VettingIcon,

				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND CV.ContactID = CC.ContactID
			ORDER BY CV.ContactVettingID DESC
			) CNC2

	--ConceptNoteClass
	SELECT 
		CNC1.ConceptNoteID,
		CN.Title
	FROM dbo.ConceptNoteClass CNC1
	JOIN dbo.ConceptNote CN ON CNC1.ConceptNoteID = CN.ConceptNoteID
	WHERE CNC1.ClassID = @ClassID

	--TrainerContact
	SELECT 
		CT.ContactID,
		dbo.FormatContactNameByContactID(CT.ContactID, 'LastFirstMiddle') AS TrainerFullNameFormatted
	FROM dbo.ClassTrainer CT
	WHERE CT.ClassID = @ClassID

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetConceptNotePaymentHistoryByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNotePaymentHistoryByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.05
-- Description:	A stored procedure to get payment history data for a ConceptNote
-- =============================================================================
CREATE PROCEDURE dbo.GetConceptNotePaymentHistoryByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tParentCNs TABLE (ConceptNoteID INT, Title VARCHAR(250), AmendedConceptNoteID INT)

	;
	WITH CNCTE AS 
		( 
		SELECT 
			CN.ConceptNoteID, 
			CN.Title, 
			CN.AmendedConceptNoteID
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.Title, 
			CN.AmendedConceptNoteID
		FROM dbo.ConceptNote CN
			JOIN CNCTE ON CNCTE.AmendedConceptNoteID = CN.ConceptNoteID
		)

	INSERT INTO @tParentCNs 
		(ConceptNoteID, Title, AmendedConceptNoteID)
	SELECT 
		CNCTE.ConceptNoteID, 
		CNCTE.Title, 
		CNCTE.AmendedConceptNoteID
	FROM CNCTE

	DECLARE @tTable1 TABLE
		(
		ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		PaymentYear INT DEFAULT 0,
		PaymentMonth INT DEFAULT 0,
		ConceptNoteID INT DEFAULT 0,
		StipendAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		StipendAmountPaid NUMERIC(18,2) DEFAULT 0,
		ProvinceID INT DEFAULT 0,
		PayeeCount INT DEFAULT 0,
		ExpenseAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		ExpenseAmountPaid NUMERIC(18,2) DEFAULT 0,
		AssetDepartmentCount INT DEFAULT 0
		)

	INSERT INTO @tTable1
		(PaymentYear, PaymentMonth, StipendAmountAuthorized, StipendAmountPaid, ProvinceID, PayeeCount)
	SELECT
		CSP.PaymentYear,
		CSP.PaymentMonth,
		IIF (CSP.StipendAuthorizedDate IS NOT NULL, SUM(CSP.StipendAmountAuthorized), 0) AS StipendAmountAuthorized,
		IIF (CSP.StipendPaidDate IS NOT NULL, SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaid,
		CSP.ProvinceID,
		COUNT(CSP.ContactID) AS PayeeCount
	FROM dbo.ContactStipendPayment CSP
	WHERE EXISTS
		(
		SELECT 1
		FROM @tParentCNs T
		WHERE T.ConceptNoteID = CSP.ConceptNoteID
		)
	GROUP BY CSP.ProvinceID, CSP.PaymentYear, CSP.PaymentMonth, CSP.StipendPaidDate, CSP.StipendAuthorizedDate

	MERGE @tTable1 T1
	USING
		(
		SELECT
			AUE.PaymentYear,
			AUE.PaymentMonth,
			IIF (AUE.ExpenseAuthorizedDate IS NOT NULL, SUM(AUE.ExpenseAmountAuthorized), 0) AS ExpenseAmountAuthorized,
			IIF (AUE.ExpensePaidDate IS NOT NULL, SUM(AUE.ExpenseAmountPaid), 0) AS ExpenseAmountPaid,
			C.ProvinceID,
			COUNT(AUE.AssetUnitID) AS AssetDepartmentCount
		FROM asset.AssetUnitExpense AUE
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dbo.Community C ON C.CommunityID = A.CommunityID
				AND EXISTS
					(
					SELECT 1
					FROM @tParentCNs T
					WHERE T.ConceptNoteID = AUE.ConceptNoteID
					)
		GROUP BY C.ProvinceID, AUE.PaymentYear, AUE.PaymentMonth, AUE.ExpenseAuthorizedDate, AUE.ExpensePaidDate
		) T3 ON T3.ProvinceID = T1.ProvinceID 
			AND T3.PaymentYear = T1.PaymentYear 
			AND T3.PaymentMonth = T1.PaymentMonth
	WHEN NOT MATCHED THEN
	INSERT
		(PaymentYear, PaymentMonth, ExpenseAmountAuthorized, ExpenseAmountPaid, ProvinceID, AssetDepartmentCount)
	VALUES
		(
		T3.PaymentYear, 
		T3.PaymentMonth, 
		T3.ExpenseAmountAuthorized, 
		T3.ExpenseAmountPaid, 
		T3.ProvinceID, 
		T3.AssetDepartmentCount
		)
	WHEN MATCHED THEN
	UPDATE 
	SET 
		T1.ExpenseAmountAuthorized = T3.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid = T3.ExpenseAmountPaid, 
		T1.AssetDepartmentCount = T3.AssetDepartmentCount
	;

	SELECT 
		P.ProvinceName,
		T1.PaymentYear,
		RIGHT('00' + CAST(T1.PaymentMonth AS VARCHAR(2)), 2) AS PaymentMonthFormatted,
		T1.StipendAmountAuthorized,
		T1.StipendAmountPaid,
		T1.PayeeCount,
		T1.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid, 
		T1.AssetDepartmentCount
	FROM @tTable1 T1
		JOIN dbo.Province P ON P.ProvinceID = T1.ProvinceID
	ORDER BY P.ProvinceName, T1.PaymentYear DESC, T1.PaymentMonth DESC

END
GO
--End procedure dbo.GetConceptNotePaymentHistoryByConceptNoteID

--Begin procedure dbo.TransferContacts
EXEC Utility.DropObject 'dbo.TransferContacts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.19
-- Description:	A stored procedure bulk to transfer contacts to a community or an asset unit
-- =========================================================================================
CREATE PROCEDURE dbo.TransferContacts

@TransferTargetCode VARCHAR(50),
@TransferTargetEntityID INT,
@ContactIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cComment VARCHAR(MAX) = 'Bulk transfer.'
	DECLARE @cContactIDList VARCHAR(MAX)
	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY)

	UPDATE C
	SET 
		C.AssetUnitID = 
			CASE
				WHEN @TransferTargetCode = 'AssetUnit'
				THEN @TransferTargetEntityID
				ELSE 0
			END,

		C.CommunityID = 
			CASE
				WHEN @TransferTargetCode = 'Community'
				THEN @TransferTargetEntityID
				ELSE (SELECT asset.GetCommunityIDByAssetUnitID(@TransferTargetEntityID))
			END

	OUTPUT INSERTED.ContactID INTO @tOutput
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@ContactIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ContactID

	SELECT @cContactIDList = STUFF((SELECT ',' + CAST(O.ContactID AS VARCHAR(10)) FROM @tOutput O ORDER BY O.ContactID FOR XML PATH('')), 1, 1, '')

	IF EXISTS (SELECT 1 FROM @tOutput)
		EXEC eventlog.LogContactAction 0, 'update', @PersonID, @cComment, @cContactIDList 
	--ENDIF

END
GO
--End procedure dbo.TransferContacts

--Begin procedure dropdown.GetIndicatorStatusData
EXEC Utility.DropObject 'dropdown.GetIndicatorStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:		Jonathan Burnham
-- Create date:	2017.12.12
-- Description:	A stored procedure to return data from the dropdown.IndicatorStatus table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetIndicatorStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IndicatorStatusID, 
		T.IndicatorStatusName
	FROM dropdown.IndicatorStatus T
	WHERE (T.IndicatorStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IndicatorStatusName, T.IndicatorStatusID

END
GO
--End procedure dropdown.GetIndicatorStatusData

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
--
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	Add 'short' formats
--
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	Removed the ActualValue column
--
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	Refactored to support new requirments
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.MeansOfVerification,
		I.PlannedDate,
		I.PositiveExample,
		dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.RiskAssumption,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName,
		I.DataCollectionPlanSource,
		I.DataCollectionPlanFrequency,
		I.StatusDescription,
		I.IndicatorStatusID,
		DIS.IndicatorStatusName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN dropdown.IndicatorStatus DIS ON DIS.IndicatorStatusID = I.IndicatorStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

	--IndicatorMilestone
	SELECT
		IM.AchievedValue,
		IM.InProgressValue,
		IM.PlannedValue,
		IM.TargetValue,
		M.MilestoneID,
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
		M.MilestoneName
	FROM logicalframework.IndicatorMilestone IM
		JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
			AND IM.IndicatorID = @IndicatorID
	ORDER BY M.MilestoneName, M.MilestoneID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetLogicalFrameworkOverviewData
EXEC Utility.DropObject 'logicalframework.GetLogicalFrameworkOverviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.12.12
-- Description:	A stored procedure to return data from the logicalframework.GetLogicalFrameworkOverviewData table
-- ==============================================================================================================
CREATE PROCEDURE logicalframework.GetLogicalFrameworkOverviewData

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OrderedObjectives TABLE (
		OrderedObjectiveId INT IDENTITY(100,1),
		ObjectiveID INT,
		ParentObjectiveID INT,
		ObjectiveName varchar(100),
		ObjectiveTypeID INT
	);

	DECLARE @GridData TABLE (
		ObjectiveID INT,
		ParentObjectiveID INT,
		ObjectiveName VARCHAR(100),
		TreeLevel INT,
		TreePath VARCHAR(50),
		ObjectiveTypeName VARCHAR(50),
		IndicatorID INT,
		IndicatorName NVARCHAR(500),
		IndicatorStatusName VARCHAR(250),
		IndicatorStatusColor VARCHAR(250),
		Q1Milestones INT,
		Q2Milestones INT,
		Q3Milestones INT,
		Q4Milestones INT,
		MilestoneTotal INT
	);

	INSERT INTO @OrderedObjectives (ObjectiveID,ParentObjectiveID,ObjectiveName,ObjectiveTypeID)
	SELECT O.ObjectiveID,O.ParentObjectiveID,O.ObjectiveName, O.ObjectiveTypeID
	FROM logicalframework.Objective O
	WHERE O.IsActive = 1
	ORDER BY O.ObjectiveName;

	WITH HD AS (
		SELECT OrderedObjectiveId, ObjectiveID, ParentObjectiveID, ObjectiveName, ObjectiveTypeID, 0 AS TreeLevel, CAST(OrderedObjectiveId AS VARCHAR(255)) AS TreePath
		FROM @OrderedObjectives T1
		WHERE ParentObjectiveID = 0

		UNION ALL

		SELECT T2.OrderedObjectiveId, T2.ObjectiveID, T2.ParentObjectiveID, T2.ObjectiveName, T2.ObjectiveTypeID, TreeLevel + 1, CAST(TreePath + '.' + CAST(T2.OrderedObjectiveId AS VARCHAR(255)) AS VARCHAR(255)) AS TreePath
		FROM @OrderedObjectives T2
		INNER JOIN HD itms ON itms.ObjectiveID = T2.ParentObjectiveID
	)

	INSERT INTO @GridData
	SELECT O.ObjectiveID, O.ParentObjectiveID, O.ObjectiveName, O.TreeLevel, O.TreePath, OT.ObjectiveTypeName
		,NULL AS IndicatorID, NULL AS IndicatorName, NULL AS IndicatorStatusName, NULL AS IndicatorStatusColor,
		NULL AS Q1Milestones, NULL AS Q2Milestones, NULL AS Q3Milestones, NULL AS Q4Milestones, NULL AS MilestoneTotal
	FROM  HD O
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
	WHERE NOT EXISTS (SELECT 'found' FROM logicalframework.Indicator I WHERE I.ObjectiveID = O.ObjectiveID AND I.IsActive = 1)

	UNION

	SELECT O.ObjectiveID, O.ParentObjectiveID, O.ObjectiveName, O.TreeLevel, O.TreePath, OT.ObjectiveTypeName
		,I.IndicatorID, I.IndicatorName, DIS.IndicatorStatusName, IIF(DIS.IndicatorStatusName='Off Track','E41A1C',IIF(DIS.IndicatorStatusName='On Track','4DAF4A',IIF(DIS.IndicatorStatusName='Progressing','FFFF33','A65628'))) AS IndicatorStatusColor,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ1Month')
		) AS Q1Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ2Month')
		) AS Q2Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ3Month')
		) AS Q3Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ4Month')
		) AS Q4Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND (
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ1Month') OR 
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ2Month') OR 
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ3Month') OR 
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ4Month')
			)
		) AS MilestonesTotal
	FROM  HD O
		JOIN logicalframework.Indicator I ON I.ObjectiveID = O.ObjectiveID
		JOIN dropdown.IndicatorStatus DIS ON DIS.IndicatorStatusID = I.IndicatorStatusID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
	AND I.IsActive = 1
	ORDER BY O.TreePath;

	--Grid Data
	SELECT * FROM @GridData;

	--ChartData
	SELECT ObjectiveID, ObjectiveName, IndicatorStatusName, COUNT(IndicatorStatusName) AS IndicatorStatusCount,
		IIF(IndicatorStatusName='Off Track','E41A1C',IIF(IndicatorStatusName='On Track','4DAF4A',IIF(IndicatorStatusName='Progressing','FFFF33','A65628'))) AS IndicatorStatusColor
	FROM @GridData
	WHERE ObjectiveTypeName = 'Intermediate Outcome'
	AND IndicatorStatusName IS NOT NULL
	GROUP BY ObjectiveID, ObjectiveName, IndicatorStatusName;

END
GO
--End procedure logicalframework.GetLogicalFrameworkOverviewData

--Begin procedure reporting.GetClasses
EXEC utility.DropObject 'reporting.GetClasses'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.01.06
-- Description:	A stored procedure to data for the Class List Export
-- =================================================================
CREATE PROCEDURE reporting.GetClasses

@ClassIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CR.CourseID,
		CR.CourseName AS Course,
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS Name,
		NULL AS Community,
		A.AssetName AS Asset
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.ListToTable(@ClassIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CL.ClassID
			AND C.AssetUnitID > 0

	UNION

	SELECT
		CR.CourseID,
		CR.CourseName AS Course,
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS Name,
		CO.CommunityName AS Community,
		NULL AS Asset
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN dbo.Community CO ON CO.CommunityID = C.CommunityID 
		JOIN dbo.ListToTable(@ClassIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CL.ClassID
			AND (C.AssetUnitID = 0 OR C.AssetUnitID IS NULL)

	ORDER BY 2, 4
END
GO
--End procedure reporting.GetClasses

--Begin procedure weeklyreport.PopulateWeeklyReportCommunities
EXEC utility.DropObject 'weeklyreport.PopulateWeeklyReportCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE weeklyreport.PopulateWeeklyReportCommunities

@CommunityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Community
		(CommunityEngagementStatusID, CommunityID, CommunityName, ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		C.CommunityEngagementStatusID,
		C.CommunityID,
		C.CommunityName,
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyHighlight,
		C.KeyPoints, 
		C.ProvinceID,
		C.RiskMitigation,
		C.StatusChangeID, 
		C.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Community C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Community WRC
				WHERE WRC.CommunityID = C.CommunityID
				AND WRC.WeeklyReportId = (SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
				)
END
GO
--End procedure weeklyreport.PopulateWeeklyReportCommunities

--Begin procedure weeklyreport.PopulateWeeklyReportProvinces
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportProvinces'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add Province data to the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportProvinces

@ProvinceIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Province
		(ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, ProvinceName, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		P.ImpactDecisionID, 
		P.Implications, 
		P.KeyHighlight,
		P.KeyPoints, 
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID, 
		P.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Province P
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Province WRP
				WHERE WRP.ProvinceID = P.ProvinceID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportProvinces
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dropdown.IndicatorStatus
TRUNCATE TABLE dropdown.IndicatorStatus
GO

SET IDENTITY_INSERT dropdown.IndicatorStatus ON
GO
INSERT INTO dropdown.IndicatorStatus (IndicatorStatusID) VALUES (CAST(0 AS VARCHAR(10)))
GO
SET IDENTITY_INSERT dropdown.IndicatorStatus OFF;
GO

INSERT INTO dropdown.IndicatorStatus 
	(IndicatorStatusName)
VALUES
	('On Track'),
	('Progressing'),
	('Off Track')
GO
--End table dropdown.IndicatorStatus

UPDATE logicalframework.Indicator SET IndicatorStatusID = 1 WHERE IndicatorStatusID IS NULL
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='ObjectiveOverview', 
	@NewMenuItemLink='/logicalframework/overview', 
	@NewMenuItemText='Overview', 
	@BeforeMenuItemCode='LogicalFrameworkUpdate', 
	@PermissionableLineageList='LogicalFramework.Overview'
GO

EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ1Month', @ServerSetupValue = '12'
GO
EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ2Month', @ServerSetupValue = '3'
GO
EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ3Month', @ServerSetupValue = '7'
GO
EXEC utility.ServerSetupKeyAddUpdate @ServerSetupKey = 'MilestoneQ4Month', @ServerSetupValue = '9'
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Export Class Lists', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LogicalFramework', @DESCRIPTION='View M&E Overview', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='LogicalFramework.Overview', @PERMISSIONCODE=NULL;
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Community', 'Community', 0;
EXEC permissionable.SavePermissionableGroup 'Contact', 'Contact', 0;
EXEC permissionable.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0;
EXEC permissionable.SavePermissionableGroup 'Documents', 'Documents', 0;
EXEC permissionable.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0;
EXEC permissionable.SavePermissionableGroup 'Equipment', 'Equipment', 0;
EXEC permissionable.SavePermissionableGroup 'Implementation', 'Implementation', 0;
EXEC permissionable.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0;
EXEC permissionable.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0;
EXEC permissionable.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0;
EXEC permissionable.SavePermissionableGroup 'Province', 'Province', 0;
EXEC permissionable.SavePermissionableGroup 'Research', 'Research', 0;
EXEC permissionable.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0;
EXEC permissionable.SavePermissionableGroup 'Training', 'Training', 0;
EXEC permissionable.SavePermissionableGroup 'Workflows', 'Workflows', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='List EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='View EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='List EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Exports', @DESCRIPTION='Business License Report Exports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='BusinessLicenseReport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Exports.BusinessLicenseReport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User Can Receive Email', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CanRecieveEmail', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.CanRecieveEmail.CanRecieveEmail', @PERMISSIONCODE='CanRecieveEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Download dashboard charts as images', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Default.DownloadChart', @PERMISSIONCODE='DownloadChart';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View ColdFusion Errors SiteConfiguration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='List PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='List Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View list of permissionables on view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Add / edit a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Recieve email for Update to Impact Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate.ImpactUpdateEmail', @PERMISSIONCODE='ImpactUpdateEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='List Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the analysis tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Analysis', @PERMISSIONCODE='Analysis';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Equipment Distributions Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Implementation Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Implementation', @PERMISSIONCODE='Implementation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the information tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Information', @PERMISSIONCODE='Information';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add contacts to a stipend payment list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddContactStipendPaymentContacts', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddContactStipendPaymentContacts', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Project Staff - ASI', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Project Staff - Creative', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Sub-Contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows view of justice stipends payments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the approve stipend payment functionality justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.ApproveContactStipendPayment', @PERMISSIONCODE='ApproveContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the cash hand over report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the op funds report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Access the reconcile stipend payment functionality on a justice stipend payments list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.ReconcileContactStipendPayment', @PERMISSIONCODE='ReconcileContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend activity report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Close out the stipend justice & police payment process', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows import of payees in payment system', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.AddContactStipendPaymentContacts', @PERMISSIONCODE='AddContactStipendPaymentContacts';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows access to the bulk transfer functionality', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.CanHaveBulkTransfer', @PERMISSIONCODE='CanHaveBulkTransfer';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type CE Team in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export payees from the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ExportPayees', @PERMISSIONCODE='ExportPayees';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Field Staff in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type IO4 in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='A bypass to allow users not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Project Staff - ASI in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Project Staff - Creative in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Sub-Contractor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the approve stipend payment functionality police stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.ApproveContactStipendPayment', @PERMISSIONCODE='ApproveContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the cash handover report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the op funds report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Access the reconcile stipend payment functionality on a police stipend payments list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.ReconcileContactStipendPayment', @PERMISSIONCODE='ReconcileContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend activity report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend payment report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Project Staff - ASI', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Project Staff - Creative', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Sub-Contractors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the more info button on the vetting history data table', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.VettingMoreInfo', @PERMISSIONCODE='VettingMoreInfo';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Add contacts to an activity from the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.AddToConceptNote', @PERMISSIONCODE='AddToConceptNote';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type CE Team in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type JO.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportJO', @PERMISSIONCODE='ExportJO';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type UK.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportUK', @PERMISSIONCODE='ExportUK';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type US.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportUS', @PERMISSIONCODE='ExportUS';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Field Staff in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type IO4 in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Project Staff - ASI in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Project Staff - Creative in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Sub-Contractor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive an email when a UK vetting outcome has changed for one or more contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.UKVettingOutcomeChangeNotification', @PERMISSIONCODE='UKVettingOutcomeChangeNotification';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive an email when a US vetting outcome has changed for one or more contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.USVettingOutcomeChangeNotification', @PERMISSIONCODE='USVettingOutcomeChangeNotification';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeConsider', @PERMISSIONCODE='VettingOutcomeConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Do Not Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeDoNotConsider', @PERMISSIONCODE='VettingOutcomeDoNotConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Insufficient Data"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeInsufficientData', @PERMISSIONCODE='VettingOutcomeInsufficientData';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Not Vetted"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeNotVetted', @PERMISSIONCODE='VettingOutcomeNotVetted';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Pending Internal Review"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomePendingInternalReview', @PERMISSIONCODE='VettingOutcomePendingInternalReview';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Allows a user to change a change a vetting outcome after a contact has been flagged as "Do Not Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeReConsider', @PERMISSIONCODE='VettingOutcomeReConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Submitted for Vetting"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeSubmittedforVetting', @PERMISSIONCODE='VettingOutcomeSubmittedforVetting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update UK vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUK', @PERMISSIONCODE='VettingTypeUK';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update US vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUS', @PERMISSIONCODE='VettingTypeUS';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive the monthly vetting expiration counts e-mail', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Notification', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.Notification.ExpirationCountEmail', @PERMISSIONCODE='ExpirationCountEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.004', @PERMISSIONCODE='004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.100', @PERMISSIONCODE='100';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.101', @PERMISSIONCODE='101';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.102', @PERMISSIONCODE='102';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.103', @PERMISSIONCODE='103';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.104', @PERMISSIONCODE='104';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.105', @PERMISSIONCODE='105';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.106', @PERMISSIONCODE='106';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.107', @PERMISSIONCODE='107';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.108', @PERMISSIONCODE='108';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.109', @PERMISSIONCODE='109';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.301', @PERMISSIONCODE='301';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.302', @PERMISSIONCODE='302';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.303', @PERMISSIONCODE='303';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.304', @PERMISSIONCODE='304';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.306', @PERMISSIONCODE='306';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.500', @PERMISSIONCODE='500';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.501', @PERMISSIONCODE='501';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.502', @PERMISSIONCODE='502';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.503', @PERMISSIONCODE='503';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.504', @PERMISSIONCODE='504';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.505', @PERMISSIONCODE='505';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.506', @PERMISSIONCODE='506';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.507', @PERMISSIONCODE='507';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.508', @PERMISSIONCODE='508';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.509', @PERMISSIONCODE='509';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.510', @PERMISSIONCODE='510';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.511', @PERMISSIONCODE='511';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.512', @PERMISSIONCODE='512';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.513', @PERMISSIONCODE='513';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.514', @PERMISSIONCODE='514';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.515', @PERMISSIONCODE='515';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.516', @PERMISSIONCODE='516';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.517', @PERMISSIONCODE='517';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.518', @PERMISSIONCODE='518';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.519', @PERMISSIONCODE='519';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.520', @PERMISSIONCODE='520';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.521', @PERMISSIONCODE='521';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 522 Archived Full Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.522', @PERMISSIONCODE='522';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.600', @PERMISSIONCODE='600';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.601', @PERMISSIONCODE='601';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.602', @PERMISSIONCODE='602';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.603', @PERMISSIONCODE='603';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.604', @PERMISSIONCODE='604';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.605', @PERMISSIONCODE='605';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.700', @PERMISSIONCODE='700';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.701', @PERMISSIONCODE='701';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.702', @PERMISSIONCODE='702';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.801', @PERMISSIONCODE='801';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.802', @PERMISSIONCODE='802';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.803', @PERMISSIONCODE='803';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.804', @PERMISSIONCODE='804';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.805', @PERMISSIONCODE='805';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.806', @PERMISSIONCODE='806';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.807', @PERMISSIONCODE='807';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.808', @PERMISSIONCODE='808';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.900', @PERMISSIONCODE='900';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.901', @PERMISSIONCODE='901';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.902', @PERMISSIONCODE='902';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.903', @PERMISSIONCODE='903';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.904', @PERMISSIONCODE='904';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.905', @PERMISSIONCODE='905';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.906', @PERMISSIONCODE='906';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.907', @PERMISSIONCODE='907';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.908', @PERMISSIONCODE='908';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.909', @PERMISSIONCODE='909';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.910', @PERMISSIONCODE='910';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc006', @PERMISSIONCODE='Doc006';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='document', @DESCRIPTION='getdocumentbydocumentname', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='getdocumentbydocumentname', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='document.getdocumentbydocumentname', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='get doc file by ID', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='getDocumentFileByDocumentID', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.getDocumentFileByDocumentID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.004', @PERMISSIONCODE='004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.100', @PERMISSIONCODE='100';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.101', @PERMISSIONCODE='101';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.102', @PERMISSIONCODE='102';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.103', @PERMISSIONCODE='103';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.104', @PERMISSIONCODE='104';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.105', @PERMISSIONCODE='105';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.106', @PERMISSIONCODE='106';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.107', @PERMISSIONCODE='107';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.108', @PERMISSIONCODE='108';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.109', @PERMISSIONCODE='109';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.301', @PERMISSIONCODE='301';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.302', @PERMISSIONCODE='302';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.303', @PERMISSIONCODE='303';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.304', @PERMISSIONCODE='304';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.306', @PERMISSIONCODE='306';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.500', @PERMISSIONCODE='500';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.501', @PERMISSIONCODE='501';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.502', @PERMISSIONCODE='502';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.503', @PERMISSIONCODE='503';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.504', @PERMISSIONCODE='504';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.505', @PERMISSIONCODE='505';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.506', @PERMISSIONCODE='506';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.507', @PERMISSIONCODE='507';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.508', @PERMISSIONCODE='508';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.509', @PERMISSIONCODE='509';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.510', @PERMISSIONCODE='510';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.511', @PERMISSIONCODE='511';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.512', @PERMISSIONCODE='512';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.513', @PERMISSIONCODE='513';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.514', @PERMISSIONCODE='514';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.515', @PERMISSIONCODE='515';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.516', @PERMISSIONCODE='516';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.517', @PERMISSIONCODE='517';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.518', @PERMISSIONCODE='518';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.519', @PERMISSIONCODE='519';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.520', @PERMISSIONCODE='520';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.521', @PERMISSIONCODE='521';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 522 Archived Full Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.522', @PERMISSIONCODE='522';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.600', @PERMISSIONCODE='600';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.601', @PERMISSIONCODE='601';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.602', @PERMISSIONCODE='602';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.603', @PERMISSIONCODE='603';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.604', @PERMISSIONCODE='604';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.605', @PERMISSIONCODE='605';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.700', @PERMISSIONCODE='700';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.701', @PERMISSIONCODE='701';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.702', @PERMISSIONCODE='702';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.801', @PERMISSIONCODE='801';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.802', @PERMISSIONCODE='802';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.803', @PERMISSIONCODE='803';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.804', @PERMISSIONCODE='804';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.805', @PERMISSIONCODE='805';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.806', @PERMISSIONCODE='806';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.807', @PERMISSIONCODE='807';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.808', @PERMISSIONCODE='808';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.900', @PERMISSIONCODE='900';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.901', @PERMISSIONCODE='901';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.902', @PERMISSIONCODE='902';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.903', @PERMISSIONCODE='903';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.904', @PERMISSIONCODE='904';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.905', @PERMISSIONCODE='905';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.906', @PERMISSIONCODE='906';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.907', @PERMISSIONCODE='907';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.908', @PERMISSIONCODE='908';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.909', @PERMISSIONCODE='909';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.910', @PERMISSIONCODE='910';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc006', @PERMISSIONCODE='Doc006';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit a donor decision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateDecision', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateDecision', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit donor meetings & actions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateMeeting', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateMeeting', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='List Donor Decisions, Meetings & Actions DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='View DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit the equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='List EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Add or update an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Audit', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Audit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Create an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Create', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Create', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Delete an active equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.FinalizeEquipmentDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='View the list of distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Enter a bulk audit result for distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory.BulkAudit', @PERMISSIONCODE='BulkAudit';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Bulk transfer distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory.BulkTransfer', @PERMISSIONCODE='BulkTransfer';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Set the delivery date of distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory.SetDeliveredToEndUserDate', @PERMISSIONCODE='SetDeliveredToEndUserDate';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListInventory', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.SetDeliveryDate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SetDeliveryDate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.SetDeliveryDate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Transfer', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Transfer', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Transfer', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit the equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='List EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Export EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='Audit Equipment EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.Audit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='List Equipment Locations EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='Add / edit a community asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='List CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='View CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='Add / edit a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View the list of community rounds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.AddUpdate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.View', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='List Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='List activites grouped by indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Analysis', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='ConceptNote.Analysis', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='Add / edit an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='List indicators grouped by activity', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Analysis', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.Analysis', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='List Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='Add / edit an indicatortype', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='List IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='View IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LogicalFramework', @DESCRIPTION='Logical Framework Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='LogicalFrameworkUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='LogicalFramework.LogicalFrameworkUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LogicalFramework', @DESCRIPTION='View M&E Overview', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='LogicalFramework.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='Add / edit a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='List Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Add / edit an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='M & E Overview Charts Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='ChartList', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.ChartList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='List Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Manage Objectives & Indicators Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Manage', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Manage', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Overview Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit a concep nNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Budget ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.ExportConceptNoteBudget', @PERMISSIONCODE='ExportConceptNoteBudget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit activity finances', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Finances', @PERMISSIONCODE='Finances';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk pane a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Risk', @PERMISSIONCODE='Risk';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Vetting List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Vetting ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList.ExportVetting', @PERMISSIONCODE='ExportVetting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Access to Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetAccesstoJustice', @PERMISSIONCODE='BudgetAccesstoJustice';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Communication', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunication', @PERMISSIONCODE='BudgetCommunication';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunityEngagement', @PERMISSIONCODE='BudgetCommunityEngagement';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Integrated Legitimate Structures', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetIntegratedLegitimateStructures', @PERMISSIONCODE='BudgetIntegratedLegitimateStructures';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetM&E', @PERMISSIONCODE='BudgetM&E';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type MER', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetMER', @PERMISSIONCODE='BudgetMER';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Police Development', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetPoliceDevelopment', @PERMISSIONCODE='BudgetPoliceDevelopment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type RAP', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetRAP', @PERMISSIONCODE='BudgetRAP';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetResearch', @PERMISSIONCODE='BudgetResearch';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk panel in a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Risk', @PERMISSIONCODE='Risk';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Add / edit equipment associated with a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Finalize Equipment Distribution ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='List ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='View ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Export ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='Add / edit a license', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='List License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='View License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Add / edit the license equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='List LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Export LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='View LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Add / edit a purchase request', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='List PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='View PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Export PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Add or update a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Delete a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='List workplans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='View a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Add / edit a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='List ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Export ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Add / edit a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='List Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the analysis tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Analysis', @PERMISSIONCODE='Analysis';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Export Equipment Distributions Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Implementation Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Implementation', @PERMISSIONCODE='Implementation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the information tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Information', @PERMISSIONCODE='Information';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='AssetUnit', @DESCRIPTION='A bypass to allow asset units not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='AssetUnit.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='Add / edit an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='List Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='Add / edit a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='List Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='List Forces Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a force Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='List Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Add / edit an media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View the list of media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Export selected media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View a media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Newsletter', @DESCRIPTION='Newsletter Add Update page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Newsletter.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Newsletter', @DESCRIPTION='This sends the newsletter', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Send', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Newsletter.Send', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Add / edit a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='List Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Export Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Add', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Add', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='List RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Export RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Amend a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Add / edit a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Export Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='List Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Amend a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='List SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Approved SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Approved', @PERMISSIONCODE='Approved';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Export SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='In Work SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.InWork', @PERMISSIONCODE='InWork';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Administer a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AdministerSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Export Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ExportSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ExportSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Questions List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListQuestions', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListQuestions', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='List Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Surveys List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveys', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Questions SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Surveys SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Weekly Report Add / Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Export the weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='weeklyreport', @DESCRIPTION='purges screwed up reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='delete', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='weeklyreport.delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='View the completed weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='Add / edit a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View the list of zones', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Newsletter', @DESCRIPTION='Newsletter list page', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Newsletter.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='Add / edit a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='List SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='View SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Add / edit a class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='List Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Export Class Lists', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List.ClassExport', @PERMISSIONCODE='ClassExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Export Class Lists', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='List Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table dbo.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO utility.BuildLog (BuildKey) VALUES ('Build - 3.06 - 2018.01.18 18.01.51')
GO
--End build tracking

