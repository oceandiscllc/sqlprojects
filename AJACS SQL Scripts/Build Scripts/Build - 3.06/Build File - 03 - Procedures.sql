USE AJACS
GO

--Begin procedure asset.GetAssetPaymentHistoryByAssetID
EXEC Utility.DropObject 'asset.GetAssetPaymentHistoryByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.05
-- Description:	A stored procedure to get payment history data for an asset
-- ========================================================================
CREATE PROCEDURE asset.GetAssetPaymentHistoryByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE
		(
		ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		PaymentYear INT DEFAULT 0,
		PaymentMonth INT DEFAULT 0,
		AssetID INT DEFAULT 0,
		StipendAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		StipendAmountPaid NUMERIC(18,2) DEFAULT 0,
		PayeeCount INT DEFAULT 0,
		ExpenseAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		ExpenseAmountPaid NUMERIC(18,2) DEFAULT 0,
		AssetDepartmentCount INT DEFAULT 0
		)

	INSERT INTO @tTable1
		(PaymentYear, PaymentMonth, StipendAmountAuthorized, StipendAmountPaid, AssetID, PayeeCount)
	SELECT
		CSP.PaymentYear,
		CSP.PaymentMonth,
		IIF (CSP.StipendAuthorizedDate IS NOT NULL, SUM(CSP.StipendAmountAuthorized), 0) AS StipendAmountAuthorized,
		IIF (CSP.StipendPaidDate IS NOT NULL, SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaid,
		A.AssetID,
		COUNT(CSP.ContactID) AS PayeeCount
	FROM dbo.ContactStipendPayment CSP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = CSP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
	GROUP BY A.AssetID, A.AssetName, CSP.PaymentYear, CSP.PaymentMonth, CSP.StipendPaidDate, CSP.StipendAuthorizedDate

	MERGE @tTable1 T1
	USING
		(
		SELECT
			AUE.PaymentYear,
			AUE.PaymentMonth,
			IIF (AUE.ExpenseAuthorizedDate IS NOT NULL, SUM(AUE.ExpenseAmountAuthorized), 0) AS ExpenseAmountAuthorized,
			IIF (AUE.ExpensePaidDate IS NOT NULL, SUM(AUE.ExpenseAmountPaid), 0) AS ExpenseAmountPaid,
			A.AssetID,
			COUNT(AUE.AssetUnitID) AS AssetDepartmentCount
		FROM asset.AssetUnitExpense AUE
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
				AND A.AssetID = @AssetID
		GROUP BY A.AssetID, A.AssetName, AUE.PaymentYear, AUE.PaymentMonth, AUE.ExpenseAuthorizedDate, AUE.ExpensePaidDate
		) T3 ON T3.PaymentYear = T1.PaymentYear 
			AND T3.PaymentMonth = T1.PaymentMonth
			AND T3.AssetID = T1.AssetID 
	WHEN NOT MATCHED THEN
	INSERT
		(PaymentYear, PaymentMonth, ExpenseAmountAuthorized, ExpenseAmountPaid, AssetID, AssetDepartmentCount)
	VALUES
		(
		T3.PaymentYear, 
		T3.PaymentMonth, 
		T3.ExpenseAmountAuthorized, 
		T3.ExpenseAmountPaid, 
		T3.AssetID, 
		T3.AssetDepartmentCount
		)
	WHEN MATCHED THEN
	UPDATE 
	SET 
		T1.ExpenseAmountAuthorized = T3.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid = T3.ExpenseAmountPaid, 
		T1.AssetDepartmentCount = T3.AssetDepartmentCount
	;

	SELECT 
		T1.PaymentYear,
		RIGHT('00' + CAST(T1.PaymentMonth AS VARCHAR(2)), 2) AS PaymentMonthFormatted,
		T1.StipendAmountAuthorized,
		T1.StipendAmountPaid,
		T1.PayeeCount,
		T1.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid, 
		T1.AssetDepartmentCount
	FROM @tTable1 T1
		JOIN asset.Asset A ON A.AssetID = T1.AssetID
	ORDER BY T1.PaymentYear DESC, T1.PaymentMonth DESC, A.AssetName

END
GO
--End procedure asset.GetAssetPaymentHistoryByAssetID

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
--
-- Author:			Todd Pires
-- Update date:	2015.05.28
-- Description:	Added vetting data
--
-- Author:			Todd Pires
-- Update date:	2016.09.09
-- Description:	Pointed to the dbo.ContactVetting table
--
-- Author:			Jonathan Burnham
-- Update date:	2017.12.31
-- Description:	Adding Trainers
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Class
	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
	
	--ClassContact
	SELECT
		dbo.GetContactCommunityByContactID(CO.ContactID) AS ContactLocation,
		CO.ContactID,
		CNC2.VettingIcon,
		CNC2.VettingOutcomeName,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT TOP 1

				CASE
					WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
					THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
					ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
				END AS VettingIcon,

				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND CV.ContactID = CC.ContactID
			ORDER BY CV.ContactVettingID DESC
			) CNC2

	--ConceptNoteClass
	SELECT 
		CNC1.ConceptNoteID,
		CN.Title
	FROM dbo.ConceptNoteClass CNC1
	JOIN dbo.ConceptNote CN ON CNC1.ConceptNoteID = CN.ConceptNoteID
	WHERE CNC1.ClassID = @ClassID

	--TrainerContact
	SELECT 
		CT.ContactID,
		dbo.FormatContactNameByContactID(CT.ContactID, 'LastFirstMiddle') AS TrainerFullNameFormatted
	FROM dbo.ClassTrainer CT
	WHERE CT.ClassID = @ClassID

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetConceptNotePaymentHistoryByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNotePaymentHistoryByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.05
-- Description:	A stored procedure to get payment history data for a ConceptNote
-- =============================================================================
CREATE PROCEDURE dbo.GetConceptNotePaymentHistoryByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tParentCNs TABLE (ConceptNoteID INT, Title VARCHAR(250), AmendedConceptNoteID INT)

	;
	WITH CNCTE AS 
		( 
		SELECT 
			CN.ConceptNoteID, 
			CN.Title, 
			CN.AmendedConceptNoteID
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.Title, 
			CN.AmendedConceptNoteID
		FROM dbo.ConceptNote CN
			JOIN CNCTE ON CNCTE.AmendedConceptNoteID = CN.ConceptNoteID
		)

	INSERT INTO @tParentCNs 
		(ConceptNoteID, Title, AmendedConceptNoteID)
	SELECT 
		CNCTE.ConceptNoteID, 
		CNCTE.Title, 
		CNCTE.AmendedConceptNoteID
	FROM CNCTE

	DECLARE @tTable1 TABLE
		(
		ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		PaymentYear INT DEFAULT 0,
		PaymentMonth INT DEFAULT 0,
		ConceptNoteID INT DEFAULT 0,
		StipendAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		StipendAmountPaid NUMERIC(18,2) DEFAULT 0,
		ProvinceID INT DEFAULT 0,
		PayeeCount INT DEFAULT 0,
		ExpenseAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		ExpenseAmountPaid NUMERIC(18,2) DEFAULT 0,
		AssetDepartmentCount INT DEFAULT 0
		)

	INSERT INTO @tTable1
		(PaymentYear, PaymentMonth, StipendAmountAuthorized, StipendAmountPaid, ProvinceID, PayeeCount)
	SELECT
		CSP.PaymentYear,
		CSP.PaymentMonth,
		IIF (CSP.StipendAuthorizedDate IS NOT NULL, SUM(CSP.StipendAmountAuthorized), 0) AS StipendAmountAuthorized,
		IIF (CSP.StipendPaidDate IS NOT NULL, SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaid,
		CSP.ProvinceID,
		COUNT(CSP.ContactID) AS PayeeCount
	FROM dbo.ContactStipendPayment CSP
	WHERE EXISTS
		(
		SELECT 1
		FROM @tParentCNs T
		WHERE T.ConceptNoteID = CSP.ConceptNoteID
		)
	GROUP BY CSP.ProvinceID, CSP.PaymentYear, CSP.PaymentMonth, CSP.StipendPaidDate, CSP.StipendAuthorizedDate

	MERGE @tTable1 T1
	USING
		(
		SELECT
			AUE.PaymentYear,
			AUE.PaymentMonth,
			IIF (AUE.ExpenseAuthorizedDate IS NOT NULL, SUM(AUE.ExpenseAmountAuthorized), 0) AS ExpenseAmountAuthorized,
			IIF (AUE.ExpensePaidDate IS NOT NULL, SUM(AUE.ExpenseAmountPaid), 0) AS ExpenseAmountPaid,
			C.ProvinceID,
			COUNT(AUE.AssetUnitID) AS AssetDepartmentCount
		FROM asset.AssetUnitExpense AUE
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dbo.Community C ON C.CommunityID = A.CommunityID
				AND EXISTS
					(
					SELECT 1
					FROM @tParentCNs T
					WHERE T.ConceptNoteID = AUE.ConceptNoteID
					)
		GROUP BY C.ProvinceID, AUE.PaymentYear, AUE.PaymentMonth, AUE.ExpenseAuthorizedDate, AUE.ExpensePaidDate
		) T3 ON T3.ProvinceID = T1.ProvinceID 
			AND T3.PaymentYear = T1.PaymentYear 
			AND T3.PaymentMonth = T1.PaymentMonth
	WHEN NOT MATCHED THEN
	INSERT
		(PaymentYear, PaymentMonth, ExpenseAmountAuthorized, ExpenseAmountPaid, ProvinceID, AssetDepartmentCount)
	VALUES
		(
		T3.PaymentYear, 
		T3.PaymentMonth, 
		T3.ExpenseAmountAuthorized, 
		T3.ExpenseAmountPaid, 
		T3.ProvinceID, 
		T3.AssetDepartmentCount
		)
	WHEN MATCHED THEN
	UPDATE 
	SET 
		T1.ExpenseAmountAuthorized = T3.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid = T3.ExpenseAmountPaid, 
		T1.AssetDepartmentCount = T3.AssetDepartmentCount
	;

	SELECT 
		P.ProvinceName,
		T1.PaymentYear,
		RIGHT('00' + CAST(T1.PaymentMonth AS VARCHAR(2)), 2) AS PaymentMonthFormatted,
		T1.StipendAmountAuthorized,
		T1.StipendAmountPaid,
		T1.PayeeCount,
		T1.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid, 
		T1.AssetDepartmentCount
	FROM @tTable1 T1
		JOIN dbo.Province P ON P.ProvinceID = T1.ProvinceID
	ORDER BY P.ProvinceName, T1.PaymentYear DESC, T1.PaymentMonth DESC

END
GO
--End procedure dbo.GetConceptNotePaymentHistoryByConceptNoteID

--Begin procedure dbo.TransferContacts
EXEC Utility.DropObject 'dbo.TransferContacts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.19
-- Description:	A stored procedure bulk to transfer contacts to a community or an asset unit
-- =========================================================================================
CREATE PROCEDURE dbo.TransferContacts

@TransferTargetCode VARCHAR(50),
@TransferTargetEntityID INT,
@ContactIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cComment VARCHAR(MAX) = 'Bulk transfer.'
	DECLARE @cContactIDList VARCHAR(MAX)
	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY)

	UPDATE C
	SET 
		C.AssetUnitID = 
			CASE
				WHEN @TransferTargetCode = 'AssetUnit'
				THEN @TransferTargetEntityID
				ELSE 0
			END,

		C.CommunityID = 
			CASE
				WHEN @TransferTargetCode = 'Community'
				THEN @TransferTargetEntityID
				ELSE (SELECT asset.GetCommunityIDByAssetUnitID(@TransferTargetEntityID))
			END

	OUTPUT INSERTED.ContactID INTO @tOutput
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@ContactIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ContactID

	SELECT @cContactIDList = STUFF((SELECT ',' + CAST(O.ContactID AS VARCHAR(10)) FROM @tOutput O ORDER BY O.ContactID FOR XML PATH('')), 1, 1, '')

	IF EXISTS (SELECT 1 FROM @tOutput)
		EXEC eventlog.LogContactAction 0, 'update', @PersonID, @cComment, @cContactIDList 
	--ENDIF

END
GO
--End procedure dbo.TransferContacts

--Begin procedure dropdown.GetIndicatorStatusData
EXEC Utility.DropObject 'dropdown.GetIndicatorStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:		Jonathan Burnham
-- Create date:	2017.12.12
-- Description:	A stored procedure to return data from the dropdown.IndicatorStatus table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetIndicatorStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IndicatorStatusID, 
		T.IndicatorStatusName
	FROM dropdown.IndicatorStatus T
	WHERE (T.IndicatorStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IndicatorStatusName, T.IndicatorStatusID

END
GO
--End procedure dropdown.GetIndicatorStatusData

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
--
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	Add 'short' formats
--
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	Removed the ActualValue column
--
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	Refactored to support new requirments
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.MeansOfVerification,
		I.PlannedDate,
		I.PositiveExample,
		dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.RiskAssumption,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName,
		I.DataCollectionPlanSource,
		I.DataCollectionPlanFrequency,
		I.StatusDescription,
		I.IndicatorStatusID,
		DIS.IndicatorStatusName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN dropdown.IndicatorStatus DIS ON DIS.IndicatorStatusID = I.IndicatorStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

	--IndicatorMilestone
	SELECT
		IM.AchievedValue,
		IM.InProgressValue,
		IM.PlannedValue,
		IM.TargetValue,
		M.MilestoneID,
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
		M.MilestoneName
	FROM logicalframework.IndicatorMilestone IM
		JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
			AND IM.IndicatorID = @IndicatorID
	ORDER BY M.MilestoneName, M.MilestoneID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetLogicalFrameworkOverviewData
EXEC Utility.DropObject 'logicalframework.GetLogicalFrameworkOverviewData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.12.12
-- Description:	A stored procedure to return data from the logicalframework.GetLogicalFrameworkOverviewData table
-- ==============================================================================================================
CREATE PROCEDURE logicalframework.GetLogicalFrameworkOverviewData

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OrderedObjectives TABLE (
		OrderedObjectiveId INT IDENTITY(100,1),
		ObjectiveID INT,
		ParentObjectiveID INT,
		ObjectiveName varchar(100),
		ObjectiveTypeID INT
	);

	DECLARE @GridData TABLE (
		ObjectiveID INT,
		ParentObjectiveID INT,
		ObjectiveName VARCHAR(100),
		TreeLevel INT,
		TreePath VARCHAR(50),
		ObjectiveTypeName VARCHAR(50),
		IndicatorID INT,
		IndicatorName NVARCHAR(500),
		IndicatorStatusName VARCHAR(250),
		IndicatorStatusColor VARCHAR(250),
		Q1Milestones INT,
		Q2Milestones INT,
		Q3Milestones INT,
		Q4Milestones INT,
		MilestoneTotal INT
	);

	INSERT INTO @OrderedObjectives (ObjectiveID,ParentObjectiveID,ObjectiveName,ObjectiveTypeID)
	SELECT O.ObjectiveID,O.ParentObjectiveID,O.ObjectiveName, O.ObjectiveTypeID
	FROM logicalframework.Objective O
	WHERE O.IsActive = 1
	ORDER BY O.ObjectiveName;

	WITH HD AS (
		SELECT OrderedObjectiveId, ObjectiveID, ParentObjectiveID, ObjectiveName, ObjectiveTypeID, 0 AS TreeLevel, CAST(OrderedObjectiveId AS VARCHAR(255)) AS TreePath
		FROM @OrderedObjectives T1
		WHERE ParentObjectiveID = 0

		UNION ALL

		SELECT T2.OrderedObjectiveId, T2.ObjectiveID, T2.ParentObjectiveID, T2.ObjectiveName, T2.ObjectiveTypeID, TreeLevel + 1, CAST(TreePath + '.' + CAST(T2.OrderedObjectiveId AS VARCHAR(255)) AS VARCHAR(255)) AS TreePath
		FROM @OrderedObjectives T2
		INNER JOIN HD itms ON itms.ObjectiveID = T2.ParentObjectiveID
	)

	INSERT INTO @GridData
	SELECT O.ObjectiveID, O.ParentObjectiveID, O.ObjectiveName, O.TreeLevel, O.TreePath, OT.ObjectiveTypeName
		,NULL AS IndicatorID, NULL AS IndicatorName, NULL AS IndicatorStatusName, NULL AS IndicatorStatusColor,
		NULL AS Q1Milestones, NULL AS Q2Milestones, NULL AS Q3Milestones, NULL AS Q4Milestones, NULL AS MilestoneTotal
	FROM  HD O
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
	WHERE NOT EXISTS (SELECT 'found' FROM logicalframework.Indicator I WHERE I.ObjectiveID = O.ObjectiveID AND I.IsActive = 1)

	UNION

	SELECT O.ObjectiveID, O.ParentObjectiveID, O.ObjectiveName, O.TreeLevel, O.TreePath, OT.ObjectiveTypeName
		,I.IndicatorID, I.IndicatorName, DIS.IndicatorStatusName, IIF(DIS.IndicatorStatusName='Off Track','E41A1C',IIF(DIS.IndicatorStatusName='On Track','4DAF4A',IIF(DIS.IndicatorStatusName='Progressing','FFFF33','A65628'))) AS IndicatorStatusColor,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ1Month')
		) AS Q1Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ2Month')
		) AS Q2Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ3Month')
		) AS Q3Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ4Month')
		) AS Q4Milestones,
		(
			SELECT IsNULL(SUM(IM.AchievedValue), 0)
			FROM logicalframework.Milestone M
				JOIN logicalframework.IndicatorMilestone IM ON IM.MilestoneID = M.MilestoneID
			AND M.IsActive = 1
			AND IM.IndicatorID = I.IndicatorID
			AND (
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ1Month') OR 
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ2Month') OR 
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ3Month') OR 
				DATEPART(m, M.MilestoneDate) = (SELECT ServerSetupValue FROM dbo.ServerSetup WHERE ServerSetupKey = 'MilestoneQ4Month')
			)
		) AS MilestonesTotal
	FROM  HD O
		JOIN logicalframework.Indicator I ON I.ObjectiveID = O.ObjectiveID
		JOIN dropdown.IndicatorStatus DIS ON DIS.IndicatorStatusID = I.IndicatorStatusID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
	AND I.IsActive = 1
	ORDER BY O.TreePath;

	--Grid Data
	SELECT * FROM @GridData;

	--ChartData
	SELECT ObjectiveID, ObjectiveName, IndicatorStatusName, COUNT(IndicatorStatusName) AS IndicatorStatusCount,
		IIF(IndicatorStatusName='Off Track','E41A1C',IIF(IndicatorStatusName='On Track','4DAF4A',IIF(IndicatorStatusName='Progressing','FFFF33','A65628'))) AS IndicatorStatusColor
	FROM @GridData
	WHERE ObjectiveTypeName = 'Intermediate Outcome'
	AND IndicatorStatusName IS NOT NULL
	GROUP BY ObjectiveID, ObjectiveName, IndicatorStatusName;

END
GO
--End procedure logicalframework.GetLogicalFrameworkOverviewData

--Begin procedure reporting.GetClasses
EXEC utility.DropObject 'reporting.GetClasses'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.01.06
-- Description:	A stored procedure to data for the Class List Export
-- =================================================================
CREATE PROCEDURE reporting.GetClasses

@ClassIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CR.CourseID,
		CR.CourseName AS Course,
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS Name,
		NULL AS Community,
		A.AssetName AS Asset
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.ListToTable(@ClassIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CL.ClassID
			AND C.AssetUnitID > 0

	UNION

	SELECT
		CR.CourseID,
		CR.CourseName AS Course,
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS Name,
		CO.CommunityName AS Community,
		NULL AS Asset
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN dbo.Community CO ON CO.CommunityID = C.CommunityID 
		JOIN dbo.ListToTable(@ClassIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CL.ClassID
			AND (C.AssetUnitID = 0 OR C.AssetUnitID IS NULL)

	ORDER BY 2, 4
END
GO
--End procedure reporting.GetClasses

--Begin procedure weeklyreport.PopulateWeeklyReportCommunities
EXEC utility.DropObject 'weeklyreport.PopulateWeeklyReportCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE weeklyreport.PopulateWeeklyReportCommunities

@CommunityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Community
		(CommunityEngagementStatusID, CommunityID, CommunityName, ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		C.CommunityEngagementStatusID,
		C.CommunityID,
		C.CommunityName,
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyHighlight,
		C.KeyPoints, 
		C.ProvinceID,
		C.RiskMitigation,
		C.StatusChangeID, 
		C.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Community C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Community WRC
				WHERE WRC.CommunityID = C.CommunityID
				AND WRC.WeeklyReportId = (SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
				)
END
GO
--End procedure weeklyreport.PopulateWeeklyReportCommunities

--Begin procedure weeklyreport.PopulateWeeklyReportProvinces
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportProvinces'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add Province data to the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportProvinces

@ProvinceIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Province
		(ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, ProvinceName, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		P.ImpactDecisionID, 
		P.Implications, 
		P.KeyHighlight,
		P.KeyPoints, 
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID, 
		P.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Province P
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Province WRP
				WHERE WRP.ProvinceID = P.ProvinceID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportProvinces