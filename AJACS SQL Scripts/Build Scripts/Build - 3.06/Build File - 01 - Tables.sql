USE AJACS
GO

--Begin table dbo.ClassTrainer
DECLARE @TableName VARCHAR(250) = 'dbo.ClassTrainer'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ClassTrainer
	(
	ClassTrainerID INT IDENTITY(1,1) NOT NULL,
	ClassID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClassTrainerID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ClassTrainer', 'ClassID,ContactID'
GO
--End table dbo.ClassTrainer

--Begin table dropdown.IndicatorStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IndicatorStatus
	(
	IndicatorStatusID INT IDENTITY(0,1) NOT NULL,
	IndicatorStatusName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_IndicatorStatus', 'DisplayOrder,IndicatorStatusName', 'IndicatorStatusID'
GO
--End table dropdown.IndicatorStatus

--Begin table logicalframework.Indicator
EXEC Utility.DropColumn 'logicalframework.Indicator', 'DataCollectionPlanSource'
EXEC Utility.DropColumn 'logicalframework.Indicator', 'DataCollectionPlanFrequency'
EXEC Utility.DropColumn 'logicalframework.Indicator', 'StatusDescription'
GO

EXEC Utility.AddColumn 'logicalframework.Indicator', 'DataCollectionPlanSource', 'VARCHAR(MAX)'
EXEC Utility.AddColumn 'logicalframework.Indicator', 'DataCollectionPlanFrequency', 'VARCHAR(MAX)'
EXEC Utility.AddColumn 'logicalframework.Indicator', 'StatusDescription', 'VARCHAR(MAX)'
EXEC Utility.AddColumn 'logicalframework.Indicator', 'IndicatorStatusID', 'INT', '0'
GO
--End table logicalframework.Indicator
