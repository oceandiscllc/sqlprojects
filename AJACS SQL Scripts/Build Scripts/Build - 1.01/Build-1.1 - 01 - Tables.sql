USE AJACS
GO

--Begin Schemas
--Begin schema logicalframework
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'logicalframework')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA logicalframework'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema logicalframework

--Begin schema permissionable
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'permissionable')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA permissionable'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema permissionable
--End Schemas

--Begin Tables
--Begin table dbo.ClassContact
DECLARE @TableName VARCHAR(250) = 'dbo.ClassContact'

EXEC utility.DropObject 'dbo.ClassStudent'
EXEC utility.DropObject @TableName

CREATE TABLE dbo.ClassContact
	(
	ClassContactID INT IDENTITY(1,1) NOT NULL,
	ClassID INT,
	ContactID INT,
	StudentOutcomeTypeID INT,
	Comments VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StudentOutcomeTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClassContactID'
EXEC utility.SetIndexClustered 'IX_ClassContact', @TableName, 'ClassID,ContactID'
GO
--End table dbo.ClassContact

--Begin table dbo.ConceptNoteBudget
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteBudget'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteBudget
	(
	ConceptNoteBudgetID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	ItemName VARCHAR(100),
	BudgetTypeID INT,
	Quantity INT,
	UnitCost NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'BudgetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UnitCost', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteBudgetID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteBudget', @TableName, 'ConceptNoteID,ConceptNoteBudgetID'
GO
--End table dbo.ConceptNoteBudget

--Begin table dbo.ConceptNoteContact
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContact'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteContact
	(
	ConceptNoteContactID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteContactID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteContact', @TableName, 'ConceptNoteID,ContactID'
GO
--End table dbo.ConceptNoteContact

--Begin table dbo.ConceptNoteCourse
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteCourse'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteCourse
	(
	ConceptNoteCourseID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	CourseID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CourseID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteCourseID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteBudget', @TableName, 'ConceptNoteID,CourseID'
GO
--End table dbo.ConceptNoteCourse

--Begin table dbo.ConceptNoteIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteIndicator
	(
	ConceptNoteIndicatorID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteIndicatorID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteIndicator', @TableName, 'ConceptNoteID,IndicatorID'
GO
--End table dbo.ConceptNoteIndicator

--Begin table dbo.Equipment
ALTER TABLE dbo.Equipment ALTER COLUMN ItemName VARCHAR(250)
GO
--End table dbo.Equipment

--Begin table dbo.MenuItem
DECLARE @TableName VARCHAR(250) = 'dbo.MenuItem'

EXEC Utility.AddColumn @TableName, 'PermissionableLineage', 'VARCHAR(MAX)'
GO
--End table dbo.MenuItem

--Begin table dbo.SpotReport
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReport'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SpotReport
	(
	SpotReportID INT IDENTITY(1,1) NOT NULL,
	SpotReportTitle VARCHAR(100),
	SpotReportDate DATE,
	Summary NVARCHAR(MAX),
	IncidentDetails NVARCHAR(MAX),
	Implications NVARCHAR(MAX),
	Resourcing NVARCHAR(MAX),
	ResourcingStatus NVARCHAR(MAX),
	RiskMitigation NVARCHAR(MAX),
	AnalystComments NVARCHAR(MAX),
	Recommendation NVARCHAR(MAX),
	AdditionalInformation NVARCHAR(MAX),
	ImpactDecisionID INT, 
	StatusChangeID INT, 
	CommunityID INT,
	ProvinceID INT,
	IsCritical BIT,
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ImpactDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsCritical', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StatusChangeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SpotReportID'
GO
--End table dbo.SpotReport

--Begin table dropdown.ConceptNoteBudgetItemType
DECLARE @TableName VARCHAR(250) = 'dropdown.ConceptNoteBudgetItemType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConceptNoteBudgetItemType
	(
	ConceptNoteBudgetItemTypeID INT IDENTITY(0,1) NOT NULL,
	ConceptNoteBudgetItemTypeName VARCHAR(100),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteBudgetItemTypeID'
EXEC utility.SetIndexNonClustered 'IX_ConceptNoteBudgetItemTypeName', @TableName, 'DisplayOrder,ConceptNoteBudgetItemTypeName', 'ConceptNoteBudgetItemTypeID'
GO

SET IDENTITY_INSERT dropdown.ConceptNoteBudgetItemType ON
GO

INSERT INTO dropdown.ConceptNoteBudgetItemType (ConceptNoteBudgetItemTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ConceptNoteBudgetItemType OFF
GO

INSERT INTO dropdown.ConceptNoteBudgetItemType 
	(ConceptNoteBudgetItemTypeName,DisplayOrder)
VALUES
	('Other Direct Costs (Training)', 1),
	('Travel, per diem and Accommodation ', 2),
	('Short term Technical Advisors (Police, RSD and TNA)', 3),
	('Contractual', 4),
	('Equipment', 5)
GO
--End table dropdown.ConceptNoteBudgetItemType

--Begin table dropdown.ConceptNoteType
DECLARE @TableName VARCHAR(250) = 'dropdown.ConceptNoteType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConceptNoteType
	(
	ConceptNoteTypeID INT IDENTITY(0,1) NOT NULL,
	ConceptNoteTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteTypeID'
EXEC utility.SetIndexNonClustered 'IX_ConceptNoteTypeName', @TableName, 'DisplayOrder,ConceptNoteTypeName', 'ConceptNoteTypeID'
GO

SET IDENTITY_INSERT dropdown.ConceptNoteType ON
GO

INSERT INTO dropdown.ConceptNoteType (ConceptNoteTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ConceptNoteType OFF
GO

INSERT INTO dropdown.ConceptNoteType 
	(ConceptNoteTypeName,DisplayOrder)
VALUES
	('Temp', 0)
--End table dropdown.ConceptNoteType

--Begin table dropdown.IndicatorType
DECLARE @TableName VARCHAR(250) = 'dropdown.IndicatorType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IndicatorType
	(
	IndicatorTypeID INT IDENTITY(0,1) NOT NULL,
	IndicatorTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorTypeID'
EXEC utility.SetIndexNonClustered 'IX_IndicatorTypeName', @TableName, 'DisplayOrder,IndicatorTypeName', 'IndicatorTypeID'
GO

SET IDENTITY_INSERT dropdown.IndicatorType ON
GO

INSERT INTO dropdown.IndicatorType (IndicatorTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.IndicatorType OFF
GO
--End table dropdown.IndicatorType

--Begin table dropdown.ObjectiveType
DECLARE @TableName VARCHAR(250) = 'dropdown.ObjectiveType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ObjectiveType
	(
	ObjectiveTypeID INT IDENTITY(0,1) NOT NULL,
	ObjectiveTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ObjectiveTypeID'
EXEC utility.SetIndexNonClustered 'IX_ObjectiveTypeName', @TableName, 'DisplayOrder,ObjectiveTypeName', 'ObjectiveTypeID'
GO

SET IDENTITY_INSERT dropdown.ObjectiveType ON
GO

INSERT INTO dropdown.ObjectiveType (ObjectiveTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ObjectiveType OFF
GO

INSERT INTO dropdown.ObjectiveType 
	(ObjectiveTypeName,DisplayOrder)
VALUES
	('Impact', 1),
	('Outcome', 2),
	('Intermediate Outcome', 3),
	('Output', 4)
GO
--End table dropdown.ObjectiveType

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframework.Indicator
	(
	IndicatorID INT IDENTITY(1,1) NOT NULL,
	IndicatorTypeID INT,
	ObjectiveID INT,
	IndicatorName VARCHAR(100),
	IndicatorDescription VARCHAR(MAX),
	IndicatorSource VARCHAR(MAX),
	BaselineValue VARCHAR(100),
	BaselineDate DATE,
	TargetValue VARCHAR(100),
	TargetDate DATE,
	AchievedValue VARCHAR(100),
	AchievedDate DATE	
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IndicatorID'
EXEC utility.SetIndexClustered 'IX_Indicator', @TableName, 'ObjectiveID,IndicatorName'
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframework.Milestone
	(
	MilestoneID INT IDENTITY(1,1) NOT NULL,
	IndicatorID INT,
	MilestoneName VARCHAR(100),
	TargetValue VARCHAR(100),
	TargetDate DATE,
	AchievedValue VARCHAR(100),
	AchievedDate DATE	
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MilestoneID'
EXEC utility.SetIndexClustered 'IX_Milestone', @TableName, 'IndicatorID,MilestoneName'
GO
--End table logicalframework.Milestone

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframework.Objective
	(
	ObjectiveID INT IDENTITY(1,1) NOT NULL,
	ParentObjectiveID INT,
	ObjectiveTypeID INT,
	ObjectiveName VARCHAR(100),
	ObjectiveDescription VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentObjectiveID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ObjectiveID'
GO
--End table logicalframework.Objective

--Begin table permissionable.DisplayGroup
DECLARE @TableName VARCHAR(250) = 'permissionable.DisplayGroup'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.DisplayGroup
	(
	DisplayGroupID INT IDENTITY(1,1) NOT NULL,
	DisplayGroupCode VARCHAR(50),
	DisplayGroupName VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DisplayGroupID'
EXEC utility.SetIndexClustered 'IX_DisplayGroup', @TableName, 'DisplayOrder,DisplayGroupName,DisplayGroupID'
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.DisplayGroupPermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.DisplayGroupPermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.DisplayGroupPermissionable
	(
	DisplayGroupPermissionableID INT IDENTITY(1,1) NOT NULL,
	DisplayGroupID INT,
	PermissionableID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DisplayGroupPermissionableID'
EXEC utility.SetIndexClustered 'IX_DisplayGroup', @TableName, 'DisplayGroupID,PermissionableID'
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.Permissionable
	(
	PermissionableID INT IDENTITY(1,1) NOT NULL,
	ParentPermissionableID INT,
	PermissionableCode VARCHAR(50),
	PermissionableName VARCHAR(100),
	PermissionableLineage VARCHAR(MAX),
	DisplayGroupID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ParentPermissionableID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableID'
GO

EXEC Utility.DropObject 'permissionable.TR_Permissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A trigger to update the permissionable.Permissionable table
-- ========================================================================
CREATE TRIGGER permissionable.TR_Permissionable ON permissionable.Permissionable FOR INSERT, UPDATE
AS
SET ARITHABORT ON

DECLARE @nPermissionableID INT

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT I.PermissionableID
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPermissionableID
	WHILE @@fetch_status = 0
		BEGIN

		;
		WITH HD (PermissionableID,ParentPermissionableID,NodeLevel)
			AS 
			(
			SELECT
				P.PermissionableID, 
				P.ParentPermissionableID, 
				1 
			FROM permissionable.Permissionable P
			WHERE P.PermissionableID = @nPermissionableID
	
			UNION ALL
		
			SELECT
				P.PermissionableID, 
				P.ParentPermissionableID, 
				HD.NodeLevel + 1 AS NodeLevel
			FROM permissionable.Permissionable P
				JOIN HD ON HD.ParentPermissionableID = P.PermissionableID 
			)

		UPDATE P
		SET P.PermissionableLineage = STUFF(CAST((SELECT '.' + P.PermissionableCode FROM HD JOIN permissionable.Permissionable P ON P.PermissionableID = HD.PermissionableID ORDER BY HD.NodeLevel DESC FOR XML PATH('')) AS VARCHAR(MAX)), 1, 1, '')
		FROM permissionable.Permissionable P
		WHERE P.PermissionableID = @nPermissionableID

		FETCH oCursor INTO @nPermissionableID
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO		
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PersonPermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PersonPermissionable
	(
	PersonPermissionableID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	PermissionableID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PermissionableID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PersonPermissionableID'
EXEC utility.SetIndexClustered 'IX_PersonPermissionable', @TableName, 'PersonID,PermissionableID'
GO
--End table permissionable.PersonPermissionable

--Begin table weeklyreport.WeeklyReport
DECLARE @TableName VARCHAR(250) = 'weeklyreport.WeeklyReport'

EXEC Utility.AddColumn @TableName, 'WorkflowStepNumber', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 1
GO
--End table weeklyreport.WeeklyReport
--End Tables