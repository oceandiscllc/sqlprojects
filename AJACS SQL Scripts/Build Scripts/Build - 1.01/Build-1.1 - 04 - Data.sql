USE AJACS
GO

/*
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LogicalFramework', @AfterMenuItemCode='ActivityManagement', @NewMenuItemText='Logical Framework', @PersonIDList='4,17,26'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveAdd', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Objectives', @NewMenuItemLink='/objective/addupdate/id/0', @PersonIDList='4,17,26'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorAdd', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Indicators', @NewMenuItemLink='/indicator/addupdate/id/0', @AfterMenuItemCode='ObjectiveAdd', @PersonIDList='4,17,26'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorTypeList', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Indicator Types', @NewMenuItemLink='/indicatortype/list', @AfterMenuItemCode='IndicatorAdd', @PersonIDList='4,17,26'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='MilestoneAdd', @ParentMenuItemCode='LogicalFramework', @NewMenuItemText='Milestones', @NewMenuItemLink='/milestone/addupdate/id/0', @AfterMenuItemCode='IndicatorTypeList', @PersonIDList='4,17,26'
GO
*/

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Indicator')
	INSERT INTO dbo.EntityType (EntityTypeGroupCode,EntityTypeCode,EntityTypeName) VALUES ('LogicalFramwork', 'Indicator', 'Indicator')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'IndicatorType')
	INSERT INTO dbo.EntityType (EntityTypeGroupCode,EntityTypeCode,EntityTypeName) VALUES ('LogicalFramwork', 'IndicatorType', 'Indicator Type')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Objective')
	INSERT INTO dbo.EntityType (EntityTypeGroupCode,EntityTypeCode,EntityTypeName) VALUES ('LogicalFramwork', 'Objective', 'Objective')
--ENDIF
GO

--Begin table dropdown.DateFilter
TRUNCATE TABLE dropdown.DateFilter
GO

SET IDENTITY_INSERT dropdown.DateFilter ON
GO

INSERT INTO dropdown.DateFilter (DateFilterID) VALUES (0)

SET IDENTITY_INSERT dropdown.DateFilter OFF
GO

INSERT INTO dropdown.DateFilter 
	(DateFilterName,DateNumber,DisplayOrder)
VALUES
	('Today', 1, 1),
	('Last Week', 7, 2),
	('Last 2 Weeks', 14, 3),
	('Last 30 days', 30, 4),
	('Last 60 days', 60, 5),
	('Last 90 days', 90, 6)
GO
--End table dropdown.DateFilter

UPDATE dropdown.CommunityEngagementStatus
SET CommunityEngagementStatusName = 'Engagement Suspended'
WHERE CommunityEngagementStatusName = 'Engagement Suspected'
GO

DELETE FROM dbo.Person WHERE IsActive = 0
GO

IF (SELECT dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '')) = ''
	BEGIN
	
	INSERT INTO AJACSUtility.dbo.ServerSetup
		(ServerSetupKey,ServerSetupValue)
	VALUES
		(
		'ConceptNoteBackgroundText',
		'The Access to Justice and Community Security (AMNUNA) project aims to enable people in opposition-controlled Syria to experience increasingly effective accountable and transparent security and justice services, which are delivered on a progressively formalised basis by civilian-led institutions; in increasingly resilient partnership with communities.' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
			+ 'In order to do this, the programme will work with both communities and formal/informal S&J service providers (in particular the Free Syrian Police or FSP) to develop their capacity to design and deliver services that respond to the needs of the population.  The programme seeks to promote local ownership and leadership of the decision-making processes which guide the delivery of resources, including those supplied by AMNUNA.'
		)

	END
--ENDIF
GO

--Begin table permissionable.DisplayGroup
TRUNCATE TABLE permissionable.DisplayGroup
GO

INSERT INTO permissionable.DisplayGroup
	(DisplayGroupCode,DisplayGroupName,DisplayOrder)
VALUES
	('CommunityProvince', 'Communities & Provinces', 1),
	('RAPData', 'RAP Data', 2),
	('RAPDelivery', 'RAP Delivery', 3),
	('Training', 'Training', 4),
	('Procurement', 'Procurement', 5),
	('ConceptNote', 'Concept Note', 6),
	('ObservationReport', 'Observations & Reports', 7),
	('Administrative', 'Administrative', 8)
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

INSERT INTO permissionable.Permissionable
	(PermissionableCode, PermissionableName, DisplayOrder)
VALUES
	('ActivityManagement','Activity Management', 0),
	('Atmospheric','General Atmospherics', 0),
	('Class','Class Schedules', 2),
	('Community','Communities', 0),
	('CommunityMemberSurvey','Community Member Survey', 2),
	('ConceptNote','Concept Notes', 0),
	('Contact','Contacts', 0),
	('Course','Course Catalog', 1),
	('DailyReport','Daily Reports', 0),
	('Document','Documents', 0),
	('EquipmentCatalog','Equipment Catalog', 0),
	('EquipmentInventory','Equipment Inventory', 0),
	('FocusGroupSurvey','Focus Group Questionnaire', 3),
	('Indicator','Indicators', 0),
	('IndicatorType','Indicator Types', 0),
	('KeyEvent','Key Events', 0),
	('KeyInformantSurvey','Key Informant Interview', 4),
	('LicenseEquipmentCatalog','Licensed Equipment', 0),
	('License','Licenses', 0),
	('Milestone','Milestones', 0),
	('Objective','Objectives', 0),
	('Person','Users', 0),
	('Province','Provinces', 0),
	('RAPData','View RAP Data', 1),
	('RapidPerceptionSurvey','Rapid Perception Survey', 5),
	('SpotReport','Spot Reports', 0),
	('StakeholderGroupSurvey','Stakeholder Group Questionnaire', 6),
	('StationCommanderSurvey','FSP Station Commander Survey', 7),
	('Team','Teams', 0),
	('WeeklyReport','Weekly Reports', 0)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	1
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN 
	(
	'Atmospheric',
	'Class',
	'Community',
	'CommunityMemberSurvey',
	'ConceptNote',
	'Contact',
	'Course',
	'DailyReport',
	'Document',
	'EquipmentCatalog',
	'EquipmentInventory',
	'FocusGroupSurvey',
	'Indicator',
	'IndicatorType',
	'KeyEvent',
	'KeyInformantSurvey',
	'LicenseEquipmentCatalog',
	'License',
	'Milestone',
	'Objective',
	'Person',
	'Province',
	'SpotReport',
	'StakeholderGroupSurvey',
	'StationCommanderSurvey',
	'Team',
	'WeeklyReport'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'View',
	'View',
	2
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN 
	(
	'Atmospheric',
	'Class',
	'Community',
	'CommunityMemberSurvey',
	'ConceptNote',
	'Contact',
	'Course',
	'DailyReport',
	'Document',
	'EquipmentCatalog',
	'EquipmentInventory',
	'FocusGroupSurvey',
	'Indicator',
	'IndicatorType',
	'KeyEvent',
	'KeyInformantSurvey',
	'LicenseEquipmentCatalog',
	'License',
	'Milestone',
	'Objective',
	'Person',
	'Province',
	'RapidPerceptionSurvey',
	'SpotReport',
	'StakeholderGroupSurvey',
	'StationCommanderSurvey',
	'Team',
	'WeeklyReport'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'List',
	'List',
	3
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN 
	(
	'Atmospheric',
	'Class',
	'Community',
	'ConceptNote',
	'Contact',
	'Course',
	'DailyReport',
	'Document',
	'EquipmentCatalog',
	'EquipmentInventory',
	'IndicatorType',
	'KeyEvent',
	'LicenseEquipmentCatalog',
	'License',
	'Person',
	'Province',
	'RAPData',
	'SpotReport',
	'Team',
	'WeeklyReport'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'WorkflowStep1',
	'Workflow Step 1',
	1
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'SpotReport.AddUpdate',
	'WeeklyReport.AddUpdate'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'WorkflowStep2',
	'Workflow Step 2',
	1
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'SpotReport.AddUpdate',
	'WeeklyReport.AddUpdate'
	)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P.PermissionableID,
	'WorkflowStep3',
	'Workflow Step 3',
	1
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN 
	(
	'SpotReport.AddUpdate',
	'WeeklyReport.AddUpdate'
	)
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
TRUNCATE TABLE permissionable.DisplayGroupPermissionable
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityProvince'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Community','Province')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'RAPData'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('CommunityMemberSurvey','FocusGroupSurvey','KeyInformantSurvey','RAPData','RapidPerceptionSurvey','StakeholderGroupSurvey','StationCommanderSurvey')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'RAPDelivery'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('DailyReport','Team')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Training'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Class','Course')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Procurement'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('EquipmentCatalog','EquipmentInventory','LicenseEquipmentCatalog','License')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'ConceptNote'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('ActivityManagement','ConceptNote','Indicator','IndicatorType','Milestone','Objective')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'ObservationReport'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Atmospheric','Document','KeyEvent','SpotReport','WeeklyReport')
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Administrative'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Contact','Person')
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table dbo.MenuItem
/*
UPDATE dbo.MenuItem
SET
	MenuItemCode = 'SpotReport',
	MenuItemLink = '/spotreport/list',
	MenuItemText = 'Spot Reports'
WHERE MenuItemCode = 'KeyEvent'
GO
*/

UPDATE MI
SET MI.PermissionableLineage = P.PermissionableLineage
FROM dbo.MenuItem MI
	JOIN permissionable.Permissionable P ON P.PermissionableLineage = REPLACE(REPLACE(STUFF(MI.MenuItemLink, 1, 1, ''), '/', '.'), '.id.0', '')
GO

UPDATE MI
SET MI.PermissionableLineage = 'EquipmentInventory.List'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'EquipmentInventoryList'
GO
--End table dbo.MenuItem

--Begin table permissionable.PersonPermissionable
TRUNCATE TABLE permissionable.PersonPermissionable
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableID)
SELECT
	PMI.PersonID,
	P.PermissionableID
FROM dbo.PersonMenuItem PMI
	JOIN dbo.MenuItem MI ON MI.MenuItemID = PMI.MenuItemID
	JOIN permissionable.Permissionable P ON P.PermissionableLineage = MI.PermissionableLineage
GO
--End table permissionable.PersonPermissionable
