USE AJACS
GO

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
-- ====================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.ExternalCapacity,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CO.CourseID,
		CO.CourseName
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
	
	SELECT

		CASE
			WHEN CO.CommunityID > 0
			THEN (SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CO.CommunityID)
			WHEN CO.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CO.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		
END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
-- ======================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		CN.ConceptNoteID,
		CN.ConceptNoteID, 
		CN.Title, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.PointOfContactPersonID1, 
		CN.PointOfContactPersonID2, 
		CN.Background, 
		CN.Objectives, 
		CN.SoleSourceJustification, 
		CN.BeneficiaryDetails, 
		CN.MonitoringEvaluation, 
		CN.ActivityCode, 
		CN.OtherDeliverable, 
		CN.FundedBy,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.ConceptNoteBudgetID,
		CNB.ItemName,
		CNB.Quantity,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CourseID,
		C.CourseName
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
			
	SELECT
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	Fixed the province name bug
-- ==================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.ArabicName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID,
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID
		
END
GO
--End procedure dbo.GetContactByContactID

/*
--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT,
@IncludeFullMenu BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		0,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				MI.PermissionableLineage IS NULL 
					OR EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
							JOIN permissionable.Permissionable P ON P.PermissionableID = PP.PermissionableID
								AND P.PermissionableLineage = MI.PermissionableLineage
								AND PP.PersonID = @PersonID
						)
				)			
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID
*/

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Added the IsAccountLockedOut bit
-- ============================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.FirstName, 
		P.IsAccountLockedOut,
		P.LastName,
		P.Organization,
		P.PersonID,
		P.RoleID, 
		P.Title,
		P.UserName,
		R.RoleName
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
-- =====================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName,
		C.Icon AS CurrentCommunityIcon,
		C.ImpactDecisionName AS CurrentCommunityImpactDecisionName,
		C.StatusChangeName AS CurrentCommunityStatusChangeName,
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		P.Icon AS CurrentProvinceIcon,
		P.ProvinceName,
		P.ImpactDecisionName AS CurrentProvinceImpactDecisionName,
		P.StatusChangeName AS CurrentProvinceStatusChangeName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunityID,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProvinceID,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportTitle,
		SR.Summary,
		'AJACS-SR-A' + RIGHT('0000' + CAST(SR.SpotReportID AS VARCHAR(10)), 4) AS ReferenceCode,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		OUTER APPLY
				(
				SELECT
					C1.CommunityName,
					ID1.ImpactDecisionName,
					SC1.StatusChangeName,
					dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID1.HexColor, '#', '') + '-' + SC1.Direction + '.png' AS Icon
				FROM dbo.Community C1
					JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = C1.ImpactDecisionID
					JOIN dropdown.StatusChange SC1 ON SC1.StatusChangeID = C1.StatusChangeID
						AND C1.CommunityID = SR.CommunityID
				) C
		OUTER APPLY
				(
				SELECT
					P1.ProvinceName,
					ID2.ImpactDecisionName,
					SC2.StatusChangeName,
					dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID2.HexColor, '#', '') + '-' + SC2.Direction + '.png' AS Icon
				FROM dbo.Province P1
					JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = P1.ImpactDecisionID
					JOIN dropdown.StatusChange SC2 ON SC2.StatusChangeID = P1.StatusChangeID
						AND P1.ProvinceID = SR.ProvinceID
				) P

END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin
	@UserName VARCHAR(250),
	@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50)
		)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.GetPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
				THEN 1
				ELSE 0
			END,

		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsValidPassword,IsValidUserName,FullName,RoleName) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)
		
			SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

END
GO
--End procedure dbo.ValidateLogin

--Begin procedure dropdown.GetConceptNoteBudgetItemTypeData
EXEC Utility.DropObject 'dropdown.GetConceptNoteBudgetItemTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A stored procedure to return data from the dropdown.ConceptNoteBudgetItemType table
-- ================================================================================================
CREATE PROCEDURE dropdown.GetConceptNoteBudgetItemTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConceptNoteBudgetItemTypeID, 
		T.ConceptNoteBudgetItemTypeName,
		T.IsActive
	FROM dropdown.ConceptNoteBudgetItemType T
	WHERE (T.ConceptNoteBudgetItemTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConceptNoteBudgetItemTypeName, T.ConceptNoteBudgetItemTypeID

END
GO
--End procedure dropdown.GetConceptNoteBudgetItemTypeData

--Begin procedure dropdown.GetIndicatorTypeData
EXEC Utility.DropObject 'dropdown.GetIndicatorTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	A stored procedure to return data from the dropdown.IndicatorType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetIndicatorTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IndicatorTypeID, 
		T.IndicatorTypeName,
		T.IsActive
	FROM dropdown.IndicatorType T
	WHERE (T.IndicatorTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.IndicatorTypeName, T.IndicatorTypeID

END
GO
--End procedure dropdown.GetIndicatorTypeData

--Begin procedure dropdown.GetObjectiveTypeData
EXEC Utility.DropObject 'dropdown.GetObjectiveTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	A stored procedure to return data from the dropdown.ObjectiveType table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetObjectiveTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ObjectiveTypeID, 
		T.ObjectiveTypeName
	FROM dropdown.ObjectiveType T
	WHERE (T.ObjectiveTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ObjectiveTypeName, T.ObjectiveTypeID

END
GO
--End procedure dropdown.GetObjectiveTypeData

--Begin procedure eventlog.LogEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogEquipmentCatalogAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentCatalogAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentCatalog',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('EquipmentCatalog'), ELEMENTS
			)
		FROM dbo.EquipmentCatalog T
		WHERE T.EquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentCatalogAction

--Begin procedure eventlog.LogIndicatorAction
EXEC utility.DropObject 'eventlog.LogIndicatorAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIndicatorAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('Indicator'), ELEMENTS
			)
		FROM dbo.Indicator T
		WHERE T.IndicatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIndicatorAction

--Begin procedure eventlog.LogLicenseAction
EXEC utility.DropObject 'eventlog.LogLicenseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLicenseAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'License',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'License',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('License'), ELEMENTS
			)
		FROM dbo.License T
		WHERE T.LicenseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLicenseAction

--Begin procedure eventlog.LogLicenseEquipmentCatalogAction
EXEC utility.DropObject 'eventlog.LogLicenseEquipmentCatalogAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLicenseEquipmentCatalogAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'LicenseEquipmentCatalog',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'LicenseEquipmentCatalog',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('LicenseEquipmentCatalog'), ELEMENTS
			)
		FROM dbo.LicenseEquipmentCatalog T
		WHERE T.LicenseEquipmentCatalogID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLicenseEquipmentCatalogAction

--Begin procedure eventlog.LogMilestoneAction
EXEC utility.DropObject 'eventlog.LogMilestoneAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogMilestoneAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Milestone',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Milestone',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('Milestone'), ELEMENTS
			)
		FROM dbo.Milestone T
		WHERE T.MilestoneID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogMilestoneAction

--Begin procedure eventlog.LogObjectiveAction
EXEC utility.DropObject 'eventlog.LogObjectiveAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogObjectiveAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Objective',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Objective',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('Objective'), ELEMENTS
			)
		FROM dbo.Objective T
		WHERE T.ObjectiveID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogObjectiveAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Person',
			@EntityID
			)

		END
	ELSE
		BEGIN

		DECLARE @cPersonPermissionables VARCHAR(MAX) 
	
		SELECT 
			@cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PersonPermissionable'), ELEMENTS) AS PersonPermissionable
			FROM permissionable.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			@EntityID,
			(
			SELECT T.*, 
			CAST(('<PersonPermissionables>' + ISNULL(@cPersonPermissionables, '') + '</PersonPermissionables>') AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM dbo.Person T
		WHERE T.PersonID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('SpotReport'), ELEMENTS
			)
		FROM dbo.SpotReport T
		WHERE T.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		I.AchievedValue, 	
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorSource, 	
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 	
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetIndicators
EXEC Utility.DropObject 'logicalframework.GetIndicators'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicators

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IndicatorID,
		I.IndicatorName
	FROM logicalframework.Indicator I
	ORDER BY I.IndicatorName, I.IndicatorID

END
GO
--End procedure logicalframework.GetIndicators

--Begin procedure logicalframework.GetMilestoneByMilestoneID
EXEC Utility.DropObject 'logicalframework.GetMilestoneByMilestoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByMilestoneID

@MilestoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IndicatorID, 	
		I.IndicatorName, 	
		M.AchievedDate,
		dbo.FormatDate(M.AchievedDate) AS AchievedDateFormatted,
		M.AchievedValue, 	
		M.MilestoneID, 	
		M.MilestoneName, 	
		M.TargetDate, 	
		dbo.FormatDate(M.TargetDate) AS TargetDateFormatted,
		M.TargetValue, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Milestone') AS EntityTypeName
	FROM logicalframework.Milestone M
		JOIN logicalframework.Indicator I ON I.IndicatorID = M.IndicatorID
			AND M.MilestoneID = @MilestoneID

END
GO
--End procedure logicalframework.GetMilestoneByMilestoneID

--Begin procedure logicalframework.GetObjectiveByObjectiveID
EXEC Utility.DropObject 'logicalframework.GetObjectiveByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	A stored procedure to return Objective data
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectiveByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O1.ObjectiveDescription,
		O1.ObjectiveID, 
		O1.ObjectiveName, 
		O1.ParentObjectiveID, 

		CASE
			WHEN O1.ParentObjectiveID = 0
			THEN NULL
			ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
		END AS ParentObjectiveName,
		
		OT.ObjectiveTypeID, 
		OT.ObjectiveTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
	FROM logicalframework.Objective O1
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
			AND O1.ObjectiveID = @ObjectiveID

END
GO
--End procedure logicalframework.GetObjectiveByObjectiveID

--Begin procedure logicalframework.GetObjectives
EXEC Utility.DropObject 'logicalframework.GetObjectives'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	A stored procedure to return Objective data
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectives

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		O.ObjectiveID,
		O.ObjectiveName
	FROM logicalframework.Objective O
	ORDER BY O.ObjectiveName, O.ObjectiveID

END
GO
--End procedure logicalframework.GetObjectives

--Begin procedure permissionable.GetPermissionables
EXEC Utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date: 2015.03.08
-- Description:	A stored procedure to get system permissionables
-- =============================================================
CREATE PROCEDURE permissionable.GetPermissionables
@PersonID INT

AS
BEGIN

	DECLARE @nPadLength INT
	
	SELECT @nPadLength = LEN(CAST(COUNT(P.PermissionableID) AS VARCHAR(50)))
	FROM permissionable.Permissionable P
	
	;
	WITH HD (DisplayIndex,PermissionableID,PermissionableClass,ParentPermissionableID,DisplayGroupID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY DG.DisplayOrder, P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			'perm-' + CAST(P.PermissionableID AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			DG.DisplayGroupID,
			1
		FROM permissionable.Permissionable P
			JOIN permissionable.DisplayGroupPermissionable DGP ON DGP.PermissionableID = P.PermissionableID
			JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = DGP.DisplayGroupID
		WHERE P.ParentPermissionableID = 0
	
		UNION ALL
	
		SELECT
			CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			CAST(HD.PermissionableClass + ' perm-' + CAST(P.PermissionableID AS VARCHAR(10)) AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			HD.DisplayGroupID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM permissionable.Permissionable P
			JOIN HD ON HD.PermissionableID = P.ParentPermissionableID
		)
	
	SELECT
		HD1.NodeLevel,
		HD1.ParentPermissionableID,
		HD1.PermissionableID,
		'group-' + CAST(DG.DisplayGroupID AS VARCHAR(10)) + ' ' + RTRIM(REPLACE(HD1.PermissionableClass, 'perm-' + CAST(HD1.PermissionableID AS VARCHAR(10)), '')) AS PermissionableClass,
		HD1.DisplayGroupID,
		DG.DisplayGroupName,
		P.PermissionableName,
	
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentPermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasChildren,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP WHERE PP.PersonID = @PersonID AND PP.PermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
	
	FROM HD HD1
		JOIN permissionable.Permissionable P ON P.PermissionableID = HD1.PermissionableID
		JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = HD1.DisplayGroupID
	ORDER BY HD1.DisplayIndex

END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure reporting.GetWeeklyReportCommunity
EXEC Utility.DropObject 'reporting.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportCommunity

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		CES.CommunityEngagementStatusName,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,
		
		CASE
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID IN (2,3)
			THEN 'Watch'
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID = 4
			THEN 'Engage'
			WHEN C.StatusChangeID = 1
			THEN 'Alert'
			ELSE ''
		END AS CommunityReportStatusName
			
	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure reporting.GetWeeklyReportCommunity

--Begin procedure reporting.GetWeeklyReportImpactDecision
EXEC Utility.DropObject 'reporting.GetWeeklyReport'
EXEC Utility.DropObject 'reporting.GetWeeklyReportImpactDecision'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportImpactDecision

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ISNULL(ID.ImpactDecisionName, 'None') AS ImpactDecisionName,
		ID.HexColor
	FROM
		(
		SELECT
			C.ImpactDecisionID
		FROM weeklyreport.Community C
	
		UNION
		
		SELECT
			P.ImpactDecisionID
		FROM weeklyreport.Province P
		) D
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = D.ImpactDecisionID
	ORDER BY ID.DisplayOrder, ID.ImpactDecisionName, ID.ImpactDecisionID

END
GO
--End procedure reporting.GetWeeklyReportImpactDecision

--Begin procedure reporting.GetWeeklyReportProvince
EXEC Utility.DropObject 'reporting.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportProvince

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceName, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
	ORDER BY P.ProvinceName, P.ProvinceID

END
GO
--End procedure reporting.GetWeeklyReportProvince

--Begin procedure reporting.GetWeeklyReportStatusChange
EXEC Utility.DropObject 'reporting.GetWeeklyReportStatusChange'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportStatusChange

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ISNULL(SC.StatusChangeName, 'None') AS StatusChangeName,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/000000' + '-' + SC.Direction + '.png' AS Icon
	FROM
		(
		SELECT
			C.StatusChangeID
		FROM weeklyreport.Community C
	
		UNION
		
		SELECT
			P.StatusChangeID
		FROM weeklyreport.Province P
		) D
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = D.StatusChangeID
	ORDER BY SC.DisplayOrder, SC.StatusChangeName, SC.StatusChangeID

END
GO
--End procedure reporting.GetWeeklyReportStatusChange

--Begin procedure utility.MenuItemAddUpdate
EXEC Utility.DropObject 'utility.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to add / update a menu item
-- ===============================================================
CREATE PROCEDURE utility.MenuItemAddUpdate
	@NewMenuItemCode VARCHAR(50),
	@ParentMenuItemCode VARCHAR(50) = NULL,
	@BeforeMenuItemCode VARCHAR(50) = NULL,
	@AfterMenuItemCode VARCHAR(50) = NULL,
	@NewMenuItemText VARCHAR(250) = NULL,
	@NewMenuItemLink VARCHAR(500) = NULL,
	@IsActive BIT = 1,
	@PersonIDList VARCHAR(MAX) = NULL,
	@PermissionableLineage VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nMenuItemID INT
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF EXISTS (SELECT 1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID,
			@nOldParentMenuItemID = MI.ParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @NewMenuItemCode

		IF @ParentMenuItemCode IS NOT NULL
			BEGIN

			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM dbo.MenuItem MI 
			WHERE MI.MenuItemCode = @ParentMenuItemCode

			END
		--ENDIF
		
		END
	ELSE
		BEGIN

		DECLARE @tOutput TABLE (MenuItemID INT)

		SET @nIsUpdate = 0
		
		SELECT @nParentMenuItemID = MI.MenuItemID
		FROM dbo.MenuItem MI
		WHERE MI.MenuItemCode = @ParentMenuItemCode
		
		INSERT INTO dbo.MenuItem 
			(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,PermissionableLineage,IsActive)
		OUTPUT INSERTED.MenuItemID INTO @tOutput
		VALUES
			(
			@nParentMenuItemID,
			@NewMenuItemText,
			@NewMenuItemCode,
			@NewMenuItemLink,
			@PermissionableLineage,
			@IsActive
			)

		SELECT @nNewMenuItemID = O.MenuItemID 
		FROM @tOutput O
		
		END
	--ENDIF

	IF @nIsUpdate = 1
		BEGIN

		UPDATE dbo.MenuItem SET IsActive = @IsActive WHERE MenuItemID = @nNewMenuItemID
		IF @NewMenuItemLink IS NOT NULL
			UPDATE dbo.MenuItem SET MenuItemLink = @NewMenuItemLink WHERE MenuItemID = @nNewMenuItemID
		--ENDIF
		IF @NewMenuItemText IS NOT NULL
			UPDATE dbo.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
		--ENDIF
		IF @nOldParentMenuItemID <> @nParentMenuItemID
			UPDATE dbo.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
		--ENDIF
		IF @PermissionableLineage IS NOT NULL
			UPDATE dbo.MenuItem SET PermissionableLineage = @PermissionableLineage WHERE MenuItemID = @nNewMenuItemID
		--ENDIF

		END
	--ENDIF

	DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			MI.MenuItemID,
			MI.MenuItemCode
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = @nParentMenuItemID
			AND MI.MenuItemID <> @nNewMenuItemID
		ORDER BY MI.DisplayOrder

	OPEN oCursor
	FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	WHILE @@fetch_status = 0
		BEGIN

		IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
			BEGIN
			
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
			SET @nIsInserted = 1
			
			END
		--ENDIF

		INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)

		IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
			BEGIN
			
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
			SET @nIsInserted = 1
			
			END
		--ENDIF
		
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode

		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor	

	UPDATE MI
	SET MI.DisplayOrder = T1.DisplayOrder
	FROM dbo.MenuItem MI
		JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID

	IF @PersonIDLIst IS NOT NULL
		BEGIN
		
		INSERT INTO dbo.PersonMenuItem
			(PersonID,MenuItemID)
		SELECT
			CAST(LTT.ListItem AS INT),
			@nNewMenuItemID
		FROM dbo.ListToTable(@PersonIDLIst, ',') LTT
		WHERE NOT EXISTS
			(
			SELECT 1
			FROM dbo.PersonMenuItem PMI
			WHERE PMI.PersonID = CAST(LTT.ListItem AS INT)
				AND PMI.MenuItemID = @nNewMenuItemID
			)
		
		END
	--ENDIF
	
END
GO
--End procedure utility.MenuItemAddUpdate
