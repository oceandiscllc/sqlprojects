USE AJACS
GO

--Begin procedure reporting.GetCommunityRoundList
EXEC utility.DropObject 'reporting.GetCommunityRoundList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:		Justin Branum
-- Create date:	2016.05.29
-- Description:	A stored procedure to get data from the dbo.CommunityRound table
-- =============================================================================

CREATE PROCEDURE reporting.GetCommunityRoundList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.communityName,
		C.Population,
		CES.CommunityEngagementStatusName,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.CommunityCode,
		CR.CommunityRoundID,
		CR.CommunityRoundRoundID,
		CR.FemaleQuestionnairCount,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficer,
		FOT.FieldOfficerHiredTypeName,
		CR.IsFieldOfficerIntroToFSPandLAC AS IsFieldOfficerIntroToFSPandLA,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.NewCommunityEngagementCluster,
		CR.PostAssessment,
		CR.PreAssessment,
		CR.QuestionnairCount,
		CR.SCAMeetingCount,
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRES.CommunityRoundEngagementStatusName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusNam,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		case when CR.CSAPFinalizedDate is null then 'No' else 'Yes' end as 'FinalizedCSAP', 
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted,
		case when CR.SCAFinalizedDate is null then 'No' else 'Yes' end as 'FinalizedSCA',
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		ID.ImpactDecisionName,
		P.provinceName,
		PPC.PolicePresenceCategoryName,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
			AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Male' 
		) AS CSWGMaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Female' 
		) AS CSWGFemaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.USVettingExpirationDate > getdate()
		) AS CSWGRAMActiveCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.UKVettingExpirationDate  > getdate()
		) AS CSWGUKActiveCount,
		dbo.FormatDate(
		(
		SELECT MAX(UpdateDate)
		FROM dbo.CommunityRoundUpdate 
		WHERE CommunityRoundID = CR.CommunityRoundID
		)) AS CurrentUpdateDateFormatted,
		(
		SELECT Top 1 
		FOUpdates +' <br>' + POUpdates+' <br>' + NextStepsUpdates +' <br>' +NextStepsProcess +' <br>' + Risks
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateData,
			dbo.FormatDate(
			(
				SELECT MAX(UpdateDate)
				FROM dbo.CommunityRoundUpdate 
				WHERE CommunityRoundID = CR.CommunityRoundID
				AND UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
			)) AS LastWeekUpdateDateFormatted
			,(SELECT Top 1 
				FOUpdates +' <br>' + POUpdates+' <br>' + NextStepsUpdates+' <br>' +NextStepsProcess+' <br>' + Risks
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateData,
		(
		SELECT Top 1 
		FOUpdates
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateFOUpdates,
		(
		SELECT Top 1 
		POUpdates
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdatePOUpdates,
		(
		SELECT Top 1 
		NextStepsUpdates +' <br>' +NextStepsProcess
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateNextStepsUpdates,
		(
		SELECT Top 1 
		Risks
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateRisk,
		(SELECT Top 1 
				FOUpdates
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateFOUpdates,
		(SELECT Top 1 
				POUpdates
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdatePOUpdates,
		(SELECT Top 1 
				NextStepsUpdates +' <br>' +NextStepsProcess
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateNextSteps,
		(SELECT Top 1 
				Risks
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateRisks
	FROM dbo.CommunityRound CR
		JOIN reporting.SearchResult RSR ON RSR.EntityID = CR.communityroundID
				AND RSR.EntityTypeCode = 'CommunityRound'
				AND RSR.PersonID = @PersonID
		JOIN dbo.community C ON C.communityID = CR.communityID
		JOIN dbo.province P ON P.provinceID = C.provinceID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityRoundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID	
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
		JOIN dropdown.FieldOfficerHiredType FOT ON CR.FieldOfficerHiredTypeID = FOT.FieldOfficerHiredTypeID

END
GO
--End procedure reporting.GetCommunityRoundList

--Begin procedure reporting.GetJusticeStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPayments

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UPPER(LEFT(DateName( month , DateAdd( month , D.PaymentMonth , -1 ) ),3)) + ' - ' + CAST(D.PaymentYear as varchar(5)) AS YearMonthFormatted,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		SUM(D.StipendAmountPaid) AS StipendAmountPaid,
		SUM(D.StipendAmountAuthorized) AS StipendAmountAuthorized,
		(
		SELECT SUM(ExpenseAmountAuthorized)  
		FROM dbo.CommunityAssetUnitExpense CAUE 
			JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = CAUE.CommunityAssetUnitID
			JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = CAU.CommunityAssetID 
				AND CAUE.ProvinceID = D.ProvinceID 
				AND CAUE.PaymentYear = D.PaymentYear 
				AND CAUE.PaymentMonth = D.PaymentMonth
		) AS StipendExpenseAmount
	FROM
		(
		SELECT
			CSP.StipendAmountPaid,
			CSP.StipendAmountAuthorized,
			CSP.ContactID,
			CSP.ProvinceID,
			CSP.PaymentYear,
			CSP.PaymentMonth,
		
			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			CAST(CSP.PAYMENTYEAR AS VARCHAR(50)) + CAST(CSP.PAYMENTMONTH AS VARCHAR(50)) AS YEARMONTH,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID 
			AND StipendName IN ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') 
			AND StipendTypeCode ='JusticeStipend' 
		) D
	GROUP BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.provinceID
	ORDER BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.ProvinceID ASC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPayments

--Begin procedure reporting.GetJusticeStipendActivitySummaryPeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPeople

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(PVT.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(PVT.YearMonth, 4) AS YearMonthFormatted,
		dbo.GetProvinceNameByProvinceID(@ProvinceID) AS ProvinceName,
		*
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			CAST(CSP.PaymentYear AS VARCHAR(50)) + CAST(CSP.PaymentMonth AS VARCHAR(50)) AS YearMonth,
			CSP.PaymentYear,
			CSP.PaymentMonth
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
			AND StipendName IN ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') 
			AND StipendTypeCode = 'JusticeStipend' 
			AND ProvinceID = @ProvinceID
		GROUP BY CSP.PaymentYear, CSP.PaymentMonth, CSP.StipendName
		) AS D
	PIVOT
		(
		MAX(D.StipendNameCount)
		FOR D.StipendName IN
			(
			[Rank 1],
			[Rank 2],
			[Rank 3],
			[Rank 4],
			[Rank 5]
			)
		) AS PVT
	ORDER BY PVT.YearMonth DESC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPeople

--Begin procedure reporting.GetJusticeStipendActivityVariancePayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePayments

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendAmountAuthorizedTotal,
		B.StipendAmountPaidTotal,
		   B.StipendAmountPaidTotal - A.StipendAmountAuthorizedTotal  AS Variance
	FROM
		(
			SELECT 
		SUM(D.StipendAmountAuthorizedTotal) as StipendAmountAuthorizedTotal,
		D.StipendName,
		D.DisplayOrder
		FROM (
		SELECT 
			CSP.StipendAmountAuthorized,
			CASE 
			WHEN CSP.StipendAuthorizedDate IS NULL THEN 0 
			ELSE CSP.StipendAmountAuthorized
			END as StipendAmountAuthorizedTotal,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
				AND CSP.ProvinceID = @ProvinceID
				 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
				 ) D
		GROUP BY D.StipendName, D.DisplayOrder
		) A
		JOIN
			(
	SELECT 
		SUM(D.StipendAmountPaidTotal) as StipendAmountPaidTotal,
		D.StipendName
		FROM (
			SELECT 
			CASE 
			WHEN CSP.StipendPaidDate IS NULL THEN 0 
			ELSE CSP.StipendAmountPaid
			END as StipendAmountPaidTotal,
			CSP.StipendName

			FROM dbo.ContactStipendPayment CSP
			WHERE CSP.ProvinceID = @ProvinceID
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
			 )D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePayments

--Begin procedure reporting.GetJusticeStipendActivityVariancePeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePeople

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendNameCount AS AuthorizedStipendNameCount,
		B.StipendNameCount AS ReconciledStipendNameCount,
		B.StipendNameCount - A.StipendNameCount AS Variance
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' AND ProvinceID =@ProvinceID 
		GROUP BY CSP.StipendName, S.DisplayOrder
		) A
		JOIN
			(
			SELECT

			SUM(StipendCount)AS StipendNameCount,
			D.StipendName
			FROM
						(SELECT 
							CASE
							WHEN CSP.StipendPaidDate IS NULL THEN  0
							ELSE COUNT(	CSP.StipendName)
							END as StipendCount,
							CSP.StipendName
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.ProvinceID = @ProvinceID
						 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend'  
						GROUP BY CSP.StipendName,csp.StipendPaidDate) D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePeople