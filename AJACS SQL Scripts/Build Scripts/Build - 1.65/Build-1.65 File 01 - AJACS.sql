-- File Name:	Build-1.65 File 01 - AJACS.sql
-- Build Key:	Build-1.65 File 01 - AJACS - 2016.08.30 13.13.48

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatSpotReportReferenceCode
--		procurement.GetLastEquipmentAuditDate
--		procurement.GetLastEquipmentAuditOutcome
--
-- Procedures:
--		reporting.GetCommunityRoundList
--		reporting.GetJusticeStipendActivitySummaryPayments
--		reporting.GetJusticeStipendActivitySummaryPeople
--		reporting.GetJusticeStipendActivityVariancePayments
--		reporting.GetJusticeStipendActivityVariancePeople
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table procurement.DistributedInventory
DECLARE @TableName VARCHAR(250) = 'procurement.DistributedInventory'

EXEC utility.AddColumn @TableName, 'TransferredFromDistributedInventoryID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'TransferredFromDistributedInventoryID', 'INT', 0
GO
--End table procurement.DistributedInventory
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-SR-' + RIGHT('0000' + CAST(@SpotReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function procurement.GetLastEquipmentAuditDate
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditDate'
GO

-- =================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last auditdate for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
--
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	Refactored to work with the procurement.DistributedInventoryAudit table
--
-- Author:			Todd Pires
-- Create date:	2016.08.24
-- Description:	Excluded Transfer records
-- =================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditDate
(
@DistributedInventoryID INT
)

RETURNS DATE

AS
BEGIN

	DECLARE @dLastAuditDate DATE
	
	SELECT TOP 1 @dLastAuditDate = DIA.AuditDate
	FROM procurement.DistributedInventoryAudit DIA
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = DIA.AuditOutcomeID
			AND (AO.AuditOutcomeCode IS NULL OR AO.AuditOutcomeCode <> 'Transfer')
			AND DIA.DistributedInventoryID = @DistributedInventoryID
	ORDER BY DIA.DistributedInventoryAuditID DESC
	
	RETURN @dLastAuditDate

END
GO
--End function procurement.GetLastEquipmentAuditDate

--Begin function procurement.GetLastEquipmentAuditOutcome
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditOutcome'
GO

-- ====================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last Audit Outcome for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
--
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	Refactored to work with the procurement.DistributedInventoryAudit table
--
-- Author:			Todd Pires
-- Create date:	2016.08.24
-- Description:	Excluded Transfer records
-- ====================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditOutcome
(
@DistributedInventoryID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cLastAuditOutcomeName VARCHAR(50)
	
	SELECT TOP 1 @cLastAuditOutcomeName = AO.AuditOutcomeName
	FROM procurement.DistributedInventoryAudit DIA
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = DIA.AuditOutcomeID
			AND (AO.AuditOutcomeCode IS NULL OR AO.AuditOutcomeCode <> 'Transfer')
			AND DIA.DistributedInventoryID = @DistributedInventoryID
	ORDER BY DIA.AuditDate DESC

	IF @cLastAuditOutcomeName IS NULL
		BEGIN
		
		SELECT @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM dropdown.AuditOutcome AO
		WHERE AO.AuditOutcomeID = 0

		END
	--ENDIF
	
	RETURN @cLastAuditOutcomeName

END
GO
--End function procurement.GetLastEquipmentAuditOutcome


--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure reporting.GetCommunityRoundList
EXEC utility.DropObject 'reporting.GetCommunityRoundList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:		Justin Branum
-- Create date:	2016.05.29
-- Description:	A stored procedure to get data from the dbo.CommunityRound table
-- =============================================================================

CREATE PROCEDURE reporting.GetCommunityRoundList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.communityName,
		C.Population,
		CES.CommunityEngagementStatusName,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.CommunityCode,
		CR.CommunityRoundID,
		CR.CommunityRoundRoundID,
		CR.FemaleQuestionnairCount,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficer,
		FOT.FieldOfficerHiredTypeName,
		CR.IsFieldOfficerIntroToFSPandLAC AS IsFieldOfficerIntroToFSPandLA,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.NewCommunityEngagementCluster,
		CR.PostAssessment,
		CR.PreAssessment,
		CR.QuestionnairCount,
		CR.SCAMeetingCount,
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRES.CommunityRoundEngagementStatusName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusNam,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		case when CR.CSAPFinalizedDate is null then 'No' else 'Yes' end as 'FinalizedCSAP', 
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted,
		case when CR.SCAFinalizedDate is null then 'No' else 'Yes' end as 'FinalizedSCA',
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		ID.ImpactDecisionName,
		P.provinceName,
		PPC.PolicePresenceCategoryName,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
			AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Male' 
		) AS CSWGMaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Female' 
		) AS CSWGFemaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.USVettingExpirationDate > getdate()
		) AS CSWGRAMActiveCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.UKVettingExpirationDate  > getdate()
		) AS CSWGUKActiveCount,
		dbo.FormatDate(
		(
		SELECT MAX(UpdateDate)
		FROM dbo.CommunityRoundUpdate 
		WHERE CommunityRoundID = CR.CommunityRoundID
		)) AS CurrentUpdateDateFormatted,
		(
		SELECT Top 1 
		FOUpdates +' <br>' + POUpdates+' <br>' + NextStepsUpdates +' <br>' +NextStepsProcess +' <br>' + Risks
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateData,
			dbo.FormatDate(
			(
				SELECT MAX(UpdateDate)
				FROM dbo.CommunityRoundUpdate 
				WHERE CommunityRoundID = CR.CommunityRoundID
				AND UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
			)) AS LastWeekUpdateDateFormatted
			,(SELECT Top 1 
				FOUpdates +' <br>' + POUpdates+' <br>' + NextStepsUpdates+' <br>' +NextStepsProcess+' <br>' + Risks
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateData,
		(
		SELECT Top 1 
		FOUpdates
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateFOUpdates,
		(
		SELECT Top 1 
		POUpdates
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdatePOUpdates,
		(
		SELECT Top 1 
		NextStepsUpdates +' <br>' +NextStepsProcess
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateNextStepsUpdates,
		(
		SELECT Top 1 
		Risks
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateRisk,
		(SELECT Top 1 
				FOUpdates
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateFOUpdates,
		(SELECT Top 1 
				POUpdates
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdatePOUpdates,
		(SELECT Top 1 
				NextStepsUpdates +' <br>' +NextStepsProcess
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateNextSteps,
		(SELECT Top 1 
				Risks
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateRisks
	FROM dbo.CommunityRound CR
		JOIN reporting.SearchResult RSR ON RSR.EntityID = CR.communityroundID
				AND RSR.EntityTypeCode = 'CommunityRound'
				AND RSR.PersonID = @PersonID
		JOIN dbo.community C ON C.communityID = CR.communityID
		JOIN dbo.province P ON P.provinceID = C.provinceID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityRoundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID	
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
		JOIN dropdown.FieldOfficerHiredType FOT ON CR.FieldOfficerHiredTypeID = FOT.FieldOfficerHiredTypeID

END
GO
--End procedure reporting.GetCommunityRoundList

--Begin procedure reporting.GetJusticeStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPayments

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UPPER(LEFT(DateName( month , DateAdd( month , D.PaymentMonth , -1 ) ),3)) + ' - ' + CAST(D.PaymentYear as varchar(5)) AS YearMonthFormatted,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		SUM(D.StipendAmountPaid) AS StipendAmountPaid,
		SUM(D.StipendAmountAuthorized) AS StipendAmountAuthorized,
		(
		SELECT SUM(ExpenseAmountAuthorized)  
		FROM dbo.CommunityAssetUnitExpense CAUE 
			JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = CAUE.CommunityAssetUnitID
			JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = CAU.CommunityAssetID 
				AND CAUE.ProvinceID = D.ProvinceID 
				AND CAUE.PaymentYear = D.PaymentYear 
				AND CAUE.PaymentMonth = D.PaymentMonth
		) AS StipendExpenseAmount
	FROM
		(
		SELECT
			CSP.StipendAmountPaid,
			CSP.StipendAmountAuthorized,
			CSP.ContactID,
			CSP.ProvinceID,
			CSP.PaymentYear,
			CSP.PaymentMonth,
		
			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			CAST(CSP.PAYMENTYEAR AS VARCHAR(50)) + CAST(CSP.PAYMENTMONTH AS VARCHAR(50)) AS YEARMONTH,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID 
			AND StipendName IN ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') 
			AND StipendTypeCode ='JusticeStipend' 
		) D
	GROUP BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.provinceID
	ORDER BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.ProvinceID ASC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPayments

--Begin procedure reporting.GetJusticeStipendActivitySummaryPeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPeople

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(PVT.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(PVT.YearMonth, 4) AS YearMonthFormatted,
		dbo.GetProvinceNameByProvinceID(@ProvinceID) AS ProvinceName,
		*
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			CAST(CSP.PaymentYear AS VARCHAR(50)) + CAST(CSP.PaymentMonth AS VARCHAR(50)) AS YearMonth,
			CSP.PaymentYear,
			CSP.PaymentMonth
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
			AND StipendName IN ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') 
			AND StipendTypeCode = 'JusticeStipend' 
			AND ProvinceID = @ProvinceID
		GROUP BY CSP.PaymentYear, CSP.PaymentMonth, CSP.StipendName
		) AS D
	PIVOT
		(
		MAX(D.StipendNameCount)
		FOR D.StipendName IN
			(
			[Rank 1],
			[Rank 2],
			[Rank 3],
			[Rank 4],
			[Rank 5]
			)
		) AS PVT
	ORDER BY PVT.YearMonth DESC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPeople

--Begin procedure reporting.GetJusticeStipendActivityVariancePayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePayments

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendAmountAuthorizedTotal,
		B.StipendAmountPaidTotal,
		   B.StipendAmountPaidTotal - A.StipendAmountAuthorizedTotal  AS Variance
	FROM
		(
			SELECT 
		SUM(D.StipendAmountAuthorizedTotal) as StipendAmountAuthorizedTotal,
		D.StipendName,
		D.DisplayOrder
		FROM (
		SELECT 
			CSP.StipendAmountAuthorized,
			CASE 
			WHEN CSP.StipendAuthorizedDate IS NULL THEN 0 
			ELSE CSP.StipendAmountAuthorized
			END as StipendAmountAuthorizedTotal,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
				AND CSP.ProvinceID = @ProvinceID
				 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
				 ) D
		GROUP BY D.StipendName, D.DisplayOrder
		) A
		JOIN
			(
	SELECT 
		SUM(D.StipendAmountPaidTotal) as StipendAmountPaidTotal,
		D.StipendName
		FROM (
			SELECT 
			CASE 
			WHEN CSP.StipendPaidDate IS NULL THEN 0 
			ELSE CSP.StipendAmountPaid
			END as StipendAmountPaidTotal,
			CSP.StipendName

			FROM dbo.ContactStipendPayment CSP
			WHERE CSP.ProvinceID = @ProvinceID
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' 
			 )D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePayments

--Begin procedure reporting.GetJusticeStipendActivityVariancePeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivityVariancePeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivityVariancePeople

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendNameCount AS AuthorizedStipendNameCount,
		B.StipendNameCount AS ReconciledStipendNameCount,
		B.StipendNameCount - A.StipendNameCount AS Variance
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
			 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend' AND ProvinceID =@ProvinceID 
		GROUP BY CSP.StipendName, S.DisplayOrder
		) A
		JOIN
			(
			SELECT

			SUM(StipendCount)AS StipendNameCount,
			D.StipendName
			FROM
						(SELECT 
							CASE
							WHEN CSP.StipendPaidDate IS NULL THEN  0
							ELSE COUNT(	CSP.StipendName)
							END as StipendCount,
							CSP.StipendName
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.ProvinceID = @ProvinceID
						 AND CSP.StipendName in ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') AND CSP.StipendTypeCode ='JusticeStipend'  
						GROUP BY CSP.StipendName,csp.StipendPaidDate) D
			GROUP BY D.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetJusticeStipendActivityVariancePeople
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

DELETE ETF
FROM dbo.EmailTemplateField ETF
WHERE ETF.PlaceHolderText LIKE '%SubmitterSubmitter%'
GO
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Community', 'Community', 0
GO
EXEC utility.SavePermissionableGroup 'CommunityRound', 'CommunityRound', 0
GO
EXEC utility.SavePermissionableGroup 'CommunityRoundActivity', 'CommunityRoundActivity', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contact', 0
GO
EXEC utility.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'Implementation', 'Implementation', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Insight & Understanding', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Province', 'Province', 0
GO
EXEC utility.SavePermissionableGroup 'RapidAssessments', 'Rapid Assessments', 0
GO
EXEC utility.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflows', 'Workflows', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Data Export', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'DataImport', 'Default', NULL, 'DataImport.Default', 'Data Import', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'AddUpdate', NULL, 'EmailTemplateAdministration.AddUpdate', 'Add / edit an email template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'List EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'View', NULL, 'EmailTemplateAdministration.View', 'View EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'List EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Exports', 'BusinessLicenseReport', NULL, 'Exports.BusinessLicenseReport', 'Business License Report Exports', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'CanRecieveEmail', 'CanRecieveEmail', 'Main.CanRecieveEmail.CanRecieveEmail', 'User Can Receive Email', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors SiteConfiguration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'Delete', NULL, 'Permissionable.Delete', 'Delete a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'List PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'Delete', NULL, 'Person.Delete', 'Delete a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'ViewPermissionables', NULL, 'Person.ViewPermissionables', 'View list of permissionables on view page', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'List the server setup keys', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Community', 'AddUpdate', NULL, 'Community.AddUpdate', 'Add / edit a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'List Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'View the analysis tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Export', 'Community.View.Export', 'Export Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'ExportEquipmentDistribution', 'Community.View.ExportEquipmentDistribution', 'Export Equipment Distributions Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'Implementation Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'View the information tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'AddUpdate', NULL, 'CommunityRound.AddUpdate', 'Add / edit a community round', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'List', NULL, 'CommunityRound.List', 'View the list of community rounds', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'View', NULL, 'CommunityRound.View', 'View a community round', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'AddUpdate', NULL, 'CommunityRoundActivity.AddUpdate', 'CommunityRoundActivity.AddUpdate', 0, 0, 'CommunityRoundActivity'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'List', NULL, 'CommunityRoundActivity.List', 'CommunityRoundActivity.List', 0, 0, 'CommunityRoundActivity'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'View', NULL, 'CommunityRoundActivity.View', 'CommunityRoundActivity.View', 0, 0, 'CommunityRoundActivity'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'CETeam', 'Contact.AddUpdate.CETeam', 'Add / edit contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'FieldStaff', 'Contact.AddUpdate.FieldStaff', 'Add / edit contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'IO4', 'Contact.AddUpdate.IO4', 'Add / edit contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeOther', 'Contact.AddUpdate.JusticeOther', 'Add / edit contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeStipend', 'Contact.AddUpdate.JusticeStipend', 'Add / edit contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersStakeholder', 'Contact.AddUpdate.PartnersStakeholder', 'Add / edit contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersSupplierVendor', 'Contact.AddUpdate.PartnersSupplierVendor', 'Add / edit contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceOther', 'Contact.AddUpdate.PoliceOther', 'Add / edit contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceStipend', 'Contact.AddUpdate.PoliceStipend', 'Add / edit contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'ProjectStaff', 'Contact.AddUpdate.ProjectStaff', 'Add / edit contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'SubContractors', 'Contact.AddUpdate.SubContractors', 'Add / edit contacts of type Sub-Contractor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'ImportPayees', NULL, 'Contact.ImportPayees', 'Allows import of payees in payment system', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', NULL, 'Contact.JusticePaymentList', 'Allows view of justice stipends payments', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'CETeam', 'Contact.List.CETeam', 'Include contacts of type CE Team in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ExportPayees', 'Contact.List.ExportPayees', 'Export payees from the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'FieldStaff', 'Contact.List.FieldStaff', 'Include contacts of type Field Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'IO4', 'Contact.List.IO4', 'Include contacts of type IO4 in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeOther', 'Contact.List.JusticeOther', 'Include contacts of type Justice Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeStipend', 'Contact.List.JusticeStipend', 'Include contacts of type Justice Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersStakeholder', 'Contact.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersSupplierVendor', 'Contact.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceOther', 'Contact.List.PoliceOther', 'Include contacts of type Police Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceStipend', 'Contact.List.PoliceStipend', 'Include contacts of type Police Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ProjectStaff', 'Contact.List.ProjectStaff', 'Include contacts of type Project Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'SubContractors', 'Contact.List.SubContractors', 'Include contacts of type Sub-Contractor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', NULL, 'Contact.PaymentList', 'View the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'CashHandOverExport', 'Contact.PaymentList.CashHandOverExport', 'Export the cash handover report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'OpFundsReport', 'Contact.PaymentList.OpFundsReport', 'Export the op funds report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendActivity', 'Contact.PaymentList.StipendActivity', 'Export the stipend activity report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendPaymentReport', 'Contact.PaymentList.StipendPaymentReport', 'Export the stipend payment report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'CETeam', 'Contact.View.CETeam', 'View contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'FieldStaff', 'Contact.View.FieldStaff', 'View contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'IO4', 'Contact.View.IO4', 'View contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeOther', 'Contact.View.JusticeOther', 'View contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeStipend', 'Contact.View.JusticeStipend', 'View contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersStakeholder', 'Contact.View.PartnersStakeholder', 'View contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersSupplierVendor', 'Contact.View.PartnersSupplierVendor', 'View contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceOther', 'Contact.View.PoliceOther', 'View contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceStipend', 'Contact.View.PoliceStipend', 'View contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'ProjectStaff', 'Contact.View.ProjectStaff', 'View contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'SubContractors', 'Contact.View.SubContractors', 'View contacts of type Sub-Contractors', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'VettingMoreInfo', 'Contact.View.VettingMoreInfo', 'View the more info button on the vetting history data table', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'CETeam', 'Vetting.List.CETeam', 'Include contacts of type CE Team in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'Export', 'Vetting.List.Export', 'Export the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'FieldStaff', 'Vetting.List.FieldStaff', 'Include contacts of type Field Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'IO4', 'Vetting.List.IO4', 'Include contacts of type IO4 in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeOther', 'Vetting.List.JusticeOther', 'Include contacts of type Justice Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeStipend', 'Vetting.List.JusticeStipend', 'Include contacts of type Justice Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersStakeholder', 'Vetting.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersSupplierVendor', 'Vetting.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceOther', 'Vetting.List.PoliceOther', 'Include contacts of type Police Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceStipend', 'Vetting.List.PoliceStipend', 'Include contacts of type Police Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'ProjectStaff', 'Vetting.List.ProjectStaff', 'Include contacts of type Project Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'SubContractors', 'Vetting.List.SubContractors', 'Include contacts of type Sub-Contractor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeChangeNotification', 'Vetting.List.VettingOutcomeChangeNotification', 'Recieve an email when a vetting outcome has changed for one or more contacts', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeConsider', 'Vetting.List.VettingOutcomeConsider', 'Assign a vetting outcome of "Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeDoNotConsider', 'Vetting.List.VettingOutcomeDoNotConsider', 'Assign a vetting outcome of "Do Not Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeInsufficientData', 'Vetting.List.VettingOutcomeInsufficientData', 'Assign a vetting outcome of "Insufficient Data"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeNotVetted', 'Vetting.List.VettingOutcomeNotVetted', 'Assign a vetting outcome of "Not Vetted"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomePendingInternalReview', 'Vetting.List.VettingOutcomePendingInternalReview', 'Assign a vetting outcome of "Pending Internal Review"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeSubmittedforVetting', 'Vetting.List.VettingOutcomeSubmittedforVetting', 'Assign a vetting outcome of "Submitted for Vetting"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUK', 'Vetting.List.VettingTypeUK', 'Update UK vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUS', 'Vetting.List.VettingTypeUS', 'Update US vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'Notification', 'ExpirationCountEmail', 'Vetting.Notification.ExpirationCountEmail', 'Recieve the monthly vetting expiration counts e-mail', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', NULL, 'Document.AddUpdate', '', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '004', 'Document.AddUpdate.004', 'Add / edit 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '100', 'Document.AddUpdate.100', 'Add / edit 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '101', 'Document.AddUpdate.101', 'Add / edit 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '102', 'Document.AddUpdate.102', 'Add / edit 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '103', 'Document.AddUpdate.103', 'Add / edit 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '104', 'Document.AddUpdate.104', 'Add / edit 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '105', 'Document.AddUpdate.105', 'Add / edit 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '106', 'Document.AddUpdate.106', 'Add / edit 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '107', 'Document.AddUpdate.107', 'Add / edit 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '108', 'Document.AddUpdate.108', 'Add / edit 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '109', 'Document.AddUpdate.109', 'Add / edit 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '301', 'Document.AddUpdate.301', 'Add / edit 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '302', 'Document.AddUpdate.302', 'Add / edit 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '303', 'Document.AddUpdate.303', 'Add / edit 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '304', 'Document.AddUpdate.304', 'Add / edit 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '306', 'Document.AddUpdate.306', 'Add / edit 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '500', 'Document.AddUpdate.500', 'Add / edit 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '501', 'Document.AddUpdate.501', 'Add / edit 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '502', 'Document.AddUpdate.502', 'Add / edit 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '503', 'Document.AddUpdate.503', 'Add / edit 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '504', 'Document.AddUpdate.504', 'Add / edit 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '505', 'Document.AddUpdate.505', 'Add / edit 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '506', 'Document.AddUpdate.506', 'Add / edit 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '507', 'Document.AddUpdate.507', 'Add / edit 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '508', 'Document.AddUpdate.508', 'Add / edit 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '509', 'Document.AddUpdate.509', 'Add / edit 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '510', 'Document.AddUpdate.510', 'Add / edit 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '511', 'Document.AddUpdate.511', 'Add / edit 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '512', 'Document.AddUpdate.512', 'Add / edit 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '513', 'Document.AddUpdate.513', 'Add / edit 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '514', 'Document.AddUpdate.514', 'Add / edit 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '515', 'Document.AddUpdate.515', 'Add / edit 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '516', 'Document.AddUpdate.516', 'Add / edit 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '517', 'Document.AddUpdate.517', 'Add / edit 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '518', 'Document.AddUpdate.518', 'Add / edit 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '519', 'Document.AddUpdate.519', 'Add / edit 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '520', 'Document.AddUpdate.520', 'Add / edit 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '521', 'Document.AddUpdate.521', 'Add / edit 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '600', 'Document.AddUpdate.600', 'Add / edit 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '601', 'Document.AddUpdate.601', 'Add / edit 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '602', 'Document.AddUpdate.602', 'Add / edit 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '603', 'Document.AddUpdate.603', 'Add / edit 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '604', 'Document.AddUpdate.604', 'Add / edit 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '605', 'Document.AddUpdate.605', 'Add / edit 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '700', 'Document.AddUpdate.700', 'Add / edit 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '701', 'Document.AddUpdate.701', 'Add / edit 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '702', 'Document.AddUpdate.702', 'Add / edit 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '801', 'Document.AddUpdate.801', 'Add / edit 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '802', 'Document.AddUpdate.802', 'Add / edit 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '803', 'Document.AddUpdate.803', 'Add / edit 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '804', 'Document.AddUpdate.804', 'Add / edit 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '805', 'Document.AddUpdate.805', 'Add / edit 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '806', 'Document.AddUpdate.806', 'Add / edit 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '807', 'Document.AddUpdate.807', 'Add / edit 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '808', 'Document.AddUpdate.808', 'Add / edit 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '900', 'Document.AddUpdate.900', 'Add / edit 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '901', 'Document.AddUpdate.901', 'Add / edit 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '902', 'Document.AddUpdate.902', 'Add / edit 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '903', 'Document.AddUpdate.903', 'Add / edit 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '904', 'Document.AddUpdate.904', 'Add / edit 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '905', 'Document.AddUpdate.905', 'Add / edit 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '906', 'Document.AddUpdate.906', 'Add / edit 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '907', 'Document.AddUpdate.907', 'Add / edit 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '908', 'Document.AddUpdate.908', 'Add / edit 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '909', 'Document.AddUpdate.909', 'Add / edit 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '910', 'Document.AddUpdate.910', 'Add / edit 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '004', 'Document.View.004', 'View 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '100', 'Document.View.100', 'View 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '101', 'Document.View.101', 'View 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '102', 'Document.View.102', 'View 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '103', 'Document.View.103', 'View 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '104', 'Document.View.104', 'View 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '105', 'Document.View.105', 'View 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '106', 'Document.View.106', 'View 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '107', 'Document.View.107', 'View 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '108', 'Document.View.108', 'View 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '109', 'Document.View.109', 'View 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '301', 'Document.View.301', 'View 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '302', 'Document.View.302', 'View 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '303', 'Document.View.303', 'View 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '304', 'Document.View.304', 'View 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '306', 'Document.View.306', 'View 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '500', 'Document.View.500', 'View 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '501', 'Document.View.501', 'View 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '502', 'Document.View.502', 'View 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '503', 'Document.View.503', 'View 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '504', 'Document.View.504', 'View 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '505', 'Document.View.505', 'View 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '506', 'Document.View.506', 'View 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '507', 'Document.View.507', 'View 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '508', 'Document.View.508', 'View 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '509', 'Document.View.509', 'View 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '510', 'Document.View.510', 'View 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '511', 'Document.View.511', 'View 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '512', 'Document.View.512', 'View 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '513', 'Document.View.513', 'View 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '514', 'Document.View.514', 'View 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '515', 'Document.View.515', 'View 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '516', 'Document.View.516', 'View 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '517', 'Document.View.517', 'View 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '518', 'Document.View.518', 'View 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '519', 'Document.View.519', 'View 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '520', 'Document.View.520', 'View 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '521', 'Document.View.521', 'View 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '600', 'Document.View.600', 'View 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '601', 'Document.View.601', 'View 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '602', 'Document.View.602', 'View 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '603', 'Document.View.603', 'View 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '604', 'Document.View.604', 'View 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '605', 'Document.View.605', 'View 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '700', 'Document.View.700', 'View 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '701', 'Document.View.701', 'View 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '702', 'Document.View.702', 'View 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '801', 'Document.View.801', 'View 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '802', 'Document.View.802', 'View 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '803', 'Document.View.803', 'View 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '804', 'Document.View.804', 'View 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '805', 'Document.View.805', 'View 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '806', 'Document.View.806', 'View 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '807', 'Document.View.807', 'View 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '808', 'Document.View.808', 'View 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '900', 'Document.View.900', 'View 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '901', 'Document.View.901', 'View 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '902', 'Document.View.902', 'View 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '903', 'Document.View.903', 'View 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '904', 'Document.View.904', 'View 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '905', 'Document.View.905', 'View 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '906', 'Document.View.906', 'View 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '907', 'Document.View.907', 'View 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '908', 'Document.View.908', 'View 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '909', 'Document.View.909', 'View 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '910', 'Document.View.910', 'View 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateDecision', NULL, 'DonorDecision.AddUpdateDecision', 'Add / edit a donor decision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateMeeting', NULL, 'DonorDecision.AddUpdateMeeting', 'Add / edit donor meetings & actions', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'List', NULL, 'DonorDecision.List', 'List Donor Decisions, Meetings & Actions DonorDecision', 1, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'View', NULL, 'DonorDecision.View', 'View DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'AddUpdate', NULL, 'EquipmentCatalog.AddUpdate', 'Add / edit the equipment catalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'List', NULL, 'EquipmentCatalog.List', 'List EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'View', NULL, 'EquipmentCatalog.View', 'View EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Audit', NULL, 'EquipmentDistribution.Audit', 'EquipmentDistribution.Audit', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'EquipmentDistribution.Create', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'FinalizeEquipmentDistribution', NULL, 'EquipmentDistribution.FinalizeEquipmentDistribution', 'EquipmentDistribution.FinalizeEquipmentDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistributedInventory', NULL, 'EquipmentDistribution.ListDistributedInventory', 'EquipmentDistribution.ListDistributedInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistribution', NULL, 'EquipmentDistribution.ListDistribution', 'EquipmentDistribution.ListDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListInventory', NULL, 'EquipmentDistribution.ListInventory', 'EquipmentDistribution.ListInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'SetDeliveryDate', NULL, 'EquipmentDistribution.SetDeliveryDate', 'EquipmentDistribution.SetDeliveryDate', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Transfer', NULL, 'EquipmentDistribution.Transfer', 'EquipmentDistribution.Transfer', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit the equipment inventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'List EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', 'Export', 'EquipmentInventory.List.Export', 'Export EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'Audit', NULL, 'EquipmentManagement.Audit', 'Audit Equipment EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'List', NULL, 'EquipmentManagement.List', 'List Equipment Locations EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'AddUpdate', NULL, 'CommunityAsset.AddUpdate', 'Add / edit a community asset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'List', NULL, 'CommunityAsset.List', 'List CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'View', NULL, 'CommunityAsset.View', 'View CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'AddUpdate', NULL, 'CommunityMemberSurvey.AddUpdate', 'Add / edit a community member survey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'View', NULL, 'CommunityMemberSurvey.View', 'View CommunityMemberSurvey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagement', 'AddUpdate', NULL, 'CommunityProvinceEngagement.AddUpdate', 'Add / edit a community province engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagementUpdate', 'Export', NULL, 'CommunityProvinceEngagementUpdate.Export', 'Export CommunityProvinceEngagementUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'FIFUpdate', 'View', NULL, 'FIFUpdate.View', 'View FIFUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Justice', 'AddUpdate', NULL, 'Justice.AddUpdate', 'Add / edit a justice report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'AddUpdate', NULL, 'KeyEvent.AddUpdate', 'Add / edit a key event', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'List', NULL, 'KeyEvent.List', 'List KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'View', NULL, 'KeyEvent.View', 'View KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'PoliceEngagement', 'AddUpdate', NULL, 'PoliceEngagement.AddUpdate', 'Add / edit a police engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'AddUpdate', NULL, 'Project.AddUpdate', 'Add / edit a project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'List', NULL, 'Project.List', 'List Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'View', NULL, 'Project.View', 'View Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'AddUpdate', NULL, 'Atmospheric.AddUpdate', 'Add / edit an atmospheric report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'List Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'AddUpdate', NULL, 'Finding.AddUpdate', 'Add / edit a finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'List Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'AddUpdate', NULL, 'Force.AddUpdate', 'Add / edit a force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'List Forces Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'AddUpdate', NULL, 'Incident.AddUpdate', 'Add / edit an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'List Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'AddUpdate', NULL, 'Recommendation.AddUpdate', 'Add / edit a recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'List Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', 'Export', 'Recommendation.List.Export', 'Export Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Add', NULL, 'RequestForInformation.Add', 'Add a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'List RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', 'Export', 'RequestForInformation.List.Export', 'Export RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', NULL, 'RequestForInformation.Update', 'Edit a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', 'Amend', 'RequestForInformation.Update.Amend', 'Amend a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', NULL, 'Risk.AddUpdate', 'Add / edit a risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'List Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', 'Export', 'Risk.List.Export', 'Export Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', NULL, 'SpotReport.AddUpdate', 'Add / edit a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', 'Amend', 'SpotReport.AddUpdate.Amend', 'Amend a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'List SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Approved', 'SpotReport.View.Approved', 'Approved SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Export', 'SpotReport.View.Export', 'Export SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'InWork', 'SpotReport.View.InWork', 'In Work SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Add / edit a survey question', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Add / edit a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Administer a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Export Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListQuestions', NULL, 'SurveyManagement.ListQuestions', 'Questions List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveyResponses', NULL, 'SurveyManagement.ListSurveyResponses', 'List Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Surveys List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'View Questions SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'View Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', 'Export', 'WeeklyReport.AddUpdate.Export', 'Export the weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'View', NULL, 'WeeklyReport.View', 'View the completed weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagement', 'Export', NULL, 'CommunityProvinceEngagement.Export', 'Export the engagement report', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'AddUpdate', NULL, 'Indicator.AddUpdate', 'Add / edit an indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'List', NULL, 'Indicator.List', 'List Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'View', NULL, 'Indicator.View', 'View Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'AddUpdate', NULL, 'IndicatorType.AddUpdate', 'Add / edit an indicatortype', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'List', NULL, 'IndicatorType.List', 'List IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'View', NULL, 'IndicatorType.View', 'View IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'AddUpdate', NULL, 'Milestone.AddUpdate', 'Add / edit a milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'List', NULL, 'Milestone.List', 'List Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'View', NULL, 'Milestone.View', 'View Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'AddUpdate', NULL, 'Objective.AddUpdate', 'Add / edit an objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', NULL, 'Objective.ChartList', 'M & E Overview Charts Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'List', NULL, 'Objective.List', 'List Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage Objectives & Indicators Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'Overview Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'View', NULL, 'Objective.View', 'View Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', NULL, 'ConceptNote.AddUpdate', 'Add / edit a concep nNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'ExportConceptNoteBudget', 'ConceptNote.AddUpdate.ExportConceptNoteBudget', 'Export Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'Finances', 'ConceptNote.AddUpdate.Finances', 'Add / edit activity finances', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', NULL, 'ConceptNote.List', 'List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', 'Export', 'ConceptNote.List.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', NULL, 'ConceptNote.VettingList', 'Vetting List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', 'ExportVetting', 'ConceptNote.VettingList.ExportVetting', 'Export Vetting ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', NULL, 'ConceptNote.View', 'View ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'Export', 'ConceptNote.View.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'ViewBudget', 'ConceptNote.View.ViewBudget', 'View Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'AddUpdate', NULL, 'ConceptNoteContactEquipment.AddUpdate', 'Add / edit equipment associated with a concept note', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'FinalizeEquipmentDistribution', NULL, 'ConceptNoteContactEquipment.FinalizeEquipmentDistribution', 'Finalize Equipment Distribution ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'List', NULL, 'ConceptNoteContactEquipment.List', 'List ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', NULL, 'ConceptNoteContactEquipment.View', 'View ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', 'Export', 'ConceptNoteContactEquipment.View.Export', 'Export ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'AddUpdate', NULL, 'License.AddUpdate', 'Add / edit a license', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'List', NULL, 'License.List', 'List License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'View', NULL, 'License.View', 'View License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'AddUpdate', NULL, 'LicenseEquipmentCatalog.AddUpdate', 'Add / edit the license equipment catalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', NULL, 'LicenseEquipmentCatalog.List', 'List LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', 'Export', 'LicenseEquipmentCatalog.List.Export', 'Export LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'View', NULL, 'LicenseEquipmentCatalog.View', 'View LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'AddUpdate', NULL, 'PurchaseRequest.AddUpdate', 'Add / edit a purchase request', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'List', NULL, 'PurchaseRequest.List', 'List PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', NULL, 'PurchaseRequest.View', 'View PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', 'Export', 'PurchaseRequest.View.Export', 'Export PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'AddUpdate', NULL, 'ProgramReport.AddUpdate', 'Add / edit a program report', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'List', NULL, 'ProgramReport.List', 'List ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', NULL, 'ProgramReport.View', 'View ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', 'Export', 'ProgramReport.View.Export', 'Export ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'Province', 'AddUpdate', NULL, 'Province.AddUpdate', 'Add / edit a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'List', NULL, 'Province.List', 'List Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', NULL, 'Province.View', 'View Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Analysis', 'Province.View.Analysis', 'View the analysis tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'ExportEquipmentDistribution', 'Province.View.ExportEquipmentDistribution', 'Export Equipment Distributions Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Implementation', 'Province.View.Implementation', 'Implementation Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Information', 'Province.View.Information', 'View the information tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'DailyReport', 'AddUpdate', NULL, 'DailyReport.AddUpdate', 'Add / edit a daily report', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'List', NULL, 'DailyReport.List', 'List DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'View', NULL, 'DailyReport.View', 'View DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'AddUpdate', NULL, 'FocusGroupSurvey.AddUpdate', 'Add / edit a focus group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'View', NULL, 'FocusGroupSurvey.View', 'View FocusGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'AddUpdate', NULL, 'KeyInformantSurvey.AddUpdate', 'Add / edit a key informant survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'View', NULL, 'KeyInformantSurvey.View', 'View KeyInformantSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', NULL, 'RAPData.List', 'List RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', 'Export', 'RAPData.List.Export', 'Export RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RapidPerceptionSurvey', 'View', NULL, 'RapidPerceptionSurvey.View', 'View RapidPerceptionSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'AddUpdate', NULL, 'StakeholderGroupSurvey.AddUpdate', 'Add / edit a stakeholder group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'View', NULL, 'StakeholderGroupSurvey.View', 'View StakeholderGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'AddUpdate', NULL, 'StationCommanderSurvey.AddUpdate', 'Add / edit a station commander survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'View', NULL, 'StationCommanderSurvey.View', 'View StationCommanderSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'AddUpdate', NULL, 'Team.AddUpdate', 'Add / edit a team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'List', NULL, 'Team.List', 'List Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'View', NULL, 'Team.View', 'View Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'SubContractor', 'AddUpdate', NULL, 'SubContractor.AddUpdate', 'Add / edit a sub-contractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'List', NULL, 'SubContractor.List', 'List SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'View', NULL, 'SubContractor.View', 'View SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / edit a class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'List Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / edit a course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'List Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the list of workflows', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflows'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP FROM permissionable.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table permissionable.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.65 File 01 - AJACS - 2016.08.30 13.13.48')
GO
--End build tracking

