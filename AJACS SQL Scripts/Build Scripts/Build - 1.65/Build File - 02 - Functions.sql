USE AJACS
GO

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-SR-' + RIGHT('0000' + CAST(@SpotReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function procurement.GetLastEquipmentAuditDate
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditDate'
GO

-- =================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last auditdate for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
--
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	Refactored to work with the procurement.DistributedInventoryAudit table
--
-- Author:			Todd Pires
-- Create date:	2016.08.24
-- Description:	Excluded Transfer records
-- =================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditDate
(
@DistributedInventoryID INT
)

RETURNS DATE

AS
BEGIN

	DECLARE @dLastAuditDate DATE
	
	SELECT TOP 1 @dLastAuditDate = DIA.AuditDate
	FROM procurement.DistributedInventoryAudit DIA
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = DIA.AuditOutcomeID
			AND (AO.AuditOutcomeCode IS NULL OR AO.AuditOutcomeCode <> 'Transfer')
			AND DIA.DistributedInventoryID = @DistributedInventoryID
	ORDER BY DIA.DistributedInventoryAuditID DESC
	
	RETURN @dLastAuditDate

END
GO
--End function procurement.GetLastEquipmentAuditDate

--Begin function procurement.GetLastEquipmentAuditOutcome
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditOutcome'
GO

-- ====================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last Audit Outcome for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
--
-- Author:			Todd Pires
-- Create date:	2015.03.04
-- Description:	Refactored to work with the procurement.DistributedInventoryAudit table
--
-- Author:			Todd Pires
-- Create date:	2016.08.24
-- Description:	Excluded Transfer records
-- ====================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditOutcome
(
@DistributedInventoryID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cLastAuditOutcomeName VARCHAR(50)
	
	SELECT TOP 1 @cLastAuditOutcomeName = AO.AuditOutcomeName
	FROM procurement.DistributedInventoryAudit DIA
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = DIA.AuditOutcomeID
			AND (AO.AuditOutcomeCode IS NULL OR AO.AuditOutcomeCode <> 'Transfer')
			AND DIA.DistributedInventoryID = @DistributedInventoryID
	ORDER BY DIA.AuditDate DESC

	IF @cLastAuditOutcomeName IS NULL
		BEGIN
		
		SELECT @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM dropdown.AuditOutcome AO
		WHERE AO.AuditOutcomeID = 0

		END
	--ENDIF
	
	RETURN @cLastAuditOutcomeName

END
GO
--End function procurement.GetLastEquipmentAuditOutcome

