USE AJACS
GO

--Begin table procurement.DistributedInventory
DECLARE @TableName VARCHAR(250) = 'procurement.DistributedInventory'

EXEC utility.AddColumn @TableName, 'TransferredFromDistributedInventoryID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'TransferredFromDistributedInventoryID', 'INT', 0
GO
--End table procurement.DistributedInventory