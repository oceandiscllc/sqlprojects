USE AJACS
GO

DELETE ETF
FROM dbo.EmailTemplateField ETF
WHERE ETF.PlaceHolderText LIKE '%SubmitterSubmitter%'
GO