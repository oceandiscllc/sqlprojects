-- File Name:	Build-1.21 File 01 - AJACS.sql
-- Build Key:	Build-1.21 File 01 - AJACS - 2015.07.24 15.34.57

USE AJACS
GO

-- ==============================================================================================================================
-- Procedures:
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetRiskByRiskID
--		dropdown.GetLanguageData
--		dropdown.GetRiskCategoryData
--		dropdown.GetRiskStatusData
--		permissionable.HasDocumentLibraryAccess
--		permissionable.HasFileAccess
--		reporting.GetStipendActivitySummaryPayments
--		survey.GetSurveyQuestionBySurveyQuestionID
--
-- Tables:
--		dbo.ConceptNoteRisk
--		dbo.Risk
--		dropdown.Language
--		dropdown.RiskCategory
--		dropdown.RiskStatus
--		survey.SurveyLanguage
--		survey.SurveyQuestionLabel
--		survey.SurveyQuestionLanguage
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.ConceptNoteRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteRisk
	(
	ConceptNoteRiskID INT IDENTITY(1,1),
	ConceptNoteID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteRiskID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteRisk', @TableName, 'ConceptNoteID,RiskID'
GO
--End table dbo.ConceptNoteRisk

--Begin table dbo.Risk
DECLARE @TableName VARCHAR(250) = 'dbo.Risk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Risk
	(
	RiskID INT IDENTITY(1,1),
	RiskName VARCHAR(250),
	RiskDescription VARCHAR(MAX),
	RiskCategoryID INT,
	DateRaised DATE,
	RaisedBy VARCHAR(100),
	Likelihood INT,
	Impact INT,
	Overall INT,
	MitigationActions VARCHAR(MAX),
	EscalationCriteria VARCHAR(MAX),
	Notes VARCHAR(MAX),
	RiskStatusID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'Impact', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Likelihood', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Overall', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskStatusID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskID'
GO
--End table dbo.Risk

--Begin table dropdown.Language
DECLARE @TableName VARCHAR(250) = 'dropdown.Language'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Language
	(
	LanguageID INT IDENTITY(0,1) NOT NULL,
	LanguageCode CHAR(2),
	LanguageName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'LanguageID'
EXEC utility.SetIndexNonClustered 'IX_Language', @TableName, 'DisplayOrder,LanguageName', 'LanguageID'
GO

SET IDENTITY_INSERT dropdown.Language ON
GO

INSERT INTO dropdown.Language (LanguageID,IsActive) VALUES (0,1)

SET IDENTITY_INSERT dropdown.Language OFF
GO

INSERT INTO dropdown.Language 
	(LanguageCode,LanguageName)
VALUES
	('AB','Abkhazian'),
	('AA','Afar'),
	('AF','Afrikaans'),
	('SQ','Albanian'),
	('AM','Amharic'),
	('AR','Arabic'),
	('HY','Armenian'),
	('AS','Assamese'),
	('AY','Aymara'),
	('AZ','Azerbaijani'),
	('BA','Bashkir'),
	('EU','Basque'),
	('BN','Bengali, Bangla'),
	('DZ','Bhutani'),
	('BH','Bihari'),
	('BI','Bislama'),
	('BR','Breton'),
	('BG','Bulgarian'),
	('MY','Burmese'),
	('BE','Byelorussian'),
	('KM','Cambodian'),
	('CA','Catalan'),
	('ZH','Chinese'),
	('CO','Corsican'),
	('HR','Croatian'),
	('CS','Czech'),
	('DA','Danish'),
	('NL','Dutch'),
	('EN','English'),
	('EO','Esperanto'),
	('ET','Estonian'),
	('FO','Faeroese'),
	('FJ','Fiji'),
	('FI','Finnish'),
	('FR','French'),
	('FY','Frisian'),
	('GD','Gaelic'),
	('GL','Galician'),
	('KA','Georgian'),
	('DE','German'),
	('EL','Greek'),
	('KL','Greenlandic'),
	('GN','Guarani'),
	('GU','Gujarati'),
	('HA','Hausa'),
	('IW','Hebrew'),
	('HI','Hindi'),
	('HU','Hungarian'),
	('IS','Icelandic'),
	('IN','Indonesian'),
	('IA','Interlingua'),
	('IE','Interlingue'),
	('IK','Inupiak'),
	('GA','Irish'),
	('IT','Italian'),
	('JA','Japanese'),
	('JW','Javanese'),
	('KN','Kannada'),
	('KS','Kashmiri'),
	('KK','Kazakh'),
	('RW','Kinyarwanda'),
	('KY','Kirghiz'),
	('RN','Kirundi'),
	('KO','Korean'),
	('KU','Kurdish'),
	('LO','Laothian'),
	('LA','Latin'),
	('LV','Latvian, Lettish'),
	('LN','Lingala'),
	('LT','Lithuanian'),
	('MK','Macedonian'),
	('MG','Malagasy'),
	('MS','Malay'),
	('ML','Malayalam'),
	('MT','Maltese'),
	('MI','Maori'),
	('MR','Marathi'),
	('MO','Moldavian'),
	('MN','Mongolian'),
	('NA','Nauru'),
	('NE','Nepali'),
	('NO','Norwegian'),
	('OC','Occitan'),
	('OR','Oriya'),
	('OM','Oromo, Afan'),
	('PS','Pashto, Pushto'),
	('FA','Persian'),
	('PL','Polish'),
	('PT','Portuguese'),
	('PA','Punjabi'),
	('QU','Quechua'),
	('RM','Rhaeto-Romance'),
	('RO','Romanian'),
	('RU','Russian'),
	('SM','Samoan'),
	('SG','Sangro'),
	('SA','Sanskrit'),
	('SR','Serbian'),
	('SH','Serbo-Croatian'),
	('ST','Sesotho'),
	('TN','Setswana'),
	('SN','Shona'),
	('SD','Sindhi'),
	('SI','Singhalese'),
	('SS','Siswati'),
	('SK','Slovak'),
	('SL','Slovenian'),
	('SO','Somali'),
	('ES','Spanish'),
	('SU','Sudanese'),
	('SW','Swahili'),
	('SV','Swedish'),
	('TL','Tagalog'),
	('TG','Tajik'),
	('TA','Tamil'),
	('TT','Tatar'),
	('TE','Tegulu'),
	('TH','Thai'),
	('BO','Tibetan'),
	('TI','Tigrinya'),
	('TO','Tonga'),
	('TS','Tsonga'),
	('TR','Turkish'),
	('TK','Turkmen'),
	('TW','Twi'),
	('UK','Ukrainian'),
	('UR','Urdu'),
	('UZ','Uzbek'),
	('VI','Vietnamese'),
	('VO','Volapuk'),
	('CY','Welsh'),
	('WO','Wolof'),
	('XH','Xhosa'),
	('JI','Yiddish'),
	('YO','Yoruba'),
	('ZU','Zulu')
GO

UPDATE dropdown.Language
SET IsActive = 0
WHERE LanguageCode NOT IN ('AR','EN')
GO
--End table dropdown.Language

--Begin table dropdown.RiskCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.RiskCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RiskCategory
	(
	RiskCategoryID INT IDENTITY(0,1) NOT NULL,
	RiskCategoryName VARCHAR(100),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskCategoryID'
EXEC utility.SetIndexNonClustered 'IX_RiskCategory', @TableName, 'DisplayOrder,RiskCategoryName', 'RiskCategoryID'
GO

SET IDENTITY_INSERT dropdown.RiskCategory ON
GO

INSERT INTO dropdown.RiskCategory (RiskCategoryID) VALUES (0)

SET IDENTITY_INSERT dropdown.RiskCategory OFF
GO

INSERT INTO dropdown.RiskCategory 
	(RiskCategoryName,DisplayOrder)
VALUES
	('Beneficiaries/Community',1),
	('Organizational capacity: overall',2),
	('Organizational capacity: funds transfer from implementer to sub-grantee',3),
	('Organizational capacity: procurement and payments',4),
	('Project implementation',5),
	('Security situation',6),
	('Sustainability',7)
GO
--End table dropdown.RiskCategory

--Begin table dropdown.RiskStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.RiskStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RiskStatus
	(
	RiskStatusID INT IDENTITY(0,1) NOT NULL,
	RiskStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskStatusID'
EXEC utility.SetIndexNonClustered 'IX_RiskStatus', @TableName, 'DisplayOrder,RiskStatusName', 'RiskStatusID'
GO

SET IDENTITY_INSERT dropdown.RiskStatus ON
GO

INSERT INTO dropdown.RiskStatus (RiskStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.RiskStatus OFF
GO

INSERT INTO dropdown.RiskStatus 
	(RiskStatusName,DisplayOrder)
VALUES
	('New',1),
	('Closed',2),
	('Live-Internal',3),
	('Live-External',4),
	('Activity Specific',5)
GO
--End table dropdown.RiskStatus

--Begin table survey.SurveyLanguage
DECLARE @TableName VARCHAR(250) = 'survey.SurveyLanguage'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyLanguage
	(
	SurveyLanguageID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	LanguageCode CHAR(2),
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyLanguageID'
EXEC utility.SetIndexClustered 'IX_SurveyLanguage', @TableName, 'SurveyID,LanguageCode'
GO
--Begin table survey.SurveyLanguage

--Begin table survey.SurveyQuestion
DECLARE @cTableName VARCHAR(250) = 'survey.SurveyQuestion'

EXEC Utility.DropColumn @cTableName, 'DisplayOrder'
EXEC Utility.DropColumn @cTableName, 'SurveyQuestionDescription'
EXEC Utility.DropColumn @cTableName, 'SurveyQuestionText'
GO
--End table survey.SurveyQuestion

--Begin table survey.SurveyQuestionLabel
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionLabel'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionLabel
	(
	SurveyQuestionLabelID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	SurveyQuestionText NVARCHAR(500),
	SurveyQuestionDescription NVARCHAR(MAX),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionLabelID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionLabel', @TableName, 'SurveyQuestionID,LanguageCode,DisplayOrder'
GO
--Begin table survey.SurveyQuestionLabel

--Begin table survey.SurveyQuestionLanguage
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionLanguage'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionLanguage
	(
	SurveyQuestionLanguageID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionLanguageID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionLanguage', @TableName, 'SurveyQuestionID,LanguageCode'
GO
--Begin table survey.SurveyQuestionLanguage

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin dbo.GetRiskByRiskID
EXEC Utility.DropObject 'dbo.GetRiskByRiskID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===========================================================================
CREATE PROCEDURE dbo.GetRiskByRiskID

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName
	FROM dbo.Risk R
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND R.RiskID = @RiskID

END
GO
--End dbo.GetRiskByRiskID

--Begin procedure dropdown.GetLanguageData
EXEC Utility.DropObject 'dropdown.GetLanguageData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.22
-- Description:	A stored procedure to return data from the dropdown.Language table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetLanguageData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.LanguageID,
		T.LanguageCode,
		T.LanguageName
	FROM dropdown.Language T
	WHERE (T.LanguageID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.LanguageName, T.LanguageID

END
GO
--End procedure dropdown.GetLanguageData

--Begin procedure dropdown.GetRiskCategoryData
EXEC Utility.DropObject 'dropdown.GetRiskCategoryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.07.21
-- Description:	A stored procedure to return data from the dropdown.RiskCategory table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetRiskCategoryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RiskCategoryID,
		T.RiskCategoryName
	FROM dropdown.RiskCategory T
	WHERE (T.RiskCategoryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RiskCategoryName, T.RiskCategoryID

END
GO
--End procedure dropdown.GetRiskCategoryData

--Begin procedure dropdown.GetRiskStatusData
EXEC Utility.DropObject 'dropdown.GetRiskStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.07.21
-- Description:	A stored procedure to return data from the dropdown.RiskStatus table
-- =================================================================================
CREATE PROCEDURE dropdown.GetRiskStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RiskStatusID,
		T.RiskStatusName
	FROM dropdown.RiskStatus T
	WHERE (T.RiskStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RiskStatusName, T.RiskStatusID

END
GO
--End procedure dropdown.GetRiskStatusData

--Begin permissionable.HasDocumentLibraryAccess
EXEC Utility.DropObject 'permissionable.HasDocumentLibraryAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.19
-- Description:	A stored procedure to get check access to the document library
-- ===========================================================================
CREATE PROCEDURE permissionable.HasDocumentLibraryAccess

@PersonID INT

AS
BEGIN

	SELECT 1
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		AND PP.PermissionableLineage LIKE 'DocumentType.%'

END
GO
--End procedure permissionable.HasDocumentLibraryAccess

--Begin permissionable.HasFileAccess
EXEC Utility.DropObject 'permissionable.HasFileAccess'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date: 2015.07.18
-- Description:	A stored procedure to get check access to a file
-- =============================================================
CREATE PROCEDURE permissionable.HasFileAccess

@PersonID INT,
@PhysicalFileName VARCHAR(50)

AS
BEGIN

	SELECT 1
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		AND 
			(
			PP.PermissionableLineage = 
				(
				SELECT 'DocumentType.' + ISNULL(DT.DocumentTypePermissionCode, '000') + '.View'
				FROM dbo.Document D
					JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
						AND D.PhysicalFileName = @PhysicalFileName
				)
				OR
					( 
					(SELECT D.DocumentTypeID FROM dbo.Document D WHERE D.PhysicalFileName = @PhysicalFileName) = 0
					)
			)

END
GO
--End procedure permissionable.HasFileAccess

--Begin procedure reporting.GetStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.05
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2015.07.19
-- Description:	Bug fixes
-- =======================================================================================
CREATE PROCEDURE reporting.GetStipendActivitySummaryPayments

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(D.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(D.YearMonth, 4) AS YearMonthFormatted,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		SUM(D.StipendAmountPaid) AS StipendAmountPaid,
		SUM(D.StipendAmountAuthorized) AS StipendAmountAuthorized
	FROM
		(
		SELECT
			CSP.StipendAmountPaid,
			CSP.StipendAmountAuthorized,

			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN CSP.PaymentYear * 100 + CSP.PaymentMonth
				WHEN CSP.StipendPaidDate IS NULL
				THEN YEAR(CSP.StipendAuthorizedDate) * 100 + MONTH(CSP.StipendAuthorizedDate)
				ELSE YEAR(CSP.StipendPaidDate) * 100 + MONTH(CSP.StipendPaidDate)
			END AS YearMonth,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
		) D
	GROUP BY D.YearMonth, D.StipendStatus
	ORDER BY D.YearMonth DESC, D.StipendStatus

END
GO
--End procedure reporting.GetStipendActivitySummaryPayments

--Begin survey.GetSurveyQuestionBySurveyQuestionID
EXEC Utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.13
-- Description:	A stored procedure to get a survey question from the database
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionBySurveyQuestionID

@SurveyQuestionID INT

AS
BEGIN

	SELECT 
		SQ.IsActive,
		SQ.SurveyQuestionID, 
		SQT.SurveyQuestionResponseTypeID, 
		SQT.SurveyQuestionResponseTypeCode, 
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveyQuestion SQ
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SQ.SurveyQuestionID = @SurveyQuestionID

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyQuestionLanguage SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	
	
	SELECT
		SQRC.SurveyQuestionResponseChoiceID, 
		SQRC.SurveyQuestionResponseChoiceText
	FROM survey.SurveyQuestionResponseChoice SQRC
	WHERE SQRC.SurveyQuestionID = @SurveyQuestionID
	ORDER BY SQRC.DisplayOrder, SQRC.SurveyQuestionResponseChoiceText

END
GO
--End procedure survey.GetSurveyQuestionBySurveyQuestionID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'Risk')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('Risk', 'Submit')

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Risk')
	BEGIN
	
	INSERT INTO dbo.EmailTemplateField 
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('Risk','[[Title]]','Title',1),
		('Risk','[[TitleLink]]','Title Link',2)
	
	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Risk')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('Risk', 'Risk')

	END
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'SurveyManagement'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyList', @ParentMenuItemCode='Surveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='QuestionList', @ParentMenuItemCode='Surveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskManagement', @NewMenuItemLink='/risk/list', @NewMenuItemText='Risk Management', @AfterMenuItemCode='Activity', @PermissionableLineageList='Risk.List', @Icon='fa fa-fw fa-exclamation-triangle'
GO
--End table dbo.MenuItem

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Risk')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Risk','Risks', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Risk')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Risk','Risks')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Risk'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Risk'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Risk'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'AdministerSurvey')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AdministerSurvey',
		'Administer Surveys'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.21 File 01 - AJACS - 2015.07.24 15.34.57')
GO
--End build tracking

