USE AJACS
GO

--Begin table dbo.ConceptNoteRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteRisk
	(
	ConceptNoteRiskID INT IDENTITY(1,1),
	ConceptNoteID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteRiskID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteRisk', @TableName, 'ConceptNoteID,RiskID'
GO
--End table dbo.ConceptNoteRisk

--Begin table dbo.Risk
DECLARE @TableName VARCHAR(250) = 'dbo.Risk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Risk
	(
	RiskID INT IDENTITY(1,1),
	RiskName VARCHAR(250),
	RiskDescription VARCHAR(MAX),
	RiskCategoryID INT,
	DateRaised DATE,
	RaisedBy VARCHAR(100),
	Likelihood INT,
	Impact INT,
	Overall INT,
	MitigationActions VARCHAR(MAX),
	EscalationCriteria VARCHAR(MAX),
	Notes VARCHAR(MAX),
	RiskStatusID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'Impact', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Likelihood', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Overall', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskStatusID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskID'
GO
--End table dbo.Risk

--Begin table dropdown.Language
DECLARE @TableName VARCHAR(250) = 'dropdown.Language'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Language
	(
	LanguageID INT IDENTITY(0,1) NOT NULL,
	LanguageCode CHAR(2),
	LanguageName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'LanguageID'
EXEC utility.SetIndexNonClustered 'IX_Language', @TableName, 'DisplayOrder,LanguageName', 'LanguageID'
GO

SET IDENTITY_INSERT dropdown.Language ON
GO

INSERT INTO dropdown.Language (LanguageID,IsActive) VALUES (0,1)

SET IDENTITY_INSERT dropdown.Language OFF
GO

INSERT INTO dropdown.Language 
	(LanguageCode,LanguageName)
VALUES
	('AB','Abkhazian'),
	('AA','Afar'),
	('AF','Afrikaans'),
	('SQ','Albanian'),
	('AM','Amharic'),
	('AR','Arabic'),
	('HY','Armenian'),
	('AS','Assamese'),
	('AY','Aymara'),
	('AZ','Azerbaijani'),
	('BA','Bashkir'),
	('EU','Basque'),
	('BN','Bengali, Bangla'),
	('DZ','Bhutani'),
	('BH','Bihari'),
	('BI','Bislama'),
	('BR','Breton'),
	('BG','Bulgarian'),
	('MY','Burmese'),
	('BE','Byelorussian'),
	('KM','Cambodian'),
	('CA','Catalan'),
	('ZH','Chinese'),
	('CO','Corsican'),
	('HR','Croatian'),
	('CS','Czech'),
	('DA','Danish'),
	('NL','Dutch'),
	('EN','English'),
	('EO','Esperanto'),
	('ET','Estonian'),
	('FO','Faeroese'),
	('FJ','Fiji'),
	('FI','Finnish'),
	('FR','French'),
	('FY','Frisian'),
	('GD','Gaelic'),
	('GL','Galician'),
	('KA','Georgian'),
	('DE','German'),
	('EL','Greek'),
	('KL','Greenlandic'),
	('GN','Guarani'),
	('GU','Gujarati'),
	('HA','Hausa'),
	('IW','Hebrew'),
	('HI','Hindi'),
	('HU','Hungarian'),
	('IS','Icelandic'),
	('IN','Indonesian'),
	('IA','Interlingua'),
	('IE','Interlingue'),
	('IK','Inupiak'),
	('GA','Irish'),
	('IT','Italian'),
	('JA','Japanese'),
	('JW','Javanese'),
	('KN','Kannada'),
	('KS','Kashmiri'),
	('KK','Kazakh'),
	('RW','Kinyarwanda'),
	('KY','Kirghiz'),
	('RN','Kirundi'),
	('KO','Korean'),
	('KU','Kurdish'),
	('LO','Laothian'),
	('LA','Latin'),
	('LV','Latvian, Lettish'),
	('LN','Lingala'),
	('LT','Lithuanian'),
	('MK','Macedonian'),
	('MG','Malagasy'),
	('MS','Malay'),
	('ML','Malayalam'),
	('MT','Maltese'),
	('MI','Maori'),
	('MR','Marathi'),
	('MO','Moldavian'),
	('MN','Mongolian'),
	('NA','Nauru'),
	('NE','Nepali'),
	('NO','Norwegian'),
	('OC','Occitan'),
	('OR','Oriya'),
	('OM','Oromo, Afan'),
	('PS','Pashto, Pushto'),
	('FA','Persian'),
	('PL','Polish'),
	('PT','Portuguese'),
	('PA','Punjabi'),
	('QU','Quechua'),
	('RM','Rhaeto-Romance'),
	('RO','Romanian'),
	('RU','Russian'),
	('SM','Samoan'),
	('SG','Sangro'),
	('SA','Sanskrit'),
	('SR','Serbian'),
	('SH','Serbo-Croatian'),
	('ST','Sesotho'),
	('TN','Setswana'),
	('SN','Shona'),
	('SD','Sindhi'),
	('SI','Singhalese'),
	('SS','Siswati'),
	('SK','Slovak'),
	('SL','Slovenian'),
	('SO','Somali'),
	('ES','Spanish'),
	('SU','Sudanese'),
	('SW','Swahili'),
	('SV','Swedish'),
	('TL','Tagalog'),
	('TG','Tajik'),
	('TA','Tamil'),
	('TT','Tatar'),
	('TE','Tegulu'),
	('TH','Thai'),
	('BO','Tibetan'),
	('TI','Tigrinya'),
	('TO','Tonga'),
	('TS','Tsonga'),
	('TR','Turkish'),
	('TK','Turkmen'),
	('TW','Twi'),
	('UK','Ukrainian'),
	('UR','Urdu'),
	('UZ','Uzbek'),
	('VI','Vietnamese'),
	('VO','Volapuk'),
	('CY','Welsh'),
	('WO','Wolof'),
	('XH','Xhosa'),
	('JI','Yiddish'),
	('YO','Yoruba'),
	('ZU','Zulu')
GO

UPDATE dropdown.Language
SET IsActive = 0
WHERE LanguageCode NOT IN ('AR','EN')
GO
--End table dropdown.Language

--Begin table dropdown.RiskCategory
DECLARE @TableName VARCHAR(250) = 'dropdown.RiskCategory'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RiskCategory
	(
	RiskCategoryID INT IDENTITY(0,1) NOT NULL,
	RiskCategoryName VARCHAR(100),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskCategoryID'
EXEC utility.SetIndexNonClustered 'IX_RiskCategory', @TableName, 'DisplayOrder,RiskCategoryName', 'RiskCategoryID'
GO

SET IDENTITY_INSERT dropdown.RiskCategory ON
GO

INSERT INTO dropdown.RiskCategory (RiskCategoryID) VALUES (0)

SET IDENTITY_INSERT dropdown.RiskCategory OFF
GO

INSERT INTO dropdown.RiskCategory 
	(RiskCategoryName,DisplayOrder)
VALUES
	('Beneficiaries/Community',1),
	('Organizational capacity: overall',2),
	('Organizational capacity: funds transfer from implementer to sub-grantee',3),
	('Organizational capacity: procurement and payments',4),
	('Project implementation',5),
	('Security situation',6),
	('Sustainability',7)
GO
--End table dropdown.RiskCategory

--Begin table dropdown.RiskStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.RiskStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RiskStatus
	(
	RiskStatusID INT IDENTITY(0,1) NOT NULL,
	RiskStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskStatusID'
EXEC utility.SetIndexNonClustered 'IX_RiskStatus', @TableName, 'DisplayOrder,RiskStatusName', 'RiskStatusID'
GO

SET IDENTITY_INSERT dropdown.RiskStatus ON
GO

INSERT INTO dropdown.RiskStatus (RiskStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.RiskStatus OFF
GO

INSERT INTO dropdown.RiskStatus 
	(RiskStatusName,DisplayOrder)
VALUES
	('New',1),
	('Closed',2),
	('Live-Internal',3),
	('Live-External',4),
	('Activity Specific',5)
GO
--End table dropdown.RiskStatus

--Begin table survey.SurveyLanguage
DECLARE @TableName VARCHAR(250) = 'survey.SurveyLanguage'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyLanguage
	(
	SurveyLanguageID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	LanguageCode CHAR(2),
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyLanguageID'
EXEC utility.SetIndexClustered 'IX_SurveyLanguage', @TableName, 'SurveyID,LanguageCode'
GO
--Begin table survey.SurveyLanguage

--Begin table survey.SurveyQuestion
DECLARE @cTableName VARCHAR(250) = 'survey.SurveyQuestion'

EXEC Utility.DropColumn @cTableName, 'DisplayOrder'
EXEC Utility.DropColumn @cTableName, 'SurveyQuestionDescription'
EXEC Utility.DropColumn @cTableName, 'SurveyQuestionText'
GO
--End table survey.SurveyQuestion

--Begin table survey.SurveyQuestionLabel
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionLabel'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionLabel
	(
	SurveyQuestionLabelID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	SurveyQuestionText NVARCHAR(500),
	SurveyQuestionDescription NVARCHAR(MAX),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionLabelID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionLabel', @TableName, 'SurveyQuestionID,LanguageCode,DisplayOrder'
GO
--Begin table survey.SurveyQuestionLabel

--Begin table survey.SurveyQuestionLanguage
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionLanguage'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionLanguage
	(
	SurveyQuestionLanguageID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	LanguageCode CHAR(2),
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionLanguageID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionLanguage', @TableName, 'SurveyQuestionID,LanguageCode'
GO
--Begin table survey.SurveyQuestionLanguage
