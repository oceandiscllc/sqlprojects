USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'Risk')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('Risk', 'Submit')

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Risk')
	BEGIN
	
	INSERT INTO dbo.EmailTemplateField 
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('Risk','[[Title]]','Title',1),
		('Risk','[[TitleLink]]','Title Link',2)
	
	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Risk')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('Risk', 'Risk')

	END
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'SurveyManagement'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyList', @ParentMenuItemCode='Surveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='QuestionList', @ParentMenuItemCode='Surveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskManagement', @NewMenuItemLink='/risk/list', @NewMenuItemText='Risk Management', @AfterMenuItemCode='Activity', @PermissionableLineageList='Risk.List', @Icon='fa fa-fw fa-exclamation-triangle'
GO
--End table dbo.MenuItem

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Risk')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Risk','Risks', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Risk')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Risk','Risks')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Risk'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Risk'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Risk'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'AdministerSurvey')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AdministerSurvey',
		'Administer Surveys'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
