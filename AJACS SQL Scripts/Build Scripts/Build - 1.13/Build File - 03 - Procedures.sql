USE AJACS
GO

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName,
		ISNULL(CNC2.ConceptNoteID, 0) AS ConceptNoteID
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT CNC1.ConceptNoteID 
			FROM dbo.ConceptNoteClass CNC1
			WHERE CNC1.ClassID = @ClassID
			) CNC2
	
	SELECT

		CASE
			WHEN CO.CommunityID > 0
			THEN (SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CO.CommunityID)
			WHEN CO.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CO.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		CO.ContactID,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'Class'
			AND DE.EntityID = @ClassID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.Summary,
		CN.Title,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CourseID,
		C.CourseName
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
-- ==============================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		MI.Icon,
		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		OAWR.PhysicalFileName,
		D.EntityID,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
		JOIN dbo.MenuItem MI ON MI.MenuItemCode = 
			CASE
				WHEN D.EntityTypeCode = 'WeeklyReport'
				THEN 'ManageWeeklyReports'
				ELSE D.EntityTypeCode
			END

			AND OAWR.PhysicalFileName IS NOT NULL
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
	ORDER BY D.UpdateDate DESC, D.EntityTypeCode, D.EntityID
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dropdown.GetEthnicityData
EXEC Utility.DropObject 'dropdown.GetEthnicityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.23
-- Description:	A stored procedure to return data from the dropdown.Ethnicity table
-- ================================================================================
CREATE PROCEDURE dropdown.GetEthnicityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EthnicityID, 
		T.EthnicityName
	FROM dropdown.Ethnicity T
	WHERE (T.EthnicityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EthnicityName, T.EthnicityID

END
GO
--End procedure dropdown.GetEthnicityData
