USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'AwardeeSubContractor1', 'INT'
EXEC utility.AddColumn @TableName, 'AwardeeSubContractor2', 'INT'
EXEC utility.AddColumn @TableName, 'FemaleAdultCount', 'INT'
EXEC utility.AddColumn @TableName, 'FemaleYouthCount', 'INT'
EXEC utility.AddColumn @TableName, 'MaleAdultCount', 'INT'
EXEC utility.AddColumn @TableName, 'MaleYouthCount', 'INT'
EXEC utility.AddColumn @TableName, 'RiskAssessment', 'VARCHAR(500)'
EXEC utility.AddColumn @TableName, 'RiskMitigationMeasures', 'VARCHAR(500)'

EXEC utility.SetDefaultConstraint @TableName, 'AwardeeSubContractor1', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AwardeeSubContractor2', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleAdultCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleYouthCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'MaleAdultCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'MaleYouthCount', 'INT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteEthnicity
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteEthnicity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteEthnicity
	(
	ConceptNoteEthnicityID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	EthnicityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EthnicityID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteEthnicityID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteEthnicity', @TableName, 'ConceptNoteID,EthnicityID'
GO
--End table dbo.ConceptNoteEthnicity

--Begin table dropdown.Ethnicity
DECLARE @TableName VARCHAR(250) = 'dropdown.Ethnicity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Ethnicity
	(
	EthnicityID INT IDENTITY(0,1) NOT NULL,
	EthnicityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EthnicityID'
EXEC utility.SetIndexNonClustered 'IX_EthnicityName', @TableName, 'DisplayOrder,EthnicityName','EthnicityID'
GO

SET IDENTITY_INSERT dropdown.Ethnicity ON
GO

INSERT INTO dropdown.Ethnicity (EthnicityID) VALUES (0)

SET IDENTITY_INSERT dropdown.Ethnicity OFF
GO

INSERT INTO dropdown.Ethnicity 
	(EthnicityName,DisplayOrder)
VALUES
	('Arabs',1),
	('Kurds',2),
	('Mixed',3),
	('Other',4)
GO 
--End table dropdown.Ethnicity