USE AJACS
GO

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EmailTemplateAdministration')
	BEGIN

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage)
	VALUES 
		(0,'EmailTemplateAdministration','Email Template Administration','EmailTemplateAdministration')
	
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	'EmailTemplateAdministration.AddUpdate',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'EmailTemplateAdministration'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EmailTemplateAdministration.AddUpdate'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'View',
	'View',
	'EmailTemplateAdministration.View',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'EmailTemplateAdministration'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EmailTemplateAdministration.View'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'List',
	'List',
	'EmailTemplateAdministration.List',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'EmailTemplateAdministration'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EmailTemplateAdministration.List'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Amend',
	'Amend',
	'RequestForInformation.Update.Amend',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'RequestForInformation.Update'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'RequestForInformation.Update.Amend'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Administrative'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('EmailTemplateAdministration')
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.PersonPermissionable
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (17,26)

	END
--ENDIF
GO
--End table permissionable.PersonPermissionable
