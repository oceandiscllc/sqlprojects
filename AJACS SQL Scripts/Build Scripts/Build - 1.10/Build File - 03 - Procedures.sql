USE AJACS
GO

--Begin procedure dbo.ValidatePersonMenuItem
EXEC utility.DropObject 'dbo.ValidatePersonMenuItem'
GO
--End procedure dbo.ValidatePersonMenuItem

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Todd Pires
-- Create date: 2015.04.30
-- Description:	Add FailedLogin support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction