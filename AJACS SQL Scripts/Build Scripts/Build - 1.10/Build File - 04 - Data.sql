USE AJACS
GO

--Begin Table dropdown.BudgetType
UPDATE BT
SET BT.BudgetTypeName = 'Other Direct Costs'
FROM dropdown.BudgetType BT
WHERE BT.BudgetTypeName = 'Other Direct Costs (Training)'
GO

UPDATE BT
SET BT.BudgetTypeName = 'Travel'
FROM dropdown.BudgetType BT
WHERE BT.BudgetTypeName = 'Travel, per diem and Accommodation'
GO

UPDATE BT
SET BT.BudgetTypeName = 'Short term Technical Advisors (STTA)'
FROM dropdown.BudgetType BT
WHERE BT.BudgetTypeName = 'Short term Technical Advisors (Police, RSD and TNA)'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.BudgetType BT WHERE BT.BudgetTypeName = 'Other')
	BEGIN
	
	INSERT INTO dropdown.BudgetType
		(BudgetTypeName,DisplayOrder)
	VALUES 
		('Other', 6)
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.BudgetType BT WHERE BT.BudgetTypeName = 'General and Administrative (G&A)')
	BEGIN
	
	INSERT INTO dropdown.BudgetType
		(BudgetTypeName,DisplayOrder)
	VALUES 
		('General and Administrative (G&A)', 7)
	
	END
--ENDIF
GO
--End Table dropdown.BudgetType

--Begin Table dropdown.IncidentType
IF NOT EXISTS (SELECT 1 FROM dropdown.IncidentType IT WHERE IT.IncidentTypeName = 'Other')
	BEGIN
	
	INSERT INTO dropdown.IncidentType
		(IncidentTypeCategory,IncidentTypeName,Icon)
	VALUES 
		('Other','Other','incident.png')
	
	END
--ENDIF
GO
