USE AJACS
GO

--Begin function dbo.GetCommunityNameByCommunityID
EXEC utility.DropObject 'dbo.GetCommunityNameByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.30
-- Description:	A function to return a CommunityName for a specific CommunityID
-- ============================================================================

CREATE FUNCTION dbo.GetCommunityNameByCommunityID
(
@CommunityID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCommunityName VARCHAR(250) = ''
	
	SELECT @cCommunityName = C.CommunityName
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	RETURN ISNULL(@cCommunityName, '')

END
GO
--End function dbo.GetCommunityNameByCommunityID

--Begin function dbo.GetProvinceNameByProvinceID
EXEC utility.DropObject 'dbo.GetProvinceNameByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.30
-- Description:	A function to return a ProvinceName for a specific ProvinceID
-- ==========================================================================

CREATE FUNCTION dbo.GetProvinceNameByProvinceID
(
@ProvinceID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cProvinceName VARCHAR(250) = ''
	
	SELECT @cProvinceName = P.ProvinceName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	RETURN ISNULL(@cProvinceName, '')

END
GO
--End function dbo.GetProvinceNameByProvinceID

