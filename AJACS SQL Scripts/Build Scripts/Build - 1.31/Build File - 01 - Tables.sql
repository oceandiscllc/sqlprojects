USE AJACS
GO

--Begin table reporting.StipendPayment
DECLARE @TableName VARCHAR(250) = 'reporting.StipendPayment'

EXEC utility.DropObject @TableName

CREATE TABLE reporting.StipendPayment
	(
	StipendPaymentID INT IDENTITY(1,1),
	PersonID INT,
	CommunityAssetID INT,
	CommunityAssetUnitID INT,
	[Total Stipend] NUMERIC(18,2),
	[Total Count] INT,
	[Command] NUMERIC(18,2),
	[Command Count] INT,
	[General] NUMERIC(18,2),
	[General Count] INT,
	[Colonel] NUMERIC(18,2),
	[Colonel Count] INT,
	[Colonel Doctor] NUMERIC(18,2),
	[Colonel Doctor Count] INT,
	[Lieutenant Colonel] NUMERIC(18,2),
	[Lieutenant Colonel Count] INT,
	[Major] NUMERIC(18,2),
	[Major Count] INT,
	[Captain] NUMERIC(18,2),
	[Captain Count] INT,
	[Captain Doctor] NUMERIC(18,2),
	[Captain Doctor Count] INT,
	[First Lieutenant] NUMERIC(18,2),
	[First Lieutenant Count] INT,
	[Contracted Officer] NUMERIC(18,2),
	[Contracted Officer Count] INT,
	[First Sergeant] NUMERIC(18,2),
	[First Sergeant Count] INT,
	[Sergeant] NUMERIC(18,2),
	[Sergeant Count] INT,
	[First Adjutant] NUMERIC(18,2),
	[First Adjutant Count] INT,
	[Adjutant] NUMERIC(18,2),
	[Adjutant Count] INT,
	[Policeman] NUMERIC(18,2),
	[Policeman Count] INT,
	[Contracted Policeman] NUMERIC(18,2),
	[Contracted Policeman Count] INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitID', 'INT', 0

ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Total Stipend] DEFAULT ((0)) FOR [Total Stipend]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Total Count] DEFAULT ((0)) FOR [Total Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Command] DEFAULT ((0)) FOR [Command]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Command Count] DEFAULT ((0)) FOR [Command Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_General] DEFAULT ((0)) FOR [General]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_General Count] DEFAULT ((0)) FOR [General Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Colonel] DEFAULT ((0)) FOR [Colonel]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Colonel Count] DEFAULT ((0)) FOR [Colonel Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Colonel Doctor] DEFAULT ((0)) FOR [Colonel Doctor]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Colonel Doctor Count] DEFAULT ((0)) FOR [Colonel Doctor Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Lieutenant Colonel] DEFAULT ((0)) FOR [Lieutenant Colonel]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Lieutenant Colonel Count] DEFAULT ((0)) FOR [Lieutenant Colonel Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Major] DEFAULT ((0)) FOR [Major]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Major Count] DEFAULT ((0)) FOR [Major Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Captain] DEFAULT ((0)) FOR [Captain]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Captain Count] DEFAULT ((0)) FOR [Captain Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Captain Doctor] DEFAULT ((0)) FOR [Captain Doctor]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Captain Doctor Count] DEFAULT ((0)) FOR [Captain Doctor Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_First Lieutenant] DEFAULT ((0)) FOR [First Lieutenant]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_First Lieutenant Count] DEFAULT ((0)) FOR [First Lieutenant Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Contracted Officer] DEFAULT ((0)) FOR [Contracted Officer]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Contracted Officer Count] DEFAULT ((0)) FOR [Contracted Officer Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_First Sergeant] DEFAULT ((0)) FOR [First Sergeant]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_First Sergeant Count] DEFAULT ((0)) FOR [First Sergeant Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Sergeant] DEFAULT ((0)) FOR [Sergeant]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Sergeant Count] DEFAULT ((0)) FOR [Sergeant Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_First Adjutant] DEFAULT ((0)) FOR [First Adjutant]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_First Adjutant Count] DEFAULT ((0)) FOR [First Adjutant Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Adjutant] DEFAULT ((0)) FOR [Adjutant]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Adjutant Count] DEFAULT ((0)) FOR [Adjutant Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Policeman] DEFAULT ((0)) FOR [Policeman]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Policeman Count] DEFAULT ((0)) FOR [Policeman Count]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Contracted Policeman] DEFAULT ((0)) FOR [Contracted Policeman]
ALTER TABLE reporting.StipendPayment ADD CONSTRAINT [DF_StipendPayment_Contracted Policeman Count] DEFAULT ((0)) FOR [Contracted Policeman Count]

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'StipendPaymentID'
EXEC utility.SetIndexClustered 'IX_StipendPayment', @TableName, 'PersonID,StipendPaymentID'
GO
--End table reporting.StipendPayment