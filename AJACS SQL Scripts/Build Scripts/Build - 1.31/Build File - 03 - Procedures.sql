USE AJACS
GO

--Begin procedure eventlog.LogPoliceEngagementAction
EXEC utility.DropObject 'eventlog.LogPoliceEngagementAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPoliceEngagementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML))
			FOR XML RAW('PoliceEngagementUpdate'), ELEMENTS
			)
		FROM policeengagementupdate.PoliceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PoliceEngagementUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPoliceEngagementAction

--Begin procedure policeengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Community and policeengagementupdate.Community tables
-- =================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CapacityAssessmentDate,
		dbo.FormatDate(C.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.MaterialSupportStatus,
		C.PoliceEngagementOutput1,
		C.PPPDate,
		dbo.FormatDate(C.PPPDate) AS PPPDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CapacityAssessmentDate,
		dbo.FormatDate(C1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C1.MaterialSupportStatus,
		C1.PoliceEngagementOutput1,
		C1.PPPDate,
		dbo.FormatDate(C1.PPPDate) AS PPPDateFormatted,
		C2.CommunityID,
		C2.CommunityName AS EntityName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.CommunityID = @CommunityID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''Class'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(

				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.PoliceEngagementAchievedValue,
		OACI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.PoliceEngagementAchievedValue, 
				CI.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(RC.RecommendationCommunityID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.PoliceEngagementRiskValue,
		OACR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''Class'', ' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID,
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.CommunityID = @CommunityID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.CommunityID = @CommunityID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.CommunityID = @CommunityID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetCommunityByCommunityID

--Begin procedure policeengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Province table and policeengagementupdate.Province tables
-- =====================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CapacityAssessmentDate,
		dbo.FormatDate(P.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P.MaterialSupportStatus,
		P.PoliceEngagementOutput1,
		P.PPPDate,
		dbo.FormatDate(P.PPPDate) AS PPPDateFormatted,
		P.ProvinceID,
		P.ProvinceName AS EntityName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CapacityAssessmentDate,
		dbo.FormatDate(P1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P1.MaterialSupportStatus,
		P1.PoliceEngagementOutput1,
		P1.PPPDate,
		dbo.FormatDate(P1.PPPDate) AS PPPDateFormatted,
		P2.ProvinceID,
		P2.ProvinceName AS EntityName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
			AND P1.ProvinceID = @ProvinceID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''Class'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.PoliceEngagementAchievedValue,
		OAPI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.PoliceEngagementAchievedValue, 
				PRI.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(RP.RecommendationProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.PoliceEngagementRiskValue,
		OAPR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID,
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.ProvinceID = @ProvinceID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.ProvinceID = @ProvinceID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.ProvinceID = @ProvinceID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetProvinceByProvinceID

--Begin procedure reporting.GetStipendPaymentReport
EXEC utility.DropObject 'reporting.GetStipendPaymentReport'
GO

-- =================================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.30
-- Description:	A stored procedure to add data to the reporting.StipendPayment table
-- =================================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT = 17

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nCommunityAssetID INT	
	DECLARE @nCommunityAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = 17
				AND C.CommunityAssetID > 0
				AND C.CommunityAssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.CommunityAssetID = @nCommunityAssetID AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,CommunityAssetID,CommunityAssetUnitID) VALUES (@PersonID,@nCommunityAssetID,@nCommunityAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Command] = SP.[Command] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Command Count] = SP.[Command Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END,
			SP.[General] = SP.[General] + CASE WHEN @cStipendName = 'General' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[General Count] = SP.[General Count] + CASE WHEN @cStipendName = 'General' THEN @nContactCount ELSE 0 END,
			SP.[Colonel] = SP.[Colonel] + CASE WHEN @cStipendName = 'Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Count] = SP.[Colonel Count] + CASE WHEN @cStipendName = 'Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Colonel Doctor] = SP.[Colonel Doctor] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Doctor Count] = SP.[Colonel Doctor Count] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nContactCount ELSE 0 END,
			SP.[Lieutenant Colonel] = SP.[Lieutenant Colonel] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Lieutenant Colonel Count] = SP.[Lieutenant Colonel Count] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Major] = SP.[Major] + CASE WHEN @cStipendName = 'Major' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Major Count] = SP.[Major Count] + CASE WHEN @cStipendName = 'Major' THEN @nContactCount ELSE 0 END,
			SP.[Captain] = SP.[Captain] + CASE WHEN @cStipendName = 'Captain' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Count] = SP.[Captain Count] + CASE WHEN @cStipendName = 'Captain' THEN @nContactCount ELSE 0 END,
			SP.[Captain Doctor] = SP.[Captain Doctor] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Doctor Count] = SP.[Captain Doctor Count] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nContactCount ELSE 0 END,
			SP.[First Lieutenant] = SP.[First Lieutenant] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Lieutenant Count] = SP.[First Lieutenant Count] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Officer] = SP.[Contracted Officer] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Officer Count] = SP.[Contracted Officer Count] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nContactCount ELSE 0 END,
			SP.[First Sergeant] = SP.[First Sergeant] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Sergeant Count] = SP.[First Sergeant Count] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[Sergeant] = SP.[Sergeant] + CASE WHEN @cStipendName = 'Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Sergeant Count] = SP.[Sergeant Count] + CASE WHEN @cStipendName = 'Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[First Adjutant] = SP.[First Adjutant] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Adjutant Count] = SP.[First Adjutant Count] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Adjutant] = SP.[Adjutant] + CASE WHEN @cStipendName = 'Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Adjutant Count] = SP.[Adjutant Count] + CASE WHEN @cStipendName = 'Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Policeman] = SP.[Policeman] + CASE WHEN @cStipendName = 'Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Policeman Count] = SP.[Policeman Count] + CASE WHEN @cStipendName = 'Policeman' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Policeman] = SP.[Contracted Policeman] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Policeman Count] = SP.[Contracted Policeman Count] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.CommunityAssetID = @nCommunityAssetID
			AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		SP.*,
		CAUCR.CommunityAssetUnitCostRate AS [Running Costs],

		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetCommunityNameByCommunityID(CA.CommunityID)
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center

	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID

END
GO
--End procedure reporting.GetStipendPaymentReport
