USE AJACS2
GO

--Begin object delete
/*
DECLARE @tTable TABLE (SchemaName VARCHAR(100), TableName VARCHAR(100), Type VARCHAR(5))

INSERT INTO @tTable
	(SchemaName, TableName, Type)
SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	O.Type
FROM sys.Objects O
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
		AND O.Type IN ('FN','P','U')
		AND 
			(
			S.Name IN ('communityprovinceengagementupdate', 'deprecated', 'fifupdate', 'justiceupdate', 'policeengagementupdate', 'survey')
				OR O.Name LIKE 'Atmospheric%'
				OR O.Name LIKE 'Daily%'
				OR O.Name LIKE 'FocusGroup%'
				OR O.Name LIKE 'GetKey%'
				OR O.Name LIKE 'Key%'
				OR O.Name LIKE '%Survey'
				OR O.Name LIKE 'Team%'
			)
ORDER BY S.Name, O.Name

SELECT
	'EXEC utility.DropObject ''' + T.SchemaName + '.' + T.TableName + '''' AS SQLText
FROM @tTable T

UNION

SELECT
	'EXEC utility.DropObject ''' + T.SchemaName + '.Get' + T.TableName + 'Data'''
FROM @tTable T
WHERE T.SchemaName = 'dropdown'
	AND T.Type = 'U'

UNION

SELECT
	'EXEC utility.DropObject ''' + S.Name + '.' + O.Name + '''' AS SQLText
FROM sys.Objects O
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
		AND O.Type IN ('P')
		AND S.Name = 'eventlog'
		AND EXISTS
			(
			SELECT 1
			FROM @tTable T
			WHERE 'Log' + T.TableName + 'Action' = O.Name
				AND T.TableName NOT IN ('Community','Province')
			)
			
ORDER BY 1
*/
EXEC utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity'
EXEC utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince'
EXEC utility.DropObject 'communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate'
EXEC utility.DropObject 'communityprovinceengagementupdate.Community'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityContact'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityIndicator'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityProject'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityProvinceEngagementUpdate'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityRecommendation'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityRisk'
EXEC utility.DropObject 'communityprovinceengagementupdate.DeleteCommunityProvinceEngagementCommunity'
EXEC utility.DropObject 'communityprovinceengagementupdate.DeleteCommunityProvinceEngagementProvince'
EXEC utility.DropObject 'communityprovinceengagementupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate'
EXEC utility.DropObject 'communityprovinceengagementupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'communityprovinceengagementupdate.Province'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceContact'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceIndicator'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceProject'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceRecommendation'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceRisk'
EXEC utility.DropObject 'dbo.Atmospheric'
EXEC utility.DropObject 'dbo.AtmosphericCommunity'
EXEC utility.DropObject 'dbo.AtmosphericEngagementCriteriaResult'
EXEC utility.DropObject 'dbo.AtmosphericSource'
EXEC utility.DropObject 'dbo.CommunityMemberSurvey'
EXEC utility.DropObject 'dbo.DailyReport'
EXEC utility.DropObject 'dbo.DailyReportCommunity'
EXEC utility.DropObject 'dbo.FocusGroupSurvey'
EXEC utility.DropObject 'dbo.FocusGroupSurveyParticipant'
EXEC utility.DropObject 'dbo.GetKeyEventByKeyEventID'
EXEC utility.DropObject 'dbo.GetKeyInformantSurveyByKeyInformantSurveyID'
EXEC utility.DropObject 'dbo.KeyEvent'
EXEC utility.DropObject 'dbo.KeyEventCommunity'
EXEC utility.DropObject 'dbo.KeyEventEngagementCriteriaResult'
EXEC utility.DropObject 'dbo.KeyEventProvince'
EXEC utility.DropObject 'dbo.KeyEventSource'
EXEC utility.DropObject 'dbo.KeyInformantSurvey'
EXEC utility.DropObject 'dbo.RapidPerceptionSurvey'
EXEC utility.DropObject 'dbo.StakeholderGroupSurvey'
EXEC utility.DropObject 'dbo.StationCommanderSurvey'
EXEC utility.DropObject 'dbo.Team'
EXEC utility.DropObject 'dbo.TeamMember'
EXEC utility.DropObject 'deprecated.ConceptNoteContactEquipment'
EXEC utility.DropObject 'deprecated.EquipmentAuditOutcome'
EXEC utility.DropObject 'deprecated.EquipmentDistributionPlan'
EXEC utility.DropObject 'dropdown.AtmosphericType'
EXEC utility.DropObject 'dropdown.DailyReportType'
EXEC utility.DropObject 'dropdown.GetAtmosphericTypeData'
EXEC utility.DropObject 'dropdown.GetDailyReportTypeData'
EXEC utility.DropObject 'dropdown.GetKeyEventCategoryData'
EXEC utility.DropObject 'dropdown.GetKeyEventDateFilterData'
EXEC utility.DropObject 'dropdown.GetKeyEventGroupData'
EXEC utility.DropObject 'dropdown.GetKeyEventGroupKeyEventTypeData'
EXEC utility.DropObject 'dropdown.GetKeyEventTypeData'
EXEC utility.DropObject 'dropdown.KeyEventCategory'
EXEC utility.DropObject 'dropdown.KeyEventGroup'
EXEC utility.DropObject 'dropdown.KeyEventGroupKeyEventType'
EXEC utility.DropObject 'dropdown.KeyEventType'
EXEC utility.DropObject 'eventlog.LogAtmosphericAction'
EXEC utility.DropObject 'eventlog.LogCommunityMemberSurveyAction'
EXEC utility.DropObject 'eventlog.LogConceptNoteContactEquipmentAction'
EXEC utility.DropObject 'eventlog.LogDailyReportAction'
EXEC utility.DropObject 'eventlog.LogEquipmentDistributionPlanAction'
EXEC utility.DropObject 'eventlog.LogFocusGroupSurveyAction'
EXEC utility.DropObject 'eventlog.LogKeyEventAction'
EXEC utility.DropObject 'eventlog.LogKeyInformantSurveyAction'
EXEC utility.DropObject 'eventlog.LogRapidPerceptionSurveyAction'
EXEC utility.DropObject 'eventlog.LogStakeholderGroupSurveyAction'
EXEC utility.DropObject 'eventlog.LogStationCommanderSurveyAction'
EXEC utility.DropObject 'eventlog.LogSurveyAction'
EXEC utility.DropObject 'eventlog.LogSurveyQuestionAction'
EXEC utility.DropObject 'eventlog.LogSurveyResponseAction'
EXEC utility.DropObject 'eventlog.LogTeamAction'
EXEC utility.DropObject 'fifupdate.AddFIFCommunity'
EXEC utility.DropObject 'fifupdate.AddFIFProvince'
EXEC utility.DropObject 'fifupdate.ApproveFIFUpdate'
EXEC utility.DropObject 'fifupdate.Community'
EXEC utility.DropObject 'fifupdate.CommunityIndicator'
EXEC utility.DropObject 'fifupdate.CommunityMeeting'
EXEC utility.DropObject 'fifupdate.CommunityRisk'
EXEC utility.DropObject 'fifupdate.DeleteFIFCommunity'
EXEC utility.DropObject 'fifupdate.DeleteFIFProvince'
EXEC utility.DropObject 'fifupdate.FIFUpdate'
EXEC utility.DropObject 'fifupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'fifupdate.GetFIFUpdate'
EXEC utility.DropObject 'fifupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'fifupdate.Province'
EXEC utility.DropObject 'fifupdate.ProvinceIndicator'
EXEC utility.DropObject 'fifupdate.ProvinceMeeting'
EXEC utility.DropObject 'fifupdate.ProvinceRisk'
EXEC utility.DropObject 'justiceupdate.AddJusticeCommunity'
EXEC utility.DropObject 'justiceupdate.AddJusticeProvince'
EXEC utility.DropObject 'justiceupdate.ApproveJusticeUpdate'
EXEC utility.DropObject 'justiceupdate.Community'
EXEC utility.DropObject 'justiceupdate.CommunityIndicator'
EXEC utility.DropObject 'justiceupdate.CommunityRisk'
EXEC utility.DropObject 'justiceupdate.CommunityTraining'
EXEC utility.DropObject 'justiceupdate.DeleteJusticeCommunity'
EXEC utility.DropObject 'justiceupdate.DeleteJusticeProvince'
EXEC utility.DropObject 'justiceupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'justiceupdate.GetJusticeUpdate'
EXEC utility.DropObject 'justiceupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'justiceupdate.JusticeUpdate'
EXEC utility.DropObject 'justiceupdate.Province'
EXEC utility.DropObject 'justiceupdate.ProvinceIndicator'
EXEC utility.DropObject 'justiceupdate.ProvinceRisk'
EXEC utility.DropObject 'justiceupdate.ProvinceTraining'
EXEC utility.DropObject 'policeengagementupdate.AddPoliceEngagementCommunity'
EXEC utility.DropObject 'policeengagementupdate.AddPoliceEngagementProvince'
EXEC utility.DropObject 'policeengagementupdate.ApprovePoliceEngagementUpdate'
EXEC utility.DropObject 'policeengagementupdate.Community'
EXEC utility.DropObject 'policeengagementupdate.CommunityClass'
EXEC utility.DropObject 'policeengagementupdate.CommunityIndicator'
EXEC utility.DropObject 'policeengagementupdate.CommunityRecommendation'
EXEC utility.DropObject 'policeengagementupdate.CommunityRisk'
EXEC utility.DropObject 'policeengagementupdate.DeletePoliceEngagementCommunity'
EXEC utility.DropObject 'policeengagementupdate.DeletePoliceEngagementProvince'
EXEC utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'policeengagementupdate.GetPoliceEngagementUpdate'
EXEC utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'policeengagementupdate.PoliceEngagementUpdate'
EXEC utility.DropObject 'policeengagementupdate.Province'
EXEC utility.DropObject 'policeengagementupdate.ProvinceClass'
EXEC utility.DropObject 'policeengagementupdate.ProvinceIndicator'
EXEC utility.DropObject 'policeengagementupdate.ProvinceRecommendation'
EXEC utility.DropObject 'policeengagementupdate.ProvinceRisk'
EXEC utility.DropObject 'reporting.GetCommunityMemberSurvey'
EXEC utility.DropObject 'reporting.GetFocusGroupSurvey'
EXEC utility.DropObject 'reporting.GetKeyInformantSurvey'
EXEC utility.DropObject 'reporting.GetStakeholderGroupSurvey'
EXEC utility.DropObject 'reporting.GetStationCommanderSurvey'
EXEC utility.DropObject 'survey.FormatSurveyQuestionResponse'
EXEC utility.DropObject 'survey.GetAdministerButtonHTMLBySurveyID'
EXEC utility.DropObject 'survey.GetSurveyBySurveyID'
EXEC utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
EXEC utility.DropObject 'survey.GetSurveyQuestionResponseChoices'
EXEC utility.DropObject 'survey.GetSurveyQuestionResponsesBySurveyResponseID'
EXEC utility.DropObject 'survey.GetSurveyResponseBySurveyResponseID'
EXEC utility.DropObject 'survey.GetViewButtonHTMLBySurveyID'
EXEC utility.DropObject 'survey.Survey'
EXEC utility.DropObject 'survey.SurveyLabel'
EXEC utility.DropObject 'survey.SurveyLanguage'
EXEC utility.DropObject 'survey.SurveyQuestion'
EXEC utility.DropObject 'survey.SurveyQuestionLabel'
EXEC utility.DropObject 'survey.SurveyQuestionLanguage'
EXEC utility.DropObject 'survey.SurveyQuestionResponseChoice'
EXEC utility.DropObject 'survey.SurveyResponse'
EXEC utility.DropObject 'survey.SurveySurveyQuestion'
EXEC utility.DropObject 'survey.SurveySurveyQuestionResponse'
GO

-- Added manually
EXEC utility.DropObject 'dbo.GetAtmosphericByAtmosphericID' 
EXEC utility.DropObject 'dbo.GetCommunityAssetLocations'
EXEC utility.DropObject 'dbo.GetCommunityAssetUnitExpenseNotes'
EXEC utility.DropObject 'dbo.GetCommunityIDFromCommunityAssetID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyByCommunityMemberSurveyID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByProvinceID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByProvinceID'
EXEC utility.DropObject 'dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetProvinceIDFromCommunityAssetID'
EXEC utility.DropObject 'dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.SpotReportSummaryMapCommunityAsset'
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
EXEC utility.DropObject 'eventlog.LogPoliceEngagementAction'
EXEC utility.DropObject 'reporting.GetCommunityMemberSurvey'
GO

UPDATE P
SET P.PermissionableGroupID = (SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Implementation')
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityRound','CommunityRoundActivity')
GO

UPDATE P
SET P.IsActive = 0
FROM permissionable.Permissionable P
	JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
		AND PG.PermissionableGroupName = 'Insight & Understanding'
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityMemberSurvey','CommunityProvinceEngagement','CommunityProvinceEngagementUpdate','DailyReport','FIFUpdate','FocusGroupSurvey','Justice','KeyEvent','KeyInformantSurvey','PoliceEngagement','RAPData','RapidPerceptionSurvey','StakeholderGroupSurvey','StationCommanderSurvey','Team')
GO

DELETE PG
FROM permissionable.PermissionableGroup PG
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableGroupID = PG.PermissionableGroupID
	)
GO

DECLARE @JobID UNIQUEIDENTIFIER

SELECT @JobID = J.Job_ID
FROM msdb.dbo.sysjobs J WHERE J.Name = 'AJACS dbo.Atmospheric IsCritical Expiration'

IF @JobID IS NOT NULL
	EXEC msdb.dbo.sp_delete_job @job_id=@JobID, @delete_unused_schedule=1
--ENDIF	

SELECT @JobID = J.Job_ID
FROM msdb.dbo.sysjobs J WHERE J.Name = 'AJACS dbo.KeyEvent IsCritical Expiration'

IF @JobID IS NOT NULL
	EXEC msdb.dbo.sp_delete_job @job_id=@JobID, @delete_unused_schedule=1
--ENDIF	
GO
--End object delete

--Begin schema delete
EXEC utility.DropSchema 'communityprovinceengagementupdate'
EXEC utility.DropSchema 'deprecated'
EXEC utility.DropSchema 'fifupdate'
EXEC utility.DropSchema 'justiceupdate'
EXEC utility.DropSchema 'policeengagementupdate'
EXEC utility.DropSchema 'survey'
GO
--End schema delete

--Begin table asset.Asset
DECLARE @TableName VARCHAR(250) = 'asset.Asset'

EXEC utility.DropObject @TableName

CREATE TABLE asset.Asset
	(
	AssetID INT IDENTITY(1,1),
	AssetName NVARCHAR(250),
	AssetDescription NVARCHAR(MAX),
	AssetStatusID INT,
	AssetTypeID INT,
	CommunityID INT,
	IsActive BIT,
	Location GEOMETRY,
	RelocationLocation NVARCHAR(250),
	RelocationDate DATE,
	RelocationNotes NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AssetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetID'
GO

SET IDENTITY_INSERT asset.Asset ON
GO

INSERT INTO asset.Asset
	(AssetID, AssetName, AssetDescription, AssetTypeID, CommunityID, Location)
SELECT
	CA.CommunityAssetID, 
	CA.CommunityAssetName, 
	CA.CommunityAssetDescription, 
	CA.AssetTypeID, 
	CA.CommunityID,
	CA.Location
FROM dbo.CommunityAsset CA
	JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		AND CAT.CommunityAssetTypeName = 'Asset'
GO

SET IDENTITY_INSERT asset.Asset OFF
GO
--End table asset.Asset

--Begin table asset.AssetUnit
DECLARE @TableName VARCHAR(250) = 'asset.AssetUnit'

EXEC utility.DropObject @TableName

CREATE TABLE asset.AssetUnit
	(
	AssetUnitID INT IDENTITY(1,1),
	AssetID INT,
	AssetUnitName NVARCHAR(250),
	AssetUnitCostID INT,
	CommanderContactID INT,
	DeputyCommanderContactID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitCostID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeputyCommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AssetUnitID'
EXEC utility.SetIndexClustered @TableName, 'IX_AssetUnit', 'AssetID,AssetUnitName,AssetUnitID'
GO
--End table asset.AssetUnit

--Begin table asset.AssetUnitExpense
DECLARE @TableName VARCHAR(250) = 'asset.AssetUnitExpense'

EXEC utility.DropObject @TableName

CREATE TABLE asset.AssetUnitExpense
	(
	AssetUnitExpenseID INT IDENTITY(1,1) NOT NULL,
	AssetUnitID INT,
	ExpenseAmountAuthorized NUMERIC(18,2),
	ExpenseAuthorizedDate DATE,
	ExpenseAmountPaid NUMERIC(18,2),
	ExpensePaidDate DATE,
	Notes VARCHAR(MAX),
	ProvinceID INT,
	PaymentMonth INT,
	PaymentYear INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmountAuthorized', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmountPaid', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentMonth', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentYear', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AssetUnitExpenseID'
EXEC utility.SetIndexClustered @TableName, 'IX_AssetUnitExpense', 'PaymentYear,PaymentMonth,ProvinceID,AssetUnitID'
GO

INSERT INTO asset.AssetUnitExpense
	(AssetUnitID, ExpenseAmountAuthorized, ExpenseAuthorizedDate, ExpenseAmountPaid, ExpensePaidDate, Notes, ProvinceID, PaymentMonth, PaymentYear)
SELECT
	CAUE.CommunityAssetUnitID, 
	CAUE.ExpenseAmountAuthorized, 
	CAUE.ExpenseAuthorizedDate, 
	CAUE.ExpenseAmountPaid, 
	CAUE.ExpensePaidDate, 
	CAUE.Notes, 
	CAUE.ProvinceID, 
	CAUE.PaymentMonth,
	CAUE.PaymentYear
FROM dbo.CommunityAssetUnitExpense CAUE
GO
--End table asset.AssetUnitExpense

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'CommunityComponentStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'FIFComponentStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'
EXEC utility.AddColumn @TableName, 'JusticeComponentStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'PoliceComponentStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CommunityComponentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFComponentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'JusticeComponentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceComponentStatusID', 'INT', 0
GO
--End table dbo.Community

--Begin table dbo.CommunityImpactDecisionHistory
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityImpactDecisionHistory'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityImpactDecisionHistory
	(
	CommunityImpactDecisionHistoryID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	CurrentImpactDecisionID INT,
	PreviousImpactDecisionID INT,
	PersonID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CurrentImpactDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PreviousImpactDecisionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityImpactDecisionHistoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityImpactDecisionHistory', 'CreateDateTime,CommunityID,CurrentImpactDecisionID,PreviousImpactDecisionID'
GO
--End table dbo.CommunityImpactDecisionHistory

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.DropColumn @TableName, 'AmmendedConceptNoteID'

EXEC utility.AddColumn @TableName, 'AmendedConceptNoteID', 'INT'
EXEC utility.AddColumn @TableName, 'WorkplanActivityID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'AmendedConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkplanActivityID', 'INT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteContact
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContact'

EXEC utility.DropColumn @TableName, 'USVettingOutcomeID'
EXEC utility.DropColumn @TableName, 'USVettingDate'
EXEC utility.DropColumn @TableName, 'Notes'
EXEC utility.DropColumn @TableName, 'UKVettingOutcomeID'
EXEC utility.DropColumn @TableName, 'UKVettingDate'
GO
--End table dbo.ConceptNoteContact

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.DropColumn @TableName, 'ProvinceID'

EXEC utility.DropColumn @TableName, 'AssetID'
EXEC utility.DropColumn @TableName, 'IsAssetCommander'

EXEC utility.AddColumn @TableName, 'AssetUnitID', 'INT'
EXEC utility.AddColumn @TableName, 'StipendArrears', 'NUMERIC(18,2)'

EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StipendArrears', 'NUMERIC(18,2)', 0
GO

UPDATE C
SET C.AssetUnitID = C.CommunityAssetUnitID
FROM dbo.Contact C
GO
--End table dbo.Contact

--Begin table dbo.Document
DECLARE @TableName VARCHAR(250) = 'dbo.Document'

EXEC utility.AddColumn @TableName, 'DisplayInCommunityNewsFeed', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayInCommunityNewsFeed', 'BIT', 0
GO
--End table dbo.Document

--Begin table dbo.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'dbo.DocumentEntity'

EXEC utility.AddColumn @TableName, 'EntityTypeSubCode', 'VARCHAR(50)'
GO
--End table dbo.DocumentEntity

--Begin table dropdown.AssetStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetStatus
	(
	AssetStatusID INT IDENTITY(0,1) NOT NULL,
	AssetStatusCode VARCHAR(50),
	AssetStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AssetStatusName', 'DisplayOrder,AssetStatusName', 'AssetStatusID'
GO

SET IDENTITY_INSERT dropdown.AssetStatus ON
GO

INSERT INTO dropdown.AssetStatus 
	(AssetStatusID, AssetStatusCode, AssetStatusName, DisplayOrder, IsActive)
VALUES
	(0, NULL, NULL, 0, 1),
	(1, 'Approved', 'Approved', 1, 1),
	(2, 'Planned', 'Planned', 2, 1),
	(3, 'Operational', 'Operational', 3, 1),
	(4, 'NonOperational', 'Non Operational', 4, 1),
	(5, 'TemporarilyRelocated', 'Temporarily Relocated', 5, 1)
GO

SET IDENTITY_INSERT dropdown.AssetStatus OFF
GO
--End table dropdown.AssetStatus

--Begin table dropdown.AssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetType'

EXEC utility.AddColumn @TableName, 'AssetTypeCategory', 'VARCHAR(50)'
GO

DECLARE @tTable TABLE (AssetTypeCategory VARCHAR(50), AssetTypeName VARCHAR(250), Icon VARCHAR(50))

INSERT INTO @tTable
	(AssetTypeCategory, AssetTypeName, Icon)
VALUES
	('Community Engagement', 'Community Building', 'asset-community-building.png'),
	('Community Engagement', 'CSWG Office', 'asset-community-building.png'),
	('Community Engagement', 'Project Location', 'asset-community-building.png'),
	('Justice', 'Court', 'asset-court.png'),
	('Justice', 'Document Centre', 'asset-court.png'),
	('Justice', 'Document Headquarters', 'asset-court.png'),
	('Justice', 'Prison', 'asset-court.png'),
	('Police', 'Criminal Investigation Department', 'asset-police-station.png'),
	('Police', 'Media Office', 'asset-police-station.png'),
	('Police', 'Police Headquarters', 'asset-police-station.png'),
	('Police', 'Police Station', 'asset-police-station.png'),
	('Police', 'Traffic Centre', 'asset-police-station.png'),
	('Police', 'Training Center', 'asset-police-station.png'),
	('Police', 'Warehouse', 'asset-police-station.png'),
	('Research', 'Administrative Centre', 'asset-document-centre.png'),
	('Research', 'Airport', 'asset-document-centre.png'),
	('Research', 'Medical Facility', 'asset-document-centre.png'),
	('Other', 'Other', 'asset-other.png')

MERGE dropdown.AssetType T1
USING (SELECT AssetTypeCategory, AssetTypeName, Icon FROM @tTable T) T2
	ON T2.AssetTypeName = T1.AssetTypeName
WHEN MATCHED THEN 
UPDATE 
SET 
	T1.AssetTypeCategory = T2.AssetTypeCategory,
	T1.Icon = T2.Icon
WHEN NOT MATCHED THEN
INSERT 
	(AssetTypeCategory, AssetTypeName, Icon)
VALUES
	(
	T2.AssetTypeCategory, 
	T2.AssetTypeName, 
	T2.Icon
	);

UPDATE AT
SET AT.DisplayOrder = 
	CASE
		WHEN AT.AssetTypeCategory = 'Other'
		THEN 99
		ELSE 0
	END
FROM dropdown.AssetType AT
GO
--End table dropdown.AssetType

--Begin table dropdown.AssetUnitCost
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetUnitCost'

EXEC utility.DropObject 'dropdown.AssetUnitCostRate'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetUnitCost
	(
	AssetUnitCostID INT IDENTITY(0,1) NOT NULL,
	AssetUnitCostName INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitCostName', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetUnitCostID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AssetUnitCost', 'DisplayOrder,AssetUnitCostName', 'AssetUnitCostID'
GO

SET IDENTITY_INSERT dropdown.AssetUnitCost ON
GO

INSERT INTO dropdown.AssetUnitCost 
	(AssetUnitCostID, AssetUnitCostName, DisplayOrder)
VALUES
	(0, 0, 1),
	(1, 500, 2),
	(2, 3000, 3)
GO

SET IDENTITY_INSERT dropdown.AssetUnitCost OFF
GO

SET IDENTITY_INSERT asset.AssetUnit ON
GO

INSERT INTO asset.AssetUnit
	(AssetUnitID, AssetID, AssetUnitName, AssetUnitCostID)
SELECT
	CAU.CommunityAssetUnitID, 
	CAU.CommunityAssetID, 
	CAU.CommunityAssetUnitName, 
	(SELECT AUCR.AssetUnitCostID FROM dropdown.AssetUnitCost AUCR WHERE AUCR.AssetUnitCostName = CAUCR.CommunityAssetUnitCostRate)
FROM dbo.CommunityAssetUnit CAU
	JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
GO

SET IDENTITY_INSERT asset.AssetUnit OFF
GO

;
WITH CAUD AS
	(
	SELECT 
		CAU.CommunityAssetID,
		MIN(CAU.CommunityAssetUnitID) AS CommunityAssetUnitID
	FROM dbo.CommunityAssetUnit CAU
	GROUP BY CommunityAssetID
	)

UPDATE DI
SET DI.EndUserEntityID = CAUD.CommunityAssetUnitID
FROM procurement.DistributedInventory DI
	JOIN CAUD ON CAUD.CommunityAssetID = DI.EndUserEntityID
		AND DI.EndUserEntityTypeCode = 'CommunityAsset'

UPDATE DI
SET DI.EndUserEntityTypeCode = 'AssetUnit'
FROM procurement.DistributedInventory DI
WHERE DI.EndUserEntityTypeCode = 'CommunityAsset'
GO
--End table dropdown.AssetUnitCost

--Begin table dropdown.ComponentStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ComponentStatus'

EXEC utility.DropObject 'dropdown.ComponentStatus'

CREATE TABLE dropdown.ComponentStatus
	(
	ComponentStatusID INT IDENTITY(0,1) NOT NULL,
	ComponentStatusName VARCHAR(250),
	DisplayOrder INT,
	HexColor VARCHAR(7),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ComponentStatusID'
EXEC utility.SetIndexClustered @TableName, 'IX_ComponentStatus', 'DisplayOrder,ComponentStatusName,ComponentStatusID'
GO

SET IDENTITY_INSERT dropdown.ComponentStatus ON
GO

INSERT INTO dropdown.ComponentStatus
	(ComponentStatusID, ComponentStatusName, DisplayOrder, HexColor)
VALUES 
	(0, NULL, 0, NULL),
	(1, 'Yes', 1, '#00BB54'),
	(2, 'Pending', 2, '#FFA500'),
	(3, 'No', 3, '#FF0000')

SET IDENTITY_INSERT dropdown.ComponentStatus OFF
GO
--End table dropdown.ComponentStatus

--Begin table dropdown.ConceptNoteType
DECLARE @TableName VARCHAR(250) = 'dropdown.ConceptNoteType'

EXEC utility.AddColumn @TableName, 'ConceptNoteTypeCode', 'VARCHAR(50)'
GO

UPDATE CNT
SET CNT.ConceptNoteTypeCode = REPLACE(CNT.ConceptNoteTypeName, ' ', '')
FROM dropdown.ConceptNoteType CNT
WHERE CNT.ConceptNoteTypeID > 0
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage LIKE 'ConceptNote.View.Budget%'
	OR P.PermissionableLineage = 'ConceptNote.View.ViewBudget'
	OR P.PermissionableLineage = 'ConceptNote.AddUpdate.Risk'
	OR P.PermissionableLineage = 'ConceptNote.View.Risk'
GO

INSERT INTO permissionable.Permissionable
	(IsGlobal, ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsSuperAdministrator)
SELECT
	0,
	'ConceptNote',
	'View',
	'Budget' + CNT.ConceptNoteTypeCode,
	(SELECT P.PermissionableGroupID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'ConceptNote.List'),
	'View the budget on concept notes of component type ' + CNT.ConceptNoteTypeName,
	0
FROM dropdown.ConceptNoteType CNT
WHERE CNT.ConceptNoteTypeID > 0

UNION

SELECT
	0,
	'ConceptNote',
	'AddUpdate',
	'Risk',
	P.PermissionableGroupID,
	'View the risk pane a concept note',
	1
FROM permissionable.Permissionable P 
WHERE P.PermissionableLineage = 'ConceptNote.List'

UNION

SELECT
	0,
	'ConceptNote',
	'View',
	'Risk',
	P.PermissionableGroupID,
	'View the risk pane a concept note',
	1
FROM permissionable.Permissionable P 
WHERE P.PermissionableLineage = 'ConceptNote.List'
GO
--End table dropdown.ConceptNoteType

--Begin table dropdown.ImpactDecision
UPDATE dropdown.ImpactDecision
SET HexColor = '#00BB54'
WHERE ImpactDecisionName = 'Yes'
GO

UPDATE dropdown.ImpactDecision
SET HexColor = '#FFA500'
WHERE ImpactDecisionName = 'Under Review'
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.MediaReportSourceType
DECLARE @TableName VARCHAR(250) = 'dropdown.MediaReportSourceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MediaReportSourceType
	(
	MediaReportSourceTypeID INT IDENTITY(0,1) NOT NULL,
	MediaReportSourceTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'MediaReportSourceTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MediaReportSourceTypeName', 'DisplayOrder,MediaReportSourceTypeName', 'MediaReportSourceTypeID'
GO

SET IDENTITY_INSERT dropdown.MediaReportSourceType ON
GO

INSERT INTO dropdown.MediaReportSourceType (MediaReportSourceTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.MediaReportSourceType OFF
GO

INSERT INTO dropdown.MediaReportSourceType 
	(MediaReportSourceTypeName)
VALUES
	('Guerrilla Communications'),
	('Outreach'),
	('Print/Physical Product'),
	('Radio'),
	('Social Media'),
	('Traditional Broadcast'),
	('Website')
GO
--End table dropdown.MediaReportSourceType

--Begin table dropdown.MediaReportType
DECLARE @TableName VARCHAR(250) = 'dropdown.MediaReportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MediaReportType
	(
	MediaReportTypeID INT IDENTITY(0,1) NOT NULL,
	MediaReportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'MediaReportTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MediaReportTypeName', 'DisplayOrder,MediaReportTypeName', 'MediaReportTypeID'
GO

SET IDENTITY_INSERT dropdown.MediaReportType ON
GO

INSERT INTO dropdown.MediaReportType (MediaReportTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.MediaReportType OFF
GO

INSERT INTO dropdown.MediaReportType 
	(MediaReportTypeName, DisplayOrder)
VALUES
	('International', 1),
	('Syria', 2)
GO

INSERT INTO dropdown.MediaReportType 
	(MediaReportTypeName, DisplayOrder)
SELECT
	P.ProvinceName,
	99
FROM dbo.Province P
ORDER BY P.ProvinceName, P.ProvinceID
GO
--End table dropdown.MediaReportType

--Begin table dropdown.ZoneStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ZoneStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ZoneStatus
	(
	ZoneStatusID INT IDENTITY(0,1) NOT NULL,
	ZoneStatusCode VARCHAR(50),
	ZoneStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ZoneStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ZoneStatusName', 'DisplayOrder,ZoneStatusName', 'ZoneStatusID'
GO

SET IDENTITY_INSERT dropdown.ZoneStatus ON
GO

INSERT INTO dropdown.ZoneStatus 
	(ZoneStatusID, ZoneStatusCode, ZoneStatusName, DisplayOrder, IsActive)
VALUES
	(0, NULL, NULL, 0, 1),
	(1, 'Approved', 'Approved', 1, 1),
	(2, 'Planned', 'Planned', 2, 1),
	(3, 'Operational', 'Operational', 3, 1),
	(4, 'NonOperational', 'Non Operational', 4, 1),
	(5, 'TemporarilyRelocated', 'Temporarily Relocated', 5, 1)
GO

SET IDENTITY_INSERT dropdown.ZoneStatus OFF
GO
--End table dropdown.ZoneStatus

--Begin table force.Force
DECLARE @TableName VARCHAR(250) = 'force.Force'

EXEC utility.AddColumn @TableName, 'CommanderFullName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'DeputyCommanderFullName', 'NVARCHAR(250)'
GO

UPDATE F
SET 
	F.CommanderFullName = dbo.FormatContactNameByContactID(F.CommanderContactID, 'LastFirstMiddle'),
	F.DeputyCommanderFullName = dbo.FormatContactNameByContactID(F.DeputyCommanderContactID, 'LastFirstMiddle')
FROM force.Force F
GO	
--End table force.Force

--Begin table force.ForceUnit
DECLARE @TableName VARCHAR(250) = 'force.ForceUnit'

EXEC utility.AddColumn @TableName, 'CommanderFullName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'DeputyCommanderFullName', 'NVARCHAR(250)'
GO

UPDATE FU
SET 
	FU.CommanderFullName = dbo.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle'),
	FU.DeputyCommanderFullName = dbo.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle')
FROM force.ForceUnit FU
GO	
--End table force.ForceUnit

--Begin table mediareport.MediaReport
DECLARE @TableName VARCHAR(250) = 'mediareport.MediaReport'

EXEC utility.DropObject @TableName

CREATE TABLE mediareport.MediaReport
	(
	MediaReportID INT IDENTITY(1,1) NOT NULL,
	MediaReportTitle VARCHAR(250),
	MediaReportDate DATE,
	MediaReportTypeID INT,
	MediaReportLocation NVARCHAR(250),
	Summary VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediaReportTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'MediaReportID'
GO
--End table mediareport.MediaReport

--Begin table mediareport.MediaReportCommunity
DECLARE @TableName VARCHAR(250) = 'mediareport.MediaReportCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE mediareport.MediaReportCommunity
	(
	MediaReportCommunityID INT IDENTITY(1,1) NOT NULL,
	MediaReportID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediaReportID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_MediaReportCommunity', 'MediaReportID,CommunityID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MediaReportCommunityID'
GO
--End table mediareport.MediaReportCommunity

--Begin table mediareport.MediaReportSource
DECLARE @TableName VARCHAR(250) = 'mediareport.MediaReportSource'

EXEC utility.DropObject @TableName

CREATE TABLE mediareport.MediaReportSource
	(
	MediaReportSourceID INT IDENTITY(1,1) NOT NULL,
	MediaReportID INT,
	SourceName NVARCHAR(250),
	SourceAttribution NVARCHAR(MAX),
	SourceDate DATE,
	MediaReportSourceTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediaReportID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'MediaReportSourceTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MediaReportSourceID'
EXEC utility.SetIndexClustered @TableName, 'IX_MediaReportSource', 'MediaReportID,MediaReportSourceID'
GO
--End table mediareport.MediaReportSource

--Begin table reporting.StipendPayment
DECLARE @TableName VARCHAR(250) = 'reporting.StipendPayment'

EXEC utility.AddColumn @TableName, 'AssetID', 'INT'
EXEC utility.AddColumn @TableName, 'AssetUnitID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitID', 'INT', 0
GO

UPDATE SP
SET 
	SP.AssetID = SP.CommunityAssetID,
	SP.AssetUnitID = SP.CommunityAssetUnitID
FROM reporting.StipendPayment SP
GO
--End table reporting.StipendPayment

--Begin table weeklyreport.Community
DECLARE @TableName VARCHAR(250) = 'weeklyreport.Community'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
GO
--End table weeklyreport.Community

--Begin table weeklyreport.SummaryMapAsset
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapAsset'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapAsset
	(
	SummaryMapAssetID INT IDENTITY(1,1) NOT NULL,
	AssetID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_SummaryMapAsset', 'WeeklyReportID,AssetID'
GO

SET IDENTITY_INSERT weeklyreport.SummaryMapAsset ON
GO

INSERT INTO weeklyreport.SummaryMapAsset
	(SummaryMapAssetID,AssetID,WeeklyReportID)
SELECT
	SMCA.SummaryMapCommunityAssetID,
	SMCA.CommunityAssetID,
	SMCA.WeeklyReportID
FROM weeklyreport.SummaryMapCommunityAsset SMCA
GO

SET IDENTITY_INSERT weeklyreport.SummaryMapAsset OFF
GO
--End table weeklyreport.SummaryMapAsset

--Begin table workplan.Workplan
DECLARE @TableName VARCHAR(250) = 'workplan.Workplan'

IF NOT EXISTS (SELECT 1 FROM sys.tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	EXEC utility.DropObject @TableName

	CREATE TABLE workplan.Workplan
		(
		WorkplanID INT IDENTITY(1,1) NOT NULL,
		WorkplanName VARCHAR(250),
		StartDate DATE,
		EndDate DATE,
		USContractNumber VARCHAR(50),
		UKContractNumber VARCHAR(50),
		FinancialYear INT,
		IsForDashboard BIT,
		IsActive BIT,
		USDToGBPExchangeRate NUMERIC(18,2)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'FinancialYear', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'USDToGBPExchangeRate', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
	EXEC utility.SetDefaultConstraint @TableName, 'IsForDashboard', 'BIT', 0

	EXEC utility.SetPrimaryKeyClustered @TableName, 'WorkplanID'

	END
--ENDIF	

EXEC utility.DropColumn @TableName, 'FinancialYear'
GO
--End table workplan.Workplan

--Begin table workplan.WorkplanActivity
DECLARE @TableName VARCHAR(250) = 'workplan.WorkplanActivity'

IF NOT EXISTS (SELECT 1 FROM sys.tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	EXEC utility.DropObject @TableName

	CREATE TABLE workplan.WorkplanActivity
		(
		WorkplanActivityID INT IDENTITY(1,1) NOT NULL,
		WorkplanActivityName VARCHAR(250),
		WorkplanID INT,
		ConceptNoteTypeID INT,
		CurrentFundingSourceID INT,
		OriginalFundingSourceID INT,
		CurrentGBPAllocation NUMERIC(18,2),
		OriginalGBPAllocation NUMERIC(18,2),
		CurrentUSDAllocation NUMERIC(18,2),
		OriginalUSDAllocation NUMERIC(18,2),
		Description VARCHAR(MAX),
		Notes VARCHAR(MAX)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteTypeID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'CurrentFundingSourceID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'CurrentGBPAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'CurrentUSDAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'OriginalFundingSourceID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'OriginalGBPAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'OriginalUSDAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'WorkplanID', 'INT', 0

	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'WorkplanActivityID'
	EXEC utility.SetIndexClustered @TableName, 'IX_WorkplanActivity', 'WorkplanID,WorkplanActivityName'

	END
--ENDIF	
GO
--End table workplan.WorkplanActivity

--Begin table zone.Zone
DECLARE @TableName VARCHAR(250) = 'zone.Zone'

EXEC utility.DropObject @TableName

CREATE TABLE zone.Zone
	(
	ZoneID INT IDENTITY(1,1),
	ZoneName NVARCHAR(250),
	ZoneDescription NVARCHAR(MAX),
	ZoneStatusID INT,
	ZoneTypeID INT,
	TerritoryTypeCode VARCHAR(50),
	TerritoryID INT,
	Location GEOMETRY
	)

EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ZoneStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ZoneTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ZoneID'
GO

INSERT INTO zone.Zone
	(ZoneName, ZoneDescription, ZoneTypeID, TerritoryTypeCode, TerritoryID, Location)
SELECT
	CA.CommunityAssetName, 
	CA.CommunityAssetDescription, 
	CA.ZoneTypeID, 

	CASE
		WHEN CA.CommunityID > 0
		THEN 'Community'
		ELSE 'Province'
	END,

	CASE
		WHEN CA.CommunityID > 0
		THEN CA.CommunityID
		ELSE CA.ProvinceID
	END,

	CA.Location
FROM dbo.CommunityAsset CA
	JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		AND CAT.CommunityAssetTypeName = 'Zone'
GO
--End table zone.Zone