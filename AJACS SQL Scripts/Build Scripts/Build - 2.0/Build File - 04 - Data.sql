USE AJACS2
GO

--Begin table dbo.Community
UPDATE C
SET C.ImpactDecisionID = 1
FROM dbo.Community C
WHERE C.ImpactDecisionID NOT IN (3,4)
GO

DECLARE @nComponentStatusID INT

SELECT @nComponentStatusID = CS.ComponentStatusID
FROM dropdown.ComponentStatus CS
WHERE CS.ComponentStatusName = 'No'

UPDATE C
SET 
	C.CommunityComponentStatusID = CASE WHEN C.CommunityComponentStatusID = 0 THEN @nComponentStatusID ELSE C.CommunityComponentStatusID END,
	C.JusticeComponentStatusID = CASE WHEN C.JusticeComponentStatusID = 0 THEN @nComponentStatusID ELSE C.JusticeComponentStatusID END,
	C.PoliceComponentStatusID = CASE WHEN C.PoliceComponentStatusID = 0 THEN @nComponentStatusID ELSE C.PoliceComponentStatusID END
FROM dbo.Community C
GO
--End table dbo.Community

--Begin table dbo.Contact
UPDATE C
SET C.CommunityID = asset.GetCommunityIDByAssetUnitID(C.AssetUnitID)
FROM dbo.Contact C
WHERE C.AssetUnitID > 0
GO
--End table dbo.Contact

--Begin table dbo.DocumentEntity
UPDATE DE
SET DE.EntityTypeCode = 'SpotReportDocument'
FROM dbo.DocumentEntity DE
WHERE DE.EntityTypeCode = 'SpotReport'
GO

UPDATE DE
SET DE.EntityTypeCode = 'SpotReport'
FROM dbo.DocumentEntity DE
WHERE DE.EntityTypeCode = 'SpotReportDocument'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 'SpotReport'
FROM dbo.DocumentEntity DE
WHERE DE.EntityTypeCode = 'SpotReportDocument'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 
	CASE
		WHEN D.DocumentDescription = 'Participants Document'
		THEN 'ParticipantDocument'
		WHEN D.DocumentDescription = 'Trainer 1 Document'
		THEN 'Trainer1Document'
		WHEN D.DocumentDescription = 'Trainer 2 Document'
		THEN 'Trainer2Document'
	END
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'Class'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 'ARAPDocument'
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'Community'
		AND D.DocumentDescription LIKE '%ARAP%'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 
	CASE
		WHEN D.DocumentDescription = 'RAP Report'
		THEN 'RAPReport'
		WHEN D.DocumentDescription = 'Finalized CSAP'
		THEN 'FinalizedCSAP'
		WHEN D.DocumentDescription = 'Finalized SCA'
		THEN 'FinalizedSCA'
		WHEN D.DocumentDescription = 'Key Achievement'
		THEN 'KeyAchievement'
		WHEN D.DocumentDescription = 'TOR Document'
		THEN 'TORDocument'
	END
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'CommunityRound'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 
	CASE
		WHEN D.DocumentDescription = 'Final Vendor/Resource Partner Report'
		THEN 'VendorFinalReport'
		WHEN D.DocumentDescription = 'M&E final report'
		THEN 'MonitoringFinalReport'
	END
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'ConceptNote'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 'GOVIDDocument'
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'Contact'
		AND D.DocumentDescription = 'ID Document'
GO

UPDATE DE
SET DE.EntityTypeCode = 'SpotReport'
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'SpotReport_v2'
GO

--End table dbo.DocumentEntity

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'Community' AND ET.WorkflowActionCode = 'ImpactChange')
	BEGIN

	INSERT INTO EmailTemplate 
		(EntityTypeCode, WorkflowActionCode, EmailText) 
	VALUES 
		('Community', 'ImpactChange', '<p>[[Community]]</p><p>The above community has changed status. For more detail view the community profile [[CommunityLink]] or contact the MER team.</p>')

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Community' AND ETF.PlaceHolderText = '[[Community]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField 
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder) 
	VALUES 
		('Community', '[[Community]]', 'Community', 0),
		('Community', '[[CommunityLink]]', 'Community Link', 0)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
DELETE ET
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode IN 
	(
	'Atmospheric',
	'CommunityMemberSurvey',
	'CommunityMemberSurveyAdd',
	'CommunityProvinceEngagementCommunity',
	'CommunityProvinceEngagementProvince',
	'CommunityProvinceEngagementUpdate',
	'DailyReport',
	'FIFCommunity',
	'FIFProvince',
	'FIFUpdate',
	'FocusGroupSurvey',
	'JusticeCommunity',
	'JusticeProvince',
	'JusticeUpdate',
	'KeyEvent',
	'KeyInformantSurvey',
	'PoliceEngagementUpdate',
	'RAPData',
	'RapidPerceptionSurvey',
	'StakeholderGroupSurvey',
	'StationCommanderSurvey',
	'Survey',
	'SurveyQuestion',
	'SurveyResponse',
	'Team'
	)

EXEC utility.EntityTypeAddUpdate 'Asset', 'Asset', NULL
GO
EXEC utility.EntityTypeAddUpdate 'AssetUnit', 'Asset Unit', NULL
GO
EXEC utility.EntityTypeAddUpdate 'EventLog', 'Event Log', NULL
GO
EXEC utility.EntityTypeAddUpdate 'MediaReport', 'Media Report', NULL
GO
EXEC utility.EntityTypeAddUpdate 'Workplan', 'Work Plan', NULL
GO
EXEC utility.EntityTypeAddUpdate 'Zone', 'Zone', NULL
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN 
	(
	'Atmospheric',
	'AtmosphericReportList',
	'CommunityAssetList',
	'CommunityMemberSurveyAdd',
	'CommunityProvinceEngagementReport',
	'DailyReportList',
	'DownloadRAPData',
	'FIFUpdate',
	'Finding',
	'RecommendationManagement',
	'RecommendationList',
	'RecommendationUpdate',	
	'FocusGroupSurveyAdd',
	'JusticeUpdate',
	'KeyInformantSurveyAdd',
	'PoliceEngagementUpdate',
	'QuestionList',
	'RAPData',
	'RAPDataList',
	'RAPDelivery',
	'RAPImplementation',
	'RAPTools',
	'StakeholderGroupSurveyAdd',
	'StationCommanderSurveyAdd',
	'SurveyList',
	'Surveys',
	'TeamList'
	)

UPDATE MI
SET MI.MenuItemCode = 'ForceList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'Force'
GO

UPDATE MI
SET MI.MenuItemCode = 'IncidentList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'IncidentReport'
GO

UPDATE MI
SET MI.MenuItemCode = 'MediaReportList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'MediaReport'
GO

UPDATE MI
SET MI.MenuItemCode = 'OperationsSupport'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'OpperationsSupport'
GO

UPDATE MI
SET MI.MenuItemCode = 'SpotReportList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'SpotReport'
GO

UPDATE MI
SET MI.MenuItemLink = '/contact/policepaymentlist'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'PolicePaymentList'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='OperationsSupport', @NewMenuItemCode='WorkplanList', @NewMenuItemLink='/workplan/list', @NewMenuItemText='Workplans', @AfterMenuItemCode='Activity', @PermissionableLineageList='Workplan.List'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='AssetList', @NewMenuItemLink='/asset/list', @NewMenuItemText='Assets', @PermissionableLineageList='Asset.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='WeeklyReport', @NewMenuItemLink='/weeklyreport/addupdate', @NewMenuItemText='Atmospheric Report'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='IncidentList', @NewMenuItemLink='/incident/list', @NewMenuItemText='Incident Reports', @PermissionableLineageList='Incident.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='MediaReportList', @NewMenuItemLink='/mediareport/list', @NewMenuItemText='Media Reports', @PermissionableLineageList='MediaReport.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='SpotReportList', @NewMenuItemLink='/spotreport/list', @NewMenuItemText='Spot Reports', @PermissionableLineageList='SpotReport.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='ZoneList', @NewMenuItemLink='/zone/list', @NewMenuItemText='Zones', @PermissionableLineageList='Zone.List'
GO

UPDATE MI
SET MI.DisplayOrder = 
	CASE
		WHEN MI.MenuItemCode = 'AssetList'
		THEN 1
		WHEN MI.MenuItemCode = 'WeeklyReport'
		THEN 2
		WHEN MI.MenuItemCode = 'IncidentList'
		THEN 3
		WHEN MI.MenuItemCode = 'MediaReportList'
		THEN 4
		WHEN MI.MenuItemCode = 'SpotReportList'
		THEN 5
		WHEN MI.MenuItemCode = 'ZoneList'
		THEN 6
	END
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode IN ('AssetList','WeeklyReport','IncidentList','MediaReportList','SpotReportList','ZoneList')
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='OpperationsSupport'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO

UPDATE MI
SET MI.MenuItemText = 'Operations Support'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode IN ('OperationsSupport','OpperationsSupport')
GO
--End table dbo.MenuItem

--Begin table dropdown.AreaOfOperationType
UPDATE dropdown.AreaOfOperationType
SET IsActive = 0
WHERE AreaOfOperationTypeName = 'FSA Area of Operation'
GO

UPDATE dropdown.AreaOfOperationType
SET AreaOfOperationTypeName = 'Extremist Islamic Group Area of Operation'
WHERE AreaOfOperationTypeName = 'Violent Extremists Area of Operation'
GO
--End table dropdown.AreaOfOperationType

--Begin table dropdown.ConceptNoteStatus
IF NOT EXISTS (SELECT 1 FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Amended')
	BEGIN

	INSERT INTO dropdown.ConceptNoteStatus 
		(ConceptNoteStatusCode, ConceptNoteStatusName, DisplayOrder, IsActive)
	VALUES 
		('Amended','Amended',5,1)

	END
--ENDIF
GO
--End table dropdown.ConceptNoteStatus

--Begin table dropdown.ConceptNoteType
IF NOT EXISTS (SELECT 1 FROM dropdown.ConceptNoteType CNT WHERE CNT.ConceptNoteTypeName = 'Communication')
	BEGIN

	INSERT INTO dropdown.ConceptNoteType 
		(ConceptNoteTypeName, IsActive)
	VALUES 
		('Communication',1),
		('MER',1)

	END
--ENDIF
GO

UPDATE dropdown.ConceptNoteType
SET IsActive = 0
WHERE ConceptNoteTypeName IN ('RAP','Rapid Assessment Program','M&E','Research')
GO

UPDATE CN
SET CN.ConceptNoteTypeID = (SELECT CNT1.ConceptNoteTypeID FROM dropdown.ConceptNoteType CNT1 WHERE CNT1.ConceptNoteTypeName = 'MER')
FROM dbo.ConceptNote CN
	JOIN dropdown.ConceptNoteType CNT2 ON CNT2.ConceptNoteTypeID = CN.ConceptNoteTypeID
		AND CNT2.ConceptNoteTypeName IN ('M&E', 'RAP','Research')
GO

UPDATE WPA
SET WPA.ConceptNoteTypeID = (SELECT CNT1.ConceptNoteTypeID FROM dropdown.ConceptNoteType CNT1 WHERE CNT1.ConceptNoteTypeName = 'MER')
FROM workplan.WorkplanActivity WPA
	JOIN dropdown.ConceptNoteType CNT2 ON CNT2.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		AND CNT2.ConceptNoteTypeName IN ('M&E', 'RAP','Research')
GO

UPDATE dropdown.ConceptNoteType
SET DisplayOrder = 0
GO
--End table dropdown.ConceptNoteType

--Begin table dropdown.ContactType
UPDATE dropdown.ContactType
SET DisplayOrder = 0
GO
--End table dropdown.ContactType

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'ARAP')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode, DocumentTypeName, DocumentTypePermissionCode)
	VALUES
		('ARAP', '523 ARAP Document', '523')

	END
--ENDIF
GO

UPDATE DT
SET DT.DocumentGroupID = (SELECT DG.DocumentGroupID FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = '500-599')
FROM dropdown.DocumentType DT
WHERE LEFT(DT.DocumentTypePermissionCode, 1) = '5'
GO
--End table dropdown.DocumentType

--Begin table dropdown.DonorMeetingComponent
UPDATE DMC
SET DMC.IsActive = 0
FROM dropdown.DonorMeetingComponent DMC
WHERE DMC.DonorMeetingComponentName IN ('FIF')
GO
--End table dropdown.DonorMeetingComponent

--Begin table dropdown.FundingSource
UPDATE FS
SET FS.IsActive = 0
FROM dropdown.FundingSource FS
WHERE FS.FundingSourceName IN ('US - CSO', 'US - OPS')
GO

UPDATE FS
SET FS.FundingSourceName = 'EU - North'
FROM dropdown.FundingSource FS
WHERE FS.FundingSourceName IN ('EU')
GO

UPDATE FS
SET FS.FundingSourceName = 'US - North'
FROM dropdown.FundingSource FS
WHERE FS.FundingSourceName IN ('US - NEA')
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.FundingSource FS WHERE FS.FundingSourceName = 'EU - South')
	BEGIN

	INSERT INTO dropdown.FundingSource
		(FundingSourceName, IsActive)
	VALUES
		('EU - South', 1),
		('US - South', 1)
	END
--ENDIF
GO

UPDATE FS
SET FS.DisplayOrder = 0
FROM dropdown.FundingSource FS
GO
--End table dropdown.FundingSource

--Begin table dropdown.ImpactDecision
UPDATE ID
SET ID.ImpactDecisionName = 'Pending'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'Under Review'
GO

UPDATE ID
SET ID.HexColor = '#FF0000'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'No'
GO

UPDATE ID
SET ID.HexColor = '#FFA500'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'Pending'
GO

UPDATE ID
SET ID.HexColor = '#00BB54'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'Yes'
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.ResourceProvider
IF NOT EXISTS (SELECT 1 FROM dropdown.ResourceProvider RP WHERE RP.ResourceProviderName = 'Local')
	BEGIN

	INSERT INTO dropdown.ResourceProvider
		(ResourceProviderName)
	VALUES
		('Local'),
		('Private Donor')

	END
--ENDIF
GO
--End table dropdown.ResourceProvider

--Begin table dropdown.StatusChange
TRUNCATE TABLE dropdown.StatusChange
GO

SET IDENTITY_INSERT dropdown.StatusChange ON
GO

INSERT INTO dropdown.StatusChange
	(StatusChangeID, StatusChangeName)
VALUES
	(0, NULL),
	(1, 'No to Pending'),
	(2, 'No to Yes'),
	(3, 'Pending to No'),
	(4, 'Pending to Yes'),
	(5, 'Yes to No'),
	(6, 'Yes to Pending')
GO

SET IDENTITY_INSERT dropdown.StatusChange OFF
GO
--End table dropdown.StatusChange

--Begin table dropdown.UnitType
UPDATE dropdown.UnitType
SET IsActive = 0
WHERE UnitTypeName IN ('Unit', 'Sub-Unit')
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.UnitType UT WHERE UT.UnitTypeName = 'Battalion')
	BEGIN

	INSERT INTO dropdown.UnitType
		(UnitTypeName, DisplayOrder)
	VALUES
		('Battalion', 3),
		('Company', 4)
		
	END
--ENDIF
GO
--End table dropdown.UnitType

--Begin table eventlog.EventLog
DELETE EL
FROM eventlog.EventLog EL
WHERE EL.EntityTypeCode IN ('Atmospheric')
GO
--End table eventlog.EventLog

--Begin table procurement.DistributedInventory
UPDATE DI
SET DI.EndUserEntityTypeCode = 'Asset'
FROM procurement.DistributedInventory DI
WHERE DI.EndUserEntityTypeCode = 'CommunityAsset'
GO
--End table dropdown.UnitType

UPDATE P
SET P.PermissionableGroupID = (SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Implementation')
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityRound','CommunityRoundActivity')
GO

UPDATE P
SET 
	P.IsGlobal = 0,
	P.IsSuperAdministrator = 1
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('DonorDecision','Zone')
GO

UPDATE P
SET P.IsActive = 0
FROM permissionable.Permissionable P
	JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
		AND PG.PermissionableGroupName = 'Insight & Understanding'
GO

UPDATE P
SET P.MethodName = 'List'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Community.View.Export'
GO

UPDATE P
SET P.MethodName = 'PolicePaymentList'
FROM permissionable.Permissionable P
WHERE P.MethodName = 'PaymentList'
GO

UPDATE PP
SET PP.PermissionableLineage = 'Community.List.Export'
FROM permissionable.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'Community.View.Export'
GO

UPDATE P
SET P.MethodName = 'AddUpdate'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Risk.List.Export'
GO

UPDATE PP
SET PP.PermissionableLineage = 'Risk.AddUpdate.Export'
FROM permissionable.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'Risk.List.Export'
GO

UPDATE P
SET 
	P.MethodName = 'List',
	P.PermissionCode = 'AddContactStipendPaymentContacts'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Contact.ImportPayees'
GO

UPDATE PP
SET PP.PermissionableLineage = 'Contact.List.AddContactStipendPaymentContacts'
FROM permissionable.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'Contact.ImportPayees'
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityMemberSurvey','CommunityProvinceEngagement','CommunityProvinceEngagementUpdate','DailyReport','FIFUpdate','FocusGroupSurvey','Justice','KeyEvent','KeyInformantSurvey','PoliceEngagement','RAPData','RapidPerceptionSurvey','StakeholderGroupSurvey','StationCommanderSurvey','Team')
GO

DELETE PG
FROM permissionable.PermissionableGroup PG
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableGroupID = PG.PermissionableGroupID
	)
GO

EXEC utility.SavePermissionable 'Asset', 'AddUpdate', NULL, 'Asset.AddUpdate', 'Add / edit an asset', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Asset', 'List', NULL, 'Asset.List', 'View the list of assets', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Asset', 'View', NULL, 'Asset.View', 'View an asset', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Contact', 'AddContactStipendPaymentContacts', NULL, 'Contact.AddContactStipendPaymentContacts', 'Add contacts to a stipend payment list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'CashHandOverExport', 'Contact.JusticePaymentList.CashHandOverExport', 'Allow access to the cash hand over report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'OpFundsReport', 'Contact.JusticePaymentList.OpFundsReport', 'Allow access to the op funds report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendActivity', 'Contact.JusticePaymentList.StipendActivity', 'Allow access to the stipend activity report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendPaymentReport', 'Contact.JusticePaymentList.StipendPaymentReport', 'Close out the stipend justice & police payment process', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'ReconcileContactStipendPayment', NULL, 'Contact.ReconcileContactStipendPayment', 'Allow access to the stipend payment report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'AddUpdate', NULL, 'EquipmentDistribution.AddUpdate', 'Add or update an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'Create an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Delete', NULL, 'EquipmentDistribution.Delete', 'Delete an active equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'MediaReport', 'AddUpdate', NULL, 'MediaReport.AddUpdate', 'Add / edit an media report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'List', NULL, 'MediaReport.List', 'View the list of media reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'List', 'Export', 'MediaReport.List.Export', 'Export selected media reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'View', NULL, 'MediaReport.View', 'View a media report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'Delete', NULL, 'PermissionableTemplate.Delete', 'Delete a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add or update a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'Delete', NULL, 'Workplan.Delete', 'Delete a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'List workplans', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Zone', 'AddUpdate', NULL, 'Zone.AddUpdate', 'Add / edit a zone', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Zone', 'List', NULL, 'Zone.List', 'View the list of zones', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Zone', 'View', NULL, 'Zone.View', 'View a zone', 0, 0, 'Research'
GO

EXEC utility.SavePermissionableGroup 'Research', 'Research'
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO

TRUNCATE TABLE workplan.Workplan
TRUNCATE TABLE workplan.WorkplanActivity
GO

USE AJACS2
GO
SET IDENTITY_INSERT workplan.Workplan ON 

GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (1, N'***TEST**** Workplan Number One ***TEST*** Please Disregard***', CAST(N'2016-10-14' AS Date), CAST(N'2016-10-31' AS Date), N'US-ConNo - 9', N'UK - ConNo - 3', 0, 1, CAST(1.35 AS Numeric(18, 2)))
GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (2, N'October 2016 - March 2017', CAST(N'2016-10-01' AS Date), CAST(N'2017-03-31' AS Date), N'CN37100', N'CPG/1129/2016', 0, 1, CAST(0.74 AS Numeric(18, 2)))
GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (3, N'10/20/2016 - WorkPlan Test 3 ', CAST(N'2016-10-20' AS Date), CAST(N'2016-10-27' AS Date), N'US-9000', N'UK-9000', 0, 1, CAST(1.35 AS Numeric(18, 2)))
GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (4, N'***TEST***10212016: OCNDSC-QA WORKPLAN TEST TWO***TEST***', CAST(N'2016-10-21' AS Date), CAST(N'2016-10-28' AS Date), N'WPT: 10212016', N'UK-CONNO: 98658', 0, 1, CAST(0.00 AS Numeric(18, 2)))
GO
SET IDENTITY_INSERT workplan.Workplan OFF
GO
SET IDENTITY_INSERT workplan.WorkplanActivity ON 

GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (17, NULL, 1, 0, 0, 0, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), NULL, NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (1, N'Pencils For Pupils Program', 1, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(1000.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), N'A Community Based Programs to provide basic school supplies for Children', N'THE LINK BETWEEN THE "CURRENT" AND "ORIGINAL" FIELDS IS HIGHLY SUSPECT AT THIS TIME.')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (13, N'Police - EU South', 1, 3, 6, 6, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(150000.00 AS Numeric(18, 2)), CAST(150000.00 AS Numeric(18, 2)), NULL, N'Test Linkage to AS')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (2, N'School Meal Program', 1, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), N'If a Student attends school, the Community has pooled resources for offering a Mid - Day Meal for aspiring students.', N'USD ALLOCATION FIELD COPIES CURRENT VALUE TO ORIGINAL VALUE FIELD AUTOMATICALLY AND CANNOT BE EDITED THEREAFTER')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (9, N'Aleppo Stipends ', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(1880000.00 AS Numeric(18, 2)), CAST(1880000.00 AS Numeric(18, 2)), N'Stipend and operating costs for existing 56 assets and approximately 2,000 officers together with an budgeted expansion of 20% together with the PRU.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (3, N'Equipment (European)', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(900000.00 AS Numeric(18, 2)), CAST(900000.00 AS Numeric(18, 2)), N'Procurement, delivery and distribution of equipment in line with identified station standards and needs. Will be completed over two phases in each province.
Round 1 Aleppo (November to January): USD 300,000
Round 2 Aleppo (January to March): USD 300,000
Round 1 Idlib (November to January): USD 300,000
Round 2 Idlib (January to March): USD 300,000
NB: 75% European and 25% US funded.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (4, N'Equipment (US)', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(300000.00 AS Numeric(18, 2)), CAST(300000.00 AS Numeric(18, 2)), N'Procurement, delivery and distribution of equipment in line with identified station standards and needs. Will be completed over two phases in each province.
Round 1 Aleppo (November to January): USD 300,000
Round 2 Aleppo (January to March): USD 300,000
Round 1 Idlib (November to January): USD 300,000
Round 2 Idlib (January to March): USD 300,000
NB: 75% European and 25% US funded.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (11, N'FSP Liaison Support', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(63000.00 AS Numeric(18, 2)), CAST(63000.00 AS Numeric(18, 2)), N'Support to 2 FSP Police Liaison Officers and 2 Police Support Services Officers to strengthen coordination and relationships between the FSP and AJACS. Also includes PRU Liaison Officer to work with PRU lead to ensure timely development of the PRU. ', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (8, N'Idlib Stipends', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(1200000.00 AS Numeric(18, 2)), CAST(1200000.00 AS Numeric(18, 2)), N'Stipend and operating costs for existing 31 assets and approx. 1,250 officers together with an budgted expansion of 20%. ', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (10, N'Meetings and Workshops', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(125000.00 AS Numeric(18, 2)), CAST(125000.00 AS Numeric(18, 2)), N'Support to Turkey-based meetings and workshops for FSP training and engagement. Assumes the FSP will provide/release new, qualified and competant officers to be trained and that border crossings are obtainable. Includes:
Monthly Management Meetings, Provincial Planning Meetings,  Cross-programme workshops, Province Command meetings, PDU planning,  
Leadership development - District Level and Community Policing workshops. ', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (7, N'Station/ Facilities Repairs and Refurbishments', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(800000.00 AS Numeric(18, 2)), CAST(800000.00 AS Numeric(18, 2)), N'Support to FSP infrastrucutre to repair, refurbish and improve. This includes existing stations, new stations and specialist faciltiies (e.g. training centers). Will be completed over four phases in each province with four projects per phase:', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (12, N'STTA/SME Support', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(543000.00 AS Numeric(18, 2)), CAST(543000.00 AS Numeric(18, 2)), N'Support to the FSP in delivering effective doctrine, plans, policies, procedures and standards. Expected to include 5 STTA/SMEs for 6 months of a joint AJACS/FSP 12 month project,  covering the following topics:              
SME 1: Doctrine Development 
SME 2: C2 Review and Implementation of  District Commands 
SME 3: Creation of Policies & Standards 
SME 4: Community Policing 
SME 5: Review FSP Support Structures (Procurement, Logistics, HR)', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (5, N'Training Delivery', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(212000.00 AS Numeric(18, 2)), CAST(212000.00 AS Numeric(18, 2)), N'Support to in-country delivery of training by the FSP and includes trainer stipends, trainee stipends and operating costs for the delivery of training. The following training will be delivered: Modules 1 & 2 in Aleppo and Idlib, PRU in Aleppo and Leadership in Aleppo & Idlib.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (6, N'Training Development', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(450000.00 AS Numeric(18, 2)), CAST(450000.00 AS Numeric(18, 2)), N'Development of training packages - module 2, PRU and leadership - using new training tools will be used to better capture the learning cycle in accordance with logframe. This will apply for both the cascade (trainer development) as well as the trainees.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (14, N'Tactical Pylon', 3, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), N'A WayPoint Pylon for enhanced navigation of Defense Forces.', N'OCNDSC-QA  
SUPER ADMIN
AJACS-35
***TESTING***')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (15, N'Tactical Pylon Signal Repeater', 3, 3, 6, 6, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(200.00 AS Numeric(18, 2)), CAST(200.00 AS Numeric(18, 2)), N'A signal reshaping and amplification device.', N'Sort Column seems to poplulate the "Original" Column Value.')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (16, N'Physical Fitness Training Program', 4, 3, 4, 6, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), N'An "Action Oriented" Physical Fitness Program that helps ensure optimal 
physical performance. ', NULL)
GO
SET IDENTITY_INSERT workplan.WorkplanActivity OFF
GO
