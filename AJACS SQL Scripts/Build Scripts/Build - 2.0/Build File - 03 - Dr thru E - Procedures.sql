USE AJACS2
GO


--Begin procedure dropdown.GetAssetStatusData
EXEC Utility.DropObject 'dropdown.GetAssetStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.12
-- Description:	A stored procedure to return data from the dropdown.AssetStatus table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetAssetStatusData
								 
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetStatusID,
		T.AssetStatusCode,
		T.AssetStatusName
	FROM dropdown.AssetStatus T
	WHERE (T.AssetStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetStatusName, T.AssetStatusID

END
GO
--End procedure dropdown.GetAssetStatusData

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID, 
		T.AssetTypeCategory,
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
	WHERE (T.AssetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeCategory, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetAssetUnitCostData
EXEC Utility.DropObject 'dropdown.GetAssetUnitCostData'
EXEC Utility.DropObject 'dropdown.GetAssetUnitCostRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.13
-- Description:	A stored procedure to return data from the dropdown.AssetUnitCost table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetAssetUnitCostData

@IncludeZero BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetUnitCostID,
		T.AssetUnitCostName
	FROM dropdown.AssetUnitCost T
	WHERE (T.AssetUnitCostID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetUnitCostName, T.AssetUnitCostID

END
GO
--End procedure dropdown.GetAssetUnitCostData

--Begin procedure dropdown.GetComponentStatusData
EXEC Utility.DropObject 'dropdown.GetComponentStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.08.23
-- Description:	A stored procedure to return data from the dropdown.ComponentStatus table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetComponentStatusData
								 
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ComponentStatusID,
		T.ComponentStatusName
	FROM dropdown.ComponentStatus T
	WHERE (T.ComponentStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ComponentStatusName, T.ComponentStatusID

END
GO
--End procedure dropdown.GetComponentStatusData

--Begin procedure dropdown.GetConceptNoteTypeData
EXEC Utility.DropObject 'dropdown.GetConceptNoteTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	A stored procedure to return data from the dropdown.ConceptNoteType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConceptNoteTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConceptNoteTypeCode, 
		T.ConceptNoteTypeID, 
		T.ConceptNoteTypeName
	FROM dropdown.ConceptNoteType T
	WHERE (T.ConceptNoteTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConceptNoteTypeName, T.ConceptNoteTypeID

END
GO
--End procedure dropdown.GetConceptNoteTypeData

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.18
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID, 
		T.ContactTypeCode, 
		T.ContactTypeName,
		T.IsStipend
	FROM dropdown.ContactType T
	WHERE (T.ContactTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dropdown.GetMediaReportSourceTypeData
EXEC utility.DropObject 'dropdown.GetMediaReportSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.07
-- Description:	A stored procedure to return data from the dropdown.MediaReportSourceType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetMediaReportSourceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportSourceTypeID,
		T.MediaReportSourceTypeName,
		T.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		T.MediaReportSourceTypeName AS SourceTypeName -- Aliased for the common source cfmodule
	FROM dropdown.MediaReportSourceType T
	WHERE (T.MediaReportSourceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportSourceTypeName, T.MediaReportSourceTypeID

END
GO
--End procedure dropdown.GetMediaReportSourceTypeData

--Begin procedure dropdown.GetMediaReportTypeData
EXEC utility.DropObject 'dropdown.GetMediaReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.07
-- Description:	A stored procedure to return data from the dropdown.MediaReportType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetMediaReportTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportTypeID,
		T.MediaReportTypeName
	FROM dropdown.MediaReportType T
	WHERE (T.MediaReportTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportTypeName, T.MediaReportTypeID

END
GO
--End procedure dropdown.GetMediaReportTypeData

--Begin procedure dropdown.GetPaymentGroupData
EXEC Utility.DropObject 'dropdown.GetPaymentGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.24
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	Added StipendTypeCode support
--
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	Added the (locked) option to YearMonthFormatted
-- =======================================================================================
CREATE PROCEDURE dropdown.GetPaymentGroupData

@IncludeZero BIT = 0,
@StipendTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(T.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(T.YearMonth, 4) + ' - ' + T.ProvinceName AS YearMonthFormatted,
		T.YearMonth + '-' + CAST(T.ProvinceID AS VARCHAR(10)) AS YearMonthProvince,

		CASE
			WHEN NOT EXISTS (SELECT 1 FROM dbo.ContactStipendPayment CSP WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = CAST(T.YearMonth AS INT) AND CSP.ProvinceID = T.ProvinceID AND CSP.StipendPaidDate IS NULL)
			THEN 1
			ELSE 0
		END AS IsLocked

	FROM
		(
		SELECT DISTINCT
			CAST((CSP.PaymentYear * 100 + CSP.PaymentMonth) AS CHAR(6)) AS YearMonth, 
			P.ProvinceID,
			P.ProvinceName
		FROM dbo.ContactStipendPayment CSP
			JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
				AND CSP.StipendTypeCode = @StipendTypeCode
		) T
	ORDER BY T.YearMonth DESC, 1

END
GO
--End procedure dropdown.GetPaymentGroupData

--Begin procedure dropdown.GetZoneStatusData
EXEC Utility.DropObject 'dropdown.GetZoneStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.12
-- Description:	A stored procedure to return data from the dropdown.ZoneStatus table
-- =================================================================================
CREATE PROCEDURE dropdown.GetZoneStatusData
								 
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ZoneStatusID,
		T.ZoneStatusName
	FROM dropdown.ZoneStatus T
	WHERE (T.ZoneStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ZoneStatusName, T.ZoneStatusID

END
GO
--End procedure dropdown.GetZoneStatusData

--Begin procedure eventlog.LogAssetAction
EXEC utility.DropObject 'eventlog.LogAssetAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Asset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogAssetActionTable
		--ENDIF
		
		SELECT *
		INTO #LogAssetActionTable
		FROM asset.Asset CA
		WHERE CA.AssetID = @EntityID
		
		ALTER TABLE #LogAssetActionTable DROP COLUMN Location

		DECLARE @cAssetUnits NVARCHAR(MAX)

		SELECT 
			@cAssetUnits = COALESCE(@cAssetUnits, '') + D.AssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetUnit'), ELEMENTS) AS AssetUnit
			FROM asset.AssetUnit T 
			WHERE T.AssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Asset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(('<Location>' + CAST(CA.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<AssetUnits>' + ISNULL(@cAssetUnits  , '') + '</AssetUnits>') AS XML)
			FOR XML RAW('Asset'), ELEMENTS
			)
		FROM #LogAssetActionTable T
			JOIN asset.Asset CA ON CA.AssetID = T.AssetID

		DROP TABLE #LogAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAssetAction

--Begin procedure eventlog.LogMediaReportAction
EXEC utility.DropObject 'eventlog.LogMediaReportAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.06.24
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogMediaReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'MediaReport'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cMediaReportSources VARCHAR(MAX) 
	
		SELECT 
			@cMediaReportSources = COALESCE(@cMediaReportSources, '') + D.MediaReportSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('MediaReportSource'), ELEMENTS) AS MediaReportSource
			FROM mediareport.MediaReportSource T 
			WHERE T.MediaReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT
			T.*, 
			CAST(('<MediaReportSources>' + ISNULL(@cMediaReportSources, '') + '</MediaReportSources>') AS XML)
			FOR XML RAW('MediaReport'), ELEMENTS
			)
		FROM mediareport.MediaReport T
		WHERE T.MediaReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogMediaReportAction

--Begin procedure eventlog.LogWorkplanAction
EXEC utility.DropObject 'eventlog.LogWorkplanAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWorkplanAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Workplan',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cWorkplanActivities NVARCHAR(MAX)

		SELECT 
			@cWorkplanActivities = COALESCE(@cWorkplanActivities, '') + D.WorkplanActivity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkplanActivity'), ELEMENTS) AS WorkplanActivity
			FROM workplan.WorkplanActivity T 
			WHERE T.WorkplanID = @EntityID
			) D	

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Workplan',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<WorkplanActivities>' + ISNULL(@cWorkplanActivities, '') + '</WorkplanActivities>') AS XML)
			FOR XML RAW('Workplan'), ELEMENTS
			)
		FROM workplan.Workplan T
		WHERE T.WorkplanID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWorkplanAction

--Begin procedure eventlog.LogZoneAction
EXEC utility.DropObject 'eventlog.LogZoneAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogZoneAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Zone',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogZoneActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogZoneActionTable
		--ENDIF
		
		SELECT *
		INTO #LogZoneActionTable
		FROM zone.Zone Z
		WHERE Z.ZoneID = @EntityID
		
		ALTER TABLE #LogZoneActionTable DROP COLUMN Location

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Zone',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(('<Location>' + CAST(Z.Location AS VARCHAR(MAX)) + '</Location>') AS XML)
			FOR XML RAW('Zone'), ELEMENTS
			)
		FROM #LogZoneActionTable T
			JOIN zone.Zone Z ON Z.ZoneID = T.ZoneID

		DROP TABLE #LogZoneActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogZoneAction
