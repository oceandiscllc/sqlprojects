USE AJACS
GO

--Begin function asset.GetAssetUnitEquipmentEligibilityByAssetUnitID
EXEC utility.DropObject 'dbo.GetAssetUnitEquipmentEligibilityByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityByAssetUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit Equipment eligibility data
-- =================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibilityByAssetUnitID
(
@AssetUnitID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN C.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND AU.AssetUnitID = @AssetUnitID
		JOIN dbo.Contact C ON C.ContactID = AU.CommanderContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function asset.GetAssetUnitEquipmentEligibilityByAssetUnitID

--Begin function asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID
EXEC utility.DropObject 'dbo.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit Equipment eligibility data
-- =================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID
(
@AssetUnitID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not equipment elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE 
		 		WHEN NOT EXISTS (SELECT 1 FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID)
		 		THEN 'Department has no department head assigned,'
		 		WHEN (SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID) IS NULL 
		 			OR (SELECT DATEADD(m, 6, C.USVettingExpirationDate) FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID) < getDate()  
		 		THEN 'Assigned asset department head has expired vetting,' 
		 		ELSE '' 
		 	END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND AU.AssetUnitID = @AssetUnitID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equipment Eligible'
	ELSE
		SET @cReturn = 'Equipment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID

--Begin function asset.GetCommunityIDByAssetUnitID
EXEC utility.DropObject 'asset.GetCommunityIDFromAssetID'
EXEC utility.DropObject 'asset.GetCommunityIDFromAssetUnitID'
EXEC utility.DropObject 'asset.GetCommunityIDByAssetUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the community associated with an asset unit
-- ==========================================================================
CREATE FUNCTION asset.GetCommunityIDByAssetUnitID
(
@AssetUnitID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCommunityID INT = 0

	SELECT 
		@nCommunityID = A.CommunityID
	FROM asset.Asset A
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
			AND AU.AssetUnitID = @AssetUnitID

	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function asset.GetCommunityIDByAssetUnitID

--Begin function dbo.FormatProgramReportReferenceCode
EXEC utility.DropObject 'dbo.FormatProgramReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted program report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ===========================================================================

CREATE FUNCTION dbo.FormatProgramReportReferenceCode
(
@ProgramReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-PR-' + RIGHT('0000' + CAST(@ProgramReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatProgramReportReferenceCode

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-SR-' + RIGHT('0000' + CAST(@SpotReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function dbo.FormatStaticGoogleMapForCommunityExport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForCommunityExport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date:	2015.12.02
-- Description:	A function to return communities assets for placement on a google map
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForCommunityExport
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	DECLARE @CommunityMarker VARCHAR(MAX)=''

	SELECT
		@CommunityMarker += '&markers=icon:' 
		+ replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + REPLACE(ID.HexColor, '#', '') + '.png' + '|' 
		+ CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.CommunityID = @CommunityID
	
	SET @Gresult = @CommunityMarker

	SELECT
		@GResult += '&markers=icon:' 
		+ replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + AT.Icon + '|' 
		+ CAST(ISNULL(A.Location.STY ,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(A.Location.STX, 0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN asset.Asset A ON A.CommunityID = C.communityID  
			AND C.CommunityID = @CommunityID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID 
			AND AT.AssetTypeID != 0

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForCommunityExport

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@EquipmentDistribbtionID INT,
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @tSource TABLE (CommunityID INT, ProvinceID INT)

	INSERT INTO @tSource
		(CommunityID, ProvinceID)
	SELECT 
		A.CommunityID,
		0 AS ProvinceID				
	FROM procurement.DistributedInventory DI
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = DI.EndUserEntityID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND DI.EndUserEntityTypeCode = 'AssetUnit'

	UNION

	SELECT 
		C.CommunityID,
		0 AS ProvinceID				
	FROM procurement.DistributedInventory DI
		JOIN Contact C ON C.Contactid = DI.EndUserEntityID 
			AND DI.EquipmentDistributionID = @EquipmentDistribbtionID 
			AND DI.EndUserEntityTypeCode = 'Contact'

	UNION 

	SELECT 

		CASE F.TerritoryTypeCode
			WHEN 'Province' 
			THEN 0
			ELSE F.TerritoryID 
		END AS CommunityID,

		CASE F.TerritoryTypeCode
			WHEN 'Community' 
			THEN dbo.GetProvinceIDByCommunityID(TerritoryID)
			ELSE F.TerritoryID 
		END AS ProvinceID 

	FROM procurement.DistributedInventory DI
		JOIN Force.Force F ON F.ForceID = DI.EndUserEntityID 
			AND DI.EquipmentDistributionID = @EquipmentDistribbtionID 
			AND DI.EndUserEntityTypeCode = 'Force'

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(ISNULL(C.Latitude, 0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	FROM @tSource S
		JOIN dbo.Community C ON S.CommunityID = C.CommunityID  
			AND C.ProvinceID = @ProvinceID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / LEN('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan

--Begin function dbo.FormatWeeklyReportReferenceCode
EXEC utility.DropObject 'dbo.FormatWeeklyReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return a formatted Weekly report reference code
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ==========================================================================

CREATE FUNCTION dbo.FormatWeeklyReportReferenceCode
(
@WeeklyReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-WR-' + RIGHT('0000' + CAST(@WeeklyReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatWeeklyReportReferenceCode

--Begin dbo dbo.GetCommunityIDFromContactID
EXEC utility.DropObject 'dbo.GetCommunityIDFromContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the community associated with a contact
-- ======================================================================
CREATE FUNCTION dbo.GetCommunityIDFromContactID
(
@ContactID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCommunityID INT = 0

	SELECT 
		@nCommunityID = C.CommunityID
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function dbo.GetCommunityIDFromContactID

--Begin function dbo.GetCommunityIDFromForceID
EXEC utility.DropObject 'dbo.GetCommunityIDFromForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the community associated with a force
-- ====================================================================
CREATE FUNCTION dbo.GetCommunityIDFromForceID
(
@ForceID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @cTerritoryTypeCode VARCHAR(50)
	DECLARE @nCommunityID INT = 0
	DECLARE @nTerritoryID INT = 0
	
	SELECT 
		@cTerritoryTypeCode = F.TerritoryTypeCode,
		@nTerritoryID = F.TerritoryID
	FROM force.Force F
	WHERE F.ForceID = @ForceID

	IF @cTerritoryTypeCode = 'Community'
		SET @nCommunityID = ISNULL(@nTerritoryID, 0)
	--ENDIF

	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function dbo.GetCommunityIDFromForceID

--Begin function dbo.GetConceptNoteTotalCost
EXEC utility.DropObject 'dbo.GetConceptNoteTotalCost'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.21
-- Description:	A function to return the total budget and equipment cost for a comcept note
-- ========================================================================================

CREATE FUNCTION dbo.GetConceptNoteTotalCost
(
@ConceptNoteID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nTotalCost NUMERIC(18,2)

	SELECT @nTotalCost = SUM(D.TotalCost)
	FROM
		(
		(SELECT ISNULL(SUM(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost), 0) AS TotalCost FROM dbo.ConceptNoteBudget CNB WHERE CNB.ConceptNoteID = @ConceptNoteID)

		UNION ALL

		(SELECT ISNULL(SUM(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0) AS TotalCost FROM dbo.ConceptNoteEquipmentCatalog CNEC JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID AND CNEC.ConceptNoteID = @ConceptNoteID)
		) D

	RETURN ISNULL(@nTotalCost, 0)

END
GO
--End function dbo.GetConceptNoteTotalCost

--Begin function dbo.GetConceptNoteVersionNumberByConceptNoteID
EXEC utility.DropObject 'dbo.GetConceptNoteVersionNumberByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Kevin Ross
-- Create date:	2016.09.27
-- Description:	A function to return the latest version number of a concept note
-- =============================================================================

CREATE FUNCTION dbo.GetConceptNoteVersionNumberByConceptNoteID
(
@ConceptNoteID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nVersionNumber INT;

	WITH HD AS
		(
		SELECT 
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			1 AS Depth
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			HD.Depth + 1 AS Depth
		FROM dbo.ConceptNote CN
			JOIN HD ON CN.ConceptNoteID = HD.AmendedConceptNoteID
		)

	SELECT @nVersionNumber = (SELECT TOP 1 MAX(HD.Depth) OVER() - HD.Depth + 1 AS InverseDepth
	FROM HD
	ORDER BY InverseDepth DESC)

	RETURN ISNULL(@nVersionNumber, 0)

END
GO
--End function dbo.GetConceptNoteVersionNumberByConceptNoteID

--Begin function dbo.GetContactCommunityByContactID
EXEC utility.DropObject 'dbo.GetContactLocationByContactID'
EXEC utility.DropObject 'dbo.GetContactCommunityByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to return the name of a contact's community or province from a ContactID
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Removed ProvinceID
-- ================================================================================================

CREATE FUNCTION dbo.GetContactCommunityByContactID
(
@ContactID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)
	
	SELECT @cRetVal = C2.CommunityName
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.ContactID = @ContactID
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.GetContactCommunityByContactID

--Begin function dbo.GetContactEquipmentEligibilityByContactID
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityByContactID
(
@ContactID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT


	SELECT @nIsCompliant = 
		CASE
			WHEN C.CommunityID > 0
			THEN dbo.IsCommunityStipendEligible(C.CommunityID)
			ELSE 
				(
				SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
				FROM asset.Asset A
					JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
						AND AU.AssetUnitID = C.AssetUnitID
				)
			END
		 + CASE WHEN C.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 2
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibilityByContactID

--Begin function dbo.GetContactEquipmentEligibilityNotesByContactID
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityNotesByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact equpiment eligibility data
-- ================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityNotesByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cReturn =
		CASE
			WHEN C.CommunityID > 0
			THEN 
				CASE
					WHEN dbo.IsCommunityStipendEligible(C.CommunityID) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			WHEN C.AssetUnitID > 0
			THEN
				CASE
					WHEN 
						(
						SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
						FROM asset.Asset A
							JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
								AND AU.AssetUnitID = C.AssetUnitID
						) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			END
		 + CASE WHEN C.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C.USVettingExpirationDate) >= getDate() THEN '' ELSE 'Contact has expired vetting,' END 
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equpiment Eligible'
	ELSE
		SET @cReturn = 'Equpiment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactEquipmentEligibilityNotesByContactID

--Begin function dbo.GetContactStipendEligibilityByContactID
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityByContactID
(
@ContactID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN C2.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C2.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND C1.ContactID = @ContactID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibilityByContactID

--Begin function dbo.GetContactStipendEligibilityNotesByContactID
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotesByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotesByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE 
		 		WHEN NOT EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID)
		 		THEN 'Department has no department head assigned,'
		 		WHEN (SELECT C1.USVettingExpirationDate FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) IS NULL 
		 			OR (SELECT DATEADD(m, 6, C1.USVettingExpirationDate) FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) < getDate()  
		 		THEN 'Assigned asset department head has expired vetting,' 
		 		ELSE '' 
		 	END
	FROM dbo.Contact C2
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C2.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND C2.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotesByContactID

--Begin function dbo.GetEventNameByEventCode
EXEC utility.DropObject 'dbo.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
--
-- Author:			Todd Pires
-- Create date:	2015.04.30
-- Description:	Added failedlogin support
-- ====================================================================
CREATE FUNCTION dbo.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'finalize'
			THEN 'Finalize'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'read'
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			WHEN @EventCode = 'vettingupdate'
			THEN 'Vetting Update'
			WHEN @EventCode = 'emailed'
			THEN 'Document Emailed'
			ELSE @EventCode + ' not found'
		END

	RETURN @EventCodeName

END
GO
--End function dbo.GetEventNameByEventCode

--Begin function dbo.GetLastVettingOutcome
EXEC utility.DropObject 'dbo.GetLastVettingOutcome'
GO
--End function dbo.GetLastVettingOutcome

--Begin dbo dbo.GetProvinceIDByContactID
EXEC utility.DropObject 'dbo.GetProvinceIDFromContactID'
EXEC utility.DropObject 'dbo.GetProvinceIDByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the province associated with a contact
--
-- Author:		Todd Pires
-- Create date:	2016.09.29
-- Description:	Refactored to use the CommunityID
-- =====================================================================
CREATE FUNCTION dbo.GetProvinceIDByContactID
(
@ContactID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nProvinceID INT = 0

	SELECT 
		@nProvinceID = C2.ProvinceID
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.ContactID = @ContactID

	RETURN ISNULL(@nProvinceID, 0)

END
GO
--End function dbo.GetProvinceIDByContactID

--Begin function dbo.GetProvinceIDFromForceID
EXEC utility.DropObject 'dbo.GetProvinceIDFromForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the province associated with a force
-- ===================================================================
CREATE FUNCTION dbo.GetProvinceIDFromForceID
(
@ForceID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @cTerritoryTypeCode VARCHAR(50)
	DECLARE @nCommunityID INT = 0
	DECLARE @nProvinceID INT = 0
	DECLARE @nTerritoryID INT = 0
	
	SELECT 
		@cTerritoryTypeCode = F.TerritoryTypeCode,
		@nTerritoryID = F.TerritoryID
	FROM force.Force F
	WHERE F.ForceID = @ForceID

	IF @cTerritoryTypeCode = 'Province'
		SET @nProvinceID = ISNULL(@nTerritoryID, 0)
	ELSE
		SET @nProvinceID = dbo.GetProvinceIDByCommunityID(ISNULL(@nTerritoryID, 0))
	--ENDIF

	RETURN ISNULL(@nProvinceID, 0)

END
GO
--End function dbo.GetProvinceIDFromForceID

--Begin function dbo.GetServerSetupValueByServerSetupKey
EXEC utility.DropObject 'dbo.GetServerSetupValueByServerSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a ServerSetupValue from a ServerSetupKey from the dbo.ServerSetup table
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ======================================================================================================================

CREATE FUNCTION dbo.GetServerSetupValueByServerSetupKey
(
@ServerSetupKey VARCHAR(250),
@DefaultServerSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cServerSetupValue VARCHAR(MAX)
	
	SELECT @cServerSetupValue = SS.ServerSetupValue 
	FROM dbo.ServerSetup SS 
	WHERE SS.ServerSetupKey = @ServerSetupKey
	
	RETURN ISNULL(@cServerSetupValue, @DefaultServerSetupValue)

END
GO
--End function dbo.GetServerSetupValueByServerSetupKey

--Begin function dbo.IsAssetUnitStipendEligible
EXEC utility.DropObject 'dbo.IsAssetUnitStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if an asset unit is stipend eligible
-- =========================================================================

CREATE FUNCTION dbo.IsAssetUnitStipendEligible
(
@AssetUnitID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN C2.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C2.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C1 ON C1.CommunityID = A.CommunityID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.IsAssetUnitStipendEligible

--Begin function dbo.IsAssetUnitCommunityStipendEligible
EXEC utility.DropObject 'dbo.IsAssetUnitCommunityStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if a community associated with an asset unit (via an asset) is stipend eligible
-- ====================================================================================================================

CREATE FUNCTION dbo.IsAssetUnitCommunityStipendEligible
(
@AssetUnitID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 1
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Community C ON C.ImpactDecisionID = ID.ImpactDecisionID
		JOIN asset.Asset A ON A.CommunityID = C.CommunityID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
			AND AU.AssetUnitID = @AssetUnitID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsAssetUnitCommunityStipendEligible

--Begin function procurement.FormatEndUserByEntityTypCodeAndEntityID
EXEC utility.DropObject 'procurement.FormatEndUserByEntityTypCodeAndEntityID'
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.03.05
-- Description:	A function a formatted end user name for a distributed inventory item
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================================
CREATE FUNCTION procurement.FormatEndUserByEntityTypCodeAndEntityID
(
@EntityTypCode VARCHAR(50),
@EntityID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(300)

	IF @EntityTypCode = 'AssetUnit'
		SELECT @cReturn = ISNULL(AU.AssetUnitName, '') + ' (Department)' FROM asset.AssetUnit AU WHERE AU.AssetUnitID = @EntityID
	ELSE IF @EntityTypCode = 'Force'
		SELECT @cReturn = ISNULL(F.ForceName, '') + ' (Force)' FROM force.Force F WHERE F.ForceID = @EntityID
	ELSE IF @EntityTypCode = 'Contact'
		SELECT @cReturn = dbo.FormatContactNameByContactID(@EntityID, 'LastFirst')
	--ENDIF
	
	RETURN @cReturn

END
GO
--End function procurement.FormatEndUserByEntityTypCodeAndEntityID

--Begin function procurement.GetDistributedInventoryQuantityAvailable
EXEC utility.DropObject 'procurement.GetDistributedInventoryQuantityAvailable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.28
-- Description:	A function to get the quantity available for a DistributedInventory item
--
-- Author:			Todd Pires
-- Create date:	2016.10.24
-- Description:	Bug fix
-- =====================================================================================

CREATE FUNCTION procurement.GetDistributedInventoryQuantityAvailable
(
@EquipmentDistributionID INT,
@EquipmentInventoryID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0

	SELECT @nReturn = EI.Quantity - 
										EI.QuantityDistributed - 
										--(
										--SELECT ISNULL(SUM(DI.Quantity), 0)
										--FROM procurement.DistributedInventory DI 
										--	JOIN procurement.EquipmentDistribution ED ON ED.EquipmentDistributionID = DI.EquipmentDistributionID 
										--		AND ED.IsActive = 0
										--		AND DI.EquipmentInventoryID = @EquipmentInventoryID
										--) - 
										(
										SELECT ISNULL(SUM(DI.Quantity), 0)
										FROM procurement.DistributedInventory DI 
										WHERE DI.EquipmentDistributionID = @EquipmentDistributionID
										)
	FROM procurement.EquipmentInventory EI
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
	
	RETURN @nReturn
END
GO
--End function procurement.GetDistributedInventoryQuantityAvailable

--Begin function reporting.GetMediaReportSourceLinks
EXEC utility.DropObject 'reporting.GetMediaReportSourceLinks'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.29
-- Description:	A function to get source link data for the MediaReport SSRS
-- ========================================================================
CREATE FUNCTION reporting.GetMediaReportSourceLinks
(
@MediaReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cSiteURL VARCHAR(MAX)
	DECLARE @cSourceLinks VARCHAR(MAX)

	SELECT @cSiteURL = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SiteURL'

	SET @cSiteURL = '<a href="' + ISNULL(@cSiteURL, '') + '/mediareport/view/id/' + CAST(@MediaReportID AS VARCHAR(10)) + '/">'

	SELECT @cSourceLinks = COALESCE(@cSourceLinks + '<br>', '') + @cSiteURL + MRS.SourceName + '</a>'
	FROM mediareport.MediaReportSource MRS
	WHERE MRS.MediaReportID = @MediaReportID

	RETURN @cSourceLinks
END
GO	
--End function reporting.GetMediaReportSourceLinks

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nEntityID INT = @EntityID
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nEntityID = T.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nEntityID = T.RiskUpdateID FROM riskupdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport'
			SELECT TOP 1 @nEntityID = T.WeeklyReportID FROM weeklyreport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
			
		END
	--ENDIF

	IF @nEntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @nEntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @nEntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @nEntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
	
END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.GetConceptNoteWorkflowStatus
EXEC utility.DropObject 'workflow.GetConceptNoteWorkflowStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.05
-- Description:	A function to get a workflow status name for a concept note
-- ========================================================================
CREATE FUNCTION workflow.GetConceptNoteWorkflowStatus
(
@ConceptNoteID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cStatusName VARCHAR(250) = 'Bad WorkFlow'
	DECLARE @cWorkflowName VARCHAR(250) = LTRIM(RTRIM((SELECT TOP 1 EWSGP.WorkflowName FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Conceptnote' AND EWSGP.EntityID = @ConceptNoteID)))
	DECLARE @nTargetWorkflowStepNumber INT = 0

	IF @cWorkflowName IN ('UK Activity Workflow','US Activity Workflow')
		BEGIN

		IF @cWorkflowName = 'UK Activity Workflow'
			SET @nTargetWorkflowStepNumber = 6
		ELSE IF @cWorkflowName = 'US Activity Workflow'	
			SET @nTargetWorkflowStepNumber = 7
		--ENDIF

		SELECT @cStatusName = 
			CASE
				WHEN CNS.ConceptNoteStatusCode IN ('Amended','Cancelled','Closed','OnHold') 
				THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < @nTargetWorkflowStepNumber
						THEN 'Development'
						WHEN CN.WorkflowStepNumber = @nTargetWorkflowStepNumber
						THEN 'Implementation'
						ELSE 'Closedown'
					END
			END

		FROM dbo.ConceptNote CN
			JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
				AND CN.ConceptNoteID = @ConceptNoteID

		END
	--ENDIF

	RETURN @cStatusName

END
GO
--End function workflow.GetConceptNoteWorkflowStatus