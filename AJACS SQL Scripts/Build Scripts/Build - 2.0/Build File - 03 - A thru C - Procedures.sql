USE AJACS2
GO

--Begin procedure asset.GetAssetByAssetID
EXEC Utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the asset.Asset table
-- ======================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		A.AssetDescription,
		A.AssetID,
		A.AssetName,
		A.CommunityID,
		dbo.GetCommunityNameByCommunityID(A.CommunityID) AS CommunityName,
		A.IsActive,
		A.Location.STAsText() AS Location,
		A.RelocationDate,
		dbo.FormatDate(A.RelocationDate) AS RelocationDateFormatted,
		A.RelocationLocation,
		A.RelocationNotes,
		AST.AssetStatusCode,
		AST.AssetStatusID,
		AST.AssetStatusName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon
	FROM asset.Asset A
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	SELECT
		AU.AssetUnitID,
		AU.AssetUnitName,
		AU.CommanderContactID,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullNameFormatted,
		AU.DeputyCommanderContactID,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullNameFormatted,
		AU.IsActive,
		AUC.AssetUnitCostID,
		AUC.AssetUnitCostName
	FROM asset.AssetUnit AU
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			AND AU.AssetID = @AssetID

	;
	WITH CD AS
		(
		SELECT AU.CommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID

		UNION

		SELECT AU.DeputyCommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID
		)

	SELECT 
		dbo.FormatContactNameByContactID(US.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'US' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(US.VettingDate) AS VettingDateFormatted,
		VO1.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN DATEADD(m, 6, US.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO1.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			A.ContactID,
			A.ContactVettingID,
			A.VettingDate,
			A.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD1.ContactID ORDER BY CV1.VettingDate DESC, CV1.ContactVettingID DESC) AS RowIndex,
				CD1.ContactID,
				CV1.ContactVettingID,
				CV1.VettingDate,
				CV1.VettingOutcomeID
			FROM CD CD1
				LEFT JOIN dbo.ContactVetting CV1 ON CV1.ContactID = CD1.ContactID
			WHERE CV1.ContactVettingTypeID = 1
			) A
			WHERE A.RowIndex = 1
		) US
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = ISNULL(US.VettingOutcomeID, 0)

	UNION

	SELECT 
		dbo.FormatContactNameByContactID(UK.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'UK' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(UK.VettingDate) AS VettingDateFormatted,
		VO2.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN DATEADD(m, 6, UK.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO2.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			B.ContactID,
			B.ContactVettingID,
			B.VettingDate,
			B.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD2.ContactID ORDER BY CV2.VettingDate DESC, CV2.ContactVettingID DESC) AS RowIndex,
				CD2.ContactID,
				CV2.ContactVettingID,
				CV2.VettingDate,
				CV2.VettingOutcomeID
			FROM CD CD2
				LEFT JOIN dbo.ContactVetting CV2 ON CV2.ContactID = CD2.ContactID
			WHERE CV2.ContactVettingTypeID = 2
			) B
			WHERE B.RowIndex = 1
		) UK
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = ISNULL(UK.VettingOutcomeID, 0)

	ORDER BY 1, 2, 3, 4

END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure asset.GetAssetUnitExpenseNotes
EXEC Utility.DropObject 'asset.GetAssetUnitExpenseNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	A stored procedure to get notes field from the AssetUnitExpense table
-- ==================================================================================
CREATE PROCEDURE asset.GetAssetUnitExpenseNotes

@AssetUnitExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		AUE.Notes
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.AssetUnitExpenseID = @AssetUnitExpenseID
	
END
GO
--End procedure asset.GetAssetUnitExpenseNotes

--Begin procedure asset.GetAssetLocations
EXEC Utility.DropObject 'asset.GetAssetLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.17
-- Description:	A stored procedure to get location data from the asset.Asset table
-- ===============================================================================
CREATE PROCEDURE asset.GetAssetLocations

@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF ISNULL(@Boundary, '') != ''
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	--ENDIF

	SELECT
		A.AssetID,
		A.AssetName,
		A.Location.STAsText() AS Location,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND 
				(
				ISNULL(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(A.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR A.CommunityID = @CommunityID
				)

END
GO
--End procedure asset.GetAssetLocations

--Begin procedure asset.RelocateAssetUnitContacts
EXEC Utility.DropObject 'asset.RelocateAssetUnitContacts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.17
-- Description:	A stored procedure move assetuintcontacts to a new community when an asset community changes
-- =========================================================================================================
CREATE PROCEDURE asset.RelocateAssetUnitContacts

@AssetID INT,
@AssetUnitID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cComment VARCHAR(MAX) = 'Community changed due to relocation of parent of assigned asset department.'
	DECLARE @cContactIDList VARCHAR(MAX)
	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY)

	IF @AssetUnitID IS NOT NULL
		BEGIN

		SET @cComment = 'Community changed due to transfer of assigned asset department.'

		UPDATE AU
		SET AU.AssetID = @AssetID
		FROM asset.AssetUnit AU
		WHERE AU.AssetUnitID = @AssetUnitID

		EXEC eventlog.LogAssetAction @AssetID, 'update', @PersonID, 'Asset Department Transfer'

		END
	--ENDIF

	UPDATE C
	SET C.CommunityID = A.CommunityID
	OUTPUT INSERTED.ContactID INTO @tOutput
	FROM dbo.Contact C
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			AND (@AssetUnitID IS NULL OR AU.AssetUnitID = @AssetUnitID)
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID

	SELECT @cContactIDList = STUFF((SELECT ',' + CAST(O.ContactID AS VARCHAR(10)) FROM @tOutput O ORDER BY O.ContactID FOR XML PATH('')), 1, 1, '')

	IF EXISTS (SELECT 1 FROM @tOutput)
		EXEC eventlog.LogContactAction 0, 'update', @PersonID, @cComment, @cContactIDList 
	--ENDIF

END
GO
--End procedure asset.RelocateAssetUnitContacts

--Begin procedure asset.SaveAssetUnitExpenseNotes
EXEC Utility.DropObject 'asset.SaveAssetUnitExpenseNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	A stored procedure to update the comments field in the AssetUnitExpense table
-- ==========================================================================================
CREATE PROCEDURE asset.SaveAssetUnitExpenseNotes

@AssetUnitExpenseID INT,
@Notes VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE AUE
	SET AUE.Notes = @Notes
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.AssetUnitExpenseID = @AssetUnitExpenseID
	
END
GO
--End procedure asset.SaveAssetUnitExpenseNotes
