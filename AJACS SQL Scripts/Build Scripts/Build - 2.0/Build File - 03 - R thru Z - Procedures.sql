USE AJACS2
GO

--Begin procedure reporting.AssetUnitCostRateTotal
EXEC utility.DropObject 'reporting.AssetUnitCostRateTotal'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure 
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =================================================================================
CREATE PROCEDURE reporting.AssetUnitCostRateTotal

@CommunityID INT = 0 

AS
BEGIN
	
	DECLARE @nLastPaymentYYYY INT, @nLastPaymentMM INT, @dLastPayment DATE 
	
	--Material Support 01
	SELECT 
		ISNULL(SUM(AUCR.AssetUnitCostRate), 0) AS AssetUnitCostRateTotal
	FROM asset.Asset A
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
		JOIN dropdown.AssetUnitCostRate AUCR ON AUCR.AssetUnitCostRateID = AU.AssetUnitCostRateID
		JOIN dropdown.AssetUnitType AUT ON AUT.AssetUnitTypeID = AU.AssetUnitTypeID
			AND A.CommunityID = @CommunityID

END
GO
--End procedure reporting.AssetUnitCostRateTotal

--Begin procedure reporting.GetCashHandoverReport
EXEC Utility.DropObject 'reporting.GetCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2015.09.28
-- Description:	Added the ArabicProvinceName column
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================
CREATE PROCEDURE reporting.GetCashHandoverReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ArabicProvinceName NVARCHAR(250)
	DECLARE @FullName VARCHAR(200) = (SELECT dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst'))
	DECLARE @PaymentMonthYear VARCHAR(20)
	DECLARE @PaymentMonth INT
	DECLARE @PaymentYear  INT
	DECLARE @ProvinceName VARCHAR(250)
	DECLARE @RunningCost INT 
	DECLARE @TotalCost INT 
	DECLARE @TotalExpenseCost INT
	
	SELECT @RunningCost = SUM(E.RunningCost)
	FROM
		(
		SELECT
			(
			SELECT AE.ExpenseAmountAuthorized
			FROM asset.Asset A
				JOIN asset.AssetUnit AU ON A.AssetID = AU.AssetUnitID
				JOIN asset.AssetUnitExpense AE ON AE.AssetUnitID = AU.AssetUnitID
					AND AE.PaymentMonth = @PaymentMonth  
					AND AE.PaymentYear = @PaymentYear	
			) AS RunningCost
		FROM
			(
			SELECT DISTINCT 
				CSP.CommunityID
			FROM dbo.ContactStipendPayment CSP
				JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
					AND SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
			) D
		) E
	
	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT 
		@PaymentMonth = PaymentMonth, 
		@PaymentYear = PaymentYear  
	FROM dbo.ContactStipendPayment CSP 
		JOIN 
			(
			SELECT TOP 1 EntityID	
			FROM reporting.SearchResult SR 
			WHERE SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
			) D ON D.EntityID = CSP.ContactStipendPaymentID

	SELECT 
		@TotalExpenseCost = SUM(AUE.ExpenseAmountAuthorized) 
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.PaymentMonth = @PaymentMonth 
		AND AUE.PaymentYear = @PaymentYear 

	SELECT TOP 1 
		@ArabicProvinceName = P.ArabicProvinceName,
		@PaymentMonthYear = DateName(month , DateAdd(month, CSP.PaymentMonth, 0) - 1) + ' - ' + CAST(CSP.PaymentYear AS CHAR(4)),
		@ProvinceName = P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT
		@ArabicProvinceName AS ArabicProvinceName,
		@FullName AS FullName,
		@PaymentMonthYear AS PaymentMonthYear,
		@ProvinceName AS ProvinceName,
		ISNULL(@TotalCost, 0) AS TotalCost,
		ISNULL(@TotalExpenseCost, 0) AS TotalExpenseCost,
		ISNULL(@TotalCost, 0) + ISNULL(@TotalExpenseCost, 0) as CompleteCost
END
GO
--End procedure reporting.GetCashHandoverReport

--Begin procedure reporting.GetCommunities
EXEC Utility.DropObject 'reporting.GetCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.15
-- Description:	A stored procedure to get data from the dbo.Community table
-- ========================================================================
CREATE PROCEDURE reporting.GetCommunities

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ArabicCommunityName,
		C.CommunityID,
		C.CommunityName,
		C.Population, 
		C.PopulationSource, 
		CG.CommunityGroupID,
		CG.CommunityGroupName,
		CS1.ComponentStatusID AS CommunityComponentStatusID,
		CS1.ComponentStatusName AS CommunityComponentStatusName,
		CS2.ComponentStatusID AS JusticeComponentStatusID,
		CS2.ComponentStatusName AS JusticeComponentStatusName,
		CS3.ComponentStatusID AS PoliceComponentStatusID,
		CS3.ComponentStatusName AS PoliceComponentStatusName,
		CSG.CommunitySubGroupID,
		CSG.CommunitySubGroupName,
		ID.ImpactDecisionID,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Community C 
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.ComponentStatus CS1 ON CS1.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus CS2 ON CS2.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus CS3 ON CS3.ComponentStatusID = C.PoliceComponentStatusID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID 
		JOIN Reporting.SearchResult SR ON SR.EntityID = C.CommunityID 
			AND SR.EntityTypeCode = 'Community' 
			AND SR.PersonID = @PersonID
	ORDER BY C.CommunityName

END
GO
--End procedure reporting.GetCommunities

--Begin procedure reporting.GetConceptNoteAmendmentByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteAmmendmentByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteAmendmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to return data from the dbo.ConceptNoteAmendment table
-- ======================================================================================
CREATE PROCEDURE reporting.GetConceptNoteAmendmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		CNA.AmendmentNumber,
		CNA.ConceptNoteAmendmentID,
		CNA.Cost,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID
	ORDER BY 1, 2

END
GO
--End procedure reporting.GetConceptNoteAmendmentByConceptNoteID

--Begin procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.31
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.GetContactCommunityByContactID(CON.ContactID) AS Location,
		CON.ContactID,
		CON.EmployerName,
		CN.ConceptNoteID,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ConceptNoteReferenceCode,
		dbo.FormatContactNameByContactID(CON.ContactID, 'LastFirst') AS Fullname
	FROM
		(
		SELECT DISTINCT
			CNCE.ConceptNoteID,
			CNCE.ContactID
		FROM dbo.ConceptNoteContactEquipment CNCE
		WHERE CNCE.ConceptNoteID = @ConceptNoteID
		) D
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = D.ConceptNoteID
		JOIN dbo.Contact CON ON CON.ContactID = D.ContactID
	ORDER BY Fullname, D.ContactID

END
GO
--End procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		(SELECT TOP 1 WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote') AS WorkflowType,
		CNS.ConceptNoteStatusCode,
		CN.WorkflowStepNumber,
		CN.ConceptNoteID,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,
		CN.TaskCode,
		workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) AS Status,
		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1 SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,

		CASE
			WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Cancelled','Development','Closed','Closedown')
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			WHEN CN.TaskCode ='Closed'						
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			ELSE FORMAT((IsNull(OACNB.TotalBudget,0)+ IsNull(OACNA.ConceptNoteAmendmentTotal,0) + isnull(OACNCE.ConceptNoteEquimentTotal, 0)), 'C', 'en-us') 
		END AS TotalBudget,

		CASE
			WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Cancelled','Development')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us') 
		END AS TotalSpent,
		
		CASE 
			WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Development','Closedown')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT(((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmendmentTotal,0)+ isnull(OACNCE.ConceptNoteEquimentTotal, 0)) - (IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0))), 'C', 'en-us') 
		END AS TotalRemaining

	FROM dbo.ConceptNote CN
		JOIN Reporting.SearchResult SR ON SR.EntityID = CN.ConceptNoteID AND SR.EntityTypeCode='ConceptNote' AND SR.PersonID = @PersonID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB
		OUTER APPLY
			(
			SELECT
				SUM(CNF.drAmt) - SUM(CNF.CrAmt) as ConceptNoteFinanceTotal
			FROM dbo.ConceptNoteFinance CNF
			WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
			) OACNF
		OUTER APPLY
			(
			SELECT
				SUM(CNA.Cost) as ConceptNoteAmendmentTotal
			FROM dbo.ConceptNoteAmendment CNA
			WHERE CNA.ConceptNoteID = CN.ConceptNoteID
			) OACNA
		OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue) as ConceptNoteEquimentTotal
			FROM dbo.ConceptNoteEquipmentCatalog CNCE
				JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					AND CNCE.ConceptNoteID = CN.ConceptNoteID
			) OACNCE
	ORDER BY 9 DESC

END
GO
--End procedure reporting.GetConceptNotes

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
--
-- Author:			Justin Branum
-- Create date: 2016.01.21
-- Description: Added New columns regrading Previous Unit data for vetting export report
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =====================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.City,
		C1.AssetUnitID,
		C1.CommunityID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', C1.CommunityID) AS CommunityName,
		C1.ContactID,
		dbo.getContactTypesByContactID(C1.ContactID) AS ContactTypeName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.DescriptionOfDuties,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsRegimeDefector,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.PassportNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.PreviousDuties,
		C1.PreviousProfession,
		C1.PreviousRankOrTitle,
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDate,
		dbo.FormatUSDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateUS,
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDate,
		dbo.FormatUSDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateUS,
		C1.PreviousUnit AS PreviousUnitLocation,
		C1.Profession,
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,
		dbo.FormatUSDate(C1.StartDate) AS JoinDateUSFormatted,
		C1.State,
		C1.Title,
		dbo.FormatDate(C1.UKVettingExpirationDate) AS UKVettingExpirationDateFormatted,
		dbo.FormatUSDate(C1.USVettingExpirationDate) AS USVettingExpirationDateFormatted,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		OAA.AssetName,
		OAA.TerritoryName,
		OAVO1.VettingOutcomeName AS USVettingOutcomeName,
		OAVO2.VettingOutcomeName AS UKVettingOutcomeName,
		P1.ProjectID,
		P1.ProjectName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		OUTER APPLY
			(
			SELECT 
				A.AssetName,
				dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', A.CommunityID) AS TerritoryName
			FROM asset.Asset A 
				JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
					AND AU.AssetUnitID = C1.AssetUnitID
			) OAA
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 1
			ORDER BY CV.ContactVettingID DESC
			) OAVO1
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 2
			ORDER BY CV.ContactVettingID DESC
			) OAVO2
	ORDER BY C1.ContactID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetJusticeCashHandoverReport
EXEC Utility.DropObject 'reporting.GetJusticeCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================

CREATE PROCEDURE reporting.GetJusticeCashHandoverReport
@PersonID INT,	 
@PaymentMonth INT,	 
@PaymentYear INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@cStipendName nvarchar(250), 
		@nAssetUnitID INT, 
		@nContactCount INT, 
		@nStipendAmountAuthorized NUMERIC(18, 2), 
		@RunningCost INT,
		@TotalCost INT = 0

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
			SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
			SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
			SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
			SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID

	SELECT 
		@RunningCost = SUM(AUE.ExpenseAmountAuthorized)
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.AssetUnitExpense AUE ON AUE.AssetUnitID = AU.AssetUnitID 
			AND AUE.PaymentMonth = @PaymentMonth 
			AND AUE.PaymentYear = @PaymentYear

	SELECT
		dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(A.CommunityID)) AS ProvinceName,
		dbo.GetArabicProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(A.CommunityID)) AS ArabicProvinceName,
		dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst') AS FullName,
		DateName(month , DateAdd(month,@PaymentMonth, 0) - 1) + ' - ' + CAST(@PaymentYear AS CHAR(4)) AS PaymentMonthYear,
		ISNULL(ISNULL(@TotalCost, 0), 0) + ISNULL(@RunningCost, 0) as CompleteCost,
		dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(A.CommunityID)) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID 
		JOIN asset.AssetUnitExpense AUE ON AUE.AssetUnitID = AU.AssetUnitID 
			AND SP.PersonID = @PersonID
			AND AUE.PaymentMonth = @PaymentMonth 
			AND AUE.PaymentYear = @PaymentYear

END
GO
--End procedure reporting.GetJusticeCashHandoverReport

--Begin procedure reporting.GetJusticeStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPayments

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UPPER(LEFT(DateName( month , DateAdd( month , D.PaymentMonth , -1 ) ),3)) + ' - ' + CAST(D.PaymentYear as varchar(5)) AS YearMonthFormatted,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		SUM(D.StipendAmountPaid) AS StipendAmountPaid,
		SUM(D.StipendAmountAuthorized) AS StipendAmountAuthorized,
		(
		SELECT SUM(AUE.ExpenseAmountAuthorized)  
		FROM asset.AssetUnitExpense AUE 
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID 
				AND AUE.ProvinceID = D.ProvinceID 
				AND AUE.PaymentYear = D.PaymentYear 
				AND AUE.PaymentMonth = D.PaymentMonth
		) AS StipendExpenseAmount
	FROM
		(
		SELECT
			CSP.StipendAmountPaid,
			CSP.StipendAmountAuthorized,
			CSP.ContactID,
			CSP.ProvinceID,
			CSP.PaymentYear,
			CSP.PaymentMonth,
		
			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			CAST(CSP.PAYMENTYEAR AS VARCHAR(50)) + CAST(CSP.PAYMENTMONTH AS VARCHAR(50)) AS YEARMONTH,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID 
			AND StipendName IN ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') 
			AND StipendTypeCode ='JusticeStipend' 
		) D
	GROUP BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.provinceID
	ORDER BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.ProvinceID ASC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPayments

--Begin procedure reporting.GetJusticeStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetJusticeStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendPaymentReport

@PersonID INT = 0, 
@Year int = 0, 
@Month int = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT
	DECLARE @nContactCount INT
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
			SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
			SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
			SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
			SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		SP.[Total Count],
		SP.[Total Stipend],
		SP.[Rank 1],
		SP.[Rank 2],
		SP.[Rank 3],
		SP.[Rank 4],
		SP.[Rank 5],
		SP.[Rank 1 Count],
		SP.[Rank 2 Count],
		SP.[Rank 3 Count],
		SP.[Rank 4 Count],
		SP.[Rank 5 Count],
		sp.stipendpaymentid, 
		sp.personid,
		sp.AssetUnitid,
		AUE.ExpenseAmountAuthorized AS [Running Costs],
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID 
		JOIN asset.Asset A ON A.AssetID = A.AssetID 
		JOIN asset.AssetUnitExpense AUE ON AUE.AssetUnitID = AU.AssetUnitID 
			AND SP.PersonID = @PersonID
			AND AUE.PaymentMonth = @Month 
			AND AUE.PaymentYear = @Year

END
GO
--End procedure reporting.GetJusticeStipendPaymentReport

--Begin procedure reporting.GetMediaReports
EXEC utility.DropObject 'reporting.GetMediaReports'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.29
-- Description:	A stored procedure to data for the MediaReport SSRS
-- ================================================================
CREATE PROCEDURE reporting.GetMediaReports

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MR.MediaReportDate,
		dbo.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MR.MediaReportID,
		MR.MediareportLocation AS Location,
		reporting.GetMediaReportSourceLinks(MR.MediaReportID) AS MediaReportSourceLinks,
		MR.MediaReportTitle,
		MR.Summary,
		MRT.MediaReportTypeID,
		MRT.MediaReportTypeName, 
        dbo.GetProvinceIDByCommunityID(C.CommunityID) as ProvinceName
	FROM reporting.SearchResult SR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = SR.EntityID  	AND SR.EntityTypeCode = 'MediaReport' AND SR.PersonID = @personID
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
        JOIN mediareport.MediaReportCommunity MRC ON MRC.MediaReportID = MR.MediaReportID
        JOIN mediareport.MediaReportSource MRS ON MRS.MediaReportID = MR.MediaReportID
        JOIN Community C on C.CommunityID = MRC.CommunityID

END
GO
--End procedure reporting.GetMediaReports

--Begin procedure reporting.GetOpsFundReport
EXEC Utility.DropObject 'reporting.GetOpsFundReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ======================================================================
CREATE PROCEDURE reporting.GetOpsFundReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
	(
	SELECT
		CSP.ContactStipendPaymentID,
		CSP.StipendAmountAuthorized,
		CSP.CommunityID,

	CASE
		WHEN CSP.CommunityID > 0
		THEN dbo.GetProvinceIDByCommunityID(CSP.CommunityID)
		ELSE CSP.ProvinceID
	END AS ProvinceID

	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID 
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	)

	SELECT 
		C.GovernmentIDNumber,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ProvinceName,
		(SELECT P.ArabicProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ArabicProvinceName,
		(SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS CommunityName,
		(SELECT C.ArabicCommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS ArabicCommunityName,
		CSP.StipendName,
		A.AssetName,
		AU.AssetUnitName,
		CSP.ContactID,
		dbo.FormatPersonNameByPersonID(@PersonID ,'LastFirst') AS AJACSRepresentative,
		dbo.FormatContactNameByContactID(CSP.ContactID, 'LastFirst') AS BeneficaryName,
		C.ArabicFirstName +' ' + C.ArabicMiddlename + ' ' + C.ArabicLastName  AS ArabicBeneficaryName,
		SD.StipendAmountAuthorized,
		dbo.FormatDate(StipendAuthorizedDate) AS StipendAuthorizedDateFormatted,
		DateName(month, DateAdd(month, CSP.PaymentMonth, 0 ) - 1) + ' - ' + CAST(PaymentYear AS VARCHAR(50)) AS PaymentMonthYear
	FROM dbo.ContactStipendPayment CSP
		JOIN SD ON SD.ContactStipendPaymentID = CSP.ContactStipendPaymentID
		JOIN dbo.Contact C ON C.contactid = CSP.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
	ORDER BY ProvinceName, CommunityName, StipendName, BeneficaryName
	
END
GO
--End procedure reporting.GetOpsFundReport

--Begin procedure reporting.GetStipendPaymentReport
EXEC utility.DropObject 'reporting.GetStipendPaymentReport'
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2015.09.30
-- Description:	A stored procedure to add data to the reporting.StipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =================================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT = 17

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = 17
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Command] = SP.[Command] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Command Count] = SP.[Command Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END,
			SP.[General] = SP.[General] + CASE WHEN @cStipendName = 'General' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[General Count] = SP.[General Count] + CASE WHEN @cStipendName = 'General' THEN @nContactCount ELSE 0 END,
			SP.[Colonel] = SP.[Colonel] + CASE WHEN @cStipendName = 'Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Count] = SP.[Colonel Count] + CASE WHEN @cStipendName = 'Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Colonel Doctor] = SP.[Colonel Doctor] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Doctor Count] = SP.[Colonel Doctor Count] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nContactCount ELSE 0 END,
			SP.[Lieutenant Colonel] = SP.[Lieutenant Colonel] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Lieutenant Colonel Count] = SP.[Lieutenant Colonel Count] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Major] = SP.[Major] + CASE WHEN @cStipendName = 'Major' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Major Count] = SP.[Major Count] + CASE WHEN @cStipendName = 'Major' THEN @nContactCount ELSE 0 END,
			SP.[Captain] = SP.[Captain] + CASE WHEN @cStipendName = 'Captain' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Count] = SP.[Captain Count] + CASE WHEN @cStipendName = 'Captain' THEN @nContactCount ELSE 0 END,
			SP.[Captain Doctor] = SP.[Captain Doctor] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Doctor Count] = SP.[Captain Doctor Count] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nContactCount ELSE 0 END,
			SP.[First Lieutenant] = SP.[First Lieutenant] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Lieutenant Count] = SP.[First Lieutenant Count] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Officer] = SP.[Contracted Officer] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Officer Count] = SP.[Contracted Officer Count] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nContactCount ELSE 0 END,
			SP.[First Sergeant] = SP.[First Sergeant] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Sergeant Count] = SP.[First Sergeant Count] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[Sergeant] = SP.[Sergeant] + CASE WHEN @cStipendName = 'Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Sergeant Count] = SP.[Sergeant Count] + CASE WHEN @cStipendName = 'Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[First Adjutant] = SP.[First Adjutant] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Adjutant Count] = SP.[First Adjutant Count] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Adjutant] = SP.[Adjutant] + CASE WHEN @cStipendName = 'Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Adjutant Count] = SP.[Adjutant Count] + CASE WHEN @cStipendName = 'Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Policeman] = SP.[Policeman] + CASE WHEN @cStipendName = 'Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Policeman Count] = SP.[Policeman Count] + CASE WHEN @cStipendName = 'Policeman' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Policeman] = SP.[Contracted Policeman] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Policeman Count] = SP.[Contracted Policeman Count] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		SP.*,
		AUCR.AssetUnitCostRate AS [Running Costs],
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dropdown.AssetUnitCostRate AUCR ON AUCR.AssetUnitCostRateID = AU.AssetUnitCostRateID

END
GO
--End procedure reporting.GetStipendPaymentReport

--Begin procedure reporting.GetStipendPaymentsByProvince
EXEC Utility.DropObject 'reporting.GetStipendPaymentsByProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data for stipend payments
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ===================================================================
CREATE PROCEDURE reporting.GetStipendPaymentsByProvince

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.ContactID AS SysID, 
		C1.CellPhoneNumber,
		C1.ContactID,
		dbo.GetContactCommunityByContactID(C1.ContactID) AS ContactLocation,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		
		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Community C2 JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID AND C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ProvinceName,
		
		CASE
			WHEN C1.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C1.CommunityID) = 1
			THEN S.StipendAmount
			WHEN C1.ProvinceID > 0 AND dbo.IsProvinceStipendEligible(C1.ProvinceID) = 1
			THEN S.StipendAmount
			ELSE 0
		END AS StipendAmount,
			
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center,
		C1.ArabicMotherName,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		S.StipendName 
	FROM reporting.SearchResult SR
		JOIN dbo.Contact C1 ON C1.ContactID = SR.EntityID 
			AND SR.PersonID = @PersonID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dropdown.AssetUnitCostRate AUCR ON AUCR.AssetUnitCostRateID = AU.AssetUnitCostRateID
	ORDER BY ContactLocation, Center, C1.LastName, C1.FirstName, C1.MiddleName, C1.ContactID

END
GO
--End procedure reporting.GetStipendPaymentsByProvince

--Begin procedure weeklyreport.ApproveWeeklyReport
EXEC Utility.DropObject 'weeklyreport.ApproveWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
--
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	Renamed from SubmitWeeklyReport to ApproveWeeklyReport
--
-- Author:			Todd Pires
-- Create date:	2015.05.16
-- Description:	Added the post-approval initialization call
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ======================================================================
CREATE PROCEDURE weeklyreport.ApproveWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @nWeeklyReportID INT
	DECLARE @tOutput1 TABLE (EntityTypeCode VARCHAR(50), EntityID INT)
	DECLARE @tOutput2 TABLE (WeeklyReportID INT)

	SELECT @nWeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput1
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @nWeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput1
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @nWeeklyReportID

	INSERT INTO @tOutput1 (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @nWeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O1.EntityTypeCode, O1.EntityID
		FROM @tOutput1 O1
		ORDER BY O1.EntityTypeCode, O1.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	TRUNCATE TABLE weeklyreport.Community
	TRUNCATE TABLE weeklyreport.Province
	TRUNCATE TABLE weeklyreport.SummaryMapCommunity
	TRUNCATE TABLE weeklyreport.SummaryMapAsset
	TRUNCATE TABLE weeklyreport.SummaryMapIncident
	DELETE FROM weeklyreport.WeeklyReport

	INSERT INTO weeklyreport.WeeklyReport 
		(WorkflowStepNumber) 
	OUTPUT INSERTED.WeeklyReportID INTO @tOutput2
	VALUES 
		(1)

	SELECT @nWeeklyReportID = O2.WeeklyReportID FROM @tOutput2 O2

	EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

END
GO
--End procedure weeklyreport.ApproveWeeklyReport

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O

		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('WeeklyReport', @nWeeklyReportID)

	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.SummaryMapZoom 
	FROM weeklyreport.WeeklyReport WR

	SELECT
		C.CommunityID,
		C.CommunityName,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID,
		ID.ImpactDecisionName,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		C2.Location.STAsText() AS Location,

		CASE 
			WHEN EXISTS (SELECT 1 FROM weeklyreport.SummaryMapCommunity SMC WHERE SMC.CommunityID = C.CommunityID AND SMC.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM weeklyreport.Community C
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Community C2 ON C2.CommunityID = C.CommunityID
			 AND C.WeeklyReportID = @nWeeklyReportID

	SELECT
		A.AssetID,
		A.AssetName,
		A.Location.STAsText() AS Location,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,

		CASE 
			WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapAsset SMA WHERE SMA.AssetID = A.AssetID AND SMA.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND EXISTS 
				(
				SELECT 1 
				FROM weeklyreport.Community C 
				WHERE C.CommunityID = A.CommunityID 
					AND C.WeeklyReportID = @nWeeklyReportID
				)

	SELECT
		A.AssetID,
		A.AssetName,
		A.Location.STAsText() AS Location,
		AT.AssetTypeID,
		AT.AssetTypeName,

		CASE 
			WHEN EXISTS (SELECT 1 FROM weeklyreport.SummaryMapAsset SMA WHERE SMA.AssetID = A.AssetID AND SMA.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND EXISTS 
				(
				SELECT 1 
				FROM weeklyreport.Community C 
				WHERE C.CommunityID = A.CommunityID 
					AND C.WeeklyReportID = @nWeeklyReportID
				)

	SELECT
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.Location.STAsText() AS Location,

		CASE 
			WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapIncident SMI WHERE SMI.IncidentID = I.IncidentID AND SMI.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				EXISTS	
					(
					SELECT 1
					FROM dbo.IncidentCommunity IC
						JOIN weeklyreport.Community C ON C.CommunityID = IC.CommunityID
							AND IC.IncidentID = I.IncidentID
							AND C.WeeklyReportID = @nWeeklyReportID
					)
				OR EXISTS 
					(
					SELECT 1
					FROM dbo.IncidentProvince IP
						JOIN weeklyreport.Province P ON P.ProvinceID = IP.ProvinceID
							AND IP.IncidentID = I.IncidentID
							AND P.WeeklyReportID = @nWeeklyReportID
					)
				)		

	SELECT
		F.ForceID,
		F.ForceName,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor,
		'/assets/img/icons/' + REPLACE(AOT.HexColor, '#', '') + '.png' AS Icon,
		F.Location.STAsText() AS Location,

		CASE 
			WHEN EXISTS (SELECT 1 FROM weeklyreport.SummaryMapForce SMF WHERE SMF.ForceID = F.ForceID AND SMF.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND EXISTS
				(
				SELECT 1
				FROM force.ForceCommunity FC
					JOIN weeklyreport.Community C ON C.CommunityID = FC.CommunityID
						AND FC.ForceID = F.ForceID
						AND C.WeeklyReportID = @nWeeklyReportID
				)

	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('WeeklyReport', @nWeeklyReportID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('WeeklyReport', @nWeeklyReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Weekly Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Weekly Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Weekly Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Weekly Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'WeeklyReport'
		AND EL.EntityID = @nWeeklyReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure workplan.deleteWorkplanActivity
EXEC Utility.DropObject 'workplan.DeleteWorkplanActivity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.22
-- Description:	A stored procedure to delete data from the workplan.WorkplanActivity table
-- =======================================================================================
CREATE PROCEDURE workplan.DeleteWorkplanActivity

@WorkplanActivityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE WPA
	FROM workplan.WorkplanActivity WPA
	WHERE WPA.WorkplanActivityID = @WorkplanActivityID

END
GO
--End procedure workplan.DeleteWorkplanActivity

--Begin procedure workplan.GetWorkplanActivityByWorkplanActivityID
EXEC Utility.DropObject 'workplan.GetWorkplanActivityByWorkplanActivityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Kevin Ross
-- Create date: 2016.09.30
-- Description:	A stored procedure to get data from the workplan.WorkplanActivity table
-- ====================================================================================
CREATE PROCEDURE workplan.GetWorkplanActivityByWorkplanActivityID

@WorkplanActivityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		FS1.FundingSourceID AS CurrentFundingSourceID,
		FS1.FundingSourceName AS CurrentFundingSourceName,
		FS2.FundingSourceID AS OriginalFundingSourceID,
		FS2.FundingSourceName AS OriginalFundingSourceName,
		FORMAT(WPA.CurrentUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = WPA.WorkplanID), 'C', 'en-gb') AS CurrentGBPAllocation,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		WPA.Description,
		WPA.Notes,
		FORMAT(WPA.OriginalUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = WPA.WorkplanID), 'C', 'en-gb') AS OriginalGBPAllocation,
		FORMAT(WPA.OriginalUSDAllocation, 'C', 'en-us') AS OriginalUSDAllocation,
		WPA.WorkplanActivityID,
		WPA.WorkplanActivityName,
		WP.EndDate, 
		dbo.FormatDate(WP.EndDate) AS EndDateFormatted
	FROM workplan.WorkplanActivity WPA
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		JOIN dropdown.FundingSource FS1 ON FS1.FundingSourceID = WPA.CurrentFundingSourceID
		JOIN dropdown.FundingSource FS2 ON FS2.FundingSourceID = WPA.OriginalFundingSourceID
		JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
			AND WPA.WorkplanActivityID = @WorkplanActivityID

END
GO
--End procedure workplan.GetWorkplanActivityByWorkplanActivityID

--Begin procedure workplan.GetWorkplanByWorkplanID
EXEC Utility.DropObject 'workplan.GetWorkplanByWorkplanID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.21
-- Description:	A stored procedure to get data from the workplan.Workplan table
-- ============================================================================
CREATE PROCEDURE workplan.GetWorkplanByWorkplanID

@WorkplanID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		WP.EndDate, 
		dbo.FormatDate(WP.EndDate) AS EndDateFormatted,
		WP.IsActive, 
		WP.IsForDashboard, 
		WP.StartDate,
		dbo.FormatDate(WP.StartDate) AS StartDateFormatted,
		WP.UKContractNumber, 
		WP.USContractNumber, 
		WP.USDToGBPExchangeRate,
		WP.WorkplanID, 
		WP.WorkplanName
	FROM workplan.Workplan WP
	WHERE	WP.WorkplanID = @WorkplanID

	SELECT
		CNT.ConceptNoteTypeName,
		FS1.FundingSourceName AS CurrentFundingSourceName,
		FS2.FundingSourceName AS OriginalFundingSourceName,
		FORMAT(WPA.CurrentUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS CurrentGBPAllocation,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		WPA.Description,
		WPA.Notes,
		FORMAT(WPA.OriginalUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS OriginalGBPAllocation,
		FORMAT(WPA.OriginalUSDAllocation, 'C', 'en-us') AS OriginalUSDAllocation,
		WPA.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM workplan.WorkplanActivity WPA
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		JOIN dropdown.FundingSource FS1 ON FS1.FundingSourceID = WPA.CurrentFundingSourceID
		JOIN dropdown.FundingSource FS2 ON FS2.FundingSourceID = WPA.OriginalFundingSourceID
			AND WPA.WorkplanID = @WorkplanID

	SELECT
		D.WorkplanActivityID,
		WPA.WorkplanActivityName,
		CNT.ConceptNoteTypeName,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		FORMAT(SUM(D.Planned), 'C', 'en-us') AS Planned,
		FORMAT(SUM(D.Disbursed), 'C', 'en-us') AS Disbursed,
		FORMAT(SUM(D.CommittedBalance), 'C', 'en-us') AS CommittedBalance
	FROM
		(
		SELECT 
			WPA.WorkplanActivityID,

			CASE
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) = 'Development'
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal
				ELSE 0
			END AS Planned,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				THEN CN.ActualTotalAmount + OACNF.ConceptNoteFinanceTotal
				ELSE 0
			END AS Disbursed,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal - CN.ActualTotalAmount - OACNF.ConceptNoteFinanceTotal
				ELSE 0
			END AS CommittedBalance

		FROM dbo.ConceptNote CN
			JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
				AND WPA.WorkplanID = @WorkplanID
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue), 0) AS TotalBudget
				FROM dbo.ConceptNoteBudget CNB
				WHERE CNB.ConceptNoteID = CN.ConceptNoteID
				) OACNB
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0) AS ConceptNoteEquimentTotal
				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
						AND CNCE.ConceptNoteID = CN.ConceptNoteID
				) OACNCE
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNF.drAmt) - SUM(CNF.CrAmt), 0) AS ConceptNoteFinanceTotal
				FROM dbo.ConceptNoteFinance CNF
				WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
				) OACNF
		) D
	JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = D.WorkplanActivityID
	JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
	GROUP BY D.WorkplanActivityID, WPA.WorkplanActivityName, WPA.CurrentUSDAllocation, CNT.ConceptNoteTypeName
	ORDER BY WPA.WorkplanActivityName

END
GO
--End procedure workplan.GetWorkplanByWorkplanID

--Begin procedure zone.GetZoneByZoneID
EXEC Utility.DropObject 'dbo.GetZoneByZoneID'
EXEC Utility.DropObject 'zone.GetZoneByZoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the zone.Zone table
-- ====================================================================
CREATE PROCEDURE zone.GetZoneByZoneID

@ZoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
 		Z.Location.STAsText() AS Location,
		Z.TerritoryID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Z.TerritoryTypeCode, Z.TerritoryID) AS TerritoryName,
		Z.TerritoryTypeCode,
		Z.ZoneDescription,
		Z.ZoneID,
		Z.ZoneName,
		ZS.ZoneStatusID,
		ZS.ZoneStatusName,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName
	FROM zone.Zone Z
		JOIN dropdown.ZoneStatus ZS ON ZS.ZoneStatusID = Z.ZoneStatusID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = Z.ZoneTypeID
			AND Z.ZoneID = @ZoneID

END
GO
--End procedure zone.GetZoneByZoneID
