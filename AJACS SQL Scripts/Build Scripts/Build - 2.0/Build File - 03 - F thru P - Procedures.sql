USE AJACS2
GO

--Begin procedure force.GetForceByEventLogID
EXEC Utility.DropObject 'force.GetForceByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.29
-- Description:	A stored procedure to force data from the eventlog.EventLog table
-- Notes:				Changes here must ALSO be made to force.GetForceByForceID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
-- ==============================================================================
CREATE PROCEDURE force.GetForceByEventLogID

@EventLogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Force.value('CommanderFullName[1]', 'VARCHAR(250)') AS CommanderFullName,
		Force.value('Comments[1]', 'VARCHAR(250)') AS Comments,
		Force.value('DeputyCommanderFullName[1]', 'VARCHAR(250)') AS DeputyCommanderFullName,
		Force.value('ForceDescription[1]', 'VARCHAR(250)') AS ForceDescription,
		Force.value('ForceID[1]', 'INT') AS ForceID,
		Force.value('ForceName[1]', 'VARCHAR(250)') AS ForceName,
		Force.value('History[1]', 'VARCHAR(250)') AS History,
		Force.value('Location[1]', 'VARCHAR(MAX)') AS Location,
		Force.value('Notes[1]', 'VARCHAR(250)') AS Notes,
		Force.value('TerritoryID[1]', 'INT') AS TerritoryID,
		Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)'), Force.value('TerritoryID[1]', 'INT')) AS TerritoryName,
		Force.value('WebLinks[1]', 'VARCHAR(250)') AS WebLinks,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		AOO.AreaOfOperationTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force') AS T(Force)
		JOIN dropdown.AreaOfOperationType AOO ON AOO.AreaOfOperationTypeID = Force.value('AreaOfOperationTypeID[1]', 'INT')
			AND EL.EventLogID = @EventLogID

	SELECT
		Force.value('(CommunityID)[1]', 'INT') AS CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceCommunities/ForceCommunity') AS T(Force)
			JOIN dbo.Community C ON C.CommunityID = Force.value('(CommunityID)[1]', 'INT')
			JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
				AND EL.EventLogID = @EventLogID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceEquipmentResourceProviders/ForceEquipmentResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceFinancialResourceProviders/ForceFinancialResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(RiskID)[1]', 'INT') AS RiskID,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceRisks/ForceRisk') AS T(Force)
			JOIN dbo.Risk R ON R.RiskID = Force.value('(RiskID)[1]', 'INT')
			JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
				AND EL.EventLogID = @EventLogID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		Force.value('(ForceUnitID)[1]', 'INT') AS ForceUnitID,
		Force.value('CommanderFullName[1]', 'VARCHAR(250)') AS CommanderFullName,
		Force.value('DeputyCommanderFullName[1]', 'VARCHAR(250)') AS DeputyCommanderFullName,
		Force.value('(TerritoryID)[1]', 'INT') AS TerritoryID,
		Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)'), Force.value('(TerritoryID)[1]', 'INT')) AS TerritoryName,
		Force.value('(UnitName)[1]', 'VARCHAR(250)') AS UnitName,
		Force.value('(UnitTypeID)[1]', 'INT') AS UnitTypeID,
		UT.UnitTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceUnits/ForceUnit') AS T(Force)
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = Force.value('(UnitTypeID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY Force.value('(UnitName)[1]', 'VARCHAR(250)'), Force.value('(ForceUnitID)[1]', 'INT')
	
END
GO
--End procedure force.GetForceByEventLogID

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- Notes:				Changes here must ALSO be made to force.GetForceByEventLogID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
-- =========================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderContactID, 
		FU.DeputyCommanderContactID,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure force.GetForceUnitByForceUnitID
EXEC Utility.DropObject 'force.GetForceUnitByForceUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.01
-- Description:	A stored procedure to data from the force.ForceUnit table
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
-- ======================================================================
CREATE PROCEDURE force.GetForceUnitByForceUnitID

@ForceUnitID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		FU.CommanderFullName,
		FU.DeputyCommanderFullName,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceUnitID = @ForceUnitID
	
END
GO
--End procedure force.GetForceUnitByForceUnitID

--Begin procedure mediareport.GetMediaReportByMediaReportID
EXEC Utility.DropObject 'mediareport.GetMediaReportByMediaReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.07
-- Description:	A stored procedure to data from the mediareport.MediaReport table
-- ==============================================================================
CREATE PROCEDURE mediareport.GetMediaReportByMediaReportID

@MediaReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MR.MediaReportDate,
		dbo.FormatDate(MR.MediaReportDate) AS MediaReportDateFormatted,
		MR.MediaReportID,
		MR.MediaReportLocation,
		MR.MediaReportTitle,
		MR.Summary,
		MRT.MediaReportTypeID,
		MRT.MediaReportTypeName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
			AND MR.MediaReportID = @MediaReportID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM mediareport.MediaReportCommunity MRC
		JOIN dbo.Community C ON C.CommunityID = MRC.CommunityID
			AND MRC.MediaReportID = @MediaReportID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		LOWER(CAST(newID() AS VARCHAR(50))) AS MediaReportSourceGUID,
		MRS.MediaReportSourceID, 
		MRS.SourceAttribution, 
		MRS.SourceDate, 
		dbo.FormatDate(MRS.SourceDate) AS SourceDateFormatted, 
		MRS.SourceName, 
		MRST.MediaReportSourceTypeID, 
		MRST.MediaReportSourceTypeName,
		MRST.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		MRST.MediaReportSourceTypeName AS SourceTypeName -- Aliased for the common source cfmodule
	FROM mediareport.MediaReportSource MRS
		JOIN dropdown.MediaReportSourceType MRST ON MRST.MediaReportSourceTypeID = MRS.MediaReportSourceTypeID
			AND MRS.MediaReportID = @MediaReportID
	ORDER BY MRS.SourceName, MRS.MediaReportSourceID

	SELECT
		getDate() AS MediaReportDate,
		dbo.FormatDate(getDate()) AS MediaReportDateFormatted

END
GO
--End procedure mediareport.GetMediaReportByMediaReportID

--Begin procedure permissionable.DeletePermissionableTemplate
EXEC Utility.DropObject 'permissionable.DeletePermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date: 2016.10.16
-- Description:	A stored procedure to delete Permissionable Template data
-- ======================================================================
CREATE PROCEDURE permissionable.DeletePermissionableTemplate

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PT
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID

END
GO
--End procedure permissionable.DeletePermissionableTemplate

--Begin procedure permissionable.GetPersonPermissionables
EXEC Utility.DropObject 'permissionable.GetPersonPermissionables'
GO
--End procedure permissionable.GetPersonPermissionables

--Begin procedure procurement.DeleteEquipmentDistributionByEquipmentDistributionID
EXEC Utility.DropObject 'procurement.DeleteEquipmentDistributionByEquipmentDistributionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.04
-- Description:	A stored procedure to delete Equipment Distribution Plan data
-- ==========================================================================
CREATE PROCEDURE procurement.DeleteEquipmentDistributionByEquipmentDistributionID

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE ED
	FROM procurement.EquipmentDistribution ED
	WHERE ED.EquipmentDistributionID = @EquipmentDistributionID

	DELETE DI
	FROM procurement.DistributedInventory DI
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM procurement.EquipmentDistribution ED
		WHERE ED.EquipmentDistributionID = DI.EquipmentDistributionID
		)

	DELETE D
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'EquipmentDistribution'
			AND DE.DocumentEntityID = @EquipmentDistributionID

	DELETE DE
	FROM dbo.DocumentEntity DE
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.Document D
		WHERE D.DocumentID = DE.DocumentID
		)

END
GO
--End procedure procurement.DeleteEquipmentDistributionByEquipmentDistributionID

--Begin procedure procurement.GetCommunityEquipmentInventory
EXEC Utility.DropObject 'dbo.GetCommunityEquipmentInventory'
EXEC Utility.DropObject 'procurement.GetCommunityEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.CommunityEquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2016.05.19
-- Description:	Refactored to support the redesigned inventory system
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =========================================================================================
CREATE PROCEDURE procurement.GetCommunityEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DeliveredToEndUserDate) AS DeliveredToEndUserDateFormatted,
		dbo.FormatConceptNoteTitle(EI.ConceptNoteID) AS Title	
	FROM
		(
		SELECT
			DI.EquipmentInventoryID,
			SUM(DI.Quantity) AS Quantity,
			MAX(DI.DeliveredToEndUserDate) AS DeliveredToEndUserDate
		FROM
			(
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				A.CommunityID,
				0 AS ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN asset.AssetUnit AU ON AU.AssetUnitID = DI.EndUserEntityID
				JOIN asset.Asset A ON A.AssetID = AU.AssetID
					AND DI.EndUserEntityTypeCode = 'AssetUnit'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				C.CommunityID,
				dbo.GetProvinceIDByCommunityID(C.CommunityID) AS ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Contact'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Community'
					THEN F.TerritoryID
					ELSE 0
				END AS CommunityID,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Province'
					THEN F.TerritoryID
					ELSE 0
				END AS ProvinceID
	
			FROM procurement.DistributedInventory DI
				JOIN force.Force F ON F.ForceID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Force'
	
			) DI
		WHERE DI.CommunityID = @EntityID
		GROUP BY DI.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure procurement.GetCommunityEquipmentInventory

--Begin procedure procurement.GetProvinceEquipmentInventory
EXEC Utility.DropObject 'dbo.GetProvinceEquipmentInventory'
EXEC Utility.DropObject 'procurement.GetProvinceEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.ProvinceEquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2016.05.19
-- Description:	Refactored to support the redesigned inventory system
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =========================================================================================
CREATE PROCEDURE procurement.GetProvinceEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DeliveredToEndUserDate) AS DeliveredToEndUserDateFormatted,
		dbo.FormatConceptNoteTitle(EI.ConceptNoteID) AS Title	
	FROM
		(
		SELECT
			DI.EquipmentInventoryID,
			SUM(DI.Quantity) AS Quantity,
			MAX(DI.DeliveredToEndUserDate) AS DeliveredToEndUserDate
		FROM
			(
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				C.CommunityID,
				dbo.GetProvinceIDByCommunityID(C.CommunityID) AS ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Contact'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Community'
					THEN F.TerritoryID
					ELSE 0
				END AS CommunityID,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Province'
					THEN F.TerritoryID
					ELSE 0
				END AS ProvinceID
	
			FROM procurement.DistributedInventory DI
				JOIN force.Force F ON F.ForceID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Force'
	
			) DI
		WHERE DI.ProvinceID = @EntityID
		GROUP BY DI.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure procurement.GetProvinceEquipmentInventory

--Begin procedure procurement.validateSerialNumber
EXEC Utility.DropObject 'procurement.validateSerialNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2016.09.02
-- Description:	A stored procedure to validate serial numbers
-- ==========================================================
CREATE PROCEDURE procurement.validateSerialNumber

@EquipmentInventoryID INT,
@SerialNumber VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT LTT.ListItem AS SerialNumber
	FROM dbo.ListToTable(@SerialNumber, ',') LTT
	WHERE EXISTS
		(
		SELECT 1
		FROM procurement.EquipmentInventory EI
		WHERE EI.SerialNumber = LTT.ListItem
			AND EI.EquipmentInventoryID <> @EquipmentInventoryID
		)
	ORDER BY LTT.ListItem

END
GO
--End procedure procurement.validateSerialNumber

