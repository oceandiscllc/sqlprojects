-- File Name:	Build-2.0 File 01 - AJACS.sql
-- Build Key:	Build-2.0 File 01 - AJACS - 2016.10.30 14.22.53

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		asset.GetAssetUnitEquipmentEligibilityByAssetUnitID
--		asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID
--		asset.GetCommunityIDByAssetUnitID
--		dbo.FormatProgramReportReferenceCode
--		dbo.FormatSpotReportReferenceCode
--		dbo.FormatStaticGoogleMapForCommunityExport
--		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
--		dbo.FormatWeeklyReportReferenceCode
--		dbo.GetCommunityIDFromContactID
--		dbo.GetCommunityIDFromForceID
--		dbo.GetConceptNoteTotalCost
--		dbo.GetConceptNoteVersionNumberByConceptNoteID
--		dbo.GetContactCommunityByContactID
--		dbo.GetContactEquipmentEligibilityByContactID
--		dbo.GetContactEquipmentEligibilityNotesByContactID
--		dbo.GetContactStipendEligibilityByContactID
--		dbo.GetContactStipendEligibilityNotesByContactID
--		dbo.GetEventNameByEventCode
--		dbo.GetProvinceIDByContactID
--		dbo.GetProvinceIDFromForceID
--		dbo.GetServerSetupValueByServerSetupKey
--		dbo.IsAssetUnitCommunityStipendEligible
--		dbo.IsAssetUnitStipendEligible
--		procurement.FormatEndUserByEntityTypCodeAndEntityID
--		procurement.GetDistributedInventoryQuantityAvailable
--		reporting.GetMediaReportSourceLinks
--		workflow.CanHaveAddUpdate
--		workflow.GetConceptNoteWorkflowStatus
--
-- Procedures:
--		asset.GetAssetByAssetID
--		asset.GetAssetLocations
--		asset.GetAssetUnitExpenseNotes
--		asset.RelocateAssetUnitContacts
--		asset.SaveAssetUnitExpenseNotes
--		dbo.AddContactStipendPaymentContacts
--		dbo.AddContactsToConceptNote
--		dbo.ApproveContactStipendPayment
--		dbo.CloneConceptNote
--		dbo.DeleteContactStipendPayment
--		dbo.GetClassByClassID
--		dbo.GetCommunityByCommunityID
--		dbo.GetCommunityFeed
--		dbo.GetCommunityLocations
--		dbo.GetCommunityRoundByCommunityRoundID
--		dbo.GetConceptNoteBudgetByConceptNoteID
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetContactByContactID
--		dbo.GetDashboardItemCounts
--		dbo.GetDocumentByDocumentID
--		dbo.GetDonorFeed
--		dbo.GetEntityDocuments
--		dbo.GetIncidentByIncidentID
--		dbo.GetMenuItemsByPersonID
--		dbo.GetProvinceByProvinceID
--		dbo.GetProvinceFeed
--		dbo.GetServerSetupDataByServerSetupID
--		dbo.GetServerSetupValuesByServerSetupKey
--		dbo.GetSpotReportBySpotReportID
--		dbo.ReconcileContactStipendPayment
--		dbo.SaveEntityDocuments
--		dbo.SetContactAssetUnit
--		dbo.ValidateLogin
--		dropdown.GetAssetStatusData
--		dropdown.GetAssetTypeData
--		dropdown.GetAssetUnitCostData
--		dropdown.GetComponentStatusData
--		dropdown.GetConceptNoteTypeData
--		dropdown.GetContactTypeData
--		dropdown.GetMediaReportSourceTypeData
--		dropdown.GetMediaReportTypeData
--		dropdown.GetPaymentGroupData
--		dropdown.GetZoneStatusData
--		eventlog.LogAssetAction
--		eventlog.LogMediaReportAction
--		eventlog.LogWorkplanAction
--		eventlog.LogZoneAction
--		force.GetForceByEventLogID
--		force.GetForceByForceID
--		force.GetForceUnitByForceUnitID
--		mediareport.GetMediaReportByMediaReportID
--		permissionable.DeletePermissionableTemplate
--		procurement.DeleteEquipmentDistributionByEquipmentDistributionID
--		procurement.GetCommunityEquipmentInventory
--		procurement.GetProvinceEquipmentInventory
--		procurement.validateSerialNumber
--		reporting.AssetUnitCostRateTotal
--		reporting.GetCashHandoverReport
--		reporting.GetCommunities
--		reporting.GetConceptNoteAmendmentByConceptNoteID
--		reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID
--		reporting.GetConceptNotes
--		reporting.GetContact
--		reporting.GetJusticeCashHandoverReport
--		reporting.GetJusticeStipendActivitySummaryPayments
--		reporting.GetJusticeStipendPaymentReport
--		reporting.GetMediaReports
--		reporting.GetOpsFundReport
--		reporting.GetStipendPaymentReport
--		reporting.GetStipendPaymentsByProvince
--		weeklyreport.ApproveWeeklyReport
--		weeklyreport.GetWeeklyReport
--		workplan.DeleteWorkplanActivity
--		workplan.GetWorkplanActivityByWorkplanActivityID
--		workplan.GetWorkplanByWorkplanID
--		zone.GetZoneByZoneID
--
-- Schemas:
--		asset
--		mediareport
--		workplan
--		zone
--
-- Tables:
--		asset.Asset
--		asset.AssetUnit
--		asset.AssetUnitExpense
--		dbo.CommunityImpactDecisionHistory
--		dropdown.AssetStatus
--		dropdown.AssetUnitCost
--		dropdown.ComponentStatus
--		dropdown.MediaReportSourceType
--		dropdown.MediaReportType
--		dropdown.ZoneStatus
--		mediareport.MediaReport
--		mediareport.MediaReportCommunity
--		mediareport.MediaReportSource
--		weeklyreport.SummaryMapAsset
--		workplan.Workplan
--		workplan.WorkplanActivity
--		zone.Zone
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS2
GO

EXEC utility.AddSchema asset
EXEC utility.AddSchema mediareport
EXEC utility.AddSchema workplan
EXEC utility.AddSchema zone
GO

EXEC utility.DropObject 'dbo.fnGetServerSetupValuesByServerSetupKey'
EXEC utility.DropObject 'dbo.GetDateByReferenceDateDayAbbreviationAndOffset'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'ConceptNoteAmmendment')
	BEGIN

	EXEC sp_rename 'dbo.ConceptNoteAmmendment', 'ConceptNoteAmendment'

	EXEC sp_rename 'dbo.ConceptNoteAmendment.ConceptNoteAmmendmentID', 'ConceptNoteAmendmentID', 'COLUMN';  
	EXEC sp_rename 'dbo.ConceptNoteAmendment.AmmendmentNumber', 'AmendmentNumber', 'COLUMN';

	EXEC sp_rename 'dbo.ConceptNoteBudget.Ammendments', 'Amendments', 'COLUMN';

	End
--ENDIF
GO

/*
--Begin synonyms
EXEC utility.DropObject 'dbo.ServerSetup'
EXEC utility.DropObject 'dbo.udf_hashBytes'
EXEC utility.DropObject 'syslog.ApplicationErrorLog'
EXEC utility.DropObject 'syslog.DuoWebTwoFactorLog'
GO

CREATE SYNONYM dbo.ServerSetup FOR AJACSUtility2.dbo.ServerSetup
CREATE SYNONYM dbo.udf_hashBytes FOR AJACSUtility2.dbo.udf_hashBytes
CREATE SYNONYM syslog.ApplicationErrorLog FOR AJACSUtility2.syslog.ApplicationErrorLog
CREATE SYNONYM syslog.DuoWebTwoFactorLog FOR AJACSUtility2.syslog.DuoWebTwoFactorLog
GO
--End synonyms
*/
--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS2
GO

--Begin object delete
/*
DECLARE @tTable TABLE (SchemaName VARCHAR(100), TableName VARCHAR(100), Type VARCHAR(5))

INSERT INTO @tTable
	(SchemaName, TableName, Type)
SELECT
	S.Name AS SchemaName,
	O.Name AS TableName,
	O.Type
FROM sys.Objects O
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
		AND O.Type IN ('FN','P','U')
		AND 
			(
			S.Name IN ('communityprovinceengagementupdate', 'deprecated', 'fifupdate', 'justiceupdate', 'policeengagementupdate', 'survey')
				OR O.Name LIKE 'Atmospheric%'
				OR O.Name LIKE 'Daily%'
				OR O.Name LIKE 'FocusGroup%'
				OR O.Name LIKE 'GetKey%'
				OR O.Name LIKE 'Key%'
				OR O.Name LIKE '%Survey'
				OR O.Name LIKE 'Team%'
			)
ORDER BY S.Name, O.Name

SELECT
	'EXEC utility.DropObject ''' + T.SchemaName + '.' + T.TableName + '''' AS SQLText
FROM @tTable T

UNION

SELECT
	'EXEC utility.DropObject ''' + T.SchemaName + '.Get' + T.TableName + 'Data'''
FROM @tTable T
WHERE T.SchemaName = 'dropdown'
	AND T.Type = 'U'

UNION

SELECT
	'EXEC utility.DropObject ''' + S.Name + '.' + O.Name + '''' AS SQLText
FROM sys.Objects O
	JOIN sys.Schemas S ON S.schema_ID = O.schema_ID
		AND O.Type IN ('P')
		AND S.Name = 'eventlog'
		AND EXISTS
			(
			SELECT 1
			FROM @tTable T
			WHERE 'Log' + T.TableName + 'Action' = O.Name
				AND T.TableName NOT IN ('Community','Province')
			)
			
ORDER BY 1
*/
EXEC utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity'
EXEC utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince'
EXEC utility.DropObject 'communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate'
EXEC utility.DropObject 'communityprovinceengagementupdate.Community'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityContact'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityIndicator'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityProject'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityProvinceEngagementUpdate'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityRecommendation'
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityRisk'
EXEC utility.DropObject 'communityprovinceengagementupdate.DeleteCommunityProvinceEngagementCommunity'
EXEC utility.DropObject 'communityprovinceengagementupdate.DeleteCommunityProvinceEngagementProvince'
EXEC utility.DropObject 'communityprovinceengagementupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate'
EXEC utility.DropObject 'communityprovinceengagementupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'communityprovinceengagementupdate.Province'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceContact'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceIndicator'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceProject'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceRecommendation'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceRisk'
EXEC utility.DropObject 'dbo.Atmospheric'
EXEC utility.DropObject 'dbo.AtmosphericCommunity'
EXEC utility.DropObject 'dbo.AtmosphericEngagementCriteriaResult'
EXEC utility.DropObject 'dbo.AtmosphericSource'
EXEC utility.DropObject 'dbo.CommunityMemberSurvey'
EXEC utility.DropObject 'dbo.DailyReport'
EXEC utility.DropObject 'dbo.DailyReportCommunity'
EXEC utility.DropObject 'dbo.FocusGroupSurvey'
EXEC utility.DropObject 'dbo.FocusGroupSurveyParticipant'
EXEC utility.DropObject 'dbo.GetKeyEventByKeyEventID'
EXEC utility.DropObject 'dbo.GetKeyInformantSurveyByKeyInformantSurveyID'
EXEC utility.DropObject 'dbo.KeyEvent'
EXEC utility.DropObject 'dbo.KeyEventCommunity'
EXEC utility.DropObject 'dbo.KeyEventEngagementCriteriaResult'
EXEC utility.DropObject 'dbo.KeyEventProvince'
EXEC utility.DropObject 'dbo.KeyEventSource'
EXEC utility.DropObject 'dbo.KeyInformantSurvey'
EXEC utility.DropObject 'dbo.RapidPerceptionSurvey'
EXEC utility.DropObject 'dbo.StakeholderGroupSurvey'
EXEC utility.DropObject 'dbo.StationCommanderSurvey'
EXEC utility.DropObject 'dbo.Team'
EXEC utility.DropObject 'dbo.TeamMember'
EXEC utility.DropObject 'deprecated.ConceptNoteContactEquipment'
EXEC utility.DropObject 'deprecated.EquipmentAuditOutcome'
EXEC utility.DropObject 'deprecated.EquipmentDistributionPlan'
EXEC utility.DropObject 'dropdown.AtmosphericType'
EXEC utility.DropObject 'dropdown.DailyReportType'
EXEC utility.DropObject 'dropdown.GetAtmosphericTypeData'
EXEC utility.DropObject 'dropdown.GetDailyReportTypeData'
EXEC utility.DropObject 'dropdown.GetKeyEventCategoryData'
EXEC utility.DropObject 'dropdown.GetKeyEventDateFilterData'
EXEC utility.DropObject 'dropdown.GetKeyEventGroupData'
EXEC utility.DropObject 'dropdown.GetKeyEventGroupKeyEventTypeData'
EXEC utility.DropObject 'dropdown.GetKeyEventTypeData'
EXEC utility.DropObject 'dropdown.KeyEventCategory'
EXEC utility.DropObject 'dropdown.KeyEventGroup'
EXEC utility.DropObject 'dropdown.KeyEventGroupKeyEventType'
EXEC utility.DropObject 'dropdown.KeyEventType'
EXEC utility.DropObject 'eventlog.LogAtmosphericAction'
EXEC utility.DropObject 'eventlog.LogCommunityMemberSurveyAction'
EXEC utility.DropObject 'eventlog.LogConceptNoteContactEquipmentAction'
EXEC utility.DropObject 'eventlog.LogDailyReportAction'
EXEC utility.DropObject 'eventlog.LogEquipmentDistributionPlanAction'
EXEC utility.DropObject 'eventlog.LogFocusGroupSurveyAction'
EXEC utility.DropObject 'eventlog.LogKeyEventAction'
EXEC utility.DropObject 'eventlog.LogKeyInformantSurveyAction'
EXEC utility.DropObject 'eventlog.LogRapidPerceptionSurveyAction'
EXEC utility.DropObject 'eventlog.LogStakeholderGroupSurveyAction'
EXEC utility.DropObject 'eventlog.LogStationCommanderSurveyAction'
EXEC utility.DropObject 'eventlog.LogSurveyAction'
EXEC utility.DropObject 'eventlog.LogSurveyQuestionAction'
EXEC utility.DropObject 'eventlog.LogSurveyResponseAction'
EXEC utility.DropObject 'eventlog.LogTeamAction'
EXEC utility.DropObject 'fifupdate.AddFIFCommunity'
EXEC utility.DropObject 'fifupdate.AddFIFProvince'
EXEC utility.DropObject 'fifupdate.ApproveFIFUpdate'
EXEC utility.DropObject 'fifupdate.Community'
EXEC utility.DropObject 'fifupdate.CommunityIndicator'
EXEC utility.DropObject 'fifupdate.CommunityMeeting'
EXEC utility.DropObject 'fifupdate.CommunityRisk'
EXEC utility.DropObject 'fifupdate.DeleteFIFCommunity'
EXEC utility.DropObject 'fifupdate.DeleteFIFProvince'
EXEC utility.DropObject 'fifupdate.FIFUpdate'
EXEC utility.DropObject 'fifupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'fifupdate.GetFIFUpdate'
EXEC utility.DropObject 'fifupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'fifupdate.Province'
EXEC utility.DropObject 'fifupdate.ProvinceIndicator'
EXEC utility.DropObject 'fifupdate.ProvinceMeeting'
EXEC utility.DropObject 'fifupdate.ProvinceRisk'
EXEC utility.DropObject 'justiceupdate.AddJusticeCommunity'
EXEC utility.DropObject 'justiceupdate.AddJusticeProvince'
EXEC utility.DropObject 'justiceupdate.ApproveJusticeUpdate'
EXEC utility.DropObject 'justiceupdate.Community'
EXEC utility.DropObject 'justiceupdate.CommunityIndicator'
EXEC utility.DropObject 'justiceupdate.CommunityRisk'
EXEC utility.DropObject 'justiceupdate.CommunityTraining'
EXEC utility.DropObject 'justiceupdate.DeleteJusticeCommunity'
EXEC utility.DropObject 'justiceupdate.DeleteJusticeProvince'
EXEC utility.DropObject 'justiceupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'justiceupdate.GetJusticeUpdate'
EXEC utility.DropObject 'justiceupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'justiceupdate.JusticeUpdate'
EXEC utility.DropObject 'justiceupdate.Province'
EXEC utility.DropObject 'justiceupdate.ProvinceIndicator'
EXEC utility.DropObject 'justiceupdate.ProvinceRisk'
EXEC utility.DropObject 'justiceupdate.ProvinceTraining'
EXEC utility.DropObject 'policeengagementupdate.AddPoliceEngagementCommunity'
EXEC utility.DropObject 'policeengagementupdate.AddPoliceEngagementProvince'
EXEC utility.DropObject 'policeengagementupdate.ApprovePoliceEngagementUpdate'
EXEC utility.DropObject 'policeengagementupdate.Community'
EXEC utility.DropObject 'policeengagementupdate.CommunityClass'
EXEC utility.DropObject 'policeengagementupdate.CommunityIndicator'
EXEC utility.DropObject 'policeengagementupdate.CommunityRecommendation'
EXEC utility.DropObject 'policeengagementupdate.CommunityRisk'
EXEC utility.DropObject 'policeengagementupdate.DeletePoliceEngagementCommunity'
EXEC utility.DropObject 'policeengagementupdate.DeletePoliceEngagementProvince'
EXEC utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
EXEC utility.DropObject 'policeengagementupdate.GetPoliceEngagementUpdate'
EXEC utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
EXEC utility.DropObject 'policeengagementupdate.PoliceEngagementUpdate'
EXEC utility.DropObject 'policeengagementupdate.Province'
EXEC utility.DropObject 'policeengagementupdate.ProvinceClass'
EXEC utility.DropObject 'policeengagementupdate.ProvinceIndicator'
EXEC utility.DropObject 'policeengagementupdate.ProvinceRecommendation'
EXEC utility.DropObject 'policeengagementupdate.ProvinceRisk'
EXEC utility.DropObject 'reporting.GetCommunityMemberSurvey'
EXEC utility.DropObject 'reporting.GetFocusGroupSurvey'
EXEC utility.DropObject 'reporting.GetKeyInformantSurvey'
EXEC utility.DropObject 'reporting.GetStakeholderGroupSurvey'
EXEC utility.DropObject 'reporting.GetStationCommanderSurvey'
EXEC utility.DropObject 'survey.FormatSurveyQuestionResponse'
EXEC utility.DropObject 'survey.GetAdministerButtonHTMLBySurveyID'
EXEC utility.DropObject 'survey.GetSurveyBySurveyID'
EXEC utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
EXEC utility.DropObject 'survey.GetSurveyQuestionResponseChoices'
EXEC utility.DropObject 'survey.GetSurveyQuestionResponsesBySurveyResponseID'
EXEC utility.DropObject 'survey.GetSurveyResponseBySurveyResponseID'
EXEC utility.DropObject 'survey.GetViewButtonHTMLBySurveyID'
EXEC utility.DropObject 'survey.Survey'
EXEC utility.DropObject 'survey.SurveyLabel'
EXEC utility.DropObject 'survey.SurveyLanguage'
EXEC utility.DropObject 'survey.SurveyQuestion'
EXEC utility.DropObject 'survey.SurveyQuestionLabel'
EXEC utility.DropObject 'survey.SurveyQuestionLanguage'
EXEC utility.DropObject 'survey.SurveyQuestionResponseChoice'
EXEC utility.DropObject 'survey.SurveyResponse'
EXEC utility.DropObject 'survey.SurveySurveyQuestion'
EXEC utility.DropObject 'survey.SurveySurveyQuestionResponse'
GO

-- Added manually
EXEC utility.DropObject 'dbo.GetAtmosphericByAtmosphericID' 
EXEC utility.DropObject 'dbo.GetCommunityAssetLocations'
EXEC utility.DropObject 'dbo.GetCommunityAssetUnitExpenseNotes'
EXEC utility.DropObject 'dbo.GetCommunityIDFromCommunityAssetID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyByCommunityMemberSurveyID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByProvinceID'
EXEC utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByProvinceID'
EXEC utility.DropObject 'dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.GetProvinceIDFromCommunityAssetID'
EXEC utility.DropObject 'dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID'
EXEC utility.DropObject 'dbo.SpotReportSummaryMapCommunityAsset'
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
EXEC utility.DropObject 'eventlog.LogPoliceEngagementAction'
EXEC utility.DropObject 'reporting.GetCommunityMemberSurvey'
GO

UPDATE P
SET P.PermissionableGroupID = (SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Implementation')
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityRound','CommunityRoundActivity')
GO

UPDATE P
SET P.IsActive = 0
FROM permissionable.Permissionable P
	JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
		AND PG.PermissionableGroupName = 'Insight & Understanding'
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityMemberSurvey','CommunityProvinceEngagement','CommunityProvinceEngagementUpdate','DailyReport','FIFUpdate','FocusGroupSurvey','Justice','KeyEvent','KeyInformantSurvey','PoliceEngagement','RAPData','RapidPerceptionSurvey','StakeholderGroupSurvey','StationCommanderSurvey','Team')
GO

DELETE PG
FROM permissionable.PermissionableGroup PG
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableGroupID = PG.PermissionableGroupID
	)
GO

DECLARE @JobID UNIQUEIDENTIFIER

SELECT @JobID = J.Job_ID
FROM msdb.dbo.sysjobs J WHERE J.Name = 'AJACS dbo.Atmospheric IsCritical Expiration'

IF @JobID IS NOT NULL
	EXEC msdb.dbo.sp_delete_job @job_id=@JobID, @delete_unused_schedule=1
--ENDIF	

SELECT @JobID = J.Job_ID
FROM msdb.dbo.sysjobs J WHERE J.Name = 'AJACS dbo.KeyEvent IsCritical Expiration'

IF @JobID IS NOT NULL
	EXEC msdb.dbo.sp_delete_job @job_id=@JobID, @delete_unused_schedule=1
--ENDIF	
GO
--End object delete

--Begin schema delete
EXEC utility.DropSchema 'communityprovinceengagementupdate'
EXEC utility.DropSchema 'deprecated'
EXEC utility.DropSchema 'fifupdate'
EXEC utility.DropSchema 'justiceupdate'
EXEC utility.DropSchema 'policeengagementupdate'
EXEC utility.DropSchema 'survey'
GO
--End schema delete

--Begin table asset.Asset
DECLARE @TableName VARCHAR(250) = 'asset.Asset'

EXEC utility.DropObject @TableName

CREATE TABLE asset.Asset
	(
	AssetID INT IDENTITY(1,1),
	AssetName NVARCHAR(250),
	AssetDescription NVARCHAR(MAX),
	AssetStatusID INT,
	AssetTypeID INT,
	CommunityID INT,
	IsActive BIT,
	Location GEOMETRY,
	RelocationLocation NVARCHAR(250),
	RelocationDate DATE,
	RelocationNotes NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AssetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetID'
GO

SET IDENTITY_INSERT asset.Asset ON
GO

INSERT INTO asset.Asset
	(AssetID, AssetName, AssetDescription, AssetTypeID, CommunityID, Location)
SELECT
	CA.CommunityAssetID, 
	CA.CommunityAssetName, 
	CA.CommunityAssetDescription, 
	CA.AssetTypeID, 
	CA.CommunityID,
	CA.Location
FROM dbo.CommunityAsset CA
	JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		AND CAT.CommunityAssetTypeName = 'Asset'
GO

SET IDENTITY_INSERT asset.Asset OFF
GO
--End table asset.Asset

--Begin table asset.AssetUnit
DECLARE @TableName VARCHAR(250) = 'asset.AssetUnit'

EXEC utility.DropObject @TableName

CREATE TABLE asset.AssetUnit
	(
	AssetUnitID INT IDENTITY(1,1),
	AssetID INT,
	AssetUnitName NVARCHAR(250),
	AssetUnitCostID INT,
	CommanderContactID INT,
	DeputyCommanderContactID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitCostID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeputyCommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AssetUnitID'
EXEC utility.SetIndexClustered @TableName, 'IX_AssetUnit', 'AssetID,AssetUnitName,AssetUnitID'
GO
--End table asset.AssetUnit

--Begin table asset.AssetUnitExpense
DECLARE @TableName VARCHAR(250) = 'asset.AssetUnitExpense'

EXEC utility.DropObject @TableName

CREATE TABLE asset.AssetUnitExpense
	(
	AssetUnitExpenseID INT IDENTITY(1,1) NOT NULL,
	AssetUnitID INT,
	ExpenseAmountAuthorized NUMERIC(18,2),
	ExpenseAuthorizedDate DATE,
	ExpenseAmountPaid NUMERIC(18,2),
	ExpensePaidDate DATE,
	Notes VARCHAR(MAX),
	ProvinceID INT,
	PaymentMonth INT,
	PaymentYear INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmountAuthorized', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmountPaid', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentMonth', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentYear', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AssetUnitExpenseID'
EXEC utility.SetIndexClustered @TableName, 'IX_AssetUnitExpense', 'PaymentYear,PaymentMonth,ProvinceID,AssetUnitID'
GO

INSERT INTO asset.AssetUnitExpense
	(AssetUnitID, ExpenseAmountAuthorized, ExpenseAuthorizedDate, ExpenseAmountPaid, ExpensePaidDate, Notes, ProvinceID, PaymentMonth, PaymentYear)
SELECT
	CAUE.CommunityAssetUnitID, 
	CAUE.ExpenseAmountAuthorized, 
	CAUE.ExpenseAuthorizedDate, 
	CAUE.ExpenseAmountPaid, 
	CAUE.ExpensePaidDate, 
	CAUE.Notes, 
	CAUE.ProvinceID, 
	CAUE.PaymentMonth,
	CAUE.PaymentYear
FROM dbo.CommunityAssetUnitExpense CAUE
GO
--End table asset.AssetUnitExpense

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'CommunityComponentStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'FIFComponentStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'
EXEC utility.AddColumn @TableName, 'JusticeComponentStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'PoliceComponentStatusID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CommunityComponentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFComponentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'JusticeComponentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PoliceComponentStatusID', 'INT', 0
GO
--End table dbo.Community

--Begin table dbo.CommunityImpactDecisionHistory
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityImpactDecisionHistory'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityImpactDecisionHistory
	(
	CommunityImpactDecisionHistoryID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	CurrentImpactDecisionID INT,
	PreviousImpactDecisionID INT,
	PersonID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CurrentImpactDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PreviousImpactDecisionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityImpactDecisionHistoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityImpactDecisionHistory', 'CreateDateTime,CommunityID,CurrentImpactDecisionID,PreviousImpactDecisionID'
GO
--End table dbo.CommunityImpactDecisionHistory

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.DropColumn @TableName, 'AmmendedConceptNoteID'

EXEC utility.AddColumn @TableName, 'AmendedConceptNoteID', 'INT'
EXEC utility.AddColumn @TableName, 'WorkplanActivityID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'AmendedConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkplanActivityID', 'INT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteContact
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContact'

EXEC utility.DropColumn @TableName, 'USVettingOutcomeID'
EXEC utility.DropColumn @TableName, 'USVettingDate'
EXEC utility.DropColumn @TableName, 'Notes'
EXEC utility.DropColumn @TableName, 'UKVettingOutcomeID'
EXEC utility.DropColumn @TableName, 'UKVettingDate'
GO
--End table dbo.ConceptNoteContact

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.DropColumn @TableName, 'ProvinceID'

EXEC utility.DropColumn @TableName, 'AssetID'
EXEC utility.DropColumn @TableName, 'IsAssetCommander'

EXEC utility.AddColumn @TableName, 'AssetUnitID', 'INT'
EXEC utility.AddColumn @TableName, 'StipendArrears', 'NUMERIC(18,2)'

EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StipendArrears', 'NUMERIC(18,2)', 0
GO

UPDATE C
SET C.AssetUnitID = C.CommunityAssetUnitID
FROM dbo.Contact C
GO
--End table dbo.Contact

--Begin table dbo.Document
DECLARE @TableName VARCHAR(250) = 'dbo.Document'

EXEC utility.AddColumn @TableName, 'DisplayInCommunityNewsFeed', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayInCommunityNewsFeed', 'BIT', 0
GO
--End table dbo.Document

--Begin table dbo.DocumentEntity
DECLARE @TableName VARCHAR(250) = 'dbo.DocumentEntity'

EXEC utility.AddColumn @TableName, 'EntityTypeSubCode', 'VARCHAR(50)'
GO
--End table dbo.DocumentEntity

--Begin table dropdown.AssetStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetStatus
	(
	AssetStatusID INT IDENTITY(0,1) NOT NULL,
	AssetStatusCode VARCHAR(50),
	AssetStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AssetStatusName', 'DisplayOrder,AssetStatusName', 'AssetStatusID'
GO

SET IDENTITY_INSERT dropdown.AssetStatus ON
GO

INSERT INTO dropdown.AssetStatus 
	(AssetStatusID, AssetStatusCode, AssetStatusName, DisplayOrder, IsActive)
VALUES
	(0, NULL, NULL, 0, 1),
	(1, 'Approved', 'Approved', 1, 1),
	(2, 'Planned', 'Planned', 2, 1),
	(3, 'Operational', 'Operational', 3, 1),
	(4, 'NonOperational', 'Non Operational', 4, 1),
	(5, 'TemporarilyRelocated', 'Temporarily Relocated', 5, 1)
GO

SET IDENTITY_INSERT dropdown.AssetStatus OFF
GO
--End table dropdown.AssetStatus

--Begin table dropdown.AssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetType'

EXEC utility.AddColumn @TableName, 'AssetTypeCategory', 'VARCHAR(50)'
GO

DECLARE @tTable TABLE (AssetTypeCategory VARCHAR(50), AssetTypeName VARCHAR(250), Icon VARCHAR(50))

INSERT INTO @tTable
	(AssetTypeCategory, AssetTypeName, Icon)
VALUES
	('Community Engagement', 'Community Building', 'asset-community-building.png'),
	('Community Engagement', 'CSWG Office', 'asset-community-building.png'),
	('Community Engagement', 'Project Location', 'asset-community-building.png'),
	('Justice', 'Court', 'asset-court.png'),
	('Justice', 'Document Centre', 'asset-court.png'),
	('Justice', 'Document Headquarters', 'asset-court.png'),
	('Justice', 'Prison', 'asset-court.png'),
	('Police', 'Criminal Investigation Department', 'asset-police-station.png'),
	('Police', 'Media Office', 'asset-police-station.png'),
	('Police', 'Police Headquarters', 'asset-police-station.png'),
	('Police', 'Police Station', 'asset-police-station.png'),
	('Police', 'Traffic Centre', 'asset-police-station.png'),
	('Police', 'Training Center', 'asset-police-station.png'),
	('Police', 'Warehouse', 'asset-police-station.png'),
	('Research', 'Administrative Centre', 'asset-document-centre.png'),
	('Research', 'Airport', 'asset-document-centre.png'),
	('Research', 'Medical Facility', 'asset-document-centre.png'),
	('Other', 'Other', 'asset-other.png')

MERGE dropdown.AssetType T1
USING (SELECT AssetTypeCategory, AssetTypeName, Icon FROM @tTable T) T2
	ON T2.AssetTypeName = T1.AssetTypeName
WHEN MATCHED THEN 
UPDATE 
SET 
	T1.AssetTypeCategory = T2.AssetTypeCategory,
	T1.Icon = T2.Icon
WHEN NOT MATCHED THEN
INSERT 
	(AssetTypeCategory, AssetTypeName, Icon)
VALUES
	(
	T2.AssetTypeCategory, 
	T2.AssetTypeName, 
	T2.Icon
	);

UPDATE AT
SET AT.DisplayOrder = 
	CASE
		WHEN AT.AssetTypeCategory = 'Other'
		THEN 99
		ELSE 0
	END
FROM dropdown.AssetType AT
GO
--End table dropdown.AssetType

--Begin table dropdown.AssetUnitCost
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetUnitCost'

EXEC utility.DropObject 'dropdown.AssetUnitCostRate'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetUnitCost
	(
	AssetUnitCostID INT IDENTITY(0,1) NOT NULL,
	AssetUnitCostName INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitCostName', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetUnitCostID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_AssetUnitCost', 'DisplayOrder,AssetUnitCostName', 'AssetUnitCostID'
GO

SET IDENTITY_INSERT dropdown.AssetUnitCost ON
GO

INSERT INTO dropdown.AssetUnitCost 
	(AssetUnitCostID, AssetUnitCostName, DisplayOrder)
VALUES
	(0, 0, 1),
	(1, 500, 2),
	(2, 3000, 3)
GO

SET IDENTITY_INSERT dropdown.AssetUnitCost OFF
GO

SET IDENTITY_INSERT asset.AssetUnit ON
GO

INSERT INTO asset.AssetUnit
	(AssetUnitID, AssetID, AssetUnitName, AssetUnitCostID)
SELECT
	CAU.CommunityAssetUnitID, 
	CAU.CommunityAssetID, 
	CAU.CommunityAssetUnitName, 
	(SELECT AUCR.AssetUnitCostID FROM dropdown.AssetUnitCost AUCR WHERE AUCR.AssetUnitCostName = CAUCR.CommunityAssetUnitCostRate)
FROM dbo.CommunityAssetUnit CAU
	JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
GO

SET IDENTITY_INSERT asset.AssetUnit OFF
GO

;
WITH CAUD AS
	(
	SELECT 
		CAU.CommunityAssetID,
		MIN(CAU.CommunityAssetUnitID) AS CommunityAssetUnitID
	FROM dbo.CommunityAssetUnit CAU
	GROUP BY CommunityAssetID
	)

UPDATE DI
SET DI.EndUserEntityID = CAUD.CommunityAssetUnitID
FROM procurement.DistributedInventory DI
	JOIN CAUD ON CAUD.CommunityAssetID = DI.EndUserEntityID
		AND DI.EndUserEntityTypeCode = 'CommunityAsset'

UPDATE DI
SET DI.EndUserEntityTypeCode = 'AssetUnit'
FROM procurement.DistributedInventory DI
WHERE DI.EndUserEntityTypeCode = 'CommunityAsset'
GO
--End table dropdown.AssetUnitCost

--Begin table dropdown.ComponentStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ComponentStatus'

EXEC utility.DropObject 'dropdown.ComponentStatus'

CREATE TABLE dropdown.ComponentStatus
	(
	ComponentStatusID INT IDENTITY(0,1) NOT NULL,
	ComponentStatusName VARCHAR(250),
	DisplayOrder INT,
	HexColor VARCHAR(7),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ComponentStatusID'
EXEC utility.SetIndexClustered @TableName, 'IX_ComponentStatus', 'DisplayOrder,ComponentStatusName,ComponentStatusID'
GO

SET IDENTITY_INSERT dropdown.ComponentStatus ON
GO

INSERT INTO dropdown.ComponentStatus
	(ComponentStatusID, ComponentStatusName, DisplayOrder, HexColor)
VALUES 
	(0, NULL, 0, NULL),
	(1, 'Yes', 1, '#00BB54'),
	(2, 'Pending', 2, '#FFA500'),
	(3, 'No', 3, '#FF0000')

SET IDENTITY_INSERT dropdown.ComponentStatus OFF
GO
--End table dropdown.ComponentStatus

--Begin table dropdown.ConceptNoteType
DECLARE @TableName VARCHAR(250) = 'dropdown.ConceptNoteType'

EXEC utility.AddColumn @TableName, 'ConceptNoteTypeCode', 'VARCHAR(50)'
GO

UPDATE CNT
SET CNT.ConceptNoteTypeCode = REPLACE(CNT.ConceptNoteTypeName, ' ', '')
FROM dropdown.ConceptNoteType CNT
WHERE CNT.ConceptNoteTypeID > 0
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage LIKE 'ConceptNote.View.Budget%'
	OR P.PermissionableLineage = 'ConceptNote.View.ViewBudget'
	OR P.PermissionableLineage = 'ConceptNote.AddUpdate.Risk'
	OR P.PermissionableLineage = 'ConceptNote.View.Risk'
GO

INSERT INTO permissionable.Permissionable
	(IsGlobal, ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsSuperAdministrator)
SELECT
	0,
	'ConceptNote',
	'View',
	'Budget' + CNT.ConceptNoteTypeCode,
	(SELECT P.PermissionableGroupID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'ConceptNote.List'),
	'View the budget on concept notes of component type ' + CNT.ConceptNoteTypeName,
	0
FROM dropdown.ConceptNoteType CNT
WHERE CNT.ConceptNoteTypeID > 0

UNION

SELECT
	0,
	'ConceptNote',
	'AddUpdate',
	'Risk',
	P.PermissionableGroupID,
	'View the risk pane a concept note',
	1
FROM permissionable.Permissionable P 
WHERE P.PermissionableLineage = 'ConceptNote.List'

UNION

SELECT
	0,
	'ConceptNote',
	'View',
	'Risk',
	P.PermissionableGroupID,
	'View the risk pane a concept note',
	1
FROM permissionable.Permissionable P 
WHERE P.PermissionableLineage = 'ConceptNote.List'
GO
--End table dropdown.ConceptNoteType

--Begin table dropdown.ImpactDecision
UPDATE dropdown.ImpactDecision
SET HexColor = '#00BB54'
WHERE ImpactDecisionName = 'Yes'
GO

UPDATE dropdown.ImpactDecision
SET HexColor = '#FFA500'
WHERE ImpactDecisionName = 'Under Review'
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.MediaReportSourceType
DECLARE @TableName VARCHAR(250) = 'dropdown.MediaReportSourceType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MediaReportSourceType
	(
	MediaReportSourceTypeID INT IDENTITY(0,1) NOT NULL,
	MediaReportSourceTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'MediaReportSourceTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MediaReportSourceTypeName', 'DisplayOrder,MediaReportSourceTypeName', 'MediaReportSourceTypeID'
GO

SET IDENTITY_INSERT dropdown.MediaReportSourceType ON
GO

INSERT INTO dropdown.MediaReportSourceType (MediaReportSourceTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.MediaReportSourceType OFF
GO

INSERT INTO dropdown.MediaReportSourceType 
	(MediaReportSourceTypeName)
VALUES
	('Guerrilla Communications'),
	('Outreach'),
	('Print/Physical Product'),
	('Radio'),
	('Social Media'),
	('Traditional Broadcast'),
	('Website')
GO
--End table dropdown.MediaReportSourceType

--Begin table dropdown.MediaReportType
DECLARE @TableName VARCHAR(250) = 'dropdown.MediaReportType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.MediaReportType
	(
	MediaReportTypeID INT IDENTITY(0,1) NOT NULL,
	MediaReportTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'MediaReportTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_MediaReportTypeName', 'DisplayOrder,MediaReportTypeName', 'MediaReportTypeID'
GO

SET IDENTITY_INSERT dropdown.MediaReportType ON
GO

INSERT INTO dropdown.MediaReportType (MediaReportTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.MediaReportType OFF
GO

INSERT INTO dropdown.MediaReportType 
	(MediaReportTypeName, DisplayOrder)
VALUES
	('International', 1),
	('Syria', 2)
GO

INSERT INTO dropdown.MediaReportType 
	(MediaReportTypeName, DisplayOrder)
SELECT
	P.ProvinceName,
	99
FROM dbo.Province P
ORDER BY P.ProvinceName, P.ProvinceID
GO
--End table dropdown.MediaReportType

--Begin table dropdown.ZoneStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ZoneStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ZoneStatus
	(
	ZoneStatusID INT IDENTITY(0,1) NOT NULL,
	ZoneStatusCode VARCHAR(50),
	ZoneStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ZoneStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ZoneStatusName', 'DisplayOrder,ZoneStatusName', 'ZoneStatusID'
GO

SET IDENTITY_INSERT dropdown.ZoneStatus ON
GO

INSERT INTO dropdown.ZoneStatus 
	(ZoneStatusID, ZoneStatusCode, ZoneStatusName, DisplayOrder, IsActive)
VALUES
	(0, NULL, NULL, 0, 1),
	(1, 'Approved', 'Approved', 1, 1),
	(2, 'Planned', 'Planned', 2, 1),
	(3, 'Operational', 'Operational', 3, 1),
	(4, 'NonOperational', 'Non Operational', 4, 1),
	(5, 'TemporarilyRelocated', 'Temporarily Relocated', 5, 1)
GO

SET IDENTITY_INSERT dropdown.ZoneStatus OFF
GO
--End table dropdown.ZoneStatus

--Begin table force.Force
DECLARE @TableName VARCHAR(250) = 'force.Force'

EXEC utility.AddColumn @TableName, 'CommanderFullName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'DeputyCommanderFullName', 'NVARCHAR(250)'
GO

UPDATE F
SET 
	F.CommanderFullName = dbo.FormatContactNameByContactID(F.CommanderContactID, 'LastFirstMiddle'),
	F.DeputyCommanderFullName = dbo.FormatContactNameByContactID(F.DeputyCommanderContactID, 'LastFirstMiddle')
FROM force.Force F
GO	
--End table force.Force

--Begin table force.ForceUnit
DECLARE @TableName VARCHAR(250) = 'force.ForceUnit'

EXEC utility.AddColumn @TableName, 'CommanderFullName', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'DeputyCommanderFullName', 'NVARCHAR(250)'
GO

UPDATE FU
SET 
	FU.CommanderFullName = dbo.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle'),
	FU.DeputyCommanderFullName = dbo.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle')
FROM force.ForceUnit FU
GO	
--End table force.ForceUnit

--Begin table mediareport.MediaReport
DECLARE @TableName VARCHAR(250) = 'mediareport.MediaReport'

EXEC utility.DropObject @TableName

CREATE TABLE mediareport.MediaReport
	(
	MediaReportID INT IDENTITY(1,1) NOT NULL,
	MediaReportTitle VARCHAR(250),
	MediaReportDate DATE,
	MediaReportTypeID INT,
	MediaReportLocation NVARCHAR(250),
	Summary VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediaReportTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'MediaReportID'
GO
--End table mediareport.MediaReport

--Begin table mediareport.MediaReportCommunity
DECLARE @TableName VARCHAR(250) = 'mediareport.MediaReportCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE mediareport.MediaReportCommunity
	(
	MediaReportCommunityID INT IDENTITY(1,1) NOT NULL,
	MediaReportID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediaReportID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0

EXEC utility.SetIndexClustered @TableName, 'IX_MediaReportCommunity', 'MediaReportID,CommunityID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MediaReportCommunityID'
GO
--End table mediareport.MediaReportCommunity

--Begin table mediareport.MediaReportSource
DECLARE @TableName VARCHAR(250) = 'mediareport.MediaReportSource'

EXEC utility.DropObject @TableName

CREATE TABLE mediareport.MediaReportSource
	(
	MediaReportSourceID INT IDENTITY(1,1) NOT NULL,
	MediaReportID INT,
	SourceName NVARCHAR(250),
	SourceAttribution NVARCHAR(MAX),
	SourceDate DATE,
	MediaReportSourceTypeID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'MediaReportID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'MediaReportSourceTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MediaReportSourceID'
EXEC utility.SetIndexClustered @TableName, 'IX_MediaReportSource', 'MediaReportID,MediaReportSourceID'
GO
--End table mediareport.MediaReportSource

--Begin table reporting.StipendPayment
DECLARE @TableName VARCHAR(250) = 'reporting.StipendPayment'

EXEC utility.AddColumn @TableName, 'AssetID', 'INT'
EXEC utility.AddColumn @TableName, 'AssetUnitID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AssetUnitID', 'INT', 0
GO

UPDATE SP
SET 
	SP.AssetID = SP.CommunityAssetID,
	SP.AssetUnitID = SP.CommunityAssetUnitID
FROM reporting.StipendPayment SP
GO
--End table reporting.StipendPayment

--Begin table weeklyreport.Community
DECLARE @TableName VARCHAR(250) = 'weeklyreport.Community'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
GO
--End table weeklyreport.Community

--Begin table weeklyreport.SummaryMapAsset
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapAsset'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapAsset
	(
	SummaryMapAssetID INT IDENTITY(1,1) NOT NULL,
	AssetID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_SummaryMapAsset', 'WeeklyReportID,AssetID'
GO

SET IDENTITY_INSERT weeklyreport.SummaryMapAsset ON
GO

INSERT INTO weeklyreport.SummaryMapAsset
	(SummaryMapAssetID,AssetID,WeeklyReportID)
SELECT
	SMCA.SummaryMapCommunityAssetID,
	SMCA.CommunityAssetID,
	SMCA.WeeklyReportID
FROM weeklyreport.SummaryMapCommunityAsset SMCA
GO

SET IDENTITY_INSERT weeklyreport.SummaryMapAsset OFF
GO
--End table weeklyreport.SummaryMapAsset

--Begin table workplan.Workplan
DECLARE @TableName VARCHAR(250) = 'workplan.Workplan'

IF NOT EXISTS (SELECT 1 FROM sys.tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	EXEC utility.DropObject @TableName

	CREATE TABLE workplan.Workplan
		(
		WorkplanID INT IDENTITY(1,1) NOT NULL,
		WorkplanName VARCHAR(250),
		StartDate DATE,
		EndDate DATE,
		USContractNumber VARCHAR(50),
		UKContractNumber VARCHAR(50),
		FinancialYear INT,
		IsForDashboard BIT,
		IsActive BIT,
		USDToGBPExchangeRate NUMERIC(18,2)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'FinancialYear', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'USDToGBPExchangeRate', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
	EXEC utility.SetDefaultConstraint @TableName, 'IsForDashboard', 'BIT', 0

	EXEC utility.SetPrimaryKeyClustered @TableName, 'WorkplanID'

	END
--ENDIF	

EXEC utility.DropColumn @TableName, 'FinancialYear'
GO
--End table workplan.Workplan

--Begin table workplan.WorkplanActivity
DECLARE @TableName VARCHAR(250) = 'workplan.WorkplanActivity'

IF NOT EXISTS (SELECT 1 FROM sys.tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	EXEC utility.DropObject @TableName

	CREATE TABLE workplan.WorkplanActivity
		(
		WorkplanActivityID INT IDENTITY(1,1) NOT NULL,
		WorkplanActivityName VARCHAR(250),
		WorkplanID INT,
		ConceptNoteTypeID INT,
		CurrentFundingSourceID INT,
		OriginalFundingSourceID INT,
		CurrentGBPAllocation NUMERIC(18,2),
		OriginalGBPAllocation NUMERIC(18,2),
		CurrentUSDAllocation NUMERIC(18,2),
		OriginalUSDAllocation NUMERIC(18,2),
		Description VARCHAR(MAX),
		Notes VARCHAR(MAX)
		)

	EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteTypeID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'CurrentFundingSourceID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'CurrentGBPAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'CurrentUSDAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'OriginalFundingSourceID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'OriginalGBPAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'OriginalUSDAllocation', 'NUMERIC(18,2)', 0
	EXEC utility.SetDefaultConstraint @TableName, 'WorkplanID', 'INT', 0

	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'WorkplanActivityID'
	EXEC utility.SetIndexClustered @TableName, 'IX_WorkplanActivity', 'WorkplanID,WorkplanActivityName'

	END
--ENDIF	
GO
--End table workplan.WorkplanActivity

--Begin table zone.Zone
DECLARE @TableName VARCHAR(250) = 'zone.Zone'

EXEC utility.DropObject @TableName

CREATE TABLE zone.Zone
	(
	ZoneID INT IDENTITY(1,1),
	ZoneName NVARCHAR(250),
	ZoneDescription NVARCHAR(MAX),
	ZoneStatusID INT,
	ZoneTypeID INT,
	TerritoryTypeCode VARCHAR(50),
	TerritoryID INT,
	Location GEOMETRY
	)

EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ZoneStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ZoneTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ZoneID'
GO

INSERT INTO zone.Zone
	(ZoneName, ZoneDescription, ZoneTypeID, TerritoryTypeCode, TerritoryID, Location)
SELECT
	CA.CommunityAssetName, 
	CA.CommunityAssetDescription, 
	CA.ZoneTypeID, 

	CASE
		WHEN CA.CommunityID > 0
		THEN 'Community'
		ELSE 'Province'
	END,

	CASE
		WHEN CA.CommunityID > 0
		THEN CA.CommunityID
		ELSE CA.ProvinceID
	END,

	CA.Location
FROM dbo.CommunityAsset CA
	JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		AND CAT.CommunityAssetTypeName = 'Zone'
GO
--End table zone.Zone
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function asset.GetAssetUnitEquipmentEligibilityByAssetUnitID
EXEC utility.DropObject 'dbo.GetAssetUnitEquipmentEligibilityByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityByAssetUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit Equipment eligibility data
-- =================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibilityByAssetUnitID
(
@AssetUnitID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN C.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND AU.AssetUnitID = @AssetUnitID
		JOIN dbo.Contact C ON C.ContactID = AU.CommanderContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function asset.GetAssetUnitEquipmentEligibilityByAssetUnitID

--Begin function asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID
EXEC utility.DropObject 'dbo.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit Equipment eligibility data
-- =================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID
(
@AssetUnitID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not equipment elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE 
		 		WHEN NOT EXISTS (SELECT 1 FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID)
		 		THEN 'Department has no department head assigned,'
		 		WHEN (SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID) IS NULL 
		 			OR (SELECT DATEADD(m, 6, C.USVettingExpirationDate) FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID) < getDate()  
		 		THEN 'Assigned asset department head has expired vetting,' 
		 		ELSE '' 
		 	END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND AU.AssetUnitID = @AssetUnitID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equipment Eligible'
	ELSE
		SET @cReturn = 'Equipment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID

--Begin function asset.GetCommunityIDByAssetUnitID
EXEC utility.DropObject 'asset.GetCommunityIDFromAssetID'
EXEC utility.DropObject 'asset.GetCommunityIDFromAssetUnitID'
EXEC utility.DropObject 'asset.GetCommunityIDByAssetUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the community associated with an asset unit
-- ==========================================================================
CREATE FUNCTION asset.GetCommunityIDByAssetUnitID
(
@AssetUnitID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCommunityID INT = 0

	SELECT 
		@nCommunityID = A.CommunityID
	FROM asset.Asset A
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
			AND AU.AssetUnitID = @AssetUnitID

	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function asset.GetCommunityIDByAssetUnitID

--Begin function dbo.FormatProgramReportReferenceCode
EXEC utility.DropObject 'dbo.FormatProgramReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted program report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ===========================================================================

CREATE FUNCTION dbo.FormatProgramReportReferenceCode
(
@ProgramReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-PR-' + RIGHT('0000' + CAST(@ProgramReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatProgramReportReferenceCode

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-SR-' + RIGHT('0000' + CAST(@SpotReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function dbo.FormatStaticGoogleMapForCommunityExport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForCommunityExport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date:	2015.12.02
-- Description:	A function to return communities assets for placement on a google map
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForCommunityExport
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	DECLARE @CommunityMarker VARCHAR(MAX)=''

	SELECT
		@CommunityMarker += '&markers=icon:' 
		+ replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + REPLACE(ID.HexColor, '#', '') + '.png' + '|' 
		+ CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.CommunityID = @CommunityID
	
	SET @Gresult = @CommunityMarker

	SELECT
		@GResult += '&markers=icon:' 
		+ replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + AT.Icon + '|' 
		+ CAST(ISNULL(A.Location.STY ,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(A.Location.STX, 0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN asset.Asset A ON A.CommunityID = C.communityID  
			AND C.CommunityID = @CommunityID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID 
			AND AT.AssetTypeID != 0

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForCommunityExport

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@EquipmentDistribbtionID INT,
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @tSource TABLE (CommunityID INT, ProvinceID INT)

	INSERT INTO @tSource
		(CommunityID, ProvinceID)
	SELECT 
		A.CommunityID,
		0 AS ProvinceID				
	FROM procurement.DistributedInventory DI
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = DI.EndUserEntityID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND DI.EndUserEntityTypeCode = 'AssetUnit'

	UNION

	SELECT 
		C.CommunityID,
		0 AS ProvinceID				
	FROM procurement.DistributedInventory DI
		JOIN Contact C ON C.Contactid = DI.EndUserEntityID 
			AND DI.EquipmentDistributionID = @EquipmentDistribbtionID 
			AND DI.EndUserEntityTypeCode = 'Contact'

	UNION 

	SELECT 

		CASE F.TerritoryTypeCode
			WHEN 'Province' 
			THEN 0
			ELSE F.TerritoryID 
		END AS CommunityID,

		CASE F.TerritoryTypeCode
			WHEN 'Community' 
			THEN dbo.GetProvinceIDByCommunityID(TerritoryID)
			ELSE F.TerritoryID 
		END AS ProvinceID 

	FROM procurement.DistributedInventory DI
		JOIN Force.Force F ON F.ForceID = DI.EndUserEntityID 
			AND DI.EquipmentDistributionID = @EquipmentDistribbtionID 
			AND DI.EndUserEntityTypeCode = 'Force'

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(ISNULL(C.Latitude, 0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	FROM @tSource S
		JOIN dbo.Community C ON S.CommunityID = C.CommunityID  
			AND C.ProvinceID = @ProvinceID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / LEN('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan

--Begin function dbo.FormatWeeklyReportReferenceCode
EXEC utility.DropObject 'dbo.FormatWeeklyReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return a formatted Weekly report reference code
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ==========================================================================

CREATE FUNCTION dbo.FormatWeeklyReportReferenceCode
(
@WeeklyReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-WR-' + RIGHT('0000' + CAST(@WeeklyReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatWeeklyReportReferenceCode

--Begin dbo dbo.GetCommunityIDFromContactID
EXEC utility.DropObject 'dbo.GetCommunityIDFromContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the community associated with a contact
-- ======================================================================
CREATE FUNCTION dbo.GetCommunityIDFromContactID
(
@ContactID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCommunityID INT = 0

	SELECT 
		@nCommunityID = C.CommunityID
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function dbo.GetCommunityIDFromContactID

--Begin function dbo.GetCommunityIDFromForceID
EXEC utility.DropObject 'dbo.GetCommunityIDFromForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the community associated with a force
-- ====================================================================
CREATE FUNCTION dbo.GetCommunityIDFromForceID
(
@ForceID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @cTerritoryTypeCode VARCHAR(50)
	DECLARE @nCommunityID INT = 0
	DECLARE @nTerritoryID INT = 0
	
	SELECT 
		@cTerritoryTypeCode = F.TerritoryTypeCode,
		@nTerritoryID = F.TerritoryID
	FROM force.Force F
	WHERE F.ForceID = @ForceID

	IF @cTerritoryTypeCode = 'Community'
		SET @nCommunityID = ISNULL(@nTerritoryID, 0)
	--ENDIF

	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function dbo.GetCommunityIDFromForceID

--Begin function dbo.GetConceptNoteTotalCost
EXEC utility.DropObject 'dbo.GetConceptNoteTotalCost'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.21
-- Description:	A function to return the total budget and equipment cost for a comcept note
-- ========================================================================================

CREATE FUNCTION dbo.GetConceptNoteTotalCost
(
@ConceptNoteID INT
)

RETURNS NUMERIC(18,2)

AS
BEGIN

	DECLARE @nTotalCost NUMERIC(18,2)

	SELECT @nTotalCost = SUM(D.TotalCost)
	FROM
		(
		(SELECT ISNULL(SUM(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost), 0) AS TotalCost FROM dbo.ConceptNoteBudget CNB WHERE CNB.ConceptNoteID = @ConceptNoteID)

		UNION ALL

		(SELECT ISNULL(SUM(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0) AS TotalCost FROM dbo.ConceptNoteEquipmentCatalog CNEC JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID AND CNEC.ConceptNoteID = @ConceptNoteID)
		) D

	RETURN ISNULL(@nTotalCost, 0)

END
GO
--End function dbo.GetConceptNoteTotalCost

--Begin function dbo.GetConceptNoteVersionNumberByConceptNoteID
EXEC utility.DropObject 'dbo.GetConceptNoteVersionNumberByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Kevin Ross
-- Create date:	2016.09.27
-- Description:	A function to return the latest version number of a concept note
-- =============================================================================

CREATE FUNCTION dbo.GetConceptNoteVersionNumberByConceptNoteID
(
@ConceptNoteID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nVersionNumber INT;

	WITH HD AS
		(
		SELECT 
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			1 AS Depth
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			HD.Depth + 1 AS Depth
		FROM dbo.ConceptNote CN
			JOIN HD ON CN.ConceptNoteID = HD.AmendedConceptNoteID
		)

	SELECT @nVersionNumber = (SELECT TOP 1 MAX(HD.Depth) OVER() - HD.Depth + 1 AS InverseDepth
	FROM HD
	ORDER BY InverseDepth DESC)

	RETURN ISNULL(@nVersionNumber, 0)

END
GO
--End function dbo.GetConceptNoteVersionNumberByConceptNoteID

--Begin function dbo.GetContactCommunityByContactID
EXEC utility.DropObject 'dbo.GetContactLocationByContactID'
EXEC utility.DropObject 'dbo.GetContactCommunityByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to return the name of a contact's community or province from a ContactID
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Removed ProvinceID
-- ================================================================================================

CREATE FUNCTION dbo.GetContactCommunityByContactID
(
@ContactID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)
	
	SELECT @cRetVal = C2.CommunityName
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.ContactID = @ContactID
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.GetContactCommunityByContactID

--Begin function dbo.GetContactEquipmentEligibilityByContactID
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityByContactID
(
@ContactID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT


	SELECT @nIsCompliant = 
		CASE
			WHEN C.CommunityID > 0
			THEN dbo.IsCommunityStipendEligible(C.CommunityID)
			ELSE 
				(
				SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
				FROM asset.Asset A
					JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
						AND AU.AssetUnitID = C.AssetUnitID
				)
			END
		 + CASE WHEN C.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 2
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibilityByContactID

--Begin function dbo.GetContactEquipmentEligibilityNotesByContactID
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityNotesByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact equpiment eligibility data
-- ================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityNotesByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cReturn =
		CASE
			WHEN C.CommunityID > 0
			THEN 
				CASE
					WHEN dbo.IsCommunityStipendEligible(C.CommunityID) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			WHEN C.AssetUnitID > 0
			THEN
				CASE
					WHEN 
						(
						SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
						FROM asset.Asset A
							JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
								AND AU.AssetUnitID = C.AssetUnitID
						) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			END
		 + CASE WHEN C.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C.USVettingExpirationDate) >= getDate() THEN '' ELSE 'Contact has expired vetting,' END 
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equpiment Eligible'
	ELSE
		SET @cReturn = 'Equpiment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactEquipmentEligibilityNotesByContactID

--Begin function dbo.GetContactStipendEligibilityByContactID
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityByContactID
(
@ContactID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN C2.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C2.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND C1.ContactID = @ContactID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibilityByContactID

--Begin function dbo.GetContactStipendEligibilityNotesByContactID
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotesByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotesByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE 
		 		WHEN NOT EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID)
		 		THEN 'Department has no department head assigned,'
		 		WHEN (SELECT C1.USVettingExpirationDate FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) IS NULL 
		 			OR (SELECT DATEADD(m, 6, C1.USVettingExpirationDate) FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) < getDate()  
		 		THEN 'Assigned asset department head has expired vetting,' 
		 		ELSE '' 
		 	END
	FROM dbo.Contact C2
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C2.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND C2.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotesByContactID

--Begin function dbo.GetEventNameByEventCode
EXEC utility.DropObject 'dbo.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
--
-- Author:			Todd Pires
-- Create date:	2015.04.30
-- Description:	Added failedlogin support
-- ====================================================================
CREATE FUNCTION dbo.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'finalize'
			THEN 'Finalize'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'read'
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			WHEN @EventCode = 'vettingupdate'
			THEN 'Vetting Update'
			WHEN @EventCode = 'emailed'
			THEN 'Document Emailed'
			ELSE @EventCode + ' not found'
		END

	RETURN @EventCodeName

END
GO
--End function dbo.GetEventNameByEventCode

--Begin function dbo.GetLastVettingOutcome
EXEC utility.DropObject 'dbo.GetLastVettingOutcome'
GO
--End function dbo.GetLastVettingOutcome

--Begin dbo dbo.GetProvinceIDByContactID
EXEC utility.DropObject 'dbo.GetProvinceIDFromContactID'
EXEC utility.DropObject 'dbo.GetProvinceIDByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the province associated with a contact
--
-- Author:		Todd Pires
-- Create date:	2016.09.29
-- Description:	Refactored to use the CommunityID
-- =====================================================================
CREATE FUNCTION dbo.GetProvinceIDByContactID
(
@ContactID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nProvinceID INT = 0

	SELECT 
		@nProvinceID = C2.ProvinceID
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.ContactID = @ContactID

	RETURN ISNULL(@nProvinceID, 0)

END
GO
--End function dbo.GetProvinceIDByContactID

--Begin function dbo.GetProvinceIDFromForceID
EXEC utility.DropObject 'dbo.GetProvinceIDFromForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:		Todd Pires
-- Create date:	2016.09.04
-- Description:	A function to get the province associated with a force
-- ===================================================================
CREATE FUNCTION dbo.GetProvinceIDFromForceID
(
@ForceID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @cTerritoryTypeCode VARCHAR(50)
	DECLARE @nCommunityID INT = 0
	DECLARE @nProvinceID INT = 0
	DECLARE @nTerritoryID INT = 0
	
	SELECT 
		@cTerritoryTypeCode = F.TerritoryTypeCode,
		@nTerritoryID = F.TerritoryID
	FROM force.Force F
	WHERE F.ForceID = @ForceID

	IF @cTerritoryTypeCode = 'Province'
		SET @nProvinceID = ISNULL(@nTerritoryID, 0)
	ELSE
		SET @nProvinceID = dbo.GetProvinceIDByCommunityID(ISNULL(@nTerritoryID, 0))
	--ENDIF

	RETURN ISNULL(@nProvinceID, 0)

END
GO
--End function dbo.GetProvinceIDFromForceID

--Begin function dbo.GetServerSetupValueByServerSetupKey
EXEC utility.DropObject 'dbo.GetServerSetupValueByServerSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A function to return a ServerSetupValue from a ServerSetupKey from the dbo.ServerSetup table
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ======================================================================================================================

CREATE FUNCTION dbo.GetServerSetupValueByServerSetupKey
(
@ServerSetupKey VARCHAR(250),
@DefaultServerSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cServerSetupValue VARCHAR(MAX)
	
	SELECT @cServerSetupValue = SS.ServerSetupValue 
	FROM dbo.ServerSetup SS 
	WHERE SS.ServerSetupKey = @ServerSetupKey
	
	RETURN ISNULL(@cServerSetupValue, @DefaultServerSetupValue)

END
GO
--End function dbo.GetServerSetupValueByServerSetupKey

--Begin function dbo.IsAssetUnitStipendEligible
EXEC utility.DropObject 'dbo.IsAssetUnitStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if an asset unit is stipend eligible
-- =========================================================================

CREATE FUNCTION dbo.IsAssetUnitStipendEligible
(
@AssetUnitID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @nIsCompliant INT

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN C2.USVettingExpirationDate IS NOT NULL AND DATEADD(m, 6, C2.USVettingExpirationDate) >= getDate() THEN 1 ELSE 0 END 
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C1 ON C1.CommunityID = A.CommunityID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.IsAssetUnitStipendEligible

--Begin function dbo.IsAssetUnitCommunityStipendEligible
EXEC utility.DropObject 'dbo.IsAssetUnitCommunityStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if a community associated with an asset unit (via an asset) is stipend eligible
-- ====================================================================================================================

CREATE FUNCTION dbo.IsAssetUnitCommunityStipendEligible
(
@AssetUnitID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 1
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Community C ON C.ImpactDecisionID = ID.ImpactDecisionID
		JOIN asset.Asset A ON A.CommunityID = C.CommunityID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
			AND AU.AssetUnitID = @AssetUnitID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsAssetUnitCommunityStipendEligible

--Begin function procurement.FormatEndUserByEntityTypCodeAndEntityID
EXEC utility.DropObject 'procurement.FormatEndUserByEntityTypCodeAndEntityID'
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.03.05
-- Description:	A function a formatted end user name for a distributed inventory item
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================================
CREATE FUNCTION procurement.FormatEndUserByEntityTypCodeAndEntityID
(
@EntityTypCode VARCHAR(50),
@EntityID INT
)

RETURNS NVARCHAR(300)

AS
BEGIN

	DECLARE @cReturn NVARCHAR(300)

	IF @EntityTypCode = 'AssetUnit'
		SELECT @cReturn = ISNULL(AU.AssetUnitName, '') + ' (Department)' FROM asset.AssetUnit AU WHERE AU.AssetUnitID = @EntityID
	ELSE IF @EntityTypCode = 'Force'
		SELECT @cReturn = ISNULL(F.ForceName, '') + ' (Force)' FROM force.Force F WHERE F.ForceID = @EntityID
	ELSE IF @EntityTypCode = 'Contact'
		SELECT @cReturn = dbo.FormatContactNameByContactID(@EntityID, 'LastFirst')
	--ENDIF
	
	RETURN @cReturn

END
GO
--End function procurement.FormatEndUserByEntityTypCodeAndEntityID

--Begin function procurement.GetDistributedInventoryQuantityAvailable
EXEC utility.DropObject 'procurement.GetDistributedInventoryQuantityAvailable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.02.28
-- Description:	A function to get the quantity available for a DistributedInventory item
--
-- Author:			Todd Pires
-- Create date:	2016.10.24
-- Description:	Bug fix
-- =====================================================================================

CREATE FUNCTION procurement.GetDistributedInventoryQuantityAvailable
(
@EquipmentDistributionID INT,
@EquipmentInventoryID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nReturn INT = 0

	SELECT @nReturn = EI.Quantity - 
										EI.QuantityDistributed - 
										--(
										--SELECT ISNULL(SUM(DI.Quantity), 0)
										--FROM procurement.DistributedInventory DI 
										--	JOIN procurement.EquipmentDistribution ED ON ED.EquipmentDistributionID = DI.EquipmentDistributionID 
										--		AND ED.IsActive = 0
										--		AND DI.EquipmentInventoryID = @EquipmentInventoryID
										--) - 
										(
										SELECT ISNULL(SUM(DI.Quantity), 0)
										FROM procurement.DistributedInventory DI 
										WHERE DI.EquipmentDistributionID = @EquipmentDistributionID
										)
	FROM procurement.EquipmentInventory EI
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
	
	RETURN @nReturn
END
GO
--End function procurement.GetDistributedInventoryQuantityAvailable

--Begin function reporting.GetMediaReportSourceLinks
EXEC utility.DropObject 'reporting.GetMediaReportSourceLinks'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.29
-- Description:	A function to get source link data for the MediaReport SSRS
-- ========================================================================
CREATE FUNCTION reporting.GetMediaReportSourceLinks
(
@MediaReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cSiteURL VARCHAR(MAX)
	DECLARE @cSourceLinks VARCHAR(MAX)

	SELECT @cSiteURL = SS.ServerSetupValue
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SiteURL'

	SET @cSiteURL = '<a href="' + ISNULL(@cSiteURL, '') + '/mediareport/view/id/' + CAST(@MediaReportID AS VARCHAR(10)) + '/">'

	SELECT @cSourceLinks = COALESCE(@cSourceLinks + '<br>', '') + @cSiteURL + MRS.SourceName + '</a>'
	FROM mediareport.MediaReportSource MRS
	WHERE MRS.MediaReportID = @MediaReportID

	RETURN @cSourceLinks
END
GO	
--End function reporting.GetMediaReportSourceLinks

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nEntityID INT = @EntityID
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nEntityID = T.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nEntityID = T.RiskUpdateID FROM riskupdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport'
			SELECT TOP 1 @nEntityID = T.WeeklyReportID FROM weeklyreport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
			
		END
	--ENDIF

	IF @nEntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @nEntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @nEntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @nEntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
	
END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.GetConceptNoteWorkflowStatus
EXEC utility.DropObject 'workflow.GetConceptNoteWorkflowStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.05
-- Description:	A function to get a workflow status name for a concept note
-- ========================================================================
CREATE FUNCTION workflow.GetConceptNoteWorkflowStatus
(
@ConceptNoteID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cStatusName VARCHAR(250) = 'Bad WorkFlow'
	DECLARE @cWorkflowName VARCHAR(250) = LTRIM(RTRIM((SELECT TOP 1 EWSGP.WorkflowName FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Conceptnote' AND EWSGP.EntityID = @ConceptNoteID)))
	DECLARE @nTargetWorkflowStepNumber INT = 0

	IF @cWorkflowName IN ('UK Activity Workflow','US Activity Workflow')
		BEGIN

		IF @cWorkflowName = 'UK Activity Workflow'
			SET @nTargetWorkflowStepNumber = 6
		ELSE IF @cWorkflowName = 'US Activity Workflow'	
			SET @nTargetWorkflowStepNumber = 7
		--ENDIF

		SELECT @cStatusName = 
			CASE
				WHEN CNS.ConceptNoteStatusCode IN ('Amended','Cancelled','Closed','OnHold') 
				THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN CN.WorkflowStepNumber < @nTargetWorkflowStepNumber
						THEN 'Development'
						WHEN CN.WorkflowStepNumber = @nTargetWorkflowStepNumber
						THEN 'Implementation'
						ELSE 'Closedown'
					END
			END

		FROM dbo.ConceptNote CN
			JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
				AND CN.ConceptNoteID = @ConceptNoteID

		END
	--ENDIF

	RETURN @cStatusName

END
GO
--End function workflow.GetConceptNoteWorkflowStatus
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - A thru C - Procedures.sql
USE AJACS2
GO

--Begin procedure asset.GetAssetByAssetID
EXEC Utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the asset.Asset table
-- ======================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		A.AssetDescription,
		A.AssetID,
		A.AssetName,
		A.CommunityID,
		dbo.GetCommunityNameByCommunityID(A.CommunityID) AS CommunityName,
		A.IsActive,
		A.Location.STAsText() AS Location,
		A.RelocationDate,
		dbo.FormatDate(A.RelocationDate) AS RelocationDateFormatted,
		A.RelocationLocation,
		A.RelocationNotes,
		AST.AssetStatusCode,
		AST.AssetStatusID,
		AST.AssetStatusName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon
	FROM asset.Asset A
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	SELECT
		AU.AssetUnitID,
		AU.AssetUnitName,
		AU.CommanderContactID,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullNameFormatted,
		AU.DeputyCommanderContactID,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullNameFormatted,
		AU.IsActive,
		AUC.AssetUnitCostID,
		AUC.AssetUnitCostName
	FROM asset.AssetUnit AU
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			AND AU.AssetID = @AssetID

	;
	WITH CD AS
		(
		SELECT AU.CommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID

		UNION

		SELECT AU.DeputyCommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID
		)

	SELECT 
		dbo.FormatContactNameByContactID(US.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'US' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(US.VettingDate) AS VettingDateFormatted,
		VO1.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN DATEADD(m, 6, US.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO1.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			A.ContactID,
			A.ContactVettingID,
			A.VettingDate,
			A.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD1.ContactID ORDER BY CV1.VettingDate DESC, CV1.ContactVettingID DESC) AS RowIndex,
				CD1.ContactID,
				CV1.ContactVettingID,
				CV1.VettingDate,
				CV1.VettingOutcomeID
			FROM CD CD1
				LEFT JOIN dbo.ContactVetting CV1 ON CV1.ContactID = CD1.ContactID
			WHERE CV1.ContactVettingTypeID = 1
			) A
			WHERE A.RowIndex = 1
		) US
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = ISNULL(US.VettingOutcomeID, 0)

	UNION

	SELECT 
		dbo.FormatContactNameByContactID(UK.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'UK' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(UK.VettingDate) AS VettingDateFormatted,
		VO2.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN DATEADD(m, 6, UK.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO2.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			B.ContactID,
			B.ContactVettingID,
			B.VettingDate,
			B.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD2.ContactID ORDER BY CV2.VettingDate DESC, CV2.ContactVettingID DESC) AS RowIndex,
				CD2.ContactID,
				CV2.ContactVettingID,
				CV2.VettingDate,
				CV2.VettingOutcomeID
			FROM CD CD2
				LEFT JOIN dbo.ContactVetting CV2 ON CV2.ContactID = CD2.ContactID
			WHERE CV2.ContactVettingTypeID = 2
			) B
			WHERE B.RowIndex = 1
		) UK
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = ISNULL(UK.VettingOutcomeID, 0)

	ORDER BY 1, 2, 3, 4

END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure asset.GetAssetUnitExpenseNotes
EXEC Utility.DropObject 'asset.GetAssetUnitExpenseNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	A stored procedure to get notes field from the AssetUnitExpense table
-- ==================================================================================
CREATE PROCEDURE asset.GetAssetUnitExpenseNotes

@AssetUnitExpenseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		AUE.Notes
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.AssetUnitExpenseID = @AssetUnitExpenseID
	
END
GO
--End procedure asset.GetAssetUnitExpenseNotes

--Begin procedure asset.GetAssetLocations
EXEC Utility.DropObject 'asset.GetAssetLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.17
-- Description:	A stored procedure to get location data from the asset.Asset table
-- ===============================================================================
CREATE PROCEDURE asset.GetAssetLocations

@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF ISNULL(@Boundary, '') != ''
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	--ENDIF

	SELECT
		A.AssetID,
		A.AssetName,
		A.Location.STAsText() AS Location,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND 
				(
				ISNULL(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(A.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR A.CommunityID = @CommunityID
				)

END
GO
--End procedure asset.GetAssetLocations

--Begin procedure asset.RelocateAssetUnitContacts
EXEC Utility.DropObject 'asset.RelocateAssetUnitContacts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.17
-- Description:	A stored procedure move assetuintcontacts to a new community when an asset community changes
-- =========================================================================================================
CREATE PROCEDURE asset.RelocateAssetUnitContacts

@AssetID INT,
@AssetUnitID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @cComment VARCHAR(MAX) = 'Community changed due to relocation of parent of assigned asset department.'
	DECLARE @cContactIDList VARCHAR(MAX)
	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY)

	IF @AssetUnitID IS NOT NULL
		BEGIN

		SET @cComment = 'Community changed due to transfer of assigned asset department.'

		UPDATE AU
		SET AU.AssetID = @AssetID
		FROM asset.AssetUnit AU
		WHERE AU.AssetUnitID = @AssetUnitID

		EXEC eventlog.LogAssetAction @AssetID, 'update', @PersonID, 'Asset Department Transfer'

		END
	--ENDIF

	UPDATE C
	SET C.CommunityID = A.CommunityID
	OUTPUT INSERTED.ContactID INTO @tOutput
	FROM dbo.Contact C
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			AND (@AssetUnitID IS NULL OR AU.AssetUnitID = @AssetUnitID)
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID

	SELECT @cContactIDList = STUFF((SELECT ',' + CAST(O.ContactID AS VARCHAR(10)) FROM @tOutput O ORDER BY O.ContactID FOR XML PATH('')), 1, 1, '')

	IF EXISTS (SELECT 1 FROM @tOutput)
		EXEC eventlog.LogContactAction 0, 'update', @PersonID, @cComment, @cContactIDList 
	--ENDIF

END
GO
--End procedure asset.RelocateAssetUnitContacts

--Begin procedure asset.SaveAssetUnitExpenseNotes
EXEC Utility.DropObject 'asset.SaveAssetUnitExpenseNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	A stored procedure to update the comments field in the AssetUnitExpense table
-- ==========================================================================================
CREATE PROCEDURE asset.SaveAssetUnitExpenseNotes

@AssetUnitExpenseID INT,
@Notes VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE AUE
	SET AUE.Notes = @Notes
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.AssetUnitExpenseID = @AssetUnitExpenseID
	
END
GO
--End procedure asset.SaveAssetUnitExpenseNotes

--End file Build File - 03 - A thru C - Procedures.sql

--Begin file Build File - 03 - D thru Dq - Procedures.sql
USE AJACS2
GO

--Begin procedure dbo.AddContactStipendPaymentContacts
EXEC Utility.DropObject 'dbo.AddContactStipendPaymentContacts'
EXEC Utility.DropObject 'dbo.ImportPayees'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	A stored procedure to add payees to the ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016..09.18
-- Description:	Refactored for asset support
-- ================================================================================
CREATE PROCEDURE dbo.AddContactStipendPaymentContacts

@PaymentMonth INT,
@PaymentYear INT,
@PersonID INT,
@ContactIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY, StipendTypeCode VARCHAR(50))

	IF @ContactIDList = ''
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)
			JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Contact'
				AND SR.EntityID = C.ContactID
				AND SR.PersonID = @PersonID

		END
	ELSE
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)
			JOIN dbo.ListToTable(@ContactIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ContactID

		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM @tOutput T WHERE T.StipendTypeCode = 'JusticeStipend')
		BEGIN
		
		INSERT INTO asset.AssetUnitExpense
			(AssetUnitID, ExpenseAmountAuthorized, ProvinceID, PaymentMonth, PaymentYear)
		SELECT DISTINCT
			AU.AssetUnitID,
			AUC.AssetUnitCostName,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			@PaymentMonth,
			@PaymentYear
		FROM asset.AssetUnit AU
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			JOIN dbo.Contact C ON C.AssetUnitID = AU.AssetUnitID
			JOIN @tOutput O ON O.ContactID = C.ContactID
				AND NOT EXISTS
					(
					SELECT 1
					FROM asset.AssetUnitExpense AUE
					WHERE AUE.AssetUnitID = AU.AssetUnitID
						AND AUE.PaymentMonth = @PaymentMonth
						AND AUE.PaymentYear = @PaymentYear
					)

		END
	--ENDIF
	
END
GO
--End procedure dbo.AddContactStipendPaymentContacts

--Begin procedure dbo.AddContactsToConceptNote
EXEC Utility.DropObject 'dbo.AddContactsToConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.03
-- Description:	A stored procedure to add a list of contacts to a concept note
-- ===========================================================================
CREATE PROCEDURE dbo.AddContactsToConceptNote

@ConceptNoteID INT,
@ContactIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.ConceptNoteContact
		(ConceptNoteID, ContactID)
	SELECT
		@ConceptNoteID,
		CAST(LTT.ListItem AS INT)
	FROM dbo.ListToTable(@ContactIDList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.ConceptNoteContact CNC
		WHERE CNC.ConceptNoteID = @ConceptNoteID
			AND CNC.ContactID = CAST(LTT.ListItem AS INT)
		)

END
GO
--End procedure dbo.AddContactsToConceptNote

--Begin procedure dbo.ApproveContactStipendPayment
EXEC Utility.DropObject 'dbo.ApproveContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ApproveContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET 
		CSP.StipendAuthorizedDate = getDate(),
		CSP.StipendAmountPaid = 
			CASE
				WHEN dbo.GetContactStipendEligibilityByContactID(CSP.ContactID) = 1
				THEN CSP.StipendAmountAuthorized
				ELSE 0
			END

	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = @StipendTypeCode
		AND CSP.StipendAuthorizedDate IS NULL

	IF @StipendTypeCode = 'JusticeStipend'
		BEGIN

		UPDATE AUE
		SET 
			AUE.ExpenseAuthorizedDate = getDate(),
			AUE.ExpenseAmountPaid = AUE.ExpenseAmountAuthorized
		FROM asset.AssetUnitExpense AUE
		WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
			AND AUE.ProvinceID = @ProvinceID
			AND AUE.ExpenseAuthorizedDate IS NULL

		END
	--ENDIF

END
GO
--End procedure dbo.ApproveContactStipendPayment

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT,
@WorkflowID INT,
@IsAmendment BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @IsAmendment = 1
		BEGIN

		SELECT @WorkflowID = EWSGP.WorkflowID
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = 'ConceptNote' 
			AND EWSGP.EntityID = @ConceptNoteID

		END
	--ENDIF

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,ActualTotalAmount,AmendedConceptNoteID,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,EndDate,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,StartDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber,WorkplanActivityID)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,

		CASE
			WHEN @IsAmendment = 1
			THEN C.ActualTotalAmount
			ELSE 0
		END,

		CASE
			WHEN @IsAmendment = 1
			THEN @ConceptNoteID
			ELSE 0
		END,

		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.EndDate,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.StartDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,

		CASE
			WHEN @IsAmendment = 1
			THEN C.Title
			ELSE 'Clone of:  ' + C.Title
		END,

		C.VettingRequirements,
		1,
		WorkplanActivityID		
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@nConceptNoteID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.WorkflowID = @WorkflowID

	INSERT INTO	dbo.ConceptNoteAmendment
		(ConceptNoteID, AmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Amendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Amendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.DeleteContactStipendPayment
EXEC Utility.DropObject 'dbo.DeleteContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to delete data from the dbo.DeleteContactStipendPayment table
-- =============================================================================================
CREATE PROCEDURE dbo.DeleteContactStipendPayment

@ContactStipendPaymentID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE CSP
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.ContactStipendPaymentID = @ContactStipendPaymentID

END
GO
--End procedure dbo.DeleteContactStipendPayment

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
--
-- Author:			Todd Pires
-- Update date:	2015.05.28
-- Description:	Added vetting data
--
-- Author:			Todd Pires
-- Update date:	2016.09.09
-- Description:	Pointed to the dbo.ContactVetting table
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName,
		ISNULL(CNC2.ConceptNoteID, 0) AS ConceptNoteID
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT CNC1.ConceptNoteID 
			FROM dbo.ConceptNoteClass CNC1
			WHERE CNC1.ClassID = @ClassID
			) CNC2
	
	SELECT
		dbo.GetContactCommunityByContactID(CO.ContactID) AS ContactLocation,
		CO.ContactID,
		CNC2.VettingIcon,
		CNC2.VettingOutcomeName,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT TOP 1

				CASE
					WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
					THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
					ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
				END AS VettingIcon,

				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND CV.ContactID = CC.ContactID
			ORDER BY CV.ContactVettingID DESC
			) CNC2
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'Class'
			AND DE.EntityID = @ClassID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetCommunityByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Community table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
--
-- Author:			John Lyons
-- Create date:	2015.09.28
-- Description:	Added the ArabicCommunityName column
--
-- Author:			Johnathan Burnham
-- Create date:	2016.09.25
-- Description:	Refactored
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nCEProjectsCount INT
	DECLARE @nCESpendValue NUMERIC(18,2)
	DECLARE @nJusticeAssetsCount INT
	DECLARE @nJusticeSpendValue NUMERIC(18,2)
	DECLARE @nJusticeStipendaryCount INT
	DECLARE @nPoliceAssetsCount INT
	DECLARE @nPoliceSpendValue NUMERIC(18,2)
	DECLARE @nPoliceStipendaryCount INT

	DECLARE @tTable TABLE (ComponentCode VARCHAR(50), ConceptNoteID INT, BudgetTotal NUMERIC(18,2))

	SELECT 
		C.ArabicCommunityName, 
		C.CommunityEngagementStatusID, 
		C.CommunityGroupID, 
		C.CommunityID, 
		C.CommunityName, 
		C.CommunitySubGroupID, 
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyPoints, 
		C.Latitude, 
		C.Longitude,
		C.PolicePresenceCategoryID, 
		C.PolicePresenceStatusID, 
		C.PoliceStationNumber, 
		C.PoliceStationStatusID, 
		C.Population, 
		C.PopulationSource, 
		C.ProgramNotes1, 
		C.ProgramNotes2, 
		C.ProgramNotes3, 
		C.ProgramNotes4, 
		C.ProgramNotes5, 
		C.ProvinceID,
		C.RiskMitigation, 
		C.StatusChangeID, 
		C.Summary, 
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CSG.CommunitySubGroupName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Community', @CommunityID) AS UpdateDateFormatted,
		ID.ImpactDecisionName,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		PPS.PolicePresenceStatusName,
		PSS.PoliceStationStatusName,
		SC.StatusChangeName,
		C.CommunityComponentStatusID,
		C.JusticeComponentStatusID,
		C.PoliceComponentStatusID,
		CS1.ComponentStatusName AS CommunityComponentStatusName,
		CS2.ComponentStatusName AS JusticeComponentStatusName,
		CS3.ComponentStatusName AS PoliceComponentStatusName,
		ISNULL(CS1.HexColor,'#000') AS CommunityComponentStatusHex,
		ISNULL(CS2.HexColor,'#000') AS JusticeComponentStatusHex,
		ISNULL(CS3.HexColor,'#000') AS PoliceComponentStatusHex
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.PolicePresenceStatus PPS ON PPS.PolicePresenceStatusID = C.PolicePresenceStatusID
		JOIN dropdown.PoliceStationStatus PSS ON PSS.PoliceStationStatusID = C.PoliceStationStatusID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
		JOIN dropdown.ComponentStatus CS1 ON CS1.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus CS2 ON CS2.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus CS3 ON CS3.ComponentStatusID = C.PoliceComponentStatusID
			AND C.CommunityID = @CommunityID

	SELECT @nJusticeStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'JusticeStipend' 
			) 
			AND C.IsActive = 1
	
	SELECT @nPoliceStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'PoliceStipend' 
			) 
			AND C.IsActive = 1

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'AccesstoJustice',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'AccesstoJustice'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'PoliceDeveoopment',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'PoliceDevelopment'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'CommunityEngagement',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'CommunityEngagement'

	SELECT @nCEProjectsCount = COUNT(P.ProjectID)
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
		JOIN dropdown.Component CT ON P.ComponentID = CT.ComponentID
			AND PC.CommunityID = @CommunityID
			AND CT.ComponentAbbreviation = 'CE'
	
	SELECT @nJusticeAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Justice'
	
	SELECT @nPoliceAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Police'

	SELECT @nCESpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'CommunityEngagement'

	SELECT @nJusticeSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'AccesstoJustice'

	SELECT @nPoliceSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'PoliceDevelopment'

	SELECT 
		@nCEProjectsCount AS CEProjectsCount,
		@nJusticeAssetsCount AS JusticeAssetsCount,
		@nJusticeStipendaryCount AS JusticeStipendaryCount,
		@nPoliceAssetsCount AS PoliceAssetsCount,
		@nPoliceStipendaryCount AS PoliceStipendaryCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'AccesstoJustice') AS ActiveJusticeActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'CommunityEngagement') AS ActiveCEActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'PoliceDevelopment') AS ActivePoliceActivitySheetCount,
		FORMAT(@nCESpendValue, 'C', 'en-us') AS CESpendValueFormatted,
		FORMAT(@nJusticeSpendValue, 'C', 'en-us') AS JusticeSpendValueFormatted,
		FORMAT(@nPoliceSpendValue, 'C', 'en-us') AS PoliceSpendValueFormatted

END
GO
--End procedure dbo.GetCommunityByCommunityID

--Begin procedure dbo.GetCommunityFeed
EXEC Utility.DropObject 'dbo.GetCommunityFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetCommunityFeed

@PersonID INT = 0,
@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID = @CommunityID

				UNION

				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.ProvinceID = dbo.GetProvinceIDByCommunityID(@CommunityID)
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID = @CommunityID

				UNION

				SELECT 1
				FROM dbo.SpotReportProvince SRP
				WHERE SRP.ProvinceID = dbo.GetProvinceIDByCommunityID(@CommunityID)
				)

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
			AND ELD.EntityTypeCode = 'WeeklyReport'
			AND D.PhysicalFileName IS NOT NULL

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Communit Engagement Permitted Change' AS EntityTypeName,			
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS PhysicalFileName,
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
			AND CIDH.CommunityID = @CommunityID

	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetCommunityFeed

--Begin procedure dbo.GetCommunityLocations
EXEC Utility.DropObject 'dbo.GetCommunityLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to get location data from the dbo.Community table
--
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	Refactored
-- =================================================================================
CREATE PROCEDURE dbo.GetCommunityLocations
@CommunityID INT = 0,
@ProvinceID INT = 0,
@Boundary VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT 
		C.CommunityID,
		C.CommunityName,
		CAST(C.Latitude AS NUMERIC(13,8)) AS Latitude,
		CAST(C.Longitude AS NUMERIC(13,8)) AS Longitude,
		C.Population,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + LEFT(ACS.ComponentStatusName, 1) + '.png' AS Icon, 
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName, 
		ID.ImpactDecisionID,
		ACS.ComponentStatusID,
		ACS.ComponentStatusName,
		ACS.HexColor AS EngagedHexColor,
		CES.CommunityEngagementStatusName, 
		CES.CommunityEngagementStatusID,
		CCS.ComponentStatusID AS CommunityComponentStatusID,
		CCS.ComponentStatusName AS CommunityComponentStatusName,
		PCS.ComponentStatusID AS PoliceComponentStatusID,
		PCS.ComponentStatusName AS PoliceComponentStatusName,
		JCS.ComponentStatusID AS JusticeComponentStatusID,
		JCS.ComponentStatusName AS JusticeComponentStatusName
	FROM dbo.Community C 
		JOIN 
			(
			SELECT 
				C.CommunityID,
			  (
				SELECT MIN(T.ComponentStatusID)
				FROM 
					(
					VALUES (CommunityComponentStatusID),(JusticeComponentStatusID), (PoliceComponentStatusID)
					) AS T(ComponentStatusID)
				) AS ComponentStatusID
			FROM dbo.Community C
			) D ON D.CommunityID = C.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ComponentStatus CCS ON CCS.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus PCS ON PCS.ComponentStatusID = C.PoliceComponentStatusID
		JOIN dropdown.ComponentStatus JCS ON JCS.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus FCS ON FCS.ComponentStatusID = C.FIFComponentStatusID
		JOIN dropdown.ComponentStatus ACS ON ACS.ComponentStatusID = D.ComponentStatusID
			AND C.IsActive = 1
			AND 
				(
				@CommunityID = 0
					OR C.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR C.ProvinceID = @ProvinceID
				)
			AND 
				(
				ISNULL(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(C.Location) = 1
				)
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure dbo.GetCommunityLocations

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
--
-- Author:			Todd Pires
-- Create date: 2016.09.30
-- Description:	Removed the document query
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID,
		C.CommunityName,
		C.ImpactDecisionID,
		C.PolicePresenceCategoryID,
		C.Population,
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CR.ActivitiesOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.ActivityImplementationEndDate,
		dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
		CR.ActivityImplementationStartDate,
		dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
		CR.ApprovedActivityCount,
		CR.AreaManagerPersonID,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		CR.CommunityCode,
		CR.CommunityID,
		CR.CommunityRoundCivilDefenseCoverageID,
		CR.CommunityRoundGroupID,
		CR.CommunityRoundID,
		CR.CommunityRoundJusticeActivityID,
		CR.CommunityRoundOutput11StatusID,
		CR.CommunityRoundOutput12StatusID,
		CR.CommunityRoundOutput13StatusID,
		CR.CommunityRoundOutput14StatusID,
		CR.CommunityRoundPreAssessmentStatusID,
		CR.CommunityRoundRAPInfoID,
		CR.CommunityRoundRoundID,
		CR.CommunityRoundTamkeenID,
		CR.CSAP2PeerReviewMeetingDate,
		dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
		CR.CSAP2SubmittedDate,
		dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
		CR.CSAPBeginDevelopmentDate,
		dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
		CR.CSAPCommunityAnnouncementDate,
		dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
		CR.CSAPFinalizedDate,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		CR.CSAPProcessStartDate,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		CR.FemaleQuestionnairCount,
		CR.FieldFinanceOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerContactName,
		CR.FieldLogisticOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerContactName,
		CR.FieldOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficerContactName,
		CR.FinalSWOTDate,
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
		CR.InitialSCADate,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
		CR.IsActive,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.KickoffMeetingCount,
		CR.MOUDate,
		dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
		CR.NewCommunityEngagementCluster,
		CR.Preassessment,
		CR.Postassessment,
		CR.ProjectOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		CR.QuestionnairCount,
		CR.SCAFinalizedDate,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
		CR.SCAMeetingCount,
		CR.SensitizationDefineFSPRoleDate,
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
		CR.SensitizationDiscussCommMakeupDate,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
		CR.SensitizationMeetingMOUDate,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
		CR.ToRDate,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		FOHT.FieldOfficerHiredTypeID,
		FOHT.FieldOfficerHiredTypeName,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		CRES.CommunityRoundEngagementStatusID,
		CRES.CommunityRoundEngagementStatusName,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Male'  
		) AS CSWGMaleCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Female' 
		) AS CSWGFemaleCount
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.FieldOfficerHiredType FOHT ON FOHT.FieldOfficerHiredTypeID = CR.FieldOfficerHiredTypeID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
			AND CR.CommunityRoundID = @CommunityRoundID

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		C.EmployerName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,
		ISNULL(OACV12.VettingOutcomeName, 'Not Vetted') AS USVettingOutcomeName,
		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,
		ISNULL(OACV22.VettingOutcomeName, 'Not Vetted') AS UKVettingOutcomeName,
		CCSWGC.ContactCSWGClassificationID,
		CCSWGC.ContactCSWGClassificationName
	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = CV0.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C1.ContactID
					AND CRC.CommunityRoundID = @CommunityRoundID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT 
				CV12.VettingOutcomeID,
				VO1.VettingOutcomeName
			FROM dbo.ContactVetting CV12
			JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT 
				CV22.VettingOutcomeID,
				VO2.VettingOutcomeName
			FROM dbo.ContactVetting CV22
			JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
	ORDER BY 3, 1

	--Updates
	SELECT
		CRU.CommunityRoundUpdateID, 
		CRU.CommunityRoundID, 
		CRU.FOUpdates, 
		CRU.POUpdates, 
		CRU.NextStepsUpdates, 
		CRU.NextStepsProcess, 
		CRU.Risks, 
		CRU.UpdateDate,
		dbo.FormatDate(CRU.UpdateDate) AS UpdateDateFormatted
	FROM dbo.CommunityRoundUpdate CRU
	WHERE CRU.CommunityRoundID = @CommunityRoundID
	ORDER BY CRU.UpdateDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure dbo.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteBudget table
--
-- Author:			Greg Yingling
-- Update Date: 2015.05.02
-- Description:	Added BudgetSubType information
-- ================================================================================
CREATE PROCEDURE dbo.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Amendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteBudgetByConceptNoteID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
--
-- Author:			Todd Pires
-- Update Date: 2016.09.01
-- Description:	Added the ConceptNoteTypeCode field
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeCode AS ConceptNoteComponentCode,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed,
		CN.AmendedConceptNoteID,
		ACN.Title AS AmendedConceptNoteTitle,
		ABCN.ConceptNoteID AS AmendedByConceptNoteID,
		ABCN.Title AS AmendedByConceptNoteTitle,
		CN.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
		LEFT JOIN dbo.ConceptNote ACN ON ACN.ConceptNoteID = CN.AmendedConceptNoteID
		LEFT JOIN dbo.ConceptNote ABCN ON ABCN.AmendedConceptNoteID = CN.ConceptNoteID
		LEFT JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
	WHERE CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	--Budget
	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Amendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	--Communities
	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	--Contacts
	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.USVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.USVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.UKVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.UKVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID
			FROM dbo.ContactVetting CV12
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID
			FROM dbo.ContactVetting CV22
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1
				
	--Ethnicity
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	--Classes
	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	--Equipment Budget
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	--Indicators
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	--Provinces
	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	--Tasks
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	--Documents
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	--Authors
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--Amendments (deprecated)
	SELECT			
		CNA.ConceptNoteAmendmentID,
		CNA.AmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--Risks
	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	--Projects
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	--Transactions
	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID
	
	--Update Details
	SELECT
		CNU.ConceptNoteUpdateID,
		CNU.PersonID, 
		dbo.FormatPersonNameByPersonID(CNU.PersonID, 'LastFirst') AS FullNameFormatted,
		CNU.Remarks,
		CNU.UpdateDate,
		dbo.FormatDate(CNU.UpdateDate) AS UpdateDateFormatted,
		CNUT.ConceptNoteUpdateTypeID,
		CNUT.ConceptNoteUpdateTypeName,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		CNU.ActualTotalAmount
	FROM dbo.ConceptNoteUpdate CNU
		JOIN dropdown.ConceptNoteUpdateType CNUT ON CNUT.ConceptNoteUpdateTypeID = CNU.ConceptNoteUpdateTypeID
		LEFT JOIN dbo.Document D ON D.DocumentID = CNU.DocumentID
	WHERE CNU.ConceptNoteID = @ConceptNoteID
	
	--Budget Versions
	;WITH HD AS
	(
	SELECT
		CN.ConceptNoteID, 
		CN.AmendedConceptNoteID, 
		CN.Title, 
		1 AS Depth
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	UNION ALL

	SELECT 
		CN.ConceptNoteID, 
		CN.AmendedConceptNoteID, 
		CN.Title, 
		HD.Depth + 1 AS Depth
	FROM dbo.ConceptNote CN
		JOIN HD ON CN.ConceptNoteID = HD.AmendedConceptNoteID
	)

	SELECT 
		HD.ConceptNoteID,
		HD.Title,
		MAX(HD.Depth) OVER() - HD.Depth + 1 AS VersionNumber,
		(SELECT ISNULL(SUM(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost), 0)
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = HD.ConceptNoteID
		) + (SELECT ISNULL(SUM(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0)
			FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = HD.ConceptNoteID
		) AS TotalCost
	FROM HD
	ORDER BY VersionNumber
	
	--Workflow Data
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD
	
	--Workflow People
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--Workflow Event Log
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.AssetUnitID,		
		(SELECT AU.AssetUnitName FROM asset.AssetUnit AU WHERE AU.AssetUnitID = C1.AssetUnitID) AS AssetUnitName,		
		(SELECT A.AssetName FROM asset.Asset A JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID AND AU.AssetUnitID = C1.AssetUnitID) AS AssetName,
		C1.CommunityID,		
		dbo.GetCommunityNameByCommunityID(C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate((SELECT EL.CreateDateTime FROM eventlog.EventLog EL WHERE EL.EventCode = 'create' AND EL.EntityTypeCode = 'Contact' AND EL.EntityID = C1.ContactID)) AS InitialEntryDateFormatted,
		C1.IsActive,
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(SELECT CSG.CommunitySubGroupName FROM dropdown.CommunitySubGroup CSG JOIN dbo.Community C ON C. CommunitySubGroupID = CSG.CommunitySubGroupID AND C.CommunityID = C1.CommunityID) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID, 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,

		CASE
			WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
			AND CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
	--Get Document By Contact ID
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID
	ORDER BY D.DocumentDescription

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetDashboardItemCounts
EXEC Utility.DropObject 'dbo.GetDashboardItemCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.19
-- Description:	A stored procedure to get item count data for the dashboard
--
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	Added Spot Reports
--
-- Author:			Todd Pires
-- Create date:	2016.09.30
-- Description:	Refactored
-- ========================================================================
CREATE PROCEDURE dbo.GetDashboardItemCounts

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE
		(
		ConceptNoteTypeName VARCHAR(250),
		ActivityCount INT DEFAULT 0,
		ActivityCountPercent NUMERIC(18,2) DEFAULT 0,
		CommittedBalance NUMERIC(18,2) DEFAULT 0,
		CommittedBalancePercent NUMERIC(18,2) DEFAULT 0,
		PendingCommitments NUMERIC(18,2) DEFAULT 0,
		PendingCommitmentsPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanAllocation NUMERIC(18,2) DEFAULT 0,
		WorkplanAllocationPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanCommitted NUMERIC(18,2) DEFAULT 0,
		WorkplanCommittedPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanDisbursed NUMERIC(18,2) DEFAULT 0,
		WorkplanDisbursedPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanUncommitted NUMERIC(18,2) DEFAULT 0,
		WorkplanUncommittedPercent NUMERIC(18,2) DEFAULT 0
		)

	DECLARE @tTable2 TABLE (ItemCount INT  DEFAULT 0, ItemName VARCHAR(250))

	INSERT INTO @tTable1 (ConceptNoteTypeName) SELECT CNT.ConceptNoteTypeName FROM dropdown.ConceptNoteType CNT WHERE CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
	INSERT INTO @tTable1 (ConceptNoteTypeName) VALUES ('Other'), ('Total')

	--Component Engagement Counts
	SELECT 
		PVT.Component,
		ISNULL(PVT.No, 0) AS No,
		ISNULL(PVT.Pending, 0) AS Pending,
		ISNULL(PVT.Yes, 0) AS Yes
	FROM
		(
		SELECT
			'CommunityEngagement' AS Component,
			COUNT(C.CommunityComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.CommunityComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		UNION

		SELECT
			'JusticeEngagement' AS Component,
			COUNT(C.JusticeComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.JusticeComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		UNION

		SELECT
			'PoliceEngagement' AS Component,
			COUNT(C.PoliceComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.PoliceComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		) AS D
	PIVOT
		(
		MAX(D.ItemCount)
		FOR D.ItemName IN
			(
			No,Pending,Yes
			)
	) AS PVT
	ORDER BY PVT.Component

	--Funds Data
	--ActivityCount
	UPDATE T1
	SET T1.ActivityCount = ISNULL(E.ActivityCount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				COUNT(D.ConceptNoteID) AS ActivityCount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ConceptNoteID,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNote CN
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) NOT IN ('Amended','Cancelled') 
						AND WP.IsForDashboard = 1
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--PendingCommitments
	UPDATE T1
	SET T1.PendingCommitments = T1.PendingCommitments + E.ConceptNoteBudgetTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteBudgetTotal) AS ConceptNoteBudgetTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS ConceptNoteBudgetTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteBudget CNB
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNB.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Development')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.PendingCommitments = T1.PendingCommitments + E.ConceptNoteEquimentTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteEquimentTotal) AS ConceptNoteEquimentTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue AS ConceptNoteEquimentTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Development')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanAllocation
	UPDATE T1
	SET T1.WorkplanAllocation = E.WorkplanAllocation
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.CurrentUSDAllocation) AS WorkplanAllocation,
				ConceptNoteTypeName
			FROM
				(
				SELECT 
					WPA.CurrentUSDAllocation,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM workplan.WorkplanActivity WPA
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanCommitted
	UPDATE T1
	SET T1.WorkplanCommitted = T1.WorkplanCommitted + E.ConceptNoteBudgetTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteBudgetTotal) AS ConceptNoteBudgetTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS ConceptNoteBudgetTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteBudget CNB
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNB.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.WorkplanCommitted = T1.WorkplanCommitted + E.ConceptNoteEquimentTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteEquimentTotal) AS ConceptNoteEquimentTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue AS ConceptNoteEquimentTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanDisbursed
	UPDATE T1
	SET T1.WorkplanDisbursed = T1.WorkplanDisbursed + ISNULL(E.ConceptNoteFinanceTotal, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteFinanceTotal) AS ConceptNoteFinanceTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNF.drAmt - CNF.CrAmt AS ConceptNoteFinanceTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteFinance CNF
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.WorkplanDisbursed = T1.WorkplanDisbursed + ISNULL(E.ActualTotalAmount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ActualTotalAmount) AS ActualTotalAmount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ActualTotalAmount,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNote CN
						JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
							AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--CommittedBalance
	UPDATE T1
	SET T1.CommittedBalance = T1.CommittedBalance + T1.WorkplanCommitted
	FROM @tTable1 T1

	UPDATE T1
	SET T1.CommittedBalance = T1.CommittedBalance - ISNULL(E.ConceptNoteFinanceTotal, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteFinanceTotal) AS ConceptNoteFinanceTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNF.drAmt - CNF.CrAmt AS ConceptNoteFinanceTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteFinance CNF
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.CommittedBalance = T1.CommittedBalance - ISNULL(E.ActualTotalAmount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ActualTotalAmount) AS ActualTotalAmount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ActualTotalAmount,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNote CN
						JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
							AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanUncommitted
	UPDATE T1
	SET T1.WorkplanUncommitted = T1.WorkplanUncommitted + T1.WorkplanAllocation
	FROM @tTable1 T1

	UPDATE T1
	SET T1.WorkplanUncommitted = T1.WorkplanUncommitted - T1.WorkplanCommitted
	FROM @tTable1 T1

	--NegativeDataProtection
	UPDATE T1
	SET
		T1.CommittedBalance = CASE WHEN T1.CommittedBalance < 0 THEN 0 ELSE T1.CommittedBalance END,
		T1.WorkplanAllocation = CASE WHEN T1.WorkplanAllocation < 0 THEN 0 ELSE T1.WorkplanAllocation END,
		T1.WorkplanCommitted = CASE WHEN T1.WorkplanCommitted < 0 THEN 0 ELSE T1.WorkplanCommitted END,
		T1.WorkplanDisbursed = CASE WHEN T1.WorkplanDisbursed < 0 THEN 0 ELSE T1.WorkplanDisbursed END,
		T1.WorkplanUncommitted = CASE WHEN T1.WorkplanUncommitted < 0 THEN 0 ELSE T1.WorkplanUncommitted END
	FROM @tTable1 T1

	--ActivityCountPercent
	IF (SELECT SUM(T2.ActivityCount) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.ActivityCountPercent = T1.ActivityCount / (SELECT CAST(SUM(T2.ActivityCount) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--ActivityCountTotal
	UPDATE T1
	SET T1.ActivityCount = (SELECT SUM(T2.ActivityCount) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--CommittedBalancePercent
	IF (SELECT SUM(T2.CommittedBalance) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.CommittedBalancePercent = T1.CommittedBalance / (SELECT CAST(SUM(T2.CommittedBalance) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--CommittedBalanceTotal
	UPDATE T1
	SET T1.CommittedBalance = (SELECT SUM(T2.CommittedBalance) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--PendingCommitmentsPercent
	IF (SELECT SUM(T2.PendingCommitments) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.PendingCommitmentsPercent = T1.PendingCommitments / (SELECT CAST(SUM(T2.PendingCommitments) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--PendingCommitmentsTotal
	UPDATE T1
	SET T1.PendingCommitments = (SELECT SUM(T2.PendingCommitments) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanAllocationPercent
	IF (SELECT SUM(T2.WorkplanAllocation) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanAllocationPercent = T1.WorkplanAllocation / (SELECT CAST(SUM(T2.WorkplanAllocation) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF
					
	--WorkplanAllocationTotal
	UPDATE T1
	SET T1.WorkplanAllocation = (SELECT SUM(T2.WorkplanAllocation) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanCommittedPercent
	IF (SELECT SUM(T2.WorkplanCommitted) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanCommittedPercent = T1.WorkplanCommitted / (SELECT CAST(SUM(T2.WorkplanCommitted) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--WorkplanCommittedTotal
	UPDATE T1
	SET T1.WorkplanCommitted = (SELECT SUM(T2.WorkplanCommitted) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanDisbursedPercent
	IF (SELECT SUM(T2.WorkplanDisbursed) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanDisbursedPercent = T1.WorkplanDisbursed / (SELECT CAST(SUM(T2.WorkplanDisbursed) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--WorkplanDisbursedTotal
	UPDATE T1
	SET T1.WorkplanDisbursed = (SELECT SUM(T2.WorkplanDisbursed) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanUncommittedPercent
	IF (SELECT SUM(T2.WorkplanUncommitted) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanUncommittedPercent = T1.WorkplanUncommitted / (SELECT CAST(SUM(T2.WorkplanUncommitted) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--WorkplanUncommittedTotal
	UPDATE T1
	SET T1.WorkplanUncommitted = (SELECT SUM(T2.WorkplanUncommitted) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	SELECT 
		T1.ConceptNoteTypeName,
		T1.ActivityCount,
		T1.ActivityCountPercent,
		T1.CommittedBalance,
		T1.CommittedBalancePercent,
		T1.PendingCommitments,
		T1.PendingCommitmentsPercent,
		T1.WorkplanAllocation,
		T1.WorkplanAllocationPercent,
		T1.WorkplanCommitted,
		T1.WorkplanCommittedPercent,
		T1.WorkplanDisbursed,
		T1.WorkplanDisbursedPercent,
		T1.WorkplanUncommitted,
		T1.WorkplanUncommittedPercent
	FROM @tTable1 T1

	--Overall Engagement Counts
	INSERT INTO @tTable2 (ItemName) SELECT CS.ComponentStatusName FROM dropdown.ComponentStatus CS WHERE CS.ComponentStatusID > 0 AND CS.IsActive = 1

	;
	WITH OEC AS
		(
		SELECT
			COUNT(D.ComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM
			(
			SELECT 
				C.CommunityID,
				(
				SELECT MIN(T.ComponentStatusID)
				FROM 
					(
					VALUES (CommunityComponentStatusID),(JusticeComponentStatusID),(PoliceComponentStatusID)
					) AS T(ComponentStatusID)
				) AS ComponentStatusID
			FROM dbo.Community C
			WHERE C.IsActive = 1
			) D
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = D.ComponentStatusID
				AND CS.ComponentStatusID > 0
		GROUP BY D.ComponentStatusID, CS.ComponentStatusName
		)

	UPDATE T2
	SET T2.ItemCount = OEC.ItemCount
	FROM @tTable2 T2
		JOIN OEC ON OEC.ItemName = T2.ItemName

	SELECT 
		T2.ItemName,
		T2.ItemCount
	FROM @tTable2 T2
	ORDER BY T2.ItemName

	--Permitted Engagement Counts
	DELETE FROM @tTable2
	INSERT INTO @tTable2 (ItemName) SELECT ID.ImpactDecisionName FROM dropdown.ImpactDecision ID WHERE ID.ImpactDecisionID > 0 AND ID.IsActive = 1

	;
	WITH PEC AS
		(
		SELECT
			COUNT(ID.ImpactDecisionID) AS ItemCount,
			ID.ImpactDecisionName AS ItemName
		FROM dbo.Community C
			JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
				AND C.IsActive = 1
				AND ID.IsActive = 1
		GROUP BY ID.ImpactDecisionID, ID.ImpactDecisionName
		)

	UPDATE T2
	SET T2.ItemCount = PEC.ItemCount
	FROM @tTable2 T2
		JOIN PEC ON PEC.ItemName = T2.ItemName

	SELECT 
		T2.ItemName,
		T2.ItemCount
	FROM @tTable2 T2
	ORDER BY T2.ItemName

END
GO
--End procedure dbo.GetDashboardItemCounts

--Begin procedure dbo.GetDocumentByDocumentID
EXEC Utility.DropObject 'dbo.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	A stored procedure to get data from the dbo.Document table
--
-- Author:			Todd Pires
-- Create date:	2016.01.25
-- Description:	Implemented the IsForDonorPortal field
--
-- Author:			Brandon Green
-- Create date:	2016.08.16
-- Description:	Implemented the DisplayInCommunityNewsFeed field
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentByDocumentID

@DocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentDate, 
		dbo.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle, 
		D.DocumentTypeID, 
		D.IsForDonorPortal,
		D.PhysicalFileExtension, 
		D.PhysicalFileName, 
		D.PhysicalFilePath, 
		D.PhysicalFileSize,
		D.DisplayInCommunityNewsFeed
  FROM dbo.Document D
	WHERE D.DocumentID = @DocumentID

	SELECT 
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.DocumentEntity DE ON DE.EntityID = P.ProvinceID
			AND DE.EntityTypeCode = 'Province'
			AND DE.DocumentID = @DocumentID

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.Community C
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dbo.DocumentEntity DE ON DE.EntityID = C.CommunityID
			AND DE.EntityTypeCode = 'Community'
			AND DE.DocumentID = @DocumentID
	
END
GO
--End procedure dbo.GetDocumentByDocumentID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.27
-- Description:	Bug fixes
--
-- Author:			Todd Pires
-- Create Date: 2015.09.27
-- Description:	Added Recommendations
--
-- Author:			Todd Pires
-- Create Date: 2016.02.11
-- Description:	Added Program Report
--
-- Author:			Todd Pires
-- Create Date: 2016.03.22
-- Description:	Implemented support for the new workflow system
--
-- Author:			Brandon Green
-- Create Date: 2016.08.15
-- Description:	Implemented support incident reports, atmospheric reports and ARAP docs
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
-- ====================================================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN 'fa fa-fw fa-lightbulb-o'
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN 'fa fa-fw fa-question-circle'
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN 'fa fa-fw fa-calendar'
			WHEN ET.EntityTypeCode = 'Incident'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'Document'
			THEN 'fa fa-fw fa-file'
		END AS Icon,

		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN OAR.RecommendationName
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			WHEN D.EntityTypeCode = 'Incident'
			THEN OAI.IncidentName
			WHEN D.EntityTypeCode = 'Document'
			THEN OAD.DocumentTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		CASE
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN OAWR.PhysicalFileName
			WHEN ET.EntityTypeCode = 'Document'
			THEN OAD.PhysicalFileName
			ELSE NULL
		END AS PhysicalFileName,

		D.EntityID,
		D.UpdateDate,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('Recommendation','RequestForInformation','SpotReport','WeeklyReport','Incident','Document')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'Recommendation' AND permissionable.HasPermission('Recommendation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'Document')
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				R.RecommendationName,
				R.IsActive
			FROM recommendation.Recommendation R
			WHERE R.RecommendationID = D.EntityID
					AND D.EntityTypeCode = 'Recommendation'
			) OAR
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle,
				workflow.GetWorkflowStepNumber(D.EntityTypeCode, D.EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(D.EntityTypeCode, D.EntityID) AS WorkflowStepCount
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		OUTER APPLY
			(
			SELECT
				INC.IncidentName
			FROM dbo.Incident INC
			WHERE INC.IncidentID = D.EntityID
				AND D.EntityTypeCode = 'Incident'
			) OAI
		OUTER APPLY
			(
			SELECT
				CASE
					WHEN permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
					THEN DOC.DocumentTitle
					ELSE NULL
				END AS DocumentTitle,

				CASE
					WHEN permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
					THEN DOC.PhysicalFileName
					ELSE NULL
				END AS PhysicalFileName
			FROM dbo.Document DOC
			JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = DOC.DocumentTypeID
				AND DOC.DocumentTypeID > 0 
				AND DT.IsActive = 1
			WHERE DOC.DocumentID = D.EntityID
				AND DOC.DisplayInCommunityNewsFeed = 1 
				AND D.EntityTypeCode = 'Document'
			) OAD
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
			AND
				(
				D.EntityTypeCode <> 'Recommendation'
					OR OAR.IsActive = 1
				)
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
			AND
				(
				D.EntityTypeCode <> 'SpotReport'
					OR OASR.WorkflowStepNumber > OASR.WorkflowStepCount
				)
			AND
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)
			AND
				(
				D.EntityTypeCode <> 'Incident'
					OR OAI.IncidentName IS NOT NULL
				)
			AND
				(
				D.EntityTypeCode <> 'Document'
					OR OAD.DocumentTitle IS NOT NULL
				)

	UNION
	
	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		'fa-newspaper-o' AS Icon,
		ET.EntityTypeName,
		D.DocumentTitle AS Title,
		D.PhysicalFileName,
		0 AS EntityID,
		D.DocumentDate AS UpdateDate,
		dbo.FormatDate(D.DocumentDate) AS UpdateDateFormatted
	FROM dbo.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DT.DocumentTypeCode = 'ProgramReport'
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = DT.DocumentTypeCode
			AND D.DocumentDate >= DATEADD(d, -14, getDate())
			AND permissionable.HasPermission('ProgramReport.View', @PersonID) = 1

	UNION

	SELECT
		'EngagementPermitted' AS EntityTypeCode,
		LOWER('community') AS Controller,
		'fa fa-fw fa-bolt' AS Icon,
		'Communit Engagement Permitted Change' AS EntityTypeName,			
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title,
		NULL AS PhysicalFileName,
		CIDH.CommunityID AS EntityID,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID

	ORDER BY 8 DESC, 1, 7
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dbo.GetEntityDocuments
EXEC Utility.DropObject 'dbo.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.30
-- Description:	A stored procedure to get records from the dbo.DocumentEntity table
-- ================================================================================
CREATE PROCEDURE dbo.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	SELECT
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.PhysicalFileName,
		D.PhysicalFileSize,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), '0') AS ThumbnailLength,

		CASE
			WHEN ISNULL(DATALENGTH(D.Thumbnail), '0') > 0
			THEN ISNULL(DATALENGTH(D.Thumbnail), '0')
			ELSE D.PhysicalFileSize
		END AS FileSize,

		CASE
			WHEN D.DocumentDescription IS NOT NULL AND LEN(RTRIM(D.DocumentDescription)) > 0
			THEN D.DocumentDescription + ' (' + D.DocumentName + ')'
			ELSE D.DocumentName
		END AS DisplayName

	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
	ORDER BY D.DocumentDescription, D.DocumentName, D.DocumentID

END
GO
--End procedure dbo.GetEntityDocuments

--Begin procedure dbo.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.19
-- Description:	A stored procedure to data from the dbo.Incident table
-- ===================================================================
CREATE PROCEDURE dbo.GetIncidentByIncidentID

@IncidentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentDate,
		I.IncidentName,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		SR.SourceReliabilityID,
		SR.SourceReliabilityName,
		IV.InformationValidityID,
		IV.InformationValidityName,
		I.Summary,
		I.KeyPoints,
		I.Implications,
		I.RiskMitigation,
		I.Latitude,
		I.Longitude,
		I.Location.STAsText() AS Location
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.SourceReliability SR ON SR.SourceReliabilityID = I.SourceReliabilityID
		JOIN dropdown.InformationValidity IV ON IV.InformationValidityID = I.InformationValidityID
			AND I.IncidentID = @IncidentID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.IncidentCommunity IC ON IC.CommunityID = C.CommunityID
			AND IC.IncidentID = @IncidentID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.IncidentProvince IP ON IP.ProvinceID = P.ProvinceID
			AND IP.IncidentID = @IncidentID
		
END
GO
--End procedure dbo.GetIncidentByIncidentID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not permissionable.PersonPermissionable
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for IsSuperAdministrator menu items
-- =====================================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPadLength INT
	DECLARE @tPersonPermissionable TABLE (PermissionableLineage VARCHAR(MAX))	

	SELECT @nPadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	INSERT INTO @tPersonPermissionable
		(PermissionableLineage)
	SELECT
		PP.PermissionableLineage
	FROM permissionable.PersonPermissionable PP
		JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PP.PermissionableLineage
		JOIN dbo.Person P2 ON P2.PersonID = PP.PersonID
			AND P2.PersonID = @PersonID
			AND 
				(
				P1.IsSuperAdministrator = 0
					OR P2.IsSuperAdministrator = 1
				)
			
	UNION

	SELECT 
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM @tPersonPermissionable TPP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND MIPL.PermissionableLineage = TPP.PermissionableLineage
						)
					)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM @tPersonPermissionable TPP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND MIPL.PermissionableLineage = TPP.PermissionableLineage
							)
						)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetProvinceByProvinceID
EXEC Utility.DropObject 'dbo.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.14
-- Description:	A stored procedure to data from the dbo.Province table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
--
-- Author:			Jonathan Burnham
-- Create date:	2016.09.27
-- Description:	Refactored
-- ===================================================================
CREATE PROCEDURE dbo.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nCEProjectsCount INT
	DECLARE @nCESpendValue NUMERIC(18,2)
	DECLARE @nJusticeAssetsCount INT
	DECLARE @nJusticeSpendValue NUMERIC(18,2)
	DECLARE @nJusticeStipendaryCount INT
	DECLARE @nPoliceAssetsCount INT
	DECLARE @nPoliceSpendValue NUMERIC(18,2)
	DECLARE @nPoliceStipendaryCount INT

	DECLARE @tTable TABLE (ComponentCode VARCHAR(50), ConceptNoteID INT, BudgetTotal NUMERIC(18,2))

	SELECT 
		ID.ImpactDecisionName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Province', @ProvinceID) AS UpdateDateFormatted,
		P.ArabicProvinceName,
		P.ImpactDecisionID,
		P.Implications,
		P.KeyPoints,
		P.ProgramNotes1,
		P.ProgramNotes2,
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID,
		P.Summary,
		SC.StatusChangeName
	FROM dbo.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

	SELECT
		CES.CommunityEngagementStatusName,
		ISNULL(D.CommunityCount, 0) AS CommunityCount
	FROM dropdown.CommunityEngagementStatus CES
	OUTER APPLY
		(
		SELECT
			COUNT(C.CommunityID) AS CommunityCount,
			C.CommunityEngagementStatusID
		FROM dbo.Community C
		WHERE C.CommunityEngagementStatusID = CES.CommunityEngagementStatusID
			AND C.ProvinceID = @ProvinceID
		GROUP BY C.CommunityEngagementStatusID
		) D 
	WHERE CES.CommunityEngagementStatusID > 0
	ORDER BY CES.DisplayOrder, CES.CommunityEngagementStatusName, D.CommunityCount
	
	SELECT @nJusticeStipendaryCount = COUNT(C1.ContactID)
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C2.ProvinceID = @ProvinceID
			AND EXISTS 
				(
				SELECT 1 
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C1.ContactID
						AND CT.contacttypecode = 'JusticeStipend' 
				) 
			AND C1.IsActive = 1
	
	SELECT @nPoliceStipendaryCount = COUNT(C1.ContactID)
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C2.ProvinceID = @ProvinceID
			AND EXISTS 
				(
				SELECT 1 
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C1.ContactID
						AND CT.contacttypecode = 'PoliceStipend' 
				) 
			AND C1.IsActive = 1
	
	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'AccesstoJustice',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.Community C 
					JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
						AND C.CommunityID = CNC.CommunityID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM dbo.Province P
					JOIN dbo.ConceptNoteProvince CNP ON CNP.ConceptNoteID = CN.ConceptNoteID
						AND CNP.ProvinceID = @ProvinceID
				)
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'AccesstoJustice'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'PoliceDeveoopment',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.Community C 
					JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
						AND C.CommunityID = CNC.CommunityID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM dbo.Province P
					JOIN dbo.ConceptNoteProvince CNP ON CNP.ConceptNoteID = CN.ConceptNoteID
						AND CNP.ProvinceID = @ProvinceID
				)
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'PoliceDevelopment'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'CommunityEngagement',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.Community C 
					JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
						AND C.CommunityID = CNC.CommunityID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM dbo.Province P
					JOIN dbo.ConceptNoteProvince CNP ON CNP.ConceptNoteID = CN.ConceptNoteID
						AND CNP.ProvinceID = @ProvinceID
				)
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'CommunityEngagement'

	SELECT @nCEProjectsCount = COUNT(P.ProjectID)
	FROM project.Project P
		JOIN dropdown.Component CT ON P.ComponentID = CT.ComponentID
			AND CT.ComponentAbbreviation = 'CE'
			AND EXISTS
				(
				SELECT 1
				FROM project.ProjectCommunity PC
					JOIN dbo.Community C ON C.CommunityID = PC.CommunityID
						AND PC.ProjectID = P.ProjectID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM project.ProjectProvince PP
				WHERE PP.ProjectID = P.ProjectID
					AND PP.ProvinceID = @ProvinceID
				)	

	SELECT @nJusticeAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
			AND C.ProvinceID = @ProvinceID
			AND AT.AssetTypeCategory = 'Justice'
	
	SELECT @nPoliceAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
			AND C.ProvinceID = @ProvinceID
			AND AT.AssetTypeCategory = 'Police'

	SELECT @nCESpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'CommunityEngagement'

	SELECT @nJusticeSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'AccesstoJustice'

	SELECT @nPoliceSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'PoliceDevelopment'

	SELECT 
		@nCEProjectsCount AS CEProjectsCount,
		@nJusticeAssetsCount AS JusticeAssetsCount,
		@nJusticeStipendaryCount AS JusticeStipendaryCount,
		@nPoliceAssetsCount AS PoliceAssetsCount,
		@nPoliceStipendaryCount AS PoliceStipendaryCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'AccesstoJustice') AS ActiveJusticeActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'CommunityEngagement') AS ActiveCEActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'PoliceDevelopment') AS ActivePoliceActivitySheetCount,
		FORMAT(@nCESpendValue, 'C', 'en-us') AS CESpendValueFormatted,
		FORMAT(@nJusticeSpendValue, 'C', 'en-us') AS JusticeSpendValueFormatted,
		FORMAT(@nPoliceSpendValue, 'C', 'en-us') AS PoliceSpendValueFormatted

END
GO
--End procedure dbo.GetProvinceByProvinceID

--Begin procedure dbo.GetProvinceFeed
EXEC Utility.DropObject 'dbo.GetProvinceFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.25
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetProvinceFeed

@PersonID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

				UNION

				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.ProvinceID = @ProvinceID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

				UNION

				SELECT 1
				FROM dbo.SpotReportProvince SRP
				WHERE SRP.ProvinceID = @ProvinceID
				)

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
			AND ELD.EntityTypeCode = 'WeeklyReport'
			AND D.PhysicalFileName IS NOT NULL

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityImpactDecisionHistoryID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Engagement Permitted' AS EntityTypeName,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('EngagementPermitted') AS Controller,
		NULL AS PhysicalFileName,
		C.CommunityName + 'Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
	WHERE P.ProvinceID = @ProvinceID


	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetProvinceFeed

--Begin procedure dbo.GetServerSetupDataByServerSetupID
EXEC Utility.DropObject 'dbo.GetServerSetupDataByServerSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.12
-- Description:	A stored procedure to get data from the dbo.ServerSetup table
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ==========================================================================
CREATE PROCEDURE dbo.GetServerSetupDataByServerSetupID

@ServerSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.ServerSetupID, 
		SS.ServerSetupKey, 
		SS.ServerSetupValue 
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupID = @ServerSetupID

END
GO
--End procedure dbo.GetServerSetupDataByServerSetupID

--Begin procedure dbo.GetServerSetupValuesByServerSetupKey
EXEC Utility.DropObject 'dbo.GetServerSetupValuesByServerSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.ServerSetup table
-- ==========================================================================
CREATE PROCEDURE dbo.GetServerSetupValuesByServerSetupKey

@ServerSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.ServerSetupValue 
	FROM dbo.ServerSetup SS 
	WHERE SS.ServerSetupKey = @ServerSetupKey
	ORDER BY SS.ServerSetupID

END
GO
--End procedure dbo.GetServerSetupValuesByServerSetupKey

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:			Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
--
-- Author:			Todd Pires
-- Update date:	2016.08.30
-- Description:	Removed the SpotReport documents recordset
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('SpotReport', @SpotReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('SpotReport', @SpotReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @SpotReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END 
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dbo.ReconcileContactStipendPayment
EXEC Utility.DropObject 'dbo.ReconcileContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ReconcileContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET 
		CSP.StipendPaidDate = getDate()
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = @StipendTypeCode
		AND CSP.StipendPaidDate IS NULL

	UPDATE C
	SET C.StipendArrears = C.StipendArrears + CSP.StipendAmountAuthorized - CSP.StipendAmountPaid
	FROM dbo.Contact C
		JOIN dbo.ContactStipendPayment CSP ON CSP.ContactID = C.ContactID
			AND CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = @StipendTypeCode
			AND CSP.StipendAmountAuthorized > CSP.StipendAmountPaid

	IF @StipendTypeCode = 'JusticeStipend'
		BEGIN

		UPDATE AUE
		SET 
			AUE.ExpensePaidDate = getDate()
		FROM asset.AssetUnitExpense AUE
		WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
			AND AUE.ProvinceID = @ProvinceID
			AND AUE.ExpensePaidDate IS NULL

		END
	--ENDIF

END
GO
--End procedure dbo.ReconcileContactStipendPayment

--Begin procedure dbo.SaveEntityDocuments
EXEC Utility.DropObject 'dbo.SaveDocumentEntityRecords'
EXEC Utility.DropObject 'dbo.SaveEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2016.08.30
-- Description:	A stored procedure to manage records in the dbo.DocumentEntity table
-- =================================================================================
CREATE PROCEDURE dbo.SaveEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	INSERT INTO @tTable
		(DocumentID)
	SELECT D.DocumentID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID

	DELETE DE
	FROM dbo.DocumentEntity DE
	WHERE DE.EntityTypeCode = @EntityTypeCode
		AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO dbo.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		SELECT
			CAST(LTT.ListItem AS INT),
			@EntityTypeCode,
			@EntityTypeSubCode,
			@EntityID
		FROM dbo.ListToTable(@DocumentIDList, ',') LTT

		DELETE T
		FROM @tTable T
			JOIN @tOutput O ON O.DocumentID = T.DocumentID

		END
	--ENDIF

	DELETE D
	FROM dbo.Document D
		JOIN @tTable T ON T.DocumentID = D.DocumentID
			AND NOT EXISTS
				(
				SELECT 1
				FROM dbo.DocumentEntity DE
				WHERE DE.DocumentID = D.DocumentID
				)

END
GO
--End procedure dbo.SaveEntityDocuments

--Begin procedure dbo.SetContactAssetUnit
EXEC Utility.DropObject 'dbo.SetContactAssetUnit'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.16
-- Description:	A stored procedure to update an assetunitid in a contact record
-- ============================================================================
CREATE PROCEDURE dbo.SetContactAssetUnit

@ContactID INT,
@AssetUnitID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE C
	SET C.AssetUnitID = @AssetUnitID
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID
		
END
GO
--End procedure dbo.SetContactAssetUnit

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Added password expiration support
--
-- Author:			Todd Pires
-- Create date:	2015.08.31
-- Description:	Implemented the FormatPersonNameByPersonID function
--
-- Author:			Todd Pires
-- Create date:	2016.05.08
-- Description:	Added the IsSuperAdministrator bit
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cPhone =P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
		JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PP.PermissionableLineage
		JOIN dbo.Person P2 ON P2.PersonID = PP.PersonID
			AND P2.PersonID = @nPersonID
			AND 
				(
				P1.IsSuperAdministrator = 0
					OR P2.IsSuperAdministrator = 1
				)
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin

--End file Build File - 03 - D thru Dq - Procedures.sql

--Begin file Build File - 03 - Dr thru E - Procedures.sql
USE AJACS2
GO


--Begin procedure dropdown.GetAssetStatusData
EXEC Utility.DropObject 'dropdown.GetAssetStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.12
-- Description:	A stored procedure to return data from the dropdown.AssetStatus table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetAssetStatusData
								 
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetStatusID,
		T.AssetStatusCode,
		T.AssetStatusName
	FROM dropdown.AssetStatus T
	WHERE (T.AssetStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetStatusName, T.AssetStatusID

END
GO
--End procedure dropdown.GetAssetStatusData

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.02
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID, 
		T.AssetTypeCategory,
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
	WHERE (T.AssetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeCategory, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetAssetUnitCostData
EXEC Utility.DropObject 'dropdown.GetAssetUnitCostData'
EXEC Utility.DropObject 'dropdown.GetAssetUnitCostRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.13
-- Description:	A stored procedure to return data from the dropdown.AssetUnitCost table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetAssetUnitCostData

@IncludeZero BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetUnitCostID,
		T.AssetUnitCostName
	FROM dropdown.AssetUnitCost T
	WHERE (T.AssetUnitCostID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetUnitCostName, T.AssetUnitCostID

END
GO
--End procedure dropdown.GetAssetUnitCostData

--Begin procedure dropdown.GetComponentStatusData
EXEC Utility.DropObject 'dropdown.GetComponentStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2016.08.23
-- Description:	A stored procedure to return data from the dropdown.ComponentStatus table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetComponentStatusData
								 
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ComponentStatusID,
		T.ComponentStatusName
	FROM dropdown.ComponentStatus T
	WHERE (T.ComponentStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ComponentStatusName, T.ComponentStatusID

END
GO
--End procedure dropdown.GetComponentStatusData

--Begin procedure dropdown.GetConceptNoteTypeData
EXEC Utility.DropObject 'dropdown.GetConceptNoteTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	A stored procedure to return data from the dropdown.ConceptNoteType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetConceptNoteTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConceptNoteTypeCode, 
		T.ConceptNoteTypeID, 
		T.ConceptNoteTypeName
	FROM dropdown.ConceptNoteType T
	WHERE (T.ConceptNoteTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConceptNoteTypeName, T.ConceptNoteTypeID

END
GO
--End procedure dropdown.GetConceptNoteTypeData

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.18
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID, 
		T.ContactTypeCode, 
		T.ContactTypeName,
		T.IsStipend
	FROM dropdown.ContactType T
	WHERE (T.ContactTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dropdown.GetMediaReportSourceTypeData
EXEC utility.DropObject 'dropdown.GetMediaReportSourceTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.07
-- Description:	A stored procedure to return data from the dropdown.MediaReportSourceType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetMediaReportSourceTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportSourceTypeID,
		T.MediaReportSourceTypeName,
		T.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		T.MediaReportSourceTypeName AS SourceTypeName -- Aliased for the common source cfmodule
	FROM dropdown.MediaReportSourceType T
	WHERE (T.MediaReportSourceTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportSourceTypeName, T.MediaReportSourceTypeID

END
GO
--End procedure dropdown.GetMediaReportSourceTypeData

--Begin procedure dropdown.GetMediaReportTypeData
EXEC utility.DropObject 'dropdown.GetMediaReportTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.07
-- Description:	A stored procedure to return data from the dropdown.MediaReportType table
-- ======================================================================================
CREATE PROCEDURE dropdown.GetMediaReportTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.MediaReportTypeID,
		T.MediaReportTypeName
	FROM dropdown.MediaReportType T
	WHERE (T.MediaReportTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.MediaReportTypeName, T.MediaReportTypeID

END
GO
--End procedure dropdown.GetMediaReportTypeData

--Begin procedure dropdown.GetPaymentGroupData
EXEC Utility.DropObject 'dropdown.GetPaymentGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.24
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	Added StipendTypeCode support
--
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	Added the (locked) option to YearMonthFormatted
-- =======================================================================================
CREATE PROCEDURE dropdown.GetPaymentGroupData

@IncludeZero BIT = 0,
@StipendTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(T.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(T.YearMonth, 4) + ' - ' + T.ProvinceName AS YearMonthFormatted,
		T.YearMonth + '-' + CAST(T.ProvinceID AS VARCHAR(10)) AS YearMonthProvince,

		CASE
			WHEN NOT EXISTS (SELECT 1 FROM dbo.ContactStipendPayment CSP WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = CAST(T.YearMonth AS INT) AND CSP.ProvinceID = T.ProvinceID AND CSP.StipendPaidDate IS NULL)
			THEN 1
			ELSE 0
		END AS IsLocked

	FROM
		(
		SELECT DISTINCT
			CAST((CSP.PaymentYear * 100 + CSP.PaymentMonth) AS CHAR(6)) AS YearMonth, 
			P.ProvinceID,
			P.ProvinceName
		FROM dbo.ContactStipendPayment CSP
			JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
				AND CSP.StipendTypeCode = @StipendTypeCode
		) T
	ORDER BY T.YearMonth DESC, 1

END
GO
--End procedure dropdown.GetPaymentGroupData

--Begin procedure dropdown.GetZoneStatusData
EXEC Utility.DropObject 'dropdown.GetZoneStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.12
-- Description:	A stored procedure to return data from the dropdown.ZoneStatus table
-- =================================================================================
CREATE PROCEDURE dropdown.GetZoneStatusData
								 
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ZoneStatusID,
		T.ZoneStatusName
	FROM dropdown.ZoneStatus T
	WHERE (T.ZoneStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ZoneStatusName, T.ZoneStatusID

END
GO
--End procedure dropdown.GetZoneStatusData

--Begin procedure eventlog.LogAssetAction
EXEC utility.DropObject 'eventlog.LogAssetAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Asset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogAssetActionTable
		--ENDIF
		
		SELECT *
		INTO #LogAssetActionTable
		FROM asset.Asset CA
		WHERE CA.AssetID = @EntityID
		
		ALTER TABLE #LogAssetActionTable DROP COLUMN Location

		DECLARE @cAssetUnits NVARCHAR(MAX)

		SELECT 
			@cAssetUnits = COALESCE(@cAssetUnits, '') + D.AssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('AssetUnit'), ELEMENTS) AS AssetUnit
			FROM asset.AssetUnit T 
			WHERE T.AssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Asset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(('<Location>' + CAST(CA.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<AssetUnits>' + ISNULL(@cAssetUnits  , '') + '</AssetUnits>') AS XML)
			FOR XML RAW('Asset'), ELEMENTS
			)
		FROM #LogAssetActionTable T
			JOIN asset.Asset CA ON CA.AssetID = T.AssetID

		DROP TABLE #LogAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAssetAction

--Begin procedure eventlog.LogMediaReportAction
EXEC utility.DropObject 'eventlog.LogMediaReportAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.06.24
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogMediaReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'MediaReport'
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cMediaReportSources VARCHAR(MAX) 
	
		SELECT 
			@cMediaReportSources = COALESCE(@cMediaReportSources, '') + D.MediaReportSource 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('MediaReportSource'), ELEMENTS) AS MediaReportSource
			FROM mediareport.MediaReportSource T 
			WHERE T.MediaReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT
			T.*, 
			CAST(('<MediaReportSources>' + ISNULL(@cMediaReportSources, '') + '</MediaReportSources>') AS XML)
			FOR XML RAW('MediaReport'), ELEMENTS
			)
		FROM mediareport.MediaReport T
		WHERE T.MediaReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogMediaReportAction

--Begin procedure eventlog.LogWorkplanAction
EXEC utility.DropObject 'eventlog.LogWorkplanAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWorkplanAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Workplan',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cWorkplanActivities NVARCHAR(MAX)

		SELECT 
			@cWorkplanActivities = COALESCE(@cWorkplanActivities, '') + D.WorkplanActivity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkplanActivity'), ELEMENTS) AS WorkplanActivity
			FROM workplan.WorkplanActivity T 
			WHERE T.WorkplanID = @EntityID
			) D	

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Workplan',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<WorkplanActivities>' + ISNULL(@cWorkplanActivities, '') + '</WorkplanActivities>') AS XML)
			FOR XML RAW('Workplan'), ELEMENTS
			)
		FROM workplan.Workplan T
		WHERE T.WorkplanID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWorkplanAction

--Begin procedure eventlog.LogZoneAction
EXEC utility.DropObject 'eventlog.LogZoneAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogZoneAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Zone',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogZoneActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogZoneActionTable
		--ENDIF
		
		SELECT *
		INTO #LogZoneActionTable
		FROM zone.Zone Z
		WHERE Z.ZoneID = @EntityID
		
		ALTER TABLE #LogZoneActionTable DROP COLUMN Location

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Zone',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(('<Location>' + CAST(Z.Location AS VARCHAR(MAX)) + '</Location>') AS XML)
			FOR XML RAW('Zone'), ELEMENTS
			)
		FROM #LogZoneActionTable T
			JOIN zone.Zone Z ON Z.ZoneID = T.ZoneID

		DROP TABLE #LogZoneActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogZoneAction

--End file Build File - 03 - Dr thru E - Procedures.sql

--Begin file Build File - 03 - F thru P - Procedures.sql
USE AJACS2
GO

--Begin procedure force.GetForceByEventLogID
EXEC Utility.DropObject 'force.GetForceByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.29
-- Description:	A stored procedure to force data from the eventlog.EventLog table
-- Notes:				Changes here must ALSO be made to force.GetForceByForceID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
-- ==============================================================================
CREATE PROCEDURE force.GetForceByEventLogID

@EventLogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Force.value('CommanderFullName[1]', 'VARCHAR(250)') AS CommanderFullName,
		Force.value('Comments[1]', 'VARCHAR(250)') AS Comments,
		Force.value('DeputyCommanderFullName[1]', 'VARCHAR(250)') AS DeputyCommanderFullName,
		Force.value('ForceDescription[1]', 'VARCHAR(250)') AS ForceDescription,
		Force.value('ForceID[1]', 'INT') AS ForceID,
		Force.value('ForceName[1]', 'VARCHAR(250)') AS ForceName,
		Force.value('History[1]', 'VARCHAR(250)') AS History,
		Force.value('Location[1]', 'VARCHAR(MAX)') AS Location,
		Force.value('Notes[1]', 'VARCHAR(250)') AS Notes,
		Force.value('TerritoryID[1]', 'INT') AS TerritoryID,
		Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)'), Force.value('TerritoryID[1]', 'INT')) AS TerritoryName,
		Force.value('WebLinks[1]', 'VARCHAR(250)') AS WebLinks,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		AOO.AreaOfOperationTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force') AS T(Force)
		JOIN dropdown.AreaOfOperationType AOO ON AOO.AreaOfOperationTypeID = Force.value('AreaOfOperationTypeID[1]', 'INT')
			AND EL.EventLogID = @EventLogID

	SELECT
		Force.value('(CommunityID)[1]', 'INT') AS CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceCommunities/ForceCommunity') AS T(Force)
			JOIN dbo.Community C ON C.CommunityID = Force.value('(CommunityID)[1]', 'INT')
			JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
				AND EL.EventLogID = @EventLogID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceEquipmentResourceProviders/ForceEquipmentResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceFinancialResourceProviders/ForceFinancialResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(RiskID)[1]', 'INT') AS RiskID,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceRisks/ForceRisk') AS T(Force)
			JOIN dbo.Risk R ON R.RiskID = Force.value('(RiskID)[1]', 'INT')
			JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
				AND EL.EventLogID = @EventLogID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		Force.value('(ForceUnitID)[1]', 'INT') AS ForceUnitID,
		Force.value('CommanderFullName[1]', 'VARCHAR(250)') AS CommanderFullName,
		Force.value('DeputyCommanderFullName[1]', 'VARCHAR(250)') AS DeputyCommanderFullName,
		Force.value('(TerritoryID)[1]', 'INT') AS TerritoryID,
		Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)'), Force.value('(TerritoryID)[1]', 'INT')) AS TerritoryName,
		Force.value('(UnitName)[1]', 'VARCHAR(250)') AS UnitName,
		Force.value('(UnitTypeID)[1]', 'INT') AS UnitTypeID,
		UT.UnitTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceUnits/ForceUnit') AS T(Force)
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = Force.value('(UnitTypeID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY Force.value('(UnitName)[1]', 'VARCHAR(250)'), Force.value('(ForceUnitID)[1]', 'INT')
	
END
GO
--End procedure force.GetForceByEventLogID

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- Notes:				Changes here must ALSO be made to force.GetForceByEventLogID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
-- =========================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderContactID, 
		FU.DeputyCommanderContactID,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure force.GetForceUnitByForceUnitID
EXEC Utility.DropObject 'force.GetForceUnitByForceUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.01
-- Description:	A stored procedure to data from the force.ForceUnit table
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
-- ======================================================================
CREATE PROCEDURE force.GetForceUnitByForceUnitID

@ForceUnitID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		FU.CommanderFullName,
		FU.DeputyCommanderFullName,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceUnitID = @ForceUnitID
	
END
GO
--End procedure force.GetForceUnitByForceUnitID

--Begin procedure mediareport.GetMediaReportByMediaReportID
EXEC Utility.DropObject 'mediareport.GetMediaReportByMediaReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.07
-- Description:	A stored procedure to data from the mediareport.MediaReport table
-- ==============================================================================
CREATE PROCEDURE mediareport.GetMediaReportByMediaReportID

@MediaReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MR.MediaReportDate,
		dbo.FormatDate(MR.MediaReportDate) AS MediaReportDateFormatted,
		MR.MediaReportID,
		MR.MediaReportLocation,
		MR.MediaReportTitle,
		MR.Summary,
		MRT.MediaReportTypeID,
		MRT.MediaReportTypeName
	FROM mediareport.MediaReport MR
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
			AND MR.MediaReportID = @MediaReportID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM mediareport.MediaReportCommunity MRC
		JOIN dbo.Community C ON C.CommunityID = MRC.CommunityID
			AND MRC.MediaReportID = @MediaReportID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		LOWER(CAST(newID() AS VARCHAR(50))) AS MediaReportSourceGUID,
		MRS.MediaReportSourceID, 
		MRS.SourceAttribution, 
		MRS.SourceDate, 
		dbo.FormatDate(MRS.SourceDate) AS SourceDateFormatted, 
		MRS.SourceName, 
		MRST.MediaReportSourceTypeID, 
		MRST.MediaReportSourceTypeName,
		MRST.MediaReportSourceTypeID AS SourceTypeID, -- Aliased for the common source cfmodule
		MRST.MediaReportSourceTypeName AS SourceTypeName -- Aliased for the common source cfmodule
	FROM mediareport.MediaReportSource MRS
		JOIN dropdown.MediaReportSourceType MRST ON MRST.MediaReportSourceTypeID = MRS.MediaReportSourceTypeID
			AND MRS.MediaReportID = @MediaReportID
	ORDER BY MRS.SourceName, MRS.MediaReportSourceID

	SELECT
		getDate() AS MediaReportDate,
		dbo.FormatDate(getDate()) AS MediaReportDateFormatted

END
GO
--End procedure mediareport.GetMediaReportByMediaReportID

--Begin procedure permissionable.DeletePermissionableTemplate
EXEC Utility.DropObject 'permissionable.DeletePermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date: 2016.10.16
-- Description:	A stored procedure to delete Permissionable Template data
-- ======================================================================
CREATE PROCEDURE permissionable.DeletePermissionableTemplate

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PT
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID

END
GO
--End procedure permissionable.DeletePermissionableTemplate

--Begin procedure permissionable.GetPersonPermissionables
EXEC Utility.DropObject 'permissionable.GetPersonPermissionables'
GO
--End procedure permissionable.GetPersonPermissionables

--Begin procedure procurement.DeleteEquipmentDistributionByEquipmentDistributionID
EXEC Utility.DropObject 'procurement.DeleteEquipmentDistributionByEquipmentDistributionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.04
-- Description:	A stored procedure to delete Equipment Distribution Plan data
-- ==========================================================================
CREATE PROCEDURE procurement.DeleteEquipmentDistributionByEquipmentDistributionID

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE ED
	FROM procurement.EquipmentDistribution ED
	WHERE ED.EquipmentDistributionID = @EquipmentDistributionID

	DELETE DI
	FROM procurement.DistributedInventory DI
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM procurement.EquipmentDistribution ED
		WHERE ED.EquipmentDistributionID = DI.EquipmentDistributionID
		)

	DELETE D
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'EquipmentDistribution'
			AND DE.DocumentEntityID = @EquipmentDistributionID

	DELETE DE
	FROM dbo.DocumentEntity DE
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.Document D
		WHERE D.DocumentID = DE.DocumentID
		)

END
GO
--End procedure procurement.DeleteEquipmentDistributionByEquipmentDistributionID

--Begin procedure procurement.GetCommunityEquipmentInventory
EXEC Utility.DropObject 'dbo.GetCommunityEquipmentInventory'
EXEC Utility.DropObject 'procurement.GetCommunityEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.CommunityEquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2016.05.19
-- Description:	Refactored to support the redesigned inventory system
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =========================================================================================
CREATE PROCEDURE procurement.GetCommunityEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DeliveredToEndUserDate) AS DeliveredToEndUserDateFormatted,
		dbo.FormatConceptNoteTitle(EI.ConceptNoteID) AS Title	
	FROM
		(
		SELECT
			DI.EquipmentInventoryID,
			SUM(DI.Quantity) AS Quantity,
			MAX(DI.DeliveredToEndUserDate) AS DeliveredToEndUserDate
		FROM
			(
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				A.CommunityID,
				0 AS ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN asset.AssetUnit AU ON AU.AssetUnitID = DI.EndUserEntityID
				JOIN asset.Asset A ON A.AssetID = AU.AssetID
					AND DI.EndUserEntityTypeCode = 'AssetUnit'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				C.CommunityID,
				dbo.GetProvinceIDByCommunityID(C.CommunityID) AS ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Contact'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Community'
					THEN F.TerritoryID
					ELSE 0
				END AS CommunityID,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Province'
					THEN F.TerritoryID
					ELSE 0
				END AS ProvinceID
	
			FROM procurement.DistributedInventory DI
				JOIN force.Force F ON F.ForceID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Force'
	
			) DI
		WHERE DI.CommunityID = @EntityID
		GROUP BY DI.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure procurement.GetCommunityEquipmentInventory

--Begin procedure procurement.GetProvinceEquipmentInventory
EXEC Utility.DropObject 'dbo.GetProvinceEquipmentInventory'
EXEC Utility.DropObject 'procurement.GetProvinceEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.ProvinceEquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2016.05.19
-- Description:	Refactored to support the redesigned inventory system
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =========================================================================================
CREATE PROCEDURE procurement.GetProvinceEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DeliveredToEndUserDate) AS DeliveredToEndUserDateFormatted,
		dbo.FormatConceptNoteTitle(EI.ConceptNoteID) AS Title	
	FROM
		(
		SELECT
			DI.EquipmentInventoryID,
			SUM(DI.Quantity) AS Quantity,
			MAX(DI.DeliveredToEndUserDate) AS DeliveredToEndUserDate
		FROM
			(
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
				C.CommunityID,
				dbo.GetProvinceIDByCommunityID(C.CommunityID) AS ProvinceID
			FROM procurement.DistributedInventory DI
				JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Contact'
	
			UNION
	
			SELECT 
				DI.EquipmentInventoryID,
				DI.DeliveredToEndUserDate,
				DI.Quantity,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Community'
					THEN F.TerritoryID
					ELSE 0
				END AS CommunityID,
	
				CASE
					WHEN F.TerritoryTypeCode = 'Province'
					THEN F.TerritoryID
					ELSE 0
				END AS ProvinceID
	
			FROM procurement.DistributedInventory DI
				JOIN force.Force F ON F.ForceID = DI.EndUserEntityID
					AND DI.EndUserEntityTypeCode = 'Force'
	
			) DI
		WHERE DI.ProvinceID = @EntityID
		GROUP BY DI.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure procurement.GetProvinceEquipmentInventory

--Begin procedure procurement.validateSerialNumber
EXEC Utility.DropObject 'procurement.validateSerialNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2016.09.02
-- Description:	A stored procedure to validate serial numbers
-- ==========================================================
CREATE PROCEDURE procurement.validateSerialNumber

@EquipmentInventoryID INT,
@SerialNumber VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT LTT.ListItem AS SerialNumber
	FROM dbo.ListToTable(@SerialNumber, ',') LTT
	WHERE EXISTS
		(
		SELECT 1
		FROM procurement.EquipmentInventory EI
		WHERE EI.SerialNumber = LTT.ListItem
			AND EI.EquipmentInventoryID <> @EquipmentInventoryID
		)
	ORDER BY LTT.ListItem

END
GO
--End procedure procurement.validateSerialNumber


--End file Build File - 03 - F thru P - Procedures.sql

--Begin file Build File - 03 - R thru Z - Procedures.sql
USE AJACS2
GO

--Begin procedure reporting.AssetUnitCostRateTotal
EXEC utility.DropObject 'reporting.AssetUnitCostRateTotal'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure 
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =================================================================================
CREATE PROCEDURE reporting.AssetUnitCostRateTotal

@CommunityID INT = 0 

AS
BEGIN
	
	DECLARE @nLastPaymentYYYY INT, @nLastPaymentMM INT, @dLastPayment DATE 
	
	--Material Support 01
	SELECT 
		ISNULL(SUM(AUCR.AssetUnitCostRate), 0) AS AssetUnitCostRateTotal
	FROM asset.Asset A
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
		JOIN dropdown.AssetUnitCostRate AUCR ON AUCR.AssetUnitCostRateID = AU.AssetUnitCostRateID
		JOIN dropdown.AssetUnitType AUT ON AUT.AssetUnitTypeID = AU.AssetUnitTypeID
			AND A.CommunityID = @CommunityID

END
GO
--End procedure reporting.AssetUnitCostRateTotal

--Begin procedure reporting.GetCashHandoverReport
EXEC Utility.DropObject 'reporting.GetCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2015.09.28
-- Description:	Added the ArabicProvinceName column
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================
CREATE PROCEDURE reporting.GetCashHandoverReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ArabicProvinceName NVARCHAR(250)
	DECLARE @FullName VARCHAR(200) = (SELECT dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst'))
	DECLARE @PaymentMonthYear VARCHAR(20)
	DECLARE @PaymentMonth INT
	DECLARE @PaymentYear  INT
	DECLARE @ProvinceName VARCHAR(250)
	DECLARE @RunningCost INT 
	DECLARE @TotalCost INT 
	DECLARE @TotalExpenseCost INT
	
	SELECT @RunningCost = SUM(E.RunningCost)
	FROM
		(
		SELECT
			(
			SELECT AE.ExpenseAmountAuthorized
			FROM asset.Asset A
				JOIN asset.AssetUnit AU ON A.AssetID = AU.AssetUnitID
				JOIN asset.AssetUnitExpense AE ON AE.AssetUnitID = AU.AssetUnitID
					AND AE.PaymentMonth = @PaymentMonth  
					AND AE.PaymentYear = @PaymentYear	
			) AS RunningCost
		FROM
			(
			SELECT DISTINCT 
				CSP.CommunityID
			FROM dbo.ContactStipendPayment CSP
				JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
					AND SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
			) D
		) E
	
	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT 
		@PaymentMonth = PaymentMonth, 
		@PaymentYear = PaymentYear  
	FROM dbo.ContactStipendPayment CSP 
		JOIN 
			(
			SELECT TOP 1 EntityID	
			FROM reporting.SearchResult SR 
			WHERE SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
			) D ON D.EntityID = CSP.ContactStipendPaymentID

	SELECT 
		@TotalExpenseCost = SUM(AUE.ExpenseAmountAuthorized) 
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.PaymentMonth = @PaymentMonth 
		AND AUE.PaymentYear = @PaymentYear 

	SELECT TOP 1 
		@ArabicProvinceName = P.ArabicProvinceName,
		@PaymentMonthYear = DateName(month , DateAdd(month, CSP.PaymentMonth, 0) - 1) + ' - ' + CAST(CSP.PaymentYear AS CHAR(4)),
		@ProvinceName = P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT
		@ArabicProvinceName AS ArabicProvinceName,
		@FullName AS FullName,
		@PaymentMonthYear AS PaymentMonthYear,
		@ProvinceName AS ProvinceName,
		ISNULL(@TotalCost, 0) AS TotalCost,
		ISNULL(@TotalExpenseCost, 0) AS TotalExpenseCost,
		ISNULL(@TotalCost, 0) + ISNULL(@TotalExpenseCost, 0) as CompleteCost
END
GO
--End procedure reporting.GetCashHandoverReport

--Begin procedure reporting.GetCommunities
EXEC Utility.DropObject 'reporting.GetCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.15
-- Description:	A stored procedure to get data from the dbo.Community table
-- ========================================================================
CREATE PROCEDURE reporting.GetCommunities

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ArabicCommunityName,
		C.CommunityID,
		C.CommunityName,
		C.Population, 
		C.PopulationSource, 
		CG.CommunityGroupID,
		CG.CommunityGroupName,
		CS1.ComponentStatusID AS CommunityComponentStatusID,
		CS1.ComponentStatusName AS CommunityComponentStatusName,
		CS2.ComponentStatusID AS JusticeComponentStatusID,
		CS2.ComponentStatusName AS JusticeComponentStatusName,
		CS3.ComponentStatusID AS PoliceComponentStatusID,
		CS3.ComponentStatusName AS PoliceComponentStatusName,
		CSG.CommunitySubGroupID,
		CSG.CommunitySubGroupName,
		ID.ImpactDecisionID,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Community C 
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.ComponentStatus CS1 ON CS1.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus CS2 ON CS2.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus CS3 ON CS3.ComponentStatusID = C.PoliceComponentStatusID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID 
		JOIN Reporting.SearchResult SR ON SR.EntityID = C.CommunityID 
			AND SR.EntityTypeCode = 'Community' 
			AND SR.PersonID = @PersonID
	ORDER BY C.CommunityName

END
GO
--End procedure reporting.GetCommunities

--Begin procedure reporting.GetConceptNoteAmendmentByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteAmmendmentByConceptNoteID'
EXEC Utility.DropObject 'reporting.GetConceptNoteAmendmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to return data from the dbo.ConceptNoteAmendment table
-- ======================================================================================
CREATE PROCEDURE reporting.GetConceptNoteAmendmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		CNA.AmendmentNumber,
		CNA.ConceptNoteAmendmentID,
		CNA.Cost,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID
	ORDER BY 1, 2

END
GO
--End procedure reporting.GetConceptNoteAmendmentByConceptNoteID

--Begin procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.31
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.GetContactCommunityByContactID(CON.ContactID) AS Location,
		CON.ContactID,
		CON.EmployerName,
		CN.ConceptNoteID,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ConceptNoteReferenceCode,
		dbo.FormatContactNameByContactID(CON.ContactID, 'LastFirst') AS Fullname
	FROM
		(
		SELECT DISTINCT
			CNCE.ConceptNoteID,
			CNCE.ContactID
		FROM dbo.ConceptNoteContactEquipment CNCE
		WHERE CNCE.ConceptNoteID = @ConceptNoteID
		) D
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = D.ConceptNoteID
		JOIN dbo.Contact CON ON CON.ContactID = D.ContactID
	ORDER BY Fullname, D.ContactID

END
GO
--End procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		(SELECT TOP 1 WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote') AS WorkflowType,
		CNS.ConceptNoteStatusCode,
		CN.WorkflowStepNumber,
		CN.ConceptNoteID,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,
		CN.TaskCode,
		workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) AS Status,
		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1 SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,

		CASE
			WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Cancelled','Development','Closed','Closedown')
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			WHEN CN.TaskCode ='Closed'						
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			ELSE FORMAT((IsNull(OACNB.TotalBudget,0)+ IsNull(OACNA.ConceptNoteAmendmentTotal,0) + isnull(OACNCE.ConceptNoteEquimentTotal, 0)), 'C', 'en-us') 
		END AS TotalBudget,

		CASE
			WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Cancelled','Development')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us') 
		END AS TotalSpent,
		
		CASE 
			WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Development','Closedown')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT(((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmendmentTotal,0)+ isnull(OACNCE.ConceptNoteEquimentTotal, 0)) - (IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0))), 'C', 'en-us') 
		END AS TotalRemaining

	FROM dbo.ConceptNote CN
		JOIN Reporting.SearchResult SR ON SR.EntityID = CN.ConceptNoteID AND SR.EntityTypeCode='ConceptNote' AND SR.PersonID = @PersonID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB
		OUTER APPLY
			(
			SELECT
				SUM(CNF.drAmt) - SUM(CNF.CrAmt) as ConceptNoteFinanceTotal
			FROM dbo.ConceptNoteFinance CNF
			WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
			) OACNF
		OUTER APPLY
			(
			SELECT
				SUM(CNA.Cost) as ConceptNoteAmendmentTotal
			FROM dbo.ConceptNoteAmendment CNA
			WHERE CNA.ConceptNoteID = CN.ConceptNoteID
			) OACNA
		OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue) as ConceptNoteEquimentTotal
			FROM dbo.ConceptNoteEquipmentCatalog CNCE
				JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					AND CNCE.ConceptNoteID = CN.ConceptNoteID
			) OACNCE
	ORDER BY 9 DESC

END
GO
--End procedure reporting.GetConceptNotes

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
--
-- Author:			Justin Branum
-- Create date: 2016.01.21
-- Description: Added New columns regrading Previous Unit data for vetting export report
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =====================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.City,
		C1.AssetUnitID,
		C1.CommunityID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', C1.CommunityID) AS CommunityName,
		C1.ContactID,
		dbo.getContactTypesByContactID(C1.ContactID) AS ContactTypeName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.DescriptionOfDuties,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsRegimeDefector,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.PassportNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.PreviousDuties,
		C1.PreviousProfession,
		C1.PreviousRankOrTitle,
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDate,
		dbo.FormatUSDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateUS,
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDate,
		dbo.FormatUSDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateUS,
		C1.PreviousUnit AS PreviousUnitLocation,
		C1.Profession,
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,
		dbo.FormatUSDate(C1.StartDate) AS JoinDateUSFormatted,
		C1.State,
		C1.Title,
		dbo.FormatDate(C1.UKVettingExpirationDate) AS UKVettingExpirationDateFormatted,
		dbo.FormatUSDate(C1.USVettingExpirationDate) AS USVettingExpirationDateFormatted,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		OAA.AssetName,
		OAA.TerritoryName,
		OAVO1.VettingOutcomeName AS USVettingOutcomeName,
		OAVO2.VettingOutcomeName AS UKVettingOutcomeName,
		P1.ProjectID,
		P1.ProjectName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		OUTER APPLY
			(
			SELECT 
				A.AssetName,
				dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', A.CommunityID) AS TerritoryName
			FROM asset.Asset A 
				JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
					AND AU.AssetUnitID = C1.AssetUnitID
			) OAA
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 1
			ORDER BY CV.ContactVettingID DESC
			) OAVO1
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 2
			ORDER BY CV.ContactVettingID DESC
			) OAVO2
	ORDER BY C1.ContactID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetJusticeCashHandoverReport
EXEC Utility.DropObject 'reporting.GetJusticeCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			John Lyons
-- Create date:	2016.07.12
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ==================================================================

CREATE PROCEDURE reporting.GetJusticeCashHandoverReport
@PersonID INT,	 
@PaymentMonth INT,	 
@PaymentYear INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@cStipendName nvarchar(250), 
		@nAssetUnitID INT, 
		@nContactCount INT, 
		@nStipendAmountAuthorized NUMERIC(18, 2), 
		@RunningCost INT,
		@TotalCost INT = 0

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
			SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
			SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
			SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
			SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID

	SELECT 
		@RunningCost = SUM(AUE.ExpenseAmountAuthorized)
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.AssetUnitExpense AUE ON AUE.AssetUnitID = AU.AssetUnitID 
			AND AUE.PaymentMonth = @PaymentMonth 
			AND AUE.PaymentYear = @PaymentYear

	SELECT
		dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(A.CommunityID)) AS ProvinceName,
		dbo.GetArabicProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(A.CommunityID)) AS ArabicProvinceName,
		dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst') AS FullName,
		DateName(month , DateAdd(month,@PaymentMonth, 0) - 1) + ' - ' + CAST(@PaymentYear AS CHAR(4)) AS PaymentMonthYear,
		ISNULL(ISNULL(@TotalCost, 0), 0) + ISNULL(@RunningCost, 0) as CompleteCost,
		dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(A.CommunityID)) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID 
		JOIN asset.AssetUnitExpense AUE ON AUE.AssetUnitID = AU.AssetUnitID 
			AND SP.PersonID = @PersonID
			AND AUE.PaymentMonth = @PaymentMonth 
			AND AUE.PaymentYear = @PaymentYear

END
GO
--End procedure reporting.GetJusticeCashHandoverReport

--Begin procedure reporting.GetJusticeStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.07.09
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPayments

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UPPER(LEFT(DateName( month , DateAdd( month , D.PaymentMonth , -1 ) ),3)) + ' - ' + CAST(D.PaymentYear as varchar(5)) AS YearMonthFormatted,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		SUM(D.StipendAmountPaid) AS StipendAmountPaid,
		SUM(D.StipendAmountAuthorized) AS StipendAmountAuthorized,
		(
		SELECT SUM(AUE.ExpenseAmountAuthorized)  
		FROM asset.AssetUnitExpense AUE 
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID 
				AND AUE.ProvinceID = D.ProvinceID 
				AND AUE.PaymentYear = D.PaymentYear 
				AND AUE.PaymentMonth = D.PaymentMonth
		) AS StipendExpenseAmount
	FROM
		(
		SELECT
			CSP.StipendAmountPaid,
			CSP.StipendAmountAuthorized,
			CSP.ContactID,
			CSP.ProvinceID,
			CSP.PaymentYear,
			CSP.PaymentMonth,
		
			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			CAST(CSP.PAYMENTYEAR AS VARCHAR(50)) + CAST(CSP.PAYMENTMONTH AS VARCHAR(50)) AS YEARMONTH,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID 
			AND StipendName IN ('Rank 1','Rank 2','Rank 3','Rank 4','Rank 5') 
			AND StipendTypeCode ='JusticeStipend' 
		) D
	GROUP BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.provinceID
	ORDER BY D.PaymentYear,D.PaymentMonth  ,  D.StipendStatus , d.ProvinceID ASC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPayments

--Begin procedure reporting.GetJusticeStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetJusticeStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendPaymentReport

@PersonID INT = 0, 
@Year int = 0, 
@Month int = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT
	DECLARE @nContactCount INT
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
			SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
			SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
			SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
			SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		SP.[Total Count],
		SP.[Total Stipend],
		SP.[Rank 1],
		SP.[Rank 2],
		SP.[Rank 3],
		SP.[Rank 4],
		SP.[Rank 5],
		SP.[Rank 1 Count],
		SP.[Rank 2 Count],
		SP.[Rank 3 Count],
		SP.[Rank 4 Count],
		SP.[Rank 5 Count],
		sp.stipendpaymentid, 
		sp.personid,
		sp.AssetUnitid,
		AUE.ExpenseAmountAuthorized AS [Running Costs],
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID 
		JOIN asset.Asset A ON A.AssetID = A.AssetID 
		JOIN asset.AssetUnitExpense AUE ON AUE.AssetUnitID = AU.AssetUnitID 
			AND SP.PersonID = @PersonID
			AND AUE.PaymentMonth = @Month 
			AND AUE.PaymentYear = @Year

END
GO
--End procedure reporting.GetJusticeStipendPaymentReport

--Begin procedure reporting.GetMediaReports
EXEC utility.DropObject 'reporting.GetMediaReports'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Todd Pires
-- Create date:	2016.06.29
-- Description:	A stored procedure to data for the MediaReport SSRS
-- ================================================================
CREATE PROCEDURE reporting.GetMediaReports

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		MR.MediaReportDate,
		dbo.FormatDateTime(MR.MediaReportDate) AS MediaReportDateFormatted,
		MR.MediaReportID,
		MR.MediareportLocation AS Location,
		reporting.GetMediaReportSourceLinks(MR.MediaReportID) AS MediaReportSourceLinks,
		MR.MediaReportTitle,
		MR.Summary,
		MRT.MediaReportTypeID,
		MRT.MediaReportTypeName, 
        dbo.GetProvinceIDByCommunityID(C.CommunityID) as ProvinceName
	FROM reporting.SearchResult SR
		JOIN mediareport.MediaReport MR ON MR.MediaReportID = SR.EntityID  	AND SR.EntityTypeCode = 'MediaReport' AND SR.PersonID = @personID
		JOIN dropdown.MediaReportType MRT ON MRT.MediaReportTypeID = MR.MediaReportTypeID
        JOIN mediareport.MediaReportCommunity MRC ON MRC.MediaReportID = MR.MediaReportID
        JOIN mediareport.MediaReportSource MRS ON MRS.MediaReportID = MR.MediaReportID
        JOIN Community C on C.CommunityID = MRC.CommunityID

END
GO
--End procedure reporting.GetMediaReports

--Begin procedure reporting.GetOpsFundReport
EXEC Utility.DropObject 'reporting.GetOpsFundReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ======================================================================
CREATE PROCEDURE reporting.GetOpsFundReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
	(
	SELECT
		CSP.ContactStipendPaymentID,
		CSP.StipendAmountAuthorized,
		CSP.CommunityID,

	CASE
		WHEN CSP.CommunityID > 0
		THEN dbo.GetProvinceIDByCommunityID(CSP.CommunityID)
		ELSE CSP.ProvinceID
	END AS ProvinceID

	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID 
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	)

	SELECT 
		C.GovernmentIDNumber,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ProvinceName,
		(SELECT P.ArabicProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ArabicProvinceName,
		(SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS CommunityName,
		(SELECT C.ArabicCommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS ArabicCommunityName,
		CSP.StipendName,
		A.AssetName,
		AU.AssetUnitName,
		CSP.ContactID,
		dbo.FormatPersonNameByPersonID(@PersonID ,'LastFirst') AS AJACSRepresentative,
		dbo.FormatContactNameByContactID(CSP.ContactID, 'LastFirst') AS BeneficaryName,
		C.ArabicFirstName +' ' + C.ArabicMiddlename + ' ' + C.ArabicLastName  AS ArabicBeneficaryName,
		SD.StipendAmountAuthorized,
		dbo.FormatDate(StipendAuthorizedDate) AS StipendAuthorizedDateFormatted,
		DateName(month, DateAdd(month, CSP.PaymentMonth, 0 ) - 1) + ' - ' + CAST(PaymentYear AS VARCHAR(50)) AS PaymentMonthYear
	FROM dbo.ContactStipendPayment CSP
		JOIN SD ON SD.ContactStipendPaymentID = CSP.ContactStipendPaymentID
		JOIN dbo.Contact C ON C.contactid = CSP.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
	ORDER BY ProvinceName, CommunityName, StipendName, BeneficaryName
	
END
GO
--End procedure reporting.GetOpsFundReport

--Begin procedure reporting.GetStipendPaymentReport
EXEC utility.DropObject 'reporting.GetStipendPaymentReport'
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2015.09.30
-- Description:	A stored procedure to add data to the reporting.StipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =================================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT = 17

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = 17
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Command] = SP.[Command] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Command Count] = SP.[Command Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END,
			SP.[General] = SP.[General] + CASE WHEN @cStipendName = 'General' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[General Count] = SP.[General Count] + CASE WHEN @cStipendName = 'General' THEN @nContactCount ELSE 0 END,
			SP.[Colonel] = SP.[Colonel] + CASE WHEN @cStipendName = 'Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Count] = SP.[Colonel Count] + CASE WHEN @cStipendName = 'Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Colonel Doctor] = SP.[Colonel Doctor] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Doctor Count] = SP.[Colonel Doctor Count] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nContactCount ELSE 0 END,
			SP.[Lieutenant Colonel] = SP.[Lieutenant Colonel] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Lieutenant Colonel Count] = SP.[Lieutenant Colonel Count] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Major] = SP.[Major] + CASE WHEN @cStipendName = 'Major' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Major Count] = SP.[Major Count] + CASE WHEN @cStipendName = 'Major' THEN @nContactCount ELSE 0 END,
			SP.[Captain] = SP.[Captain] + CASE WHEN @cStipendName = 'Captain' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Count] = SP.[Captain Count] + CASE WHEN @cStipendName = 'Captain' THEN @nContactCount ELSE 0 END,
			SP.[Captain Doctor] = SP.[Captain Doctor] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Doctor Count] = SP.[Captain Doctor Count] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nContactCount ELSE 0 END,
			SP.[First Lieutenant] = SP.[First Lieutenant] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Lieutenant Count] = SP.[First Lieutenant Count] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Officer] = SP.[Contracted Officer] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Officer Count] = SP.[Contracted Officer Count] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nContactCount ELSE 0 END,
			SP.[First Sergeant] = SP.[First Sergeant] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Sergeant Count] = SP.[First Sergeant Count] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[Sergeant] = SP.[Sergeant] + CASE WHEN @cStipendName = 'Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Sergeant Count] = SP.[Sergeant Count] + CASE WHEN @cStipendName = 'Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[First Adjutant] = SP.[First Adjutant] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Adjutant Count] = SP.[First Adjutant Count] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Adjutant] = SP.[Adjutant] + CASE WHEN @cStipendName = 'Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Adjutant Count] = SP.[Adjutant Count] + CASE WHEN @cStipendName = 'Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Policeman] = SP.[Policeman] + CASE WHEN @cStipendName = 'Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Policeman Count] = SP.[Policeman Count] + CASE WHEN @cStipendName = 'Policeman' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Policeman] = SP.[Contracted Policeman] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Policeman Count] = SP.[Contracted Policeman Count] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		SP.*,
		AUCR.AssetUnitCostRate AS [Running Costs],
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dropdown.AssetUnitCostRate AUCR ON AUCR.AssetUnitCostRateID = AU.AssetUnitCostRateID

END
GO
--End procedure reporting.GetStipendPaymentReport

--Begin procedure reporting.GetStipendPaymentsByProvince
EXEC Utility.DropObject 'reporting.GetStipendPaymentsByProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data for stipend payments
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ===================================================================
CREATE PROCEDURE reporting.GetStipendPaymentsByProvince

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.ContactID AS SysID, 
		C1.CellPhoneNumber,
		C1.ContactID,
		dbo.GetContactCommunityByContactID(C1.ContactID) AS ContactLocation,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		
		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Community C2 JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID AND C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ProvinceName,
		
		CASE
			WHEN C1.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C1.CommunityID) = 1
			THEN S.StipendAmount
			WHEN C1.ProvinceID > 0 AND dbo.IsProvinceStipendEligible(C1.ProvinceID) = 1
			THEN S.StipendAmount
			ELSE 0
		END AS StipendAmount,
			
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center,
		C1.ArabicMotherName,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		S.StipendName 
	FROM reporting.SearchResult SR
		JOIN dbo.Contact C1 ON C1.ContactID = SR.EntityID 
			AND SR.PersonID = @PersonID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dropdown.AssetUnitCostRate AUCR ON AUCR.AssetUnitCostRateID = AU.AssetUnitCostRateID
	ORDER BY ContactLocation, Center, C1.LastName, C1.FirstName, C1.MiddleName, C1.ContactID

END
GO
--End procedure reporting.GetStipendPaymentsByProvince

--Begin procedure weeklyreport.ApproveWeeklyReport
EXEC Utility.DropObject 'weeklyreport.ApproveWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
--
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	Renamed from SubmitWeeklyReport to ApproveWeeklyReport
--
-- Author:			Todd Pires
-- Create date:	2015.05.16
-- Description:	Added the post-approval initialization call
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ======================================================================
CREATE PROCEDURE weeklyreport.ApproveWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @nWeeklyReportID INT
	DECLARE @tOutput1 TABLE (EntityTypeCode VARCHAR(50), EntityID INT)
	DECLARE @tOutput2 TABLE (WeeklyReportID INT)

	SELECT @nWeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput1
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @nWeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput1
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @nWeeklyReportID

	INSERT INTO @tOutput1 (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @nWeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O1.EntityTypeCode, O1.EntityID
		FROM @tOutput1 O1
		ORDER BY O1.EntityTypeCode, O1.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	TRUNCATE TABLE weeklyreport.Community
	TRUNCATE TABLE weeklyreport.Province
	TRUNCATE TABLE weeklyreport.SummaryMapCommunity
	TRUNCATE TABLE weeklyreport.SummaryMapAsset
	TRUNCATE TABLE weeklyreport.SummaryMapIncident
	DELETE FROM weeklyreport.WeeklyReport

	INSERT INTO weeklyreport.WeeklyReport 
		(WorkflowStepNumber) 
	OUTPUT INSERTED.WeeklyReportID INTO @tOutput2
	VALUES 
		(1)

	SELECT @nWeeklyReportID = O2.WeeklyReportID FROM @tOutput2 O2

	EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

END
GO
--End procedure weeklyreport.ApproveWeeklyReport

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O

		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('WeeklyReport', @nWeeklyReportID)

	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.SummaryMapZoom 
	FROM weeklyreport.WeeklyReport WR

	SELECT
		C.CommunityID,
		C.CommunityName,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID,
		ID.ImpactDecisionName,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		C2.Location.STAsText() AS Location,

		CASE 
			WHEN EXISTS (SELECT 1 FROM weeklyreport.SummaryMapCommunity SMC WHERE SMC.CommunityID = C.CommunityID AND SMC.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM weeklyreport.Community C
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Community C2 ON C2.CommunityID = C.CommunityID
			 AND C.WeeklyReportID = @nWeeklyReportID

	SELECT
		A.AssetID,
		A.AssetName,
		A.Location.STAsText() AS Location,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,

		CASE 
			WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapAsset SMA WHERE SMA.AssetID = A.AssetID AND SMA.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND EXISTS 
				(
				SELECT 1 
				FROM weeklyreport.Community C 
				WHERE C.CommunityID = A.CommunityID 
					AND C.WeeklyReportID = @nWeeklyReportID
				)

	SELECT
		A.AssetID,
		A.AssetName,
		A.Location.STAsText() AS Location,
		AT.AssetTypeID,
		AT.AssetTypeName,

		CASE 
			WHEN EXISTS (SELECT 1 FROM weeklyreport.SummaryMapAsset SMA WHERE SMA.AssetID = A.AssetID AND SMA.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND EXISTS 
				(
				SELECT 1 
				FROM weeklyreport.Community C 
				WHERE C.CommunityID = A.CommunityID 
					AND C.WeeklyReportID = @nWeeklyReportID
				)

	SELECT
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.Location.STAsText() AS Location,

		CASE 
			WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapIncident SMI WHERE SMI.IncidentID = I.IncidentID AND SMI.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND 
				(
				EXISTS	
					(
					SELECT 1
					FROM dbo.IncidentCommunity IC
						JOIN weeklyreport.Community C ON C.CommunityID = IC.CommunityID
							AND IC.IncidentID = I.IncidentID
							AND C.WeeklyReportID = @nWeeklyReportID
					)
				OR EXISTS 
					(
					SELECT 1
					FROM dbo.IncidentProvince IP
						JOIN weeklyreport.Province P ON P.ProvinceID = IP.ProvinceID
							AND IP.IncidentID = I.IncidentID
							AND P.WeeklyReportID = @nWeeklyReportID
					)
				)		

	SELECT
		F.ForceID,
		F.ForceName,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor,
		'/assets/img/icons/' + REPLACE(AOT.HexColor, '#', '') + '.png' AS Icon,
		F.Location.STAsText() AS Location,

		CASE 
			WHEN EXISTS (SELECT 1 FROM weeklyreport.SummaryMapForce SMF WHERE SMF.ForceID = F.ForceID AND SMF.WeeklyReportID = @nWeeklyReportID) 
			THEN 1 
			ELSE 0 
		END AS IsMapped

	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND EXISTS
				(
				SELECT 1
				FROM force.ForceCommunity FC
					JOIN weeklyreport.Community C ON C.CommunityID = FC.CommunityID
						AND FC.ForceID = F.ForceID
						AND C.WeeklyReportID = @nWeeklyReportID
				)

	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('WeeklyReport', @nWeeklyReportID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('WeeklyReport', @nWeeklyReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Weekly Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Weekly Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Weekly Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Weekly Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'WeeklyReport'
		AND EL.EntityID = @nWeeklyReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure workplan.deleteWorkplanActivity
EXEC Utility.DropObject 'workplan.DeleteWorkplanActivity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.22
-- Description:	A stored procedure to delete data from the workplan.WorkplanActivity table
-- =======================================================================================
CREATE PROCEDURE workplan.DeleteWorkplanActivity

@WorkplanActivityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE WPA
	FROM workplan.WorkplanActivity WPA
	WHERE WPA.WorkplanActivityID = @WorkplanActivityID

END
GO
--End procedure workplan.DeleteWorkplanActivity

--Begin procedure workplan.GetWorkplanActivityByWorkplanActivityID
EXEC Utility.DropObject 'workplan.GetWorkplanActivityByWorkplanActivityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Kevin Ross
-- Create date: 2016.09.30
-- Description:	A stored procedure to get data from the workplan.WorkplanActivity table
-- ====================================================================================
CREATE PROCEDURE workplan.GetWorkplanActivityByWorkplanActivityID

@WorkplanActivityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		FS1.FundingSourceID AS CurrentFundingSourceID,
		FS1.FundingSourceName AS CurrentFundingSourceName,
		FS2.FundingSourceID AS OriginalFundingSourceID,
		FS2.FundingSourceName AS OriginalFundingSourceName,
		FORMAT(WPA.CurrentUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = WPA.WorkplanID), 'C', 'en-gb') AS CurrentGBPAllocation,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		WPA.Description,
		WPA.Notes,
		FORMAT(WPA.OriginalUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = WPA.WorkplanID), 'C', 'en-gb') AS OriginalGBPAllocation,
		FORMAT(WPA.OriginalUSDAllocation, 'C', 'en-us') AS OriginalUSDAllocation,
		WPA.WorkplanActivityID,
		WPA.WorkplanActivityName,
		WP.EndDate, 
		dbo.FormatDate(WP.EndDate) AS EndDateFormatted
	FROM workplan.WorkplanActivity WPA
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		JOIN dropdown.FundingSource FS1 ON FS1.FundingSourceID = WPA.CurrentFundingSourceID
		JOIN dropdown.FundingSource FS2 ON FS2.FundingSourceID = WPA.OriginalFundingSourceID
		JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
			AND WPA.WorkplanActivityID = @WorkplanActivityID

END
GO
--End procedure workplan.GetWorkplanActivityByWorkplanActivityID

--Begin procedure workplan.GetWorkplanByWorkplanID
EXEC Utility.DropObject 'workplan.GetWorkplanByWorkplanID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.21
-- Description:	A stored procedure to get data from the workplan.Workplan table
-- ============================================================================
CREATE PROCEDURE workplan.GetWorkplanByWorkplanID

@WorkplanID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		WP.EndDate, 
		dbo.FormatDate(WP.EndDate) AS EndDateFormatted,
		WP.IsActive, 
		WP.IsForDashboard, 
		WP.StartDate,
		dbo.FormatDate(WP.StartDate) AS StartDateFormatted,
		WP.UKContractNumber, 
		WP.USContractNumber, 
		WP.USDToGBPExchangeRate,
		WP.WorkplanID, 
		WP.WorkplanName
	FROM workplan.Workplan WP
	WHERE	WP.WorkplanID = @WorkplanID

	SELECT
		CNT.ConceptNoteTypeName,
		FS1.FundingSourceName AS CurrentFundingSourceName,
		FS2.FundingSourceName AS OriginalFundingSourceName,
		FORMAT(WPA.CurrentUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS CurrentGBPAllocation,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		WPA.Description,
		WPA.Notes,
		FORMAT(WPA.OriginalUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS OriginalGBPAllocation,
		FORMAT(WPA.OriginalUSDAllocation, 'C', 'en-us') AS OriginalUSDAllocation,
		WPA.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM workplan.WorkplanActivity WPA
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		JOIN dropdown.FundingSource FS1 ON FS1.FundingSourceID = WPA.CurrentFundingSourceID
		JOIN dropdown.FundingSource FS2 ON FS2.FundingSourceID = WPA.OriginalFundingSourceID
			AND WPA.WorkplanID = @WorkplanID

	SELECT
		D.WorkplanActivityID,
		WPA.WorkplanActivityName,
		CNT.ConceptNoteTypeName,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		FORMAT(SUM(D.Planned), 'C', 'en-us') AS Planned,
		FORMAT(SUM(D.Disbursed), 'C', 'en-us') AS Disbursed,
		FORMAT(SUM(D.CommittedBalance), 'C', 'en-us') AS CommittedBalance
	FROM
		(
		SELECT 
			WPA.WorkplanActivityID,

			CASE
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) = 'Development'
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal
				ELSE 0
			END AS Planned,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				THEN CN.ActualTotalAmount + OACNF.ConceptNoteFinanceTotal
				ELSE 0
			END AS Disbursed,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal - CN.ActualTotalAmount - OACNF.ConceptNoteFinanceTotal
				ELSE 0
			END AS CommittedBalance

		FROM dbo.ConceptNote CN
			JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
				AND WPA.WorkplanID = @WorkplanID
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue), 0) AS TotalBudget
				FROM dbo.ConceptNoteBudget CNB
				WHERE CNB.ConceptNoteID = CN.ConceptNoteID
				) OACNB
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0) AS ConceptNoteEquimentTotal
				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
						AND CNCE.ConceptNoteID = CN.ConceptNoteID
				) OACNCE
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNF.drAmt) - SUM(CNF.CrAmt), 0) AS ConceptNoteFinanceTotal
				FROM dbo.ConceptNoteFinance CNF
				WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
				) OACNF
		) D
	JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = D.WorkplanActivityID
	JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
	GROUP BY D.WorkplanActivityID, WPA.WorkplanActivityName, WPA.CurrentUSDAllocation, CNT.ConceptNoteTypeName
	ORDER BY WPA.WorkplanActivityName

END
GO
--End procedure workplan.GetWorkplanByWorkplanID

--Begin procedure zone.GetZoneByZoneID
EXEC Utility.DropObject 'dbo.GetZoneByZoneID'
EXEC Utility.DropObject 'zone.GetZoneByZoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the zone.Zone table
-- ====================================================================
CREATE PROCEDURE zone.GetZoneByZoneID

@ZoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
 		Z.Location.STAsText() AS Location,
		Z.TerritoryID,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Z.TerritoryTypeCode, Z.TerritoryID) AS TerritoryName,
		Z.TerritoryTypeCode,
		Z.ZoneDescription,
		Z.ZoneID,
		Z.ZoneName,
		ZS.ZoneStatusID,
		ZS.ZoneStatusName,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName
	FROM zone.Zone Z
		JOIN dropdown.ZoneStatus ZS ON ZS.ZoneStatusID = Z.ZoneStatusID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = Z.ZoneTypeID
			AND Z.ZoneID = @ZoneID

END
GO
--End procedure zone.GetZoneByZoneID

--End file Build File - 03 - R thru Z - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS2
GO

--Begin table dbo.Community
UPDATE C
SET C.ImpactDecisionID = 1
FROM dbo.Community C
WHERE C.ImpactDecisionID NOT IN (3,4)
GO

DECLARE @nComponentStatusID INT

SELECT @nComponentStatusID = CS.ComponentStatusID
FROM dropdown.ComponentStatus CS
WHERE CS.ComponentStatusName = 'No'

UPDATE C
SET 
	C.CommunityComponentStatusID = CASE WHEN C.CommunityComponentStatusID = 0 THEN @nComponentStatusID ELSE C.CommunityComponentStatusID END,
	C.JusticeComponentStatusID = CASE WHEN C.JusticeComponentStatusID = 0 THEN @nComponentStatusID ELSE C.JusticeComponentStatusID END,
	C.PoliceComponentStatusID = CASE WHEN C.PoliceComponentStatusID = 0 THEN @nComponentStatusID ELSE C.PoliceComponentStatusID END
FROM dbo.Community C
GO
--End table dbo.Community

--Begin table dbo.Contact
UPDATE C
SET C.CommunityID = asset.GetCommunityIDByAssetUnitID(C.AssetUnitID)
FROM dbo.Contact C
WHERE C.AssetUnitID > 0
GO
--End table dbo.Contact

--Begin table dbo.DocumentEntity
UPDATE DE
SET DE.EntityTypeCode = 'SpotReportDocument'
FROM dbo.DocumentEntity DE
WHERE DE.EntityTypeCode = 'SpotReport'
GO

UPDATE DE
SET DE.EntityTypeCode = 'SpotReport'
FROM dbo.DocumentEntity DE
WHERE DE.EntityTypeCode = 'SpotReportDocument'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 'SpotReport'
FROM dbo.DocumentEntity DE
WHERE DE.EntityTypeCode = 'SpotReportDocument'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 
	CASE
		WHEN D.DocumentDescription = 'Participants Document'
		THEN 'ParticipantDocument'
		WHEN D.DocumentDescription = 'Trainer 1 Document'
		THEN 'Trainer1Document'
		WHEN D.DocumentDescription = 'Trainer 2 Document'
		THEN 'Trainer2Document'
	END
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'Class'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 'ARAPDocument'
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'Community'
		AND D.DocumentDescription LIKE '%ARAP%'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 
	CASE
		WHEN D.DocumentDescription = 'RAP Report'
		THEN 'RAPReport'
		WHEN D.DocumentDescription = 'Finalized CSAP'
		THEN 'FinalizedCSAP'
		WHEN D.DocumentDescription = 'Finalized SCA'
		THEN 'FinalizedSCA'
		WHEN D.DocumentDescription = 'Key Achievement'
		THEN 'KeyAchievement'
		WHEN D.DocumentDescription = 'TOR Document'
		THEN 'TORDocument'
	END
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'CommunityRound'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 
	CASE
		WHEN D.DocumentDescription = 'Final Vendor/Resource Partner Report'
		THEN 'VendorFinalReport'
		WHEN D.DocumentDescription = 'M&E final report'
		THEN 'MonitoringFinalReport'
	END
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'ConceptNote'
GO

UPDATE DE
SET DE.EntityTypeSubCode = 'GOVIDDocument'
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'Contact'
		AND D.DocumentDescription = 'ID Document'
GO

UPDATE DE
SET DE.EntityTypeCode = 'SpotReport'
FROM dbo.DocumentEntity DE
	JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		AND DE.EntityTypeCode = 'SpotReport_v2'
GO

--End table dbo.DocumentEntity

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'Community' AND ET.WorkflowActionCode = 'ImpactChange')
	BEGIN

	INSERT INTO EmailTemplate 
		(EntityTypeCode, WorkflowActionCode, EmailText) 
	VALUES 
		('Community', 'ImpactChange', '<p>[[Community]]</p><p>The above community has changed status. For more detail view the community profile [[CommunityLink]] or contact the MER team.</p>')

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Community' AND ETF.PlaceHolderText = '[[Community]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField 
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder) 
	VALUES 
		('Community', '[[Community]]', 'Community', 0),
		('Community', '[[CommunityLink]]', 'Community Link', 0)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
DELETE ET
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode IN 
	(
	'Atmospheric',
	'CommunityMemberSurvey',
	'CommunityMemberSurveyAdd',
	'CommunityProvinceEngagementCommunity',
	'CommunityProvinceEngagementProvince',
	'CommunityProvinceEngagementUpdate',
	'DailyReport',
	'FIFCommunity',
	'FIFProvince',
	'FIFUpdate',
	'FocusGroupSurvey',
	'JusticeCommunity',
	'JusticeProvince',
	'JusticeUpdate',
	'KeyEvent',
	'KeyInformantSurvey',
	'PoliceEngagementUpdate',
	'RAPData',
	'RapidPerceptionSurvey',
	'StakeholderGroupSurvey',
	'StationCommanderSurvey',
	'Survey',
	'SurveyQuestion',
	'SurveyResponse',
	'Team'
	)

EXEC utility.EntityTypeAddUpdate 'Asset', 'Asset', NULL
GO
EXEC utility.EntityTypeAddUpdate 'AssetUnit', 'Asset Unit', NULL
GO
EXEC utility.EntityTypeAddUpdate 'EventLog', 'Event Log', NULL
GO
EXEC utility.EntityTypeAddUpdate 'MediaReport', 'Media Report', NULL
GO
EXEC utility.EntityTypeAddUpdate 'Workplan', 'Work Plan', NULL
GO
EXEC utility.EntityTypeAddUpdate 'Zone', 'Zone', NULL
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN 
	(
	'Atmospheric',
	'AtmosphericReportList',
	'CommunityAssetList',
	'CommunityMemberSurveyAdd',
	'CommunityProvinceEngagementReport',
	'DailyReportList',
	'DownloadRAPData',
	'FIFUpdate',
	'Finding',
	'RecommendationManagement',
	'RecommendationList',
	'RecommendationUpdate',	
	'FocusGroupSurveyAdd',
	'JusticeUpdate',
	'KeyInformantSurveyAdd',
	'PoliceEngagementUpdate',
	'QuestionList',
	'RAPData',
	'RAPDataList',
	'RAPDelivery',
	'RAPImplementation',
	'RAPTools',
	'StakeholderGroupSurveyAdd',
	'StationCommanderSurveyAdd',
	'SurveyList',
	'Surveys',
	'TeamList'
	)

UPDATE MI
SET MI.MenuItemCode = 'ForceList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'Force'
GO

UPDATE MI
SET MI.MenuItemCode = 'IncidentList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'IncidentReport'
GO

UPDATE MI
SET MI.MenuItemCode = 'MediaReportList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'MediaReport'
GO

UPDATE MI
SET MI.MenuItemCode = 'OperationsSupport'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'OpperationsSupport'
GO

UPDATE MI
SET MI.MenuItemCode = 'SpotReportList'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'SpotReport'
GO

UPDATE MI
SET MI.MenuItemLink = '/contact/policepaymentlist'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode = 'PolicePaymentList'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='OperationsSupport', @NewMenuItemCode='WorkplanList', @NewMenuItemLink='/workplan/list', @NewMenuItemText='Workplans', @AfterMenuItemCode='Activity', @PermissionableLineageList='Workplan.List'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='AssetList', @NewMenuItemLink='/asset/list', @NewMenuItemText='Assets', @PermissionableLineageList='Asset.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='WeeklyReport', @NewMenuItemLink='/weeklyreport/addupdate', @NewMenuItemText='Atmospheric Report'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='IncidentList', @NewMenuItemLink='/incident/list', @NewMenuItemText='Incident Reports', @PermissionableLineageList='Incident.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='MediaReportList', @NewMenuItemLink='/mediareport/list', @NewMenuItemText='Media Reports', @PermissionableLineageList='MediaReport.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='SpotReportList', @NewMenuItemLink='/spotreport/list', @NewMenuItemText='Spot Reports', @PermissionableLineageList='SpotReport.List'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='ZoneList', @NewMenuItemLink='/zone/list', @NewMenuItemText='Zones', @PermissionableLineageList='Zone.List'
GO

UPDATE MI
SET MI.DisplayOrder = 
	CASE
		WHEN MI.MenuItemCode = 'AssetList'
		THEN 1
		WHEN MI.MenuItemCode = 'WeeklyReport'
		THEN 2
		WHEN MI.MenuItemCode = 'IncidentList'
		THEN 3
		WHEN MI.MenuItemCode = 'MediaReportList'
		THEN 4
		WHEN MI.MenuItemCode = 'SpotReportList'
		THEN 5
		WHEN MI.MenuItemCode = 'ZoneList'
		THEN 6
	END
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode IN ('AssetList','WeeklyReport','IncidentList','MediaReportList','SpotReportList','ZoneList')
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='OpperationsSupport'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO

UPDATE MI
SET MI.MenuItemText = 'Operations Support'
FROM dbo.MenuItem MI 
WHERE MI.MenuItemCode IN ('OperationsSupport','OpperationsSupport')
GO
--End table dbo.MenuItem

--Begin table dropdown.AreaOfOperationType
UPDATE dropdown.AreaOfOperationType
SET IsActive = 0
WHERE AreaOfOperationTypeName = 'FSA Area of Operation'
GO

UPDATE dropdown.AreaOfOperationType
SET AreaOfOperationTypeName = 'Extremist Islamic Group Area of Operation'
WHERE AreaOfOperationTypeName = 'Violent Extremists Area of Operation'
GO
--End table dropdown.AreaOfOperationType

--Begin table dropdown.ConceptNoteStatus
IF NOT EXISTS (SELECT 1 FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Amended')
	BEGIN

	INSERT INTO dropdown.ConceptNoteStatus 
		(ConceptNoteStatusCode, ConceptNoteStatusName, DisplayOrder, IsActive)
	VALUES 
		('Amended','Amended',5,1)

	END
--ENDIF
GO
--End table dropdown.ConceptNoteStatus

--Begin table dropdown.ConceptNoteType
IF NOT EXISTS (SELECT 1 FROM dropdown.ConceptNoteType CNT WHERE CNT.ConceptNoteTypeName = 'Communication')
	BEGIN

	INSERT INTO dropdown.ConceptNoteType 
		(ConceptNoteTypeName, IsActive)
	VALUES 
		('Communication',1),
		('MER',1)

	END
--ENDIF
GO

UPDATE dropdown.ConceptNoteType
SET IsActive = 0
WHERE ConceptNoteTypeName IN ('RAP','Rapid Assessment Program','M&E','Research')
GO

UPDATE CN
SET CN.ConceptNoteTypeID = (SELECT CNT1.ConceptNoteTypeID FROM dropdown.ConceptNoteType CNT1 WHERE CNT1.ConceptNoteTypeName = 'MER')
FROM dbo.ConceptNote CN
	JOIN dropdown.ConceptNoteType CNT2 ON CNT2.ConceptNoteTypeID = CN.ConceptNoteTypeID
		AND CNT2.ConceptNoteTypeName IN ('M&E', 'RAP','Research')
GO

UPDATE WPA
SET WPA.ConceptNoteTypeID = (SELECT CNT1.ConceptNoteTypeID FROM dropdown.ConceptNoteType CNT1 WHERE CNT1.ConceptNoteTypeName = 'MER')
FROM workplan.WorkplanActivity WPA
	JOIN dropdown.ConceptNoteType CNT2 ON CNT2.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		AND CNT2.ConceptNoteTypeName IN ('M&E', 'RAP','Research')
GO

UPDATE dropdown.ConceptNoteType
SET DisplayOrder = 0
GO
--End table dropdown.ConceptNoteType

--Begin table dropdown.ContactType
UPDATE dropdown.ContactType
SET DisplayOrder = 0
GO
--End table dropdown.ContactType

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'ARAP')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode, DocumentTypeName, DocumentTypePermissionCode)
	VALUES
		('ARAP', '523 ARAP Document', '523')

	END
--ENDIF
GO

UPDATE DT
SET DT.DocumentGroupID = (SELECT DG.DocumentGroupID FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = '500-599')
FROM dropdown.DocumentType DT
WHERE LEFT(DT.DocumentTypePermissionCode, 1) = '5'
GO
--End table dropdown.DocumentType

--Begin table dropdown.DonorMeetingComponent
UPDATE DMC
SET DMC.IsActive = 0
FROM dropdown.DonorMeetingComponent DMC
WHERE DMC.DonorMeetingComponentName IN ('FIF')
GO
--End table dropdown.DonorMeetingComponent

--Begin table dropdown.FundingSource
UPDATE FS
SET FS.IsActive = 0
FROM dropdown.FundingSource FS
WHERE FS.FundingSourceName IN ('US - CSO', 'US - OPS')
GO

UPDATE FS
SET FS.FundingSourceName = 'EU - North'
FROM dropdown.FundingSource FS
WHERE FS.FundingSourceName IN ('EU')
GO

UPDATE FS
SET FS.FundingSourceName = 'US - North'
FROM dropdown.FundingSource FS
WHERE FS.FundingSourceName IN ('US - NEA')
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.FundingSource FS WHERE FS.FundingSourceName = 'EU - South')
	BEGIN

	INSERT INTO dropdown.FundingSource
		(FundingSourceName, IsActive)
	VALUES
		('EU - South', 1),
		('US - South', 1)
	END
--ENDIF
GO

UPDATE FS
SET FS.DisplayOrder = 0
FROM dropdown.FundingSource FS
GO
--End table dropdown.FundingSource

--Begin table dropdown.ImpactDecision
UPDATE ID
SET ID.ImpactDecisionName = 'Pending'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'Under Review'
GO

UPDATE ID
SET ID.HexColor = '#FF0000'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'No'
GO

UPDATE ID
SET ID.HexColor = '#FFA500'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'Pending'
GO

UPDATE ID
SET ID.HexColor = '#00BB54'
FROM dropdown.ImpactDecision ID
WHERE ID.ImpactDecisionName = 'Yes'
GO
--End table dropdown.ImpactDecision

--Begin table dropdown.ResourceProvider
IF NOT EXISTS (SELECT 1 FROM dropdown.ResourceProvider RP WHERE RP.ResourceProviderName = 'Local')
	BEGIN

	INSERT INTO dropdown.ResourceProvider
		(ResourceProviderName)
	VALUES
		('Local'),
		('Private Donor')

	END
--ENDIF
GO
--End table dropdown.ResourceProvider

--Begin table dropdown.StatusChange
TRUNCATE TABLE dropdown.StatusChange
GO

SET IDENTITY_INSERT dropdown.StatusChange ON
GO

INSERT INTO dropdown.StatusChange
	(StatusChangeID, StatusChangeName)
VALUES
	(0, NULL),
	(1, 'No to Pending'),
	(2, 'No to Yes'),
	(3, 'Pending to No'),
	(4, 'Pending to Yes'),
	(5, 'Yes to No'),
	(6, 'Yes to Pending')
GO

SET IDENTITY_INSERT dropdown.StatusChange OFF
GO
--End table dropdown.StatusChange

--Begin table dropdown.UnitType
UPDATE dropdown.UnitType
SET IsActive = 0
WHERE UnitTypeName IN ('Unit', 'Sub-Unit')
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.UnitType UT WHERE UT.UnitTypeName = 'Battalion')
	BEGIN

	INSERT INTO dropdown.UnitType
		(UnitTypeName, DisplayOrder)
	VALUES
		('Battalion', 3),
		('Company', 4)
		
	END
--ENDIF
GO
--End table dropdown.UnitType

--Begin table eventlog.EventLog
DELETE EL
FROM eventlog.EventLog EL
WHERE EL.EntityTypeCode IN ('Atmospheric')
GO
--End table eventlog.EventLog

--Begin table procurement.DistributedInventory
UPDATE DI
SET DI.EndUserEntityTypeCode = 'Asset'
FROM procurement.DistributedInventory DI
WHERE DI.EndUserEntityTypeCode = 'CommunityAsset'
GO
--End table dropdown.UnitType

UPDATE P
SET P.PermissionableGroupID = (SELECT PG.PermissionableGroupID FROM permissionable.PermissionableGroup PG WHERE PG.PermissionableGroupName = 'Implementation')
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityRound','CommunityRoundActivity')
GO

UPDATE P
SET 
	P.IsGlobal = 0,
	P.IsSuperAdministrator = 1
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('DonorDecision','Zone')
GO

UPDATE P
SET P.IsActive = 0
FROM permissionable.Permissionable P
	JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
		AND PG.PermissionableGroupName = 'Insight & Understanding'
GO

UPDATE P
SET P.MethodName = 'List'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Community.View.Export'
GO

UPDATE P
SET P.MethodName = 'PolicePaymentList'
FROM permissionable.Permissionable P
WHERE P.MethodName = 'PaymentList'
GO

UPDATE PP
SET PP.PermissionableLineage = 'Community.List.Export'
FROM permissionable.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'Community.View.Export'
GO

UPDATE P
SET P.MethodName = 'AddUpdate'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Risk.List.Export'
GO

UPDATE PP
SET PP.PermissionableLineage = 'Risk.AddUpdate.Export'
FROM permissionable.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'Risk.List.Export'
GO

UPDATE P
SET 
	P.MethodName = 'List',
	P.PermissionCode = 'AddContactStipendPaymentContacts'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Contact.ImportPayees'
GO

UPDATE PP
SET PP.PermissionableLineage = 'Contact.List.AddContactStipendPaymentContacts'
FROM permissionable.PersonPermissionable PP
WHERE PP.PermissionableLineage = 'Contact.ImportPayees'
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityMemberSurvey','CommunityProvinceEngagement','CommunityProvinceEngagementUpdate','DailyReport','FIFUpdate','FocusGroupSurvey','Justice','KeyEvent','KeyInformantSurvey','PoliceEngagement','RAPData','RapidPerceptionSurvey','StakeholderGroupSurvey','StationCommanderSurvey','Team')
GO

DELETE PG
FROM permissionable.PermissionableGroup PG
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableGroupID = PG.PermissionableGroupID
	)
GO

EXEC utility.SavePermissionable 'Asset', 'AddUpdate', NULL, 'Asset.AddUpdate', 'Add / edit an asset', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Asset', 'List', NULL, 'Asset.List', 'View the list of assets', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Asset', 'View', NULL, 'Asset.View', 'View an asset', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Contact', 'AddContactStipendPaymentContacts', NULL, 'Contact.AddContactStipendPaymentContacts', 'Add contacts to a stipend payment list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'CashHandOverExport', 'Contact.JusticePaymentList.CashHandOverExport', 'Allow access to the cash hand over report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'OpFundsReport', 'Contact.JusticePaymentList.OpFundsReport', 'Allow access to the op funds report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendActivity', 'Contact.JusticePaymentList.StipendActivity', 'Allow access to the stipend activity report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendPaymentReport', 'Contact.JusticePaymentList.StipendPaymentReport', 'Close out the stipend justice & police payment process', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'ReconcileContactStipendPayment', NULL, 'Contact.ReconcileContactStipendPayment', 'Allow access to the stipend payment report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'AddUpdate', NULL, 'EquipmentDistribution.AddUpdate', 'Add or update an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'Create an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Delete', NULL, 'EquipmentDistribution.Delete', 'Delete an active equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'MediaReport', 'AddUpdate', NULL, 'MediaReport.AddUpdate', 'Add / edit an media report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'List', NULL, 'MediaReport.List', 'View the list of media reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'List', 'Export', 'MediaReport.List.Export', 'Export selected media reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'View', NULL, 'MediaReport.View', 'View a media report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'Delete', NULL, 'PermissionableTemplate.Delete', 'Delete a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add or update a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'Delete', NULL, 'Workplan.Delete', 'Delete a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'List workplans', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Zone', 'AddUpdate', NULL, 'Zone.AddUpdate', 'Add / edit a zone', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Zone', 'List', NULL, 'Zone.List', 'View the list of zones', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Zone', 'View', NULL, 'Zone.View', 'View a zone', 0, 0, 'Research'
GO

EXEC utility.SavePermissionableGroup 'Research', 'Research'
GO

EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO

TRUNCATE TABLE workplan.Workplan
TRUNCATE TABLE workplan.WorkplanActivity
GO

USE AJACS2
GO
SET IDENTITY_INSERT workplan.Workplan ON 

GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (1, N'***TEST**** Workplan Number One ***TEST*** Please Disregard***', CAST(N'2016-10-14' AS Date), CAST(N'2016-10-31' AS Date), N'US-ConNo - 9', N'UK - ConNo - 3', 0, 1, CAST(1.35 AS Numeric(18, 2)))
GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (2, N'October 2016 - March 2017', CAST(N'2016-10-01' AS Date), CAST(N'2017-03-31' AS Date), N'CN37100', N'CPG/1129/2016', 0, 1, CAST(0.74 AS Numeric(18, 2)))
GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (3, N'10/20/2016 - WorkPlan Test 3 ', CAST(N'2016-10-20' AS Date), CAST(N'2016-10-27' AS Date), N'US-9000', N'UK-9000', 0, 1, CAST(1.35 AS Numeric(18, 2)))
GO
INSERT workplan.Workplan (WorkplanID, WorkplanName, StartDate, EndDate, USContractNumber, UKContractNumber, IsForDashboard, IsActive, USDToGBPExchangeRate) VALUES (4, N'***TEST***10212016: OCNDSC-QA WORKPLAN TEST TWO***TEST***', CAST(N'2016-10-21' AS Date), CAST(N'2016-10-28' AS Date), N'WPT: 10212016', N'UK-CONNO: 98658', 0, 1, CAST(0.00 AS Numeric(18, 2)))
GO
SET IDENTITY_INSERT workplan.Workplan OFF
GO
SET IDENTITY_INSERT workplan.WorkplanActivity ON 

GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (17, NULL, 1, 0, 0, 0, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), NULL, NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (1, N'Pencils For Pupils Program', 1, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(1000.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), N'A Community Based Programs to provide basic school supplies for Children', N'THE LINK BETWEEN THE "CURRENT" AND "ORIGINAL" FIELDS IS HIGHLY SUSPECT AT THIS TIME.')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (13, N'Police - EU South', 1, 3, 6, 6, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(150000.00 AS Numeric(18, 2)), CAST(150000.00 AS Numeric(18, 2)), NULL, N'Test Linkage to AS')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (2, N'School Meal Program', 1, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), N'If a Student attends school, the Community has pooled resources for offering a Mid - Day Meal for aspiring students.', N'USD ALLOCATION FIELD COPIES CURRENT VALUE TO ORIGINAL VALUE FIELD AUTOMATICALLY AND CANNOT BE EDITED THEREAFTER')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (9, N'Aleppo Stipends ', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(1880000.00 AS Numeric(18, 2)), CAST(1880000.00 AS Numeric(18, 2)), N'Stipend and operating costs for existing 56 assets and approximately 2,000 officers together with an budgeted expansion of 20% together with the PRU.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (3, N'Equipment (European)', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(900000.00 AS Numeric(18, 2)), CAST(900000.00 AS Numeric(18, 2)), N'Procurement, delivery and distribution of equipment in line with identified station standards and needs. Will be completed over two phases in each province.
Round 1 Aleppo (November to January): USD 300,000
Round 2 Aleppo (January to March): USD 300,000
Round 1 Idlib (November to January): USD 300,000
Round 2 Idlib (January to March): USD 300,000
NB: 75% European and 25% US funded.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (4, N'Equipment (US)', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(300000.00 AS Numeric(18, 2)), CAST(300000.00 AS Numeric(18, 2)), N'Procurement, delivery and distribution of equipment in line with identified station standards and needs. Will be completed over two phases in each province.
Round 1 Aleppo (November to January): USD 300,000
Round 2 Aleppo (January to March): USD 300,000
Round 1 Idlib (November to January): USD 300,000
Round 2 Idlib (January to March): USD 300,000
NB: 75% European and 25% US funded.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (11, N'FSP Liaison Support', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(63000.00 AS Numeric(18, 2)), CAST(63000.00 AS Numeric(18, 2)), N'Support to 2 FSP Police Liaison Officers and 2 Police Support Services Officers to strengthen coordination and relationships between the FSP and AJACS. Also includes PRU Liaison Officer to work with PRU lead to ensure timely development of the PRU. ', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (8, N'Idlib Stipends', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(1200000.00 AS Numeric(18, 2)), CAST(1200000.00 AS Numeric(18, 2)), N'Stipend and operating costs for existing 31 assets and approx. 1,250 officers together with an budgted expansion of 20%. ', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (10, N'Meetings and Workshops', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(125000.00 AS Numeric(18, 2)), CAST(125000.00 AS Numeric(18, 2)), N'Support to Turkey-based meetings and workshops for FSP training and engagement. Assumes the FSP will provide/release new, qualified and competant officers to be trained and that border crossings are obtainable. Includes:
Monthly Management Meetings, Provincial Planning Meetings,  Cross-programme workshops, Province Command meetings, PDU planning,  
Leadership development - District Level and Community Policing workshops. ', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (7, N'Station/ Facilities Repairs and Refurbishments', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(800000.00 AS Numeric(18, 2)), CAST(800000.00 AS Numeric(18, 2)), N'Support to FSP infrastrucutre to repair, refurbish and improve. This includes existing stations, new stations and specialist faciltiies (e.g. training centers). Will be completed over four phases in each province with four projects per phase:', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (12, N'STTA/SME Support', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(543000.00 AS Numeric(18, 2)), CAST(543000.00 AS Numeric(18, 2)), N'Support to the FSP in delivering effective doctrine, plans, policies, procedures and standards. Expected to include 5 STTA/SMEs for 6 months of a joint AJACS/FSP 12 month project,  covering the following topics:              
SME 1: Doctrine Development 
SME 2: C2 Review and Implementation of  District Commands 
SME 3: Creation of Policies & Standards 
SME 4: Community Policing 
SME 5: Review FSP Support Structures (Procurement, Logistics, HR)', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (5, N'Training Delivery', 2, 3, 2, 2, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(212000.00 AS Numeric(18, 2)), CAST(212000.00 AS Numeric(18, 2)), N'Support to in-country delivery of training by the FSP and includes trainer stipends, trainee stipends and operating costs for the delivery of training. The following training will be delivered: Modules 1 & 2 in Aleppo and Idlib, PRU in Aleppo and Leadership in Aleppo & Idlib.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (6, N'Training Development', 2, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(450000.00 AS Numeric(18, 2)), CAST(450000.00 AS Numeric(18, 2)), N'Development of training packages - module 2, PRU and leadership - using new training tools will be used to better capture the learning cycle in accordance with logframe. This will apply for both the cascade (trainer development) as well as the trainees.', NULL)
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (14, N'Tactical Pylon', 3, 3, 4, 4, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), N'A WayPoint Pylon for enhanced navigation of Defense Forces.', N'OCNDSC-QA  
SUPER ADMIN
AJACS-35
***TESTING***')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (15, N'Tactical Pylon Signal Repeater', 3, 3, 6, 6, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(200.00 AS Numeric(18, 2)), CAST(200.00 AS Numeric(18, 2)), N'A signal reshaping and amplification device.', N'Sort Column seems to poplulate the "Original" Column Value.')
GO
INSERT workplan.WorkplanActivity (WorkplanActivityID, WorkplanActivityName, WorkplanID, ConceptNoteTypeID, CurrentFundingSourceID, OriginalFundingSourceID, CurrentGBPAllocation, OriginalGBPAllocation, CurrentUSDAllocation, OriginalUSDAllocation, Description, Notes) VALUES (16, N'Physical Fitness Training Program', 4, 3, 4, 6, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), N'An "Action Oriented" Physical Fitness Program that helps ensure optimal 
physical performance. ', NULL)
GO
SET IDENTITY_INSERT workplan.WorkplanActivity OFF
GO

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Community', 'Community', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contact', 0
GO
EXEC utility.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'Implementation', 'Implementation', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Province', 'Province', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Research', 0
GO
EXEC utility.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflows', 'Workflows', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Data Export', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'DataImport', 'Default', NULL, 'DataImport.Default', 'Data Import', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'AddUpdate', NULL, 'EmailTemplateAdministration.AddUpdate', 'Add / edit an email template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'List EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'View', NULL, 'EmailTemplateAdministration.View', 'View EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'List EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Exports', 'BusinessLicenseReport', NULL, 'Exports.BusinessLicenseReport', 'Business License Report Exports', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'CanRecieveEmail', 'CanRecieveEmail', 'Main.CanRecieveEmail.CanRecieveEmail', 'User Can Receive Email', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors SiteConfiguration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'Delete', NULL, 'Permissionable.Delete', 'Delete a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'Delete', NULL, 'PermissionableTemplate.Delete', 'Delete a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'List PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'Delete', NULL, 'Person.Delete', 'Delete a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'ViewPermissionables', NULL, 'Person.ViewPermissionables', 'View list of permissionables on view page', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'List the server setup keys', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Community', 'AddUpdate', NULL, 'Community.AddUpdate', 'Add / edit a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'AddUpdate', 'ImpactUpdateEmail', 'Community.AddUpdate.ImpactUpdateEmail', 'Recieve email for Update to Impact Engagement', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'List Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'List', 'Export', 'Community.List.Export', 'Export Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'View the analysis tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'ExportEquipmentDistribution', 'Community.View.ExportEquipmentDistribution', 'Export Equipment Distributions Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'Implementation Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'View the information tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Contact', 'AddContactStipendPaymentContacts', NULL, 'Contact.AddContactStipendPaymentContacts', 'Add contacts to a stipend payment list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'CETeam', 'Contact.AddUpdate.CETeam', 'Add / edit contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'FieldStaff', 'Contact.AddUpdate.FieldStaff', 'Add / edit contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'IO4', 'Contact.AddUpdate.IO4', 'Add / edit contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeOther', 'Contact.AddUpdate.JusticeOther', 'Add / edit contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeStipend', 'Contact.AddUpdate.JusticeStipend', 'Add / edit contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersStakeholder', 'Contact.AddUpdate.PartnersStakeholder', 'Add / edit contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersSupplierVendor', 'Contact.AddUpdate.PartnersSupplierVendor', 'Add / edit contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceOther', 'Contact.AddUpdate.PoliceOther', 'Add / edit contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceStipend', 'Contact.AddUpdate.PoliceStipend', 'Add / edit contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'ProjectStaff', 'Contact.AddUpdate.ProjectStaff', 'Add / edit contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'SubContractors', 'Contact.AddUpdate.SubContractors', 'Add / edit contacts of type Sub-Contractor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', NULL, 'Contact.JusticePaymentList', 'Allows view of justice stipends payments', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'CashHandOverExport', 'Contact.JusticePaymentList.CashHandOverExport', 'Allow access to the cash hand over report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'OpFundsReport', 'Contact.JusticePaymentList.OpFundsReport', 'Allow access to the op funds report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendActivity', 'Contact.JusticePaymentList.StipendActivity', 'Allow access to the stipend activity report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'JusticePaymentList', 'StipendPaymentReport', 'Contact.JusticePaymentList.StipendPaymentReport', 'Close out the stipend justice & police payment process', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'AddContactStipendPaymentContacts', 'Contact.List.AddContactStipendPaymentContacts', 'Allows import of payees in payment system', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'CETeam', 'Contact.List.CETeam', 'Include contacts of type CE Team in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ExportPayees', 'Contact.List.ExportPayees', 'Export payees from the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'FieldStaff', 'Contact.List.FieldStaff', 'Include contacts of type Field Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'IO4', 'Contact.List.IO4', 'Include contacts of type IO4 in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeOther', 'Contact.List.JusticeOther', 'Include contacts of type Justice Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeStipend', 'Contact.List.JusticeStipend', 'Include contacts of type Justice Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersStakeholder', 'Contact.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersSupplierVendor', 'Contact.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceOther', 'Contact.List.PoliceOther', 'Include contacts of type Police Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceStipend', 'Contact.List.PoliceStipend', 'Include contacts of type Police Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ProjectStaff', 'Contact.List.ProjectStaff', 'Include contacts of type Project Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'SubContractors', 'Contact.List.SubContractors', 'Include contacts of type Sub-Contractor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PolicePaymentList', NULL, 'Contact.PolicePaymentList', 'View the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PolicePaymentList', 'CashHandOverExport', 'Contact.PolicePaymentList.CashHandOverExport', 'Export the cash handover report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PolicePaymentList', 'OpFundsReport', 'Contact.PolicePaymentList.OpFundsReport', 'Export the op funds report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PolicePaymentList', 'StipendActivity', 'Contact.PolicePaymentList.StipendActivity', 'Export the stipend activity report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PolicePaymentList', 'StipendPaymentReport', 'Contact.PolicePaymentList.StipendPaymentReport', 'Export the stipend payment report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'ReconcileContactStipendPayment', NULL, 'Contact.ReconcileContactStipendPayment', 'Allow access to the stipend payment report on the justice stipends list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'CETeam', 'Contact.View.CETeam', 'View contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'FieldStaff', 'Contact.View.FieldStaff', 'View contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'IO4', 'Contact.View.IO4', 'View contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeOther', 'Contact.View.JusticeOther', 'View contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeStipend', 'Contact.View.JusticeStipend', 'View contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersStakeholder', 'Contact.View.PartnersStakeholder', 'View contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersSupplierVendor', 'Contact.View.PartnersSupplierVendor', 'View contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceOther', 'Contact.View.PoliceOther', 'View contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceStipend', 'Contact.View.PoliceStipend', 'View contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'ProjectStaff', 'Contact.View.ProjectStaff', 'View contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'SubContractors', 'Contact.View.SubContractors', 'View contacts of type Sub-Contractors', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'VettingMoreInfo', 'Contact.View.VettingMoreInfo', 'View the more info button on the vetting history data table', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'CETeam', 'Vetting.List.CETeam', 'Include contacts of type CE Team in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'Export', 'Vetting.List.Export', 'Export the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'FieldStaff', 'Vetting.List.FieldStaff', 'Include contacts of type Field Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'IO4', 'Vetting.List.IO4', 'Include contacts of type IO4 in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeOther', 'Vetting.List.JusticeOther', 'Include contacts of type Justice Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeStipend', 'Vetting.List.JusticeStipend', 'Include contacts of type Justice Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersStakeholder', 'Vetting.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersSupplierVendor', 'Vetting.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceOther', 'Vetting.List.PoliceOther', 'Include contacts of type Police Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceStipend', 'Vetting.List.PoliceStipend', 'Include contacts of type Police Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'ProjectStaff', 'Vetting.List.ProjectStaff', 'Include contacts of type Project Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'SubContractors', 'Vetting.List.SubContractors', 'Include contacts of type Sub-Contractor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeChangeNotification', 'Vetting.List.VettingOutcomeChangeNotification', 'Recieve an email when a vetting outcome has changed for one or more contacts', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeConsider', 'Vetting.List.VettingOutcomeConsider', 'Assign a vetting outcome of "Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeDoNotConsider', 'Vetting.List.VettingOutcomeDoNotConsider', 'Assign a vetting outcome of "Do Not Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeInsufficientData', 'Vetting.List.VettingOutcomeInsufficientData', 'Assign a vetting outcome of "Insufficient Data"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeNotVetted', 'Vetting.List.VettingOutcomeNotVetted', 'Assign a vetting outcome of "Not Vetted"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomePendingInternalReview', 'Vetting.List.VettingOutcomePendingInternalReview', 'Assign a vetting outcome of "Pending Internal Review"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeSubmittedforVetting', 'Vetting.List.VettingOutcomeSubmittedforVetting', 'Assign a vetting outcome of "Submitted for Vetting"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUK', 'Vetting.List.VettingTypeUK', 'Update UK vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUS', 'Vetting.List.VettingTypeUS', 'Update US vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'Notification', 'ExpirationCountEmail', 'Vetting.Notification.ExpirationCountEmail', 'Recieve the monthly vetting expiration counts e-mail', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', NULL, 'Document.AddUpdate', '', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '004', 'Document.AddUpdate.004', 'Add / edit 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '100', 'Document.AddUpdate.100', 'Add / edit 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '101', 'Document.AddUpdate.101', 'Add / edit 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '102', 'Document.AddUpdate.102', 'Add / edit 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '103', 'Document.AddUpdate.103', 'Add / edit 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '104', 'Document.AddUpdate.104', 'Add / edit 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '105', 'Document.AddUpdate.105', 'Add / edit 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '106', 'Document.AddUpdate.106', 'Add / edit 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '107', 'Document.AddUpdate.107', 'Add / edit 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '108', 'Document.AddUpdate.108', 'Add / edit 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '109', 'Document.AddUpdate.109', 'Add / edit 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '301', 'Document.AddUpdate.301', 'Add / edit 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '302', 'Document.AddUpdate.302', 'Add / edit 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '303', 'Document.AddUpdate.303', 'Add / edit 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '304', 'Document.AddUpdate.304', 'Add / edit 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '306', 'Document.AddUpdate.306', 'Add / edit 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '500', 'Document.AddUpdate.500', 'Add / edit 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '501', 'Document.AddUpdate.501', 'Add / edit 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '502', 'Document.AddUpdate.502', 'Add / edit 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '503', 'Document.AddUpdate.503', 'Add / edit 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '504', 'Document.AddUpdate.504', 'Add / edit 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '505', 'Document.AddUpdate.505', 'Add / edit 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '506', 'Document.AddUpdate.506', 'Add / edit 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '507', 'Document.AddUpdate.507', 'Add / edit 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '508', 'Document.AddUpdate.508', 'Add / edit 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '509', 'Document.AddUpdate.509', 'Add / edit 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '510', 'Document.AddUpdate.510', 'Add / edit 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '511', 'Document.AddUpdate.511', 'Add / edit 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '512', 'Document.AddUpdate.512', 'Add / edit 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '513', 'Document.AddUpdate.513', 'Add / edit 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '514', 'Document.AddUpdate.514', 'Add / edit 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '515', 'Document.AddUpdate.515', 'Add / edit 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '516', 'Document.AddUpdate.516', 'Add / edit 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '517', 'Document.AddUpdate.517', 'Add / edit 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '518', 'Document.AddUpdate.518', 'Add / edit 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '519', 'Document.AddUpdate.519', 'Add / edit 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '520', 'Document.AddUpdate.520', 'Add / edit 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '521', 'Document.AddUpdate.521', 'Add / edit 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '600', 'Document.AddUpdate.600', 'Add / edit 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '601', 'Document.AddUpdate.601', 'Add / edit 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '602', 'Document.AddUpdate.602', 'Add / edit 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '603', 'Document.AddUpdate.603', 'Add / edit 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '604', 'Document.AddUpdate.604', 'Add / edit 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '605', 'Document.AddUpdate.605', 'Add / edit 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '700', 'Document.AddUpdate.700', 'Add / edit 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '701', 'Document.AddUpdate.701', 'Add / edit 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '702', 'Document.AddUpdate.702', 'Add / edit 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '801', 'Document.AddUpdate.801', 'Add / edit 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '802', 'Document.AddUpdate.802', 'Add / edit 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '803', 'Document.AddUpdate.803', 'Add / edit 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '804', 'Document.AddUpdate.804', 'Add / edit 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '805', 'Document.AddUpdate.805', 'Add / edit 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '806', 'Document.AddUpdate.806', 'Add / edit 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '807', 'Document.AddUpdate.807', 'Add / edit 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '808', 'Document.AddUpdate.808', 'Add / edit 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '900', 'Document.AddUpdate.900', 'Add / edit 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '901', 'Document.AddUpdate.901', 'Add / edit 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '902', 'Document.AddUpdate.902', 'Add / edit 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '903', 'Document.AddUpdate.903', 'Add / edit 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '904', 'Document.AddUpdate.904', 'Add / edit 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '905', 'Document.AddUpdate.905', 'Add / edit 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '906', 'Document.AddUpdate.906', 'Add / edit 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '907', 'Document.AddUpdate.907', 'Add / edit 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '908', 'Document.AddUpdate.908', 'Add / edit 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '909', 'Document.AddUpdate.909', 'Add / edit 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '910', 'Document.AddUpdate.910', 'Add / edit 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '004', 'Document.View.004', 'View 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '100', 'Document.View.100', 'View 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '101', 'Document.View.101', 'View 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '102', 'Document.View.102', 'View 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '103', 'Document.View.103', 'View 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '104', 'Document.View.104', 'View 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '105', 'Document.View.105', 'View 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '106', 'Document.View.106', 'View 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '107', 'Document.View.107', 'View 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '108', 'Document.View.108', 'View 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '109', 'Document.View.109', 'View 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '301', 'Document.View.301', 'View 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '302', 'Document.View.302', 'View 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '303', 'Document.View.303', 'View 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '304', 'Document.View.304', 'View 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '306', 'Document.View.306', 'View 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '500', 'Document.View.500', 'View 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '501', 'Document.View.501', 'View 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '502', 'Document.View.502', 'View 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '503', 'Document.View.503', 'View 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '504', 'Document.View.504', 'View 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '505', 'Document.View.505', 'View 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '506', 'Document.View.506', 'View 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '507', 'Document.View.507', 'View 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '508', 'Document.View.508', 'View 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '509', 'Document.View.509', 'View 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '510', 'Document.View.510', 'View 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '511', 'Document.View.511', 'View 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '512', 'Document.View.512', 'View 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '513', 'Document.View.513', 'View 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '514', 'Document.View.514', 'View 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '515', 'Document.View.515', 'View 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '516', 'Document.View.516', 'View 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '517', 'Document.View.517', 'View 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '518', 'Document.View.518', 'View 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '519', 'Document.View.519', 'View 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '520', 'Document.View.520', 'View 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '521', 'Document.View.521', 'View 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '600', 'Document.View.600', 'View 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '601', 'Document.View.601', 'View 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '602', 'Document.View.602', 'View 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '603', 'Document.View.603', 'View 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '604', 'Document.View.604', 'View 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '605', 'Document.View.605', 'View 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '700', 'Document.View.700', 'View 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '701', 'Document.View.701', 'View 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '702', 'Document.View.702', 'View 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '801', 'Document.View.801', 'View 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '802', 'Document.View.802', 'View 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '803', 'Document.View.803', 'View 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '804', 'Document.View.804', 'View 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '805', 'Document.View.805', 'View 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '806', 'Document.View.806', 'View 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '807', 'Document.View.807', 'View 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '808', 'Document.View.808', 'View 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '900', 'Document.View.900', 'View 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '901', 'Document.View.901', 'View 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '902', 'Document.View.902', 'View 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '903', 'Document.View.903', 'View 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '904', 'Document.View.904', 'View 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '905', 'Document.View.905', 'View 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '906', 'Document.View.906', 'View 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '907', 'Document.View.907', 'View 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '908', 'Document.View.908', 'View 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '909', 'Document.View.909', 'View 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '910', 'Document.View.910', 'View 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateDecision', NULL, 'DonorDecision.AddUpdateDecision', 'Add / edit a donor decision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateMeeting', NULL, 'DonorDecision.AddUpdateMeeting', 'Add / edit donor meetings & actions', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'List', NULL, 'DonorDecision.List', 'List Donor Decisions, Meetings & Actions DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'View', NULL, 'DonorDecision.View', 'View DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'AddUpdate', NULL, 'EquipmentCatalog.AddUpdate', 'Add / edit the equipment catalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'List', NULL, 'EquipmentCatalog.List', 'List EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'View', NULL, 'EquipmentCatalog.View', 'View EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'AddUpdate', NULL, 'EquipmentDistribution.AddUpdate', 'Add or update an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Audit', NULL, 'EquipmentDistribution.Audit', 'EquipmentDistribution.Audit', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'Create an equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Delete', NULL, 'EquipmentDistribution.Delete', 'Delete an active equipment distribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'FinalizeEquipmentDistribution', NULL, 'EquipmentDistribution.FinalizeEquipmentDistribution', 'EquipmentDistribution.FinalizeEquipmentDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistributedInventory', NULL, 'EquipmentDistribution.ListDistributedInventory', 'EquipmentDistribution.ListDistributedInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistribution', NULL, 'EquipmentDistribution.ListDistribution', 'EquipmentDistribution.ListDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListInventory', NULL, 'EquipmentDistribution.ListInventory', 'EquipmentDistribution.ListInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'SetDeliveryDate', NULL, 'EquipmentDistribution.SetDeliveryDate', 'EquipmentDistribution.SetDeliveryDate', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Transfer', NULL, 'EquipmentDistribution.Transfer', 'EquipmentDistribution.Transfer', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit the equipment inventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'List EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', 'Export', 'EquipmentInventory.List.Export', 'Export EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'Audit', NULL, 'EquipmentManagement.Audit', 'Audit Equipment EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'List', NULL, 'EquipmentManagement.List', 'List Equipment Locations EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'AddUpdate', NULL, 'CommunityAsset.AddUpdate', 'Add / edit a community asset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'List', NULL, 'CommunityAsset.List', 'List CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'View', NULL, 'CommunityAsset.View', 'View CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'AddUpdate', NULL, 'CommunityRound.AddUpdate', 'Add / edit a community round', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'List', NULL, 'CommunityRound.List', 'View the list of community rounds', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'View', NULL, 'CommunityRound.View', 'View a community round', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'AddUpdate', NULL, 'CommunityRoundActivity.AddUpdate', 'CommunityRoundActivity.AddUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'List', NULL, 'CommunityRoundActivity.List', 'CommunityRoundActivity.List', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityRoundActivity', 'View', NULL, 'CommunityRoundActivity.View', 'CommunityRoundActivity.View', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'AddUpdate', NULL, 'Project.AddUpdate', 'Add / edit a project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'List', NULL, 'Project.List', 'List Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'View', NULL, 'Project.View', 'View Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Indicator', 'AddUpdate', NULL, 'Indicator.AddUpdate', 'Add / edit an indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'List', NULL, 'Indicator.List', 'List Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'View', NULL, 'Indicator.View', 'View Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'AddUpdate', NULL, 'IndicatorType.AddUpdate', 'Add / edit an indicatortype', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'List', NULL, 'IndicatorType.List', 'List IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'View', NULL, 'IndicatorType.View', 'View IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'AddUpdate', NULL, 'Milestone.AddUpdate', 'Add / edit a milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'List', NULL, 'Milestone.List', 'List Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'View', NULL, 'Milestone.View', 'View Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'AddUpdate', NULL, 'Objective.AddUpdate', 'Add / edit an objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', NULL, 'Objective.ChartList', 'M & E Overview Charts Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'List', NULL, 'Objective.List', 'List Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage Objectives & Indicators Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'Overview Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'View', NULL, 'Objective.View', 'View Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', NULL, 'ConceptNote.AddUpdate', 'Add / edit a concep nNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'ExportConceptNoteBudget', 'ConceptNote.AddUpdate.ExportConceptNoteBudget', 'Export Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'Finances', 'ConceptNote.AddUpdate.Finances', 'Add / edit activity finances', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'Risk', 'ConceptNote.AddUpdate.Risk', 'View the risk pane a concept note', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', NULL, 'ConceptNote.List', 'List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', 'Export', 'ConceptNote.List.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', NULL, 'ConceptNote.VettingList', 'Vetting List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', 'ExportVetting', 'ConceptNote.VettingList.ExportVetting', 'Export Vetting ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', NULL, 'ConceptNote.View', 'View ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetAccesstoJustice', 'ConceptNote.View.BudgetAccesstoJustice', 'View the budget on concept notes of component type Access to Justice', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetCommunication', 'ConceptNote.View.BudgetCommunication', 'View the budget on concept notes of component type Communication', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetCommunityEngagement', 'ConceptNote.View.BudgetCommunityEngagement', 'View the budget on concept notes of component type Community Engagement', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetIntegratedLegitimateStructures', 'ConceptNote.View.BudgetIntegratedLegitimateStructures', 'View the budget on concept notes of component type Integrated Legitimate Structures', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetM&E', 'ConceptNote.View.BudgetM&E', 'View the budget on concept notes of component type M&E', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetMER', 'ConceptNote.View.BudgetMER', 'View the budget on concept notes of component type MER', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetPoliceDevelopment', 'ConceptNote.View.BudgetPoliceDevelopment', 'View the budget on concept notes of component type Police Development', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetRAP', 'ConceptNote.View.BudgetRAP', 'View the budget on concept notes of component type RAP', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'BudgetResearch', 'ConceptNote.View.BudgetResearch', 'View the budget on concept notes of component type Research', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'Export', 'ConceptNote.View.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'Risk', 'ConceptNote.View.Risk', 'View the risk pane a concept note', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'AddUpdate', NULL, 'ConceptNoteContactEquipment.AddUpdate', 'Add / edit equipment associated with a concept note', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'FinalizeEquipmentDistribution', NULL, 'ConceptNoteContactEquipment.FinalizeEquipmentDistribution', 'Finalize Equipment Distribution ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'List', NULL, 'ConceptNoteContactEquipment.List', 'List ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', NULL, 'ConceptNoteContactEquipment.View', 'View ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', 'Export', 'ConceptNoteContactEquipment.View.Export', 'Export ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'AddUpdate', NULL, 'License.AddUpdate', 'Add / edit a license', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'List', NULL, 'License.List', 'List License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'View', NULL, 'License.View', 'View License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'AddUpdate', NULL, 'LicenseEquipmentCatalog.AddUpdate', 'Add / edit the license equipment catalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', NULL, 'LicenseEquipmentCatalog.List', 'List LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', 'Export', 'LicenseEquipmentCatalog.List.Export', 'Export LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'View', NULL, 'LicenseEquipmentCatalog.View', 'View LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'AddUpdate', NULL, 'PurchaseRequest.AddUpdate', 'Add / edit a purchase request', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'List', NULL, 'PurchaseRequest.List', 'List PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', NULL, 'PurchaseRequest.View', 'View PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', 'Export', 'PurchaseRequest.View.Export', 'Export PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'AddUpdate', NULL, 'Workplan.AddUpdate', 'Add or update a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'Delete', NULL, 'Workplan.Delete', 'Delete a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'List', NULL, 'Workplan.List', 'List workplans', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'Workplan', 'View', NULL, 'Workplan.View', 'View a workplan', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'AddUpdate', NULL, 'ProgramReport.AddUpdate', 'Add / edit a program report', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'List', NULL, 'ProgramReport.List', 'List ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', NULL, 'ProgramReport.View', 'View ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', 'Export', 'ProgramReport.View.Export', 'Export ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'Province', 'AddUpdate', NULL, 'Province.AddUpdate', 'Add / edit a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'List', NULL, 'Province.List', 'List Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', NULL, 'Province.View', 'View Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Analysis', 'Province.View.Analysis', 'View the analysis tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'ExportEquipmentDistribution', 'Province.View.ExportEquipmentDistribution', 'Export Equipment Distributions Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Implementation', 'Province.View.Implementation', 'Implementation Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Information', 'Province.View.Information', 'View the information tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Asset', 'AddUpdate', NULL, 'Asset.AddUpdate', 'Add / edit an asset', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Asset', 'List', NULL, 'Asset.List', 'View the list of assets', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Asset', 'View', NULL, 'Asset.View', 'View an asset', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'AddUpdate', NULL, 'Atmospheric.AddUpdate', 'Add / edit an atmospheric report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'List Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'AddUpdate', NULL, 'Finding.AddUpdate', 'Add / edit a finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'List Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'AddUpdate', NULL, 'Force.AddUpdate', 'Add / edit a force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'List Forces Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'AddUpdate', NULL, 'Incident.AddUpdate', 'Add / edit an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'List Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'AddUpdate', NULL, 'MediaReport.AddUpdate', 'Add / edit an media report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'List', NULL, 'MediaReport.List', 'View the list of media reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'List', 'Export', 'MediaReport.List.Export', 'Export selected media reports', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'MediaReport', 'View', NULL, 'MediaReport.View', 'View a media report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'AddUpdate', NULL, 'Recommendation.AddUpdate', 'Add / edit a recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'List Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', 'Export', 'Recommendation.List.Export', 'Export Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Add', NULL, 'RequestForInformation.Add', 'Add a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'List RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', 'Export', 'RequestForInformation.List.Export', 'Export RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', NULL, 'RequestForInformation.Update', 'Edit a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', 'Amend', 'RequestForInformation.Update.Amend', 'Amend a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', NULL, 'Risk.AddUpdate', 'Add / edit a risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', 'Export', 'Risk.AddUpdate.Export', 'Export Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'List Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', NULL, 'SpotReport.AddUpdate', 'Add / edit a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', 'Amend', 'SpotReport.AddUpdate.Amend', 'Amend a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'List SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Approved', 'SpotReport.View.Approved', 'Approved SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Export', 'SpotReport.View.Export', 'Export SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'InWork', 'SpotReport.View.InWork', 'In Work SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Add / edit a survey question', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Add / edit a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Administer a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Export Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListQuestions', NULL, 'SurveyManagement.ListQuestions', 'Questions List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveyResponses', NULL, 'SurveyManagement.ListSurveyResponses', 'List Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Surveys List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'View Questions SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'View Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', NULL, 'WeeklyReport.AddUpdate', 'Weekly Report Add / Update', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', 'Export', 'WeeklyReport.AddUpdate.Export', 'Export the weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'View', NULL, 'WeeklyReport.View', 'View the completed weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Zone', 'AddUpdate', NULL, 'Zone.AddUpdate', 'Add / edit a zone', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Zone', 'List', NULL, 'Zone.List', 'View the list of zones', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Zone', 'View', NULL, 'Zone.View', 'View a zone', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SubContractor', 'AddUpdate', NULL, 'SubContractor.AddUpdate', 'Add / edit a sub-contractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'List', NULL, 'SubContractor.List', 'List SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'View', NULL, 'SubContractor.View', 'View SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / edit a class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'List Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / edit a course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'List Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the list of workflows', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflows'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP FROM permissionable.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table permissionable.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-2.0 File 01 - AJACS - 2016.10.30 14.22.53')
GO
--End build tracking

