USE AJACS2
GO

--Begin procedure dbo.AddContactStipendPaymentContacts
EXEC Utility.DropObject 'dbo.AddContactStipendPaymentContacts'
EXEC Utility.DropObject 'dbo.ImportPayees'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	A stored procedure to add payees to the ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016..09.18
-- Description:	Refactored for asset support
-- ================================================================================
CREATE PROCEDURE dbo.AddContactStipendPaymentContacts

@PaymentMonth INT,
@PaymentYear INT,
@PersonID INT,
@ContactIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY, StipendTypeCode VARCHAR(50))

	IF @ContactIDList = ''
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)
			JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Contact'
				AND SR.EntityID = C.ContactID
				AND SR.PersonID = @PersonID

		END
	ELSE
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)
			JOIN dbo.ListToTable(@ContactIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ContactID

		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM @tOutput T WHERE T.StipendTypeCode = 'JusticeStipend')
		BEGIN
		
		INSERT INTO asset.AssetUnitExpense
			(AssetUnitID, ExpenseAmountAuthorized, ProvinceID, PaymentMonth, PaymentYear)
		SELECT DISTINCT
			AU.AssetUnitID,
			AUC.AssetUnitCostName,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			@PaymentMonth,
			@PaymentYear
		FROM asset.AssetUnit AU
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			JOIN dbo.Contact C ON C.AssetUnitID = AU.AssetUnitID
			JOIN @tOutput O ON O.ContactID = C.ContactID
				AND NOT EXISTS
					(
					SELECT 1
					FROM asset.AssetUnitExpense AUE
					WHERE AUE.AssetUnitID = AU.AssetUnitID
						AND AUE.PaymentMonth = @PaymentMonth
						AND AUE.PaymentYear = @PaymentYear
					)

		END
	--ENDIF
	
END
GO
--End procedure dbo.AddContactStipendPaymentContacts

--Begin procedure dbo.AddContactsToConceptNote
EXEC Utility.DropObject 'dbo.AddContactsToConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.03
-- Description:	A stored procedure to add a list of contacts to a concept note
-- ===========================================================================
CREATE PROCEDURE dbo.AddContactsToConceptNote

@ConceptNoteID INT,
@ContactIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.ConceptNoteContact
		(ConceptNoteID, ContactID)
	SELECT
		@ConceptNoteID,
		CAST(LTT.ListItem AS INT)
	FROM dbo.ListToTable(@ContactIDList, ',') LTT
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM dbo.ConceptNoteContact CNC
		WHERE CNC.ConceptNoteID = @ConceptNoteID
			AND CNC.ContactID = CAST(LTT.ListItem AS INT)
		)

END
GO
--End procedure dbo.AddContactsToConceptNote

--Begin procedure dbo.ApproveContactStipendPayment
EXEC Utility.DropObject 'dbo.ApproveContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ApproveContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET 
		CSP.StipendAuthorizedDate = getDate(),
		CSP.StipendAmountPaid = 
			CASE
				WHEN dbo.GetContactStipendEligibilityByContactID(CSP.ContactID) = 1
				THEN CSP.StipendAmountAuthorized
				ELSE 0
			END

	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = @StipendTypeCode
		AND CSP.StipendAuthorizedDate IS NULL

	IF @StipendTypeCode = 'JusticeStipend'
		BEGIN

		UPDATE AUE
		SET 
			AUE.ExpenseAuthorizedDate = getDate(),
			AUE.ExpenseAmountPaid = AUE.ExpenseAmountAuthorized
		FROM asset.AssetUnitExpense AUE
		WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
			AND AUE.ProvinceID = @ProvinceID
			AND AUE.ExpenseAuthorizedDate IS NULL

		END
	--ENDIF

END
GO
--End procedure dbo.ApproveContactStipendPayment

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT,
@WorkflowID INT,
@IsAmendment BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @IsAmendment = 1
		BEGIN

		SELECT @WorkflowID = EWSGP.WorkflowID
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = 'ConceptNote' 
			AND EWSGP.EntityID = @ConceptNoteID

		END
	--ENDIF

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,ActualTotalAmount,AmendedConceptNoteID,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,EndDate,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,StartDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber,WorkplanActivityID)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,

		CASE
			WHEN @IsAmendment = 1
			THEN C.ActualTotalAmount
			ELSE 0
		END,

		CASE
			WHEN @IsAmendment = 1
			THEN @ConceptNoteID
			ELSE 0
		END,

		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.EndDate,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.StartDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,

		CASE
			WHEN @IsAmendment = 1
			THEN C.Title
			ELSE 'Clone of:  ' + C.Title
		END,

		C.VettingRequirements,
		1,
		WorkplanActivityID		
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@nConceptNoteID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.WorkflowID = @WorkflowID

	INSERT INTO	dbo.ConceptNoteAmendment
		(ConceptNoteID, AmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Amendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Amendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.DeleteContactStipendPayment
EXEC Utility.DropObject 'dbo.DeleteContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to delete data from the dbo.DeleteContactStipendPayment table
-- =============================================================================================
CREATE PROCEDURE dbo.DeleteContactStipendPayment

@ContactStipendPaymentID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE CSP
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.ContactStipendPaymentID = @ContactStipendPaymentID

END
GO
--End procedure dbo.DeleteContactStipendPayment

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
--
-- Author:			Todd Pires
-- Update date:	2015.05.28
-- Description:	Added vetting data
--
-- Author:			Todd Pires
-- Update date:	2016.09.09
-- Description:	Pointed to the dbo.ContactVetting table
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName,
		ISNULL(CNC2.ConceptNoteID, 0) AS ConceptNoteID
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT CNC1.ConceptNoteID 
			FROM dbo.ConceptNoteClass CNC1
			WHERE CNC1.ClassID = @ClassID
			) CNC2
	
	SELECT
		dbo.GetContactCommunityByContactID(CO.ContactID) AS ContactLocation,
		CO.ContactID,
		CNC2.VettingIcon,
		CNC2.VettingOutcomeName,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT TOP 1

				CASE
					WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
					THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
					ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
				END AS VettingIcon,

				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
					AND CV.ContactID = CC.ContactID
			ORDER BY CV.ContactVettingID DESC
			) CNC2
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'Class'
			AND DE.EntityID = @ClassID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetCommunityByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Community table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
--
-- Author:			John Lyons
-- Create date:	2015.09.28
-- Description:	Added the ArabicCommunityName column
--
-- Author:			Johnathan Burnham
-- Create date:	2016.09.25
-- Description:	Refactored
-- ====================================================================
CREATE PROCEDURE dbo.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nCEProjectsCount INT
	DECLARE @nCESpendValue NUMERIC(18,2)
	DECLARE @nJusticeAssetsCount INT
	DECLARE @nJusticeSpendValue NUMERIC(18,2)
	DECLARE @nJusticeStipendaryCount INT
	DECLARE @nPoliceAssetsCount INT
	DECLARE @nPoliceSpendValue NUMERIC(18,2)
	DECLARE @nPoliceStipendaryCount INT

	DECLARE @tTable TABLE (ComponentCode VARCHAR(50), ConceptNoteID INT, BudgetTotal NUMERIC(18,2))

	SELECT 
		C.ArabicCommunityName, 
		C.CommunityEngagementStatusID, 
		C.CommunityGroupID, 
		C.CommunityID, 
		C.CommunityName, 
		C.CommunitySubGroupID, 
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyPoints, 
		C.Latitude, 
		C.Longitude,
		C.PolicePresenceCategoryID, 
		C.PolicePresenceStatusID, 
		C.PoliceStationNumber, 
		C.PoliceStationStatusID, 
		C.Population, 
		C.PopulationSource, 
		C.ProgramNotes1, 
		C.ProgramNotes2, 
		C.ProgramNotes3, 
		C.ProgramNotes4, 
		C.ProgramNotes5, 
		C.ProvinceID,
		C.RiskMitigation, 
		C.StatusChangeID, 
		C.Summary, 
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CSG.CommunitySubGroupName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Community', @CommunityID) AS UpdateDateFormatted,
		ID.ImpactDecisionName,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		PPS.PolicePresenceStatusName,
		PSS.PoliceStationStatusName,
		SC.StatusChangeName,
		C.CommunityComponentStatusID,
		C.JusticeComponentStatusID,
		C.PoliceComponentStatusID,
		CS1.ComponentStatusName AS CommunityComponentStatusName,
		CS2.ComponentStatusName AS JusticeComponentStatusName,
		CS3.ComponentStatusName AS PoliceComponentStatusName,
		ISNULL(CS1.HexColor,'#000') AS CommunityComponentStatusHex,
		ISNULL(CS2.HexColor,'#000') AS JusticeComponentStatusHex,
		ISNULL(CS3.HexColor,'#000') AS PoliceComponentStatusHex
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.PolicePresenceStatus PPS ON PPS.PolicePresenceStatusID = C.PolicePresenceStatusID
		JOIN dropdown.PoliceStationStatus PSS ON PSS.PoliceStationStatusID = C.PoliceStationStatusID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
		JOIN dropdown.ComponentStatus CS1 ON CS1.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus CS2 ON CS2.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus CS3 ON CS3.ComponentStatusID = C.PoliceComponentStatusID
			AND C.CommunityID = @CommunityID

	SELECT @nJusticeStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'JusticeStipend' 
			) 
			AND C.IsActive = 1
	
	SELECT @nPoliceStipendaryCount = COUNT(C.ContactID)
	FROM dbo.Contact C
	WHERE C.CommunityID = @CommunityID
		AND EXISTS 
			(
			SELECT 1 
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
					AND CT.contacttypecode = 'PoliceStipend' 
			) 
			AND C.IsActive = 1

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'AccesstoJustice',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'AccesstoJustice'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'PoliceDeveoopment',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'PoliceDevelopment'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'CommunityEngagement',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND CNC.CommunityID = @CommunityID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'CommunityEngagement'

	SELECT @nCEProjectsCount = COUNT(P.ProjectID)
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
		JOIN dropdown.Component CT ON P.ComponentID = CT.ComponentID
			AND PC.CommunityID = @CommunityID
			AND CT.ComponentAbbreviation = 'CE'
	
	SELECT @nJusticeAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Justice'
	
	SELECT @nPoliceAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
			AND A.CommunityID = @CommunityID
			AND AT.AssetTypeCategory = 'Police'

	SELECT @nCESpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'CommunityEngagement'

	SELECT @nJusticeSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'AccesstoJustice'

	SELECT @nPoliceSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'PoliceDevelopment'

	SELECT 
		@nCEProjectsCount AS CEProjectsCount,
		@nJusticeAssetsCount AS JusticeAssetsCount,
		@nJusticeStipendaryCount AS JusticeStipendaryCount,
		@nPoliceAssetsCount AS PoliceAssetsCount,
		@nPoliceStipendaryCount AS PoliceStipendaryCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'AccesstoJustice') AS ActiveJusticeActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'CommunityEngagement') AS ActiveCEActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'PoliceDevelopment') AS ActivePoliceActivitySheetCount,
		FORMAT(@nCESpendValue, 'C', 'en-us') AS CESpendValueFormatted,
		FORMAT(@nJusticeSpendValue, 'C', 'en-us') AS JusticeSpendValueFormatted,
		FORMAT(@nPoliceSpendValue, 'C', 'en-us') AS PoliceSpendValueFormatted

END
GO
--End procedure dbo.GetCommunityByCommunityID

--Begin procedure dbo.GetCommunityFeed
EXEC Utility.DropObject 'dbo.GetCommunityFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetCommunityFeed

@PersonID INT = 0,
@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID = @CommunityID

				UNION

				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.ProvinceID = dbo.GetProvinceIDByCommunityID(@CommunityID)
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID = @CommunityID

				UNION

				SELECT 1
				FROM dbo.SpotReportProvince SRP
				WHERE SRP.ProvinceID = dbo.GetProvinceIDByCommunityID(@CommunityID)
				)

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
			AND ELD.EntityTypeCode = 'WeeklyReport'
			AND D.PhysicalFileName IS NOT NULL

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Communit Engagement Permitted Change' AS EntityTypeName,			
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS PhysicalFileName,
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
			AND CIDH.CommunityID = @CommunityID

	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetCommunityFeed

--Begin procedure dbo.GetCommunityLocations
EXEC Utility.DropObject 'dbo.GetCommunityLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to get location data from the dbo.Community table
--
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	Refactored
-- =================================================================================
CREATE PROCEDURE dbo.GetCommunityLocations
@CommunityID INT = 0,
@ProvinceID INT = 0,
@Boundary VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT 
		C.CommunityID,
		C.CommunityName,
		CAST(C.Latitude AS NUMERIC(13,8)) AS Latitude,
		CAST(C.Longitude AS NUMERIC(13,8)) AS Longitude,
		C.Population,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + LEFT(ACS.ComponentStatusName, 1) + '.png' AS Icon, 
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName, 
		ID.ImpactDecisionID,
		ACS.ComponentStatusID,
		ACS.ComponentStatusName,
		ACS.HexColor AS EngagedHexColor,
		CES.CommunityEngagementStatusName, 
		CES.CommunityEngagementStatusID,
		CCS.ComponentStatusID AS CommunityComponentStatusID,
		CCS.ComponentStatusName AS CommunityComponentStatusName,
		PCS.ComponentStatusID AS PoliceComponentStatusID,
		PCS.ComponentStatusName AS PoliceComponentStatusName,
		JCS.ComponentStatusID AS JusticeComponentStatusID,
		JCS.ComponentStatusName AS JusticeComponentStatusName
	FROM dbo.Community C 
		JOIN 
			(
			SELECT 
				C.CommunityID,
			  (
				SELECT MIN(T.ComponentStatusID)
				FROM 
					(
					VALUES (CommunityComponentStatusID),(JusticeComponentStatusID), (PoliceComponentStatusID)
					) AS T(ComponentStatusID)
				) AS ComponentStatusID
			FROM dbo.Community C
			) D ON D.CommunityID = C.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ComponentStatus CCS ON CCS.ComponentStatusID = C.CommunityComponentStatusID
		JOIN dropdown.ComponentStatus PCS ON PCS.ComponentStatusID = C.PoliceComponentStatusID
		JOIN dropdown.ComponentStatus JCS ON JCS.ComponentStatusID = C.JusticeComponentStatusID
		JOIN dropdown.ComponentStatus FCS ON FCS.ComponentStatusID = C.FIFComponentStatusID
		JOIN dropdown.ComponentStatus ACS ON ACS.ComponentStatusID = D.ComponentStatusID
			AND C.IsActive = 1
			AND 
				(
				@CommunityID = 0
					OR C.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR C.ProvinceID = @ProvinceID
				)
			AND 
				(
				ISNULL(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(C.Location) = 1
				)
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure dbo.GetCommunityLocations

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
--
-- Author:			Todd Pires
-- Create date: 2016.09.30
-- Description:	Removed the document query
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID,
		C.CommunityName,
		C.ImpactDecisionID,
		C.PolicePresenceCategoryID,
		C.Population,
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CR.ActivitiesOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.ActivityImplementationEndDate,
		dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
		CR.ActivityImplementationStartDate,
		dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
		CR.ApprovedActivityCount,
		CR.AreaManagerPersonID,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		CR.CommunityCode,
		CR.CommunityID,
		CR.CommunityRoundCivilDefenseCoverageID,
		CR.CommunityRoundGroupID,
		CR.CommunityRoundID,
		CR.CommunityRoundJusticeActivityID,
		CR.CommunityRoundOutput11StatusID,
		CR.CommunityRoundOutput12StatusID,
		CR.CommunityRoundOutput13StatusID,
		CR.CommunityRoundOutput14StatusID,
		CR.CommunityRoundPreAssessmentStatusID,
		CR.CommunityRoundRAPInfoID,
		CR.CommunityRoundRoundID,
		CR.CommunityRoundTamkeenID,
		CR.CSAP2PeerReviewMeetingDate,
		dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
		CR.CSAP2SubmittedDate,
		dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
		CR.CSAPBeginDevelopmentDate,
		dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
		CR.CSAPCommunityAnnouncementDate,
		dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
		CR.CSAPFinalizedDate,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		CR.CSAPProcessStartDate,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		CR.FemaleQuestionnairCount,
		CR.FieldFinanceOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerContactName,
		CR.FieldLogisticOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerContactName,
		CR.FieldOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficerContactName,
		CR.FinalSWOTDate,
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
		CR.InitialSCADate,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
		CR.IsActive,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.KickoffMeetingCount,
		CR.MOUDate,
		dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
		CR.NewCommunityEngagementCluster,
		CR.Preassessment,
		CR.Postassessment,
		CR.ProjectOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		CR.QuestionnairCount,
		CR.SCAFinalizedDate,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
		CR.SCAMeetingCount,
		CR.SensitizationDefineFSPRoleDate,
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
		CR.SensitizationDiscussCommMakeupDate,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
		CR.SensitizationMeetingMOUDate,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
		CR.ToRDate,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		FOHT.FieldOfficerHiredTypeID,
		FOHT.FieldOfficerHiredTypeName,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		CRES.CommunityRoundEngagementStatusID,
		CRES.CommunityRoundEngagementStatusName,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Male'  
		) AS CSWGMaleCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Female' 
		) AS CSWGFemaleCount
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.FieldOfficerHiredType FOHT ON FOHT.FieldOfficerHiredTypeID = CR.FieldOfficerHiredTypeID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
			AND CR.CommunityRoundID = @CommunityRoundID

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		C.EmployerName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,
		ISNULL(OACV12.VettingOutcomeName, 'Not Vetted') AS USVettingOutcomeName,
		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,
		ISNULL(OACV22.VettingOutcomeName, 'Not Vetted') AS UKVettingOutcomeName,
		CCSWGC.ContactCSWGClassificationID,
		CCSWGC.ContactCSWGClassificationName
	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = CV0.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C1.ContactID
					AND CRC.CommunityRoundID = @CommunityRoundID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT 
				CV12.VettingOutcomeID,
				VO1.VettingOutcomeName
			FROM dbo.ContactVetting CV12
			JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT 
				CV22.VettingOutcomeID,
				VO2.VettingOutcomeName
			FROM dbo.ContactVetting CV22
			JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
	ORDER BY 3, 1

	--Updates
	SELECT
		CRU.CommunityRoundUpdateID, 
		CRU.CommunityRoundID, 
		CRU.FOUpdates, 
		CRU.POUpdates, 
		CRU.NextStepsUpdates, 
		CRU.NextStepsProcess, 
		CRU.Risks, 
		CRU.UpdateDate,
		dbo.FormatDate(CRU.UpdateDate) AS UpdateDateFormatted
	FROM dbo.CommunityRoundUpdate CRU
	WHERE CRU.CommunityRoundID = @CommunityRoundID
	ORDER BY CRU.UpdateDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure dbo.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteBudget table
--
-- Author:			Greg Yingling
-- Update Date: 2015.05.02
-- Description:	Added BudgetSubType information
-- ================================================================================
CREATE PROCEDURE dbo.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Amendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteBudgetByConceptNoteID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
--
-- Author:			Todd Pires
-- Update Date: 2016.09.01
-- Description:	Added the ConceptNoteTypeCode field
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeCode AS ConceptNoteComponentCode,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed,
		CN.AmendedConceptNoteID,
		ACN.Title AS AmendedConceptNoteTitle,
		ABCN.ConceptNoteID AS AmendedByConceptNoteID,
		ABCN.Title AS AmendedByConceptNoteTitle,
		CN.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
		LEFT JOIN dbo.ConceptNote ACN ON ACN.ConceptNoteID = CN.AmendedConceptNoteID
		LEFT JOIN dbo.ConceptNote ABCN ON ABCN.AmendedConceptNoteID = CN.ConceptNoteID
		LEFT JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
	WHERE CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	--Budget
	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Amendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	--Communities
	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	--Contacts
	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.USVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.USVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.UKVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.UKVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID
			FROM dbo.ContactVetting CV12
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID
			FROM dbo.ContactVetting CV22
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1
				
	--Ethnicity
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	--Classes
	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	--Equipment Budget
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	--Indicators
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	--Provinces
	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	--Tasks
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	--Documents
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	--Authors
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--Amendments (deprecated)
	SELECT			
		CNA.ConceptNoteAmendmentID,
		CNA.AmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--Risks
	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	--Projects
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	--Transactions
	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID
	
	--Update Details
	SELECT
		CNU.ConceptNoteUpdateID,
		CNU.PersonID, 
		dbo.FormatPersonNameByPersonID(CNU.PersonID, 'LastFirst') AS FullNameFormatted,
		CNU.Remarks,
		CNU.UpdateDate,
		dbo.FormatDate(CNU.UpdateDate) AS UpdateDateFormatted,
		CNUT.ConceptNoteUpdateTypeID,
		CNUT.ConceptNoteUpdateTypeName,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		CNU.ActualTotalAmount
	FROM dbo.ConceptNoteUpdate CNU
		JOIN dropdown.ConceptNoteUpdateType CNUT ON CNUT.ConceptNoteUpdateTypeID = CNU.ConceptNoteUpdateTypeID
		LEFT JOIN dbo.Document D ON D.DocumentID = CNU.DocumentID
	WHERE CNU.ConceptNoteID = @ConceptNoteID
	
	--Budget Versions
	;WITH HD AS
	(
	SELECT
		CN.ConceptNoteID, 
		CN.AmendedConceptNoteID, 
		CN.Title, 
		1 AS Depth
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	UNION ALL

	SELECT 
		CN.ConceptNoteID, 
		CN.AmendedConceptNoteID, 
		CN.Title, 
		HD.Depth + 1 AS Depth
	FROM dbo.ConceptNote CN
		JOIN HD ON CN.ConceptNoteID = HD.AmendedConceptNoteID
	)

	SELECT 
		HD.ConceptNoteID,
		HD.Title,
		MAX(HD.Depth) OVER() - HD.Depth + 1 AS VersionNumber,
		(SELECT ISNULL(SUM(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost), 0)
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = HD.ConceptNoteID
		) + (SELECT ISNULL(SUM(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0)
			FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = HD.ConceptNoteID
		) AS TotalCost
	FROM HD
	ORDER BY VersionNumber
	
	--Workflow Data
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD
	
	--Workflow People
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--Workflow Event Log
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.AssetUnitID,		
		(SELECT AU.AssetUnitName FROM asset.AssetUnit AU WHERE AU.AssetUnitID = C1.AssetUnitID) AS AssetUnitName,		
		(SELECT A.AssetName FROM asset.Asset A JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID AND AU.AssetUnitID = C1.AssetUnitID) AS AssetName,
		C1.CommunityID,		
		dbo.GetCommunityNameByCommunityID(C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate((SELECT EL.CreateDateTime FROM eventlog.EventLog EL WHERE EL.EventCode = 'create' AND EL.EntityTypeCode = 'Contact' AND EL.EntityID = C1.ContactID)) AS InitialEntryDateFormatted,
		C1.IsActive,
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(SELECT CSG.CommunitySubGroupName FROM dropdown.CommunitySubGroup CSG JOIN dbo.Community C ON C. CommunitySubGroupID = CSG.CommunitySubGroupID AND C.CommunityID = C1.CommunityID) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CV.ContactVettingID, 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,

		CASE
			WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
			AND CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
	--Get Document By Contact ID
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID
	ORDER BY D.DocumentDescription

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetDashboardItemCounts
EXEC Utility.DropObject 'dbo.GetDashboardItemCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.19
-- Description:	A stored procedure to get item count data for the dashboard
--
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	Added Spot Reports
--
-- Author:			Todd Pires
-- Create date:	2016.09.30
-- Description:	Refactored
-- ========================================================================
CREATE PROCEDURE dbo.GetDashboardItemCounts

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE
		(
		ConceptNoteTypeName VARCHAR(250),
		ActivityCount INT DEFAULT 0,
		ActivityCountPercent NUMERIC(18,2) DEFAULT 0,
		CommittedBalance NUMERIC(18,2) DEFAULT 0,
		CommittedBalancePercent NUMERIC(18,2) DEFAULT 0,
		PendingCommitments NUMERIC(18,2) DEFAULT 0,
		PendingCommitmentsPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanAllocation NUMERIC(18,2) DEFAULT 0,
		WorkplanAllocationPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanCommitted NUMERIC(18,2) DEFAULT 0,
		WorkplanCommittedPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanDisbursed NUMERIC(18,2) DEFAULT 0,
		WorkplanDisbursedPercent NUMERIC(18,2) DEFAULT 0,
		WorkplanUncommitted NUMERIC(18,2) DEFAULT 0,
		WorkplanUncommittedPercent NUMERIC(18,2) DEFAULT 0
		)

	DECLARE @tTable2 TABLE (ItemCount INT  DEFAULT 0, ItemName VARCHAR(250))

	INSERT INTO @tTable1 (ConceptNoteTypeName) SELECT CNT.ConceptNoteTypeName FROM dropdown.ConceptNoteType CNT WHERE CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
	INSERT INTO @tTable1 (ConceptNoteTypeName) VALUES ('Other'), ('Total')

	--Component Engagement Counts
	SELECT 
		PVT.Component,
		ISNULL(PVT.No, 0) AS No,
		ISNULL(PVT.Pending, 0) AS Pending,
		ISNULL(PVT.Yes, 0) AS Yes
	FROM
		(
		SELECT
			'CommunityEngagement' AS Component,
			COUNT(C.CommunityComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.CommunityComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		UNION

		SELECT
			'JusticeEngagement' AS Component,
			COUNT(C.JusticeComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.JusticeComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		UNION

		SELECT
			'PoliceEngagement' AS Component,
			COUNT(C.PoliceComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM dbo.Community C 
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = C.PoliceComponentStatusID 
				AND C.IsActive = 1
				AND CS.ComponentStatusID > 0
		GROUP BY CS.ComponentStatusName

		) AS D
	PIVOT
		(
		MAX(D.ItemCount)
		FOR D.ItemName IN
			(
			No,Pending,Yes
			)
	) AS PVT
	ORDER BY PVT.Component

	--Funds Data
	--ActivityCount
	UPDATE T1
	SET T1.ActivityCount = ISNULL(E.ActivityCount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				COUNT(D.ConceptNoteID) AS ActivityCount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ConceptNoteID,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNote CN
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) NOT IN ('Amended','Cancelled') 
						AND WP.IsForDashboard = 1
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--PendingCommitments
	UPDATE T1
	SET T1.PendingCommitments = T1.PendingCommitments + E.ConceptNoteBudgetTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteBudgetTotal) AS ConceptNoteBudgetTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS ConceptNoteBudgetTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteBudget CNB
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNB.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Development')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.PendingCommitments = T1.PendingCommitments + E.ConceptNoteEquimentTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteEquimentTotal) AS ConceptNoteEquimentTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue AS ConceptNoteEquimentTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Development')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanAllocation
	UPDATE T1
	SET T1.WorkplanAllocation = E.WorkplanAllocation
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.CurrentUSDAllocation) AS WorkplanAllocation,
				ConceptNoteTypeName
			FROM
				(
				SELECT 
					WPA.CurrentUSDAllocation,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM workplan.WorkplanActivity WPA
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanCommitted
	UPDATE T1
	SET T1.WorkplanCommitted = T1.WorkplanCommitted + E.ConceptNoteBudgetTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteBudgetTotal) AS ConceptNoteBudgetTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS ConceptNoteBudgetTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteBudget CNB
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNB.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.WorkplanCommitted = T1.WorkplanCommitted + E.ConceptNoteEquimentTotal
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteEquimentTotal) AS ConceptNoteEquimentTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue AS ConceptNoteEquimentTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanDisbursed
	UPDATE T1
	SET T1.WorkplanDisbursed = T1.WorkplanDisbursed + ISNULL(E.ConceptNoteFinanceTotal, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteFinanceTotal) AS ConceptNoteFinanceTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNF.drAmt - CNF.CrAmt AS ConceptNoteFinanceTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteFinance CNF
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.WorkplanDisbursed = T1.WorkplanDisbursed + ISNULL(E.ActualTotalAmount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ActualTotalAmount) AS ActualTotalAmount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ActualTotalAmount,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNote CN
						JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
							AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--CommittedBalance
	UPDATE T1
	SET T1.CommittedBalance = T1.CommittedBalance + T1.WorkplanCommitted
	FROM @tTable1 T1

	UPDATE T1
	SET T1.CommittedBalance = T1.CommittedBalance - ISNULL(E.ConceptNoteFinanceTotal, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ConceptNoteFinanceTotal) AS ConceptNoteFinanceTotal,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CNF.drAmt - CNF.CrAmt AS ConceptNoteFinanceTotal,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNoteFinance CNF
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
					JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
						AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	UPDATE T1
	SET T1.CommittedBalance = T1.CommittedBalance - ISNULL(E.ActualTotalAmount, 0)
	FROM @tTable1 T1
		JOIN
			(
			SELECT
				SUM(D.ActualTotalAmount) AS ActualTotalAmount,
				ConceptNoteTypeName
			FROM
				(
				SELECT
					CN.ActualTotalAmount,

					CASE
						WHEN CNT.ConceptNoteTypeCode IN ('AccesstoJustice','CommunityEngagement','PoliceDevelopment')
						THEN CNT.ConceptNoteTypeName
						ELSE 'Other'
					END AS ConceptNoteTypeName

				FROM dbo.ConceptNote CN
						JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
					JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
					JOIN workplan.Workplan WP ON WP.WorkplanID = WPA.WorkplanID
						AND WP.IsForDashboard = 1
							AND workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				) D
			GROUP BY D.ConceptNoteTypeName
			) E ON E.ConceptNoteTypeName = T1.ConceptNoteTypeName

	--WorkplanUncommitted
	UPDATE T1
	SET T1.WorkplanUncommitted = T1.WorkplanUncommitted + T1.WorkplanAllocation
	FROM @tTable1 T1

	UPDATE T1
	SET T1.WorkplanUncommitted = T1.WorkplanUncommitted - T1.WorkplanCommitted
	FROM @tTable1 T1

	--NegativeDataProtection
	UPDATE T1
	SET
		T1.CommittedBalance = CASE WHEN T1.CommittedBalance < 0 THEN 0 ELSE T1.CommittedBalance END,
		T1.WorkplanAllocation = CASE WHEN T1.WorkplanAllocation < 0 THEN 0 ELSE T1.WorkplanAllocation END,
		T1.WorkplanCommitted = CASE WHEN T1.WorkplanCommitted < 0 THEN 0 ELSE T1.WorkplanCommitted END,
		T1.WorkplanDisbursed = CASE WHEN T1.WorkplanDisbursed < 0 THEN 0 ELSE T1.WorkplanDisbursed END,
		T1.WorkplanUncommitted = CASE WHEN T1.WorkplanUncommitted < 0 THEN 0 ELSE T1.WorkplanUncommitted END
	FROM @tTable1 T1

	--ActivityCountPercent
	IF (SELECT SUM(T2.ActivityCount) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.ActivityCountPercent = T1.ActivityCount / (SELECT CAST(SUM(T2.ActivityCount) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--ActivityCountTotal
	UPDATE T1
	SET T1.ActivityCount = (SELECT SUM(T2.ActivityCount) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--CommittedBalancePercent
	IF (SELECT SUM(T2.CommittedBalance) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.CommittedBalancePercent = T1.CommittedBalance / (SELECT CAST(SUM(T2.CommittedBalance) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--CommittedBalanceTotal
	UPDATE T1
	SET T1.CommittedBalance = (SELECT SUM(T2.CommittedBalance) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--PendingCommitmentsPercent
	IF (SELECT SUM(T2.PendingCommitments) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.PendingCommitmentsPercent = T1.PendingCommitments / (SELECT CAST(SUM(T2.PendingCommitments) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--PendingCommitmentsTotal
	UPDATE T1
	SET T1.PendingCommitments = (SELECT SUM(T2.PendingCommitments) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanAllocationPercent
	IF (SELECT SUM(T2.WorkplanAllocation) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanAllocationPercent = T1.WorkplanAllocation / (SELECT CAST(SUM(T2.WorkplanAllocation) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF
					
	--WorkplanAllocationTotal
	UPDATE T1
	SET T1.WorkplanAllocation = (SELECT SUM(T2.WorkplanAllocation) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanCommittedPercent
	IF (SELECT SUM(T2.WorkplanCommitted) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanCommittedPercent = T1.WorkplanCommitted / (SELECT CAST(SUM(T2.WorkplanCommitted) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--WorkplanCommittedTotal
	UPDATE T1
	SET T1.WorkplanCommitted = (SELECT SUM(T2.WorkplanCommitted) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanDisbursedPercent
	IF (SELECT SUM(T2.WorkplanDisbursed) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanDisbursedPercent = T1.WorkplanDisbursed / (SELECT CAST(SUM(T2.WorkplanDisbursed) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--WorkplanDisbursedTotal
	UPDATE T1
	SET T1.WorkplanDisbursed = (SELECT SUM(T2.WorkplanDisbursed) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	--WorkplanUncommittedPercent
	IF (SELECT SUM(T2.WorkplanUncommitted) FROM @tTable1 T2) > 0
		BEGIN

		UPDATE T1
		SET T1.WorkplanUncommittedPercent = T1.WorkplanUncommitted / (SELECT CAST(SUM(T2.WorkplanUncommitted) AS NUMERIC(18,2)) FROM @tTable1 T2) * 100
		FROM @tTable1 T1
		WHERE T1.ConceptNoteTypeName <> 'Total'

		END
	--ENDIF

	--WorkplanUncommittedTotal
	UPDATE T1
	SET T1.WorkplanUncommitted = (SELECT SUM(T2.WorkplanUncommitted) FROM @tTable1 T2)
	FROM @tTable1 T1
	WHERE T1.ConceptNoteTypeName = 'Total'

	SELECT 
		T1.ConceptNoteTypeName,
		T1.ActivityCount,
		T1.ActivityCountPercent,
		T1.CommittedBalance,
		T1.CommittedBalancePercent,
		T1.PendingCommitments,
		T1.PendingCommitmentsPercent,
		T1.WorkplanAllocation,
		T1.WorkplanAllocationPercent,
		T1.WorkplanCommitted,
		T1.WorkplanCommittedPercent,
		T1.WorkplanDisbursed,
		T1.WorkplanDisbursedPercent,
		T1.WorkplanUncommitted,
		T1.WorkplanUncommittedPercent
	FROM @tTable1 T1

	--Overall Engagement Counts
	INSERT INTO @tTable2 (ItemName) SELECT CS.ComponentStatusName FROM dropdown.ComponentStatus CS WHERE CS.ComponentStatusID > 0 AND CS.IsActive = 1

	;
	WITH OEC AS
		(
		SELECT
			COUNT(D.ComponentStatusID) AS ItemCount,
			CS.ComponentStatusName AS ItemName
		FROM
			(
			SELECT 
				C.CommunityID,
				(
				SELECT MIN(T.ComponentStatusID)
				FROM 
					(
					VALUES (CommunityComponentStatusID),(JusticeComponentStatusID),(PoliceComponentStatusID)
					) AS T(ComponentStatusID)
				) AS ComponentStatusID
			FROM dbo.Community C
			WHERE C.IsActive = 1
			) D
			JOIN dropdown.ComponentStatus CS ON CS.ComponentStatusID = D.ComponentStatusID
				AND CS.ComponentStatusID > 0
		GROUP BY D.ComponentStatusID, CS.ComponentStatusName
		)

	UPDATE T2
	SET T2.ItemCount = OEC.ItemCount
	FROM @tTable2 T2
		JOIN OEC ON OEC.ItemName = T2.ItemName

	SELECT 
		T2.ItemName,
		T2.ItemCount
	FROM @tTable2 T2
	ORDER BY T2.ItemName

	--Permitted Engagement Counts
	DELETE FROM @tTable2
	INSERT INTO @tTable2 (ItemName) SELECT ID.ImpactDecisionName FROM dropdown.ImpactDecision ID WHERE ID.ImpactDecisionID > 0 AND ID.IsActive = 1

	;
	WITH PEC AS
		(
		SELECT
			COUNT(ID.ImpactDecisionID) AS ItemCount,
			ID.ImpactDecisionName AS ItemName
		FROM dbo.Community C
			JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
				AND C.IsActive = 1
				AND ID.IsActive = 1
		GROUP BY ID.ImpactDecisionID, ID.ImpactDecisionName
		)

	UPDATE T2
	SET T2.ItemCount = PEC.ItemCount
	FROM @tTable2 T2
		JOIN PEC ON PEC.ItemName = T2.ItemName

	SELECT 
		T2.ItemName,
		T2.ItemCount
	FROM @tTable2 T2
	ORDER BY T2.ItemName

END
GO
--End procedure dbo.GetDashboardItemCounts

--Begin procedure dbo.GetDocumentByDocumentID
EXEC Utility.DropObject 'dbo.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	A stored procedure to get data from the dbo.Document table
--
-- Author:			Todd Pires
-- Create date:	2016.01.25
-- Description:	Implemented the IsForDonorPortal field
--
-- Author:			Brandon Green
-- Create date:	2016.08.16
-- Description:	Implemented the DisplayInCommunityNewsFeed field
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentByDocumentID

@DocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentDate, 
		dbo.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle, 
		D.DocumentTypeID, 
		D.IsForDonorPortal,
		D.PhysicalFileExtension, 
		D.PhysicalFileName, 
		D.PhysicalFilePath, 
		D.PhysicalFileSize,
		D.DisplayInCommunityNewsFeed
  FROM dbo.Document D
	WHERE D.DocumentID = @DocumentID

	SELECT 
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.DocumentEntity DE ON DE.EntityID = P.ProvinceID
			AND DE.EntityTypeCode = 'Province'
			AND DE.DocumentID = @DocumentID

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.Community C
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dbo.DocumentEntity DE ON DE.EntityID = C.CommunityID
			AND DE.EntityTypeCode = 'Community'
			AND DE.DocumentID = @DocumentID
	
END
GO
--End procedure dbo.GetDocumentByDocumentID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.27
-- Description:	Bug fixes
--
-- Author:			Todd Pires
-- Create Date: 2015.09.27
-- Description:	Added Recommendations
--
-- Author:			Todd Pires
-- Create Date: 2016.02.11
-- Description:	Added Program Report
--
-- Author:			Todd Pires
-- Create Date: 2016.03.22
-- Description:	Implemented support for the new workflow system
--
-- Author:			Brandon Green
-- Create Date: 2016.08.15
-- Description:	Implemented support incident reports, atmospheric reports and ARAP docs
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
-- ====================================================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN 'fa fa-fw fa-lightbulb-o'
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN 'fa fa-fw fa-question-circle'
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN 'fa fa-fw fa-calendar'
			WHEN ET.EntityTypeCode = 'Incident'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'Document'
			THEN 'fa fa-fw fa-file'
		END AS Icon,

		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN OAR.RecommendationName
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			WHEN D.EntityTypeCode = 'Incident'
			THEN OAI.IncidentName
			WHEN D.EntityTypeCode = 'Document'
			THEN OAD.DocumentTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		CASE
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN OAWR.PhysicalFileName
			WHEN ET.EntityTypeCode = 'Document'
			THEN OAD.PhysicalFileName
			ELSE NULL
		END AS PhysicalFileName,

		D.EntityID,
		D.UpdateDate,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('Recommendation','RequestForInformation','SpotReport','WeeklyReport','Incident','Document')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'Recommendation' AND permissionable.HasPermission('Recommendation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'Document')
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				R.RecommendationName,
				R.IsActive
			FROM recommendation.Recommendation R
			WHERE R.RecommendationID = D.EntityID
					AND D.EntityTypeCode = 'Recommendation'
			) OAR
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle,
				workflow.GetWorkflowStepNumber(D.EntityTypeCode, D.EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(D.EntityTypeCode, D.EntityID) AS WorkflowStepCount
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		OUTER APPLY
			(
			SELECT
				INC.IncidentName
			FROM dbo.Incident INC
			WHERE INC.IncidentID = D.EntityID
				AND D.EntityTypeCode = 'Incident'
			) OAI
		OUTER APPLY
			(
			SELECT
				CASE
					WHEN permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
					THEN DOC.DocumentTitle
					ELSE NULL
				END AS DocumentTitle,

				CASE
					WHEN permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
					THEN DOC.PhysicalFileName
					ELSE NULL
				END AS PhysicalFileName
			FROM dbo.Document DOC
			JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = DOC.DocumentTypeID
				AND DOC.DocumentTypeID > 0 
				AND DT.IsActive = 1
			WHERE DOC.DocumentID = D.EntityID
				AND DOC.DisplayInCommunityNewsFeed = 1 
				AND D.EntityTypeCode = 'Document'
			) OAD
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
			AND
				(
				D.EntityTypeCode <> 'Recommendation'
					OR OAR.IsActive = 1
				)
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
			AND
				(
				D.EntityTypeCode <> 'SpotReport'
					OR OASR.WorkflowStepNumber > OASR.WorkflowStepCount
				)
			AND
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)
			AND
				(
				D.EntityTypeCode <> 'Incident'
					OR OAI.IncidentName IS NOT NULL
				)
			AND
				(
				D.EntityTypeCode <> 'Document'
					OR OAD.DocumentTitle IS NOT NULL
				)

	UNION
	
	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		'fa-newspaper-o' AS Icon,
		ET.EntityTypeName,
		D.DocumentTitle AS Title,
		D.PhysicalFileName,
		0 AS EntityID,
		D.DocumentDate AS UpdateDate,
		dbo.FormatDate(D.DocumentDate) AS UpdateDateFormatted
	FROM dbo.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DT.DocumentTypeCode = 'ProgramReport'
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = DT.DocumentTypeCode
			AND D.DocumentDate >= DATEADD(d, -14, getDate())
			AND permissionable.HasPermission('ProgramReport.View', @PersonID) = 1

	UNION

	SELECT
		'EngagementPermitted' AS EntityTypeCode,
		LOWER('community') AS Controller,
		'fa fa-fw fa-bolt' AS Icon,
		'Communit Engagement Permitted Change' AS EntityTypeName,			
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title,
		NULL AS PhysicalFileName,
		CIDH.CommunityID AS EntityID,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID

	ORDER BY 8 DESC, 1, 7
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dbo.GetEntityDocuments
EXEC Utility.DropObject 'dbo.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.30
-- Description:	A stored procedure to get records from the dbo.DocumentEntity table
-- ================================================================================
CREATE PROCEDURE dbo.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	SELECT
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.PhysicalFileName,
		D.PhysicalFileSize,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), '0') AS ThumbnailLength,

		CASE
			WHEN ISNULL(DATALENGTH(D.Thumbnail), '0') > 0
			THEN ISNULL(DATALENGTH(D.Thumbnail), '0')
			ELSE D.PhysicalFileSize
		END AS FileSize,

		CASE
			WHEN D.DocumentDescription IS NOT NULL AND LEN(RTRIM(D.DocumentDescription)) > 0
			THEN D.DocumentDescription + ' (' + D.DocumentName + ')'
			ELSE D.DocumentName
		END AS DisplayName

	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID
	ORDER BY D.DocumentDescription, D.DocumentName, D.DocumentID

END
GO
--End procedure dbo.GetEntityDocuments

--Begin procedure dbo.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.19
-- Description:	A stored procedure to data from the dbo.Incident table
-- ===================================================================
CREATE PROCEDURE dbo.GetIncidentByIncidentID

@IncidentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentDate,
		I.IncidentName,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		SR.SourceReliabilityID,
		SR.SourceReliabilityName,
		IV.InformationValidityID,
		IV.InformationValidityName,
		I.Summary,
		I.KeyPoints,
		I.Implications,
		I.RiskMitigation,
		I.Latitude,
		I.Longitude,
		I.Location.STAsText() AS Location
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.SourceReliability SR ON SR.SourceReliabilityID = I.SourceReliabilityID
		JOIN dropdown.InformationValidity IV ON IV.InformationValidityID = I.InformationValidityID
			AND I.IncidentID = @IncidentID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.IncidentCommunity IC ON IC.CommunityID = C.CommunityID
			AND IC.IncidentID = @IncidentID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.IncidentProvince IP ON IP.ProvinceID = P.ProvinceID
			AND IP.IncidentID = @IncidentID
		
END
GO
--End procedure dbo.GetIncidentByIncidentID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not permissionable.PersonPermissionable
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for IsSuperAdministrator menu items
-- =====================================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nPadLength INT
	DECLARE @tPersonPermissionable TABLE (PermissionableLineage VARCHAR(MAX))	

	SELECT @nPadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	INSERT INTO @tPersonPermissionable
		(PermissionableLineage)
	SELECT
		PP.PermissionableLineage
	FROM permissionable.PersonPermissionable PP
		JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PP.PermissionableLineage
		JOIN dbo.Person P2 ON P2.PersonID = PP.PersonID
			AND P2.PersonID = @PersonID
			AND 
				(
				P1.IsSuperAdministrator = 0
					OR P2.IsSuperAdministrator = 1
				)
			
	UNION

	SELECT 
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM @tPersonPermissionable TPP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND MIPL.PermissionableLineage = TPP.PermissionableLineage
						)
					)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @nPadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM @tPersonPermissionable TPP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND MIPL.PermissionableLineage = TPP.PermissionableLineage
							)
						)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetProvinceByProvinceID
EXEC Utility.DropObject 'dbo.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.14
-- Description:	A stored procedure to data from the dbo.Province table
--
-- Author:			Todd Pires
-- Create date:	2015.06.05
-- Description:	Added the UpdateDateFormatted column
--
-- Author:			Jonathan Burnham
-- Create date:	2016.09.27
-- Description:	Refactored
-- ===================================================================
CREATE PROCEDURE dbo.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @nCEProjectsCount INT
	DECLARE @nCESpendValue NUMERIC(18,2)
	DECLARE @nJusticeAssetsCount INT
	DECLARE @nJusticeSpendValue NUMERIC(18,2)
	DECLARE @nJusticeStipendaryCount INT
	DECLARE @nPoliceAssetsCount INT
	DECLARE @nPoliceSpendValue NUMERIC(18,2)
	DECLARE @nPoliceStipendaryCount INT

	DECLARE @tTable TABLE (ComponentCode VARCHAR(50), ConceptNoteID INT, BudgetTotal NUMERIC(18,2))

	SELECT 
		ID.ImpactDecisionName,
		eventlog.GetLastUpdateDateByEntityTypeCodeAndEntityID('Province', @ProvinceID) AS UpdateDateFormatted,
		P.ArabicProvinceName,
		P.ImpactDecisionID,
		P.Implications,
		P.KeyPoints,
		P.ProgramNotes1,
		P.ProgramNotes2,
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID,
		P.Summary,
		SC.StatusChangeName
	FROM dbo.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

	SELECT
		CES.CommunityEngagementStatusName,
		ISNULL(D.CommunityCount, 0) AS CommunityCount
	FROM dropdown.CommunityEngagementStatus CES
	OUTER APPLY
		(
		SELECT
			COUNT(C.CommunityID) AS CommunityCount,
			C.CommunityEngagementStatusID
		FROM dbo.Community C
		WHERE C.CommunityEngagementStatusID = CES.CommunityEngagementStatusID
			AND C.ProvinceID = @ProvinceID
		GROUP BY C.CommunityEngagementStatusID
		) D 
	WHERE CES.CommunityEngagementStatusID > 0
	ORDER BY CES.DisplayOrder, CES.CommunityEngagementStatusName, D.CommunityCount
	
	SELECT @nJusticeStipendaryCount = COUNT(C1.ContactID)
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C2.ProvinceID = @ProvinceID
			AND EXISTS 
				(
				SELECT 1 
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C1.ContactID
						AND CT.contacttypecode = 'JusticeStipend' 
				) 
			AND C1.IsActive = 1
	
	SELECT @nPoliceStipendaryCount = COUNT(C1.ContactID)
	FROM dbo.Contact C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C2.ProvinceID = @ProvinceID
			AND EXISTS 
				(
				SELECT 1 
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C1.ContactID
						AND CT.contacttypecode = 'PoliceStipend' 
				) 
			AND C1.IsActive = 1
	
	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'AccesstoJustice',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.Community C 
					JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
						AND C.CommunityID = CNC.CommunityID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM dbo.Province P
					JOIN dbo.ConceptNoteProvince CNP ON CNP.ConceptNoteID = CN.ConceptNoteID
						AND CNP.ProvinceID = @ProvinceID
				)
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'AccesstoJustice'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'PoliceDeveoopment',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.Community C 
					JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
						AND C.CommunityID = CNC.CommunityID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM dbo.Province P
					JOIN dbo.ConceptNoteProvince CNP ON CNP.ConceptNoteID = CN.ConceptNoteID
						AND CNP.ProvinceID = @ProvinceID
				)
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'PoliceDevelopment'

	INSERT INTO @tTable
		(ComponentCode, ConceptNoteID, BudgetTotal)
	SELECT 
		'CommunityEngagement',
		CN.ConceptNoteID,
		dbo.GetConceptNoteTotalCost(CN.ConceptNoteID)
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.Community C 
					JOIN dbo.ConceptNoteCommunity CNC ON CNC.ConceptNoteID = CN.ConceptNoteID
						AND C.CommunityID = CNC.CommunityID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM dbo.Province P
					JOIN dbo.ConceptNoteProvince CNP ON CNP.ConceptNoteID = CN.ConceptNoteID
						AND CNP.ProvinceID = @ProvinceID
				)
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND CN.ConceptNoteTypeCode = 'ActivitySheet'
			AND CNT.ConceptNoteTypeCode = 'CommunityEngagement'

	SELECT @nCEProjectsCount = COUNT(P.ProjectID)
	FROM project.Project P
		JOIN dropdown.Component CT ON P.ComponentID = CT.ComponentID
			AND CT.ComponentAbbreviation = 'CE'
			AND EXISTS
				(
				SELECT 1
				FROM project.ProjectCommunity PC
					JOIN dbo.Community C ON C.CommunityID = PC.CommunityID
						AND PC.ProjectID = P.ProjectID
						AND C.ProvinceID = @ProvinceID

				UNION

				SELECT 1
				FROM project.ProjectProvince PP
				WHERE PP.ProjectID = P.ProjectID
					AND PP.ProvinceID = @ProvinceID
				)	

	SELECT @nJusticeAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
			AND C.ProvinceID = @ProvinceID
			AND AT.AssetTypeCategory = 'Justice'
	
	SELECT @nPoliceAssetsCount = COUNT(A.AssetID)
	FROM asset.Asset A
		JOIN dropdown.AssetType AT ON A.AssetTypeID = AT.AssetTypeID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
			AND C.ProvinceID = @ProvinceID
			AND AT.AssetTypeCategory = 'Police'

	SELECT @nCESpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'CommunityEngagement'

	SELECT @nJusticeSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'AccesstoJustice'

	SELECT @nPoliceSpendValue = SUM(T.BudgetTotal) 
	FROM @tTable T
	WHERE T.ComponentCode = 'PoliceDevelopment'

	SELECT 
		@nCEProjectsCount AS CEProjectsCount,
		@nJusticeAssetsCount AS JusticeAssetsCount,
		@nJusticeStipendaryCount AS JusticeStipendaryCount,
		@nPoliceAssetsCount AS PoliceAssetsCount,
		@nPoliceStipendaryCount AS PoliceStipendaryCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'AccesstoJustice') AS ActiveJusticeActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'CommunityEngagement') AS ActiveCEActivitySheetCount,
		(SELECT COUNT(T.ConceptNoteID) FROM @tTable T WHERE T.ComponentCode = 'PoliceDevelopment') AS ActivePoliceActivitySheetCount,
		FORMAT(@nCESpendValue, 'C', 'en-us') AS CESpendValueFormatted,
		FORMAT(@nJusticeSpendValue, 'C', 'en-us') AS JusticeSpendValueFormatted,
		FORMAT(@nPoliceSpendValue, 'C', 'en-us') AS PoliceSpendValueFormatted

END
GO
--End procedure dbo.GetProvinceByProvinceID

--Begin procedure dbo.GetProvinceFeed
EXEC Utility.DropObject 'dbo.GetProvinceFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.25
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetProvinceFeed

@PersonID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

				UNION

				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.ProvinceID = @ProvinceID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS PhysicalFileName,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

				UNION

				SELECT 1
				FROM dbo.SpotReportProvince SRP
				WHERE SRP.ProvinceID = @ProvinceID
				)

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.PhysicalFileName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN dbo.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
			AND ELD.EntityTypeCode = 'WeeklyReport'
			AND D.PhysicalFileName IS NOT NULL

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityImpactDecisionHistoryID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Engagement Permitted' AS EntityTypeName,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('EngagementPermitted') AS Controller,
		NULL AS PhysicalFileName,
		C.CommunityName + 'Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
	WHERE P.ProvinceID = @ProvinceID


	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetProvinceFeed

--Begin procedure dbo.GetServerSetupDataByServerSetupID
EXEC Utility.DropObject 'dbo.GetServerSetupDataByServerSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.12
-- Description:	A stored procedure to get data from the dbo.ServerSetup table
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
-- ==========================================================================
CREATE PROCEDURE dbo.GetServerSetupDataByServerSetupID

@ServerSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.ServerSetupID, 
		SS.ServerSetupKey, 
		SS.ServerSetupValue 
	FROM dbo.ServerSetup SS
	WHERE SS.ServerSetupID = @ServerSetupID

END
GO
--End procedure dbo.GetServerSetupDataByServerSetupID

--Begin procedure dbo.GetServerSetupValuesByServerSetupKey
EXEC Utility.DropObject 'dbo.GetServerSetupValuesByServerSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.ServerSetup table
-- ==========================================================================
CREATE PROCEDURE dbo.GetServerSetupValuesByServerSetupKey

@ServerSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.ServerSetupValue 
	FROM dbo.ServerSetup SS 
	WHERE SS.ServerSetupKey = @ServerSetupKey
	ORDER BY SS.ServerSetupID

END
GO
--End procedure dbo.GetServerSetupValuesByServerSetupKey

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:			Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
--
-- Author:			Todd Pires
-- Update date:	2016.08.30
-- Description:	Removed the SpotReport documents recordset
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('SpotReport', @SpotReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('SpotReport', @SpotReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @SpotReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END 
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dbo.ReconcileContactStipendPayment
EXEC Utility.DropObject 'dbo.ReconcileContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ReconcileContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET 
		CSP.StipendPaidDate = getDate()
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = @StipendTypeCode
		AND CSP.StipendPaidDate IS NULL

	UPDATE C
	SET C.StipendArrears = C.StipendArrears + CSP.StipendAmountAuthorized - CSP.StipendAmountPaid
	FROM dbo.Contact C
		JOIN dbo.ContactStipendPayment CSP ON CSP.ContactID = C.ContactID
			AND CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = @StipendTypeCode
			AND CSP.StipendAmountAuthorized > CSP.StipendAmountPaid

	IF @StipendTypeCode = 'JusticeStipend'
		BEGIN

		UPDATE AUE
		SET 
			AUE.ExpensePaidDate = getDate()
		FROM asset.AssetUnitExpense AUE
		WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
			AND AUE.ProvinceID = @ProvinceID
			AND AUE.ExpensePaidDate IS NULL

		END
	--ENDIF

END
GO
--End procedure dbo.ReconcileContactStipendPayment

--Begin procedure dbo.SaveEntityDocuments
EXEC Utility.DropObject 'dbo.SaveDocumentEntityRecords'
EXEC Utility.DropObject 'dbo.SaveEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2016.08.30
-- Description:	A stored procedure to manage records in the dbo.DocumentEntity table
-- =================================================================================
CREATE PROCEDURE dbo.SaveEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (DocumentID INT NOT NULL PRIMARY KEY)
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	INSERT INTO @tTable
		(DocumentID)
	SELECT D.DocumentID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND DE.EntityID = @EntityID

	DELETE DE
	FROM dbo.DocumentEntity DE
	WHERE DE.EntityTypeCode = @EntityTypeCode
		AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
		AND DE.EntityID = @EntityID

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO dbo.DocumentEntity
			(DocumentID, EntityTypeCode, EntityTypeSubCode, EntityID)
		OUTPUT INSERTED.DocumentID INTO @tOutput
		SELECT
			CAST(LTT.ListItem AS INT),
			@EntityTypeCode,
			@EntityTypeSubCode,
			@EntityID
		FROM dbo.ListToTable(@DocumentIDList, ',') LTT

		DELETE T
		FROM @tTable T
			JOIN @tOutput O ON O.DocumentID = T.DocumentID

		END
	--ENDIF

	DELETE D
	FROM dbo.Document D
		JOIN @tTable T ON T.DocumentID = D.DocumentID
			AND NOT EXISTS
				(
				SELECT 1
				FROM dbo.DocumentEntity DE
				WHERE DE.DocumentID = D.DocumentID
				)

END
GO
--End procedure dbo.SaveEntityDocuments

--Begin procedure dbo.SetContactAssetUnit
EXEC Utility.DropObject 'dbo.SetContactAssetUnit'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.16
-- Description:	A stored procedure to update an assetunitid in a contact record
-- ============================================================================
CREATE PROCEDURE dbo.SetContactAssetUnit

@ContactID INT,
@AssetUnitID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE C
	SET C.AssetUnitID = @AssetUnitID
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID
		
END
GO
--End procedure dbo.SetContactAssetUnit

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Added password expiration support
--
-- Author:			Todd Pires
-- Create date:	2015.08.31
-- Description:	Implemented the FormatPersonNameByPersonID function
--
-- Author:			Todd Pires
-- Create date:	2016.05.08
-- Description:	Added the IsSuperAdministrator bit
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cPhone =P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
		JOIN permissionable.Permissionable P1 ON P1.PermissionableLineage = PP.PermissionableLineage
		JOIN dbo.Person P2 ON P2.PersonID = PP.PersonID
			AND P2.PersonID = @nPersonID
			AND 
				(
				P1.IsSuperAdministrator = 0
					OR P2.IsSuperAdministrator = 1
				)
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin
