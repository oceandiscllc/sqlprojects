USE AJACS2
GO

EXEC utility.AddSchema asset
EXEC utility.AddSchema mediareport
EXEC utility.AddSchema workplan
EXEC utility.AddSchema zone
GO

EXEC utility.DropObject 'dbo.fnGetServerSetupValuesByServerSetupKey'
EXEC utility.DropObject 'dbo.GetDateByReferenceDateDayAbbreviationAndOffset'
GO

IF EXISTS (SELECT 1 FROM sys.Tables T WHERE T.Name = 'ConceptNoteAmmendment')
	BEGIN

	EXEC sp_rename 'dbo.ConceptNoteAmmendment', 'ConceptNoteAmendment'

	EXEC sp_rename 'dbo.ConceptNoteAmendment.ConceptNoteAmmendmentID', 'ConceptNoteAmendmentID', 'COLUMN';  
	EXEC sp_rename 'dbo.ConceptNoteAmendment.AmmendmentNumber', 'AmendmentNumber', 'COLUMN';

	EXEC sp_rename 'dbo.ConceptNoteBudget.Ammendments', 'Amendments', 'COLUMN';

	End
--ENDIF
GO

/*
--Begin synonyms
EXEC utility.DropObject 'dbo.ServerSetup'
EXEC utility.DropObject 'dbo.udf_hashBytes'
EXEC utility.DropObject 'syslog.ApplicationErrorLog'
EXEC utility.DropObject 'syslog.DuoWebTwoFactorLog'
GO

CREATE SYNONYM dbo.ServerSetup FOR AJACSUtility2.dbo.ServerSetup
CREATE SYNONYM dbo.udf_hashBytes FOR AJACSUtility2.dbo.udf_hashBytes
CREATE SYNONYM syslog.ApplicationErrorLog FOR AJACSUtility2.syslog.ApplicationErrorLog
CREATE SYNONYM syslog.DuoWebTwoFactorLog FOR AJACSUtility2.syslog.DuoWebTwoFactorLog
GO
--End synonyms
*/