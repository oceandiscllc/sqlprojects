USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
		
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'dbo.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.31
-- Description:	A stored procedure to data from the dbo.EmailTemplate table
-- ========================================================================
CREATE PROCEDURE dbo.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET1.EmailTemplateID, 
		ET1.EntityTypeCode, 
		ET1.WorkflowActionCode, 
		ET1.EmailText,
		ET2.EntityTypeName,
		WA.WorkflowActionName
	FROM dbo.EmailTemplate ET1
		JOIN dbo.EntityType ET2 ON ET2.EntityTypeCode = ET1.EntityTypeCode
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionCode = ET1.WorkflowActionCode
			AND 
				(
				WA.WorkflowActionCode <> 'IncrementWorkflow'
					OR WA.WorkflowActionName = 'Approve'
				)
			AND ET1.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM dbo.EmailTemplateField ETF
		JOIN dbo.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure dbo.GetEmailTemplateByEmailTemplateID

--Begin procedure dbo.GetEmailTemplateFieldsByEntityTypeCode
EXEC utility.DropObject 'dbo.GetEmailTemplateFieldsByEntityTypeCode'
GO

-- =================================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to get data from the dbo.EmailTemplateField table
-- =================================================================================
CREATE PROCEDURE dbo.GetEmailTemplateFieldsByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ETF.PlaceHolderText
	FROM dbo.EmailTemplateField ETF
	WHERE ETF.EntityTypeCode = @EntityTypeCode
	ORDER BY ETF.PlaceHolderText
	
END
GO
--End procedure dbo.GetEmailTemplateFieldsByEntityTypeCode

--Begin procedure dbo.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode
EXEC utility.DropObject 'dbo.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode'
GO

-- ============================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to get data from the dbo.EmailTemplate table
-- ============================================================================
CREATE PROCEDURE dbo.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode

@EntityTypeCode VARCHAR(50),
@WorkflowActionCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET.EmailText
	FROM dbo.EmailTemplate ET
	WHERE ET.EntityTypeCode = @EntityTypeCode
		AND ET.WorkflowActionCode = @WorkflowActionCode

END
GO
--End procedure dbo.GetEmailTemplateTextByEntityTypeCodeAndWorkflowActionCode

--Begin procedure eventlog.LogEmailTemplateAction
EXEC utility.DropObject 'eventlog.LogEmailTemplateAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.05.31
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEmailTemplateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EmailTemplate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EmailTemplate',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EmailTemplate'), ELEMENTS
			)
		FROM dbo.EmailTemplate T
		WHERE T.EmailTemplateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEmailTemplateAction

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
-- ==================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		--dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirst') AS FullName,
		C1.lastname + ' , ' + C1.Firstname + ' ' + C1.Middlename as FullName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceID FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,	
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.FaceBookpageURL
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'ConceptNoteContact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		
END
GO
--End procedure reporting.GetContact