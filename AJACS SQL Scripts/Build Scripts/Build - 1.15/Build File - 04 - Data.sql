USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'EmailTemplate')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('EmailTemplate', 'Email Template')
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EmailTemplateAdministrationList', @NewMenuItemText='Email Templates', @NewMenuItemLink='/emailtemplateadministration/list', @ParentMenuItemCode='Admin', @AfterMenuItemCode='PersonList', @PermissionableLineageList='EmailTemplateAdministration.List', @IsActive=1
GO
--End table dbo.MenuItem

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.List.Export'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Export',
	'Export',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.View.Export'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (17,26)

	END
--ENDIF
GO
--End table permissionable.PersonPermissionable

--Begin Table workflow.WorkflowAction
IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'Comment')
	INSERT INTO workflow.WorkflowAction (WorkflowActionCode, WorkflowActionName) VALUES ('Comment', 'Comment')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'Complete')
	INSERT INTO workflow.WorkflowAction (WorkflowActionCode, WorkflowActionName) VALUES ('Complete', 'Complete')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'Release')
	INSERT INTO workflow.WorkflowAction (WorkflowActionCode, WorkflowActionName) VALUES ('Release', 'Release')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'Submit')
	INSERT INTO workflow.WorkflowAction (WorkflowActionCode, WorkflowActionName) VALUES ('Submit', 'Submit')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'UnHold')
	INSERT INTO workflow.WorkflowAction (WorkflowActionCode, WorkflowActionName) VALUES ('UnHold', 'UnHold')
--ENDIF
GO
--End Table workflow.WorkflowAction
