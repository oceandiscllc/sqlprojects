USE AJACS
GO

--Begin function dbo.FormatUSDate
EXEC utility.DropObject 'dbo.FormatUSDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			John Lyons
-- Create date:	2015.05.31
-- Description:	A function to format Date data
-- ===========================================
CREATE FUNCTION dbo.FormatUSDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(10)

AS
BEGIN

	RETURN CONVERT(VARCHAR(10), @DateTimeData, 110)

END
GO
--End function dbo.FormatUSDate
