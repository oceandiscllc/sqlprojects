USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'PassportExpirationDate', 'DATE'
EXEC utility.AddColumn @TableName, 'FaceBookPageURL', 'VARCHAR(500)'
GO
--End table dbo.Contact

--Begin table dbo.EmailTemplate
DECLARE @TableName VARCHAR(250) = 'dbo.EmailTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.EmailTemplate
	(
	EmailTemplateID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	WorkflowActionCode VARCHAR(50),
	EmailText VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateID'
EXEC utility.SetIndexClustered 'IX_EmailTemplate', @TableName, 'EntityTypeCode,WorkflowActionCode'
GO

INSERT dbo.EmailTemplate
	(EntityTypeCode,WorkflowActionCode,EmailText) 
VALUES
	('ConceptNote', 'Cancel', '<p>A previously submitted AJACS [[ConceptNoteTypeName]] has been cancelled:</p><p><strong>[[ConceptNoteTypeName]]: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the [[ConceptNoteTypeName]] workflow.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
	('ConceptNote', 'DecrementWorkflow', '<p>A previously submitted AJACS [[ConceptNoteTypeName]] has been disapproved:</p><p><strong>[[ConceptNoteTypeName]]:&nbsp;</strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the [[ConceptNoteTypeName]] workflow. Please click the link above to review this [[ConceptNoteTypeName]].</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p><p>&nbsp;</p>'),
	('ConceptNote', 'Hold', '<p>A previously submitted AJACS [[ConceptNoteTypeName]]&nbsp;has been placed on hold:</p><p><strong>[[ConceptNoteTypeName]]:&nbsp;</strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the [[ConceptNoteTypeName]] workflow. Please click the link above to review this [[ConceptNoteTypeName]].</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
	('ConceptNote', 'IncrementWorkflow', '<p>An AJACS [[ConceptNoteTypeName]] has been submitted for your review:</p><p><strong>[[ConceptNoteTypeName]]: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Activity Management workflow. Please click the link above to review this [[ConceptNoteTypeName]].</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p><p>&nbsp;</p>'),
	('ConceptNote', 'UnHold', '<p>A previously submitted AJACS [[ConceptNoteTypeName]] has been reactivated:</p><p><strong>[[ConceptNoteTypeName]]:&nbsp;</strong>[[TitleLink]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the [[ConceptNoteTypeName]] workflow. Please click the link above to review this [[ConceptNoteTypeName]].</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
	('ProgramReport', 'DecrementWorkflow', '<p>A previously submitted AJACS program report has been disapproved:</p><p><strong>Program Report: </strong>[[ReferenceCodeLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Program Reports workflow. Please log in and click the link above to review this program report.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
	('ProgramReport', 'IncrementWorkflow', '<p>An AJACS program report has been submitted for your review:</p><p><strong>Program Report: </strong>[[ReferenceCodeLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Program Reports workflow. Please log in and click the link above to review this program report.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
	('ProgramReport', 'Release', '<p><strong>Report:</strong> [[ReferenceCode]]</p><p><strong>Name:</strong> [[ProgramReportName]]</p><p><strong>Program Report Comment:</strong> [[Comments]]</p><p>The report is available at the following URL [[ReferenceCodeLink]]<br />Note that you will not be able to access this URL unless you are logged into the Donor Portal.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br />The AJACS Team</p>'),
	('RequestForInformation', 'Complete', '<p><strong>Incident/Event:</strong> [[Title]]<br /><strong>Community/Location:</strong> [[CommunityOrLocation]]<br /><strong>Date:</strong> [[IncidentDateFormatted]]<br /><strong>Known details of event:</strong> [[KnownDetails]]<br /><strong>Information Requested:</strong> [[InformationRequested]]<br /><strong>Summary Answer:</strong> [[SummaryAnswer]]</p><p>&nbsp;</p>'),
	('RequestForInformation', 'Rerelease', '<p><strong>Incident/Event:</strong> [[Title]]<br /><strong>Community/Location:</strong> [[CommunityOrLocation]]<br /><strong>Date:</strong> [[IncidentDateFormatted]]<br /><strong>Known details of event:</strong> [[KnownDetails]]<br /><strong>Information Requested:</strong> [[InformationRequested]]<br /><strong>Summary Answer:</strong> [[SummaryAnswer]]</p>'),
	('RequestForInformation', 'Submit', '<p><strong>Incident/Event:</strong> [[Title]]</p><p><strong>Community/Location:</strong> [[CommunityOrLocation]]</p><p><strong>Date:</strong> [[IncidentDateFormatted]]<br /><strong>Known details of event:</strong> [[KnownDetails]]<br /><strong>Information Requested:</strong> [[InformationRequested]]</p><p>&nbsp;</p>'),
	('SpotReport', 'Comment', '<p>An AJACS Spot Report Comment has been submitted for your review:&nbsp;</p><p><strong>Spot Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Spot Reports workflow. Please log in and click the link above to review this spot report.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p><p>&nbsp;</p>'),
	('SpotReport', 'DecrementWorkflow', '<p>A previously submitted AJACS spot report has been disapproved&nbsp; ()</p><p><strong>Reference Code:</strong> [[ReferenceCode]]</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Spot Reports workflow.<br />The report is available at the following URL [[TitleLink]]<br /><br />Please do not reply to this email as it is generated automatically by the AJACS system.<br /><br />Thank you,<br />The AJACS Team</p><p>&nbsp;</p>'),
	('SpotReport', 'IncrementWorkflow', '<p>An AJACS Spot Report Comment has been submitted for your review:&nbsp; SR</p><p><strong>Spot Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Spot Reports workflow. Please log in and click the link above to review this spot report.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
	('SpotReport', 'Release', '<p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Donor Portal.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br />The AJACS Team</p><p>&nbsp;</p>'),
	('SpotReport', 'Rerelease', '<p>A previously released AJACS spot report has been re-released:</p><p><strong>Reference Code:</strong> [[ReferenceCode]]</p><p><strong>Title:</strong> [[Title]]</p><p><strong>Comments:</strong> [[Comments]]</p><p>The report has been re-released is available at the following URL [[TitleLink]]<br />Note that you will not be able to access this URL unless you are logged into the Donor Portal.<br /><br />Please do not reply to this email as it is generated automatically by the AJACS system.<br /><br />Thank you,<br />The AJACS Team</p><p>&nbsp;</p>'),
	('WeeklyReport', 'DecrementWorkflow', '<p>A previously submitted AJACS weekly atmospheric report has been disapproved:</p><p><strong>Weekly Atmospheric Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Weekly Atmospheric Reports workflow. Please click the link above to review this weekly atmospheric report.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p><p>&nbsp;</p>'),
	('WeeklyReport', 'IncrementWorkflow', '<p>An AJACS weekly atmospheric report has been submitted for your review:</p><p><strong>Weekly Atmospheric Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Weekly Atmospheric Reports workflow. Please click the link above to review this weekly atmospheric report.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
	('WeeklyReport', 'Release', '<p>An AJACS weekly atmospheric report has been released:</p><p><strong>Weekly Atmospheric Report: </strong>[[TitleLink]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been designated as a recipient of the AJACS weekly report.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p><p>&nbsp;</p>')
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
DECLARE @TableName VARCHAR(250) = 'dbo.EmailTemplateField'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.EmailTemplateField
	(
	EmailTemplateFieldID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	PlaceHolderText VARCHAR(50),
	PlaceHolderDescription VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EmailTemplateFieldID'
EXEC utility.SetIndexClustered 'IX_EmailTemplate', @TableName, 'EntityTypeCode,DisplayOrder'
GO

INSERT INTO dbo.EmailTemplateField
	(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
VALUES
	('ConceptNote', '[[Title]]', 'Title', 1),
 	('ConceptNote', '[[ConceptNoteTypeName]]', 'Type', 2),
 	('ConceptNote', '[[ConceptNoteID]]', 'ID', 3),
	('ConceptNote', '[[Comments]]', 'Comments', 4),
 	('ConceptNote', '[[TitleLink]]', 'Title Link', 5),
	
	('Person', '[[FirstName]]', 'First Name', 1),
	('Person', '[[LastName]]', 'Last Name', 2),
	('Person', '[[Username]]', 'Username', 3),
	('Person', '[[TokenLink]]', 'Token Link', 4),
	
 	('ProgramReport', '[[ReferenceCode]]', 'Reference Code', 1),
	('ProgramReport', '[[ProgramReportName]]', 'Name', 2),
	('ProgramReport', '[[Comments]]', 'Comments', 3),
	('ProgramReport', '[[ReferenceCodeLink]]', 'Reference Code Link', 4),
	
	('RequestForInformation', '[[Title]]', 'Title', 1),
	('RequestForInformation', '[[CommunityOrLocation]]', 'Community Or Location', 2),
	('RequestForInformation', '[[IncidentDateFormatted]]', 'Incident Date', 3),
	('RequestForInformation', '[[KnownDetails]]', 'Known Details', 4),
	('RequestForInformation', '[[InformationRequested]]', 'Information Requested', 5),
	('RequestForInformation', '[[SummaryAnswer]]', 'Summary Answer', 6),
	
	('SpotReport', '[[Title]]', 'Title', 1),
	('SpotReport', '[[TitleLink]]', 'Title Link', 2),
	('SpotReport', '[[Comments]]', 'Comments', 3),
	('SpotReport', '[[ReferenceCode]]', 'Reference Code', 4),
	
	('WeeklyReport', '[[Title]]', 'Title', 1),
	('WeeklyReport', '[[TitleLink]]', 'Report Link', 2),
	('WeeklyReport', '[[Comments]]', 'Comments', 3)
GO
--End table dbo.EmailTemplateField
