USE AJACS
GO

--Begin procedure asset.GetAssetByAssetID
EXEC Utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the asset.Asset table
-- ======================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Asset
	SELECT
		A.AssetDescription,
		A.AssetID,
		A.AssetName,
		A.CommunityID,
		dbo.GetCommunityNameByCommunityID(A.CommunityID) AS CommunityName,
		A.IsActive,
		A.Location.STAsText() AS Location,
		A.RelocationDate,
		dbo.FormatDate(A.RelocationDate) AS RelocationDateFormatted,
		A.RelocationLocation,
		A.RelocationNotes,
		AST.AssetStatusCode,
		AST.AssetStatusID,
		AST.AssetStatusName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon
	FROM asset.Asset A
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	--AssetPaymentHistory
	EXEC asset.GetAssetPaymentHistoryByAssetID @AssetID = @AssetID

	--AssetTrainingHistory
	SELECT
		COUNT(CC.ContactID) AS StudentCount,
		CR.CourseID,
		CR.CourseName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
	GROUP BY A.AssetID, CR.CourseName, CR.CourseID
	ORDER BY 3

	--AssetUnit
	SELECT
		AU.AssetUnitID,
		AU.AssetUnitName,
		AU.CommanderContactID,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullNameFormatted,
		AU.DeputyCommanderContactID,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullNameFormatted,
		AU.IsActive,
		AUC.AssetUnitCostID,
		AUC.AssetUnitCostName
	FROM asset.AssetUnit AU
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			AND AU.AssetID = @AssetID

	--AssetUnitContactVetting
	;
	WITH CD AS
		(
		SELECT AU.CommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID

		UNION

		SELECT AU.DeputyCommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID
		)

	SELECT 
		dbo.FormatContactNameByContactID(US.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'US' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(US.VettingDate) AS VettingDateFormatted,
		VO1.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN DATEADD(m, 6, US.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO1.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			A.ContactID,
			A.ContactVettingID,
			A.VettingDate,
			A.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD1.ContactID ORDER BY CV1.VettingDate DESC, CV1.ContactVettingID DESC) AS RowIndex,
				CD1.ContactID,
				CV1.ContactVettingID,
				CV1.VettingDate,
				CV1.VettingOutcomeID
			FROM CD CD1
				LEFT JOIN dbo.ContactVetting CV1 ON CV1.ContactID = CD1.ContactID
			WHERE CV1.ContactVettingTypeID = 1
			) A
			WHERE A.RowIndex = 1
		) US
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = ISNULL(US.VettingOutcomeID, 0)

	UNION

	SELECT 
		dbo.FormatContactNameByContactID(UK.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'UK' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(UK.VettingDate) AS VettingDateFormatted,
		VO2.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN DATEADD(m, 6, UK.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO2.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			B.ContactID,
			B.ContactVettingID,
			B.VettingDate,
			B.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD2.ContactID ORDER BY CV2.VettingDate DESC, CV2.ContactVettingID DESC) AS RowIndex,
				CD2.ContactID,
				CV2.ContactVettingID,
				CV2.VettingDate,
				CV2.VettingOutcomeID
			FROM CD CD2
				LEFT JOIN dbo.ContactVetting CV2 ON CV2.ContactID = CD2.ContactID
			WHERE CV2.ContactVettingTypeID = 2
			) B
			WHERE B.RowIndex = 1
		) UK
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = ISNULL(UK.VettingOutcomeID, 0)

	ORDER BY 1, 2, 3, 4

END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure asset.GetAssetPaymentHistoryByAssetID
EXEC Utility.DropObject 'asset.GetAssetPaymentHistoryByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.05
-- Description:	A stored procedure to get payment history data for an asset
-- ========================================================================
CREATE PROCEDURE asset.GetAssetPaymentHistoryByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable1 TABLE
		(
		ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		PaymentYear INT DEFAULT 0,
		PaymentMonth INT DEFAULT 0,
		AssetID INT DEFAULT 0,
		StipendAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		StipendAmountPaid NUMERIC(18,2) DEFAULT 0,
		PayeeCount INT DEFAULT 0,
		ExpenseAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		ExpenseAmountPaid NUMERIC(18,2) DEFAULT 0,
		AssetDepartmentCount INT DEFAULT 0
		)

	INSERT INTO @tTable1
		(PaymentYear, PaymentMonth, StipendAmountAuthorized, StipendAmountPaid, AssetID, PayeeCount)
	SELECT
		CSP.PaymentYear,
		CSP.PaymentMonth,
		SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
		SUM(CSP.StipendAmountPaid) AS StipendAmountPaid,
		A.AssetID,
		COUNT(CSP.ContactID) AS PayeeCount
	FROM dbo.ContactStipendPayment CSP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = CSP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
	GROUP BY A.AssetID, A.AssetName, CSP.PaymentYear, CSP.PaymentMonth

	MERGE @tTable1 T1
	USING
		(
		SELECT
			AUE.PaymentYear,
			AUE.PaymentMonth,
			SUM(AUE.ExpenseAmountAuthorized) AS ExpenseAmountAuthorized,
			SUM(AUE.ExpenseAmountPaid) AS ExpenseAmountPaid,
			A.AssetID,
			COUNT(AUE.AssetUnitID) AS AssetDepartmentCount
		FROM asset.AssetUnitExpense AUE
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
				AND A.AssetID = @AssetID
		GROUP BY A.AssetID, A.AssetName, AUE.PaymentYear, AUE.PaymentMonth
		) T3 ON T3.PaymentYear = T1.PaymentYear 
			AND T3.PaymentMonth = T1.PaymentMonth
			AND T3.AssetID = T1.AssetID 
	WHEN NOT MATCHED THEN
	INSERT
		(PaymentYear, PaymentMonth, ExpenseAmountAuthorized, ExpenseAmountPaid, AssetID, AssetDepartmentCount)
	VALUES
		(
		T3.PaymentYear, 
		T3.PaymentMonth, 
		T3.ExpenseAmountAuthorized, 
		T3.ExpenseAmountPaid, 
		T3.AssetID, 
		T3.AssetDepartmentCount
		)
	WHEN MATCHED THEN
	UPDATE 
	SET 
		T1.ExpenseAmountAuthorized = T3.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid = T3.ExpenseAmountPaid, 
		T1.AssetDepartmentCount = T3.AssetDepartmentCount
	;

	SELECT 
		T1.PaymentYear,
		RIGHT('00' + CAST(T1.PaymentMonth AS VARCHAR(2)), 2) AS PaymentMonthFormatted,
		T1.StipendAmountAuthorized,
		T1.StipendAmountPaid,
		T1.PayeeCount,
		T1.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid, 
		T1.AssetDepartmentCount
	FROM @tTable1 T1
		JOIN asset.Asset A ON A.AssetID = T1.AssetID
	ORDER BY T1.PaymentYear DESC, T1.PaymentMonth DESC, A.AssetName

END
GO
--End procedure asset.GetAssetPaymentHistoryByAssetID

--Begin procedure asset.GetAssetTrainingHistoryDetails
EXEC Utility.DropObject 'asset.GetAssetTrainingHistoryDetails'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.05
-- Description:	A stored procedure to get training details for an asset
-- ====================================================================
CREATE PROCEDURE asset.GetAssetTrainingHistoryDetails

@AssetID INT,
@CourseID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CR.CourseName,
		dbo.FormatContactNameByContactID(CC.ContactID, 'LastFirst') AS ContactNameFormatted,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.ClassID,
		CL.Location
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
			AND CR.CourseID = @CourseID
	ORDER BY CL.StartDate DESC, CL.EndDate DESC

END
GO
--End procedure asset.GetAssetTrainingHistoryDetails

--Begin procedure dbo.ApproveContactStipendPayment
EXEC Utility.DropObject 'dbo.ApproveContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ApproveContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET
		CSP.AssetUnitID = C.AssetUnitID,
		CSP.StipendAuthorizedDate = getDate(),
		CSP.StipendAmountPaid = 
			CASE
				WHEN dbo.GetContactStipendEligibility(CSP.ContactID) = 1
				THEN CSP.StipendAmountAuthorized
				ELSE 0
			END

	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			AND CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = @StipendTypeCode
			AND CSP.StipendAuthorizedDate IS NULL

	IF @StipendTypeCode = 'JusticeStipend'
		BEGIN

		UPDATE AUE
		SET 
			AUE.ExpenseAuthorizedDate = getDate(),
			AUE.ExpenseAmountPaid = AUE.ExpenseAmountAuthorized
		FROM asset.AssetUnitExpense AUE
		WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
			AND AUE.ProvinceID = @ProvinceID
			AND AUE.ExpenseAuthorizedDate IS NULL

		END
	--ENDIF

END
GO
--End procedure dbo.ApproveContactStipendPayment

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT,
@WorkflowID INT,
@IsAmendment BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @IsAmendment = 1
		BEGIN

		SELECT @WorkflowID = EWSGP.WorkflowID
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = 'ConceptNote' 
			AND EWSGP.EntityID = @ConceptNoteID

		END
	--ENDIF

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,ActualTotalAmount,AmendedConceptNoteID,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,EndDate,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,StartDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber,WorkplanActivityID)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,

		CASE
			WHEN @IsAmendment = 1
			THEN C.ActualTotalAmount
			ELSE 0
		END,

		CASE
			WHEN @IsAmendment = 1
			THEN @ConceptNoteID
			ELSE 0
		END,

		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.EndDate,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.StartDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,

		CASE
			WHEN @IsAmendment = 1
			THEN C.Title
			ELSE 'Clone of:  ' + C.Title
		END,

		C.VettingRequirements,
		1,
		WorkplanActivityID		
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@nConceptNoteID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.WorkflowID = @WorkflowID

	INSERT INTO	dbo.ConceptNoteAmendment
		(ConceptNoteID, AmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Amendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Amendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	IF @IsAmendment = 1
		BEGIN

		INSERT INTO	dbo.ConceptNoteContact
			(ConceptNoteID, ContactID)
		SELECT
			@nConceptNoteID,
			CNC.ContactID
		FROM dbo.ConceptNoteContact CNC
		WHERE CNC.ConceptNoteID = @ConceptNoteID

		END
	--ENDIF

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added the Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
--
-- Author:			Eric Jones
-- Create date:	2016.11.20
-- Description:	Added the IsCommunityAssociationRequired field
--
-- Author:			Todd Pires
-- Create date:	2016.12.31
-- Description:	Added the ContactLocation field
--
-- Author:			Todd Pires
-- Create date:	2017.03.02
-- Description:	Added the Training History recordset & 3 nw arabic data fields
-- ===========================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Contact
	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicAddress,
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,
		C1.ArabicPlaceOfBirth,
		C1.ArabicTitle,
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.AssetUnitID,		
		(SELECT AU.AssetUnitName FROM asset.AssetUnit AU WHERE AU.AssetUnitID = C1.AssetUnitID) AS AssetUnitName,		
		(SELECT A.AssetName FROM asset.Asset A JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID AND AU.AssetUnitID = C1.AssetUnitID) AS AssetName,
		dbo.GetCommunityNameByCommunityID(asset.GetCommunityIDByAssetUnitID(C1.AssetUnitID)) AS AssetCommunityName,
		C1.CommunityID,		
		dbo.GetCommunityNameByCommunityID(C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		dbo.GetLocationByContactID(C1.ContactID) AS ContactLocation,
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate((SELECT EL.CreateDateTime FROM eventlog.EventLog EL WHERE EL.EventCode = 'create' AND EL.EntityTypeCode = 'Contact' AND EL.EntityID = C1.ContactID)) AS InitialEntryDateFormatted,
		C1.IsActive,
		C1.IsCommunityAssociationRequired,	
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(SELECT CSG.CommunitySubGroupName FROM dropdown.CommunitySubGroup CSG JOIN dbo.Community C ON C. CommunitySubGroupID = CSG.CommunitySubGroupID AND C.CommunityID = C1.CommunityID) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	--ContactContactAffiliation
	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID
	ORDER BY 2, 1

	--ContactContactType
	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID
		ORDER BY 3, 1
	
	--ContactDocument
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Contact'
			AND DE.EntityID = @ContactID
	ORDER BY D.DocumentDescription

	--ContactStipendPaymentHistory
	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC

	--ContactTrainingHistory
	SELECT
		CC.ContactID,
		CR.CourseName, 
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Location
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
			AND CC.ContactID = @ContactID
	ORDER BY 3, 4

	--ContactVettingHistory
	SELECT
		CV.ContactVettingID, 
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName,
		VT.ContactVettingTypeName,

		CASE
			WHEN DATEADD(m, 6, CV.VettingDate) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
		JOIN dropdown.ContactVettingType VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID
			AND CV.ContactID = @ContactID
	ORDER BY CV.VettingDate DESC

END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetVettingExpirationCounts
EXEC Utility.DropObject 'dbo.GetVettingExpirationCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A procedure to get counts of expired and soon to expire vetting records
-- ====================================================================================
CREATE PROCEDURE dbo.GetVettingExpirationCounts

AS
BEGIN
	SET NOCOUNT ON;

	SELECT D.*
	FROM
		(
		SELECT
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) < 0) AS UKExpired,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 30) AS UK30,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 31 AND 60) AS UK60,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 61 AND 90) AS UK90,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) < 0) AS USExpired,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 30) AS US30,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 31 AND 60) AS US60,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 61 AND 90) AS US90
		) AS D

	SELECT D.*
	FROM
		(
		SELECT
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) < 0 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS UKExpiredContactIDList,
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 30 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS UK30ExpiredContactIDList,
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 31 AND 60 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS UK60ExpiredContactIDList,
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 61 AND 90 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS UK90ExpiredContactIDList,
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) < 0 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS USExpiredContactIDList,
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 30 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS US30ExpiredContactIDList,
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 31 AND 60 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS US60ExpiredContactIDList,
			(SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 61 AND 90 ORDER BY C.ContactID FOR XML PATH ('')), 1, 1, '')) AS US90ExpiredContactIDList
		) AS D
		
END
GO
--End procedure dbo.GetVettingExpirationCounts

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.20
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@IncludeZero BIT = 0,
@PersonID INT = 0,
@HasAddUpdate BIT = 0,
@HasView BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DG.DocumentGroupID,
		DG.DocumentGroupName,
		DT.DocumentTypeID,
		DT.DocumentTypePermissionCode,
		DT.DocumentTypeName
	FROM dropdown.DocumentType DT
		JOIN dropdown.DocumentGroup DG ON DG.DocumentGroupID = DT.DocumentGroupID
			AND DT.IsActive = 1
			AND (DT.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PersonPermissionable PP
				WHERE PP.PersonID = @PersonID
					AND 
						(
						@HasAddUpdate = 1 AND PP.PermissionableLineage = 'Document.AddUpdate.' + DT.DocumentTypePermissionCode
							OR @HasView = 1 AND PP.PermissionableLineage = 'Document.View.' + DT.DocumentTypePermissionCode
							OR DT.DocumentTypePermissionCode IS NULL
						)
				)
	ORDER BY DG.DisplayOrder, DT.DocumentTypeName

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- Notes:				Changes here must ALSO be made to force.GetForceByEventLogID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
--
-- Author:			Eric Jones
-- Create date:	2016.12.07
-- Description:	implemented eventlog data to populate revision history table
--
-- Author:			Eric Jones
-- Create date:	2016.12.15
-- Description:	added eventlog id to query result set.
-- =========================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullName,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
	SELECT 
		EL.eventLogID,
		EL.eventcode, 
		EL.eventdata, 
		dbo.FormatDateTime(EL.createdatetime) AS createDateFormatted, 
		dbo.FormatPersonName(P.firstname, P.lastname,'','LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
		JOIN person P on p.personid = el.personid
	WHERE EL.EntityTypeCode = 'force' AND EL.entityid = @ForceID AND EL.eventcode != 'read' 
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure reporting.GetJusticeStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetJusticeStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendPaymentReport

@PersonID INT = 0, 
@Year int = 0, 
@Month int = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT
	DECLARE @nContactCount INT
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP


	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Rank 1] = SP.[Rank 1] + CASE WHEN @cStipendName = 'Rank 1' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 1 Count] = SP.[Rank 1 Count] + CASE WHEN @cStipendName = 'Rank 1' THEN @nContactCount ELSE 0 END,
			SP.[Rank 2] = SP.[Rank 2] + CASE WHEN @cStipendName = 'Rank 2' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 2 Count] = SP.[Rank 2 Count] + CASE WHEN @cStipendName = 'Rank 2' THEN @nContactCount ELSE 0 END,
			SP.[Rank 3] = SP.[Rank 3] + CASE WHEN @cStipendName = 'Rank 3' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 3 Count] = SP.[Rank 3 Count] + CASE WHEN @cStipendName = 'Rank 3' THEN @nContactCount ELSE 0 END,
			SP.[Rank 4] = SP.[Rank 4] + CASE WHEN @cStipendName = 'Rank 4' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 4 Count] = SP.[Rank 4 Count] + CASE WHEN @cStipendName = 'Rank 4' THEN @nContactCount ELSE 0 END,
			SP.[Rank 5] = SP.[Rank 5] + CASE WHEN @cStipendName = 'Rank 5' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Rank 5 Count] = SP.[Rank 5 Count] + CASE WHEN @cStipendName = 'Rank 5' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		SP.[Total Count],
		SP.[Total Stipend],
		SP.[Rank 1],
		SP.[Rank 2],
		SP.[Rank 3],
		SP.[Rank 4],
		SP.[Rank 5],
		SP.[Rank 1 Count],
		SP.[Rank 2 Count],
		SP.[Rank 3 Count],
		SP.[Rank 4 Count],
		SP.[Rank 5 Count],
		sp.stipendpaymentid, 
		sp.personid,
		sp.AssetUnitid,
		AUE.ExpenseAmountAuthorized AS [Running Costs],
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID 
		JOIN asset.Asset A ON A.AssetID = AU.AssetID 
		JOIN asset.AssetUnitExpense AUE ON AUE.AssetUnitID = AU.AssetUnitID 
			AND SP.PersonID = @PersonID
			AND AUE.PaymentMonth = @Month 
			AND AUE.PaymentYear = @Year

END
GO
--End procedure reporting.GetJusticeStipendPaymentReport

--Begin procedure reporting.GetStipendPaymentReport
EXEC utility.DropObject 'reporting.GetStipendPaymentReport'
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2015.09.30
-- Description:	A stored procedure to add data to the reporting.StipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =================================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT 

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP


	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendName,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Command] = SP.[Command] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Command Count] = SP.[Command Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END,
			SP.[General] = SP.[General] + CASE WHEN @cStipendName = 'General' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[General Count] = SP.[General Count] + CASE WHEN @cStipendName = 'General' THEN @nContactCount ELSE 0 END,
			SP.[Colonel] = SP.[Colonel] + CASE WHEN @cStipendName = 'Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Count] = SP.[Colonel Count] + CASE WHEN @cStipendName = 'Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Colonel Doctor] = SP.[Colonel Doctor] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Colonel Doctor Count] = SP.[Colonel Doctor Count] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nContactCount ELSE 0 END,
			SP.[Lieutenant Colonel] = SP.[Lieutenant Colonel] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Lieutenant Colonel Count] = SP.[Lieutenant Colonel Count] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nContactCount ELSE 0 END,
			SP.[Major] = SP.[Major] + CASE WHEN @cStipendName = 'Major' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Major Count] = SP.[Major Count] + CASE WHEN @cStipendName = 'Major' THEN @nContactCount ELSE 0 END,
			SP.[Captain] = SP.[Captain] + CASE WHEN @cStipendName = 'Captain' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Count] = SP.[Captain Count] + CASE WHEN @cStipendName = 'Captain' THEN @nContactCount ELSE 0 END,
			SP.[Captain Doctor] = SP.[Captain Doctor] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Captain Doctor Count] = SP.[Captain Doctor Count] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nContactCount ELSE 0 END,
			SP.[First Lieutenant] = SP.[First Lieutenant] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Lieutenant Count] = SP.[First Lieutenant Count] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Officer] = SP.[Contracted Officer] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Officer Count] = SP.[Contracted Officer Count] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nContactCount ELSE 0 END,
			SP.[First Sergeant] = SP.[First Sergeant] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Sergeant Count] = SP.[First Sergeant Count] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[Sergeant] = SP.[Sergeant] + CASE WHEN @cStipendName = 'Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Sergeant Count] = SP.[Sergeant Count] + CASE WHEN @cStipendName = 'Sergeant' THEN @nContactCount ELSE 0 END,
			SP.[First Adjutant] = SP.[First Adjutant] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[First Adjutant Count] = SP.[First Adjutant Count] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Adjutant] = SP.[Adjutant] + CASE WHEN @cStipendName = 'Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Adjutant Count] = SP.[Adjutant Count] + CASE WHEN @cStipendName = 'Adjutant' THEN @nContactCount ELSE 0 END,
			SP.[Policeman] = SP.[Policeman] + CASE WHEN @cStipendName = 'Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Policeman Count] = SP.[Policeman Count] + CASE WHEN @cStipendName = 'Policeman' THEN @nContactCount ELSE 0 END,
			SP.[Contracted Policeman] = SP.[Contracted Policeman] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Contracted Policeman Count] = SP.[Contracted Policeman Count] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		SP.*,
		AUCR.AssetUnitCostName AS [Running Costs],
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dropdown.AssetUnitCost AUCR ON AUCR.AssetUnitCostID = AU.AssetUnitCostID

END
GO
--End procedure reporting.GetStipendPaymentReport

--Begin procedure workplan.GetWorkplanByWorkplanID
EXEC Utility.DropObject 'workplan.GetWorkplanByWorkplanID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.21
-- Description:	A stored procedure to get data from the workplan.Workplan table
--
-- Author:			Todd Pires
-- Create date: 2016.12.17
-- Description:	Added activity counts to the Activity Performance result set
--
-- Author:			Todd Pires
-- Create date: 2017.02.16
-- Description:	Tweaked the committed balance calculation
-- ============================================================================
CREATE PROCEDURE workplan.GetWorkplanByWorkplanID

@WorkplanID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		WP.EndDate, 
		dbo.FormatDate(WP.EndDate) AS EndDateFormatted,
		WP.IsActive, 
		WP.IsForDashboard, 
		WP.StartDate,
		dbo.FormatDate(WP.StartDate) AS StartDateFormatted,
		WP.UKContractNumber, 
		WP.USContractNumber, 
		WP.USDToGBPExchangeRate,
		WP.WorkplanID, 
		WP.WorkplanName
	FROM workplan.Workplan WP
	WHERE	WP.WorkplanID = @WorkplanID

	SELECT
		CNT.ConceptNoteTypeName,
		FS1.FundingSourceName AS CurrentFundingSourceName,
		FS2.FundingSourceName AS OriginalFundingSourceName,
		FORMAT(WPA.CurrentUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS CurrentGBPAllocation,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		WPA.Description,
		WPA.Notes,
		FORMAT(WPA.OriginalUSDAllocation * (SELECT WP.USDToGBPExchangeRate FROM workplan.Workplan WP WHERE WP.WorkplanID = @WorkplanID), 'C', 'en-gb') AS OriginalGBPAllocation,
		FORMAT(WPA.OriginalUSDAllocation, 'C', 'en-us') AS OriginalUSDAllocation,
		WPA.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM workplan.WorkplanActivity WPA
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
		JOIN dropdown.FundingSource FS1 ON FS1.FundingSourceID = WPA.CurrentFundingSourceID
		JOIN dropdown.FundingSource FS2 ON FS2.FundingSourceID = WPA.OriginalFundingSourceID
			AND WPA.WorkplanID = @WorkplanID

	SELECT
		D.WorkplanActivityID,
		WPA.WorkplanActivityName,
		CNT.ConceptNoteTypeName,
		FORMAT(WPA.CurrentUSDAllocation, 'C', 'en-us') AS CurrentUSDAllocation,
		FORMAT(SUM(D.Planned), 'C', 'en-us') AS Planned,
		FORMAT(SUM(D.Disbursed), 'C', 'en-us') AS Disbursed,
		FORMAT(SUM(D.CommittedBalance), 'C', 'en-us') AS CommittedBalance,
		(
		SELECT COUNT(CN1.ConceptNoteID) 
		FROM dbo.ConceptNote CN1 
			JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN1.ConceptNoteStatusID
				AND CNS.ConceptNoteStatusCode NOT IN ('Cancelled','Closed','OnHold')
				AND CN1.WorkplanActivityID = D.WorkplanActivityID
		) AS ConceptNoteCount
	FROM
		(
		SELECT 
			WPA.WorkplanActivityID,

			CASE
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) = 'Development'
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal
				ELSE 0
			END AS Planned,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closed','Cancelled','OnHold','Closedown','Implementation')
				THEN CN.ActualTotalAmount + OACNF.ConceptNoteFinanceTotal
				ELSE 0
			END AS Disbursed,

			CASE 
				WHEN workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) IN ('Closedown','Implementation')
				THEN OACNB.TotalBudget + OACNCE.ConceptNoteEquimentTotal
				ELSE 0
			END AS CommittedBalance

		FROM dbo.ConceptNote CN
			JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
				AND WPA.WorkplanID = @WorkplanID
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue), 0) AS TotalBudget
				FROM dbo.ConceptNoteBudget CNB
				WHERE CNB.ConceptNoteID = CN.ConceptNoteID
				) OACNB
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0) AS ConceptNoteEquimentTotal
				FROM dbo.ConceptNoteEquipmentCatalog CNCE
					JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
						AND CNCE.ConceptNoteID = CN.ConceptNoteID
				) OACNCE
			OUTER APPLY
				(
				SELECT
					ISNULL(SUM(CNF.drAmt) - SUM(CNF.CrAmt), 0) AS ConceptNoteFinanceTotal
				FROM dbo.ConceptNoteFinance CNF
				WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
				) OACNF
		) D
	JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = D.WorkplanActivityID
	JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = WPA.ConceptNoteTypeID
	GROUP BY D.WorkplanActivityID, WPA.WorkplanActivityName, WPA.CurrentUSDAllocation, CNT.ConceptNoteTypeName
	ORDER BY WPA.WorkplanActivityName

END
GO
--End procedure workplan.GetWorkplanByWorkplanID