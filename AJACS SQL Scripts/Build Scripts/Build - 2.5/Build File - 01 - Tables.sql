USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ArabicAddress', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'ArabicPlaceOfBirth', 'NVARCHAR(800)'
EXEC utility.AddColumn @TableName, 'ArabicTitle', 'NVARCHAR(400)'
GO
--End table dbo.Contact

--Begin table dbo.Document
DECLARE @TableName VARCHAR(250) = 'dbo.Document'

EXEC utility.AddColumn @TableName, 'DocumentData', 'VARBINARY'
GO
--End table dbo.Document

--Begin table dbo.ContactStipendPayment
DECLARE @TableName VARCHAR(250) = 'dbo.ContactStipendPayment'

EXEC utility.AddColumn @TableName, 'AssetUnitID', 'INT', '0'
GO
--End table dbo.ContactStipendPayment
