USE AJACS
GO

--Begin table dbo.ContactStipendPayment
UPDATE CSP
SET CSP.AssetUnitID = C.AssetUnitID
FROM dbo.ContactStipendPayment CSP
	JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
GO
--End table dbo.ContactStipendPayment

--Begin table dropdown.AssetType
UPDATE AT
SET AT.AssetTypeName = 'Traffic Police Centre'
FROM dropdown.AssetType AT
WHERE AT.AssetTypeCategory = 'Police'
	AND AT.AssetTypeName = 'Traffic Centre'
GO

UPDATE AT
SET AT.IsActive = 0
FROM dropdown.AssetType AT
WHERE AT.AssetTypeCategory = 'Police'
	AND AT.AssetTypeName IN ('Criminal Investigation Department', 'Police Headquarters', 'Prison', 'Training Center')
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.AssetType AT WHERE AT.AssetTypeCategory = 'Police' AND AT.AssetTypeName = 'City Serious Case Unit')
	BEGIN

	INSERT INTO dropdown.AssetType
		(AssetTypeName, AssetTypeCategory, Icon)
	VALUES
		('City Serious Case Unit', 'Police', 'asset-police-station.png'),
		('Guards', 'Police', 'asset-police-station.png'),
		('Medical Services', 'Police', 'asset-police-station.png'),
		('Police Command', 'Police', 'asset-police-station.png'),
		('Pre-Detention Holding Facility', 'Police', 'asset-police-station.png'),
		('Specialised Administrative Police', 'Police', 'asset-police-station.png')

	END
--ENDIF
GO

UPDATE AT
SET AT.AssetTypeName = 'Custody Facility'
FROM dropdown.AssetType AT
WHERE AT.AssetTypeName = 'Pre-Detention Holding Facility'
GO
--End table dropdown.AssetType 

--Begin table dropdown.AuditOutcome 
IF NOT EXISTS (SELECT 1 FROM dropdown.AuditOutcome AO WHERE AO.AuditOutcomeCode = 'Transit')
	BEGIN

	INSERT INTO dropdown.AuditOutcome 
		(AuditOutcomeCode, AuditOutcomeName, DisplayOrder) 
	VALUES
		('Transit', 'In Transit', (SELECT MAX(AO.DisplayOrder) + 1 FROM dropdown.AuditOutcome AO))

	END
--ENDIF
GO
--End table dropdown.AuditOutcome 

--Begin table dropdown.ConceptNoteType 
UPDATE dropdown.ConceptNoteType 
SET ConceptNoteTypeCode = 'MER' 
WHERE ConceptNoteTypeName = 'MER'
GO
--End table dropdown.ConceptNoteType 

--Begin table dropdown.DocumentGroup 
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = 'DonorDocument')
	BEGIN

	INSERT INTO dropdown.DocumentGroup 
		(DocumentGroupCode,DocumentGroupName,DisplayOrder)
	SELECT
		'DonorDocument', 
		'Donor Document',
		MAX(DIsplayOrder) + 1
	FROM dropdown.DocumentGroup 

	INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName, DisplayOrder) SELECT DG.DocumentGroupID, 'Research Documents', 1 FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = 'DonorDocument'
	INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName, DisplayOrder) SELECT DG.DocumentGroupID, 'Project Reports', 2 FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = 'DonorDocument'
	INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName, DisplayOrder) SELECT DG.DocumentGroupID, 'Financial and Management Information', 3 FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = 'DonorDocument'
	INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName, DisplayOrder) SELECT DG.DocumentGroupID, 'Meeting Minutes and Papers', 4 FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = 'DonorDocument'
	INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName, DisplayOrder) SELECT DG.DocumentGroupID, 'Monitoring and Evaluation', 5 FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = 'DonorDocument'
	INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName, DisplayOrder) SELECT DG.DocumentGroupID, 'Other', 6 FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = 'DonorDocument'

	END
--ENDIF
GO
--End table dropdown.DocumentGroup 

--Begin table dropdown.DocumentType
UPDATE DT
SET DT.DocumentTypeName = '519 Weekly Atmospheric Summary Report'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode = 'WeeklySummaryReport'
GO

UPDATE DT
SET 
	DT.DocumentTypeName = '522 Archived Full Atmospheric Report',
	DT.DocumentTypePermissionCode = '522'
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode = 'ArchivedFullAtmosphericReport'
GO

UPDATE DT
SET DT.DocumentTypeCode = 
	CASE
		WHEN DT.DocumentTypeName = 'Research Documents'
		THEN 'ResearchDocuments'
		WHEN DT.DocumentTypeName = 'Project Reports'
		THEN 'ProjectReports'
		WHEN DT.DocumentTypeName = 'Financial and Management Information'
		THEN 'FinancialManagementInformation'
		WHEN DT.DocumentTypeName = 'Meeting Minutes and Papers'
		THEN 'MeetingRecords'
		WHEN DT.DocumentTypeName = 'Monitoring and Evaluation'
		THEN 'MAndE'
		WHEN DT.DocumentTypeName = 'Other'
		THEN 'Other'
		ELSE DT.DocumentTypeCode
	END
FROM dropdown.DocumentType DT
GO

UPDATE DT
SET DT.DocumentTypePermissionCode = 
	CASE
		WHEN DT.DocumentTypeCode = 'ResearchDocuments'
		THEN 'Doc001'
		WHEN DT.DocumentTypeCode = 'ProjectReports'
		THEN 'Doc002'
		WHEN DT.DocumentTypeCode = 'FinancialManagementInformation'
		THEN 'Doc003'
		WHEN DT.DocumentTypeCode = 'MeetingRecords'
		THEN 'Doc004'
		WHEN DT.DocumentTypeCode = 'MAndE'
		THEN 'Doc005'
		WHEN DT.DocumentTypeCode = 'Other'
		THEN 'Doc006'
		ELSE DT.DocumentTypePermissionCode
	END
FROM dropdown.DocumentType DT
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc006', @PERMISSIONCODE='Doc006';
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc006', @PERMISSIONCODE='Doc006';
GO
--End table dropdown.DocumentType

--Begin table dropdown.Stipend 
UPDATE S
SET S.StipendAmount = 125
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'PoliceStipend'
	AND S.StipendAmount = 100
GO
--End table dropdown.Stipend 
