-- File Name:	Build-1.55 File 01 - AJACS.sql
-- Build Key:	Build-1.55 File 01 - AJACS - 2016.04.29 20.59.30

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		eventlog.GetCommunityRoundContactXMLByCommunityRoundID
--		eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID
--		workflow.CanHaveAddUpdate
--
-- Procedures:
--		dbo.GetCommunityRoundByCommunityRoundID
--		dbo.GetMenuItemsByPersonID
--		dbo.GetVettingExpirationCounts
--		dropdown.GetCommunityRoundCivilDefenseCoverageData
--		dropdown.GetCommunityRoundGroupData
--		dropdown.GetCommunityRoundJusticeActivityData
--		dropdown.GetCommunityRoundOutputStatusData
--		dropdown.GetCommunityRoundPreAssessmentStatusData
--		dropdown.GetCommunityRoundRAPInfoData
--		dropdown.GetCommunityRoundRoundData
--		dropdown.GetCommunityRoundTamkeenData
--		eventlog.LogCommunityRoundAction
--		eventlog.LogServerSetupAction
--		workflow.GetWorkflowByWorkflowID
--
-- Tables:
--		dbo.CommunityRound
--		dbo.CommunityRoundContact
--		dbo.CommunityRoundUpate
--		dropdown.CommunityRoundCivilDefenseCoverage
--		dropdown.CommunityRoundGroup
--		dropdown.CommunityRoundJusticeActivity
--		dropdown.CommunityRoundOutputStatus
--		dropdown.CommunityRoundPreAssessmentStatus
--		dropdown.CommunityRoundRAPInfo
--		dropdown.CommunityRoundRound
--		dropdown.CommunityRoundTamkeen
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.CommunityRound
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRound'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRound
	(
	CommunityRoundID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	CommunityCode NVARCHAR(25),
	CommunityRoundRoundID INT,
	CommunityRoundGroupID INT,
	AreaManagerPersonID INT,
	ProjectOfficerPersonID INT,
	CommunityRoundRAPInfoID INT,
	CommunityRoundJusticeActivityID INT,
	CommunityRoundTamkeenID INT,
	CommunityRoundCivilDefenseCoverageID INT,
	FieldOfficer NVARCHAR(25),
	MOUDate DATE,
	FieldFinanceOfficerPersonID INT,
	FieldLogisticOfficerPersonID INT,
	ActivitiesOfficerPersonID INT,
	CommunityRoundOutput11StatusID INT,
	CommunityRoundOutput12StatusID INT,
	CommunityRoundOutput13StatusID INT,
	CommunityRoundOutput14StatusID INT,
	CommunityRoundPreAssessmentStatusID INT,
	IsFieldOfficerHired BIT,
	IsFieldOfficerTrainedOnProgramObjectives BIT,
	IsFieldOfficerIntroToPLO BIT,
	IsFieldOfficerIntroToFSPandLAC BIT,
	ToRDate DATE,
	SensitizationDefineFSPRoleDate DATE,
	SensitizationMeetingMOUDate DATE,
	SensitizationDiscussCommMakeupDate DATE,
	InitialSCADate DATE,
	FinalSWOTDate DATE,
	SCAMeetingCount INT,
	QuestionnairCount INT,
	FemaleQuestionnairCount INT,
	IsSCAAdjusted BIT,
	SCAFinalizedDate DATE,
	CSAPMeetingDate DATE,
	CSAP2PeerReviewMeetingDate DATE,
	CSAP2SubmittedDate DATE,
	CSAPSignedDate DATE,
	CSAPBeginDevelopmentDate DATE,
	CSAPCommunityAnnouncementDate DATE,
	ApprovedActivityCount INT,
	KickoffMeetingCount INT,
	ActivityImplementationStartDate DATE,
	ActivityImplementationEndDate DATE,
	IsActive BIT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivitiesOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ApprovedActivityCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AreaManagerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundCivilDefenseCoverageID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundJusticeActivityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput11StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput12StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput13StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput14StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundPreAssessmentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundRAPInfoID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundRoundID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundTamkeenID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleQuestionnairCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FieldFinanceOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FieldLogisticOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerHired', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerIntroToFSPandLAC', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerIntroToPLO', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerTrainedOnProgramObjectives', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsSCAAdjusted', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KickoffMeetingCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuestionnairCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SCAMeetingCount', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundID'
GO
--End table dbo.CommunityRound

--Begin table dbo.CommunityRoundContact
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRoundContact'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRoundContact
	(
	CommunityRoundContactID INT IDENTITY(1,1) NOT NULL,
	CommunityRoundID INT,
	CommunityID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRoundContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityRoundContact', 'CommunityRoundID,CommunityID,ContactID'
GO
--End table dbo.CommunityRoundContact

--Begin table dbo.CommunityRoundUpate
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRoundUpate'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRoundUpate
	(
	CommunityRoundUpateID INT IDENTITY(1,1) NOT NULL,
	CommunityRoundID INT,
	CommunityUpdates NVARCHAR(MAX),
	ProcessUpdates NVARCHAR(MAX),
	NextPlans NVARCHAR(MAX),
	Risks NVARCHAR(MAX),
	UpdateDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRoundUpateID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityRoundUpate', 'CommunityRoundID'
GO
--End table dbo.CommunityRoundUpate

--Begin table dbo.EntityType
DECLARE @TableName VARCHAR(250) = 'dbo.EntityType'

EXEC utility.AddColumn @TableName, 'HasMenuItemAccessViaWorkflow', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'HasMenuItemAccessViaWorkflow', 'INT', 0
GO
--End table dbo.EntityType

--Begin table dropdown.CommunityRoundCivilDefenseCoverage
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundCivilDefenseCoverage'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundCivilDefenseCoverage
	(
	CommunityRoundCivilDefenseCoverageID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundCivilDefenseCoverageName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundCivilDefenseCoverageID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundCivilDefenseCoverage', @TableName, 'DisplayOrder,CommunityRoundCivilDefenseCoverageName', 'CommunityRoundCivilDefenseCoverageID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundCivilDefenseCoverage ON
GO

INSERT INTO dropdown.CommunityRoundCivilDefenseCoverage (CommunityRoundCivilDefenseCoverageID, CommunityRoundCivilDefenseCoverageName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundCivilDefenseCoverage OFF
GO

INSERT INTO dropdown.CommunityRoundCivilDefenseCoverage 
	(CommunityRoundCivilDefenseCoverageName,DisplayOrder)
VALUES
	(NULL,0),
	('Cluster', 1),
	('CSWG Member', 2),
	('HQ', 3),
	('Not Present', 4)
GO	
--End table dropdown.CommunityRoundCivilDefenseCoverage

--Begin table dropdown.CommunityRoundGroup
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundGroup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundGroup
	(
	CommunityRoundGroupID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundGroupName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundGroupID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundGroup', @TableName, 'DisplayOrder,CommunityRoundGroupName', 'CommunityRoundGroupID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundGroup ON
GO

INSERT INTO dropdown.CommunityRoundGroup (CommunityRoundGroupID, CommunityRoundGroupName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundGroup OFF
GO

INSERT INTO dropdown.CommunityRoundGroup 
	(CommunityRoundGroupName,DisplayOrder)
VALUES
	(NULL,0),
	('Group A', 1),
	('Group B', 2),
	('Group C', 3),
	('Group D', 4),
	('Group E', 5),
	('Group F', 6),
	('Group G', 7)
GO	
--End table dropdown.CommunityRoundGroup

--Begin table dropdown.CommunityRoundJusticeActivity
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundJusticeActivity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundJusticeActivity
	(
	CommunityRoundJusticeActivityID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundJusticeActivityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundJusticeActivityID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundJusticeActivity', @TableName, 'DisplayOrder,CommunityRoundJusticeActivityName', 'CommunityRoundJusticeActivityID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundJusticeActivity ON
GO

INSERT INTO dropdown.CommunityRoundJusticeActivity (CommunityRoundJusticeActivityID, CommunityRoundJusticeActivityName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundJusticeActivity OFF
GO

INSERT INTO dropdown.CommunityRoundJusticeActivity 
	(CommunityRoundJusticeActivityName,DisplayOrder)
VALUES
	(NULL,0),
	('Yes', 1),
	('No', 2),
	('Pilot Project', 3)
GO	
--End table dropdown.CommunityRoundJusticeActivity

--Begin table dropdown.CommunityRoundOutputStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundOutputStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundOutputStatus
	(
	CommunityRoundOutputStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundOutputStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundOutputStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundOutputStatus', @TableName, 'DisplayOrder,CommunityRoundOutputStatusName', 'CommunityRoundOutputStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundOutputStatus ON
GO

INSERT INTO dropdown.CommunityRoundOutputStatus (CommunityRoundOutputStatusID, CommunityRoundOutputStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundOutputStatus OFF
GO

INSERT INTO dropdown.CommunityRoundOutputStatus 
	(CommunityRoundOutputStatusName,DisplayOrder)
VALUES
	(NULL,0),
	('Active', 1),
	('Complete', 2),
	('Hold', 3)
GO	
--End table dropdown.CommunityRoundOutputStatus

--Begin table dropdown.CommunityRoundPreAssessmentStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundPreAssessmentStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundPreAssessmentStatus
	(
	CommunityRoundPreAssessmentStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundPreAssessmentStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundPreAssessmentStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundPreAssessmentStatus', @TableName, 'DisplayOrder,CommunityRoundPreAssessmentStatusName', 'CommunityRoundPreAssessmentStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundPreAssessmentStatus ON
GO

INSERT INTO dropdown.CommunityRoundPreAssessmentStatus (CommunityRoundPreAssessmentStatusID, CommunityRoundPreAssessmentStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundPreAssessmentStatus OFF
GO

INSERT INTO dropdown.CommunityRoundPreAssessmentStatus 
	(CommunityRoundPreAssessmentStatusName,DisplayOrder)
VALUES
	(NULL,0),
	('Active', 1),
	('Complete', 2),
	('Hold', 3)
GO	
--End table dropdown.CommunityRoundPreAssessmentStatus

--Begin table dropdown.CommunityRoundRAPInfo
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundRAPInfo'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundRAPInfo
	(
	CommunityRoundRAPInfoID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundRAPInfoName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundRAPInfoID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundRAPInfo', @TableName, 'DisplayOrder,CommunityRoundRAPInfoName', 'CommunityRoundRAPInfoID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRAPInfo ON
GO

INSERT INTO dropdown.CommunityRoundRAPInfo (CommunityRoundRAPInfoID, CommunityRoundRAPInfoName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRAPInfo OFF
GO

INSERT INTO dropdown.CommunityRoundRAPInfo 
	(CommunityRoundRAPInfoName,DisplayOrder)
VALUES
	(NULL,0),
	('Yes', 1),
	('No', 2),
	('ARAP', 3)
GO	
--End table dropdown.CommunityRoundRAPInfo

--Begin table dropdown.CommunityRoundRound
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundRound'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundRound
	(
	CommunityRoundRoundID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundRoundName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundRoundID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundRound', @TableName, 'DisplayOrder,CommunityRoundRoundName', 'CommunityRoundRoundID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRound ON
GO

INSERT INTO dropdown.CommunityRoundRound (CommunityRoundRoundID, CommunityRoundRoundName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRound OFF
GO

INSERT INTO dropdown.CommunityRoundRound 
	(CommunityRoundRoundName,DisplayOrder)
VALUES
	(NULL,0),
	('Round 1', 1),
	('Round 2', 2),
	('Round 3', 3),
	('Round 4', 4),
	('Round 5', 5),
	('Round 6', 6),
	('Round 7', 7),
	('Round 8', 8),
	('Round 9', 9),
	('Round 10', 10)
GO	
--End table dropdown.CommunityRoundRound

--Begin table dropdown.CommunityRoundTamkeen
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundTamkeen'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundTamkeen
	(
	CommunityRoundTamkeenID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundTamkeenName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundTamkeenID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundTamkeen', @TableName, 'DisplayOrder,CommunityRoundTamkeenName', 'CommunityRoundTamkeenID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundTamkeen ON
GO

INSERT INTO dropdown.CommunityRoundTamkeen (CommunityRoundTamkeenID, CommunityRoundTamkeenName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundTamkeen OFF
GO

INSERT INTO dropdown.CommunityRoundTamkeen 
	(CommunityRoundTamkeenName,DisplayOrder)
VALUES
	(NULL,0),
	('Yes', 1),
	('No', 2)
GO	
--End table dropdown.CommunityRoundTamkeen


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function eventlog.GetCommunityRoundContactXMLByCommunityRoundID
EXEC utility.DropObject 'eventlog.GetCommunityRoundContactXMLByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION eventlog.GetCommunityRoundContactXMLByCommunityRoundID
(
@CommunityRoundID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRoundContacts VARCHAR(MAX) = ''
	
	SELECT @cCommunityRoundContacts = COALESCE(@cCommunityRoundContacts, '') + D.CommunityRoundContact
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRoundContact'), ELEMENTS) AS CommunityRoundContact
		FROM dbo.CommunityRoundContact T 
		WHERE T.CommunityRoundID = @CommunityRoundID
		) D

	RETURN '<CommunityRoundContacts>' + ISNULL(@cCommunityRoundContacts, '') + '</CommunityRoundContacts>'

END
GO
--End function eventlog.GetCommunityRoundContactXMLByCommunityRoundID

--Begin function eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID
EXEC utility.DropObject 'eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID
(
@CommunityRoundID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRoundUpdates VARCHAR(MAX) = ''
	
	SELECT @cCommunityRoundUpdates = COALESCE(@cCommunityRoundUpdates, '') + D.CommunityRoundUpdate
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRoundUpdate'), ELEMENTS) AS CommunityRoundUpdate
		FROM dbo.CommunityRoundUpdate T 
		WHERE T.CommunityRoundID = @CommunityRoundID
		) D

	RETURN '<CommunityRoundUpdates>' + ISNULL(@cCommunityRoundUpdates, '') + '</CommunityRoundUpdates>'

END
GO
--End function eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T ORDER BY T.CommunityProvinceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'FIFUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM fifupdate.FIFUpdate T ORDER BY T.FIFUpdateID DESC
		ELSE IF @EntityTypeCode = 'JusticeUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM justiceupdate.JusticeUpdate T ORDER BY T.JusticeUpdateID DESC
		ELSE IF @EntityTypeCode = 'PoliceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate T ORDER BY T.PoliceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM RiskUpdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM WeeklyReport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
		
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = @nWorkflowStepNumber
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE IF @EntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	CR.CommunityRoundID,
	CR.CommunityID,
	C.CommunityName,
	P.ProvinceID,
	P.ProvinceName,
	CR.CommunityCode,
	CG.CommunityGroupName,
	CSG.CommunitySubGroupName,
	CR.CommunityRoundRoundID,
	CRR.CommunityRoundRoundName,
	CR.CommunityRoundGroupID,
	CRG.CommunityRoundGroupName,
	C.ImpactDecisionID,
	ID.ImpactDecisionName,
	C.CommunityEngagementStatusID,
	CES.CommunityEngagementStatusName,
	CR.AreaManagerPersonID,
	dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
	CR.ProjectOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
	CR.FieldOfficer,
	CR.MOUDate,
	dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
	C.Population,
	C.PolicePresenceCategoryID,
	PPC.PolicePresenceCategoryName,
	CR.CommunityRoundRAPInfoID,
	CRRI.CommunityRoundRAPInfoName,
	CR.CommunityRoundJusticeActivityID,
	CRJA.CommunityRoundJusticeActivityName,
	CR.CommunityRoundTamkeenID,
	CRT.CommunityRoundTamkeenName,
	CR.CommunityRoundCivilDefenseCoverageID,
	CRCDC.CommunityRoundCivilDefenseCoverageName,
	CR.FieldFinanceOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.FieldFinanceOfficerPersonID, 'LastFirstTitle') AS FieldFinanceOfficerPersonName,
	CR.FieldLogisticOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.FieldLogisticOfficerPersonID, 'LastFirstTitle') AS FieldLogisticOfficerPersonName,
	CR.ActivitiesOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
	CR.CommunityRoundOutput11StatusID,
	CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
	CR.CommunityRoundOutput12StatusID,
	CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
	CR.CommunityRoundOutput13StatusID,
	CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
	CR.CommunityRoundOutput14StatusID,
	CROS4.CommunityRoundOutputStatusName AS CommunityRoundOutput14StatusName,
	CR.CommunityRoundPreAssessmentStatusID,
	CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
	CR.IsFieldOfficerHired,
	CR.IsFieldOfficerTrainedOnProgramObjectives,
	CR.IsFieldOfficerIntroToPLO,
	CR.IsFieldOfficerIntroToFSPandLAC,
	CR.ToRDate,
	dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
	CR.SensitizationDefineFSPRoleDate,
	dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
	CR.SensitizationMeetingMOUDate,
	dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
	CR.SensitizationDiscussCommMakeupDate,
	dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
	CR.InitialSCADate,
	dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
	CR.FinalSWOTDate,
	dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
	CR.SCAMeetingCount,
	CR.QuestionnairCount,
	CR.FemaleQuestionnairCount,
	CR.IsSCAAdjusted,
	CR.SCAFinalizedDate,
	dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
	CR.CSAPMeetingDate,
	dbo.FormatDate(CR.CSAPMeetingDate) AS CSAPMeetingDateFormatted, 
	CR.CSAP2PeerReviewMeetingDate,
	dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
	CR.CSAP2SubmittedDate,
	dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
	CR.CSAPSignedDate,
	dbo.FormatDate(CR.CSAPSignedDate) AS CSAPSignedDateFormatted, 
	CR.CSAPBeginDevelopmentDate,
	dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
	CR.CSAPCommunityAnnouncementDate,
	dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
	CR.ApprovedActivityCount,
	CR.KickoffMeetingCount,
	CR.ActivityImplementationStartDate,
	dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
	CR.ActivityImplementationEndDate,
	dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
	(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
		JOIN CONTACT C ON C.ContactID = CCA.ContactID
		WHERE CCA.ContactAffiliationID = 2
		AND CommunityID = CR.CommunityID
		AND C.IsActive = 1
	) AS CSWGCount,
	(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
		JOIN CONTACT C ON C.ContactID = CCA.ContactID
		WHERE CCA.ContactAffiliationID = 2
		AND CommunityID = CR.CommunityID
		AND C.IsActive = 1
		AND C.Gender = 'Male' 
	) AS CSWGMaleCount,
	(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
		JOIN CONTACT C ON C.ContactID = CCA.ContactID
		WHERE CCA.ContactAffiliationID = 2
		AND CommunityID = CR.CommunityID
		AND C.IsActive = 1
		AND C.Gender = 'Female' 
	) AS CSWGFemaleCount,
	CR.IsActive
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS4 ON CROS4.CommunityRoundOutputStatusID = CR.CommunityRoundOutput14StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
	WHERE CR.CommunityRoundID = @CommunityRoundID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityRound'
			AND DE.EntityID = @CommunityRoundID
			AND D.DocumentDescription IN ('MOU Document','TOR Document')
	ORDER BY D.DocumentDescription

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
	ORDER BY 2

	--UpDATEs
	SELECT
		C.CommunityRoundUpDATEID,
		C.CommunityUpDATEs,
		C.ProcessUpDATEs,
		C.NextPlans,
		C.Risks,
		C.UpDATEDate,
		dbo.FormatDate(C.UpDATEDate) AS UpDATEDateFormatted
	FROM dbo.CommunityRoundUpDATE C
	WHERE C.CommunityRoundID = @CommunityRoundID
	ORDER BY C.UpDATEDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not permissionable.PersonPermissionable
-- =====================================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetVettingExpirationCounts
EXEC Utility.DropObject 'dbo.GetVettingExpirationCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A procedure to get counts of expired and soon to expire vetting records
-- ====================================================================================
CREATE PROCEDURE dbo.GetVettingExpirationCounts

AS
BEGIN
	SET NOCOUNT ON;

	SELECT D.*
	FROM
		(
		SELECT
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) < 0) AS UKExpired,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 30) AS UK30,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 31 AND 60) AS UK60,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 61 AND 90) AS UK90,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) < 0) AS USExpired,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 30) AS US30,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 31 AND 60) AS US60,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 61 AND 90) AS US90
		) AS D
		
END
GO
--End procedure dbo.GetVettingExpirationCounts

--Begin procedure dropdown.GetCommunityRoundCivilDefenseCoverageData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundCivilDefenseCoverageData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundCivilDefenseCoverage table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundCivilDefenseCoverageData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundCivilDefenseCoverageID, 
		T.CommunityRoundCivilDefenseCoverageName
	FROM dropdown.CommunityRoundCivilDefenseCoverage T
	WHERE (T.CommunityRoundCivilDefenseCoverageID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundCivilDefenseCoverageName, T.CommunityRoundCivilDefenseCoverageID

END
GO
--End procedure dropdown.GetCommunityRoundCivilDefenseCoverageData

--Begin procedure dropdown.GetCommunityRoundGroupData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundGroup table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundGroupData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundGroupID, 
		T.CommunityRoundGroupName
	FROM dropdown.CommunityRoundGroup T
	WHERE (T.CommunityRoundGroupID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundGroupName, T.CommunityRoundGroupID

END
GO
--End procedure dropdown.GetCommunityRoundGroupData

--Begin procedure dropdown.GetCommunityRoundJusticeActivityData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundJusticeActivityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundJusticeActivity table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundJusticeActivityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundJusticeActivityID, 
		T.CommunityRoundJusticeActivityName
	FROM dropdown.CommunityRoundJusticeActivity T
	WHERE (T.CommunityRoundJusticeActivityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundJusticeActivityName, T.CommunityRoundJusticeActivityID

END
GO
--End procedure dropdown.GetCommunityRoundJusticeActivityData

--Begin procedure dropdown.GetCommunityRoundOutputStatusData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundOutputStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundOutputStatus table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundOutputStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundOutputStatusID, 
		T.CommunityRoundOutputStatusName
	FROM dropdown.CommunityRoundOutputStatus T
	WHERE (T.CommunityRoundOutputStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundOutputStatusName, T.CommunityRoundOutputStatusID

END
GO
--End procedure dropdown.GetCommunityRoundOutputStatusData

--Begin procedure dropdown.GetCommunityRoundPreAssessmentStatusData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundPreAssessmentStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundPreAssessmentStatus table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundPreAssessmentStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundPreAssessmentStatusID, 
		T.CommunityRoundPreAssessmentStatusName
	FROM dropdown.CommunityRoundPreAssessmentStatus T
	WHERE (T.CommunityRoundPreAssessmentStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundPreAssessmentStatusName, T.CommunityRoundPreAssessmentStatusID

END
GO
--End procedure dropdown.GetCommunityRoundPreAssessmentStatusData

--Begin procedure dropdown.GetCommunityRoundRAPInfoData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundRAPInfoData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundRAPInfoData table
-- ================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundRAPInfoData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundRAPInfoID, 
		T.CommunityRoundRAPInfoName
	FROM dropdown.CommunityRoundRAPInfo T
	WHERE (T.CommunityRoundRAPInfoID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundRAPInfoName, T.CommunityRoundRAPInfoID

END
GO
--End procedure dropdown.GetCommunityRoundRAPInfoData

--Begin procedure dropdown.GetCommunityRoundRoundData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundRoundData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundRound table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundRoundData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundRoundID, 
		T.CommunityRoundRoundName
	FROM dropdown.CommunityRoundRound T
	WHERE (T.CommunityRoundRoundID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundRoundName, T.CommunityRoundRoundID

END
GO
--End procedure dropdown.GetCommunityRoundRoundData

--Begin procedure dropdown.GetCommunityRoundTamkeenData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundTamkeenData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundTamkeen table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundTamkeenData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundTamkeenID, 
		T.CommunityRoundTamkeenName
	FROM dropdown.CommunityRoundTamkeen T
	WHERE (T.CommunityRoundTamkeenID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundTamkeenName, T.CommunityRoundTamkeenID

END
GO
--End procedure dropdown.GetCommunityRoundTamkeenData

--Begin procedure eventlog.LogCommunityRoundAction
EXEC utility.DropObject 'eventlog.LogCommunityRoundAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityRoundAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityRound',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('CommunityRound', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityRound',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityRoundContactXMLByCommunityRoundID(T.CommunityRoundID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID(T.CommunityRoundID) AS XML)),
			CAST(@cDocuments AS XML) 
			FOR XML RAW('CommunityRound'), ELEMENTS
			)
		FROM dbo.CommunityRound T
		WHERE T.CommunityRoundID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityRoundAction

--Begin procedure eventlog.LogServerSetupAction
EXEC utility.DropObject 'eventlog.LogServerSetupAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogServerSetupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogServerSetupActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogServerSetupActionTable
		--ENDIF
		
		SELECT *
		INTO #LogServerSetupActionTable
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupID = @EntityID
		
		ALTER TABLE #LogServerSetupActionTable DROP COLUMN ServerSetupBinaryValue
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('ServerSetup'), ELEMENTS
			)
		FROM #LogServerSetupActionTable T
			JOIN dbo.ServerSetup SS ON SS.ServerSetupID = T.ServerSetupID

		DROP TABLE #LogServerSetupActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogServerSetupAction

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Todd Pires
-- Create Date: 2016.04.21
-- Description:	Added the HasActiveAssignement bit
-- ============================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.WorkflowID,	
		W.WorkflowName,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.WorkflowID = W.WorkflowID AND EWSGP.IsComplete = 0)
			THEN 1
			ELSE 0
		END AS HasActiveAssignement
		
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		dbo.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
	ORDER BY 5, WSGP.PersonID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'ConceptNote' AND ET.WorkflowActionCode = 'ConceptNoteBeneficiaryVetting')
	BEGIN

	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode,WorkflowActionCode,EmailText)
	VALUES
		('ConceptNote','ConceptNoteBeneficiaryVetting','<p>Dear [[FirstName]] [[LastName]]<br /><br />There are [[ContactCount]] beneficiaries associated with activity [[Title]] that may require vetting action.<br /><br />[[Comments]]</p>')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'Vetting' AND ET.WorkflowActionCode = 'BeneficiaryVettingListEmail')
	BEGIN

	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode,WorkflowActionCode,EmailText)
	VALUES
		('Vetting','BeneficiaryVettingListEmail','<p>There are [[ContactCount]] contacts that are pending internal review as a vetting action. Please use the KMS to filter for these contacts and action accordingly.<br /><br />[[Comments]]</p>')

    END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Vetting' AND ETF.PlaceHolderText = '[[ContactCount]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('Vetting','[[ContactCount]]','Contact Count',1)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Vetting' AND ETF.PlaceHolderText = '[[Comments]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('Vetting','[[Comments]]','Comments',2)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'ConceptNote' AND ETF.PlaceHolderText = '[[ContactCount]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('ConceptNote','[[ContactCount]]','Contact Count',8)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
UPDATE ET
SET ET.HasMenuItemAccessViaWorkflow = 1
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode IN ('CommunityProvinceEngagementUpdate','FIFUpdate','JusticeUpdate','PoliceEngagementUpdate','RecommendationUpdate','RiskUpdate','WeeklyReport')
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE MI
SET 
	MI.EntityTypeCode = 'CommunityProvinceEngagementUpdate',
	MI.MenuItemCode = 'CommunityProvinceEngagementUpdate'	
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'CommunityProvenceEngagementUpdate'
GO
--End table dbo.MenuItem

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
	JOIN dbo.EntityType ET ON ET.EntityTypeCode = MI.EntityTypeCode
		AND ET.HasMenuItemAccessViaWorkflow = 1
GO
--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.Permissionable
DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityProvinceEngagement','FIFUpdate','Justice','PoliceEngagement','WeeklyReport')
	AND P.PermissionCode <> 'Export'
GO

EXEC permissionable.DeletePermissionable 0
GO
--End table permissionable.Permissionable

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Community', 'Community', 0
GO
EXEC utility.SavePermissionableGroup 'CommunityRound', 'CommunityRound', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contact', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'Implementation', 'Implementation', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Insight & Understanding', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Province', 'Province', 0
GO
EXEC utility.SavePermissionableGroup 'RapidAssessments', 'Rapid Assessments', 0
GO
EXEC utility.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflows', 'Workflows', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Data Export', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'DataImport', 'Default', NULL, 'DataImport.Default', 'Data Import', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'AddUpdate', NULL, 'EmailTemplateAdministration.AddUpdate', 'Add / edit an email template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'List EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'View', NULL, 'EmailTemplateAdministration.View', 'View EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'List EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Exports', 'BusinessLicenseReport', NULL, 'Exports.BusinessLicenseReport', 'Business License Report Exports', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors SiteConfiguration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'Delete', NULL, 'Permissionable.Delete', 'Delete a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'List PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'Delete', NULL, 'Person.Delete', 'Delete a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'List ServerSetup', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Community', 'AddUpdate', NULL, 'Community.AddUpdate', 'Add / edit a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'List Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'View the analysis tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Export', 'Community.View.Export', 'Export Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'ExportEquipmentDistribution', 'Community.View.ExportEquipmentDistribution', 'Export Equipment Distributions Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'Implementation Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'View the information tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'AddUpdate', NULL, 'CommunityRound.AddUpdate', 'CommunityRound.AddUpdate', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'List', NULL, 'CommunityRound.List', 'CommunityRound.List', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'View', NULL, 'CommunityRound.View', 'CommunityRound.View', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'CETeam', 'Contact.AddUpdate.CETeam', 'Add / edit contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'FieldStaff', 'Contact.AddUpdate.FieldStaff', 'Add / edit contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'IO4', 'Contact.AddUpdate.IO4', 'Add / edit contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeOther', 'Contact.AddUpdate.JusticeOther', 'Add / edit contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeStipend', 'Contact.AddUpdate.JusticeStipend', 'Add / edit contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersStakeholder', 'Contact.AddUpdate.PartnersStakeholder', 'Add / edit contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersSupplierVendor', 'Contact.AddUpdate.PartnersSupplierVendor', 'Add / edit contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceOther', 'Contact.AddUpdate.PoliceOther', 'Add / edit contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceStipend', 'Contact.AddUpdate.PoliceStipend', 'Add / edit contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'ProjectStaff', 'Contact.AddUpdate.ProjectStaff', 'Add / edit contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'SubContractors', 'Contact.AddUpdate.SubContractors', 'Add / edit contacts of type Sub-Contractor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'CETeam', 'Contact.List.CETeam', 'Include contacts of type CE Team in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ExportPayees', 'Contact.List.ExportPayees', 'Export payees from the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'FieldStaff', 'Contact.List.FieldStaff', 'Include contacts of type Field Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'IO4', 'Contact.List.IO4', 'Include contacts of type IO4 in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeOther', 'Contact.List.JusticeOther', 'Include contacts of type Justice Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeStipend', 'Contact.List.JusticeStipend', 'Include contacts of type Justice Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersStakeholder', 'Contact.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersSupplierVendor', 'Contact.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceOther', 'Contact.List.PoliceOther', 'Include contacts of type Police Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceStipend', 'Contact.List.PoliceStipend', 'Include contacts of type Police Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ProjectStaff', 'Contact.List.ProjectStaff', 'Include contacts of type Project Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'SubContractors', 'Contact.List.SubContractors', 'Include contacts of type Sub-Contractor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', NULL, 'Contact.PaymentList', 'View the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'CashHandOverExport', 'Contact.PaymentList.CashHandOverExport', 'Export the cash handover report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'OpFundsReport', 'Contact.PaymentList.OpFundsReport', 'Export the op funds report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendActivity', 'Contact.PaymentList.StipendActivity', 'Export the stipend activity report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendPaymentReport', 'Contact.PaymentList.StipendPaymentReport', 'Export the stipend payment report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'CETeam', 'Contact.View.CETeam', 'View contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'FieldStaff', 'Contact.View.FieldStaff', 'View contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'IO4', 'Contact.View.IO4', 'View contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeOther', 'Contact.View.JusticeOther', 'View contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeStipend', 'Contact.View.JusticeStipend', 'View contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersStakeholder', 'Contact.View.PartnersStakeholder', 'View contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersSupplierVendor', 'Contact.View.PartnersSupplierVendor', 'View contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceOther', 'Contact.View.PoliceOther', 'View contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceStipend', 'Contact.View.PoliceStipend', 'View contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'ProjectStaff', 'Contact.View.ProjectStaff', 'View contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'SubContractors', 'Contact.View.SubContractors', 'View contacts of type Sub-Contractors', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'VettingMoreInfo', 'Contact.View.VettingMoreInfo', 'View the more info button on the vetting history data table', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'CETeam', 'Vetting.List.CETeam', 'Include contacts of type CE Team in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'Export', 'Vetting.List.Export', 'Export the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'FieldStaff', 'Vetting.List.FieldStaff', 'Include contacts of type Field Staff in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'IO4', 'Vetting.List.IO4', 'Include contacts of type IO4 in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeOther', 'Vetting.List.JusticeOther', 'Include contacts of type Justice Other in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeStipend', 'Vetting.List.JusticeStipend', 'Include contacts of type Justice Stipend in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersStakeholder', 'Vetting.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersSupplierVendor', 'Vetting.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceOther', 'Vetting.List.PoliceOther', 'Include contacts of type Police Other in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceStipend', 'Vetting.List.PoliceStipend', 'Include contacts of type Police Stipend in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'ProjectStaff', 'Vetting.List.ProjectStaff', 'Include contacts of type Project Staff in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'SubContractors', 'Vetting.List.SubContractors', 'Include contacts of type Sub-Contractor in the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'UpdateVetting', 'Vetting.List.UpdateVetting', 'Update vetting data on the vetting list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'Notification', 'ExpirationCountEmail', 'Vetting.Notification.ExpirationCountEmail', 'Recieve the monthly vetting expiration counts e-mail', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', NULL, 'Document.AddUpdate', '', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '004', 'Document.AddUpdate.004', 'Add / edit 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '100', 'Document.AddUpdate.100', 'Add / edit 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '101', 'Document.AddUpdate.101', 'Add / edit 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '102', 'Document.AddUpdate.102', 'Add / edit 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '103', 'Document.AddUpdate.103', 'Add / edit 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '104', 'Document.AddUpdate.104', 'Add / edit 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '105', 'Document.AddUpdate.105', 'Add / edit 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '106', 'Document.AddUpdate.106', 'Add / edit 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '107', 'Document.AddUpdate.107', 'Add / edit 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '108', 'Document.AddUpdate.108', 'Add / edit 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '109', 'Document.AddUpdate.109', 'Add / edit 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '301', 'Document.AddUpdate.301', 'Add / edit 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '302', 'Document.AddUpdate.302', 'Add / edit 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '303', 'Document.AddUpdate.303', 'Add / edit 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '304', 'Document.AddUpdate.304', 'Add / edit 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '306', 'Document.AddUpdate.306', 'Add / edit 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '500', 'Document.AddUpdate.500', 'Add / edit 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '501', 'Document.AddUpdate.501', 'Add / edit 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '502', 'Document.AddUpdate.502', 'Add / edit 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '503', 'Document.AddUpdate.503', 'Add / edit 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '504', 'Document.AddUpdate.504', 'Add / edit 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '505', 'Document.AddUpdate.505', 'Add / edit 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '506', 'Document.AddUpdate.506', 'Add / edit 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '507', 'Document.AddUpdate.507', 'Add / edit 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '508', 'Document.AddUpdate.508', 'Add / edit 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '509', 'Document.AddUpdate.509', 'Add / edit 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '510', 'Document.AddUpdate.510', 'Add / edit 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '511', 'Document.AddUpdate.511', 'Add / edit 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '512', 'Document.AddUpdate.512', 'Add / edit 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '513', 'Document.AddUpdate.513', 'Add / edit 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '514', 'Document.AddUpdate.514', 'Add / edit 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '515', 'Document.AddUpdate.515', 'Add / edit 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '516', 'Document.AddUpdate.516', 'Add / edit 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '517', 'Document.AddUpdate.517', 'Add / edit 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '518', 'Document.AddUpdate.518', 'Add / edit 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '519', 'Document.AddUpdate.519', 'Add / edit 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '520', 'Document.AddUpdate.520', 'Add / edit 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '521', 'Document.AddUpdate.521', 'Add / edit 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '600', 'Document.AddUpdate.600', 'Add / edit 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '601', 'Document.AddUpdate.601', 'Add / edit 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '602', 'Document.AddUpdate.602', 'Add / edit 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '603', 'Document.AddUpdate.603', 'Add / edit 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '604', 'Document.AddUpdate.604', 'Add / edit 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '605', 'Document.AddUpdate.605', 'Add / edit 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '700', 'Document.AddUpdate.700', 'Add / edit 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '701', 'Document.AddUpdate.701', 'Add / edit 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '702', 'Document.AddUpdate.702', 'Add / edit 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '801', 'Document.AddUpdate.801', 'Add / edit 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '802', 'Document.AddUpdate.802', 'Add / edit 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '803', 'Document.AddUpdate.803', 'Add / edit 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '804', 'Document.AddUpdate.804', 'Add / edit 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '805', 'Document.AddUpdate.805', 'Add / edit 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '806', 'Document.AddUpdate.806', 'Add / edit 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '807', 'Document.AddUpdate.807', 'Add / edit 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '808', 'Document.AddUpdate.808', 'Add / edit 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '900', 'Document.AddUpdate.900', 'Add / edit 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '901', 'Document.AddUpdate.901', 'Add / edit 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '902', 'Document.AddUpdate.902', 'Add / edit 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '903', 'Document.AddUpdate.903', 'Add / edit 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '904', 'Document.AddUpdate.904', 'Add / edit 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '905', 'Document.AddUpdate.905', 'Add / edit 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '906', 'Document.AddUpdate.906', 'Add / edit 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '907', 'Document.AddUpdate.907', 'Add / edit 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '908', 'Document.AddUpdate.908', 'Add / edit 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '909', 'Document.AddUpdate.909', 'Add / edit 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '910', 'Document.AddUpdate.910', 'Add / edit 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '004', 'Document.View.004', 'View 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '100', 'Document.View.100', 'View 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '101', 'Document.View.101', 'View 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '102', 'Document.View.102', 'View 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '103', 'Document.View.103', 'View 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '104', 'Document.View.104', 'View 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '105', 'Document.View.105', 'View 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '106', 'Document.View.106', 'View 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '107', 'Document.View.107', 'View 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '108', 'Document.View.108', 'View 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '109', 'Document.View.109', 'View 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '301', 'Document.View.301', 'View 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '302', 'Document.View.302', 'View 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '303', 'Document.View.303', 'View 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '304', 'Document.View.304', 'View 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '306', 'Document.View.306', 'View 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '500', 'Document.View.500', 'View 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '501', 'Document.View.501', 'View 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '502', 'Document.View.502', 'View 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '503', 'Document.View.503', 'View 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '504', 'Document.View.504', 'View 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '505', 'Document.View.505', 'View 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '506', 'Document.View.506', 'View 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '507', 'Document.View.507', 'View 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '508', 'Document.View.508', 'View 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '509', 'Document.View.509', 'View 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '510', 'Document.View.510', 'View 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '511', 'Document.View.511', 'View 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '512', 'Document.View.512', 'View 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '513', 'Document.View.513', 'View 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '514', 'Document.View.514', 'View 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '515', 'Document.View.515', 'View 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '516', 'Document.View.516', 'View 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '517', 'Document.View.517', 'View 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '518', 'Document.View.518', 'View 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '519', 'Document.View.519', 'View 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '520', 'Document.View.520', 'View 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '521', 'Document.View.521', 'View 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '600', 'Document.View.600', 'View 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '601', 'Document.View.601', 'View 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '602', 'Document.View.602', 'View 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '603', 'Document.View.603', 'View 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '604', 'Document.View.604', 'View 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '605', 'Document.View.605', 'View 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '700', 'Document.View.700', 'View 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '701', 'Document.View.701', 'View 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '702', 'Document.View.702', 'View 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '801', 'Document.View.801', 'View 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '802', 'Document.View.802', 'View 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '803', 'Document.View.803', 'View 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '804', 'Document.View.804', 'View 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '805', 'Document.View.805', 'View 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '806', 'Document.View.806', 'View 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '807', 'Document.View.807', 'View 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '808', 'Document.View.808', 'View 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '900', 'Document.View.900', 'View 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '901', 'Document.View.901', 'View 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '902', 'Document.View.902', 'View 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '903', 'Document.View.903', 'View 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '904', 'Document.View.904', 'View 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '905', 'Document.View.905', 'View 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '906', 'Document.View.906', 'View 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '907', 'Document.View.907', 'View 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '908', 'Document.View.908', 'View 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '909', 'Document.View.909', 'View 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '910', 'Document.View.910', 'View 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateDecision', NULL, 'DonorDecision.AddUpdateDecision', 'Add / edit a donor decision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateMeeting', NULL, 'DonorDecision.AddUpdateMeeting', 'Add / edit donor meetings & actions', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'List', NULL, 'DonorDecision.List', 'List Donor Decisions, Meetings & Actions DonorDecision', 1, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'View', NULL, 'DonorDecision.View', 'View DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'AddUpdate', NULL, 'EquipmentCatalog.AddUpdate', 'Add / edit the equipment catalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'List', NULL, 'EquipmentCatalog.List', 'List EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'View', NULL, 'EquipmentCatalog.View', 'View EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Audit', NULL, 'EquipmentDistribution.Audit', 'EquipmentDistribution.Audit', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'EquipmentDistribution.Create', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'FinalizeEquipmentDistribution', NULL, 'EquipmentDistribution.FinalizeEquipmentDistribution', 'EquipmentDistribution.FinalizeEquipmentDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistributedInventory', NULL, 'EquipmentDistribution.ListDistributedInventory', 'EquipmentDistribution.ListDistributedInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistribution', NULL, 'EquipmentDistribution.ListDistribution', 'EquipmentDistribution.ListDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListInventory', NULL, 'EquipmentDistribution.ListInventory', 'EquipmentDistribution.ListInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'SetDeliveryDate', NULL, 'EquipmentDistribution.SetDeliveryDate', 'EquipmentDistribution.SetDeliveryDate', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Transfer', NULL, 'EquipmentDistribution.Transfer', 'EquipmentDistribution.Transfer', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'AddUpdate', NULL, 'EquipmentDistributionPlan.AddUpdate', 'Add / edit an equipment distribution plan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'List', NULL, 'EquipmentDistributionPlan.List', 'List EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', NULL, 'EquipmentDistributionPlan.View', 'View EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', 'EquipmentDistributionPlan', 'EquipmentDistributionPlan.View.EquipmentDistributionPlan', 'Export Equipment Distribution Plan EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', 'EquipmentHandoverSheet', 'EquipmentDistributionPlan.View.EquipmentHandoverSheet', 'Export Equipment Handover Sheet EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit the equipment inventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'List EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', 'Export', 'EquipmentInventory.List.Export', 'Export EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'Audit', NULL, 'EquipmentManagement.Audit', 'Audit Equipment EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'List', NULL, 'EquipmentManagement.List', 'List Equipment Locations EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'AddUpdate', NULL, 'CommunityAsset.AddUpdate', 'Add / edit a community asset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'List', NULL, 'CommunityAsset.List', 'List CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'View', NULL, 'CommunityAsset.View', 'View CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'AddUpdate', NULL, 'CommunityMemberSurvey.AddUpdate', 'Add / edit a community member survey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'View', NULL, 'CommunityMemberSurvey.View', 'View CommunityMemberSurvey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagement', 'AddUpdate', NULL, 'CommunityProvinceEngagement.AddUpdate', 'Add / edit a community province engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagementUpdate', 'Export', NULL, 'CommunityProvinceEngagementUpdate.Export', 'Export CommunityProvinceEngagementUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'FIFUpdate', 'View', NULL, 'FIFUpdate.View', 'View FIFUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Justice', 'AddUpdate', NULL, 'Justice.AddUpdate', 'Add / edit a justice report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'AddUpdate', NULL, 'KeyEvent.AddUpdate', 'Add / edit a key event', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'List', NULL, 'KeyEvent.List', 'List KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'View', NULL, 'KeyEvent.View', 'View KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'PoliceEngagement', 'AddUpdate', NULL, 'PoliceEngagement.AddUpdate', 'Add / edit a police engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'AddUpdate', NULL, 'Project.AddUpdate', 'Add / edit a project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'List', NULL, 'Project.List', 'List Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'View', NULL, 'Project.View', 'View Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'AddUpdate', NULL, 'Atmospheric.AddUpdate', 'Add / edit an atmospheric report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'List Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'AddUpdate', NULL, 'Finding.AddUpdate', 'Add / edit a finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'List Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'AddUpdate', NULL, 'Force.AddUpdate', 'Add / edit a force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'List Forces Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'AddUpdate', NULL, 'Incident.AddUpdate', 'Add / edit an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'List Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'AddUpdate', NULL, 'Recommendation.AddUpdate', 'Add / edit a recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'List Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', 'Export', 'Recommendation.List.Export', 'Export Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Add', NULL, 'RequestForInformation.Add', 'Add a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'List RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', 'Export', 'RequestForInformation.List.Export', 'Export RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', NULL, 'RequestForInformation.Update', 'Edit a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', 'Amend', 'RequestForInformation.Update.Amend', 'Amend a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', NULL, 'Risk.AddUpdate', 'Add / edit a risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'List Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', 'Export', 'Risk.List.Export', 'Export Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', NULL, 'SpotReport.AddUpdate', 'Add / edit a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', 'Amend', 'SpotReport.AddUpdate.Amend', 'Amend a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'List SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Approved', 'SpotReport.View.Approved', 'Approved SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Export', 'SpotReport.View.Export', 'Export SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'InWork', 'SpotReport.View.InWork', 'In Work SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Add / edit a survey question', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Add / edit a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Administer a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Export Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListQuestions', NULL, 'SurveyManagement.ListQuestions', 'Questions List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveyResponses', NULL, 'SurveyManagement.ListSurveyResponses', 'List Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Surveys List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'View Questions SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'View Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', NULL, 'WeeklyReport.AddUpdate', 'Add / edit a weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', 'Export', 'WeeklyReport.AddUpdate.Export', 'Export WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'List', NULL, 'WeeklyReport.List', 'List WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'View', NULL, 'WeeklyReport.View', 'View WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Indicator', 'AddUpdate', NULL, 'Indicator.AddUpdate', 'Add / edit an indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'List', NULL, 'Indicator.List', 'List Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'View', NULL, 'Indicator.View', 'View Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'AddUpdate', NULL, 'IndicatorType.AddUpdate', 'Add / edit an indicatortype', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'List', NULL, 'IndicatorType.List', 'List IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'View', NULL, 'IndicatorType.View', 'View IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'AddUpdate', NULL, 'Milestone.AddUpdate', 'Add / edit a milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'List', NULL, 'Milestone.List', 'List Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'View', NULL, 'Milestone.View', 'View Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'AddUpdate', NULL, 'Objective.AddUpdate', 'Add / edit an objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', NULL, 'Objective.ChartList', 'M & E Overview Charts Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'List', NULL, 'Objective.List', 'List Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage Objectives & Indicators Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'Overview Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'View', NULL, 'Objective.View', 'View Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', NULL, 'ConceptNote.AddUpdate', 'Add / edit a concep nNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'ExportConceptNoteBudget', 'ConceptNote.AddUpdate.ExportConceptNoteBudget', 'Export Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'Finances', 'ConceptNote.AddUpdate.Finances', 'Add / edit activity finances', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', NULL, 'ConceptNote.List', 'List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', 'Export', 'ConceptNote.List.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', NULL, 'ConceptNote.VettingList', 'Vetting List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', 'ExportVetting', 'ConceptNote.VettingList.ExportVetting', 'Export Vetting ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', NULL, 'ConceptNote.View', 'View ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'Export', 'ConceptNote.View.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'ViewBudget', 'ConceptNote.View.ViewBudget', 'View Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'AddUpdate', NULL, 'ConceptNoteContactEquipment.AddUpdate', 'Add / edit equipment associated with a concept note', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'FinalizeEquipmentDistribution', NULL, 'ConceptNoteContactEquipment.FinalizeEquipmentDistribution', 'Finalize Equipment Distribution ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'List', NULL, 'ConceptNoteContactEquipment.List', 'List ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', NULL, 'ConceptNoteContactEquipment.View', 'View ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', 'Export', 'ConceptNoteContactEquipment.View.Export', 'Export ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'AddUpdate', NULL, 'License.AddUpdate', 'Add / edit a license', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'List', NULL, 'License.List', 'List License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'View', NULL, 'License.View', 'View License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'AddUpdate', NULL, 'LicenseEquipmentCatalog.AddUpdate', 'Add / edit the license equipment catalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', NULL, 'LicenseEquipmentCatalog.List', 'List LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', 'Export', 'LicenseEquipmentCatalog.List.Export', 'Export LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'View', NULL, 'LicenseEquipmentCatalog.View', 'View LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'AddUpdate', NULL, 'PurchaseRequest.AddUpdate', 'Add / edit a purchase request', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'List', NULL, 'PurchaseRequest.List', 'List PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', NULL, 'PurchaseRequest.View', 'View PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', 'Export', 'PurchaseRequest.View.Export', 'Export PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'AddUpdate', NULL, 'ProgramReport.AddUpdate', 'Add / edit a program report', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'List', NULL, 'ProgramReport.List', 'List ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', NULL, 'ProgramReport.View', 'View ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', 'Export', 'ProgramReport.View.Export', 'Export ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'Province', 'AddUpdate', NULL, 'Province.AddUpdate', 'Add / edit a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'List', NULL, 'Province.List', 'List Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', NULL, 'Province.View', 'View Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Analysis', 'Province.View.Analysis', 'View the analysis tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'ExportEquipmentDistribution', 'Province.View.ExportEquipmentDistribution', 'Export Equipment Distributions Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Implementation', 'Province.View.Implementation', 'Implementation Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Information', 'Province.View.Information', 'View the information tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'DailyReport', 'AddUpdate', NULL, 'DailyReport.AddUpdate', 'Add / edit a daily report', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'List', NULL, 'DailyReport.List', 'List DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'View', NULL, 'DailyReport.View', 'View DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'AddUpdate', NULL, 'FocusGroupSurvey.AddUpdate', 'Add / edit a focus group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'View', NULL, 'FocusGroupSurvey.View', 'View FocusGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'AddUpdate', NULL, 'KeyInformantSurvey.AddUpdate', 'Add / edit a key informant survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'View', NULL, 'KeyInformantSurvey.View', 'View KeyInformantSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', NULL, 'RAPData.List', 'List RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', 'Export', 'RAPData.List.Export', 'Export RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RapidPerceptionSurvey', 'View', NULL, 'RapidPerceptionSurvey.View', 'View RapidPerceptionSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'AddUpdate', NULL, 'StakeholderGroupSurvey.AddUpdate', 'Add / edit a stakeholder group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'View', NULL, 'StakeholderGroupSurvey.View', 'View StakeholderGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'AddUpdate', NULL, 'StationCommanderSurvey.AddUpdate', 'Add / edit a station commander survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'View', NULL, 'StationCommanderSurvey.View', 'View StationCommanderSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'AddUpdate', NULL, 'Team.AddUpdate', 'Add / edit a team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'List', NULL, 'Team.List', 'List Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'View', NULL, 'Team.View', 'View Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'SubContractor', 'AddUpdate', NULL, 'SubContractor.AddUpdate', 'Add / edit a sub-contractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'List', NULL, 'SubContractor.List', 'List SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'View', NULL, 'SubContractor.View', 'View SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / edit a class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'List Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / edit a course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'List Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the list of workflows', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflows'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP FROM permissionable.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table permissionable.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.55 File 01 - AJACS - 2016.04.29 20.59.30')
GO
--End build tracking

