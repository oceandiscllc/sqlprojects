USE AJACS
GO

--Begin table dbo.CommunityRound
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRound'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRound
	(
	CommunityRoundID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	CommunityCode NVARCHAR(25),
	CommunityRoundRoundID INT,
	CommunityRoundGroupID INT,
	AreaManagerPersonID INT,
	ProjectOfficerPersonID INT,
	CommunityRoundRAPInfoID INT,
	CommunityRoundJusticeActivityID INT,
	CommunityRoundTamkeenID INT,
	CommunityRoundCivilDefenseCoverageID INT,
	FieldOfficer NVARCHAR(25),
	MOUDate DATE,
	FieldFinanceOfficerPersonID INT,
	FieldLogisticOfficerPersonID INT,
	ActivitiesOfficerPersonID INT,
	CommunityRoundOutput11StatusID INT,
	CommunityRoundOutput12StatusID INT,
	CommunityRoundOutput13StatusID INT,
	CommunityRoundOutput14StatusID INT,
	CommunityRoundPreAssessmentStatusID INT,
	IsFieldOfficerHired BIT,
	IsFieldOfficerTrainedOnProgramObjectives BIT,
	IsFieldOfficerIntroToPLO BIT,
	IsFieldOfficerIntroToFSPandLAC BIT,
	ToRDate DATE,
	SensitizationDefineFSPRoleDate DATE,
	SensitizationMeetingMOUDate DATE,
	SensitizationDiscussCommMakeupDate DATE,
	InitialSCADate DATE,
	FinalSWOTDate DATE,
	SCAMeetingCount INT,
	QuestionnairCount INT,
	FemaleQuestionnairCount INT,
	IsSCAAdjusted BIT,
	SCAFinalizedDate DATE,
	CSAPMeetingDate DATE,
	CSAP2PeerReviewMeetingDate DATE,
	CSAP2SubmittedDate DATE,
	CSAPSignedDate DATE,
	CSAPBeginDevelopmentDate DATE,
	CSAPCommunityAnnouncementDate DATE,
	ApprovedActivityCount INT,
	KickoffMeetingCount INT,
	ActivityImplementationStartDate DATE,
	ActivityImplementationEndDate DATE,
	IsActive BIT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'ActivitiesOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ApprovedActivityCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AreaManagerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundCivilDefenseCoverageID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundJusticeActivityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput11StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput12StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput13StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundOutput14StatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundPreAssessmentStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundRAPInfoID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundRoundID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundTamkeenID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleQuestionnairCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FieldFinanceOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FieldLogisticOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerHired', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerIntroToFSPandLAC', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerIntroToPLO', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsFieldOfficerTrainedOnProgramObjectives', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsSCAAdjusted', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'KickoffMeetingCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectOfficerPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'QuestionnairCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SCAMeetingCount', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundID'
GO
--End table dbo.CommunityRound

--Begin table dbo.CommunityRoundContact
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRoundContact'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRoundContact
	(
	CommunityRoundContactID INT IDENTITY(1,1) NOT NULL,
	CommunityRoundID INT,
	CommunityID INT,
	ContactID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRoundContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityRoundContact', 'CommunityRoundID,CommunityID,ContactID'
GO
--End table dbo.CommunityRoundContact

--Begin table dbo.CommunityRoundUpate
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRoundUpate'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityRoundUpate
	(
	CommunityRoundUpateID INT IDENTITY(1,1) NOT NULL,
	CommunityRoundID INT,
	CommunityUpdates NVARCHAR(MAX),
	ProcessUpdates NVARCHAR(MAX),
	NextPlans NVARCHAR(MAX),
	Risks NVARCHAR(MAX),
	UpdateDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityRoundID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRoundUpateID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityRoundUpate', 'CommunityRoundID'
GO
--End table dbo.CommunityRoundUpate

--Begin table dbo.EntityType
DECLARE @TableName VARCHAR(250) = 'dbo.EntityType'

EXEC utility.AddColumn @TableName, 'HasMenuItemAccessViaWorkflow', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'HasMenuItemAccessViaWorkflow', 'INT', 0
GO
--End table dbo.EntityType

--Begin table dropdown.CommunityRoundCivilDefenseCoverage
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundCivilDefenseCoverage'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundCivilDefenseCoverage
	(
	CommunityRoundCivilDefenseCoverageID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundCivilDefenseCoverageName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundCivilDefenseCoverageID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundCivilDefenseCoverage', @TableName, 'DisplayOrder,CommunityRoundCivilDefenseCoverageName', 'CommunityRoundCivilDefenseCoverageID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundCivilDefenseCoverage ON
GO

INSERT INTO dropdown.CommunityRoundCivilDefenseCoverage (CommunityRoundCivilDefenseCoverageID, CommunityRoundCivilDefenseCoverageName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundCivilDefenseCoverage OFF
GO

INSERT INTO dropdown.CommunityRoundCivilDefenseCoverage 
	(CommunityRoundCivilDefenseCoverageName,DisplayOrder)
VALUES
	(NULL,0),
	('Cluster', 1),
	('CSWG Member', 2),
	('HQ', 3),
	('Not Present', 4)
GO	
--End table dropdown.CommunityRoundCivilDefenseCoverage

--Begin table dropdown.CommunityRoundGroup
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundGroup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundGroup
	(
	CommunityRoundGroupID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundGroupName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundGroupID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundGroup', @TableName, 'DisplayOrder,CommunityRoundGroupName', 'CommunityRoundGroupID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundGroup ON
GO

INSERT INTO dropdown.CommunityRoundGroup (CommunityRoundGroupID, CommunityRoundGroupName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundGroup OFF
GO

INSERT INTO dropdown.CommunityRoundGroup 
	(CommunityRoundGroupName,DisplayOrder)
VALUES
	(NULL,0),
	('Group A', 1),
	('Group B', 2),
	('Group C', 3),
	('Group D', 4),
	('Group E', 5),
	('Group F', 6),
	('Group G', 7)
GO	
--End table dropdown.CommunityRoundGroup

--Begin table dropdown.CommunityRoundJusticeActivity
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundJusticeActivity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundJusticeActivity
	(
	CommunityRoundJusticeActivityID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundJusticeActivityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundJusticeActivityID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundJusticeActivity', @TableName, 'DisplayOrder,CommunityRoundJusticeActivityName', 'CommunityRoundJusticeActivityID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundJusticeActivity ON
GO

INSERT INTO dropdown.CommunityRoundJusticeActivity (CommunityRoundJusticeActivityID, CommunityRoundJusticeActivityName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundJusticeActivity OFF
GO

INSERT INTO dropdown.CommunityRoundJusticeActivity 
	(CommunityRoundJusticeActivityName,DisplayOrder)
VALUES
	(NULL,0),
	('Yes', 1),
	('No', 2),
	('Pilot Project', 3)
GO	
--End table dropdown.CommunityRoundJusticeActivity

--Begin table dropdown.CommunityRoundOutputStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundOutputStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundOutputStatus
	(
	CommunityRoundOutputStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundOutputStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundOutputStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundOutputStatus', @TableName, 'DisplayOrder,CommunityRoundOutputStatusName', 'CommunityRoundOutputStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundOutputStatus ON
GO

INSERT INTO dropdown.CommunityRoundOutputStatus (CommunityRoundOutputStatusID, CommunityRoundOutputStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundOutputStatus OFF
GO

INSERT INTO dropdown.CommunityRoundOutputStatus 
	(CommunityRoundOutputStatusName,DisplayOrder)
VALUES
	(NULL,0),
	('Active', 1),
	('Complete', 2),
	('Hold', 3)
GO	
--End table dropdown.CommunityRoundOutputStatus

--Begin table dropdown.CommunityRoundPreAssessmentStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundPreAssessmentStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundPreAssessmentStatus
	(
	CommunityRoundPreAssessmentStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundPreAssessmentStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundPreAssessmentStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundPreAssessmentStatus', @TableName, 'DisplayOrder,CommunityRoundPreAssessmentStatusName', 'CommunityRoundPreAssessmentStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundPreAssessmentStatus ON
GO

INSERT INTO dropdown.CommunityRoundPreAssessmentStatus (CommunityRoundPreAssessmentStatusID, CommunityRoundPreAssessmentStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundPreAssessmentStatus OFF
GO

INSERT INTO dropdown.CommunityRoundPreAssessmentStatus 
	(CommunityRoundPreAssessmentStatusName,DisplayOrder)
VALUES
	(NULL,0),
	('Active', 1),
	('Complete', 2),
	('Hold', 3)
GO	
--End table dropdown.CommunityRoundPreAssessmentStatus

--Begin table dropdown.CommunityRoundRAPInfo
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundRAPInfo'

EXEC Utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundRAPInfo
	(
	CommunityRoundRAPInfoID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundRAPInfoName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundRAPInfoID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundRAPInfo', @TableName, 'DisplayOrder,CommunityRoundRAPInfoName', 'CommunityRoundRAPInfoID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRAPInfo ON
GO

INSERT INTO dropdown.CommunityRoundRAPInfo (CommunityRoundRAPInfoID, CommunityRoundRAPInfoName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRAPInfo OFF
GO

INSERT INTO dropdown.CommunityRoundRAPInfo 
	(CommunityRoundRAPInfoName,DisplayOrder)
VALUES
	(NULL,0),
	('Yes', 1),
	('No', 2),
	('ARAP', 3)
GO	
--End table dropdown.CommunityRoundRAPInfo

--Begin table dropdown.CommunityRoundRound
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundRound'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundRound
	(
	CommunityRoundRoundID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundRoundName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundRoundID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundRound', @TableName, 'DisplayOrder,CommunityRoundRoundName', 'CommunityRoundRoundID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRound ON
GO

INSERT INTO dropdown.CommunityRoundRound (CommunityRoundRoundID, CommunityRoundRoundName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundRound OFF
GO

INSERT INTO dropdown.CommunityRoundRound 
	(CommunityRoundRoundName,DisplayOrder)
VALUES
	(NULL,0),
	('Round 1', 1),
	('Round 2', 2),
	('Round 3', 3),
	('Round 4', 4),
	('Round 5', 5),
	('Round 6', 6),
	('Round 7', 7),
	('Round 8', 8),
	('Round 9', 9),
	('Round 10', 10)
GO	
--End table dropdown.CommunityRoundRound

--Begin table dropdown.CommunityRoundTamkeen
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityRoundTamkeen'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityRoundTamkeen
	(
	CommunityRoundTamkeenID INT IDENTITY(0,1) NOT NULL,
	CommunityRoundTamkeenName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityRoundTamkeenID'
EXEC utility.SetIndexNonClustered 'IX_CommunityRoundTamkeen', @TableName, 'DisplayOrder,CommunityRoundTamkeenName', 'CommunityRoundTamkeenID'
GO

SET IDENTITY_INSERT dropdown.CommunityRoundTamkeen ON
GO

INSERT INTO dropdown.CommunityRoundTamkeen (CommunityRoundTamkeenID, CommunityRoundTamkeenName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityRoundTamkeen OFF
GO

INSERT INTO dropdown.CommunityRoundTamkeen 
	(CommunityRoundTamkeenName,DisplayOrder)
VALUES
	(NULL,0),
	('Yes', 1),
	('No', 2)
GO	
--End table dropdown.CommunityRoundTamkeen

