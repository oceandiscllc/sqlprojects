USE AJACS
GO

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	CR.CommunityRoundID,
	CR.CommunityID,
	C.CommunityName,
	P.ProvinceID,
	P.ProvinceName,
	CR.CommunityCode,
	CG.CommunityGroupName,
	CSG.CommunitySubGroupName,
	CR.CommunityRoundRoundID,
	CRR.CommunityRoundRoundName,
	CR.CommunityRoundGroupID,
	CRG.CommunityRoundGroupName,
	C.ImpactDecisionID,
	ID.ImpactDecisionName,
	C.CommunityEngagementStatusID,
	CES.CommunityEngagementStatusName,
	CR.AreaManagerPersonID,
	dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
	CR.ProjectOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
	CR.FieldOfficer,
	CR.MOUDate,
	dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
	C.Population,
	C.PolicePresenceCategoryID,
	PPC.PolicePresenceCategoryName,
	CR.CommunityRoundRAPInfoID,
	CRRI.CommunityRoundRAPInfoName,
	CR.CommunityRoundJusticeActivityID,
	CRJA.CommunityRoundJusticeActivityName,
	CR.CommunityRoundTamkeenID,
	CRT.CommunityRoundTamkeenName,
	CR.CommunityRoundCivilDefenseCoverageID,
	CRCDC.CommunityRoundCivilDefenseCoverageName,
	CR.FieldFinanceOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.FieldFinanceOfficerPersonID, 'LastFirstTitle') AS FieldFinanceOfficerPersonName,
	CR.FieldLogisticOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.FieldLogisticOfficerPersonID, 'LastFirstTitle') AS FieldLogisticOfficerPersonName,
	CR.ActivitiesOfficerPersonID,
	dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
	CR.CommunityRoundOutput11StatusID,
	CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
	CR.CommunityRoundOutput12StatusID,
	CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
	CR.CommunityRoundOutput13StatusID,
	CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
	CR.CommunityRoundOutput14StatusID,
	CROS4.CommunityRoundOutputStatusName AS CommunityRoundOutput14StatusName,
	CR.CommunityRoundPreAssessmentStatusID,
	CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
	CR.IsFieldOfficerHired,
	CR.IsFieldOfficerTrainedOnProgramObjectives,
	CR.IsFieldOfficerIntroToPLO,
	CR.IsFieldOfficerIntroToFSPandLAC,
	CR.ToRDate,
	dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
	CR.SensitizationDefineFSPRoleDate,
	dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
	CR.SensitizationMeetingMOUDate,
	dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
	CR.SensitizationDiscussCommMakeupDate,
	dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
	CR.InitialSCADate,
	dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
	CR.FinalSWOTDate,
	dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
	CR.SCAMeetingCount,
	CR.QuestionnairCount,
	CR.FemaleQuestionnairCount,
	CR.IsSCAAdjusted,
	CR.SCAFinalizedDate,
	dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
	CR.CSAPMeetingDate,
	dbo.FormatDate(CR.CSAPMeetingDate) AS CSAPMeetingDateFormatted, 
	CR.CSAP2PeerReviewMeetingDate,
	dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
	CR.CSAP2SubmittedDate,
	dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
	CR.CSAPSignedDate,
	dbo.FormatDate(CR.CSAPSignedDate) AS CSAPSignedDateFormatted, 
	CR.CSAPBeginDevelopmentDate,
	dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
	CR.CSAPCommunityAnnouncementDate,
	dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
	CR.ApprovedActivityCount,
	CR.KickoffMeetingCount,
	CR.ActivityImplementationStartDate,
	dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
	CR.ActivityImplementationEndDate,
	dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
	(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
		JOIN CONTACT C ON C.ContactID = CCA.ContactID
		WHERE CCA.ContactAffiliationID = 2
		AND CommunityID = CR.CommunityID
		AND C.IsActive = 1
	) AS CSWGCount,
	(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
		JOIN CONTACT C ON C.ContactID = CCA.ContactID
		WHERE CCA.ContactAffiliationID = 2
		AND CommunityID = CR.CommunityID
		AND C.IsActive = 1
		AND C.Gender = 'Male' 
	) AS CSWGMaleCount,
	(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
		JOIN CONTACT C ON C.ContactID = CCA.ContactID
		WHERE CCA.ContactAffiliationID = 2
		AND CommunityID = CR.CommunityID
		AND C.IsActive = 1
		AND C.Gender = 'Female' 
	) AS CSWGFemaleCount,
	CR.IsActive
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS4 ON CROS4.CommunityRoundOutputStatusID = CR.CommunityRoundOutput14StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
	WHERE CR.CommunityRoundID = @CommunityRoundID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityRound'
			AND DE.EntityID = @CommunityRoundID
			AND D.DocumentDescription IN ('MOU Document','TOR Document')
	ORDER BY D.DocumentDescription

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
	ORDER BY 2

	--UpDATEs
	SELECT
		C.CommunityRoundUpDATEID,
		C.CommunityUpDATEs,
		C.ProcessUpDATEs,
		C.NextPlans,
		C.Risks,
		C.UpDATEDate,
		dbo.FormatDate(C.UpDATEDate) AS UpDATEDateFormatted
	FROM dbo.CommunityRoundUpDATE C
	WHERE C.CommunityRoundID = @CommunityRoundID
	ORDER BY C.UpDATEDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.08.02
-- Description:	Tweaked to show items with no entries in the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	Added "LIKE" support for MenuItemPermissionableLineage data
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not permissionable.PersonPermissionable
-- =====================================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND 
				(
				EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
						)
						AND PP.PersonID = @PersonID
					)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM permissionable.PersonPermissionable PP
						WHERE EXISTS
							(
							SELECT 1
							FROM dbo.MenuItemPermissionableLineage MIPL
							WHERE MIPL.MenuItemID = MI.MenuItemID
								AND PP.PermissionableLineage LIKE MIPL.PermissionableLineage + '%'
							)
							AND PP.PersonID = @PersonID
						)
				OR EXISTS
					(
					SELECT 1
					FROM dbo.EntityType ET
					WHERE ET.EntityTypeCode = MI.EntityTypeCode
						AND ET.HasMenuItemAccessViaWorkflow = 1
						AND (SELECT workflow.CanHaveAddUpdate(MI.EntityTypeCode, 0, @PersonID)) = 1
					)
				OR
					(
					NOT EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
						)
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.EntityType ET
						WHERE ET.EntityTypeCode = MI.EntityTypeCode
							AND ET.HasMenuItemAccessViaWorkflow = 1
						)
					)
				)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure dbo.GetVettingExpirationCounts
EXEC Utility.DropObject 'dbo.GetVettingExpirationCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A procedure to get counts of expired and soon to expire vetting records
-- ====================================================================================
CREATE PROCEDURE dbo.GetVettingExpirationCounts

AS
BEGIN
	SET NOCOUNT ON;

	SELECT D.*
	FROM
		(
		SELECT
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) < 0) AS UKExpired,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 30) AS UK30,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 31 AND 60) AS UK60,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.UKVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 61 AND 90) AS UK90,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) < 0) AS USExpired,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 30) AS US30,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 31 AND 60) AS US60,
			(SELECT COUNT(C.ContactID) FROM dbo.Contact C WHERE C.USVettingExpirationDate IS NOT NULL AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 61 AND 90) AS US90
		) AS D
		
END
GO
--End procedure dbo.GetVettingExpirationCounts

--Begin procedure dropdown.GetCommunityRoundCivilDefenseCoverageData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundCivilDefenseCoverageData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundCivilDefenseCoverage table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundCivilDefenseCoverageData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundCivilDefenseCoverageID, 
		T.CommunityRoundCivilDefenseCoverageName
	FROM dropdown.CommunityRoundCivilDefenseCoverage T
	WHERE (T.CommunityRoundCivilDefenseCoverageID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundCivilDefenseCoverageName, T.CommunityRoundCivilDefenseCoverageID

END
GO
--End procedure dropdown.GetCommunityRoundCivilDefenseCoverageData

--Begin procedure dropdown.GetCommunityRoundGroupData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundGroup table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundGroupData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundGroupID, 
		T.CommunityRoundGroupName
	FROM dropdown.CommunityRoundGroup T
	WHERE (T.CommunityRoundGroupID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundGroupName, T.CommunityRoundGroupID

END
GO
--End procedure dropdown.GetCommunityRoundGroupData

--Begin procedure dropdown.GetCommunityRoundJusticeActivityData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundJusticeActivityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundJusticeActivity table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundJusticeActivityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundJusticeActivityID, 
		T.CommunityRoundJusticeActivityName
	FROM dropdown.CommunityRoundJusticeActivity T
	WHERE (T.CommunityRoundJusticeActivityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundJusticeActivityName, T.CommunityRoundJusticeActivityID

END
GO
--End procedure dropdown.GetCommunityRoundJusticeActivityData

--Begin procedure dropdown.GetCommunityRoundOutputStatusData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundOutputStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundOutputStatus table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundOutputStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundOutputStatusID, 
		T.CommunityRoundOutputStatusName
	FROM dropdown.CommunityRoundOutputStatus T
	WHERE (T.CommunityRoundOutputStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundOutputStatusName, T.CommunityRoundOutputStatusID

END
GO
--End procedure dropdown.GetCommunityRoundOutputStatusData

--Begin procedure dropdown.GetCommunityRoundPreAssessmentStatusData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundPreAssessmentStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundPreAssessmentStatus table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundPreAssessmentStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundPreAssessmentStatusID, 
		T.CommunityRoundPreAssessmentStatusName
	FROM dropdown.CommunityRoundPreAssessmentStatus T
	WHERE (T.CommunityRoundPreAssessmentStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundPreAssessmentStatusName, T.CommunityRoundPreAssessmentStatusID

END
GO
--End procedure dropdown.GetCommunityRoundPreAssessmentStatusData

--Begin procedure dropdown.GetCommunityRoundRAPInfoData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundRAPInfoData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundRAPInfoData table
-- ================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundRAPInfoData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundRAPInfoID, 
		T.CommunityRoundRAPInfoName
	FROM dropdown.CommunityRoundRAPInfo T
	WHERE (T.CommunityRoundRAPInfoID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundRAPInfoName, T.CommunityRoundRAPInfoID

END
GO
--End procedure dropdown.GetCommunityRoundRAPInfoData

--Begin procedure dropdown.GetCommunityRoundRoundData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundRoundData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundRound table
-- ==========================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundRoundData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundRoundID, 
		T.CommunityRoundRoundName
	FROM dropdown.CommunityRoundRound T
	WHERE (T.CommunityRoundRoundID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundRoundName, T.CommunityRoundRoundID

END
GO
--End procedure dropdown.GetCommunityRoundRoundData

--Begin procedure dropdown.GetCommunityRoundTamkeenData
EXEC Utility.DropObject 'dropdown.GetCommunityRoundTamkeenData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A stored procedure to return data from the dropdown.CommunityRoundTamkeen table
-- ====================================================================================================
CREATE PROCEDURE dropdown.GetCommunityRoundTamkeenData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityRoundTamkeenID, 
		T.CommunityRoundTamkeenName
	FROM dropdown.CommunityRoundTamkeen T
	WHERE (T.CommunityRoundTamkeenID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityRoundTamkeenName, T.CommunityRoundTamkeenID

END
GO
--End procedure dropdown.GetCommunityRoundTamkeenData

--Begin procedure eventlog.LogCommunityRoundAction
EXEC utility.DropObject 'eventlog.LogCommunityRoundAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityRoundAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityRound',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('CommunityRound', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityRound',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityRoundContactXMLByCommunityRoundID(T.CommunityRoundID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID(T.CommunityRoundID) AS XML)),
			CAST(@cDocuments AS XML) 
			FOR XML RAW('CommunityRound'), ELEMENTS
			)
		FROM dbo.CommunityRound T
		WHERE T.CommunityRoundID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityRoundAction

--Begin procedure eventlog.LogServerSetupAction
EXEC utility.DropObject 'eventlog.LogServerSetupAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogServerSetupAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogServerSetupActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogServerSetupActionTable
		--ENDIF
		
		SELECT *
		INTO #LogServerSetupActionTable
		FROM dbo.ServerSetup SS
		WHERE SS.ServerSetupID = @EntityID
		
		ALTER TABLE #LogServerSetupActionTable DROP COLUMN ServerSetupBinaryValue
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ServerSetup',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*
			FOR XML RAW('ServerSetup'), ELEMENTS
			)
		FROM #LogServerSetupActionTable T
			JOIN dbo.ServerSetup SS ON SS.ServerSetupID = T.ServerSetupID

		DROP TABLE #LogServerSetupActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogServerSetupAction

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Todd Pires
-- Create Date: 2016.04.21
-- Description:	Added the HasActiveAssignement bit
-- ============================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.WorkflowID,	
		W.WorkflowName,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.WorkflowID = W.WorkflowID AND EWSGP.IsComplete = 0)
			THEN 1
			ELSE 0
		END AS HasActiveAssignement
		
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		dbo.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
	ORDER BY 5, WSGP.PersonID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID