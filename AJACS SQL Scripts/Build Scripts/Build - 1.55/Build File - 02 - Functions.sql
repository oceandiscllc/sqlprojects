USE AJACS
GO

--Begin function eventlog.GetCommunityRoundContactXMLByCommunityRoundID
EXEC utility.DropObject 'eventlog.GetCommunityRoundContactXMLByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION eventlog.GetCommunityRoundContactXMLByCommunityRoundID
(
@CommunityRoundID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRoundContacts VARCHAR(MAX) = ''
	
	SELECT @cCommunityRoundContacts = COALESCE(@cCommunityRoundContacts, '') + D.CommunityRoundContact
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRoundContact'), ELEMENTS) AS CommunityRoundContact
		FROM dbo.CommunityRoundContact T 
		WHERE T.CommunityRoundID = @CommunityRoundID
		) D

	RETURN '<CommunityRoundContacts>' + ISNULL(@cCommunityRoundContacts, '') + '</CommunityRoundContacts>'

END
GO
--End function eventlog.GetCommunityRoundContactXMLByCommunityRoundID

--Begin function eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID
EXEC utility.DropObject 'eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID
(
@CommunityRoundID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRoundUpdates VARCHAR(MAX) = ''
	
	SELECT @cCommunityRoundUpdates = COALESCE(@cCommunityRoundUpdates, '') + D.CommunityRoundUpdate
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRoundUpdate'), ELEMENTS) AS CommunityRoundUpdate
		FROM dbo.CommunityRoundUpdate T 
		WHERE T.CommunityRoundID = @CommunityRoundID
		) D

	RETURN '<CommunityRoundUpdates>' + ISNULL(@cCommunityRoundUpdates, '') + '</CommunityRoundUpdates>'

END
GO
--End function eventlog.GetCommunityRoundUpDATEXMLByCommunityRoundID

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T ORDER BY T.CommunityProvinceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'FIFUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM fifupdate.FIFUpdate T ORDER BY T.FIFUpdateID DESC
		ELSE IF @EntityTypeCode = 'JusticeUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM justiceupdate.JusticeUpdate T ORDER BY T.JusticeUpdateID DESC
		ELSE IF @EntityTypeCode = 'PoliceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate T ORDER BY T.PoliceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM RiskUpdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM WeeklyReport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
		
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = @nWorkflowStepNumber
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE IF @EntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate