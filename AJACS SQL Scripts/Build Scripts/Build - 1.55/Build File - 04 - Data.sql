USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'ConceptNote' AND ET.WorkflowActionCode = 'ConceptNoteBeneficiaryVetting')
	BEGIN

	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode,WorkflowActionCode,EmailText)
	VALUES
		('ConceptNote','ConceptNoteBeneficiaryVetting','<p>Dear [[FirstName]] [[LastName]]<br /><br />There are [[ContactCount]] beneficiaries associated with activity [[Title]] that may require vetting action.<br /><br />[[Comments]]</p>')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'Vetting' AND ET.WorkflowActionCode = 'BeneficiaryVettingListEmail')
	BEGIN

	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode,WorkflowActionCode,EmailText)
	VALUES
		('Vetting','BeneficiaryVettingListEmail','<p>There are [[ContactCount]] contacts that are pending internal review as a vetting action. Please use the KMS to filter for these contacts and action accordingly.<br /><br />[[Comments]]</p>')

    END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Vetting' AND ETF.PlaceHolderText = '[[ContactCount]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('Vetting','[[ContactCount]]','Contact Count',1)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'Vetting' AND ETF.PlaceHolderText = '[[Comments]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('Vetting','[[Comments]]','Comments',2)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'ConceptNote' AND ETF.PlaceHolderText = '[[ContactCount]]')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode,PlaceHolderText,PlaceHolderDescription,DisplayOrder)
	VALUES
		('ConceptNote','[[ContactCount]]','Contact Count',8)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
UPDATE ET
SET ET.HasMenuItemAccessViaWorkflow = 1
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode IN ('CommunityProvinceEngagementUpdate','FIFUpdate','JusticeUpdate','PoliceEngagementUpdate','RecommendationUpdate','RiskUpdate','WeeklyReport')
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE MI
SET 
	MI.EntityTypeCode = 'CommunityProvinceEngagementUpdate',
	MI.MenuItemCode = 'CommunityProvinceEngagementUpdate'	
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'CommunityProvenceEngagementUpdate'
GO
--End table dbo.MenuItem

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
	JOIN dbo.EntityType ET ON ET.EntityTypeCode = MI.EntityTypeCode
		AND ET.HasMenuItemAccessViaWorkflow = 1
GO
--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.Permissionable
DELETE P
FROM permissionable.Permissionable P
WHERE P.ControllerName IN ('CommunityProvinceEngagement','FIFUpdate','Justice','PoliceEngagement','WeeklyReport')
	AND P.PermissionCode <> 'Export'
GO

EXEC permissionable.DeletePermissionable 0
GO
--End table permissionable.Permissionable
