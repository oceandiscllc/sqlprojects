USE AJACS
GO

--Begin table dbo.SpotReport
EXEC utility.AddColumn 'dbo.SpotReport', 'SummaryMap', 'VARBINARY(MAX)'
GO
--End table dbo.SpotReport

--Begin table weeklyreport.SummaryMapForce
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapForce'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapForce
	(
	SummaryMapForceID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapForceID'
EXEC utility.SetIndexClustered 'IX_SummaryMapForce', @TableName, 'ForceID,WeeklyReportID'
GO
--End table weeklyreport.SummaryMapForce
