USE AJACS
GO

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSpotReportComments VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportComments = COALESCE(@cSpotReportComments, '') + D.SpotReportComment 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportComment'), ELEMENTS) AS SpotReportComment
			FROM dbo.SpotReportComment T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportCommunities VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportCommunities = COALESCE(@cSpotReportCommunities, '') + D.SpotReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportCommunity'), ELEMENTS) AS SpotReportCommunity
			FROM dbo.SpotReportCommunity T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportIncidents = COALESCE(@cSpotReportIncidents, '') + D.SpotReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportIncident'), ELEMENTS) AS SpotReportIncident
			FROM dbo.SpotReportIncident T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportProvinces VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportProvinces = COALESCE(@cSpotReportProvinces, '') + D.SpotReportProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportProvince'), ELEMENTS) AS SpotReportProvince
			FROM dbo.SpotReportProvince T 
			WHERE T.SpotReportID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogSpotReportActionTable', 'u')) IS NOT NULL
			DROP TABLE LogSpotReportActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogSpotReportActionTable
		FROM dbo.SpotReport T
		WHERE T.SpotReportID = @EntityID
		
		ALTER TABLE #LogSpotReportActionTable DROP COLUMN SummaryMap

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<SummaryMap>' + CAST(SR.SpotReportID AS VARCHAR(MAX)) + '</SummaryMap>') AS XML),
			CAST(('<SpotReportComments>' + ISNULL(@cSpotReportComments, '') + '</SpotReportComments>') AS XML),
			CAST(('<SpotReportCommunities>' + ISNULL(@cSpotReportCommunities, '') + '</SpotReportCommunities>') AS XML),
			CAST(('<SpotReportIncidents>' + ISNULL(@cSpotReportIncidents, '') + '</SpotReportIncidents>') AS XML),
			CAST(('<SpotReportProvinces>' + ISNULL(@cSpotReportProvinces, '') + '</SpotReportProvinces>') AS XML)
			FOR XML RAW('SpotReport'), ELEMENTS
			)
		FROM #LogSpotReportActionTable T
			JOIN dbo.SpotReport SR ON SR.SpotReportID = T.SpotReportID
		WHERE T.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure fifupdate.AddFIFCommunity
EXEC Utility.DropObject 'fifupdate.AddFIFCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Community table
-- ============================================================================
CREATE PROCEDURE fifupdate.AddFIFCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)
	
	INSERT INTO fifupdate.Community
		(CommunityID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@FIFUpdateID,
		C1.FIFCommunityEngagement, 
		C1.FIFJustice, 
		C1.FIFPoliceEngagement, 
		C1.FIFUpdateNotes, 
		C1.IndicatorUpdate, 
		C1.MeetingNotes, 
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO fifupdate.CommunityIndicator
		(FIFUpdateID, CommunityID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.FIFAchievedValue, 0),
		OACI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.FIFAchievedValue,
				CI.FIFNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO fifupdate.CommunityRisk
		(FIFUpdateID, CommunityID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		D.CommunityID,
		R.RiskID,
		ISNULL(OACR.FIFRiskValue, 0),
		OACR.FIFNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RC.CommunityID
			FROM recommendation.RecommendationCommunity RC 
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC.RecommendationID
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				CR.FIFRiskValue, 
				CR.FIFNotes
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = D.CommunityID
			) OACR

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFCommunity

--Begin procedure fifupdate.AddFIFProvince
EXEC Utility.DropObject 'fifupdate.AddFIFProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Province table
-- ===========================================================================
CREATE PROCEDURE fifupdate.AddFIFProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)

	INSERT INTO fifupdate.Province
		(ProvinceID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@FIFUpdateID,
		P1.FIFCommunityEngagement, 
		P1.FIFJustice, 
		P1.FIFPoliceEngagement, 
		P1.FIFUpdateNotes, 
		P1.IndicatorUpdate, 
		P1.MeetingNotes,
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO fifupdate.ProvinceIndicator
		(FIFUpdateID, ProvinceID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.FIFAchievedValue, 0),
		OAPI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.FIFAchievedValue,
				PRI.FIFNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO fifupdate.ProvinceRisk
		(FIFUpdateID, ProvinceID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		D.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.FIFRiskValue, 0),
		OAPR.FIFNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RP.ProvinceID
			FROM recommendation.RecommendationProvince RP
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				PR.FIFRiskValue, 
				PR.FIFNotes
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = D.ProvinceID
			) OAPR

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFProvince

--Begin procedure fifupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'fifupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to return data from the dbo.Community and fifupdate.Community tables
-- ====================================================================================================
CREATE PROCEDURE fifupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.FIFCommunityEngagement,
		C.FIFJustice,
		C.FIFPoliceEngagement,
		C.FIFUpdateNotes,
		C.IndicatorUpdate,
		C.MeetingNotes,
		C.MOUDate,
		dbo.FormatDate(C.MOUDate) AS MOUDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CommunityID,
		C1.CommunityName AS EntityName,
		C2.FIFCommunityEngagement,
		C2.FIFJustice,
		C2.FIFPoliceEngagement,
		C2.FIFUpdateNotes,
		C2.IndicatorUpdate,
		C2.MeetingNotes,
		C2.MOUDate,
		dbo.FormatDate(C2.MOUDate) AS MOUDateFormatted
	FROM fifupdate.Community C2
		JOIN dbo.Community C1 ON C1.CommunityID = C2.CommunityID
			AND C2.CommunityID = @CommunityID

	--EntityMeetingsCurrent
	SELECT
		CM.AttendeeCount, 
		CM.MeetingDate, 
		dbo.FormatDate(CM.MeetingDate) AS MeetingDateFormatted,
		CM.CommunityMeetingID AS MeetingID, 
		CM.MeetingTitle, 
		CM.RegularAttendeeCount
	FROM dbo.CommunityMeeting CM
	WHERE CM.CommunityID = @CommunityID

	--EntityMeetingsUpdate
	SELECT
		CM.AttendeeCount, 
		CM.MeetingDate, 
		dbo.FormatDate(CM.MeetingDate) AS MeetingDateFormatted,
		CM.CommunityMeetingID AS MeetingID, 
		CM.MeetingTitle, 
		CM.RegularAttendeeCount
	FROM fifupdate.CommunityMeeting CM
	WHERE CM.CommunityID = @CommunityID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'FIFUpdateCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.FIFAchievedValue,
		OACI.FIFNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.FIFAchievedValue, 
				CI.FIFNotes
			FROM fifupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.FIFRiskValue,
		OACR.FIFNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.FIFRiskValue, 
				CR.FIFNotes
			FROM fifupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure fifupdate.GetCommunityByCommunityID

--Begin procedure fifupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'fifupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to return data from the dbo.Province and fifupdate.Province tables
-- ==================================================================================================
CREATE PROCEDURE fifupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.ProvinceID,
		P.ProvinceName AS EntityName,
		P.FIFCommunityEngagement,
		P.FIFJustice,
		P.FIFPoliceEngagement,
		P.FIFUpdateNotes,
		P.IndicatorUpdate,
		P.MeetingNotes,
		P.MOUDate,
		dbo.FormatDate(P.MOUDate) AS MOUDateFormatted
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.ProvinceID,
		P1.ProvinceName AS EntityName,
		P2.FIFCommunityEngagement,
		P2.FIFJustice,
		P2.FIFPoliceEngagement,
		P2.FIFUpdateNotes,
		P2.IndicatorUpdate,
		P2.MeetingNotes,
		P2.MOUDate,
		dbo.FormatDate(P2.MOUDate) AS MOUDateFormatted
	FROM fifupdate.Province P2
		JOIN dbo.Province P1 ON P1.ProvinceID = P2.ProvinceID
			AND P2.ProvinceID = @ProvinceID

	--EntityMeetingsCurrent
	SELECT
		PM.AttendeeCount, 
		PM.MeetingDate, 
		dbo.FormatDate(PM.MeetingDate) AS MeetingDateFormatted,
		PM.ProvinceMeetingID AS MeetingID, 
		PM.MeetingTitle, 
		PM.RegularAttendeeCount
	FROM dbo.ProvinceMeeting PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityMeetingsUpdate
	SELECT
		PM.AttendeeCount, 
		PM.MeetingDate, 
		dbo.FormatDate(PM.MeetingDate) AS MeetingDateFormatted,
		PM.ProvinceMeetingID AS MeetingID, 
		PM.MeetingTitle, 
		PM.RegularAttendeeCount
	FROM fifupdate.ProvinceMeeting PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'FIFUpdateProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PI.ProvinceIndicatorID
			FROM dbo.ProvinceIndicator PI 
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OAPI.FIFAchievedValue,
		OAPI.FIFNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PI.FIFAchievedValue, 
				PI.FIFNotes
			FROM fifupdate.ProvinceIndicator PI
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.FIFRiskValue,
		OAPR.FIFNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.FIFRiskValue, 
				PR.FIFNotes
			FROM fifupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure fifupdate.GetProvinceByProvinceID

--Begin procedure policeengagementupdate.AddPoliceEngagementCommunity
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Community table
-- =========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Community
		(CommunityID, PoliceEngagementUpdateID, UpdatePersonID, CapacityAssessmentDate, PPPDate, PoliceEngagementOutput1, MaterialSupportStatus)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID,
		C1.CapacityAssessmentDate, 
		C1.PPPDate, 
		C1.PoliceEngagementOutput1, 
		C1.MaterialSupportStatus
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM policeengagementupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO policeengagementupdate.CommunityClass
		(PoliceEngagementUpdateID, CommunityID, ClassID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OACC.PoliceEngagementNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
					JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNC2.CommunityID
						AND CNC2.ConceptNoteID = CNC1.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('PEO2','PEO3')
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = CAST(LTT.ListItem AS INT)
				) OACC

	INSERT INTO policeengagementupdate.CommunityIndicator
		(PoliceEngagementUpdateID, CommunityID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.PoliceEngagementAchievedValue, 0),
		OACI.PoliceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.PoliceEngagementAchievedValue,
				CI.PoliceEngagementNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI
	
	INSERT INTO policeengagementupdate.CommunityRecommendation
		(PoliceEngagementUpdateID, CommunityID, RecommendationID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RC.CommunityID, 
		RC.RecommendationID, 
		RC.PoliceEngagementNotes
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
	
	INSERT INTO policeengagementupdate.CommunityRisk
		(PoliceEngagementUpdateID, CommunityID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		D.CommunityID,
		R.RiskID,
		ISNULL(OACR.PoliceEngagementRiskValue, 0),
		OACR.PoliceEngagementNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RC.CommunityID
			FROM recommendation.RecommendationCommunity RC 
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC.RecommendationID
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = D.CommunityID
			) OACR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'PoliceEngagementCommunity', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementCommunity

--Begin procedure policeengagementupdate.AddPoliceEngagementProvince
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Province table
-- ========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Province
		(ProvinceID, PoliceEngagementUpdateID, UpdatePersonID, CapacityAssessmentDate, PPPDate, PoliceEngagementOutput1, MaterialSupportStatus)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID,
		P1.CapacityAssessmentDate, 
		P1.PPPDate, 
		P1.PoliceEngagementOutput1, 
		P1.MaterialSupportStatus
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM policeengagementupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO policeengagementupdate.ProvinceClass
		(PoliceEngagementUpdateID, ProvinceID, ClassID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OAPC.PoliceEngagementNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
					JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNP.ProvinceID
						AND CNP.ConceptNoteID = CNC.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('PEO2','PEO3')
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = CAST(LTT.ListItem AS INT)
				) OAPC

	INSERT INTO policeengagementupdate.ProvinceIndicator
		(PoliceEngagementUpdateID, ProvinceID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.PoliceEngagementAchievedValue, 0),
		OAPI.PoliceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.PoliceEngagementAchievedValue,
				PRI.PoliceEngagementNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO policeengagementupdate.ProvinceRecommendation
		(PoliceEngagementUpdateID, ProvinceID, RecommendationID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RP.ProvinceID, 
		RP.RecommendationID, 
		RP.PoliceEngagementNotes
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
	
	INSERT INTO policeengagementupdate.ProvinceRisk
		(PoliceEngagementUpdateID, ProvinceID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		D.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.PoliceEngagementRiskValue, 0),
		OAPR.CommunityProvinceEngagementNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RP.ProvinceID
			FROM recommendation.RecommendationProvince RP 
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = D.ProvinceID
			) OAPR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'PoliceEngagementProvince', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementProvince

--Begin procedure reporting.GetSerialNumberTracker
EXEC Utility.DropObject 'reporting.GetSerialNumberTracker'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data from the procurement.EquipmentInventory table
-- =========================================================================================
CREATE PROCEDURE reporting.GetSerialNumberTracker

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT

		C.CommunityName,
		dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(C.CommunityID)) as ProvinceName,
		dbo.FormatConceptNoteReferenceCode(CEI.ConceptNoteID) AS ReferenceCode,
		(SELECT Title from ConceptNote where conceptnoteid  = CEI.ConceptNoteID) AS ConceptNoteTitle,
		(SELECT CNT.ConceptNoteTypeName FROM dropdown.ConceptNoteType CNT JOIN dbo.ConceptNote CN ON CN.ConceptNoteTypeID = CNT.ConceptNoteTypeID AND CN.ConceptNoteID = CEI.ConceptNoteID) AS ConceptNoteTypeName,
		EC.ItemName,
		EI.SerialNumber,
		EI.IMEIMACAddress,
		EI.Sim,
		EI.BudgetCode,
		EI.UnitCost,
		CEI.Quantity,
		EC.QuantityOfIssue,
		(EI.UnitCost * CEI.Quantity * EC.QuantityOfIssue) TotalCost,
		dbo.FormatDate(CEI.DistributionDate) AS DistributionDateFormatted,
		dbo.FormatDate(procurement.GetLastEquipmentAuditDate('Community', CEI.CommunityEquipmentInventoryID)) AS AuditDateFormatted,
		procurement.GetLastEquipmentAuditOutcome('Community', CEI.CommunityEquipmentInventoryID) AS AuditOutcome,
		OAC.City,
		OAC.Gender,
		OAC.ContactFullName,
		OAC.EmployerName,
		OAC.ContactID,
		EC.ItemMake + '/' + EC.ItemModel as MakeAndModel,
		ECC.EquipmentCatalogCategoryName
	FROM procurement.CommunityEquipmentInventory CEI
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.Community C ON C.CommunityID = CEI.CommunityID
				JOIN dropdown.EquipmentCatalogCategory ECC on ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
		JOIN reporting.SearchResult SR ON SR.EntityID = CEI.EquipmentInventoryID
			AND SR.EntityTypeCode = 'TerritoryEquipmentInventory'
			AND SR.PersonID = @PersonID
		OUTER APPLY
			(
			SELECT
				C.City,
				C.Gender,
				dbo.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactFullName,
				C.EmployerName,
				C.ContactID
			FROM
				(
				SELECT 
					ISNULL(CNCE.ContactID, 0) AS ContactID
				FROM dbo.ConceptNoteContactEquipment CNCE 
				WHERE CNCE.ConceptNoteID = CEI.ConceptNoteID 
					AND CNCE.Quantity = CEI.Quantity 
					AND CNCE.EquipmentInventoryID = CEI.EquipmentInventoryID 
					AND CNCE.TerritoryTypeCode = 'Community' 
					AND CNCE.TerritoryID = CEI.CommunityID
				) D
				JOIN dbo.Contact C ON C.ContactID = D.ContactID
			) OAC

	UNION

	SELECT 
		'' as CommunityName,
		P.ProvinceName,
		dbo.FormatConceptNoteReferenceCode(PEI.ConceptNoteID) AS ReferenceCode,
		(SELECT Title from ConceptNote where conceptnoteid  = PEI.ConceptNoteID) AS ConceptNoteTitle,
		(SELECT CNT.ConceptNoteTypeName FROM dropdown.ConceptNoteType CNT JOIN dbo.ConceptNote CN ON CN.ConceptNoteTypeID = CNT.ConceptNoteTypeID AND CN.ConceptNoteID = PEI.ConceptNoteID) AS ConceptNoteTypeName,
		EC.ItemName,
		EI.SerialNumber,
		EI.IMEIMACAddress,
		EI.Sim,
		EI.BudgetCode,
		EI.UnitCost,
		PEI.Quantity,
		EC.QuantityOfIssue,
		(EI.UnitCost * PEI.Quantity * EC.QuantityOfIssue) TotalCost,
		dbo.FormatDate(PEI.DistributionDate) AS DistributionDateFormatted,
		dbo.FormatDate(procurement.GetLastEquipmentAuditDate('Province', PEI.ProvinceEquipmentInventoryID)) AS AuditDateFormatted,
		procurement.GetLastEquipmentAuditOutcome('Province', PEI.ProvinceEquipmentInventoryID) AS AuditOutcome,
		OAC.City,
		OAC.Gender,
		OAC.ContactFullName,
		OAC.EmployerName,
		OAC.ContactID,
		EC.ItemMake + '/' + EC.ItemModel as MakeAndModel,
		ECC.EquipmentCatalogCategoryName
	FROM procurement.ProvinceEquipmentInventory PEI
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = PEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.Province P ON P.ProvinceID = PEI.ProvinceID
		JOIN dropdown.EquipmentCatalogCategory ECC on ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
		JOIN reporting.SearchResult SR ON SR.EntityID = PEI.EquipmentInventoryID
			AND SR.EntityTypeCode = 'TerritoryEquipmentInventory'
			AND SR.PersonID = @PersonID
		OUTER APPLY
			(
			SELECT
				C.City,
				C.Gender,
				dbo.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactFullName,
				C.EmployerName,
				C.ContactID
			FROM
				(
				SELECT 
					ISNULL(CNCE.ContactID, 0) AS ContactID
				FROM dbo.ConceptNoteContactEquipment CNCE 
				WHERE CNCE.ConceptNoteID = PEI.ConceptNoteID 
					AND CNCE.Quantity = PEI.Quantity 
					AND CNCE.EquipmentInventoryID = PEI.EquipmentInventoryID 
					AND CNCE.TerritoryTypeCode = 'Province' 
					AND CNCE.TerritoryID = PEI.ProvinceID
				) D
				JOIN dbo.Contact C ON C.ContactID = D.ContactID
			) OAC

END
GO
--End procedure reporting.GetSerialNumberTracker

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.WeeklyReportID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O
		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.WorkflowStepNumber 
	FROM weeklyreport.WeeklyReport WR

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'WeeklyReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nWeeklyReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'WeeklyReport'
			AND WSWA.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @nWeeklyReportID)
	ORDER BY WSWA.DisplayOrder

	SELECT
		C.CommunityID,
		C.CommunityName,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID,
		ID.ImpactDecisionName,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		C2.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunity SMC WHERE SMC.CommunityID = C.CommunityID AND SMC.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM weeklyreport.Community C
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Community C2 ON C2.CommunityID = C.CommunityID
			 AND C.WeeklyReportID = @nWeeklyReportID

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
			AND CAT.CommunityAssetTypeID = 1
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor,
		'/assets/img/icons/' + REPLACE(ZT.HexColor, '#', '') + '.png' AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CAT.CommunityAssetTypeID = 2
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapIncident SMI WHERE SMI.IncidentID = I.IncidentID AND SMI.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (
				EXISTS	(
					SELECT 1
					FROM dbo.IncidentCommunity IC
						JOIN weeklyreport.Community C ON C.CommunityID = IC.CommunityID
							AND IC.IncidentID = I.IncidentID
							AND C.WeeklyReportID = @nWeeklyReportID
				)
				OR
				EXISTS (
					SELECT 1
					FROM dbo.IncidentProvince IP
						JOIN weeklyreport.Province P ON P.ProvinceID = IP.ProvinceID
							AND IP.IncidentID = I.IncidentID
							AND P.WeeklyReportID = @nWeeklyReportID
				)
			)		

	SELECT
		F.ForceID,
		F.ForceName,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor,
		'/assets/img/icons/' + REPLACE(AOT.HexColor, '#', '') + '.png' AS Icon,
		F.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapForce SMF WHERE SMF.ForceID = F.ForceID AND SMF.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND EXISTS
				(
					SELECT 1
					FROM force.ForceCommunity FC
						JOIN weeklyreport.Community C ON C.CommunityID = FC.CommunityID
							AND FC.ForceID = F.ForceID
							AND C.WeeklyReportID = @nWeeklyReportID
				)

END
GO
--End procedure weeklyreport.GetWeeklyReport