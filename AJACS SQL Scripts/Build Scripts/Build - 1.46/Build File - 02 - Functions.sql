USE AJACS
GO

--Begin function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
EXEC utility.DropObject 'dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2015.08.02
-- Description:	A function to return the name of a territory
-- =========================================================

CREATE FUNCTION dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryID INT
)

RETURNS VARCHAR(355)

AS
BEGIN

	DECLARE @cReturn VARCHAR(355) = ''
	DECLARE @cTerritoryName VARCHAR(250) = ''
	DECLARE @cTerritoryTypeName VARCHAR(50) = ' (' + @TerritoryTypeCode + ')'

	IF @TerritoryID > 0
		BEGIN
		
		IF @TerritoryTypeCode = 'Community'
			SELECT @cTerritoryName = T.CommunityName FROM dbo.Community T WHERE T.CommunityID = @TerritoryID
		ELSE IF @TerritoryTypeCode = 'Province'
			SELECT @cTerritoryName = T.ProvinceName FROM dbo.Province T WHERE T.ProvinceID = @TerritoryID
		--ENDIF

		SET @cReturn = ISNULL(@cTerritoryName, '') + @cTerritoryTypeName
		
		END
	--ENDIF
		
	RETURN @cReturn
	
END
GO
--End function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
