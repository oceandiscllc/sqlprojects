USE AJACS
GO

EXEC utility.DropObject 'mobile'
GO

--Begin tables
EXEC Utility.DropObject 'dbo.CommunityRoundActivity'
EXEC Utility.DropObject 'dbo.CommunityRoundActivityUpdate'
EXEC Utility.DropObject 'dbo.ProvinceMeeeting'
EXEC Utility.DropObject 'dbo.SpotReportSummaryMapCommunity'
EXEC Utility.DropObject 'dbo.SpotReportSummaryMapForce'
EXEC Utility.DropObject 'dbo.SpotReportSummaryMapIncident'
EXEC Utility.DropObject 'dropdown.CommunityRoundActivityCloseoutStatus'
EXEC Utility.DropObject 'dropdown.CommunityRoundActivityDirectIndirectActivity'
EXEC Utility.DropObject 'dropdown.CommunityRoundActivityProcurementAchievement'
EXEC Utility.DropObject 'dropdown.CommunityRoundActivityStatus'
EXEC Utility.DropObject 'mobile.HelpLink'
GO

EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'ActualTotalAmount', 'NUMERIC (18, 2)', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'AmendedConceptNoteID', 'INT', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'DeobligatedAmount', 'NUMERIC (18, 2)', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'FemaleAdultCountActual', 'INT', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'FemaleYouthCountActual', 'INT', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'FinalAwardAmount', 'NUMERIC (18, 2)', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'MaleAdultCountActual', 'INT', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'MaleYouthCountActual', 'INT', '0', 1
GO

EXEC utility.SetDefaultConstraint 'dbo.Contact', 'CellPhoneNumberCountryCallingCodeID', 'INT', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.Contact', 'FaxNumberCountryCallingCodeID', 'INT', '0', 1
EXEC utility.SetDefaultConstraint 'dbo.Contact', 'PhoneNumberCountryCallingCodeID', 'INT', '0', 1
GO
--End tables

EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'newsletter'
EXEC utility.AddSchema 'person'
GO

--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.DropIndex @TableName, 'IX_ConceptNote'

EXEC utility.SetIndexNonClustered @TableName, 'IX_ConceptNote', 'WorkplanActivityID', 'ConceptNoteTypeID'
GO
--End table dbo.ConceptNote

--Begin table dbo.EmailDigest
DECLARE @TableName VARCHAR(250) = 'dbo.EmailDigest'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.EmailDigest
	(
	EmailDigestID INT IDENTITY(1,1) NOT NULL,
	EmailAddress VARCHAR(320),
	EmailSubject VARCHAR(100),
	EmailBody VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'EmailDigestID'
GO
--End table dbo.EmailDigest

--Begin table dbo.EntityType
EXEC utility.AddColumn 'dbo.EntityType', 'IsActive', 'BIT', '1'
EXEC utility.AddColumn 'dbo.EntityType', 'IsForNewsLetter', 'BIT', '0'
GO
--End table dbo.EntityType

--Begin table dbo.Person
EXEC utility.AddColumn 'dbo.Person', 'IsEmailDigest', 'BIT', '0'
EXEC utility.AddColumn 'dbo.Person', 'IsNewsLetterSubscriber', 'BIT', '0'
GO
--End table dbo.Person

--Begin table dbo.Province
EXEC utility.DropColumn 'dbo.Province', 'StipendFundingSourceID'
GO
--End table dbo.Province

--Begin table newsletter.NewsLetter
DECLARE @TableName VARCHAR(250) = 'newsletter.NewsLetter'

EXEC utility.DropObject @TableName

CREATE TABLE newsletter.NewsLetter
	(
	NewsLetterID INT IDENTITY(1,1) NOT NULL,
	NewsLetterDate DATE,
	NewsLetterPublishDateTime DATETIME,
	Comments VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'NewsLetterID'
EXEC utility.SetIndexClustered @TableName, 'IX_NewsLetter', 'NewsLetterDate, NewsLetterID'
GO
--End table newsletter.NewsLetter

--Begin table newsletter.NewsLetterEntity
DECLARE @TableName VARCHAR(250) = 'newsletter.NewsLetterEntity'

EXEC utility.DropObject @TableName

CREATE TABLE newsletter.NewsLetterEntity
	(
	NewsLetterEntityID INT IDENTITY(1,1) NOT NULL,
	NewsLetterID INT,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Notes VARCHAR(500),
	)

EXEC utility.SetDefaultConstraint @TableName, 'NewsLetterID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'NewsLetterEntityID'
EXEC utility.SetIndexClustered @TableName, 'IX_NewsLetterEntity', 'NewsLetterID, EntityTypeCode, EntityID'
GO
--End table newsletter.NewsLetterEntity

--Begin table workflow.EntityWorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowStepGroupPerson'

EXEC utility.DropIndex @TableName, 'IX_EntityWorkflowStepGroupPerson'
EXEC utility.DropIndex @TableName, 'IX_EntityWorkflowStepGroupPerson_Entity'
EXEC utility.DropIndex @TableName, 'IX_EntityWorkflowStepGroupPerson_Person'

EXEC utility.SetIndexNonClustered @TableName, 'IX_EntityWorkflowStepGroupPerson_Entity', 'EntityTypeCode, EntityID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_EntityWorkflowStepGroupPerson_Person', 'PersonID, EntityTypeCode, EntityID, WorkflowStepNumber'
GO
--End table workflow.EntityWorkflowStepGroupPerson
