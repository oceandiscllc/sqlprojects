USE AJACS
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Research', @NewMenuItemCode='NewsLetterList', @NewMenuItemLink='/newsletter/list', @NewMenuItemText='Newsletters', @BeforeMenuItemCode='SpotReportList', @PermissionableLineageList='NewsLetter.List'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='AnnouncementList', @NewMenuItemLink='/announcement/list', @NewMenuItemText='Announcements', @BeforeMenuItemCode='EventLogList', @PermissionableLineageList='Announcement.List'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO

UPDATE ET
SET ET.EntityTypeName = 'Activity'
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode = 'ConceptNote'
GO

UPDATE ET
SET ET.IsActive = 0
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode IN ('EquipmentDistributionPlan', 'RecommendationUpdate', 'RiskUpdate')
GO

UPDATE ET
SET ET.IsForNewsLetter = 1
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode IN ('Asset', 'Community', 'Document', 'Force', 'Province', 'RequestForInformation', 'SpotReport')
GO

UPDATE MI
SET MI.MenuItemText = 'Bi-Weekly Program Report'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'ProgramReport'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.LearnerProfileType LPT WHERE LPT.LearnerProfileTypeName = 'CDC Staff')
	BEGIN

	INSERT INTO dropdown.LearnerProfileType
		(LearnerProfileTypeName)
	VALUES
		('CDC Staff'),
		('Civil Staff'),
		('Provincial Directorate Staff')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ProgramType PT WHERE PT.ProgramTypeName = 'Justice Training')
	BEGIN

	INSERT INTO dropdown.ProgramType
		(ProgramTypeName)
	VALUES
		('Justice Training')

	END
--ENDIF
GO


