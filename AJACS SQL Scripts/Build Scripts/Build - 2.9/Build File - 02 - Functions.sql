USE AJACS
GO

--Begin functions
EXEC utility.DropObject 'dbo.fnMoneyToEnglish'
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForContact'
EXEC utility.DropObject 'dbo.GetContactCountByCommunityIDAndContactTypeCode'
EXEC utility.DropObject 'dbo.IsAssetUnitStipendEligible'
EXEC utility.DropObject 'dbo.udfn_NumberToWords'
EXEC utility.DropObject 'eventlog.GetCommunityRoundActivityUpdateXMLByCommunityRoundActivityID'
GO
--End functions

EXEC utility.DropObject 'core.FormatDate'
EXEC utility.DropObject 'core.FormatDateTime'
EXEC utility.DropObject 'core.ListToTable'
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

CREATE SYNONYM core.FormatDate FOR dbo.FormatDate
CREATE SYNONYM core.FormatDateTime FOR dbo.FormatDate
CREATE SYNONYM core.ListToTable FOR dbo.ListToTable
CREATE SYNONYM person.FormatPersonNameByPersonID FOR dbo.FormatPersonNameByPersonID
GO

--Begin function asset.GetAssetNameByAssetID
EXEC utility.DropObject 'asset.GetAssetNameByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.29
-- Description:	A function to return the name of a Asset
-- =========================================================

CREATE FUNCTION asset.GetAssetNameByAssetID
(
@AssetID INT
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cAssetName VARCHAR(500) = ''
	
	SELECT @cAssetName = A.AssetName
	FROM asset.Asset A
	WHERE A.AssetID = @AssetID

	RETURN ISNULL(@cAssetName, '')

END
GO
--End function asset.GetAssetNameByAssetID


--Begin function dbo.GetContactEquipmentEligibility
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibility
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		CASE
			WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) 
			THEN 1 
			ELSE 0 
			END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 2
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibility

--Begin function dbo.GetRequestForInformationNameByRequestForInformationID
EXEC utility.DropObject 'dbo.GetRequestForInformationNameByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.29
-- Description:	A function to return the name of a RequestForInformation
-- =====================================================================

CREATE FUNCTION dbo.GetRequestForInformationNameByRequestForInformationID
(
@RequestForInformationID INT
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cRequestForInformationName VARCHAR(500) = ''
	
	SELECT @cRequestForInformationName = RFI.RequestForInformationTitle
	FROM dbo.RequestForInformation RFI
	WHERE RFI.RequestForInformationID = @RequestForInformationID

	RETURN ISNULL(@cRequestForInformationName, '')

END
GO
--End function dbo.GetRequestForInformationNameByRequestForInformationID

--Begin function dbo.GetSpotReportNameBySpotReportID
EXEC utility.DropObject 'dbo.GetSpotReportNameBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.29
-- Description:	A function to return the name of a SpotReport
-- =====================================================================

CREATE FUNCTION dbo.GetSpotReportNameBySpotReportID
(
@SpotReportID INT
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cSpotReportName VARCHAR(500) = ''
	
	SELECT @cSpotReportName = SR.SpotReportTitle
	FROM dbo.SpotReport SR
	WHERE SR.SpotReportID = @SpotReportID

	RETURN ISNULL(@cSpotReportName, '')

END
GO
--End function dbo.GetSpotReportNameBySpotReportID

--Begin function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
EXEC utility.DropObject 'dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2015.08.02
-- Description:	A function to return the name of a territory
-- =========================================================

CREATE FUNCTION dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryID INT
)

RETURNS VARCHAR(355)

AS
BEGIN

	DECLARE @cTerritoryName VARCHAR(250) = ''
	DECLARE @cTerritoryTypeName VARCHAR(50) = ' (' + @TerritoryTypeCode + ')'
	
	IF @TerritoryTypeCode = 'Community'
		SELECT @cTerritoryName = T.CommunityName FROM dbo.Community T WHERE T.CommunityID = @TerritoryID
	ELSE IF @TerritoryTypeCode = 'Province'
		SELECT @cTerritoryName = T.ProvinceName FROM dbo.Province T WHERE T.ProvinceID = @TerritoryID
	--ENDIF
	
	RETURN ISNULL(@cTerritoryName, '') + @cTerritoryTypeName
	
END
GO
--End function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID

--Begin function document.FormatFullTextSearchString
EXEC utility.DropObject 'document.FormatFullTextSearchString'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to format a string for use in a full text search
-- ========================================================================

CREATE FUNCTION document.FormatFullTextSearchString
(
@SearchString VARCHAR(1000),
@MatchTypeCode VARCHAR(10)
)

RETURNS VARCHAR(2000)

AS
BEGIN

	SET @SearchString = LTRIM(RTRIM(REPLACE(@SearchString, '"', '')))

	IF @MatchTypeCode = 'Exact'
		SET @SearchString = '"' + @SearchString + '"'
	ELSE IF CHARINDEX(' ', @SearchString) > 0
		SET @SearchString = CASE WHEN @MatchTypeCode = 'All' THEN REPLACE(@SearchString, ' ', ' AND ') ELSE REPLACE(@SearchString, ' ', ' OR ') END
	--ENDIF

	RETURN @SearchString

END
GO
--End function document.FormatFullTextSearchString

--Begin function document.FormatPhysicalFileSize
EXEC utility.DropObject 'document.FormatPhysicalFileSize'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A function to return a formatted file size
-- =======================================================

CREATE FUNCTION document.FormatPhysicalFileSize
(
@PhysicalFileSize INT
)

RETURNS VARCHAR(50)

AS
BEGIN

DECLARE @cFileSize VARCHAR(50)

SELECT @cFileSize = 

	CASE
		WHEN @PhysicalFileSize < 1000
		THEN CAST(@PhysicalFileSize as VARCHAR(50)) + ' b'
		WHEN @PhysicalFileSize < 1000000
		THEN CAST(ROUND((@PhysicalFileSize/1000), 2) AS VARCHAR(50)) + ' kb'
		WHEN @PhysicalFileSize < 1000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000), 2) AS VARCHAR(50)) + ' mb'
		WHEN @PhysicalFileSize < 1000000000000
		THEN CAST(ROUND((@PhysicalFileSize/1000000000), 2) AS VARCHAR(50)) + ' gb'
		ELSE 'More than 1000 gb'
	END

RETURN ISNULL(@cFileSize, '0 b')

END
GO
--End function document.FormatPhysicalFileSize

--Begin function document.GetDocumentNameByDocumentID
EXEC utility.DropObject 'document.GetDocumentNameByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.29
-- Description:	A function to return the name of a Document
-- =========================================================

CREATE FUNCTION document.GetDocumentNameByDocumentID
(
@DocumentID INT
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cDocumentName VARCHAR(500) = ''
	
	SELECT @cDocumentName = D.DocumentName
	FROM document.Document D
	WHERE D.DocumentID = @DocumentID

	RETURN ISNULL(@cDocumentName, '')

END
GO
--End function document.GetDocumentNameByDocumentID

--Begin function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
EXEC utility.DropObject 'eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.09
-- Description:	A function to return document data for a specific entity record
--
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	Fixed a nodes bug
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- ============================================================================

CREATE FUNCTION eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cDocuments VARCHAR(MAX) = ''
	
	IF EXISTS (SELECT 1 FROM document.DocumentEntity T WHERE T.EntityTypeCode = @EntityTypeCode AND T.EntityID = @EntityID)
		BEGIN

		SELECT 
			@cDocuments = COALESCE(@cDocuments, '') + D.Document 
		FROM
			(
			SELECT
				(SELECT T.DocumentID FOR XML RAW('Documents'), ELEMENTS) AS Document
			FROM document.DocumentEntity T 
			WHERE T.EntityTypeCode = @EntityTypeCode
				AND T.EntityID = @EntityID
			) D
	
		IF @cDocuments IS NULL OR LEN(LTRIM(@cDocuments)) = 0
			SET @cDocuments = '<Documents></Documents>'
		--ENDIF

		END
	--ENDIF
		
	RETURN @cDocuments

END
GO
--End function eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID

--Begin function force.GetForceNameByForceID
EXEC utility.DropObject 'force.GetForceNameByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.29
-- Description:	A function to return the name of a Force
-- =========================================================

CREATE FUNCTION force.GetForceNameByForceID
(
@ForceID INT
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cForceName VARCHAR(500) = ''
	
	SELECT @cForceName = F.ForceName
	FROM force.Force F
	WHERE F.ForceID = @ForceID

	RETURN ISNULL(@cForceName, '')

END
GO
--End function force.GetForceNameByForceID
