-- File Name:	Build-1.41 File 01 - AJACS.sql
-- Build Key:	Build-1.41 File 01 - AJACS - 2015.12.11 20.44.33

USE AJACS
GO

-- ==============================================================================================================================
-- Procedures:
--		dbo.GetSpotReportBySpotReportID
--		eventlog.LogCommunityAssetAction
--		eventlog.LogDocumentAction
--		eventlog.LogDonorDecisionAction
--		eventlog.LogEventLogAction
--		eventlog.LogIndicatorAction
--		eventlog.LogIndicatorTypeAction
--		eventlog.LogMilestoneAction
--		eventlog.LogObjectiveAction
--		eventlog.LogPurchaseRequestAction
--		eventlog.LogSpotReportAction
--		policeengagementupdate.AddPoliceEngagementProvince
--		policeengagementupdate.GetCommunityByCommunityID
--		policeengagementupdate.GetProvinceByProvinceID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.DonorMeeting
ALTER TABLE dbo.DonorMeeting ALTER COLUMN MeetingAction VARCHAR(MAX)
GO
--End table dbo.DonorMeeting

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunityID,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProvinceID,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		CAST(D.Thumbnail AS VARCHAR(MAX)) AS Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), 0) AS ThumbnailLength,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure eventlog.LogCommunityAssetAction
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.09.25
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytAssetActionTable
		--ENDIF
		
		SELECT *
		INTO #LogCommuniytAssetActionTable
		FROM dbo.CommunityAsset CA
		WHERE CA.CommunityAssetID = @EntityID
		
		ALTER TABLE #LogCommuniytAssetActionTable DROP COLUMN Location

		DECLARE @cCommunityAssetRisks NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetRisks = COALESCE(@cCommunityAssetRisks, '') + D.CommunityAssetRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetRisk'), ELEMENTS) AS CommunityAssetRisk
			FROM dbo.CommunityAssetRisk T 
			WHERE T.CommunityAssetID = @EntityID
			) D	

		DECLARE @cCommunityAssetUnits NVARCHAR(MAX)

		SELECT 
			@cCommunityAssetUnits = COALESCE(@cCommunityAssetUnits, '') + D.CommunityAssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetUnit'), ELEMENTS) AS CommunityAssetUnit
			FROM dbo.CommunityAssetUnit T 
			WHERE T.CommunityAssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			'<Location>' + CAST(CA.Location AS VARCHAR(MAX)) + '</Location>',
			CAST(('<CommunityAssetRisks>' + ISNULL(@cCommunityAssetRisks  , '') + '</CommunityAssetRisks>') AS XML),
			CAST(('<CommunityAssetUnits>' + ISNULL(@cCommunityAssetUnits  , '') + '</CommunityAssetUnits>') AS XML)
			FOR XML RAW('CommunityAsset'), ELEMENTS
			)
		FROM #LogCommuniytAssetActionTable T
			JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = T.CommunityAssetID

		DROP TABLE #LogCommuniytAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAssetAction

--Begin procedure eventlog.LogDocumentAction
EXEC utility.DropObject 'eventlog.LogDocumentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDocumentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogDocumentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogDocumentActionTable
		--ENDIF
		
		SELECT *
		INTO #LogDocumentActionTable
		FROM dbo.Document D
		WHERE D.DocumentID = @EntityID
		
		ALTER TABLE #LogDocumentActionTable DROP COLUMN Thumbnail

		DECLARE @cDocumentEntities VARCHAR(MAX) 
	
		SELECT 
			@cDocumentEntities = COALESCE(@cDocumentEntities, '') + D.DocumentEntity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('DocumentEntity'), ELEMENTS) AS DocumentEntity
			FROM dbo.DocumentEntity T 
			WHERE T.DocumentID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Document',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<DocumentEntities>' + ISNULL(@cDocumentEntities, '') + '</DocumentEntities>') AS XML)
			FOR XML RAW('Document'), ELEMENTS
			)
		FROM #LogDocumentActionTable T
			JOIN dbo.Document D ON D.DocumentID = T.DocumentID

		DROP TABLE #LogDocumentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDocumentAction

--Begin procedure eventlog.LogDonorDecisionAction
EXEC utility.DropObject 'eventlog.LogDonorDecisionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogDonorDecisionAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'DonorDecision',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'DonorDecision',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('DonorDecision'), ELEMENTS
			)
		FROM dbo.DonorDecision T
		WHERE T.DonorDecisionID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogDonorDecisionAction

--Begin procedure eventlog.LogEventLogAction
EXEC utility.DropObject 'eventlog.LogEventLogAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEventLogAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT,
	@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EventLog',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEventLogAction

--Begin procedure eventlog.LogIndicatorAction
EXEC utility.DropObject 'eventlog.LogIndicatorAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.11.01
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIndicatorAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT,
	@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Indicator'), ELEMENTS
			)
		FROM logicalframework.Indicator T
		WHERE T.IndicatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIndicatorAction

--Begin procedure eventlog.LogIndicatorTypeAction
EXEC utility.DropObject 'eventlog.LogIndicatorTypeAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIndicatorTypeAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT,
	@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'IndicatorType',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'IndicatorType',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('IndicatorType'), ELEMENTS
			)
		FROM dropdown.IndicatorType T
		WHERE T.IndicatorTypeID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIndicatorTypeAction

--Begin procedure eventlog.LogMilestoneAction
EXEC utility.DropObject 'eventlog.LogMilestoneAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogMilestoneAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT,
	@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Milestone',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Milestone',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Milestone'), ELEMENTS
			)
		FROM logicalframework.Milestone T
		WHERE T.MilestoneID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogMilestoneAction

--Begin procedure eventlog.LogObjectiveAction
EXEC utility.DropObject 'eventlog.LogObjectiveAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogObjectiveAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT,
	@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Objective',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Objective',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Objective'), ELEMENTS
			)
		FROM logicalframework.Objective T
		WHERE T.ObjectiveID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogObjectiveAction

--Begin procedure eventlog.LogPurchaseRequestAction
EXEC utility.DropObject 'eventlog.LogPurchaseRequestAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.04.18
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPurchaseRequestAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PurchaseRequest',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cPurchaseRequestConceptNoteBudgets VARCHAR(MAX) 
	
		SELECT 
			@cPurchaseRequestConceptNoteBudgets = COALESCE(@cPurchaseRequestConceptNoteBudgets, '') + D.PurchaseRequestConceptNoteBudget 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PurchaseRequestConceptNoteBudget'), ELEMENTS) AS PurchaseRequestConceptNoteBudget
			FROM procurement.PurchaseRequestConceptNoteBudget T 
			WHERE T.PurchaseRequestID = @EntityID
			) D

		DECLARE @cPurchaseRequestConceptNoteEquipmentCatalogs VARCHAR(MAX) 
	
		SELECT 
			@cPurchaseRequestConceptNoteEquipmentCatalogs = COALESCE(@cPurchaseRequestConceptNoteEquipmentCatalogs, '') + D.PurchaseRequestConceptNoteEquipmentCatalog 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PurchaseRequestConceptNoteEquipmentCatalog'), ELEMENTS) AS PurchaseRequestConceptNoteEquipmentCatalog
			FROM procurement.PurchaseRequestConceptNoteEquipmentCatalog T 
			WHERE T.PurchaseRequestID = @EntityID
			) D

		DECLARE @cPurchaseRequestSubContractors VARCHAR(MAX) 
	
		SELECT 
			@cPurchaseRequestSubContractors = COALESCE(@cPurchaseRequestSubContractors, '') + D.PurchaseRequestSubContractor 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PurchaseRequestSubContractor'), ELEMENTS) AS PurchaseRequestSubContractor
			FROM procurement.PurchaseRequestSubContractor T 
			WHERE T.PurchaseRequestID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PurchaseRequest',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<PurchaseRequestConceptNoteBudgets>' + ISNULL(@cPurchaseRequestConceptNoteBudgets, '') + '</PurchaseRequestConceptNoteBudgets>') AS XML),
			CAST(('<PurchaseRequestConceptNoteEquipmentCatalogs>' + ISNULL(@cPurchaseRequestConceptNoteEquipmentCatalogs, '') + '</PurchaseRequestConceptNoteEquipmentCatalogs>') AS XML),
			CAST(('<PurchaseRequestSubContractors>' + ISNULL(@cPurchaseRequestSubContractors, '') + '</PurchaseRequestSubContractors>') AS XML)
			FOR XML RAW('PurchaseRequest'), ELEMENTS
			)
		FROM procurement.PurchaseRequest T
		WHERE T.PurchaseRequestID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPurchaseRequestAction

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSpotReportComments VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportComments = COALESCE(@cSpotReportComments, '') + D.SpotReportComment 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportComment'), ELEMENTS) AS SpotReportComment
			FROM dbo.SpotReportComment T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportCommunities VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportCommunities = COALESCE(@cSpotReportCommunities, '') + D.SpotReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportCommunity'), ELEMENTS) AS SpotReportCommunity
			FROM dbo.SpotReportCommunity T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportIncidents = COALESCE(@cSpotReportIncidents, '') + D.SpotReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportIncident'), ELEMENTS) AS SpotReportIncident
			FROM dbo.SpotReportIncident T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportProvinces VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportProvinces = COALESCE(@cSpotReportProvinces, '') + D.SpotReportProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportProvince'), ELEMENTS) AS SpotReportProvince
			FROM dbo.SpotReportProvince T 
			WHERE T.SpotReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<SpotReportComments>' + ISNULL(@cSpotReportComments, '') + '</SpotReportComments>') AS XML),
			CAST(('<SpotReportCommunities>' + ISNULL(@cSpotReportCommunities, '') + '</SpotReportCommunities>') AS XML),
			CAST(('<SpotReportIncidents>' + ISNULL(@cSpotReportIncidents, '') + '</SpotReportIncidents>') AS XML),
			CAST(('<SpotReportProvinces>' + ISNULL(@cSpotReportProvinces, '') + '</SpotReportProvinces>') AS XML)
			FOR XML RAW('SpotReport'), ELEMENTS
			)
		FROM dbo.SpotReport T
		WHERE T.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure policeengagementupdate.AddPoliceEngagementProvince
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Province table
-- ========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Province
		(ProvinceID, PoliceEngagementUpdateID, UpdatePersonID, CapacityAssessmentDate, PPPDate, PoliceEngagementOutput1, MaterialSupportStatus)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID,
		P1.CapacityAssessmentDate, 
		P1.PPPDate, 
		P1.PoliceEngagementOutput1, 
		P1.MaterialSupportStatus
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM policeengagementupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO policeengagementupdate.ProvinceClass
		(PoliceEngagementUpdateID, ProvinceID, ClassID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OAPC.PoliceEngagementNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteClass CNC 
					JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
					JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
						AND CNS.ConceptNoteStatusCode = 'Active'
						AND CNC.ClassID = CL.ClassID
						AND EXISTS
							(
							SELECT 1
							FROM dbo.ConceptNoteProvince CNP
							WHERE CNP.ProvinceID = CAST(LTT.ListItem AS INT)
								AND CNP.ConceptNoteID = CNC.ConceptNoteID
							)				
						AND EXISTS
							(
							SELECT 1
							FROM dbo.ConceptNoteIndicator CNI
								JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
									AND CNI.ConceptNoteID = CN.ConceptNoteID
								JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
								JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
									AND CRA.ComponentReportingAssociationCode IN ('PEO2','PEO3')
							)
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = CAST(LTT.ListItem AS INT)
				) OAPC

	INSERT INTO policeengagementupdate.ProvinceIndicator
		(PoliceEngagementUpdateID, ProvinceID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.PoliceEngagementAchievedValue, 0),
		OAPI.PoliceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.PoliceEngagementAchievedValue,
				PRI.PoliceEngagementNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO policeengagementupdate.ProvinceRecommendation
		(PoliceEngagementUpdateID, ProvinceID, RecommendationID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RP.ProvinceID, 
		RP.RecommendationID, 
		RP.PoliceEngagementNotes
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
	
	INSERT INTO policeengagementupdate.ProvinceRisk
		(PoliceEngagementUpdateID, ProvinceID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RP.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.PoliceEngagementRiskValue, 0),
		OAPR.CommunityProvinceEngagementNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceRisk PR
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PR.ProvinceID
					AND PR.RiskID = R.RiskID
			) OAPR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'PoliceEngagementProvince', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementProvince

--Begin procedure policeengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Community and policeengagementupdate.Community tables
--
-- Author:		Eric Jones
-- Create date:	2015.11.29
-- Description:	altered to allow pulling back of Field Activitied (aka projects) with POL component codes
-- =================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CapacityAssessmentDate,
		dbo.FormatDate(C.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.MaterialSupportStatus,
		C.PoliceEngagementOutput1,
		C.PoliceEngagementOutput2,
		C.PPPDate,
		dbo.FormatDate(C.PPPDate) AS PPPDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CapacityAssessmentDate,
		dbo.FormatDate(C1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C1.MaterialSupportStatus,
		C1.PoliceEngagementOutput1,
		C1.PoliceEngagementOutput2,
		C1.PPPDate,
		dbo.FormatDate(C1.PPPDate) AS PPPDateFormatted,
		C2.CommunityID,
		C2.CommunityName AS EntityName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.CommunityID = @CommunityID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.PoliceEngagementAchievedValue,
		OACI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.PoliceEngagementAchievedValue, 
				CI.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(ISNULL(OACP.ProjectCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PC.ProjectCommunityID
			FROM project.ProjectCommunity PC
			WHERE PC.ProjectID = P.ProjectID
				AND PC.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate (note that the button for current & update are the same as projects are read only)
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(ISNULL(OACP.ProjectCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PC.ProjectCommunityID
			FROM project.ProjectCommunity PC
			WHERE PC.ProjectID = P.ProjectID
				AND PC.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(ISNULL(OACR.RecommendationCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RC.RecommendationCommunityID
			FROM recommendation.RecommendationCommunity RC
			WHERE RC.RecommendationID = R.RecommendationID
				AND RC.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.PoliceEngagementRiskValue,
		OACR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID,
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.CommunityID = @CommunityID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.CommunityID = @CommunityID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.CommunityID = @CommunityID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetCommunityByCommunityID

--Begin procedure policeengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Province table and policeengagementupdate.Province tables
--
-- Author:		Eric Jones
-- Create date:	2015.11.29
-- Description:	altered to allow pulling back of Field Activitied (aka projects) with POL component codes
-- =====================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CapacityAssessmentDate,
		dbo.FormatDate(P.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P.MaterialSupportStatus,
		P.PoliceEngagementOutput1,
		P.PoliceEngagementOutput2,
		P.PPPDate,
		dbo.FormatDate(P.PPPDate) AS PPPDateFormatted,
		P.ProvinceID,
		P.ProvinceName AS EntityName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CapacityAssessmentDate,
		dbo.FormatDate(P1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P1.MaterialSupportStatus,
		P1.PoliceEngagementOutput1,
		P1.PoliceEngagementOutput2,
		P1.PPPDate,
		dbo.FormatDate(P1.PPPDate) AS PPPDateFormatted,
		P2.ProvinceID,
		P2.ProvinceName AS EntityName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
			AND P1.ProvinceID = @ProvinceID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.PoliceEngagementAchievedValue,
		OAPI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.PoliceEngagementAchievedValue, 
				PRI.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectProvinceNotes(' + CAST(OAPP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PP.ProjectProvinceID
			FROM project.ProjectProvince PP
			WHERE PP.ProjectID = P.ProjectID
				AND PP.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate (note that the button for current & update are the same as projects are read only)
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectProvinceNotes(' + CAST(OAPP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PP.ProjectProvinceID
			FROM project.ProjectProvince PP
			WHERE PP.ProjectID = P.ProjectID
				AND PP.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(ISNULL(OAPR.RecommendationProvinceID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RP.RecommendationProvinceID
			FROM recommendation.RecommendationProvince RP
			WHERE RP.RecommendationID = R.RecommendationID
				AND RP.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.PoliceEngagementRiskValue,
		OAPR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID,
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.ProvinceID = @ProvinceID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.ProvinceID = @ProvinceID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.ProvinceID = @ProvinceID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetProvinceByProvinceID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'ConceptNote' AND ETF.PlaceHolderText = '[[FirstName]]')
	BEGIN
	
	DECLARE @cEntityTypeCode VARCHAR(50) = 'ConceptNote'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[FirstName]]'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[LastName]]'
	
	SET @cEntityTypeCode = 'RequestForInformation'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[FirstName]]'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[LastName]]'
	
	SET @cEntityTypeCode = 'SpotReport'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[FirstName]]'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[LastName]]'
	
	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'DonorDecision', 'Donor Decisions & Meetings'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DonorDecision', @ParentMenuItemCode='', @AfterMenuItemCode='Dashboard', @Icon='fa fa-fw fa-book', @PermissionableLineageList='DonorDecision.View'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityProvinceEngagementReport', @NewMenuItemLink='/communityprovinceengagement/export', @NewMenuItemText='Engagement Report', @ParentMenuItemCode='Admin', @AfterMenuItemCode='DownloadRAPData', @PermissionableLineageList='CommunityProvinceEngagementUpdate.Export'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table dbo.MenuItem

--Begin table dropdown.DocumentType
UPDATE DT
SET DT.DocumentTypePermissionCode = 701
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IN ('MonitoringFinalReport','VendorFinalReport')
GO
--End table dropdown.DocumentType

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	P.PermissionableID,
	'Export',
	'Export'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Community.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Community.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	P.PermissionableID,
	'View',
	'View'
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'DonorDecision'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'DonorDecision.View'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.41 File 01 - AJACS - 2015.12.11 20.44.33')
GO
--End build tracking

