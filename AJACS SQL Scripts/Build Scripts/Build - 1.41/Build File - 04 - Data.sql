USE AJACS
GO

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'ConceptNote' AND ETF.PlaceHolderText = '[[FirstName]]')
	BEGIN
	
	DECLARE @cEntityTypeCode VARCHAR(50) = 'ConceptNote'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[FirstName]]'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[LastName]]'
	
	SET @cEntityTypeCode = 'RequestForInformation'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[FirstName]]'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[LastName]]'
	
	SET @cEntityTypeCode = 'SpotReport'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[FirstName]]'
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	SELECT
		@cEntityTypeCode,
		ETF1.PlaceHolderText,
		'Action Officer ' + ETF1.PlaceHolderDescription,
		(SELECT MAX(ETF2.DisplayOrder) + 1 FROM dbo.EmailTemplateField ETF2 WHERE ETF2.EntityTypeCode = @cEntityTypeCode)
	FROM dbo.EmailTemplateField ETF1 
	WHERE ETF1.EntityTypeCode = 'Person'
		AND ETF1.PlaceHolderText = '[[LastName]]'
	
	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'DonorDecision', 'Donor Decisions & Meetings'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DonorDecision', @ParentMenuItemCode='', @AfterMenuItemCode='Dashboard', @Icon='fa fa-fw fa-book', @PermissionableLineageList='DonorDecision.View'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityProvinceEngagementReport', @NewMenuItemLink='/communityprovinceengagement/export', @NewMenuItemText='Engagement Report', @ParentMenuItemCode='Admin', @AfterMenuItemCode='DownloadRAPData', @PermissionableLineageList='CommunityProvinceEngagementUpdate.Export'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Admin'
GO
--End table dbo.MenuItem

--Begin table dropdown.DocumentType
UPDATE DT
SET DT.DocumentTypePermissionCode = 701
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IN ('MonitoringFinalReport','VendorFinalReport')
GO
--End table dropdown.DocumentType

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	P.PermissionableID,
	'Export',
	'Export'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Community.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Community.View.Export'
		)
GO

INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	P.PermissionableID,
	'View',
	'View'
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'DonorDecision'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'DonorDecision.View'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
