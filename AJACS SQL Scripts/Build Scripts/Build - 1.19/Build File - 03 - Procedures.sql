USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-circle.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetMenuItemsByPersonID
EXEC Utility.DropObject 'dbo.GetMenuItemsByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.03
-- Description:	A stored procedure to return a set of hierarchical menu items of a specific person
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Tweaked to prevent duplicate PersonMenuitem records from being a problem, implemented the permissionables system
--
-- Author:			Todd Pires
-- Create date:	2015.03.22
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Implemented the dbo.MenuItemPermissionableLineage table
-- =============================================================================================================================
CREATE PROCEDURE dbo.GetMenuItemsByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PadLength INT
		
	SELECT @PadLength = LEN(CAST(COUNT(MI.MenuItemID) AS VARCHAR(50)))
	FROM dbo.MenuItem MI

	;
	WITH HD (DisplayIndex,MenuItemID,ParentMenuItemID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(VARCHAR(255), RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			1
		FROM dbo.MenuItem MI
		WHERE MI.ParentMenuItemID = 0
			AND MI.IsActive = 1
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PersonPermissionable PP
				WHERE EXISTS
					(
					SELECT 1
					FROM dbo.MenuItemPermissionableLineage MIPL
					WHERE MIPL.MenuItemID = MI.MenuItemID
						AND PP.PermissionableLineage = MIPL.PermissionableLineage
					)
					AND PP.PersonID = @PersonID
				)

		UNION ALL

		SELECT
			CONVERT(VARCHAR(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @PadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY MI.DisplayOrder, MI.MenuItemText) AS VARCHAR(10)), @PadLength)),
			MI.MenuItemID,
			MI.ParentMenuItemID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM dbo.MenuItem MI
			JOIN HD ON HD.MenuItemID = MI.ParentMenuItemID
				AND MI.IsActive = 1
				AND EXISTS
					(
					SELECT 1
					FROM permissionable.PersonPermissionable PP
					WHERE EXISTS
						(
						SELECT 1
						FROM dbo.MenuItemPermissionableLineage MIPL
						WHERE MIPL.MenuItemID = MI.MenuItemID
							AND PP.PermissionableLineage = MIPL.PermissionableLineage
						)
						AND PP.PersonID = @PersonID
					)
		)

	SELECT
		HD1.MenuItemID,
		HD1.DisplayIndex,
		HD1.NodeLevel,
		HD1.ParentMenuItemID,
		MI.MenuItemCode,
		MI.MenuItemText,
		MI.MenuItemLink,
		MI.Icon,
		
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
			THEN 1
			ELSE 0
		END AS HasChildren
	
	FROM HD HD1
		JOIN dbo.MenuItem MI ON MI.MenuItemID = HD1.MenuItemID
			AND 
				(
				LEN(RTRIM(MI.MenuItemLink)) > 0 
					OR MI.MenuItemLink IS NOT NULL 
					OR 
						(
						MI.MenuItemLink IS NULL 
							AND EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentMenuItemID = HD1.MenuItemID)
						)
				)
	ORDER BY HD1.DisplayIndex
		
END
GO
--End procedure dbo.GetMenuItemsByPersonID

--Begin procedure procurement.GetPurchaseRequestByPurchaseRequestID
EXEC Utility.DropObject 'procurement.GetPurchaseRequestByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.01
-- Description:	A stored procedure to return data for a purchase request
-- =====================================================================
CREATE PROCEDURE procurement.GetPurchaseRequestByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT

	SELECT @nConceptNoteID = PR.ConceptNoteID
	FROM procurement.PurchaseRequest PR
	WHERE PR.PurchaseRequestID = @PurchaseRequestID

	SELECT
		PR.PurchaseRequestID,
		PR.ConceptNoteID,
		PR.RequestPersonID,
		PR.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(PR.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		PR.DeliveryDate,
		dbo.FormatDate(PR.DeliveryDate) AS DeliveryDateFormatted,
		PR.ReferenceCode,
		PR.Address,
		PR.AddressCountryID,
		PR.Notes,
		CUR.CurrencyID,
		CUR.CurrencyName,
		COU.CountryName,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' ' + CN.Title AS ConceptNoteTitle
	FROM procurement.PurchaseRequest PR
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = PR.CurrencyID
		JOIN dropdown.Country COU ON COU.CountryID = PR.AddressCountryID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = PR.ConceptNoteID
	WHERE PR.PurchaseRequestID = @PurchaseRequestID
	
	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.ConceptNoteBudgetID,
		CNB.ItemName,
		ISNULL(OAPRCNB.Quantity, CNB.Quantity) AS Quantity,
		CNB.QuantityOfIssue,
		CNB.UnitOfIssue,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.QuantityOfIssue * CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.QuantityOfIssue * CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNB.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT 
				PRCNB.Quantity
			FROM procurement.PurchaseRequestConceptNoteBudget PRCNB 
			WHERE PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID 
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNB

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.QuantityOfIssue * CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(EC.QuantityOfIssue * CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNEC.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT PRCNEC.Quantity 
			FROM procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC 
			WHERE PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID 
				AND PRCNEC.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNEC

	SELECT
		SC.SubContractorID,
		SC.SubContractorName
	FROM procurement.PurchaseRequestSubContractor PRSC
		JOIN dbo.SubContractor SC ON SC.SubContractorID = PRSC.SubContractorID
			AND PRSC.PurchaseRequestID = @PurchaseRequestID
END
GO
--End procedure procurement.GetPurchaseRequestByPurchaseRequestID

--Begin procedure reporting.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.06
-- Description:	A stored procedure to get data for the Concept Note Budget Export
-- ==============================================================================
CREATE PROCEDURE reporting.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CNBD AS
		(
		SELECT
			BT.BudgetTypeName,
			BST.BudgetSubTypeName,
			CNB.ConceptNoteBudgetID AS EntityID,
			CNB.ItemName,
			CNB.ItemDescription,
			CNB.UnitCost,
			FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNB.Quantity,
			FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue as TotalCost, 
			CNB.UnitOfIssue,
			CNB.QuantityOfIssue
		FROM dbo.ConceptNoteBudget CNB
				JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
				JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
					AND CNB.conceptnoteid = @ConceptNoteID

		UNION ALL

		SELECT
			'Equipment' as BudgetTypeName,
			'' as BudgetSubTypeName,
			CNEC.ConceptNoteEquipmentCatalogID AS EntityID,
			EC.ItemName,
			EC.ItemDescription,
			EC.UnitCost,
			FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNEC.Quantity,
			FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNEC.Quantity * EC.UnitCost * EC.UnitOfIssue AS TotalCost,
			EC.UnitOfIssue,
			EC.QuantityOfIssue
		FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = @ConceptNoteID
		)

	SELECT
		BTS.BudgetTypeSequence,
		BSS.BudgetSubTypeSequence,
		ROW_NUMBER() OVER (PARTITION BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName ORDER BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName, CNBD.ItemName, CNBD.Quantity * CNBD.UnitCost, CNBD.EntityID) AS ItemSequence,
		CNBD.BudgetTypeName, 
		CNBD.BudgetSubTypeName,
		CNBD.ItemName,
		CNBD.ItemDescription,
		CNBD.UnitCostFormatted,
		CNBD.Quantity,
		CNBD.TotalCostFormatted,
		CNBD.TotalCost,
		CNBD.UnitOfIssue,
		CNBD.QuantityOfIssue
	FROM CNBD
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (ORDER BY BTN.BudgetTypeName) AS BudgetTypeSequence,
				BTN.BudgetTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName 
				FROM CNBD
				) BTN
			) BTS ON BTS.BudgetTypeName = CNBD.BudgetTypeName
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY BSN.BudgetTypeName ORDER BY BSN.BudgetTypeName, BSN.BudgetSubTypeName) AS BudgetSubTypeSequence,
				BSN.BudgetTypeName,
				BSN.BudgetSubTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName,
					CNBD.BudgetSubTypeName
				FROM CNBD
				) BSN
			) BSS ON BSS.BudgetTypeName = CNBD.BudgetTypeName
				AND BSS.BudgetSubTypeName = CNBD.BudgetSubTypeName

END
GO
--End procedure reporting.GetConceptNoteBudgetByConceptNoteID

--Begin procedure reporting.GetConceptNoteClassLocationsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteClassLocationsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteClassLocationsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cLocation NVARCHAR(MAX) = ' ', @cCommunity NVARCHAR(MAX) = '', @cResult NVARCHAR(MAX) = ' '
	
	SELECT @cLocation += Location + N' ' 
	FROM dbo.ConceptNoteClass CC
		JOIN dbo.Class C ON C.ClassID = CC.ClassID	AND CC.ConceptNoteID = @ConceptNoteID

	SELECT @cCommunity += C.CommunityName + N' ' 
	FROM dbo.ConceptNoteClass CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID	AND CC.ConceptNoteID = @ConceptNoteID
		JOIN dbo.Community C ON C.CommunityID = CL.CommunityID	
	IF LEN(@cCommunity) > 0 
		SELECT @cResult = @cCommunity
	ELSE 
		SELECT @cResult = @cLocation
	--ENDIF
	
	IF LEN(RTRIM(@cResult)) = 0 
		SELECT @cResult AS Locations
	ELSE
		SELECT SUBSTRING(@cResult, 0, LEN(@cResult)) AS Locations
	--ENDIF

END
GO
--End procedure reporting.GetConceptNoteClassLocationsByConceptNoteID

--Begin procedure reporting.GetConceptNoteCloseoutByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteCloseoutByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to return data for the concept note closeout reports
-- ====================================================================================
CREATE PROCEDURE reporting.GetConceptNoteCloseoutByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActualTotalAmount,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID ) AS ReferenceCode,
		CN.DeobligatedAmount,
		CN.DescriptionOfImpact,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.FemaleAdultCount,
		CN.FemaleAdultCountActual,
		CN.FemaleAdultDetails,
		CN.FemaleYouthCount,
		CN.FemaleYouthCountActual,
		CN.FemaleYouthDetails,
		CN.FemaleAdultCount + CN.FemaleYouthCount + CN.MaleAdultCount + CN.MaleYouthCount AS TotalCount,
		CN.FemaleAdultCountActual + CN.FemaleYouthCountActual + CN.MaleAdultCountActual + CN.MaleYouthCountActual AS TotalCountActual,
		CN.FinalAwardAmount,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		dbo.FormatPersonNameByPersonID(CN.ImplementerID, 'LastFirst') AS ImplementerName,
		CN.IsFinalPaymentMade,
		CN.MaleAdultCount,
		CN.MaleAdultCountActual,
		CN.MaleAdultDetails,
		CN.MaleYouthCount,
		CN.MaleYouthCountActual,
		CN.MaleYouthDetails,
		CN.SpentToDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SuccessStories,
		CN.Summary,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.SpentToDate AS InitialAmount,
		CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

END
GO
--End procedure reporting.GetConceptNoteCloseoutByConceptNoteID

--Begin procedure reporting.GetConceptNoteIndicatorByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteIndicatorByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
--
-- Author:			John Lyons
-- Create date:	2015.06.29
-- Description:	Added 
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteIndicatorByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName,	
		I.AchievedDate,	
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,	
		I.AchievedValue, 	
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,	
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName + '<br>' + I.IndicatorDescription as FullLabel,	
		I.IndicatorName, 	
		I.IndicatorSource, 	
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,	
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 	
		O.ObjectiveID, 	
		O.ObjectiveName,
		CNI.targetQuantity
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
			AND CNI.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = I.IndicatorID
	ORDER BY I.IndicatorName, I.IndicatorID

END
GO
--End procedure reporting.GetConceptNoteIndicatorByConceptNoteID

--Begin procedure reporting.GetConceptNotePartnersByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNotePartnersByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			John Lyons
-- Create date:	2015.07.05
-- Description:	
-- ==================================================================
CREATE PROCEDURE reporting.GetConceptNotePartnersByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SC.SubContractorName
	FROM dbo.SubContractor SC
	WHERE EXISTS
		(
		SELECT 1
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID
			AND
				(
				CN.AwardeeSubContractorID1 = SC.SubContractorID 
					OR CN.AwardeeSubContractorID2 = SC.SubContractorID
				)
		)
	ORDER BY SC.SubContractorName
		
END
GO
--End procedure reporting.GetConceptNotePartnersByConceptNoteID

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
-- ==================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) as DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,	
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.FaceBookpageURL
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetEquipmentDistributionPlanByConceptNoteID
EXEC Utility.DropObject 'reporting.GetEquipmentDistributionPlanByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetEquipmentDistributionPlanByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CommunityList nvarchar(max) = ''

	SELECT @CommunityList += CT.CommunityName + ', ' 
	FROM dbo.ConceptNoteContactEquipment CNCE
	 JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
	  AND CNCE.ConceptNoteID = @ConceptNoteID
	  AND C.CommunityID > 0
	  JOIN Community CT ON c.CommunityID = ct.CommunityID

	IF LEN(RTRIM(@CommunityList)) > 0 
		SET @CommunityList = RTRIM(SUBSTRING(@CommunityList, 0, LEN(@CommunityList)))
	--ENDIF

	SET @CommunityList =  RTRIM(@CommunityList)
	;

	SELECT 
		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan(ConceptnoteID) AS gLink,
		dbo.FormatConceptNoteReferenceCode(ConceptNoteID) AS ReferenceCode,
		@CommunityList as CommunityList,
		ConceptNoteID,
		ExportRoute,
		CurrentSituation,
		Aim,
		PlanOutline,
		Phase1,
		Phase2,
		Phase3,
		Phase4,
		Phase5,
		OperationalResponsibility,
		Annexes,
		Distribution,
		Title,
		Summary
	FROM procurement.EquipmentDistributionPlan 
	WHERE ConceptnoteID = @ConceptNoteID
	
END
GO
--End procedure reporting.GetEquipmentDistributionPlanByConceptNoteID

--Begin procedure reporting.GetEquipmentDistributionByProvinceIDOrCommunityID
EXEC Utility.DropObject 'reporting.GetEquipmentDistributionByProvinceIDOrCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create date:	2015.06.18
-- Description:	A stored procedure to data from the EquipmentDistribution
-- ======================================================================
CREATE PROCEDURE reporting.GetEquipmentDistributionByProvinceIDOrCommunityID

@CommunityID INT = 0, 
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ProvinceName AS CommunityOrProvinceName, 
		EC.ItemName, 
		EC.ItemDescription, 
		EI.SerialNumber, 
		D.Quantity,
		ES.EquipmentStatusName,
		EC.risk,
		dbo.FormatDate(D.DistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(OACN.ConceptNoteID) AS Title
	FROM
		(
		SELECT
			SUM(T.Quantity) AS Quantity,
			MAX(T.DistributionDate) AS DistributionDate,
			T.EquipmentInventoryID,
			T.ConceptNoteID,
			P.ProvinceName 
		FROM procurement.ProvinceEquipmentInventory T
			JOIN Province P on P.ProvinceID = T.ProvinceID
				AND T.ProvinceID = @ProvinceID
		GROUP BY T.ConceptNoteID, T.EquipmentInventoryID, P.ProvinceName 
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = D.ConceptNoteID
			) OACN

	UNION ALL

	SELECT 
		D.CommunityName AS CommunityOrProvinceName, 
		EC.ItemName, 
		EC.ItemDescription, 
		EI.SerialNumber, 
		D.Quantity,
		ES.EquipmentStatusName,
		EC.risk,
		dbo.FormatDate(D.DistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(OACN.ConceptNoteID) AS Title
	FROM
		(
		SELECT
			SUM(T.Quantity) AS Quantity,
			MAX(T.DistributionDate) AS DistributionDate,
			T.EquipmentInventoryID,
			T.ConceptNoteID,
			C.CommunityName 
		FROM procurement.CommunityEquipmentInventory T
			JOIN Community C on C.CommunityID = T.CommunityID
				AND T.CommunityID = @CommunityID
		GROUP BY T.ConceptNoteID, T.EquipmentInventoryID, C.CommunityName 
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = D.ConceptNoteID
			) OACN

END
GO
--End procedure reporting.GetEquipmentDistributionByProvinceIDOrCommunityID

--Begin procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestEquipmentByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purchase request reports
-- ===============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ID INT, 
		BudgetTypeName Varchar(250),
		Quantity NUMERIC(18,4),
		ItemName VARCHAR(250), 
		UnitCost NUMERIC(18,2), 
		UnitOfIssue VARCHAR(250), 
		UnitCostFormatted VARCHAR(50), 
		TotalCost NUMERIC(18,2), 
		TotalCostFormatted VARCHAR(50),
		QuantityOfIssue NUMERIC(18,4)
		)

	INSERT INTO @tTable
		(ID,BudgetTypeName,Quantity,ItemName,UnitCost,UnitOfIssue,UnitCostFormatted,TotalCost,TotalCostFormatted,QuantityOfIssue)
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID as ID,
		'Equipment' as BudgetTypeName,
		PRCNEC.Quantity,
		EC.ItemName,
		EC.UnitCost,
		CAST(EC.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.QuantityOfIssue
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
		JOIN procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC ON PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID
			AND PRCNEC.PurchaseRequestID = @PurchaseRequestID

	UNION

	SELECT			
		CNB.ConceptNoteBudgetID as ID,
		BT.BudgetTypeName,
		PRCNB.Quantity,
		CNB.ItemName,
		CNB.UnitCost,
		CAST(CNB.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID	
		JOIN procurement.PurchaseRequestConceptNoteBudget PRCNB ON PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(ID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		T.ID,
		T.BudgetTypeName,
		T.Quantity,
		T.ItemName,
		T.UnitCost,
		T.UnitOfIssue,
		T.UnitCostFormatted,
		T.TotalCost,
		T.TotalCostFormatted,
		T.QuantityOfIssue
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

--Begin procedure reporting.GetStipendActivitySummaryPayments
EXEC Utility.DropObject 'reporting.GetStipendActivitySummaryPayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.05
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetStipendActivitySummaryPayments

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(D.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(D.YearMonth, 4) AS YearMonthFormatted,
		(SELECT P.ProvinceID FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		D.StipendStatus,
		SUM(D.StipendAmount) AS StipendAmountTotal,
		D.StipendAmountPaid,
		D.StipendAmountAuthorized
	FROM
		(
		SELECT
		CSP.StipendAmountPaid,
		CSP.StipendAmountAuthorized,
			CASE
				WHEN CSP.StipendAmountPaid IS NULL OR CSP.StipendAmountPaid = 0
				THEN CSP.StipendAmountAuthorized
				ELSE CSP.StipendAmountPaid
			END AS StipendAmount,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN CSP.PaymentYear * 100 + CSP.PaymentMonth
				WHEN CSP.StipendPaidDate IS NULL
				THEN YEAR(CSP.StipendAuthorizedDate) * 100 + MONTH(CSP.StipendAuthorizedDate)
				ELSE YEAR(CSP.StipendPaidDate) * 100 + MONTH(CSP.StipendPaidDate)
			END AS YearMonth,

			CASE
				WHEN CSP.StipendAuthorizedDate IS NULL
				THEN 'Preparation'
				WHEN CSP.StipendAuthorizedDate IS NOT NULL AND CSP.StipendPaidDate IS NULL
				THEN 'Authorized'
				ELSE 'Reconciled'
			END AS StipendStatus

		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
		) D
	GROUP BY D.YearMonth, D.StipendStatus, StipendAmountPaid,StipendAmountAuthorized
	ORDER BY D.YearMonth DESC, D.StipendStatus

END
GO
--End procedure reporting.GetStipendActivitySummaryPayments

--Begin procedure reporting.GetStipendActivitySummaryPeople
EXEC Utility.DropObject 'reporting.GetStipendActivitySummaryPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.05
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetStipendActivitySummaryPeople

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(PVT.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(PVT.YearMonth, 4) AS YearMonthFormatted,
		(SELECT P.ProvinceID FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		*
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			YEAR(CSP.StipendPaidDate) * 100 + MONTH(CSP.StipendPaidDate) AS YearMonth
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
			AND CSP.StipendPaidDate IS NOT NULL
		GROUP BY CSP.StipendPaidDate, CSP.StipendName
		) AS D
	PIVOT
		(
		MAX(D.StipendNameCount)
		FOR D.StipendName IN
			(
			[Command],[General],[Colonel],[Colonel Doctor],[Lieutenant Colonel],[Major],[Captain],[Captain Doctor],[First Lieutenant],[Contracted Officer],[First Sergeant],[Sergeant],[First Adjutant],[Adjutant],[Policeman],[Contracted Policeman]
			)
		) AS PVT
	ORDER BY PVT.YearMonth DESC

END
GO
--End procedure reporting.GetStipendActivitySummaryPeople

--Begin procedure reporting.GetStipendActivityVariancePayments
EXEC Utility.DropObject 'reporting.GetStipendActivityVariancePayments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.05
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetStipendActivityVariancePayments

@ProvinceID INT,
@Year INT,
@Month INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendAmountAuthorizedTotal,
		B.StipendAmountPaidTotal,
		B.StipendAmountPaidTotal - A.StipendAmountAuthorizedTotal AS Variance
	FROM
		(
		SELECT 
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorizedTotal,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
				AND CSP.ProvinceID = @ProvinceID
				AND CSP.PaymentYear = @Year
				AND CSP.PaymentMonth = @Month
				AND CSP.StipendAuthorizedDate IS NOT NULL
		GROUP BY CSP.StipendName, S.DisplayOrder
		) A
		JOIN
			(
			SELECT 
				SUM(CSP.StipendAmountPaid) AS StipendAmountPaidTotal,
				CSP.StipendName
			FROM dbo.ContactStipendPayment CSP
			WHERE CSP.ProvinceID = @ProvinceID
				AND CSP.PaymentYear = @Year
				AND CSP.PaymentMonth = @Month
				AND CSP.StipendPaidDate IS NOT NULL
			GROUP BY CSP.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetStipendActivityVariancePayments

--Begin procedure reporting.GetStipendActivityVariancePeople
EXEC Utility.DropObject 'reporting.GetStipendActivityVariancePeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.05
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetStipendActivityVariancePeople

@ProvinceID INT,
@Year INT,
@Month INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		A.StipendName,
		A.StipendNameCount AS AuthorizedStipendNameCount,
		B.StipendNameCount AS ReconciledStipendNameCount,
		B.StipendNameCount - A.StipendNameCount AS Variance
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			S.DisplayOrder
		FROM dbo.ContactStipendPayment CSP
			JOIN dropdown.Stipend S ON S.Stipendname = CSP.StipendName
				AND CSP.ProvinceID = @ProvinceID
				AND CSP.PaymentYear = @Year
				AND CSP.PaymentMonth = @Month
				AND CSP.StipendAuthorizedDate IS NOT NULL
		GROUP BY CSP.StipendName, S.DisplayOrder
		) A
		JOIN
			(
			SELECT 
				COUNT(CSP.StipendName) AS StipendNameCount,
				CSP.StipendName
			FROM dbo.ContactStipendPayment CSP
			WHERE CSP.ProvinceID = @ProvinceID
				AND CSP.PaymentYear = @Year
				AND CSP.PaymentMonth = @Month
				AND CSP.StipendPaidDate IS NOT NULL
			GROUP BY CSP.StipendName
			) B ON B.StipendName = A.StipendName

	ORDER BY A.DisplayOrder

END
GO
--End procedure reporting.GetStipendActivityVariancePeople
