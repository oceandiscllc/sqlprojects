USE AJACS
GO

--Begin schema finding
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'finding')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA finding'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema finding

--Begin schema project
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'project')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA project'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema project

--Begin schema recommendation
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'recommendation')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA recommendation'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema recommendation

--Begin schema recommendationupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'recommendationupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA recommendationupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema recommendationupdate

--Begin schema riskupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'riskupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA riskupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema riskupdate
