USE AJACS
GO

--Begin function survey.FormatSurveyQuestionResponse
EXEC Utility.DropObject 'dbo.FormatSurveyQuestionResponse'
EXEC Utility.DropObject 'survey.FormatSurveyQuestionResponse'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			John Lyons
-- Create date: 2015.08.10
-- Description:	A function to get survey responses
-- ===============================================
CREATE FUNCTION survey.FormatSurveyQuestionResponse
(
@SurveyQuestionLanguageCode VARCHAR(2),
@SurveyQuestionID INT,
@SurveyResponseID  INT
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @ChoiceResult NVARCHAR(MAX) = ''

	SELECT 
		@ChoiceResult += 
		(
		SELECT 
	
			CASE SQT.SurveyQuestionResponseTypeCode
				WHEN 'CommunityPicker' 
				THEN (SELECT CommunityName FROM Community WHERE CommunityID = CAST(SSQR.SurveyQuestionResponse AS INT))
				WHEN 'ProvincePicker' 
				THEN (SELECT ProvinceName FROM Province WHERE ProvinceID = CAST(SSQR.SurveyQuestionResponse AS INT))
				WHEN 'Select' 
				THEN (SELECT SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice WHERE SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND LanguageCode = @SurveyQuestionLanguageCode)
				WHEN 'SelectMultiple' 
				THEN (SELECT SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice WHERE SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND LanguageCode = @SurveyQuestionLanguageCode)
				WHEN 'YesNo' 
				THEN (SELECT SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice WHERE SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND LanguageCode = @SurveyQuestionLanguageCode)
				WHEN 'DatePicker' 
				THEN dbo.FormatDate(SSQR.SurveyQuestionResponse)
				ELSE SSQR.SurveyQuestionResponse 
			END AS Result) + ', '

	FROM survey.SurveySurveyQuestionResponse SSQR
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQR.SurveyQuestionID 
			AND SSQR.SurveyQuestionID = @SurveyQuestionID 
			AND SSQR.SurveyResponseID = @SurveyResponseID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID

	IF LEN(RTRIM(@ChoiceResult)) > 0
		SET @ChoiceResult = RTRIM(SUBSTRING(@ChoiceResult, 0, LEN(@ChoiceResult)))
	--ENDIF

	RETURN RTRIM(@ChoiceResult)

END
GO
--End function survey.FormatSurveyQuestionResponse