USE AJACS
GO
--Begin procedure dbo.DeleteConceptNoteContactByConceptNoteContactID
EXEC Utility.DropObject 'dbo.DeleteConceptNoteContactByConceptNoteContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	A stored procedure to delete data from the dbo.ConceptNoteContact table
-- ====================================================================================
CREATE PROCEDURE dbo.DeleteConceptNoteContactByConceptNoteContactID

@ConceptNoteContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE CNC
	FROM dbo.ConceptNoteContact CNC
	WHERE CNC.ConceptNoteContactID = @ConceptNoteContactID
	
END
GO
--End procedure dbo.DeleteConceptNoteContactByConceptNoteContactID

--Begin procedure dbo.GetConceptNoteContactNotesByConceptNoteContactID
EXEC Utility.DropObject 'dbo.GetConceptNoteContactNotesByConceptNoteContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	A stored procedure to get data from the dbo.ConceptNoteContact table
-- =================================================================================
CREATE PROCEDURE dbo.GetConceptNoteContactNotesByConceptNoteContactID

@ConceptNoteContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT CNC.Notes
	FROM dbo.ConceptNoteContact CNC
	WHERE CNC.ConceptNoteContactID = @ConceptNoteContactID
	
END
GO
--End procedure dbo.GetConceptNoteContactNotesByConceptNoteContactID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.ArabicMotherName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PlaceOfBirthCountryID,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetCommunityAssetByCommunityAssetID
EXEC Utility.DropObject 'dbo.GetCommunityAssetByCommunityAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to get data from the dbo.CommunityAsset table
-- =============================================================================
CREATE PROCEDURE dbo.GetCommunityAssetByCommunityAssetID

@CommunityAssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CA.CommunityAssetDescription,
		CA.Location.STAsText() AS Location,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CA.CommunityAssetID = @CommunityAssetID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.CommunityAssetCommunity CAC ON CAC.CommunityID = C.CommunityID
			AND CAC.CommunityAssetID = @CommunityAssetID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.CommunityAssetProvince CAP ON CAP.ProvinceID = P.ProvinceID
			AND CAP.CommunityAssetID = @CommunityAssetID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.CommunityAssetRisk CAR ON CAR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CAR.CommunityAssetID = @CommunityAssetID
	
	SELECT
		CAU.CommunityAssetUnitID,
		CAU.CommunityAssetUnitName,
		CAUT.CommunityAssetUnitTypeID,
		CAUT.CommunityAssetUnitTypeName,
		CAUC.CommunityAssetUnitCostRateID,
		CAUC.CommunityAssetUnitCostRate
	FROM dbo.CommunityAssetUnit CAU
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
		JOIN dropdown.CommunityAssetUnitCostRate CAUC ON CAUC.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
			AND CAU.CommunityAssetID = @CommunityAssetID

END
GO
--End procedure dbo.GetCommunityAssetByCommunityAssetID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.27
-- Description:	Bug fixes
--
-- Author:			Todd Pires
-- Create Date: 2015.0.27
-- Description:	Added Recommendations
-- ==============================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN 'fa fa-fw fa-lightbulb-o'
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN 'fa fa-fw fa-question-circle'
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN 'fa fa-fw fa-calendar'
		END AS Icon,

		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN OAR.RecommendationName
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		CASE
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN OAWR.PhysicalFileName
			ELSE NULL
		END AS PhysicalFileName,

		D.EntityID,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('Recommendation','RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'Recommendation' AND permissionable.HasPermission('Recommendation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				R.RecommendationName,
				R.IsActive
			FROM recommendation.Recommendation R
			WHERE R.RecommendationID = D.EntityID
					AND D.EntityTypeCode = 'Recommendation'
			) OAR
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle,
				SR.WorkflowStepNumber
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
			AND
				(
				D.EntityTypeCode <> 'Recommendation'
					OR OAR.IsActive = 1
				)
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
			AND
				(
				D.EntityTypeCode <> 'SpotReport'
					OR OASR.WorkflowStepNumber > (SELECT W.WorkflowStepCount FROM workflow.Workflow W WHERE W.EntityTypeCode = 'SpotReport')
				)
			AND
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)

	ORDER BY D.UpdateDate DESC, D.EntityTypeCode, D.EntityID
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dbo.GetRiskByRiskID
EXEC Utility.DropObject 'dbo.GetRiskByRiskID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
--
-- Author:			Kevin Ross
-- Create date: 2015.08.09
-- Description:	Add support for the RiskType
-- ===================================================================
CREATE PROCEDURE dbo.GetRiskByRiskID

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeID,
		RT.RiskTypeName
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND R.RiskID = @RiskID

END
GO
--End procedure dbo.GetRiskByRiskID

--Begin procedure dropdown.GetComponentData
EXEC Utility.DropObject 'dropdown.GetComponentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.05
-- Description:	A stored procedure to return data from the dropdown.Component table
-- ================================================================================
CREATE PROCEDURE dropdown.GetComponentData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ComponentID,
		T.ComponentAbbreviation,
		T.ComponentCode,
		T.ComponentName
	FROM dropdown.Component T
	WHERE (T.ComponentID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ComponentName, T.ComponentID

END
GO
--End procedure dropdown.GetComponentData

--Begin procedure dbo.ValidateGovernmentIDNumber
EXEC Utility.DropObject 'dbo.ValidateGovernmentIDNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the dbo.Contact table
-- ======================================================================
CREATE PROCEDURE dbo.ValidateGovernmentIDNumber

@GovernmentIDNumber NVARCHAR(40),
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.ContactID) AS ItemCount
	FROM dbo.Contact C
	WHERE C.GovernmentIDNumber = @GovernmentIDNumber
		AND C.ContactID <> @ContactID

END
GO
--End procedure dbo.ValidateGovernmentIDNumber

--Begin procedure dbo.ValidatePassportNumber
EXEC Utility.DropObject 'dbo.ValidatePassportNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the dbo.Contact table
-- ======================================================================
CREATE PROCEDURE dbo.ValidatePassportNumber

@PassportNumber NVARCHAR(40),
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.ContactID) AS ItemCount
	FROM dbo.Contact C
	WHERE C.PassportNumber = @PassportNumber
		AND C.ContactID <> @ContactID

END
GO
--End procedure dbo.ValidatePassportNumber

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID,
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
	WHERE (T.AssetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetCommunityAssetTypeData
EXEC Utility.DropObject 'dropdown.GetCommunityAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.CommunityAssetType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCommunityAssetTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityAssetTypeID,
		T.CommunityAssetTypeName
	FROM dropdown.CommunityAssetType T
	WHERE (T.CommunityAssetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityAssetTypeName, T.CommunityAssetTypeID

END
GO
--End procedure dropdown.GetCommunityAssetTypeData

--Begin procedure dropdown.GetCommunityAssetUnitCostRateData
EXEC Utility.DropObject 'dropdown.GetCommunityAssetUnitCostRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.CommunityAssetUnitCostRate table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetCommunityAssetUnitCostRateData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityAssetUnitCostRateID,
		T.CommunityAssetUnitCostRate
	FROM dropdown.CommunityAssetUnitCostRate T
	WHERE (T.CommunityAssetUnitCostRateID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityAssetUnitCostRate, T.CommunityAssetUnitCostRateID

END
GO
--End procedure dropdown.GetCommunityAssetUnitCostRateData

--Begin procedure dropdown.GetCommunityAssetUnitTypeData
EXEC Utility.DropObject 'dropdown.GetCommunityAssetUnitTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.CommunityAssetUnitType table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetCommunityAssetUnitTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityAssetUnitTypeID,
		T.CommunityAssetUnitTypeName
	FROM dropdown.CommunityAssetUnitType T
	WHERE (T.CommunityAssetUnitTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityAssetUnitTypeName, T.CommunityAssetUnitTypeID

END
GO
--End procedure dropdown.GetCommunityAssetUnitTypeData

--Begin procedure dropdown.GetCountryCallingDataByCountryCallingCodeID
EXEC Utility.DropObject 'dropdown.GetCountryCallingDataByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.15
-- Description:	A stored procedure to return data from the dropdown.Country and dropdown.CountryCallingCode tables
-- ===============================================================================================================
CREATE PROCEDURE dropdown.GetCountryCallingDataByCountryCallingCodeID

@CountryCallingCodeID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ISOCountryCode2, 
		CCC.CountryCallingCode
	FROM dropdown.CountryCallingCode CCC
		JOIN dropdown.Country C ON C.CountryID = CCC.CountryID
			AND CCC.CountryCallingCodeID = @CountryCallingCodeID

END
GO
--End procedure dropdown.GetCountryCallingDataByCountryCallingCodeID

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to return data from the dropdown.Country table
--
-- Author:			Todd Pires
-- Create date:	2015.08.15
-- Description:	Added CountryCallingCode support
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		C.CountryID, 
		C.CountryName,
		C.ISOCountryCode2, 
		C.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country C
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = C.CountryID 
			AND (C.CountryID > 0 OR @IncludeZero = 1)
			AND C.IsActive = 1
	ORDER BY C.DisplayOrder, C.CountryName, C.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetFindingStatusData
EXEC Utility.DropObject 'dropdown.GetFindingStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.05
-- Description:	A stored procedure to return data from the dropdown.FindingStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFindingStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingStatusID,
		T.FindingStatusName
	FROM dropdown.FindingStatus T
	WHERE (T.FindingStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingStatusName, T.FindingStatusID

END
GO
--End procedure dropdown.GetFindingStatusData

--Begin procedure dropdown.GetFindingTypeData
EXEC Utility.DropObject 'dropdown.GetFindingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.05
-- Description:	A stored procedure to return data from the dropdown.FindingType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetFindingTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingTypeID,
		T.FindingTypeName
	FROM dropdown.FindingType T
	WHERE (T.FindingTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingTypeName, T.FindingTypeID

END
GO
--End procedure dropdown.GetFindingTypeData

--Begin procedure dropdown.GetProjectStatusData
EXEC Utility.DropObject 'dropdown.GetProjectStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.07
-- Description:	A stored procedure to return data from the dropdown.ProjectStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProjectStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectStatusID,
		T.ProjectStatusName
	FROM dropdown.ProjectStatus T
	WHERE (T.ProjectStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectStatusName, T.ProjectStatusID

END
GO
--End procedure dropdown.GetProjectStatusData

--Begin procedure dropdown.GetRiskTypeData
EXEC Utility.DropObject 'dropdown.GetRiskTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to return data from the dropdown.RiskType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetRiskTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RiskTypeID,
		T.RiskTypeName
	FROM dropdown.RiskType T
	WHERE (T.RiskTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RiskTypeName, T.RiskTypeID

END
GO
--End procedure dropdown.GetRiskTypeData

--Begin procedure dropdown.GetZoneTypeData
EXEC Utility.DropObject 'dropdown.GetZoneTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.ZoneType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetZoneTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ZoneTypeID,
		T.ZoneTypeName,
		T.HexColor
	FROM dropdown.ZoneType T
	WHERE (T.ZoneTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ZoneTypeName, T.ZoneTypeID

END
GO
--End procedure dropdown.GetZoneTypeData

--Begin procedure eventlog.LogCommunityAssetAction
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cCommunityAssetCommunities NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetCommunities = COALESCE(@cCommunityAssetCommunities, '') + D.CommunityAssetCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetCommunity'), ELEMENTS) AS CommunityAssetCommunity
			FROM dbo.CommunityAssetCommunity T 
			WHERE T.CommunityAssetID = @EntityID
			) D
				
		DECLARE @cCommunityAssetProvinces NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetProvinces = COALESCE(@cCommunityAssetProvinces, '') + D.CommunityAssetProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetProvince'), ELEMENTS) AS CommunityAssetProvince
			FROM dbo.CommunityAssetProvince T 
			WHERE T.CommunityAssetID = @EntityID
			) D			

		DECLARE @cCommunityAssetRisks NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetRisks = COALESCE(@cCommunityAssetRisks, '') + D.CommunityAssetRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetRisk'), ELEMENTS) AS CommunityAssetRisk
			FROM dbo.CommunityAssetRisk T 
			WHERE T.CommunityAssetID = @EntityID
			) D	

		DECLARE @cCommunityAssetUnits NVARCHAR(MAX)

		SELECT 
			@cCommunityAssetUnits = COALESCE(@cCommunityAssetUnits, '') + D.CommunityAssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetUnit'), ELEMENTS) AS CommunityAssetUnit
			FROM dbo.CommunityAssetUnit T 
			WHERE T.CommunityAssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.CommunityAssetID,
			T.CommunityAssetName,
			T.CommunityAssetDescription,
			T.CommunityAssetTypeID,
			T.AssetTypeID,
			T.ZoneTypeID,
			T.Location.STAsText(),
			CAST(('<CommunityAssetCommunities>' + ISNULL(@cCommunityAssetCommunities , '') + '</CommunityAssetCommunities>') AS XML), 
			CAST(('<CommunityAssetProvinces>' + ISNULL(@cCommunityAssetProvinces , '') + '</CommunityAssetProvinces>') AS XML),
			CAST(('<CommunityAssetRisks>' + ISNULL(@cCommunityAssetRisks  , '') + '</CommunityAssetRisks>') AS XML),
			CAST(('<CommunityAssetUnits>' + ISNULL(@cCommunityAssetUnits  , '') + '</CommunityAssetUnits>') AS XML)
			FOR XML RAW('CommunityAsset'), ELEMENTS
			)
		FROM dbo.CommunityAsset T
		WHERE T.CommunityAssetID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAssetAction

--Begin procedure eventlog.LogFindingAction
EXEC utility.DropObject 'eventlog.LogFindingAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFindingAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cFindingCommunities VARCHAR(MAX) 
	
		SELECT 
			@cFindingCommunities = COALESCE(@cFindingCommunities, '') + D.FindingCommunities 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingCommunities'), ELEMENTS) AS FindingCommunities
			FROM finding.FindingCommunity T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingComponents VARCHAR(MAX) 
	
		SELECT 
			@cFindingComponents = COALESCE(@cFindingComponents, '') + D.FindingComponents 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingComponents'), ELEMENTS) AS FindingComponents
			FROM finding.FindingComponent T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingIndicators VARCHAR(MAX) 
	
		SELECT 
			@cFindingIndicators = COALESCE(@cFindingIndicators, '') + D.FindingIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingIndicators'), ELEMENTS) AS FindingIndicators
			FROM finding.FindingIndicator T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingProvinces VARCHAR(MAX) 
	
		SELECT 
			@cFindingProvinces = COALESCE(@cFindingProvinces, '') + D.FindingProvinces 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingProvinces'), ELEMENTS) AS FindingProvinces
			FROM finding.FindingProvince T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cFindingRecommendations = COALESCE(@cFindingRecommendations, '') + D.FindingRecommendations 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingRecommendations'), ELEMENTS) AS FindingRecommendations
			FROM finding.FindingRecommendation T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingRisks VARCHAR(MAX) 
	
		SELECT 
			@cFindingRisks = COALESCE(@cFindingRisks, '') + D.FindingRisks 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingRisks'), ELEMENTS) AS FindingRisks
			FROM finding.FindingRisk T 
			WHERE T.FindingID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<FindingCommunities>' + ISNULL(@cFindingCommunities, '') + '</FindingCommunities>') AS XML),
			CAST(('<FindingComponents>' + ISNULL(@cFindingComponents, '') + '</FindingComponents>') AS XML),
			CAST(('<FindingIndicators>' + ISNULL(@cFindingIndicators, '') + '</FindingIndicators>') AS XML),
			CAST(('<FindingProvinces>' + ISNULL(@cFindingProvinces, '') + '</FindingProvinces>') AS XML),
			CAST(('<FindingRecommendations>' + ISNULL(@cFindingRecommendations, '') + '</FindingRecommendations>') AS XML),
			CAST(('<FindingRisks>' + ISNULL(@cFindingRisks, '') + '</FindingRisks>') AS XML)
			FOR XML RAW('Finding'), ELEMENTS
			)
		FROM finding.Finding T
		WHERE T.FindingID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFindingAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.07
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Project',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cProjectCommunities VARCHAR(MAX) 
	
		SELECT 
			@cProjectCommunities = COALESCE(@cProjectCommunities, '') + D.ProjectCommunities 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCommunities'), ELEMENTS) AS ProjectCommunities
			FROM project.ProjectCommunity T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectFindings VARCHAR(MAX) 
	
		SELECT 
			@cProjectFindings = COALESCE(@cProjectFindings, '') + D.ProjectFindings 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectFindings'), ELEMENTS) AS ProjectFindings
			FROM project.ProjectFinding T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectIndicators VARCHAR(MAX) 
	
		SELECT 
			@cProjectIndicators = COALESCE(@cProjectIndicators, '') + D.ProjectIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectIndicators'), ELEMENTS) AS ProjectIndicators
			FROM project.ProjectIndicator T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectProvinces VARCHAR(MAX) 
	
		SELECT 
			@cProjectProvinces = COALESCE(@cProjectProvinces, '') + D.ProjectProvinces 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectProvinces'), ELEMENTS) AS ProjectProvinces
			FROM project.ProjectProvince T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cProjectRecommendations = COALESCE(@cProjectRecommendations, '') + D.ProjectRecommendations 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectRecommendations'), ELEMENTS) AS ProjectRecommendations
			FROM project.ProjectRecommendation T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Project',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ProjectCommunities>' + ISNULL(@cProjectCommunities, '') + '</ProjectCommunities>') AS XML),
			CAST(('<ProjectFindings>' + ISNULL(@cProjectFindings, '') + '</ProjectFindings>') AS XML),
			CAST(('<ProjectIndicators>' + ISNULL(@cProjectIndicators, '') + '</ProjectIndicators>') AS XML),
			CAST(('<ProjectProvinces>' + ISNULL(@cProjectProvinces, '') + '</ProjectProvinces>') AS XML),
			CAST(('<ProjectRecommendations>' + ISNULL(@cProjectRecommendations, '') + '</ProjectRecommendations>') AS XML)
			FOR XML RAW('Project'), ELEMENTS
			)
		FROM project.Project T
		WHERE T.ProjectID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction

--Begin procedure eventlog.LogRecommendationAction
EXEC utility.DropObject 'eventlog.LogRecommendationAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRecommendationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRecommendationCommunities VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationCommunities = COALESCE(@cRecommendationCommunities, '') + D.RecommendationCommunities 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationCommunities'), ELEMENTS) AS RecommendationCommunities
			FROM recommendation.RecommendationCommunity T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationComponents VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationComponents = COALESCE(@cRecommendationComponents, '') + D.RecommendationComponents 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationComponents'), ELEMENTS) AS RecommendationComponents
			FROM recommendation.RecommendationComponent T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationIndicators VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationIndicators = COALESCE(@cRecommendationIndicators, '') + D.RecommendationIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationIndicators'), ELEMENTS) AS RecommendationIndicators
			FROM recommendation.RecommendationIndicator T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationProvinces VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationProvinces = COALESCE(@cRecommendationProvinces, '') + D.RecommendationProvinces 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationProvinces'), ELEMENTS) AS RecommendationProvinces
			FROM recommendation.RecommendationProvince T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationRisks VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationRisks = COALESCE(@cRecommendationRisks, '') + D.RecommendationRisks 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationRisks'), ELEMENTS) AS RecommendationRisks
			FROM recommendation.RecommendationRisk T 
			WHERE T.RecommendationID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RecommendationCommunities>' + ISNULL(@cRecommendationCommunities, '') + '</RecommendationCommunities>') AS XML),
			CAST(('<RecommendationComponents>' + ISNULL(@cRecommendationComponents, '') + '</RecommendationComponents>') AS XML),
			CAST(('<RecommendationIndicators>' + ISNULL(@cRecommendationIndicators, '') + '</RecommendationIndicators>') AS XML),
			CAST(('<RecommendationProvinces>' + ISNULL(@cRecommendationProvinces, '') + '</RecommendationProvinces>') AS XML),
			CAST(('<RecommendationRisks>' + ISNULL(@cRecommendationRisks, '') + '</RecommendationRisks>') AS XML)
			FOR XML RAW('Recommendation'), ELEMENTS
			)
		FROM recommendation.Recommendation T
		WHERE T.RecommendationID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRecommendationAction

--Begin procedure eventlog.LogRecommendationUpdateAction
EXEC utility.DropObject 'eventlog.LogRecommendationUpdateAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.16
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRecommendationUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'RecommendationUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRecommendationUpdateRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationUpdateRecommendations = COALESCE(@cRecommendationUpdateRecommendations, '') + D.Recommendations
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('Recommendations'), ELEMENTS) AS Recommendations
			FROM recommendationupdate.Recommendation T 
			WHERE T.RecommendationUpdateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'RecommendationUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RecommendationUpdateRecommendations>' + ISNULL(@cRecommendationUpdateRecommendations, '') + '</RecommendationUpdateRecommendations>') AS XML)
			FOR XML RAW('RecommendationUpdate'), ELEMENTS
			)
		FROM recommendationupdate.RecommendationUpdate T
		WHERE T.RecommendationUpdateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRecommendationUpdateAction

--Begin procedure eventlog.LogRiskUpdateAction
EXEC utility.DropObject 'eventlog.LogRiskUpdateAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRiskUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'RiskUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRiskUpdateRisks VARCHAR(MAX) 
	
		SELECT 
			@cRiskUpdateRisks = COALESCE(@cRiskUpdateRisks, '') + D.Risks
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('Risks'), ELEMENTS) AS Risks
			FROM riskupdate.Risk T 
			WHERE T.RiskUpdateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'RiskUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RiskUpdateRisks>' + ISNULL(@cRiskUpdateRisks, '') + '</RiskUpdateRisks>') AS XML)
			FOR XML RAW('RiskUpdate'), ELEMENTS
			)
		FROM riskupdate.RiskUpdate T
		WHERE T.RiskUpdateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRiskUpdateAction

--Begin procedure finding.GetFindingByFindingID
EXEC Utility.DropObject 'finding.GetFindingByFindingID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.06
-- Description:	A stored procedure to get data from the finding.Finding table
-- ==========================================================================
CREATE PROCEDURE finding.GetFindingByFindingID

@FindingID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingDescription,
		F.IsResearchElement,
		F.IsActive,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND F.FindingID = @FindingID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM finding.FindingCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
			AND FC.FindingID = @FindingID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		C.ComponentID,
		C.ComponentName
	FROM finding.FindingComponent FC
		JOIN dropdown.Component C ON C.ComponentID = FC.ComponentID
			AND FC.FindingID = @FindingID
	ORDER BY C.ComponentName, C.ComponentID

	SELECT
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM finding.FindingIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.FindingID = @FindingID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM finding.FindingProvince FP
		JOIN dbo.Province P ON P.ProvinceID = FP.ProvinceID
			AND FP.FindingID = @FindingID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RecommendationID,
		R.RecommendationName
	FROM finding.FindingRecommendation FR
		JOIN recommendation.Recommendation R ON R.RecommendationID = FR.RecommendationID
			AND FR.FindingID = @FindingID
	ORDER BY R.RecommendationName, R.RecommendationID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName
	FROM finding.FindingRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.FindingID = @FindingID
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure finding.GetFindingByFindingID

--Begin procedure project.GetProjectByProjectID
EXEC Utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.07
-- Description:	A stored procedure to get data from the project.Project table
-- ==========================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProjectID, 
		P.ProjectName, 
		P.ProjectDescription, 
		P.ProjectSummary, 
		P.ProjectPartner, 
		P.ProjectValue, 
		P.ConceptNoteID,
		dbo.FormatConceptNoteTitle(P.ConceptNoteID) AS ConceptNoteTitle,
		PS.ProjectStatusID,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND P.ProjectID = @ProjectID

	SELECT
		C.CommunityID,
		C.CommunityName,
		PC.ProjectCommunityValue
	FROM project.ProjectCommunity PC
		JOIN dbo.Community C ON C.CommunityID = PC.CommunityID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusName,
		FT.FindingTypeName
	FROM project.ProjectFinding PF
		JOIN finding.Finding F ON F.FindingID = PF.FindingID
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND PF.ProjectID = @ProjectID
	ORDER BY F.FindingName, F.FindingID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM project.ProjectIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.ProjectID = @ProjectID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName,
		PP.ProjectProvinceValue
	FROM project.ProjectProvince PP
		JOIN dbo.Province P ON P.ProvinceID = PP.ProvinceID
			AND PP.ProjectID = @ProjectID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RecommendationID,
		R.RecommendationName
	FROM project.ProjectRecommendation FR
		JOIN recommendation.Recommendation R ON R.RecommendationID = FR.RecommendationID
			AND FR.ProjectID = @ProjectID
	ORDER BY R.RecommendationName, R.RecommendationID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure recommendation.GetRecommendationByRecommendationID
EXEC Utility.DropObject 'recommendation.GetRecommendationByRecommendationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to get data from the recommendation.Recommendation table
-- ========================================================================================
CREATE PROCEDURE recommendation.GetRecommendationByRecommendationID

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendation.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.Community C ON C.CommunityID = RC.CommunityID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		C.ComponentID,
		C.ComponentName
	FROM recommendation.RecommendationComponent RC
		JOIN dropdown.Component C ON C.ComponentID = RC.ComponentID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.ComponentName, C.ComponentID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM recommendation.RecommendationIndicator RI
		JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND RI.RecommendationID = @RecommendationID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.Province P ON P.ProvinceID = RP.ProvinceID
			AND RP.RecommendationID = @RecommendationID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RT.RiskTypeName
	FROM recommendation.RecommendationRisk RR
		JOIN dbo.Risk R ON R.RiskID = RR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND RR.RecommendationID = @RecommendationID
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure recommendation.GetRecommendationByRecommendationID

--Begin procedure recommendationupdate.ApproveRecommendationUpdateRecommendations
EXEC Utility.DropObject 'recommendationupdate.ApproveRecommendationUpdateRecommendations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to submit recommendations in a recommendation update for approval
-- =============================================================================
CREATE PROCEDURE recommendationupdate.ApproveRecommendationUpdateRecommendations

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	
	DECLARE @RecommendationID INT
	DECLARE @RecommendationUpdateID INT = (SELECT TOP 1 RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU ORDER BY RU.RecommendationUpdateID DESC)
	DECLARE @tOutput TABLE (RecommendationID INT)

	UPDATE R
	SET
		R.IsActive = RUR.IsActive,
		R.IsResearchElement = RUR.IsResearchElement,
		R.RecommendationDescription = RUR.RecommendationDescription,
		R.RecommendationName = RUR.RecommendationName
	OUTPUT INSERTED.RecommendationID INTO @tOutput
	FROM dbo.Recommendation R
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = R.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.RecommendationID
		FROM @tOutput O
		ORDER BY O.RecommendationID
	
	OPEN oCursor
	FETCH oCursor INTO @RecommendationID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'read', @PersonID, NULL
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @RecommendationID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'update', @PersonID, NULL
	
	TRUNCATE TABLE recommendationupdate.Recommendation
	DELETE FROM recommendationupdate.RecommendationUpdate

END
GO
--End procedure recommendationupdate.ApproveRecommendationUpdateRisks

--Begin procedure recommendationupdate.DeleteRecommendationUpdateRecommendation
EXEC Utility.DropObject 'recommendationupdate.DeleteRecommendationUpdateRecommendation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to remove a recommendation from the recommendation update
-- =========================================================================================
CREATE PROCEDURE recommendationupdate.DeleteRecommendationUpdateRecommendation

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE R
	FROM recommendationupdate.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

END
GO
--End procedure recommendationupdate.DeleteRecommendationUpdateRecommendation

--Begin procedure recommendationupdate.GetRecommendationUpdate
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the recommendationupdate.RecommendationUpdate table
-- ====================================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRecommendationUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM recommendationupdate.RecommendationUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RecommendationUpdateID INT)

		INSERT INTO recommendationupdate.RecommendationUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RecommendationUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.RecommendationUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'

		SELECT @nRecommendationUpdateID = O.RecommendationUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRecommendationUpdateAction @EntityID=@nRecommendationUpdateID, @EventCode='Create', @PersonID = @PersonID

		END
	ELSE
		SELECT @nRecommendationUpdateID = RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU
	--ENDIF
	
	SELECT
		RU.RecommendationUpdateID, 
		RU.WorkflowStepNumber 
	FROM recommendationupdate.RecommendationUpdate RU

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
			JOIN recommendationupdate.RecommendationUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'Recommendation.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nRecommendationUpdateID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'RecommendationUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @nRecommendationUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure recommendationupdate.GetRecommendationUpdate

--Begin procedure recommendationupdate.GetRecommendationUpdateRecommendation
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdateRecommendation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get recommendation data for the recommendation update
-- ========================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdateRecommendation

@RecommendationID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendation.Recommendation R
	WHERE R.RecommendationID = @RecommendationID
			AND R.RecommendationID = @RecommendationID

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendationupdate.Recommendation R
	WHERE R.RecommendationID = @RecommendationID
			AND R.RecommendationID = @RecommendationID

END
GO
--End procedure recommendationupdate.GetRecommendationUpdateRecommendation

--Begin procedure recommendationupdate.PopulateRecommendationUpdateRecommendations
EXEC Utility.DropObject 'recommendationupdate.PopulateRecommendationUpdateRecommendations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to add recommendation data to the recommendation update
-- =======================================================================================
CREATE PROCEDURE recommendationupdate.PopulateRecommendationUpdateRecommendations

@RecommendationIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO recommendationupdate.Recommendation
		(RecommendationID, RecommendationUpdateID, RecommendationName, RecommendationDescription, IsResearchElement, IsActive)

	SELECT
		R.RecommendationID, 
		(SELECT TOP 1 RU.RecommendationUpdateID FROM recommendation.RecommendationUpdate RU ORDER BY RU.RecommendationUpdateID DESC),
		R.RecommendationName, 
		R.RecommendationDescription, 
		R.IsResearchElement, 
		R.IsActive
	FROM recommendation.Recommendation R
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = R.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.Recommendation RUR
				WHERE RUR.RecommendationID = R.RecommendationID
				)

END
GO
--End procedure recommendationupdate.PopulateRecommendationUpdateRecommendations

--Begin procedure reporting.GetConceptNoteRiskByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteRiskByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetConceptNoteRiskByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeName
	FROM ConceptNoteRisk CNR
		JOIN Risk R ON R.RiskID = CNR.RiskID AND CNR.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType  RT ON RT.RiskTypeID = R.RiskTypeID
END
GO
--End procedure reporting.GetConceptNoteRiskByConceptNoteID

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
-- ==================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) as DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.MiddleName,
		C1.PassportNumber,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetRiskReportByPersonID
EXEC Utility.DropObject 'reporting.GetRiskReportByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.08.01
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetRiskReportByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeName
	FROM reporting.SearchResult SR
		JOIN Risk R ON R.RiskID = SR.EntityID  
			AND SR.PersonID = @PersonID  
			AND SR.EntityTypeCode='Risk'
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType  RT ON RT.RiskTypeID = R.RiskTypeID

END
GO
--End procedure reporting.GetRiskReportByPersonID

--Begin procedure reporting.GetSurveyQuestionAndAnswers
EXEC Utility.DropObject 'reporting.GetSurveyQuestionAndAnswers'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.10
-- Description:	A stored procedure to get data from the survey.SurveyResponse table
-- ================================================================================
CREATE PROCEDURE reporting.GetSurveyQuestionAndAnswers

@SurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		SR.SurveyResponseID,
		survey.FormatSurveyQuestionResponse(SR.LanguageCode, SSQ.SurveyQuestionID, SR.SurveyResponseID) AS SurveyQuestionAnswer,
		SSQL.SurveyQuestionText
	FROM survey.SurveyResponse SR
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyID = SR.SurveyID 
			AND SR.SurveyID = @SurveyID
		JOIN survey.SurveyQuestionLabel SSQL ON SSQL.SurveyQuestionID = SSQ.SurveyQuestionID 
			AND SSQL.LanguageCode = SR.LanguageCode

END
GO
--End procedure reporting.GetSurveyQuestionAndAnswers

--Begin procedure riskupdate.ApproveRiskUpdateRisks
EXEC Utility.DropObject 'riskupdate.ApproveRiskUpdateRisks'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to submit risks in a risk update for approval
-- =============================================================================
CREATE PROCEDURE riskupdate.ApproveRiskUpdateRisks

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	
	DECLARE @RiskID INT
	DECLARE @RiskUpdateID INT = (SELECT TOP 1 RU.RiskUpdateID FROM riskupdate.RiskUpdate RU ORDER BY RU.RiskUpdateID DESC)
	DECLARE @tOutput TABLE (RiskID INT)

	UPDATE R
	SET
		R.DateRaised = RUR.DateRaised,
		R.EscalationCriteria = RUR.EscalationCriteria,
		R.Impact = RUR.Impact,
		R.Likelihood = RUR.Likelihood,
		R.MitigationActions = RUR.MitigationActions,
		R.Notes = RUR.Notes,
		R.Overall = RUR.Overall,
		R.RaisedBy = RUR.RaisedBy,
		R.RiskCategoryID = RUR.RiskCategoryID,
		R.RiskDescription = RUR.RiskDescription,
		R.RiskName = RUR.RiskName,
		R.RiskStatusID = RUR.RiskStatusID,
		R.RiskTypeID = RUR.RiskTypeID
	OUTPUT INSERTED.RiskID INTO @tOutput
	FROM dbo.Risk R
		JOIN riskupdate.Risk RUR ON RUR.RiskID = R.RiskID
			AND RUR.RiskUpdateID = @RiskUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.RiskID
		FROM @tOutput O
		ORDER BY O.RiskID
	
	OPEN oCursor
	FETCH oCursor INTO @RiskID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogRiskAction @RiskID, 'read', @PersonID, NULL
		EXEC eventlog.LogRiskAction @RiskID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @RiskID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogRiskUpdateAction @RiskUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogRiskUpdateAction @RiskUpdateID, 'update', @PersonID, NULL
	
	TRUNCATE TABLE riskupdate.Risk
	DELETE FROM riskupdate.RiskUpdate

END
GO
--End procedure riskupdate.ApproveRiskUpdateRisks

--Begin procedure riskupdate.DeleteRiskUpdateRisk
EXEC Utility.DropObject 'riskupdate.DeleteRiskUpdateRisk'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to remove a risk from the risk update
-- =====================================================================
CREATE PROCEDURE riskupdate.DeleteRiskUpdateRisk

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE R
	FROM riskupdate.Risk R
	WHERE R.RiskID = @RiskID

END
GO
--End procedure riskupdate.DeleteRiskUpdateRisk

--Begin procedure riskupdate.GetRiskUpdate
EXEC Utility.DropObject 'riskupdate.GetRiskUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.08
-- Description:	A stored procedure to get data from the riskupdate.RiskUpdate table
-- ================================================================================
CREATE PROCEDURE riskupdate.GetRiskUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRiskUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM riskupdate.RiskUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RiskUpdateID INT)

		INSERT INTO riskupdate.RiskUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RiskUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.RiskUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'

		SELECT @nRiskUpdateID = O.RiskUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRiskUpdateAction @EntityID=@nRiskUpdateID, @EventCode='Create', @PersonID = @PersonID

		END
	ELSE
		SELECT @nRiskUpdateID = RU.RiskUpdateID FROM riskupdate.RiskUpdate RU
	--ENDIF
	
	SELECT
		RU.RiskUpdateID, 
		RU.WorkflowStepNumber 
	FROM riskupdate.RiskUpdate RU

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
			JOIN riskupdate.RiskUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'Risk.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nRiskUpdateID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'RiskUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @nRiskUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure riskupdate.GetRiskUpdate

--Begin procedure riskupdate.GetRiskUpdateRisk
EXEC Utility.DropObject 'riskupdate.GetRiskUpdateRisk'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to get Risk data for the risk update
-- ====================================================================
CREATE PROCEDURE riskupdate.GetRiskUpdateRisk

@RiskID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeID,
		RT.RiskTypeName
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND R.RiskID = @RiskID

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeID,
		RT.RiskTypeName
	FROM riskupdate.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND R.RiskID = @RiskID

END
GO
--End procedure riskupdate.GetRiskUpdateRisk

--Begin procedure riskupdate.PopulateRiskUpdateRisks
EXEC Utility.DropObject 'riskupdate.PopulateRiskUpdateRisks'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to add risk data to the risk update
-- ===================================================================
CREATE PROCEDURE riskupdate.PopulateRiskUpdateRisks

@RiskIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO riskupdate.Risk
		(RiskID, RiskUpdateID, RiskName, RiskDescription, RiskCategoryID, DateRaised, RaisedBy, Likelihood, Impact, Overall, MitigationActions, EscalationCriteria, Notes, RiskStatusID, RiskTypeID)
	SELECT
		R.RiskID, 
		(SELECT TOP 1 RU.RiskUpdateID FROM riskupdate.RiskUpdate RU ORDER BY RU.RiskUpdateID DESC),
		R.RiskName, 
		R.RiskDescription, 
		R.RiskCategoryID, 
		R.DateRaised, 
		R.RaisedBy, 
		R.Likelihood, 
		R.Impact, 
		R.Overall, 
		R.MitigationActions, 
		R.EscalationCriteria, 
		R.Notes, 
		R.RiskStatusID, 
		R.RiskTypeID
	FROM dbo.Risk R
		JOIN dbo.ListToTable(@RiskIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = R.RiskID
			AND NOT EXISTS
				(
				SELECT 1
				FROM riskupdate.Risk RUR
				WHERE RUR.RiskID = R.RiskID
				)

END
GO
--End procedure riskupdate.PopulateRiskUpdateRisks

--Begin procedure survey.GetSurveyBySurveyID
EXEC Utility.DropObject 'survey.GetSurveyBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.15
-- Description:	A stored procedure to get a survey from the database
--
-- Author:			Todd Pires
-- Create date: 2015.08.08
-- Description:	Added the IsAdministered flag
-- =================================================================
CREATE PROCEDURE survey.GetSurveyBySurveyID

@SurveyID INT,
@LanguageCode CHAR(2)

AS
BEGIN

	SELECT 
		S.IsActive,
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyResponse SR WHERE SR.SurveyID = S.SurveyID)
			THEN 1
			ELSE 0
		END AS IsAdministered

	FROM survey.Survey S
	WHERE S.SurveyID = @SurveyID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SL.SurveyName, 
		SL.SurveyDescription
	FROM survey.SurveyLabel SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyLanguage SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		SQ.SurveyQuestionID,
		(SELECT STUFF((SELECT ', ' + SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID ORDER BY SQL.LanguageCode FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'),1,2,'')) AS LanguageCodes,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionText,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionDescription,
	
		SQT.SurveyQuestionResponseTypeCode,
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveySurveyQuestion SSQ
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQ.SurveyID = @SurveyID
			AND 
				(
				@LanguageCode = ''
					OR EXISTS
						(
						SELECT 1
						FROM survey.SurveyQuestionLanguage SQL
						WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID
							AND SQL.LanguageCode = @LanguageCode 
						)
				)
	
END
GO
--End procedure survey.GetSurveyBySurveyID

--Begin procedure survey.GetSurveyQuestionBySurveyQuestionID
EXEC Utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.13
-- Description:	A stored procedure to get a survey question from the database
--
-- Author:			Todd Pires
-- Create date: 2015.08.08
-- Description:	Added the IsAdministered flag
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionBySurveyQuestionID

@SurveyQuestionID INT

AS
BEGIN

	SELECT 
		SQ.IsActive,
		SQ.SurveyQuestionID, 
		SQT.SurveyQuestionResponseTypeID,
		SQT.SurveyQuestionResponseTypeCode, 
		SQT.SurveyQuestionResponseTypeName,

		CASE
			WHEN EXISTS 
				(
				SELECT SSQ.SurveyQuestionID
				FROM survey.SurveySurveyQuestion SSQ
					JOIN survey.SurveyResponse SR ON SR.SurveyID = SSQ.SurveyID
						AND SSQ.SurveyQuestionID = @SurveyQuestionID

				UNION

				SELECT SSQR.SurveyQuestionID
				FROM survey.SurveySurveyQuestionResponse SSQR
				WHERE SSQr.SurveyQuestionID = @SurveyQuestionID
				)
			THEN 1
			ELSE 0
		END AS IsAdministered

	FROM survey.SurveyQuestion SQ
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SQ.SurveyQuestionID = @SurveyQuestionID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SQL.SurveyQuestionText, 
		SQL.SurveyQuestionDescription
	FROM survey.SurveyQuestionLabel SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyQuestionLanguage SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

END
GO
--End procedure survey.GetSurveyQuestionBySurveyQuestionID

--Begin procedure utility.MenuItemAddUpdate
EXEC Utility.DropObject 'utility.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to add / update a menu item
--
-- Author:			Todd Pires
-- Create Date: 2015.03.22
-- Description:	Added support for the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create Date: 2015.08.08
-- Description:	Added the ability to make a link null
-- ==========================================================================
CREATE PROCEDURE utility.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM dbo.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM dbo.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM dbo.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO dbo.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE dbo.MenuItem SET IsActive = @IsActive WHERE MenuItemID = @nNewMenuItemID
			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM dbo.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM dbo.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE dbo.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE dbo.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM dbo.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO dbo.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM dbo.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM dbo.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM dbo.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure utility.MenuItemAddUpdate

--Begin procedure workflow.CanIncrementRiskUpdateWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementRiskUpdateWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementRiskUpdateWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
			JOIN riskupdate.RiskUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RiskUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementRiskUpdateWorkflow

--Begin procedure workflow.GetRiskUpdateWorkflowData
EXEC Utility.DropObject 'workflow.GetRiskUpdateWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A procedure to return workflow data from the riskupdate.RiskUpdate table
-- =====================================================================================

CREATE PROCEDURE workflow.GetRiskUpdateWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
			JOIN riskupdate.RiskUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RiskUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'Risk.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'RiskUpdate'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Risk Updates'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Risk Updates'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Risk Updates'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Risk Updates'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'RiskUpdate'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetRiskUpdateWorkflowData

--Begin procedure workflow.GetRiskUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetRiskUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A stored procedure to people associated with the workflow steps on a risk update
-- =============================================================================================
CREATE PROCEDURE workflow.GetRiskUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'Risk.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetRiskUpdateWorkflowStepPeople