-- File Name:	Build-1.24 File 01 - AJACS.sql
-- Build Key:	Build-1.24 File 01 - AJACS - 2015.08.22 20.51.28

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		survey.FormatSurveyQuestionResponse
--
-- Procedures:
--		dbo.DeleteConceptNoteContactByConceptNoteContactID
--		dbo.GetCommunityAssetByCommunityAssetID
--		dbo.GetConceptNoteContactNotesByConceptNoteContactID
--		dbo.GetContactByContactID
--		dbo.GetDonorFeed
--		dbo.GetRiskByRiskID
--		dbo.ValidateGovernmentIDNumber
--		dbo.ValidatePassportNumber
--		dropdown.GetAssetTypeData
--		dropdown.GetCommunityAssetTypeData
--		dropdown.GetCommunityAssetUnitCostRateData
--		dropdown.GetCommunityAssetUnitTypeData
--		dropdown.GetComponentData
--		dropdown.GetCountryCallingDataByCountryCallingCodeID
--		dropdown.GetCountryData
--		dropdown.GetFindingStatusData
--		dropdown.GetFindingTypeData
--		dropdown.GetProjectStatusData
--		dropdown.GetRiskTypeData
--		dropdown.GetZoneTypeData
--		eventlog.LogCommunityAssetAction
--		eventlog.LogFindingAction
--		eventlog.LogProjectAction
--		eventlog.LogRecommendationAction
--		eventlog.LogRecommendationUpdateAction
--		eventlog.LogRiskUpdateAction
--		finding.GetFindingByFindingID
--		project.GetProjectByProjectID
--		recommendation.GetRecommendationByRecommendationID
--		recommendationupdate.ApproveRecommendationUpdateRecommendations
--		recommendationupdate.DeleteRecommendationUpdateRecommendation
--		recommendationupdate.GetRecommendationUpdate
--		recommendationupdate.GetRecommendationUpdateRecommendation
--		recommendationupdate.PopulateRecommendationUpdateRecommendations
--		reporting.GetConceptNoteRiskByConceptNoteID
--		reporting.GetContact
--		reporting.GetRiskReportByPersonID
--		reporting.GetSurveyQuestionAndAnswers
--		riskupdate.ApproveRiskUpdateRisks
--		riskupdate.DeleteRiskUpdateRisk
--		riskupdate.GetRiskUpdate
--		riskupdate.GetRiskUpdateRisk
--		riskupdate.PopulateRiskUpdateRisks
--		survey.GetSurveyBySurveyID
--		survey.GetSurveyQuestionBySurveyQuestionID
--		utility.MenuItemAddUpdate
--		workflow.CanIncrementRiskUpdateWorkflow
--		workflow.GetRiskUpdateWorkflowData
--		workflow.GetRiskUpdateWorkflowStepPeople
--
-- Schemas:
--		finding
--		project
--		recommendation
--		recommendationupdate
--		riskupdate
--
-- Tables:
--		dbo.CommunityAsset
--		dbo.CommunityAssetCommunity
--		dbo.CommunityAssetProvince
--		dbo.CommunityAssetRisk
--		dbo.CommunityAssetUnit
--		dbo.Number
--		dropdown.AssetType
--		dropdown.CommunityAssetType
--		dropdown.CommunityAssetUnitCostRate
--		dropdown.CommunityAssetUnitType
--		dropdown.Component
--		dropdown.FindingStatus
--		dropdown.FindingType
--		dropdown.ProjectStatus
--		dropdown.RiskType
--		dropdown.ZoneType
--		finding.Finding
--		finding.FindingCommunity
--		finding.FindingComponent
--		finding.FindingIndicator
--		finding.FindingProvince
--		finding.FindingRecommendation
--		finding.FindingRisk
--		project.Project
--		project.ProjectCommunity
--		project.ProjectFinding
--		project.ProjectIndicator
--		project.ProjectProvince
--		project.ProjectRecommendation
--		recommendation.Recommendation
--		recommendation.RecommendationCommunity
--		recommendation.RecommendationComponent
--		recommendation.RecommendationIndicator
--		recommendation.RecommendationProvince
--		recommendation.RecommendationRisk
--		recommendationupdate.Recommendation
--		recommendationupdate.RecommendationUpdate
--		riskupdate.Risk
--		riskupdate.RiskUpdate
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin schema finding
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'finding')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA finding'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema finding

--Begin schema project
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'project')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA project'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema project

--Begin schema recommendation
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'recommendation')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA recommendation'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema recommendation

--Begin schema recommendationupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'recommendationupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA recommendationupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema recommendationupdate

--Begin schema riskupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'riskupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA riskupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema riskupdate

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACSUtility
GO

--Begin table dbo.Number
IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = 'dbo.Number')
	BEGIN

	CREATE TABLE dbo.Number
		(
		Number INT
		)
	
	;
	WITH 
		Tx1(N) AS (SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL  SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1)
		, Tx2(N) AS (SELECT 1 FROM Tx1 CROSS JOIN Tx1 AS b)
		, Tx4(N) AS (SELECT 1 FROM Tx2 CROSS JOIN Tx2 AS b)
		, Tx8(N) AS (SELECT 1 FROM Tx4 CROSS JOIN Tx4 AS b)

	INSERT INTO dbo.Number
		(Number)
	SELECT ROW_NUMBER() OVER (ORDER BY Tx8.N) 
	FROM Tx8
	ORDER BY Tx8.N

	END
--ENDIF
GO	
--End table dbo.Number

USE AJACS
GO
--Begin table dbo.CommunityAsset
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAsset'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAsset
	(
	CommunityAssetID INT IDENTITY(1,1),
	CommunityAssetName NVARCHAR(250),
	CommunityAssetDescription NVARCHAR(MAX),
	CommunityAssetTypeID INT,
	AssetTypeID INT,
	ZoneTypeID INT,
	Location GEOGRAPHY,
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ZoneTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetID'
GO
--Begin table dbo.CommunityAsset

--Begin table dbo.CommunityAssetCommunity
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetCommunity
	(
	CommunityAssetCommunityID INT IDENTITY(1,1),
	CommunityAssetID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetCommunityID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetCommunity', @TableName, 'CommunityAssetID,CommunityID'
GO
--Begin table dbo.CommunityAsset

--Begin table dbo.CommunityAssetProvince
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetProvince'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetProvince
	(
	CommunityAssetProvinceID INT IDENTITY(1,1),
	CommunityAssetID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetProvinceID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetProvince', @TableName, 'CommunityAssetID,ProvinceID'
GO
--Begin table dbo.CommunityAssetProvince

--Begin table dbo.CommunityAssetRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetRisk
	(
	CommunityAssetRiskID INT IDENTITY(1,1),
	CommunityAssetID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetRisk', @TableName, 'CommunityAssetID,RiskID'
GO
--Begin table dbo.CommunityAssetRisk

--Begin table dbo.CommunityAssetUnit
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetUnit'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetUnit
	(
	CommunityAssetUnitID INT IDENTITY(1,1),
	CommunityAssetID INT,
	CommunityAssetUnitName NVARCHAR(250),
	CommunityAssetUnitTypeID INT,
	CommunityAssetUnitCostRateID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitCostRateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetUnitID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetUnit', @TableName, 'CommunityAssetID,CommunityAssetUnitName'
GO
--Begin table dbo.CommunityAssetUnit

--Begin table dbo.ConceptNoteContact
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContact'

EXEC utility.AddColumn @TableName, 'Notes', 'NVARCHAR(MAX)'
GO
--End table dbo.ConceptNoteContact

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ArabicMotherName', 'NVARCHAR(200)'
EXEC utility.AddColumn @TableName, 'CellPhoneNumberCountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'FaxNumberCountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'IsValid', 'BIT'
EXEC utility.AddColumn @TableName, 'Notes', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PhoneNumberCountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'PlaceOfBirthCountryID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CellPhoneNumberCountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FaxNumberCountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsValid', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'PhoneNumberCountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlaceOfBirthCountryID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.Risk / riskupdate.Risk
DECLARE @TableName VARCHAR(250) = 'dbo.Risk'

EXEC utility.AddColumn @TableName, 'RiskTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'RiskTypeID', 'INT', 0
GO

DECLARE @TableName VARCHAR(250) = 'riskupdate.Risk'

EXEC utility.AddColumn @TableName, 'RiskTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'RiskTypeID', 'INT', 0
GO
--End table dbo.Risk / riskupdate.Risk

--Begin table dropdown.AssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetType
	(
	AssetTypeID INT IDENTITY(0,1) NOT NULL,
	AssetTypeName VARCHAR(50),
	Icon VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetTypeID'
EXEC utility.SetIndexNonClustered 'IX_AssetType', @TableName, 'DisplayOrder,AssetTypeName', 'AssetTypeID'
GO

SET IDENTITY_INSERT dropdown.AssetType ON
GO

INSERT INTO dropdown.AssetType (AssetTypeID, DisplayOrder) VALUES (0, 1)

SET IDENTITY_INSERT dropdown.AssetType OFF
GO

INSERT INTO dropdown.AssetType 
	(AssetTypeName,Icon,DisplayOrder)
VALUES
	('Police Station', 'asset-police-station.png', 1),
	('Court', 'asset-court.png', 2),
	('Prison', 'asset-prison.png', 3),
	('Community Building', 'asset-community-building.png', 4),
	('Document Centre', 'asset-document-centre.png', 5),
	('Other', 'asset-other.png', 6)
GO
--End table dropdown.AssetType

--Begin table dropdown.CommunityAssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityAssetType
	(
	CommunityAssetTypeID INT IDENTITY(0,1) NOT NULL,
	CommunityAssetTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetTypeID'
EXEC utility.SetIndexNonClustered 'IX_CommunityAssetType', @TableName, 'DisplayOrder,CommunityAssetTypeName', 'CommunityAssetTypeID'
GO

SET IDENTITY_INSERT dropdown.CommunityAssetType ON
GO

INSERT INTO dropdown.CommunityAssetType (CommunityAssetTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityAssetType OFF
GO

INSERT INTO dropdown.CommunityAssetType 
	(CommunityAssetTypeName,DisplayOrder)
VALUES
	('Asset', 1),
	('Zone', 2)
GO
--End table dropdown.CommunityAssetType

--Begin table dropdown.CommunityAssetUnitCostRate
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetUnitCostRate'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityAssetUnitCostRate
	(
	CommunityAssetUnitCostRateID INT IDENTITY(0,1) NOT NULL,
	CommunityAssetUnitCostRate INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitCostRate', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetUnitCostRateID'
EXEC utility.SetIndexNonClustered 'IX_CommunityAssetUnitCostRate', @TableName, 'DisplayOrder,CommunityAssetUnitCostRate', 'CommunityAssetUnitCostRateID'
GO

SET IDENTITY_INSERT dropdown.CommunityAssetUnitCostRate ON
GO

INSERT INTO dropdown.CommunityAssetUnitCostRate (CommunityAssetUnitCostRateID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityAssetUnitCostRate OFF
GO

INSERT INTO dropdown.CommunityAssetUnitCostRate 
	(CommunityAssetUnitCostRate, DisplayOrder)
VALUES
	(0, 1),
	(500, 2),
	(3000, 3)
GO
--End table dropdown.CommunityAssetUnitCostRate

--Begin table dropdown.CommunityAssetUnitType
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetUnitType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityAssetUnitType
	(
	CommunityAssetUnitTypeID INT IDENTITY(0,1) NOT NULL,
	CommunityAssetUnitTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetUnitTypeID'
EXEC utility.SetIndexNonClustered 'IX_CommunityAssetUnitType', @TableName, 'DisplayOrder,CommunityAssetUnitTypeName', 'CommunityAssetUnitTypeID'
GO

SET IDENTITY_INSERT dropdown.CommunityAssetUnitType ON
GO

INSERT INTO dropdown.CommunityAssetUnitType (CommunityAssetUnitTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityAssetUnitType OFF
GO

INSERT INTO dropdown.CommunityAssetUnitType 
	(CommunityAssetUnitTypeName, DisplayOrder)
VALUES
	('Police', 1),
	('Community',2),
	('Justice', 3)
GO
--End table dropdown.CommunityAssetUnitType

--Begin table dropdown.Component
DECLARE @TableName VARCHAR(250) = 'dropdown.Component'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Component
	(
	ComponentID INT IDENTITY(0,1) NOT NULL,
	ComponentCode VARCHAR(50),
	ComponentAbbreviation VARCHAR(15),
	ComponentName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ComponentID'
EXEC utility.SetIndexNonClustered 'IX_Component', @TableName, 'DisplayOrder,ComponentName', 'ComponentID'
GO

SET IDENTITY_INSERT dropdown.Component ON
GO

INSERT INTO dropdown.Component (ComponentID) VALUES (0)

SET IDENTITY_INSERT dropdown.Component OFF
GO

INSERT INTO dropdown.Component 
	(ComponentName,ComponentAbbreviation,DisplayOrder)
VALUES
	('Community Engagement', 'CE', 1),
	('Police', 'POL', 2),
	('Justice', 'JUS', 3),
	('Integrated Legitimate Structures', 'ILS', 4)
GO	
--End table dropdown.Component

--Begin table dropdown.FindingStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.FindingStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FindingStatus
	(
	FindingStatusID INT IDENTITY(0,1) NOT NULL,
	FindingStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FindingStatusID'
EXEC utility.SetIndexNonClustered 'IX_FindingStatus', @TableName, 'DisplayOrder,FindingStatusName', 'FindingStatusID'
GO

SET IDENTITY_INSERT dropdown.FindingStatus ON
GO

INSERT INTO dropdown.FindingStatus (FindingStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.FindingStatus OFF
GO

INSERT INTO dropdown.FindingStatus 
	(FindingStatusName,DisplayOrder)
VALUES
	('Pending', 1),
	('Current', 2),
	('Historic', 3)
GO	
--End table dropdown.FindingStatus

--Begin table dropdown.FindingType
DECLARE @TableName VARCHAR(250) = 'dropdown.FindingType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FindingType
	(
	FindingTypeID INT IDENTITY(0,1) NOT NULL,
	FindingTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FindingTypeID'
EXEC utility.SetIndexNonClustered 'IX_FindingType', @TableName, 'DisplayOrder,FindingTypeName', 'FindingTypeID'
GO

SET IDENTITY_INSERT dropdown.FindingType ON
GO

INSERT INTO dropdown.FindingType (FindingTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.FindingType OFF
GO

INSERT INTO dropdown.FindingType 
	(FindingTypeName,DisplayOrder)
VALUES
	('RAP', 1),
	('Community Engagement', 2),
	('Police', 3),
	('Integrated Legitimate Structures', 4),
	('Justice', 5),
	('FIF', 6),
	('Future Consideration', 7)
GO	
--End table dropdown.FindingType

--Begin table dropdown.ProjectStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectStatus
	(
	ProjectStatusID INT IDENTITY(0,1) NOT NULL,
	ProjectStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectStatusID'
EXEC utility.SetIndexNonClustered 'IX_ProjectStatus', @TableName, 'DisplayOrder,ProjectStatusName', 'ProjectStatusID'
GO

SET IDENTITY_INSERT dropdown.ProjectStatus ON
GO

INSERT INTO dropdown.ProjectStatus (ProjectStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.ProjectStatus OFF
GO

INSERT INTO dropdown.ProjectStatus 
	(ProjectStatusName,DisplayOrder)
VALUES
	('Planning', 1),
	('Implementation', 2),
	('Close Down', 3),
	('Closed', 4),
	('Cancelled', 5), 
	('Suspended', 6)
GO	
--End table dropdown.ProjectStatus

--Begin table dropdown.RiskType
DECLARE @TableName VARCHAR(250) = 'dropdown.RiskType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RiskType
	(
	RiskTypeID INT IDENTITY(0,1) NOT NULL,
	RiskTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskTypeID'
EXEC utility.SetIndexNonClustered 'IX_RiskType', @TableName, 'DisplayOrder,RiskTypeName', 'RiskTypeID'
GO

SET IDENTITY_INSERT dropdown.RiskType ON
GO

INSERT INTO dropdown.RiskType (RiskTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.RiskType OFF
GO

INSERT INTO dropdown.RiskType 
	(RiskTypeName)
VALUES
	('Activity Specific'),
	('Community Engagement Specific'),
	('Component For Specific'),
	('Finding/Recommendation Specific'),
	('Justice Specific'),
	('Police Specific'),
	('Programme'),
	('Project Specific'),
	('Recommendation Specific')
GO	
--End table dropdown.RiskType

--Begin table dropdown.ZoneType
DECLARE @TableName VARCHAR(250) = 'dropdown.ZoneType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ZoneType
	(
	ZoneTypeID INT IDENTITY(0,1) NOT NULL,
	ZoneTypeName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ZoneTypeID'
EXEC utility.SetIndexNonClustered 'IX_ZoneType', @TableName, 'DisplayOrder,ZoneTypeName', 'ZoneTypeID'
GO

SET IDENTITY_INSERT dropdown.ZoneType ON
GO

INSERT INTO dropdown.ZoneType (ZoneTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ZoneType OFF
GO

INSERT INTO dropdown.ZoneType 
	(ZoneTypeName, HexColor, DisplayOrder)
VALUES
	('FSP Area of Control', '#1b9e77', 1),
	('Islamic Extremist Group Area of Control', '#d95f02', 2),
	('Regime Area of Control', '#7570b3', 3),
	('Crime Hotspot', '#e7298a', 4)
GO
--End table dropdown.ZoneType

--Begin table finding.Finding
DECLARE @TableName VARCHAR(250) = 'finding.Finding'

EXEC utility.DropObject @TableName

CREATE TABLE finding.Finding
	(
	FindingID INT IDENTITY(1,1) NOT NULL,
	FindingName VARCHAR(250),
	FindingDescription VARCHAR(MAX),
	FindingTypeID INT,
	FindingStatusID INT,
	IsResearchElement BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsResearchElement', 'BIT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'FindingID'
GO
--End table finding.Finding

--Begin table finding.FindingCommunity
DECLARE @TableName VARCHAR(250) = 'finding.FindingCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingCommunity
	(
	FindingCommunityID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingCommunityID'
EXEC utility.SetIndexClustered 'IX_FindingCommunity', @TableName, 'FindingID,CommunityID'
GO
--End table finding.FindingCommunity

--Begin table finding.FindingComponent
DECLARE @TableName VARCHAR(250) = 'finding.FindingComponent'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingComponent
	(
	FindingComponentID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	ComponentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingComponentID'
EXEC utility.SetIndexClustered 'IX_FindingComponent', @TableName, 'FindingID,ComponentID'
GO
--End table finding.FindingComponent

--Begin table finding.FindingIndicator
DECLARE @TableName VARCHAR(250) = 'finding.FindingIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingIndicator
	(
	FindingIndicatorID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingIndicatorID'
EXEC utility.SetIndexClustered 'IX_FindingIndicator', @TableName, 'FindingID,IndicatorID'
GO
--End table finding.FindingIndicator

--Begin table finding.FindingProvince
DECLARE @TableName VARCHAR(250) = 'finding.FindingProvince'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingProvince
	(
	FindingProvinceID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingProvinceID'
EXEC utility.SetIndexClustered 'IX_FindingProvince', @TableName, 'FindingID,ProvinceID'
GO
--End table finding.FindingProvince

--Begin table finding.FindingRecommendation
DECLARE @TableName VARCHAR(250) = 'finding.FindingRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingRecommendation
	(
	FindingRecommendationID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	RecommendationID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingRecommendationID'
EXEC utility.SetIndexClustered 'IX_FindingRecommendation', @TableName, 'FindingID,RecommendationID'
GO
--End table finding.FindingRecommendation

--Begin table finding.FindingRisk
DECLARE @TableName VARCHAR(250) = 'finding.FindingRisk'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingRisk
	(
	FindingRiskID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingRiskID'
EXEC utility.SetIndexClustered 'IX_FindingRisk', @TableName, 'FindingID,RiskID'
GO
--End table finding.FindingRisk

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table logicalframework.Milestone

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table logicalframework.Objective

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.DropObject @TableName

CREATE TABLE project.Project
	(
	ProjectID INT IDENTITY(1,1) NOT NULL,
	ProjectName VARCHAR(250),
	ProjectDescription VARCHAR(MAX),
	ProjectSummary VARCHAR(MAX),
	ProjectPartner VARCHAR(250),
	ProjectValue NUMERIC(18,2),
	ConceptNoteID INT,
	ProjectStatusID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectValue', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID'
GO
--End table project.Project

--Begin table project.ProjectCommunity
DECLARE @TableName VARCHAR(250) = 'project.ProjectCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectCommunity
	(
	ProjectCommunityID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	CommunityID INT,
	ProjectCommunityValue NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectCommunityValue', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectCommunityID'
EXEC utility.SetIndexClustered 'IX_ProjectCommunity', @TableName, 'ProjectID,CommunityID'
GO
--End table project.ProjectCommunity

--Begin table project.ProjectFinding
DECLARE @TableName VARCHAR(250) = 'project.ProjectFinding'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectFinding
	(
	ProjectFindingID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	FindingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectFindingID'
EXEC utility.SetIndexClustered 'IX_ProjectFinding', @TableName, 'ProjectID,FindingID'
GO
--End table project.ProjectFinding

--Begin table project.ProjectIndicator
DECLARE @TableName VARCHAR(250) = 'project.ProjectIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectIndicator
	(
	ProjectIndicatorID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProjectIndicator', @TableName, 'ProjectID,IndicatorID'
GO
--End table project.ProjectIndicator

--Begin table project.ProjectProvince
DECLARE @TableName VARCHAR(250) = 'project.ProjectProvince'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectProvince
	(
	ProjectProvinceID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	ProvinceID INT,
	ProjectProvinceValue NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectProvinceValue', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectProvinceID'
EXEC utility.SetIndexClustered 'IX_ProjectProvince', @TableName, 'ProjectID,ProvinceID'
GO
--End table project.ProjectProvince

--Begin table project.ProjectRecommendation
DECLARE @TableName VARCHAR(250) = 'project.ProjectRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectRecommendation
	(
	ProjectRecommendationID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	RecommendationID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectRecommendationID'
EXEC utility.SetIndexClustered 'IX_ProjectRecommendation', @TableName, 'ProjectID,RecommendationID'
GO
--End table project.ProjectRecommendation

--Begin table recommendation.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendation.Recommendation'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.Recommendation
	(
	RecommendationID INT IDENTITY(1,1) NOT NULL,
	RecommendationName VARCHAR(250),
	RecommendationDescription VARCHAR(MAX),
	IsResearchElement BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsResearchElement', 'BIT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationID'
GO
--End table recommendation.Recommendation

--Begin table recommendation.RecommendationCommunity
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationCommunity
	(
	RecommendationCommunityID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationCommunityID'
EXEC utility.SetIndexClustered 'IX_RecommendationCommunity', @TableName, 'RecommendationID,CommunityID'
GO
--End table recommendation.RecommendationCommunity

--Begin table recommendation.RecommendationComponent
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationComponent'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationComponent
	(
	RecommendationComponentID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ComponentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationComponentID'
EXEC utility.SetIndexClustered 'IX_RecommendationComponent', @TableName, 'RecommendationID,ComponentID'
GO
--End table recommendation.RecommendationComponent

--Begin table recommendation.RecommendationIndicator
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationIndicator
	(
	RecommendationIndicatorID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationIndicatorID'
EXEC utility.SetIndexClustered 'IX_RecommendationIndicator', @TableName, 'RecommendationID,IndicatorID'
GO
--End table recommendation.RecommendationIndicator

--Begin table recommendation.RecommendationProvince
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationProvince'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationProvince
	(
	RecommendationProvinceID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationProvinceID'
EXEC utility.SetIndexClustered 'IX_RecommendationProvince', @TableName, 'RecommendationID,ProvinceID'
GO
--End table recommendation.RecommendationProvince

--Begin table recommendation.RecommendationRisk
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationRisk'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationRisk
	(
	RecommendationRiskID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationRiskID'
EXEC utility.SetIndexClustered 'IX_RecommendationRisk', @TableName, 'RecommendationID,RiskID'
GO
--End table recommendation.RecommendationRisk

--Begin table recommendationupdate.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.Recommendation'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.Recommendation
	(
	RecommendationID INT,
	RecommendationUpdateID INT,
	RecommendationName VARCHAR(250),
	RecommendationDescription VARCHAR(MAX),
	IsResearchElement BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsResearchElement', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationID'
GO
--End table recommendationupdate.Recommendation

--Begin table recommendationupdate.RecommendationUpdate
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationUpdate
	(
	RecommendationUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationUpdateID'
GO
--End table recommendationupdate.RecommendationUpdate

--Begin table riskupdate.Risk
DECLARE @TableName VARCHAR(250) = 'riskupdate.Risk'

EXEC utility.DropObject @TableName

CREATE TABLE riskupdate.Risk
	(
	RiskID INT,
	RiskUpdateID INT,
	RiskName VARCHAR(250),
	RiskDescription VARCHAR(MAX),
	RiskCategoryID INT,
	DateRaised DATE,
	RaisedBy VARCHAR(100),
	Likelihood INT,
	Impact INT,
	Overall INT,
	MitigationActions VARCHAR(MAX),
	EscalationCriteria VARCHAR(MAX),
	Notes VARCHAR(MAX),
	RiskStatusID INT,
	RiskTypeID INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'Impact', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Likelihood', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Overall', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskID'
GO
--End table riskupdate.Risk

--Begin table riskupdate.RiskUpdate
DECLARE @TableName VARCHAR(250) = 'riskupdate.RiskUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE riskupdate.RiskUpdate
	(
	RiskUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskUpdateID'
GO
--End table riskupdate.RiskUpdate
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function survey.FormatSurveyQuestionResponse
EXEC Utility.DropObject 'dbo.FormatSurveyQuestionResponse'
EXEC Utility.DropObject 'survey.FormatSurveyQuestionResponse'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			John Lyons
-- Create date: 2015.08.10
-- Description:	A function to get survey responses
-- ===============================================
CREATE FUNCTION survey.FormatSurveyQuestionResponse
(
@SurveyQuestionLanguageCode VARCHAR(2),
@SurveyQuestionID INT,
@SurveyResponseID  INT
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @ChoiceResult NVARCHAR(MAX) = ''

	SELECT 
		@ChoiceResult += 
		(
		SELECT 
	
			CASE SQT.SurveyQuestionResponseTypeCode
				WHEN 'CommunityPicker' 
				THEN (SELECT CommunityName FROM Community WHERE CommunityID = CAST(SSQR.SurveyQuestionResponse AS INT))
				WHEN 'ProvincePicker' 
				THEN (SELECT ProvinceName FROM Province WHERE ProvinceID = CAST(SSQR.SurveyQuestionResponse AS INT))
				WHEN 'Select' 
				THEN (SELECT SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice WHERE SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND LanguageCode = @SurveyQuestionLanguageCode)
				WHEN 'SelectMultiple' 
				THEN (SELECT SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice WHERE SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND LanguageCode = @SurveyQuestionLanguageCode)
				WHEN 'YesNo' 
				THEN (SELECT SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice WHERE SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND LanguageCode = @SurveyQuestionLanguageCode)
				WHEN 'DatePicker' 
				THEN dbo.FormatDate(SSQR.SurveyQuestionResponse)
				ELSE SSQR.SurveyQuestionResponse 
			END AS Result) + ', '

	FROM survey.SurveySurveyQuestionResponse SSQR
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQR.SurveyQuestionID 
			AND SSQR.SurveyQuestionID = @SurveyQuestionID 
			AND SSQR.SurveyResponseID = @SurveyResponseID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID

	IF LEN(RTRIM(@ChoiceResult)) > 0
		SET @ChoiceResult = RTRIM(SUBSTRING(@ChoiceResult, 0, LEN(@ChoiceResult)))
	--ENDIF

	RETURN RTRIM(@ChoiceResult)

END
GO
--End function survey.FormatSurveyQuestionResponse
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO
--Begin procedure dbo.DeleteConceptNoteContactByConceptNoteContactID
EXEC Utility.DropObject 'dbo.DeleteConceptNoteContactByConceptNoteContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	A stored procedure to delete data from the dbo.ConceptNoteContact table
-- ====================================================================================
CREATE PROCEDURE dbo.DeleteConceptNoteContactByConceptNoteContactID

@ConceptNoteContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE CNC
	FROM dbo.ConceptNoteContact CNC
	WHERE CNC.ConceptNoteContactID = @ConceptNoteContactID
	
END
GO
--End procedure dbo.DeleteConceptNoteContactByConceptNoteContactID

--Begin procedure dbo.GetConceptNoteContactNotesByConceptNoteContactID
EXEC Utility.DropObject 'dbo.GetConceptNoteContactNotesByConceptNoteContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	A stored procedure to get data from the dbo.ConceptNoteContact table
-- =================================================================================
CREATE PROCEDURE dbo.GetConceptNoteContactNotesByConceptNoteContactID

@ConceptNoteContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT CNC.Notes
	FROM dbo.ConceptNoteContact CNC
	WHERE CNC.ConceptNoteContactID = @ConceptNoteContactID
	
END
GO
--End procedure dbo.GetConceptNoteContactNotesByConceptNoteContactID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.ArabicMotherName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PlaceOfBirthCountryID,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetCommunityAssetByCommunityAssetID
EXEC Utility.DropObject 'dbo.GetCommunityAssetByCommunityAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to get data from the dbo.CommunityAsset table
-- =============================================================================
CREATE PROCEDURE dbo.GetCommunityAssetByCommunityAssetID

@CommunityAssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CA.CommunityAssetDescription,
		CA.Location.STAsText() AS Location,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CA.CommunityAssetID = @CommunityAssetID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.CommunityAssetCommunity CAC ON CAC.CommunityID = C.CommunityID
			AND CAC.CommunityAssetID = @CommunityAssetID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.CommunityAssetProvince CAP ON CAP.ProvinceID = P.ProvinceID
			AND CAP.CommunityAssetID = @CommunityAssetID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.CommunityAssetRisk CAR ON CAR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CAR.CommunityAssetID = @CommunityAssetID
	
	SELECT
		CAU.CommunityAssetUnitID,
		CAU.CommunityAssetUnitName,
		CAUT.CommunityAssetUnitTypeID,
		CAUT.CommunityAssetUnitTypeName,
		CAUC.CommunityAssetUnitCostRateID,
		CAUC.CommunityAssetUnitCostRate
	FROM dbo.CommunityAssetUnit CAU
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
		JOIN dropdown.CommunityAssetUnitCostRate CAUC ON CAUC.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
			AND CAU.CommunityAssetID = @CommunityAssetID

END
GO
--End procedure dbo.GetCommunityAssetByCommunityAssetID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.27
-- Description:	Bug fixes
--
-- Author:			Todd Pires
-- Create Date: 2015.0.27
-- Description:	Added Recommendations
-- ==============================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN 'fa fa-fw fa-lightbulb-o'
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN 'fa fa-fw fa-question-circle'
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN 'fa fa-fw fa-calendar'
		END AS Icon,

		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN OAR.RecommendationName
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		CASE
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN OAWR.PhysicalFileName
			ELSE NULL
		END AS PhysicalFileName,

		D.EntityID,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('Recommendation','RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'Recommendation' AND permissionable.HasPermission('Recommendation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				R.RecommendationName,
				R.IsActive
			FROM recommendation.Recommendation R
			WHERE R.RecommendationID = D.EntityID
					AND D.EntityTypeCode = 'Recommendation'
			) OAR
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle,
				SR.WorkflowStepNumber
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
			AND
				(
				D.EntityTypeCode <> 'Recommendation'
					OR OAR.IsActive = 1
				)
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
			AND
				(
				D.EntityTypeCode <> 'SpotReport'
					OR OASR.WorkflowStepNumber > (SELECT W.WorkflowStepCount FROM workflow.Workflow W WHERE W.EntityTypeCode = 'SpotReport')
				)
			AND
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)

	ORDER BY D.UpdateDate DESC, D.EntityTypeCode, D.EntityID
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dbo.GetRiskByRiskID
EXEC Utility.DropObject 'dbo.GetRiskByRiskID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
--
-- Author:			Kevin Ross
-- Create date: 2015.08.09
-- Description:	Add support for the RiskType
-- ===================================================================
CREATE PROCEDURE dbo.GetRiskByRiskID

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeID,
		RT.RiskTypeName
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND R.RiskID = @RiskID

END
GO
--End procedure dbo.GetRiskByRiskID

--Begin procedure dropdown.GetComponentData
EXEC Utility.DropObject 'dropdown.GetComponentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.05
-- Description:	A stored procedure to return data from the dropdown.Component table
-- ================================================================================
CREATE PROCEDURE dropdown.GetComponentData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ComponentID,
		T.ComponentAbbreviation,
		T.ComponentCode,
		T.ComponentName
	FROM dropdown.Component T
	WHERE (T.ComponentID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ComponentName, T.ComponentID

END
GO
--End procedure dropdown.GetComponentData

--Begin procedure dbo.ValidateGovernmentIDNumber
EXEC Utility.DropObject 'dbo.ValidateGovernmentIDNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the dbo.Contact table
-- ======================================================================
CREATE PROCEDURE dbo.ValidateGovernmentIDNumber

@GovernmentIDNumber NVARCHAR(40),
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.ContactID) AS ItemCount
	FROM dbo.Contact C
	WHERE C.GovernmentIDNumber = @GovernmentIDNumber
		AND C.ContactID <> @ContactID

END
GO
--End procedure dbo.ValidateGovernmentIDNumber

--Begin procedure dbo.ValidatePassportNumber
EXEC Utility.DropObject 'dbo.ValidatePassportNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the dbo.Contact table
-- ======================================================================
CREATE PROCEDURE dbo.ValidatePassportNumber

@PassportNumber NVARCHAR(40),
@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(C.ContactID) AS ItemCount
	FROM dbo.Contact C
	WHERE C.PassportNumber = @PassportNumber
		AND C.ContactID <> @ContactID

END
GO
--End procedure dbo.ValidatePassportNumber

--Begin procedure dropdown.GetAssetTypeData
EXEC Utility.DropObject 'dropdown.GetAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.AssetType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetAssetTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AssetTypeID,
		T.AssetTypeName,
		T.Icon
	FROM dropdown.AssetType T
	WHERE (T.AssetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AssetTypeName, T.AssetTypeID

END
GO
--End procedure dropdown.GetAssetTypeData

--Begin procedure dropdown.GetCommunityAssetTypeData
EXEC Utility.DropObject 'dropdown.GetCommunityAssetTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.CommunityAssetType table
-- =========================================================================================
CREATE PROCEDURE dropdown.GetCommunityAssetTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityAssetTypeID,
		T.CommunityAssetTypeName
	FROM dropdown.CommunityAssetType T
	WHERE (T.CommunityAssetTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityAssetTypeName, T.CommunityAssetTypeID

END
GO
--End procedure dropdown.GetCommunityAssetTypeData

--Begin procedure dropdown.GetCommunityAssetUnitCostRateData
EXEC Utility.DropObject 'dropdown.GetCommunityAssetUnitCostRateData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.CommunityAssetUnitCostRate table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetCommunityAssetUnitCostRateData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityAssetUnitCostRateID,
		T.CommunityAssetUnitCostRate
	FROM dropdown.CommunityAssetUnitCostRate T
	WHERE (T.CommunityAssetUnitCostRateID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityAssetUnitCostRate, T.CommunityAssetUnitCostRateID

END
GO
--End procedure dropdown.GetCommunityAssetUnitCostRateData

--Begin procedure dropdown.GetCommunityAssetUnitTypeData
EXEC Utility.DropObject 'dropdown.GetCommunityAssetUnitTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.CommunityAssetUnitType table
-- =============================================================================================
CREATE PROCEDURE dropdown.GetCommunityAssetUnitTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityAssetUnitTypeID,
		T.CommunityAssetUnitTypeName
	FROM dropdown.CommunityAssetUnitType T
	WHERE (T.CommunityAssetUnitTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityAssetUnitTypeName, T.CommunityAssetUnitTypeID

END
GO
--End procedure dropdown.GetCommunityAssetUnitTypeData

--Begin procedure dropdown.GetCountryCallingDataByCountryCallingCodeID
EXEC Utility.DropObject 'dropdown.GetCountryCallingDataByCountryCallingCodeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.15
-- Description:	A stored procedure to return data from the dropdown.Country and dropdown.CountryCallingCode tables
-- ===============================================================================================================
CREATE PROCEDURE dropdown.GetCountryCallingDataByCountryCallingCodeID

@CountryCallingCodeID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.ISOCountryCode2, 
		CCC.CountryCallingCode
	FROM dropdown.CountryCallingCode CCC
		JOIN dropdown.Country C ON C.CountryID = CCC.CountryID
			AND CCC.CountryCallingCodeID = @CountryCallingCodeID

END
GO
--End procedure dropdown.GetCountryCallingDataByCountryCallingCodeID

--Begin procedure dropdown.GetCountryData
EXEC Utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to return data from the dropdown.Country table
--
-- Author:			Todd Pires
-- Create date:	2015.08.15
-- Description:	Added CountryCallingCode support
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		C.CountryID, 
		C.CountryName,
		C.ISOCountryCode2, 
		C.ISOCountryCode3,
		CCC.CountryCallingCode, 
		CCC.CountryCallingCodeID
	FROM dropdown.Country C
		JOIN Dropdown.CountryCallingCode CCC ON CCC.CountryID = C.CountryID 
			AND (C.CountryID > 0 OR @IncludeZero = 1)
			AND C.IsActive = 1
	ORDER BY C.DisplayOrder, C.CountryName, C.CountryID, CCC.CountryCallingCode

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetFindingStatusData
EXEC Utility.DropObject 'dropdown.GetFindingStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.05
-- Description:	A stored procedure to return data from the dropdown.FindingStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetFindingStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingStatusID,
		T.FindingStatusName
	FROM dropdown.FindingStatus T
	WHERE (T.FindingStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingStatusName, T.FindingStatusID

END
GO
--End procedure dropdown.GetFindingStatusData

--Begin procedure dropdown.GetFindingTypeData
EXEC Utility.DropObject 'dropdown.GetFindingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.05
-- Description:	A stored procedure to return data from the dropdown.FindingType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetFindingTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FindingTypeID,
		T.FindingTypeName
	FROM dropdown.FindingType T
	WHERE (T.FindingTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FindingTypeName, T.FindingTypeID

END
GO
--End procedure dropdown.GetFindingTypeData

--Begin procedure dropdown.GetProjectStatusData
EXEC Utility.DropObject 'dropdown.GetProjectStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.07
-- Description:	A stored procedure to return data from the dropdown.ProjectStatus table
-- ====================================================================================
CREATE PROCEDURE dropdown.GetProjectStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProjectStatusID,
		T.ProjectStatusName
	FROM dropdown.ProjectStatus T
	WHERE (T.ProjectStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProjectStatusName, T.ProjectStatusID

END
GO
--End procedure dropdown.GetProjectStatusData

--Begin procedure dropdown.GetRiskTypeData
EXEC Utility.DropObject 'dropdown.GetRiskTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to return data from the dropdown.RiskType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetRiskTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RiskTypeID,
		T.RiskTypeName
	FROM dropdown.RiskType T
	WHERE (T.RiskTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RiskTypeName, T.RiskTypeID

END
GO
--End procedure dropdown.GetRiskTypeData

--Begin procedure dropdown.GetZoneTypeData
EXEC Utility.DropObject 'dropdown.GetZoneTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.08.20
-- Description:	A stored procedure to return data from the dropdown.ZoneType table
-- ================================================================================
CREATE PROCEDURE dropdown.GetZoneTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ZoneTypeID,
		T.ZoneTypeName,
		T.HexColor
	FROM dropdown.ZoneType T
	WHERE (T.ZoneTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ZoneTypeName, T.ZoneTypeID

END
GO
--End procedure dropdown.GetZoneTypeData

--Begin procedure eventlog.LogCommunityAssetAction
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cCommunityAssetCommunities NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetCommunities = COALESCE(@cCommunityAssetCommunities, '') + D.CommunityAssetCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetCommunity'), ELEMENTS) AS CommunityAssetCommunity
			FROM dbo.CommunityAssetCommunity T 
			WHERE T.CommunityAssetID = @EntityID
			) D
				
		DECLARE @cCommunityAssetProvinces NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetProvinces = COALESCE(@cCommunityAssetProvinces, '') + D.CommunityAssetProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetProvince'), ELEMENTS) AS CommunityAssetProvince
			FROM dbo.CommunityAssetProvince T 
			WHERE T.CommunityAssetID = @EntityID
			) D			

		DECLARE @cCommunityAssetRisks NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetRisks = COALESCE(@cCommunityAssetRisks, '') + D.CommunityAssetRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetRisk'), ELEMENTS) AS CommunityAssetRisk
			FROM dbo.CommunityAssetRisk T 
			WHERE T.CommunityAssetID = @EntityID
			) D	

		DECLARE @cCommunityAssetUnits NVARCHAR(MAX)

		SELECT 
			@cCommunityAssetUnits = COALESCE(@cCommunityAssetUnits, '') + D.CommunityAssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetUnit'), ELEMENTS) AS CommunityAssetUnit
			FROM dbo.CommunityAssetUnit T 
			WHERE T.CommunityAssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.CommunityAssetID,
			T.CommunityAssetName,
			T.CommunityAssetDescription,
			T.CommunityAssetTypeID,
			T.AssetTypeID,
			T.ZoneTypeID,
			T.Location.STAsText(),
			CAST(('<CommunityAssetCommunities>' + ISNULL(@cCommunityAssetCommunities , '') + '</CommunityAssetCommunities>') AS XML), 
			CAST(('<CommunityAssetProvinces>' + ISNULL(@cCommunityAssetProvinces , '') + '</CommunityAssetProvinces>') AS XML),
			CAST(('<CommunityAssetRisks>' + ISNULL(@cCommunityAssetRisks  , '') + '</CommunityAssetRisks>') AS XML),
			CAST(('<CommunityAssetUnits>' + ISNULL(@cCommunityAssetUnits  , '') + '</CommunityAssetUnits>') AS XML)
			FOR XML RAW('CommunityAsset'), ELEMENTS
			)
		FROM dbo.CommunityAsset T
		WHERE T.CommunityAssetID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAssetAction

--Begin procedure eventlog.LogFindingAction
EXEC utility.DropObject 'eventlog.LogFindingAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFindingAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cFindingCommunities VARCHAR(MAX) 
	
		SELECT 
			@cFindingCommunities = COALESCE(@cFindingCommunities, '') + D.FindingCommunities 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingCommunities'), ELEMENTS) AS FindingCommunities
			FROM finding.FindingCommunity T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingComponents VARCHAR(MAX) 
	
		SELECT 
			@cFindingComponents = COALESCE(@cFindingComponents, '') + D.FindingComponents 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingComponents'), ELEMENTS) AS FindingComponents
			FROM finding.FindingComponent T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingIndicators VARCHAR(MAX) 
	
		SELECT 
			@cFindingIndicators = COALESCE(@cFindingIndicators, '') + D.FindingIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingIndicators'), ELEMENTS) AS FindingIndicators
			FROM finding.FindingIndicator T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingProvinces VARCHAR(MAX) 
	
		SELECT 
			@cFindingProvinces = COALESCE(@cFindingProvinces, '') + D.FindingProvinces 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingProvinces'), ELEMENTS) AS FindingProvinces
			FROM finding.FindingProvince T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cFindingRecommendations = COALESCE(@cFindingRecommendations, '') + D.FindingRecommendations 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingRecommendations'), ELEMENTS) AS FindingRecommendations
			FROM finding.FindingRecommendation T 
			WHERE T.FindingID = @EntityID
			) D

		DECLARE @cFindingRisks VARCHAR(MAX) 
	
		SELECT 
			@cFindingRisks = COALESCE(@cFindingRisks, '') + D.FindingRisks 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('FindingRisks'), ELEMENTS) AS FindingRisks
			FROM finding.FindingRisk T 
			WHERE T.FindingID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Finding',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<FindingCommunities>' + ISNULL(@cFindingCommunities, '') + '</FindingCommunities>') AS XML),
			CAST(('<FindingComponents>' + ISNULL(@cFindingComponents, '') + '</FindingComponents>') AS XML),
			CAST(('<FindingIndicators>' + ISNULL(@cFindingIndicators, '') + '</FindingIndicators>') AS XML),
			CAST(('<FindingProvinces>' + ISNULL(@cFindingProvinces, '') + '</FindingProvinces>') AS XML),
			CAST(('<FindingRecommendations>' + ISNULL(@cFindingRecommendations, '') + '</FindingRecommendations>') AS XML),
			CAST(('<FindingRisks>' + ISNULL(@cFindingRisks, '') + '</FindingRisks>') AS XML)
			FOR XML RAW('Finding'), ELEMENTS
			)
		FROM finding.Finding T
		WHERE T.FindingID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogFindingAction

--Begin procedure eventlog.LogProjectAction
EXEC utility.DropObject 'eventlog.LogProjectAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.07
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProjectAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Project',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cProjectCommunities VARCHAR(MAX) 
	
		SELECT 
			@cProjectCommunities = COALESCE(@cProjectCommunities, '') + D.ProjectCommunities 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectCommunities'), ELEMENTS) AS ProjectCommunities
			FROM project.ProjectCommunity T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectFindings VARCHAR(MAX) 
	
		SELECT 
			@cProjectFindings = COALESCE(@cProjectFindings, '') + D.ProjectFindings 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectFindings'), ELEMENTS) AS ProjectFindings
			FROM project.ProjectFinding T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectIndicators VARCHAR(MAX) 
	
		SELECT 
			@cProjectIndicators = COALESCE(@cProjectIndicators, '') + D.ProjectIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectIndicators'), ELEMENTS) AS ProjectIndicators
			FROM project.ProjectIndicator T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectProvinces VARCHAR(MAX) 
	
		SELECT 
			@cProjectProvinces = COALESCE(@cProjectProvinces, '') + D.ProjectProvinces 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectProvinces'), ELEMENTS) AS ProjectProvinces
			FROM project.ProjectProvince T 
			WHERE T.ProjectID = @EntityID
			) D

		DECLARE @cProjectRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cProjectRecommendations = COALESCE(@cProjectRecommendations, '') + D.ProjectRecommendations 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProjectRecommendations'), ELEMENTS) AS ProjectRecommendations
			FROM project.ProjectRecommendation T 
			WHERE T.ProjectID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Project',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ProjectCommunities>' + ISNULL(@cProjectCommunities, '') + '</ProjectCommunities>') AS XML),
			CAST(('<ProjectFindings>' + ISNULL(@cProjectFindings, '') + '</ProjectFindings>') AS XML),
			CAST(('<ProjectIndicators>' + ISNULL(@cProjectIndicators, '') + '</ProjectIndicators>') AS XML),
			CAST(('<ProjectProvinces>' + ISNULL(@cProjectProvinces, '') + '</ProjectProvinces>') AS XML),
			CAST(('<ProjectRecommendations>' + ISNULL(@cProjectRecommendations, '') + '</ProjectRecommendations>') AS XML)
			FOR XML RAW('Project'), ELEMENTS
			)
		FROM project.Project T
		WHERE T.ProjectID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProjectAction

--Begin procedure eventlog.LogRecommendationAction
EXEC utility.DropObject 'eventlog.LogRecommendationAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRecommendationAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRecommendationCommunities VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationCommunities = COALESCE(@cRecommendationCommunities, '') + D.RecommendationCommunities 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationCommunities'), ELEMENTS) AS RecommendationCommunities
			FROM recommendation.RecommendationCommunity T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationComponents VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationComponents = COALESCE(@cRecommendationComponents, '') + D.RecommendationComponents 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationComponents'), ELEMENTS) AS RecommendationComponents
			FROM recommendation.RecommendationComponent T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationIndicators VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationIndicators = COALESCE(@cRecommendationIndicators, '') + D.RecommendationIndicators 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationIndicators'), ELEMENTS) AS RecommendationIndicators
			FROM recommendation.RecommendationIndicator T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationProvinces VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationProvinces = COALESCE(@cRecommendationProvinces, '') + D.RecommendationProvinces 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationProvinces'), ELEMENTS) AS RecommendationProvinces
			FROM recommendation.RecommendationProvince T 
			WHERE T.RecommendationID = @EntityID
			) D

		DECLARE @cRecommendationRisks VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationRisks = COALESCE(@cRecommendationRisks, '') + D.RecommendationRisks 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('RecommendationRisks'), ELEMENTS) AS RecommendationRisks
			FROM recommendation.RecommendationRisk T 
			WHERE T.RecommendationID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Recommendation',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RecommendationCommunities>' + ISNULL(@cRecommendationCommunities, '') + '</RecommendationCommunities>') AS XML),
			CAST(('<RecommendationComponents>' + ISNULL(@cRecommendationComponents, '') + '</RecommendationComponents>') AS XML),
			CAST(('<RecommendationIndicators>' + ISNULL(@cRecommendationIndicators, '') + '</RecommendationIndicators>') AS XML),
			CAST(('<RecommendationProvinces>' + ISNULL(@cRecommendationProvinces, '') + '</RecommendationProvinces>') AS XML),
			CAST(('<RecommendationRisks>' + ISNULL(@cRecommendationRisks, '') + '</RecommendationRisks>') AS XML)
			FOR XML RAW('Recommendation'), ELEMENTS
			)
		FROM recommendation.Recommendation T
		WHERE T.RecommendationID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRecommendationAction

--Begin procedure eventlog.LogRecommendationUpdateAction
EXEC utility.DropObject 'eventlog.LogRecommendationUpdateAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.16
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRecommendationUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'RecommendationUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRecommendationUpdateRecommendations VARCHAR(MAX) 
	
		SELECT 
			@cRecommendationUpdateRecommendations = COALESCE(@cRecommendationUpdateRecommendations, '') + D.Recommendations
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('Recommendations'), ELEMENTS) AS Recommendations
			FROM recommendationupdate.Recommendation T 
			WHERE T.RecommendationUpdateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'RecommendationUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RecommendationUpdateRecommendations>' + ISNULL(@cRecommendationUpdateRecommendations, '') + '</RecommendationUpdateRecommendations>') AS XML)
			FOR XML RAW('RecommendationUpdate'), ELEMENTS
			)
		FROM recommendationupdate.RecommendationUpdate T
		WHERE T.RecommendationUpdateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRecommendationUpdateAction

--Begin procedure eventlog.LogRiskUpdateAction
EXEC utility.DropObject 'eventlog.LogRiskUpdateAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.09
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogRiskUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'RiskUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cRiskUpdateRisks VARCHAR(MAX) 
	
		SELECT 
			@cRiskUpdateRisks = COALESCE(@cRiskUpdateRisks, '') + D.Risks
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('Risks'), ELEMENTS) AS Risks
			FROM riskupdate.Risk T 
			WHERE T.RiskUpdateID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'RiskUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<RiskUpdateRisks>' + ISNULL(@cRiskUpdateRisks, '') + '</RiskUpdateRisks>') AS XML)
			FOR XML RAW('RiskUpdate'), ELEMENTS
			)
		FROM riskupdate.RiskUpdate T
		WHERE T.RiskUpdateID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogRiskUpdateAction

--Begin procedure finding.GetFindingByFindingID
EXEC Utility.DropObject 'finding.GetFindingByFindingID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.06
-- Description:	A stored procedure to get data from the finding.Finding table
-- ==========================================================================
CREATE PROCEDURE finding.GetFindingByFindingID

@FindingID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.FindingID,
		F.FindingName,
		F.FindingDescription,
		F.IsResearchElement,
		F.IsActive,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND F.FindingID = @FindingID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM finding.FindingCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
			AND FC.FindingID = @FindingID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		C.ComponentID,
		C.ComponentName
	FROM finding.FindingComponent FC
		JOIN dropdown.Component C ON C.ComponentID = FC.ComponentID
			AND FC.FindingID = @FindingID
	ORDER BY C.ComponentName, C.ComponentID

	SELECT
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM finding.FindingIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.FindingID = @FindingID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM finding.FindingProvince FP
		JOIN dbo.Province P ON P.ProvinceID = FP.ProvinceID
			AND FP.FindingID = @FindingID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RecommendationID,
		R.RecommendationName
	FROM finding.FindingRecommendation FR
		JOIN recommendation.Recommendation R ON R.RecommendationID = FR.RecommendationID
			AND FR.FindingID = @FindingID
	ORDER BY R.RecommendationName, R.RecommendationID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName
	FROM finding.FindingRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.FindingID = @FindingID
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure finding.GetFindingByFindingID

--Begin procedure project.GetProjectByProjectID
EXEC Utility.DropObject 'project.GetProjectByProjectID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.07
-- Description:	A stored procedure to get data from the project.Project table
-- ==========================================================================
CREATE PROCEDURE project.GetProjectByProjectID

@ProjectID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProjectID, 
		P.ProjectName, 
		P.ProjectDescription, 
		P.ProjectSummary, 
		P.ProjectPartner, 
		P.ProjectValue, 
		P.ConceptNoteID,
		dbo.FormatConceptNoteTitle(P.ConceptNoteID) AS ConceptNoteTitle,
		PS.ProjectStatusID,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND P.ProjectID = @ProjectID

	SELECT
		C.CommunityID,
		C.CommunityName,
		PC.ProjectCommunityValue
	FROM project.ProjectCommunity PC
		JOIN dbo.Community C ON C.CommunityID = PC.CommunityID
			AND PC.ProjectID = @ProjectID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusName,
		FT.FindingTypeName
	FROM project.ProjectFinding PF
		JOIN finding.Finding F ON F.FindingID = PF.FindingID
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND PF.ProjectID = @ProjectID
	ORDER BY F.FindingName, F.FindingID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM project.ProjectIndicator FI
		JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND FI.ProjectID = @ProjectID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName,
		PP.ProjectProvinceValue
	FROM project.ProjectProvince PP
		JOIN dbo.Province P ON P.ProvinceID = PP.ProvinceID
			AND PP.ProjectID = @ProjectID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RecommendationID,
		R.RecommendationName
	FROM project.ProjectRecommendation FR
		JOIN recommendation.Recommendation R ON R.RecommendationID = FR.RecommendationID
			AND FR.ProjectID = @ProjectID
	ORDER BY R.RecommendationName, R.RecommendationID

END
GO
--End procedure project.GetProjectByProjectID

--Begin procedure recommendation.GetRecommendationByRecommendationID
EXEC Utility.DropObject 'recommendation.GetRecommendationByRecommendationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.05
-- Description:	A stored procedure to get data from the recommendation.Recommendation table
-- ========================================================================================
CREATE PROCEDURE recommendation.GetRecommendationByRecommendationID

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendation.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.Community C ON C.CommunityID = RC.CommunityID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		C.ComponentID,
		C.ComponentName
	FROM recommendation.RecommendationComponent RC
		JOIN dropdown.Component C ON C.ComponentID = RC.ComponentID
			AND RC.RecommendationID = @RecommendationID
	ORDER BY C.ComponentName, C.ComponentID

	SELECT
		I.IndicatorID,
		I.IndicatorName,
		I.IndicatorDescription,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM recommendation.RecommendationIndicator RI
		JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND RI.RecommendationID = @RecommendationID
	ORDER BY O.ObjectiveName, I.IndicatorName

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.Province P ON P.ProvinceID = RP.ProvinceID
			AND RP.RecommendationID = @RecommendationID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RT.RiskTypeName
	FROM recommendation.RecommendationRisk RR
		JOIN dbo.Risk R ON R.RiskID = RR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND RR.RecommendationID = @RecommendationID
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure recommendation.GetRecommendationByRecommendationID

--Begin procedure recommendationupdate.ApproveRecommendationUpdateRecommendations
EXEC Utility.DropObject 'recommendationupdate.ApproveRecommendationUpdateRecommendations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to submit recommendations in a recommendation update for approval
-- =============================================================================
CREATE PROCEDURE recommendationupdate.ApproveRecommendationUpdateRecommendations

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	
	DECLARE @RecommendationID INT
	DECLARE @RecommendationUpdateID INT = (SELECT TOP 1 RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU ORDER BY RU.RecommendationUpdateID DESC)
	DECLARE @tOutput TABLE (RecommendationID INT)

	UPDATE R
	SET
		R.IsActive = RUR.IsActive,
		R.IsResearchElement = RUR.IsResearchElement,
		R.RecommendationDescription = RUR.RecommendationDescription,
		R.RecommendationName = RUR.RecommendationName
	OUTPUT INSERTED.RecommendationID INTO @tOutput
	FROM dbo.Recommendation R
		JOIN recommendationupdate.Recommendation RUR ON RUR.RecommendationID = R.RecommendationID
			AND RUR.RecommendationUpdateID = @RecommendationUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.RecommendationID
		FROM @tOutput O
		ORDER BY O.RecommendationID
	
	OPEN oCursor
	FETCH oCursor INTO @RecommendationID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'read', @PersonID, NULL
		EXEC eventlog.LogRecommendationAction @RecommendationID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @RecommendationID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogRecommendationUpdateAction @RecommendationUpdateID, 'update', @PersonID, NULL
	
	TRUNCATE TABLE recommendationupdate.Recommendation
	DELETE FROM recommendationupdate.RecommendationUpdate

END
GO
--End procedure recommendationupdate.ApproveRecommendationUpdateRisks

--Begin procedure recommendationupdate.DeleteRecommendationUpdateRecommendation
EXEC Utility.DropObject 'recommendationupdate.DeleteRecommendationUpdateRecommendation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to remove a recommendation from the recommendation update
-- =========================================================================================
CREATE PROCEDURE recommendationupdate.DeleteRecommendationUpdateRecommendation

@RecommendationID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE R
	FROM recommendationupdate.Recommendation R
	WHERE R.RecommendationID = @RecommendationID

END
GO
--End procedure recommendationupdate.DeleteRecommendationUpdateRecommendation

--Begin procedure recommendationupdate.GetRecommendationUpdate
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the recommendationupdate.RecommendationUpdate table
-- ====================================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRecommendationUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM recommendationupdate.RecommendationUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RecommendationUpdateID INT)

		INSERT INTO recommendationupdate.RecommendationUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RecommendationUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.RecommendationUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'

		SELECT @nRecommendationUpdateID = O.RecommendationUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRecommendationUpdateAction @EntityID=@nRecommendationUpdateID, @EventCode='Create', @PersonID = @PersonID

		END
	ELSE
		SELECT @nRecommendationUpdateID = RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU
	--ENDIF
	
	SELECT
		RU.RecommendationUpdateID, 
		RU.WorkflowStepNumber 
	FROM recommendationupdate.RecommendationUpdate RU

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
			JOIN recommendationupdate.RecommendationUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'Recommendation.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nRecommendationUpdateID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'RecommendationUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @nRecommendationUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure recommendationupdate.GetRecommendationUpdate

--Begin procedure recommendationupdate.GetRecommendationUpdateRecommendation
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdateRecommendation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get recommendation data for the recommendation update
-- ========================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdateRecommendation

@RecommendationID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendation.Recommendation R
	WHERE R.RecommendationID = @RecommendationID
			AND R.RecommendationID = @RecommendationID

	SELECT 
		R.RecommendationID,
		R.RecommendationName,
		R.RecommendationDescription,
		R.IsResearchElement,
		R.IsActive
	FROM recommendationupdate.Recommendation R
	WHERE R.RecommendationID = @RecommendationID
			AND R.RecommendationID = @RecommendationID

END
GO
--End procedure recommendationupdate.GetRecommendationUpdateRecommendation

--Begin procedure recommendationupdate.PopulateRecommendationUpdateRecommendations
EXEC Utility.DropObject 'recommendationupdate.PopulateRecommendationUpdateRecommendations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to add recommendation data to the recommendation update
-- =======================================================================================
CREATE PROCEDURE recommendationupdate.PopulateRecommendationUpdateRecommendations

@RecommendationIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO recommendationupdate.Recommendation
		(RecommendationID, RecommendationUpdateID, RecommendationName, RecommendationDescription, IsResearchElement, IsActive)

	SELECT
		R.RecommendationID, 
		(SELECT TOP 1 RU.RecommendationUpdateID FROM recommendation.RecommendationUpdate RU ORDER BY RU.RecommendationUpdateID DESC),
		R.RecommendationName, 
		R.RecommendationDescription, 
		R.IsResearchElement, 
		R.IsActive
	FROM recommendation.Recommendation R
		JOIN dbo.ListToTable(@RecommendationIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = R.RecommendationID
			AND NOT EXISTS
				(
				SELECT 1
				FROM recommendationupdate.Recommendation RUR
				WHERE RUR.RecommendationID = R.RecommendationID
				)

END
GO
--End procedure recommendationupdate.PopulateRecommendationUpdateRecommendations

--Begin procedure reporting.GetConceptNoteRiskByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteRiskByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetConceptNoteRiskByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeName
	FROM ConceptNoteRisk CNR
		JOIN Risk R ON R.RiskID = CNR.RiskID AND CNR.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType  RT ON RT.RiskTypeID = R.RiskTypeID
END
GO
--End procedure reporting.GetConceptNoteRiskByConceptNoteID

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
-- ==================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) as DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.MiddleName,
		C1.PassportNumber,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetRiskReportByPersonID
EXEC Utility.DropObject 'reporting.GetRiskReportByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			John Lyons
-- Create date: 2015.08.01
-- Description:	A stored procedure to get data from the dbo.Risk table
-- ===================================================================
CREATE PROCEDURE reporting.GetRiskReportByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeName
	FROM reporting.SearchResult SR
		JOIN Risk R ON R.RiskID = SR.EntityID  
			AND SR.PersonID = @PersonID  
			AND SR.EntityTypeCode='Risk'
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskType  RT ON RT.RiskTypeID = R.RiskTypeID

END
GO
--End procedure reporting.GetRiskReportByPersonID

--Begin procedure reporting.GetSurveyQuestionAndAnswers
EXEC Utility.DropObject 'reporting.GetSurveyQuestionAndAnswers'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.10
-- Description:	A stored procedure to get data from the survey.SurveyResponse table
-- ================================================================================
CREATE PROCEDURE reporting.GetSurveyQuestionAndAnswers

@SurveyID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		SR.SurveyResponseID,
		survey.FormatSurveyQuestionResponse(SR.LanguageCode, SSQ.SurveyQuestionID, SR.SurveyResponseID) AS SurveyQuestionAnswer,
		SSQL.SurveyQuestionText
	FROM survey.SurveyResponse SR
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyID = SR.SurveyID 
			AND SR.SurveyID = @SurveyID
		JOIN survey.SurveyQuestionLabel SSQL ON SSQL.SurveyQuestionID = SSQ.SurveyQuestionID 
			AND SSQL.LanguageCode = SR.LanguageCode

END
GO
--End procedure reporting.GetSurveyQuestionAndAnswers

--Begin procedure riskupdate.ApproveRiskUpdateRisks
EXEC Utility.DropObject 'riskupdate.ApproveRiskUpdateRisks'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to submit risks in a risk update for approval
-- =============================================================================
CREATE PROCEDURE riskupdate.ApproveRiskUpdateRisks

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	
	DECLARE @RiskID INT
	DECLARE @RiskUpdateID INT = (SELECT TOP 1 RU.RiskUpdateID FROM riskupdate.RiskUpdate RU ORDER BY RU.RiskUpdateID DESC)
	DECLARE @tOutput TABLE (RiskID INT)

	UPDATE R
	SET
		R.DateRaised = RUR.DateRaised,
		R.EscalationCriteria = RUR.EscalationCriteria,
		R.Impact = RUR.Impact,
		R.Likelihood = RUR.Likelihood,
		R.MitigationActions = RUR.MitigationActions,
		R.Notes = RUR.Notes,
		R.Overall = RUR.Overall,
		R.RaisedBy = RUR.RaisedBy,
		R.RiskCategoryID = RUR.RiskCategoryID,
		R.RiskDescription = RUR.RiskDescription,
		R.RiskName = RUR.RiskName,
		R.RiskStatusID = RUR.RiskStatusID,
		R.RiskTypeID = RUR.RiskTypeID
	OUTPUT INSERTED.RiskID INTO @tOutput
	FROM dbo.Risk R
		JOIN riskupdate.Risk RUR ON RUR.RiskID = R.RiskID
			AND RUR.RiskUpdateID = @RiskUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.RiskID
		FROM @tOutput O
		ORDER BY O.RiskID
	
	OPEN oCursor
	FETCH oCursor INTO @RiskID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogRiskAction @RiskID, 'read', @PersonID, NULL
		EXEC eventlog.LogRiskAction @RiskID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @RiskID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogRiskUpdateAction @RiskUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogRiskUpdateAction @RiskUpdateID, 'update', @PersonID, NULL
	
	TRUNCATE TABLE riskupdate.Risk
	DELETE FROM riskupdate.RiskUpdate

END
GO
--End procedure riskupdate.ApproveRiskUpdateRisks

--Begin procedure riskupdate.DeleteRiskUpdateRisk
EXEC Utility.DropObject 'riskupdate.DeleteRiskUpdateRisk'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to remove a risk from the risk update
-- =====================================================================
CREATE PROCEDURE riskupdate.DeleteRiskUpdateRisk

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE R
	FROM riskupdate.Risk R
	WHERE R.RiskID = @RiskID

END
GO
--End procedure riskupdate.DeleteRiskUpdateRisk

--Begin procedure riskupdate.GetRiskUpdate
EXEC Utility.DropObject 'riskupdate.GetRiskUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.08
-- Description:	A stored procedure to get data from the riskupdate.RiskUpdate table
-- ================================================================================
CREATE PROCEDURE riskupdate.GetRiskUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRiskUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM riskupdate.RiskUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RiskUpdateID INT)

		INSERT INTO riskupdate.RiskUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RiskUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.RiskUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'

		SELECT @nRiskUpdateID = O.RiskUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRiskUpdateAction @EntityID=@nRiskUpdateID, @EventCode='Create', @PersonID = @PersonID

		END
	ELSE
		SELECT @nRiskUpdateID = RU.RiskUpdateID FROM riskupdate.RiskUpdate RU
	--ENDIF
	
	SELECT
		RU.RiskUpdateID, 
		RU.WorkflowStepNumber 
	FROM riskupdate.RiskUpdate RU

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
			JOIN riskupdate.RiskUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'Risk.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nRiskUpdateID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'RiskUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @nRiskUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure riskupdate.GetRiskUpdate

--Begin procedure riskupdate.GetRiskUpdateRisk
EXEC Utility.DropObject 'riskupdate.GetRiskUpdateRisk'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to get Risk data for the risk update
-- ====================================================================
CREATE PROCEDURE riskupdate.GetRiskUpdateRisk

@RiskID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeID,
		RT.RiskTypeName
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND R.RiskID = @RiskID

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeID,
		RT.RiskTypeName
	FROM riskupdate.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND R.RiskID = @RiskID

END
GO
--End procedure riskupdate.GetRiskUpdateRisk

--Begin procedure riskupdate.PopulateRiskUpdateRisks
EXEC Utility.DropObject 'riskupdate.PopulateRiskUpdateRisks'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A stored procedure to add risk data to the risk update
-- ===================================================================
CREATE PROCEDURE riskupdate.PopulateRiskUpdateRisks

@RiskIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO riskupdate.Risk
		(RiskID, RiskUpdateID, RiskName, RiskDescription, RiskCategoryID, DateRaised, RaisedBy, Likelihood, Impact, Overall, MitigationActions, EscalationCriteria, Notes, RiskStatusID, RiskTypeID)
	SELECT
		R.RiskID, 
		(SELECT TOP 1 RU.RiskUpdateID FROM riskupdate.RiskUpdate RU ORDER BY RU.RiskUpdateID DESC),
		R.RiskName, 
		R.RiskDescription, 
		R.RiskCategoryID, 
		R.DateRaised, 
		R.RaisedBy, 
		R.Likelihood, 
		R.Impact, 
		R.Overall, 
		R.MitigationActions, 
		R.EscalationCriteria, 
		R.Notes, 
		R.RiskStatusID, 
		R.RiskTypeID
	FROM dbo.Risk R
		JOIN dbo.ListToTable(@RiskIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = R.RiskID
			AND NOT EXISTS
				(
				SELECT 1
				FROM riskupdate.Risk RUR
				WHERE RUR.RiskID = R.RiskID
				)

END
GO
--End procedure riskupdate.PopulateRiskUpdateRisks

--Begin procedure survey.GetSurveyBySurveyID
EXEC Utility.DropObject 'survey.GetSurveyBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.15
-- Description:	A stored procedure to get a survey from the database
--
-- Author:			Todd Pires
-- Create date: 2015.08.08
-- Description:	Added the IsAdministered flag
-- =================================================================
CREATE PROCEDURE survey.GetSurveyBySurveyID

@SurveyID INT,
@LanguageCode CHAR(2)

AS
BEGIN

	SELECT 
		S.IsActive,
		S.SurveyID,

		CASE
			WHEN EXISTS (SELECT 1 FROM survey.SurveyResponse SR WHERE SR.SurveyID = S.SurveyID)
			THEN 1
			ELSE 0
		END AS IsAdministered

	FROM survey.Survey S
	WHERE S.SurveyID = @SurveyID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SL.SurveyName, 
		SL.SurveyDescription
	FROM survey.SurveyLabel SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyLanguage SL
		JOIN dropdown.Language L ON L.LanguageCode = SL.LanguageCode
			AND SL.SurveyID = @SurveyID
			AND 
				(
				SL.LanguageCode = @LanguageCode 
					OR @LanguageCode = ''
				)
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		SQ.SurveyQuestionID,
		(SELECT STUFF((SELECT ', ' + SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID ORDER BY SQL.LanguageCode FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)'),1,2,'')) AS LanguageCodes,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionText FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionText,
	
		CASE
			WHEN @LanguageCode != ''
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = @LanguageCode)
			WHEN EXISTS (SELECT 1 FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			THEN (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = 'EN')
			ELSE (SELECT SQL.SurveyQuestionDescription FROM survey.SurveyQuestionLabel SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID AND SQL.LanguageCode = (SELECT TOP 1 SQL.LanguageCode FROM survey.SurveyQuestionLanguage SQL WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID))
		END AS SurveyQuestionDescription,
	
		SQT.SurveyQuestionResponseTypeCode,
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveySurveyQuestion SSQ
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQ.SurveyID = @SurveyID
			AND 
				(
				@LanguageCode = ''
					OR EXISTS
						(
						SELECT 1
						FROM survey.SurveyQuestionLanguage SQL
						WHERE SQL.SurveyQuestionID = SQ.SurveyQuestionID
							AND SQL.LanguageCode = @LanguageCode 
						)
				)
	
END
GO
--End procedure survey.GetSurveyBySurveyID

--Begin procedure survey.GetSurveyQuestionBySurveyQuestionID
EXEC Utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.13
-- Description:	A stored procedure to get a survey question from the database
--
-- Author:			Todd Pires
-- Create date: 2015.08.08
-- Description:	Added the IsAdministered flag
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionBySurveyQuestionID

@SurveyQuestionID INT

AS
BEGIN

	SELECT 
		SQ.IsActive,
		SQ.SurveyQuestionID, 
		SQT.SurveyQuestionResponseTypeID,
		SQT.SurveyQuestionResponseTypeCode, 
		SQT.SurveyQuestionResponseTypeName,

		CASE
			WHEN EXISTS 
				(
				SELECT SSQ.SurveyQuestionID
				FROM survey.SurveySurveyQuestion SSQ
					JOIN survey.SurveyResponse SR ON SR.SurveyID = SSQ.SurveyID
						AND SSQ.SurveyQuestionID = @SurveyQuestionID

				UNION

				SELECT SSQR.SurveyQuestionID
				FROM survey.SurveySurveyQuestionResponse SSQR
				WHERE SSQr.SurveyQuestionID = @SurveyQuestionID
				)
			THEN 1
			ELSE 0
		END AS IsAdministered

	FROM survey.SurveyQuestion SQ
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SQ.SurveyQuestionID = @SurveyQuestionID

	SELECT
		L.LanguageCode,
		L.LanguageName,
		SQL.SurveyQuestionText, 
		SQL.SurveyQuestionDescription
	FROM survey.SurveyQuestionLabel SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

	SELECT
		L.LanguageCode,
		L.LanguageName
	FROM survey.SurveyQuestionLanguage SQL
		JOIN dropdown.Language L ON L.LanguageCode = SQL.LanguageCode
			AND SQL.SurveyQuestionID = @SurveyQuestionID
	ORDER BY L.DisplayOrder, L.LanguageName	

END
GO
--End procedure survey.GetSurveyQuestionBySurveyQuestionID

--Begin procedure utility.MenuItemAddUpdate
EXEC Utility.DropObject 'utility.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to add / update a menu item
--
-- Author:			Todd Pires
-- Create Date: 2015.03.22
-- Description:	Added support for the dbo.MenuItemPermissionableLineage table
--
-- Author:			Todd Pires
-- Create Date: 2015.08.08
-- Description:	Added the ability to make a link null
-- ==========================================================================
CREATE PROCEDURE utility.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM dbo.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM dbo.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM dbo.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO dbo.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE dbo.MenuItem SET IsActive = @IsActive WHERE MenuItemID = @nNewMenuItemID
			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM dbo.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM dbo.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE dbo.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE dbo.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM dbo.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO dbo.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM dbo.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM dbo.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM dbo.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure utility.MenuItemAddUpdate

--Begin procedure workflow.CanIncrementRiskUpdateWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementRiskUpdateWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementRiskUpdateWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
			JOIN riskupdate.RiskUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RiskUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementRiskUpdateWorkflow

--Begin procedure workflow.GetRiskUpdateWorkflowData
EXEC Utility.DropObject 'workflow.GetRiskUpdateWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.09
-- Description:	A procedure to return workflow data from the riskupdate.RiskUpdate table
-- =====================================================================================

CREATE PROCEDURE workflow.GetRiskUpdateWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
			JOIN riskupdate.RiskUpdate RU ON RU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND RU.RiskUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'Risk.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'RiskUpdate'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Risk Updates'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Risk Updates'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Risk Updates'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Risk Updates'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'RiskUpdate'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetRiskUpdateWorkflowData

--Begin procedure workflow.GetRiskUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetRiskUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.11
-- Description:	A stored procedure to people associated with the workflow steps on a risk update
-- =============================================================================================
CREATE PROCEDURE workflow.GetRiskUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RiskUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM riskupdate.RiskUpdate RU WHERE RU.RiskUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'Risk.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetRiskUpdateWorkflowStepPeople
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO
--Begin table dbo.Contact
UPDATE C
	SET
		C.FirstName = STUFF(LOWER(LTRIM(RTRIM(C.FirstName))), 1, 1, UPPER(LEFT(LTRIM(C.FirstName), 1))),
		C.MiddleName = STUFF(LOWER(LTRIM(RTRIM(C.MiddleName))), 1, 1, UPPER(LEFT(LTRIM(C.MiddleName), 1))),
		C.LastName = STUFF(LOWER(LTRIM(RTRIM(C.LastName))), 1, 1, UPPER(LEFT(LTRIM(C.LastName), 1)))
FROM dbo.Contact C
GO

UPDATE C
	SET
		C.FirstName = STUFF(LOWER(LTRIM(RTRIM(C.FirstName))), 1, 1, UPPER(LEFT(LTRIM(C.FirstName), 1))),
		C.MiddleName = STUFF(LOWER(LTRIM(RTRIM(C.MiddleName))), 1, 1, UPPER(LEFT(LTRIM(C.MiddleName), 1))),
		C.LastName = STUFF(LOWER(LTRIM(RTRIM(C.LastName))), 1, 1, UPPER(LEFT(LTRIM(C.LastName), 1)))
FROM dbo.Contact C
GO

UPDATE C
SET 
	C.CellPhoneNumberCountryCallingCodeID = 
		CASE
			WHEN LEFT(C.CellPhoneNumber, 3) = '+44' OR LEFT(C.CellPhoneNumber, 4) = '0044'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'GB')
			WHEN LEFT(C.CellPhoneNumber, 3) = '+90' OR LEFT(C.CellPhoneNumber, 4) = '0090'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'TR')
			WHEN LEFT(C.CellPhoneNumber, 3) = '+96' OR LEFT(C.CellPhoneNumber, 4) = '0096'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'SY')
		END,

	C.CellPhoneNumber = 
		CASE
			WHEN LEFT(C.CellPhoneNumber, 3) IN ('+44','+90','+96')
			THEN STUFF(C.CellPhoneNumber, 1, 3, '0')
			WHEN LEFT(C.CellPhoneNumber, 4) IN ('0044','0090','0096')
			THEN STUFF(C.CellPhoneNumber, 1, 4, '0')
		END
FROM dbo.Contact C
WHERE LEFT(C.CellPhoneNumber, 3) IN ('+44','+90','+96')
	OR LEFT(C.CellPhoneNumber, 4) IN ('0044','0090','0096')
GO

UPDATE C
SET 
	C.FaxNumberCountryCallingCodeID = 
		CASE
			WHEN LEFT(C.FaxNumber, 3) = '+44' OR LEFT(C.FaxNumber, 4) = '0044'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'GB')
			WHEN LEFT(C.FaxNumber, 3) = '+90' OR LEFT(C.FaxNumber, 4) = '0090'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'TR')
			WHEN LEFT(C.FaxNumber, 3) = '+96' OR LEFT(C.FaxNumber, 4) = '0096'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'SY')
		END,

	C.FaxNumber = 
		CASE
			WHEN LEFT(C.FaxNumber, 3) IN ('+44','+90','+96')
			THEN STUFF(C.FaxNumber, 1, 3, '0')
			WHEN LEFT(C.FaxNumber, 4) IN ('0044','0090','0096')
			THEN STUFF(C.CellPhoneNumber, 1, 4, '0')
		END
FROM dbo.Contact C
WHERE LEFT(C.FaxNumber, 3) IN ('+44','+90','+96')
	OR LEFT(C.FaxNumber, 4) IN ('0044','0090','0096')
GO

UPDATE C
SET 
	C.PhoneNumberCountryCallingCodeID = 
		CASE
			WHEN LEFT(C.PhoneNumber, 3) = '+44' OR LEFT(C.PhoneNumber, 4) = '0044'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'GB')
			WHEN LEFT(C.PhoneNumber, 3) = '+90' OR LEFT(C.PhoneNumber, 4) = '0090'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'TR')
			WHEN LEFT(C.PhoneNumber, 3) = '+96' OR LEFT(C.PhoneNumber, 4) = '0096'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'SY')
		END,

	C.PhoneNumber = 
		CASE
			WHEN LEFT(C.PhoneNumber, 3) IN ('+44','+90','+96')
			THEN STUFF(C.PhoneNumber, 1, 3, '0')
			WHEN LEFT(C.PhoneNumber, 4) IN ('0044','0090','0096')
			THEN STUFF(C.CellPhoneNumber, 1, 4, '0')
		END
FROM dbo.Contact C
WHERE LEFT(C.PhoneNumber, 3) IN ('+44','+90','+96')
	OR LEFT(C.PhoneNumber, 4) IN ('0044','0090','0096')
GO

UPDATE C
SET C.CellPhoneNumber = DO.Result
FROM dbo.Contact C
	CROSS APPLY
		(
    SELECT SUBSTRING(C.CellPhoneNumber, N.Number, 1)
    FROM AJACSUtility.dbo.Number N
    WHERE N.Number BETWEEN 1 AND LEN(C.CellPhoneNumber)
			AND SUBSTRING(C.CellPhoneNumber, N.Number, 1) LIKE '[0-9]'
    ORDER BY N.Number
    FOR XML PATH ('')
) DO(Result)
GO

UPDATE C
SET C.FaxNumber = DO.Result
FROM dbo.Contact C
	CROSS APPLY
		(
    SELECT SUBSTRING(C.FaxNumber, N.Number, 1)
    FROM AJACSUtility.dbo.Number N
    WHERE N.Number BETWEEN 1 AND LEN(C.FaxNumber)
			AND SUBSTRING(C.FaxNumber, N.Number, 1) LIKE '[0-9]'
    ORDER BY N.Number
    FOR XML PATH ('')
) DO(Result)
GO

UPDATE C
SET C.PhoneNumber = DO.Result
FROM dbo.Contact C
	CROSS APPLY
		(
    SELECT SUBSTRING(C.PhoneNumber, N.Number, 1)
    FROM AJACSUtility.dbo.Number N
    WHERE N.Number BETWEEN 1 AND LEN(C.PhoneNumber)
			AND SUBSTRING(C.PhoneNumber, N.Number, 1) LIKE '[0-9]'
    ORDER BY N.Number
    FOR XML PATH ('')
) DO(Result)
GO
--End Table dbo.Contact

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'RecommendationUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	VALUES
		('RecommendationUpdate', 'IncrementWorkflow', '<p>An AJACS recommendation update has been submitted for your review:</p><p><strong>Recommendation Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the recommendation update workflow. Please click the link above to review the updated recommendations.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RecommendationUpdate', 'DecrementWorkflow', '<p>A previously submitted recommendation update has been disapproved:</p><p><strong>Recommendation Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the recommendation update workflow. Please click the link above to review the updated recommendations.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RecommendationUpdate', 'Release', '<p>An AJACS recommendation update has been released:</p><p><strong>Recommendation Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the recommendation update workflow. Please click the link above to review the updated recommendations.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'RiskUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	VALUES
		('RiskUpdate', 'IncrementWorkflow', '<p>An AJACS risk update has been submitted for your review:</p><p><strong>Risk Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the risk update workflow. Please click the link above to review the updated risks.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RiskUpdate', 'DecrementWorkflow', '<p>A previously submitted risk update has been disapproved:</p><p><strong>Risk Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the risk update workflow. Please click the link above to review the updated risks.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RiskUpdate', 'Release', '<p>An AJACS risk update has been released:</p><p><strong>Risk Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the risk update workflow. Please click the link above to review the updated risks.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>')

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'RecommendationUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('RecommendationUpdate', '[[Link]]', 'Link', 1),
		('RecommendationUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'RiskUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('RiskUpdate', '[[Link]]', 'Link', 1),
		('RiskUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Recommendation')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Recommendation', 'Recommendation')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'RecommendationUpdate')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('RecommendationUpdate', 'Recommendation Update')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'RiskUpdate')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('RiskUpdate', 'Risk Update')
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE dbo.MenuItem
SET MenuItemCode = 'Implementation'
WHERE MenuItemCode = 'ManageWeeklyReports'
GO

UPDATE dbo.MenuItem
SET MenuItemCode = 'CommunityAssetList'
WHERE MenuItemCode = 'CommunityAsset'
GO

UPDATE dbo.MenuItem
SET MenuItemCode = 'RecommendationList'
WHERE MenuItemCode = 'Recommendation'
GO

UPDATE dbo.MenuItem
SET DisplayOrder = 1
WHERE MenuItemCode = 'Dashboard'
GO

UPDATE dbo.MenuItem
SET Icon = NULL
GO

DELETE 
FROM dbo.MenuItem
WHERE MenuItemCode = 'SurveyResults'
GO

--Dashboard
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Dashboard', @Icon='fa fa-fw fa-dashboard'
GO

--Provinces
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Province', @AfterMenuItemCode='Dashboard', @Icon='fa fa-fw fa-flag-o'
GO

--Communities
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Community', @AfterMenuItemCode='Province', @Icon='fa fa-fw fa-building'
GO

--Document Library
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Document', @AfterMenuItemCode='Community', @Icon='fa fa-fw fa-folder-open'
GO

--Requests For Information
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RequestForInformation', @AfterMenuItemCode='Document', @Icon='fa fa-fw fa-question-circle'
GO

--Research
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Research', @NewMenuItemText='Research', @AfterMenuItemCode='RequestForInformation', @Icon='fa fa-fw fa-balance-scale'
GO
--Research >> Atmospherics
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Atmospheric', @ParentMenuItemCode='Research'
GO
--Research >> Atmospherics >> General Atmospheric Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='AtmosphericReportList', @ParentMenuItemCode='Atmospheric'
GO
--Research >> Atmospherics >> Incident Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IncidentReportList', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='AtmosphericReportList'
GO
--Research >> Atmospherics >> Weekly Atmospheric Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='IncidentReportList', @NewMenuItemText='Atmospheric Report'
GO
--Research >> Spot Reports
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SpotReport', @ParentMenuItemCode='Research', @AfterMenuItemCode='Atmospheric'
GO
--Research >> Surveys
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Surveys', @ParentMenuItemCode='Research', @AfterMenuItemCode='SpotReport'
GO
--Research >> Surveys >> Questions
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='QuestionList', @ParentMenuItemCode='Surveys'
GO
--Research >> Surveys >> Surveys
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyList', @ParentMenuItemCode='Surveys', @AfterMenuItemCode='QuestionList'
GO

--Rapid Assessments
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPData', @AfterMenuItemCode='Research', @Icon='fa fa-fw fa-bar-chart'
GO
--Rapid Assessments >> View RAP Data
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPDataList', @ParentMenuItemCode='RAPData'
GO
--Rapid Assessments >> RAP Tools
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPTools', @NewMenuItemText='RAP Tools', @ParentMenuItemCode='RAPData', @AfterMenuItemCode='RAPDataList'
GO
--Rapid Assessments >> RAP Tools >> Add Community Member Survey
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityMemberSurveyAdd', @ParentMenuItemCode='RAPTools'
GO
--Rapid Assessments >> RAP Tools >> Add Focus Group Questionnaire
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='FocusGroupSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='CommunityMemberSurveyAdd'
GO
--Rapid Assessments >> RAP Tools >> Add FSP Station Commander Survey
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StationCommanderSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='FocusGroupSurveyAdd'
GO
--Rapid Assessments >> RAP Tools >> Add Key Informant Interview
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='KeyInformantSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='StationCommanderSurveyAdd'
GO
--Rapid Assessments >> RAP Tools >> Add Stakeholder Group Questionnaire
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StakeholderGroupSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='KeyInformantSurveyAdd'
GO
--Rapid Assessments >> RAP Delivery
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPDelivery', @ParentMenuItemCode='RAPData', @AfterMenuItemCode='RAPTools'
GO
--Rapid Assessments >> RAP Delivery >> View Daily Reports
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DailyReportList', @ParentMenuItemCode='RAPDelivery'
GO
--Rapid Assessments >> RAP Delivery >> View Teams
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='TeamList', @ParentMenuItemCode='RAPDelivery', @AfterMenuItemCode='DailyReportList'
GO
--Rapid Assessments >> RAP To Implementation
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPImplementation', @NewMenuItemText='RAP To Implementation', @ParentMenuItemCode='RAPData', @AfterMenuItemCode='RAPDelivery'
GO
--Rapid Assessments >> RAP To Implementation >> Findings
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Finding', @NewMenuItemLink='/finding/list', @NewMenuItemText='Findings', @ParentMenuItemCode='RAPImplementation', @PermissionableLineageList='Finding.List'
GO
--Rapid Assessments >> RAP To Implementation >> Recommendations
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RecommendationManagement', @ParentMenuItemCode='RAPImplementation', @AfterMenuItemCode='Finding', @NewMenuItemText='Recommendations', @NewMenuItemLink='', @PermissionableLineageList=''
GO
--Rapid Assessments >> RAP To Implementation >> Recommendations >> List Recommendations
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RecommendationList', @NewMenuItemLink='/recommendation/list', @NewMenuItemText='List Recommendations', @ParentMenuItemCode='RecommendationManagement', @PermissionableLineageList='Recommendation.List'
GO
--Rapid Assessments >> RAP To Implementation >> Recommendations >> Update Recommendations
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RecommendationUpdate', @ParentMenuItemCode='RecommendationManagement', @NewMenuItemLink='/recommendation/recommendationupdate', @NewMenuItemText='Update Recommendations', @PermissionableLineageList='Recommendation.AddUpdate', @AfterMenuItemCode='RecommendationList'
GO

--Implementation
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Implementation', @NewMenuItemText='Implementation', @AfterMenuItemCode='RAPData', @Icon='fa fa-fw fa-calendar'
GO
--Implementation >> Risk Management
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskManagement', @ParentMenuItemCode='Implementation', @NewMenuItemLink='', @PermissionableLineageList=''
GO
--Implementation >> Risk Management >> List Risks
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskList', @ParentMenuItemCode='RiskManagement', @NewMenuItemLink='/risk/list', @NewMenuItemText='List Risks', @PermissionableLineageList='Risk.List'
GO
--Implementation >> Risk Management >> Update Risks 
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskUpdate', @ParentMenuItemCode='RiskManagement', @NewMenuItemLink='/risk/riskupdate', @NewMenuItemText='Update Risks', @PermissionableLineageList='Risk.AddUpdate', @AfterMenuItemCode='RiskList'
GO
--Implementation >> Community Assets
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityAssetList', @ParentMenuItemCode='Implementation', @NewMenuItemLink='/communityasset/list', @AfterMenuItemCode='RiskManagement', @NewMenuItemText='Community Assets', @PermissionableLineageList='CommunityAsset.List'
GO
--Implementation >> Projects
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ProjectList', @ParentMenuItemCode='Implementation', @NewMenuItemLink='/project/list', @AfterMenuItemCode='RiskManagement', @NewMenuItemText='Projects', @PermissionableLineageList='Project.List'
GO
--Implementation >> Training
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Training', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='ProjectList'
GO
--Implementation >> Training >> View Course Catalog
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CourseList', @ParentMenuItemCode='Training'
GO
--Implementation >> Training >> View Class Schedules
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ClassList', @ParentMenuItemCode='Training', @AfterMenuItemCode='CourseList'
GO
--Implementation >> Equipment
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Equipment', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='Training'
GO
--Implementation >> Equipment >> Equipment Catalog
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentCatalogList', @ParentMenuItemCode='Equipment'
GO
--Implementation >> Equipment >> Equipment Inventory
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentInventoryList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentCatalogList'
GO
--Implementation >> Equipment >> Equipment Allocation 
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactEquipmentList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentInventoryList'
GO
--Implementation >> Equipment >> Equipment Distribution Plan
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentDistributionPlanList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='ConceptNoteContactEquipmentList'
GO
--Implementation >> Contacts
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Contacts', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='Equipment'
GO
--Implementation >> Contacts >> Beneficiaries
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='BeneficiaryList', @ParentMenuItemCode='Contacts'
GO
--Implementation >> Contacts >> Staff & Partners
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerList', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='BeneficiaryList'
GO
--Implementation >> Contacts >> Staff & Partner Vetting
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerVettingList', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='StaffPartnerList'
GO
--Implementation >> Contacts >> Sub Contractors
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SubContractorList', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='StaffPartnerVettingList'
GO
--Implementation >> Stipends
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Stipends', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='Contacts'
GO
--Implementation >> Stipends >>	Recipients
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StipendList', @ParentMenuItemCode='Stipends'
GO
--Implementation >> Stipends >>	Payments
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PaymentList', @ParentMenuItemCode='Stipends', @AfterMenuItemCode='StipendList'
GO

--Operations and Implementation Support
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='OpperationsSupport', @NewMenuItemText='Operations &amp; Implementation Support', @AfterMenuItemCode='Implementation', @Icon='fa fa-fw fa-compass'
GO
--Operations and Implementation Support >> Procurement
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Procurement', @ParentMenuItemCode='OpperationsSupport'
GO
--Operations and Implementation Support >> Procurement >>	Purchase Requests
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PurchaseRequestList', @ParentMenuItemCode='Procurement'
GO
--Operations and Implementation Support >> Procurement >>	Licenses
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LicenseList', @ParentMenuItemCode='Procurement', @AfterMenuItemCode='PurchaseRequestList'
GO
--Operations and Implementation Support >> Procurement >>	Licensed Equipment
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LicenseEquipmentCatalogList', @ParentMenuItemCode='Procurement', @AfterMenuItemCode='LicenseList'
GO
--Operations and Implementation Support >> Activities
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Activity', @ParentMenuItemCode='OpperationsSupport', @AfterMenuItemCode='Procurement'
GO
--Operations and Implementation Support >> Activities >>	Activity Management
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNote', @ParentMenuItemCode='Activity'
GO
--Operations and Implementation Support >> Activities >>	Activity Vetting
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactVettingList', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNote'
GO

--Monitoring & Evaluation
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LogicalFramework', @AfterMenuItemCode='OpperationsSupport', @Icon='fa fa-fw fa-heartbeat'
GO
--Monitoring & Evaluation >>	Overview
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveOverview', @ParentMenuItemCode='LogicalFramework'
GO
--Monitoring & Evaluation >>	Objectives
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='ObjectiveOverview'
GO
--Monitoring & Evaluation >>	Indicators
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='ObjectiveList'
GO
--Monitoring & Evaluation >>	Indicator Types
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorTypeList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='IndicatorList'
GO
--Monitoring & Evaluation >>	Milestones
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='MilestoneList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='IndicatorTypeList'
GO
--Monitoring & Evaluation >>	Program Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ProgramReportList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='MilestoneList'
GO

--Admin
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Admin', @AfterMenuItemCode='LogicalFramework', @Icon='fa fa-fw fa-cogs'
GO
--Admin >> Event Log
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EventLogList', @ParentMenuItemCode='Admin'
GO
--Admin >> Users
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PersonList', @ParentMenuItemCode='Admin', @AfterMenuItemCode='EventLogList'
GO
--Admin >> Permission Templates
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PermissionableTemplateList', @ParentMenuItemCode='Admin', @AfterMenuItemCode='PersonList'
GO
--Admin >>	Email Templates
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EmailTemplateAdministrationList', @ParentMenuItemCode='Admin', @AfterMenuItemCode='PermissionableTemplateList'
GO
--Admin >>	Download RAP Data
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DownloadRAPData', @NewMenuItemLink='/rapdata/download', @NewMenuItemText='Download RAP Data', @ParentMenuItemCode='Admin', @AfterMenuItemCode='EmailTemplateAdministrationList'
GO
--Admin >> Server Setup Keys
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ServerSetup', @ParentMenuItemCode='Admin', @AfterMenuItemCode='DownloadRAPData'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Activity'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Atmospheric'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Contacts'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Equipment'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Procurement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RecommendationManagement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RiskManagement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPDelivery'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPImplementation'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPTools'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Stipends'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Surveys'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Training'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='OpperationsSupport'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPData'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO
--End table dbo.MenuItem

--Begin table dropdown.CountryCallingCode
IF NOT EXISTS (SELECT 1 FROM dropdown.CountryCallingCode CCC WHERE CCC.CountryCallingCodeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT dropdown.CountryCallingCode ON
	
	INSERT INTO dropdown.CountryCallingCode (CountryCallingCodeID) VALUES (0)

	SET IDENTITY_INSERT dropdown.CountryCallingCode OFF
	
	END
--ENDIF
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.RiskStatus
UPDATE dropdown.RiskStatus
SET IsActive = 0
WHERE RiskStatusName = 'Activity Specific'
GO
--End table dropdown.RiskStatus

--Begin table dropdown.VettingOutcome
UPDATE VO
SET VO.HexColor =
	CASE
		WHEN VO.VettingOutcomeName = 'Not Vetted'
		THEN '#757575'
		WHEN VO.VettingOutcomeName = 'Pending Internal Review'
		THEN '#FCEB1F'
		WHEN VO.VettingOutcomeName = 'Consider'
		THEN '#32AC41'
		WHEN VO.VettingOutcomeName = 'Do Not Consider'
		THEN '#EA1921'
		WHEN VO.VettingOutcomeName = 'Insufficient Data'
		THEN '#757575'
		WHEN VO.VettingOutcomeName = 'Submitted for Vetting'
		THEN '#FCEB1F'
		ELSE VO.HexColor
	END
FROM dropdown.VettingOutcome VO
GO
--End table dropdown.VettingOutcome

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityAsset')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('CommunityAsset','Community Assets', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Finding')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Finding','Findings', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Project')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Project','Projects', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Recommendation')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Recommendation','Recommendations', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DownloadRAPData')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES 
		(0, 'DownloadRAPData', 'Download RAP Data')
		
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportVetting',
	'Export Vetting',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.VettingList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.VettingList.ExportVetting'
		)
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CommunityAsset')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'CommunityAsset','Community Assets')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'CommunityAsset'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'CommunityAsset'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'CommunityAsset'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Finding')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Finding','Findings')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Finding'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Finding'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Finding'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Project')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Project','Projects')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Project'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Project'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Project'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Recommendation')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Recommendation','Recommendations')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Recommendation'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Recommendation'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Recommendation'

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Finding'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Finding')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityAsset'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('CommunityAsset')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Project'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Project')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Recommendation'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Recommendation')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'RecommendationUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('RecommendationUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RecommendationUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RecommendationUpdate'),
		WSWA.WorkflowStepNumber,
		
		CASE
			WHEN WA1.WorkflowActionCode = 'IncrementWorkflowAndUploadFile'
			THEN (SELECT WA2.WorkflowActionID FROM workflow.WorkflowAction WA2 WHERE WA2.WorkflowActionCode = 'IncrementWorkflow' AND WA2.WorkflowActionName = 'Approve')
			ELSE WSWA.WorkflowActionID
		END,
		
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'RiskUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('RiskUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RiskUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RiskUpdate'),
		WSWA.WorkflowStepNumber,
		
		CASE
			WHEN WA1.WorkflowActionCode = 'IncrementWorkflowAndUploadFile'
			THEN (SELECT WA2.WorkflowActionID FROM workflow.WorkflowAction WA2 WHERE WA2.WorkflowActionCode = 'IncrementWorkflow' AND WA2.WorkflowActionName = 'Approve')
			ELSE WSWA.WorkflowActionID
		END,
		
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table permissionable.Permissionable
DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'RecommendationUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'RecommendationUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Recommendation.AddUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'RiskUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'RiskUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Risk.AddUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.24 File 01 - AJACS - 2015.08.22 20.51.28')
GO
--End build tracking

