USE AJACS
GO
--Begin table dbo.Contact
UPDATE C
	SET
		C.FirstName = STUFF(LOWER(LTRIM(RTRIM(C.FirstName))), 1, 1, UPPER(LEFT(LTRIM(C.FirstName), 1))),
		C.MiddleName = STUFF(LOWER(LTRIM(RTRIM(C.MiddleName))), 1, 1, UPPER(LEFT(LTRIM(C.MiddleName), 1))),
		C.LastName = STUFF(LOWER(LTRIM(RTRIM(C.LastName))), 1, 1, UPPER(LEFT(LTRIM(C.LastName), 1)))
FROM dbo.Contact C
GO

UPDATE C
	SET
		C.FirstName = STUFF(LOWER(LTRIM(RTRIM(C.FirstName))), 1, 1, UPPER(LEFT(LTRIM(C.FirstName), 1))),
		C.MiddleName = STUFF(LOWER(LTRIM(RTRIM(C.MiddleName))), 1, 1, UPPER(LEFT(LTRIM(C.MiddleName), 1))),
		C.LastName = STUFF(LOWER(LTRIM(RTRIM(C.LastName))), 1, 1, UPPER(LEFT(LTRIM(C.LastName), 1)))
FROM dbo.Contact C
GO

UPDATE C
SET 
	C.CellPhoneNumberCountryCallingCodeID = 
		CASE
			WHEN LEFT(C.CellPhoneNumber, 3) = '+44' OR LEFT(C.CellPhoneNumber, 4) = '0044'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'GB')
			WHEN LEFT(C.CellPhoneNumber, 3) = '+90' OR LEFT(C.CellPhoneNumber, 4) = '0090'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'TR')
			WHEN LEFT(C.CellPhoneNumber, 3) = '+96' OR LEFT(C.CellPhoneNumber, 4) = '0096'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'SY')
		END,

	C.CellPhoneNumber = 
		CASE
			WHEN LEFT(C.CellPhoneNumber, 3) IN ('+44','+90','+96')
			THEN STUFF(C.CellPhoneNumber, 1, 3, '0')
			WHEN LEFT(C.CellPhoneNumber, 4) IN ('0044','0090','0096')
			THEN STUFF(C.CellPhoneNumber, 1, 4, '0')
		END
FROM dbo.Contact C
WHERE LEFT(C.CellPhoneNumber, 3) IN ('+44','+90','+96')
	OR LEFT(C.CellPhoneNumber, 4) IN ('0044','0090','0096')
GO

UPDATE C
SET 
	C.FaxNumberCountryCallingCodeID = 
		CASE
			WHEN LEFT(C.FaxNumber, 3) = '+44' OR LEFT(C.FaxNumber, 4) = '0044'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'GB')
			WHEN LEFT(C.FaxNumber, 3) = '+90' OR LEFT(C.FaxNumber, 4) = '0090'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'TR')
			WHEN LEFT(C.FaxNumber, 3) = '+96' OR LEFT(C.FaxNumber, 4) = '0096'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'SY')
		END,

	C.FaxNumber = 
		CASE
			WHEN LEFT(C.FaxNumber, 3) IN ('+44','+90','+96')
			THEN STUFF(C.FaxNumber, 1, 3, '0')
			WHEN LEFT(C.FaxNumber, 4) IN ('0044','0090','0096')
			THEN STUFF(C.CellPhoneNumber, 1, 4, '0')
		END
FROM dbo.Contact C
WHERE LEFT(C.FaxNumber, 3) IN ('+44','+90','+96')
	OR LEFT(C.FaxNumber, 4) IN ('0044','0090','0096')
GO

UPDATE C
SET 
	C.PhoneNumberCountryCallingCodeID = 
		CASE
			WHEN LEFT(C.PhoneNumber, 3) = '+44' OR LEFT(C.PhoneNumber, 4) = '0044'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'GB')
			WHEN LEFT(C.PhoneNumber, 3) = '+90' OR LEFT(C.PhoneNumber, 4) = '0090'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'TR')
			WHEN LEFT(C.PhoneNumber, 3) = '+96' OR LEFT(C.PhoneNumber, 4) = '0096'
			THEN (SELECT CCC.CountryCallingCodeID FROM dropdown.CountryCallingCode CCC JOIN dropdown.Country C ON C.CountryID = CCC.CountryID AND C.ISOCountryCode2 = 'SY')
		END,

	C.PhoneNumber = 
		CASE
			WHEN LEFT(C.PhoneNumber, 3) IN ('+44','+90','+96')
			THEN STUFF(C.PhoneNumber, 1, 3, '0')
			WHEN LEFT(C.PhoneNumber, 4) IN ('0044','0090','0096')
			THEN STUFF(C.CellPhoneNumber, 1, 4, '0')
		END
FROM dbo.Contact C
WHERE LEFT(C.PhoneNumber, 3) IN ('+44','+90','+96')
	OR LEFT(C.PhoneNumber, 4) IN ('0044','0090','0096')
GO

UPDATE C
SET C.CellPhoneNumber = DO.Result
FROM dbo.Contact C
	CROSS APPLY
		(
    SELECT SUBSTRING(C.CellPhoneNumber, N.Number, 1)
    FROM AJACSUtility.dbo.Number N
    WHERE N.Number BETWEEN 1 AND LEN(C.CellPhoneNumber)
			AND SUBSTRING(C.CellPhoneNumber, N.Number, 1) LIKE '[0-9]'
    ORDER BY N.Number
    FOR XML PATH ('')
) DO(Result)
GO

UPDATE C
SET C.FaxNumber = DO.Result
FROM dbo.Contact C
	CROSS APPLY
		(
    SELECT SUBSTRING(C.FaxNumber, N.Number, 1)
    FROM AJACSUtility.dbo.Number N
    WHERE N.Number BETWEEN 1 AND LEN(C.FaxNumber)
			AND SUBSTRING(C.FaxNumber, N.Number, 1) LIKE '[0-9]'
    ORDER BY N.Number
    FOR XML PATH ('')
) DO(Result)
GO

UPDATE C
SET C.PhoneNumber = DO.Result
FROM dbo.Contact C
	CROSS APPLY
		(
    SELECT SUBSTRING(C.PhoneNumber, N.Number, 1)
    FROM AJACSUtility.dbo.Number N
    WHERE N.Number BETWEEN 1 AND LEN(C.PhoneNumber)
			AND SUBSTRING(C.PhoneNumber, N.Number, 1) LIKE '[0-9]'
    ORDER BY N.Number
    FOR XML PATH ('')
) DO(Result)
GO
--End Table dbo.Contact

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'RecommendationUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	VALUES
		('RecommendationUpdate', 'IncrementWorkflow', '<p>An AJACS recommendation update has been submitted for your review:</p><p><strong>Recommendation Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the recommendation update workflow. Please click the link above to review the updated recommendations.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RecommendationUpdate', 'DecrementWorkflow', '<p>A previously submitted recommendation update has been disapproved:</p><p><strong>Recommendation Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the recommendation update workflow. Please click the link above to review the updated recommendations.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RecommendationUpdate', 'Release', '<p>An AJACS recommendation update has been released:</p><p><strong>Recommendation Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the recommendation update workflow. Please click the link above to review the updated recommendations.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>')

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'RiskUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	VALUES
		('RiskUpdate', 'IncrementWorkflow', '<p>An AJACS risk update has been submitted for your review:</p><p><strong>Risk Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the risk update workflow. Please click the link above to review the updated risks.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RiskUpdate', 'DecrementWorkflow', '<p>A previously submitted risk update has been disapproved:</p><p><strong>Risk Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the risk update workflow. Please click the link above to review the updated risks.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'),
		('RiskUpdate', 'Release', '<p>An AJACS risk update has been released:</p><p><strong>Risk Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the risk update workflow. Please click the link above to review the updated risks.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>')

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'RecommendationUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('RecommendationUpdate', '[[Link]]', 'Link', 1),
		('RecommendationUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'RiskUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('RiskUpdate', '[[Link]]', 'Link', 1),
		('RiskUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Recommendation')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Recommendation', 'Recommendation')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'RecommendationUpdate')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('RecommendationUpdate', 'Recommendation Update')
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'RiskUpdate')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('RiskUpdate', 'Risk Update')
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE dbo.MenuItem
SET MenuItemCode = 'Implementation'
WHERE MenuItemCode = 'ManageWeeklyReports'
GO

UPDATE dbo.MenuItem
SET MenuItemCode = 'CommunityAssetList'
WHERE MenuItemCode = 'CommunityAsset'
GO

UPDATE dbo.MenuItem
SET MenuItemCode = 'RecommendationList'
WHERE MenuItemCode = 'Recommendation'
GO

UPDATE dbo.MenuItem
SET DisplayOrder = 1
WHERE MenuItemCode = 'Dashboard'
GO

UPDATE dbo.MenuItem
SET Icon = NULL
GO

DELETE 
FROM dbo.MenuItem
WHERE MenuItemCode = 'SurveyResults'
GO

--Dashboard
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Dashboard', @Icon='fa fa-fw fa-dashboard'
GO

--Provinces
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Province', @AfterMenuItemCode='Dashboard', @Icon='fa fa-fw fa-flag-o'
GO

--Communities
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Community', @AfterMenuItemCode='Province', @Icon='fa fa-fw fa-building'
GO

--Document Library
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Document', @AfterMenuItemCode='Community', @Icon='fa fa-fw fa-folder-open'
GO

--Requests For Information
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RequestForInformation', @AfterMenuItemCode='Document', @Icon='fa fa-fw fa-question-circle'
GO

--Research
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Research', @NewMenuItemText='Research', @AfterMenuItemCode='RequestForInformation', @Icon='fa fa-fw fa-balance-scale'
GO
--Research >> Atmospherics
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Atmospheric', @ParentMenuItemCode='Research'
GO
--Research >> Atmospherics >> General Atmospheric Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='AtmosphericReportList', @ParentMenuItemCode='Atmospheric'
GO
--Research >> Atmospherics >> Incident Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IncidentReportList', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='AtmosphericReportList'
GO
--Research >> Atmospherics >> Weekly Atmospheric Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='IncidentReportList', @NewMenuItemText='Atmospheric Report'
GO
--Research >> Spot Reports
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SpotReport', @ParentMenuItemCode='Research', @AfterMenuItemCode='Atmospheric'
GO
--Research >> Surveys
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Surveys', @ParentMenuItemCode='Research', @AfterMenuItemCode='SpotReport'
GO
--Research >> Surveys >> Questions
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='QuestionList', @ParentMenuItemCode='Surveys'
GO
--Research >> Surveys >> Surveys
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyList', @ParentMenuItemCode='Surveys', @AfterMenuItemCode='QuestionList'
GO

--Rapid Assessments
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPData', @AfterMenuItemCode='Research', @Icon='fa fa-fw fa-bar-chart'
GO
--Rapid Assessments >> View RAP Data
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPDataList', @ParentMenuItemCode='RAPData'
GO
--Rapid Assessments >> RAP Tools
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPTools', @NewMenuItemText='RAP Tools', @ParentMenuItemCode='RAPData', @AfterMenuItemCode='RAPDataList'
GO
--Rapid Assessments >> RAP Tools >> Add Community Member Survey
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityMemberSurveyAdd', @ParentMenuItemCode='RAPTools'
GO
--Rapid Assessments >> RAP Tools >> Add Focus Group Questionnaire
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='FocusGroupSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='CommunityMemberSurveyAdd'
GO
--Rapid Assessments >> RAP Tools >> Add FSP Station Commander Survey
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StationCommanderSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='FocusGroupSurveyAdd'
GO
--Rapid Assessments >> RAP Tools >> Add Key Informant Interview
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='KeyInformantSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='StationCommanderSurveyAdd'
GO
--Rapid Assessments >> RAP Tools >> Add Stakeholder Group Questionnaire
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StakeholderGroupSurveyAdd', @ParentMenuItemCode='RAPTools', @AfterMenuItemCode='KeyInformantSurveyAdd'
GO
--Rapid Assessments >> RAP Delivery
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPDelivery', @ParentMenuItemCode='RAPData', @AfterMenuItemCode='RAPTools'
GO
--Rapid Assessments >> RAP Delivery >> View Daily Reports
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DailyReportList', @ParentMenuItemCode='RAPDelivery'
GO
--Rapid Assessments >> RAP Delivery >> View Teams
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='TeamList', @ParentMenuItemCode='RAPDelivery', @AfterMenuItemCode='DailyReportList'
GO
--Rapid Assessments >> RAP To Implementation
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RAPImplementation', @NewMenuItemText='RAP To Implementation', @ParentMenuItemCode='RAPData', @AfterMenuItemCode='RAPDelivery'
GO
--Rapid Assessments >> RAP To Implementation >> Findings
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Finding', @NewMenuItemLink='/finding/list', @NewMenuItemText='Findings', @ParentMenuItemCode='RAPImplementation', @PermissionableLineageList='Finding.List'
GO
--Rapid Assessments >> RAP To Implementation >> Recommendations
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RecommendationManagement', @ParentMenuItemCode='RAPImplementation', @AfterMenuItemCode='Finding', @NewMenuItemText='Recommendations', @NewMenuItemLink='', @PermissionableLineageList=''
GO
--Rapid Assessments >> RAP To Implementation >> Recommendations >> List Recommendations
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RecommendationList', @NewMenuItemLink='/recommendation/list', @NewMenuItemText='List Recommendations', @ParentMenuItemCode='RecommendationManagement', @PermissionableLineageList='Recommendation.List'
GO
--Rapid Assessments >> RAP To Implementation >> Recommendations >> Update Recommendations
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RecommendationUpdate', @ParentMenuItemCode='RecommendationManagement', @NewMenuItemLink='/recommendation/recommendationupdate', @NewMenuItemText='Update Recommendations', @PermissionableLineageList='Recommendation.AddUpdate', @AfterMenuItemCode='RecommendationList'
GO

--Implementation
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Implementation', @NewMenuItemText='Implementation', @AfterMenuItemCode='RAPData', @Icon='fa fa-fw fa-calendar'
GO
--Implementation >> Risk Management
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskManagement', @ParentMenuItemCode='Implementation', @NewMenuItemLink='', @PermissionableLineageList=''
GO
--Implementation >> Risk Management >> List Risks
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskList', @ParentMenuItemCode='RiskManagement', @NewMenuItemLink='/risk/list', @NewMenuItemText='List Risks', @PermissionableLineageList='Risk.List'
GO
--Implementation >> Risk Management >> Update Risks 
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskUpdate', @ParentMenuItemCode='RiskManagement', @NewMenuItemLink='/risk/riskupdate', @NewMenuItemText='Update Risks', @PermissionableLineageList='Risk.AddUpdate', @AfterMenuItemCode='RiskList'
GO
--Implementation >> Community Assets
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityAssetList', @ParentMenuItemCode='Implementation', @NewMenuItemLink='/communityasset/list', @AfterMenuItemCode='RiskManagement', @NewMenuItemText='Community Assets', @PermissionableLineageList='CommunityAsset.List'
GO
--Implementation >> Projects
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ProjectList', @ParentMenuItemCode='Implementation', @NewMenuItemLink='/project/list', @AfterMenuItemCode='RiskManagement', @NewMenuItemText='Projects', @PermissionableLineageList='Project.List'
GO
--Implementation >> Training
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Training', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='ProjectList'
GO
--Implementation >> Training >> View Course Catalog
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CourseList', @ParentMenuItemCode='Training'
GO
--Implementation >> Training >> View Class Schedules
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ClassList', @ParentMenuItemCode='Training', @AfterMenuItemCode='CourseList'
GO
--Implementation >> Equipment
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Equipment', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='Training'
GO
--Implementation >> Equipment >> Equipment Catalog
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentCatalogList', @ParentMenuItemCode='Equipment'
GO
--Implementation >> Equipment >> Equipment Inventory
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentInventoryList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentCatalogList'
GO
--Implementation >> Equipment >> Equipment Allocation 
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactEquipmentList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentInventoryList'
GO
--Implementation >> Equipment >> Equipment Distribution Plan
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentDistributionPlanList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='ConceptNoteContactEquipmentList'
GO
--Implementation >> Contacts
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Contacts', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='Equipment'
GO
--Implementation >> Contacts >> Beneficiaries
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='BeneficiaryList', @ParentMenuItemCode='Contacts'
GO
--Implementation >> Contacts >> Staff & Partners
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerList', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='BeneficiaryList'
GO
--Implementation >> Contacts >> Staff & Partner Vetting
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerVettingList', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='StaffPartnerList'
GO
--Implementation >> Contacts >> Sub Contractors
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SubContractorList', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='StaffPartnerVettingList'
GO
--Implementation >> Stipends
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Stipends', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='Contacts'
GO
--Implementation >> Stipends >>	Recipients
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StipendList', @ParentMenuItemCode='Stipends'
GO
--Implementation >> Stipends >>	Payments
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PaymentList', @ParentMenuItemCode='Stipends', @AfterMenuItemCode='StipendList'
GO

--Operations and Implementation Support
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='OpperationsSupport', @NewMenuItemText='Operations &amp; Implementation Support', @AfterMenuItemCode='Implementation', @Icon='fa fa-fw fa-compass'
GO
--Operations and Implementation Support >> Procurement
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Procurement', @ParentMenuItemCode='OpperationsSupport'
GO
--Operations and Implementation Support >> Procurement >>	Purchase Requests
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PurchaseRequestList', @ParentMenuItemCode='Procurement'
GO
--Operations and Implementation Support >> Procurement >>	Licenses
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LicenseList', @ParentMenuItemCode='Procurement', @AfterMenuItemCode='PurchaseRequestList'
GO
--Operations and Implementation Support >> Procurement >>	Licensed Equipment
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LicenseEquipmentCatalogList', @ParentMenuItemCode='Procurement', @AfterMenuItemCode='LicenseList'
GO
--Operations and Implementation Support >> Activities
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Activity', @ParentMenuItemCode='OpperationsSupport', @AfterMenuItemCode='Procurement'
GO
--Operations and Implementation Support >> Activities >>	Activity Management
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNote', @ParentMenuItemCode='Activity'
GO
--Operations and Implementation Support >> Activities >>	Activity Vetting
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactVettingList', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNote'
GO

--Monitoring & Evaluation
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LogicalFramework', @AfterMenuItemCode='OpperationsSupport', @Icon='fa fa-fw fa-heartbeat'
GO
--Monitoring & Evaluation >>	Overview
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveOverview', @ParentMenuItemCode='LogicalFramework'
GO
--Monitoring & Evaluation >>	Objectives
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ObjectiveList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='ObjectiveOverview'
GO
--Monitoring & Evaluation >>	Indicators
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='ObjectiveList'
GO
--Monitoring & Evaluation >>	Indicator Types
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IndicatorTypeList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='IndicatorList'
GO
--Monitoring & Evaluation >>	Milestones
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='MilestoneList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='IndicatorTypeList'
GO
--Monitoring & Evaluation >>	Program Report
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ProgramReportList', @ParentMenuItemCode='LogicalFramework', @AfterMenuItemCode='MilestoneList'
GO

--Admin
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Admin', @AfterMenuItemCode='LogicalFramework', @Icon='fa fa-fw fa-cogs'
GO
--Admin >> Event Log
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EventLogList', @ParentMenuItemCode='Admin'
GO
--Admin >> Users
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PersonList', @ParentMenuItemCode='Admin', @AfterMenuItemCode='EventLogList'
GO
--Admin >> Permission Templates
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PermissionableTemplateList', @ParentMenuItemCode='Admin', @AfterMenuItemCode='PersonList'
GO
--Admin >>	Email Templates
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EmailTemplateAdministrationList', @ParentMenuItemCode='Admin', @AfterMenuItemCode='PermissionableTemplateList'
GO
--Admin >>	Download RAP Data
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DownloadRAPData', @NewMenuItemLink='/rapdata/download', @NewMenuItemText='Download RAP Data', @ParentMenuItemCode='Admin', @AfterMenuItemCode='EmailTemplateAdministrationList'
GO
--Admin >> Server Setup Keys
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ServerSetup', @ParentMenuItemCode='Admin', @AfterMenuItemCode='DownloadRAPData'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Activity'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Atmospheric'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Contacts'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Equipment'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Procurement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RecommendationManagement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RiskManagement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPDelivery'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPImplementation'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPTools'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Stipends'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Surveys'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Training'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='OpperationsSupport'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPData'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO
--End table dbo.MenuItem

--Begin table dropdown.CountryCallingCode
IF NOT EXISTS (SELECT 1 FROM dropdown.CountryCallingCode CCC WHERE CCC.CountryCallingCodeID = 0)
	BEGIN
	
	SET IDENTITY_INSERT dropdown.CountryCallingCode ON
	
	INSERT INTO dropdown.CountryCallingCode (CountryCallingCodeID) VALUES (0)

	SET IDENTITY_INSERT dropdown.CountryCallingCode OFF
	
	END
--ENDIF
GO
--End table dropdown.CountryCallingCode

--Begin table dropdown.RiskStatus
UPDATE dropdown.RiskStatus
SET IsActive = 0
WHERE RiskStatusName = 'Activity Specific'
GO
--End table dropdown.RiskStatus

--Begin table dropdown.VettingOutcome
UPDATE VO
SET VO.HexColor =
	CASE
		WHEN VO.VettingOutcomeName = 'Not Vetted'
		THEN '#757575'
		WHEN VO.VettingOutcomeName = 'Pending Internal Review'
		THEN '#FCEB1F'
		WHEN VO.VettingOutcomeName = 'Consider'
		THEN '#32AC41'
		WHEN VO.VettingOutcomeName = 'Do Not Consider'
		THEN '#EA1921'
		WHEN VO.VettingOutcomeName = 'Insufficient Data'
		THEN '#757575'
		WHEN VO.VettingOutcomeName = 'Submitted for Vetting'
		THEN '#FCEB1F'
		ELSE VO.HexColor
	END
FROM dropdown.VettingOutcome VO
GO
--End table dropdown.VettingOutcome

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityAsset')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('CommunityAsset','Community Assets', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Finding')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Finding','Findings', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Project')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Project','Projects', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Recommendation')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Recommendation','Recommendations', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DownloadRAPData')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES 
		(0, 'DownloadRAPData', 'Download RAP Data')
		
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'ExportVetting',
	'Export Vetting',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'ConceptNote.VettingList'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNote.VettingList.ExportVetting'
		)
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CommunityAsset')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'CommunityAsset','Community Assets')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'CommunityAsset'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'CommunityAsset'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'CommunityAsset'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Finding')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Finding','Findings')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Finding'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Finding'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Finding'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Project')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Project','Projects')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Project'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Project'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Project'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Recommendation')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Recommendation','Recommendations')
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdate',
		'Add / Edit'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Recommendation'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'List',
		'List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Recommendation'
		
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'View',
		'View'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'Recommendation'

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Finding'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Finding')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'CommunityAsset'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('CommunityAsset')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Project'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Project')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Recommendation'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Recommendation')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'RecommendationUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('RecommendationUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RecommendationUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RecommendationUpdate'),
		WSWA.WorkflowStepNumber,
		
		CASE
			WHEN WA1.WorkflowActionCode = 'IncrementWorkflowAndUploadFile'
			THEN (SELECT WA2.WorkflowActionID FROM workflow.WorkflowAction WA2 WHERE WA2.WorkflowActionCode = 'IncrementWorkflow' AND WA2.WorkflowActionName = 'Approve')
			ELSE WSWA.WorkflowActionID
		END,
		
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'RiskUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('RiskUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RiskUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'RiskUpdate'),
		WSWA.WorkflowStepNumber,
		
		CASE
			WHEN WA1.WorkflowActionCode = 'IncrementWorkflowAndUploadFile'
			THEN (SELECT WA2.WorkflowActionID FROM workflow.WorkflowAction WA2 WHERE WA2.WorkflowActionCode = 'IncrementWorkflow' AND WA2.WorkflowActionName = 'Approve')
			ELSE WSWA.WorkflowActionID
		END,
		
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'WeeklyReport'
	
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table permissionable.Permissionable
DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'RecommendationUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'RecommendationUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Recommendation.AddUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'RiskUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'RiskUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Risk.AddUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
