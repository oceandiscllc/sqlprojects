USE AJACSUtility
GO

--Begin table dbo.Number
IF NOT EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = 'dbo.Number')
	BEGIN

	CREATE TABLE dbo.Number
		(
		Number INT
		)
	
	;
	WITH 
		Tx1(N) AS (SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL  SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1)
		, Tx2(N) AS (SELECT 1 FROM Tx1 CROSS JOIN Tx1 AS b)
		, Tx4(N) AS (SELECT 1 FROM Tx2 CROSS JOIN Tx2 AS b)
		, Tx8(N) AS (SELECT 1 FROM Tx4 CROSS JOIN Tx4 AS b)

	INSERT INTO dbo.Number
		(Number)
	SELECT ROW_NUMBER() OVER (ORDER BY Tx8.N) 
	FROM Tx8
	ORDER BY Tx8.N

	END
--ENDIF
GO	
--End table dbo.Number

USE AJACS
GO
--Begin table dbo.CommunityAsset
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAsset'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAsset
	(
	CommunityAssetID INT IDENTITY(1,1),
	CommunityAssetName NVARCHAR(250),
	CommunityAssetDescription NVARCHAR(MAX),
	CommunityAssetTypeID INT,
	AssetTypeID INT,
	ZoneTypeID INT,
	Location GEOGRAPHY,
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ZoneTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetID'
GO
--Begin table dbo.CommunityAsset

--Begin table dbo.CommunityAssetCommunity
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetCommunity
	(
	CommunityAssetCommunityID INT IDENTITY(1,1),
	CommunityAssetID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetCommunityID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetCommunity', @TableName, 'CommunityAssetID,CommunityID'
GO
--Begin table dbo.CommunityAsset

--Begin table dbo.CommunityAssetProvince
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetProvince'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetProvince
	(
	CommunityAssetProvinceID INT IDENTITY(1,1),
	CommunityAssetID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetProvinceID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetProvince', @TableName, 'CommunityAssetID,ProvinceID'
GO
--Begin table dbo.CommunityAssetProvince

--Begin table dbo.CommunityAssetRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetRisk'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetRisk
	(
	CommunityAssetRiskID INT IDENTITY(1,1),
	CommunityAssetID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetRisk', @TableName, 'CommunityAssetID,RiskID'
GO
--Begin table dbo.CommunityAssetRisk

--Begin table dbo.CommunityAssetUnit
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetUnit'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetUnit
	(
	CommunityAssetUnitID INT IDENTITY(1,1),
	CommunityAssetID INT,
	CommunityAssetUnitName NVARCHAR(250),
	CommunityAssetUnitTypeID INT,
	CommunityAssetUnitCostRateID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitCostRateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetUnitID'
EXEC utility.SetIndexClustered 'IX_CommunityAssetUnit', @TableName, 'CommunityAssetID,CommunityAssetUnitName'
GO
--Begin table dbo.CommunityAssetUnit

--Begin table dbo.ConceptNoteContact
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContact'

EXEC utility.AddColumn @TableName, 'Notes', 'NVARCHAR(MAX)'
GO
--End table dbo.ConceptNoteContact

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ArabicMotherName', 'NVARCHAR(200)'
EXEC utility.AddColumn @TableName, 'CellPhoneNumberCountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'FaxNumberCountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'IsValid', 'BIT'
EXEC utility.AddColumn @TableName, 'Notes', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'PhoneNumberCountryCallingCodeID', 'INT'
EXEC utility.AddColumn @TableName, 'PlaceOfBirthCountryID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CellPhoneNumberCountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FaxNumberCountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsValid', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'PhoneNumberCountryCallingCodeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PlaceOfBirthCountryID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.Risk / riskupdate.Risk
DECLARE @TableName VARCHAR(250) = 'dbo.Risk'

EXEC utility.AddColumn @TableName, 'RiskTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'RiskTypeID', 'INT', 0
GO

DECLARE @TableName VARCHAR(250) = 'riskupdate.Risk'

EXEC utility.AddColumn @TableName, 'RiskTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'RiskTypeID', 'INT', 0
GO
--End table dbo.Risk / riskupdate.Risk

--Begin table dropdown.AssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.AssetType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AssetType
	(
	AssetTypeID INT IDENTITY(0,1) NOT NULL,
	AssetTypeName VARCHAR(50),
	Icon VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AssetTypeID'
EXEC utility.SetIndexNonClustered 'IX_AssetType', @TableName, 'DisplayOrder,AssetTypeName', 'AssetTypeID'
GO

SET IDENTITY_INSERT dropdown.AssetType ON
GO

INSERT INTO dropdown.AssetType (AssetTypeID, DisplayOrder) VALUES (0, 1)

SET IDENTITY_INSERT dropdown.AssetType OFF
GO

INSERT INTO dropdown.AssetType 
	(AssetTypeName,Icon,DisplayOrder)
VALUES
	('Police Station', 'asset-police-station.png', 1),
	('Court', 'asset-court.png', 2),
	('Prison', 'asset-prison.png', 3),
	('Community Building', 'asset-community-building.png', 4),
	('Document Centre', 'asset-document-centre.png', 5),
	('Other', 'asset-other.png', 6)
GO
--End table dropdown.AssetType

--Begin table dropdown.CommunityAssetType
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityAssetType
	(
	CommunityAssetTypeID INT IDENTITY(0,1) NOT NULL,
	CommunityAssetTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetTypeID'
EXEC utility.SetIndexNonClustered 'IX_CommunityAssetType', @TableName, 'DisplayOrder,CommunityAssetTypeName', 'CommunityAssetTypeID'
GO

SET IDENTITY_INSERT dropdown.CommunityAssetType ON
GO

INSERT INTO dropdown.CommunityAssetType (CommunityAssetTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityAssetType OFF
GO

INSERT INTO dropdown.CommunityAssetType 
	(CommunityAssetTypeName,DisplayOrder)
VALUES
	('Asset', 1),
	('Zone', 2)
GO
--End table dropdown.CommunityAssetType

--Begin table dropdown.CommunityAssetUnitCostRate
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetUnitCostRate'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityAssetUnitCostRate
	(
	CommunityAssetUnitCostRateID INT IDENTITY(0,1) NOT NULL,
	CommunityAssetUnitCostRate INT,
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitCostRate', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetUnitCostRateID'
EXEC utility.SetIndexNonClustered 'IX_CommunityAssetUnitCostRate', @TableName, 'DisplayOrder,CommunityAssetUnitCostRate', 'CommunityAssetUnitCostRateID'
GO

SET IDENTITY_INSERT dropdown.CommunityAssetUnitCostRate ON
GO

INSERT INTO dropdown.CommunityAssetUnitCostRate (CommunityAssetUnitCostRateID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityAssetUnitCostRate OFF
GO

INSERT INTO dropdown.CommunityAssetUnitCostRate 
	(CommunityAssetUnitCostRate, DisplayOrder)
VALUES
	(0, 1),
	(500, 2),
	(3000, 3)
GO
--End table dropdown.CommunityAssetUnitCostRate

--Begin table dropdown.CommunityAssetUnitType
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityAssetUnitType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityAssetUnitType
	(
	CommunityAssetUnitTypeID INT IDENTITY(0,1) NOT NULL,
	CommunityAssetUnitTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityAssetUnitTypeID'
EXEC utility.SetIndexNonClustered 'IX_CommunityAssetUnitType', @TableName, 'DisplayOrder,CommunityAssetUnitTypeName', 'CommunityAssetUnitTypeID'
GO

SET IDENTITY_INSERT dropdown.CommunityAssetUnitType ON
GO

INSERT INTO dropdown.CommunityAssetUnitType (CommunityAssetUnitTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.CommunityAssetUnitType OFF
GO

INSERT INTO dropdown.CommunityAssetUnitType 
	(CommunityAssetUnitTypeName, DisplayOrder)
VALUES
	('Police', 1),
	('Community',2),
	('Justice', 3)
GO
--End table dropdown.CommunityAssetUnitType

--Begin table dropdown.Component
DECLARE @TableName VARCHAR(250) = 'dropdown.Component'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Component
	(
	ComponentID INT IDENTITY(0,1) NOT NULL,
	ComponentCode VARCHAR(50),
	ComponentAbbreviation VARCHAR(15),
	ComponentName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ComponentID'
EXEC utility.SetIndexNonClustered 'IX_Component', @TableName, 'DisplayOrder,ComponentName', 'ComponentID'
GO

SET IDENTITY_INSERT dropdown.Component ON
GO

INSERT INTO dropdown.Component (ComponentID) VALUES (0)

SET IDENTITY_INSERT dropdown.Component OFF
GO

INSERT INTO dropdown.Component 
	(ComponentName,ComponentAbbreviation,DisplayOrder)
VALUES
	('Community Engagement', 'CE', 1),
	('Police', 'POL', 2),
	('Justice', 'JUS', 3),
	('Integrated Legitimate Structures', 'ILS', 4)
GO	
--End table dropdown.Component

--Begin table dropdown.FindingStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.FindingStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FindingStatus
	(
	FindingStatusID INT IDENTITY(0,1) NOT NULL,
	FindingStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FindingStatusID'
EXEC utility.SetIndexNonClustered 'IX_FindingStatus', @TableName, 'DisplayOrder,FindingStatusName', 'FindingStatusID'
GO

SET IDENTITY_INSERT dropdown.FindingStatus ON
GO

INSERT INTO dropdown.FindingStatus (FindingStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.FindingStatus OFF
GO

INSERT INTO dropdown.FindingStatus 
	(FindingStatusName,DisplayOrder)
VALUES
	('Pending', 1),
	('Current', 2),
	('Historic', 3)
GO	
--End table dropdown.FindingStatus

--Begin table dropdown.FindingType
DECLARE @TableName VARCHAR(250) = 'dropdown.FindingType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FindingType
	(
	FindingTypeID INT IDENTITY(0,1) NOT NULL,
	FindingTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FindingTypeID'
EXEC utility.SetIndexNonClustered 'IX_FindingType', @TableName, 'DisplayOrder,FindingTypeName', 'FindingTypeID'
GO

SET IDENTITY_INSERT dropdown.FindingType ON
GO

INSERT INTO dropdown.FindingType (FindingTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.FindingType OFF
GO

INSERT INTO dropdown.FindingType 
	(FindingTypeName,DisplayOrder)
VALUES
	('RAP', 1),
	('Community Engagement', 2),
	('Police', 3),
	('Integrated Legitimate Structures', 4),
	('Justice', 5),
	('FIF', 6),
	('Future Consideration', 7)
GO	
--End table dropdown.FindingType

--Begin table dropdown.ProjectStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProjectStatus
	(
	ProjectStatusID INT IDENTITY(0,1) NOT NULL,
	ProjectStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectStatusID'
EXEC utility.SetIndexNonClustered 'IX_ProjectStatus', @TableName, 'DisplayOrder,ProjectStatusName', 'ProjectStatusID'
GO

SET IDENTITY_INSERT dropdown.ProjectStatus ON
GO

INSERT INTO dropdown.ProjectStatus (ProjectStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.ProjectStatus OFF
GO

INSERT INTO dropdown.ProjectStatus 
	(ProjectStatusName,DisplayOrder)
VALUES
	('Planning', 1),
	('Implementation', 2),
	('Close Down', 3),
	('Closed', 4),
	('Cancelled', 5), 
	('Suspended', 6)
GO	
--End table dropdown.ProjectStatus

--Begin table dropdown.RiskType
DECLARE @TableName VARCHAR(250) = 'dropdown.RiskType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.RiskType
	(
	RiskTypeID INT IDENTITY(0,1) NOT NULL,
	RiskTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskTypeID'
EXEC utility.SetIndexNonClustered 'IX_RiskType', @TableName, 'DisplayOrder,RiskTypeName', 'RiskTypeID'
GO

SET IDENTITY_INSERT dropdown.RiskType ON
GO

INSERT INTO dropdown.RiskType (RiskTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.RiskType OFF
GO

INSERT INTO dropdown.RiskType 
	(RiskTypeName)
VALUES
	('Activity Specific'),
	('Community Engagement Specific'),
	('Component For Specific'),
	('Finding/Recommendation Specific'),
	('Justice Specific'),
	('Police Specific'),
	('Programme'),
	('Project Specific'),
	('Recommendation Specific')
GO	
--End table dropdown.RiskType

--Begin table dropdown.ZoneType
DECLARE @TableName VARCHAR(250) = 'dropdown.ZoneType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ZoneType
	(
	ZoneTypeID INT IDENTITY(0,1) NOT NULL,
	ZoneTypeName VARCHAR(50),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ZoneTypeID'
EXEC utility.SetIndexNonClustered 'IX_ZoneType', @TableName, 'DisplayOrder,ZoneTypeName', 'ZoneTypeID'
GO

SET IDENTITY_INSERT dropdown.ZoneType ON
GO

INSERT INTO dropdown.ZoneType (ZoneTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ZoneType OFF
GO

INSERT INTO dropdown.ZoneType 
	(ZoneTypeName, HexColor, DisplayOrder)
VALUES
	('FSP Area of Control', '#1b9e77', 1),
	('Islamic Extremist Group Area of Control', '#d95f02', 2),
	('Regime Area of Control', '#7570b3', 3),
	('Crime Hotspot', '#e7298a', 4)
GO
--End table dropdown.ZoneType

--Begin table finding.Finding
DECLARE @TableName VARCHAR(250) = 'finding.Finding'

EXEC utility.DropObject @TableName

CREATE TABLE finding.Finding
	(
	FindingID INT IDENTITY(1,1) NOT NULL,
	FindingName VARCHAR(250),
	FindingDescription VARCHAR(MAX),
	FindingTypeID INT,
	FindingStatusID INT,
	IsResearchElement BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsResearchElement', 'BIT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'FindingID'
GO
--End table finding.Finding

--Begin table finding.FindingCommunity
DECLARE @TableName VARCHAR(250) = 'finding.FindingCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingCommunity
	(
	FindingCommunityID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingCommunityID'
EXEC utility.SetIndexClustered 'IX_FindingCommunity', @TableName, 'FindingID,CommunityID'
GO
--End table finding.FindingCommunity

--Begin table finding.FindingComponent
DECLARE @TableName VARCHAR(250) = 'finding.FindingComponent'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingComponent
	(
	FindingComponentID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	ComponentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingComponentID'
EXEC utility.SetIndexClustered 'IX_FindingComponent', @TableName, 'FindingID,ComponentID'
GO
--End table finding.FindingComponent

--Begin table finding.FindingIndicator
DECLARE @TableName VARCHAR(250) = 'finding.FindingIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingIndicator
	(
	FindingIndicatorID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingIndicatorID'
EXEC utility.SetIndexClustered 'IX_FindingIndicator', @TableName, 'FindingID,IndicatorID'
GO
--End table finding.FindingIndicator

--Begin table finding.FindingProvince
DECLARE @TableName VARCHAR(250) = 'finding.FindingProvince'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingProvince
	(
	FindingProvinceID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingProvinceID'
EXEC utility.SetIndexClustered 'IX_FindingProvince', @TableName, 'FindingID,ProvinceID'
GO
--End table finding.FindingProvince

--Begin table finding.FindingRecommendation
DECLARE @TableName VARCHAR(250) = 'finding.FindingRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingRecommendation
	(
	FindingRecommendationID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	RecommendationID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingRecommendationID'
EXEC utility.SetIndexClustered 'IX_FindingRecommendation', @TableName, 'FindingID,RecommendationID'
GO
--End table finding.FindingRecommendation

--Begin table finding.FindingRisk
DECLARE @TableName VARCHAR(250) = 'finding.FindingRisk'

EXEC utility.DropObject @TableName

CREATE TABLE finding.FindingRisk
	(
	FindingRiskID INT IDENTITY(1,1) NOT NULL,
	FindingID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'FindingRiskID'
EXEC utility.SetIndexClustered 'IX_FindingRisk', @TableName, 'FindingID,RiskID'
GO
--End table finding.FindingRisk

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table logicalframework.Indicator

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table logicalframework.Milestone

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table logicalframework.Objective

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.DropObject @TableName

CREATE TABLE project.Project
	(
	ProjectID INT IDENTITY(1,1) NOT NULL,
	ProjectName VARCHAR(250),
	ProjectDescription VARCHAR(MAX),
	ProjectSummary VARCHAR(MAX),
	ProjectPartner VARCHAR(250),
	ProjectValue NUMERIC(18,2),
	ConceptNoteID INT,
	ProjectStatusID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectValue', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProjectID'
GO
--End table project.Project

--Begin table project.ProjectCommunity
DECLARE @TableName VARCHAR(250) = 'project.ProjectCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectCommunity
	(
	ProjectCommunityID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	CommunityID INT,
	ProjectCommunityValue NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectCommunityValue', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectCommunityID'
EXEC utility.SetIndexClustered 'IX_ProjectCommunity', @TableName, 'ProjectID,CommunityID'
GO
--End table project.ProjectCommunity

--Begin table project.ProjectFinding
DECLARE @TableName VARCHAR(250) = 'project.ProjectFinding'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectFinding
	(
	ProjectFindingID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	FindingID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'FindingID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectFindingID'
EXEC utility.SetIndexClustered 'IX_ProjectFinding', @TableName, 'ProjectID,FindingID'
GO
--End table project.ProjectFinding

--Begin table project.ProjectIndicator
DECLARE @TableName VARCHAR(250) = 'project.ProjectIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectIndicator
	(
	ProjectIndicatorID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProjectIndicator', @TableName, 'ProjectID,IndicatorID'
GO
--End table project.ProjectIndicator

--Begin table project.ProjectProvince
DECLARE @TableName VARCHAR(250) = 'project.ProjectProvince'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectProvince
	(
	ProjectProvinceID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	ProvinceID INT,
	ProjectProvinceValue NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProjectProvinceValue', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectProvinceID'
EXEC utility.SetIndexClustered 'IX_ProjectProvince', @TableName, 'ProjectID,ProvinceID'
GO
--End table project.ProjectProvince

--Begin table project.ProjectRecommendation
DECLARE @TableName VARCHAR(250) = 'project.ProjectRecommendation'

EXEC utility.DropObject @TableName

CREATE TABLE project.ProjectRecommendation
	(
	ProjectRecommendationID INT IDENTITY(1,1) NOT NULL,
	ProjectID INT,
	RecommendationID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProjectID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProjectRecommendationID'
EXEC utility.SetIndexClustered 'IX_ProjectRecommendation', @TableName, 'ProjectID,RecommendationID'
GO
--End table project.ProjectRecommendation

--Begin table recommendation.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendation.Recommendation'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.Recommendation
	(
	RecommendationID INT IDENTITY(1,1) NOT NULL,
	RecommendationName VARCHAR(250),
	RecommendationDescription VARCHAR(MAX),
	IsResearchElement BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsResearchElement', 'BIT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationID'
GO
--End table recommendation.Recommendation

--Begin table recommendation.RecommendationCommunity
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationCommunity
	(
	RecommendationCommunityID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationCommunityID'
EXEC utility.SetIndexClustered 'IX_RecommendationCommunity', @TableName, 'RecommendationID,CommunityID'
GO
--End table recommendation.RecommendationCommunity

--Begin table recommendation.RecommendationComponent
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationComponent'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationComponent
	(
	RecommendationComponentID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ComponentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationComponentID'
EXEC utility.SetIndexClustered 'IX_RecommendationComponent', @TableName, 'RecommendationID,ComponentID'
GO
--End table recommendation.RecommendationComponent

--Begin table recommendation.RecommendationIndicator
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationIndicator
	(
	RecommendationIndicatorID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	IndicatorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationIndicatorID'
EXEC utility.SetIndexClustered 'IX_RecommendationIndicator', @TableName, 'RecommendationID,IndicatorID'
GO
--End table recommendation.RecommendationIndicator

--Begin table recommendation.RecommendationProvince
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationProvince'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationProvince
	(
	RecommendationProvinceID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationProvinceID'
EXEC utility.SetIndexClustered 'IX_RecommendationProvince', @TableName, 'RecommendationID,ProvinceID'
GO
--End table recommendation.RecommendationProvince

--Begin table recommendation.RecommendationRisk
DECLARE @TableName VARCHAR(250) = 'recommendation.RecommendationRisk'

EXEC utility.DropObject @TableName

CREATE TABLE recommendation.RecommendationRisk
	(
	RecommendationRiskID INT IDENTITY(1,1) NOT NULL,
	RecommendationID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'RecommendationRiskID'
EXEC utility.SetIndexClustered 'IX_RecommendationRisk', @TableName, 'RecommendationID,RiskID'
GO
--End table recommendation.RecommendationRisk

--Begin table recommendationupdate.Recommendation
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.Recommendation'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.Recommendation
	(
	RecommendationID INT,
	RecommendationUpdateID INT,
	RecommendationName VARCHAR(250),
	RecommendationDescription VARCHAR(MAX),
	IsResearchElement BIT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'IsResearchElement', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RecommendationUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationID'
GO
--End table recommendationupdate.Recommendation

--Begin table recommendationupdate.RecommendationUpdate
DECLARE @TableName VARCHAR(250) = 'recommendationupdate.RecommendationUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE recommendationupdate.RecommendationUpdate
	(
	RecommendationUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RecommendationUpdateID'
GO
--End table recommendationupdate.RecommendationUpdate

--Begin table riskupdate.Risk
DECLARE @TableName VARCHAR(250) = 'riskupdate.Risk'

EXEC utility.DropObject @TableName

CREATE TABLE riskupdate.Risk
	(
	RiskID INT,
	RiskUpdateID INT,
	RiskName VARCHAR(250),
	RiskDescription VARCHAR(MAX),
	RiskCategoryID INT,
	DateRaised DATE,
	RaisedBy VARCHAR(100),
	Likelihood INT,
	Impact INT,
	Overall INT,
	MitigationActions VARCHAR(MAX),
	EscalationCriteria VARCHAR(MAX),
	Notes VARCHAR(MAX),
	RiskStatusID INT,
	RiskTypeID INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'Impact', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Likelihood', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Overall', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskCategoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskID'
GO
--End table riskupdate.Risk

--Begin table riskupdate.RiskUpdate
DECLARE @TableName VARCHAR(250) = 'riskupdate.RiskUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE riskupdate.RiskUpdate
	(
	RiskUpdateID INT IDENTITY(1,1),
	WorkflowStepNumber INT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'RiskUpdateID'
GO
--End table riskupdate.RiskUpdate