USE AJACS
GO

--Begin table dbo.ContactVetting
DECLARE @TableName VARCHAR(250) = 'dbo.ContactVetting'

EXEC utility.DropIndex @TableName, 'IX_ContactVetting'
EXEC utility.SetIndexClustered @TableName, 'IX_ContactVetting', 'ContactVettingTypeID,ContactID,VettingOutcomeID,VettingDate'
GO
--End table dbo.ContactVetting

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'StipendFundingSourceID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'StipendFundingSourceID', 'INT', '0'
GO

UPDATE dbo.Province
SET StipendFundingSourceID = 
	CASE
		WHEN ProvinceName = 'IdLib'
		THEN (SELECT TOP 1 FS.FundingSourceID FROM dropdown.FundingSource FS WHERE FS.FundingSourceCode = 'US' AND FS.IsActive = 1)
		ELSE (SELECT TOP 1 FS.FundingSourceID FROM dropdown.FundingSource FS WHERE FS.FundingSourceCode = 'EU' AND FS.IsActive = 1)
	END
GO
--End table dbo.Province

--Begin table dropdown.FundingSource
DECLARE @TableName VARCHAR(250) = 'dropdown.FundingSource'

EXEC utility.AddColumn @TableName, 'FundingSourceCode', 'VARCHAR(50)'
GO

UPDATE dropdown.FundingSource
SET FundingSourceCode = 
	CASE
		WHEN LEFT(FundingSourceName, 2) = 'EU'
		THEN 'EU'
		WHEN LEFT(FundingSourceName, 2) = 'US'
		THEN 'US'
	END
GO
--End table dropdown.FundingSource

--Begin table procurement.DistributedInventory
DECLARE @TableName VARCHAR(250) = 'procurement.DistributedInventory'

EXEC utility.AddColumn @TableName, 'TransferredFromDistributedInventoryID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'TransferredFromDistributedInventoryID', 'INT', 0
GO
--End table procurement.DistributedInventory

/*
--Begin table dropdown.Stipend
IF NOT EXISTS (SELECT 1 FROM dropdown.Stipend S WHERE S.StipendName = 'Commissioned Officer')
	BEGIN

	INSERT INTO dropdown.Stipend
		(StipendAmount,StipendName,StipendTypeCode)
	VALUES
		(300,'Commissioned Officer','PoliceStipend'),
		(150,'Non Commissioned Officer','PoliceStipend')

	END
--ENDIF

UPDATE S
SET S.StipendName = 'Police'
FROM dropdown.Stipend S
WHERE S.StipendName = 'Policeman'
GO

UPDATE S
SET S.IsActive = 0
FROM dropdown.Stipend S
WHERE S.StipendTypeCode = 'PoliceStipend'
	AND S.StipendID > 0
	AND S.StipendName NOT IN ('Command','Commissioned Officer','Non Commissioned Officer','Police')
GO
--End table dropdown.Stipend

--Begin table dbo.Contact
UPDATE C
SET C.StipendID = 
	CASE
		WHEN S1.StipendName IN ('General','Colonel','Colonel Doctor','Lieutenant Colonel','Major','Captain','Captain Doctor','First Lieutenant','Contracted Officer')
		THEN (SELECT S2.StipendID FROM dropdown.Stipend S2 WHERE S2.StipendName = 'Commissioned Officer')
		WHEN S1.StipendName IN ('First Sergeant','Sergeant','First Adjutant','Adjutant')
		THEN (SELECT S2.StipendID FROM dropdown.Stipend S2 WHERE S2.StipendName = 'Non Commissioned Officer')
		ELSE C.StipendID
	END
FROM dbo.Contact C
	JOIN dropdown.Stipend S1 ON S1.StipendID = C.StipendID
GO
--End table dbo.Contact

--Begin table reporting.StipendPayment
DECLARE @TableName VARCHAR(250) = 'reporting.StipendPayment'

EXEC utility.DropConstraintsAndIndexes @TableName

EXEC utility.AddColumn @TableName, 'Commissioned Officer', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Commissioned Officer Count', 'INT'
EXEC utility.AddColumn @TableName, 'Non Commissioned Officer', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Non Commissioned Officer Count', 'INT'
EXEC utility.AddColumn @TableName, 'Police', 'NUMERIC(18, 0)'
EXEC utility.AddColumn @TableName, 'Police Count', 'INT'

EXEC utility.DropColumn @TableName, 'Adjutant'
EXEC utility.DropColumn @TableName, 'Adjutant Count'
EXEC utility.DropColumn @TableName, 'Captain'
EXEC utility.DropColumn @TableName, 'Captain Count'
EXEC utility.DropColumn @TableName, 'Captain Doctor'
EXEC utility.DropColumn @TableName, 'Captain Doctor Count'
EXEC utility.DropColumn @TableName, 'Colonel'
EXEC utility.DropColumn @TableName, 'Colonel Count'
EXEC utility.DropColumn @TableName, 'Colonel Doctor'
EXEC utility.DropColumn @TableName, 'Colonel Doctor Count'
EXEC utility.DropColumn @TableName, 'Contracted Officer'
EXEC utility.DropColumn @TableName, 'Contracted Officer Count'
EXEC utility.DropColumn @TableName, 'Contracted Policeman'
EXEC utility.DropColumn @TableName, 'Contracted Policeman Count'
EXEC utility.DropColumn @TableName, 'First Adjutant'
EXEC utility.DropColumn @TableName, 'First Adjutant Count'
EXEC utility.DropColumn @TableName, 'First Lieutenant'
EXEC utility.DropColumn @TableName, 'First Lieutenant Count'
EXEC utility.DropColumn @TableName, 'First Sergeant'
EXEC utility.DropColumn @TableName, 'First Sergeant Count'
EXEC utility.DropColumn @TableName, 'General'
EXEC utility.DropColumn @TableName, 'General Count'
EXEC utility.DropColumn @TableName, 'Lieutenant Colonel'
EXEC utility.DropColumn @TableName, 'Lieutenant Colonel Count'
EXEC utility.DropColumn @TableName, 'Major'
EXEC utility.DropColumn @TableName, 'Major Count'
EXEC utility.DropColumn @TableName, 'Policeman'
EXEC utility.DropColumn @TableName, 'Policeman Count'
EXEC utility.DropColumn @TableName, 'Sergeant'
EXEC utility.DropColumn @TableName, 'Sergeant Count'

EXEC utility.SetDefaultConstraint @TableName, 'Command Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Command', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Commissioned Officer Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Commissioned Officer', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Non Commissioned Officer Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Non Commissioned Officer', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Police Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Police', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 1 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 1', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 2 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 2', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 3 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 3', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 4 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 4', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 5 Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Rank 5', 'NUMERIC(18, 0)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Total Count', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Total Stipend', 'NUMERIC(18, 0)', 0

EXEC utility.SetIndexClustered @TableName, 'IX_StipendPayment', 'PersonID,StipendPaymentID'
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'StipendPaymentID'
GO
--End table reporting.StipendPayment
*/