USE AJACS
GO

--Begin function asset.GetAssetUnitEquipmentEligibility
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit Equipment eligibility data
-- =================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibility
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND AU.AssetUnitID = @AssetUnitID

	IF ISNULL(@nIsCompliant, 0) = 4
		SET @bReturn = 1
	--ENDIF

	IF @bReturn = 1 AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Contact C
			JOIN asset.AssetUnit AU ON AU.CommanderContactID = C.ContactID
				AND AU.AssetUnitID = @AssetUnitID
				AND 
					(
						(
						@cFundingSourceCode = 'EU'
							AND C.UKVettingExpirationDate IS NOT NULL
							AND C.UKVettingExpirationDate >= getDate()
							AND EXISTS
								(
								SELECT 1
								FROM
									(
									SELECT 
										ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
										CV.ContactID
									FROM dbo.ContactVetting CV
										JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
											AND CVT.ContactVettingTypeCode = 'US'
										JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
											AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
									) D
								WHERE D.RowIndex = 1
									AND D.ContactID = C.ContactID
								)
						)
					OR
						( 									
						@cFundingSourceCode = 'US'
							AND C.USVettingExpirationDate IS NOT NULL
							AND C.USVettingExpirationDate >= getDate()
						)
					)
		)
		SET @bReturn = 0
	--ENDIF

	RETURN @bReturn

END
GO
--End function asset.GetAssetUnitEquipmentEligibility

--Begin function asset.GetAssetUnitEquipmentEligibilityNotes
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit equipment eligibility data
-- ===================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibilityNotes
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL
	DECLARE @cFundingSourceCode VARCHAR(50)

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not equipment elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND AU.AssetUnitID = @AssetUnitID

	IF NOT EXISTS
		(
		SELECT 1
		FROM dbo.Contact C
			JOIN asset.AssetUnit AU ON AU.CommanderContactID = C.ContactID
				AND AU.AssetUnitID = @AssetUnitID
				AND 
					(
						(
						@cFundingSourceCode = 'EU'
							AND C.UKVettingExpirationDate IS NOT NULL
							AND C.UKVettingExpirationDate >= getDate()
							AND EXISTS
								(
								SELECT 1
								FROM
									(
									SELECT 
										ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
										CV.ContactID
									FROM dbo.ContactVetting CV
										JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
											AND CVT.ContactVettingTypeCode = 'US'
										JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
											AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
									) D
								WHERE D.RowIndex = 1
									AND D.ContactID = C.ContactID
								)
						)
					OR
						( 									
						@cFundingSourceCode = 'US'
							AND C.USVettingExpirationDate IS NOT NULL
							AND C.USVettingExpirationDate >= getDate()
						)
					)
		)
		SET @cReturn += ISNULL(@cReturn, '') + 'Assigned asset department head has expired vetting,'
	--ENDIF

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equipment Eligible'
	ELSE
		SET @cReturn = 'Equipment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function asset.GetAssetUnitEquipmentEligibilityNotes

--Begin function asset.GetAssetUnitStipendEligibilityByAssetUnitID
EXEC utility.DropObject 'asset.GetAssetUnitStipendEligibilityByAssetUnitID'
GO
--End function asset.GetAssetUnitStipendEligibilityByAssetUnitID

--Begin function asset.GetAssetUnitStipendEligibilityNotesByAssetUnitID
EXEC utility.DropObject 'asset.GetAssetUnitStipendEligibilityNotesByAssetUnitID'
GO
--End function asset.GetAssetUnitStipendEligibilityNotesByAssetUnitID

--Begin function dbo.GetContactEquipmentEligibility
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityByContactID'
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibility
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		CASE
			WHEN C.CommunityID > 0
			THEN dbo.IsCommunityStipendEligible(C.CommunityID)
			ELSE 
				(
				SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
				FROM asset.Asset A
					JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
						AND AU.AssetUnitID = C.AssetUnitID
				)
			END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 1
		SET @bReturn = 1
	--ENDIF

	IF @bReturn = 1 AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
			AND 
				(
					(
					@cFundingSourceCode = 'EU'
						AND C.UKVettingExpirationDate IS NOT NULL
						AND C.UKVettingExpirationDate >= getDate()
						AND EXISTS
							(
							SELECT 1
							FROM
								(
								SELECT 
									ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
									CV.ContactID
								FROM dbo.ContactVetting CV
									JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
										AND CVT.ContactVettingTypeCode = 'US'
									JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
										AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
								) D
							WHERE D.RowIndex = 1
								AND D.ContactID = C.ContactID
							)
					)
				OR
					( 									
					@cFundingSourceCode = 'US'
						AND C.USVettingExpirationDate IS NOT NULL
						AND C.USVettingExpirationDate >= getDate()
					)
				)
		)
		SET @bReturn = 0
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibility

--Begin function dbo.GetContactEquipmentEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityNotesByContactID'
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityNotes
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @cReturn =
		CASE
			WHEN C.CommunityID > 0
			THEN 
				CASE
					WHEN dbo.IsCommunityStipendEligible(C.CommunityID) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			WHEN C.AssetUnitID > 0
			THEN
				CASE
					WHEN 
						(
						SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
						FROM asset.Asset A
							JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
								AND AU.AssetUnitID = C.AssetUnitID
						) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF NOT EXISTS
		(
		SELECT 1
		FROM dbo.Contact C
		WHERE C.ContactID = @ContactID
			AND 
				(
					(
					@cFundingSourceCode = 'EU'
						AND C.UKVettingExpirationDate IS NOT NULL
						AND C.UKVettingExpirationDate >= getDate()
						AND EXISTS
							(
							SELECT 1
							FROM
								(
								SELECT 
									ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
									CV.ContactID
								FROM dbo.ContactVetting CV
									JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
										AND CVT.ContactVettingTypeCode = 'US'
									JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
										AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
								) D
							WHERE D.RowIndex = 1
								AND D.ContactID = C.ContactID
							)
					)
				OR
					( 									
					@cFundingSourceCode = 'US'
						AND C.USVettingExpirationDate IS NOT NULL
						AND C.USVettingExpirationDate >= getDate()
					)
				)
		)
		SET @cReturn += ISNULL(@cReturn, '') + 'Assigned asset department head has expired vetting,'
	--ENDIF

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equpiment Eligible'
	ELSE
		SET @cReturn = 'Equpiment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactEquipmentEligibilityNotes

--Begin function dbo.GetContactStipendEligibility
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityByContactID'
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibility
(
@ContactID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C2 ON C2.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = P.StipendFundingSourceID
			AND C1.ContactID = @ContactID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND C1.ContactID = @ContactID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID

	IF ISNULL(@nIsCompliant, 0) = 4
		SET @bReturn = 1
	--ENDIF

	IF @bReturn = 1 AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Contact C1
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
				AND C1.ContactID = @ContactID
				AND 
					(
						(
						@cFundingSourceCode = 'EU'
							AND C2.UKVettingExpirationDate IS NOT NULL
							AND C2.UKVettingExpirationDate >= getDate()
							AND EXISTS
								(
								SELECT 1
								FROM
									(
									SELECT 
										ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
										CV.ContactID
									FROM dbo.ContactVetting CV
										JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
											AND CVT.ContactVettingTypeCode = 'US'
										JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
											AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
									) D
								WHERE D.RowIndex = 1
									AND D.ContactID = C2.ContactID
								)
						)
					OR
						( 									
						@cFundingSourceCode = 'US'
							AND C2.USVettingExpirationDate IS NOT NULL
							AND C2.USVettingExpirationDate >= getDate()
						)
					)
		)
		SET @bReturn = 0
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibility

--Begin function dbo.GetContactStipendEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotesByContactID'
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotes
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C2 ON C2.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = P.StipendFundingSourceID
			AND C1.ContactID = @ContactID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
	FROM dbo.Contact C2
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C2.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND C2.ContactID = @ContactID

	IF NOT EXISTS
		(
		SELECT 1
		FROM dbo.Contact C1
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
				AND C1.ContactID = @ContactID
				AND 
					(
						(
						@cFundingSourceCode = 'EU'
							AND C2.UKVettingExpirationDate IS NOT NULL
							AND C2.UKVettingExpirationDate >= getDate()
							AND EXISTS
								(
								SELECT 1
								FROM
									(
									SELECT 
										ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
										CV.ContactID
									FROM dbo.ContactVetting CV
										JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
											AND CVT.ContactVettingTypeCode = 'US'
										JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
											AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
									) D
								WHERE D.RowIndex = 1
									AND D.ContactID = C2.ContactID
								)
						)
					OR
						( 									
						@cFundingSourceCode = 'US'
							AND C2.USVettingExpirationDate IS NOT NULL
							AND C2.USVettingExpirationDate >= getDate()
						)
					)
		)
		SET @cReturn += ISNULL(@cReturn, '') + 'Assigned asset department head has expired vetting,'
	--ENDIF

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotes

--Begin function dbo.IsAssetUnitEquipmentEligible
EXEC utility.DropObject 'dbo.IsAssetUnitEquipmentEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if an asset unit is equipment eligible
-- ===========================================================================

CREATE FUNCTION dbo.IsAssetUnitEquipmentEligible
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C1 ON C1.CommunityID = A.CommunityID
			AND AU.AssetUnitID = @AssetUnitID

	IF ISNULL(@nIsCompliant, 0) = 4
		SET @bReturn = 1
	--ENDIF

	IF @bReturn = 1 AND NOT EXISTS
		(
		SELECT 1
		FROM dbo.Contact C
			JOIN asset.AssetUnit AU ON AU.CommanderContactID = C.ContactID
				AND AU.AssetUnitID = @AssetUnitID
				AND 
					(
						(
						@cFundingSourceCode = 'EU'
							AND C.UKVettingExpirationDate IS NOT NULL
							AND C.UKVettingExpirationDate >= getDate()
							AND EXISTS
								(
								SELECT 1
								FROM
									(
									SELECT 
										ROW_NUMBER() OVER (PARTITION BY CV.ContactID ORDER BY CV.VettingDate DESC) AS RowIndex,
										CV.ContactID
									FROM dbo.ContactVetting CV
										JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
											AND CVT.ContactVettingTypeCode = 'US'
										JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
											AND VO.VettingOutcomeCode IN ('Consider','SubmittedforVetting')
									) D
								WHERE D.RowIndex = 1
									AND D.ContactID = C.ContactID
								)
						)
					OR
						( 									
						@cFundingSourceCode = 'US'
							AND C.USVettingExpirationDate IS NOT NULL
							AND C.USVettingExpirationDate >= getDate()
						)
					)
		)
		SET @bReturn = 0
	--ENDIF
	
	RETURN @bReturn

END
GO
--End function dbo.IsAssetUnitEquipmentEligible