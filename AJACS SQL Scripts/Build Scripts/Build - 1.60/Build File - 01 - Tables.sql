USE AJACS
GO

--Begin WorkflowStepNumber constraint reset
EXEC utility.SetDefaultConstraint 'dbo.ConceptNote', 'WorkflowStepNumber', 'INT', 0, 1
GO

EXEC utility.SetDefaultConstraint 'dbo.SpotReport', 'WorkflowStepNumber', 'INT', 0, 1
GO
--End WorkflowStepNumber constraint reset

--Begin table reporting.StipendPayment
DECLARE @TableName VARCHAR(250) = 'reporting.StipendPayment'

EXEC utility.AddColumn @TableName, '[Assistance Count]', 'INT'
EXEC utility.AddColumn @TableName, '[Assistance]', 'NUMERIC(18, 2)'
EXEC utility.AddColumn @TableName, '[Cleaner Count]', 'INT'
EXEC utility.AddColumn @TableName, '[Cleaner]', 'NUMERIC(18, 2)'
EXEC utility.AddColumn @TableName, '[Correspondent Count]', 'INT'
EXEC utility.AddColumn @TableName, '[Correspondent]', 'NUMERIC(18, 2)'
EXEC utility.AddColumn @TableName, '[Dep Manager Count]', 'INT'
EXEC utility.AddColumn @TableName, '[Dep Manager]', 'NUMERIC(18, 2)'
EXEC utility.AddColumn @TableName, '[Head Office / Center Count]', 'INT'
EXEC utility.AddColumn @TableName, '[Head Office / Center]', 'NUMERIC(18, 2)'
EXEC utility.AddColumn @TableName, '[Headquarters Management Count]', 'INT'
EXEC utility.AddColumn @TableName, '[Headquarters Management]', 'NUMERIC(18, 2)'
EXEC utility.AddColumn @TableName, '[Registrar Count]', 'INT'
EXEC utility.AddColumn @TableName, '[Registrar]', 'NUMERIC(18, 2)'

EXEC utility.SetDefaultConstraint @TableName, '[Assistance Count]', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, '[Assistance]', 'NUMERIC(18, 2)', 0
EXEC utility.SetDefaultConstraint @TableName, '[Cleaner Count]', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, '[Cleaner]', 'NUMERIC(18, 2)', 0
EXEC utility.SetDefaultConstraint @TableName, '[Correspondent Count]', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, '[Correspondent]', 'NUMERIC(18, 2)', 0
EXEC utility.SetDefaultConstraint @TableName, '[Dep Manager Count]', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, '[Dep Manager]', 'NUMERIC(18, 2)', 0
EXEC utility.SetDefaultConstraint @TableName, '[Head Office / Center Count]', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, '[Head Office / Center]', 'NUMERIC(18, 2)', 0
EXEC utility.SetDefaultConstraint @TableName, '[Headquarters Management Count]', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, '[Headquarters Management]', 'NUMERIC(18, 2)', 0
EXEC utility.SetDefaultConstraint @TableName, '[Registrar Count]', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, '[Registrar]', 'NUMERIC(18, 2)', 0
GO
--End table reporting.StipendPayment
