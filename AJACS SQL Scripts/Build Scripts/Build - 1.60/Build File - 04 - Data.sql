USE AJACS
GO

UPDATE CN
SET CN.WorkflowStepNumber = 0
FROM dbo.ConceptNote CN
WHERE EXISTS
	(
	SELECT 1
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = 'ConceptNote'
		AND EWSGP.EntityID = CN.ConceptNoteID
	)
GO

UPDATE CN
SET CN.WorkflowStepNumber = 11
FROM dbo.ConceptNote CN
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = 'ConceptNote'
		AND EWSGP.EntityID = CN.ConceptNoteID
	)
GO

UPDATE SR
SET SR.WorkflowStepNumber = 0
FROM dbo.SpotReport SR
WHERE EXISTS
	(
	SELECT 1
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = 'SpotReport'
		AND EWSGP.EntityID = SR.SpotReportID
	)
GO

UPDATE SR
SET SR.WorkflowStepNumber = 4
FROM dbo.SpotReport SR
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP 
	WHERE EWSGP.EntityTypeCode = 'SpotReport'
		AND EWSGP.EntityID = SR.SpotReportID
	)
GO
	