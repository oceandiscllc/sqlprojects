USE AJACS
GO

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,
		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,
		'Clone of:  ' + C.Title,
		C.VettingRequirements,
		1
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
	SELECT
		EWSGP.EntityTypeCode, 
		EWSGP.EntityID, 
		EWSGP.WorkflowID, 
		EWSGP.WorkflowStepID, 
		EWSGP.WorkflowStepGroupID, 
		EWSGP.WorkflowName, 
		EWSGP.WorkflowStepNumber, 
		EWSGP.WorkflowStepName, 
		EWSGP.WorkflowStepGroupName, 
		EWSGP.PersonID, 
		0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'ConceptNote'
		AND EWSGP.EntityID = @nConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO	dbo.ConceptNoteAmmendment
		(ConceptNoteID, AmmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Ammendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Ammendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote


--Begin procedure policeengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Community and policeengagementupdate.Community tables
--
-- Author:		Eric Jones
-- Create date:	2015.11.29
-- Description:	altered to allow pulling back of Field Activitied (aka projects) with POL component codes
-- =================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CapacityAssessmentDate,
		dbo.FormatDate(C.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.MaterialSupportStatus,
		C.PoliceEngagementOutput1,
		C.PoliceEngagementOutput2,
		C.PPPDate,
		dbo.FormatDate(C.PPPDate) AS PPPDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CapacityAssessmentDate,
		dbo.FormatDate(C1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C1.MaterialSupportStatus,
		C1.PoliceEngagementOutput1,
		C1.PoliceEngagementOutput2,
		C1.PPPDate,
		dbo.FormatDate(C1.PPPDate) AS PPPDateFormatted,
		C2.CommunityID,
		C2.CommunityName AS EntityName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.CommunityID = @CommunityID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.PoliceEngagementAchievedValue,
		OACI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.PoliceEngagementAchievedValue, 
				CI.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(ISNULL(OACP.ProjectCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PC.ProjectCommunityID
			FROM project.ProjectCommunity PC
			WHERE PC.ProjectID = P.ProjectID
				AND PC.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate (note that the button for current & update are the same as projects are read only)
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(ISNULL(OACP.ProjectCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PC.ProjectCommunityID
			FROM project.ProjectCommunity PC
			WHERE PC.ProjectID = P.ProjectID
				AND PC.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(ISNULL(OACR.RecommendationCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RC.RecommendationCommunityID
			FROM recommendation.RecommendationCommunity RC
			WHERE RC.RecommendationID = R.RecommendationID
				AND RC.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.PoliceEngagementRiskValue,
		OACR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID,
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.CommunityID = @CommunityID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID
		AND CSP.StipendTypeCode = 'PoliceStipend'

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID
		AND CSP.StipendTypeCode = 'PoliceStipend'
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.CommunityID = @CommunityID
		AND CSP.StipendTypeCode = 'PoliceStipend'
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'PoliceStipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.CommunityID = @CommunityID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
		AND S.StipendTypeCode = 'PoliceStipend'
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.CommunityID = @CommunityID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetCommunityByCommunityID

--Begin procedure policeengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Province table and policeengagementupdate.Province tables
--
-- Author:		Eric Jones
-- Create date:	2015.11.29
-- Description:	altered to allow pulling back of Field Activitied (aka projects) with POL component codes
-- =====================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CapacityAssessmentDate,
		dbo.FormatDate(P.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P.MaterialSupportStatus,
		P.PoliceEngagementOutput1,
		P.PoliceEngagementOutput2,
		P.PPPDate,
		dbo.FormatDate(P.PPPDate) AS PPPDateFormatted,
		P.ProvinceID,
		P.ProvinceName AS EntityName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CapacityAssessmentDate,
		dbo.FormatDate(P1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P1.MaterialSupportStatus,
		P1.PoliceEngagementOutput1,
		P1.PoliceEngagementOutput2,
		P1.PPPDate,
		dbo.FormatDate(P1.PPPDate) AS PPPDateFormatted,
		P2.ProvinceID,
		P2.ProvinceName AS EntityName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
			AND P1.ProvinceID = @ProvinceID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.PoliceEngagementAchievedValue,
		OAPI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.PoliceEngagementAchievedValue, 
				PRI.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectProvinceNotes(' + CAST(OAPP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PP.ProjectProvinceID
			FROM project.ProjectProvince PP
			WHERE PP.ProjectID = P.ProjectID
				AND PP.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate (note that the button for current & update are the same as projects are read only)
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectProvinceNotes(' + CAST(OAPP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PP.ProjectProvinceID
			FROM project.ProjectProvince PP
			WHERE PP.ProjectID = P.ProjectID
				AND PP.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(ISNULL(OAPR.RecommendationProvinceID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RP.RecommendationProvinceID
			FROM recommendation.RecommendationProvince RP
			WHERE RP.RecommendationID = R.RecommendationID
				AND RP.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.PoliceEngagementRiskValue,
		OAPR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID,
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.ProvinceID = @ProvinceID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = 'PoliceStipend'

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = 'PoliceStipend'
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = 'PoliceStipend'
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'PoliceStipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.ProvinceID = @ProvinceID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
		AND S.StipendTypeCode = 'PoliceStipend'
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = 'PoliceStipend'
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetProvinceByProvinceID

--Begin procedure reporting.GetCashHandoverReport
EXEC Utility.DropObject 'reporting.GetCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A stored procedure to data for the cash handover form
--
-- Author:			Todd Pires
-- Create date:	2015.09.28
-- Description:	Added the ArabicProvinceName column
-- ==================================================================
CREATE PROCEDURE reporting.GetCashHandoverReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ArabicProvinceName NVARCHAR(250)
	DECLARE @FullName VARCHAR(200) = (SELECT dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst'))
	DECLARE @PaymentMonthYear VARCHAR(20)
	DECLARE @PaymentMonth INT
	DECLARE @PaymentYear  INT
	DECLARE @ProvinceName VARCHAR(250)
	DECLARE @RunningCost INT 
	DECLARE @TotalCost INT 
	DECLARE @TotalExpenseCost INT
	
	SELECT @RunningCost = SUM(E.RunningCost)
	FROM
		(
		SELECT
			CASE
				WHEN D.CommunityID = 0
				THEN CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('RunningCostProvince', 0)) AS INT)
				ELSE CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('RunningCostCommunity', 0)) AS INT)
			END AS RunningCost
		FROM
			(
			SELECT DISTINCT 
				CSP.CommunityID
			FROM dbo.ContactStipendPayment CSP
				JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
					AND SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
			) D
		) E
	
	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	

		SELECT @PaymentMonth = PaymentMonth, @PaymentYear=PaymentYear  FROM dbo.ContactStipendPayment CSP 
	JOIN (
	
	SELECT TOP 1 EntityID	FROM reporting.SearchResult SR 
			WHERE SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = 17 
	
	) D ON  D.EntityID = CSP.ContactStipendPaymentID

	SELECT 
	@TotalExpenseCost = SUM(CAUE.ExpenseAmountAuthorized) 
	FROM dbo.CommunityAssetUnitExpense CAUE
	WHERE PaymentMonth = @PaymentMonth AND PaymentYear=@PaymentYear 

	SELECT TOP 1 
		@ArabicProvinceName = P.ArabicProvinceName,
		@PaymentMonthYear = DateName(month , DateAdd(month, CSP.PaymentMonth, 0) - 1) + ' - ' + CAST(CSP.PaymentYear AS CHAR(4)),
		@ProvinceName = P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT
		@ArabicProvinceName AS ArabicProvinceName,
		@FullName AS FullName,
		@PaymentMonthYear AS PaymentMonthYear,
		@ProvinceName AS ProvinceName,
		ISNULL(@TotalCost, 0) AS TotalCost,
		ISNULL(@TotalExpenseCost, 0) AS TotalExpenseCost,
		ISNULL(@TotalCost, 0) + ISNULL(@TotalExpenseCost, 0) as CompleteCost
END
GO
--End procedure reporting.GetCashHandoverReport

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ConceptNoteID,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,
		CN.TaskCode,
		CASE (SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')
		WHEN 'US Activity Workflow'
		THEN

		CASE
			WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
			ELSE
				CASE
				WHEN CN.WorkflowStepNumber <5
					THEN 'Development'
				WHEN CN.WorkflowStepNumber IN( 5 , 6)
				THEN 'Submitted for Approval'
				WHEN CN.WorkflowStepNumber = 7
				THEN 'Implimentation'
				ELSE 'Closedown'
				END

		 END
		ELSE

			CASE WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
			ELSE
			CASE
			WHEN CN.WorkflowStepNumber < 5
			THEN 'Development'
			WHEN CN.WorkflowStepNumber = 5
			THEN 'Submitted for Approval'
					WHEN CN.WorkflowStepNumber = 6
			THEN 'Implimentation'
			ELSE 'Closedown'
			END
				END
			END AS Status,

		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1
			SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		FORMAT((IsNull(OACNB.TotalBudget,0)+ IsNull(OACNA.ConceptNoteAmmendmentTotal,0) + isnull(OACNCE.ConceptNoteEquimentTotal, 0)), 'C', 'en-us') as totalBudget,
		FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us') as TotalSpent,
		FORMAT(((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmmendmentTotal,0)+ isnull(OACNCE.ConceptNoteEquimentTotal, 0)) - (IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0))), 'C', 'en-us')  AS TotalRemaining
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB


		OUTER APPLY
			(
			SELECT
				SUM(CNF.drAmt) - SUM(CNF.CrAmt) as ConceptNoteFinanceTotal
			FROM dbo.ConceptNoteFinance CNF
			WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
			) OACNF


		OUTER APPLY
			(
			SELECT
				SUM(CNA.Cost) as ConceptNoteAmmendmentTotal
			FROM dbo.ConceptNoteAmmendment CNA
			WHERE CNA.ConceptNoteID = CN.ConceptNoteID
			) OACNA



					OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue) as ConceptNoteEquimentTotal
			FROM dbo.ConceptNoteEquipmentCatalog CNCE
			JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
			AND CNCE.ConceptNoteID = CN.ConceptNoteID
			) OACNCE


			ORDER BY 2


END
GO
--End procedure reporting.GetConceptNotes

--Begin procedure reporting.GetJusticeStipendActivitySummaryPeople
EXEC Utility.DropObject 'reporting.GetJusticeStipendActivitySummaryPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendActivitySummaryPeople

@ProvinceID INT 

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(PVT.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(PVT.YearMonth, 4) AS YearMonthFormatted,
		(SELECT P.ProvinceID FROM dbo.Province P WHERE P.ProvinceID = @ProvinceID) AS ProvinceName,
		*
	FROM
		(
		SELECT 
			COUNT(CSP.StipendName) AS StipendNameCount,
			CSP.StipendName,
			YEAR(CSP.StipendPaidDate) * 100 + MONTH(CSP.StipendPaidDate) AS YearMonth
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.ProvinceID = @ProvinceID
			AND CSP.StipendPaidDate IS NOT NULL
		GROUP BY CSP.StipendPaidDate, CSP.StipendName
		) AS D
	PIVOT
		(
		MAX(D.StipendNameCount)
		FOR D.StipendName IN
			(
			Assistance,
			Cleaner,
			Correspondent,
			[Dep Manager],
			[Head Office / Center],
			[Headquarters Management],
			Registrar
			)
		) AS PVT
	ORDER BY PVT.YearMonth DESC

END
GO
--End procedure reporting.GetJusticeStipendActivitySummaryPeople

--Begin procedure reporting.GetJusticeStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetJusticeStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			John Lyons
-- Create date:	2016.05.31
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetJusticeStipendPaymentReport

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nCommunityAssetID INT	
	DECLARE @nCommunityAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)

	DELETE SP
	FROM reporting.StipendPayment SP
	WHERE SP.PersonID = @PersonID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,

			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.CommunityAssetID > 0
				AND C.CommunityAssetUnitID > 0
		GROUP BY 
			S.StipendName,
			C.CommunityAssetID,
			C.CommunityAssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.CommunityAssetID = @nCommunityAssetID AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,CommunityAssetID,CommunityAssetUnitID) VALUES (@PersonID,@nCommunityAssetID,@nCommunityAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Assistance] = SP.[Assistance] + CASE WHEN @cStipendName = 'Assistance' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Cleaner]  = SP.[Cleaner] + CASE WHEN @cStipendName = 'Cleaner' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Correspondent] = SP.[Correspondent] + CASE WHEN @cStipendName = 'Correspondent' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Dep Manager]  = SP.[Dep Manager] + CASE WHEN @cStipendName = 'Dep Manager' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Head Office / Center] = SP.[Head Office / Center] + CASE WHEN @cStipendName = 'Head Office / Center' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Headquarters Management] = SP.[Headquarters Management] + CASE WHEN @cStipendName = 'Headquarters Management' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Registrar] = SP.[Registrar] + CASE WHEN @cStipendName = 'Registrar' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Assistance Count] = SP.[Assistance Count] + CASE WHEN @cStipendName = 'Assistance' THEN @nContactCount ELSE 0 END,
			SP.[Cleaner Count] = SP.[Cleaner Count] + CASE WHEN @cStipendName = 'Cleaner' THEN @nContactCount ELSE 0 END,
			SP.[Correspondent Count] = SP.[Correspondent Count] + CASE WHEN @cStipendName = 'Correspondent' THEN @nContactCount ELSE 0 END,
			SP.[Dep Manager Count] = SP.[Dep Manager Count] + CASE WHEN @cStipendName = 'Dep Manager' THEN @nContactCount ELSE 0 END,
			SP.[Head Office / Center Count] = SP.[Head Office / Center Count] + CASE WHEN @cStipendName = 'Head Office / Center' THEN @nContactCount ELSE 0 END,
			SP.[Headquarters Management Count] = SP.[Headquarters Management Count] + CASE WHEN @cStipendName = 'Headquarters Management' THEN @nContactCount ELSE 0 END,
			SP.[Registrar Count] = SP.[Registrar Count] + CASE WHEN @cStipendName = 'Registrar' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.CommunityAssetID = @nCommunityAssetID
			AND SP.CommunityAssetUnitID = @nCommunityAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nCommunityAssetID, @nCommunityAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
			SP.[Total Count] ,
			SP.[Total Stipend],
			SP.[Assistance] ,
			SP.[Cleaner]  ,
			SP.[Correspondent] ,
			SP.[Dep Manager]  ,
			SP.[Head Office / Center],
			SP.[Headquarters Management] ,
			SP.[Registrar] ,
			SP.[Assistance Count],
			SP.[Cleaner Count] ,
			SP.[Correspondent Count],
			SP.[Dep Manager Count] ,
			SP.[Head Office / Center Count] ,
			SP.[Headquarters Management Count],
			SP.[Registrar Count] ,
			sp.stipendpaymentid, 
			sp.personid,
			sp.communityassetid,
			sp.CommunityAssetunitID,

		CAUCR.CommunityAssetUnitCostRate AS [Running Costs],

		CASE
			WHEN CA.CommunityID > 0
			THEN dbo.GetCommunityNameByCommunityID(CA.CommunityID)
			ELSE dbo.GetProvinceNameByProvinceID(CA.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center

	FROM reporting.StipendPayment SP
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = SP.CommunityAssetID AND SP.PersonID = @PersonID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = SP.CommunityAssetUnitID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID

END
GO
--End procedure reporting.GetJusticeStipendPaymentReport

--Begin procedure reporting.GetStipendPaymentsByProvince
EXEC Utility.DropObject 'reporting.GetStipendPaymentsByProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:		Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data for stipend payments
-- ===================================================================
CREATE PROCEDURE reporting.GetStipendPaymentsByProvince

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.ContactID AS SysID, 
		C1.CellPhoneNumber,
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		
		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Community C2 JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID AND C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ProvinceName,
		
		CASE
			WHEN C1.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C1.CommunityID) = 1
			THEN S.StipendAmount
			WHEN C1.ProvinceID > 0 AND dbo.IsProvinceStipendEligible(C1.ProvinceID) = 1
			THEN S.StipendAmount
			ELSE 0
		END AS StipendAmount,
			
		CASE
			WHEN C1.CommunityID > 0
			THEN dbo.GetCommunityNameByCommunityID(C1.CommunityID)
			ELSE dbo.GetProvinceNameByProvinceID(C1.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center,

		C1.ArabicMotherName,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		S.StipendName 
	FROM reporting.SearchResult SR
		JOIN dbo.Contact C1 ON C1.ContactID = SR.EntityID 
			AND SR.EntityTypeCode = 'Contact'	
			AND SR.PersonID = @PersonID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
		LEFT JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = C1.CommunityAssetID
		LEFT JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID
		LEFT JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
	ORDER BY ContactLocation, Center, C1.LastName, C1.FirstName, C1.MiddleName, C1.ContactID

END
GO
--End procedure reporting.GetStipendPaymentsByProvince