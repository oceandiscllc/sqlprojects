USE AJACS
GO

--Begin function workflow.GetWorkflowStepNumber
EXEC Utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.05.28
-- Description:	Added backwards compatibility logic for concept notes & spot reports
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = (SELECT TOP 1 EWSGP.WorkflowStepNumber FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID AND IsComplete = 0 ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID)

	IF @nWorkflowStepNumber IS NULL AND NOT EXISTS (SELECT 1 FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID)
		BEGIN
		
		SET @nWorkflowStepNumber = 
			CASE
				WHEN @EntityTypeCode = 'ConceptNote'
				THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @EntityID)
				WHEN @EntityTypeCode = 'SpotReport'
				THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @EntityID)
				ELSE NULL
			END
		
		END
	--ENDIF

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber

END
GO
--End function workflow.GetWorkflowStepNumber