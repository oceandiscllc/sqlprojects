USE AJACS
GO

UPDATE ET
SET ET.HasWorkflow = 0
FROM dbo.EntityType ET
WHERE ET.EntityTypeCode = 'ProgramReport'
GO

UPDATE D2  
SET
	D2.DocumentFileName = D2.DocumentTitle,
	D2.DocumentTitle = D1.DocumentTitle
FROM dbo.Document D1
	JOIN Document.Document D2 On D2.DocumentId = D1.DocumentID 

UPDATE D1
SET D1.DocumentFileName = D1.DocumentTitle
FROM document.Document D1
WHERE D1.DocumentID NOT IN 
	(
	SELECT D2.DocumentID 
	FROM dbo.Document D2
	)

--cleanup weekly full reports for ajacs leo--
DELETE D1
FROM LEO0000.document.Document D1
WHERE D1.Documentid IN
	(
	SELECT D2.DocumentID 
	FROM LEO0000.document.Document D2
		JOIN LEO0000.dropdown.DocumentType DT ON DT.DocumentTypeID = D2.DocumentTypeID 
			AND D2.DocumentDescription = 'AJACS Weekly Atmospheric Report'
			AND D2.IntegrationCode = 'Ajacs'
	)
