USE AJACS
GO

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Bug fix
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT,
@WorkflowID INT,
@IsAmendment BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @IsAmendment = 1
		BEGIN

		SELECT @WorkflowID = EWSGP.WorkflowID
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = 'ConceptNote' 
			AND EWSGP.EntityID = @ConceptNoteID

		END
	--ENDIF

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,ActualTotalAmount,AmendedConceptNoteID,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,EndDate,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,StartDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber,WorkplanActivityID)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,

		CASE
			WHEN @IsAmendment = 1
			THEN C.ActualTotalAmount
			ELSE 0
		END,

		CASE
			WHEN @IsAmendment = 1
			THEN @ConceptNoteID
			ELSE 0
		END,

		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.EndDate,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.StartDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,

		CASE
			WHEN @IsAmendment = 1
			THEN C.Title
			ELSE 'Clone of:  ' + C.Title
		END,

		C.VettingRequirements,
		1,
		WorkplanActivityID		
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@nConceptNoteID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.WorkflowID = @WorkflowID

	INSERT INTO	dbo.ConceptNoteAmendment
		(ConceptNoteID, AmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Amendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Amendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteContact
		(ConceptNoteID, ContactID)
	SELECT
		@nConceptNoteID,
		CNC.ContactID
	FROM dbo.ConceptNoteContact CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.GetEmailDigestRecords
EXEC Utility.DropObject 'dbo.GetEmailDigestRecords'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Damon Miller
-- Create date:	2017.05.26
-- Description:	Gets records from the dbo.EmailDigest table
-- ========================================================
CREATE PROCEDURE dbo.GetEmailDigestRecords

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ED.EmailDigestID,
		ED.EmailAddress,
		ED.EmailSubject,
		ED.EmailBody
	FROM dbo.EmailDigest ED
	ORDER BY ED.EmailAddress

END
GO
--End procedure dbo.GetEmailDigestRecords

--Begin procedure document.GetDocumentByDocumentData
EXEC utility.DropObject 'document.GetDocumentByDocumentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentData

@DocumentData VARBINARY(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData, 
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.DocumentFileName,
		D.PhysicalFileSize
	FROM document.Document D
	WHERE D.DocumentData = @DocumentData
	
END
GO
--End procedure document.GetDocumentByDocumentData

--Begin procedure document.GetDocumentByDocumentID
EXEC Utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	A stored procedure to get data from the document.Document table
--
-- Author:			Todd Pires
-- Create date:	2016.01.25
-- Description:	Implemented the IsForDonorPortal field
--
-- Author:			Brandon Green
-- Create date:	2016.08.16
-- Description:	Implemented the DisplayInCommunityNewsFeed field
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- =======================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		dbo.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DisplayInCommunityNewsFeed,
		D.DocumentData, 
		D.DocumentDate, 
		dbo.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.DocumentFileName,
		D.Extension,
		D.IsForDonorPortal,
		D.PhysicalFileSize,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @DocumentID

	SELECT 
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN document.DocumentEntity DE ON DE.EntityID = P.ProvinceID
			AND DE.EntityTypeCode = 'Province'
			AND DE.DocumentID = @DocumentID

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.Community C
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN document.DocumentEntity DE ON DE.EntityID = C.CommunityID
			AND DE.EntityTypeCode = 'Community'
			AND DE.DocumentID = @DocumentID
	
END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.DocumentFileName,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentName = @DocumentName
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure document.GetEntityDocuments
EXEC Utility.DropObject 'dbo.GetEntityDocuments'
EXEC Utility.DropObject 'document.GetEntityDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:		Todd Pires
-- Create date: 2016.09.30
-- Description:	A stored procedure to get records from the dbo.DocumentEntity table
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- ================================================================================
CREATE PROCEDURE document.GetEntityDocuments

@EntityTypeCode VARCHAR(50),
@EntityTypeSubCode VARCHAR(50),
@EntityID INT,
@DocumentIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bHaveDocumentIDList BIT = 0
	DECLARE @tTable TABLE (DocumentID INT NOT NULL)

	IF @EntityTypeCode = @EntityTypeSubCode OR LEN(RTRIM(@EntityTypeSubCode)) = 0
		SET @EntityTypeSubCode = NULL
	--ENDIF

	IF @DocumentIDList IS NOT NULL AND LEN(RTRIM(@DocumentIDList)) > 0
		BEGIN

		INSERT INTO @tTable
			(DocumentID)
	SELECT
			CAST(LTT.ListItem AS INT)
		FROM core.ListToTable(@DocumentIDList, ',') LTT

		SET @bHaveDocumentIDList = 1

		END
	--ENDIF

	SELECT
		D.ContentType, 
		D.ContentSubtype,
		D.DocumentDate, 
		D.DocumentDescription,
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentFileName,
		D.PhysicalFileSize,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), '0') AS ThumbnailLength,

		CASE
			WHEN ISNULL(DATALENGTH(D.Thumbnail), '0') > 0
			THEN ISNULL(DATALENGTH(D.Thumbnail), '0')
			ELSE D.PhysicalFileSize
		END AS FileSize,

		DE.EntityID,
		DE.EntityTypeCode,
		DE.EntityTypeSubCode
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND (@EntityTypeSubCode IS NULL OR DE.EntityTypeSubCode = @EntityTypeSubCode)
			AND 
				(
				DE.EntityID = @EntityID
					OR 
						(
						@bHaveDocumentIDList = 1 AND EXISTS
							(
							SELECT 1
							FROM @tTable T
							WHERE T.DocumentID = D.DocumentID
							)
						)
				)
	ORDER BY D.DocumentDescription, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetEntityDocuments

--Begin procedure newsletter.GetNewsLetterByNewsLetterID
EXEC utility.DropObject 'newsletter.GetNewsLetterByNewsLetterID'
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date: 2017.05.21
-- Description:	A stored procedure to get newsletter data based on a newsletterid
-- ==============================================================================
CREATE PROCEDURE newsletter.GetNewsLetterByNewsLetterID

@NewsLetterID INT 

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSiteURL VARCHAR(500) = (SELECT dbo.GetServerSetupValueByServerSetupKey('SiteURL', '""'))

	SELECT
		NL.Comments,
		NL.NewsLetterDate,
		dbo.FormatDate(NL.NewsLetterDate) AS NewsLetterDateFormatted
	FROM newsletter.NewsLetter NL
	WHERE NL.NewsLetterID = @NewsLetterID

	SELECT
		ET.EntityTypeCode,
		ET.EntityTypeName,
		NLE.EntityID,
		NLE.NewsLetterEntityID,
		NLE.Notes,

		CASE
			WHEN ET.EntityTypeCode = 'Document'
			THEN @cSiteURL + '/document/getDocumentByDocumentName/DocumentName/' + document.GetDocumentNameByDocumentID(NLE.EntityID)
			ELSE @cSiteURL + '/' + LOWER(ET.EntityTypeCode) + '/view/id/' + CAST(NLE.EntityID AS VARCHAR(10))
		END AS EntityHREF,

		CASE 
			WHEN ET.EntityTypeCode = 'Asset'
			THEN asset.GetAssetNameByAssetID(NLE.EntityID)
			WHEN ET.EntityTypeCode = 'Community'
			THEN dbo.GetCommunityNameByCommunityID(NLE.EntityID)
			WHEN ET.EntityTypeCode = 'Document'
			THEN ISNULL((SELECT D.DocumentDescription FROM document.Document D WHERE D.DocumentID = NLE.EntityID), '')
			WHEN ET.EntityTypeCode = 'Force'
			THEN force.GetForceNameByForceID(NLE.EntityID)
			WHEN ET.EntityTypeCode = 'Province'
			THEN dbo.GetProvinceNameByProvinceID(NLE.EntityID)
			WHEN ET.EntityTypeCode = 'RequestForInformation'
			THEN dbo.GetRequestForInformationNameByRequestForInformationID(NLE.EntityID)
			WHEN ET.EntityTypeCode = 'SpotReport'
			THEN dbo.GetSpotReportNameBySpotReportID(NLE.EntityID)
		END AS EntityName

	FROM newsletter.NewsLetterEntity NLE
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = NLE.EntityTypeCode
			AND NLE.NewsLetterID = @NewsLetterID
	ORDER BY ET.EntityTypeName, NLE.EntityID

END
GO
--End procedure newsletter.GetNewsLetterByNewsLetterID

--Begin procedure newsletter.GetNewsLetterSubscribers
EXEC utility.DropObject 'newsletter.GetNewsLetterSubscribers'
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date: 2017.05.21
-- Description:	A stored procedure to get newsletter data based on a newsletterid
-- ==============================================================================
CREATE PROCEDURE newsletter.GetNewsLetterSubscribers

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		dbo.FormatPersonNameByPersonID(P.PersonID, 'FirstLast') AS PersonNameFormatted,
		P.FirstName
	FROM dbo.Person P
	WHERE P.IsNewsLetterSubscriber = 1
	ORDER BY P.EmailAddress

END
GO
--End procedure newsletter.GetNewsLetterSubscribers

--Begin procedure workflow.PurgeEntityWorkflowStepGroupPerson
EXEC utility.DropObject 'workflow.PurgeEntityWorkflowStepGroupPerson'
GO

-- =============================================================================================================
-- Author:			Todd Pires
-- Create date: 2017.06.21
-- Description:	A stored procedure to purge orphan records from the workflow.EntityWorkflowStepGroupPerson table
-- =============================================================================================================
CREATE PROCEDURE workflow.PurgeEntityWorkflowStepGroupPerson

AS
BEGIN
	SET NOCOUNT ON;

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'ConceptNote'
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EWSGP.EntityID
			)

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'ProgramReport'
		AND NOT EXISTS
			(
			SELECT 1
			FROM weeklyreport.ProgramReport PR
			WHERE PR.ProgramReportID = EWSGP.EntityID
			)

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'RecommendationUpdate'
		AND NOT EXISTS
			(
			SELECT 1
			FROM recommendationupdate.Recommendation R
			WHERE R.RecommendationID = EWSGP.EntityID
			)

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'RiskUpdate'
		AND NOT EXISTS
			(
			SELECT 1
			FROM riskupdate.Risk R
			WHERE R.RiskID = EWSGP.EntityID
			)

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'SpotReport'
		AND NOT EXISTS
			(
			SELECT 1
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = EWSGP.EntityID
			)

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'WeeklyReport'
		AND NOT EXISTS
			(
			SELECT 1
			FROM weeklyreport.WeeklyReport WR
			WHERE WR.WeeklyReportID = EWSGP.EntityID
			)

END
GO
--End procedure workflow.PurgeEntityWorkflowStepGroupPerson
