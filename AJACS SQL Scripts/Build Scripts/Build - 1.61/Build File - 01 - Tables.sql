USE AJACS
GO

--Begin table dbo.CommunityRound
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRound'

EXEC utility.DropColumn @TableName, 'FieldOfficer'
EXEC utility.AddColumn @TableName, 'FieldOfficerContactID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'FieldOfficerContactID', 'INT', 0
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.CommunityRound') AND SC.Name = 'ActivitiesOfficerPersonID')
	EXEC sp_RENAME 'dbo.CommunityRound.ActivitiesOfficerPersonID', 'ActivitiesOfficerContactID', 'COLUMN';
--ENDIF
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.CommunityRound') AND SC.Name = 'FieldFinanceOfficerPersonID')
	EXEC sp_RENAME 'dbo.CommunityRound.FieldFinanceOfficerPersonID', 'FieldFinanceOfficerContactID', 'COLUMN';
--ENDIF
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.CommunityRound') AND SC.Name = 'FieldLogisticOfficerPersonID')
	EXEC sp_RENAME 'dbo.CommunityRound.FieldLogisticOfficerPersonID', 'FieldLogisticOfficerContactID', 'COLUMN';
--ENDIF
GO
--End table dbo.CommunityRound

--Begin table dbo.CommunityRoundUpate
EXEC utility.DropObject 'dbo.CommunityRoundUpate'
GO
--End table dbo.CommunityRoundUpate
