USE AJACS
GO

EXEC utility.AddColumn 'dbo.Community', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'dbo.Province', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'weeklyreport.Community', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'weeklyreport.Province', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'weeklyreport.WeeklyReport', 'SummaryMap', 'VARBINARY(MAX)'
GO

--Begin table weeklyreport.SummaryMapCommunity
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapCommunity
	(
	SummaryMapCommunityID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapCommunityID'
EXEC utility.SetIndexClustered 'IX_SummaryMapCommunity', @TableName, 'WeeklyReportID,CommunityID'
GO
--End table weeklyreport.SummaryMapCommunity

--Begin table weeklyreport.SummaryMapCommunityAsset
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapCommunityAsset'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapCommunityAsset
	(
	SummaryMapCommunityAssetID INT IDENTITY(1,1) NOT NULL,
	CommunityAssetID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapCommunityAssetID'
EXEC utility.SetIndexClustered 'IX_SummaryMapCommunityAsset', @TableName, 'WeeklyReportID,CommunityAssetID'
GO
--End table weeklyreport.SummaryMapCommunityAsset

--Begin table weeklyreport.SummaryMapIncident
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapIncident'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapIncident
	(
	SummaryMapIncidentID INT IDENTITY(1,1) NOT NULL,
	IncidentID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapIncidentID'
EXEC utility.SetIndexClustered 'IX_SummaryMapIncident', @TableName, 'WeeklyReportID,IncidentID'
GO
--End table weeklyreport.SummaryMapIncident

