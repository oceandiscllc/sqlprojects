USE AJACS
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT 
	P.PersonID, 
	'DocumentType.000.View' 
FROM dbo.Person P
WHERE NOT EXISTS 
	(
	SELECT 1
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = P.PersonID
		AND PP.PermissionableLineage = 'DocumentType.000.View'
	)
GO