-- File Name:	Build-1.35 File 01 - AJACS.sql
-- Build Key:	Build-1.35 File 01 - AJACS - 2015.10.30 23.15.29

USE AJACS
GO

-- ==============================================================================================================================
-- Procedures:
--		eventlog.LogSubContractorAction
--		permissionable.CheckPartialPermission
--		reporting.getEDPByConceptNoteID
--		reporting.GetStipendPaymentsByProvince
--		reporting.GetWeeklyReportCommunity
--		reporting.GetWeeklyReportProvince
--		weeklyreport.ApproveWeeklyReport
--		weeklyreport.GetWeeklyReport
--		weeklyreport.GetWeeklyReportCommunity
--		weeklyreport.GetWeeklyReportProvince
--		weeklyreport.PopulateWeeklyReportCommunities
--		weeklyreport.PopulateWeeklyReportProvinces
--
-- Tables:
--		weeklyreport.SummaryMapCommunity
--		weeklyreport.SummaryMapCommunityAsset
--		weeklyreport.SummaryMapIncident
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

EXEC utility.AddColumn 'dbo.Community', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'dbo.Province', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'weeklyreport.Community', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'weeklyreport.Province', 'KeyHighlight', 'VARCHAR(MAX)'
EXEC utility.AddColumn 'weeklyreport.WeeklyReport', 'SummaryMap', 'VARBINARY(MAX)'
GO

--Begin table weeklyreport.SummaryMapCommunity
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapCommunity
	(
	SummaryMapCommunityID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapCommunityID'
EXEC utility.SetIndexClustered 'IX_SummaryMapCommunity', @TableName, 'WeeklyReportID,CommunityID'
GO
--End table weeklyreport.SummaryMapCommunity

--Begin table weeklyreport.SummaryMapCommunityAsset
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapCommunityAsset'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapCommunityAsset
	(
	SummaryMapCommunityAssetID INT IDENTITY(1,1) NOT NULL,
	CommunityAssetID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapCommunityAssetID'
EXEC utility.SetIndexClustered 'IX_SummaryMapCommunityAsset', @TableName, 'WeeklyReportID,CommunityAssetID'
GO
--End table weeklyreport.SummaryMapCommunityAsset

--Begin table weeklyreport.SummaryMapIncident
DECLARE @TableName VARCHAR(250) = 'weeklyreport.SummaryMapIncident'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.SummaryMapIncident
	(
	SummaryMapIncidentID INT IDENTITY(1,1) NOT NULL,
	IncidentID INT,
	WeeklyReportID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WeeklyReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SummaryMapIncidentID'
EXEC utility.SetIndexClustered 'IX_SummaryMapIncident', @TableName, 'WeeklyReportID,IncidentID'
GO
--End table weeklyreport.SummaryMapIncident


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure eventlog.LogSubContractorAction
EXEC utility.DropObject 'eventlog.LogSubContractorAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.16
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSubContractorAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('SubContractor', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SubContractor',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('SubContractor'), ELEMENTS
			)
		FROM dbo.SubContractor T
		WHERE T.SubContractorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSubContractorAction

--Begin procedure permissionable.CheckPartialPermission
EXEC Utility.DropObject 'permissionable.CheckPartialPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.19
-- Description:	A stored procedure to check for a partial permission
-- =================================================================
CREATE PROCEDURE permissionable.CheckPartialPermission

@PersonID INT,
@PermissionableLineage VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT PP.PersonPermissionableID
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		AND PP.PermissionableLineage LIKE @PermissionableLineage + '%'
	
END
GO
--End procedure permissionable.CheckPartialPermission

--Begin procedure reporting.getEDPByConceptNoteID
EXEC Utility.DropObject 'reporting.getEDPByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			John Lyons
-- Create date:	2015.10.17
-- Description:	A stored procedure to get reporting data
-- =====================================================
CREATE PROCEDURE reporting.getEDPByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
	
		CASE 
			WHEN CN.ConceptNoteContactEquipmentDistributionDate IS NULL
			THEN 'Planned'
			ELSE 'Distributed'
		END AS EquipmentDistributionPlanStatus,
	
		EDP.ExportRoute, 
		EDP.CurrentSituation, 
		EDP.Aim, 
		EDP.PlanOutline, 
		EDP.Phase1, 
		EDP.Phase2, 
		EDP.Phase3, 
		EDP.Phase4, 
		EDP.Phase5, 
		EDP.OperationalResponsibility, 
		EDP.Annexes, 
		EDP.Distribution, 
		EDP.Title, 
		EDP.Summary,
		EDP.OperationalSecurity,
		EDP.ErrataTitle,
		EDP.Errata
	FROM procurement.EquipmentDistributionPlan EDP 
		JOIN dbo.ConceptNote CN ON EDP.ConceptNoteID = CN.ConceptNoteID 
			AND EDP.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure reporting.getEDPByConceptNoteID

--Begin procedure reporting.GetStipendPaymentsByProvince
EXEC Utility.DropObject 'reporting.GetStipendPaymentsByProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data for stipend payments
-- ===================================================================
CREATE PROCEDURE reporting.GetStipendPaymentsByProvince

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.ContactID AS SysID, 
		C1.CellPhoneNumber,
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		
		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Community C2 JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID AND C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ProvinceName,
		
		CASE
			WHEN C1.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C1.CommunityID) = 1
			THEN S.StipendAmount
			WHEN C1.ProvinceID > 0 AND dbo.IsProvinceStipendEligible(C1.ProvinceID) = 1
			THEN S.StipendAmount
			ELSE 0
		END AS StipendAmount,
			
		CASE
			WHEN C1.CommunityID > 0
			THEN dbo.GetCommunityNameByCommunityID(C1.CommunityID)
			ELSE dbo.GetProvinceNameByProvinceID(C1.ProvinceID)
		END + '-' + CA.CommunityAssetName + '-' + CAU.CommunityAssetUnitName AS Center,

		ArabicMotherName,
		ArabicFirstName,
		ArabicMiddleName,
		ArabicLastName,
		S.StipendName 
	FROM reporting.SearchResult SR
		JOIN dbo.Contact C1 ON C1.ContactID = SR.EntityID AND SR.EntityTypeCode = 'Contact'	AND SR.PersonID = @PersonID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = c1.CommunityAssetID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = c1.CommunityAssetUnitID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
	ORDER BY ContactLocation, Center, C1.LastName, C1.FirstName, C1.MiddleName, C1.ContactID

END
GO
--End procedure reporting.GetStipendPaymentsByProvince

--Begin procedure reporting.GetWeeklyReportCommunity
EXEC Utility.DropObject 'reporting.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.03.06
-- Description:	Changed the CommunityReportStatusName logic
--
-- Author:			Todd Pires
-- Update date:	2015.05.06
-- Description:	Repointed the reference code at the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Update date:	2015.09.24
-- Description:	Repointed the icons at /i/
--
-- Author:			Todd Pires
-- Update date:	2015.10.22
-- Description:	Added the KeyHighlight column
-- ================================================================================
CREATE PROCEDURE reporting.GetWeeklyReportCommunity

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName, 	
		C.Implications, 	
		C.KeyHighlight,	
		C.KeyPoints, 	
		C.RiskMitigation,	
		C.Summary, 	
		C.WeeklyReportID,	
		reporting.IsDraftReport('WeeklyReport', C.WeeklyReportID) AS IsDraft,
		CES.CommunityEngagementStatusName,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/i/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,

		CASE
			WHEN C.ImpactDecisionID IN (0,1,2,3,5)
			THEN 'Watch'
			WHEN C.ImpactDecisionID IN (4) 
			THEN 'Engage'
			ELSE 'Alert'			
		END AS CommunityReportStatusName	

	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure reporting.GetWeeklyReportCommunity

--Begin procedure reporting.GetWeeklyReportProvince
EXEC Utility.DropObject 'reporting.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.04.27
-- Description:	Added the dbo.FormatProvinceIncidentsForGoogleMap call
--
-- Author:			Todd Pires
-- Update date:	2015.05.06
-- Description:	Repointed the reference code at the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Update date:	2015.09.24
-- Description:	Repointed the icons at /i/
--
-- Author:			Todd Pires
-- Update date:	2015.10.22
-- Description:	Added the KeyHighlight column
-- ================================================================================
CREATE PROCEDURE reporting.GetWeeklyReportProvince

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.Implications, 
		P.KeyHighlight,
		P.KeyPoints, 
		P.ProvinceName, 
		P.RiskMitigation,
		P.Summary, 
		P.WeeklyReportID,
		reporting.IsDraftReport('WeeklyReport', P.WeeklyReportID) AS IsDraft,
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/i/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,
		dbo.FormatStaticGoogleMapForWeeklyReport(P.ProvinceID) AS MapImage
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
	ORDER BY P.ProvinceName, P.ProvinceID

END
GO
--End procedure reporting.GetWeeklyReportProvince

--Begin procedure weeklyreport.ApproveWeeklyReport
EXEC Utility.DropObject 'weeklyreport.ApproveWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
--
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	Renamed from SubmitWeeklyReport to ApproveWeeklyReport
-- ======================================================================
CREATE PROCEDURE weeklyreport.ApproveWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @WeeklyReportID INT
	DECLARE @tOutput TABLE (EntityTypeCode VARCHAR(50), EntityID INT)

	SELECT @WeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @WeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @WeeklyReportID

	INSERT INTO @tOutput (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @WeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.EntityTypeCode, O.EntityID
		FROM @tOutput O
		ORDER BY O.EntityTypeCode, O.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	TRUNCATE TABLE weeklyreport.Community
	TRUNCATE TABLE weeklyreport.Province
	TRUNCATE TABLE weeklyreport.SummaryMapCommunity
	TRUNCATE TABLE weeklyreport.SummaryMapCommunityAsset
	TRUNCATE TABLE weeklyreport.SummaryMapCommunityIncident
	DELETE FROM weeklyreport.WeeklyReport

END
GO
--End procedure weeklyreport.ApproveWeeklyReport

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.WeeklyReportID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O
		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.WorkflowStepNumber 
	FROM weeklyreport.WeeklyReport WR

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'WeeklyReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nWeeklyReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'WeeklyReport'
			AND WSWA.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @nWeeklyReportID)
	ORDER BY WSWA.DisplayOrder

	SELECT
		C.CommunityID,
		C.CommunityName,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID,
		ID.ImpactDecisionName,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		C2.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunity SMC WHERE SMC.CommunityID = C.CommunityID AND SMC.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM weeklyreport.Community C
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Community C2 ON C2.CommunityID = C.CommunityID
			 AND C.WeeklyReportID = @nWeeklyReportID

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
			AND CAT.CommunityAssetTypeID = 1
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor,
		'/assets/img/icons/' + REPLACE(ZT.HexColor, '#', '') + '.png' AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CAT.CommunityAssetTypeID = 2
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapIncident SMI WHERE SMI.IncidentID = I.IncidentID AND SMI.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (
				EXISTS	(
					SELECT 1
					FROM dbo.IncidentCommunity IC
						JOIN weeklyreport.Community C ON C.CommunityID = IC.CommunityID
							AND IC.IncidentID = I.IncidentID
							AND C.WeeklyReportID = @nWeeklyReportID
				)
				OR
				EXISTS (
					SELECT 1
					FROM dbo.IncidentProvince IP
						JOIN weeklyreport.Province P ON P.ProvinceID = IP.ProvinceID
							AND IP.IncidentID = I.IncidentID
							AND P.WeeklyReportID = @nWeeklyReportID
				)
			)		

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure weeklyreport.GetWeeklyReportCommunity
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get community data for the weekly report
-- ===========================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReportCommunity

@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID, 
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID, 
		C.StatusChangeID, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		C.KeyHighlight,
		CES.CommunityEngagementStatusName,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM dbo.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
			AND C.CommunityID = @CommunityID
	
	SELECT
		C.CommunityID, 
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID, 
		C.StatusChangeID, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		CES.CommunityEngagementStatusName,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
			AND C.CommunityID = @CommunityID

END
GO
--End procedure weeklyreport.GetWeeklyReportCommunity

--Begin procedure weeklyreport.GetWeeklyReportProvince
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get Province data for the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReportProvince

@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceID, 
		P.ImpactDecisionID, 
		P.StatusChangeID, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		P.KeyHighlight,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM dbo.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

	SELECT
		P.ProvinceID, 
		P.ImpactDecisionID, 
		P.StatusChangeID, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		P.KeyHighlight,
		ID.ImpactDecisionName,
		SC.StatusChangeName
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
			AND P.ProvinceID = @ProvinceID

END
GO
--End procedure weeklyreport.GetWeeklyReportProvince

--Begin procedure weeklyreport.PopulateWeeklyReportCommunities
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add community data to the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.10.22
-- Description:	Added the KeyHighlight column
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportCommunities

@CommunityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Community
		(CommunityEngagementStatusID, CommunityID, CommunityName, ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		C.CommunityEngagementStatusID,
		C.CommunityID,
		C.CommunityName,
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyHighlight,
		C.KeyPoints, 
		C.ProvinceID,
		C.RiskMitigation,
		C.StatusChangeID, 
		C.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Community C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Community WRC
				WHERE WRC.CommunityID = C.CommunityID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportCommunities

--Begin procedure weeklyreport.PopulateWeeklyReportProvinces
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportProvinces'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add Province data to the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportProvinces

@ProvinceIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Province
		(ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, ProvinceName, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		P.ImpactDecisionID, 
		P.Implications, 
		P.KeyHighlight,
		P.KeyPoints, 
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID, 
		P.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Province P
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Province WRP
				WHERE WRP.ProvinceID = P.ProvinceID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportProvinces
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT 
	P.PersonID, 
	'DocumentType.000.View' 
FROM dbo.Person P
WHERE NOT EXISTS 
	(
	SELECT 1
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = P.PersonID
		AND PP.PermissionableLineage = 'DocumentType.000.View'
	)
GO
--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.35 File 01 - AJACS - 2015.10.30 23.15.29')
GO
--End build tracking

