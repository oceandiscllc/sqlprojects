USE AJACS
GO

--Begin procedure dropdown.GetPaymentGroupData
EXEC Utility.DropObject 'dropdown.GetPaymentGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.24
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	Added StipendTypeCode support
--
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	Added the (locked) option to YearMonthFormatted
--
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	Added the (authorized) option to YearMonthFormatted
-- =======================================================================================
CREATE PROCEDURE dropdown.GetPaymentGroupData

@IncludeZero BIT = 0,
@StipendTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(T.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(T.YearMonth, 4) + ' - ' + T.ProvinceName AS YearMonthFormatted,
		T.YearMonth + '-' + CAST(T.ProvinceID AS VARCHAR(10)) AS YearMonthProvince,

		CASE
			WHEN NOT EXISTS (SELECT 1 FROM dbo.ContactStipendPayment CSP WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = CAST(T.YearMonth AS INT) AND CSP.ProvinceID = T.ProvinceID AND CSP.StipendTypeCode = @StipendTypeCode AND CSP.StipendAuthorizedDate IS NULL)
			THEN 1
			ELSE 0
		END AS IsAuthorized,

		CASE
			WHEN NOT EXISTS (SELECT 1 FROM dbo.ContactStipendPayment CSP WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = CAST(T.YearMonth AS INT) AND CSP.ProvinceID = T.ProvinceID AND CSP.StipendTypeCode = @StipendTypeCode AND CSP.StipendPaidDate IS NULL)
			THEN 1
			ELSE 0
		END AS IsLocked

	FROM
		(
		SELECT DISTINCT
			CAST((CSP.PaymentYear * 100 + CSP.PaymentMonth) AS CHAR(6)) AS YearMonth, 
			P.ProvinceID,
			P.ProvinceName
		FROM dbo.ContactStipendPayment CSP
			JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
				AND CSP.StipendTypeCode = @StipendTypeCode
		) T
	ORDER BY T.YearMonth DESC, 1

END
GO
--End procedure dropdown.GetPaymentGroupData