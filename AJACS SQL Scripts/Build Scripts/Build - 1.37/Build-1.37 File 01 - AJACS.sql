-- File Name:	Build-1.37 File 01 - AJACS.sql
-- Build Key:	Build-1.37 File 01 - AJACS - 2015.11.05 20.14.35

USE AJACS
GO

-- ==============================================================================================================================
-- Procedures:
--		reporting.IndicatorReport
--		reporting.RecommendationReport
--		reporting.RiskReport
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure reporting.IndicatorReport
EXEC Utility.DropObject 'reporting.IndicatorReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure to get CommunityEngagementReport data FROM the eventlog table
-- =================================================================================
CREATE PROCEDURE reporting.IndicatorReport

 @StartDate Date , @EndDate Date

AS
BEGIN
	



SELECT DISTINCT D.ReportType, D.SubreportType,D.EntityLocationName, D.EntityName, D.Author, D.Notes, D.Value, D.Updatedate
FROM
(

SELECT

'Police Engagement' AS ReportType,
'Province Indicator' AS SubReportType,
C.CommunityName AS EntityLocationName,
E.CommunityID AS EntityLocationID,
I.IndicatorName AS EntityName,
I.IndicatorID AS EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
E.CommunityProvinceEngagementNotes AS Notes,
E.PoliceEngagementRiskValue AS Value,
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') as PoliceEngagementRiskValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityIndicators/CommunityIndicator') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID

UNION ALL


SELECT

'Polic Engagement' AS ReportType,
'Province Indicator' AS SubReportType,
P.ProvinceName AS EntityLocationName,
E.ProvinceID AS EntityLocationID,
I.IndicatorName AS EntityName,
I.IndicatorID AS EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
E.CommunityProvinceEngagementNotes AS Notes,
E.PoliceEngagementRiskValue AS Value,
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT
P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') as PoliceEngagementRiskValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceIndicators/ProvinceIndicator') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID


UNION ALL

SELECT

'Community Engagement' AS ReportType,
'Community Indicator' AS SubReportType,
C.CommunityName AS EntityLocationName,
E.CommunityID AS EntityLocationID,
I.IndicatorName AS EntityName,
I.IndicatorID AS EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
E.CommunityProvinceEngagementNotes AS Notes, 
E.CommunityProvinceEngagementAchievedValue AS Value,
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('CommunityProvinceEngagementAchievedValue[1]', 'nvarchar(max)') as CommunityProvinceEngagementAchievedValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityIndicators/CommunityIndicator') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID



UNION ALL




SELECT

'Community Engagement' AS ReportType,
'Province Indicator' AS SubReportType,
P.ProvinceName AS EntityLocationName,
E.ProvinceID AS EntityLocationID,
I.IndicatorName AS EntityName,
I.IndicatorID AS EntityID,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
E.CommunityProvinceEngagementNotes AS Notes, 
E.CommunityProvinceEngagementAchievedValue AS Value,
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT
P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('IndicatorID[1]', 'nvarchar(max)') AS IndicatorID,
P.value('CommunityProvinceEngagementAchievedValue[1]', 'nvarchar(max)') as CommunityProvinceEngagementAchievedValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
 E.PersonID , 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceIndicators/ProvinceIndicator') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN logicalframework.Indicator I ON I.IndicatorID = E.IndicatorID
) D

ORDER BY D.ReportType ,D.SubReportType , D.EntityLocationName, D.EntityName, D.Author , D.UpdateDate
END
GO
--End procedure reporting.IndicatorReport

--Begin procedure reporting.RecommendationReport
EXEC Utility.DropObject 'reporting.RecommendationReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure to get CommunityEngagementReport data from the eventlog table
-- =============================================================================================
CREATE PROCEDURE reporting.RecommendationReport

@StartDate DATE, 
@EndDate DATE

AS
BEGIN
	

SELECT DISTINCT D.ReportType, D.SubreportType,D.EntityLocationName, D.EntityName, D.Author, D.Notes, D.Value, D.Updatedate
FROM
(


SELECT 
 'Police Engagement' AS ReportType,
  'Province Recommnedation' AS SubReportType,
C.CommunityName AS EntityLocationName,
E.CommunityID AS EntityLocationID,
I.RecommendationName AS EntityName,
I.RecommendationID AS EntityID,
E.PoliceEngagementRiskNotes AS Notes,
E.PoliceEngagementRiskValue AS Value,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('RecommendationID[1]', 'nvarchar(max)') AS RecommendationID,
P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
E.PersonID , 
E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityRecommendations/CommunityRecommendation') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN Recommendation.Recommendation I ON I.RecommendationID = E.RecommendationID

UNION ALL

SELECT 
 'Police Engagement' AS ReportType,
  'Province Recommnedation' AS SubReportType,
P.ProvinceName AS EntityLocationName,
E.ProvinceID AS EntityLocationID,
I.RecommendationName AS EntityName,
I.RecommendationID AS EntityID,
E.PoliceEngagementRiskNotes AS Notes,
E.PoliceEngagementRiskValue AS Value,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT
P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('RecommendationID[1]', 'nvarchar(max)') AS RecommendationID,
P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
E.PersonID , 
E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceRecommendations/ProvinceRecommendation') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN Recommendation.Recommendation I ON I.RecommendationID = E.RecommendationID

UNION ALL
SELECT

'Community Engagement' AS ReportType,
'Community Recommendation' AS SubReportType,
C.CommunityName AS EntityLocationName,
E.CommunityID AS EntityLocationID,
I.RecommendationName AS EntityName,
I.RecommendationID AS EntityID,
E.CommunityProvinceEngagementNotes AS Notes, 
E.CommunityProvinceEngagementAchievedValue AS Value,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT
P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
P.value('RecommendationID[1]', 'nvarchar(max)') AS RecommendationID,
'-' as CommunityProvinceEngagementAchievedValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
E.PersonID, 
 E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Community/CommunityRecommendations/CommunityRecommendation') t(p)

) E
JOIN Community C ON C.CommunityID = E.CommunityID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN Recommendation.Recommendation I ON I.RecommendationID = E.RecommendationID



UNION ALL




SELECT
'Community Engagement' AS ReportType,
'Province Recommendation' AS SubReportType,
P.ProvinceName AS EntityLocationName,
E.ProvinceID AS EntityLocationID,
I.RecommendationName AS EntityName,
I.RecommendationID AS EntityID,
E.CommunityProvinceEngagementNotes AS Notes, 
E.CommunityProvinceEngagementAchievedValue AS Value,
dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate

FROM
(
SELECT

P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
P.value('RecommendationID[1]', 'nvarchar(max)') AS RecommendationID,
'-' as CommunityProvinceEngagementAchievedValue,
P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes, 
E.PersonID,
E.CreateDateTime

FROM eventlog.EventLog E
CROSS APPLY E.EventData.nodes('/Province/ProvinceRecommendations/ProvinceRecommendation') t(p)

) E
JOIN Province P ON P.ProvinceID = E.ProvinceID AND E.CreateDateTime Between @StartDate AND @EndDate
JOIN Recommendation.Recommendation I ON I.RecommendationID = E.RecommendationID


) D

ORDER BY D.ReportType ,D.SubReportType , D.EntityLocationName, D.EntityName, D.Author , D.UpdateDate
END

GO
--End procedure reporting.RecommendationReport

--Begin procedure reporting.RiskReport
EXEC Utility.DropObject 'reporting.RiskReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			John Lyons
-- Create date: 2015.08.25
-- Description:	A stored procedure to get CommunityEngagementReport data from the eventlog table
-- =============================================================================================
CREATE PROCEDURE reporting.RiskReport

@StartDate DATE, 
@EndDate DATE

AS
BEGIN

	SELECT DISTINCT 
		D.ReportType, 
		D.SubreportType,
		D.EntityLocationName, 
		D.EntityName, 
		D.Author, 
		D.Notes, 
		D.Value, 
		D.Updatedate
	FROM
		(
		SELECT 
			'Police Engagement' AS ReportType,
			'Community Risk' AS SubReportType,
			C.CommunityName AS EntityLocationName,
			E.CommunityID AS EntityLocationID,
			r.RiskName AS EntityName,
			r.RiskID AS EntityID,
			E.PoliceEngagementRiskNotes AS Notes,
			E.PoliceEngagementRiskValue AS Value,
			dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author,
			dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate
		FROM 
			(
			SELECT

		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
		 P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
				E.PersonID , 
				E.CreateDateTime
			FROM eventlog.EventLog E
				CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
			) E
		JOIN Community C ON C.CommunityID = E.CommunityID 
			AND E.CreateDateTime BETWEEN @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID

		UNION ALL

		SELECT 
			'Police Engagement' AS ReportType,
			'Province Risk' AS SubReportType,
			P.ProvinceName AS EntityLocationName,
			E.ProvinceID AS EntityLocationID,
			r.RiskName AS EntityName , 
			r.RiskID AS EntityID , 
			E.PoliceEngagementRiskNotes AS Notes,
			E.PoliceEngagementRiskValue AS Value,
			dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author,
			dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate
		FROM 
			(
			SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('PoliceEngagementRiskNotes[1]', 'nvarchar(max)') AS PoliceEngagementRiskNotes,
		 P.value('PoliceEngagementRiskValue[1]', 'nvarchar(max)') AS PoliceEngagementRiskValue,
				 E.PersonID , 
				 E.CreateDateTime
			FROM eventlog.EventLog E
				CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
			) E
		JOIN Province P ON P.ProvinceID = E.ProvinceID 
			AND E.CreateDateTime BETWEEN @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID

		UNION ALL

		SELECT 
			'Community Engagement' AS ReportType,
			'Province Risk' AS SubReportType,
			P.ProvinceName AS EntityLocationName,
			E.ProvinceID AS EntityLocationID,
			r.RiskName AS EntityName , 
			r.RiskID AS EntityID , 
			E.CommunityProvinceEngagementNotes AS Notes,
			E.CommunityProvinceEngagementRiskValue AS Value,
			dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author ,  
			dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate
		FROM 
			(
			SELECT

		 P.value('ProvinceID[1]', 'nvarchar(max)') AS ProvinceID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes,
		 P.value('CommunityProvinceEngagementRiskValue[1]', 'nvarchar(max)') AS CommunityProvinceEngagementRiskValue,
				 E.PersonID , 
				 E.CreateDateTime
			FROM eventlog.EventLog E
				CROSS APPLY E.EventData.nodes('/Province/ProvinceRisks/ProvinceRisk') t(p)
			) E
		JOIN Province P ON P.ProvinceID = E.ProvinceID 
			AND E.CreateDateTime BETWEEN @StartDate AND @EndDate
		JOIN Risk R ON R.RiskID = E.RiskID

		UNION ALL

		SELECT
			'Community Engagement' AS ReportType,
			'Community Risk' AS SubReportType,
			C.CommunityName AS EntityLocationName,
			E.CommunityID AS EntityLocationID,
			r.RiskName AS EntityName , 
			r.RiskID AS EntityID , 
			E.CommunityProvinceEngagementNotes AS Notes, 
			E.CommunityProvinceEngagementRiskValue AS Value,
			dbo.FormatPersonNameByPersonID(E.PersonID, 'FirstLast') AS Author , 
			dbo.FormatDateTime( E.CreateDateTime) AS UpdateDate
		FROM
			(
			SELECT
		 P.value('CommunityID[1]', 'nvarchar(max)') AS CommunityID,
		 P.value('RiskID[1]', 'nvarchar(max)') AS RiskID,
		 P.value('CommunityProvinceEngagementNotes[1]', 'nvarchar(max)') AS CommunityProvinceEngagementNotes,
		 P.value('CommunityProvinceEngagementRiskValue[1]', 'nvarchar(max)') AS CommunityProvinceEngagementRiskValue,
				E.PersonID , 
				E.CreateDateTime
			FROM eventlog.EventLog E
				CROSS APPLY E.EventData.nodes('/Community/CommunityRisks/CommunityRisk') t(p)
			) E
			JOIN Community C ON C.CommunityID = E.CommunityID 
				AND E.CreateDateTime BETWEEN @StartDate AND @EndDate
			JOIN Risk R ON R.RiskID = E.RiskID
			) D

	ORDER BY D.ReportType ,D.SubReportType , D.EntityLocationName, D.EntityName, D.Author , D.UpdateDate

END
GO
--End procedure reporting.RiskReport

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityProvinceEngagementReport', @NewMenuItemLink='/communityprovinceengagement/export', @NewMenuItemText='Engagement Report', @ParentMenuItemCode='Admin', @AfterMenuItemCode='DownloadRAPData'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.Role R WHERE R.RoleName = 'FIF')
	BEGIN
	
	INSERT INTO dropdown.Role
		(RoleName)
	VALUES
		('FIF'),
		('RAP')

	END
--ENDIF
GO
--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.37 File 01 - AJACS - 2015.11.05 20.14.35')
GO
--End build tracking

