USE AJACS
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CommunityProvinceEngagementReport', @NewMenuItemLink='/communityprovinceengagement/export', @NewMenuItemText='Engagement Report', @ParentMenuItemCode='Admin', @AfterMenuItemCode='DownloadRAPData'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.Role R WHERE R.RoleName = 'FIF')
	BEGIN
	
	INSERT INTO dropdown.Role
		(RoleName)
	VALUES
		('FIF'),
		('RAP')

	END
--ENDIF
GO