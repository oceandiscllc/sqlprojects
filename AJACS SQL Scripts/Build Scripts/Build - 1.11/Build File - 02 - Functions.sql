USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForWeeklyReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
--
-- Author:			John Lyons & Todd Pires
-- Create date:	2015.05.09
-- Description:	Implemented the date range and icon grouping
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForWeeklyReport
(
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=600x480'
	DECLARE @cMarkers VARCHAR(MAX) = ''
	
	;
	WITH D AS
		(
		SELECT
			REPLACE(ID.HexColor, '#', '') +  '.png' AS Icon,
			LEFT(CAST(C.Latitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(C.Longitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM WeeklyReport.Community WRC
		JOIN dbo.Community C ON WRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.ProvinceID = @ProvinceID
	
		UNION
	
		SELECT
			it.icon AS Icon,
			LEFT(CAST(I.Latitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(I.Longitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM dbo.Incident I
		JOIN Dropdown.IncidentType IT ON i.incidenttypeid = it.incidenttypeid
			AND I.IncidentDate >= (SELECT TOP 1 weeklyreport.WeeklyReport.StartDate FROM weeklyreport.WeeklyReport)
			AND I.IncidentDate <= (SELECT TOP 1 weeklyreport.WeeklyReport.EndDate FROM weeklyreport.WeeklyReport)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.IncidentID = I.IncidentID 
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Community C 
						WHERE C.CommunityID = IC.CommunityID AND C.ProvinceID = @ProvinceID
						)
	
				UNION
	
				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.IncidentID = I.IncidentID AND  IP.ProvinceID = @ProvinceID
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Province P 
						WHERE P.ProvinceID = IP.ProvinceID AND P.ProvinceID = @ProvinceID
						)
				)
		)
	
	SELECT @cMarkers +=
		'&markers=icon:' 
		+ REPLACE(dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/'
		+ E.Icon
		+ (
			SELECT '|' + D.LatLong 
			FROM D
			WHERE D.Icon = E.Icon
			FOR XML PATH(''), TYPE
			).value('.[1]', 'VARCHAR(MAX)')
	FROM D AS E
	GROUP BY E.Icon
	ORDER BY E.Icon
	
	RETURN (@cLink + @cMarkers)

END
GO
--End function dbo.FormatStaticGoogleMapForWeeklyReport
