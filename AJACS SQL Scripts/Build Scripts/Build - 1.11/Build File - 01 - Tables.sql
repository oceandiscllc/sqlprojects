USE AJACS
GO

--Begin table dbo.ConceptNoteEquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteEquipmentCatalog'

EXEC utility.AddColumn @TableName, 'BudgetSubTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'BudgetSubTypeID', 'INT', 0
GO
--End table dbo.ConceptNoteEquipmentCatalog
