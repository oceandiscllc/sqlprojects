USE AJACS
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ServerSetup', @NewMenuItemText='Server Setup Keys', @NewMenuItemLink='/serversetup/list', @ParentMenuItemCode='Admin', @AfterMenuItemCode='PersonList', @PermissionableLineageList='ServerSetup.List', @IsActive=1
GO
--End table dbo.MenuItem
