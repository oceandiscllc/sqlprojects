USE AJACS
GO

--Begin procedure dbo.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteBudget table
--
-- Author:			Greg Yingling
-- Update Date: 2015.05.02
-- Description:	Added BudgetSubType information
-- ================================================================================
CREATE PROCEDURE dbo.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteBudgetByConceptNoteID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================

-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.Summary,
		CN.Title,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CourseID,
		C.CourseName
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Community COM on COM.CommunityID = C.CommunityID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetDocumentByDocumentID
EXEC Utility.DropObject 'dbo.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	A stored procedure to get data from the dbo.Document table
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentByDocumentID

@DocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.DocumentID, 
		D.DocumentDate, 
		dbo.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentName, 
		D.DocumentTitle, 
		D.DocumentDescription, 
		D.DocumentTypeID, 
		D.PhysicalFilePath, 
		D.PhysicalFileName, 
		D.PhysicalFileExtension, 
		D.PhysicalFileSize, 
		D.ContentType, 
		D.ContentSubtype
  FROM dbo.Document D
	WHERE D.DocumentID = @DocumentID

	SELECT 
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.DocumentEntity DE ON DE.EntityID = P.ProvinceID
			AND DE.EntityTypeCode = 'Province'
			AND DE.DocumentID = @DocumentID

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.Community C
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dbo.DocumentEntity DE ON DE.EntityID = C.CommunityID
			AND DE.EntityTypeCode = 'Community'
			AND DE.DocumentID = @DocumentID
	
END
GO
--End procedure dbo.GetDocumentByDocumentID

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	Tweaked the OAWR.PhysicalFileName IS NOT NULL statement
-- ====================================================================
CREATE PROCEDURE dbo.GetDonorFeed

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		MI.Icon,
		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		OAWR.PhysicalFileName,
		D.EntityID,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
					AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE LEFT(DOC.DocumentTitle, 14) = dbo.FormatWeeklyReportReferenceCode(D.EntityID)
			) OAWR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
		JOIN dbo.MenuItem MI ON MI.MenuItemCode = 
			CASE
				WHEN D.EntityTypeCode = 'WeeklyReport'
				THEN 'ManageWeeklyReports'
				ELSE D.EntityTypeCode
			END
			
			AND 
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)

			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
	ORDER BY D.UpdateDate DESC, D.EntityTypeCode, D.EntityID
		
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dbo.GetServerSetupDataByServerSetupID
EXEC Utility.DropObject 'dbo.GetServerSetupDataByServerSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.12
-- Description:	A stored procedure to get data from the AJACSUtility.dbo.ServerSetup table
-- =======================================================================================
CREATE PROCEDURE dbo.GetServerSetupDataByServerSetupID

@ServerSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.ServerSetupID, 
		SS.ServerSetupKey, 
		SS.ServerSetupValue 
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupID = @ServerSetupID

END
GO
--End procedure dbo.GetServerSetupDataByServerSetupID
		
--Begin procedure reporting.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.06
-- Description:	A stored procedure to get data for the Concept Note Budget Export
-- ==============================================================================
CREATE PROCEDURE reporting.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CNBD AS
		(
		SELECT
			BT.BudgetTypeName,
			BST.BudgetSubTypeName,
			CNB.ConceptNoteBudgetID,
			CNB.ItemName,
			CNB.ItemDescription,
			CNB.UnitCost,
			FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNB.Quantity,
			FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted
		FROM dbo.ConceptNoteBudget CNB
				JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
				JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
					AND CNB.conceptnoteid = @ConceptNoteID
		)

	SELECT
		BTS.BudgetTypeSequence,
		BSS.BudgetSubTypeSequence,
		ROW_NUMBER() OVER (PARTITION BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName ORDER BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName, CNBD.ItemName, CNBD.Quantity * CNBD.UnitCost, CNBD.ConceptNoteBudgetID) AS ItemSequence,
		CNBD.BudgetTypeName, 
		CNBD.BudgetSubTypeName,
		CNBD.ItemName,
		CNBD.ItemDescription,
		CNBD.UnitCostFormatted,
		CNBD.Quantity,
		CNBD.TotalCostFormatted
	FROM CNBD
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (ORDER BY BTN.BudgetTypeName) AS BudgetTypeSequence,
				BTN.BudgetTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName 
				FROM CNBD
				) BTN
			) BTS ON BTS.BudgetTypeName = CNBD.BudgetTypeName
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY BSN.BudgetTypeName ORDER BY BSN.BudgetTypeName, BSN.BudgetSubTypeName) AS BudgetSubTypeSequence,
				BSN.BudgetTypeName,
				BSN.BudgetSubTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName,
					CNBD.BudgetSubTypeName
				FROM CNBD
				) BSN
			) BSS ON BSS.BudgetTypeName = CNBD.BudgetTypeName
				AND BSS.BudgetSubTypeName = CNBD.BudgetSubTypeName

END
GO
--End procedure reporting.GetConceptNoteBudgetByConceptNoteID

--Begin procedure reporting.GetWeeklyReport
EXEC Utility.DropObject 'reporting.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	A stored procedure to get data for the weekly report
-- =================================================================
CREATE PROCEDURE reporting.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode--,
		--dbo.FormatStaticGoogleMapForWeeklyReport() AS MapImage
	FROM weeklyreport.WeeklyReport WR 
	ORDER BY WR.WeeklyReportID DESC

END
GO
--End procedure reporting.GetWeeklyReport

--Begin procedure reporting.GetWeeklyReportCommunity
EXEC Utility.DropObject 'reporting.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.03.06
-- Description:	Changed the CommunityReportStatusName logic
--
-- Author:			Todd Pires
-- Update date:	2015.05.06
-- Description:	Repointed the reference code at the weeklyreport.WeeklyReport table
-- ================================================================================
CREATE PROCEDURE reporting.GetWeeklyReportCommunity

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		C.WeeklyReportID,	
		reporting.IsDraftReport('WeeklyReport', C.WeeklyReportID) AS IsDraft,
		CES.CommunityEngagementStatusName,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,

		CASE
			WHEN C.ImpactDecisionID IN (0,1,2,3,5)
			THEN 'Watch'
			WHEN C.ImpactDecisionID IN (4) 
			THEN 'Engage'
			ELSE 'Alert'			
		END AS CommunityReportStatusName	

	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure reporting.GetWeeklyReportCommunity

--Begin procedure reporting.GetWeeklyReportProvince
EXEC Utility.DropObject 'reporting.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.04.27
-- Description:	Added the dbo.FormatProvinceIncidentsForGoogleMap call
--
-- Author:			Todd Pires
-- Update date:	2015.05.06
-- Description:	Repointed the reference code at the weeklyreport.WeeklyReport table
-- ================================================================================
CREATE PROCEDURE reporting.GetWeeklyReportProvince

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceName, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		P.WeeklyReportID,
		reporting.IsDraftReport('WeeklyReport', P.WeeklyReportID) AS IsDraft,
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,
		dbo.FormatStaticGoogleMapForWeeklyReport(P.ProvinceID) AS MapImage
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
	ORDER BY P.ProvinceName, P.ProvinceID

END
GO
--End procedure reporting.GetWeeklyReportProvince

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.WeeklyReportID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O
		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.WorkflowStepNumber 
	FROM weeklyreport.WeeklyReport WR

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
			JOIN weeklyreport.WeeklyReport WR ON WR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'WeeklyReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @nWeeklyReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'WeeklyReport'
			AND WSWA.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @nWeeklyReportID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin procedure weeklyreport.PopulateWeeklyReportCommunities
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add community data to the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportCommunities

@CommunityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Community
		(CommunityID, CommunityName, ProvinceID, CommunityEngagementStatusID, ImpactDecisionID, StatusChangeID, Summary, KeyPoints, Implications, RiskMitigation, WeeklyReportID)
	SELECT
		C.CommunityID,
		C.CommunityName,
		C.ProvinceID,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID, 
		C.StatusChangeID, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Community C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Community WRC
				WHERE WRC.CommunityID = C.CommunityID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportCommunities

--Begin procedure weeklyreport.PopulateWeeklyReportProvinces
EXEC Utility.DropObject 'weeklyreport.PopulateWeeklyReportProvinces'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to add Province data to the weekly report
-- ==========================================================================
CREATE PROCEDURE weeklyreport.PopulateWeeklyReportProvinces

@ProvinceIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Province
		(ProvinceID, ProvinceName, ImpactDecisionID, StatusChangeID, Summary, KeyPoints, Implications, RiskMitigation, WeeklyReportID)
	SELECT
		P.ProvinceID,
		P.ProvinceName,
		P.ImpactDecisionID, 
		P.StatusChangeID, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Province P
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Province WRP
				WHERE WRP.ProvinceID = P.ProvinceID
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportProvinces
