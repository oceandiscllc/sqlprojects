-- File Name:	Build-1.18 File 01 - AJACS.sql
-- Build Key:	Build-1.18 File 01 - AJACS - 2015.07.01 22.58.43

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatConceptNoteTitle
--		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
--		dbo.GetProvinceIDByCommunityID
--		eventlog.GetContactContactAffiliationsXMLByContactID
--		eventlog.GetContactContactTypesXMLByContactID
--		eventlog.GetContactStipendPaymentsXMLByContactID
--		eventlog.GetContactVettingsXMLByContactID
--
-- Procedures:
--		dbo.GetCommunityEquipmentInventory
--		dbo.GetConceptNoteBudgetByConceptNoteID
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetConceptNoteEquipmentByConceptNoteID
--		dbo.GetDocumentsByEntityTypeCodeAndEntityID
--		dbo.GetProvinceEquipmentInventory
--		dropdown.GetAuditOutcomeData
--		dropdown.GetPaymentGroupData
--		eventlog.LogContactAction
--		eventlog.LogEquipmentDistributionPlanAction
--		eventlog.LogEquipmentInventoryAction
--		procurement.GetEquipmentCatalogByEquipmentCatalogID
--		procurement.GetEquipmentDistributionPlanByConceptNoteID
--		procurement.GetEquipmentInventoryByEquipmentInventoryID
--		reporting.GetCashHandoverReport
--		reporting.GetCommunityEquipmentDistributionPlanByConceptNoteID
--		reporting.GetConceptNoteAmmendmentByConceptNoteID
--		reporting.GetConceptNoteAuthorByConceptNoteID
--		reporting.GetConceptNoteBudgetByConceptNoteID
--		reporting.GetConceptNoteCloseoutByConceptNoteID
--		reporting.GetConceptNoteIndicatorByConceptNoteID
--		reporting.GetContact
--		reporting.GetEquipmentDistributionPlanByConceptNoteID
--		reporting.GetEquipmentDistributionPlanCommunitiesByConceptnoteID
--		reporting.GetOpsFundReport
--		reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
--		reporting.GetStipendPaymentReport
--		utility.ServerSetupKeyAddUpdate
--		workflow.GetEquipmentDistributionPlanWorkflowStepPeople
--
-- Tables:
--		dbo.ConceptNoteAmmendment
--		dbo.ConceptNoteAuthor
--		dropdown.AuditOutcome
--		procurement.EquipmentAuditOutcome
--		procurement.EquipmentDistributionPlan
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ContactImportID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ContactImportID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'ContactImportID', 'INT'

EXEC utility.AddColumn @TableName, 'ActualTotalAmount', 'NUMERIC(18,2)'
EXEC utility.AddColumn @TableName, 'DeobligatedAmount', 'NUMERIC(18,2)'
EXEC utility.AddColumn @TableName, 'DescriptionOfImpact', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FemaleAdultCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'FemaleAdultDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FemaleYouthCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'FemaleYouthDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FinalAwardAmount', 'NUMERIC(18,2)'
EXEC utility.AddColumn @TableName, 'FinalReportDate', 'DATE'
EXEC utility.AddColumn @TableName, 'MaleAdultCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'MaleAdultDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MaleYouthCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'MaleYouthDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SuccessStories', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SummaryOfBackground', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SummaryOfImplementation', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'ActualTotalAmount', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeobligatedAmount', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleAdultCountActual', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleYouthCountActual', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FinalAwardAmount', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'MaleAdultCountActual', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'MaleYouthCountActual', 'INT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteAmmendment
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteAmmendment'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteAmmendment
	(
	ConceptNoteAmmendmentID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	AmmendmentNumber VARCHAR(100),
	Date DATE,
	Description VARCHAR(MAX),
	Cost NUMERIC(18, 2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Cost', 'NUMERIC(18, 2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteAmmendmentID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteAmmendment', @TableName, 'ConceptNoteID,AmmendmentNumber,Date'
GO
--End table dbo.ConceptNoteAmmendment

--Begin table dbo.ConceptNoteAuthor
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteAuthor'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteAuthor
	(
	ConceptNoteAuthorID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteAuthorID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteAuthor', @TableName, 'ConceptNoteID,PersonID'
GO
--End table dbo.ConceptNoteAuthor

--Begin table dbo.ConceptNoteIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteIndicator'

EXEC utility.AddColumn @TableName, 'ActualNumber', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ActualNumber', 'INT', 0
GO
--End table dbo.ConceptNoteIndicator

--Begin table dbo.ContactStipendPayment
DECLARE @TableName VARCHAR(250) = 'dbo.ContactStipendPayment'

EXEC utility.DropObject 'dbo.Payment'

EXEC Utility.DropColumn @TableName, 'PaymentDate'

EXEC utility.AddColumn @TableName, 'DocumentID', 'INT'
EXEC utility.AddColumn @TableName, 'PaymentMonth', 'INT'
EXEC utility.AddColumn @TableName, 'PaymentYear', 'INT'
EXEC utility.AddColumn @TableName, 'StipendAuthorizedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'StipendPaidDate', 'DATE'

EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentMonth', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentYear', 'INT', 0

EXEC utility.DropIndex @TableName, 'IX_ContactStipendPayment'
EXEC utility.SetIndexClustered 'IX_ContactStipendPayment', @TableName, 'PaymentYear DESC,PaymentMonth DESC, ContactID'
GO
--End table dbo.ContactStipendPayment

--Begin table dropdown.AuditOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.AuditOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AuditOutcome
	(
	AuditOutcomeID INT IDENTITY(0,1) NOT NULL,
	AuditOutcomeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AuditOutcomeID'
EXEC utility.SetIndexNonClustered 'IX_AuditOutcomeName', @TableName, 'DisplayOrder,AuditOutcomeName', 'AuditOutcomeID'
GO

SET IDENTITY_INSERT dropdown.AuditOutcome ON
GO

INSERT INTO dropdown.AuditOutcome 
	(AuditOutcomeID, AuditOutcomeName, DisplayOrder) 
VALUES 
	(0, 'Not Audited', 1),
	(1, 'Verified - Good', 2),
	(2, 'Verified - Damaged', 3),
	(3, 'Verified - Destroyed', 4),
	(4, 'Not Verified - Believed Good', 5),
	(5, 'Not Verified - Believed Missing', 6),
	(6, 'Not Verified - Believed Destroyed', 7)
GO

SET IDENTITY_INSERT dropdown.AuditOutcome OFF
GO
--End table dropdown.AuditOutcome
 
--Begin table procurement.EquipmentAuditOutcome
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentAuditOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentAuditOutcome
	(
	EquipmentAuditOutcomeID INT IDENTITY(1,1),
	EquipmentInventoryID INT,
	AuditOutcomeID INT,
	EquipmentAuditOutcomeDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentAuditOutcomeID'
EXEC utility.SetIndexClustered 'IX_EquipmentAuditOutcome', @TableName, 'EquipmentInventoryID,AuditOutcomeID'
GO
--End table procurement.EquipmentAuditOutcome

--Begin table procurement.EquipmentDistributionPlan
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistributionPlan'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentDistributionPlan
	(
	EquipmentDistributionPlanID INT IDENTITY(1,1),
	ConceptNoteID INT,
	ExportRoute VARCHAR(MAX),
	CurrentSituation VARCHAR(MAX),
	Aim VARCHAR(MAX),
	PlanOutline VARCHAR(MAX),
	Phase1 VARCHAR(MAX),
	Phase2 VARCHAR(MAX),
	Phase3 VARCHAR(MAX),
	Phase4 VARCHAR(MAX),
	Phase5 VARCHAR(MAX),
	OperationalResponsibility VARCHAR(MAX),
	Annexes VARCHAR(MAX),
	Distribution VARCHAR(MAX),
	Title VARCHAR(250),
	Summary VARCHAR(MAX),
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentDistributionPlanID'
GO
--End table procurement.EquipmentDistributionPlan

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'AuditDate', 'DATE'
EXEC utility.AddColumn @TableName, 'AuditOutcomeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
GO
--End table procurement.EquipmentInventory

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
--
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	Added ISNULL support
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(300)
	
	SELECT @cRetVal = 'AJACS-AS-' + RIGHT('0000' + CAST(CN.ConceptNoteID AS VARCHAR(10)), 4) + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID
	
	RETURN RTRIM(LTRIM(ISNULL(@cRetVal, '')))

END
GO
--End function dbo.FormatConceptNoteTitle

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@EquipmentDistributionPlanID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(C.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(C.Longitude AS VARCHAR(MAX))
	 FROM dbo.ConceptNoteCommunity CNC
		JOIN procurement.EquipmentDistributionPlan EDP ON EDP.ConceptNoteID = CNC.ConceptNoteID
			AND EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID
		JOIN dbo.Community C ON CNC.CommunityID = C.CommunityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan

--Begin function dbo.GetProvinceIDByCommunityID
EXEC utility.DropObject 'dbo.GetProvinceIDByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.26
-- Description:	A function to return the province id associated with a community id
-- ================================================================================

CREATE FUNCTION dbo.GetProvinceIDByCommunityID
(
@CommunityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nRetVal INT
	
	SELECT @nRetVal = C.ProvinceID
	FROM dbo.Community C 
	WHERE C.CommunityID = @CommunityID
	
	RETURN ISNULL(@nRetVal, 0)

END
GO
--End function dbo.GetProvinceIDByCommunityID

--Begin function eventlog.GetContactContactAffiliationsXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactContactAffiliationsXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactContactAffiliation data for a specific contact record
-- ==============================================================================================

CREATE FUNCTION eventlog.GetContactContactAffiliationsXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactContactAffiliations VARCHAR(MAX) = ''
	
	SELECT @cContactContactAffiliations = COALESCE(@cContactContactAffiliations, '') + D.ContactContactAffiliation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactContactAffiliation'), ELEMENTS) AS ContactContactAffiliation
		FROM dbo.ContactContactAffiliation T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactContactAffiliations>' + ISNULL(@cContactContactAffiliations, '') + '</ContactContactAffiliations>'

END
GO
--End function eventlog.GetContactContactAffiliationsXMLByContactID

--Begin function eventlog.GetContactContactTypesXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactContactTypesXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactContactType data for a specific contact record
-- =======================================================================================

CREATE FUNCTION eventlog.GetContactContactTypesXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactContactTypes VARCHAR(MAX) = ''
	
	SELECT @cContactContactTypes = COALESCE(@cContactContactTypes, '') + D.ContactContactType
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactContactType'), ELEMENTS) AS ContactContactType
		FROM dbo.ContactContactType T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactContactTypes>' + ISNULL(@cContactContactTypes, '') + '</ContactContactTypes>'

END
GO
--End function eventlog.GetContactContactTypesXMLByContactID

--Begin function eventlog.GetContactStipendPaymentsXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactStipendPaymentsXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactStipendPayment data for a specific contact record
-- ==========================================================================================

CREATE FUNCTION eventlog.GetContactStipendPaymentsXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactStipendPayments VARCHAR(MAX) = ''
	
	SELECT @cContactStipendPayments = COALESCE(@cContactStipendPayments, '') + D.ContactStipendPayment
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactStipendPayment'), ELEMENTS) AS ContactStipendPayment
		FROM dbo.ContactStipendPayment T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactStipendPayments>' + ISNULL(@cContactStipendPayments, '') + '</ContactStipendPayments>'

END
GO
--End function eventlog.GetContactStipendPaymentsXMLByContactID

--Begin function eventlog.GetContactVettingsXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactVettingsXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactVetting data for a specific contact record
-- ===================================================================================

CREATE FUNCTION eventlog.GetContactVettingsXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactVettings VARCHAR(MAX) = ''
	
	SELECT @cContactVettings = COALESCE(@cContactVettings, '') + D.ContactVetting
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactVetting'), ELEMENTS) AS ContactVetting
		FROM dbo.ContactVetting T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactVettings>' + ISNULL(@cContactVettings, '') + '</ContactVettings>'

END
GO
--End function eventlog.GetContactVettingsXMLByContactID
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteBudget table
--
-- Author:			Greg Yingling
-- Update Date: 2015.05.02
-- Description:	Added BudgetSubType information
-- ================================================================================
CREATE PROCEDURE dbo.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteBudgetByConceptNoteID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID


	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetConceptNoteEquipmentByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteEquipmentCatalog table
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
-- ==========================================================================================
CREATE PROCEDURE dbo.GetConceptNoteEquipmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.UnitOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteEquipmentByConceptNoteID

--Begin procedure dbo.GetCommunityEquipmentInventory
EXEC Utility.DropObject 'dbo.GetCommunityEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.CommunityEquipmentInventory table
-- =========================================================================================
CREATE PROCEDURE dbo.GetCommunityEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(OACN.ConceptNoteID) AS Title
	FROM
		(
		SELECT
			SUM(T.Quantity) AS Quantity,
			MAX(T.DistributionDate) AS DistributionDate,
			T.EquipmentInventoryID,
			T.ConceptNoteID
		FROM procurement.CommunityEquipmentInventory T
		WHERE T.CommunityID = @EntityID
		GROUP BY T.ConceptNoteID, T.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = D.ConceptNoteID
			) OACN
	ORDER BY 4, 1, 3
	
END
GO
--End procedure dbo.GetCommunityEquipmentInventory

--Begin procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A stored procedure to get data from the dbo.Document table
--
-- Author:			Todd Pires
-- Create date:	2015.05.21
-- Description:	Modified to support weekly reports
-- ========================================================================
CREATE PROCEDURE dbo.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.ContentSubtype,
		D.ContentType
	FROM dbo.Document D
	WHERE 
		(@EntityTypeCode = 'SpotReport' AND D.DocumentTitle = dbo.FormatSpotReportReferenceCode(@EntityID))
			OR (@EntityTypeCode = 'WeeklyReport' AND D.DocumentTitle = dbo.FormatWeeklyReportReferenceCode(@EntityID))
	
	UNION

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.ContentSubtype,
		D.ContentType
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID

	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure dbo.GetProvinceEquipmentInventory
EXEC Utility.DropObject 'dbo.GetProvinceEquipmentInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.28
-- Description:	A stored procedure to get data from the dbo.ProvinceEquipmentInventory table
-- =========================================================================================
CREATE PROCEDURE dbo.GetProvinceEquipmentInventory

@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(D.DistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(OACN.ConceptNoteID) AS Title
	FROM
		(
		SELECT
			SUM(T.Quantity) AS Quantity,
			MAX(T.DistributionDate) AS DistributionDate,
			T.EquipmentInventoryID,
			T.ConceptNoteID
		FROM procurement.ProvinceEquipmentInventory T
		WHERE T.ProvinceID = @EntityID
		GROUP BY T.ConceptNoteID, T.EquipmentInventoryID
		) D
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = D.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = D.ConceptNoteID
			) OACN
	ORDER BY 4, 1, 3
	
END
GO
--End procedure dbo.GetProvinceEquipmentInventory

--Begin procedure dropdown.GetAuditOutcomeData
EXEC Utility.DropObject 'dropdown.GetAuditOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.23
-- Description:	A stored procedure to return data from the dropdown.AuditOutcome table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetAuditOutcomeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AuditOutcomeID, 
		T.AuditOutcomeName
	FROM dropdown.AuditOutcome T
	WHERE (T.AuditOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AuditOutcomeName, T.AuditOutcomeID

END
GO
--End procedure dropdown.GetAuditOutcomeData

--Begin procedure dropdown.GetPaymentGroupData
EXEC Utility.DropObject 'dropdown.GetPaymentGroupData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.24
-- Description:	A stored procedure to return data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE dropdown.GetPaymentGroupData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LEFT(DATENAME(MONTH, DateAdd(MONTH, CAST(RIGHT(T.YearMonth, 2) AS INT), -1)), 3) + ' - ' + LEFT(T.YearMonth, 4) + ' - ' + T.ProvinceName AS YearMonthFormatted,
		T.YearMonth + '-' + CAST(T.ProvinceID AS VARCHAR(10)) AS YearMonthProvince
	FROM
		(
		SELECT DISTINCT
			CAST((CSP.PaymentYear * 100 + CSP.PaymentMonth) AS CHAR(6)) AS YearMonth, 
			P.ProvinceID,
			P.ProvinceName
		FROM dbo.ContactStipendPayment CSP
			JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		) T
	ORDER BY T.YearMonth DESC, 1

END
GO
--End procedure dropdown.GetPaymentGroupData

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Todd Pires
-- Create date: 2015.05.19
-- Description:	Added ContactType support
--
-- Author:		Todd Pires
-- Create date: 2015.06.19
-- Description:	Added ContactStipendPayment support
--
-- Author:		Todd Pires
-- Create date: 2015.06.29
-- Description:	Added EntityIDList support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT = 0,
@EventCode VARCHAR(50) = '',
@PersonID INT = 0,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			T.ContactID,
			@Comments
		FROM dbo.Contact T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.ContactID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetContactContactAffiliationsXMLByContactID(T.ContactID) AS XML)),
			(SELECT CAST(eventlog.GetContactContactTypesXMLByContactID(T.ContactID) AS XML)),
			(SELECT CAST(eventlog.GetContactStipendPaymentsXMLByContactID(T.ContactID) AS XML)),
			(SELECT CAST(eventlog.GetContactVettingsXMLByContactID(T.ContactID) AS XML))
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM dbo.Contact T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.ContactID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogContactStipendPaymentAction
EXEC utility.DropObject 'eventlog.LogPaymentAction'
EXEC utility.DropObject 'eventlog.LogContactStipendPaymentAction'
GO
--End procedure eventlog.LogContactStipendPaymentAction

--Begin procedure eventlog.LogEquipmentDistributionPlanAction
EXEC utility.DropObject 'eventlog.LogEquipmentDistributionPlanAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.06.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentDistributionPlanAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentDistributionPlan',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentDistributionPlan',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('EquipmentDistributionPlan'), ELEMENTS
			)
		FROM procurement.EquipmentDistributionPlan T
		WHERE T.EquipmentDistributionPlanID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentDistributionPlanAction

--Begin procedure eventlog.LogEquipmentInventoryAction
EXEC utility.DropObject 'eventlog.LogEquipmentInventoryAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Greg Yingling
-- Create date: 2015.06.25
-- Description:	Add audit support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentInventoryAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cEquipmentInventoryAuditOutcomes VARCHAR(MAX) 
	
		SELECT 
			@cEquipmentInventoryAuditOutcomes = COALESCE(@cEquipmentInventoryAuditOutcomes, '') + D.EquipmentInventoryAuditOutcomes 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('EquipmentInventoryAuditOutcomes'), ELEMENTS) AS EquipmentInventoryAuditOutcomes
			FROM procurement.EquipmentAuditOutcome T 
			WHERE T.EquipmentInventoryID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentInventory',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<EquipmentInventoryAuditOutcomes>' + ISNULL(@cEquipmentInventoryAuditOutcomes, '') + '</EquipmentInventoryAuditOutcomes>') AS XML)
			FOR XML RAW('EquipmentInventory'), ELEMENTS
			)
		FROM procurement.EquipmentInventory T
		WHERE T.EquipmentInventoryID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentInventoryAction

--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.17
-- Description:	Added Risk, Impact, and Mitigation Notes fields
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.EquipmentCatalogID, 
		EC.IsCommon,
		EC.ItemName, 
		EC.Notes, 
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.UnitOfIssue, 
		EC.QuantityOfIssue,
		ISNULL(EC.Risk, 1) AS Risk,
		ISNULL(EC.Impact, 1) AS Impact,
		EC.MitigationNotes, 
		ECC.EquipmentCatalogCategoryID, 
		ECC.EquipmentCatalogCategoryName, 
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentCatalog') AS EntityTypeName
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.EquipmentCatalogCategory ECC ON ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
		
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID

--Begin procedure procurement.GetEquipmentDistributionPlanByConceptNoteID
EXEC Utility.DropObject 'procurement.GetEquipmentDistributionPlanByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.06.28
-- Description:	A stored procedure to get data from the procurement.EquipmentDistributionPlan table
-- ================================================================================================
CREATE PROCEDURE procurement.GetEquipmentDistributionPlanByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EquipmentDistributionPlanID INT

	SELECT @EquipmentDistributionPlanID = EDP.EquipmentDistributionPlanID
	FROM procurement.EquipmentDistributionPlan EDP
	WHERE EDP.ConceptNoteID = @ConceptNoteID

	SELECT 
		EDP.EquipmentDistributionPlanID,
		EDP.ConceptNoteID,
		EDP.Title,
		EDP.Summary,
		EDP.ExportRoute,
		EDP.CurrentSituation,
		EDP.Aim,
		EDP.PlanOutline,
		EDP.Phase1,
		EDP.Phase2,
		EDP.Phase3,
		EDP.Phase4,
		EDP.Phase5,
		EDP.OperationalResponsibility,
		EDP.Annexes,
		EDP.Distribution,
		EDP.WorkflowStepNumber,
		CN.Title AS ConceptNoteTitle
	FROM procurement.EquipmentDistributionPlan EDP
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EDP.ConceptNoteID
			AND EDP.ConceptNoteID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'EquipmentDistributionPlan'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID) > 0
					THEN (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure procurement.GetEquipmentDistributionPlanByConceptNoteID

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================

-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
--
-- Author:			Greg Yingling
-- Update date:	2015.05.26
-- Description:	Added the Audit Outcome, Audit Date, and Audit Evidence Call
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.ExpirationDate,
		dbo.FormatDate(EI.ExpirationDate) AS ExpirationDateFormatted,
		EI.IMEIMACAddress,
		EI.UnitCost,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.EquipmentStatusID,
		ES.EquipmentStatusName,
		EI.EquipmentRemovalReasonID,
		ER.EquipmentRemovalReasonName,
		EI.EquipmentRemovalDate,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'EquipmentInventory'
			AND DE.EntityID = @EquipmentInventoryID
	ORDER BY DT.DocumentTypeName

	SELECT
		AO.AuditOutcomeName,
		EAO.EquipmentAuditOutcomeDate,
		dbo.FormatDate(EAO.EquipmentAuditOutcomeDate) AS EquipmentAuditOutcomeDateFormatted,
		D.DocumentName,
		D.PhysicalFileName
	FROM procurement.EquipmentAuditOutcome EAO 
		JOIN dropdown.AuditOutcome AO ON EAO.AuditOutcomeID = AO.AuditOutcomeID
		OUTER APPLY
			(
			SELECT
				D2.DocumentName,
				D2.PhysicalFileName
			FROM dbo.DocumentEntity DE
				JOIN dbo.Document D2 ON D2.DocumentID = DE.DocumentID
				JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D2.DocumentTypeID
					AND DE.EntityTypeCode = 'EquipmentInventoryAudit'
					AND DE.EntityID = EAO.EquipmentAuditOutcomeID
			) D 
	WHERE EquipmentInventoryID = @EquipmentInventoryID
	ORDER BY EquipmentAuditOutcomeDate DESC, EquipmentAuditOutcomeID DESC

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure reporting.GetCashHandoverReport
EXEC Utility.DropObject 'reporting.GetCashHandoverReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A stored procedure to data for the cash handover form
-- ==================================================================
CREATE PROCEDURE reporting.GetCashHandoverReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FullName VARCHAR(200) = (SELECT dbo.FormatPersonNameByPersonID(@PersonID, 'LastFirst'))
	DECLARE @PaymentMonthYear VARCHAR(20)
	DECLARE @ProvinceName VARCHAR(250)
	DECLARE @RunningCost INT 
	DECLARE @TotalCost INT 
	
	SELECT @RunningCost = SUM(E.RunningCost)
	FROM
		(
		SELECT
			CASE
				WHEN D.CommunityID = 0
				THEN CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('RunningCostProvince', 0)) AS INT)
				ELSE CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('RunningCostCommunity', 0)) AS INT)
			END AS RunningCost
		FROM
			(
			SELECT DISTINCT 
				CSP.CommunityID
			FROM dbo.ContactStipendPayment CSP
				JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
					AND SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
			) D
		) E
	
	SELECT @TotalCost = SUM(CSP.StipendAmountAuthorized) + ISNULL(@RunningCost, 0)
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT TOP 1 
		@PaymentMonthYear = DateName(month , DateAdd(month, CSP.PaymentMonth, 0) - 1) + ' - ' + CAST(CSP.PaymentYear AS CHAR(4)),
		@ProvinceName = P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	
	SELECT
		@ProvinceName AS ProvinceName,
		@FullName AS FullName,
		@PaymentMonthYear AS PaymentMonthYear,
		ISNULL(@TotalCost, 0) AS TotalCost
		
END
GO
--End procedure reporting.GetCashHandoverReport

--Begin procedure reporting.GetCommunityEquipmentDistributionPlanByConceptNoteID
EXEC Utility.DropObject 'reporting.GetCommunityEquipmentDistributionPlanByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to get data from the dbo.ConceptNoteBudget table
-- ================================================================================
CREATE PROCEDURE reporting.GetCommunityEquipmentDistributionPlanByConceptNoteID

@ConceptNoteID INT , 
@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.ItemName, 
		EC.ItemDescription, 
		EI.SerialNumber, 
		EC.Risk,
		GED.EquipmentStatusName,
		dbo.FormatContactNameByContactID(CNCE.ContactID ,'LastFirst') as Signee
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
		JOIN Procurement.EquipmentInventory EI on EI.EquipmentInventoryID = CNCE.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC on EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus GED ON GED.EquipmentStatusID = EI.EquipmentStatusID
			AND CNCE.ConceptNoteID = @ConceptNoteID
			AND C.CommunityID =@CommunityID
	
END
GO
--End procedure reporting.GetCommunityEquipmentDistributionPlanByConceptNoteID

--Begin procedure reporting.GetConceptNoteAmmendmentByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteAmmendmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to return data from the dbo.ConceptNoteAmmendment table
-- =======================================================================================
CREATE PROCEDURE reporting.GetConceptNoteAmmendmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT			
		CNA.AmmendmentNumber,
		CNA.ConceptNoteAmmendmentID,
		CNA.Cost,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID
	ORDER BY 1, 2

END
GO
--End procedure reporting.GetConceptNoteAmmendmentByConceptNoteID

--Begin procedure reporting.GetConceptNoteAuthorByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteAuthorByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to return data from the dbo.ConceptNoteAuthor table
-- ===================================================================================
CREATE PROCEDURE reporting.GetConceptNoteAuthorByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID
	ORDER BY 2, 1

END
GO
--End procedure reporting.GetConceptNoteAuthorByConceptNoteID

--Begin procedure reporting.GetConceptNoteBudgetByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteBudgetByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.06
-- Description:	A stored procedure to get data for the Concept Note Budget Export
-- ==============================================================================
CREATE PROCEDURE reporting.GetConceptNoteBudgetByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH CNBD AS
		(
		SELECT
			BT.BudgetTypeName,
			BST.BudgetSubTypeName,
			CNB.ConceptNoteBudgetID AS EntityID,
			CNB.ItemName,
			CNB.ItemDescription,
			CNB.UnitCost,
			FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNB.Quantity,
			FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNB.Quantity * CNB.UnitCost as TotalCost, 
			CNB.UnitOfIssue,
			CNB.QuantityOfIssue
		FROM dbo.ConceptNoteBudget CNB
				JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
				JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
					AND CNB.conceptnoteid = @ConceptNoteID

		UNION ALL

		SELECT
			'Equipment' as BudgetTypeName,
			'' as BudgetSubTypeName,
			CNEC.ConceptNoteEquipmentCatalogID AS EntityID,
			EC.ItemName,
			EC.ItemDescription,
			EC.UnitCost,
			FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
			CNEC.Quantity,
			FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
			CNEC.Quantity * EC.UnitCost * EC.UnitOfIssue AS TotalCost,
			EC.UnitOfIssue,
			EC.QuantityOfIssue
		FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = @ConceptNoteID
		)

	SELECT
		BTS.BudgetTypeSequence,
		BSS.BudgetSubTypeSequence,
		ROW_NUMBER() OVER (PARTITION BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName ORDER BY CNBD.BudgetTypeName, CNBD.BudgetSubTypeName, CNBD.ItemName, CNBD.Quantity * CNBD.UnitCost, CNBD.EntityID) AS ItemSequence,
		CNBD.BudgetTypeName, 
		CNBD.BudgetSubTypeName,
		CNBD.ItemName,
		CNBD.ItemDescription,
		CNBD.UnitCostFormatted,
		CNBD.Quantity,
		CNBD.TotalCostFormatted,
		CNBD.TotalCost,
		CNBD.UnitOfIssue,
		CNBD.QuantityOfIssue
	FROM CNBD
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (ORDER BY BTN.BudgetTypeName) AS BudgetTypeSequence,
				BTN.BudgetTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName 
				FROM CNBD
				) BTN
			) BTS ON BTS.BudgetTypeName = CNBD.BudgetTypeName
		JOIN
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY BSN.BudgetTypeName ORDER BY BSN.BudgetTypeName, BSN.BudgetSubTypeName) AS BudgetSubTypeSequence,
				BSN.BudgetTypeName,
				BSN.BudgetSubTypeName
			FROM 
				(
				SELECT DISTINCT 
					CNBD.BudgetTypeName,
					CNBD.BudgetSubTypeName
				FROM CNBD
				) BSN
			) BSS ON BSS.BudgetTypeName = CNBD.BudgetTypeName
				AND BSS.BudgetSubTypeName = CNBD.BudgetSubTypeName

END
GO
--End procedure reporting.GetConceptNoteBudgetByConceptNoteID

--Begin procedure reporting.GetConceptNoteCloseoutByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteCloseoutByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to return data for the concept note closeout reports
-- ====================================================================================
CREATE PROCEDURE reporting.GetConceptNoteCloseoutByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActualTotalAmount,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID),
		CN.DeobligatedAmount,
		CN.DescriptionOfImpact,
		dbo.FormatDate(CN.EndDate),
		CN.FemaleAdultCount,
		CN.FemaleAdultCountActual,
		CN.FemaleAdultDetails,
		CN.FemaleYouthCount,
		CN.FemaleYouthCountActual,
		CN.FemaleYouthDetails,
		CN.FemaleAdultCount + CN.FemaleYouthCount + CN.MaleAdultCount + CN.MaleYouthCount AS TotalCount,
		CN.FemaleAdultCountActual + CN.FemaleYouthCountActual + CN.MaleAdultCountActual + CN.MaleYouthCountActual AS TotalCountActual,
		CN.FinalAwardAmount,
		dbo.FormatDate(CN.FinalReportDate),
		dbo.FormatPersonNameByPersonID(CN.ImplementerID, 'LastFirst'),
		CN.IsFinalPaymentMade,
		CN.MaleAdultCount,
		CN.MaleAdultCountActual,
		CN.MaleAdultDetails,
		CN.MaleYouthCount,
		CN.MaleYouthCountActual,
		CN.MaleYouthDetails,
		CN.SpentToDate,
		dbo.FormatDate(CN.StartDate),
		CN.SuccessStories,
		CN.Summary,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

END
GO
--End procedure reporting.GetConceptNoteCloseoutByConceptNoteID

--Begin procedure reporting.GetConceptNoteIndicatorByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteIndicatorByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
--
-- Author:			John Lyons
-- Create date:	2015.06.29
-- Description:	Added 
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteIndicatorByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNI.ActualNumber,
		CNI.Comments,	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName,	
		I.AchievedDate,	
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,	
		I.AchievedValue, 	
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,	
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName + '<br>' + I.IndicatorDescription as FullLabel,	
		I.IndicatorName, 	
		I.IndicatorSource, 	
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,	
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 	
		O.ObjectiveID, 	
		O.ObjectiveName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
			AND CNI.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = I.IndicatorID
	ORDER BY I.IndicatorName, I.IndicatorID

END
GO
--End procedure reporting.GetConceptNoteIndicatorByConceptNoteID

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
-- ==================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) as DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,	
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.FaceBookpageURL
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'ConceptNoteContact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetEquipmentDistributionPlanByConceptNoteID
EXEC Utility.DropObject 'reporting.GetEquipmentDistributionPlanByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetEquipmentDistributionPlanByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CommunityList nvarchar(max) = ''

	SELECT @CommunityList += CT.CommunityName + ', ' 
	FROM dbo.ConceptNoteContactEquipment CNCE
	 JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
	  AND CNCE.ConceptNoteID = @ConceptNoteID
	  AND C.CommunityID > 0
	  JOIN Community CT ON c.CommunityID = ct.CommunityID

	IF LEN(RTRIM(@CommunityList)) > 0 
		SET @CommunityList = RTRIM(SUBSTRING(@CommunityList, 0, LEN(@CommunityList)))
	--ENDIF

	SET @CommunityList =  RTRIM(@CommunityList)
	;

	SELECT 
		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan (EquipmentDistributionPlanID) as gLink,
		dbo.FormatConceptNoteReferenceCode(ConceptNoteID) AS ReferenceCode,
		@CommunityList as CommunityList,
		ConceptNoteID,
		ExportRoute,
		CurrentSituation,
		Aim,
		PlanOutline,
		Phase1,
		Phase2,
		Phase3,
		Phase4,
		Phase5,
		OperationalResponsibility,
		Annexes,
		Distribution,
		Title,
		Summary
	FROM procurement.EquipmentDistributionPlan 
	WHERE ConceptnoteID = @ConceptNoteID
	
END
GO
--End procedure reporting.GetEquipmentDistributionPlanByConceptNoteID

--Begin procedure reporting.GetEquipmentDistributionPlanCommunitiesByConceptnoteID
EXEC Utility.DropObject 'reporting.GetEquipmentDistributionPlanCommunitiesByConceptnoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to return data for the purchase request reports
-- ===============================================================================
CREATE PROCEDURE reporting.GetEquipmentDistributionPlanCommunitiesByConceptnoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	 C.CommunityID,
	 CNCE.ConceptNoteID,
	 CT.CommunityName, 
	 CT.Summary

	FROM dbo.ConceptNoteContactEquipment CNCE
	 JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
	  AND CNCE.ConceptNoteID = @ConceptNoteID
	  AND C.CommunityID > 0
	  JOIN Community CT ON c.CommunityID = ct.CommunityID
	
END
GO
--End procedure reporting.GetEquipmentDistributionPlanCommunitiesByConceptnoteID

--Begin procedure reporting.GetOpsFundReport
EXEC Utility.DropObject 'reporting.GetOpsFundReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetOpsFundReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
	(
	SELECT
		CSP.ContactStipendPaymentID,
		CSP.ProvinceID,
		CSP.CommunityID,
		CSP.StipendAmountAuthorized
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID 
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	)

	SELECT 
		CSP.ContactID,
		SD.ProvinceID,
		SD.CommunityID,
		dbo.FormatPersonNameByPersonID(@PersonID ,'LastFirst') AS AJACSRepresentative,
		dbo.FormatContactNameByContactID(CSP.ContactID, 'LastFirst') AS BeneficaryName,
		'$' + FORMAT(SD.StipendAmountAuthorized, '#,0.00') AS StipendAmountAuthorized,
		'$' + FORMAT((SELECT SUM(SD.StipendAmountAuthorized) FROM SD), '#,0.00') AS TotalStipendAmountAuthorized,
		'$' + FORMAT((SELECT SUM(SD.StipendAmountAuthorized) FROM SD WHERE SD.CommunityID = CSP.CommunityID), '#,0.00') AS CommunityStipendAmountAuthorized,
		(SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS CommunityName,
		P.ProvinceName,
		dbo.FormatDate(StipendAuthorizedDate) AS StipendAuthorizedDateFormatted,
		DateName(month, DateAdd(month, CSP.PaymentMonth, 0 ) - 1) + ' - ' + CAST(PaymentYear AS VARCHAR(50)) AS PaymentMonthYear,
		CSP.StipendName
	FROM dbo.ContactStipendPayment CSP
		JOIN SD ON SD.ContactStipendPaymentID = CSP.ContactStipendPaymentID
		JOIN dbo.Province P ON P.ProvinceID = SD.ProvinceID
	ORDER BY CSP.CommunityID , CSP.ContactID
	
END
GO
--End procedure reporting.GetOpsFundReport

--Begin procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestEquipmentByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purchase request reports
-- ===============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ID INT, 
		BudgetTypeName Varchar(250),
		Quantity NUMERIC(18,4),
		ItemName VARCHAR(250), 
		UnitCost NUMERIC(18,2), 
		UnitOfIssue VARCHAR(250), 
		UnitCostFormatted VARCHAR(50), 
		TotalCost NUMERIC(18,2), 
		TotalCostFormatted VARCHAR(50),
		QuantityOfIssue NUMERIC(18,4)
		)

	INSERT INTO @tTable
		(ID,BudgetTypeName,Quantity,ItemName,UnitCost,UnitOfIssue,UnitCostFormatted,TotalCost,TotalCostFormatted,QuantityOfIssue)
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID as ID,
		'Equipment' as BudgetTypeName,
		PRCNEC.Quantity,
		EC.ItemName,
		EC.UnitCost,
		CAST(EC.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.QuantityOfIssue
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
		JOIN procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC ON PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID
			AND PRCNEC.PurchaseRequestID = @PurchaseRequestID

	UNION

	SELECT			
		CNB.ConceptNoteBudgetID as ID,
		BT.BudgetTypeName,
		PRCNB.Quantity,
		CNB.ItemName,
		CNB.UnitCost,
		CAST(CNB.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID	
		JOIN procurement.PurchaseRequestConceptNoteBudget PRCNB ON PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(ID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		T.ID,
		T.BudgetTypeName,
		T.Quantity,
		T.ItemName,
		T.UnitCost,
		T.UnitOfIssue,
		T.UnitCostFormatted,
		T.TotalCost,
		T.TotalCostFormatted,
		T.QuantityOfIssue
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

--Begin procedure reporting.GetStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
		(
		SELECT
			CSP.StipendName,
			CSP.CommunityID,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			COUNT(CSP.StipendName) AS StipendNameCount
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
		GROUP BY CSP.StipendName, CSP.CommunityID
		)

	SELECT
		CASE
			WHEN OAC.CommunityName IS NULL
			THEN (SELECT P.ProvinceName + ' Province' FROM dbo.Province P WHERE P.ProvinceID = 2)
			ELSE OAC.CommunityName
		END AS Center,

		A.[Command],
		B.[Command Count],
		A.[General],
		B.[General Count],
		A.[Colonel],
		B.[Colonel Count],
		A.[Colonel Doctor],
		B.[Colonel Doctor Count],
		A.[Lieutenant Colonel],
		B.[Lieutenant Colonel Count],
		A.[Major],
		B.[Major Count],
		A.[Captain],
		B.[Captain Count],
		A.[Captain Doctor],
		B.[Captain Doctor Count],
		A.[First Lieutenant],
		B.[First Lieutenant Count],
		A.[Contracted Officer],
		B.[Contracted Officer Count],
		A.[First Sergeant],
		B.[First Sergeant Count],
		A.[Sergeant],
		B.[Sergeant Count],
		A.[First Adjutant],
		B.[First Adjutant Count],
		A.[Adjutant],
		B.[Adjutant Count],
		A.[Policeman],
		B.[Policeman Count],
		A.[Contracted Policeman],
		B.[Contracted Policeman Count],
		(ISNULL(A.[Adjutant], 0) + ISNULL(A.[Captain Doctor], 0) + ISNULL(A.[Captain], 0) + ISNULL(A.[Colonel Doctor], 0) + ISNULL(A.[Colonel], 0) + ISNULL(A.[Command], 0) + ISNULL(A.[Contracted Officer], 0) + ISNULL(A.[Contracted Policeman], 0) + ISNULL(A.[First Adjutant], 0) + ISNULL(A.[First Lieutenant], 0) + ISNULL(A.[First Sergeant], 0) + ISNULL(A.[General], 0) + ISNULL(A.[Lieutenant Colonel], 0) + ISNULL(A.[Major], 0) + ISNULL(A.[Policeman], 0) + ISNULL(A.[Sergeant], 0)) AS [Total Stipend],

		CASE
			WHEN A.CommunityID = 0
			THEN 3000.00
			ELSE 500.00
		END AS [Running Costs]

	FROM
		(
		SELECT
			PVT.*
		FROM
			(
			SELECT
				SD.StipendName,
				SD.CommunityID,
				SD.StipendAmountAuthorized
			FROM SD
			) AS D
		PIVOT
			(
			MAX(D.StipendAmountAuthorized)
			FOR D.StipendName IN
				(
				[Command],[General],[Colonel],[Colonel Doctor],[Lieutenant Colonel],[Major],[Captain],[Captain Doctor],[First Lieutenant],[Contracted Officer],[First Sergeant],[Sergeant],[First Adjutant],[Adjutant],[Policeman],[Contracted Policeman]
				)
			) AS PVT
		) A
		JOIN
			(
			SELECT
				PVT.*
			FROM
				(
				SELECT
					SD.StipendName + ' Count' AS StipendName,
					SD.CommunityID,
					SD.StipendNameCount
				FROM SD
				) AS D
			PIVOT
				(
				MAX(D.StipendNameCount)
				FOR D.StipendName IN
					(
					[Command Count],[General Count],[Colonel Count],[Colonel Doctor Count],[Lieutenant Colonel Count],[Major Count],[Captain Count],[Captain Doctor Count],[First Lieutenant Count],[Contracted Officer Count],[First Sergeant Count],[Sergeant Count],[First Adjutant Count],[Adjutant Count],[Policeman Count],[Contracted Policeman Count]
					)
				) AS PVT
			) B ON B.CommunityID = A.CommunityID
		OUTER APPLY
			(
			SELECT
				C.CommunityName
			FROM dbo.Community C
			WHERE C.CommunityID = A.CommunityID
			) OAC
	ORDER BY OAC.CommunityName, A.CommunityID
	
END
GO
--End procedure reporting.GetStipendPaymentReport

--Begin procedure utility.ServerSetupKeyAddUpdate
EXEC Utility.DropObject 'utility.ServerSetupKeyAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update server setup key records
-- ========================================================================
CREATE PROCEDURE utility.ServerSetupKeyAddUpdate

@ServerSetupKey VARCHAR(250),
@ServerSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM AJACSUtility.dbo.ServerSetup SS WHERE SS.ServerSetupKey = @ServerSetupKey)
		BEGIN

		INSERT INTO AJACSUtility.dbo.ServerSetup
			(ServerSetupKey,ServerSetupValue)
		VALUES
			(@ServerSetupKey,@ServerSetupValue)

		END
	ELSE
		BEGIN

		UPDATE AJACSUtility.dbo.ServerSetup
		SET ServerSetupValue = @ServerSetupValue
		WHERE ServerSetupKey = @ServerSetupKey

		END
	--ENDIF
	
END
GO
--End procedure utility.ServerSetupKeyAddUpdate

--Begin procedure workflow.GetEquipmentDistributionPlanWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetEquipmentDistributionPlanWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Kevin Ross
-- Create date:	2015.07.01
-- Description:	A stored procedure to return workflow data
-- =======================================================
CREATE PROCEDURE workflow.GetEquipmentDistributionPlanWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'EquipmentDistributionPlan'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'EquipmentDistributionPlan.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetEquipmentDistributionPlanWorkflowStepPeople

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan' AND ET.WorkflowActionCode = 'DecrementWorkflow')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('EquipmentDistributionPlan', 'DecrementWorkflow')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan' AND ET.WorkflowActionCode = 'IncrementWorkflow')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('EquipmentDistributionPlan', 'IncrementWorkflow')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan' AND ET.WorkflowActionCode = 'Release')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('EquipmentDistributionPlan', 'IncrementWorkflow')
		
	END
--ENDIF
GO
--End table dbo.EmailTemplate & dbo.EmailTemplateField

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
	VALUES
		('EquipmentDistributionPlan', '[[Title]]','Title'),
		('EquipmentDistributionPlan', '[[TitleLink]]','Title Link'),
		('EquipmentDistributionPlan', '[[Comments]]','Comments'),
		('EquipmentDistributionPlan', '[[ConceptNoteTitle]]','Activity Title')
		
	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
DELETE FROM dbo.EntityType WHERE EntityTypeCode = 'ContactStipendPayment'
DELETE FROM dbo.EntityType WHERE EntityTypeCode = 'Payment'
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('EquipmentDistributionPlan', 'Equipment Distribution Plan')

	END
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
DELETE
FROM dbo.MenuItem
WHERE MenuItemCode = 'EquipmentDistributionPlanLisat'
GO

UPDATE dbo.MenuItem
SET 
	MenuItemCode = 'PaymentList',
	MenuItemLink = '/contact/paymentlist'
WHERE MenuItemText = 'Payments'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactEquipmentList', @NewMenuItemText='Equipment Allocation'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentDistributionPlanList', @NewMenuItemText='Equipment Distribution Plan', @NewMenuItemLink='/equipmentdistributionplan/list', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNoteContactEquipmentList', @PermissionableLineageList='EquipmentDistributionPlan.List'
GO
--End table dbo.MenuItem

--Begin table dbo.ServerSetup
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', '')) = 'Dev'
	EXEC utility.ServerSetupKeyAddUpdate 'VettingMailTo', 'todd.pires@oceandisc.com'
ELSE
	EXEC utility.ServerSetupKeyAddUpdate 'VettingMailTo', 'admin@projectkms.com'
--ENDIF
GO
--End table dbo.ServerSetup

--Begin table dropdown.VettingOutcome
UPDATE dropdown.VettingOutcome
SET VettingOutcomeName = 'Pending Internal Review'
WHERE VettingOutcomeName = 'Pending'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.VettingOutcome VO WHERE VO.VettingOutcomeName = 'Insufficient Data')
	BEGIN

	INSERT INTO dropdown.VettingOutcome
		(VettingOutcomeName, DisplayOrder, HexColor)
	VALUES 
		('Insufficient Data', 2, '#999999'),
		('Submitted for Vetting', 3, '#FFFF00')

	END
--ENDIF	
GO

UPDATE dropdown.VettingOutcome SET DisplayOrder = 0 WHERE VettingOutcomeName = 'Not Vetted'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 1 WHERE VettingOutcomeName = 'Pending Internal Review'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 2 WHERE VettingOutcomeName = 'Insufficient Data'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 3 WHERE VettingOutcomeName = 'Submitted for Vetting'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 4 WHERE VettingOutcomeName = 'Consider'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 5 WHERE VettingOutcomeName = 'Do Not Consider'
GO
--End table dropdown.VettingOutcome

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'EquipmentInventoryAudit')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode,DocumentTypeName,IsActive)
	VALUES 
		('EquipmentInventoryAudit', 'Equipment Inventory Audit Evidence Document', 0)

	END
--ENDIF	
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'ContactStipendPaymentReconcilliation')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode,DocumentTypeName,IsActive)
	VALUES 
		('ContactStipendPaymentReconcilliation', 'Contact Stipend Payment Reconcilliation', 1)

	END
--ENDIF	
GO

UPDATE dropdown.DocumentType
SET DisplayOrder = 
	CASE
		WHEN DocumentTypeCode = 'OtherDocument'
		THEN 99
		ELSE 0
	END
GO
--End table dropdown.DocumentType

--Begin table dropdown.Country
UPDATE dropdown.Country
SET CountryName = 'Syria'
WHERE CountryName = 'Syrian Arab Republic'
GO
--End table dropdown.Country

--Begin table dropdown.Stipend 
TRUNCATE TABLE dropdown.Stipend
GO

SET IDENTITY_INSERT dropdown.Stipend ON
GO

INSERT INTO dropdown.Stipend (StipendID) VALUES (0)

SET IDENTITY_INSERT dropdown.Stipend OFF
GO

INSERT INTO dropdown.Stipend
	(StipendName,StipendAmount,DisplayOrder)
VALUES
	('Command',400,1),
	('General',300,2),
	('Colonel',300,3),
	('Colonel Doctor',300,4),
	('Lieutenant Colonel',300,5),
	('Major',300,6),
	('Captain',300,7),
	('Captain Doctor',300,8),
	('First Lieutenant',300,9),
	('Contracted Officer',300,10),
	('First Sergeant',150,11),
	('Sergeant',150,12),
	('First Adjutant',150,13),
	('Adjutant',150,14),
	('Policeman',100,15),
	('Contracted Policeman',100,16),
	('Suspended',0,17)
GO	
--End table dropdown.Stipend 

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode, WorkflowStepCount)
	VALUES
		('EquipmentDistributionPlan', 2)
		
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
DECLARE @nWorkflowID INT

SELECT @nWorkflowID = W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'EquipmentDistributionPlan'

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStep WS WHERE WS.WorkflowID = @nWorkflowID)
	BEGIN

	INSERT INTO workflow.WorkflowStep 
		(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	VALUES
		(0, @nWorkflowID, 1, 'Draft', 'In Draft'),
		(0, @nWorkflowID, 2, 'Approval', 'Pending Approval')

	END
--ENDIF
GO
--End table workflow.WorkflowStep

--Begin table workflow.WorkflowStepWorkflowAction
DECLARE @nWorkflowActionID INT = (SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionName = 'Submit For Approval')
DECLARE @nWorkflowID INT = (SELECT W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'EquipmentDistributionPlan')
DECLARE @nWorkflowStepNumber INT = 1

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStepWorkflowAction WSWA WHERE WSWA.WorkflowID = @nWorkflowID AND WSWA.WorkflowStepNumber = @nWorkflowStepNumber AND WSWA.WorkflowActionID = @nWorkflowActionID)
	BEGIN		

	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
	VALUES
		(
		@nWorkflowID,
		@nWorkflowStepNumber,
		@nWorkflowActionID,
		1
		)

	END
--ENDIF

SET @nWorkflowActionID = (SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'IncrementWorkflow' AND WA.WorkflowActionName = 'Approve')
SET @nWorkflowStepNumber += 1

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStepWorkflowAction WSWA WHERE WSWA.WorkflowID = @nWorkflowID AND WSWA.WorkflowStepNumber = @nWorkflowStepNumber AND WSWA.WorkflowActionID = @nWorkflowActionID)
	BEGIN		

	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
	VALUES
		(
		@nWorkflowID,
		@nWorkflowStepNumber,
		@nWorkflowActionID,
		1
		)

	END
--ENDIF

SET @nWorkflowActionID = (SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionName = 'Reject')

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStepWorkflowAction WSWA WHERE WSWA.WorkflowID = @nWorkflowID AND WSWA.WorkflowStepNumber = @nWorkflowStepNumber AND WSWA.WorkflowActionID = @nWorkflowActionID)
	BEGIN		

	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
	VALUES
		(
		@nWorkflowID,
		@nWorkflowStepNumber,
		@nWorkflowActionID,
		1
		)

	END
--ENDIF
GO
--End table workflow.WorkflowStepWorkflowAction

--Begin table permissionable.Permissionable
UPDATE permissionable.Permissionable
SET
	ParentPermissionableID = (SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Contact'),
	PermissionableCode = 'PaymentList',
	PermissionableName = 'Payment List'
WHERE PermissionableCode = 'Payment'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'EquipmentDistributionPlan','Equipment Distribution Plan')
		
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.AddUpdate'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'EquipmentDistributionPlan.AddUpdate'),
	'WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	1
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'EquipmentDistributionPlan'
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = 'WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10))
				AND P.PermissionableLineage = 'EquipmentDistributionPlan.AddUpdate.WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10))
			)
ORDER BY WS.WorkflowStepNumber
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'View',
	'View',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.View'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'List',
	'List',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.List'
		)
GO
--End table permissionable.Permissionable

--Begin table dbo.MenuItemPermissionableLineage
UPDATE dbo.MenuItemPermissionableLineage
SET PermissionableLineage = 'Contact.PaymentList'
WHERE PermissionableLineage = 'Contact.List.Payment'
GO
--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.18 File 01 - AJACS - 2015.07.01 22.58.43')
GO
--End build tracking

