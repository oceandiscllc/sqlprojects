USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ContactImportID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ContactImportID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'ContactImportID', 'INT'

EXEC utility.AddColumn @TableName, 'ActualTotalAmount', 'NUMERIC(18,2)'
EXEC utility.AddColumn @TableName, 'DeobligatedAmount', 'NUMERIC(18,2)'
EXEC utility.AddColumn @TableName, 'DescriptionOfImpact', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FemaleAdultCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'FemaleAdultDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FemaleYouthCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'FemaleYouthDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FinalAwardAmount', 'NUMERIC(18,2)'
EXEC utility.AddColumn @TableName, 'FinalReportDate', 'DATE'
EXEC utility.AddColumn @TableName, 'MaleAdultCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'MaleAdultDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MaleYouthCountActual', 'INT'
EXEC utility.AddColumn @TableName, 'MaleYouthDetails', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SuccessStories', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SummaryOfBackground', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'SummaryOfImplementation', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'ActualTotalAmount', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeobligatedAmount', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleAdultCountActual', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FemaleYouthCountActual', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FinalAwardAmount', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'MaleAdultCountActual', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'MaleYouthCountActual', 'INT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteAmmendment
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteAmmendment'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteAmmendment
	(
	ConceptNoteAmmendmentID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	AmmendmentNumber VARCHAR(100),
	Date DATE,
	Description VARCHAR(MAX),
	Cost NUMERIC(18, 2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Cost', 'NUMERIC(18, 2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteAmmendmentID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteAmmendment', @TableName, 'ConceptNoteID,AmmendmentNumber,Date'
GO
--End table dbo.ConceptNoteAmmendment

--Begin table dbo.ConceptNoteAuthor
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteAuthor'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteAuthor
	(
	ConceptNoteAuthorID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteAuthorID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteAuthor', @TableName, 'ConceptNoteID,PersonID'
GO
--End table dbo.ConceptNoteAuthor

--Begin table dbo.ConceptNoteIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteIndicator'

EXEC utility.AddColumn @TableName, 'ActualNumber', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ActualNumber', 'INT', 0
GO
--End table dbo.ConceptNoteIndicator

--Begin table dbo.ContactStipendPayment
DECLARE @TableName VARCHAR(250) = 'dbo.ContactStipendPayment'

EXEC utility.DropObject 'dbo.Payment'

EXEC Utility.DropColumn @TableName, 'PaymentDate'

EXEC utility.AddColumn @TableName, 'DocumentID', 'INT'
EXEC utility.AddColumn @TableName, 'PaymentMonth', 'INT'
EXEC utility.AddColumn @TableName, 'PaymentYear', 'INT'
EXEC utility.AddColumn @TableName, 'StipendAuthorizedDate', 'DATE'
EXEC utility.AddColumn @TableName, 'StipendPaidDate', 'DATE'

EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentMonth', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentYear', 'INT', 0

EXEC utility.DropIndex @TableName, 'IX_ContactStipendPayment'
EXEC utility.SetIndexClustered 'IX_ContactStipendPayment', @TableName, 'PaymentYear DESC,PaymentMonth DESC, ContactID'
GO
--End table dbo.ContactStipendPayment

--Begin table dropdown.AuditOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.AuditOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AuditOutcome
	(
	AuditOutcomeID INT IDENTITY(0,1) NOT NULL,
	AuditOutcomeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AuditOutcomeID'
EXEC utility.SetIndexNonClustered 'IX_AuditOutcomeName', @TableName, 'DisplayOrder,AuditOutcomeName', 'AuditOutcomeID'
GO

SET IDENTITY_INSERT dropdown.AuditOutcome ON
GO

INSERT INTO dropdown.AuditOutcome 
	(AuditOutcomeID, AuditOutcomeName, DisplayOrder) 
VALUES 
	(0, 'Not Audited', 1),
	(1, 'Verified - Good', 2),
	(2, 'Verified - Damaged', 3),
	(3, 'Verified - Destroyed', 4),
	(4, 'Not Verified - Believed Good', 5),
	(5, 'Not Verified - Believed Missing', 6),
	(6, 'Not Verified - Believed Destroyed', 7)
GO

SET IDENTITY_INSERT dropdown.AuditOutcome OFF
GO
--End table dropdown.AuditOutcome
 
--Begin table procurement.EquipmentAuditOutcome
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentAuditOutcome'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentAuditOutcome
	(
	EquipmentAuditOutcomeID INT IDENTITY(1,1),
	EquipmentInventoryID INT,
	AuditOutcomeID INT,
	EquipmentAuditOutcomeDate DATE
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentAuditOutcomeID'
EXEC utility.SetIndexClustered 'IX_EquipmentAuditOutcome', @TableName, 'EquipmentInventoryID,AuditOutcomeID'
GO
--End table procurement.EquipmentAuditOutcome

--Begin table procurement.EquipmentDistributionPlan
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistributionPlan'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentDistributionPlan
	(
	EquipmentDistributionPlanID INT IDENTITY(1,1),
	ConceptNoteID INT,
	ExportRoute VARCHAR(MAX),
	CurrentSituation VARCHAR(MAX),
	Aim VARCHAR(MAX),
	PlanOutline VARCHAR(MAX),
	Phase1 VARCHAR(MAX),
	Phase2 VARCHAR(MAX),
	Phase3 VARCHAR(MAX),
	Phase4 VARCHAR(MAX),
	Phase5 VARCHAR(MAX),
	OperationalResponsibility VARCHAR(MAX),
	Annexes VARCHAR(MAX),
	Distribution VARCHAR(MAX),
	Title VARCHAR(250),
	Summary VARCHAR(MAX),
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentDistributionPlanID'
GO
--End table procurement.EquipmentDistributionPlan

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'AuditDate', 'DATE'
EXEC utility.AddColumn @TableName, 'AuditOutcomeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
GO
--End table procurement.EquipmentInventory
