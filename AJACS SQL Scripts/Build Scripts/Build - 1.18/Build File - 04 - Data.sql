USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan' AND ET.WorkflowActionCode = 'DecrementWorkflow')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('EquipmentDistributionPlan', 'DecrementWorkflow')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan' AND ET.WorkflowActionCode = 'IncrementWorkflow')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('EquipmentDistributionPlan', 'IncrementWorkflow')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan' AND ET.WorkflowActionCode = 'Release')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode)
	VALUES
		('EquipmentDistributionPlan', 'IncrementWorkflow')
		
	END
--ENDIF
GO
--End table dbo.EmailTemplate & dbo.EmailTemplateField

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
	VALUES
		('EquipmentDistributionPlan', '[[Title]]','Title'),
		('EquipmentDistributionPlan', '[[TitleLink]]','Title Link'),
		('EquipmentDistributionPlan', '[[Comments]]','Comments'),
		('EquipmentDistributionPlan', '[[ConceptNoteTitle]]','Activity Title')
		
	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
DELETE FROM dbo.EntityType WHERE EntityTypeCode = 'ContactStipendPayment'
DELETE FROM dbo.EntityType WHERE EntityTypeCode = 'Payment'
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('EquipmentDistributionPlan', 'Equipment Distribution Plan')

	END
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
DELETE
FROM dbo.MenuItem
WHERE MenuItemCode = 'EquipmentDistributionPlanLisat'
GO

UPDATE dbo.MenuItem
SET 
	MenuItemCode = 'PaymentList',
	MenuItemLink = '/contact/paymentlist'
WHERE MenuItemText = 'Payments'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactEquipmentList', @NewMenuItemText='Equipment Allocation'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentDistributionPlanList', @NewMenuItemText='Equipment Distribution Plan', @NewMenuItemLink='/equipmentdistributionplan/list', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNoteContactEquipmentList', @PermissionableLineageList='EquipmentDistributionPlan.List'
GO
--End table dbo.MenuItem

--Begin table dbo.ServerSetup
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', '')) = 'Dev'
	EXEC utility.ServerSetupKeyAddUpdate 'VettingMailTo', 'todd.pires@oceandisc.com'
ELSE
	EXEC utility.ServerSetupKeyAddUpdate 'VettingMailTo', 'admin@projectkms.com'
--ENDIF
GO
--End table dbo.ServerSetup

--Begin table dropdown.VettingOutcome
UPDATE dropdown.VettingOutcome
SET VettingOutcomeName = 'Pending Internal Review'
WHERE VettingOutcomeName = 'Pending'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.VettingOutcome VO WHERE VO.VettingOutcomeName = 'Insufficient Data')
	BEGIN

	INSERT INTO dropdown.VettingOutcome
		(VettingOutcomeName, DisplayOrder, HexColor)
	VALUES 
		('Insufficient Data', 2, '#999999'),
		('Submitted for Vetting', 3, '#FFFF00')

	END
--ENDIF	
GO

UPDATE dropdown.VettingOutcome SET DisplayOrder = 0 WHERE VettingOutcomeName = 'Not Vetted'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 1 WHERE VettingOutcomeName = 'Pending Internal Review'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 2 WHERE VettingOutcomeName = 'Insufficient Data'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 3 WHERE VettingOutcomeName = 'Submitted for Vetting'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 4 WHERE VettingOutcomeName = 'Consider'
UPDATE dropdown.VettingOutcome SET DisplayOrder = 5 WHERE VettingOutcomeName = 'Do Not Consider'
GO
--End table dropdown.VettingOutcome

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'EquipmentInventoryAudit')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode,DocumentTypeName,IsActive)
	VALUES 
		('EquipmentInventoryAudit', 'Equipment Inventory Audit Evidence Document', 0)

	END
--ENDIF	
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'ContactStipendPaymentReconcilliation')
	BEGIN

	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode,DocumentTypeName,IsActive)
	VALUES 
		('ContactStipendPaymentReconcilliation', 'Contact Stipend Payment Reconcilliation', 1)

	END
--ENDIF	
GO

UPDATE dropdown.DocumentType
SET DisplayOrder = 
	CASE
		WHEN DocumentTypeCode = 'OtherDocument'
		THEN 99
		ELSE 0
	END
GO
--End table dropdown.DocumentType

--Begin table dropdown.Country
UPDATE dropdown.Country
SET CountryName = 'Syria'
WHERE CountryName = 'Syrian Arab Republic'
GO
--End table dropdown.Country

--Begin table dropdown.Stipend 
TRUNCATE TABLE dropdown.Stipend
GO

SET IDENTITY_INSERT dropdown.Stipend ON
GO

INSERT INTO dropdown.Stipend (StipendID) VALUES (0)

SET IDENTITY_INSERT dropdown.Stipend OFF
GO

INSERT INTO dropdown.Stipend
	(StipendName,StipendAmount,DisplayOrder)
VALUES
	('Command',400,1),
	('General',300,2),
	('Colonel',300,3),
	('Colonel Doctor',300,4),
	('Lieutenant Colonel',300,5),
	('Major',300,6),
	('Captain',300,7),
	('Captain Doctor',300,8),
	('First Lieutenant',300,9),
	('Contracted Officer',300,10),
	('First Sergeant',150,11),
	('Sergeant',150,12),
	('First Adjutant',150,13),
	('Adjutant',150,14),
	('Policeman',100,15),
	('Contracted Policeman',100,16),
	('Suspended',0,17)
GO	
--End table dropdown.Stipend 

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode, WorkflowStepCount)
	VALUES
		('EquipmentDistributionPlan', 2)
		
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStep
DECLARE @nWorkflowID INT

SELECT @nWorkflowID = W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'EquipmentDistributionPlan'

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStep WS WHERE WS.WorkflowID = @nWorkflowID)
	BEGIN

	INSERT INTO workflow.WorkflowStep 
		(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	VALUES
		(0, @nWorkflowID, 1, 'Draft', 'In Draft'),
		(0, @nWorkflowID, 2, 'Approval', 'Pending Approval')

	END
--ENDIF
GO
--End table workflow.WorkflowStep

--Begin table workflow.WorkflowStepWorkflowAction
DECLARE @nWorkflowActionID INT = (SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionName = 'Submit For Approval')
DECLARE @nWorkflowID INT = (SELECT W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'EquipmentDistributionPlan')
DECLARE @nWorkflowStepNumber INT = 1

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStepWorkflowAction WSWA WHERE WSWA.WorkflowID = @nWorkflowID AND WSWA.WorkflowStepNumber = @nWorkflowStepNumber AND WSWA.WorkflowActionID = @nWorkflowActionID)
	BEGIN		

	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
	VALUES
		(
		@nWorkflowID,
		@nWorkflowStepNumber,
		@nWorkflowActionID,
		1
		)

	END
--ENDIF

SET @nWorkflowActionID = (SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'IncrementWorkflow' AND WA.WorkflowActionName = 'Approve')
SET @nWorkflowStepNumber += 1

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStepWorkflowAction WSWA WHERE WSWA.WorkflowID = @nWorkflowID AND WSWA.WorkflowStepNumber = @nWorkflowStepNumber AND WSWA.WorkflowActionID = @nWorkflowActionID)
	BEGIN		

	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
	VALUES
		(
		@nWorkflowID,
		@nWorkflowStepNumber,
		@nWorkflowActionID,
		1
		)

	END
--ENDIF

SET @nWorkflowActionID = (SELECT WA.WorkflowActionID FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionName = 'Reject')

IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowStepWorkflowAction WSWA WHERE WSWA.WorkflowID = @nWorkflowID AND WSWA.WorkflowStepNumber = @nWorkflowStepNumber AND WSWA.WorkflowActionID = @nWorkflowActionID)
	BEGIN		

	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
	VALUES
		(
		@nWorkflowID,
		@nWorkflowStepNumber,
		@nWorkflowActionID,
		1
		)

	END
--ENDIF
GO
--End table workflow.WorkflowStepWorkflowAction

--Begin table permissionable.Permissionable
UPDATE permissionable.Permissionable
SET
	ParentPermissionableID = (SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Contact'),
	PermissionableCode = 'PaymentList',
	PermissionableName = 'Payment List'
WHERE PermissionableCode = 'Payment'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentDistributionPlan')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'EquipmentDistributionPlan','Equipment Distribution Plan')
		
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.AddUpdate'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'EquipmentDistributionPlan.AddUpdate'),
	'WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	1
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'EquipmentDistributionPlan'
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = 'WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10))
				AND P.PermissionableLineage = 'EquipmentDistributionPlan.AddUpdate.WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10))
			)
ORDER BY WS.WorkflowStepNumber
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'View',
	'View',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.View'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'List',
	'List',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'EquipmentDistributionPlan'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EquipmentDistributionPlan.List'
		)
GO
--End table permissionable.Permissionable

--Begin table dbo.MenuItemPermissionableLineage
UPDATE dbo.MenuItemPermissionableLineage
SET PermissionableLineage = 'Contact.PaymentList'
WHERE PermissionableLineage = 'Contact.List.Payment'
GO
--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
