USE AJACS
GO

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
--
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	Added ISNULL support
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(300)
	
	SELECT @cRetVal = 'AJACS-AS-' + RIGHT('0000' + CAST(CN.ConceptNoteID AS VARCHAR(10)), 4) + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID
	
	RETURN RTRIM(LTRIM(ISNULL(@cRetVal, '')))

END
GO
--End function dbo.FormatConceptNoteTitle

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@EquipmentDistributionPlanID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(C.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(C.Longitude AS VARCHAR(MAX))
	 FROM dbo.ConceptNoteCommunity CNC
		JOIN procurement.EquipmentDistributionPlan EDP ON EDP.ConceptNoteID = CNC.ConceptNoteID
			AND EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID
		JOIN dbo.Community C ON CNC.CommunityID = C.CommunityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan

--Begin function dbo.GetProvinceIDByCommunityID
EXEC utility.DropObject 'dbo.GetProvinceIDByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.26
-- Description:	A function to return the province id associated with a community id
-- ================================================================================

CREATE FUNCTION dbo.GetProvinceIDByCommunityID
(
@CommunityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nRetVal INT
	
	SELECT @nRetVal = C.ProvinceID
	FROM dbo.Community C 
	WHERE C.CommunityID = @CommunityID
	
	RETURN ISNULL(@nRetVal, 0)

END
GO
--End function dbo.GetProvinceIDByCommunityID

--Begin function eventlog.GetContactContactAffiliationsXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactContactAffiliationsXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactContactAffiliation data for a specific contact record
-- ==============================================================================================

CREATE FUNCTION eventlog.GetContactContactAffiliationsXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactContactAffiliations VARCHAR(MAX) = ''
	
	SELECT @cContactContactAffiliations = COALESCE(@cContactContactAffiliations, '') + D.ContactContactAffiliation
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactContactAffiliation'), ELEMENTS) AS ContactContactAffiliation
		FROM dbo.ContactContactAffiliation T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactContactAffiliations>' + ISNULL(@cContactContactAffiliations, '') + '</ContactContactAffiliations>'

END
GO
--End function eventlog.GetContactContactAffiliationsXMLByContactID

--Begin function eventlog.GetContactContactTypesXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactContactTypesXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactContactType data for a specific contact record
-- =======================================================================================

CREATE FUNCTION eventlog.GetContactContactTypesXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactContactTypes VARCHAR(MAX) = ''
	
	SELECT @cContactContactTypes = COALESCE(@cContactContactTypes, '') + D.ContactContactType
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactContactType'), ELEMENTS) AS ContactContactType
		FROM dbo.ContactContactType T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactContactTypes>' + ISNULL(@cContactContactTypes, '') + '</ContactContactTypes>'

END
GO
--End function eventlog.GetContactContactTypesXMLByContactID

--Begin function eventlog.GetContactStipendPaymentsXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactStipendPaymentsXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactStipendPayment data for a specific contact record
-- ==========================================================================================

CREATE FUNCTION eventlog.GetContactStipendPaymentsXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactStipendPayments VARCHAR(MAX) = ''
	
	SELECT @cContactStipendPayments = COALESCE(@cContactStipendPayments, '') + D.ContactStipendPayment
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactStipendPayment'), ELEMENTS) AS ContactStipendPayment
		FROM dbo.ContactStipendPayment T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactStipendPayments>' + ISNULL(@cContactStipendPayments, '') + '</ContactStipendPayments>'

END
GO
--End function eventlog.GetContactStipendPaymentsXMLByContactID

--Begin function eventlog.GetContactVettingsXMLByContactID
EXEC utility.DropObject 'eventlog.GetContactVettingsXMLByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A function to return ContactVetting data for a specific contact record
-- ===================================================================================

CREATE FUNCTION eventlog.GetContactVettingsXMLByContactID
(
@ContactID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cContactVettings VARCHAR(MAX) = ''
	
	SELECT @cContactVettings = COALESCE(@cContactVettings, '') + D.ContactVetting
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ContactVetting'), ELEMENTS) AS ContactVetting
		FROM dbo.ContactVetting T 
		WHERE T.ContactID = @ContactID
		) D

	RETURN '<ContactVettings>' + ISNULL(@cContactVettings, '') + '</ContactVettings>'

END
GO
--End function eventlog.GetContactVettingsXMLByContactID