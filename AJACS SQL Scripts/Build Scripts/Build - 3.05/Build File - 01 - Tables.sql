USE AJACS
GO

--Begin table logicalframeworkupdate.Indicator
ALTER TABLE logicalframeworkupdate.Indicator ALTER COLUMN AchievedValue INT
ALTER TABLE logicalframeworkupdate.Indicator ALTER COLUMN BaselineValue INT
ALTER TABLE logicalframeworkupdate.Indicator ALTER COLUMN IndicatorName VARCHAR(500)
ALTER TABLE logicalframeworkupdate.Indicator ALTER COLUMN TargetValue INT

EXEC utility.SetDefaultConstraint 'logicalframeworkupdate.Indicator', 'AchievedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint 'logicalframeworkupdate.Indicator', 'BaselineValue', 'INT', '0'
EXEC utility.SetDefaultConstraint 'logicalframeworkupdate.Indicator', 'TargetValue', 'INT', '0'
GO
--End table logicalframeworkupdate.Indicator

ALTER TABLE weeklyreport.Province
DROP CONSTRAINT PK_Province
GO
ALTER TABLE weeklyreport.Province
ADD CONSTRAINT PK_Province PRIMARY KEY (ProvinceID,WeeklyReportID)
GO
ALTER TABLE weeklyreport.Community
DROP CONSTRAINT PK_Community
GO
ALTER TABLE weeklyreport.Community
ADD CONSTRAINT PK_Community PRIMARY KEY (CommunityID,WeeklyReportID)
GO
