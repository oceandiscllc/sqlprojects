USE AJACS
GO


--Begin procedure dbo.GetCommunityFeed
EXEC Utility.DropObject 'dbo.GetCommunityFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.24
-- Description:	updated to pull in Impact Decision
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- ==================================================================
CREATE PROCEDURE dbo.GetCommunityFeed

@PersonID INT = 0,
@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN document.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID = @CommunityID
					AND IC.IncidentID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS DocumentName,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID = @CommunityID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID = @CommunityID
					AND SRC.SpotReportID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Community Engagement Permitted Change' AS EntityTypeName,			
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS DocumentName,
		C.CommunityName + ' Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
			AND CIDH.CommunityID = @CommunityID
			AND CIDH.CreateDateTime >= DATEADD(d, -14, getDate())

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD   
		JOIN document.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
		JOIN weeklyreport.Community WRC ON WRC.WeeklyReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'WeeklyReport'
			AND WRC.CommunityID = @CommunityID

	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetCommunityFeed

--Begin procedure dbo.GetConceptNotePaymentHistoryByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNotePaymentHistoryByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date: 2017.03.05
-- Description:	A stored procedure to get payment history data for a ConceptNote
-- =============================================================================
CREATE PROCEDURE dbo.GetConceptNotePaymentHistoryByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tParentCNs TABLE (ConceptNoteID INT, Title VARCHAR(250), AmendedConceptNoteID INT)

	;
	WITH CNCTE AS 
		( 
		SELECT 
			CN.ConceptNoteID, 
			CN.Title, 
			CN.AmendedConceptNoteID
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.Title, 
			CN.AmendedConceptNoteID
		FROM dbo.ConceptNote CN
			JOIN CNCTE ON CNCTE.AmendedConceptNoteID = CN.ConceptNoteID
		)

	INSERT INTO @tParentCNs 
		(ConceptNoteID, Title, AmendedConceptNoteID)
	SELECT 
		CNCTE.ConceptNoteID, 
		CNCTE.Title, 
		CNCTE.AmendedConceptNoteID
	FROM CNCTE

	DECLARE @tTable1 TABLE
		(
		ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		PaymentYear INT DEFAULT 0,
		PaymentMonth INT DEFAULT 0,
		ConceptNoteID INT DEFAULT 0,
		StipendAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		StipendAmountPaid NUMERIC(18,2) DEFAULT 0,
		ProvinceID INT DEFAULT 0,
		PayeeCount INT DEFAULT 0,
		ExpenseAmountAuthorized NUMERIC(18,2) DEFAULT 0,
		ExpenseAmountPaid NUMERIC(18,2) DEFAULT 0,
		AssetDepartmentCount INT DEFAULT 0
		)

	INSERT INTO @tTable1
		(PaymentYear, PaymentMonth, StipendAmountAuthorized, StipendAmountPaid, ProvinceID, PayeeCount)
	SELECT
		CSP.PaymentYear,
		CSP.PaymentMonth,
		IIF ( CSP.StipendAuthorizedDate IS NOT NULL, SUM(CSP.StipendAmountAuthorized), 0 ) AS StipendAmountAuthorized,
		IIF ( CSP.StipendPaidDate IS NOT NULL, SUM(CSP.StipendAmountPaid), 0 ) AS StipendAmountPaid,
		CSP.ProvinceID,
		COUNT(CSP.ContactID) AS PayeeCount
	FROM dbo.ContactStipendPayment CSP
	WHERE EXISTS
		(
		SELECT 1
		FROM @tParentCNs T
		WHERE T.ConceptNoteID = CSP.ConceptNoteID
		)
	GROUP BY CSP.ProvinceID, CSP.PaymentYear, CSP.PaymentMonth, CSP.StipendPaidDate, CSP.StipendAuthorizedDate

	MERGE @tTable1 T1
	USING
		(
		SELECT
			AUE.PaymentYear,
			AUE.PaymentMonth,
			SUM(AUE.ExpenseAmountAuthorized) AS ExpenseAmountAuthorized,
			SUM(AUE.ExpenseAmountPaid) AS ExpenseAmountPaid,
			C.ProvinceID,
			COUNT(AUE.AssetUnitID) AS AssetDepartmentCount
		FROM asset.AssetUnitExpense AUE
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = AUE.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dbo.Community C ON C.CommunityID = A.CommunityID
				AND EXISTS
					(
					SELECT 1
					FROM @tParentCNs T
					WHERE T.ConceptNoteID = AUE.ConceptNoteID
					)
		GROUP BY C.ProvinceID, AUE.PaymentYear, AUE.PaymentMonth
		) T3 ON T3.ProvinceID = T1.ProvinceID 
			AND T3.PaymentYear = T1.PaymentYear 
			AND T3.PaymentMonth = T1.PaymentMonth
	WHEN NOT MATCHED THEN
	INSERT
		(PaymentYear, PaymentMonth, ExpenseAmountAuthorized, ExpenseAmountPaid, ProvinceID, AssetDepartmentCount)
	VALUES
		(
		T3.PaymentYear, 
		T3.PaymentMonth, 
		T3.ExpenseAmountAuthorized, 
		T3.ExpenseAmountPaid, 
		T3.ProvinceID, 
		T3.AssetDepartmentCount
		)
	WHEN MATCHED THEN
	UPDATE 
	SET 
		T1.ExpenseAmountAuthorized = T3.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid = T3.ExpenseAmountPaid, 
		T1.AssetDepartmentCount = T3.AssetDepartmentCount
	;

	SELECT 
		P.ProvinceName,
		T1.PaymentYear,
		RIGHT('00' + CAST(T1.PaymentMonth AS VARCHAR(2)), 2) AS PaymentMonthFormatted,
		T1.StipendAmountAuthorized,
		T1.StipendAmountPaid,
		T1.PayeeCount,
		T1.ExpenseAmountAuthorized, 
		T1.ExpenseAmountPaid, 
		T1.AssetDepartmentCount
	FROM @tTable1 T1
		JOIN dbo.Province P ON P.ProvinceID = T1.ProvinceID
	ORDER BY P.ProvinceName, T1.PaymentYear DESC, T1.PaymentMonth DESC

END
GO
--End procedure dbo.GetConceptNotePaymentHistoryByConceptNoteID

--Begin procedure dbo.GetProvinceFeed
EXEC Utility.DropObject 'dbo.GetProvinceFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Jonathan Burnham
-- Create date: 2016.09.11
-- Description:	A stored procedure to get data for the dashboard feed
--
-- Author:			Eric Jones
-- Create date: 2016.10.25
-- Description:	updated to pull in Impact Decision
-- ==================================================================
CREATE PROCEDURE dbo.GetProvinceFeed

@PersonID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH ELD AS
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID,
			ET.EntityTypeName
		FROM eventlog.EventLog EL
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
				AND EL.EntityTypeCode IN ('Document','Incident','RequestForInformation','SpotReport','WeeklyReport')
				AND EL.EventCode <> 'read'
				AND EL.PersonID > 0
				AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
				AND
					(
					EL.EntityTypeCode = 'Document'
						OR (EL.EntityTypeCode = 'Incident' AND permissionable.HasPermission('Incident.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
					)
		GROUP BY EL.EntityTypeCode, ET.EntityTypeName, EL.EntityID
		)

	SELECT
		'fa fa-fw fa-file' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD
		JOIN Document.Document D ON D.DocumentID = ELD.EntityID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN Document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND ELD.EntityTypeCode = 'Document'
			AND permissionable.HasPermission(CONCAT('Document.View.', CAST(DT.DocumentTypePermissionCode AS VARCHAR(250))), @PersonID) = 1
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		I.IncidentName AS Title
	FROM ELD
		JOIN dbo.Incident I ON I.IncidentID = ELD.EntityID
			AND ELD.EntityTypeCode = 'Incident'
			AND I.IncidentID = ELD.EntityID
			AND I.IncidentName IS NOT NULL
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)
					AND IC.IncidentID = ELD.EntityID

				UNION

				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.ProvinceID = @ProvinceID
					AND IP.IncidentID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-question-circle' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		RFI.RequestForInformationTitle AS Title
	FROM ELD
		JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationID = ELD.EntityID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND ELD.EntityTypeCode = 'RequestForInformation'
			AND RFIS.RequestForInformationStatusCode = 'Completed'			
			AND RFI.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		NULL AS Documentname,
		SR.SpotReportTitle AS Title
	FROM ELD
		JOIN dbo.SpotReport SR ON SR.SpotReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'SpotReport'
			AND	workflow.GetWorkflowStepNumber(ELD.EntityTypeCode, ELD.EntityID) > workflow.GetWorkflowStepCount(ELD.EntityTypeCode, ELD.EntityID)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.SpotReportCommunity SRC
				WHERE SRC.CommunityID IN (SELECT CommunityID FROM Community C2 WHERE C2.ProvinceID = @ProvinceID)
					AND SRC.SpotReportID = ELD.EntityID

				UNION

				SELECT 1
				FROM dbo.SpotReportProvince SRP
				WHERE SRP.ProvinceID = @ProvinceID
					AND SRP.SpotReportID = ELD.EntityID
				)

	UNION

	SELECT
		'fa fa-fw fa-bolt' AS Icon,
		CIDH.CommunityID AS EntityID,
		'EngagementPermitted' AS EntityTypeCode,
		'Community Engagement Permitted Change' AS EntityTypeName,
		CIDH.CreateDateTime AS UpdateDate,
		dbo.FormatDate(CIDH.CreateDateTime) AS UpdateDateFormatted,
		LOWER('community') AS Controller,
		NULL AS DocumentName,
		C.CommunityName + 'Status change from '+ ID2.ImpactDecisionName +' to ' + ID1.ImpactDecisionName AS Title
	FROM dbo.CommunityImpactDecisionHistory CIDH
		JOIN dropdown.ImpactDecision ID1 ON ID1.ImpactDecisionID = CIDH.CurrentImpactDecisionID
		JOIN dropdown.ImpactDecision ID2 ON ID2.ImpactDecisionID = CIDH.PreviousImpactDecisionID
		JOIN dbo.Community C ON C.CommunityID = CIDH.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND P.ProvinceID = @ProvinceID
			AND CIDH.CreateDateTime >= DATEADD(d, -14, getDate())

	UNION

	SELECT
		'fa fa-fw fa-calendar' AS Icon,
		ELD.EntityID,
		ELD.EntityTypeCode,
		ELD.EntityTypeName,
		ELD.UpdateDate,
		dbo.FormatDate(ELD.UpdateDate) AS UpdateDateFormatted,
		LOWER(ELD.EntityTypeCode) AS Controller,
		D.DocumentName,
		D.DocumentTitle AS Title
	FROM ELD   
		JOIN document.Document D ON D.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(ELD.EntityID) + '%'
		JOIN weeklyreport.Province WRP ON WRP.WeeklyReportID = ELD.EntityID
			AND ELD.EntityTypeCode = 'WeeklyReport'
			AND WRP.ProvinceID = @ProvinceID

	ORDER BY 5 DESC, 4, 2
	
END
GO
--End procedure dbo.GetProvinceFeed

--Begin procedure weeklyreport.ApproveWeeklyReport
EXEC Utility.DropObject 'weeklyreport.ApproveWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
--
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	Renamed from SubmitWeeklyReport to ApproveWeeklyReport
--
-- Author:			Todd Pires
-- Create date:	2015.05.16
-- Description:	Added the post-approval initialization call
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ======================================================================
CREATE PROCEDURE weeklyreport.ApproveWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @nWeeklyReportID INT
	DECLARE @tOutput1 TABLE (EntityTypeCode VARCHAR(50), EntityID INT)
	DECLARE @tOutput2 TABLE (WeeklyReportID INT)

	SELECT @nWeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput1
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @nWeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput1
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @nWeeklyReportID

	INSERT INTO @tOutput1 (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @nWeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O1.EntityTypeCode, O1.EntityID
		FROM @tOutput1 O1
		ORDER BY O1.EntityTypeCode, O1.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	TRUNCATE TABLE weeklyreport.SummaryMapCommunity
	TRUNCATE TABLE weeklyreport.SummaryMapAsset
	TRUNCATE TABLE weeklyreport.SummaryMapIncident
	DELETE FROM weeklyreport.WeeklyReport

	INSERT INTO weeklyreport.WeeklyReport 
		(WorkflowStepNumber) 
	OUTPUT INSERTED.WeeklyReportID INTO @tOutput2
	VALUES 
		(1)

	SELECT @nWeeklyReportID = O2.WeeklyReportID FROM @tOutput2 O2

	EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

END
GO
--End procedure weeklyreport.ApproveWeeklyReport

--Begin procedure weeklyreport.PopulateWeeklyReportCommunities
EXEC utility.DropObject 'weeklyreport.PopulateWeeklyReportCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [weeklyreport].[PopulateWeeklyReportCommunities]

@CommunityIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Community
		(CommunityEngagementStatusID, CommunityID, CommunityName, ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		C.CommunityEngagementStatusID,
		C.CommunityID,
		C.CommunityName,
		C.ImpactDecisionID, 
		C.Implications, 
		C.KeyHighlight,
		C.KeyPoints, 
		C.ProvinceID,
		C.RiskMitigation,
		C.StatusChangeID, 
		C.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Community C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Community WRC
				WHERE WRC.CommunityID = C.CommunityID
				AND WRC.WeeklyReportId = (SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
				)
END
GO
--End procedure weeklyreport.PopulateWeeklyReportCommunities

--Begin procedure weeklyreport.PopulateWeeklyReportCommunities
EXEC utility.DropObject 'weeklyreport.PopulateWeeklyReportCommunities'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE weeklyreport.PopulateWeeklyReportCommunities

@ProvinceIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO weeklyreport.Province
		(ImpactDecisionID, Implications, KeyHighlight, KeyPoints, ProvinceID, ProvinceName, RiskMitigation, StatusChangeID, Summary, WeeklyReportID)
	SELECT
		P.ImpactDecisionID, 
		P.Implications, 
		P.KeyHighlight,
		P.KeyPoints, 
		P.ProvinceID,
		P.ProvinceName,
		P.RiskMitigation,
		P.StatusChangeID, 
		P.Summary, 
		(SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
	FROM dbo.Province P
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM weeklyreport.Province WRP
				WHERE WRP.ProvinceID = P.ProvinceID
				AND WRP.WeeklyReportId = (SELECT TOP 1 WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR ORDER BY WR.WeeklyReportID DESC)
				)

END
GO
--End procedure weeklyreport.PopulateWeeklyReportCommunities
