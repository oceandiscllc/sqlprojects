USE AJACS
GO

--Begin table dropdown.AssetUnitCost
IF NOT EXISTS (SELECT 1 FROM dropdown.AssetUnitCost AUC WHERE AUC.AssetUnitCostName = '550')
	BEGIN

	INSERT INTO dropdown.AssetUnitCost
		(AssetUnitCostName)
	VALUES 
		('550'),
		('800')

	END
--ENDIF
GO

;
WITH SD AS
	(
	SELECT 
		ROW_NUMBER() OVER (ORDER BY CAST(AUC.AssetUnitCostName AS INT)) AS DisplayOrder,
		AUC.AssetUnitCostID
	FROM dropdown.AssetUnitCost AUC 
	)

UPDATE AUC
SET AUC.DisplayOrder = SD.DisplayOrder
FROM dropdown.AssetUnitCost AUC
	JOIN SD ON SD.AssetUnitCostID = AUC.AssetUnitCostID
GO
--End table dropdown.AssetUnitCost
