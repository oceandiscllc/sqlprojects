USE AJACS
GO

--Begin procedure permissionable.RemovePermissionables
EXEC Utility.DropObject 'permissionable.RemovePermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Jonathan Burnham
-- Create date: 2018.08.14
-- Description:	A stored procedure to remove a permissionable template from one or more person id's
-- =================================================================================================
CREATE PROCEDURE permissionable.RemovePermissionables

@PermissionableIDs VARCHAR(MAX),
@PersonID INT,
@PersonIDList VARCHAR(MAX)

AS
BEGIN

	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM dbo.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.Permissionable P1
			JOIN dbo.ListToTable(@PermissionableIDs, ',') PIDs ON CAST(PIDs.ListItem AS INT) = P1.PermissionableID
		) D


	DELETE PP
	OUTPUT DELETED.PersonID INTO @tOutput
	FROM permissionable.PersonPermissionable PP
		JOIN @tTable T ON T.PersonID = PP.PersonID
			AND EXISTS
				(
				SELECT 1
				FROM @tTable T
				WHERE T.PermissionableLineage = PP.PermissionableLineage
				)
			

	SET @PersonIDList = ''
	
	SELECT 
		@PersonIDList = COALESCE(@PersonIDList + ',', '') + CAST(D.PersonID AS VARCHAR(10))
	FROM
		(
		SELECT DISTINCT 
			T.PersonID
		FROM @tOutput T
		) D

	SET @PersonIDList = STUFF(@PersonIDList, 1, 1, '')

	EXEC eventlog.LogPersonAction @EntityID = 0, @EventCode = 'update', @PersonID = @PersonID, @Comments = NULL, @EntityIDList = @PersonIDList

END
GO
--End procedure permissionable.RemovePermissionables

--Begin procedure permissionable.RemovePermissionableTemplate
EXEC Utility.DropObject 'permissionable.RemovePermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Jonathan Burnham
-- Create date: 2018.08.14
-- Description:	A stored procedure to remove a permissionable template from one or more person id's
-- =================================================================================================
CREATE PROCEDURE permissionable.RemovePermissionableTemplate

@PermissionableTemplateID INT,
@PersonID INT,
@PersonIDList VARCHAR(MAX)

AS
BEGIN

	DELETE PP
	FROM permissionable.PersonPermissionable PP
		JOIN dbo.ListToTable(@PersonIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.PersonID
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PermissionableTemplatePermissionable PTP
				WHERE PTP.PermissionableLineage = PP.PermissionableLineage
					AND PTP.PermissionableTemplateID = @PermissionableTemplateID
				)

	SET @PersonIDList = STUFF(@PersonIDList, 1, 1, '')

	EXEC eventlog.LogPersonAction @EntityID = 0, @EventCode = 'update', @PersonID = @PersonID, @Comments = NULL, @EntityIDList = @PersonIDList

END
GO
--End procedure permissionable.RemovePermissionableTemplate

