USE AJACS
GO

--Begin table dropdown.AuditOutcome
UPDATE AO
SET AO.AuditOutcomeName = 'No Update' 
FROM dropdown.AuditOutcome AO
WHERE AO.AuditOutcomeName = 'Not Audited'
GO
--End table dropdown.AuditOutcome
