USE AJACS
GO

--Begin function dbo.GetContactEquipmentEligibility
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibility
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @bReturn = 
		IIF(
			(@cFundingSourceCode = 'US' AND C.USVettingExpirationDate >= getDate())
			OR (
				(@cFundingSourceCode = 'EU' OR @cFundingSourceCode = 'USEU') 
				AND (
					C.UKVettingExpirationDate >= getDate()
					AND (
						C.USVettingExpirationDate >= getDate()
						OR (
							SELECT TOP 1 VO.VettingOutcomeName
							FROM dbo.ContactVetting CV
							JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
							JOIN dropdown.contactvettingtype VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID AND VT.ContactVettingTypename = 'US'
							WHERE CV.ContactID = C.ContactID
							ORDER BY CV.vettingdate DESC, CV.ContactVettingID DESC
						) = 'Submitted for Vetting'
					)
				)
			),
		1,0)
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibility

--Begin function dbo.GetContactEquipmentEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityNotes
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @cReturn = 
		IIF(
			(@cFundingSourceCode = 'US' AND C.USVettingExpirationDate >= getDate())
			OR (
				(@cFundingSourceCode = 'EU' OR @cFundingSourceCode = 'USEU') 
				AND (
					C.UKVettingExpirationDate >= getDate()
					AND (
						C.USVettingExpirationDate >= getDate()
						OR (
							SELECT TOP 1 VO.VettingOutcomeName
							FROM dbo.ContactVetting CV
							JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
							JOIN dropdown.contactvettingtype VT ON VT.ContactVettingTypeID = CV.ContactVettingTypeID AND VT.ContactVettingTypename = 'US'
							WHERE CV.ContactID = C.ContactID
							ORDER BY CV.vettingdate DESC, CV.ContactVettingID DESC
						) = 'Submitted for Vetting'
					)
				)
			),
		'Contact Eligible','Contact Ineligible')
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	RETURN @cReturn

END
GO
--End function dbo.GetContactEquipmentEligibilityNotes

--Begin function dbo.GetContactStipendEligibility
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibility
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'USEU' AND C2.IsUSEquipmentTransferEligible = 1 AND C2.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibility

--Begin function dbo.GetContactStipendEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotes
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'USEU' AND C2.IsUSEquipmentTransferEligible = 1 AND C2.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotes