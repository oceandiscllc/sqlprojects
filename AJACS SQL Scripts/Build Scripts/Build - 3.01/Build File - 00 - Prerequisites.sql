USE AJACS
GO

--Begin schema logicalframeworkupdate
EXEC utility.AddSchema 'logicalframeworkupdate'
GO
--End schema logicalframeworkupdate

--Begin procedure utility.RenameColumn
EXEC utility.DropObject 'utility.RenameColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	A procedure to rename a column
-- ===========================================
CREATE PROCEDURE utility.RenameColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@NewColumnName VARCHAR(250)
)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cTableNameColumnName VARCHAR(501) = @TableName + '.' + @ColumnName
	IF EXISTS (SELECT 1 FROM dbo.SysColumns SC WHERE SC.ID = OBJECT_ID(@TableName) AND SC.Name = @ColumnName)
		EXEC sp_RENAME @cTableNameColumnName, @NewColumnName, 'COLUMN';
	--ENDIF

END
GO
--End procedure utility.RenameColumn