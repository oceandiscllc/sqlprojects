USE AJACS
GO

--Begin function dbo.GetConceptNoteStatus
EXEC utility.DropObject 'dbo.GetConceptNoteStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.01
-- Description:	A function to return a status varchar for a concept note
-- =====================================================================
CREATE FUNCTION dbo.GetConceptNoteStatus
(
@ConceptNoteID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cStatus VARCHAR(50) = ''
	
	SELECT @cStatus = 
		CASE
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'OnHold')
			THEN
				CASE
					WHEN CN.ConceptNoteTypeCode = 'ConceptNote'
					THEN 'Concept Note On Hold'
					WHEN CN.ConceptNoteTypeCode = 'ActivitySheet'
					THEN 'Activity Sheet On Hold'
					ELSE CN.ConceptNoteTypeCode + ' On Hold'
				END
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Cancelled')
			THEN
				CASE
					WHEN CN.ConceptNoteTypeCode = 'ConceptNote'
					THEN 'Concept Note Cancelled'
					WHEN CN.ConceptNoteTypeCode = 'ActivitySheet'
					THEN 'Activity Sheet Cancelled'
					ELSE CN.ConceptNoteTypeCode + ' Cancelled'
				END
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Closed')
			THEN 'Activity Closed'
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Amended')
			THEN 'Activity Amended'
			ELSE (SELECT EWD.WorkflowStepName FROM workflow.GetEntityWorkflowData('ConceptNote', CN.ConceptNoteID) EWD)
		END

	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	RETURN @cStatus
	
END
GO
--End function dbo.GetConceptNoteStatus

--Begin function dbo.GetContactEquipmentEligibility
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibility
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		CASE
			WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) 
			THEN 1 
			ELSE 0 
			END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 1
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibility

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
--
-- Author:			Brandon Green
-- Create date:	2017.07.17
-- Description:	Rebuilt
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cReturn VARCHAR(50)
	
	SELECT @cReturn = SR.SpotReportReferenceCode
	FROM dbo.SpotReport SR
	WHERE SR.SpotReportID = @SpotReportID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function document.GetDocumentNameByDocumentID
EXEC utility.DropObject 'document.GetDocumentNameByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.29
-- Description:	A function to return the name of a Document
--
-- Author:			Damon Miller
-- Create Date:	2017.07.19
-- Description:	Altered to repoint to the DocumentTitle field
-- ==========================================================

CREATE FUNCTION document.GetDocumentNameByDocumentID
(
@DocumentID INT
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cDocumentName VARCHAR(500) = ''
	
	SELECT @cDocumentName = D.DocumentTitle
	FROM document.Document D
	WHERE D.DocumentID = @DocumentID

	RETURN ISNULL(@cDocumentName, '')

END
GO
--End function document.GetDocumentNameByDocumentID

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
--
-- Author:			Todd Pires
-- Create date:	2017.07.01
-- Description:	Tweaked for WeeklyReport
--
-- Author:			Jonathan Burnham
-- Create date:	2017.07.10
-- Description:	Added LogicalFrameworkUpdate
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nEntityID INT = @EntityID
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nEntityID = T.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nEntityID = T.RiskUpdateID FROM riskupdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'LogicalFrameworkUpdate' AND @nEntityID <> 0
			SELECT TOP 1 @nEntityID = T.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate T ORDER BY T.LogicalFrameworkUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport' AND @nEntityID <> 0
			SELECT TOP 1 @nEntityID = T.WeeklyReportID FROM weeklyreport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
			
		END
	--ENDIF

	IF @nEntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @nEntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @nEntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @nEntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
	
END
GO
--End function workflow.CanHaveAddUpdate