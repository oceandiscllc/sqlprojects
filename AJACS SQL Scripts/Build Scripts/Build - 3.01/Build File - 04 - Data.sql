USE AJACS
GO

DELETE ET
FROM dbo.EntityType ET 
WHERE ET.EntityTypeCode = 'LogicalFrameworkUpdate'
GO 

INSERT INTO dbo.EntityType 
	(EntityTypeCode, EntityTypeName, CanRejectAfterFinalApproval, HasWorkflow, HasMenuItemAccessViaWorkflow, IsActive, IsForNewsLetter)
VALUES 
	('LogicalFrameworkUpdate', 'Logical Framework Update', 0, 1, 0, 1, 0)
GO

--Begin table dropdown.LogicalFrameworkStatus
IF NOT EXISTS (SELECT 1 FROM dropdown.LogicalFrameworkStatus LFS WHERE LFS.LogicalFrameworkStatusName = 'Progressing')
	INSERT INTO dropdown.LogicalFrameworkStatus (LogicalFrameworkStatusName) VALUES ('Progressing')
--ENDIF

UPDATE LFS
SET
	LFS.DisplayOrder = 
		CASE
			WHEN LFS.LogicalFrameworkStatusName = 'Cancelled'
			THEN 7
			WHEN LFS.LogicalFrameworkStatusName = 'Closed'
			THEN 6
			WHEN LFS.LogicalFrameworkStatusName = 'Completed'
			THEN 5
			WHEN LFS.LogicalFrameworkStatusName = 'Not Started'
			THEN 1
			WHEN LFS.LogicalFrameworkStatusName = 'Off Track'
			THEN 2
			WHEN LFS.LogicalFrameworkStatusName = 'On Track'
			THEN 4
			WHEN LFS.LogicalFrameworkStatusName = 'Progressing'
			THEN 3
			ELSE 0
		END,

	LFS.HexColor = 
		CASE
			WHEN LFS.LogicalFrameworkStatusName = 'Completed'
			THEN '78cd51'
			WHEN LFS.LogicalFrameworkStatusName = 'Off Track'
			THEN 'ff5454'
			WHEN LFS.LogicalFrameworkStatusName = 'On Track'
			THEN '78cd51'
			WHEN LFS.LogicalFrameworkStatusName = 'Progressing'
			THEN 'fabb3d'
			ELSE 'ffffff'
		END
FROM dropdown.LogicalFrameworkStatus LFS
GO
--End table dropdown.LogicalFrameworkStatus

INSERT INTO logicalframework.IndicatorMilestone
	(IndicatorID, MilestoneID, AchievedValue, InProgressValue, PlannedValue, TargetValue)
SELECT
	M.IndicatorIDDeprecated,
	M.MilestoneID,
	M.AchievedValueDeprecated,
	M.InProgressValueDeprecated,
	M.PlannedValueDeprecated,
	M.TargetValueDeprecated
FROM logicalframework.Milestone M
GO

UPDATE M
SET M.MilestoneDate = ISNULL(M.TargetDateDeprecated, getDate())
FROM logicalframework.Milestone M
GO

--Update menu item
UPDATE MI
SET 
	MI.MenuItemCode = 'LogicalFrameworkUpdate', 
	MI.MenuItemLink = '/logicalframework/logicalframeworkupdate'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'ObjectiveManage'
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='ConceptNoteAnalysis', 
	@NewMenuItemLink='/conceptnote/analysis', 
	@NewMenuItemText='Activity Analysis', 
	@AfterMenuItemCode='ObjectiveManage'
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='IndicatorAnalysis', 
	@NewMenuItemLink='/indicator/analysis', 
	@NewMenuItemText='Indicator Analysis', 
	@AfterMenuItemCode='ActivityAnalysisList'
GO

DECLARE @cSystemName VARCHAR(50)
	
SELECT @cSystemName = SS.ServerSetupValue
FROM dbo.ServerSetup SS
WHERE SS.ServerSetupKey = 'SystemName'

UPDATE SR
SET SR.SpotReportReferenceCode = @cSystemName + '-SR-' + RIGHT('0000' + CAST(SR.SpotReportID AS VARCHAR(10)), 4)
FROM dbo.SpotReport SR
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'LogicalFrameworkUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplate 
		(EntityTypeCode, WorkflowActionCode, EmailText)
	VALUES 
		(
		'LogicalFrameworkUpdate', 'IncrementWorkflow', 
		'<p>An AJACS logical framework update has been submitted for your review:</p>
		<p><strong>Logical Framework Update: </strong>[[Link]]</p>
		<p><strong>Comments:</strong><br />[[Comments]]</p>
		<p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the logical framework update workflow. Please click the link above to review the updated objectives, indicators, and milestones.</p>
		<p>Please do not reply to this email as it is generated automatically by the AJACS system.</p>
		<p>Thank you,<br /><br />The AJACS Team</p>'
		),
		(
		'LogicalFrameworkUpdate', 'DecrementWorkflow', 
		'<p>A previously submitted logical framework update has been disapproved:</p>
		<p><strong>Logical Framework Update: </strong>[[Link]]</p>
		<p><strong>Comments:</strong><br />[[Comments]]</p>
		<p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the logical framework update workflow. Please click the link above to review the updated objectives, indicators, and milestones.</p>
		<p>Please do not reply to this email as it is generated automatically by the AJACS system.</p>
		<p>Thank you,<br /><br />The AJACS Team</p>'
		),
		(
		'LogicalFrameworkUpdate', 'Release', 
		'<p>An AJACS logical framework update has been released:</p>
		<p><strong>Comments:</strong><br />[[Comments]]</p>
		<p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the logical framework update workflow. All updated objectives, indicators, and milestones have been released.</p>
		<p>Please do not reply to this email as it is generated automatically by the AJACS system.</p>
		<p>Thank you,<br /><br />The AJACS Team</p>'
		)

	INSERT INTO dbo.EmailTemplateField 
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
	VALUES 
		('LogicalFrameworkUpdate', '[[Link]]', 'Link'),
		('LogicalFrameworkUpdate', '[[Comments]]', 'Comments')

	END
--ENDIF
GO