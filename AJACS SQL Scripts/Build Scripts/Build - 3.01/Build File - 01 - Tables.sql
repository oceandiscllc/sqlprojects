USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.DropColumn @TableName, 'RiskAssessment'
EXEC utility.DropColumn @TableName, 'RiskMitigationMeasures'
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteAsset
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteAsset'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteAsset
	(
	ConceptNoteAssetID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	AssetID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConceptNoteAsset', 'ConceptNoteID, AssetID'
GO
--End table dbo.ConceptNoteAsset

--Begin table dbo.ConceptNoteIndicator
EXEC utility.DropObject 'DF_ConceptNoteIndicator_ActualQuantity'
GO

ALTER TABLE dbo.ConceptNoteIndicator ALTER COLUMN ActualQuantity VARCHAR(10)
GO
--End table dbo.ConceptNoteIndicator

--Begin table dbo.SpotReport
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReport'

EXEC utility.AddColumn @TableName, 'SpotReportReferenceCode', 'VARCHAR(50)'
GO
--End table dbo.SpotReport

--Begin table dropdown.LogicalFrameworkStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LogicalFrameworkStatus'

EXEC utility.AddColumn @TableName, 'HexColor', 'VARCHAR(6)'
GO
--End table dropdown.LogicalFrameworkStatus

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.AddColumn @TableName, 'PositiveExample', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'RiskAssumption', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MeansOfVerification', 'VARCHAR(MAX)'
GO
--End table logicalframework.Indicator

--Begin table logicalframework.IndicatorMilestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.IndicatorMilestone'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframework.IndicatorMilestone
	(
	IndicatorMilestoneID INT IDENTITY(1,1) NOT NULL,
	IndicatorID INT,
	MilestoneID INT,
	AchievedValue INT,
	InProgressValue INT,
	PlannedValue INT,
	TargetValue INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AchievedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TargetValue', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IndicatorMilestoneID'
EXEC utility.SetIndexClustered @TableName, 'IX_IndicatorMilestone', 'IndicatorID, MilestoneID'
GO
--End table logicalframework.IndicatorMilestone

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.AddColumn @TableName, 'MilestoneDate', 'DATETIME', 'getDate()'

EXEC utility.RenameColumn @TableName, 'AchievedValue', 'AchievedValueDeprecated'
EXEC utility.RenameColumn @TableName, 'IndicatorID', 'IndicatorIDDeprecated'
EXEC utility.RenameColumn @TableName, 'InProgressValue', 'InProgressValueDeprecated'
EXEC utility.RenameColumn @TableName, 'PlannedValue', 'PlannedValueDeprecated'
EXEC utility.RenameColumn @TableName, 'TargetDate', 'TargetDateDeprecated'
EXEC utility.RenameColumn @TableName, 'TargetValue', 'TargetValueDeprecated'

EXEC utility.RenameColumn @TableName, 'IsActiveDeprecated', 'IsActive'
GO
--End table logicalframeworkupdate.Milestone

--Begin table logicalframeworkupdate.LogicalFrameworkUpdate
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.LogicalFrameworkUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.LogicalFrameworkUpdate
	(
	LogicalFrameworkUpdateID INT IDENTITY(1,1) NOT NULL,
	LogicalFrameworkUpdateDate DATE,
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LogicalFrameworkUpdateID'
GO
--End table logicalframeworkupdate.LogicalFrameworkUpdate

--Begin table logicalframeworkupdate.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.Indicator'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.Indicator
	(
	LogicalFrameworkUpdateID INT,
	IndicatorID INT,
	IndicatorTypeID INT,
	ObjectiveID INT,
	IndicatorName VARCHAR(100),
	IndicatorDescription VARCHAR(MAX),
	IndicatorSource VARCHAR(MAX),
	BaselineValue VARCHAR(100),
	BaselineDate DATE,
	TargetValue VARCHAR(100),
	TargetDate DATE,
	AchievedValue VARCHAR(100),
	AchievedDate DATE,
	IsActive BIT,
	ActualDate DATE,
	InProgressDate DATE,
	InProgressValue INT,
	LogicalFrameworkStatusID INT,
	PlannedDate DATE,
	PlannedValue INT,
	StatusUpdateDescription NVARCHAR(MAX),
	IndicatorNumber NVARCHAR(50),
	PositiveExample VARCHAR(MAX),
	RiskAssumption VARCHAR(MAX),
	MeansOfVerification VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorID'
GO
--End table logicalframeworkupdate.Indicator

--Begin table logicalframeworkupdate.IndicatorMilestone
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.IndicatorMilestone'

EXEC utility.DropObject 'logicalframeworkupdate.IndicatorMilestone'

CREATE TABLE logicalframeworkupdate.IndicatorMilestone
 (
 IndicatorMilestoneID INT IDENTITY(1,1) NOT NULL,
 LogicalFrameworkUpdateID INT,
 IndicatorID INT,
 MilestoneID INT,
 AchievedValue INT,
 InProgressValue INT,
 PlannedValue INT,
 TargetValue INT
 )

EXEC utility.SetDefaultConstraint @TableName, 'AchievedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TargetValue', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered 'logicalframeworkupdate.IndicatorMilestone', 'IndicatorMilestoneID'
GO
--End table logicalframeworkupdate.IndicatorMilestone

--Begin table logicalframeworkupdate.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.Milestone'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.Milestone
	(
	LogicalFrameworkUpdateID INT,
	MilestoneID INT,
	MilestoneName VARCHAR(100),
	MilestoneDate DATE,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MilestoneID'
GO
--End table logicalframeworkupdate.Milestone

--Begin table logicalframeworkupdate.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.Objective'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.Objective
	(
	LogicalFrameworkUpdateID INT,
	ObjectiveID INT,
	ParentObjectiveID INT,
	ObjectiveTypeID INT,
	ObjectiveName VARCHAR(100),
	ObjectiveDescription VARCHAR(MAX),
	IsActive BIT,
	LogicalFrameworkStatusID INT,
	StatusUpdateDescription NVARCHAR(MAX),
	ComponentReportingAssociationID INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentReportingAssociationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentObjectiveID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ObjectiveID'
GO
--End table logicalframeworkupdate.Objective