USE AJACS
GO

--Begin procedure asset.GetAssetByAssetID
EXEC Utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the asset.Asset table
-- Notes:				Changes here must ALSO be made to asset.GetAssetByEventlogID
-- =========================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Asset
	SELECT
		A.AssetDescription,
		A.AssetID,
		A.AssetName,
		A.CommunityID,
		dbo.GetCommunityNameByCommunityID(A.CommunityID) AS CommunityName,
		A.IsActive,
		A.Location.STAsText() AS Location,
		A.RelocationDate,
		dbo.FormatDate(A.RelocationDate) AS RelocationDateFormatted,
		A.RelocationLocation,
		A.RelocationNotes,
		AST.AssetStatusCode,
		AST.AssetStatusID,
		AST.AssetStatusName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon
	FROM asset.Asset A
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	--AssetConceptNote
	SELECT 
		CN.Title AS ConceptNoteName
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteAsset CNA ON CNA.ConceptNoteID = CN.ConceptNoteID
			AND CNA.AssetID = @AssetID

	--AssetEventLog
	SELECT 
		EL.EventLogID,
		EL.EventCode, 
		EL.EventData, 
		dbo.FormatDateTime(EL.createdatetime) AS CreateDateFormatted, 
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
	WHERE EL.EntityTypeCode = 'Asset' AND EL.entityid = @AssetID AND EL.eventcode != 'read' 

	--AssetPaymentHistory
	EXEC asset.GetAssetPaymentHistoryByAssetID @AssetID = @AssetID

	--AssetTrainingHistory
	SELECT
		COUNT(CC.ContactID) AS StudentCount,
		CR.CourseID,
		CR.CourseName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
	GROUP BY A.AssetID, CR.CourseName, CR.CourseID
	ORDER BY 3

	--AssetUnit
	SELECT
		AU.AssetUnitID,
		AU.AssetUnitName,
		AU.CommanderContactID,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullNameFormatted,
		AU.DeputyCommanderContactID,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullNameFormatted,
		AU.IsActive,
		AUC.AssetUnitCostID,
		AUC.AssetUnitCostName
	FROM asset.AssetUnit AU
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			AND AU.AssetID = @AssetID

	--AssetUnitContactVetting
	;
	WITH CD AS
		(
		SELECT AU.CommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID

		UNION

		SELECT AU.DeputyCommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID
		)

	SELECT 
		dbo.FormatContactNameByContactID(US.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'US' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(US.VettingDate) AS VettingDateFormatted,
		VO1.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN VO1.VettingOutcomeCode = 'DoNotConsider'
			THEN '<img src="/assets/img/icons/EA1921-vetting.png" style="height:25px; width:25px;" /> '
			WHEN ISNULL((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID), dateAdd(day, -1, getDate())) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/32AC41-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			A.ContactID,
			A.ContactVettingID,
			A.VettingDate,
			A.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD1.ContactID ORDER BY CV1.VettingDate DESC, CV1.ContactVettingID DESC) AS RowIndex,
				CD1.ContactID,
				CV1.ContactVettingID,
				CV1.VettingDate,
				CV1.VettingOutcomeID
			FROM CD CD1
				LEFT JOIN dbo.ContactVetting CV1 ON CV1.ContactID = CD1.ContactID
			WHERE CV1.ContactVettingTypeID = 1
			) A
			WHERE A.RowIndex = 1
		) US
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = ISNULL(US.VettingOutcomeID, 0)

	UNION

	SELECT 
		dbo.FormatContactNameByContactID(UK.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'UK' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(UK.VettingDate) AS VettingDateFormatted,
		VO2.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN VO2.VettingOutcomeCode = 'DoNotConsider'
			THEN '<img src="/assets/img/icons/EA1921-vetting.png" style="height:25px; width:25px;" /> '
			WHEN ISNULL((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID), dateAdd(day, -1, getDate())) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/32AC41-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			B.ContactID,
			B.ContactVettingID,
			B.VettingDate,
			B.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD2.ContactID ORDER BY CV2.VettingDate DESC, CV2.ContactVettingID DESC) AS RowIndex,
				CD2.ContactID,
				CV2.ContactVettingID,
				CV2.VettingDate,
				CV2.VettingOutcomeID
			FROM CD CD2
				LEFT JOIN dbo.ContactVetting CV2 ON CV2.ContactID = CD2.ContactID
			WHERE CV2.ContactVettingTypeID = 2
			) B
			WHERE B.RowIndex = 1
		) UK
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = ISNULL(UK.VettingOutcomeID, 0)

	ORDER BY 1, 2, 3, 4

END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
--
-- Author:			Todd Pires
-- Update Date: 2016.09.01
-- Description:	Added the ConceptNoteTypeCode field
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
--
-- Author:			Brandon Green
-- Create date:	2017.07.19
-- Description:	Removed deprecated columns
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	--ConceptNote
	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeCode AS ConceptNoteComponentCode,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed,
		CN.AmendedConceptNoteID,
		ACN.Title AS AmendedConceptNoteTitle,
		ABCN.ConceptNoteID AS AmendedByConceptNoteID,
		ABCN.Title AS AmendedByConceptNoteTitle,
		CN.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
		LEFT JOIN dbo.ConceptNote ACN ON ACN.ConceptNoteID = CN.AmendedConceptNoteID
		LEFT JOIN dbo.ConceptNote ABCN ON ABCN.AmendedConceptNoteID = CN.ConceptNoteID
		LEFT JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
	WHERE CN.ConceptNoteID = @ConceptNoteID

	--ConceptNoteAmendment (deprecated)
	SELECT			
		CNA.ConceptNoteAmendmentID,
		CNA.AmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--ConceptNoteAsset
	SELECT 
		A.AssetID,
		A.AssetName
	FROM dbo.ConceptNoteAsset CNA
		JOIN asset.Asset A ON A.AssetID = CNA.AssetID
			AND CNA.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteAuthor
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--ConceptNoteBackgroundText
	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	--ConceptNoteBudget
	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Amendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	--ConceptNoteClass
	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	--ConceptNoteCommunity
	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.ConceptNoteCommunity CNC 
		JOIN dbo.Community C ON C.CommunityID = CNC.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	--ConceptNoteContact
	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= GETDATE() AND OACV12.VettingOutcomeCode != 'DoNotConsider'
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= GETDATE() AND OACV22.VettingOutcomeCode != 'DoNotConsider'
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID, VO1.VettingOutcomeCode
			FROM dbo.ContactVetting CV12
				JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID, VO2.VettingOutcomeCode
			FROM dbo.ContactVetting CV22
				JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1

	--ConceptNoteDocument
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName

	--ConceptNoteEquipmentCatalog
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID

	--ConceptNoteEthnicity
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName
			
	--ConceptNoteFinance
	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID

	--ConceptNoteIndicator
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteProject
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	--ConceptNoteProvince
	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID

	--ConceptNoteRisk
	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
			
	--ConceptNoteTask
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID
	
	--ConceptNoteUpdate
	SELECT
		CNU.ConceptNoteUpdateID,
		CNU.PersonID, 
		dbo.FormatPersonNameByPersonID(CNU.PersonID, 'LastFirst') AS FullNameFormatted,
		CNU.Remarks,
		CNU.UpdateDate,
		dbo.FormatDate(CNU.UpdateDate) AS UpdateDateFormatted,
		CNUT.ConceptNoteUpdateTypeID,
		CNUT.ConceptNoteUpdateTypeName,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		CNU.ActualTotalAmount
	FROM dbo.ConceptNoteUpdate CNU
		JOIN dropdown.ConceptNoteUpdateType CNUT ON CNUT.ConceptNoteUpdateTypeID = CNU.ConceptNoteUpdateTypeID
		LEFT JOIN dbo.Document D ON D.DocumentID = CNU.DocumentID
	WHERE CNU.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteVersion
	;
	WITH HD AS
		(
		SELECT
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			1 AS Depth
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			HD.Depth + 1 AS Depth
		FROM dbo.ConceptNote CN
			JOIN HD ON CN.ConceptNoteID = HD.AmendedConceptNoteID
		)

	SELECT 
		HD.ConceptNoteID,
		HD.Title,
		MAX(HD.Depth) OVER() - HD.Depth + 1 AS VersionNumber,
		(SELECT ISNULL(SUM(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost), 0)
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = HD.ConceptNoteID
		) + (SELECT ISNULL(SUM(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0)
			FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = HD.ConceptNoteID
		) AS TotalCost
	FROM HD
	ORDER BY VersionNumber
	
	--ConceptNoteWorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD

	--ConceptNoteWorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
			WHEN EL.EventCode = 'Cancel'
			THEN 'Canceled Concept Note'
			WHEN EL.EventCode = 'Hold'
			THEN 'Placed Concept Note on Hold'
			WHEN EL.EventCode = 'Unhold'
			THEN 'Reactivated Concept Note'			
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update','Cancel','Hold','Unhold')
	ORDER BY EL.CreateDateTime
	
	--ConceptNoteWorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetRequestForInformationByRequestForInformationID
EXEC Utility.DropObject 'dbo.GetRequestForInformationByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
--
-- Author:			Todd Pires
-- Create date:	2015.04.04
-- Description:	Added the SummaryAnswer field
--
-- Author:			Todd Pires
-- Create date:	2015.04.22
-- Description:	Added the DesiredResponseDate and SpotReportID fields
--
-- Author:			Todd Pires
-- Create date:	2015.11.21
-- Description:	Added the CoverLetterFrom and CoverLetterTo fields
-- ====================================================================================
CREATE PROCEDURE dbo.GetRequestForInformationByRequestForInformationID

@RequestForInformationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFI.Argument,
		RFI.AttachmentsList,
		RFI.Background,
		RFI.CompletedDate,
		dbo.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		RFI.CoverLetterFrom,
		RFI.CoverLetterTo,
		RFI.DesiredResponseDate,
		dbo.FormatDate(RFI.DesiredResponseDate) AS DesiredResponseDateFormatted,
		dbo.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.IncidentDate,
		dbo.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.InformationRequested,
		RFI.InProgressDate,
		RFI.Issue,
		RFI.KnownDetails,
		RFI.Location,
		RFI.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonFullname,
		RFI.Recommendation,
		RFI.RequestDate,
		dbo.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationID,
		RFI.RequestForInformationTitle,
		RFI.RequestPersonID,
		dbo.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonFullname,
		RFI.Resources,
		RFI.Risk,
		RFI.SpotReportID,
		dbo.FormatSpotReportReferenceCode(RFI.SpotReportID) AS SpotReportReferenceCode,
		RFI.SummaryAnswer,
		RFI.Timing,
		C2.CommunityID,
		C2.CommunityName,
		RFIRT.RequestForInformationResultTypeCode,
		RFIRT.RequestForInformationResultTypeID,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusCode,
		RFIS.RequestForInformationStatusID,
		RFIS.RequestForInformationStatusName,
		dbo.GetEntityTypeNameByEntityTypeCode('RequestForInformation') AS EntityTypeName
	FROM dbo.RequestForInformation RFI
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFI.RequestForInformationID = @RequestForInformationID
		OUTER APPLY
				(
				SELECT
					C1.CommunityID,
					C1.CommunityName
				FROM dbo.Community C1
				WHERE C1.CommunityID = RFI.CommunityID
				) C2

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentFileName,
		DE.DocumentEntityID
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'RequestForInformation'
			AND DE.EntityID = @RequestForInformationID

END
GO
--End procedure dbo.GetRequestForInformationByRequestForInformationID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:			Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
--
-- Author:			Todd Pires
-- Update date:	2016.08.30
-- Description:	Removed the SpotReport documents recordset
--
-- Author:			Brandon Green
-- Create date:	2017.07.17
-- Description:	Added SpotReportReferenceCode support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('SpotReport', @SpotReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('SpotReport', @SpotReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @SpotReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END 
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dbo.GetStationCommanderSurveyByStationCommanderSurveyID
EXEC Utility.DropObject 'dbo.GetStationCommanderSurveyByStationCommanderSurveyID'
GO
--End procedure dbo.GetStationCommanderSurveyByStationCommanderSurveyID

--Begin procedure dbo.GetVettingExpirationCounts
EXEC Utility.DropObject 'dbo.GetVettingExpirationCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A procedure to get counts of expired and soon to expire vetting records
--
-- Author:			Todd Pires
-- Create date:	2017.07.08
-- Description:	Modified to exclude inactive contacts
-- ====================================================================================
CREATE PROCEDURE dbo.GetVettingExpirationCounts

AS
BEGIN
	SET NOCOUNT ON;

	SELECT D.*
	FROM
		(
		SELECT
			(
			SELECT COUNT(C.ContactID) 
			FROM dbo.Contact C 
			WHERE C.UKVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 14
				AND C.IsActive = 1
			) AS UK14,
			(
			SELECT COUNT(C.ContactID) 
			FROM dbo.Contact C 
			WHERE C.USVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 14
				AND C.IsActive = 1
			) AS US14
		) AS D

	SELECT D.*
	FROM
		(
		SELECT
			(
			SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] 
			FROM dbo.Contact C 
			WHERE C.UKVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 14 
				AND C.IsActive = 1
			ORDER BY C.ContactID 
			FOR XML PATH ('')), 1, 1, '')
			) AS UK14ExpiredContactIDList,
			(
			SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] 
			FROM dbo.Contact C 
			WHERE C.USVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 14 
				AND C.IsActive = 1
			ORDER BY C.ContactID 
			FOR XML PATH ('')), 1, 1, '')
			) AS US14ExpiredContactIDList
		) AS D
		
END
GO
--End procedure dbo.GetVettingExpirationCounts

--Begin procedure document.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetDocumentsByEntityTypeCodeAndEntityID'
EXEC Utility.DropObject 'document.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.01.01
-- Description:	A stored procedure to get data from the document.Document table
--
-- Author:			Todd Pires
-- Create date:	2015.05.21
-- Description:	Modified to support weekly reports
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- ============================================================================
CREATE PROCEDURE document.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.DocumentFileName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID
	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure eventlog.LogLogicalFrameworkUpdateAction
EXEC utility.DropObject 'eventlog.LogLogicalFrameworkUpdateAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2017.07.03
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLogicalFrameworkUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'LogicalFrameworkUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogLogicalFrameworkUpdateTable', 'u')) IS NOT NULL
			DROP TABLE #LogLogicalFrameworkUpdateTable
		--ENDIF
		
		SELECT *
		INTO #LogLogicalFrameworkUpdateTable
		FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU
		WHERE LFU.LogicalFrameworkUpdateID = @EntityID

		DECLARE @cLogicalFrameworkUpdateObjectives VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateObjectives = COALESCE(@cLogicalFrameworkUpdateObjectives, '') + D.LogicalFrameworkUpdateObjective 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateObjective'), ELEMENTS) AS LogicalFrameworkUpdateObjective
			FROM logicalframeworkupdate.Objective T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D

		DECLARE @cLogicalFrameworkUpdateIndicators VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateIndicators = COALESCE(@cLogicalFrameworkUpdateIndicators, '') + D.LogicalFrameworkUpdateIndicator 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateIndicator'), ELEMENTS) AS LogicalFrameworkUpdateIndicator
			FROM logicalframeworkupdate.Indicator T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D

		DECLARE @cLogicalFrameworkUpdateIndicatorMilestones VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateIndicatorMilestones = COALESCE(@cLogicalFrameworkUpdateIndicatorMilestones, '') + D.LogicalFrameworkUpdateIndicatorMilestone 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateIndicatorMilestone'), ELEMENTS) AS LogicalFrameworkUpdateIndicatorMilestone
			FROM logicalframeworkupdate.IndicatorMilestone T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D

		DECLARE @cLogicalFrameworkUpdateMilestones VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateMilestones = COALESCE(@cLogicalFrameworkUpdateMilestones, '') + D.LogicalFrameworkUpdateMilestone 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateMilestone'), ELEMENTS) AS LogicalFrameworkUpdateMilestone
			FROM logicalframeworkupdate.Milestone T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'LogicalFrameworkUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<LogicalFrameworkUpdateObjectives>' + ISNULL(@cLogicalFrameworkUpdateObjectives, '') + '</LogicalFrameworkUpdateObjectives>') AS XML),
			CAST(('<LogicalFrameworkUpdateIndicators>' + ISNULL(@cLogicalFrameworkUpdateIndicators, '') + '</LogicalFrameworkUpdateIndicators>') AS XML),
			CAST(('<LogicalFrameworkUpdateIndicatorMilestones>' + ISNULL(@cLogicalFrameworkUpdateIndicatorMilestones, '') + '</LogicalFrameworkUpdateIndicatorMilestones>') AS XML),
			CAST(('<LogicalFrameworkUpdateMilestones>' + ISNULL(@cLogicalFrameworkUpdateMilestones, '') + '</LogicalFrameworkUpdateMilestones>') AS XML)
			FOR XML RAW('LogicalFrameworkUpdate'), ELEMENTS
			)
		FROM #LogLogicalFrameworkUpdateTable T
			JOIN logicalframeworkupdate.LogicalFrameworkUpdate LFU ON LFU.LogicalFrameworkUpdateID = T.LogicalFrameworkUpdateID

		DROP TABLE #LogLogicalFrameworkUpdateTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLogicalFrameworkUpdateAction

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
--
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	Add 'short' formats
--
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	Removed the ActualValue column
--
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	Refactored to support new requirments
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.MeansOfVerification,
		I.PlannedDate,
		I.PositiveExample,
		dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.RiskAssumption,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

	--IndicatorMilestone
	SELECT
		IM.AchievedValue,
		IM.InProgressValue,
		IM.PlannedValue,
		IM.TargetValue,
		M.MilestoneID,
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
		M.MilestoneName
	FROM logicalframework.IndicatorMilestone IM
		JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
			AND IM.IndicatorID = @IndicatorID
	ORDER BY M.MilestoneName, M.MilestoneID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetMilestoneByMilestoneID
EXEC Utility.DropObject 'logicalframework.GetMilestoneByMilestoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
--
-- Author:			Todd Pires
-- Create date:	2017.07.04
-- Description:	Add the IsActive bit
--
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	Refactored to support new requirments
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByMilestoneID

@MilestoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Milestone
	SELECT
		M.MilestoneID, 	
		M.MilestoneDate,
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
		M.MilestoneName,
		M.IsActive
	FROM logicalframework.Milestone M
	WHERE M.MilestoneID = @MilestoneID

END
GO
--End procedure logicalframework.GetMilestoneByMilestoneID

--Begin procedure logicalframeworkupdate.ApproveLogicalFrameworkUpdate
EXEC utility.DropObject 'logicalframeworkupdate.ApproveLogicalFrameworkUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to submit Objectives, Indicators, and Milestones in a logicalframework update for approval
-- ==========================================================================================================================
CREATE PROCEDURE logicalframeworkupdate.ApproveLogicalFrameworkUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @nLogicalFrameworkUpdateID INT
	DECLARE @tOutput1 TABLE (EntityTypeCode VARCHAR(50), EntityID INT)
	DECLARE @tOutput2 TABLE (LogicalFrameworkUpdateID INT)

	SELECT @nLogicalFrameworkUpdateID = LFU.LogicalFrameworkUpdateID
	FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU

	UPDATE O
	SET
		O.ParentObjectiveID = LFUO.ParentObjectiveID, 
		O.ObjectiveTypeID = LFUO.ObjectiveTypeID, 
		O.ObjectiveName = LFUO.ObjectiveName, 
		O.ObjectiveDescription = LFUO.ObjectiveDescription, 
		O.IsActive = LFUO.IsActive, 
		O.LogicalFrameworkStatusID = LFUO.LogicalFrameworkStatusID, 
		O.StatusUpdateDescription = LFUO.StatusUpdateDescription, 
		O.ComponentReportingAssociationID = LFUO.ComponentReportingAssociationID
	OUTPUT 'Objective', INSERTED.ObjectiveID INTO @tOutput1
	FROM logicalframework.Objective O
		JOIN logicalframeworkupdate.Objective LFUO ON LFUO.ObjectiveID = O.ObjectiveID
			AND LFUO.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	UPDATE I
	SET
		I.IndicatorTypeID = LFUI.IndicatorTypeID, 
		I.ObjectiveID = LFUI.ObjectiveID, 
		I.IndicatorName = LFUI.IndicatorName, 
		I.IndicatorDescription = LFUI.IndicatorDescription, 
		I.IndicatorSource = LFUI.IndicatorSource, 
		I.PositiveExample = LFUI.PositiveExample, 
		I.RiskAssumption = LFUI.RiskAssumption, 
		I.MeansOfVerification = LFUI.MeansOfVerification, 
		I.BaselineValue = LFUI.BaselineValue, 
		I.BaselineDate = LFUI.BaselineDate, 
		I.TargetValue = LFUI.TargetValue, 
		I.TargetDate = LFUI.TargetDate, 
		I.AchievedValue = LFUI.AchievedValue, 
		I.AchievedDate = LFUI.AchievedDate, 
		I.IsActive = LFUI.IsActive, 
		I.ActualDate = LFUI.ActualDate, 
		I.InProgressDate = LFUI.InProgressDate, 
		I.InProgressValue = LFUI.InProgressValue, 
		I.LogicalFrameworkStatusID = LFUI.LogicalFrameworkStatusID, 
		I.PlannedDate = LFUI.PlannedDate, 
		I.PlannedValue = LFUI.PlannedValue, 
		I.StatusUpdateDescription = LFUI.StatusUpdateDescription, 
		I.IndicatorNumber = LFUI.IndicatorNumber
	OUTPUT 'Indicator', INSERTED.IndicatorID INTO @tOutput1
	FROM logicalframework.Indicator I
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = I.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	DELETE T
	FROM logicalframework.IndicatorMilestone T
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = T.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	INSERT INTO logicalframework.IndicatorMilestone
		(IndicatorID, MilestoneID, AchievedValue, InProgressValue, PlannedValue, TargetValue)
	SELECT
		T.IndicatorID, 
		T.MilestoneID, 
		T.AchievedValue, 
		T.InProgressValue, 
		T.PlannedValue, 
		T.TargetValue
	FROM logicalframeworkupdate.IndicatorMilestone T
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = T.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	UPDATE M
	SET
		M.MilestoneName = LFUM.MilestoneName, 
		M.IsActive = LFUM.IsActive,  
		M.MilestoneDate = LFUM.MilestoneDate
	OUTPUT 'Milestone', INSERTED.MilestoneID INTO @tOutput1
	FROM logicalframework.Milestone M
		JOIN logicalframeworkupdate.Milestone LFUM ON LFUM.MilestoneID = M.MilestoneID
			AND LFUM.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O1.EntityTypeCode, O1.EntityID
		FROM @tOutput1 O1
		ORDER BY O1.EntityTypeCode, O1.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Objective'
			BEGIN
			
			EXEC eventlog.LogObjectiveAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogObjectiveAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Indicator'
			BEGIN
			
			EXEC eventlog.LogIndicatorAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogIndicatorAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Milestone'
			BEGIN
			
			EXEC eventlog.LogMilestoneAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogMilestoneAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogLogicalFrameworkUpdateAction @nLogicalFrameworkUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogLogicalFrameworkUpdateAction @nLogicalFrameworkUpdateID, 'update', @PersonID, NULL
	
	DELETE FROM logicalframeworkupdate.LogicalFrameworkUpdate

	TRUNCATE TABLE logicalframeworkupdate.Objective
	TRUNCATE TABLE logicalframeworkupdate.Indicator
	TRUNCATE TABLE logicalframeworkupdate.IndicatorMilestone
	TRUNCATE TABLE logicalframeworkupdate.Milestone

END
GO
--End procedure logicalframeworkupdate.ApproveLogicalFrameworkUpdate

--Begin procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator
EXEC utility.DropObject 'logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to remove an indicator from the logical framework update
-- ========================================================================================
CREATE PROCEDURE logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM logicalframeworkupdate.Indicator T
	WHERE T.IndicatorID = @IndicatorID

	DELETE T
	FROM logicalframeworkupdate.IndicatorMilestone T
	WHERE T.IndicatorID = @IndicatorID

END
GO
--End procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator

--Begin procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone
EXEC utility.DropObject 'logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to remove a milestone from the logical framework update
-- =======================================================================================
CREATE PROCEDURE logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone

@MilestoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM logicalframeworkupdate.Milestone T
	WHERE T.MilestoneID = @MilestoneID

END
GO
--End procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone

--Begin procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective
EXEC utility.DropObject 'logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to remove an objective from the logical framework update
-- ========================================================================================
CREATE PROCEDURE logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM logicalframeworkupdate.Objective T
	WHERE T.ObjectiveID = @ObjectiveID

END
GO
--End procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdate
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.02
-- Description:	A stored procedure to get data from the logicalframeworkupdate.LogicalFrameworkUpdate table
-- ================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nLogicalFrameworkUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU)
		BEGIN
		
		DECLARE @tOutput TABLE (LogicalFrameworkUpdateID INT)

		INSERT INTO logicalframeworkupdate.LogicalFrameworkUpdate 
			(WorkflowStepNumber,LogicalFrameworkUpdateDate) 
		OUTPUT INSERTED.LogicalFrameworkUpdateID INTO @tOutput
		VALUES 
			(1,getDate())

		SELECT @nLogicalFrameworkUpdateID = O.LogicalFrameworkUpdateID FROM @tOutput O
		
		EXEC eventlog.LogLogicalFrameworkUpdateAction @EntityID=@nLogicalFrameworkUpdateID, @EventCode='Create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='LogicalFrameworkUpdate', @EntityID=@nLogicalFrameworkUpdateID

		END
	ELSE
		SELECT @nLogicalFrameworkUpdateID = LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('LogicalFrameworkUpdate', @nLogicalFrameworkUpdateID)
	
	SELECT
		LFU.LogicalFrameworkUpdateID, 
		LFU.LogicalFrameworkUpdateDate,
		dbo.FormatDate(LFU.LogicalFrameworkUpdateDate) AS LogicalFrameworkUpdateDateFormatted,
		LFU.WorkflowStepNumber 
	FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('LogicalFrameworkUpdate', @nLogicalFrameworkUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('LogicalFrameworkUpdate', @nLogicalFrameworkUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Logical Framework Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Logical Framework Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Logical Framework Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Logical Framework Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'LogicalFrameworkUpdate'
		AND EL.EntityID = @nLogicalFrameworkUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdate

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to get logicalframework data for the objective update
-- ========================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator

@IndicatorID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.PositiveExample, 
		I.RiskAssumption, 
		I.MeansOfVerification, 
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

	SELECT
		IM.AchievedValue, 
		IM.IndicatorID, 
		IM.InProgressValue, 
		IM.PlannedValue,
		IM.TargetValue,
		M.MilestoneID, 
		M.MilestoneName, 
		M.IsActive, 
		M.MilestoneDate, 
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
	FROM logicalframework.IndicatorMilestone IM
		JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
	WHERE IM.IndicatorID = @IndicatorID

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Indicator I WHERE I.IndicatorID =  @IndicatorID)
		BEGIN

		SELECT
			I.AchievedDate,
			dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
			LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
			I.AchievedValue, 	
			I.ActualDate,
			dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
			LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
			I.BaselineDate, 	
			dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
			LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
			I.BaselineValue, 	
			I.IndicatorDescription,	
			I.IndicatorID, 	
			I.IndicatorName, 	
			I.IndicatorNumber,
			I.IndicatorSource, 	
			I.PositiveExample, 
			I.RiskAssumption, 
			I.MeansOfVerification, 	
			I.InProgressDate,
			dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
			LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
			I.InProgressValue,
			I.IsActive,
			I.PlannedDate,
			dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
			LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
			I.PlannedValue,
			I.StatusUpdateDescription,
			I.TargetDate, 	
			dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
			LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
			I.TargetValue, 	
			IT.IndicatorTypeID, 	
			IT.IndicatorTypeName, 
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O.ObjectiveID, 	
			O.ObjectiveName, 	
			dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
		FROM logicalframeworkupdate.Indicator I
			JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
			JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
				AND I.IndicatorID = @IndicatorID

		END
	ELSE
		BEGIN

		SELECT
			I.AchievedDate,
			dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
			LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
			I.AchievedValue, 	
			I.ActualDate,
			dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
			LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
			I.BaselineDate, 	
			dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
			LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
			I.BaselineValue, 	
			I.IndicatorDescription,	
			I.IndicatorID, 	
			I.IndicatorName, 	
			I.IndicatorNumber,
			I.IndicatorSource, 	
			I.PositiveExample, 
			I.RiskAssumption, 
			I.MeansOfVerification, 	
			I.InProgressDate,
			dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
			LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
			I.InProgressValue,
			I.IsActive,
			I.PlannedDate,
			dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
			LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
			I.PlannedValue,
			I.StatusUpdateDescription,
			I.TargetDate, 	
			dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
			LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
			I.TargetValue, 	
			IT.IndicatorTypeID, 	
			IT.IndicatorTypeName, 
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O.ObjectiveID, 	
			O.ObjectiveName, 	
			dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
		FROM logicalframework.Indicator I
			JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
			JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
				AND I.IndicatorID = @IndicatorID

		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Indicator I WHERE I.IndicatorID =  @IndicatorID)
		BEGIN

		SELECT
			IM.AchievedValue, 
			IM.IndicatorID, 
			IM.InProgressValue, 
			IM.PlannedValue,
			IM.TargetValue,
			M.MilestoneID, 
			M.MilestoneName, 
			M.IsActive,  
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
		FROM logicalframeworkupdate.IndicatorMilestone IM
			JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
				AND IM.IndicatorID = @IndicatorID

		END
	ELSE
		BEGIN

		SELECT
			IM.AchievedValue, 
			IM.IndicatorID, 
			IM.InProgressValue, 
			IM.PlannedValue,
			IM.TargetValue,
			M.MilestoneID, 
			M.MilestoneName, 
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
			LEFT(DATENAME(month, M.MilestoneDate), 3) + '-' + RIGHT(CAST(YEAR(M.MilestoneDate) AS CHAR(4)), 2) AS MilestoneDateFormattedShort
		FROM logicalframework.IndicatorMilestone IM
			JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
				AND IM.IndicatorID = @IndicatorID

		END
	--ENDIF

END
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to get logicalframework data for the objective update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone

@MilestoneID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		M.MilestoneID, 
		M.MilestoneName, 
		M.IsActive,  
		M.MilestoneDate, 
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
	FROM logicalframework.Milestone M
	WHERE M.MilestoneID = @MilestoneID

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Milestone M WHERE M.MilestoneID =  @MilestoneID)
		BEGIN

		SELECT
			M.MilestoneID, 
			M.MilestoneName, 
			M.IsActive,  
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
		FROM logicalframeworkupdate.Milestone M
		WHERE M.MilestoneID = @MilestoneID

		END
	ELSE
		BEGIN

		SELECT
			M.MilestoneID, 
			M.MilestoneName, 
			M.IsActive,  
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
		FROM logicalframework.Milestone M
		WHERE M.MilestoneID = @MilestoneID

		END
	--ENDIF

END
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdateObjective
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdateObjective'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to get logicalframework data for the objective update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdateObjective

@ObjectiveID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CRA.ComponentReportingAssociationCode,
		CRA.ComponentReportingAssociationID,
		CRA.ComponentReportingAssociationName,
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O1.IsActive,
		O1.ObjectiveDescription,
		O1.ObjectiveID, 
		O1.ObjectiveName, 
		O1.ParentObjectiveID, 

		CASE
			WHEN O1.ParentObjectiveID = 0
			THEN NULL
			ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
		END AS ParentObjectiveName,
		
		O1.StatusUpdateDescription,
		OT.ObjectiveTypeID, 
		OT.ObjectiveTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
	FROM logicalframework.Objective O1
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O1.ComponentReportingAssociationID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
			AND O1.ObjectiveID = @ObjectiveID

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Objective O WHERE O.ObjectiveID =  @ObjectiveID)
		BEGIN

		SELECT
			CRA.ComponentReportingAssociationCode,
			CRA.ComponentReportingAssociationID,
			CRA.ComponentReportingAssociationName,
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O1.IsActive,
			O1.ObjectiveDescription,
			O1.ObjectiveID, 
			O1.ObjectiveName, 
			O1.ParentObjectiveID, 

			CASE
				WHEN O1.ParentObjectiveID = 0
				THEN NULL
				ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
			END AS ParentObjectiveName,
			
			O1.StatusUpdateDescription,
			OT.ObjectiveTypeID, 
			OT.ObjectiveTypeName,
			dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
		FROM logicalframeworkupdate.Objective O1
			JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O1.ComponentReportingAssociationID
			JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
				AND O1.ObjectiveID = @ObjectiveID

		END
	ELSE
		BEGIN

		SELECT
			CRA.ComponentReportingAssociationCode,
			CRA.ComponentReportingAssociationID,
			CRA.ComponentReportingAssociationName,
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O1.IsActive,
			O1.ObjectiveDescription,
			O1.ObjectiveID, 
			O1.ObjectiveName, 
			O1.ParentObjectiveID, 

			CASE
				WHEN O1.ParentObjectiveID = 0
				THEN NULL
				ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
			END AS ParentObjectiveName,
			
			O1.StatusUpdateDescription,
			OT.ObjectiveTypeID, 
			OT.ObjectiveTypeName,
			dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
		FROM logicalframework.Objective O1
			JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O1.ComponentReportingAssociationID
			JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
				AND O1.ObjectiveID = @ObjectiveID

		END
	--ENDIF

END
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdateObjective

--Begin procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators
EXEC utility.DropObject 'logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.16
-- Description:	A stored procedure to add indicator data to the logical framework update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators

@IndicatorIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO logicalframeworkupdate.Indicator
		(IndicatorID, IndicatorTypeID, ObjectiveID, IndicatorName, IndicatorDescription, IndicatorSource, BaselineValue, BaselineDate, TargetValue, TargetDate, AchievedValue, AchievedDate, IsActive, ActualDate, InProgressDate, InProgressValue, LogicalFrameworkStatusID, PlannedDate, PlannedValue, StatusUpdateDescription, IndicatorNumber, LogicalFrameworkUpdateID)
	SELECT 
		I.IndicatorID, 
		I.IndicatorTypeID, 
		I.ObjectiveID, 
		I.IndicatorName, 
		I.IndicatorDescription, 
		I.IndicatorSource, 
		I.BaselineValue, 
		I.BaselineDate, 
		I.TargetValue, 
		I.TargetDate, 
		I.AchievedValue, 
		I.AchievedDate, 
		I.IsActive, 
		I.ActualDate, 
		I.InProgressDate, 
		I.InProgressValue, 
		I.LogicalFrameworkStatusID, 
		I.PlannedDate, 
		I.PlannedValue, 
		I.StatusUpdateDescription, 
		I.IndicatorNumber, 
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.Indicator I
		JOIN dbo.ListToTable(@IndicatorIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.IndicatorID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.Indicator LFUI
				WHERE LFUI.IndicatorID = I.IndicatorID
				)

	INSERT INTO logicalframeworkupdate.IndicatorMilestone
		(IndicatorID, MilestoneID, AchievedValue, InProgressValue, PlannedValue, TargetValue, LogicalFrameworkUpdateID)
	SELECT
		IM.IndicatorID, 
		IM.MilestoneID, 
		IM.AchievedValue, 
		IM.InProgressValue, 
		IM.PlannedValue, 
		IM.TargetValue, 
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.IndicatorMilestone IM
		JOIN dbo.ListToTable(@IndicatorIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = IM.IndicatorID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.IndicatorMilestone LFUIM
				WHERE LFUIM.IndicatorID = IM.IndicatorID
				)
END
GO
--End procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators

--Begin procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones
EXEC utility.DropObject 'logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.16
-- Description:	A stored procedure to add Milestone data to the logical framework update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones

@MilestoneIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO logicalframeworkupdate.Milestone
		(MilestoneID, MilestoneName, IsActive, MilestoneDate, LogicalFrameworkUpdateID)
	SELECT
		M.MilestoneID, 
		M.MilestoneName,
		M.IsActive,  
		M.MilestoneDate,
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.Milestone M
		JOIN dbo.ListToTable(@MilestoneIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = M.MilestoneID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.Milestone LFUM
				WHERE LFUM.MilestoneID = M.MilestoneID
				)
END
GO
--End procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones

--Begin procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives
EXEC utility.DropObject 'logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.16
-- Description:	A stored procedure to add objective data to the logical framework update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives

@ObjectiveIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO logicalframeworkupdate.Objective
		(ObjectiveID, ParentObjectiveID, ObjectiveTypeID, ObjectiveName, ObjectiveDescription, IsActive, LogicalFrameworkStatusID, StatusUpdateDescription, ComponentReportingAssociationID, LogicalFrameworkUpdateID)
	SELECT
		O.ObjectiveID, 
		O.ParentObjectiveID, 
		O.ObjectiveTypeID, 
		O.ObjectiveName, 
		O.ObjectiveDescription, 
		O.IsActive, 
		O.LogicalFrameworkStatusID, 
		O.StatusUpdateDescription, 
		O.ComponentReportingAssociationID, 
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.Objective O
		JOIN dbo.ListToTable(@ObjectiveIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = O.ObjectiveID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.Objective LFUO
				WHERE LFUO.ObjectiveID = O.ObjectiveID
				)
END
GO
--End procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives

--Begin procedure reporting.GetActivityAnalysis
EXEC Utility.DropObject 'reporting.GetActivityAnalysis'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Kevin Ross
-- Create date: 2017.07.20
-- Description:	A stored procedure to get GetActivityAnalysis data
-- ===============================================================
CREATE PROCEDURE reporting.GetActivityAnalysis

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT		
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS Title,
		workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) AS Status,
		CNS.ConceptNoteStatusCode,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeName,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O.ObjectiveName,
		CNI.TargetQuantity,
		CNI.ActualNumber
	FROM logicalframework.Indicator I
		JOIN Reporting.SearchResult SR ON SR.EntityID = I.IndicatorID AND SR.EntityTypeCode='ActivityAnalysis' AND SR.PersonID = @PersonID
		JOIN dbo.ConceptNoteIndicator CNI ON CNI.IndicatorID = SR.EntityID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode NOT IN ('Amended','Cancelled','OnHold')
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
	ORDER BY 1

END
GO
--End procedure reporting.GetActivityAnalysis

--Begin procedure reporting.GetIndicatorAnalysis
EXEC Utility.DropObject 'reporting.GetIndicatorAnalysis'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Kevin Ross
-- Create date: 2017.07.20
-- Description:	A stored procedure to get GetIndicatorAnalysis data
-- ================================================================
CREATE PROCEDURE reporting.GetIndicatorAnalysis

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT		
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS Title,
		CN.ConceptNoteID,		
		dbo.GetConceptNoteVersionNumberByConceptNoteID(CN.ConceptNoteID) AS VersionNumber,
		workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) AS Status,
		CNS.ConceptNoteStatusCode,
		CN.ConceptNoteTypeCode, 
		CN.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		I.IndicatorName,
		CNI.TargetQuantity,
		CNI.ActualNumber
	FROM dbo.ConceptNote CN
		JOIN Reporting.SearchResult SR ON SR.EntityID = CN.ConceptNoteID AND SR.EntityTypeCode='IndicatorAnalysis' AND SR.PersonID = @PersonID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode NOT IN ('Amended','Cancelled','OnHold')
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dbo.ConceptNoteIndicator CNI ON CNI.ConceptNoteID = SR.EntityID
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
			AND I.IsActive = 1
	ORDER BY 1

END
GO
--End procedure reporting.GetIndicatorAnalysis

--Begin procedure reporting.GetProgramReportDocumentsByProgramReportID
EXEC Utility.DropObject 'reporting.GetProgramReportDocumentsByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date:	2015.04.24
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- =================================================================================
CREATE PROCEDURE reporting.GetProgramReportDocumentsByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

		SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.Documentname,
		DE.DocumentEntityID,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/document/getDocumentByDocumentName/DocumentName/' + D.DocumentFileName AS DocumentURL
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'ProgramReport'
			AND DE.EntityID = @ProgramReportID
	ORDER BY D.DocumentDescription		

END
GO
--End procedure reporting.GetProgramReportDocumentsByProgramReportID