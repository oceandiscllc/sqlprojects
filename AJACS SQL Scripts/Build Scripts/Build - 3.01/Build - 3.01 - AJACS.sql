-- File Name:	Build - 3.01 - AJACS.sql
-- Build Key:	Build - 3.01 - 2017.07.29 21.55.11

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatSpotReportReferenceCode
--		dbo.GetConceptNoteStatus
--		dbo.GetContactEquipmentEligibility
--		document.GetDocumentNameByDocumentID
--		workflow.CanHaveAddUpdate
--
-- Procedures:
--		asset.GetAssetByAssetID
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetRequestForInformationByRequestForInformationID
--		dbo.GetSpotReportBySpotReportID
--		dbo.GetVettingExpirationCounts
--		document.GetDocumentsByEntityTypeCodeAndEntityID
--		eventlog.LogLogicalFrameworkUpdateAction
--		logicalframework.GetIndicatorByIndicatorID
--		logicalframework.GetMilestoneByMilestoneID
--		logicalframeworkupdate.ApproveLogicalFrameworkUpdate
--		logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator
--		logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone
--		logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective
--		logicalframeworkupdate.GetLogicalFrameworkUpdate
--		logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator
--		logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone
--		logicalframeworkupdate.GetLogicalFrameworkUpdateObjective
--		logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators
--		logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones
--		logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives
--		reporting.GetActivityAnalysis
--		reporting.GetIndicatorAnalysis
--		reporting.GetProgramReportDocumentsByProgramReportID
--		utility.RenameColumn
--
-- Schemas:
--		logicalframeworkupdate
--
-- Tables:
--		dbo.ConceptNoteAsset
--		logicalframework.IndicatorMilestone
--		logicalframeworkupdate.Indicator
--		logicalframeworkupdate.IndicatorMilestone
--		logicalframeworkupdate.LogicalFrameworkUpdate
--		logicalframeworkupdate.Milestone
--		logicalframeworkupdate.Objective
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin schema logicalframeworkupdate
EXEC utility.AddSchema 'logicalframeworkupdate'
GO
--End schema logicalframeworkupdate

--Begin procedure utility.RenameColumn
EXEC utility.DropObject 'utility.RenameColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	A procedure to rename a column
-- ===========================================
CREATE PROCEDURE utility.RenameColumn
(
@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@NewColumnName VARCHAR(250)
)

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cTableNameColumnName VARCHAR(501) = @TableName + '.' + @ColumnName
	IF EXISTS (SELECT 1 FROM dbo.SysColumns SC WHERE SC.ID = OBJECT_ID(@TableName) AND SC.Name = @ColumnName)
		EXEC sp_RENAME @cTableNameColumnName, @NewColumnName, 'COLUMN';
	--ENDIF

END
GO
--End procedure utility.RenameColumn
--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.DropColumn @TableName, 'RiskAssessment'
EXEC utility.DropColumn @TableName, 'RiskMitigationMeasures'
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteAsset
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteAsset'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteAsset
	(
	ConceptNoteAssetID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	AssetID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AssetID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteAssetID'
EXEC utility.SetIndexClustered @TableName, 'IX_ConceptNoteAsset', 'ConceptNoteID, AssetID'
GO
--End table dbo.ConceptNoteAsset

--Begin table dbo.ConceptNoteIndicator
EXEC utility.DropObject 'DF_ConceptNoteIndicator_ActualQuantity'
GO

ALTER TABLE dbo.ConceptNoteIndicator ALTER COLUMN ActualQuantity VARCHAR(10)
GO
--End table dbo.ConceptNoteIndicator

--Begin table dbo.SpotReport
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReport'

EXEC utility.AddColumn @TableName, 'SpotReportReferenceCode', 'VARCHAR(50)'
GO
--End table dbo.SpotReport

--Begin table dropdown.LogicalFrameworkStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.LogicalFrameworkStatus'

EXEC utility.AddColumn @TableName, 'HexColor', 'VARCHAR(6)'
GO
--End table dropdown.LogicalFrameworkStatus

--Begin table logicalframework.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframework.Indicator'

EXEC utility.AddColumn @TableName, 'PositiveExample', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'RiskAssumption', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MeansOfVerification', 'VARCHAR(MAX)'
GO
--End table logicalframework.Indicator

--Begin table logicalframework.IndicatorMilestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.IndicatorMilestone'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframework.IndicatorMilestone
	(
	IndicatorMilestoneID INT IDENTITY(1,1) NOT NULL,
	IndicatorID INT,
	MilestoneID INT,
	AchievedValue INT,
	InProgressValue INT,
	PlannedValue INT,
	TargetValue INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'AchievedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TargetValue', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IndicatorMilestoneID'
EXEC utility.SetIndexClustered @TableName, 'IX_IndicatorMilestone', 'IndicatorID, MilestoneID'
GO
--End table logicalframework.IndicatorMilestone

--Begin table logicalframework.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframework.Milestone'

EXEC utility.AddColumn @TableName, 'MilestoneDate', 'DATETIME', 'getDate()'

EXEC utility.RenameColumn @TableName, 'AchievedValue', 'AchievedValueDeprecated'
EXEC utility.RenameColumn @TableName, 'IndicatorID', 'IndicatorIDDeprecated'
EXEC utility.RenameColumn @TableName, 'InProgressValue', 'InProgressValueDeprecated'
EXEC utility.RenameColumn @TableName, 'PlannedValue', 'PlannedValueDeprecated'
EXEC utility.RenameColumn @TableName, 'TargetDate', 'TargetDateDeprecated'
EXEC utility.RenameColumn @TableName, 'TargetValue', 'TargetValueDeprecated'

EXEC utility.RenameColumn @TableName, 'IsActiveDeprecated', 'IsActive'
GO
--End table logicalframeworkupdate.Milestone

--Begin table logicalframeworkupdate.LogicalFrameworkUpdate
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.LogicalFrameworkUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.LogicalFrameworkUpdate
	(
	LogicalFrameworkUpdateID INT IDENTITY(1,1) NOT NULL,
	LogicalFrameworkUpdateDate DATE,
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'LogicalFrameworkUpdateID'
GO
--End table logicalframeworkupdate.LogicalFrameworkUpdate

--Begin table logicalframeworkupdate.Indicator
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.Indicator'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.Indicator
	(
	LogicalFrameworkUpdateID INT,
	IndicatorID INT,
	IndicatorTypeID INT,
	ObjectiveID INT,
	IndicatorName VARCHAR(100),
	IndicatorDescription VARCHAR(MAX),
	IndicatorSource VARCHAR(MAX),
	BaselineValue VARCHAR(100),
	BaselineDate DATE,
	TargetValue VARCHAR(100),
	TargetDate DATE,
	AchievedValue VARCHAR(100),
	AchievedDate DATE,
	IsActive BIT,
	ActualDate DATE,
	InProgressDate DATE,
	InProgressValue INT,
	LogicalFrameworkStatusID INT,
	PlannedDate DATE,
	PlannedValue INT,
	StatusUpdateDescription NVARCHAR(MAX),
	IndicatorNumber NVARCHAR(50),
	PositiveExample VARCHAR(MAX),
	RiskAssumption VARCHAR(MAX),
	MeansOfVerification VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'IndicatorID'
GO
--End table logicalframeworkupdate.Indicator

--Begin table logicalframeworkupdate.IndicatorMilestone
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.IndicatorMilestone'

EXEC utility.DropObject 'logicalframeworkupdate.IndicatorMilestone'

CREATE TABLE logicalframeworkupdate.IndicatorMilestone
 (
 IndicatorMilestoneID INT IDENTITY(1,1) NOT NULL,
 LogicalFrameworkUpdateID INT,
 IndicatorID INT,
 MilestoneID INT,
 AchievedValue INT,
 InProgressValue INT,
 PlannedValue INT,
 TargetValue INT
 )

EXEC utility.SetDefaultConstraint @TableName, 'AchievedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InProgressValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PlannedValue', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'TargetValue', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered 'logicalframeworkupdate.IndicatorMilestone', 'IndicatorMilestoneID'
GO
--End table logicalframeworkupdate.IndicatorMilestone

--Begin table logicalframeworkupdate.Milestone
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.Milestone'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.Milestone
	(
	LogicalFrameworkUpdateID INT,
	MilestoneID INT,
	MilestoneName VARCHAR(100),
	MilestoneDate DATE,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'MilestoneID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'MilestoneID'
GO
--End table logicalframeworkupdate.Milestone

--Begin table logicalframeworkupdate.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframeworkupdate.Objective'

EXEC utility.DropObject @TableName

CREATE TABLE logicalframeworkupdate.Objective
	(
	LogicalFrameworkUpdateID INT,
	ObjectiveID INT,
	ParentObjectiveID INT,
	ObjectiveTypeID INT,
	ObjectiveName VARCHAR(100),
	ObjectiveDescription VARCHAR(MAX),
	IsActive BIT,
	LogicalFrameworkStatusID INT,
	StatusUpdateDescription NVARCHAR(MAX),
	ComponentReportingAssociationID INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'ComponentReportingAssociationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkUpdateID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ObjectiveTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ParentObjectiveID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ObjectiveID'
GO
--End table logicalframeworkupdate.Objective
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.GetConceptNoteStatus
EXEC utility.DropObject 'dbo.GetConceptNoteStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2017.07.01
-- Description:	A function to return a status varchar for a concept note
-- =====================================================================
CREATE FUNCTION dbo.GetConceptNoteStatus
(
@ConceptNoteID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cStatus VARCHAR(50) = ''
	
	SELECT @cStatus = 
		CASE
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'OnHold')
			THEN
				CASE
					WHEN CN.ConceptNoteTypeCode = 'ConceptNote'
					THEN 'Concept Note On Hold'
					WHEN CN.ConceptNoteTypeCode = 'ActivitySheet'
					THEN 'Activity Sheet On Hold'
					ELSE CN.ConceptNoteTypeCode + ' On Hold'
				END
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Cancelled')
			THEN
				CASE
					WHEN CN.ConceptNoteTypeCode = 'ConceptNote'
					THEN 'Concept Note Cancelled'
					WHEN CN.ConceptNoteTypeCode = 'ActivitySheet'
					THEN 'Activity Sheet Cancelled'
					ELSE CN.ConceptNoteTypeCode + ' Cancelled'
				END
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Closed')
			THEN 'Activity Closed'
			WHEN CN.ConceptNoteStatusID = (SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Amended')
			THEN 'Activity Amended'
			ELSE (SELECT EWD.WorkflowStepName FROM workflow.GetEntityWorkflowData('ConceptNote', CN.ConceptNoteID) EWD)
		END

	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	RETURN @cStatus
	
END
GO
--End function dbo.GetConceptNoteStatus

--Begin function dbo.GetContactEquipmentEligibility
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibility
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		CASE
			WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) 
			THEN 1 
			ELSE 0 
			END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 1
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibility

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented the dbo.ServerSetup SYNONYM
--
-- Author:			Brandon Green
-- Create date:	2017.07.17
-- Description:	Rebuilt
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cReturn VARCHAR(50)
	
	SELECT @cReturn = SR.SpotReportReferenceCode
	FROM dbo.SpotReport SR
	WHERE SR.SpotReportID = @SpotReportID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function document.GetDocumentNameByDocumentID
EXEC utility.DropObject 'document.GetDocumentNameByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create Date:	2017.05.29
-- Description:	A function to return the name of a Document
--
-- Author:			Damon Miller
-- Create Date:	2017.07.19
-- Description:	Altered to repoint to the DocumentTitle field
-- ==========================================================

CREATE FUNCTION document.GetDocumentNameByDocumentID
(
@DocumentID INT
)

RETURNS VARCHAR(500)

AS
BEGIN

	DECLARE @cDocumentName VARCHAR(500) = ''
	
	SELECT @cDocumentName = D.DocumentTitle
	FROM document.Document D
	WHERE D.DocumentID = @DocumentID

	RETURN ISNULL(@cDocumentName, '')

END
GO
--End function document.GetDocumentNameByDocumentID

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
--
-- Author:			Todd Pires
-- Create date:	2017.07.01
-- Description:	Tweaked for WeeklyReport
--
-- Author:			Jonathan Burnham
-- Create date:	2017.07.10
-- Description:	Added LogicalFrameworkUpdate
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nEntityID INT = @EntityID
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nEntityID = T.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nEntityID = T.RiskUpdateID FROM riskupdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'LogicalFrameworkUpdate' AND @nEntityID <> 0
			SELECT TOP 1 @nEntityID = T.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate T ORDER BY T.LogicalFrameworkUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport' AND @nEntityID <> 0
			SELECT TOP 1 @nEntityID = T.WeeklyReportID FROM weeklyreport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
			
		END
	--ENDIF

	IF @nEntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @nEntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @nEntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @nEntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
	
END
GO
--End function workflow.CanHaveAddUpdate
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure asset.GetAssetByAssetID
EXEC Utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the asset.Asset table
-- Notes:				Changes here must ALSO be made to asset.GetAssetByEventlogID
-- =========================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Asset
	SELECT
		A.AssetDescription,
		A.AssetID,
		A.AssetName,
		A.CommunityID,
		dbo.GetCommunityNameByCommunityID(A.CommunityID) AS CommunityName,
		A.IsActive,
		A.Location.STAsText() AS Location,
		A.RelocationDate,
		dbo.FormatDate(A.RelocationDate) AS RelocationDateFormatted,
		A.RelocationLocation,
		A.RelocationNotes,
		AST.AssetStatusCode,
		AST.AssetStatusID,
		AST.AssetStatusName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon
	FROM asset.Asset A
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	--AssetConceptNote
	SELECT 
		CN.Title AS ConceptNoteName
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteAsset CNA ON CNA.ConceptNoteID = CN.ConceptNoteID
			AND CNA.AssetID = @AssetID

	--AssetEventLog
	SELECT 
		EL.EventLogID,
		EL.EventCode, 
		EL.EventData, 
		dbo.FormatDateTime(EL.createdatetime) AS CreateDateFormatted, 
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
	WHERE EL.EntityTypeCode = 'Asset' AND EL.entityid = @AssetID AND EL.eventcode != 'read' 

	--AssetPaymentHistory
	EXEC asset.GetAssetPaymentHistoryByAssetID @AssetID = @AssetID

	--AssetTrainingHistory
	SELECT
		COUNT(CC.ContactID) AS StudentCount,
		CR.CourseID,
		CR.CourseName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
	GROUP BY A.AssetID, CR.CourseName, CR.CourseID
	ORDER BY 3

	--AssetUnit
	SELECT
		AU.AssetUnitID,
		AU.AssetUnitName,
		AU.CommanderContactID,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullNameFormatted,
		AU.DeputyCommanderContactID,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullNameFormatted,
		AU.IsActive,
		AUC.AssetUnitCostID,
		AUC.AssetUnitCostName
	FROM asset.AssetUnit AU
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			AND AU.AssetID = @AssetID

	--AssetUnitContactVetting
	;
	WITH CD AS
		(
		SELECT AU.CommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID

		UNION

		SELECT AU.DeputyCommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID
		)

	SELECT 
		dbo.FormatContactNameByContactID(US.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'US' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(US.VettingDate) AS VettingDateFormatted,
		VO1.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN VO1.VettingOutcomeCode = 'DoNotConsider'
			THEN '<img src="/assets/img/icons/EA1921-vetting.png" style="height:25px; width:25px;" /> '
			WHEN ISNULL((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID), dateAdd(day, -1, getDate())) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/32AC41-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			A.ContactID,
			A.ContactVettingID,
			A.VettingDate,
			A.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD1.ContactID ORDER BY CV1.VettingDate DESC, CV1.ContactVettingID DESC) AS RowIndex,
				CD1.ContactID,
				CV1.ContactVettingID,
				CV1.VettingDate,
				CV1.VettingOutcomeID
			FROM CD CD1
				LEFT JOIN dbo.ContactVetting CV1 ON CV1.ContactID = CD1.ContactID
			WHERE CV1.ContactVettingTypeID = 1
			) A
			WHERE A.RowIndex = 1
		) US
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = ISNULL(US.VettingOutcomeID, 0)

	UNION

	SELECT 
		dbo.FormatContactNameByContactID(UK.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'UK' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(UK.VettingDate) AS VettingDateFormatted,
		VO2.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN VO2.VettingOutcomeCode = 'DoNotConsider'
			THEN '<img src="/assets/img/icons/EA1921-vetting.png" style="height:25px; width:25px;" /> '
			WHEN ISNULL((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID), dateAdd(day, -1, getDate())) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/32AC41-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			B.ContactID,
			B.ContactVettingID,
			B.VettingDate,
			B.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD2.ContactID ORDER BY CV2.VettingDate DESC, CV2.ContactVettingID DESC) AS RowIndex,
				CD2.ContactID,
				CV2.ContactVettingID,
				CV2.VettingDate,
				CV2.VettingOutcomeID
			FROM CD CD2
				LEFT JOIN dbo.ContactVetting CV2 ON CV2.ContactID = CD2.ContactID
			WHERE CV2.ContactVettingTypeID = 2
			) B
			WHERE B.RowIndex = 1
		) UK
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = ISNULL(UK.VettingOutcomeID, 0)

	ORDER BY 1, 2, 3, 4

END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
--
-- Author:			Todd Pires
-- Update Date: 2016.09.01
-- Description:	Added the ConceptNoteTypeCode field
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
--
-- Author:			Brandon Green
-- Create date:	2017.07.19
-- Description:	Removed deprecated columns
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	--ConceptNote
	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeCode AS ConceptNoteComponentCode,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed,
		CN.AmendedConceptNoteID,
		ACN.Title AS AmendedConceptNoteTitle,
		ABCN.ConceptNoteID AS AmendedByConceptNoteID,
		ABCN.Title AS AmendedByConceptNoteTitle,
		CN.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
		LEFT JOIN dbo.ConceptNote ACN ON ACN.ConceptNoteID = CN.AmendedConceptNoteID
		LEFT JOIN dbo.ConceptNote ABCN ON ABCN.AmendedConceptNoteID = CN.ConceptNoteID
		LEFT JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
	WHERE CN.ConceptNoteID = @ConceptNoteID

	--ConceptNoteAmendment (deprecated)
	SELECT			
		CNA.ConceptNoteAmendmentID,
		CNA.AmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--ConceptNoteAsset
	SELECT 
		A.AssetID,
		A.AssetName
	FROM dbo.ConceptNoteAsset CNA
		JOIN asset.Asset A ON A.AssetID = CNA.AssetID
			AND CNA.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteAuthor
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--ConceptNoteBackgroundText
	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	--ConceptNoteBudget
	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Amendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	--ConceptNoteClass
	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	--ConceptNoteCommunity
	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.ConceptNoteCommunity CNC 
		JOIN dbo.Community C ON C.CommunityID = CNC.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	--ConceptNoteContact
	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= GETDATE() AND OACV12.VettingOutcomeCode != 'DoNotConsider'
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= GETDATE() AND OACV22.VettingOutcomeCode != 'DoNotConsider'
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID, VO1.VettingOutcomeCode
			FROM dbo.ContactVetting CV12
				JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID, VO2.VettingOutcomeCode
			FROM dbo.ContactVetting CV22
				JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1

	--ConceptNoteDocument
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName

	--ConceptNoteEquipmentCatalog
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID

	--ConceptNoteEthnicity
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName
			
	--ConceptNoteFinance
	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID

	--ConceptNoteIndicator
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteProject
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	--ConceptNoteProvince
	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID

	--ConceptNoteRisk
	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
			
	--ConceptNoteTask
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID
	
	--ConceptNoteUpdate
	SELECT
		CNU.ConceptNoteUpdateID,
		CNU.PersonID, 
		dbo.FormatPersonNameByPersonID(CNU.PersonID, 'LastFirst') AS FullNameFormatted,
		CNU.Remarks,
		CNU.UpdateDate,
		dbo.FormatDate(CNU.UpdateDate) AS UpdateDateFormatted,
		CNUT.ConceptNoteUpdateTypeID,
		CNUT.ConceptNoteUpdateTypeName,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		CNU.ActualTotalAmount
	FROM dbo.ConceptNoteUpdate CNU
		JOIN dropdown.ConceptNoteUpdateType CNUT ON CNUT.ConceptNoteUpdateTypeID = CNU.ConceptNoteUpdateTypeID
		LEFT JOIN dbo.Document D ON D.DocumentID = CNU.DocumentID
	WHERE CNU.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteVersion
	;
	WITH HD AS
		(
		SELECT
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			1 AS Depth
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			HD.Depth + 1 AS Depth
		FROM dbo.ConceptNote CN
			JOIN HD ON CN.ConceptNoteID = HD.AmendedConceptNoteID
		)

	SELECT 
		HD.ConceptNoteID,
		HD.Title,
		MAX(HD.Depth) OVER() - HD.Depth + 1 AS VersionNumber,
		(SELECT ISNULL(SUM(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost), 0)
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = HD.ConceptNoteID
		) + (SELECT ISNULL(SUM(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0)
			FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = HD.ConceptNoteID
		) AS TotalCost
	FROM HD
	ORDER BY VersionNumber
	
	--ConceptNoteWorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD

	--ConceptNoteWorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
			WHEN EL.EventCode = 'Cancel'
			THEN 'Canceled Concept Note'
			WHEN EL.EventCode = 'Hold'
			THEN 'Placed Concept Note on Hold'
			WHEN EL.EventCode = 'Unhold'
			THEN 'Reactivated Concept Note'			
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update','Cancel','Hold','Unhold')
	ORDER BY EL.CreateDateTime
	
	--ConceptNoteWorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetRequestForInformationByRequestForInformationID
EXEC Utility.DropObject 'dbo.GetRequestForInformationByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
--
-- Author:			Todd Pires
-- Create date:	2015.04.04
-- Description:	Added the SummaryAnswer field
--
-- Author:			Todd Pires
-- Create date:	2015.04.22
-- Description:	Added the DesiredResponseDate and SpotReportID fields
--
-- Author:			Todd Pires
-- Create date:	2015.11.21
-- Description:	Added the CoverLetterFrom and CoverLetterTo fields
-- ====================================================================================
CREATE PROCEDURE dbo.GetRequestForInformationByRequestForInformationID

@RequestForInformationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFI.Argument,
		RFI.AttachmentsList,
		RFI.Background,
		RFI.CompletedDate,
		dbo.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		RFI.CoverLetterFrom,
		RFI.CoverLetterTo,
		RFI.DesiredResponseDate,
		dbo.FormatDate(RFI.DesiredResponseDate) AS DesiredResponseDateFormatted,
		dbo.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.IncidentDate,
		dbo.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.InformationRequested,
		RFI.InProgressDate,
		RFI.Issue,
		RFI.KnownDetails,
		RFI.Location,
		RFI.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonFullname,
		RFI.Recommendation,
		RFI.RequestDate,
		dbo.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationID,
		RFI.RequestForInformationTitle,
		RFI.RequestPersonID,
		dbo.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonFullname,
		RFI.Resources,
		RFI.Risk,
		RFI.SpotReportID,
		dbo.FormatSpotReportReferenceCode(RFI.SpotReportID) AS SpotReportReferenceCode,
		RFI.SummaryAnswer,
		RFI.Timing,
		C2.CommunityID,
		C2.CommunityName,
		RFIRT.RequestForInformationResultTypeCode,
		RFIRT.RequestForInformationResultTypeID,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusCode,
		RFIS.RequestForInformationStatusID,
		RFIS.RequestForInformationStatusName,
		dbo.GetEntityTypeNameByEntityTypeCode('RequestForInformation') AS EntityTypeName
	FROM dbo.RequestForInformation RFI
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFI.RequestForInformationID = @RequestForInformationID
		OUTER APPLY
				(
				SELECT
					C1.CommunityID,
					C1.CommunityName
				FROM dbo.Community C1
				WHERE C1.CommunityID = RFI.CommunityID
				) C2

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentFileName,
		DE.DocumentEntityID
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'RequestForInformation'
			AND DE.EntityID = @RequestForInformationID

END
GO
--End procedure dbo.GetRequestForInformationByRequestForInformationID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:			Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
--
-- Author:			Todd Pires
-- Update date:	2016.08.30
-- Description:	Removed the SpotReport documents recordset
--
-- Author:			Brandon Green
-- Create date:	2017.07.17
-- Description:	Added SpotReportReferenceCode support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		SR.SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('SpotReport', @SpotReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('SpotReport', @SpotReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @SpotReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END 
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dbo.GetStationCommanderSurveyByStationCommanderSurveyID
EXEC Utility.DropObject 'dbo.GetStationCommanderSurveyByStationCommanderSurveyID'
GO
--End procedure dbo.GetStationCommanderSurveyByStationCommanderSurveyID

--Begin procedure dbo.GetVettingExpirationCounts
EXEC Utility.DropObject 'dbo.GetVettingExpirationCounts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.04.17
-- Description:	A procedure to get counts of expired and soon to expire vetting records
--
-- Author:			Todd Pires
-- Create date:	2017.07.08
-- Description:	Modified to exclude inactive contacts
-- ====================================================================================
CREATE PROCEDURE dbo.GetVettingExpirationCounts

AS
BEGIN
	SET NOCOUNT ON;

	SELECT D.*
	FROM
		(
		SELECT
			(
			SELECT COUNT(C.ContactID) 
			FROM dbo.Contact C 
			WHERE C.UKVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 14
				AND C.IsActive = 1
			) AS UK14,
			(
			SELECT COUNT(C.ContactID) 
			FROM dbo.Contact C 
			WHERE C.USVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 14
				AND C.IsActive = 1
			) AS US14
		) AS D

	SELECT D.*
	FROM
		(
		SELECT
			(
			SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] 
			FROM dbo.Contact C 
			WHERE C.UKVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.UKVettingExpirationDate) BETWEEN 0 AND 14 
				AND C.IsActive = 1
			ORDER BY C.ContactID 
			FOR XML PATH ('')), 1, 1, '')
			) AS UK14ExpiredContactIDList,
			(
			SELECT STUFF((SELECT ',' + CAST(C.ContactID AS VARCHAR(10)) AS [text()] 
			FROM dbo.Contact C 
			WHERE C.USVettingExpirationDate IS NOT NULL 
				AND DATEDIFF(d, getDate(), C.USVettingExpirationDate) BETWEEN 0 AND 14 
				AND C.IsActive = 1
			ORDER BY C.ContactID 
			FOR XML PATH ('')), 1, 1, '')
			) AS US14ExpiredContactIDList
		) AS D
		
END
GO
--End procedure dbo.GetVettingExpirationCounts

--Begin procedure document.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetDocumentsByEntityTypeCodeAndEntityID'
EXEC Utility.DropObject 'document.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.01.01
-- Description:	A stored procedure to get data from the document.Document table
--
-- Author:			Todd Pires
-- Create date:	2015.05.21
-- Description:	Modified to support weekly reports
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- ============================================================================
CREATE PROCEDURE document.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ContentSubtype,
		D.ContentType, 
		person.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.Extension,
		D.PhysicalFileSize,
		D.DocumentFileName,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID
	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure document.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure eventlog.LogLogicalFrameworkUpdateAction
EXEC utility.DropObject 'eventlog.LogLogicalFrameworkUpdateAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Jonathan Burnham
-- Create date: 2017.07.03
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLogicalFrameworkUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'LogicalFrameworkUpdate',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogLogicalFrameworkUpdateTable', 'u')) IS NOT NULL
			DROP TABLE #LogLogicalFrameworkUpdateTable
		--ENDIF
		
		SELECT *
		INTO #LogLogicalFrameworkUpdateTable
		FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU
		WHERE LFU.LogicalFrameworkUpdateID = @EntityID

		DECLARE @cLogicalFrameworkUpdateObjectives VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateObjectives = COALESCE(@cLogicalFrameworkUpdateObjectives, '') + D.LogicalFrameworkUpdateObjective 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateObjective'), ELEMENTS) AS LogicalFrameworkUpdateObjective
			FROM logicalframeworkupdate.Objective T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D

		DECLARE @cLogicalFrameworkUpdateIndicators VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateIndicators = COALESCE(@cLogicalFrameworkUpdateIndicators, '') + D.LogicalFrameworkUpdateIndicator 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateIndicator'), ELEMENTS) AS LogicalFrameworkUpdateIndicator
			FROM logicalframeworkupdate.Indicator T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D

		DECLARE @cLogicalFrameworkUpdateIndicatorMilestones VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateIndicatorMilestones = COALESCE(@cLogicalFrameworkUpdateIndicatorMilestones, '') + D.LogicalFrameworkUpdateIndicatorMilestone 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateIndicatorMilestone'), ELEMENTS) AS LogicalFrameworkUpdateIndicatorMilestone
			FROM logicalframeworkupdate.IndicatorMilestone T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D

		DECLARE @cLogicalFrameworkUpdateMilestones VARCHAR(MAX) 
	
		SELECT 
			@cLogicalFrameworkUpdateMilestones = COALESCE(@cLogicalFrameworkUpdateMilestones, '') + D.LogicalFrameworkUpdateMilestone 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('LogicalFrameworkUpdateMilestone'), ELEMENTS) AS LogicalFrameworkUpdateMilestone
			FROM logicalframeworkupdate.Milestone T 
			WHERE T.LogicalFrameworkUpdateID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'LogicalFrameworkUpdate',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<LogicalFrameworkUpdateObjectives>' + ISNULL(@cLogicalFrameworkUpdateObjectives, '') + '</LogicalFrameworkUpdateObjectives>') AS XML),
			CAST(('<LogicalFrameworkUpdateIndicators>' + ISNULL(@cLogicalFrameworkUpdateIndicators, '') + '</LogicalFrameworkUpdateIndicators>') AS XML),
			CAST(('<LogicalFrameworkUpdateIndicatorMilestones>' + ISNULL(@cLogicalFrameworkUpdateIndicatorMilestones, '') + '</LogicalFrameworkUpdateIndicatorMilestones>') AS XML),
			CAST(('<LogicalFrameworkUpdateMilestones>' + ISNULL(@cLogicalFrameworkUpdateMilestones, '') + '</LogicalFrameworkUpdateMilestones>') AS XML)
			FOR XML RAW('LogicalFrameworkUpdate'), ELEMENTS
			)
		FROM #LogLogicalFrameworkUpdateTable T
			JOIN logicalframeworkupdate.LogicalFrameworkUpdate LFU ON LFU.LogicalFrameworkUpdateID = T.LogicalFrameworkUpdateID

		DROP TABLE #LogLogicalFrameworkUpdateTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLogicalFrameworkUpdateAction

--Begin procedure logicalframework.GetIndicatorByIndicatorID
EXEC Utility.DropObject 'logicalframework.GetIndicatorByIndicatorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Indicator data
--
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	Add 'short' formats
--
-- Author:			Todd Pires
-- Create date:	2015.09.07
-- Description:	Removed the ActualValue column
--
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	Refactored to support new requirments
-- ========================================================
CREATE PROCEDURE logicalframework.GetIndicatorByIndicatorID

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.MeansOfVerification,
		I.PlannedDate,
		I.PositiveExample,
		dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.RiskAssumption,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

	--IndicatorMilestone
	SELECT
		IM.AchievedValue,
		IM.InProgressValue,
		IM.PlannedValue,
		IM.TargetValue,
		M.MilestoneID,
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
		M.MilestoneName
	FROM logicalframework.IndicatorMilestone IM
		JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
			AND IM.IndicatorID = @IndicatorID
	ORDER BY M.MilestoneName, M.MilestoneID

END
GO
--End procedure logicalframework.GetIndicatorByIndicatorID

--Begin procedure logicalframework.GetMilestoneByMilestoneID
EXEC Utility.DropObject 'logicalframework.GetMilestoneByMilestoneID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.05
-- Description:	A stored procedure to return Milestone data
--
-- Author:			Todd Pires
-- Create date:	2017.07.04
-- Description:	Add the IsActive bit
--
-- Author:			Todd Pires
-- Create date:	2017.07.16
-- Description:	Refactored to support new requirments
-- ========================================================
CREATE PROCEDURE logicalframework.GetMilestoneByMilestoneID

@MilestoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Milestone
	SELECT
		M.MilestoneID, 	
		M.MilestoneDate,
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
		M.MilestoneName,
		M.IsActive
	FROM logicalframework.Milestone M
	WHERE M.MilestoneID = @MilestoneID

END
GO
--End procedure logicalframework.GetMilestoneByMilestoneID

--Begin procedure logicalframeworkupdate.ApproveLogicalFrameworkUpdate
EXEC utility.DropObject 'logicalframeworkupdate.ApproveLogicalFrameworkUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to submit Objectives, Indicators, and Milestones in a logicalframework update for approval
-- ==========================================================================================================================
CREATE PROCEDURE logicalframeworkupdate.ApproveLogicalFrameworkUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @nLogicalFrameworkUpdateID INT
	DECLARE @tOutput1 TABLE (EntityTypeCode VARCHAR(50), EntityID INT)
	DECLARE @tOutput2 TABLE (LogicalFrameworkUpdateID INT)

	SELECT @nLogicalFrameworkUpdateID = LFU.LogicalFrameworkUpdateID
	FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU

	UPDATE O
	SET
		O.ParentObjectiveID = LFUO.ParentObjectiveID, 
		O.ObjectiveTypeID = LFUO.ObjectiveTypeID, 
		O.ObjectiveName = LFUO.ObjectiveName, 
		O.ObjectiveDescription = LFUO.ObjectiveDescription, 
		O.IsActive = LFUO.IsActive, 
		O.LogicalFrameworkStatusID = LFUO.LogicalFrameworkStatusID, 
		O.StatusUpdateDescription = LFUO.StatusUpdateDescription, 
		O.ComponentReportingAssociationID = LFUO.ComponentReportingAssociationID
	OUTPUT 'Objective', INSERTED.ObjectiveID INTO @tOutput1
	FROM logicalframework.Objective O
		JOIN logicalframeworkupdate.Objective LFUO ON LFUO.ObjectiveID = O.ObjectiveID
			AND LFUO.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	UPDATE I
	SET
		I.IndicatorTypeID = LFUI.IndicatorTypeID, 
		I.ObjectiveID = LFUI.ObjectiveID, 
		I.IndicatorName = LFUI.IndicatorName, 
		I.IndicatorDescription = LFUI.IndicatorDescription, 
		I.IndicatorSource = LFUI.IndicatorSource, 
		I.PositiveExample = LFUI.PositiveExample, 
		I.RiskAssumption = LFUI.RiskAssumption, 
		I.MeansOfVerification = LFUI.MeansOfVerification, 
		I.BaselineValue = LFUI.BaselineValue, 
		I.BaselineDate = LFUI.BaselineDate, 
		I.TargetValue = LFUI.TargetValue, 
		I.TargetDate = LFUI.TargetDate, 
		I.AchievedValue = LFUI.AchievedValue, 
		I.AchievedDate = LFUI.AchievedDate, 
		I.IsActive = LFUI.IsActive, 
		I.ActualDate = LFUI.ActualDate, 
		I.InProgressDate = LFUI.InProgressDate, 
		I.InProgressValue = LFUI.InProgressValue, 
		I.LogicalFrameworkStatusID = LFUI.LogicalFrameworkStatusID, 
		I.PlannedDate = LFUI.PlannedDate, 
		I.PlannedValue = LFUI.PlannedValue, 
		I.StatusUpdateDescription = LFUI.StatusUpdateDescription, 
		I.IndicatorNumber = LFUI.IndicatorNumber
	OUTPUT 'Indicator', INSERTED.IndicatorID INTO @tOutput1
	FROM logicalframework.Indicator I
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = I.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	DELETE T
	FROM logicalframework.IndicatorMilestone T
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = T.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	INSERT INTO logicalframework.IndicatorMilestone
		(IndicatorID, MilestoneID, AchievedValue, InProgressValue, PlannedValue, TargetValue)
	SELECT
		T.IndicatorID, 
		T.MilestoneID, 
		T.AchievedValue, 
		T.InProgressValue, 
		T.PlannedValue, 
		T.TargetValue
	FROM logicalframeworkupdate.IndicatorMilestone T
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = T.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	UPDATE M
	SET
		M.MilestoneName = LFUM.MilestoneName, 
		M.IsActive = LFUM.IsActive,  
		M.MilestoneDate = LFUM.MilestoneDate
	OUTPUT 'Milestone', INSERTED.MilestoneID INTO @tOutput1
	FROM logicalframework.Milestone M
		JOIN logicalframeworkupdate.Milestone LFUM ON LFUM.MilestoneID = M.MilestoneID
			AND LFUM.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O1.EntityTypeCode, O1.EntityID
		FROM @tOutput1 O1
		ORDER BY O1.EntityTypeCode, O1.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Objective'
			BEGIN
			
			EXEC eventlog.LogObjectiveAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogObjectiveAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Indicator'
			BEGIN
			
			EXEC eventlog.LogIndicatorAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogIndicatorAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Milestone'
			BEGIN
			
			EXEC eventlog.LogMilestoneAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogMilestoneAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogLogicalFrameworkUpdateAction @nLogicalFrameworkUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogLogicalFrameworkUpdateAction @nLogicalFrameworkUpdateID, 'update', @PersonID, NULL
	
	DELETE FROM logicalframeworkupdate.LogicalFrameworkUpdate

	TRUNCATE TABLE logicalframeworkupdate.Objective
	TRUNCATE TABLE logicalframeworkupdate.Indicator
	TRUNCATE TABLE logicalframeworkupdate.IndicatorMilestone
	TRUNCATE TABLE logicalframeworkupdate.Milestone

END
GO
--End procedure logicalframeworkupdate.ApproveLogicalFrameworkUpdate

--Begin procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator
EXEC utility.DropObject 'logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to remove an indicator from the logical framework update
-- ========================================================================================
CREATE PROCEDURE logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator

@IndicatorID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM logicalframeworkupdate.Indicator T
	WHERE T.IndicatorID = @IndicatorID

	DELETE T
	FROM logicalframeworkupdate.IndicatorMilestone T
	WHERE T.IndicatorID = @IndicatorID

END
GO
--End procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateIndicator

--Begin procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone
EXEC utility.DropObject 'logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to remove a milestone from the logical framework update
-- =======================================================================================
CREATE PROCEDURE logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone

@MilestoneID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM logicalframeworkupdate.Milestone T
	WHERE T.MilestoneID = @MilestoneID

END
GO
--End procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateMilestone

--Begin procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective
EXEC utility.DropObject 'logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to remove an objective from the logical framework update
-- ========================================================================================
CREATE PROCEDURE logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE T
	FROM logicalframeworkupdate.Objective T
	WHERE T.ObjectiveID = @ObjectiveID

END
GO
--End procedure logicalframeworkupdate.DeleteLogicalFrameworkUpdateObjective

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdate
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.02
-- Description:	A stored procedure to get data from the logicalframeworkupdate.LogicalFrameworkUpdate table
-- ================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nLogicalFrameworkUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU)
		BEGIN
		
		DECLARE @tOutput TABLE (LogicalFrameworkUpdateID INT)

		INSERT INTO logicalframeworkupdate.LogicalFrameworkUpdate 
			(WorkflowStepNumber,LogicalFrameworkUpdateDate) 
		OUTPUT INSERTED.LogicalFrameworkUpdateID INTO @tOutput
		VALUES 
			(1,getDate())

		SELECT @nLogicalFrameworkUpdateID = O.LogicalFrameworkUpdateID FROM @tOutput O
		
		EXEC eventlog.LogLogicalFrameworkUpdateAction @EntityID=@nLogicalFrameworkUpdateID, @EventCode='Create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='LogicalFrameworkUpdate', @EntityID=@nLogicalFrameworkUpdateID

		END
	ELSE
		SELECT @nLogicalFrameworkUpdateID = LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('LogicalFrameworkUpdate', @nLogicalFrameworkUpdateID)
	
	SELECT
		LFU.LogicalFrameworkUpdateID, 
		LFU.LogicalFrameworkUpdateDate,
		dbo.FormatDate(LFU.LogicalFrameworkUpdateDate) AS LogicalFrameworkUpdateDateFormatted,
		LFU.WorkflowStepNumber 
	FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('LogicalFrameworkUpdate', @nLogicalFrameworkUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('LogicalFrameworkUpdate', @nLogicalFrameworkUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Logical Framework Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Logical Framework Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Logical Framework Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Logical Framework Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'LogicalFrameworkUpdate'
		AND EL.EntityID = @nLogicalFrameworkUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdate

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to get logicalframework data for the objective update
-- ========================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator

@IndicatorID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.AchievedDate,
		dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
		LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
		I.AchievedValue, 	
		I.ActualDate,
		dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
		LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
		I.BaselineDate, 	
		dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
		LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
		I.BaselineValue, 	
		I.IndicatorDescription,	
		I.IndicatorID, 	
		I.IndicatorName, 	
		I.IndicatorNumber,
		I.IndicatorSource, 	
		I.PositiveExample, 
		I.RiskAssumption, 
		I.MeansOfVerification, 
		I.InProgressDate,
		dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
		LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
		I.InProgressValue,
		I.IsActive,
		I.PlannedDate,
		dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
		LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
		I.PlannedValue,
		I.StatusUpdateDescription,
		I.TargetDate, 	
		dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
		LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
		I.TargetValue, 	
		IT.IndicatorTypeID, 	
		IT.IndicatorTypeName, 
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O.ObjectiveID, 	
		O.ObjectiveName, 	
		dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
			AND I.IndicatorID = @IndicatorID

	SELECT
		IM.AchievedValue, 
		IM.IndicatorID, 
		IM.InProgressValue, 
		IM.PlannedValue,
		IM.TargetValue,
		M.MilestoneID, 
		M.MilestoneName, 
		M.IsActive, 
		M.MilestoneDate, 
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
	FROM logicalframework.IndicatorMilestone IM
		JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
	WHERE IM.IndicatorID = @IndicatorID

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Indicator I WHERE I.IndicatorID =  @IndicatorID)
		BEGIN

		SELECT
			I.AchievedDate,
			dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
			LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
			I.AchievedValue, 	
			I.ActualDate,
			dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
			LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
			I.BaselineDate, 	
			dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
			LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
			I.BaselineValue, 	
			I.IndicatorDescription,	
			I.IndicatorID, 	
			I.IndicatorName, 	
			I.IndicatorNumber,
			I.IndicatorSource, 	
			I.PositiveExample, 
			I.RiskAssumption, 
			I.MeansOfVerification, 	
			I.InProgressDate,
			dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
			LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
			I.InProgressValue,
			I.IsActive,
			I.PlannedDate,
			dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
			LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
			I.PlannedValue,
			I.StatusUpdateDescription,
			I.TargetDate, 	
			dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
			LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
			I.TargetValue, 	
			IT.IndicatorTypeID, 	
			IT.IndicatorTypeName, 
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O.ObjectiveID, 	
			O.ObjectiveName, 	
			dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
		FROM logicalframeworkupdate.Indicator I
			JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
			JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
				AND I.IndicatorID = @IndicatorID

		END
	ELSE
		BEGIN

		SELECT
			I.AchievedDate,
			dbo.FormatDate(I.AchievedDate) AS AchievedDateFormatted,
			LEFT(DATENAME(month, I.AchievedDate), 3) + '-' + RIGHT(CAST(YEAR(I.AchievedDate) AS CHAR(4)), 2) AS AchievedDateFormattedShort,
			I.AchievedValue, 	
			I.ActualDate,
			dbo.FormatDate(I.ActualDate) AS ActualDateFormatted,
			LEFT(DATENAME(month, I.ActualDate), 3) + '-' + RIGHT(CAST(YEAR(I.ActualDate) AS CHAR(4)), 2) AS ActualDateFormattedShort,
			I.BaselineDate, 	
			dbo.FormatDate(I.BaselineDate) AS BaselineDateFormatted,
			LEFT(DATENAME(month, I.BaselineDate), 3) + '-' + RIGHT(CAST(YEAR(I.BaselineDate) AS CHAR(4)), 2) AS BaselineDateFormattedShort,
			I.BaselineValue, 	
			I.IndicatorDescription,	
			I.IndicatorID, 	
			I.IndicatorName, 	
			I.IndicatorNumber,
			I.IndicatorSource, 	
			I.PositiveExample, 
			I.RiskAssumption, 
			I.MeansOfVerification, 	
			I.InProgressDate,
			dbo.FormatDate(I.InProgressDate) AS InProgressDateFormatted,
			LEFT(DATENAME(month, I.InProgressDate), 3) + '-' + RIGHT(CAST(YEAR(I.InProgressDate) AS CHAR(4)), 2) AS InProgressDateFormattedShort,
			I.InProgressValue,
			I.IsActive,
			I.PlannedDate,
			dbo.FormatDate(I.PlannedDate) AS PlannedDateFormatted,
			LEFT(DATENAME(month, I.PlannedDate), 3) + '-' + RIGHT(CAST(YEAR(I.PlannedDate) AS CHAR(4)), 2) AS PlannedDateFormattedShort,
			I.PlannedValue,
			I.StatusUpdateDescription,
			I.TargetDate, 	
			dbo.FormatDate(I.TargetDate) AS TargetDateFormatted,
			LEFT(DATENAME(month, I.TargetDate), 3) + '-' + RIGHT(CAST(YEAR(I.TargetDate) AS CHAR(4)), 2) AS TargetDateFormattedShort,
			I.TargetValue, 	
			IT.IndicatorTypeID, 	
			IT.IndicatorTypeName, 
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O.ObjectiveID, 	
			O.ObjectiveName, 	
			dbo.GetEntityTypeNameByEntityTypeCode('Indicator') AS EntityTypeName
		FROM logicalframework.Indicator I
			JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = I.LogicalFrameworkStatusID
			JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
				AND I.IndicatorID = @IndicatorID

		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Indicator I WHERE I.IndicatorID =  @IndicatorID)
		BEGIN

		SELECT
			IM.AchievedValue, 
			IM.IndicatorID, 
			IM.InProgressValue, 
			IM.PlannedValue,
			IM.TargetValue,
			M.MilestoneID, 
			M.MilestoneName, 
			M.IsActive,  
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
		FROM logicalframeworkupdate.IndicatorMilestone IM
			JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
				AND IM.IndicatorID = @IndicatorID

		END
	ELSE
		BEGIN

		SELECT
			IM.AchievedValue, 
			IM.IndicatorID, 
			IM.InProgressValue, 
			IM.PlannedValue,
			IM.TargetValue,
			M.MilestoneID, 
			M.MilestoneName, 
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted,
			LEFT(DATENAME(month, M.MilestoneDate), 3) + '-' + RIGHT(CAST(YEAR(M.MilestoneDate) AS CHAR(4)), 2) AS MilestoneDateFormattedShort
		FROM logicalframework.IndicatorMilestone IM
			JOIN logicalframework.Milestone M ON M.MilestoneID = IM.MilestoneID
				AND IM.IndicatorID = @IndicatorID

		END
	--ENDIF

END
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdateIndicator

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to get logicalframework data for the objective update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone

@MilestoneID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		M.MilestoneID, 
		M.MilestoneName, 
		M.IsActive,  
		M.MilestoneDate, 
		dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
	FROM logicalframework.Milestone M
	WHERE M.MilestoneID = @MilestoneID

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Milestone M WHERE M.MilestoneID =  @MilestoneID)
		BEGIN

		SELECT
			M.MilestoneID, 
			M.MilestoneName, 
			M.IsActive,  
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
		FROM logicalframeworkupdate.Milestone M
		WHERE M.MilestoneID = @MilestoneID

		END
	ELSE
		BEGIN

		SELECT
			M.MilestoneID, 
			M.MilestoneName, 
			M.IsActive,  
			M.MilestoneDate, 
			dbo.FormatDate(M.MilestoneDate) AS MilestoneDateFormatted
		FROM logicalframework.Milestone M
		WHERE M.MilestoneID = @MilestoneID

		END
	--ENDIF

END
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdateMilestone

--Begin procedure logicalframeworkupdate.GetLogicalFrameworkUpdateObjective
EXEC utility.DropObject 'logicalframeworkupdate.GetLogicalFrameworkUpdateObjective'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to get logicalframework data for the objective update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.GetLogicalFrameworkUpdateObjective

@ObjectiveID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CRA.ComponentReportingAssociationCode,
		CRA.ComponentReportingAssociationID,
		CRA.ComponentReportingAssociationName,
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O1.IsActive,
		O1.ObjectiveDescription,
		O1.ObjectiveID, 
		O1.ObjectiveName, 
		O1.ParentObjectiveID, 

		CASE
			WHEN O1.ParentObjectiveID = 0
			THEN NULL
			ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
		END AS ParentObjectiveName,
		
		O1.StatusUpdateDescription,
		OT.ObjectiveTypeID, 
		OT.ObjectiveTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
	FROM logicalframework.Objective O1
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O1.ComponentReportingAssociationID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
			AND O1.ObjectiveID = @ObjectiveID

	IF EXISTS (SELECT 1 FROM logicalframeworkupdate.Objective O WHERE O.ObjectiveID =  @ObjectiveID)
		BEGIN

		SELECT
			CRA.ComponentReportingAssociationCode,
			CRA.ComponentReportingAssociationID,
			CRA.ComponentReportingAssociationName,
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O1.IsActive,
			O1.ObjectiveDescription,
			O1.ObjectiveID, 
			O1.ObjectiveName, 
			O1.ParentObjectiveID, 

			CASE
				WHEN O1.ParentObjectiveID = 0
				THEN NULL
				ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
			END AS ParentObjectiveName,
			
			O1.StatusUpdateDescription,
			OT.ObjectiveTypeID, 
			OT.ObjectiveTypeName,
			dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
		FROM logicalframeworkupdate.Objective O1
			JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O1.ComponentReportingAssociationID
			JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
				AND O1.ObjectiveID = @ObjectiveID

		END
	ELSE
		BEGIN

		SELECT
			CRA.ComponentReportingAssociationCode,
			CRA.ComponentReportingAssociationID,
			CRA.ComponentReportingAssociationName,
			LFS.LogicalFrameworkStatusID,
			LFS.LogicalFrameworkStatusName,
			O1.IsActive,
			O1.ObjectiveDescription,
			O1.ObjectiveID, 
			O1.ObjectiveName, 
			O1.ParentObjectiveID, 

			CASE
				WHEN O1.ParentObjectiveID = 0
				THEN NULL
				ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
			END AS ParentObjectiveName,
			
			O1.StatusUpdateDescription,
			OT.ObjectiveTypeID, 
			OT.ObjectiveTypeName,
			dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
		FROM logicalframework.Objective O1
			JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O1.ComponentReportingAssociationID
			JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
			JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
				AND O1.ObjectiveID = @ObjectiveID

		END
	--ENDIF

END
GO
--End procedure logicalframeworkupdate.GetLogicalFrameworkUpdateObjective

--Begin procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators
EXEC utility.DropObject 'logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.16
-- Description:	A stored procedure to add indicator data to the logical framework update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators

@IndicatorIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO logicalframeworkupdate.Indicator
		(IndicatorID, IndicatorTypeID, ObjectiveID, IndicatorName, IndicatorDescription, IndicatorSource, BaselineValue, BaselineDate, TargetValue, TargetDate, AchievedValue, AchievedDate, IsActive, ActualDate, InProgressDate, InProgressValue, LogicalFrameworkStatusID, PlannedDate, PlannedValue, StatusUpdateDescription, IndicatorNumber, LogicalFrameworkUpdateID)
	SELECT 
		I.IndicatorID, 
		I.IndicatorTypeID, 
		I.ObjectiveID, 
		I.IndicatorName, 
		I.IndicatorDescription, 
		I.IndicatorSource, 
		I.BaselineValue, 
		I.BaselineDate, 
		I.TargetValue, 
		I.TargetDate, 
		I.AchievedValue, 
		I.AchievedDate, 
		I.IsActive, 
		I.ActualDate, 
		I.InProgressDate, 
		I.InProgressValue, 
		I.LogicalFrameworkStatusID, 
		I.PlannedDate, 
		I.PlannedValue, 
		I.StatusUpdateDescription, 
		I.IndicatorNumber, 
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.Indicator I
		JOIN dbo.ListToTable(@IndicatorIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = I.IndicatorID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.Indicator LFUI
				WHERE LFUI.IndicatorID = I.IndicatorID
				)

	INSERT INTO logicalframeworkupdate.IndicatorMilestone
		(IndicatorID, MilestoneID, AchievedValue, InProgressValue, PlannedValue, TargetValue, LogicalFrameworkUpdateID)
	SELECT
		IM.IndicatorID, 
		IM.MilestoneID, 
		IM.AchievedValue, 
		IM.InProgressValue, 
		IM.PlannedValue, 
		IM.TargetValue, 
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.IndicatorMilestone IM
		JOIN dbo.ListToTable(@IndicatorIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = IM.IndicatorID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.IndicatorMilestone LFUIM
				WHERE LFUIM.IndicatorID = IM.IndicatorID
				)
END
GO
--End procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateIndicators

--Begin procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones
EXEC utility.DropObject 'logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.16
-- Description:	A stored procedure to add Milestone data to the logical framework update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones

@MilestoneIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO logicalframeworkupdate.Milestone
		(MilestoneID, MilestoneName, IsActive, MilestoneDate, LogicalFrameworkUpdateID)
	SELECT
		M.MilestoneID, 
		M.MilestoneName,
		M.IsActive,  
		M.MilestoneDate,
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.Milestone M
		JOIN dbo.ListToTable(@MilestoneIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = M.MilestoneID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.Milestone LFUM
				WHERE LFUM.MilestoneID = M.MilestoneID
				)
END
GO
--End procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateMilestones

--Begin procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives
EXEC utility.DropObject 'logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.16
-- Description:	A stored procedure to add objective data to the logical framework update
-- =====================================================================================
CREATE PROCEDURE logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives

@ObjectiveIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO logicalframeworkupdate.Objective
		(ObjectiveID, ParentObjectiveID, ObjectiveTypeID, ObjectiveName, ObjectiveDescription, IsActive, LogicalFrameworkStatusID, StatusUpdateDescription, ComponentReportingAssociationID, LogicalFrameworkUpdateID)
	SELECT
		O.ObjectiveID, 
		O.ParentObjectiveID, 
		O.ObjectiveTypeID, 
		O.ObjectiveName, 
		O.ObjectiveDescription, 
		O.IsActive, 
		O.LogicalFrameworkStatusID, 
		O.StatusUpdateDescription, 
		O.ComponentReportingAssociationID, 
		(SELECT TOP 1 LFU.LogicalFrameworkUpdateID FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU ORDER BY LFU.LogicalFrameworkUpdateID DESC)
	FROM logicalframework.Objective O
		JOIN dbo.ListToTable(@ObjectiveIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = O.ObjectiveID
			AND NOT EXISTS
				(
				SELECT 1
				FROM logicalframeworkupdate.Objective LFUO
				WHERE LFUO.ObjectiveID = O.ObjectiveID
				)
END
GO
--End procedure logicalframeworkupdate.PopulateLogicalFrameworkUpdateObjectives

--Begin procedure reporting.GetActivityAnalysis
EXEC Utility.DropObject 'reporting.GetActivityAnalysis'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Kevin Ross
-- Create date: 2017.07.20
-- Description:	A stored procedure to get GetActivityAnalysis data
-- ===============================================================
CREATE PROCEDURE reporting.GetActivityAnalysis

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT		
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS Title,
		workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) AS Status,
		CNS.ConceptNoteStatusCode,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeName,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O.ObjectiveName,
		CNI.TargetQuantity,
		CNI.ActualNumber
	FROM logicalframework.Indicator I
		JOIN Reporting.SearchResult SR ON SR.EntityID = I.IndicatorID AND SR.EntityTypeCode='ActivityAnalysis' AND SR.PersonID = @PersonID
		JOIN dbo.ConceptNoteIndicator CNI ON CNI.IndicatorID = SR.EntityID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNI.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode NOT IN ('Amended','Cancelled','OnHold')
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
	ORDER BY 1

END
GO
--End procedure reporting.GetActivityAnalysis

--Begin procedure reporting.GetIndicatorAnalysis
EXEC Utility.DropObject 'reporting.GetIndicatorAnalysis'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Kevin Ross
-- Create date: 2017.07.20
-- Description:	A stored procedure to get GetIndicatorAnalysis data
-- ================================================================
CREATE PROCEDURE reporting.GetIndicatorAnalysis

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT		
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS Title,
		CN.ConceptNoteID,		
		dbo.GetConceptNoteVersionNumberByConceptNoteID(CN.ConceptNoteID) AS VersionNumber,
		workflow.GetConceptNoteWorkflowStatus(CN.ConceptNoteID) AS Status,
		CNS.ConceptNoteStatusCode,
		CN.ConceptNoteTypeCode, 
		CN.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		I.IndicatorName,
		CNI.TargetQuantity,
		CNI.ActualNumber
	FROM dbo.ConceptNote CN
		JOIN Reporting.SearchResult SR ON SR.EntityID = CN.ConceptNoteID AND SR.EntityTypeCode='IndicatorAnalysis' AND SR.PersonID = @PersonID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode NOT IN ('Amended','Cancelled','OnHold')
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dbo.ConceptNoteIndicator CNI ON CNI.ConceptNoteID = SR.EntityID
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
			AND I.IsActive = 1
	ORDER BY 1

END
GO
--End procedure reporting.GetIndicatorAnalysis

--Begin procedure reporting.GetProgramReportDocumentsByProgramReportID
EXEC Utility.DropObject 'reporting.GetProgramReportDocumentsByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date:	2015.04.24
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- =================================================================================
CREATE PROCEDURE reporting.GetProgramReportDocumentsByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

		SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.Documentname,
		DE.DocumentEntityID,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/document/getDocumentByDocumentName/DocumentName/' + D.DocumentFileName AS DocumentURL
	FROM document.Document D
		JOIN document.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'ProgramReport'
			AND DE.EntityID = @ProgramReportID
	ORDER BY D.DocumentDescription		

END
GO
--End procedure reporting.GetProgramReportDocumentsByProgramReportID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

DELETE ET
FROM dbo.EntityType ET 
WHERE ET.EntityTypeCode = 'LogicalFrameworkUpdate'
GO 

INSERT INTO dbo.EntityType 
	(EntityTypeCode, EntityTypeName, CanRejectAfterFinalApproval, HasWorkflow, HasMenuItemAccessViaWorkflow, IsActive, IsForNewsLetter)
VALUES 
	('LogicalFrameworkUpdate', 'Logical Framework Update', 0, 1, 0, 1, 0)
GO

--Begin table dropdown.LogicalFrameworkStatus
IF NOT EXISTS (SELECT 1 FROM dropdown.LogicalFrameworkStatus LFS WHERE LFS.LogicalFrameworkStatusName = 'Progressing')
	INSERT INTO dropdown.LogicalFrameworkStatus (LogicalFrameworkStatusName) VALUES ('Progressing')
--ENDIF

UPDATE LFS
SET
	LFS.DisplayOrder = 
		CASE
			WHEN LFS.LogicalFrameworkStatusName = 'Cancelled'
			THEN 7
			WHEN LFS.LogicalFrameworkStatusName = 'Closed'
			THEN 6
			WHEN LFS.LogicalFrameworkStatusName = 'Completed'
			THEN 5
			WHEN LFS.LogicalFrameworkStatusName = 'Not Started'
			THEN 1
			WHEN LFS.LogicalFrameworkStatusName = 'Off Track'
			THEN 2
			WHEN LFS.LogicalFrameworkStatusName = 'On Track'
			THEN 4
			WHEN LFS.LogicalFrameworkStatusName = 'Progressing'
			THEN 3
			ELSE 0
		END,

	LFS.HexColor = 
		CASE
			WHEN LFS.LogicalFrameworkStatusName = 'Completed'
			THEN '78cd51'
			WHEN LFS.LogicalFrameworkStatusName = 'Off Track'
			THEN 'ff5454'
			WHEN LFS.LogicalFrameworkStatusName = 'On Track'
			THEN '78cd51'
			WHEN LFS.LogicalFrameworkStatusName = 'Progressing'
			THEN 'fabb3d'
			ELSE 'ffffff'
		END
FROM dropdown.LogicalFrameworkStatus LFS
GO
--End table dropdown.LogicalFrameworkStatus

INSERT INTO logicalframework.IndicatorMilestone
	(IndicatorID, MilestoneID, AchievedValue, InProgressValue, PlannedValue, TargetValue)
SELECT
	M.IndicatorIDDeprecated,
	M.MilestoneID,
	M.AchievedValueDeprecated,
	M.InProgressValueDeprecated,
	M.PlannedValueDeprecated,
	M.TargetValueDeprecated
FROM logicalframework.Milestone M
GO

UPDATE M
SET M.MilestoneDate = ISNULL(M.TargetDateDeprecated, getDate())
FROM logicalframework.Milestone M
GO

--Update menu item
UPDATE MI
SET 
	MI.MenuItemCode = 'LogicalFrameworkUpdate', 
	MI.MenuItemLink = '/logicalframework/logicalframeworkupdate'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'ObjectiveManage'
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='ConceptNoteAnalysis', 
	@NewMenuItemLink='/conceptnote/analysis', 
	@NewMenuItemText='Activity Analysis', 
	@AfterMenuItemCode='ObjectiveManage'
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='IndicatorAnalysis', 
	@NewMenuItemLink='/indicator/analysis', 
	@NewMenuItemText='Indicator Analysis', 
	@AfterMenuItemCode='ActivityAnalysisList'
GO

DECLARE @cSystemName VARCHAR(50)
	
SELECT @cSystemName = SS.ServerSetupValue
FROM dbo.ServerSetup SS
WHERE SS.ServerSetupKey = 'SystemName'

UPDATE SR
SET SR.SpotReportReferenceCode = @cSystemName + '-SR-' + RIGHT('0000' + CAST(SR.SpotReportID AS VARCHAR(10)), 4)
FROM dbo.SpotReport SR
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'LogicalFrameworkUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplate 
		(EntityTypeCode, WorkflowActionCode, EmailText)
	VALUES 
		(
		'LogicalFrameworkUpdate', 'IncrementWorkflow', 
		'<p>An AJACS logical framework update has been submitted for your review:</p>
		<p><strong>Logical Framework Update: </strong>[[Link]]</p>
		<p><strong>Comments:</strong><br />[[Comments]]</p>
		<p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the logical framework update workflow. Please click the link above to review the updated objectives, indicators, and milestones.</p>
		<p>Please do not reply to this email as it is generated automatically by the AJACS system.</p>
		<p>Thank you,<br /><br />The AJACS Team</p>'
		),
		(
		'LogicalFrameworkUpdate', 'DecrementWorkflow', 
		'<p>A previously submitted logical framework update has been disapproved:</p>
		<p><strong>Logical Framework Update: </strong>[[Link]]</p>
		<p><strong>Comments:</strong><br />[[Comments]]</p>
		<p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the logical framework update workflow. Please click the link above to review the updated objectives, indicators, and milestones.</p>
		<p>Please do not reply to this email as it is generated automatically by the AJACS system.</p>
		<p>Thank you,<br /><br />The AJACS Team</p>'
		),
		(
		'LogicalFrameworkUpdate', 'Release', 
		'<p>An AJACS logical framework update has been released:</p>
		<p><strong>Comments:</strong><br />[[Comments]]</p>
		<p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the logical framework update workflow. All updated objectives, indicators, and milestones have been released.</p>
		<p>Please do not reply to this email as it is generated automatically by the AJACS system.</p>
		<p>Thank you,<br /><br />The AJACS Team</p>'
		)

	INSERT INTO dbo.EmailTemplateField 
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription)
	VALUES 
		('LogicalFrameworkUpdate', '[[Link]]', 'Link'),
		('LogicalFrameworkUpdate', '[[Comments]]', 'Comments')

	END
--ENDIF
GO
--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Community', 'Community', 0;
EXEC permissionable.SavePermissionableGroup 'Contact', 'Contact', 0;
EXEC permissionable.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0;
EXEC permissionable.SavePermissionableGroup 'Documents', 'Documents', 0;
EXEC permissionable.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0;
EXEC permissionable.SavePermissionableGroup 'Equipment', 'Equipment', 0;
EXEC permissionable.SavePermissionableGroup 'Implementation', 'Implementation', 0;
EXEC permissionable.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0;
EXEC permissionable.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0;
EXEC permissionable.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0;
EXEC permissionable.SavePermissionableGroup 'Province', 'Province', 0;
EXEC permissionable.SavePermissionableGroup 'Research', 'Research', 0;
EXEC permissionable.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0;
EXEC permissionable.SavePermissionableGroup 'Training', 'Training', 0;
EXEC permissionable.SavePermissionableGroup 'Workflows', 'Workflows', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='Add / edit an announcement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Announcement', @DESCRIPTION='View the list of announcements', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Announcement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='List EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='View EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='List EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Exports', @DESCRIPTION='Business License Report Exports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='BusinessLicenseReport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Exports.BusinessLicenseReport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User Can Receive Email', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CanRecieveEmail', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.CanRecieveEmail.CanRecieveEmail', @PERMISSIONCODE='CanRecieveEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Download dashboard charts as images', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Default.DownloadChart', @PERMISSIONCODE='DownloadChart';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View ColdFusion Errors SiteConfiguration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='List PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='List Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View list of permissionables on view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Add / edit a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Recieve email for Update to Impact Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate.ImpactUpdateEmail', @PERMISSIONCODE='ImpactUpdateEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='List Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the analysis tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Analysis', @PERMISSIONCODE='Analysis';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Equipment Distributions Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Implementation Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Implementation', @PERMISSIONCODE='Implementation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the information tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Information', @PERMISSIONCODE='Information';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add contacts to a stipend payment list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddContactStipendPaymentContacts', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddContactStipendPaymentContacts', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Project Staff - ASI', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Project Staff - Creative', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Sub-Contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows view of justice stipends payments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the approve stipend payment functionality justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.ApproveContactStipendPayment', @PERMISSIONCODE='ApproveContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the cash hand over report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the op funds report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the reconcile stipend payment functionality justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.ReconcileContactStipendPayment', @PERMISSIONCODE='ReconcileContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend activity report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Close out the stipend justice & police payment process', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows import of payees in payment system', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.AddContactStipendPaymentContacts', @PERMISSIONCODE='AddContactStipendPaymentContacts';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows access to the bulk transfer functionality', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.CanHaveBulkTransfer', @PERMISSIONCODE='CanHaveBulkTransfer';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type CE Team in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export payees from the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ExportPayees', @PERMISSIONCODE='ExportPayees';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Field Staff in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type IO4 in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='A bypass to allow users not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Project Staff - ASI in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Project Staff - Creative in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Sub-Contractor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the approve stipend payment functionality police stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.ApproveContactStipendPayment', @PERMISSIONCODE='ApproveContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the cash handover report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the op funds report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the reconcile stipend payment functionality police stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.ReconcileContactStipendPayment', @PERMISSIONCODE='ReconcileContactStipendPayment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend activity report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend payment report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='contact', @DESCRIPTION='contact.reconcilecontactstipendpayment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='reconcilecontactstipendpayment', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='contact.reconcilecontactstipendpayment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Project Staff - ASI', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Project Staff - Creative', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Sub-Contractors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the more info button on the vetting history data table', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.VettingMoreInfo', @PERMISSIONCODE='VettingMoreInfo';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Add contacts to an activity from the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.AddToConceptNote', @PERMISSIONCODE='AddToConceptNote';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type CE Team in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type JO.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportJO', @PERMISSIONCODE='ExportJO';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type UK.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportUK', @PERMISSIONCODE='ExportUK';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type US.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportUS', @PERMISSIONCODE='ExportUS';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Field Staff in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type IO4 in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Project Staff - ASI in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Project Staff - Creative in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Sub-Contractor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive an email when a UK vetting outcome has changed for one or more contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.UKVettingOutcomeChangeNotification', @PERMISSIONCODE='UKVettingOutcomeChangeNotification';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive an email when a US vetting outcome has changed for one or more contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.USVettingOutcomeChangeNotification', @PERMISSIONCODE='USVettingOutcomeChangeNotification';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeConsider', @PERMISSIONCODE='VettingOutcomeConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Do Not Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeDoNotConsider', @PERMISSIONCODE='VettingOutcomeDoNotConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Insufficient Data"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeInsufficientData', @PERMISSIONCODE='VettingOutcomeInsufficientData';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Not Vetted"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeNotVetted', @PERMISSIONCODE='VettingOutcomeNotVetted';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Pending Internal Review"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomePendingInternalReview', @PERMISSIONCODE='VettingOutcomePendingInternalReview';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Allows a user to change a change a vetting outcome after a contact has been flagged as "Do Not Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeReConsider', @PERMISSIONCODE='VettingOutcomeReConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Submitted for Vetting"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeSubmittedforVetting', @PERMISSIONCODE='VettingOutcomeSubmittedforVetting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update UK vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUK', @PERMISSIONCODE='VettingTypeUK';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update US vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUS', @PERMISSIONCODE='VettingTypeUS';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive the monthly vetting expiration counts e-mail', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Notification', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.Notification.ExpirationCountEmail', @PERMISSIONCODE='ExpirationCountEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.004', @PERMISSIONCODE='004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.100', @PERMISSIONCODE='100';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.101', @PERMISSIONCODE='101';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.102', @PERMISSIONCODE='102';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.103', @PERMISSIONCODE='103';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.104', @PERMISSIONCODE='104';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.105', @PERMISSIONCODE='105';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.106', @PERMISSIONCODE='106';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.107', @PERMISSIONCODE='107';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.108', @PERMISSIONCODE='108';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.109', @PERMISSIONCODE='109';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.301', @PERMISSIONCODE='301';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.302', @PERMISSIONCODE='302';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.303', @PERMISSIONCODE='303';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.304', @PERMISSIONCODE='304';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.306', @PERMISSIONCODE='306';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.500', @PERMISSIONCODE='500';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.501', @PERMISSIONCODE='501';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.502', @PERMISSIONCODE='502';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.503', @PERMISSIONCODE='503';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.504', @PERMISSIONCODE='504';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.505', @PERMISSIONCODE='505';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.506', @PERMISSIONCODE='506';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.507', @PERMISSIONCODE='507';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.508', @PERMISSIONCODE='508';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.509', @PERMISSIONCODE='509';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.510', @PERMISSIONCODE='510';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.511', @PERMISSIONCODE='511';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.512', @PERMISSIONCODE='512';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.513', @PERMISSIONCODE='513';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.514', @PERMISSIONCODE='514';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.515', @PERMISSIONCODE='515';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.516', @PERMISSIONCODE='516';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.517', @PERMISSIONCODE='517';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.518', @PERMISSIONCODE='518';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.519', @PERMISSIONCODE='519';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.520', @PERMISSIONCODE='520';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.521', @PERMISSIONCODE='521';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 522 Archived Full Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.522', @PERMISSIONCODE='522';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.600', @PERMISSIONCODE='600';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.601', @PERMISSIONCODE='601';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.602', @PERMISSIONCODE='602';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.603', @PERMISSIONCODE='603';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.604', @PERMISSIONCODE='604';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.605', @PERMISSIONCODE='605';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.700', @PERMISSIONCODE='700';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.701', @PERMISSIONCODE='701';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.702', @PERMISSIONCODE='702';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.801', @PERMISSIONCODE='801';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.802', @PERMISSIONCODE='802';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.803', @PERMISSIONCODE='803';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.804', @PERMISSIONCODE='804';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.805', @PERMISSIONCODE='805';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.806', @PERMISSIONCODE='806';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.807', @PERMISSIONCODE='807';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.808', @PERMISSIONCODE='808';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.900', @PERMISSIONCODE='900';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.901', @PERMISSIONCODE='901';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.902', @PERMISSIONCODE='902';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.903', @PERMISSIONCODE='903';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.904', @PERMISSIONCODE='904';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.905', @PERMISSIONCODE='905';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.906', @PERMISSIONCODE='906';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.907', @PERMISSIONCODE='907';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.908', @PERMISSIONCODE='908';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.909', @PERMISSIONCODE='909';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.910', @PERMISSIONCODE='910';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc006', @PERMISSIONCODE='Doc006';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='document', @DESCRIPTION='getdocumentbydocumentname', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='getdocumentbydocumentname', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='document.getdocumentbydocumentname', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='get doc file by ID', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='getDocumentFileByDocumentID', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.getDocumentFileByDocumentID', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.004', @PERMISSIONCODE='004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.100', @PERMISSIONCODE='100';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.101', @PERMISSIONCODE='101';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.102', @PERMISSIONCODE='102';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.103', @PERMISSIONCODE='103';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.104', @PERMISSIONCODE='104';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.105', @PERMISSIONCODE='105';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.106', @PERMISSIONCODE='106';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.107', @PERMISSIONCODE='107';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.108', @PERMISSIONCODE='108';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.109', @PERMISSIONCODE='109';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.301', @PERMISSIONCODE='301';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.302', @PERMISSIONCODE='302';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.303', @PERMISSIONCODE='303';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.304', @PERMISSIONCODE='304';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.306', @PERMISSIONCODE='306';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.500', @PERMISSIONCODE='500';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.501', @PERMISSIONCODE='501';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.502', @PERMISSIONCODE='502';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.503', @PERMISSIONCODE='503';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.504', @PERMISSIONCODE='504';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.505', @PERMISSIONCODE='505';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.506', @PERMISSIONCODE='506';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.507', @PERMISSIONCODE='507';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.508', @PERMISSIONCODE='508';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.509', @PERMISSIONCODE='509';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.510', @PERMISSIONCODE='510';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.511', @PERMISSIONCODE='511';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.512', @PERMISSIONCODE='512';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.513', @PERMISSIONCODE='513';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.514', @PERMISSIONCODE='514';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.515', @PERMISSIONCODE='515';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.516', @PERMISSIONCODE='516';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.517', @PERMISSIONCODE='517';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.518', @PERMISSIONCODE='518';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.519', @PERMISSIONCODE='519';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.520', @PERMISSIONCODE='520';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.521', @PERMISSIONCODE='521';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 522 Archived Full Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.522', @PERMISSIONCODE='522';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.600', @PERMISSIONCODE='600';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.601', @PERMISSIONCODE='601';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.602', @PERMISSIONCODE='602';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.603', @PERMISSIONCODE='603';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.604', @PERMISSIONCODE='604';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.605', @PERMISSIONCODE='605';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.700', @PERMISSIONCODE='700';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.701', @PERMISSIONCODE='701';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.702', @PERMISSIONCODE='702';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.801', @PERMISSIONCODE='801';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.802', @PERMISSIONCODE='802';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.803', @PERMISSIONCODE='803';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.804', @PERMISSIONCODE='804';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.805', @PERMISSIONCODE='805';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.806', @PERMISSIONCODE='806';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.807', @PERMISSIONCODE='807';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.808', @PERMISSIONCODE='808';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.900', @PERMISSIONCODE='900';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.901', @PERMISSIONCODE='901';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.902', @PERMISSIONCODE='902';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.903', @PERMISSIONCODE='903';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.904', @PERMISSIONCODE='904';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.905', @PERMISSIONCODE='905';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.906', @PERMISSIONCODE='906';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.907', @PERMISSIONCODE='907';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.908', @PERMISSIONCODE='908';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.909', @PERMISSIONCODE='909';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.910', @PERMISSIONCODE='910';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc006', @PERMISSIONCODE='Doc006';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit a donor decision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateDecision', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateDecision', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit donor meetings & actions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateMeeting', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateMeeting', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='List Donor Decisions, Meetings & Actions DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='View DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit the equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='List EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Add or update an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Audit', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Audit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Create an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Create', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Create', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Delete an active equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.FinalizeEquipmentDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='View the list of distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Enter a bulk audit result for distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory.BulkAudit', @PERMISSIONCODE='BulkAudit';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Bulk transfer distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory.BulkTransfer', @PERMISSIONCODE='BulkTransfer';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Set the delivery date of distributed equipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory.SetDeliveredToEndUserDate', @PERMISSIONCODE='SetDeliveredToEndUserDate';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListInventory', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.SetDeliveryDate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SetDeliveryDate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.SetDeliveryDate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Transfer', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Transfer', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Transfer', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit the equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='List EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Export EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='Audit Equipment EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.Audit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='List Equipment Locations EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='Add / edit a community asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='List CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='View CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='Add / edit a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View the list of community rounds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.AddUpdate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.View', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='List Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='List activites grouped by indicator', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Analysis', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='ConceptNote.Analysis', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='Add / edit an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='List indicators grouped by activity', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Analysis', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.Analysis', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='List Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='Add / edit an indicatortype', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='List IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='View IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LogicalFramework', @DESCRIPTION='Logical Framework Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='LogicalFrameworkUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='LogicalFramework.LogicalFrameworkUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='Add / edit a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='List Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Add / edit an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='M & E Overview Charts Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='ChartList', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.ChartList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='List Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Manage Objectives & Indicators Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Manage', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Manage', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Overview Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit a concep nNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Budget ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.ExportConceptNoteBudget', @PERMISSIONCODE='ExportConceptNoteBudget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit activity finances', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Finances', @PERMISSIONCODE='Finances';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk pane a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Risk', @PERMISSIONCODE='Risk';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Vetting List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Vetting ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList.ExportVetting', @PERMISSIONCODE='ExportVetting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Access to Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetAccesstoJustice', @PERMISSIONCODE='BudgetAccesstoJustice';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Communication', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunication', @PERMISSIONCODE='BudgetCommunication';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunityEngagement', @PERMISSIONCODE='BudgetCommunityEngagement';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Integrated Legitimate Structures', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetIntegratedLegitimateStructures', @PERMISSIONCODE='BudgetIntegratedLegitimateStructures';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetM&E', @PERMISSIONCODE='BudgetM&E';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type MER', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetMER', @PERMISSIONCODE='BudgetMER';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Police Development', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetPoliceDevelopment', @PERMISSIONCODE='BudgetPoliceDevelopment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type RAP', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetRAP', @PERMISSIONCODE='BudgetRAP';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetResearch', @PERMISSIONCODE='BudgetResearch';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk panel in a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Risk', @PERMISSIONCODE='Risk';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Add / edit equipment associated with a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Finalize Equipment Distribution ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='List ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='View ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Export ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='Add / edit a license', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='List License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='View License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Add / edit the license equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='List LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Export LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='View LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Add / edit a purchase request', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='List PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='View PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Export PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Add or update a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Delete a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='List workplans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='View a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Add / edit a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='List ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Export ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Add / edit a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='List Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the analysis tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Analysis', @PERMISSIONCODE='Analysis';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Export Equipment Distributions Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Implementation Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Implementation', @PERMISSIONCODE='Implementation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the information tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Information', @PERMISSIONCODE='Information';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='AssetUnit', @DESCRIPTION='A bypass to allow asset units not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='AssetUnit.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='Add / edit an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='List Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='Add / edit a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='List Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='List Forces Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a force Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='List Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Add / edit an media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View the list of media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Export selected media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View a media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Newsletter', @DESCRIPTION='Newsletter Add Update page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Newsletter.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Newsletter', @DESCRIPTION='This sends the newsletter', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Send', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Newsletter.Send', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Add / edit a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='List Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Export Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Add', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Add', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='List RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Export RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Amend a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Add / edit a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Export Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='List Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Amend a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='List SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Approved SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Approved', @PERMISSIONCODE='Approved';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Export SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='In Work SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.InWork', @PERMISSIONCODE='InWork';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Administer a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AdministerSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Export Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ExportSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ExportSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Questions List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListQuestions', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListQuestions', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='List Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Surveys List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveys', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Questions SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Surveys SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Weekly Report Add / Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Export the weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='weeklyreport', @DESCRIPTION='purges screwed up reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='delete', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='weeklyreport.delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='View the completed weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='Add / edit a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View the list of zones', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Newsletter', @DESCRIPTION='Newsletter list page', @DISPLAYORDER=1, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Newsletter.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='Add / edit a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='List SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='View SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Add / edit a class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='List Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='List Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table dbo.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO utility.BuildLog (BuildKey) VALUES ('Build - 3.01 - 2017.07.29 21.55.11')
GO
--End build tracking

