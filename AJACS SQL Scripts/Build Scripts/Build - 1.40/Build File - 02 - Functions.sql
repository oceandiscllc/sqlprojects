USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForCommunityExport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForCommunityExport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date:	2015.12.02
-- Description:	A function to return communities assets for placement on a google map
-- ==================================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForCommunityExport
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
		+replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + AT.Icon + '|' 
		+ CAST(ISNULL(CA.Location.STX ,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(CA.Location.STY,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dbo.communityAsset CA ON CA.CommunityID = C.communityID  
			AND C.CommunityID = @CommunityID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID 
			AND AT.AssetTypeID != 0

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForCommunityExport

--Begin function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
EXEC utility.DropObject 'dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2015.08.02
-- Description:	A function to return the name of a territory
-- =========================================================

CREATE FUNCTION dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryID INT
)

RETURNS VARCHAR(355)

AS
BEGIN

	DECLARE @cTerritoryName VARCHAR(250) = ''
	DECLARE @cTerritoryTypeName VARCHAR(50) = ' (' + @TerritoryTypeCode + ')'
	
	IF @TerritoryTypeCode = 'Community'
		SELECT @cTerritoryName = T.CommunityName FROM dbo.Community T WHERE T.CommunityID = @TerritoryID
	ELSE IF @TerritoryTypeCode = 'Province'
		SELECT @cTerritoryName = T.ProvinceName FROM dbo.Province T WHERE T.ProvinceID = @TerritoryID
	--ENDIF
	
	RETURN ISNULL(@cTerritoryName, '') + @cTerritoryTypeName
	
END
GO
--End function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID

--Begin function procurement.GetLastEquipmentAuditDate
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditDate'
GO

-- ================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last auditdate for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
-- ================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditDate
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryEquipmentInventoryID INT
)

RETURNS DATE

AS
BEGIN

	DECLARE @dLastAuditDate DATE = NULL
	
	IF @TerritoryTypeCode = 'Community'
		BEGIN
		
		SELECT TOP 1 @dLastAuditDate = T.AuditDate
		FROM procurement.CommunityEquipmentInventoryAudit T
		WHERE T.CommunityEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.CommunityEquipmentInventoryAuditID DESC

		END
	ELSE IF @TerritoryTypeCode = 'Province'	
		BEGIN
		
		SELECT TOP 1 @dLastAuditDate = T.AuditDate
		FROM procurement.ProvinceEquipmentInventoryAudit T
		WHERE T.ProvinceEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.ProvinceEquipmentInventoryAuditID DESC

		END
	--ENDIF
	
	RETURN @dLastAuditDate

END
GO
--End function procurement.GetLastEquipmentAuditDate