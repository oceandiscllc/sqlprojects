USE AJACS
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'CommunityProvinceEngagementUpdate.Export')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT
		P.PermissionableID,
		'Export',
		'Export'
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'CommunityProvinceEngagementUpdate'

	END
--ENDIF

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
