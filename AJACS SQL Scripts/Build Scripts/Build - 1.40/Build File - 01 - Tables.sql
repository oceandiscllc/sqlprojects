USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table dbo.Community

--Begin table dbo.ConceptNoteContactEquipment
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContactEquipment'

EXEC utility.AddColumn @TableName, 'TerritoryTypeCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'TerritoryID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
GO

UPDATE CNCE
SET 
	CNCE.TerritoryTypeCode = 
		CASE
			WHEN C.CommunityID > 0
			THEN 'Community'
			ELSE 'Province'
		END,
			
	CNCE.TerritoryID = 
		CASE
			WHEN C.CommunityID > 0
			THEN C.CommunityID
			ELSE C.ProvinceID
		END

FROM dbo.ConceptNoteContactEquipment CNCE
	JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
		AND CNCE.TerritoryID = 0
GO
--End table dbo.ConceptNoteContactEquipment

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table dbo.Province

--Begin table dropdown.AuditOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.AuditOutcome'

EXEC utility.AddColumn @TableName, 'AuditOutcomeCode', 'VARCHAR(50)'
GO

UPDATE dropdown.AuditOutcome
SET AuditOutcomeCode = 'Transfer'
WHERE AuditOutcomeName = 'Transferred'
GO

UPDATE dropdown.AuditOutcome
SET AuditOutcomeCode = 'Lost'
WHERE AuditOutcomeName = 'Verified - Destroyed'
GO
--End table dropdown.AuditOutcome

--Begin table policeengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Community'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Community

--Begin table policeengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Province'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Province

--Begin table procurement.CommunityEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.CommunityEquipmentInventory'

EXEC utility.AddColumn @TableName, 'IsLost', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsLost', 'BIT', 0
GO
--End table procurement.CommunityEquipmentInventory

--Begin table procurement.ProvinceEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.ProvinceEquipmentInventory'

EXEC utility.AddColumn @TableName, 'IsLost', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsLost', 'BIT', 0
GO
--End table procurement.ProvinceEquipmentInventory