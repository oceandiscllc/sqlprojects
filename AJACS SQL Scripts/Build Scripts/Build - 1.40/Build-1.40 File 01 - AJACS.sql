-- File Name:	Build-1.40 File 01 - AJACS.sql
-- Build Key:	Build-1.40 File 01 - AJACS - 2015.12.07 21.17.51

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatStaticGoogleMapForCommunityExport
--		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
--		procurement.GetLastEquipmentAuditDate
--
-- Procedures:
--		dbo.GetCommunityMemberSurveyChartDataByCommunityID
--		dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID
--		dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID
--		dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID
--		dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID
--		dbo.GetSpotReportBySpotReportID
--		dropdown.GetAuditOutcomeData
--		policeengagementupdate.GetCommunityByCommunityID
--		policeengagementupdate.GetProvinceByProvinceID
--		procurement.GetEquipmentInventoryByEquipmentInventoryID
--		reporting.GetEquipmentInventoryByCommunityID
--		reporting.GetEquipmentInventoryByProvinceID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table dbo.Community

--Begin table dbo.ConceptNoteContactEquipment
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContactEquipment'

EXEC utility.AddColumn @TableName, 'TerritoryTypeCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'TerritoryID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
GO

UPDATE CNCE
SET 
	CNCE.TerritoryTypeCode = 
		CASE
			WHEN C.CommunityID > 0
			THEN 'Community'
			ELSE 'Province'
		END,
			
	CNCE.TerritoryID = 
		CASE
			WHEN C.CommunityID > 0
			THEN C.CommunityID
			ELSE C.ProvinceID
		END

FROM dbo.ConceptNoteContactEquipment CNCE
	JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
		AND CNCE.TerritoryID = 0
GO
--End table dbo.ConceptNoteContactEquipment

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table dbo.Province

--Begin table dropdown.AuditOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.AuditOutcome'

EXEC utility.AddColumn @TableName, 'AuditOutcomeCode', 'VARCHAR(50)'
GO

UPDATE dropdown.AuditOutcome
SET AuditOutcomeCode = 'Transfer'
WHERE AuditOutcomeName = 'Transferred'
GO

UPDATE dropdown.AuditOutcome
SET AuditOutcomeCode = 'Lost'
WHERE AuditOutcomeName = 'Verified - Destroyed'
GO
--End table dropdown.AuditOutcome

--Begin table policeengagementupdate.Community
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Community'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Community

--Begin table policeengagementupdate.Province
DECLARE @TableName VARCHAR(250) = 'policeengagementupdate.Province'

EXEC utility.AddColumn @TableName, 'PoliceEngagementOutput2', 'VARCHAR(MAX)'
GO
--End table policeengagementupdate.Province

--Begin table procurement.CommunityEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.CommunityEquipmentInventory'

EXEC utility.AddColumn @TableName, 'IsLost', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsLost', 'BIT', 0
GO
--End table procurement.CommunityEquipmentInventory

--Begin table procurement.ProvinceEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.ProvinceEquipmentInventory'

EXEC utility.AddColumn @TableName, 'IsLost', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsLost', 'BIT', 0
GO
--End table procurement.ProvinceEquipmentInventory
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForCommunityExport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForCommunityExport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date:	2015.12.02
-- Description:	A function to return communities assets for placement on a google map
-- ==================================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForCommunityExport
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
		+replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + AT.Icon + '|' 
		+ CAST(ISNULL(CA.Location.STX ,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(CA.Location.STY,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dbo.communityAsset CA ON CA.CommunityID = C.communityID  
			AND C.CommunityID = @CommunityID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID 
			AND AT.AssetTypeID != 0

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForCommunityExport

--Begin function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
EXEC utility.DropObject 'dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2015.08.02
-- Description:	A function to return the name of a territory
-- =========================================================

CREATE FUNCTION dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryID INT
)

RETURNS VARCHAR(355)

AS
BEGIN

	DECLARE @cTerritoryName VARCHAR(250) = ''
	DECLARE @cTerritoryTypeName VARCHAR(50) = ' (' + @TerritoryTypeCode + ')'
	
	IF @TerritoryTypeCode = 'Community'
		SELECT @cTerritoryName = T.CommunityName FROM dbo.Community T WHERE T.CommunityID = @TerritoryID
	ELSE IF @TerritoryTypeCode = 'Province'
		SELECT @cTerritoryName = T.ProvinceName FROM dbo.Province T WHERE T.ProvinceID = @TerritoryID
	--ENDIF
	
	RETURN ISNULL(@cTerritoryName, '') + @cTerritoryTypeName
	
END
GO
--End function dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID

--Begin function procurement.GetLastEquipmentAuditDate
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditDate'
GO

-- ================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last auditdate for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
-- ================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditDate
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryEquipmentInventoryID INT
)

RETURNS DATE

AS
BEGIN

	DECLARE @dLastAuditDate DATE = NULL
	
	IF @TerritoryTypeCode = 'Community'
		BEGIN
		
		SELECT TOP 1 @dLastAuditDate = T.AuditDate
		FROM procurement.CommunityEquipmentInventoryAudit T
		WHERE T.CommunityEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.CommunityEquipmentInventoryAuditID DESC

		END
	ELSE IF @TerritoryTypeCode = 'Province'	
		BEGIN
		
		SELECT TOP 1 @dLastAuditDate = T.AuditDate
		FROM procurement.ProvinceEquipmentInventoryAudit T
		WHERE T.ProvinceEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.ProvinceEquipmentInventoryAuditID DESC

		END
	--ENDIF
	
	RETURN @dLastAuditDate

END
GO
--End function procurement.GetLastEquipmentAuditDate
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetCommunityMemberSurveyChartDataByCommunityID
EXEC Utility.DropObject 'dbo.GetCommunityMemberSurveyChartDataByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.02
-- Description:	A stored procedure to get data from the dbo.CommunityMemberSurvey table
-- ====================================================================================
CREATE PROCEDURE dbo.GetCommunityMemberSurveyChartDataByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Concern
	EXEC dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID @CommunityID
	--Confidence
	EXEC dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID @CommunityID
	--FSPPerception
	EXEC dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID @communityID
	--Services
	EXEC dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID @communityID

END
GO
--End procedure dbo.GetCommunityMemberSurveyChartDataByCommunityID

--Begin procedure dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID
EXEC Utility.DropObject 'dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.02
-- Description:	A stored procedure to get data from the dbo.CommunityMemberSurvey table
-- ====================================================================================
CREATE PROCEDURE dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	--Concern
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountC AS Most,
		D.QuestionCountB AS Neutral,
		D.QuestionCountA AS Least
	FROM
		(
		SELECT
			6 AS Question,
			'Robbery' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question54 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question54 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question54 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			5 AS Question,
			'Sexual assault/violence' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question55 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question55 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question55 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			4 AS Question,
			'Murder' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question56 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question56 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question56 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			3 AS Question,
			'Kidnapping' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question57 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question57 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question57 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			2 AS Question,
			'Vandalism' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question58 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question58 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question58 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			1 AS Question,
			'Assault' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question59 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question59 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question59 IN (4,5)) AS QuestionCountC
		) D
	ORDER BY D.Question

END
GO
--End procedure dbo.GetConcernCommunityMemberSurveyChartDataByCommunityID

--Begin procedure dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID
EXEC Utility.DropObject 'dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.02
-- Description:	A stored procedure to get data from the dbo.CommunityMemberSurvey table
-- ====================================================================================
CREATE PROCEDURE dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	--Confidence
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountC AS Most,
		D.QuestionCountB AS Neutral,
		D.QuestionCountA AS Least
	FROM
		(
		SELECT
			5 AS Question,
			'Police' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question22 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question22 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question22 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			4 AS Question,
			'Armed Groups' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question23 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question23 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question23 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			3 AS Question,
			'Formal Courts' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question24 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question24 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question24 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			2 AS Question,
			'Community leaders' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question25 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question25 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question25 IN (4,5)) AS QuestionCountC
	
		UNION
	
		SELECT
			1 AS Question,
			'Community Based Organizations' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question26 IN (1,2)) AS QuestionCountA,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question26 = 3) AS QuestionCountB,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question26 IN (4,5)) AS QuestionCountC
		) D
	ORDER BY D.Question

END
GO
--End procedure dbo.GetConfidenceCommunityMemberSurveyChartDataByCommunityID

--Begin procedure dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID
EXEC Utility.DropObject 'dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.02
-- Description:	A stored procedure to get data from the dbo.CommunityMemberSurvey table
-- ====================================================================================
CREATE PROCEDURE dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	--FSPPerception
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountYes AS Yes,
		D.QuestionCountNo AS No
	FROM
		(
		SELECT
			1 AS Question,
			'Are the police are responsive to your concerns' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question62 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question62 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			2 AS Question,
			'Do the police serve your interests' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question64 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question64 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			3 AS Question,
			'Are you able to voice your concerns' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question67 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question67 = 2) AS QuestionCountNo
		) D
	ORDER BY D.Question

END
GO
--End procedure dbo.GetFSPPerceptionCommunityMemberSurveyChartDataByCommunityID

--Begin procedure dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID
EXEC Utility.DropObject 'dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date: 2015.10.02
-- Description:	A stored procedure to get data from the dbo.CommunityMemberSurvey table
-- ====================================================================================
CREATE PROCEDURE dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	--Services
	SELECT
		D.Question,
		D.Label,
		D.QuestionCountYes AS Yes,
		D.QuestionCountNo AS No
	FROM
		(
		SELECT
			12 AS Question,
			'Formal courts' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question01 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question01 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			11 AS Question,
			'Legal support for low income populations' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question02 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question02 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			10 AS Question,
			'Victim support' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question03 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question03 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			9 AS Question,
			'Family services' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question04 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question04 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			8 AS Question,
			'Dispute mediation (property)' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question05 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question05 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			7 AS Question,
			'Dispute mediation (land)' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question06 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question06 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			6 AS Question, 
			'Dispute mediation (business)' AS Label, 
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question07 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question07 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			5 AS Question, 
			'Dispute mediation (other)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question08 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question08 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			4 AS Question, 
			'Public records services (birth/death certificates)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question10 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question10 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			3 AS Question, 
			'Public records services (land/property)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question11 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question11 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			2 AS Question, 
			'Public records services (marriage)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question12 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question12 = 2) AS QuestionCountNo
	
		UNION
	
		SELECT
			1 AS Question, 
			'Public records services (other)' AS Label,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question13 = 1) AS QuestionCountYes,
			(SELECT COUNT(CMS.CommunityMemberSurveyID) FROM dbo.CommunityMemberSurvey CMS WHERE CMS.Communityid = @CommunityID AND CMS.Question13 = 2) AS QuestionCountNo
		) D
	ORDER BY D.Question

END
GO
--End procedure dbo.GetServicesCommunityMemberSurveyChartDataByCommunityID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunityID,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProvinceID,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), 0) AS ThumbnailLength,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dropdown.GetAuditOutcomeData
EXEC Utility.DropObject 'dropdown.GetAuditOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.23
-- Description:	A stored procedure to return data from the dropdown.AuditOutcome table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetAuditOutcomeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AuditOutcomeID, 
		T.AuditOutcomeCode,
		T.AuditOutcomeName
	FROM dropdown.AuditOutcome T
	WHERE (T.AuditOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AuditOutcomeName, T.AuditOutcomeID

END
GO
--End procedure dropdown.GetAuditOutcomeData

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================

-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
--
-- Author:			Greg Yingling
-- Update date:	2015.05.26
-- Description:	Added the Audit Outcome, Audit Date, and Audit Evidence Call
--
-- Author:			Todd Pires
-- Update date:	2015.11.29
-- Description:	Refactored the audit recordset
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.ExpirationDate,
		dbo.FormatDate(EI.ExpirationDate) AS ExpirationDateFormatted,
		EI.IMEIMACAddress,
		EI.UnitCost,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.EquipmentStatusID,
		ES.EquipmentStatusName,
		EI.EquipmentRemovalReasonID,
		ER.EquipmentRemovalReasonName,
		EI.EquipmentRemovalDate,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		D.Territory,
		SUM(D.Quantity) AS Quantity,
		dbo.FormatConceptNoteTitle(D.ConceptNoteID) AS ConceptNote,
		dbo.FormatDate(D.DistributionDate) AS DistributionDateFormatted,
		IIF(D.IsLost = 1, 'Yes ', 'No ') AS IsLost
	FROM
		(
		SELECT
			C.CommunityName + ' (Community)' AS Territory,
			CEI.Quantity,
			CEI.ConceptNoteID,
			CEI.DistributionDate,
			CEI.IsLost
		FROM procurement.CommunityEquipmentInventory CEI
			JOIN dbo.Community C ON C.CommunityID = CEI.CommunityID
				AND CEI.EquipmentInventoryID = @EquipmentInventoryID
	
		UNION
	
		SELECT
			P.ProvinceName + ' (Province)' AS Territory,
			PEI.Quantity,
			PEI.ConceptNoteID,
			PEI.DistributionDate,
			PEI.IsLost
		FROM procurement.ProvinceEquipmentInventory PEI
			JOIN dbo.Province P ON P.ProvinceID = PEI.ProvinceID
				AND PEI.EquipmentInventoryID = @EquipmentInventoryID
		) D
	GROUP BY D.Territory, D.ConceptNoteID, D.DistributionDate, D.IsLost
	ORDER BY D.Territory

	SELECT
		CEIA.AuditQuantity, 
		CEIA.AuditDate,
		dbo.FormatDate(CEIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(CEIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = CEIA.DocumentID)
			THEN '<a class="btn btn-info" href="/servefile/getFile/GUID/' + (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = CEIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN CEIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(''Community'', ' + CAST(CEIA.CommunityEquipmentInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.CommunityEquipmentInventoryAudit CEIA
		JOIN procurement.CommunityEquipmentInventory CEI ON CEI.CommunityEquipmentInventoryID = CEIA.CommunityEquipmentInventoryID
			AND CEI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = CEIA.AuditOutcomeID
	
	UNION
	
	SELECT
		PEIA.AuditQuantity, 
		PEIA.AuditDate,
		dbo.FormatDate(PEIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(PEIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = PEIA.DocumentID)
			THEN '<a class="btn btn-info" href="/servefile/getFile/GUID/' + (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = PEIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN PEIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(''Province'', ' + CAST(PEIA.ProvinceEquipmentInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.ProvinceEquipmentInventoryAudit PEIA
		JOIN procurement.ProvinceEquipmentInventory PEI ON PEI.ProvinceEquipmentInventoryID = PEIA.ProvinceEquipmentInventoryID
			AND PEI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = PEIA.AuditOutcomeID
	
	ORDER BY 2 DESC

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure policeengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'policeengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Community and policeengagementupdate.Community tables
--
-- Author:		Eric Jones
-- Create date:	2015.11.29
-- Description:	altered to allow pulling back of Field Activitied (aka projects) with POL component codes
-- =================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CapacityAssessmentDate,
		dbo.FormatDate(C.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.MaterialSupportStatus,
		C.PoliceEngagementOutput1,
		C.PoliceEngagementOutput2,
		C.PPPDate,
		dbo.FormatDate(C.PPPDate) AS PPPDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CapacityAssessmentDate,
		dbo.FormatDate(C1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		C1.MaterialSupportStatus,
		C1.PoliceEngagementOutput1,
		C1.PoliceEngagementOutput2,
		C1.PPPDate,
		dbo.FormatDate(C1.PPPDate) AS PPPDateFormatted,
		C2.CommunityID,
		C2.CommunityName AS EntityName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
			AND C1.CommunityID = @CommunityID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID 
			FROM dbo.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.PoliceEngagementAchievedValue,
		OACI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.PoliceEngagementAchievedValue, 
				CI.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(ISNULL(OACP.ProjectCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PC.ProjectCommunityID
			FROM project.ProjectCommunity PC
			WHERE PC.ProjectID = P.ProjectID
				AND PC.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		OACP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(P.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				CP.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityProject CP
			WHERE CP.ProjectID = P.ProjectID
				AND CP.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(ISNULL(OACR.RecommendationCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RC.RecommendationCommunityID
			FROM recommendation.RecommendationCommunity RC
			WHERE RC.RecommendationID = R.RecommendationID
				AND RC.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR

	--EntityRiskUpdate
	SELECT
		OACR.PoliceEngagementRiskValue,
		OACR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM policeengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID,
					CC.PoliceEngagementNotes
				FROM policeengagementupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.CommunityID = @CommunityID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.CommunityID = @CommunityID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.CommunityID = @CommunityID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.CommunityID = @CommunityID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.CommunityID = @CommunityID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetCommunityByCommunityID

--Begin procedure policeengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'policeengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.21
-- Description:	A stored procedure to return data from the dbo.Province table and policeengagementupdate.Province tables
--
-- Author:		Eric Jones
-- Create date:	2015.11.29
-- Description:	altered to allow pulling back of Field Activitied (aka projects) with POL component codes
-- =====================================================================================================================
CREATE PROCEDURE policeengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CapacityAssessmentDate,
		dbo.FormatDate(P.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P.MaterialSupportStatus,
		P.PoliceEngagementOutput1,
		P.PoliceEngagementOutput2,
		P.PPPDate,
		dbo.FormatDate(P.PPPDate) AS PPPDateFormatted,
		P.ProvinceID,
		P.ProvinceName AS EntityName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CapacityAssessmentDate,
		dbo.FormatDate(P1.CapacityAssessmentDate) AS CapacityAssessmentDateFormatted,
		P1.MaterialSupportStatus,
		P1.PoliceEngagementOutput1,
		P1.PoliceEngagementOutput2,
		P1.PPPDate,
		dbo.FormatDate(P1.PPPDate) AS PPPDateFormatted,
		P2.ProvinceID,
		P2.ProvinceName AS EntityName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
			AND P1.ProvinceID = @ProvinceID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO3'
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'PoliceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.PoliceEngagementAchievedValue,
		OAPI.PoliceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.PoliceEngagementAchievedValue, 
				PRI.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(OAPP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PP.ProjectProvinceID
			FROM project.ProjectProvince PP
			WHERE PP.ProjectID = P.ProjectID
				AND PP.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		OAPP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(P.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP1 ON PP1.ProjectID = P.ProjectID
			AND PP1.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND C.ComponentAbbreviation = 'POL'
		OUTER APPLY
			(
			SELECT
				PP2.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceProject PP2
			WHERE PP2.ProjectID = P.ProjectID
				AND PP2.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(ISNULL(OAPR.RecommendationProvinceID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				RP.RecommendationProvinceID
			FROM recommendation.RecommendationProvince RP
			WHERE RP.RecommendationID = R.RecommendationID
				AND RP.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.PoliceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
					JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR

	--EntityRiskUpdate
	SELECT
		OAPR.PoliceEngagementRiskValue,
		OAPR.PoliceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.PoliceEngagementNotes
			FROM policeengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		'<a class="btn btn-info" onclick="getClassProvinceNotes(' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OAPC.PoliceEngagementNotes,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OAPC.ProvinceClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
				WHERE CNP.ConceptNoteID = CNC.ConceptNoteID
					AND CNP.ProvinceID = @ProvinceID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'PEO2'
				)
			OUTER APPLY
				(
				SELECT
					PC.ProvinceClassID,
					PC.PoliceEngagementNotes
				FROM policeengagementupdate.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = @ProvinceID
				) OAPC

	--Material Support 01
	SELECT 
		ISNULL(SUM(CAUCR.CommunityAssetUnitCostRate), 0) AS CommunityAssetUnitCostRateTotal
	FROM dbo.CommunityAsset CA
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetID = CA.CommunityAssetID
		JOIN dropdown.CommunityAssetUnitCostRate CAUCR ON CAUCR.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
			AND CAUT.CommunityAssetUnitTypeCode = 'Police'
			AND CA.ProvinceID = @ProvinceID

	--Material Support 02
	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidTotal
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID

	--Material Support 03
	DECLARE @nLastPaymentYYYY INT
	DECLARE @nLastPaymentMM INT
	DECLARE @dLastPayment DATE

	SELECT TOP 1
		@nLastPaymentYYYY = CSP.PaymentYear,
		@nLastPaymentMM = CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.StipendPaidDate IS NOT NULL
		AND CSP.ProvinceID = @ProvinceID
	ORDER BY CSP.StipendPaidDate DESC

	SET @dLastPayment = CAST(CAST(@nLastPaymentMM AS VARCHAR(2)) + '/01/' + CAST(@nLastPaymentYYYY AS VARCHAR(4)) AS DATE)

	SELECT
		ISNULL(SUM(CSP.StipendAmountPaid), 0) AS StipendAmountPaidLast,
		CSP.PaymentYear,
		CSP.PaymentMonth
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear = @nLastPaymentYYYY
		AND CSP.PaymentMonth = @nLastPaymentMM
		AND CSP.ProvinceID = @ProvinceID
	GROUP BY CSP.PaymentYear, CSP.PaymentMonth

	--Material Support 04
	SELECT 
		S.StipendName,
		ISNULL(OAC.ItemCount, 0) AS ItemCount
	FROM dropdown.Stipend S
		OUTER APPLY
			(
			SELECT 
				C.StipendID,
				COUNT(C.StipendID) AS ItemCount
			FROM dbo.Contact C
			WHERE EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CCT.ContactID = C.ContactID
						AND CT.ContactTypeCode = 'Stipend'
				)
				AND C.StipendID = S.StipendID
				AND C.IsActive = 1
				AND C.ProvinceID = @ProvinceID
			GROUP BY C.StipendID
			) OAC
	WHERE S.StipendID > 0
	ORDER BY S.DisplayOrder

	--Material Support 05
	SELECT
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = @nLastPaymentYYYY
			AND CSP.PaymentMonth = @nLastPaymentMM
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid0,
		@nLastPaymentYYYY AS StipendAmountPaidYear0,
		@nLastPaymentMM AS StipendAmountPaidMonth0,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -1, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -1, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid1,
		YEAR(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidYear1,
		MONTH(DATEADD(m, -1, @dLastPayment)) AS StipendAmountPaidMonth1,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -2, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -2, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid2,
		YEAR(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidYear2,
		MONTH(DATEADD(m, -2, @dLastPayment)) AS StipendAmountPaidMonth2,
		(
		SELECT
			ISNULL(SUM(CSP.StipendAmountPaid), 0)
		FROM dbo.ContactStipendPayment CSP
		WHERE CSP.PaymentYear = YEAR(DATEADD(m, -3, @dLastPayment))
			AND CSP.PaymentMonth = MONTH(DATEADD(m, -3, @dLastPayment))
			AND CSP.ProvinceID = @ProvinceID
		) AS StipendAmountPaid3,
		YEAR(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidYear3,
		MONTH(DATEADD(m, -3, @dLastPayment)) AS StipendAmountPaidMonth3

END
GO
--End procedure policeengagementupdate.GetProvinceByProvinceID

--Begin procedure reporting.GetEquipmentInventoryByCommunityID
EXEC Utility.DropObject 'reporting.GetEquipmentInventoryByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.02
-- Description:	A stored procedure to return data from the procurement.CommunityEquipmentInventory table
-- =====================================================================================================
CREATE PROCEDURE reporting.GetEquipmentInventoryByCommunityID

@CommunityID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		CEI.Quantity,
		dbo.FormatDate(CEI.DistributionDate) AS DistributionDateFormatted,
		dbo.FormatDate(procurement.GetLastEquipmentAuditDate('Community', CEI.CommunityEquipmentInventoryID)) AS LastAuditDateFormatted,
		procurement.GetLastEquipmentAuditOutcome('Community', CEI.CommunityEquipmentInventoryID) AS LastAuditOutcomeName
	FROM procurement.CommunityEquipmentInventory CEI
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND CEI.CommunityID = @CommunityID
	ORDER BY EC.ItemName, CEI.CommunityEquipmentInventoryID

END
GO
--End procedure reporting.GetEquipmentInventoryByCommunityID

--Begin procedure reporting.GetEquipmentInventoryByProvinceID
EXEC Utility.DropObject 'reporting.GetEquipmentInventoryByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.02
-- Description:	A stored procedure to return data from the procurement.ProvinceEquipmentInventory table
-- =====================================================================================================
CREATE PROCEDURE reporting.GetEquipmentInventoryByProvinceID

@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		CEI.Quantity,
		dbo.FormatDate(CEI.DistributionDate) AS DistributionDateFormatted,
		dbo.FormatDate(procurement.GetLastEquipmentAuditDate('Province', CEI.ProvinceEquipmentInventoryID)) AS LastAuditDateFormatted,
		procurement.GetLastEquipmentAuditOutcome('Province', CEI.ProvinceEquipmentInventoryID) AS LastAuditOutcomeName
	FROM procurement.ProvinceEquipmentInventory CEI
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
			AND CEI.ProvinceID = @ProvinceID
	ORDER BY EC.ItemName, CEI.ProvinceEquipmentInventoryID

END
GO
--End procedure reporting.GetEquipmentInventoryByProvinceID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'CommunityProvinceEngagementUpdate.Export')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT
		P.PermissionableID,
		'Export',
		'Export'
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'CommunityProvinceEngagementUpdate'

	END
--ENDIF

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.40 File 01 - AJACS - 2015.12.07 21.17.51')
GO
--End build tracking

