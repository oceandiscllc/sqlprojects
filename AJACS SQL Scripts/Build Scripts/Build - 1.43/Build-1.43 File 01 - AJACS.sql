-- File Name:	Build-1.43 File 01 - AJACS.sql
-- Build Key:	Build-1.43 File 01 - AJACS - 2016.01.04 21.36.04

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatStaticGoogleMapForCommunityExport
--		eventlog.GetCommunityIndicatorXMLByFIFUpdateID
--		eventlog.GetCommunityMeetingXMLByFIFUpdateID
--		eventlog.GetCommunityRiskXMLByFIFUpdateID
--		eventlog.GetCommunityXMLByFIFUpdateID
--		eventlog.GetProvinceIndicatorXMLByFIFUpdateID
--		eventlog.GetProvinceMeetingXMLByFIFUpdateID
--		eventlog.GetProvinceRiskXMLByFIFUpdateID
--		eventlog.GetProvinceXMLByFIFUpdateID
--		permissionable.HasPartialPermission
--
-- Procedures:
--		dbo.GetContactByContactID
--		dbo.GetRiskByRiskID
--		dbo.GetSpotReportBySpotReportID
--		eventlog.LogConceptNoteContactEquipmentAction
--		eventlog.LogFIFAction
--		fifupdate.AddFIFCommunity
--		fifupdate.AddFIFProvince
--		fifupdate.DeleteFIFCommunity
--		fifupdate.DeleteFIFProvince
--		fifupdate.GetCommunityByCommunityID
--		fifupdate.GetFIFUpdate
--		fifupdate.GetProvinceByProvinceID
--		reporting.GetSerialNumberTracker
--		utility.ServerSetupKeyAddUpdate
--		workflow.GetFIFUpdateWorkflowData
--		workflow.GetFIFUpdateWorkflowStepPeople
--
-- Schemas:
--		fifupdate
--
-- Tables:
--		dbo.CommunityMeeting
--		dbo.ProvinceMeeting
--		fifupdate.Community
--		fifupdate.CommunityIndicator
--		fifupdate.CommunityMeeting
--		fifupdate.CommunityRisk
--		fifupdate.FIFUpdate
--		fifupdate.Province
--		fifupdate.ProvinceIndicator
--		fifupdate.ProvinceMeeting
--		fifupdate.ProvinceRisk
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin schema fifupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'fifupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA fifupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema fifupdate
--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'FIFCommunityEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFJustice', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFPoliceEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFUpdateNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'IndicatorUpdate', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MeetingNotes', 'VARCHAR(MAX)'
GO
--End table dbo.Community

--Begin table dbo.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityIndicator'

EXEC utility.AddColumn @TableName, 'FIFAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
GO
--End table dbo.CommunityIndicator

--Begin table dbo.CommunityMeeting
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityMeeting
	(
	CommunityMeetingID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityMeetingID'
EXEC utility.SetIndexClustered 'IX_CommunityMeeting', @TableName, 'CommunityID,MeetingDate DESC'
GO
--End table dbo.CommunityMeeting

--Begin table dbo.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRisk'

EXEC utility.AddColumn @TableName, 'FIFRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
GO
--End table dbo.CommunityRisk

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'FIFCommunityEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFJustice', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFPoliceEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFUpdateNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'IndicatorUpdate', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MeetingNotes', 'VARCHAR(MAX)'
GO
--End table dbo.Province

--Begin table dbo.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceIndicator'

EXEC utility.AddColumn @TableName, 'FIFAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
GO
--End table dbo.ProvinceIndicator

--Begin table dbo.ProvinceMeeting
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceMeeting
	(
	ProvinceMeetingID INT IDENTITY(1,1) NOT NULL,
	ProvinceID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceMeetingID'
EXEC utility.SetIndexClustered 'IX_ProvinceMeeting', @TableName, 'ProvinceID,MeetingDate DESC'
GO
--End table dbo.ProvinceMeeting

--Begin table dbo.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceRisk'

EXEC utility.AddColumn @TableName, 'FIFRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
GO
--End table dbo.ProvinceRisk

--Begin table dbo.Risk
EXEC utility.AddColumn 'dbo.Risk', 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint 'dbo.Risk', 'IsActive', 'BIT', 1
GO
--End table dbo.Risk

--Begin table dbo.SpotReport
EXEC utility.DropColumn 'dbo.SpotReport', 'CommunityID'
EXEC utility.DropColumn 'dbo.SpotReport', 'ProvinceID'
GO
--End table dbo.SpotReport

--Begin table fifupdate.Community
DECLARE @TableName VARCHAR(250) = 'fifupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.Community
	(
	CommunityID INT,
	FIFUpdateID INT,
	FIFCommunityEngagement VARCHAR(MAX),
	FIFJustice VARCHAR(MAX),
	FIFPoliceEngagement VARCHAR(MAX),
	FIFUpdateNotes VARCHAR(MAX),
	IndicatorUpdate VARCHAR(MAX),
	MeetingNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table fifupdate.Community

--Begin table fifupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'fifupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	FIFAchievedValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table fifupdate.CommunityIndicator

--Begin table fifupdate.CommunityMeeting
DECLARE @TableName VARCHAR(250) = 'fifupdate.CommunityMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.CommunityMeeting
	(
	CommunityMeetingID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	CommunityID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityMeetingID'
EXEC utility.SetIndexClustered 'IX_CommunityMeeting', @TableName, 'CommunityID,MeetingDate DESC'
GO
--End table fifupdate.CommunityMeeting

--Begin table fifupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'fifupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	CommunityID INT,
	RiskID INT,
	FIFRiskValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table fifupdate.CommunityRisk

--Begin table fifupdate.FIFUpdate
DECLARE @TableName VARCHAR(250) = 'fifupdate.FIFUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.FIFUpdate
	(
	FIFUpdateID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'FIFUpdateID'
GO
--End table fifupdate.FIFUpdate

--Begin table fifupdate.Province
DECLARE @TableName VARCHAR(250) = 'fifupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.Province
	(
	ProvinceID INT,
	FIFUpdateID INT,
	FIFCommunityEngagement VARCHAR(MAX),
	FIFJustice VARCHAR(MAX),
	FIFPoliceEngagement VARCHAR(MAX),
	FIFUpdateNotes VARCHAR(MAX),
	IndicatorUpdate VARCHAR(MAX),
	MeetingNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table fifupdate.Province

--Begin table fifupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'fifupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	FIFAchievedValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table fifupdate.ProvinceIndicator

--Begin table fifupdate.ProvinceMeeting
DECLARE @TableName VARCHAR(250) = 'fifupdate.ProvinceMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.ProvinceMeeting
	(
	ProvinceMeetingID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	ProvinceID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceMeetingID'
EXEC utility.SetIndexClustered 'IX_ProvinceMeeting', @TableName, 'ProvinceID,MeetingDate DESC'
GO
--End table fifupdate.ProvinceMeeting

--Begin table fifupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'fifupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	FIFRiskValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table fifupdate.ProvinceRisk

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForCommunityExport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForCommunityExport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date:	2015.12.02
-- Description:	A function to return communities assets for placement on a google map
-- ==================================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForCommunityExport
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	DECLARE @CommunityMarker VARCHAR(MAX)=''

	SELECT
		@CommunityMarker += '&markers=icon:' 
		+ replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + REPLACE(ID.HexColor, '#', '') + '.png' + '|' 
		+ CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.CommunityID = @CommunityID
	
	SET @Gresult = @CommunityMarker

	SELECT
		@GResult += '&markers=icon:' 
		+replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + AT.Icon + '|' 
		+ CAST(ISNULL(CA.Location.STY ,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(CA.Location.STX,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dbo.communityAsset CA ON CA.CommunityID = C.communityID  
			AND C.CommunityID = @CommunityID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID 
			AND AT.AssetTypeID != 0

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForCommunityExport

--Begin function eventlog.GetCommunityXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return Community data for a specific FIFUpdate record
-- ================================================================================

CREATE FUNCTION eventlog.GetCommunityXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM fifupdate.Community T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByFIFUpdateID

--Begin function eventlog.GetCommunityIndicatorXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return CommunityIndicator data for a specific FIFUpdate record
-- =========================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM fifupdate.CommunityIndicator T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByFIFUpdateID

--Begin function eventlog.GetCommunityMeetingXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityMeetingXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return CommunityMeeting data for a specific FIFUpdate record
-- =======================================================================================

CREATE FUNCTION eventlog.GetCommunityMeetingXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityMeetings VARCHAR(MAX) = ''
	
	SELECT @cCommunityMeetings = COALESCE(@cCommunityMeetings, '') + D.CommunityMeeting
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityMeeting'), ELEMENTS) AS CommunityMeeting
		FROM fifupdate.CommunityMeeting T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<CommunityMeetings>' + ISNULL(@cCommunityMeetings, '') + '</CommunityMeetings>'

END
GO
--End function eventlog.GetCommunityMeetingXMLByFIFUpdateID

--Begin function eventlog.GetCommunityRiskXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return CommunityRisk data for a specific FIFUpdate record
-- ====================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM fifupdate.CommunityRisk T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByFIFUpdateID

--Begin function eventlog.GetProvinceXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return Province data for a specific FIFUpdate record
-- ================================================================================

CREATE FUNCTION eventlog.GetProvinceXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM fifupdate.Province T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByFIFUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return ProvinceIndicator data for a specific FIFUpdate record
-- ========================================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM fifupdate.ProvinceIndicator T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'

END
GO
--End function eventlog.GetProvinceIndicatorXMLByFIFUpdateID

--Begin function eventlog.GetProvinceMeetingXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceMeetingXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return ProvinceMeeting data for a specific FIFUpdate record
-- =======================================================================================

CREATE FUNCTION eventlog.GetProvinceMeetingXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceMeetings VARCHAR(MAX) = ''
	
	SELECT @cProvinceMeetings = COALESCE(@cProvinceMeetings, '') + D.ProvinceMeeting
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceMeeting'), ELEMENTS) AS ProvinceMeeting
		FROM fifupdate.ProvinceMeeting T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<ProvinceMeetings>' + ISNULL(@cProvinceMeetings, '') + '</ProvinceMeetings>'

END
GO
--End function eventlog.GetProvinceMeetingXMLByFIFUpdateID

--Begin function eventlog.GetProvinceRiskXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return ProvinceRisk data for a specific FIFUpdate record
-- ===================================================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM fifupdate.ProvinceRisk T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'

END
GO
--End function eventlog.GetProvinceRiskXMLByFIFUpdateID

--Begin function permissionable.HasPartialPermission
EXEC utility.DropObject 'permissionable.HasPartialPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.29
-- Description:	A function to determine if a PeronID has a partial permission
-- ==========================================================================

CREATE FUNCTION permissionable.HasPartialPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP WHERE PP.PermissionableLineage LIKE @PermissionableLineage + '%' AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function permissionable.HasPartialPermission

--Begin procedure utility.ServerSetupKeyAddUpdate
EXEC Utility.DropObject 'utility.ServerSetupKeyAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update server setup key records
--
-- Author:			Todd Pires
-- Update Date:	2015.07.01
-- Description:	Implemented the utility table synonym
-- ========================================================================
CREATE PROCEDURE utility.ServerSetupKeyAddUpdate

@ServerSetupKey VARCHAR(250),
@ServerSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM dbo.ServerSetup SS WHERE SS.ServerSetupKey = @ServerSetupKey)
		BEGIN

		INSERT INTO dbo.ServerSetup
			(ServerSetupKey,ServerSetupValue)
		VALUES
			(@ServerSetupKey,@ServerSetupValue)

		END
	ELSE
		BEGIN

		UPDATE dbo.ServerSetup
		SET ServerSetupValue = @ServerSetupValue
		WHERE ServerSetupKey = @ServerSetupKey

		END
	--ENDIF
	
END
GO
--End procedure utility.ServerSetupKeyAddUpdate
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.ArabicMotherName,
		C1.CellPhoneNumber,
		C1.CellPhoneNumberCountryCallingCodeID,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FaxNumberCountryCallingCodeID,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.IsValid,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.Notes,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PhoneNumberCountryCallingCodeID,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C1.CommunityAssetID,
		C1.CommunityAssetUnitID,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		C7.CountryID AS PlaceOfBirthCountryID,
		C7.CountryName AS PlaceOfBirthCountryName,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID
	ORDER BY CNC.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetRiskByRiskID
EXEC Utility.DropObject 'dbo.GetRiskByRiskID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date: 2015.07.21
-- Description:	A stored procedure to get data from the dbo.Risk table
--
-- Author:			Kevin Ross
-- Create date: 2015.08.09
-- Description:	Add support for the RiskType
--
-- Author:			Eric Jones
-- Create date: 2015.12.26
-- Description:	Added IsActive bit
-- ===================================================================
CREATE PROCEDURE dbo.GetRiskByRiskID

@RiskID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.EscalationCriteria,
		R.Impact,
		R.Likelihood,
		R.MitigationActions,
		R.Notes,
		R.Impact * R.Likelihood AS Overall,
		R.RaisedBy,
		R.RiskDescription,
		R.RiskID,
		R.RiskName,
		R.IsActive,
		RC.RiskCategoryID,
		RC.RiskCategoryName,
		RS.RiskStatusID,
		RS.RiskStatusName,
		RT.RiskTypeID,
		RT.RiskTypeName
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
		JOIN dropdown.RiskStatus RS ON RS.RiskStatusID = R.RiskStatusID
		JOIN dropdown.RiskType RT ON RT.RiskTypeID = R.RiskTypeID
			AND R.RiskID = @RiskID

END
GO
--End procedure dbo.GetRiskByRiskID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), 0) AS ThumbnailLength,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure eventlog.LogConceptNoteContactEquipmentAction
EXEC utility.DropObject 'eventlog.LogConceptNoteContactEquipmentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogConceptNoteContactEquipmentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ConceptNoteContactEquipment',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ConceptNoteContactEquipment',
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('ConceptNoteContactEquipmentUpdate'), ELEMENTS
			)
		FROM dbo.ConceptNoteContactEquipment T
		WHERE T.ConceptNoteContactEquipmentID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogConceptNoteContactEquipmentAction

--Begin procedure eventlog.LogFIFAction
EXEC utility.DropObject 'eventlog.LogFIFAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogFIFAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'FIFUpdate',
			T.FIFUpdateID,
			@Comments
		FROM fifupdate.FIFUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.FIFUpdateID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'FIFUpdate',
			T.FIFUpdateID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByFIFUpdateID(T.FIFUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByFIFUpdateID(T.FIFUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityMeetingXMLByFIFUpdateID(T.FIFUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByFIFUpdateID(T.FIFUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByFIFUpdateID(T.FIFUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByFIFUpdateID(T.FIFUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceMeetingXMLByFIFUpdateID(T.FIFUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByFIFUpdateID(T.FIFUpdateID) AS XML))
			FOR XML RAW('FIFUpdate'), ELEMENTS
			)
		FROM fifupdate.FIFUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.FIFUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityProvinceEngagementAction

--Begin procedure fifupdate.AddFIFCommunity
EXEC Utility.DropObject 'fifupdate.AddFIFCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Community table
-- ============================================================================
CREATE PROCEDURE fifupdate.AddFIFCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)
	
	INSERT INTO fifupdate.Community
		(CommunityID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@FIFUpdateID,
		C1.FIFCommunityEngagement, 
		C1.FIFJustice, 
		C1.FIFPoliceEngagement, 
		C1.FIFUpdateNotes, 
		C1.IndicatorUpdate, 
		C1.MeetingNotes, 
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO fifupdate.CommunityIndicator
		(FIFUpdateID, CommunityID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.FIFAchievedValue, 0),
		OACI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.FIFAchievedValue,
				CI.FIFNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO fifupdate.CommunityRisk
		(FIFUpdateID, CommunityID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		RC.CommunityID,
		R.RiskID,
		ISNULL(OACR.FIFRiskValue, 0),
		OACR.FIFNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
		OUTER APPLY
			(
			SELECT
				CR.FIFRiskValue, 
				CR.FIFNotes
			FROM dbo.CommunityRisk CR
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CR.CommunityID
					AND CR.RiskID = R.RiskID
			) OACR

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFCommunity

--Begin procedure fifupdate.AddFIFProvince
EXEC Utility.DropObject 'fifupdate.AddFIFProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Province table
-- ===========================================================================
CREATE PROCEDURE fifupdate.AddFIFProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)

	INSERT INTO fifupdate.Province
		(ProvinceID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@FIFUpdateID,
		P1.FIFCommunityEngagement, 
		P1.FIFJustice, 
		P1.FIFPoliceEngagement, 
		P1.FIFUpdateNotes, 
		P1.IndicatorUpdate, 
		P1.MeetingNotes,
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO fifupdate.ProvinceIndicator
		(FIFUpdateID, ProvinceID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.FIFAchievedValue, 0),
		OAPI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.FIFAchievedValue,
				PRI.FIFNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO fifupdate.ProvinceRisk
		(FIFUpdateID, ProvinceID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		RP.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.FIFRiskValue, 0),
		OAPR.FIFNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
		OUTER APPLY
			(
			SELECT
				PR.FIFRiskValue, 
				PR.FIFNotes
			FROM dbo.ProvinceRisk PR
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PR.ProvinceID
					AND PR.RiskID = R.RiskID
			) OAPR

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFProvince

--Begin procedure fifupdate.DeleteFIFCommunity
EXEC Utility.DropObject 'fifupdate.DeleteFIFCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to delete data from the fifupdate.Community table
-- =================================================================================
CREATE PROCEDURE fifupdate.DeleteFIFCommunity

@CommunityID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)

	DELETE T
	FROM fifupdate.Community T
	WHERE T.CommunityID = @CommunityID
	
	DELETE T
	FROM fifupdate.CommunityIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM fifupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM fifupdate.CommunityRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM fifupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.DeleteFIFCommunity

--Begin procedure fifupdate.DeleteFIFProvince
EXEC Utility.DropObject 'fifupdate.DeleteFIFProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to delete data from the fifupdate.Province table
-- ================================================================================
CREATE PROCEDURE fifupdate.DeleteFIFProvince

@ProvinceID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)
	
	DELETE T
	FROM fifupdate.Province T
	WHERE T.ProvinceID = @ProvinceID
	
	DELETE T
	FROM fifupdate.ProvinceIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM fifupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM fifupdate.ProvinceRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM fifupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.DeleteFIFProvince

--Begin procedure fifupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'fifupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to return data from the dbo.Community and fifupdate.Community tables
-- ====================================================================================================
CREATE PROCEDURE fifupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.FIFCommunityEngagement,
		C.FIFJustice,
		C.FIFPoliceEngagement,
		C.FIFUpdateNotes,
		C.IndicatorUpdate,
		C.MeetingNotes,
		C.MOUDate,
		dbo.FormatDate(C.MOUDate) AS MOUDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CommunityID,
		C1.CommunityName AS EntityName,
		C1.MOUDate,
		dbo.FormatDate(C1.MOUDate) AS MOUDateFormatted,
		C2.FIFCommunityEngagement,
		C2.FIFJustice,
		C2.FIFPoliceEngagement,
		C2.FIFUpdateNotes,
		C2.IndicatorUpdate,
		C2.MeetingNotes
	FROM fifupdate.Community C2
		JOIN dbo.Community C1 ON C1.CommunityID = C2.CommunityID
			AND C2.CommunityID = @CommunityID

	--EntityMeetingsCurrent
	SELECT
		C.AttendeeCount, 
		C.MeetingDate, 
		dbo.FormatDate(C.MeetingDate) AS MeetingDateFormatted,
		C.MeetingTitle, 
		C.RegularAttendeeCount
	FROM dbo.CommunityMeeting C
	WHERE C.CommunityID = @CommunityID

	--EntityMeetingsUpdate
	SELECT
		C.AttendeeCount, 
		C.MeetingDate, 
		dbo.FormatDate(C.MeetingDate) AS MeetingDateFormatted,
		C.MeetingTitle, 
		C.RegularAttendeeCount
	FROM fifupdate.CommunityMeeting C
	WHERE C.CommunityID = @CommunityID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'FIFCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.FIFAchievedValue,
		OACI.FIFNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.FIFAchievedValue, 
				CI.FIFNotes
			FROM fifupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.FIFRiskValue,
		OACR.FIFNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.FIFRiskValue, 
				CR.FIFNotes
			FROM fifupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure fifupdate.GetCommunityByCommunityID

--Begin procedure fifupdate.GetFIFUpdate
EXEC Utility.DropObject 'fifupdate.GetFIFUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to get data from the fifupdate.FIFUpdate table
-- ==============================================================================
CREATE PROCEDURE fifupdate.GetFIFUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FIFUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM fifupdate.FIFUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (FIFUpdateID INT)

		INSERT INTO fifupdate.FIFUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.FIFUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.FIFUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'

		SELECT @FIFUpdateID = O.FIFUpdateID FROM @tOutput O
		
		EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='create', @PersonID = @PersonID

		END
	ELSE
		SELECT @FIFUpdateID = FU.FIFUpdateID FROM fifupdate.FIFUpdate FU
	--ENDIF
	
	--FIF
	SELECT
		FU.FIFUpdateID, 
		FU.WorkflowStepNumber 
	FROM fifupdate.FIFUpdate FU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'
			JOIN fifupdate.FIFUpdate FU ON FU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowStatus
	SELECT
		'FIFUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @FIFUpdateID

	--WorkflowStepWorkflowAction
	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'FIFUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT FU.WorkflowStepNumber FROM fifupdate.FIFUpdate FU WHERE FU.FIFUpdateID = @FIFUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure fifupdate.GetFIFUpdate

--Begin procedure fifupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'fifupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to return data from the dbo.Province and fifupdate.Province tables
-- ==================================================================================================
CREATE PROCEDURE fifupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.ProvinceID,
		P.ProvinceName AS EntityName,
		P.FIFCommunityEngagement,
		P.FIFJustice,
		P.FIFPoliceEngagement,
		P.FIFUpdateNotes,
		P.IndicatorUpdate,
		P.MeetingNotes,
		P.MOUDate,
		dbo.FormatDate(P.MOUDate) AS MOUDateFormatted
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.ProvinceID,
		P1.ProvinceName AS EntityName,
		P1.MOUDate,
		dbo.FormatDate(P1.MOUDate) AS MOUDateFormatted,
		P2.FIFCommunityEngagement,
		P2.FIFJustice,
		P2.FIFPoliceEngagement,
		P2.FIFUpdateNotes,
		P2.IndicatorUpdate,
		P2.MeetingNotes
	FROM fifupdate.Province P2
		JOIN dbo.Province P1 ON P1.ProvinceID = P2.ProvinceID
			AND P2.ProvinceID = @ProvinceID

	--EntityMeetingsCurrent
	SELECT
		PM.AttendeeCount, 
		PM.MeetingDate, 
		dbo.FormatDate(PM.MeetingDate) AS MeetingDateFormatted,
		PM.MeetingTitle, 
		PM.RegularAttendeeCount
	FROM dbo.ProvinceMeeting PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityMeetingsUpdate
	SELECT
		PM.AttendeeCount, 
		PM.MeetingDate, 
		dbo.FormatDate(PM.MeetingDate) AS MeetingDateFormatted,
		PM.MeetingTitle, 
		PM.RegularAttendeeCount
	FROM fifupdate.ProvinceMeeting PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'FIFProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PI.ProvinceIndicatorID
			FROM dbo.ProvinceIndicator PI 
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OAPI.FIFAchievedValue,
		OAPI.FIFNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PI.FIFAchievedValue, 
				PI.FIFNotes
			FROM fifupdate.ProvinceIndicator PI
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.FIFRiskValue,
		OAPR.FIFNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.FIFRiskValue, 
				PR.FIFNotes
			FROM fifupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure fifupdate.GetProvinceByProvinceID

--Begin procedure reporting.GetSerialNumberTracker
EXEC Utility.DropObject 'reporting.GetSerialNumberTracker'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data from the procurement.EquipmentInventory table
-- =========================================================================================
CREATE PROCEDURE reporting.GetSerialNumberTracker

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT

		C.CommunityName,
		dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(C.CommunityID)) as ProvinceName,
		dbo.FormatConceptNoteReferenceCode(CEI.ConceptNoteID) AS ReferenceCode,
		(SELECT Title from ConceptNote where conceptnoteid  = CEI.ConceptNoteID) AS ConceptNoteTitle,
		(SELECT CNT.ConceptNoteTypeName FROM dropdown.ConceptNoteType CNT JOIN dbo.ConceptNote CN ON CN.ConceptNoteTypeID = CNT.ConceptNoteTypeID AND CN.ConceptNoteID = CEI.ConceptNoteID) AS ConceptNoteTypeName,
		EC.ItemName,
		EI.SerialNumber,
		EI.IMEIMACAddress,
		EI.Sim,
		EI.BudgetCode,
		EI.UnitCost,
		CEI.Quantity,
		EC.QuantityOfIssue,
		(EI.UnitCost * CEI.Quantity * EC.QuantityOfIssue) TotalCost,
		dbo.FormatDate(CEI.DistributionDate) AS DistributionDateFormatted,
		dbo.FormatDate(procurement.GetLastEquipmentAuditDate('Community', CEI.CommunityEquipmentInventoryID)) AS AuditDateFormatted,
		procurement.GetLastEquipmentAuditOutcome('Community', CEI.CommunityEquipmentInventoryID) AS AuditOutcome,
		OAC.City,
		OAC.Gender,
		OAC.ContactFullName,
		OAC.EmployerName,
		OAC.ContactID,
		EC.ItemMake + '/' + EC.ItemModel as MakeAndModel,
		ECC.EquipmentCatalogCategoryName
	FROM procurement.CommunityEquipmentInventory CEI
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.Community C ON C.CommunityID = CEI.CommunityID
				JOIN dropdown.EquipmentCatalogCategory ECC on ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
		JOIN reporting.SearchResult SR ON SR.EntityID = CEI.EquipmentInventoryID
			AND SR.EntityTypeCode = 'TerritoryEquipmentInventory'
			AND SR.PersonID = @PersonID
		OUTER APPLY
			(
			SELECT
				C.City,
				C.Gender,
				dbo.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactFullName,
				C.EmployerName,
				C.ContactID
			FROM
				(
				SELECT 
					ISNULL(CNCE.ContactID, 0) AS ContactID
				FROM dbo.ConceptNoteContactEquipment CNCE 
				WHERE CNCE.ConceptNoteID = CEI.ConceptNoteID 
					AND CNCE.Quantity = CEI.Quantity 
					AND CNCE.EquipmentInventoryID = CEI.EquipmentInventoryID 
					AND CNCE.TerritoryTypeCode = 'Community' 
					AND CNCE.TerritoryID = CEI.CommunityID
				) D
				JOIN dbo.Contact C ON C.ContactID = D.ContactID
			) OAC

	UNION

	SELECT 
		'' as CommunityName,
		P.ProvinceName,
		dbo.FormatConceptNoteReferenceCode(PEI.ConceptNoteID) AS ReferenceCode,
		(SELECT CNT.ConceptNoteTypeName FROM dropdown.ConceptNoteType CNT JOIN dbo.ConceptNote CN ON CN.ConceptNoteTypeID = CNT.ConceptNoteTypeID AND CN.ConceptNoteID = PEI.ConceptNoteID) AS ConceptNoteTypeName,
		(SELECT Title from ConceptNote where conceptnoteid  = PEI.ConceptNoteID) AS ConceptNoteTitle,
		EC.ItemName,
		EI.SerialNumber,
		EI.IMEIMACAddress,
		EI.Sim,
		EI.BudgetCode,
		EI.UnitCost,
		PEI.Quantity,
		EC.QuantityOfIssue,
		(EI.UnitCost * PEI.Quantity * EC.QuantityOfIssue) TotalCost,
		dbo.FormatDate(PEI.DistributionDate) AS DistributionDateFormatted,
		dbo.FormatDate(procurement.GetLastEquipmentAuditDate('Province', PEI.ProvinceEquipmentInventoryID)) AS AuditDateFormatted,
		procurement.GetLastEquipmentAuditOutcome('Province', PEI.ProvinceEquipmentInventoryID) AS AuditOutcome,
		OAC.City,
		OAC.Gender,
		OAC.ContactFullName,
		OAC.EmployerName,
		OAC.ContactID,
		EC.ItemMake + '/' + EC.ItemModel as MakeAndModel,
		ECC.EquipmentCatalogCategoryName
	FROM procurement.ProvinceEquipmentInventory PEI
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = PEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dbo.Province P ON P.ProvinceID = PEI.ProvinceID
		JOIN dropdown.EquipmentCatalogCategory ECC on ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
		JOIN reporting.SearchResult SR ON SR.EntityID = PEI.EquipmentInventoryID
			AND SR.EntityTypeCode = 'TerritoryEquipmentInventory'
			AND SR.PersonID = @PersonID
		OUTER APPLY
			(
			SELECT
				C.City,
				C.Gender,
				dbo.FormatContactNameByContactID(C.ContactID, 'LastFirst') AS ContactFullName,
				C.EmployerName,
				C.ContactID
			FROM
				(
				SELECT 
					ISNULL(CNCE.ContactID, 0) AS ContactID
				FROM dbo.ConceptNoteContactEquipment CNCE 
				WHERE CNCE.ConceptNoteID = PEI.ConceptNoteID 
					AND CNCE.Quantity = PEI.Quantity 
					AND CNCE.EquipmentInventoryID = PEI.EquipmentInventoryID 
					AND CNCE.TerritoryTypeCode = 'Province' 
					AND CNCE.TerritoryID = PEI.ProvinceID
				) D
				JOIN dbo.Contact C ON C.ContactID = D.ContactID
			) OAC

END
GO
--End procedure reporting.GetSerialNumberTracker

--Begin procedure workflow.GetFIFUpdateWorkflowData
EXEC Utility.DropObject 'workflow.GetFIFUpdateWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A procedure to return workflow data from the fifupdate.FIFUpdate table
-- ===================================================================================

CREATE PROCEDURE workflow.GetFIFUpdateWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'
			JOIN fifupdate.FIFUpdate FU ON FU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND FU.FIFUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'FIFUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT RU.WorkflowStepNumber FROM fifupdate.FIFUpdate RU WHERE RU.FIFUpdateID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'FIFUpdate'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created FIF Updates'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected FIF Updates'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved FIF Updates'
			WHEN EL.EventCode = 'update'
			THEN 'Updated FIF Updates'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'FIFUpdate'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetFIFUpdateWorkflowData

--Begin procedure workflow.GetFIFUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetFIFUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to people associated with the workflow steps on a fif update
-- ============================================================================================
CREATE PROCEDURE workflow.GetFIFUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT FU.WorkflowStepNumber FROM fifupdate.FIFUpdate FU WHERE FU.FIFUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT FU.WorkflowStepNumber FROM fifupdate.FIFUpdate FU WHERE FU.FIFUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'FIFUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetFIFUpdateWorkflowStepPeople

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EntityType
--End table dbo.EntityType

--Begin table dbo.MenuItem
--End table dbo.MenuItem

--Begin table dropdown.Component
DELETE C
FROM dropdown.Component C
WHERE C.ComponentAbbreviation = 'RAP'
GO
--End table dropdown.Component

--Begin table dropdown.ConceptNoteType
IF NOT EXISTS (SELECT 1 FROM dropdown.ConceptNoteType C WHERE C.ConceptNoteTypeName = 'Rapid Assessment Program')
	BEGIN
	
	INSERT INTO dropdown.ConceptNoteType
		(ConceptNoteTypeName,DisplayOrder)
	VALUES
		('Rapid Assessment Program', (SELECT MAX(C.DisplayOrder) + 1 FROM dropdown.ConceptNoteType C))
		
	END
--ENDIF
GO
--End table dropdown.ConceptNoteType

EXEC utility.ServerSetupKeyAddUpdate 'ShowAtmosphericReportsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowCommunitiesOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowFindingsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowIncidentReportsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowLogicalFrameworkOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowProvincesOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowRecommendationsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowRequestsForInformationOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowRisksOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowSpotReportsOnCSSFPortal', '0'
GO

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.43 File 01 - AJACS - 2016.01.04 21.36.04')
GO
--End build tracking

