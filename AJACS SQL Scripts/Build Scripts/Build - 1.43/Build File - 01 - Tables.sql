USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'FIFCommunityEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFJustice', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFPoliceEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFUpdateNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'IndicatorUpdate', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MeetingNotes', 'VARCHAR(MAX)'
GO
--End table dbo.Community

--Begin table dbo.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityIndicator'

EXEC utility.AddColumn @TableName, 'FIFAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
GO
--End table dbo.CommunityIndicator

--Begin table dbo.CommunityMeeting
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityMeeting
	(
	CommunityMeetingID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityMeetingID'
EXEC utility.SetIndexClustered 'IX_CommunityMeeting', @TableName, 'CommunityID,MeetingDate DESC'
GO
--End table dbo.CommunityMeeting

--Begin table dbo.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRisk'

EXEC utility.AddColumn @TableName, 'FIFRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
GO
--End table dbo.CommunityRisk

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'FIFCommunityEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFJustice', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFPoliceEngagement', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'FIFUpdateNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'IndicatorUpdate', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'MeetingNotes', 'VARCHAR(MAX)'
GO
--End table dbo.Province

--Begin table dbo.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceIndicator'

EXEC utility.AddColumn @TableName, 'FIFAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
GO
--End table dbo.ProvinceIndicator

--Begin table dbo.ProvinceMeeting
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceMeeting
	(
	ProvinceMeetingID INT IDENTITY(1,1) NOT NULL,
	ProvinceID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceMeetingID'
EXEC utility.SetIndexClustered 'IX_ProvinceMeeting', @TableName, 'ProvinceID,MeetingDate DESC'
GO
--End table dbo.ProvinceMeeting

--Begin table dbo.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceRisk'

EXEC utility.AddColumn @TableName, 'FIFRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'FIFNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
GO
--End table dbo.ProvinceRisk

--Begin table dbo.Risk
EXEC utility.AddColumn 'dbo.Risk', 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint 'dbo.Risk', 'IsActive', 'BIT', 1
GO
--End table dbo.Risk

--Begin table dbo.SpotReport
EXEC utility.DropColumn 'dbo.SpotReport', 'CommunityID'
EXEC utility.DropColumn 'dbo.SpotReport', 'ProvinceID'
GO
--End table dbo.SpotReport

--Begin table fifupdate.Community
DECLARE @TableName VARCHAR(250) = 'fifupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.Community
	(
	CommunityID INT,
	FIFUpdateID INT,
	FIFCommunityEngagement VARCHAR(MAX),
	FIFJustice VARCHAR(MAX),
	FIFPoliceEngagement VARCHAR(MAX),
	FIFUpdateNotes VARCHAR(MAX),
	IndicatorUpdate VARCHAR(MAX),
	MeetingNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table fifupdate.Community

--Begin table fifupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'fifupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	FIFAchievedValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'CommunityID,IndicatorID'
GO
--End table fifupdate.CommunityIndicator

--Begin table fifupdate.CommunityMeeting
DECLARE @TableName VARCHAR(250) = 'fifupdate.CommunityMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.CommunityMeeting
	(
	CommunityMeetingID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	CommunityID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityMeetingID'
EXEC utility.SetIndexClustered 'IX_CommunityMeeting', @TableName, 'CommunityID,MeetingDate DESC'
GO
--End table fifupdate.CommunityMeeting

--Begin table fifupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'fifupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	CommunityID INT,
	RiskID INT,
	FIFRiskValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'CommunityID,RiskID'
GO
--End table fifupdate.CommunityRisk

--Begin table fifupdate.FIFUpdate
DECLARE @TableName VARCHAR(250) = 'fifupdate.FIFUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.FIFUpdate
	(
	FIFUpdateID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'FIFUpdateID'
GO
--End table fifupdate.FIFUpdate

--Begin table fifupdate.Province
DECLARE @TableName VARCHAR(250) = 'fifupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.Province
	(
	ProvinceID INT,
	FIFUpdateID INT,
	FIFCommunityEngagement VARCHAR(MAX),
	FIFJustice VARCHAR(MAX),
	FIFPoliceEngagement VARCHAR(MAX),
	FIFUpdateNotes VARCHAR(MAX),
	IndicatorUpdate VARCHAR(MAX),
	MeetingNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table fifupdate.Province

--Begin table fifupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'fifupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	FIFAchievedValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'ProvinceID,IndicatorID'
GO
--End table fifupdate.ProvinceIndicator

--Begin table fifupdate.ProvinceMeeting
DECLARE @TableName VARCHAR(250) = 'fifupdate.ProvinceMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.ProvinceMeeting
	(
	ProvinceMeetingID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	ProvinceID INT,
	MeetingDate DATE,
	MeetingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceMeetingID'
EXEC utility.SetIndexClustered 'IX_ProvinceMeeting', @TableName, 'ProvinceID,MeetingDate DESC'
GO
--End table fifupdate.ProvinceMeeting

--Begin table fifupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'fifupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE fifupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1) NOT NULL,
	FIFUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	FIFRiskValue INT,
	FIFNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'FIFUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'ProvinceID,RiskID'
GO
--End table fifupdate.ProvinceRisk
