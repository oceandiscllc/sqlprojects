USE AJACS
GO

--Begin schema fifupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'fifupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA fifupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema fifupdate