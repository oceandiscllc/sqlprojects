USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForCommunityExport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForCommunityExport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			John Lyons
-- Create date:	2015.12.02
-- Description:	A function to return communities assets for placement on a google map
-- ==================================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForCommunityExport
(
@CommunityID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	DECLARE @CommunityMarker VARCHAR(MAX)=''

	SELECT
		@CommunityMarker += '&markers=icon:' 
		+ replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + REPLACE(ID.HexColor, '#', '') + '.png' + '|' 
		+ CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.CommunityID = @CommunityID
	
	SET @Gresult = @CommunityMarker

	SELECT
		@GResult += '&markers=icon:' 
		+replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/' + AT.Icon + '|' 
		+ CAST(ISNULL(CA.Location.STY ,0) AS VARCHAR(MAX)) 
		+ ','
		+ CAST(ISNULL(CA.Location.STX,0) AS VARCHAR(MAX))
	FROM dbo.Community C 
		JOIN dbo.communityAsset CA ON CA.CommunityID = C.communityID  
			AND C.CommunityID = @CommunityID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID 
			AND AT.AssetTypeID != 0

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForCommunityExport

--Begin function eventlog.GetCommunityXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return Community data for a specific FIFUpdate record
-- ================================================================================

CREATE FUNCTION eventlog.GetCommunityXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM fifupdate.Community T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByFIFUpdateID

--Begin function eventlog.GetCommunityIndicatorXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return CommunityIndicator data for a specific FIFUpdate record
-- =========================================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM fifupdate.CommunityIndicator T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'

END
GO
--End function eventlog.GetCommunityIndicatorXMLByFIFUpdateID

--Begin function eventlog.GetCommunityMeetingXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityMeetingXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return CommunityMeeting data for a specific FIFUpdate record
-- =======================================================================================

CREATE FUNCTION eventlog.GetCommunityMeetingXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityMeetings VARCHAR(MAX) = ''
	
	SELECT @cCommunityMeetings = COALESCE(@cCommunityMeetings, '') + D.CommunityMeeting
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityMeeting'), ELEMENTS) AS CommunityMeeting
		FROM fifupdate.CommunityMeeting T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<CommunityMeetings>' + ISNULL(@cCommunityMeetings, '') + '</CommunityMeetings>'

END
GO
--End function eventlog.GetCommunityMeetingXMLByFIFUpdateID

--Begin function eventlog.GetCommunityRiskXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return CommunityRisk data for a specific FIFUpdate record
-- ====================================================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM fifupdate.CommunityRisk T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'

END
GO
--End function eventlog.GetCommunityRiskXMLByFIFUpdateID

--Begin function eventlog.GetProvinceXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return Province data for a specific FIFUpdate record
-- ================================================================================

CREATE FUNCTION eventlog.GetProvinceXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM fifupdate.Province T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByFIFUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return ProvinceIndicator data for a specific FIFUpdate record
-- ========================================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM fifupdate.ProvinceIndicator T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'

END
GO
--End function eventlog.GetProvinceIndicatorXMLByFIFUpdateID

--Begin function eventlog.GetProvinceMeetingXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceMeetingXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return ProvinceMeeting data for a specific FIFUpdate record
-- =======================================================================================

CREATE FUNCTION eventlog.GetProvinceMeetingXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceMeetings VARCHAR(MAX) = ''
	
	SELECT @cProvinceMeetings = COALESCE(@cProvinceMeetings, '') + D.ProvinceMeeting
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceMeeting'), ELEMENTS) AS ProvinceMeeting
		FROM fifupdate.ProvinceMeeting T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<ProvinceMeetings>' + ISNULL(@cProvinceMeetings, '') + '</ProvinceMeetings>'

END
GO
--End function eventlog.GetProvinceMeetingXMLByFIFUpdateID

--Begin function eventlog.GetProvinceRiskXMLByFIFUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByFIFUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A function to return ProvinceRisk data for a specific FIFUpdate record
-- ===================================================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByFIFUpdateID
(
@FIFUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM fifupdate.ProvinceRisk T 
		WHERE T.FIFUpdateID = @FIFUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'

END
GO
--End function eventlog.GetProvinceRiskXMLByFIFUpdateID

--Begin function permissionable.HasPartialPermission
EXEC utility.DropObject 'permissionable.HasPartialPermission'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.29
-- Description:	A function to determine if a PeronID has a partial permission
-- ==========================================================================

CREATE FUNCTION permissionable.HasPartialPermission
(
@PermissionableLineage VARCHAR(MAX),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nHasPermission BIT = 0

	IF EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP WHERE PP.PermissionableLineage LIKE @PermissionableLineage + '%' AND PP.PersonID = @PersonID)
		SET @nHasPermission = 1
	--ENDIF
	
	RETURN @nHasPermission

END
GO
--End function permissionable.HasPartialPermission

--Begin procedure utility.ServerSetupKeyAddUpdate
EXEC Utility.DropObject 'utility.ServerSetupKeyAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.01
-- Description:	A stored procedure to add / update server setup key records
--
-- Author:			Todd Pires
-- Update Date:	2015.07.01
-- Description:	Implemented the utility table synonym
-- ========================================================================
CREATE PROCEDURE utility.ServerSetupKeyAddUpdate

@ServerSetupKey VARCHAR(250),
@ServerSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM dbo.ServerSetup SS WHERE SS.ServerSetupKey = @ServerSetupKey)
		BEGIN

		INSERT INTO dbo.ServerSetup
			(ServerSetupKey,ServerSetupValue)
		VALUES
			(@ServerSetupKey,@ServerSetupValue)

		END
	ELSE
		BEGIN

		UPDATE dbo.ServerSetup
		SET ServerSetupValue = @ServerSetupValue
		WHERE ServerSetupKey = @ServerSetupKey

		END
	--ENDIF
	
END
GO
--End procedure utility.ServerSetupKeyAddUpdate