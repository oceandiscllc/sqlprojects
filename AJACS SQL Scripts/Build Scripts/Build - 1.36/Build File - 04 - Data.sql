USE AJACS
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'WeeklySummaryReport')
	BEGIN
	
	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode, DocumentTypeName, DocumentGroupID, DocumentTypePermissionCode)
	SELECT
		'WeeklySummaryReport',
		'Weekly Atmospheric Summary Report',
		DG.DocumentGroupID,
		519
	FROM dropdown.DocumentGroup DG
	WHERE DG.DocumentGroupCode = '500-599'

	END
--ENDIF
GO