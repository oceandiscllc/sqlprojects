USE AJACS
GO

--Begin procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A procedure to approve a Community/Province Engagement Update
-- ==========================================================================
CREATE PROCEDURE communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nCommunityProvinceEngagementUpdateID INT = ISNULL((SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC), 0)
	DECLARE @nContactAffiliationID INT = ISNULL((SELECT CA.ContactAffiliationID FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Community Security Working Groups'), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.CAPAgreedDate = CPEU.CAPAgreedDate,
		P.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		P.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		P.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN communityprovinceengagementupdate.Province CPEU ON CPEU.ProvinceID = P.ProvinceID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputProvince O ON O.ProvinceID = C.ProvinceID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		PC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.ProvinceContact PC

	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.CommunityProvinceEngagementAchievedValue, 
		PRI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceIndicator PRI

	DELETE PC
	FROM project.ProjectProvince PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO project.ProjectProvince
		(ProvinceID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.ProvinceID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceProject CP

	DELETE RP
	FROM recommendation.RecommendationProvince RP
		JOIN @tOutputProvince O ON O.ProvinceID = RP.ProvinceID

	INSERT INTO recommendation.RecommendationProvince
		(ProvinceID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RecommendationID,
		PR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceRecommendation PR

	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.CommunityProvinceEngagementRiskValue, 
		PR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Province'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementProvince'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')

	UPDATE C
	SET
		C.CAPAgreedDate = CPEU.CAPAgreedDate,
		C.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		C.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		C.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN communityprovinceengagementupdate.Community CPEU ON CPEU.CommunityID = C.CommunityID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputCommunity O ON O.CommunityID = C.CommunityID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		CC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.CommunityContact CC

	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.CommunityProvinceEngagementAchievedValue, 
		CI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityIndicator CI

	DELETE PC
	FROM project.ProjectCommunity PC
		JOIN @tOutputCommunity O ON O.CommunityID = PC.CommunityID
	
	INSERT INTO project.ProjectCommunity
		(CommunityID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.CommunityID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityProject CP

	DELETE RC
	FROM recommendation.RecommendationCommunity RC
		JOIN @tOutputCommunity O ON O.CommunityID = RC.CommunityID
	
	INSERT INTO recommendation.RecommendationCommunity
		(CommunityID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RecommendationID,
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRecommendation CR

	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.CommunityProvinceEngagementRiskValue, 
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Community'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementCommunity'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')
	
	DELETE FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

	TRUNCATE TABLE communityprovinceengagementupdate.Province
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceContact
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceProject
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRisk

	TRUNCATE TABLE communityprovinceengagementupdate.Community
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityContact
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityProject
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRisk

END

GO
--End procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Community table
-- ====================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)
	
	INSERT INTO communityprovinceengagementupdate.Community
		(CommunityID, CommunityProvinceEngagementUpdateID, CommunityEngagementOutput1, CommunityEngagementOutput2, CommunityEngagementOutput3, CommunityEngagementOutput4, CAPAgreedDate, LastNeedsAssessmentDate, TORMOUStatusID, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@CommunityProvinceEngagementUpdateID,
		C1.CommunityEngagementOutput1, 
		C1.CommunityEngagementOutput2, 
		C1.CommunityEngagementOutput3, 
		C1.CommunityEngagementOutput4, 
		C1.CAPAgreedDate, 
		C1.LastNeedsAssessmentDate, 
		C1.TORMOUStatusID,
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM communityprovinceengagementupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO communityprovinceengagementupdate.CommunityContact
		(CommunityProvinceEngagementUpdateID, CommunityID, ContactID)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		C.CommunityID,
		C.ContactID
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND C.IsActive = 1
			AND C.IsValid = 1
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
						AND CCT.ContactID = C.ContactID
				)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactAffiliation CCA
					JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
						AND CA.ContactAffiliationName = 'Community Security Working Groups'
						AND CCA.ContactID = C.ContactID
				)

	INSERT INTO communityprovinceengagementupdate.CommunityIndicator
		(CommunityProvinceEngagementUpdateID, CommunityID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.CommunityProvinceEngagementAchievedValue, 0),
		OACI.CommunityProvinceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityProvinceEngagementAchievedValue,
				CI.CommunityProvinceEngagementNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO communityprovinceengagementupdate.CommunityProject
		(CommunityProvinceEngagementUpdateID, CommunityID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		PC.CommunityID, 
		PC.ProjectID, 
		PC.CommunityProvinceEngagementNotes
	FROM project.ProjectCommunity PC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PC.CommunityID
	
	INSERT INTO communityprovinceengagementupdate.CommunityRecommendation
		(CommunityProvinceEngagementUpdateID, CommunityID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RC.CommunityID, 
		RC.RecommendationID, 
		RC.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
	
	INSERT INTO communityprovinceengagementupdate.CommunityRisk
		(CommunityProvinceEngagementUpdateID, CommunityID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RC.CommunityID,
		R.RiskID,
		ISNULL(OACR.CommunityProvinceEngagementRiskValue, 0),
		OACR.CommunityProvinceEngagementNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementRiskValue, 
				CR.CommunityProvinceEngagementNotes
			FROM dbo.CommunityRisk CR
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CR.CommunityID
					AND CR.RiskID = R.RiskID
			) OACR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'CommunityProvinceEngagementCommunity', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Province table
-- ===================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)

	INSERT INTO communityprovinceengagementupdate.Province
		(ProvinceID, CommunityProvinceEngagementUpdateID, CommunityEngagementOutput1, CommunityEngagementOutput2, CommunityEngagementOutput3, CommunityEngagementOutput4, CAPAgreedDate, LastNeedsAssessmentDate, TORMOUStatusID, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@CommunityProvinceEngagementUpdateID,
		P1.CommunityEngagementOutput1, 
		P1.CommunityEngagementOutput2, 
		P1.CommunityEngagementOutput3, 
		P1.CommunityEngagementOutput4, 
		P1.CAPAgreedDate, 
		P1.LastNeedsAssessmentDate, 
		P1.TORMOUStatusID,
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM communityprovinceengagementupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO communityprovinceengagementupdate.ProvinceContact
		(CommunityProvinceEngagementUpdateID, ProvinceID, ContactID)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		C.ProvinceID,
		C.ContactID
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ProvinceID
			AND C.IsActive = 1
			AND C.IsValid = 1
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
						AND CCT.ContactID = C.ContactID
				)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactAffiliation CCA
					JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
						AND CA.ContactAffiliationName = 'Community Security Working Groups'
						AND CCA.ContactID = C.ContactID
				)

	INSERT INTO communityprovinceengagementupdate.ProvinceIndicator
		(CommunityProvinceEngagementUpdateID, ProvinceID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.CommunityProvinceEngagementAchievedValue, 0),
		OAPI.CommunityProvinceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.CommunityProvinceEngagementAchievedValue,
				PRI.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI

	INSERT INTO communityprovinceengagementupdate.ProvinceProject
		(CommunityProvinceEngagementUpdateID, ProvinceID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		PP.ProvinceID, 
		PP.ProjectID, 
		PP.CommunityProvinceEngagementNotes
	FROM project.ProjectProvince PP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.ProvinceID
	
	INSERT INTO communityprovinceengagementupdate.ProvinceRecommendation
		(CommunityProvinceEngagementUpdateID, ProvinceID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RP.ProvinceID, 
		RP.RecommendationID, 
		RP.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
	
	INSERT INTO communityprovinceengagementupdate.ProvinceRisk
		(CommunityProvinceEngagementUpdateID, ProvinceID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RP.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.CommunityProvinceEngagementRiskValue, 0),
		OAPR.CommunityProvinceEngagementNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceRisk PR
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PR.ProvinceID
					AND PR.RiskID = R.RiskID
			) OAPR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'CommunityProvinceEngagementProvince', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('CAP Document','Needs Assesment Document','TOR/MOU Document')

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

--Begin procedure eventlog.LogCommunityAction
EXEC utility.DropObject 'eventlog.LogCommunityAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:			Todd Pires
-- Create date: 2015.09.25
-- Description:	Modified to support the Geography data type
--
-- Author:			Todd Pires
-- Create date: 2015.11.01
-- Description:	Added recommendations
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytActionTable
		--ENDIF
		
		SELECT *
		INTO #LogCommuniytActionTable
		FROM dbo.Community C
		WHERE C.CommunityID = @EntityID
		
		ALTER TABLE #LogCommuniytActionTable DROP COLUMN Location
			
		DECLARE @cCommunityRecommendation VARCHAR(MAX) 
	
		SELECT 
			@cCommunityRecommendation = COALESCE(@cCommunityRecommendation, '') + D.CommunityRecommendation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityRecommendation'), ELEMENTS) AS CommunityRecommendation
			FROM recommendation.RecommendationCommunity T 
			WHERE T.CommunityID = @EntityID
			) D	
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			'<Location>' + CAST(C.Location AS VARCHAR(MAX)) + '</Location>',
			(SELECT CAST(eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID(T.CommunityID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByCommunityID(T.CommunityID) AS XML)),
			CAST(('<CommunityRecommendations>' + ISNULL(@cCommunityRecommendation , '') + '</CommunityRecommendations>') AS XML), 
			(SELECT CAST(eventlog.GetCommunityRiskXMLByCommunityID(T.CommunityID) AS XML))
			FOR XML RAW('Community'), ELEMENTS
			)
		FROM #LogCommuniytActionTable T
			JOIN dbo.Community C ON C.CommunityID = T.CommunityID

		DROP TABLE #LogCommuniytActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAction

--Begin procedure eventlog.LogCommunityAssetAction
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.09.25
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytActionTable
		--ENDIF
		
		SELECT *
		INTO #LogCommuniytAssetActionTable
		FROM dbo.CommunityAsset CA
		WHERE CA.CommunityAssetID = @EntityID
		
		ALTER TABLE #LogCommuniytActionTable DROP COLUMN Location

		DECLARE @cCommunityAssetCommunities NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetCommunities = COALESCE(@cCommunityAssetCommunities, '') + D.CommunityAssetCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetCommunity'), ELEMENTS) AS CommunityAssetCommunity
			FROM dbo.CommunityAssetCommunity T 
			WHERE T.CommunityAssetID = @EntityID
			) D
				
		DECLARE @cCommunityAssetProvinces NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetProvinces = COALESCE(@cCommunityAssetProvinces, '') + D.CommunityAssetProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetProvince'), ELEMENTS) AS CommunityAssetProvince
			FROM dbo.CommunityAssetProvince T 
			WHERE T.CommunityAssetID = @EntityID
			) D			

		DECLARE @cCommunityAssetRisks NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetRisks = COALESCE(@cCommunityAssetRisks, '') + D.CommunityAssetRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetRisk'), ELEMENTS) AS CommunityAssetRisk
			FROM dbo.CommunityAssetRisk T 
			WHERE T.CommunityAssetID = @EntityID
			) D	

		DECLARE @cCommunityAssetUnits NVARCHAR(MAX)

		SELECT 
			@cCommunityAssetUnits = COALESCE(@cCommunityAssetUnits, '') + D.CommunityAssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetUnit'), ELEMENTS) AS CommunityAssetUnit
			FROM dbo.CommunityAssetUnit T 
			WHERE T.CommunityAssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			'<Location>' + CAST(CA.Location AS VARCHAR(MAX)) + '</Location>',
			CAST(('<CommunityAssetCommunities>' + ISNULL(@cCommunityAssetCommunities , '') + '</CommunityAssetCommunities>') AS XML), 
			CAST(('<CommunityAssetProvinces>' + ISNULL(@cCommunityAssetProvinces , '') + '</CommunityAssetProvinces>') AS XML),
			CAST(('<CommunityAssetRisks>' + ISNULL(@cCommunityAssetRisks  , '') + '</CommunityAssetRisks>') AS XML),
			CAST(('<CommunityAssetUnits>' + ISNULL(@cCommunityAssetUnits  , '') + '</CommunityAssetUnits>') AS XML)
			FOR XML RAW('CommunityAsset'), ELEMENTS
			)
		FROM #LogCommuniytAssetActionTable T
			JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = T.CommunityAssetID

		DROP TABLE #LogCommuniytAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAssetAction

--Begin procedure eventlog.LogIncidentAction
EXEC utility.DropObject 'eventlog.LogIncidentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.04.18
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.11.01
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIncidentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogIncidentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogIncidentActionTable
		--ENDIF
		
		SELECT *
		INTO #LogIncidentActionTable
		FROM dbo.Incident I
		WHERE I.IncidentID = @EntityID
		
		ALTER TABLE #LogIncidentActionTable DROP COLUMN Location

		DECLARE @cIncidentCommunities VARCHAR(MAX) 
	
		SELECT 
			@cIncidentCommunities = COALESCE(@cIncidentCommunities, '') + D.IncidentCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentCommunity'), ELEMENTS) AS IncidentCommunity
			FROM dbo.IncidentCommunity T 
			WHERE T.IncidentID = @EntityID
			) D

		DECLARE @cIncidentProvinces VARCHAR(MAX) 
	
		SELECT 
			@cIncidentProvinces = COALESCE(@cIncidentProvinces, '') + D.IncidentProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentProvince'), ELEMENTS) AS IncidentProvince
			FROM dbo.IncidentProvince T 
			WHERE T.IncidentID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments,
			(
			SELECT
			T.*, 
			'<Location>' + CAST(I.Location AS VARCHAR(MAX)) + '</Location>',
			CAST(('<IncidentCommunities>' + ISNULL(@cIncidentCommunities, '') + '</IncidentCommunities>') AS XML),
			CAST(('<IncidentProvinces>' + ISNULL(@cIncidentProvinces, '') + '</IncidentProvinces>') AS XML)
			FOR XML RAW('Incident'), ELEMENTS
			)

		FROM #LogIncidentActionTable T
			JOIN dbo.Incident I ON I.IncidentID = T.IncidentID

		DROP TABLE #LogIncidentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIncidentAction

--Begin procedure eventlog.LogIndicatorAction
EXEC utility.DropObject 'eventlog.LogIndicatorAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.11.01
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIndicatorAction
	@EntityID INT,
	@EventCode VARCHAR(50),
	@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID
			)

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Indicator',
			@EntityID,
			(
			SELECT T.*
			FOR XML RAW('Indicator'), ELEMENTS
			)
		FROM logicalframework.Indicator T
		WHERE T.IndicatorID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIndicatorAction

--Begin procedure eventlog.LogProvinceAction
EXEC utility.DropObject 'eventlog.LogProvinceAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProvinceAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Province',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cProvinceIndicator VARCHAR(MAX) 
	
		SELECT 
			@cProvinceIndicator = COALESCE(@cProvinceIndicator, '') + D.ProvinceIndicator
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
			FROM dbo.ProvinceIndicator T 
			WHERE T.ProvinceID = @EntityID
			) D	
			
		DECLARE @cProvinceRecommendation VARCHAR(MAX) 
	
		SELECT 
			@cProvinceRecommendation = COALESCE(@cProvinceRecommendation, '') + D.ProvinceRecommendation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProvinceRecommendation'), ELEMENTS) AS ProvinceRecommendation
			FROM recommendation.RecommendationProvince T 
			WHERE T.ProvinceID = @EntityID
			) D		
			
		DECLARE @cProvinceRisk VARCHAR(MAX) 
	
		SELECT 
			@cProvinceRisk = COALESCE(@cProvinceRisk, '') + D.ProvinceRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
			FROM dbo.ProvinceRisk T 
			WHERE T.ProvinceID = @EntityID
			) D	

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Province',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<ProvinceIndicators>' + ISNULL(@cProvinceIndicator , '') + '</ProvinceIndicators>') AS XML), 
			CAST(('<ProvinceRecommendations>' + ISNULL(@cProvinceRecommendation , '') + '</ProvinceRecommendations>') AS XML), 
			CAST(('<ProvinceRisks>' + ISNULL(@cProvinceRisk  , '') + '</ProvinceRisks>') AS XML)
			FOR XML RAW('Province'), ELEMENTS
			)
		FROM dbo.Province T
		WHERE T.ProvinceID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProvinceAction

--Begin procedure eventlog.LogWeeklyReportAction
EXEC utility.DropObject 'eventlog.LogWeeklyReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:			Todd Pires
-- Create date: 2015.11.01
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWeeklyReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'WeeklyReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogWeeklyReportTable', 'u')) IS NOT NULL
			DROP TABLE #LogWeeklyReportTable
		--ENDIF
		
		SELECT *
		INTO #LogWeeklyReportTable
		FROM weeklyreport.WeeklyReport WR
		WHERE WR.WeeklyReportID = @EntityID
		
		ALTER TABLE #LogWeeklyReportTable DROP COLUMN SummaryMap

		DECLARE @cWeeklyReportCommunities VARCHAR(MAX) 
	
		SELECT 
			@cWeeklyReportCommunities = COALESCE(@cWeeklyReportCommunities, '') + D.WeeklyReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WeeklyReportCommunity'), ELEMENTS) AS WeeklyReportCommunity
			FROM weeklyreport.Community T 
			WHERE T.WeeklyReportID = @EntityID
			) D

		DECLARE @cWeeklyReportProvinces VARCHAR(MAX) 
	
		SELECT 
			@cWeeklyReportProvinces = COALESCE(@cWeeklyReportProvinces, '') + D.WeeklyReportProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WeeklyReportProvince'), ELEMENTS) AS WeeklyReportProvince
			FROM weeklyreport.Province T 
			WHERE T.WeeklyReportID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'WeeklyReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<WeeklyReportProvinces>' + ISNULL(@cWeeklyReportProvinces, '') + '</WeeklyReportProvinces>') AS XML),
			CAST(('<WeeklyReportCommunities>' + ISNULL(@cWeeklyReportCommunities, '') + '</WeeklyReportCommunities>') AS XML)
			FOR XML RAW('WeeklyReport'), ELEMENTS
			)
		FROM #LogWeeklyReportTable T
			JOIN weeklyreport.WeeklyReport WR ON WR.WeeklyReportID = T.WeeklyReportID

		DROP TABLE #LogWeeklyReportTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWeeklyReportAction

--Begin procedure policeengagementupdate.AddPoliceEngagementCommunity
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Community table
-- =========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Community
		(CommunityID, PoliceEngagementUpdateID, UpdatePersonID, CapacityAssessmentDate, PPPDate, PoliceEngagementOutput1, MaterialSupportStatus)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID,
		C1.CapacityAssessmentDate, 
		C1.PPPDate, 
		C1.PoliceEngagementOutput1, 
		C1.MaterialSupportStatus
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM policeengagementupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO policeengagementupdate.CommunityClass
		(PoliceEngagementUpdateID, CommunityID, ClassID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OACC.PoliceEngagementNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
					JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNC2.CommunityID
						AND CNC2.ConceptNoteID = CNC1.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('PEO2','PEO3')
				)
			OUTER APPLY
				(
				SELECT
					CC.PoliceEngagementNotes
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = CAST(LTT.ListItem AS INT)
				) OACC

	INSERT INTO policeengagementupdate.CommunityIndicator
		(PoliceEngagementUpdateID, CommunityID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.PoliceEngagementAchievedValue, 0),
		OACI.PoliceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.PoliceEngagementAchievedValue,
				CI.PoliceEngagementNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI
	
	INSERT INTO policeengagementupdate.CommunityRecommendation
		(PoliceEngagementUpdateID, CommunityID, RecommendationID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RC.CommunityID, 
		RC.RecommendationID, 
		RC.PoliceEngagementNotes
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
	
	INSERT INTO policeengagementupdate.CommunityRisk
		(PoliceEngagementUpdateID, CommunityID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RC.CommunityID,
		R.RiskID,
		ISNULL(OACR.PoliceEngagementRiskValue, 0),
		OACR.PoliceEngagementNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
		OUTER APPLY
			(
			SELECT
				CR.PoliceEngagementRiskValue, 
				CR.PoliceEngagementNotes
			FROM dbo.CommunityRisk CR
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CR.CommunityID
					AND CR.RiskID = R.RiskID
			) OACR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'PoliceEngagementCommunity', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementCommunity

--Begin procedure policeengagementupdate.AddPoliceEngagementProvince
EXEC Utility.DropObject 'policeengagementupdate.AddPoliceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the policeengagementupdate.Province table
-- ========================================================================================
CREATE PROCEDURE policeengagementupdate.AddPoliceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PoliceEngagementUpdateID INT = (SELECT TOP 1 PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU ORDER BY PEU.PoliceEngagementUpdateID DESC)
	
	INSERT INTO policeengagementupdate.Province
		(ProvinceID, PoliceEngagementUpdateID, UpdatePersonID, CapacityAssessmentDate, PPPDate, PoliceEngagementOutput1, MaterialSupportStatus)
	SELECT
		CAST(LTT.ListItem AS INT),
		@PoliceEngagementUpdateID,
		@PersonID,
		P1.CapacityAssessmentDate, 
		P1.PPPDate, 
		P1.PoliceEngagementOutput1, 
		P1.MaterialSupportStatus
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM policeengagementupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO policeengagementupdate.ProvinceClass
		(PoliceEngagementUpdateID, ProvinceID, ClassID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OAPC.PoliceEngagementNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
					JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNP.ProvinceID
						AND CNP.ConceptNoteID = CNC.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('PEO2','PEO3')
				)
			OUTER APPLY
				(
				SELECT
					PC.PoliceEngagementNotes
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = CAST(LTT.ListItem AS INT)
				) OAPC

	INSERT INTO policeengagementupdate.ProvinceIndicator
		(PoliceEngagementUpdateID, ProvinceID, IndicatorID, PoliceEngagementAchievedValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.PoliceEngagementAchievedValue, 0),
		OAPI.PoliceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'PEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.PoliceEngagementAchievedValue,
				PRI.PoliceEngagementNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO policeengagementupdate.ProvinceRecommendation
		(PoliceEngagementUpdateID, ProvinceID, RecommendationID, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RP.ProvinceID, 
		RP.RecommendationID, 
		RP.PoliceEngagementNotes
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
	
	INSERT INTO policeengagementupdate.ProvinceRisk
		(PoliceEngagementUpdateID, ProvinceID, RiskID, PoliceEngagementRiskValue, PoliceEngagementNotes)
	SELECT
		@PoliceEngagementUpdateID,
		RP.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.PoliceEngagementRiskValue, 0),
		OAPR.CommunityProvinceEngagementNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
		OUTER APPLY
			(
			SELECT
				PR.PoliceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceRisk PR
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PR.ProvinceID
					AND PR.RiskID = R.RiskID
			) OAPR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'PoliceEngagementProvince', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('CPAP Document','PPP Document')

	EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure policeengagementupdate.AddPoliceEngagementProvince

--Begin procedure weeklyreport.ApproveWeeklyReport
EXEC Utility.DropObject 'weeklyreport.ApproveWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to submit a weekly report for approval
--
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	Renamed from SubmitWeeklyReport to ApproveWeeklyReport
-- ======================================================================
CREATE PROCEDURE weeklyreport.ApproveWeeklyReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @WeeklyReportID INT
	DECLARE @tOutput TABLE (EntityTypeCode VARCHAR(50), EntityID INT)

	SELECT @WeeklyReportID = WR.WeeklyReportID
	FROM weeklyreport.WeeklyReport WR
	
	UPDATE C
	SET
		C.CommunityEngagementStatusID = WRC.CommunityEngagementStatusID,
		C.ImpactDecisionID = WRC.ImpactDecisionID, 
		C.Implications = WRC.Implications, 
		C.KeyPoints = WRC.KeyPoints,
		C.RiskMitigation = WRC.RiskMitigation,
		C.StatusChangeID = WRC.StatusChangeID, 
		C.Summary = WRC.Summary
	OUTPUT 'Community', INSERTED.CommunityID INTO @tOutput
	FROM dbo.Community C
		JOIN weeklyreport.Community WRC ON WRC.CommunityID = C.CommunityID
			AND WRC.WeeklyReportID = @WeeklyReportID

	UPDATE P
	SET
		P.ImpactDecisionID = WRP.ImpactDecisionID, 
		P.Implications = WRP.Implications, 
		P.KeyPoints = WRP.KeyPoints,
		P.RiskMitigation = WRP.RiskMitigation,
		P.StatusChangeID = WRP.StatusChangeID, 
		P.Summary = WRP.Summary
	OUTPUT 'Province', INSERTED.ProvinceID INTO @tOutput
	FROM dbo.Province P
		JOIN weeklyreport.Province WRP ON WRP.ProvinceID = P.ProvinceID
			AND WRP.WeeklyReportID = @WeeklyReportID

	INSERT INTO @tOutput (EntityTypeCode, EntityID) VALUES ('WeeklyReport', @WeeklyReportID)

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.EntityTypeCode, O.EntityID
		FROM @tOutput O
		ORDER BY O.EntityTypeCode, O.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Community'
			BEGIN
			
			EXEC eventlog.LogCommunityAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogCommunityAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Province'
			BEGIN
			
			EXEC eventlog.LogProvinceAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogProvinceAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'WeeklyReport'
			BEGIN
			
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogWeeklyReportAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION
	
	TRUNCATE TABLE weeklyreport.Community
	TRUNCATE TABLE weeklyreport.Province
	TRUNCATE TABLE weeklyreport.SummaryMapCommunity
	TRUNCATE TABLE weeklyreport.SummaryMapCommunityAsset
	TRUNCATE TABLE weeklyreport.SummaryMapIncident
	DELETE FROM weeklyreport.WeeklyReport

END
GO
--End procedure weeklyreport.ApproveWeeklyReport