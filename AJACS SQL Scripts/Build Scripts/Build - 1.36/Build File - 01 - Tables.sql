USE AJACS
GO

ALTER TABLE dbo.Community ALTER COLUMN Latitude NUMERIC(20,15) NULL
ALTER TABLE dbo.Community ALTER COLUMN Longitude NUMERIC(20,15) NULL
ALTER TABLE dbo.Incident ALTER COLUMN Latitude NUMERIC(20,15) NULL
ALTER TABLE dbo.Incident ALTER COLUMN Longitude NUMERIC(20,15) NULL
GO

EXEC utility.DropObject 'DF_Community_Latitude'
EXEC utility.DropObject 'DF_Community_Longitude'
EXEC utility.DropObject 'DF_Incident_Latitude'
EXEC utility.DropObject 'DF_Incident_Longitude'
GO