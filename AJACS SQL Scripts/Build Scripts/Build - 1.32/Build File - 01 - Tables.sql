USE AJACS
GO

--Begin table communityprovinceengagementupdate.*Finding
EXEC utility.DropObject 'communityprovinceengagementupdate.CommunityFinding'
EXEC utility.DropObject 'communityprovinceengagementupdate.ProvinceFinding'
GO
--Begin table communityprovinceengagementupdate.*Finding

--Begin table dropdown.ProjectStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ProjectStatus'

EXEC utility.AddColumn @TableName, 'IsOpen', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsOpen', 'INT', 1
GO

UPDATE PS
SET PS.IsOpen = 0
FROM dropdown.ProjectStatus PS
WHERE PS.ProjectStatusName IN ('Closed','Cancelled','Suspended')
GO
--End table dropdown.ProjectStatus
