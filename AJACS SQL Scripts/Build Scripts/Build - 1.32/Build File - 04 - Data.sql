USE AJACS
GO

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsGlobal, IsWorkflow)
SELECT 
	P1.PermissionableID,
	'Information', 
	'Information', 
	1, 
	0, 
	0
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Community.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2
		WHERE P2.PermissionableLineage = 'Community.View.Information'
		)

INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsGlobal, IsWorkflow)
SELECT 
	P1.PermissionableID,
	'Analysis', 
	'Analysis', 
	2, 
	0, 
	0
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Community.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2
		WHERE P2.PermissionableLineage = 'Community.View.Analysis'
		)

INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsGlobal, IsWorkflow)
SELECT 
	P1.PermissionableID,
	'Implementation', 
	'Implementation', 
	3, 
	0, 
	0
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Community.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2
		WHERE P2.PermissionableLineage = 'Community.View.Implementation'
		)


INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsGlobal, IsWorkflow)
SELECT 
	P1.PermissionableID,
	'Information', 
	'Information', 
	1, 
	0, 
	0
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Province.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2
		WHERE P2.PermissionableLineage = 'Province.View.Information'
		)

INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsGlobal, IsWorkflow)
SELECT 
	P1.PermissionableID,
	'Analysis', 
	'Analysis', 
	2, 
	0, 
	0
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Province.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2
		WHERE P2.PermissionableLineage = 'Province.View.Analysis'
		)

INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsGlobal, IsWorkflow)
SELECT 
	P1.PermissionableID,
	'Implementation', 
	'Implementation', 
	3, 
	0, 
	0
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Province.View'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2
		WHERE P2.PermissionableLineage = 'Province.View.Implementation'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
