USE AJACSUtility
GO

--Begin schema syslog
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'syslog')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA syslog'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema syslog


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'syslog' AND  TABLE_NAME = 'ApplicationErrorLog')
	BEGIN

	CREATE TABLE syslog.ApplicationErrorLog
		(
		ApplicationErrorLogID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		Error	NVARCHAR(MAX),
		ErrorDateTime	DateTime DEFAULT GETDATE(),
		ErrorRCData	NVARCHAR(MAX),
		ErrorSession NVARCHAR(MAX),
		ErrorCGIData NVARCHAR(MAX)
		)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'syslog' AND  TABLE_NAME = 'DuoWebTwoFactorLog')
	BEGIN

	CREATE TABLE syslog.DuoWebTwoFactorLog
		(
		DuoWebTwoFactorLogID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		HttpBaseURL NVARCHAR(MAX),
		DuoWebRequest NVARCHAR(MAX),
		DuoWebAuth NVARCHAR(MAX),
		DuoWebSig NVARCHAR(MAX),
		DuoMessage NVARCHAR(MAX),
		DuoWebRequestHttpDateTime	NVARCHAR(MAX),
		DuoWebResponse NVARCHAR(MAX),
		RequestDateDateTime DateTime DEFAULT GETDATE()
		)

	END
--ENDIF
GO

USE AJACS
GO
