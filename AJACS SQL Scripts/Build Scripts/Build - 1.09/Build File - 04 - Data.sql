USE AJACS
GO

--Begin table dbo.ConceptNoteBudget
UPDATE CNB
SET CNB.BudgetSubTypeID = (SELECT BST.BudgetSubTypeID FROM dropdown.BudgetSubType BST WHERE BST.BudgetSubTypeName = 'Other')
FROM dbo.ConceptNoteBudget CNB
WHERE CNB.BudgetSubTypeID = 0
GO
--End table dbo.ConceptNoteBudget

--Begin table dbo.DocumentMimeType
UPDATE DMT
SET DMT.IsActive = 1
FROM dbo.DocumentMimeType DMT 
WHERE DMT.Extension = '.pptx'
GO

IF NOT EXISTS (SELECT 1 FROM dbo.DocumentMimeType DMT WHERE DMT.Extension = '.pptx' AND DMT.IsActive = 1)
	BEGIN
	
	INSERT INTO dbo.DocumentMimeType
		(MimeType,Extension,IsActive)
	VALUES
		('application/vnd.openxmlformats-officedocument.presentationml.presentation','.pptx',1)
 
 END
--ENDIF
GO 
--End table dbo.DocumentMimeType

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'ProgramReport')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('ProgramReport', 'Weekly Program Report')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode IN ('Incident','IncidentReport'))
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('IncidentReport', 'Incident Report')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Document')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Document', 'Document')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Team')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Team', 'Team')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Person')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Person', 'Person')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'PurchaseRequest')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('PurchaseRequest', 'Purchase Request')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'DailyReport')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('DailyReport', 'Daily Report')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Community')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Community', 'Community')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Login')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Login', 'Login')
--ENDIF
GO
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Province')
	INSERT INTO dbo.EntityType (EntityTypeCode,EntityTypeName) VALUES ('Province', 'Province')
--ENDIF
GO

UPDATE dbo.EntityType SET EntityTypeCode = 'Incident' WHERE EntityTypeCode = 'IncidentReport'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE MI
SET MI.MenuItemCode = 'ProgramReportList'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'ProgramReport'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ManageWeeklyReports', @NewMenuItemText='Weekly Reports'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ProgramReportList', @NewMenuItemLink='/programreport/list', @PermissionableLineageList='ProgramReport.List', @IsActive=1

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Atmospheric', @NewMenuItemLink='', @PermissionableLineageList='Atmospheric.List,Incident.List'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='AtmosphericReportList', @NewMenuItemLink='/atmospheric/list', @NewMenuItemText='General Atmospheric Report', @ParentMenuItemCode='Atmospheric', @PermissionableLineageList='Atmospheric.List'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='IncidentReportList', @NewMenuItemLink='/incident/list', @NewMenuItemText='Incident Report', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='AtmosphericReportList', @PermissionableLineageList='Incident.List'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EventLogList', @NewMenuItemLink='/eventlog/list', @NewMenuItemText='Event Log', @ParentMenuItemCode='Admin', @BeforeMenuItemCode='PersonList', @PermissionableLineageList='EventLog.List'
GO
--End table dbo.MenuItem

--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'ProgramReport')
	BEGIN
	
	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode,DocumentTypeName,DisplayOrder)
	VALUES
		('ProgramReport','Weekly Program Report',9)
		
	END
--ENDIF
GO
--End table dropdown.DocumentType

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'ProgramReport')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('ProgramReport',3)
		
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowAction
IF NOT EXISTS (SELECT 1 FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = 'Rerelease')
	BEGIN
	
	INSERT INTO workflow.WorkflowAction
		(WorkflowActionCode,WorkflowActionName)
	VALUES
		('Rerelease','Re-Release')
	
	END
--ENDIF
GO
--End table workflow.WorkflowAction

--Begin table workflow.WorkflowStep
DECLARE @nWorkflowID INT

SELECT @nWorkflowID = W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'ProgramReport'

INSERT INTO workflow.WorkflowStep
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
SELECT
	WS1.ParentWorkflowStepID, 
	@nWorkflowID, 
	WS1.WorkflowStepNumber, 
	WS1.WorkflowStepName
FROM workflow.WorkflowStep WS1
	JOIN workflow.Workflow W ON W.WorkflowID = WS1.WorkflowID
		AND W.EntityTypeCode = 'WeeklyReport'
		AND NOT EXISTS
			(
			SELECT 1
			FROM workflow.WorkflowStep WS2
			WHERE WS2.WorkflowID = @nWorkflowID
				AND WS2.WorkflowStepNumber = WS1.WorkflowStepNumber
			)
GO

UPDATE WS
SET WS.WorkflowStatusName = 
	CASE
		WHEN WS.WorkflowStepName = 'Draft'
		THEN 'In Draft'
		WHEN WS.WorkflowStepName = 'Review'
		THEN 'Under Review'
		WHEN WS.WorkflowStepName = 'Approve'
		THEN 'Pending Approval'
	END
FROM workflow.WorkflowStep WS
GO
--End table workflow.WorkflowStep

--Begin table workflow.WorkflowStepWorkflowAction
DECLARE @nWorkflowID INT

SELECT @nWorkflowID = W.WorkflowID FROM workflow.Workflow W WHERE W.EntityTypeCode = 'ProgramReport'

INSERT INTO workflow.WorkflowStepWorkflowAction
	(WorkflowID,WorkflowStepNumber,WorkflowActionID,DisplayOrder)
SELECT
	@nWorkflowID,
	WSA1.WorkflowStepNumber,
	WSA1.WorkflowActionID,
	WSA1.DisplayOrder
FROM workflow.WorkflowStepWorkflowAction WSA1
	JOIN workflow.Workflow W ON W.WorkflowID = WSA1.WorkflowID
		AND W.EntityTypeCode = 'SpotReport'
		AND NOT EXISTS
			(
			SELECT 1
			FROM workflow.WorkflowStepWorkflowAction WSA2
			WHERE WSA2.WorkflowID = @nWorkflowID
				AND WSA2.WorkflowStepNumber = WSA1.WorkflowStepNumber
				AND WSA2.WorkflowActionID = WSA1.WorkflowActionID
			)
GO
--End table workflow.WorkflowStepWorkflowAction

--Begin table permissionable.Permissionable
DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'importBudget'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DataImport')
	BEGIN

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, IsGlobal)
	VALUES 
		(0,'DataImport','Data Import','DataImport','1')
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EventLog')
	BEGIN

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage)
	VALUES 
		(0,'EventLog','Event Log','EventLog')
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Incident')
	BEGIN

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage)
	VALUES
		(0,'Incident','Incidents','Incident')
	
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'View',
	'View',
	'EventLog.View',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'EventLog'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EventLog.View'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'List',
	'List',
	'EventLog.List',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'EventLog'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'EventLog.List'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	'Incident.AddUpdate',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'Incident'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Incident.AddUpdate'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'View',
	'View',
	'Incident.View',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'Incident'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Incident.View'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'List',
	'List',
	'Incident.List',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'Incident'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Incident.List'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'FinalizeEquipmentDistribution',
	'Finalize Equipment Distribution',
	'Incident.AddUpdate',
	4
FROM permissionable.Permissionable P1
WHERE P1.PermissionableCode = 'ConceptNoteContactEquipment'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'ConceptNoteContactEquipment.FinalizeEquipmentDistribution'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Amend',
	'Amend',
	'SpotReport.AddUpdate.Amend',
	4
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'SpotReport.AddUpdate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'SpotReport.AddUpdate.Amend'
		)
GO

UPDATE P
SET P.DisplayOrder = 1
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'SpotReport.AddUpdate'
GO

UPDATE P
SET P.DisplayOrder = 2
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'SpotReport.View'
GO

UPDATE P
SET P.DisplayOrder = 3
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'SpotReport.List'
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, PermissionableLineage, DisplayOrder, IsWorkflow)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'ProgramReport.AddUpdate'),
	'WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	'ProgramReport.AddUpdate.WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepNumber,
	1
FROM workflow.WorkflowStep WS
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
		AND W.EntityTypeCode = 'ProgramReport'
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = 'WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10))
				AND P.PermissionableLineage = 'ProgramReport.AddUpdate.WorkflowStepID' + CAST(WS.WorkflowStepID AS VARCHAR(10))
			)
ORDER BY WS.WorkflowStepNumber
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Administrative'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('EventLog')
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'ObservationReport'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Incident')
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.PersonPermissionable
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (17,26)

	END
--ENDIF
GO
--End table permissionable.PersonPermissionable
