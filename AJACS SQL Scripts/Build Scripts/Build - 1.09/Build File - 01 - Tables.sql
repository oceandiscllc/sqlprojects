USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'UpdateDate', 'DATE'
EXEC utility.AddColumn @TableName, 'UpdateNoteDocumentID', 'INT'
EXEC utility.AddColumn @TableName, 'UpdateTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'UpdateNoteDocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateTypeID', 'INT', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteBudget
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteBudget'

EXEC utility.AddColumn @TableName, 'BudgetSubTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'BudgetSubTypeID', 'INT', 0
GO
--End table dbo.ConceptNoteBudget

--Begin table dbo.Contact
ALTER TABLE dbo.Contact ALTER COLUMN Address1 NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN Address2 NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN CellPhoneNumber NVARCHAR(40)
ALTER TABLE dbo.Contact ALTER COLUMN City NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN EmailAddress1 NVARCHAR(640)
ALTER TABLE dbo.Contact ALTER COLUMN EmailAddress2 NVARCHAR(640)
ALTER TABLE dbo.Contact ALTER COLUMN EmployerName NVARCHAR(100)
ALTER TABLE dbo.Contact ALTER COLUMN FaxNumber NVARCHAR(40)
ALTER TABLE dbo.Contact ALTER COLUMN FirstName NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN Gender NVARCHAR(20)
ALTER TABLE dbo.Contact ALTER COLUMN GovernmentIDNumber NVARCHAR(40)
ALTER TABLE dbo.Contact ALTER COLUMN LastName NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN MiddleName NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN PassportNumber NVARCHAR(40)
ALTER TABLE dbo.Contact ALTER COLUMN PhoneNumber NVARCHAR(40)
ALTER TABLE dbo.Contact ALTER COLUMN PlaceOfBirth NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN PostalCode NVARCHAR(20)
ALTER TABLE dbo.Contact ALTER COLUMN Profession NVARCHAR(100)
ALTER TABLE dbo.Contact ALTER COLUMN SkypeUserName NVARCHAR(100)
ALTER TABLE dbo.Contact ALTER COLUMN State NVARCHAR(200)
ALTER TABLE dbo.Contact ALTER COLUMN Title NVARCHAR(100)
GO
--End table dbo.Contact

--Begin table dbo.Incident
DECLARE @TableName VARCHAR(250) = 'dbo.Incident'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.Incident
	(
	IncidentID INT IDENTITY(1,1) NOT NULL,
	IncidentName VARCHAR(250),
	IncidentDate DATE,
	IncidentTypeID INT,
	SourceReliabilityID INT,
	InformationValidityID INT,
	Summary VARCHAR(MAX),
	KeyPoints VARCHAR(MAX),
	Implications VARCHAR(MAX),
	RiskMitigation VARCHAR(MAX),
	Latitude NUMERIC(20,15),
	Longitude NUMERIC(20,15)
	)

EXEC utility.SetDefaultConstraint @TableName, 'IncidentTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'InformationValidityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Latitude', 'NUMERIC(20,15)', 0
EXEC utility.SetDefaultConstraint @TableName, 'Longitude', 'NUMERIC(20,15)', 0
EXEC utility.SetDefaultConstraint @TableName, 'SourceReliabilityID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'IncidentID'
GO
--End table dbo.Incident

--Begin table dbo.IncidentCommunity
DECLARE @TableName VARCHAR(250) = 'dbo.IncidentCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.IncidentCommunity
	(
	IncidentCommunityID INT IDENTITY(1,1) NOT NULL,
	IncidentID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IncidentCommunityID'
EXEC utility.SetIndexClustered 'IX_IncidentCommunity', @TableName, 'IncidentID,CommunityID'
GO
--End table dbo.IncidentCommunity

--Begin table dbo.IncidentProvince
DECLARE @TableName VARCHAR(250) = 'dbo.IncidentProvince'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.IncidentProvince
	(
	IncidentProvinceID INT IDENTITY(1,1) NOT NULL,
	IncidentID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'IncidentProvinceID'
EXEC utility.SetIndexClustered 'IX_IncidentProvince', @TableName, 'IncidentID,ProvinceID'
GO
--End table dbo.IncidentProvince

--Begin table dbo.RequestForInformation
DECLARE @TableName VARCHAR(250) = 'dbo.RequestForInformation'

EXEC utility.AddColumn @TableName, 'DesiredResponseDate', 'DATE'
EXEC utility.AddColumn @TableName, 'SpotReportID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', 0
GO
--End table dbo.RequestForInformation

--Begin table dbo.SpotReportCommunity
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReportCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SpotReportCommunity
	(
	SpotReportCommunityID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportCommunityID'
EXEC utility.SetIndexClustered 'IX_SpotReportCommunity', @TableName, 'SpotReportID,CommunityID'
GO

INSERT INTO dbo.SpotReportCommunity
	(SpotReportID,CommunityID)
SELECT
	SR.SpotReportID,
	SR.CommunityID
FROM dbo.SpotReport SR
WHERE SR.CommunityID > 0
	AND NOT EXISTS
	(
	SELECT 1
	FROM dbo.SpotReportCommunity SRC
	WHERE SRC.SpotReportID = SR.SpotReportID
		AND SRC.CommunityID = SR.CommunityID
	)
GO
--End table dbo.SpotReportCommunity

--Begin table dbo.SpotReportIncident
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReportIncident'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SpotReportIncident
	(
	SpotReportIncidentID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	IncidentID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IncidentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportIncidentID'
EXEC utility.SetIndexClustered 'IX_SpotReportIncident', @TableName, 'SpotReportID,IncidentID'
GO
--End table dbo.SpotReportIncident

--Begin table dbo.SpotReportProvince
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReportProvince'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.SpotReportProvince
	(
	SpotReportProvinceID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	ProvinceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportProvinceID'
EXEC utility.SetIndexClustered 'IX_SpotReportProvince', @TableName, 'SpotReportID,ProvinceID'
GO

INSERT INTO dbo.SpotReportProvince
	(SpotReportID,ProvinceID)
SELECT
	SR.SpotReportID,
	SR.ProvinceID
FROM dbo.SpotReport SR
WHERE SR.ProvinceID > 0
	AND NOT EXISTS
	(
	SELECT 1
	FROM dbo.SpotReportProvince SRC
	WHERE SRC.SpotReportID = SR.SpotReportID
		AND SRC.ProvinceID = SR.ProvinceID
	)
GO
--End table dbo.SpotReportProvince

--Begin table dropdown.BudgetSubType
DECLARE @TableName VARCHAR(250) = 'dropdown.BudgetSubType'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.ConceptNoteBudgetSubType'

CREATE TABLE dropdown.BudgetSubType
	(
	BudgetSubTypeID INT IDENTITY(0,1) NOT NULL,
	BudgetSubTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'BudgetSubTypeID'
EXEC utility.SetIndexNonClustered 'IX_BudgetSubTypeName', @TableName, 'DisplayOrder,BudgetSubTypeName','BudgetSubTypeID'
GO

SET IDENTITY_INSERT dropdown.BudgetSubType ON
GO

INSERT INTO dropdown.BudgetSubType (BudgetSubTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.BudgetSubType OFF
GO

INSERT INTO dropdown.BudgetSubType 
	(BudgetSubTypeName,DisplayOrder)
VALUES
	('International Travel',1),
	('Domestic Travel',2),
	('Consultants',3),
	('Per Diem',4),
	('Lodging',5),
	('Other',6)
GO 
--End table dropdown.BudgetSubType

--Begin table dropdown.ConceptNoteUpdateType
DECLARE @TableName VARCHAR(250) = 'dropdown.ConceptNoteUpdateType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ConceptNoteUpdateType
	(
	ConceptNoteUpdateTypeID INT IDENTITY(0,1) NOT NULL,
	ConceptNoteUpdateTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ConceptNoteUpdateTypeID'
EXEC utility.SetIndexNonClustered 'IX_ConceptNoteUpdateTypeName', @TableName, 'DisplayOrder,ConceptNoteUpdateTypeName','ConceptNoteUpdateTypeID'
GO

SET IDENTITY_INSERT dropdown.ConceptNoteUpdateType ON
GO

INSERT INTO dropdown.ConceptNoteUpdateType (ConceptNoteUpdateTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.ConceptNoteUpdateType OFF
GO

INSERT INTO dropdown.ConceptNoteUpdateType 
	(ConceptNoteUpdateTypeName,DisplayOrder)
VALUES
	('Amendments',1),
	('Progress',2),
	('Security',3),
	('Contractual',4),
	('M&E',5),
	('Other',6)
GO 
--End table dropdown.ConceptNoteUpdateType

--Begin table dropdown.IncidentType
DECLARE @TableName VARCHAR(250) = 'dropdown.IncidentType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.IncidentType
	(
	IncidentTypeID INT IDENTITY(0,1) NOT NULL,
	IncidentTypeCategory VARCHAR(50),
	IncidentTypeName VARCHAR(50),
	Icon VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'IncidentTypeID'
EXEC utility.SetIndexNonClustered 'IX_IncidentTypeName', @TableName, 'IncidentTypeCategory,DisplayOrder,IncidentTypeName', 'IncidentTypeID'
GO

SET IDENTITY_INSERT dropdown.IncidentType ON
GO

INSERT INTO dropdown.IncidentType (IncidentTypeID) VALUES (0)

SET IDENTITY_INSERT dropdown.IncidentType OFF
GO

INSERT INTO dropdown.IncidentType 
	(IncidentTypeCategory,IncidentTypeName)
VALUES
	('Civil Activity', 'Civil Disorder'),
	('Civil Activity', 'Protest'),
	('Criminality', 'Beating'),
	('Criminality', 'Execution'),
	('Criminality', 'Murder'),
	('Criminality', 'Robbery'),
	('IED', 'SIED (Suicide IED)'),
	('IED', 'SVBIED (Suicide Vehicle-borne IED)'),
	('IED', 'UVIED (Under Vehicle IED)'),
	('IED', 'VBIED (Vehicle-borne IED)'),
	('Illegal Police Activity', 'Corruption'),
	('Illegal Police Activity', 'Extortion'),
	('Illegal Police Activity', 'Illegal Detention'),
	('Military Activity - Air', 'Airstrike'),
	('Military Activity - Ground', 'Armed Clashes'),
	('Military Activity - Ground', 'Artillery/Mortar/Rocket Strike'),
	('Military Activity - Air', 'Barrel Bomb')
GO 

UPDATE dropdown.IncidentType SET Icon = 'civilian.png' WHERE IncidentTypeCategory = 'Civil Activity';
UPDATE dropdown.IncidentType SET Icon = 'crime.png' WHERE IncidentTypeCategory = 'Criminality';
UPDATE dropdown.IncidentType SET Icon = 'ied.png' WHERE IncidentTypeCategory = 'IED';
UPDATE dropdown.IncidentType SET Icon = 'police.png' WHERE IncidentTypeCategory = 'Illegal Police Activity';
UPDATE dropdown.IncidentType SET Icon = 'military-air.png' WHERE IncidentTypeCategory = 'Military Activity - Air';
UPDATE dropdown.IncidentType SET Icon = 'military-ground.png' WHERE IncidentTypeCategory = 'Military Activity - Ground';
GO
--End table dropdown.IncidentType

--Begin table dropdown.InformationValidity
DECLARE @TableName VARCHAR(250) = 'dropdown.InformationValidity'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.InformationValidity
	(
	InformationValidityID INT IDENTITY(0,1) NOT NULL,
	InformationValidityName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'InformationValidityID'
EXEC utility.SetIndexNonClustered 'IX_InformationValidityName', @TableName, 'DisplayOrder,InformationValidityName', 'InformationValidityID'
GO

SET IDENTITY_INSERT dropdown.InformationValidity ON
GO

INSERT INTO dropdown.InformationValidity (InformationValidityID) VALUES (0)

SET IDENTITY_INSERT dropdown.InformationValidity OFF
GO

INSERT INTO dropdown.InformationValidity 
	(InformationValidityName,DisplayOrder) 
VALUES 
	('Known to be true without reservation',1),
	('The information is known personally by the resource but not to the person reporting',2),
	('The information is not known personally to the resource but can be corroborated by other information',3),
	('The information cannot be judged',4),
	('Suspected to be false',5)
GO
--End table dropdown.InformationValidity

--Begin table dropdown.SourceReliability
DECLARE @TableName VARCHAR(250) = 'dropdown.SourceReliability'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SourceReliability
	(
	SourceReliabilityID INT IDENTITY(0,1) NOT NULL,
	SourceReliabilityName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SourceReliabilityID'
EXEC utility.SetIndexNonClustered 'IX_SourceReliabilityName', @TableName, 'DisplayOrder,SourceReliabilityName', 'SourceReliabilityID'
GO

SET IDENTITY_INSERT dropdown.SourceReliability ON
GO

INSERT INTO dropdown.SourceReliability (SourceReliabilityID) VALUES (0)

SET IDENTITY_INSERT dropdown.SourceReliability OFF
GO

INSERT INTO dropdown.SourceReliability 
	(SourceReliabilityName,DisplayOrder)
VALUES
	('Always Reliable',1),
	('Mostly Reliable',2),
	('Sometimes Reliable ',3),
	('Unreliable',4),
	('Untested Source',5)
GO
--End table dropdown.SourceReliability

--Begin table weeklyreport.ProgramReport
DECLARE @TableName VARCHAR(250) = 'weeklyreport.ProgramReport'

EXEC utility.DropObject @TableName

CREATE TABLE weeklyreport.ProgramReport
	(
	ProgramReportID INT IDENTITY(1,1) NOT NULL,
	ProgramReportName VARCHAR(250),
	ProgramReportStartDate DATE,
	ProgramReportEndDate DATE,
	OverallPrevious VARCHAR(MAX),
	OverallProjected VARCHAR(MAX),
	EngagementPrevious VARCHAR(MAX),
	EngagementProjected VARCHAR(MAX),
	PolicingPrevious VARCHAR(MAX),
	PolicingProjected VARCHAR(MAX),
	JusticePrevious VARCHAR(MAX),
	JusticeProjected VARCHAR(MAX),
	StructuresPrevious VARCHAR(MAX),
	StructuresProjected VARCHAR(MAX),
	MonitoringPrevious VARCHAR(MAX),
	MonitoringProjected VARCHAR(MAX),
	OperationsPrevious VARCHAR(MAX),
	OperationsProjected VARCHAR(MAX),
	Remarks VARCHAR(MAX),
	WorkflowStepNumber INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProgramReportID'
GO
--End table weeklyreport.ProgramReport