USE AJACS
GO

--Begin procedure dbo.GetEventLogDataByEventLogID
EXEC Utility.DropObject 'dbo.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dbo.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		dbo.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure dbo.GetEventLogDataByEventLogID

--Begin procedure dbo.GetIncidentByIncidentID
EXEC Utility.DropObject 'dbo.GetIncidentByIncidentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.19
-- Description:	A stored procedure to data from the dbo.Incident table
-- ===================================================================
CREATE PROCEDURE dbo.GetIncidentByIncidentID

@IncidentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentDate,
		I.IncidentName,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		SR.SourceReliabilityID,
		SR.SourceReliabilityName,
		IV.InformationValidityID,
		IV.InformationValidityName,
		I.Summary,
		I.KeyPoints,
		I.Implications,
		I.RiskMitigation,
		I.Latitude,
		I.Longitude
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
		JOIN dropdown.SourceReliability SR ON SR.SourceReliabilityID = I.SourceReliabilityID
		JOIN dropdown.InformationValidity IV ON IV.InformationValidityID = I.InformationValidityID
			AND I.IncidentID = @IncidentID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.IncidentCommunity IC ON IC.CommunityID = C.CommunityID
			AND IC.IncidentID = @IncidentID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.IncidentProvince IP ON IP.ProvinceID = P.ProvinceID
			AND IP.IncidentID = @IncidentID
		
END
GO
--End procedure dbo.GetIncidentByIncidentID

--Begin procedure dbo.GetIncidentLocations
EXEC Utility.DropObject 'dbo.GetIncidentLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.23
-- Description:	A stored procedure to data from the dbo.Incident table
-- ===================================================================
CREATE PROCEDURE dbo.GetIncidentLocations

@StartDate DATE,
@EndDate DATE

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentID,
		I.IncidentName,
		I.Latitude,
		I.Longitude,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND I.IncidentDate BETWEEN @StartDate AND @EndDate

END
GO
--End procedure dbo.GetIncidentLocations

--Begin procedure dbo.GetRequestForInformationByRequestForInformationID
EXEC Utility.DropObject 'dbo.GetRequestForInformationByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
--
-- Author:			Todd Pires
-- Create date:	2015.04.04
-- Description:	Added the SummaryAnswer field
--
-- Author:			Todd Pires
-- Create date:	2015.04.22
-- Description:	Added the DesiredResponseDate and SpotReportID fields
-- ====================================================================================
CREATE PROCEDURE dbo.GetRequestForInformationByRequestForInformationID

@RequestForInformationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C2.CommunityID,
		C2.CommunityName,
		RFI.CompletedDate,
		dbo.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		RFI.DesiredResponseDate,
		dbo.FormatDate(RFI.DesiredResponseDate) AS DesiredResponseDateFormatted,
		RFI.IncidentDate,
		dbo.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.InformationRequested,
		RFI.InProgressDate,
		dbo.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.KnownDetails,
		RFI.Location,
		RFI.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonFullname,
		RFI.RequestDate,
		dbo.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationID,
		RFI.RequestForInformationTitle,
		RFI.RequestPersonID,
		dbo.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonFullname,
		RFI.SpotReportID,
		dbo.FormatSpotReportReferenceCode(RFI.SpotReportID) AS SpotReportReferenceCode,
		RFI.SummaryAnswer,
		RFIRT.RequestForInformationResultTypeID,
		RFIRT.RequestForInformationResultTypeCode,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusID,
		RFIS.RequestForInformationStatusCode,
		RFIS.RequestForInformationStatusName,
		dbo.GetEntityTypeNameByEntityTypeCode('RequestForInformation') AS EntityTypeName
	FROM dbo.RequestForInformation RFI
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFI.RequestForInformationID = @RequestForInformationID
		OUTER APPLY
				(
				SELECT
					C1.CommunityID,
					C1.CommunityName
				FROM dbo.Community C1
				WHERE C1.CommunityID = RFI.CommunityID
				) C2

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'RequestForInformation'
			AND DE.EntityID = @RequestForInformationID

END
GO
--End procedure dbo.GetRequestForInformationByRequestForInformationID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunityID,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProvinceID,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dropdown.GetBudgetSubTypeData
EXEC Utility.DropObject 'dropdown.GetConceptNoteBudgetSubTypeData'
EXEC Utility.DropObject 'dropdown.GetBudgetSubTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the dropdown.BudgetSubType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetBudgetSubTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.BudgetSubTypeID, 
		T.BudgetSubTypeName
	FROM dropdown.BudgetSubType T
	WHERE (T.BudgetSubTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.BudgetSubTypeName, T.BudgetSubTypeID

END
GO
--End procedure dropdown.GetBudgetSubTypeData

--Begin procedure dropdown.GetConceptNoteUpdateTypeData
EXEC Utility.DropObject 'dropdown.GetConceptNoteUpdateTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A stored procedure to return data from the dropdown.ConceptNoteUpdateType table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetConceptNoteUpdateTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ConceptNoteUpdateTypeID, 
		T.ConceptNoteUpdateTypeName
	FROM dropdown.ConceptNoteUpdateType T
	WHERE (T.ConceptNoteUpdateTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ConceptNoteUpdateTypeName, T.ConceptNoteUpdateTypeID

END
GO
--End procedure dropdown.GetConceptNoteUpdateTypeData

--Begin procedure dropdown.GetEntityTypeNameData
EXEC Utility.DropObject 'dropdown.GetEntityTypeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.26
-- Description:	A stored procedure to return data from the dbo.EntityTypeName table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEntityTypeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EntityTypeCode,
		T.EntityTypeName
	FROM dbo.EntityType T
	ORDER BY T.EntityTypeName, T.EntityTypeCode

END
GO
--End procedure dropdown.GetEntityTypeNameData

--Begin procedure dropdown.GetEventCodeNameData
EXEC Utility.DropObject 'dropdown.GetEventCodeNameData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetEventCodeNameData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EventCode,
		dbo.getEventNameByEventCode(T.EventCode) AS EventCodeName
	FROM 
		(
		SELECT DISTINCT
			EL.EventCode
		FROM eventlog.EventLog EL
		) T
	ORDER BY 2, 1

END
GO
--End procedure dropdown.GetEventCodeNameData

--Begin procedure dropdown.GetIncidentTypeData
EXEC Utility.DropObject 'dropdown.GetIncidentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A stored procedure to return data from the dropdown.IncidentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetIncidentTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.IncidentTypeID, 
		T.IncidentTypeCategory,
		T.IncidentTypeName,
		T.Icon
	FROM dropdown.IncidentType T
	WHERE (T.IncidentTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.IncidentTypeCategory,T.DisplayOrder, T.IncidentTypeName, T.IncidentTypeID

END
GO
--End procedure dropdown.GetIncidentTypeData

--Begin procedure dropdown.GetInformationValidityData
EXEC Utility.DropObject 'dropdown.GetInformationValidityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A stored procedure to return data from the dropdown.InformationValidity table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetInformationValidityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.InformationValidityID, 
		T.InformationValidityName
	FROM dropdown.InformationValidity T
	WHERE (T.InformationValidityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.InformationValidityName, T.InformationValidityID

END
GO
--End procedure dropdown.GetInformationValidityData

--Begin procedure dropdown.GetSourceReliabilityData
EXEC Utility.DropObject 'dropdown.GetSourceReliabilityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A stored procedure to return data from the dropdown.SourceReliability table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetSourceReliabilityData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SourceReliabilityID, 
		T.SourceReliabilityName
	FROM dropdown.SourceReliability T
	WHERE (T.SourceReliabilityID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SourceReliabilityName, T.SourceReliabilityID

END
GO
--End procedure dropdown.GetSourceReliabilityData

--Begin procedure eventlog.LogConceptNoteAction
EXEC Utility.DropObject 'eventlog.LogConceptNoteAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date: 2015.04.02
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- =========================================================================================
CREATE PROCEDURE eventlog.LogConceptNoteAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN
			
		DECLARE @cConceptNoteBudget VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteBudget = COALESCE(@cConceptNoteBudget, '') + D.ConceptNoteBudget
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteBudget'), ELEMENTS) AS ConceptNoteBudget
			FROM dbo.ConceptNoteBudget T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteClass VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteClass = COALESCE(@cConceptNoteClass, '') + D.ConceptNoteClass
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteClass'), ELEMENTS) AS ConceptNoteClass
			FROM dbo.ConceptNoteClass T 
			WHERE T.ConceptNoteID = @EntityID
			) D	

		DECLARE @cConceptNoteCommunities VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCommunities = COALESCE(@cConceptNoteCommunities, '') + D.ConceptNoteCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCommunity'), ELEMENTS) AS ConceptNoteCommunity
			FROM dbo.ConceptNoteCommunity T 
			WHERE T.ConceptNoteID = @EntityID
			) D
			
		DECLARE @cConceptNoteContact VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteContact= COALESCE(@cConceptNoteContact, '') + D.ConceptNoteContact
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteContact'), ELEMENTS) AS ConceptNoteContact
			FROM dbo.ConceptNoteContact T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteCourse VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteCourse= COALESCE(@cConceptNoteCourse, '') + D.ConceptNoteCourse
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteCourse'), ELEMENTS) AS ConceptNoteCourse
			FROM dbo.ConceptNoteCourse T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteEquipmentCatalog  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteEquipmentCatalog = COALESCE(@cConceptNoteEquipmentCatalog , '') + D.ConceptNoteEquipmentCatalog 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteEquipmentCatalog'), ELEMENTS) AS ConceptNoteEquipmentCatalog 
			FROM dbo.ConceptNoteEquipmentCatalog  T 
			WHERE T.ConceptNoteID = @EntityID
			) D				
			
		DECLARE @cConceptNoteIndicator  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteIndicator = COALESCE(@cConceptNoteIndicator , '') + D.ConceptNoteIndicator 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteIndicator'), ELEMENTS) AS ConceptNoteIndicator 
			FROM dbo.ConceptNoteIndicator  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		DECLARE @cConceptNoteProvinces VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteProvinces = COALESCE(@cConceptNoteProvinces, '') + D.ConceptNoteProvince
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteProvince'), ELEMENTS) AS ConceptNoteProvince
			FROM dbo.ConceptNoteProvince T 
			WHERE T.ConceptNoteID = @EntityID
			) D			

		DECLARE @cConceptNoteTask  VARCHAR(MAX) 
	
		SELECT 
			@cConceptNoteTask = COALESCE(@cConceptNoteTask , '') + D.ConceptNoteTask 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ConceptNoteTask'), ELEMENTS) AS ConceptNoteTask 
			FROM dbo.ConceptNoteTask  T 
			WHERE T.ConceptNoteID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ConceptNote',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<ConceptNoteBudget>' + ISNULL( @cConceptNoteBudget , '') + '</ConceptNoteBudget>') AS XML), 
			CAST(('<ConceptNoteClass>' + ISNULL( @cConceptNoteClass , '') + '</ConceptNoteClass>') AS XML), 
			CAST(('<ConceptNoteCommunities>' + ISNULL( @cConceptNoteCommunities , '') + '</ConceptNoteCommunities>') AS XML), 
			CAST(('<ConceptNoteContact>' + ISNULL( @cConceptNoteContact , '') + '</ConceptNoteContact>') AS XML), 
			CAST(('<ConceptNoteCourse>' + ISNULL( @cConceptNoteCourse , '') + '</ConceptNoteCourse>') AS XML), 
			CAST(('<ConceptNoteEquipmentCatalog>' + ISNULL( @cConceptNoteEquipmentCatalog  , '') + '</ConceptNoteEquipmentCatalog>') AS XML), 
			CAST(('<ConceptNoteIndicator>' + ISNULL( @cConceptNoteIndicator  , '') + '</ConceptNoteIndicator>') AS XML), 
			CAST(('<ConceptNoteProvinces>' + ISNULL( @cConceptNoteProvinces , '') + '</ConceptNoteProvinces>') AS XML), 
			CAST(('<ConceptNoteTask>' + ISNULL( @cConceptNoteTask  , '') + '</ConceptNoteTask>') AS XML)
			FOR XML RAW('ConceptNote'), ELEMENTS
			)
		FROM dbo.ConceptNote T
		WHERE T.ConceptNoteID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogConceptNoteAction

--Begin procedure eventlog.LogIncidentAction
EXEC utility.DropObject 'eventlog.LogIncidentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.04.18
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIncidentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cIncidentCommunities VARCHAR(MAX) 
	
		SELECT 
			@cIncidentCommunities = COALESCE(@cIncidentCommunities, '') + D.IncidentCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentCommunity'), ELEMENTS) AS IncidentCommunity
			FROM dbo.IncidentCommunity T 
			WHERE T.IncidentID = @EntityID
			) D

		DECLARE @cIncidentProvinces VARCHAR(MAX) 
	
		SELECT 
			@cIncidentProvinces = COALESCE(@cIncidentProvinces, '') + D.IncidentProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentProvince'), ELEMENTS) AS IncidentProvince
			FROM dbo.IncidentProvince T 
			WHERE T.IncidentID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<IncidentCommunities>' + ISNULL(@cIncidentCommunities, '') + '</IncidentCommunities>') AS XML),
			CAST(('<IncidentProvinces>' + ISNULL(@cIncidentProvinces, '') + '</IncidentProvinces>') AS XML)
			FOR XML RAW('Incident'), ELEMENTS
			)
		FROM dbo.Incident T
		WHERE T.IncidentID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIncidentAction

--Begin procedure eventlog.LogProgramReportAction
EXEC utility.DropObject 'eventlog.LogProgramReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.04.18
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogProgramReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'ProgramReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cDocuments VARCHAR(MAX) = eventlog.GetDocumentsXMLByEntityTypeCodeAndEntityID('ProgramReport', @EntityID)
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'ProgramReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(@cDocuments AS XML) 
			FOR XML RAW('ProgramReport'), ELEMENTS
			)
		FROM weeklyreport.ProgramReport T
		WHERE T.ProgramReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogProgramReportAction

--Begin procedure eventlog.LogPurchaseRequestAction
EXEC utility.DropObject 'eventlog.LogPurchaseRequestAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.04.18
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPurchaseRequestAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'PurchaseRequest',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cPurchaseRequestConceptNoteBudgets VARCHAR(MAX) 
	
		SELECT 
			@cPurchaseRequestConceptNoteBudgets = COALESCE(@cPurchaseRequestConceptNoteBudgets, '') + D.PurchaseRequestConceptNoteBudget 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PurchaseRequestConceptNoteBudget'), ELEMENTS) AS PurchaseRequestConceptNoteBudget
			FROM dbo.PurchaseRequestConceptNoteBudget T 
			WHERE T.PurchaseRequestID = @EntityID
			) D

		DECLARE @cPurchaseRequestConceptNoteEquipmentCatalogs VARCHAR(MAX) 
	
		SELECT 
			@cPurchaseRequestConceptNoteEquipmentCatalogs = COALESCE(@cPurchaseRequestConceptNoteEquipmentCatalogs, '') + D.PurchaseRequestConceptNoteEquipmentCatalog 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PurchaseRequestConceptNoteEquipmentCatalog'), ELEMENTS) AS PurchaseRequestConceptNoteEquipmentCatalog
			FROM dbo.PurchaseRequestConceptNoteEquipmentCatalog T 
			WHERE T.PurchaseRequestID = @EntityID
			) D

		DECLARE @cPurchaseRequestSubContractors VARCHAR(MAX) 
	
		SELECT 
			@cPurchaseRequestSubContractors = COALESCE(@cPurchaseRequestSubContractors, '') + D.PurchaseRequestSubContractor 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('PurchaseRequestSubContractor'), ELEMENTS) AS PurchaseRequestSubContractor
			FROM dbo.PurchaseRequestSubContractor T 
			WHERE T.PurchaseRequestID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PurchaseRequest',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<PurchaseRequestConceptNoteBudgets>' + ISNULL(@cPurchaseRequestConceptNoteBudgets, '') + '</PurchaseRequestConceptNoteBudgets>') AS XML),
			CAST(('<PurchaseRequestConceptNoteEquipmentCatalogs>' + ISNULL(@cPurchaseRequestConceptNoteEquipmentCatalogs, '') + '</PurchaseRequestConceptNoteEquipmentCatalogs>') AS XML),
			CAST(('<PurchaseRequestSubContractors>' + ISNULL(@cPurchaseRequestSubContractors, '') + '</PurchaseRequestSubContractors>') AS XML)
			FOR XML RAW('PurchaseRequest'), ELEMENTS
			)
		FROM dbo.PurchaseRequest T
		WHERE T.PurchaseRequestID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPurchaseRequestAction

--Begin procedure eventlog.LogSpotReportAction
EXEC utility.DropObject 'eventlog.LogSpotReportAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.03.06
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSpotReportAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSpotReportComments VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportComments = COALESCE(@cSpotReportComments, '') + D.SpotReportComment 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportComment'), ELEMENTS) AS SpotReportComment
			FROM dbo.SpotReportComment T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportCommunities VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportCommunities = COALESCE(@cSpotReportCommunities, '') + D.SpotReportCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportCommunity'), ELEMENTS) AS SpotReportCommunity
			FROM dbo.SpotReportCommunity T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportIncidents VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportIncidents = COALESCE(@cSpotReportIncidents, '') + D.SpotReportIncident 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportIncident'), ELEMENTS) AS SpotReportIncident
			FROM dbo.SpotReportIncident T 
			WHERE T.SpotReportID = @EntityID
			) D

		DECLARE @cSpotReportProvinces VARCHAR(MAX) 
	
		SELECT 
			@cSpotReportProvinces = COALESCE(@cSpotReportProvinces, '') + D.SpotReportProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SpotReportProvince'), ELEMENTS) AS SpotReportProvince
			FROM dbo.SpotReportProvince T 
			WHERE T.SpotReportID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SpotReport',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<SpotReportComments>' + ISNULL(@cSpotReportComments, '') + '</SpotReportComments>') AS XML),
			CAST(('<SpotReportCommunities>' + ISNULL(@cSpotReportCommunities, '') + '</SpotReportCommunities>') AS XML),
			CAST(('<SpotReportIncidents>' + ISNULL(@cSpotReportIncidents, '') + '</SpotReportIncidents>') AS XML),
			CAST(('<SpotReportProvinces>' + ISNULL(@cSpotReportProvinces, '') + '</SpotReportProvinces>') AS XML)
			FOR XML RAW('SpotReport'), ELEMENTS
			)
		FROM dbo.SpotReport T
		WHERE T.SpotReportID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSpotReportAction

--Begin procedure reporting.GetConceptNoteActivityLocationsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteActivityLocationsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteActivityLocationsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cResult NVARCHAR(MAX) = ' '
	
	SELECT @cResult = @cResult + CommunityName + N', '
	FROM dbo.ConceptNoteCommunity CNC
		JOIN dbo.Community P ON P.CommunityID = CNC.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT @cResult = @cResult + ProvinceName + N', '
	FROM dbo.ConceptNoteProvince CNP
		JOIN dbo.Province P ON P.ProvinceID = CNP.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
	
	IF LEN(RTRIM(@cResult)) = 0 
		SELECT @cResult AS ActivityLocations
	ELSE
		SELECT SUBSTRING(@cResult, 0, LEN(@cResult)) AS ActivityLocations
	--ENDIF

END
GO
--End procedure reporting.GetConceptNoteActivityLocationsByConceptNoteID

--Begin procedure reporting.GetConceptNoteClassLocationsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteClassLocationsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			John Lyons
-- Create date:	2015.03.28
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteClassLocationsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cResult NVARCHAR(MAX) = ' '
	
	SELECT @cResult = @cResult + Location + N', '
	FROM dbo.ConceptNoteClass CC
		JOIN dbo.Class C ON C.ClassID = CC.ClassID
			AND CC.ConceptNoteID = @ConceptNoteID
	
	IF LEN(RTRIM(@cResult)) = 0 
		SELECT @cResult AS Locations
	ELSE
		SELECT SUBSTRING(@cResult, 0, LEN(@cResult)) AS Locations
	--ENDIF

END
GO
--End procedure reporting.GetConceptNoteClassLocationsByConceptNoteID

--Begin procedure reporting.GetProgramReportByProgramReportID
EXEC Utility.DropObject 'reporting.GetProgramReportByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date:	2015.04.24
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
-- =================================================================================
CREATE PROCEDURE reporting.GetProgramReportByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PR.EngagementPrevious,
		PR.EngagementProjected,
		PR.JusticePrevious,
		PR.JusticeProjected,
		PR.MonitoringPrevious,
		PR.MonitoringProjected,
		PR.OperationsPrevious,
		PR.OperationsProjected,
		PR.OverallPrevious,
		PR.OverallProjected,
		PR.PolicingPrevious,
		PR.PolicingProjected,
		dbo.FormatProgramReportReferenceCode(PR.ProgramReportID) AS ProgramReportReferenceCode,
		PR.ProgramReportEndDate,
		dbo.FormatDate(PR.ProgramReportEndDate) AS ProgramReportEndDateFormatted,
		PR.ProgramReportID,
		PR.ProgramReportName,
		PR.ProgramReportStartDate,
		dbo.FormatDate(PR.ProgramReportStartDate) AS ProgramReportStartDateFormatted,
		PR.Remarks,
		PR.StructuresPrevious,
		PR.StructuresProjected,
		PR.WorkflowStepNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('ProgramReport') AS EntityTypeName
	FROM weeklyreport.ProgramReport PR
	WHERE PR.ProgramReportID = @ProgramReportID

END
GO
--End procedure reporting.GetProgramReportByProgramReportID

--Begin procedure reporting.GetProgramReportDocumentsByProgramReportID
EXEC Utility.DropObject 'reporting.GetProgramReportDocsByProgramReportID'
EXEC Utility.DropObject 'reporting.GetProgramReportDocumentsByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			John Lyons
-- Create date:	2015.04.24
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
-- =================================================================================
CREATE PROCEDURE reporting.GetProgramReportDocumentsByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

		SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/servefile/getFile/GUID/' + D.PhysicalFileName AS DocumentURL
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'ProgramReport'
			AND DE.EntityID = @ProgramReportID
	ORDER BY D.DocumentDescription		

END
GO
--End procedure reporting.GetProgramReportDocumentsByProgramReportID

--Begin procedure reporting.GetPurchaseRequestSubContractorByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestSubContractorByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purches request reports
-- ==============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestSubContractorByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE (PrimaryContactName varchar(250) , SubContractorName VARCHAR(100), Address VARCHAR(200), PrimaryContactPhone VARCHAR(20))

	INSERT INTO @tTable
		(PrimaryContactName,SubContractorName, Address, PrimaryContactPhone)
	SELECT
		SC.PrimaryContactName,
		SC.SubContractorName, 
		SC.Address, 
		SC.PrimaryContactPhone
	FROM procurement.PurchaseRequestSubContractor PRSC
		JOIN dbo.SubContractor SC ON SC.SubContractorID = PRSC.SubContractorID
			AND PRSC.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(SubContractorName, Address, PrimaryContactPhone)
		VALUES
			(NULL, NULL, NULL)

		END
	--ENDIF

	SELECT
		T.PrimaryContactName,
		T.SubContractorName, 
		T.Address, 
		T.PrimaryContactPhone
	FROM @tTable T
	ORDER BY T.SubContractorName

END
GO
--End procedure reporting.GetPurchaseRequestSubContractorByPurchaseRequestID

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to data from the dbo.SpotReport table
-- =====================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunityID,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProvinceID,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName,
		dbo.GetSpotReportProvincesList(SR.SpotreportID) as ProvinceList,
		dbo.GetSpotReportCommunitiesList(SR.SpotreportID) as CommunityList,
		dbo.FormatStaticGoogleMapForSpotReport(SR.SpotReportID) AS GMap		
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure reporting.GetSpotReportCommunityBySpotReportID
EXEC Utility.DropObject 'reporting.GetSpotReportCommunityBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to data from the dbo.SpotReportCommunity table
-- ==============================================================================
CREATE PROCEDURE reporting.GetSpotReportCommunityBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID
		
END
GO
--End procedure dbo.GetSpotReportCommunityBySpotReportID

--Begin procedure reporting.GetSpotReportIncidentBySpotReportID
EXEC Utility.DropObject 'reporting.GetSpotReportIncidentBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to data from the dbo.SpotReportIncident table
-- =============================================================================
CREATE PROCEDURE reporting.GetSpotReportIncidentBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
		
END
GO
--End procedure dbo.GetSpotReportIncidentBySpotReportID

--Begin procedure reporting.GetSpotReportProvinceBySpotReportID
EXEC Utility.DropObject 'reporting.GetSpotReportProvinceBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to data from the dbo.SpotReportProvince table
-- =============================================================================
CREATE PROCEDURE reporting.GetSpotReportProvinceBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID
		
END
GO
--End procedure dbo.GetSpotReportProvinceBySpotReportID

--Begin procedure reporting.GetWeeklyReportCommunity
EXEC Utility.DropObject 'reporting.GetWeeklyReportCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.03.06
-- Description:	Changed the CommunityReportStatusName logic
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportCommunity

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityName, 
		C.Summary, 
		C.KeyPoints, 
		C.Implications, 
		C.RiskMitigation,
		C.WeeklyReportID,	
		reporting.IsDraftReport('WeeklyReport', C.WeeklyReportID) AS IsDraft,
		dbo.FormatWeeklyReportReferenceCode(C.WeeklyReportID) AS ReferenceCode,
		CES.CommunityEngagementStatusName,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,
		
		/* OLD Logic changed via dmsam-296
		CASE
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID IN (2,3)
			THEN 'Watch'
			WHEN C.StatusChangeID IN (1,3) AND C.ImpactDecisionID = 4
			THEN 'Engage'
			ELSE 'Alert'			
		END AS CommunityReportStatusName

		CASE
			WHEN C.StatusChangeID IN (0,1,2,3,5) AND C.ImpactDecisionID IN (2,3)
			THEN 'Watch'
			WHEN C.StatusChangeID IN (4) AND C.ImpactDecisionID = 4
			THEN 'Engage'
			ELSE 'Alert'			
		END AS CommunityReportStatusName
		*/

		CASE
			WHEN C.ImpactDecisionID IN (0,1,2,3,5)
			THEN 'Watch'
			WHEN C.ImpactDecisionID IN (4) 
			THEN 'Engage'
			ELSE 'Alert'			
		END AS CommunityReportStatusName	

	FROM weeklyreport.Community C
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = C.StatusChangeID
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure reporting.GetWeeklyReportCommunity

--Begin procedure reporting.GetWeeklyReportProvince
EXEC Utility.DropObject 'reporting.GetWeeklyReportProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.11
-- Description:	A stored procedure to get data for the weekly report
--
-- Author:			Todd Pires
-- Update date:	2015.03.06
-- Description:	Spawned from the old reporting.GetWeeklyReport stored procedure
--
-- Author:			John Lyons
-- Update date:	2015.04.27
-- Description:	Added the dbo.FormatProvinceIncidentsForGoogleMap call
-- ============================================================================
CREATE PROCEDURE reporting.GetWeeklyReportProvince

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceName, 
		P.Summary, 
		P.KeyPoints, 
		P.Implications, 
		P.RiskMitigation,
		P.WeeklyReportID,
		reporting.IsDraftReport('WeeklyReport', P.WeeklyReportID) AS IsDraft,
		dbo.FormatWeeklyReportReferenceCode(P.WeeklyReportID) AS ReferenceCode,
		ID.HexColor AS ImpactDecisionHexColor,
		ID.ImpactDecisionName,
		SC.StatusChangeName,
		SC.HexColor AS StatusChangeHexColor,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS Icon,
		dbo.FormatStaticGoogleMapForWeeklyReport(P.ProvinceID) as MapImage
	FROM weeklyreport.Province P
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = P.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = P.StatusChangeID
	ORDER BY P.ProvinceName, P.ProvinceID

END
GO
--End procedure reporting.GetWeeklyReportProvince

--Begin procedure weeklyreport.GetProgramReportByProgramReportID
EXEC Utility.DropObject 'weeklyreport.GetProgramReportByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.11
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
-- =================================================================================
CREATE PROCEDURE weeklyreport.GetProgramReportByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		PR.EngagementPrevious,
		PR.EngagementProjected,
		PR.JusticePrevious,
		PR.JusticeProjected,
		PR.MonitoringPrevious,
		PR.MonitoringProjected,
		PR.OperationsPrevious,
		PR.OperationsProjected,
		PR.OverallPrevious,
		PR.OverallProjected,
		PR.PolicingPrevious,
		PR.PolicingProjected,
		dbo.FormatProgramReportReferenceCode(PR.ProgramReportID) AS ProgramReportReferenceCode,
		PR.ProgramReportEndDate,
		dbo.FormatDate(PR.ProgramReportEndDate) AS ProgramReportEndDateFormatted,
		PR.ProgramReportID,
		PR.ProgramReportName,
		PR.ProgramReportStartDate,
		dbo.FormatDate(PR.ProgramReportStartDate) AS ProgramReportStartDateFormatted,
		PR.Remarks,
		PR.StructuresPrevious,
		PR.StructuresProjected,
		PR.WorkflowStepNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('ProgramReport') AS EntityTypeName
	FROM weeklyreport.ProgramReport PR
	WHERE PR.ProgramReportID = @ProgramReportID

	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'ProgramReport'
			AND DE.EntityID = @ProgramReportID
	ORDER BY D.DocumentDescription

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ProgramReport'
			JOIN weeklyreport.ProgramReport PR ON PR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND PR.ProgramReportID = @ProgramReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ProgramReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ProgramReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ProgramReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT PR.WorkflowStepNumber FROM weeklyreport.ProgramReport PR WHERE PR.ProgramReportID = @ProgramReportID) > 0
					THEN (SELECT PR.WorkflowStepNumber FROM weeklyreport.ProgramReport PR WHERE PR.ProgramReportID = @ProgramReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure weeklyreport.GetProgramReportByProgramReportID

--Begin procedure workflow.CanIncrementProgramReportWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementProgramReportWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementProgramReportWorkflow

@EntityID INT


AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ProgramReport'
			JOIN weeklyreport.ProgramReport PR ON PR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND PR.ProgramReportID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementProgramReportWorkflow

--Begin procedure workflow.GetProgramReportWorkflowData
EXEC Utility.DropObject 'workflow.GetProgramReportWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.11
-- Description:	A procedure to return workflow data from the weeklyreport.ProgramReport table
-- ==========================================================================================

CREATE PROCEDURE workflow.GetProgramReportWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ProgramReport'
			JOIN weeklyreport.ProgramReport PR ON PR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND PR.ProgramReportID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'ProgramReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT PR.WorkflowStepNumber FROM weeklyreport.ProgramReport PR WHERE PR.ProgramReportID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'ProgramReport'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Program Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Program Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Program Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Program Report'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ProgramReport'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure workflow.GetProgramReportWorkflowData

--Begin procedure workflow.GetProgramReportWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetProgramReportWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.12
-- Description:	A stored procedure to people associated with the workflow steps on a Program report
-- ================================================================================================
CREATE PROCEDURE workflow.GetProgramReportWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ProgramReport'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT WR.WorkflowStepNumber FROM weeklyreport.ProgramReport WR WHERE WR.ProgramReportID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.ProgramReport WR WHERE WR.ProgramReportID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'ProgramReport.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetProgramReportWorkflowStepPeople