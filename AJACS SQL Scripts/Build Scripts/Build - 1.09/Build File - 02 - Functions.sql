USE AJACS
GO

--Begin function dbo.FormatProgramReportReferenceCode
EXEC utility.DropObject 'dbo.FormatProgramReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted program report reference code
-- ===========================================================================

CREATE FUNCTION dbo.FormatProgramReportReferenceCode
(
@ProgramReportID INT
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN 'AJACS-PR-' + RIGHT('0000' + CAST(@ProgramReportID AS VARCHAR(10)), 4)

END
GO
--End function dbo.FormatProgramReportReferenceCode

--Begin function dbo.FormatSpotReportProvinceCommunitiesForGoogleMap
EXEC utility.DropObject 'dbo.FormatSpotReportProvinceCommunitiesForGoogleMap'
GO
--End function dbo.FormatWeeklyReportProvinceCommunitiesForGoogleMap

--Begin function dbo.FormatWeeklyReportProvinceCommunitiesForGoogleMap
EXEC utility.DropObject 'dbo.FormatProvinceCommunitiesForGoogleMap'
EXEC utility.DropObject 'dbo.FormatWeeklyReportProvinceCommunitiesForGoogleMap'
GO
--End function dbo.FormatWeeklyReportProvinceCommunitiesForGoogleMap

--Begin function dbo.FormatProvinceIncidentsForGoogleMap
EXEC utility.DropObject 'dbo.FormatProvinceIncidentsForGoogleMap'
GO
--End function dbo.FormatProvinceIncidentsForGoogleMap

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN 'AJACS-SR-' + RIGHT('0000' + CAST(@SpotReportID AS VARCHAR(10)), 4)

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function dbo.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(C.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(C.Longitude AS VARCHAR(MAX))
	 FROM SpotReportCommunity SRC
		JOIN dbo.Community C ON SRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND SRC.SpotReportID = @SpotReportID
		

	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + IT.Icon + '|' 
			+ CAST(I.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(I.Longitude AS VARCHAR(MAX))
	 FROM SpotReportIncident SRI
		JOIN dbo.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForSpotReport

--Begin function dbo.FormatStaticGoogleMapForWeeklyReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForWeeklyReport
(
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = '', @Gresult2 varchar(max) =''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=600x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(C.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(C.Longitude AS VARCHAR(MAX))
	 FROM WeeklyReport.Community WRC
		JOIN dbo.Community C ON WRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.ProvinceID = @ProvinceID

	SELECT
		@Gresult2 +='&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + it.icon + '|' 
			+ CAST(I.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(I.Longitude AS VARCHAR(MAX))


			FROM dbo.Incident I
			JOIN Dropdown.IncidentType IT ON i.incidenttypeid = it.incidenttypeid
			AND I.IncidentDate >= dbo.GetDateByReferenceDateDayAbbreviationAndOffset (getdate(), 'FRI', -1)
				AND EXISTS
					(
					SELECT 1
					FROM dbo.IncidentCommunity IC
					WHERE IC.IncidentID = I.IncidentID 
						AND EXISTS
							(
							SELECT 1
							FROM weeklyreport.Community C 
							WHERE C.CommunityID = IC.CommunityID AND C.ProvinceID = @ProvinceID
							)

					UNION

					SELECT 1
					FROM dbo.IncidentProvince IP
					WHERE IP.IncidentID = I.IncidentID AND  IP.ProvinceID = @ProvinceID
						AND EXISTS
							(
							SELECT 1
							FROM weeklyreport.Province P 
							WHERE P.ProvinceID = IP.ProvinceID AND P.ProvinceID = @ProvinceID
							)
					)

	RETURN RTRIM(@GLink + @GResult + @Gresult2)

END
GO
--End function dbo.FormatStaticGoogleMapForWeeklyReport

--Begin function dbo.FormatWeeklyReportReferenceCode
EXEC utility.DropObject 'dbo.FormatWeeklyReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return a formatted Weekly report reference code
-- ==========================================================================

CREATE FUNCTION dbo.FormatWeeklyReportReferenceCode
(
@WeeklyReportID INT
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN 'AJACS-WR-' + RIGHT('0000' + CAST(@WeeklyReportID AS VARCHAR(10)), 4)

END
GO
--End function dbo.FormatWeeklyReportReferenceCode

--Begin function dbo.GetDateByReferenceDateDayAbbreviationAndOffset
EXEC utility.DropObject 'dbo.GetDateByReferenceDateDayAbbreviationAndOffset'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.29
-- Description:	A function to return a date before or after a reference date based on a day abbreviation and an offset
-- ====================================================================================================================

CREATE FUNCTION dbo.GetDateByReferenceDateDayAbbreviationAndOffset
(
@ReferenceDate DATE,
@DayAbbreviation CHAR(3),
@Offset INT
)

RETURNS DATE

AS
BEGIN

	DECLARE @dReturn DATE
	
	SELECT TOP 1 @dReturn = D.CalendarDate
	FROM
		(
		SELECT TOP (ABS(@Offset)) C.CalendarDate
		FROM AJACSUtility.dbo.Calendar C
		WHERE C.CalendarDayAbbreviation = @DayAbbreviation
			AND
				(
				(@Offset < 0 AND C.CalendarDate < @ReferenceDate) OR (@Offset > 0 AND C.CalendarDate > @ReferenceDate)
				)
		ORDER BY 
			CASE WHEN @Offset < 0 THEN C.CalendarDate END DESC,
			CASE WHEN @Offset > 0 THEN C.CalendarDate END
		) D
	ORDER BY 
		CASE WHEN @Offset < 0 THEN D.CalendarDate END,
		CASE WHEN @Offset > 0 THEN D.CalendarDate END DESC

	RETURN @dReturn

END
GO
--End function dbo.GetDateByReferenceDateDayAbbreviationAndOffset

--Begin function dbo.GetEventNameByEventCode
EXEC utility.DropObject 'dbo.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
-- ====================================================================
CREATE FUNCTION dbo.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'read'
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			ELSE ''
		END

	RETURN @EventCodeName

END
GO
--End function dbo.GetEventNameByEventCode

--Begin function dbo.GetSpotReportCommunitiesList
EXEC utility.DropObject 'dbo.GetSpotReportCommunitiesList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.28
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.GetSpotReportCommunitiesList
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @CommunityList VARCHAR(MAX) = ''
	
	SELECT @CommunityList += C.CommunityName + ', '
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON SRC.CommunityID = C.CommunityID 
			AND	SRC.SpotReportID = @SpotReportID
	
	IF LEN(RTRIM(@CommunityList)) > 0 
		SET @CommunityList = RTRIM(SUBSTRING(@CommunityList, 0, LEN(@CommunityList)))
	--ENDIF

	RETURN RTRIM(@CommunityList)

END
GO
--End function dbo.GetSpotReportCommunitiesList

--Begin function dbo.GetSpotReportProvincesList
EXEC utility.DropObject 'dbo.GetSpotReportProvincesList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.28
-- Description:	A function to return provinces for placement on a google map
-- =========================================================================
CREATE FUNCTION dbo.GetSpotReportProvincesList
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @ProvinceList VARCHAR(MAX) = ''
	
	SELECT @ProvinceList += P.ProvinceName + ', '
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON SRP.ProvinceID = P.ProvinceID 
			AND	SRP.SpotReportID = @SpotReportID
	
	IF LEN(RTRIM(@ProvinceList)) > 0 
		SET @ProvinceList = RTRIM(SUBSTRING(@ProvinceList, 0, LEN(@ProvinceList)))
	--ENDIF

	RETURN RTRIM(@ProvinceList)

END
GO
--End function dbo.GetSpotReportProvincesList
