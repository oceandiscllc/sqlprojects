USE AJACS
GO

--Begin schema justiceupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'justiceupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA justiceupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema justiceupdate