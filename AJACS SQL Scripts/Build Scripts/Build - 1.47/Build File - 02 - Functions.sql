USE AJACS
GO

--Begin function eventlog.GetCommunityXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for communities
-- ==========================================================

CREATE FUNCTION eventlog.GetCommunityXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM justiceupdate.Community T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByJusticeUpdateID

--Begin function eventlog.GetCommunityClassXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityClassXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community classes
-- ================================================================

CREATE FUNCTION eventlog.GetCommunityClassXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityClasses VARCHAR(MAX) = ''
	
	SELECT @cCommunityClasses = COALESCE(@cCommunityClasses, '') + D.CommunityClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityClass'), ELEMENTS) AS CommunityClass
		FROM justiceupdate.CommunityClass T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityClasses>' + ISNULL(@cCommunityClasses, '') + '</CommunityClasses>'
	
END
GO
--End function eventlog.GetCommunityClassXMLByJusticeUpdateID

--Begin function eventlog.GetCommunityIndicatorXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community indicators
-- ===================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM justiceupdate.CommunityIndicator T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'
	
END
GO
--End function eventlog.GetCommunityIndicatorXMLByJusticeUpdateID

--Begin function eventlog.GetCommunityRiskXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community ridks
-- ==============================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM justiceupdate.CommunityRisk T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'
	
END
GO
--End function eventlog.GetCommunityRiskXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for provinces
-- ========================================================

CREATE FUNCTION eventlog.GetProvinceXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM justiceupdate.Province T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceClassXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceClassXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province classes
-- ================================================================

CREATE FUNCTION eventlog.GetProvinceClassXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceClasses VARCHAR(MAX) = ''
	
	SELECT @cProvinceClasses = COALESCE(@cProvinceClasses, '') + D.ProvinceClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceClass'), ELEMENTS) AS ProvinceClass
		FROM justiceupdate.ProvinceClass T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceClasses>' + ISNULL(@cProvinceClasses, '') + '</ProvinceClasses>'
	
END
GO
--End function eventlog.GetProvinceClassXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province indicators
-- ===================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM justiceupdate.ProvinceIndicator T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'
	
END
GO
--End function eventlog.GetProvinceIndicatorXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceRiskXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province ridks
-- ==============================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM justiceupdate.ProvinceRisk T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'
	
END
GO
--End function eventlog.GetProvinceRiskXMLByJusticeUpdateID
