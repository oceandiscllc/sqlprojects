USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'JusticeAssessmentNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeClassNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusNotes', 'VARCHAR(MAX)'
	
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
GO
--End table dbo.Community

--Begin table dbo.CommunityClass
EXEC utility.AddColumn 'dbo.CommunityClass', 'JusticeNotes', 'VARCHAR(MAX)'
GO
--End table dbo.CommunityClass

--Begin table dbo.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityIndicator'

EXEC utility.AddColumn @TableName, 'JusticeAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
GO
--End table dbo.CommunityIndicator

--Begin table dbo.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRisk'

EXEC utility.AddColumn @TableName, 'JusticeRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
GO
--End table dbo.CommunityRisk

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'JusticeAssessmentNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeClassNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusNotes', 'VARCHAR(MAX)'
	
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
GO
--End table dbo.Province

--Begin table dbo.ProvinceClass
EXEC utility.AddColumn 'dbo.ProvinceClass', 'JusticeNotes', 'VARCHAR(MAX)'
GO
--End table dbo.ProvinceClass

--Begin table dbo.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceIndicator'

EXEC utility.AddColumn @TableName, 'JusticeAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
GO
--End table dbo.ProvinceIndicator

--Begin table dbo.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceRisk'

EXEC utility.AddColumn @TableName, 'JusticeRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
GO
--End table dbo.ProvinceRisk

--Begin table justiceupdate.Community
DECLARE @TableName VARCHAR(250) = 'justiceupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.Community
	(
	CommunityID INT,
	JusticeUpdateID INT,
	JusticeCommunityDocumentCenterStatusID INT,
	JusticeAssessmentNotes VARCHAR(MAX),
	JusticeCommunityDocumentCenterStatusNotes VARCHAR(MAX),
	JusticeClassNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table justiceupdate.Community

--Begin table justiceupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	JusticeAchievedValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'JusticeUpdateID,CommunityID'
GO
--End table justiceupdate.CommunityIndicator

--Begin table justiceupdate.CommunityClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityClass'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityClass
	(
	CommunityClassID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	ClassID INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityClassID'
EXEC utility.SetIndexClustered 'IX_CommunityClass', @TableName, 'JusticeUpdateID,CommunityID'
GO
--End table justiceupdate.CommunityClass

--Begin table justiceupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	RiskID INT,
	JusticeRiskValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'JusticeUpdateID,CommunityID'
GO
--End table justiceupdate.CommunityRisk

--Begin table justiceupdate.JusticeUpdate
DECLARE @TableName VARCHAR(250) = 'justiceupdate.JusticeUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.JusticeUpdate
	(
	JusticeUpdateID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepNumber INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'JusticeUpdateID'
GO
--End table justiceupdate.JusticeUpdate

--Begin table justiceupdate.Province
DECLARE @TableName VARCHAR(250) = 'justiceupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.Province
	(
	ProvinceID INT,
	JusticeUpdateID INT,
	JusticeCommunityDocumentCenterStatusID INT,
	JusticeAssessmentNotes VARCHAR(MAX),
	JusticeCommunityDocumentCenterStatusNotes VARCHAR(MAX),
	JusticeClassNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table justiceupdate.Province

--Begin table justiceupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	JusticeAchievedValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'JusticeUpdateID,ProvinceID'
GO
--End table justiceupdate.ProvinceIndicator

--Begin table justiceupdate.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceClass'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceClass
	(
	ProvinceClassID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	ClassID INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceClassID'
EXEC utility.SetIndexClustered 'IX_ProvinceClass', @TableName, 'JusticeUpdateID,ProvinceID'
GO
--End table justiceupdate.ProvinceClass

--Begin table justiceupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	JusticeRiskValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'JusticeUpdateID,ProvinceID'
GO
--End table justiceupdate.CommunityRisk
