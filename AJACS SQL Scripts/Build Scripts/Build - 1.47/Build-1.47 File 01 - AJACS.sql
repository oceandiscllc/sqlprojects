-- File Name:	Build-1.47 File 01 - AJACS.sql
-- Build Key:	Build-1.47 File 01 - AJACS - 2016.02.16 12.01.42

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		eventlog.GetCommunityClassXMLByJusticeUpdateID
--		eventlog.GetCommunityIndicatorXMLByJusticeUpdateID
--		eventlog.GetCommunityRiskXMLByJusticeUpdateID
--		eventlog.GetCommunityXMLByJusticeUpdateID
--		eventlog.GetProvinceClassXMLByJusticeUpdateID
--		eventlog.GetProvinceIndicatorXMLByJusticeUpdateID
--		eventlog.GetProvinceRiskXMLByJusticeUpdateID
--		eventlog.GetProvinceXMLByJusticeUpdateID
--
-- Procedures:
--		dbo.GetDonorFeed
--		dropdown.GetCommunityDocumentCenterStatusData
--		eventlog.LogJusticeUpdateAction
--		force.GetForceByEventLogID
--		justiceupdate.AddJusticeCommunity
--		justiceupdate.AddJusticeProvince
--		justiceupdate.ApproveJusticeUpdate
--		justiceupdate.DeleteJusticeCommunity
--		justiceupdate.DeleteJusticeProvince
--		justiceupdate.GetCommunityByCommunityID
--		justiceupdate.GetJusticeUpdate
--		justiceupdate.GetProvinceByProvinceID
--		procurement.GetEquipmentDistributionPlanByConceptNoteID
--		workflow.CanIncrementJusticeUpdateWorkflow
--		workflow.GetJusticeUpdateWorkflowData
--		workflow.GetJusticeUpdateWorkflowStepPeople
--
-- Tables:
--		dropdown.CommunityDocumentCenterStatus
--		justiceupdate.Community
--		justiceupdate.CommunityClass
--		justiceupdate.CommunityIndicator
--		justiceupdate.CommunityRisk
--		justiceupdate.JusticeUpdate
--		justiceupdate.Province
--		justiceupdate.ProvinceClass
--		justiceupdate.ProvinceIndicator
--		justiceupdate.ProvinceRisk
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin schema justiceupdate
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'justiceupdate')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA justiceupdate'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema justiceupdate
--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Community
DECLARE @TableName VARCHAR(250) = 'dbo.Community'

EXEC utility.AddColumn @TableName, 'JusticeAssessmentNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeClassNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusNotes', 'VARCHAR(MAX)'
	
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
GO
--End table dbo.Community

--Begin table dbo.CommunityClass
EXEC utility.AddColumn 'dbo.CommunityClass', 'JusticeNotes', 'VARCHAR(MAX)'
GO
--End table dbo.CommunityClass

--Begin table dbo.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityIndicator'

EXEC utility.AddColumn @TableName, 'JusticeAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
GO
--End table dbo.CommunityIndicator

--Begin table dbo.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityRisk'

EXEC utility.AddColumn @TableName, 'JusticeRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
GO
--End table dbo.CommunityRisk

--Begin table dbo.Province
DECLARE @TableName VARCHAR(250) = 'dbo.Province'

EXEC utility.AddColumn @TableName, 'JusticeAssessmentNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeClassNotes', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeCommunityDocumentCenterStatusNotes', 'VARCHAR(MAX)'
	
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
GO
--End table dbo.Province

--Begin table dbo.ProvinceClass
EXEC utility.AddColumn 'dbo.ProvinceClass', 'JusticeNotes', 'VARCHAR(MAX)'
GO
--End table dbo.ProvinceClass

--Begin table dbo.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceIndicator'

EXEC utility.AddColumn @TableName, 'JusticeAchievedValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
GO
--End table dbo.ProvinceIndicator

--Begin table dbo.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceRisk'

EXEC utility.AddColumn @TableName, 'JusticeRiskValue', 'INT'
EXEC utility.AddColumn @TableName, 'JusticeNotes', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
GO
--End table dbo.ProvinceRisk

--Begin table dropdown.CommunityDocumentCenterStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityDocumentCenterStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityDocumentCenterStatus
	(
	CommunityDocumentCenterStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityDocumentCenterStatusCode VARCHAR(50),
	CommunityDocumentCenterStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityDocumentCenterStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityDocumentCenterStatus', @TableName, 'DisplayOrder,CommunityDocumentCenterStatusName', 'CommunityDocumentCenterStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityDocumentCenterStatus ON
GO

INSERT INTO dropdown.CommunityDocumentCenterStatus (CommunityDocumentCenterStatusID, CommunityDocumentCenterStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityDocumentCenterStatus OFF
GO

INSERT INTO dropdown.CommunityDocumentCenterStatus 
	(CommunityDocumentCenterStatusCode,CommunityDocumentCenterStatusName,DisplayOrder)
VALUES
	(NULL,NULL,0),
	('NotCurrentlyPlanned','Not Currently Planned', 1),
	('Intended','Intended', 2),
	('Planning','Planning', 3),
	('Implementation','Implementation', 4),
	('CenterOpened','Center Opened', 5),
	('CenterClosed','Center Closed', 6)
GO	
--End table dropdown.CommunityDocumentCenterStatus

--Begin table justiceupdate.Community
DECLARE @TableName VARCHAR(250) = 'justiceupdate.Community'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.Community
	(
	CommunityID INT,
	JusticeUpdateID INT,
	JusticeCommunityDocumentCenterStatusID INT,
	JusticeAssessmentNotes VARCHAR(MAX),
	JusticeCommunityDocumentCenterStatusNotes VARCHAR(MAX),
	JusticeClassNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityID'
GO
--End table justiceupdate.Community

--Begin table justiceupdate.CommunityIndicator
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityIndicator
	(
	CommunityIndicatorID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	IndicatorID INT,
	JusticeAchievedValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityIndicatorID'
EXEC utility.SetIndexClustered 'IX_CommunityIndicator', @TableName, 'JusticeUpdateID,CommunityID'
GO
--End table justiceupdate.CommunityIndicator

--Begin table justiceupdate.CommunityClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityClass'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityClass
	(
	CommunityClassID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	ClassID INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityClassID'
EXEC utility.SetIndexClustered 'IX_CommunityClass', @TableName, 'JusticeUpdateID,CommunityID'
GO
--End table justiceupdate.CommunityClass

--Begin table justiceupdate.CommunityRisk
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityRisk'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityRisk
	(
	CommunityRiskID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	RiskID INT,
	JusticeRiskValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityRiskID'
EXEC utility.SetIndexClustered 'IX_CommunityRisk', @TableName, 'JusticeUpdateID,CommunityID'
GO
--End table justiceupdate.CommunityRisk

--Begin table justiceupdate.JusticeUpdate
DECLARE @TableName VARCHAR(250) = 'justiceupdate.JusticeUpdate'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.JusticeUpdate
	(
	JusticeUpdateID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepNumber INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'JusticeUpdateID'
GO
--End table justiceupdate.JusticeUpdate

--Begin table justiceupdate.Province
DECLARE @TableName VARCHAR(250) = 'justiceupdate.Province'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.Province
	(
	ProvinceID INT,
	JusticeUpdateID INT,
	JusticeCommunityDocumentCenterStatusID INT,
	JusticeAssessmentNotes VARCHAR(MAX),
	JusticeCommunityDocumentCenterStatusNotes VARCHAR(MAX),
	JusticeClassNotes VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeCommunityDocumentCenterStatusID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProvinceID'
GO
--End table justiceupdate.Province

--Begin table justiceupdate.ProvinceIndicator
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceIndicator'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceIndicator
	(
	ProvinceIndicatorID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	IndicatorID INT,
	JusticeAchievedValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IndicatorID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeAchievedValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceIndicatorID'
EXEC utility.SetIndexClustered 'IX_ProvinceIndicator', @TableName, 'JusticeUpdateID,ProvinceID'
GO
--End table justiceupdate.ProvinceIndicator

--Begin table justiceupdate.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceClass'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceClass
	(
	ProvinceClassID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	ClassID INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClassID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceClassID'
EXEC utility.SetIndexClustered 'IX_ProvinceClass', @TableName, 'JusticeUpdateID,ProvinceID'
GO
--End table justiceupdate.ProvinceClass

--Begin table justiceupdate.ProvinceRisk
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceRisk
	(
	ProvinceRiskID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	RiskID INT,
	JusticeRiskValue INT,
	JusticeNotes VARCHAR(MAX),
	)

EXEC utility.SetDefaultConstraint @TableName, 'JusticeRiskValue', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceRiskID'
EXEC utility.SetIndexClustered 'IX_ProvinceRisk', @TableName, 'JusticeUpdateID,ProvinceID'
GO
--End table justiceupdate.CommunityRisk

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function eventlog.GetCommunityXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for communities
-- ==========================================================

CREATE FUNCTION eventlog.GetCommunityXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunities VARCHAR(MAX) = ''
	
	SELECT @cCommunities = COALESCE(@cCommunities, '') + D.Community
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Community'), ELEMENTS) AS Community
		FROM justiceupdate.Community T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<Communities>' + ISNULL(@cCommunities, '') + '</Communities>'

END
GO
--End function eventlog.GetCommunityXMLByJusticeUpdateID

--Begin function eventlog.GetCommunityClassXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityClassXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community classes
-- ================================================================

CREATE FUNCTION eventlog.GetCommunityClassXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityClasses VARCHAR(MAX) = ''
	
	SELECT @cCommunityClasses = COALESCE(@cCommunityClasses, '') + D.CommunityClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityClass'), ELEMENTS) AS CommunityClass
		FROM justiceupdate.CommunityClass T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityClasses>' + ISNULL(@cCommunityClasses, '') + '</CommunityClasses>'
	
END
GO
--End function eventlog.GetCommunityClassXMLByJusticeUpdateID

--Begin function eventlog.GetCommunityIndicatorXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityIndicatorXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community indicators
-- ===================================================================

CREATE FUNCTION eventlog.GetCommunityIndicatorXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityIndicators VARCHAR(MAX) = ''
	
	SELECT @cCommunityIndicators = COALESCE(@cCommunityIndicators, '') + D.CommunityIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityIndicator'), ELEMENTS) AS CommunityIndicator
		FROM justiceupdate.CommunityIndicator T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityIndicators>' + ISNULL(@cCommunityIndicators, '') + '</CommunityIndicators>'
	
END
GO
--End function eventlog.GetCommunityIndicatorXMLByJusticeUpdateID

--Begin function eventlog.GetCommunityRiskXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityRiskXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community ridks
-- ==============================================================

CREATE FUNCTION eventlog.GetCommunityRiskXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityRisks VARCHAR(MAX) = ''
	
	SELECT @cCommunityRisks = COALESCE(@cCommunityRisks, '') + D.CommunityRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityRisk'), ELEMENTS) AS CommunityRisk
		FROM justiceupdate.CommunityRisk T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityRisks>' + ISNULL(@cCommunityRisks, '') + '</CommunityRisks>'
	
END
GO
--End function eventlog.GetCommunityRiskXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for provinces
-- ========================================================

CREATE FUNCTION eventlog.GetProvinceXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinces VARCHAR(MAX) = ''
	
	SELECT @cProvinces = COALESCE(@cProvinces, '') + D.Province
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('Province'), ELEMENTS) AS Province
		FROM justiceupdate.Province T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<Provinces>' + ISNULL(@cProvinces, '') + '</Provinces>'

END
GO
--End function eventlog.GetProvinceXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceClassXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceClassXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province classes
-- ================================================================

CREATE FUNCTION eventlog.GetProvinceClassXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceClasses VARCHAR(MAX) = ''
	
	SELECT @cProvinceClasses = COALESCE(@cProvinceClasses, '') + D.ProvinceClass
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceClass'), ELEMENTS) AS ProvinceClass
		FROM justiceupdate.ProvinceClass T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceClasses>' + ISNULL(@cProvinceClasses, '') + '</ProvinceClasses>'
	
END
GO
--End function eventlog.GetProvinceClassXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceIndicatorXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceIndicatorXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province indicators
-- ===================================================================

CREATE FUNCTION eventlog.GetProvinceIndicatorXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceIndicators VARCHAR(MAX) = ''
	
	SELECT @cProvinceIndicators = COALESCE(@cProvinceIndicators, '') + D.ProvinceIndicator
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceIndicator'), ELEMENTS) AS ProvinceIndicator
		FROM justiceupdate.ProvinceIndicator T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceIndicators>' + ISNULL(@cProvinceIndicators, '') + '</ProvinceIndicators>'
	
END
GO
--End function eventlog.GetProvinceIndicatorXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceRiskXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceRiskXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province ridks
-- ==============================================================

CREATE FUNCTION eventlog.GetProvinceRiskXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceRisks VARCHAR(MAX) = ''
	
	SELECT @cProvinceRisks = COALESCE(@cProvinceRisks, '') + D.ProvinceRisk
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceRisk'), ELEMENTS) AS ProvinceRisk
		FROM justiceupdate.ProvinceRisk T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceRisks>' + ISNULL(@cProvinceRisks, '') + '</ProvinceRisks>'
	
END
GO
--End function eventlog.GetProvinceRiskXMLByJusticeUpdateID

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetDonorFeed
EXEC Utility.DropObject 'dbo.GetDonorFeed'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data for the donor feed
--
-- Author:			Todd Pires
-- Create Date: 2015.05.27
-- Description:	Bug fixes
--
-- Author:			Todd Pires
-- Create Date: 2015.09.27
-- Description:	Added Recommendations
--
-- Author:			Todd Pires
-- Create Date: 2016.02.11
-- Description:	Added Program Report
-- ==============================================================
CREATE PROCEDURE dbo.GetDonorFeed

@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN 'fa fa-fw fa-lightbulb-o'
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN 'fa fa-fw fa-question-circle'
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN 'fa fa-fw fa-bolt'
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN 'fa fa-fw fa-calendar'
		END AS Icon,

		ET.EntityTypeName,

		CASE
			WHEN D.EntityTypeCode = 'Recommendation'
			THEN OAR.RecommendationName
			WHEN D.EntityTypeCode = 'RequestForInformation'
			THEN OARFI.RequestForInformationTitle
			WHEN D.EntityTypeCode = 'SpotReport'
			THEN OASR.SpotReportTitle
			ELSE OAWR.DocumentTitle
		END AS Title,

		CASE
			WHEN ET.EntityTypeCode = 'WeeklyReport'
			THEN OAWR.PhysicalFileName
			ELSE NULL
		END AS PhysicalFileName,

		D.EntityID,
		D.UpdateDate,
		dbo.FormatDate(D.UpdateDate) AS UpdateDateFormatted
	FROM
		(
		SELECT
			MAX(EL.CreateDateTime) AS UpdateDate,
			EL.EntityTypeCode,
			EL.EntityID
		FROM eventlog.EventLog EL
		WHERE EL.EntityTypeCode IN ('Recommendation','RequestForInformation','SpotReport','WeeklyReport')
			AND EL.EventCode <> 'read'
			AND EL.PersonID > 0
			AND EL.CreateDateTime >= DATEADD(d, -14, getDate())
			AND
				(
					(EL.EntityTypeCode = 'Recommendation' AND permissionable.HasPermission('Recommendation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'RequestForInformation' AND permissionable.HasPermission('RequestForInformation.View', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'SpotReport' AND permissionable.HasPermission('SpotReport.View.Approved', @PersonID) = 1)
						OR (EL.EntityTypeCode = 'WeeklyReport' AND permissionable.HasPermission('WeeklyReport.View', @PersonID) = 1)
				)
		GROUP BY EL.EntityTypeCode, EL.EntityID
		) D
		OUTER APPLY
			(
			SELECT
				R.RecommendationName,
				R.IsActive
			FROM recommendation.Recommendation R
			WHERE R.RecommendationID = D.EntityID
					AND D.EntityTypeCode = 'Recommendation'
			) OAR
		OUTER APPLY
			(
			SELECT
				RFI.RequestForInformationTitle,
				RFIS.RequestForInformationStatusCode
			FROM dropdown.RequestForInformationStatus RFIS
				JOIN dbo.RequestForInformation RFI ON RFI.RequestForInformationStatusID = RFIS.RequestForInformationStatusID
					AND RFIS.RequestForInformationStatusCode = 'Completed'
					AND RFI.RequestForInformationID = D.EntityID
					AND D.EntityTypeCode = 'RequestForInformation'
			) OARFI
		OUTER APPLY
			(
			SELECT
				SR.SpotReportTitle,
				SR.WorkflowStepNumber
			FROM dbo.SpotReport SR
			WHERE SR.SpotReportID = D.EntityID
				AND D.EntityTypeCode = 'SpotReport'
			) OASR
		OUTER APPLY
			(
			SELECT
				DOC.DocumentTitle,
				DOC.PhysicalFileName
			FROM dbo.Document DOC
			WHERE DOC.DocumentTitle LIKE '%' + dbo.FormatWeeklyReportReferenceCode(D.EntityID) + '%'
			) OAWR
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = D.EntityTypeCode
			AND
				(
				D.EntityTypeCode <> 'Recommendation'
					OR OAR.IsActive = 1
				)
			AND
				(
				D.EntityTypeCode <> 'RequestForInformation'
					OR OARFI.RequestForInformationStatusCode = 'Completed'
				)
			AND
				(
				D.EntityTypeCode <> 'SpotReport'
					OR OASR.WorkflowStepNumber > (SELECT W.WorkflowStepCount FROM workflow.Workflow W WHERE W.EntityTypeCode = 'SpotReport')
				)
			AND
				(
				D.EntityTypeCode <> 'WeeklyReport'
					OR OAWR.PhysicalFileName IS NOT NULL
				)

	UNION
	
	SELECT
		ET.EntityTypeCode,
		LOWER(ET.EntityTypeCode) AS Controller,
		'fa-newspaper-o' AS Icon,
		ET.EntityTypeName,
		D.DocumentTitle AS Title,
		D.PhysicalFileName,
		0 AS EntityID,
		D.DocumentDate AS UpdateDate,
		dbo.FormatDate(D.DocumentDate) AS UpdateDateFormatted
	FROM dbo.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DT.DocumentTypeCode = 'ProgramReport'
		JOIN dbo.EntityType ET ON ET.EntityTypeCode = DT.DocumentTypeCode
			AND D.DocumentDate >= DATEADD(d, -14, getDate())
			AND permissionable.HasPermission('ProgramReport.View', @PersonID) = 1

	ORDER BY 8 DESC, 1, 7
	
END
GO
--End procedure dbo.GetDonorFeed

--Begin procedure dropdown.GetCommunityDocumentCenterStatusData
EXEC utility.DropObject 'dropdown.GetCommunityDocumentCenterStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.24
-- Description:	A stored procedure to return data from the dropdown.ContactVetting table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetCommunityDocumentCenterStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CommunityDocumentCenterStatusID, 
		T.CommunityDocumentCenterStatusCode, 
		T.CommunityDocumentCenterStatusName
	FROM dropdown.CommunityDocumentCenterStatus T
	WHERE (T.CommunityDocumentCenterStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CommunityDocumentCenterStatusName, T.CommunityDocumentCenterStatusID

END
GO
--End procedure dropdown.GetCommunityDocumentCenterStatusData

--Begin procedure eventlog.LogJusticeUpdateAction
EXEC Utility.DropObject 'eventlog.LogJusticeUpdateAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add data to the event log
-- ============================================================
CREATE PROCEDURE eventlog.LogJusticeUpdateAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'JusticeUpdate',
			T.JusticeUpdateID,
			@Comments
		FROM justiceupdate.JusticeUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.JusticeUpdateID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'JusticeUpdate',
			T.JusticeUpdateID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityClassXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceClassXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML))
			FOR XML RAW('JusticeUpdate'), ELEMENTS
			)
		FROM justiceupdate.JusticeUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.JusticeUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogJusticeUpdateAction

--Begin procedure force.GetForceByEventLogID
EXEC Utility.DropObject 'force.GetForceByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.29
-- Description:	A stored procedure to force data from the eventlog.EventLog table
-- Notes:				Changes here must ALSO be made to force.GetForceByForceID
-- ==============================================================================
CREATE PROCEDURE force.GetForceByEventLogID

@EventLogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		dbo.FormatContactNameByContactID(Force.value('CommanderContactID[1]', 'INT'), 'LastFirstMiddle') AS CommanderFullName,
		Force.value('Comments[1]', 'VARCHAR(250)') AS Comments,
		dbo.FormatContactNameByContactID(Force.value('DeputyCommanderContactID[1]', 'INT'), 'LastFirstMiddle') AS DeputyCommanderFullName,
		Force.value('ForceDescription[1]', 'VARCHAR(250)') AS ForceDescription,
		Force.value('ForceID[1]', 'INT') AS ForceID,
		Force.value('ForceName[1]', 'VARCHAR(250)') AS ForceName,
		Force.value('History[1]', 'VARCHAR(250)') AS History,
		Force.value('Location[1]', 'VARCHAR(MAX)') AS Location,
		Force.value('Notes[1]', 'VARCHAR(250)') AS Notes,
		Force.value('TerritoryID[1]', 'INT') AS TerritoryID,
		Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)'), Force.value('TerritoryID[1]', 'INT')) AS TerritoryName,
		Force.value('WebLinks[1]', 'VARCHAR(250)') AS WebLinks,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		AOO.AreaOfOperationTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force') AS T(Force)
		JOIN dropdown.AreaOfOperationType AOO ON AOO.AreaOfOperationTypeID = Force.value('AreaOfOperationTypeID[1]', 'INT')
			AND EL.EventLogID = @EventLogID

	SELECT
		Force.value('(CommunityID)[1]', 'INT') AS CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceCommunities/ForceCommunity') AS T(Force)
			JOIN dbo.Community C ON C.CommunityID = Force.value('(CommunityID)[1]', 'INT')
			JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
				AND EL.EventLogID = @EventLogID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceEquipmentResourceProviders/ForceEquipmentResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceFinancialResourceProviders/ForceFinancialResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(RiskID)[1]', 'INT') AS RiskID,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceRisks/ForceRisk') AS T(Force)
			JOIN dbo.Risk R ON R.RiskID = Force.value('(RiskID)[1]', 'INT')
			JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
				AND EL.EventLogID = @EventLogID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		Force.value('(ForceUnitID)[1]', 'INT') AS ForceUnitID,
		Force.value('(CommanderContactID)[1]', 'INT') AS CommanderContactID,
		Force.value('(DeputyCommanderContactID)[1]', 'INT') AS DeputyCommanderContactID,
		Force.value('(TerritoryID)[1]', 'INT') AS TerritoryID,
		Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)'), Force.value('(TerritoryID)[1]', 'INT')) AS TerritoryName,
		Force.value('(UnitName)[1]', 'VARCHAR(250)') AS UnitName,
		Force.value('(UnitTypeID)[1]', 'INT') AS UnitTypeID,
		UT.UnitTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceUnits/ForceUnit') AS T(Force)
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = Force.value('(UnitTypeID)[1]', 'INT')
				AND EL.EventLogID = @EventLogID
	ORDER BY Force.value('(UnitName)[1]', 'VARCHAR(250)'), Force.value('(ForceUnitID)[1]', 'INT')
	
END
GO
--End procedure force.GetForceByEventLogID

--Begin procedure justiceupdate.AddJusticeCommunity
EXEC Utility.DropObject 'justiceupdate.AddJusticeCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add communities to the justice update batch
-- ==============================================================================
CREATE PROCEDURE justiceupdate.AddJusticeCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)
	
	INSERT INTO justiceupdate.Community
		(CommunityID, JusticeUpdateID, JusticeAssessmentNotes, JusticeCommunityDocumentCenterStatusNotes, JusticeClassNotes, JusticeCommunityDocumentCenterStatusID, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@JusticeUpdateID,
		C1.JusticeAssessmentNotes, 
		C1.JusticeCommunityDocumentCenterStatusNotes, 
		C1.JusticeClassNotes, 
		C1.JusticeCommunityDocumentCenterStatusID, 
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM justiceupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO justiceupdate.CommunityIndicator
		(JusticeUpdateID, CommunityID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.JusticeAchievedValue, 0),
		OACI.JusticeNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'JO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.JusticeAchievedValue,
				CI.JusticeNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO justiceupdate.CommunityRisk
		(JusticeUpdateID, CommunityID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		D.CommunityID,
		R.RiskID,
		ISNULL(OACR.JusticeRiskValue, 0),
		OACR.JusticeNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RC.CommunityID
			FROM recommendation.RecommendationCommunity RC 
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC.RecommendationID
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				CR.JusticeRiskValue, 
				CR.JusticeNotes
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = D.CommunityID
			) OACR

	INSERT INTO justiceupdate.CommunityClass
		(JusticeUpdateID, CommunityID, ClassID, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OACC.JusticeNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
					JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNC2.CommunityID
						AND CNC2.ConceptNoteID = CNC1.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('JO2','JO3')
				)
			OUTER APPLY
				(
				SELECT
					CC.JusticeNotes
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = CAST(LTT.ListItem AS INT)
				) OACC

	EXEC eventlog.LogJusticeUpdateAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.AddJusticeCommunity

--Begin procedure justiceupdate.AddJusticeProvince
EXEC Utility.DropObject 'justiceupdate.AddJusticeProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add provincxes to the justice update batch
-- =============================================================================
CREATE PROCEDURE justiceupdate.AddJusticeProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)

	INSERT INTO justiceupdate.Province
		(ProvinceID, JusticeUpdateID, JusticeAssessmentNotes, JusticeCommunityDocumentCenterStatusNotes, JusticeClassNotes, JusticeCommunityDocumentCenterStatusID, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@JusticeUpdateID,
		P1.JusticeAssessmentNotes, 
		P1.JusticeCommunityDocumentCenterStatusNotes, 
		P1.JusticeClassNotes, 
		P1.JusticeCommunityDocumentCenterStatusID, 
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM justiceupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO justiceupdate.ProvinceIndicator
		(JusticeUpdateID, ProvinceID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.JusticeAchievedValue, 0),
		OAPI.JusticeNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'JO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.JusticeAchievedValue,
				PRI.JusticeNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO justiceupdate.ProvinceRisk
		(JusticeUpdateID, ProvinceID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		D.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.JusticeRiskValue, 0),
		OAPR.JusticeNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RP.ProvinceID
			FROM recommendation.RecommendationProvince RP
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				PR.JusticeRiskValue, 
				PR.JusticeNotes
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = D.ProvinceID
			) OAPR

	INSERT INTO justiceupdate.ProvinceClass
		(JusticeUpdateID, ProvinceID, ClassID, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		CL.ClassID, 
		OAPC.JusticeNotes
	FROM dbo.Class CL
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC ON CNC.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteProvince CNP
					JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CNP.ProvinceID
						AND CNP.ConceptNoteID = CNC.ConceptNoteID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode IN ('JO2','JO3')
				)
			OUTER APPLY
				(
				SELECT
					PC.JusticeNotes
				FROM dbo.ProvinceClass PC
				WHERE PC.ClassID = CL.ClassID
					AND PC.ProvinceID = CAST(LTT.ListItem AS INT)
				) OAPC

	EXEC eventlog.LogJusticeUpdateAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.AddJusticeProvince

--Begin procedure justiceupdate.ApproveJusticeUpdate
EXEC Utility.DropObject 'justiceupdate.ApproveJusticeUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to approve a justice update batch
-- =================================================================
CREATE PROCEDURE justiceupdate.ApproveJusticeUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nJusticeUpdateID INT = ISNULL((SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogJusticeUpdateAction @nJusticeUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogJusticeUpdateAction @nJusticeUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.JusticeAssessmentNotes = FU.JusticeAssessmentNotes,
		P.JusticeCommunityDocumentCenterStatusNotes = FU.JusticeCommunityDocumentCenterStatusNotes,
		P.JusticeClassNotes = FU.JusticeClassNotes,
		P.JusticeCommunityDocumentCenterStatusID = FU.JusticeCommunityDocumentCenterStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN justiceupdate.Province FU ON FU.ProvinceID = P.ProvinceID
			AND FU.JusticeUpdateID = @nJusticeUpdateID

	-- Province Indicator
	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.JusticeAchievedValue, 
		PRI.JusticeNotes
	FROM justiceupdate.ProvinceIndicator PRI

	-- Province Risk
	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.JusticeRiskValue, 
		PR.JusticeNotes
	FROM justiceupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE C
	SET
		C.JusticeAssessmentNotes = FU.JusticeAssessmentNotes,
		C.JusticeCommunityDocumentCenterStatusNotes = FU.JusticeCommunityDocumentCenterStatusNotes,
		C.JusticeClassNotes = FU.JusticeClassNotes,
		C.JusticeCommunityDocumentCenterStatusID = FU.JusticeCommunityDocumentCenterStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN justiceupdate.Community FU ON FU.CommunityID = C.CommunityID
			AND FU.JusticeUpdateID = @nJusticeUpdateID

	-- Community Indicator
	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.JusticeAchievedValue, 
		CI.JusticeNotes
	FROM justiceupdate.CommunityIndicator CI

	-- Community Risk
	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.JusticeRiskValue, 
		CR.JusticeNotes
	FROM justiceupdate.CommunityRisk CR

	DELETE PC
	FROM dbo.ProvinceClass PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO dbo.ProvinceClass
		(ProvinceID, ClassID, JusticeNotes)
	SELECT
		PC.ProvinceID,
		PC.ClassID,
		PC.JusticeNotes
	FROM justiceupdate.ProvinceClass PC

	DELETE CC
	FROM dbo.CommunityClass CC
		JOIN @tOutputCommunity O ON O.CommunityID = CC.CommunityID
	
	INSERT INTO dbo.CommunityClass
		(CommunityID, ClassID, JusticeNotes)
	SELECT
		CC.CommunityID,
		CC.ClassID,
		CC.JusticeNotes
	FROM justiceupdate.CommunityClass CC	

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	DELETE FROM justiceupdate.JusticeUpdate

	TRUNCATE TABLE justiceupdate.Province
	TRUNCATE TABLE justiceupdate.ProvinceIndicator
	TRUNCATE TABLE justiceupdate.ProvinceClass
	TRUNCATE TABLE justiceupdate.ProvinceRisk

	TRUNCATE TABLE justiceupdate.Community
	TRUNCATE TABLE justiceupdate.CommunityIndicator
	TRUNCATE TABLE justiceupdate.CommunityClass
	TRUNCATE TABLE justiceupdate.CommunityRisk

END
GO
--End procedure justiceupdate.ApproveJusticeUpdate

--Begin procedure justiceupdate.DeleteJusticeCommunity
EXEC Utility.DropObject 'justiceupdate.DeleteJusticeCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to remove communities from a justice update batch
-- =================================================================================
CREATE PROCEDURE justiceupdate.DeleteJusticeCommunity

@CommunityID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)

	DELETE T
	FROM justiceupdate.Community T
	WHERE T.CommunityID = @CommunityID
	
	DELETE T
	FROM justiceupdate.CommunityIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM justiceupdate.CommunityRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)

	DELETE T
	FROM justiceupdate.CommunityClass T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)

	EXEC eventlog.LogJusticeUpdateAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.DeleteJusticeCommunity

--Begin procedure justiceupdate.DeleteJusticeProvince
EXEC Utility.DropObject 'justiceupdate.DeleteJusticeProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to remove provinces from a justice update batch
-- ===============================================================================
CREATE PROCEDURE justiceupdate.DeleteJusticeProvince

@ProvinceID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)
	
	DELETE T
	FROM justiceupdate.Province T
	WHERE T.ProvinceID = @ProvinceID
	
	DELETE T
	FROM justiceupdate.ProvinceIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM justiceupdate.ProvinceRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)

	DELETE T
	FROM justiceupdate.ProvinceClass T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)

	EXEC eventlog.LogJusticeUpdateAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.DeleteJusticeProvince

--Begin procedure justiceupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'justiceupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get data for a community in a justice update batch
-- =====================================================================================
CREATE PROCEDURE justiceupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.JusticeAssessmentNotes,
		C.JusticeCommunityDocumentCenterStatusNotes,
		C.JusticeClassNotes,
		C.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM dbo.Community C
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = C.JusticeCommunityDocumentCenterStatusID
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CommunityID,
		C1.CommunityName AS EntityName,
		C2.JusticeAssessmentNotes,
		C2.JusticeCommunityDocumentCenterStatusNotes,
		C2.JusticeClassNotes,
		C2.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM justiceupdate.Community C2
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = C2.JusticeCommunityDocumentCenterStatusID
		JOIN dbo.Community C1 ON C1.CommunityID = C2.CommunityID
			AND C2.CommunityID = @CommunityID

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.JusticeAchievedValue,
		OACI.JusticeNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.JusticeAchievedValue, 
				CI.JusticeNotes
			FROM justiceupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.JusticeRiskValue,
		OACR.JusticeNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.JusticeRiskValue, 
				CR.JusticeNotes
			FROM justiceupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityCommunicationClassCurrent
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		(SELECT COUNT(CC.ClassContactID) FROM dbo.ClassContact CC WHERE CC.ClassID = CL.ClassID ) AS ClassContactCount,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'JO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.CommunityClassID
				FROM dbo.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

	--EntityCommunicationClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.JusticeNotes,
		(SELECT COUNT(CC.ClassContactID) FROM dbo.ClassContact CC WHERE CC.ClassID = CL.ClassID ) AS ClassContactCount,
		'<a class="btn btn-info" onclick="getNotes(''CommunicationClass'', ' + CAST(CL.ClassID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
			AND CNS.ConceptNoteStatusCode = 'Active'
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteCommunity CNC2
				WHERE CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND CNC2.CommunityID = @CommunityID
				)				
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ConceptNoteIndicator CNI
					JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
						AND CNI.ConceptNoteID = CN.ConceptNoteID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'JO3'
				)
			OUTER APPLY
				(
				SELECT
					CC.JusticeNotes
				FROM justiceupdate.CommunityClass CC
				WHERE CC.ClassID = CL.ClassID
					AND CC.CommunityID = @CommunityID
				) OACC

END
GO
--End procedure justiceupdate.GetCommunityByCommunityID

--Begin procedure justiceupdate.GetJusticeUpdate
EXEC Utility.DropObject 'justiceupdate.GetJusticeUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get data for a justice update batch
-- ======================================================================
CREATE PROCEDURE justiceupdate.GetJusticeUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @JusticeUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM justiceupdate.JusticeUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (JusticeUpdateID INT)

		INSERT INTO justiceupdate.JusticeUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.JusticeUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.JusticeUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'JusticeUpdate'

		SELECT @JusticeUpdateID = O.JusticeUpdateID FROM @tOutput O
		
		EXEC eventlog.LogJusticeUpdateAction @EntityID=@JusticeUpdateID, @EventCode='create', @PersonID = @PersonID

		END
	ELSE
		SELECT @JusticeUpdateID = FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU
	--ENDIF
	
	--Justice
	SELECT
		FU.JusticeUpdateID, 
		FU.WorkflowStepNumber 
	FROM justiceupdate.JusticeUpdate FU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDATETIME(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM justiceupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
	ORDER BY C2.CommunityName, C1.CommunityID
	
	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDATETIME(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM justiceupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	ORDER BY P2.ProvinceName, P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'JusticeUpdate'
			JOIN justiceupdate.JusticeUpdate FU ON FU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowStatus
	SELECT
		'JusticeUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @JusticeUpdateID

	--WorkflowStepWorkflowAction
	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'JusticeUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT FU.WorkflowStepNumber FROM justiceupdate.JusticeUpdate FU WHERE FU.JusticeUpdateID = @JusticeUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure justiceupdate.GetJusticeUpdate

--Begin procedure justiceupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'justiceupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get data for a province in a justice update batch
-- ====================================================================================
CREATE PROCEDURE justiceupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.ProvinceID,
		P.ProvinceName AS EntityName,
		P.JusticeAssessmentNotes,
		P.JusticeCommunityDocumentCenterStatusNotes,
		P.JusticeClassNotes,
		P.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM dbo.Province P
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = P.JusticeCommunityDocumentCenterStatusID
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.ProvinceID,
		P1.ProvinceName AS EntityName,
		P2.JusticeAssessmentNotes,
		P2.JusticeCommunityDocumentCenterStatusNotes,
		P2.JusticeClassNotes,
		P2.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM justiceupdate.Province P2
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = P2.JusticeCommunityDocumentCenterStatusID
		JOIN dbo.Province P1 ON P1.ProvinceID = P2.ProvinceID
			AND P2.ProvinceID = @ProvinceID

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PI.ProvinceIndicatorID
			FROM dbo.ProvinceIndicator PI 
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OAPI.JusticeAchievedValue,
		OAPI.JusticeNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PI.JusticeAchievedValue, 
				PI.JusticeNotes
			FROM justiceupdate.ProvinceIndicator PI
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.JusticeRiskValue,
		OAPR.JusticeNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.JusticeRiskValue, 
				PR.JusticeNotes
			FROM justiceupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingClassCurrent
	SELECT
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		(SELECT COUNT(CC.ClassContactID) FROM dbo.ClassContact CC WHERE CC.ClassID = CL.ClassID ) AS ClassContactCount,
		'<a class="btn btn-info" onclick="getClassCommunityNotes(' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		OUTER APPLY
			(
			SELECT
				CC.CommunityClassID
			FROM dbo.CommunityClass CC
				JOIN dbo.Community C ON C.CommunityID = CC.CommunityID
					AND CC.ClassID = CL.ClassID
					AND C.ProvinceID = @ProvinceID
			) OACC
	WHERE CNS.ConceptNoteStatusCode = 'Active'
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ConceptNoteCommunity CNC2
				JOIN dbo.Community C ON C.CommunityID = CNC2.CommunityID
					AND CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND C.ProvinceID = @ProvinceID
			)				
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ConceptNoteIndicator CNI
				JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
					AND CNI.ConceptNoteID = CN.ConceptNoteID
				JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
				JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
					AND CRA.ComponentReportingAssociationCode = 'JO2'
			)
	
	--EntityTrainingClassUpdate
	SELECT
		CL.ClassID,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		CR.CourseName,
		OACC.JusticeNotes,
		(SELECT COUNT(CC.ClassContactID) FROM dbo.ClassContact CC WHERE CC.ClassID = CL.ClassID ) AS ClassContactCount,
		'<a class="btn btn-info" onclick="getNotes(''TrainingClass'', ' + CAST(ISNULL(OACC.CommunityClassID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Class CL
		JOIN dbo.Course CR ON CR.COurseID = CL.CourseID
		JOIN dbo.ConceptNoteClass CNC1 ON CNC1.ClassID = CL.ClassID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC1.ConceptNoteID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		OUTER APPLY
			(
			SELECT
				CC.CommunityClassID,
				CC.JusticeNotes
			FROM justiceupdate.CommunityClass CC
				JOIN dbo.Community C ON C.CommunityID = CC.CommunityID
					AND CC.ClassID = CL.ClassID
					AND C.ProvinceID = @ProvinceID
			) OACC
	WHERE CNS.ConceptNoteStatusCode = 'Active'
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ConceptNoteCommunity CNC2
				JOIN dbo.Community C ON C.CommunityID = CNC2.CommunityID
					AND CNC2.ConceptNoteID = CNC1.ConceptNoteID
					AND C.ProvinceID = @ProvinceID
			)				
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ConceptNoteIndicator CNI
				JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
					AND CNI.ConceptNoteID = CN.ConceptNoteID
				JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
				JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
					AND CRA.ComponentReportingAssociationCode = 'JO2'
			)

END
GO
--End procedure justiceupdate.GetProvinceByProvinceID

--Begin procedure procurement.GetEquipmentDistributionPlanByConceptNoteID
EXEC Utility.DropObject 'procurement.GetEquipmentDistributionPlanByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.06.28
-- Description:	A stored procedure to get data from the procurement.EquipmentDistributionPlan table
-- ================================================================================================
CREATE PROCEDURE procurement.GetEquipmentDistributionPlanByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EquipmentDistributionPlanID INT

	SELECT @EquipmentDistributionPlanID = EDP.EquipmentDistributionPlanID
	FROM procurement.EquipmentDistributionPlan EDP
	WHERE EDP.ConceptNoteID = @ConceptNoteID

	SELECT 
		EDP.EquipmentDistributionPlanID,
		EDP.ConceptNoteID,
		EDP.Title,
		EDP.Summary,
		EDP.ExportRoute,
		EDP.CurrentSituation,
		EDP.Aim,
		EDP.PlanOutline,
		EDP.Phase1,
		EDP.Phase2,
		EDP.Phase3,
		EDP.Phase4,
		EDP.Phase5,
		EDP.OperationalResponsibility,
		EDP.Annexes,
		EDP.Distribution,
		EDP.WorkflowStepNumber,
		CN.Title AS ConceptNoteTitle, 
		EDP.Errata,
		EDP.ErrataTitle,
		EDP.OperationalSecurity
	FROM procurement.EquipmentDistributionPlan EDP
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EDP.ConceptNoteID
			AND EDP.ConceptNoteID = @ConceptNoteID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'EquipmentDistributionPlan'
			JOIN procurement.EquipmentDistributionPlan EDP ON EDP.WorkflowStepNumber = WS.WorkflowStepNumber
				AND EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'EquipmentDistributionPlan.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EquipmentDistributionPlanID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'EquipmentDistributionPlan'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID) > 0
					THEN (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure procurement.GetEquipmentDistributionPlanByConceptNoteID

--Begin procedure workflow.CanIncrementJusticeUpdateWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementJusticeUpdateWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2016.02.13
-- Description:	A stored procedure to determine if a justice update workflow can be incremented
-- ============================================================================================
CREATE PROCEDURE workflow.CanIncrementJusticeUpdateWorkflow

@EntityID INT

AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'JusticeUpdate'
			JOIN justiceupdate.JusticeUpdate F ON F.WorkflowStepNumber = WS.WorkflowStepNumber
				AND F.JusticeUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementJusticeUpdateWorkflow

--Begin procedure workflow.GetJusticeUpdateWorkflowData
EXEC Utility.DropObject 'workflow.GetJusticeUpdateWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Jonathan Burnham
-- Create date:	2016.02.13
-- Description:	A stored procedure to get justice update workflow data
-- ===================================================================
CREATE PROCEDURE workflow.GetJusticeUpdateWorkflowData

@EntityID INT

AS
BEGIN

	DECLARE @tTable TABLE (PermissionableLineage VARCHAR(MAX), IsComplete BIT, WorkflowStepName VARCHAR(50))
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'JusticeUpdate'
			JOIN justiceupdate.JusticeUpdate FU ON FU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND FU.JusticeUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	INSERT INTO @tTable 
		(PermissionableLineage, IsComplete, WorkflowStepName)
	SELECT
		'JusticeUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		WS.WorkflowStepName
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @EntityID
	
	SELECT 
		(SELECT RU.WorkflowStepNumber FROM justiceupdate.JusticeUpdate RU WHERE RU.JusticeUpdateID = @EntityID) AS WorkflowStepNumber,
		W.WorkflowStepCount 
	FROM workflow.Workflow W 
	WHERE W.EntityTypeCode = 'JusticeUpdate'
	
	SELECT
		T.IsComplete, 
		T.WorkflowStepName,
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress
	FROM @tTable T
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = T.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY T.WorkflowStepName, FullName

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullNameFormatted,
		dbo.FormatDATETIME(EL.CreateDATETIME) AS CreateDATETIMEFormatted,

		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Justice Updates'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Justice Updates'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Justice Updates'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Justice Updates'
		END AS EventAction,

    EL.Comments
  FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'JusticeUpdate'
		AND EL.EntityID = @EntityID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDATETIME

END
GO
--End procedure workflow.GetJusticeUpdateWorkflowData

--Begin procedure workflow.GetJusticeUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetJusticeUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2016.02.13
-- Description:	A stored procedure to get the people in a justice update workflow step
-- ===================================================================================
CREATE PROCEDURE workflow.GetJusticeUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'JusticeUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT FU.WorkflowStepNumber FROM justiceupdate.JusticeUpdate FU WHERE FU.JusticeUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT FU.WorkflowStepNumber FROM justiceupdate.JusticeUpdate FU WHERE FU.JusticeUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'JusticeUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetJusticeUpdateWorkflowStepPeople

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'PoliceEngagementUpdate', 'Police Engagement Update'
GO
--End table dbo.EntityType

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.47 File 01 - AJACS - 2016.02.16 12.01.42')
GO
--End build tracking

