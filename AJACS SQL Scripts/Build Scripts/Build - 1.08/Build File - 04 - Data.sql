USE AJACS
GO

--Begin table workflow.WorkflowStep
UPDATE WS
SET WS.WorkflowStatusName = 
	CASE
		WHEN WS.WorkflowStepName = 'Draft'
		THEN 'In Draft'
		WHEN WS.WorkflowStepName = 'Review'
		THEN 'Under Review'
		WHEN WS.WorkflowStepName = 'Approve'
		THEN 'Pending Approval'
	END
FROM workflow.WorkflowStep WS
GO
--End table workflow.WorkflowStep

