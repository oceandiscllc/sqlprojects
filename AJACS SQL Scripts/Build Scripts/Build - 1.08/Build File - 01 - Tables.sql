USE AJACS
GO

--Begin table dbo.RequestForInformation
DECLARE @TableName VARCHAR(250) = 'dbo.RequestForInformation'

EXEC utility.AddColumn @TableName, 'SummaryAnswer', 'VARCHAR(MAX)'
GO
--End table dbo.RequestForInformation

--Begin table workflow.WorkflowStep
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC utility.AddColumn @TableName, 'WorkflowStatusName', 'VARCHAR(50)'
GO
--End table workflow.WorkflowStep



