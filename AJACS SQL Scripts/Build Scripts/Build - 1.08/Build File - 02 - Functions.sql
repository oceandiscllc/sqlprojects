USE AJACS
GO

--Begin function workflow.GetWorkflowStatusNameByEntityTypeCodeAndWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStatusNameByEntityTypeCodeAndWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.04
-- Description:	A function to return the workflow status name for a specific entity
-- ================================================================================

CREATE FUNCTION workflow.GetWorkflowStatusNameByEntityTypeCodeAndWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@WorkflowStepNumber INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cWorkflowStatusName VARCHAR(50)
	
	SELECT
		@cWorkflowStatusName = WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = @EntityTypeCode
			AND WS.WorkflowStepNumber = @WorkflowStepNumber

	RETURN ISNULL(@cWorkflowStatusName, 'Approved')

END
GO
--End function workflow.GetWorkflowStatusNameByEntityTypeCodeAndWorkflowStepNumber
