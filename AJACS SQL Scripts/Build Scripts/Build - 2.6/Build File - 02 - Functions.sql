USE AJACS
GO

--Begin function dbo.GetConceptNoteCommunityListByConceptNoteID
EXEC utility.DropObject 'dbo.ConceptNoteCommunityListByConceptNoteID'
EXEC utility.DropObject 'dbo.GetConceptNoteCommunityListByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.23
-- Description:	A function to get a list of community names based on a conceptnoteid
-- =================================================================================

CREATE FUNCTION dbo.GetConceptNoteCommunityListByConceptNoteID
(
@ConceptNoteID INT
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityNameList NVARCHAR(MAX)

	SELECT @cCommunityNameList = COALESCE(@cCommunityNameList + ', ', '') + C.CommunityName
	FROM dbo.ConceptNoteCommunity CNC
		JOIN dbo.Community C ON C.CommunityID = CNC.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CommunityName
	
	RETURN @cCommunityNameList
END
GO
--End function dbo.GetConceptNoteCommunityListByConceptNoteID

--Begin function dbo.GetConceptNoteProvinceListByConceptNoteID
EXEC utility.DropObject 'dbo.ConceptNoteProvinceListByConceptNoteID'
EXEC utility.DropObject 'dbo.GetConceptNoteProvinceListByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.23
-- Description:	A function to get a list of community names based on a conceptnoteid
-- =================================================================================

CREATE FUNCTION dbo.GetConceptNoteProvinceListByConceptNoteID
(
@ConceptNoteID INT
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceNameList NVARCHAR(MAX)

	SELECT @cProvinceNameList = COALESCE(@cProvinceNameList + ', ', '') + P.ProvinceName
	FROM dbo.Province P
	WHERE EXISTS
		(
		SELECT 1
		FROM dbo.ConceptNoteCommunity CNC
			JOIN dbo.Community C ON C.CommunityID = CNC.CommunityID
				AND CNC.ConceptNoteID = @ConceptNoteID
				AND P.ProvinceID = C.ProvinceID
		)
	
	ORDER BY P.ProvinceName
	
	RETURN @cProvinceNameList
END
GO
--End function dbo.GetConceptNoteProvinceListByConceptNoteID

--Begin function workflow.GetConceptNoteWorkflowStatus
EXEC utility.DropObject 'workflow.GetConceptNoteWorkflowStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.05
-- Description:	A function to get a workflow status name for a concept note
-- ========================================================================
CREATE FUNCTION workflow.GetConceptNoteWorkflowStatus
(
@ConceptNoteID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cStatusName VARCHAR(250) = 'Bad WorkFlow'
	DECLARE @cWorkflowName VARCHAR(250) = LTRIM(RTRIM((SELECT TOP 1 EWSGP.WorkflowName FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = 'Conceptnote' AND EWSGP.EntityID = @ConceptNoteID)))
	DECLARE @nTargetWorkflowStepNumber INT = 0

	IF @cWorkflowName IN ('UK Activity Workflow','US Activity Workflow')
		BEGIN

		IF @cWorkflowName = 'UK Activity Workflow'
			SET @nTargetWorkflowStepNumber = 6
		ELSE IF @cWorkflowName = 'US Activity Workflow'	
			SET @nTargetWorkflowStepNumber = 7
		--ENDIF

		SELECT @cStatusName = 
			CASE
				WHEN CNS.ConceptNoteStatusCode IN ('Amended','Cancelled','Closed','OnHold') 
				THEN CNS.ConceptNoteStatusName
				ELSE
					CASE
						WHEN workflow.getworkflowstepnumber('ConceptNote',cn.conceptnoteid)  < @nTargetWorkflowStepNumber
						THEN 'Development'
						WHEN workflow.getworkflowstepnumber('ConceptNote',cn.conceptnoteid)  = @nTargetWorkflowStepNumber
						THEN 'Implementation'
						ELSE 'Closedown'
					END
			END

		FROM dbo.ConceptNote CN
			JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
				AND CN.ConceptNoteID = @ConceptNoteID

		END
	--ENDIF

	RETURN @cStatusName

END
GO
--End function workflow.GetConceptNoteWorkflowStatus