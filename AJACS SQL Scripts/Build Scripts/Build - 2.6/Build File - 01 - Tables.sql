USE AJACS
GO

--Begin table asset.AssetUnitExpense
DECLARE @TableName VARCHAR(250) = 'asset.AssetUnitExpense'

EXEC utility.AddColumn @TableName, 'ConceptNoteID', 'INT', '0'
GO
--End table asset.AssetUnitExpense

--Begin table dbo.ContactStipendPayment
DECLARE @TableName VARCHAR(250) = 'dbo.ContactStipendPayment'

EXEC utility.AddColumn @TableName, 'ConceptNoteID', 'INT', '0'
GO
--End table dbo.ContactStipendPayment
