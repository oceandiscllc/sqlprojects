USE AJACS
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.AssetUnitCost AUC WHERE AUC.AssetUnitCostName = '750')
	BEGIN

	INSERT INTO dropdown.AssetUnitCost
		(AssetUnitCostName)
	VALUES 
		('750'),
		('1000'),
		('5000')

	END
--ENDIF
GO

DECLARE @tTable TABLE (DisplayOrder INT NOT NULL PRIMARY KEY IDENTITY(1,1), AssetUnitCostName VARCHAR(50))

INSERT INTO @tTable (AssetUnitCostName) SELECT CAST(AUC.AssetUnitCostName AS INT) FROM dropdown.AssetUnitCost AUC ORDER BY CAST(AUC.AssetUnitCostName AS INT)

UPDATE AUC
SET AUC.DisplayOrder = T.DisplayOrder
FROM dropdown.AssetUnitCost AUC
	JOIN @tTable T ON T.AssetUnitCostName = AUC.AssetUnitCostName
GO