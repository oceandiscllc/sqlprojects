USE AJACS
GO

--Begin procedure dbo.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'dbo.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.31
-- Description:	A stored procedure to data from the dbo.EmailTemplate table
-- ========================================================================
CREATE PROCEDURE dbo.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET1.EmailTemplateID, 
		ET1.EntityTypeCode, 
		ET1.WorkflowActionCode, 
		ET1.EmailText,
		ET2.EntityTypeName,
		ISNULL((SELECT WA.WorkflowActionName FROM workflow.WorkflowAction WA WHERE WA.WorkflowActionCode = ET1.WorkflowActionCode AND (WA.WorkflowActionCode <> 'IncrementWorkflow' OR WA.WorkflowActionName = 'Approve')), ET1.WorkflowActionCode) AS WorkflowActionName
	FROM dbo.EmailTemplate ET1
		JOIN dbo.EntityType ET2 ON ET2.EntityTypeCode = ET1.EntityTypeCode
			AND ET1.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM dbo.EmailTemplateField ETF
		JOIN dbo.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure dbo.GetEmailTemplateByEmailTemplateID
