USE AJACS
GO

--Begin table dropdown.ContactVettingType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactVettingType'

EXEC utility.AddColumn @TableName, 'ContactVettingTypeCode', 'VARCHAR(50)'
GO

UPDATE CVT
SET CVT.ContactVettingTypeCode = CVT.ContactVettingTypeName
FROM dropdown.ContactVettingType CVT
GO
--End table dropdown.ContactVettingType

