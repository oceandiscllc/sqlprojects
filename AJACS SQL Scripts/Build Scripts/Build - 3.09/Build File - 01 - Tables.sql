USE AJACS
GO

--Begin table asset.AssetUnit
DECLARE @TableName VARCHAR(250) = 'asset.AssetUnit'

EXEC utility.AddColumn @TableName, 'DepartmentSizeName', 'VARCHAR(10)'
GO
--End table asset.AssetUnit

--Begin table asset.AssetUnitExpense
DECLARE @TableName VARCHAR(250) = 'asset.AssetUnitExpense'

EXEC utility.AddColumn @TableName, 'StipendTypeCode', 'VARCHAR(50)', 'JusticeStipend'
GO
--End table asset.AssetUnitExpense

--Begin table dbo.ContactStipendPayment
EXEC utility.AddColumn 'dbo.ContactStipendPayment ', 'ContactStipendStatusID', 'INT', '0'
GO
--End table dbo.ContactStipendPayment

--Begin table dbo.Contact
EXEC utility.DropIndex 'dbo.Contact', 'IX_ContactAssetUnit'
GO

EXEC utility.SetIndexNonClustered 'dbo.Contact', 'IX_ContactAssetUnit', 'AssetUnitID', 'ContactID'
GO
--End table dbo.Contact

--Begin table dropdown.ContactStipendStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactStipendStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactStipendStatus
	(
	ContactStipendStatusID INT IDENTITY(0,1) NOT NULL,
	ContactStipendStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactStipendStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ContactStipendStatus', 'DisplayOrder,ContactStipendStatusName'
GO
--End table dropdown.ContactStipendStatus
