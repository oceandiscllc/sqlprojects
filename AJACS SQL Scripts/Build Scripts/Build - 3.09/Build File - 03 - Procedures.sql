﻿USE AJACS
GO

--Begin procedure asset.GetAssetByAssetID
EXEC Utility.DropObject 'asset.GetAssetByAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.12
-- Description:	A stored procedure to get data from the asset.Asset table
-- Notes:				Changes here must ALSO be made to asset.GetAssetByEventlogID
-- =========================================================================
CREATE PROCEDURE asset.GetAssetByAssetID

@AssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Asset
	SELECT
		A.AssetDescription,
		A.AssetID,
		A.AssetName,
		A.CommunityID,
		dbo.GetCommunityNameByCommunityID(A.CommunityID) AS CommunityName,
		A.IsActive,
		A.Location.STAsText() AS Location,
		A.RelocationDate,
		dbo.FormatDate(A.RelocationDate) AS RelocationDateFormatted,
		A.RelocationLocation,
		A.RelocationNotes,
		AST.AssetStatusCode,
		AST.AssetStatusID,
		AST.AssetStatusName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon
	FROM asset.Asset A
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
			AND A.AssetID = @AssetID

	--AssetConceptNote
	SELECT 
		CN.Title AS ConceptNoteName
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteAsset CNA ON CNA.ConceptNoteID = CN.ConceptNoteID
			AND CNA.AssetID = @AssetID

	--AssetEventLog
	SELECT 
		EL.EventLogID,
		EL.EventCode, 
		EL.EventData, 
		dbo.FormatDateTime(EL.createdatetime) AS CreateDateFormatted, 
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
	WHERE EL.EntityTypeCode = 'Asset' AND EL.entityid = @AssetID AND EL.eventcode != 'read' 

	--AssetPaymentHistory
	EXEC asset.GetAssetPaymentHistoryByAssetID @AssetID = @AssetID

	--AssetTrainingHistory
	SELECT
		COUNT(CC.ContactID) AS StudentCount,
		CR.CourseID,
		CR.CourseName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Course CR ON CR.CourseID = CL.CourseID
		JOIN dbo.Contact C ON C.ContactID = CC.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
			AND A.AssetID = @AssetID
	GROUP BY A.AssetID, CR.CourseName, CR.CourseID
	ORDER BY 3

	--AssetUnit
	SELECT
		AU.AssetUnitID,
		AU.AssetUnitName,
		AU.CommanderContactID,
		AU.DepartmentSizeName,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullNameFormatted,
		AU.DeputyCommanderContactID,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullNameFormatted,
		AU.HeadOfDepartmentContactID,
		dbo.FormatContactNameByContactID(AU.HeadOfDepartmentContactID, 'LastFirstMiddle') AS HeadOfDepartmentFullNameFormatted,
		AU.IsActive,
		AUC.AssetUnitCostID,
		AUC.AssetUnitCostName
	FROM asset.AssetUnit AU
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			AND AU.AssetID = @AssetID

	--AssetUnitContactVetting
	;
	WITH CD AS
		(
		SELECT AU.CommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID

		UNION

		SELECT AU.DeputyCommanderContactID AS ContactID
		FROM asset.AssetUnit AU
		WHERE AU.AssetID = @AssetID
		)

	SELECT 
		dbo.FormatContactNameByContactID(US.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'US' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(US.VettingDate) AS VettingDateFormatted,
		VO1.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN VO1.VettingOutcomeCode = 'DoNotConsider'
			THEN '<img src="/assets/img/icons/EA1921-vetting.png" style="height:25px; width:25px;" /> '
			WHEN ISNULL((SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = US.ContactID), dateAdd(day, -1, getDate())) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/32AC41-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			A.ContactID,
			A.ContactVettingID,
			A.VettingDate,
			A.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD1.ContactID ORDER BY CV1.VettingDate DESC, CV1.ContactVettingID DESC) AS RowIndex,
				CD1.ContactID,
				CV1.ContactVettingID,
				CV1.VettingDate,
				CV1.VettingOutcomeID
			FROM CD CD1
				LEFT JOIN dbo.ContactVetting CV1 ON CV1.ContactID = CD1.ContactID
			WHERE CV1.ContactVettingTypeID = 1
			) A
			WHERE A.RowIndex = 1
		) US
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = ISNULL(US.VettingOutcomeID, 0)

	UNION

	SELECT 
		dbo.FormatContactNameByContactID(UK.ContactID, 'LastFirstMiddle') AS FullNameFormatted,
		'UK' AS VettingTypeCode,
		dbo.FormatDate((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID)) AS VettingExpirationDateFormatted,
		dbo.FormatDate(UK.VettingDate) AS VettingDateFormatted,
		VO2.VettingOutcomeName AS VettingOutcomeName,

		CASE
			WHEN VO2.VettingOutcomeCode = 'DoNotConsider'
			THEN '<img src="/assets/img/icons/EA1921-vetting.png" style="height:25px; width:25px;" /> '
			WHEN ISNULL((SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = UK.ContactID), dateAdd(day, -1, getDate())) < getDate()
			THEN '<img src="/assets/img/icons/757575-vetting.png" style="height:25px; width:25px;" /> ' 
			ELSE '<img src="/assets/img/icons/32AC41-vetting.png" style="height:25px; width:25px;" /> ' 
		END AS VettingIcon

	FROM
		(
		SELECT 
			B.ContactID,
			B.ContactVettingID,
			B.VettingDate,
			B.VettingOutcomeID
		FROM 
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CD2.ContactID ORDER BY CV2.VettingDate DESC, CV2.ContactVettingID DESC) AS RowIndex,
				CD2.ContactID,
				CV2.ContactVettingID,
				CV2.VettingDate,
				CV2.VettingOutcomeID
			FROM CD CD2
				LEFT JOIN dbo.ContactVetting CV2 ON CV2.ContactID = CD2.ContactID
			WHERE CV2.ContactVettingTypeID = 2
			) B
			WHERE B.RowIndex = 1
		) UK
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = ISNULL(UK.VettingOutcomeID, 0)

	ORDER BY 1, 2, 3, 4

END
GO
--End procedure asset.GetAssetByAssetID

--Begin procedure dbo.AddContactStipendPaymentContacts
EXEC Utility.DropObject 'dbo.AddContactStipendPaymentContacts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	A stored procedure to add payees to the ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016..09.18
-- Description:	Refactored for asset support
-- ================================================================================
CREATE PROCEDURE dbo.AddContactStipendPaymentContacts

@PaymentMonth INT,
@PaymentYear INT,
@PersonID INT,
@ContactIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY, StipendTypeCode VARCHAR(50))

	IF @ContactIDList = ''
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Contact'
				AND SR.EntityID = C.ContactID
				AND SR.PersonID = @PersonID
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)

		END
	ELSE
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN dbo.ListToTable(@ContactIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ContactID
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)

		END
	--ENDIF
		
	INSERT INTO asset.AssetUnitExpense
		(AssetUnitID, ExpenseAmountAuthorized, ProvinceID, PaymentMonth, PaymentYear, StipendTypeCode)
	SELECT DISTINCT
		AU.AssetUnitID,
		AUC.AssetUnitCostName,
		dbo.GetProvinceIDByCommunityID(A.CommunityID),
		@PaymentMonth,
		@PaymentYear,
		O.StipendTypeCode
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
		JOIN dbo.Contact C ON C.AssetUnitID = AU.AssetUnitID
		JOIN @tOutput O ON O.ContactID = C.ContactID
			AND NOT EXISTS
				(
				SELECT 1
				FROM asset.AssetUnitExpense AUE
				WHERE AUE.AssetUnitID = AU.AssetUnitID
					AND AUE.PaymentMonth = @PaymentMonth
					AND AUE.PaymentYear = @PaymentYear
				)
	
END
GO
--End procedure dbo.AddContactStipendPaymentContacts

--Begin procedure dbo.ApproveContactStipendPayment
EXEC Utility.DropObject 'dbo.ApproveContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ApproveContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT,
@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET
		CSP.AssetUnitID = C.AssetUnitID,
		CSP.ConceptNoteID = @ConceptNoteID,
		CSP.StipendAuthorizedDate = getDate(),
		CSP.StipendAmountPaid = 
			CASE
				WHEN dbo.GetContactStipendEligibility(CSP.ContactID, @ConceptNoteID) = 1
				THEN CSP.StipendAmountAuthorized
				ELSE 0
			END

	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			AND CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = @StipendTypeCode
			AND CSP.StipendAuthorizedDate IS NULL

	UPDATE AUE
	SET 
		AUE.ConceptNoteID = @ConceptNoteID,
		AUE.ExpenseAuthorizedDate = getDate(),
		AUE.ExpenseAmountPaid = AUE.ExpenseAmountAuthorized
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
		AND AUE.ProvinceID = @ProvinceID
		AND AUE.ExpenseAuthorizedDate IS NULL

END
GO
--End procedure dbo.ApproveContactStipendPayment

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
--
-- Author:			Todd Pires
-- Update Date: 2016.09.01
-- Description:	Added the ConceptNoteTypeCode field
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
--
-- Author:			Brandon Green
-- Create date:	2017.07.19
-- Description:	Removed deprecated columns				  
--
-- Author:			Brandon Green
-- Create date:	2018.08.25
-- Description:	Added the MeansOfVerification field
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	--ConceptNote
	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeCode AS ConceptNoteComponentCode,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed,
		CN.AmendedConceptNoteID,
		ACN.Title AS AmendedConceptNoteTitle,
		ABCN.ConceptNoteID AS AmendedByConceptNoteID,
		ABCN.Title AS AmendedByConceptNoteTitle,
		CN.WorkplanActivityID,
		WPA.WorkplanActivityName
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
		LEFT JOIN dbo.ConceptNote ACN ON ACN.ConceptNoteID = CN.AmendedConceptNoteID
		LEFT JOIN dbo.ConceptNote ABCN ON ABCN.AmendedConceptNoteID = CN.ConceptNoteID
		LEFT JOIN workplan.WorkplanActivity WPA ON WPA.WorkplanActivityID = CN.WorkplanActivityID
	WHERE CN.ConceptNoteID = @ConceptNoteID

	--ConceptNoteAmendment (deprecated)
	SELECT			
		CNA.ConceptNoteAmendmentID,
		CNA.AmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--ConceptNoteAsset
	SELECT 
		A.AssetID,
		A.AssetName
	FROM dbo.ConceptNoteAsset CNA
		JOIN asset.Asset A ON A.AssetID = CNA.AssetID
			AND CNA.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteAuthor
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	--ConceptNoteBackgroundText
	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	--ConceptNoteBudget
	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Amendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	--ConceptNoteClass
	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	--ConceptNoteCommunity
	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.ConceptNoteCommunity CNC 
		JOIN dbo.Community C ON C.CommunityID = CNC.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	--ConceptNoteContact
	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= GETDATE() AND OACV12.VettingOutcomeCode != 'DoNotConsider'
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= GETDATE() AND OACV22.VettingOutcomeCode != 'DoNotConsider'
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID, VO1.VettingOutcomeCode
			FROM dbo.ContactVetting CV12
				JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID, VO2.VettingOutcomeCode
			FROM dbo.ContactVetting CV22
				JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1

	--ConceptNoteDocument
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM document.DocumentEntity DE 
		JOIN document.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName

	--ConceptNoteEquipmentCatalog
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID

	--ConceptNoteEthnicity
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName
			
	--ConceptNoteFinance
	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID

	--ConceptNoteIndicator
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		I.TargetValue,
		I.MeansOfVerification,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteProject
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	--ConceptNoteProvince
	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID

	--ConceptNoteRisk
	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
			
	--ConceptNoteTask
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID
	
	--ConceptNoteUpdate
	SELECT
		CNU.ConceptNoteUpdateID,
		CNU.PersonID, 
		dbo.FormatPersonNameByPersonID(CNU.PersonID, 'LastFirst') AS FullNameFormatted,
		CNU.Remarks,
		CNU.UpdateDate,
		dbo.FormatDate(CNU.UpdateDate) AS UpdateDateFormatted,
		CNUT.ConceptNoteUpdateTypeID,
		CNUT.ConceptNoteUpdateTypeName,
		D.DocumentName,
		D.DocumentTitle,
		D.DocumentFileName,
		CNU.ActualTotalAmount
	FROM dbo.ConceptNoteUpdate CNU
		JOIN dropdown.ConceptNoteUpdateType CNUT ON CNUT.ConceptNoteUpdateTypeID = CNU.ConceptNoteUpdateTypeID
		LEFT JOIN Document.Document D ON D.DocumentID = CNU.DocumentID
	WHERE CNU.ConceptNoteID = @ConceptNoteID
	
	--ConceptNoteVersion
	;
	WITH HD AS
		(
		SELECT
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			1 AS Depth
		FROM dbo.ConceptNote CN
		WHERE CN.ConceptNoteID = @ConceptNoteID

		UNION ALL

		SELECT 
			CN.ConceptNoteID, 
			CN.AmendedConceptNoteID, 
			CN.Title, 
			HD.Depth + 1 AS Depth
		FROM dbo.ConceptNote CN
			JOIN HD ON CN.ConceptNoteID = HD.AmendedConceptNoteID
		)

	SELECT 
		HD.ConceptNoteID,
		HD.Title,
		MAX(HD.Depth) OVER() - HD.Depth + 1 AS VersionNumber,
		(SELECT ISNULL(SUM(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost), 0)
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = HD.ConceptNoteID
		) + (SELECT ISNULL(SUM(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue), 0)
			FROM dbo.ConceptNoteEquipmentCatalog CNEC
			JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
				AND CNEC.ConceptNoteID = HD.ConceptNoteID
		) AS TotalCost
	FROM HD
	ORDER BY VersionNumber
	
	--ConceptNoteWorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD

	--ConceptNoteWorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
			WHEN EL.EventCode = 'Cancel'
			THEN 'Canceled Concept Note'
			WHEN EL.EventCode = 'Hold'
			THEN 'Placed Concept Note on Hold'
			WHEN EL.EventCode = 'Unhold'
			THEN 'Reactivated Concept Note'			
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update','Cancel','Hold','Unhold')
	ORDER BY EL.CreateDateTime
	
	--ConceptNoteWorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.DeleteContactStipendPayment
EXEC Utility.DropObject 'dbo.DeleteContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to delete data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE dbo.DeleteContactStipendPayment

@ContactStipendPaymentIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @ContactStipendPaymentIDList IS NOT NULL
		BEGIN

		DELETE CSP
		FROM dbo.ContactStipendPayment CSP
		WHERE EXISTS
			(
			SELECT 1
			FROM dbo.ListToTable(@ContactStipendPaymentIDList, ',') LTT
			WHERE CAST(LTT.ListItem AS INT) = CSP.ContactStipendPaymentID
			)

		END
	--ENDIF

END
GO
--End procedure dbo.DeleteContactStipendPayment

--Begin procedure dbo.ReconcileContactStipendPayment
EXEC Utility.DropObject 'dbo.ReconcileContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ReconcileContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT,
@StipendPaidDate DATE = NULL

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET 
		CSP.StipendPaidDate = ISNULL(@StipendPaidDate, getDate())
	FROM dbo.ContactStipendPayment CSP
	WHERE CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
		AND CSP.ProvinceID = @ProvinceID
		AND CSP.StipendTypeCode = @StipendTypeCode
		AND CSP.StipendPaidDate IS NULL

	UPDATE C
	SET C.StipendArrears = C.StipendArrears + CSP.StipendAmountAuthorized - CSP.StipendAmountPaid
	FROM dbo.Contact C
		JOIN dbo.ContactStipendPayment CSP ON CSP.ContactID = C.ContactID
			AND CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = @StipendTypeCode
			AND CSP.StipendAmountAuthorized > CSP.StipendAmountPaid

	UPDATE AUE
	SET 
		AUE.ExpensePaidDate = ISNULL(@StipendPaidDate, getDate())
	FROM asset.AssetUnitExpense AUE
	WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
		AND AUE.ProvinceID = @ProvinceID
		AND AUE.ExpensePaidDate IS NULL

END
GO
--End procedure dbo.ReconcileContactStipendPayment

--Begin procedure dbo.SetContactStipendStatus
EXEC Utility.DropObject 'dbo.SetContactStipendStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date: 2018.05.28
-- Description:	A stored procedure to delete data from the dbo.ContactStipendPayment table
-- =======================================================================================
CREATE PROCEDURE dbo.SetContactStipendStatus

@ContactStipendPaymentIDList VARCHAR(MAX) = NULL,
@ContactStipendStatusID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @ContactStipendPaymentIDList IS NOT NULL
		BEGIN

		UPDATE CSP
		SET CSP.ContactStipendStatusID = @ContactStipendStatusID
		FROM dbo.ContactStipendPayment CSP
		WHERE EXISTS
			(
			SELECT 1
			FROM dbo.ListToTable(@ContactStipendPaymentIDList, ',') LTT
			WHERE CAST(LTT.ListItem AS INT) = CSP.ContactStipendPaymentID
			)

		END
	--ENDIF

END
GO
--End procedure dbo.SetContactStipendStatus

--Begin procedure dropdown.GetContactStipendStatusData
EXEC Utility.DropObject 'dropdown.GetContactStipendStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Damon Miller
-- Create date:	2018.05.17
-- Description:	A stored procedure to return data from the dropdown.ContactStipendStatus table
-- ===========================================================================================
CREATE PROCEDURE dropdown.GetContactStipendStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactStipendStatusID, 
		T.ContactStipendStatusName
	FROM dropdown.ContactStipendStatus T
	WHERE (T.ContactStipendStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactStipendStatusName, T.ContactStipendStatusID

END
GO
--End procedure dropdown.GetContactStipendStatusData

--Begin procedure logicalframeworkupdate.ApproveLogicalFrameworkUpdate
EXEC utility.DropObject 'logicalframeworkupdate.ApproveLogicalFrameworkUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================================
-- Author:			Jonathan Burnham
-- Create date:	2017.07.04
-- Description:	A stored procedure to submit Objectives, Indicators, and Milestones in a logicalframework update for approval
-- ==========================================================================================================================
CREATE PROCEDURE logicalframeworkupdate.ApproveLogicalFrameworkUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	DECLARE @cEntityTypeCode VARCHAR(50)
	DECLARE @nEntityID INT
	DECLARE @nLogicalFrameworkUpdateID INT
	DECLARE @tOutput1 TABLE (EntityTypeCode VARCHAR(50), EntityID INT)
	DECLARE @tOutput2 TABLE (LogicalFrameworkUpdateID INT)

	SELECT @nLogicalFrameworkUpdateID = LFU.LogicalFrameworkUpdateID
	FROM logicalframeworkupdate.LogicalFrameworkUpdate LFU

	UPDATE O
	SET
		O.ParentObjectiveID = LFUO.ParentObjectiveID, 
		O.ObjectiveTypeID = LFUO.ObjectiveTypeID, 
		O.ObjectiveName = LFUO.ObjectiveName, 
		O.ObjectiveDescription = LFUO.ObjectiveDescription, 
		O.IsActive = LFUO.IsActive, 
		O.LogicalFrameworkStatusID = LFUO.LogicalFrameworkStatusID, 
		O.StatusUpdateDescription = LFUO.StatusUpdateDescription, 
		O.ComponentReportingAssociationID = LFUO.ComponentReportingAssociationID
	OUTPUT 'Objective', INSERTED.ObjectiveID INTO @tOutput1
	FROM logicalframework.Objective O
		JOIN logicalframeworkupdate.Objective LFUO ON LFUO.ObjectiveID = O.ObjectiveID
			AND LFUO.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	--set children inactive of all objectives marked inactive
	UPDATE O
	SET O.IsActive = 0
	FROM logicalframework.Objective O
	WHERE EXISTS
		(
		SELECT 1
		FROM logicalframeworkupdate.Objective UO
		WHERE UO.ObjectiveID = O.ParentObjectiveID
			AND UO.IsActive = 0
		)

	UPDATE I
	SET
		I.IndicatorTypeID = LFUI.IndicatorTypeID, 
		I.ObjectiveID = LFUI.ObjectiveID, 
		I.IndicatorName = LFUI.IndicatorName, 
		I.IndicatorDescription = LFUI.IndicatorDescription, 
		I.IndicatorSource = LFUI.IndicatorSource, 
		I.PositiveExample = LFUI.PositiveExample, 
		I.RiskAssumption = LFUI.RiskAssumption, 
		I.MeansOfVerification = LFUI.MeansOfVerification, 
		I.BaselineValue = LFUI.BaselineValue, 
		I.BaselineDate = LFUI.BaselineDate, 
		I.TargetValue = LFUI.TargetValue, 
		I.TargetDate = LFUI.TargetDate, 
		I.AchievedValue = LFUI.AchievedValue, 
		I.AchievedDate = LFUI.AchievedDate, 
		I.IsActive = LFUI.IsActive, 
		I.ActualDate = LFUI.ActualDate, 
		I.InProgressDate = LFUI.InProgressDate, 
		I.InProgressValue = LFUI.InProgressValue, 
		I.LogicalFrameworkStatusID = LFUI.LogicalFrameworkStatusID, 
		I.PlannedDate = LFUI.PlannedDate, 
		I.PlannedValue = LFUI.PlannedValue, 
		I.StatusUpdateDescription = LFUI.StatusUpdateDescription, 
		I.IndicatorNumber = LFUI.IndicatorNumber
	OUTPUT 'Indicator', INSERTED.IndicatorID INTO @tOutput1
	FROM logicalframework.Indicator I
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = I.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	--set indicators inactive which are associated with objectives marked inactive
	UPDATE I
	SET I.IsActive = 0
	FROM logicalframework.Indicator I
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID 
	WHERE EXISTS
		(
		SELECT 1
		FROM logicalframeworkupdate.Objective UO
		WHERE UO.ObjectiveID = I.ObjectiveID
			AND UO.IsActive = 0
		)

	DELETE T
	FROM logicalframework.IndicatorMilestone T
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = T.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	INSERT INTO logicalframework.IndicatorMilestone
		(IndicatorID, MilestoneID, AchievedValue, InProgressValue, PlannedValue, TargetValue)
	SELECT
		T.IndicatorID, 
		T.MilestoneID, 
		T.AchievedValue, 
		T.InProgressValue, 
		T.PlannedValue, 
		T.TargetValue
	FROM logicalframeworkupdate.IndicatorMilestone T
		JOIN logicalframeworkupdate.Indicator LFUI ON LFUI.IndicatorID = T.IndicatorID
			AND LFUI.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	UPDATE M
	SET
		M.MilestoneName = LFUM.MilestoneName, 
		M.IsActive = LFUM.IsActive,  
		M.MilestoneDate = LFUM.MilestoneDate
	OUTPUT 'Milestone', INSERTED.MilestoneID INTO @tOutput1
	FROM logicalframework.Milestone M
		JOIN logicalframeworkupdate.Milestone LFUM ON LFUM.MilestoneID = M.MilestoneID
			AND LFUM.LogicalFrameworkUpdateID = @nLogicalFrameworkUpdateID

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O1.EntityTypeCode, O1.EntityID
		FROM @tOutput1 O1
		ORDER BY O1.EntityTypeCode, O1.EntityID
	
	OPEN oCursor
	FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF @cEntityTypeCode = 'Objective'
			BEGIN
			
			EXEC eventlog.LogObjectiveAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogObjectiveAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Indicator'
			BEGIN
			
			EXEC eventlog.LogIndicatorAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogIndicatorAction @nEntityID, 'update', @PersonID, NULL
			
			END
		ELSE IF @cEntityTypeCode = 'Milestone'
			BEGIN
			
			EXEC eventlog.LogMilestoneAction @nEntityID, 'read', @PersonID, NULL
			EXEC eventlog.LogMilestoneAction @nEntityID, 'update', @PersonID, NULL
			
			END
		--ENDIF
		
		FETCH oCursor INTO @cEntityTypeCode, @nEntityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	COMMIT TRANSACTION

	EXEC eventlog.LogLogicalFrameworkUpdateAction @nLogicalFrameworkUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogLogicalFrameworkUpdateAction @nLogicalFrameworkUpdateID, 'update', @PersonID, NULL
	
	DELETE FROM logicalframeworkupdate.LogicalFrameworkUpdate

	TRUNCATE TABLE logicalframeworkupdate.Objective
	TRUNCATE TABLE logicalframeworkupdate.Indicator
	TRUNCATE TABLE logicalframeworkupdate.IndicatorMilestone
	TRUNCATE TABLE logicalframeworkupdate.Milestone

END
GO
--End procedure logicalframeworkupdate.ApproveLogicalFrameworkUpdate

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
--
-- Author:			Greg Yingling
-- Update date:	2015.05.26
-- Description:	Added the Audit Outcome, Audit Date, and Audit Evidence Call
--
-- Author:			Todd Pires
-- Update date:	2015.11.29
-- Description:	Refactored the audit recordset
--
-- Author:			Todd Pires
-- Update date:	2016.03.05
-- Description:	Removed the locations recordset, Refactored the audit recordset
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID,
		D.Title,
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentDeliveredToImplementerDate,
		dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted,
		EI.EquipmentInventoryID,
		EI.EquipmentOrderDate,
		dbo.FormatDate(EI.EquipmentOrderDate) AS EquipmentOrderDateFormatted,
		EI.EquipmentRemovalDate,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReasonID,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EI.EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		EI.EquipmentStatusID,
		EI.EquipmentUsageGB,
		EI.EquipmentUsageMinutes,
		EI.ExpirationDate,
		dbo.FormatDate(EI.ExpirationDate) AS ExpirationDateFormatted,
		EI.IMEIMACAddress,
		EI.InServiceDate,
		dbo.FormatDate(EI.InServiceDate) AS InServiceDateFormatted,
		EI.LicenseKey,
		EI.PONumber,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.UnitCost,
		ER.EquipmentRemovalReasonName,
		ES.EquipmentStatusName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		DIA.AuditQuantity, 
		DIA.AuditDate,
		dbo.FormatDate(DIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(DIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.DocumentFilename FROM Document.Document D WHERE D.DocumentID = DIA.DocumentID)
			THEN '<a class="btn btn-info" href="/document/getdocumentbydocumentName/documentName/' + (SELECT D.DocumentName FROM document.Document D WHERE D.DocumentID = DIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN DIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(' + CAST(DIA.DistributedInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.DistributedInventoryAudit DIA
		JOIN procurement.DistributedInventory DI ON DI.DistributedInventoryID = DIA.DistributedInventoryID
			AND DI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = DIA.AuditOutcomeID
	ORDER BY 2 DESC

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure reporting.GetAssetExportAssetUnitData
EXEC Utility.DropObject 'reporting.GetAssetListExportData'
EXEC Utility.DropObject 'reporting.GetAssetExportAssetUnitData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.07
-- Description:	A stored procedure to get data for the asset list export
-- =====================================================================
CREATE PROCEDURE reporting.GetAssetExportAssetUnitData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContactVettingData TABLE (ContactID INT, VettingTypeCode CHAR(2), VettingExpirationDate DATE, VettingOutcomeID INT)

	;
	WITH CVD AS
		(
		SELECT 
			MAX(CV.ContactVettingID) AS ContactVettingID,
			CV.ContactID,
			CV.ContactVettingTypeID
		FROM dbo.ContactVetting CV
		WHERE EXISTS
			(
			SELECT 1
			FROM asset.AssetUnit AU
				JOIN reporting.SearchResult SR ON SR.EntityID = AU.AssetID
					AND SR.EntityTypeCode = 'Asset'
					AND SR.PersonID = @PersonID
					AND AU.CommanderContactID = CV.ContactID
			)
		GROUP BY CV.ContactID, CV.ContactVettingTypeID
		)

	INSERT INTO @tContactVettingData
		(ContactID, VettingTypeCode, VettingExpirationDate, VettingOutcomeID)
	SELECT
		CVD.ContactID,
		CVT.ContactVettingTypeCode,
		(SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = CVD.ContactID),
		CV.VettingOutcomeID
	FROM CVD
		JOIN dbo.ContactVetting CV ON CV.ContactVettingID = CVD.ContactVettingID
		JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CVD.ContactVettingTypeID
			AND CVT.ContactVettingTypeCode = 'UK'

	UNION

	SELECT
		CVD.ContactID,
		CVT.ContactVettingTypeCode,
		(SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = CVD.ContactID),
		CV.VettingOutcomeID
	FROM CVD
		JOIN dbo.ContactVetting CV ON CV.ContactVettingID = CVD.ContactVettingID
		JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CVD.ContactVettingTypeID
			AND CVT.ContactVettingTypeCode = 'US'

	;
	WITH SD AS
		(
		SELECT 
			C.AssetUnitID,
			COUNT(C.ContactID) AS ItemCount,
			SUM(S.StipendAmount) AS StipendAmount,
			S.StipendCategory
		FROM dbo.Contact C
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.AssetUnitID > 0
				AND C.StipendID > 0
				AND S.StipendTypeCode = 'PoliceStipend'
		GROUP BY C.AssetUnitID, S.StipendCategory
		)

	SELECT
		A.AssetName,
		AT.AssetTypeName,
		AST.AssetStatusName,
		CASE WHEN A.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAsset,
		C.CommunityName,
		P.ProvinceName,
		AU.AssetUnitName,
		CASE WHEN AU.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAssetUnit,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirst') AS AssetUnitCommanderNameFormatted,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirst') AS AssetUnitDeputyCommanderNameFormatted,
		(SELECT VO.VettingOutcomeName FROM dropdown.VettingOutcome VO JOIN @tContactVettingData CVD ON CVD.VettingOutcomeID = VO.VettingOutcomeID AND CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'UK') AS UKVettingOutcomeName,
		dbo.FormatDate((SELECT CVD.VettingExpirationDate FROM @tContactVettingData CVD WHERE CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'UK')) AS UKVettingExpirationDateFormatted,
		(SELECT VO.VettingOutcomeName FROM dropdown.VettingOutcome VO JOIN @tContactVettingData CVD ON CVD.VettingOutcomeID = VO.VettingOutcomeID AND CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'US') AS USVettingOutcomeName,
		dbo.FormatDate((SELECT CVD.VettingExpirationDate FROM @tContactVettingData CVD WHERE CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'US')) AS USVettingExpirationDateFormatted,
		AU.DepartmentSizeName,
		AUC.AssetUnitCostName,
		(SELECT SUM(SD.StipendAmount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID) AS StipendTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID) AS ContactTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Command') AS CommandTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Commissioned Officer') AS CommissionedOfficerTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Non Commissioned Officer') AS NonCommissionedOfficerTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Police') AS PoliceTotal
	FROM asset.Asset A
		JOIN reporting.SearchResult SR ON SR.EntityID = A.AssetID
			AND SR.EntityTypeCode = 'Asset'
			AND SR.PersonID = @PersonID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
	ORDER BY A.AssetName

END
GO
--End procedure reporting.GetAssetExportAssetUnitData

--Begin procedure reporting.GetAssetExportContactData
EXEC Utility.DropObject 'reporting.GetAssetExportContactData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.07
-- Description:	A stored procedure to get data for the asset list export
-- =====================================================================
CREATE PROCEDURE reporting.GetAssetExportContactData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContactVettingData TABLE (ContactID INT, VettingTypeCode CHAR(2), VettingExpirationDate DATE, VettingOutcomeID INT)

	;
	WITH CVD AS
		(
		SELECT 
			MAX(CV.ContactVettingID) AS ContactVettingID,
			CV.ContactID,
			CV.ContactVettingTypeID
		FROM dbo.ContactVetting CV
		WHERE EXISTS
			(
			SELECT 1
			FROM reporting.SearchResult SR
				JOIN asset.AssetUnit AU ON AU.AssetID = SR.EntityID
				JOIN dbo.Contact C ON C.AssetUnitID = AU.AssetUnitID
					AND C.ContactID = CV.ContactID
					AND SR.EntityTypeCode = 'Asset'
					AND SR.PersonID = @PersonID
			)
		GROUP BY CV.ContactID, CV.ContactVettingTypeID
		)

	INSERT INTO @tContactVettingData
		(ContactID, VettingTypeCode, VettingExpirationDate, VettingOutcomeID)
	SELECT
		CVD.ContactID,
		CVT.ContactVettingTypeCode,
		C.UKVettingExpirationDate,
		CV.VettingOutcomeID
	FROM CVD
		JOIN dbo.Contact C ON C.ContactID = CVD.ContactID
		JOIN dbo.ContactVetting CV ON CV.ContactVettingID = CVD.ContactVettingID
		JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CVD.ContactVettingTypeID
			AND CVT.ContactVettingTypeCode = 'UK'

	UNION ALL

	SELECT
		CVD.ContactID,
		CVT.ContactVettingTypeCode,
		C.USVettingExpirationDate,
		CV.VettingOutcomeID
	FROM CVD
		JOIN dbo.Contact C ON C.ContactID = CVD.ContactID
		JOIN dbo.ContactVetting CV ON CV.ContactVettingID = CVD.ContactVettingID
		JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CVD.ContactVettingTypeID
			AND CVT.ContactVettingTypeCode = 'US'

	SELECT
		A.AssetName,
		AT.AssetTypeName,
		AST.AssetStatusName,
		CASE WHEN A.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAsset,
		C.CommunityName,
		AU.AssetUnitName,
		CASE WHEN AU.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAssetUnit,
		C1.ContactID,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirst') AS ContactNameFormatted,
		C1.Gender,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		DATEDIFF(YY, C1.DateOfBirth, getDate()) AS Age,
		S.StipendName,
		S.StipendCategory,
		S.StipendAmount,
		(SELECT VO.VettingOutcomeName FROM dropdown.VettingOutcome VO JOIN @tContactVettingData CVD ON CVD.VettingOutcomeID = VO.VettingOutcomeID AND CVD.ContactID = C1.ContactID AND CVD.VettingTypeCode = 'UK') AS UKVettingOutcomeName,
		dbo.FormatDate((SELECT CVD.VettingExpirationDate FROM @tContactVettingData CVD WHERE CVD.ContactID = C1.ContactID AND CVD.VettingTypeCode = 'UK')) AS UKVettingExpirationDateFormatted,
		(SELECT VO.VettingOutcomeName FROM dropdown.VettingOutcome VO JOIN @tContactVettingData CVD ON CVD.VettingOutcomeID = VO.VettingOutcomeID AND CVD.ContactID = C1.ContactID AND CVD.VettingTypeCode = 'US') AS USVettingOutcomeName,
		dbo.FormatDate((SELECT CVD.VettingExpirationDate FROM @tContactVettingData CVD WHERE CVD.ContactID = C1.ContactID AND CVD.VettingTypeCode = 'US')) AS USVettingExpirationDateFormatted
	FROM asset.Asset A
		JOIN reporting.SearchResult SR ON SR.EntityID = A.AssetID
			AND SR.EntityTypeCode = 'Asset'
			AND SR.PersonID = @PersonID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
		JOIN dbo.Contact C1 ON C1.AssetUnitID = AU.AssetUnitID
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.IsActive = 1
			AND C1.StipendID > 0
			AND EXISTS 
				( 
				SELECT 1 
				FROM dbo.ContactContactType CCT 
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID 
						AND CCT.ContactID = C1.ContactID 
						AND CT.ContactTypeCode IN ('JusticeStipend', 'PoliceStipend') 
				) 
	ORDER BY A.AssetName, AU.AssetUnitID, C1.ContactID

END
GO
--End procedure reporting.GetAssetExportContactData

--Begin procedure reporting.GetAssetExportEquipmentData
EXEC Utility.DropObject 'reporting.GetAssetExportEquipmentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.07
-- Description:	A stored procedure to get data for the asset list export
-- =====================================================================
CREATE PROCEDURE reporting.GetAssetExportEquipmentData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
		(
		SELECT 
			C.AssetUnitID,
			COUNT(C.ContactID) AS ItemCount,
			SUM(S.StipendAmount) AS StipendAmount,
			S.StipendCategory
		FROM dbo.Contact C
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.AssetUnitID > 0
				AND C.StipendID > 0
				AND S.StipendTypeCode = 'PoliceStipend'
		GROUP BY C.AssetUnitID, S.StipendCategory
		)

	SELECT
		A.AssetName,
		AT.AssetTypeName,
		AST.AssetStatusName,
		CASE WHEN A.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAsset,
		C.CommunityName,
		P.ProvinceName,
		AU.AssetUnitName,
		CASE WHEN AU.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAssetUnit,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID) AS ContactTotal,
		E.ItemName,
		E.TotalQuantity,
		E.TotalCost
	FROM asset.Asset A
		JOIN reporting.SearchResult SR ON SR.EntityID = A.AssetID
			AND SR.EntityTypeCode = 'Asset'
			AND SR.PersonID = @PersonID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN
			(
			SELECT
				D.EquipmentCatalogID,
				D.AssetUnitID, 
				D.ItemName,
				SUM(D.TotalQuantity) AS TotalQuantity,
				SUM(D.TotalCost) AS TotalCost
			FROM
				(
				SELECT
					EC.EquipmentCatalogID,
					DI.EndUserEntityID AS AssetUnitID, 
					EC.ItemName,
					DI.Quantity AS TotalQuantity,
					DI.Quantity * EI.UnitCost AS TotalCost
				FROM procurement.DistributedInventory DI
					JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = DI.EquipmentInventoryID
					JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
						AND DI.EndUserEntityTypeCode = 'AssetUnit'

				UNION ALL

				SELECT
					EC.EquipmentCatalogID,
					C.AssetUnitID, 
					EC.ItemName,
					DI.Quantity AS TotalQuantity,
					DI.Quantity * EI.UnitCost AS TotalCost
				FROM procurement.DistributedInventory DI
					JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = DI.EquipmentInventoryID
					JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
						AND DI.EndUserEntityTypeCode = 'Contact'
				) D
			GROUP BY D.AssetUnitID, D.EquipmentCatalogID, D.ItemName
			) E ON E.AssetUnitID = AU.AssetUnitID
	ORDER BY A.AssetName

END
GO
--End procedure reporting.GetAssetExportEquipmentData

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
--
-- Author:			Justin Branum
-- Create date: 2016.01.21
-- Description: Added New columns regrading Previous Unit data for vetting export report
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- =====================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicLastName,
		C1.ArabicMiddleName,
		C1.City,
		C1.AssetUnitID,
		C1.CommunityID,
		IIF 
			(
			C1.communityid = 0 OR C1.communityid IS NULL OR C1.communityid = '',
			dbo.GetCommunityNameByAssetUnitID(C1.AssetUnitID),
			dbo.GetCommunityNameByCommunityID(C1.CommunityID)
			) AS CommunityName,
		C1.ContactID,
		dbo.getContactTypesByContactID(C1.ContactID) AS ContactTypeName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.DescriptionOfDuties,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsRegimeDefector,
		C1.LastName,
		C1.MiddleName,
		C1.Notes,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		C1.PassportNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.PreviousDuties,
		C1.PreviousProfession,
		C1.PreviousRankOrTitle,
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDate,
		dbo.FormatUSDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateUS,
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDate,
		dbo.FormatUSDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateUS,
		C1.PreviousUnit AS PreviousUnitLocation,
		C1.Profession,
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,
		dbo.FormatUSDate(C1.StartDate) AS JoinDateUSFormatted,
		C1.State,
		C1.Title,
		dbo.FormatDate(C1.UKVettingExpirationDate) AS UKVettingExpirationDateFormatted,
		dbo.FormatUSDate(C1.USVettingExpirationDate) AS USVettingExpirationDateFormatted,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		C4.CountryID,
		C4.CountryName,
		C5.CountryID AS GovernmentIDNumberCountryID,
		C5.CountryName AS GovernmentIDNumberCountryName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		OAA.AssetName,
		OAA.TerritoryName,
		OAVO1.VettingOutcomeName AS USVettingOutcomeName,
		OAVO2.VettingOutcomeName AS UKVettingOutcomeName,
		P1.ProjectID,
		P1.ProjectName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,
    C1.MotherName,
		C1.ArabicMotherName,
		C1.ArabicAddress,
		C1.ArabicTitle,
		C1.ArabicPlaceOfBirth
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C4 ON C4.CountryID = C1.CountryID
		JOIN dropdown.Country C5 ON C5.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		OUTER APPLY
			(
			SELECT 
				A.AssetName,
				dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', A.CommunityID) AS TerritoryName
			FROM asset.Asset A 
				JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
					AND AU.AssetUnitID = C1.AssetUnitID
			) OAA
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 1
			ORDER BY CV.ContactVettingID DESC
			) OAVO1
		OUTER APPLY
			(
			SELECT TOP 1
				VO.VettingOutcomeName
			FROM dbo.ContactVetting CV
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
				JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CV.ContactVettingTypeID
					AND CV.ContactID = C1.ContactID
					AND CV.ContactVettingTypeID = 2
			ORDER BY CV.ContactVettingID DESC
			) OAVO2
	ORDER BY C1.ContactID
		
END
GO
--End procedure reporting.GetContact

--Begin procedure reporting.GetContactStipendStatus
EXEC Utility.DropObject 'reporting.GetContactStipendStatus'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2018.06.16
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetContactStipendStatus

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		C.ContactID AS SysID, 
		C.CellPhoneNumber,
		C.ContactID,
		dbo.GetContactCommunityByContactID(C.ContactID) AS ContactLocation,
		C.FirstName,
		C.Gender,
		C.GovernmentIDNumber,
		C.LastName,
		C.MiddleName,
		C.MotherName,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID( C.CommunityID )) AS ProvinceName,
		CASE
			WHEN C.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C.CommunityID) = 1
			THEN S.StipendAmount
			WHEN dbo.GetProvinceIDByCommunityID( C.CommunityID )> 0 AND dbo.IsProvinceStipendEligible(dbo.GetProvinceIDByCommunityID( C.CommunityID )) = 1
			THEN S.StipendAmount
			ELSE 0
		END AS StipendAmount,

		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center,
		C.ArabicMotherName,
		C.ArabicFirstName,
		C.ArabicMiddleName,
		C.ArabicLastName,
		S.StipendName ,
		CT.ContactTypes,
		NSC.NotShowedCount,
		SWI.ShowedWithIDCount,
		SWIC.ShowedWithoutIDCount, 
		CSP.CSP201801,
		CSP.CSP201802,
		CSP.CSP201803,
		CSP.CSP201804,
		CSP.CSP201805,
		CSP.CSP201806,
		CSP.CSP201807,
		CSP.CSP201808,
		CSP.CSP201809,
		CSP.CSP201810,
		CSP.CSP201811,
		CSP.CSP201812,
		CSP.CSP201901,
		CSP.CSP201902,
		CSP.CSP201903,
		CSP.CSP201904,
		CSP.CSP201905,
		CSP.CSP201906,
		CSP.CSP201907
		FROM reporting.SearchResult SR
			JOIN dbo.Contact C ON C.ContactID = SR.EntityID AND SR.PersonID = @PersonID AND SR.EntityTypecode= 'Contact'
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
			LEFT JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			LEFT JOIN asset.Asset A ON A.AssetID = AU.AssetID
			LEFT JOIN dropdown.AssetUnitCost AUCR ON AUCR.AssetUnitCostID = AU.AssetUnitCostID
			OUTER APPLY
				(
				SELECT SUBSTRING(
					( 
						SELECT  ',' + ContactTypeName 
						FROM ContactContactType CCT
						JOIN DropDown.ContactType CT ON CCT.ContactTypeID = CT.ContactTypeID AND CCT.ContactID = C.ContactID
						ORDER BY ContactTypeName
						
						FOR XML PATH('')),2,200000) AS ContactTypes
				) CT 		
			OUTER APPLY
				(
				SELECT COUNT(SCS.ContactStipendStatusname) AS NotShowedCount
				FROM ContactStipendPayment CSP
					JOIN Dropdown.ContactStipendStatus SCS ON CSP.ContactStipendStatusID = SCS.ContactStipendStatusID 
						AND CSP.ContactID = C.ContactID
						AND SCS.ContactStipendStatusname = 'Not Showed'
	 			) NSC
			OUTER APPLY
				(
				SELECT COUNT(SCS.ContactStipendStatusname) AS ShowedWithIDCount
				FROM ContactStipendPayment CSP
					JOIN Dropdown.ContactStipendStatus SCS ON CSP.ContactStipendStatusID = SCS.ContactStipendStatusID 
						AND CSP.ContactID = C.ContactID
						AND SCS.ContactStipendStatusname = 'Showed with ID'
 				) SWI
			OUTER APPLY
				(
				SELECT COUNT(SCS.ContactStipendStatusname) AS ShowedWithoutIDCount
				FROM ContactStipendPayment CSP
					JOIN Dropdown.ContactStipendStatus SCS ON CSP.ContactStipendStatusID = SCS.ContactStipendStatusID 
					AND CSP.ContactID = C.ContactID
					AND SCS.ContactStipendStatusname = 'Showed without ID'
				) SWIC
			OUTER APPLY
				(
				SELECT
					PVT.ContactID,
					PVT.CSP201801,
					PVT.CSP201802,
					PVT.CSP201803,
					PVT.CSP201804,
					PVT.CSP201805,
					PVT.CSP201806,
					PVT.CSP201807,
					PVT.CSP201808,
					PVT.CSP201809,
					PVT.CSP201810,
					PVT.CSP201811,
					PVT.CSP201812,
					PVT.CSP201901,
					PVT.CSP201902,
					PVT.CSP201903,
					PVT.CSP201904,
					PVT.CSP201905,
					PVT.CSP201906,
					PVT.CSP201907
				FROM
					(
					SELECT 
						'CSP' + CAST(CSP.PaymentYear * 100 +  CSP.PaymentMonth AS Nvarchar(6)) AS PaymentYearMonth, 
						SCS.ContactStipendStatusname, 
						CSP.ContactID
					FROM reporting.SearchResult SR
						JOIN contactstipendpayment csp ON CSP.ContactID = SR.EntityID 
							AND SR.PersonID = @PersonID 
							AND SR.EntityTypecode= 'Contact'
						JOIN Dropdown.ContactStipendStatus SCS ON CSP.ContactStipendStatusID = SCS.ContactStipendStatusID 
					) AS D
				PIVOT
					(
	 				MAX(D.ContactStipendStatusName)
	 				FOR D.PaymentYearMonth IN 
	 					(
	 					CSP201801,
	 					CSP201802,
	 					CSP201803,
	 					CSP201804,
	 					CSP201805,
	 					CSP201806,
	 					CSP201807,
	 					CSP201808,
	 					CSP201809,
	 					CSP201810,
	 					CSP201811,
						CSP201812,
						CSP201901,
						CSP201902,
						CSP201903,
						CSP201904,
						CSP201905,
						CSP201906,
						CSP201907
						)
					) AS PVT
				) CSP
	WHERE CSP.ContactID = C.ContactID
	ORDER BY ContactLocation, Center, C.LastName, C.FirstName, C.MiddleName, C.ContactID

END
GO
--End procedure reporting.GetContactStipendStatus

--Begin procedure reporting.GetOpsFundReport
EXEC Utility.DropObject 'reporting.GetOpsFundReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
-- ======================================================================
CREATE PROCEDURE reporting.GetOpsFundReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH AUED AS
		(
		SELECT 
			SUM(AUE.ExpenseAmountAuthorized) AS ExpenseAmountAuthorized,
			AUE.AssetUnitID
		FROM asset.AssetUnitExpense AUE
			JOIN 
				(
				SELECT TOP 1 
					CSP.ConceptNoteID,
					CSP.PaymentMonth, 
					CSP.PaymentYear, 
					CSP.ProvinceID
				FROM dbo.ContactStipendPayment CSP 
					JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID
						AND SR.EntityTypeCode = 'ContactStipendPayment'
						AND SR.PersonID = @PersonID
				) D ON D.ConceptNoteID = AUE.ConceptNoteID
					AND D.PaymentMonth = AUE.PaymentMonth
					AND D.PaymentYear = AUE.PaymentYear
					AND D.ProvinceID = AUE.ProvinceID
		GROUP BY AUE.AssetUnitID
		)

	SELECT 
		C.GovernmentIDNumber,
		(
		SELECT P.ProvinceName 
		FROM dbo.Province P 
		WHERE P.ProvinceID = CASE WHEN CSP.CommunityID > 0 THEN dbo.GetProvinceIDByCommunityID(CSP.CommunityID) ELSE CSP.ProvinceID END
		) AS ProvinceName,
		(
		SELECT P.ArabicProvinceName 
		FROM dbo.Province P 
		WHERE P.ProvinceID = CASE WHEN CSP.CommunityID > 0 THEN dbo.GetProvinceIDByCommunityID(CSP.CommunityID) ELSE CSP.ProvinceID END
		) AS ArabicProvinceName,
		C1.CommunityName,
		C1.ArabicCommunityName,
		dbo.GetCommunityNameByCommunityID(CSP.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center,
		CSP.StipendName,
		A.AssetName,
		AU.AssetUnitName,
		CSP.ContactID,
		dbo.FormatPersonNameByPersonID(@PersonID ,'LastFirst') AS AJACSRepresentative,
		dbo.FormatContactNameByContactID(CSP.ContactID, 'LastFirst') AS BeneficaryName,
		C.ArabicFirstName +' ' + C.ArabicMiddlename + ' ' + C.ArabicLastName  AS ArabicBeneficaryName,
		CSP.StipendAmountAuthorized,
		dbo.FormatDate(StipendAuthorizedDate) AS StipendAuthorizedDateFormatted,
		DateName(month, DateAdd(month, CSP.PaymentMonth, 0 ) - 1) + ' - ' + CAST(PaymentYear AS VARCHAR(50)) AS PaymentMonthYear,
		AUC.AssetUnitCostName,
		ISNULL((SELECT AUED.ExpenseAmountAuthorized FROM AUED WHERE AUED.AssetUnitID = C.AssetUnitID), 0) AS ExpenseAmountAuthorized
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID 
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
		JOIN dbo.Community C1 ON C1.CommunityID = CSP.CommunityID
		JOIN dbo.Contact C ON C.contactid = CSP.ContactID
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
		JOIN dropdown.AssetUnitCost AUC on AUC.AssetUnitCostID = AU.AssetUnitCostID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
	ORDER BY ProvinceName, CommunityName, StipendName, BeneficaryName
	
END
GO
--End procedure reporting.GetOpsFundReport

--Begin procedure reporting.GetSerialNumberTracker
EXEC Utility.DropObject 'reporting.GetSerialNumberTracker'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data from the procurement.EquipmentInventory table
-- =========================================================================================
CREATE PROCEDURE reporting.GetSerialNumberTracker

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
SELECT
        0                                                    AS ContactID              ,
        0                                                    AS EquipmentDistributionID,
        0                                                    AS ConceptNoteID          ,
        dbo.FormatConceptNoteReferenceCode(EI.ConceptNoteID) AS ReferenceCode          ,
        (
                SELECT
                        Title
                FROM
                        ConceptNote
                WHERE
                        conceptnoteid = EI.ConceptNoteID) AS ConceptNoteTitle,
        (
                SELECT
                        CNT.ConceptNoteTypeName
                FROM
                        dropdown.ConceptNoteType CNT
                JOIN
                        dbo.ConceptNote CN
                ON
                        CN.ConceptNoteTypeID = CNT.ConceptNoteTypeID
                AND     CN.ConceptNoteID     = EI.ConceptNoteID) AS ConceptNoteTypeName                       ,
        EC.ItemName                                                                                           ,
        EI.SerialNumber                                                                                       ,
        CN.TaskCode                                                                                           ,
        EI.IMEIMACAddress                                                                                     ,
        EI.Sim                                                                                                ,
        EI.BudgetCode                                                                                         ,
        EI.UnitCost                                                                                           ,
        EI.Quantity - pdi.DistributedInventoryTotal AS quantity                                               ,
        EC.QuantityOfIssue                                                                                    ,
        (EI.UnitCost * EI.Quantity * EC.QuantityOfIssue)       TotalCost                                      ,
        ''                                                     AS DeliveredToRecipientDateFormatted           ,
        dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted,
        dbo.FormatDate(EI.EquipmentOrderDate)                  AS EquipmentOrderDateFormatted                 ,
        EI.Quantity - EI.QuantityDistributed                   AS QuantityInStock                             ,
        0                                                      AS QuantityDistributed                         ,
        ''                                                     AS RecipientContactFullName                    ,
        ''                                                     AS Assetname                                   ,
        ''                                                     AS AssetTypeName                               ,
        ''                                                     AS EndUserContact                              ,
        ''                                                     AS DeliveredToEndUserDateDateFormatted         ,
        NULL                                                   AS LastEquipmentAuditDate                      ,
        ''                                                     AS LastEquipmentAuditOutcome                   ,
        EC.ItemMake + '/' + EC.ItemModel                       AS MakeAndModel                                ,
        ECC.EquipmentCatalogCategoryName                                                                      ,
        '' AS ContactGender                                                                                   ,
        '' AS EmployerName                                                                                    ,
        '' AS EmployerTypeName                                                                                ,
        EI.LicenseKey                                                                                         ,
        EI.EquipmentUsageMinutes                                                                              ,
        EI.EquipmentUsageGB                                                                                   ,
        dbo.FormatDate(EI.InServiceDate) AS InServiceDateDateFormatted                                        ,
        EI.Comments                                                                                           ,
        ''   AS ContactCommunity                                                                                ,
        ''   AS AssetUnitCommunity                                                                              ,
        ''   AS ContactProvince                                                                                 ,
        ''   AS AssetUnitProvince                                                                               ,
        ''   AS ContactAssetUnit                                                                                ,
        ''   AS ContactAssetUnitCommunity                                                                       ,
        ''   AS ContactAssetUnitProvince                                                                        ,
        NULL AS AuditRecordUpdateDate
FROM
        Reporting.SearchResult SR
JOIN
        procurement.EquipmentInventory EI
ON
        EI.EquipmentInventoryID = SR.EntityID
AND     SR.PersonID             = @PersonID
AND     SR.EntityTypeCode       = 'TerritoryEquipmentInventory'
JOIN
        ConceptNote CN
ON
        EI.ConceptNoteID = CN.ConceptNoteID
JOIN
        procurement.EquipmentCatalog EC
ON
        EC.EquipmentCatalogID = EI.EquipmentCatalogID
JOIN
        dropdown.EquipmentCatalogCategory ECC
ON
        ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
AND     (
                EI.Quantity -
                (
                        SELECT
                                ISNULL(SUM(DI.Quantity),0) AS DistributedInventoryTotal
                        FROM
                                procurement.DistributedInventory DI
                        WHERE
                                DI.EquipmentInventoryID = SR.EntityID
                        AND     SR.PersonID             = @PersonID
                        AND     SR.EntityTypeCode       = 'TerritoryEquipmentInventory' )) > 0 OUTER APPLY
        (
                SELECT
                        ISNULL(SUM(DI.Quantity) ,0) AS DistributedInventoryTotal
                FROM
                        procurement.DistributedInventory DI
                WHERE
                        DI.EquipmentInventoryID = SR.EntityID
                AND     SR.PersonID             = @PersonID
                AND     SR.EntityTypeCode       = 'TerritoryEquipmentInventory' ) PDI

UNION ALL

SELECT
        CO.ContactID                                                         ,
        EI.ConceptNoteID                                                     ,
        DI.EquipmentDistributionID                                           ,
        dbo.FormatConceptNoteReferenceCode(EI.ConceptNoteID) AS ReferenceCode,
        (
                SELECT
                        Title
                FROM
                        ConceptNote
                WHERE
                        conceptnoteid = EI.ConceptNoteID) AS ConceptNoteTitle,
        (
                SELECT
                        CNT.ConceptNoteTypeName
                FROM
                        dropdown.ConceptNoteType CNT
                JOIN
                        dbo.ConceptNote CN
                ON
                        CN.ConceptNoteTypeID = CNT.ConceptNoteTypeID
                AND     CN.ConceptNoteID     = EI.ConceptNoteID) AS ConceptNoteTypeName                                                                                      ,
        EC.ItemName                                                                                                                                                          ,
        EI.SerialNumber                                                                                                                                                      ,
        CN.TaskCode                                                                                                                                                          ,
        EI.IMEIMACAddress                                                                                                                                                    ,
        EI.Sim                                                                                                                                                               ,
        EI.BudgetCode                                                                                                                                                        ,
        EI.UnitCost                                                                                                                                                          ,
        DI.Quantity                                                                                                                                                          ,
        EC.QuantityOfIssue                                                                                                                                                   ,
        (EI.UnitCost * EI.Quantity * EC.QuantityOfIssue)       TotalCost                                                                                                     ,
        dbo.FormatDate(ED.DeliveredToRecipientDate)            AS DeliveredToRecipientDateFormatted                                                                          ,
        dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted                                                               ,
        dbo.FormatDate(EI.EquipmentOrderDate)                  AS EquipmentOrderDateFormatted                                                                                ,
        EI.Quantity - EI.QuantityDistributed                   AS QuantityInStock                                                                                            ,
        EI.QuantityDistributed                                                                                                                                               ,
        dbo.FormatContactNameByContactID(ED.RecipientContactID, 'LastFirst') AS RecipientContactFullName                                                                     ,
        AU.AssetName                                                                                                                                                         ,
        AU.AssetTypeName                                                                                                                                                     ,
        CASE WHEN ISNULL(AU.AssetName, '') = '' THEN dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') ELSE AU.AssetName END                        AS EndUserContact                     ,
        dbo.FormatDate(DI.DeliveredToEndUserDate)                                                                                                             AS DeliveredToEndUserDateDateFormatted,
        CASE ISNULL(DI.EquipmentDistributionID,0) WHEN 0 THEN NULL ELSE dbo.FormatDate(procurement.GetLastEquipmentAuditDate(DI.DistributedInventoryID) ) END AS LastEquipmentAuditDate             ,
        procurement.GetLastEquipmentAuditOutcome(DI.DistributedInventoryID)                                                                                   AS LastEquipmentAuditOutcome          ,
        EC.ItemMake + '/' + EC.ItemModel                                                                                                                      AS MakeAndModel                       ,
        ECC.EquipmentCatalogCategoryName                                                                                                                                                            ,
        CO.Gender AS ContactGender                                                                                                                                                                  ,
        CO.EmployerName                                                                                                                                                                             ,
        CO.EmployerTypeName                                                                                                                                                                         ,
        EI.LicenseKey                                                                                                                                                                               ,
        EI.EquipmentUsageMinutes                                                                                                                                                                    ,
        EI.EquipmentUsageGB                                                                                                                                                                         ,
        dbo.FormatDate(EI.InServiceDate) AS InServiceDateDateFormatted                                                                                                                              ,
        EI.Comments                                                                                                                                                                                 ,
        CO.CommunityName AS ContactCommunity                                                                                                                                                        ,
        AU.CommunityName AS AssetUnitCommunity                                                                                                                                                      ,
        CO.ProvinceName  AS ContactProvince                                                                                                                                                         ,
        AU.ProvinceName  AS AssetUnitProvince                                                                                                                                                       ,
        CO.ContactAssetUnit                                                                                                                                                                         ,
        CO.ContactAssetUnitCommunity                                                                                                                                                                ,
        CO.ContactAssetUnitProvince                                                                                                                                                                 ,
       dbo.FormatDate( (
                SELECT
                        TOP 1 CreateDateTime
                FROM
                        procurement.DistributedInventoryAudit
                ORDER BY
                        CreateDateTime DESC)) AS AuditRecordUpdateDate
FROM
        Reporting.SearchResult SR
JOIN
        procurement.EquipmentInventory EI
ON
        EI.EquipmentInventoryID = SR.EntityID
AND     SR.PersonID             = @PersonID
AND     SR.EntityTypeCode       = 'TerritoryEquipmentInventory'
JOIN
        ConceptNote CN
ON
        EI.ConceptNoteID = CN.ConceptNoteID
JOIN
        procurement.DistributedInventory DI
ON
        EI.EquipmentInventoryID = DI.EquipmentInventoryID
JOIN
        procurement.EquipmentDistribution ED
ON
        ED.EquipmentDistributionID = DI.EquipmentDistributionID
JOIN
        procurement.EquipmentCatalog EC
ON
        EC.EquipmentCatalogID = EI.EquipmentCatalogID
JOIN
        dropdown.EquipmentCatalogCategory ECC
ON
        ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID OUTER APPLY
        (
                SELECT
                        AU.AssetID                                                       ,
                        A.AssetName                                                      ,
                        AT.AssetTypeName                                                 ,
                        dbo.GetCommunityNameByCommunityID(A.CommunityID)                                AS CommunityName,
                        dbo.GetProvinceNameByProvinceID( dbo.GetProvinceIDByCommunityID(A.CommunityID)) AS ProvinceName
                FROM
                        Asset.AssetUnit AU
                JOIN
                        Asset.Asset A
                ON
                        AU.AssetID = A.AssetID
                JOIN
                        DropDown.AssetType AT
                ON
                        AT.AssetTypeID           =A.AssetTypeID
                AND     AU.AssetUnitID           = DI.EndUserEntityID
                AND     DI.EndUserEntityTypeCode = 'AssetUnit'
                AND     DI.EquipmentInventoryID  = EI.EquipmentInventoryID ) AU OUTER APPLY
        (
                SELECT
                        ContactID ,
                        (
                                SELECT
                                        AssetName
                                FROM
                                        Asset.Asset A
                                JOIN
                                        Asset.AssetUnit AU
                                ON
                                        A.AssetID      = Au.AssetID
                                AND     AU.AssetUnitID = C.AssetUnitID ) AS ContactAssetUnit,
                        (
                                SELECT
                                        dbo.GetCommunityNameByCommunityID(A.CommunityID)
                                FROM
                                        Asset.Asset A
                                JOIN
                                        Asset.AssetUnit AU
                                ON
                                        A.AssetID      = Au.AssetID
                                AND     AU.AssetUnitID = C.AssetUnitID ) AS ContactAssetUnitCommunity,
                        (
                                SELECT
                                        dbo.GetProvinceNameByProvinceID( dbo.GetProvinceIDByCommunityID(A.CommunityID) )
                                FROM
                                        Asset.Asset A
                                JOIN
                                        Asset.AssetUnit AU
                                ON
                                        A.AssetID      = Au.AssetID
                                AND     AU.AssetUnitID = C.AssetUnitID ) AS ContactAssetUnitProvince,
                        dbo.GetCommunityNameByCommunityID(CommunityID)   AS CommunityName           ,
                        C.Gender                                                                    ,
                        C.EmployerName                                                              ,
                        ET.EmployerTypeName                                                         ,
                        dbo.GetProvinceNameByProvinceID( dbo.GetProvinceIDByCommunityID(C.CommunityID)) AS ProvinceName
                FROM
                        Contact C
                JOIN
                        DropDown.EmployerType ET
                ON
                        ET.EmployerTypeID = C.EmployerTypeID
                WHERE
                        DI.EndUserEntityTypeCode = 'Contact'
                AND     DI.EndUserEntityID       = C.ContactID
                AND     ED.IsActive              = 0 ) CO

END
GO
--End procedure reporting.GetSerialNumberTracker

--Begin procedure reporting.GetStipendPaymentReport
EXEC utility.DropObject 'reporting.GetStipendPaymentReport'
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date: 2015.09.30
-- Description:	A stored procedure to add data to the reporting.StipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016.09.18
-- Description:	Refactored to support assets
--
-- Author:			Todd Pires
-- Create date:	2017.04.28
-- Description:	Refactored to support categories
-- =================================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT 

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cArabicProvinceName NVARCHAR(MAX)
	DECLARE @cProvinceName NVARCHAR(MAX)
	DECLARE @cStipendName VARCHAR(50)
	DECLARE @nAssetUnitID INT	
	DECLARE @nContactCount INT	
	DECLARE @nPaymentMonth INT
	DECLARE @nPaymentYear INT
	DECLARE @nStipendAmountAuthorized NUMERIC(18, 2)
	DECLARE @nProvinceID INT = 0
	DECLARE @nConceptNoteID INT = 0

	DELETE SP
	FROM reporting.StipendPayment SP

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			COUNT(C.ContactID) AS ContactCount,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			S.StipendCategory,
			C.AssetUnitID
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
			JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
				AND C.AssetUnitID > 0
		GROUP BY 
			S.StipendCategory,
			C.AssetUnitID
	
	OPEN oCursor
	FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	WHILE @@fetch_status = 0
		BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM reporting.StipendPayment SP WHERE SP.PersonID = @PersonID AND SP.AssetUnitID = @nAssetUnitID)
			INSERT INTO reporting.StipendPayment (PersonID,AssetUnitID) VALUES (@PersonID,@nAssetUnitID)
		--ENDIF

		UPDATE SP
		SET 
			SP.[Total Count] = [Total Count] + @nContactCount,
			SP.[Total Stipend] = [Total Stipend] + @nStipendAmountAuthorized,
			SP.[Policemen] = SP.[Policemen] + CASE WHEN @cStipendName = 'Police' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Policemen Count] = SP.[Policemen Count] + CASE WHEN @cStipendName = 'Police' THEN @nContactCount ELSE 0 END,

			SP.[Non Commissioned Officer] = SP.[Non Commissioned Officer] + CASE WHEN @cStipendName = 'Non Commissioned Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Non Commissioned Officer Count] = SP.[Non Commissioned Officer Count] + CASE WHEN @cStipendName = 'Non Commissioned Officer' THEN @nContactCount ELSE 0 END,

			SP.[Commissioned Officer] = SP.[Commissioned Officer] + CASE WHEN @cStipendName = 'Commissioned Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Commissioned Officer Count] = SP.[Commissioned Officer Count] + CASE WHEN @cStipendName = 'Commissioned Officer' THEN @nContactCount ELSE 0 END,

			SP.[Commander] = SP.[Commander] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			SP.[Commander Count] = SP.[Commander Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END
			--SP.[Command] = SP.[Command] + CASE WHEN @cStipendName = 'Command' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Command Count] = SP.[Command Count] + CASE WHEN @cStipendName = 'Command' THEN @nContactCount ELSE 0 END,
			--SP.[General] = SP.[General] + CASE WHEN @cStipendName = 'General' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[General Count] = SP.[General Count] + CASE WHEN @cStipendName = 'General' THEN @nContactCount ELSE 0 END,
			--SP.[Colonel] = SP.[Colonel] + CASE WHEN @cStipendName = 'Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Colonel Count] = SP.[Colonel Count] + CASE WHEN @cStipendName = 'Colonel' THEN @nContactCount ELSE 0 END,
			--SP.[Colonel Doctor] = SP.[Colonel Doctor] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Colonel Doctor Count] = SP.[Colonel Doctor Count] + CASE WHEN @cStipendName = 'Colonel Doctor' THEN @nContactCount ELSE 0 END,
			--SP.[Lieutenant Colonel] = SP.[Lieutenant Colonel] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Lieutenant Colonel Count] = SP.[Lieutenant Colonel Count] + CASE WHEN @cStipendName = 'Lieutenant Colonel' THEN @nContactCount ELSE 0 END,
			--SP.[Major] = SP.[Major] + CASE WHEN @cStipendName = 'Major' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Major Count] = SP.[Major Count] + CASE WHEN @cStipendName = 'Major' THEN @nContactCount ELSE 0 END,
			--SP.[Captain] = SP.[Captain] + CASE WHEN @cStipendName = 'Captain' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Captain Count] = SP.[Captain Count] + CASE WHEN @cStipendName = 'Captain' THEN @nContactCount ELSE 0 END,
			--SP.[Captain Doctor] = SP.[Captain Doctor] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Captain Doctor Count] = SP.[Captain Doctor Count] + CASE WHEN @cStipendName = 'Captain Doctor' THEN @nContactCount ELSE 0 END,
			--SP.[First Lieutenant] = SP.[First Lieutenant] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[First Lieutenant Count] = SP.[First Lieutenant Count] + CASE WHEN @cStipendName = 'First Lieutenant' THEN @nContactCount ELSE 0 END,
			--SP.[Contracted Officer] = SP.[Contracted Officer] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Contracted Officer Count] = SP.[Contracted Officer Count] + CASE WHEN @cStipendName = 'Contracted Officer' THEN @nContactCount ELSE 0 END,
			--SP.[First Sergeant] = SP.[First Sergeant] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[First Sergeant Count] = SP.[First Sergeant Count] + CASE WHEN @cStipendName = 'First Sergeant' THEN @nContactCount ELSE 0 END,
			--SP.[Sergeant] = SP.[Sergeant] + CASE WHEN @cStipendName = 'Sergeant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Sergeant Count] = SP.[Sergeant Count] + CASE WHEN @cStipendName = 'Sergeant' THEN @nContactCount ELSE 0 END,
			--SP.[First Adjutant] = SP.[First Adjutant] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[First Adjutant Count] = SP.[First Adjutant Count] + CASE WHEN @cStipendName = 'First Adjutant' THEN @nContactCount ELSE 0 END,
			--SP.[Adjutant] = SP.[Adjutant] + CASE WHEN @cStipendName = 'Adjutant' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Adjutant Count] = SP.[Adjutant Count] + CASE WHEN @cStipendName = 'Adjutant' THEN @nContactCount ELSE 0 END,
			--SP.[Policeman] = SP.[Policeman] + CASE WHEN @cStipendName = 'Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Policeman Count] = SP.[Policeman Count] + CASE WHEN @cStipendName = 'Policeman' THEN @nContactCount ELSE 0 END,
			--SP.[Contracted Policeman] = SP.[Contracted Policeman] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nStipendAmountAuthorized ELSE 0 END,
			--SP.[Contracted Policeman Count] = SP.[Contracted Policeman Count] + CASE WHEN @cStipendName = 'Contracted Policeman' THEN @nContactCount ELSE 0 END
		FROM reporting.StipendPayment SP
		WHERE SP.PersonID = @PersonID
			AND SP.AssetUnitID = @nAssetUnitID
					
		FETCH oCursor INTO @nContactCount, @nStipendAmountAuthorized, @cStipendName, @nAssetUnitID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor
	
	SELECT 
		@nPaymentMonth = PaymentMonth, 
		@nPaymentYear = PaymentYear  ,
		@cProvinceName = dbo.GetProvinceNameByProvinceID(CSP.ProvinceID),
		@nProvinceID = ProvinceID , 
		@nConceptNoteID = ConceptNoteID ,
		@cArabicProvinceName =dbo.GetArabicProvinceNameByProvinceID(CSP.ProvinceID)
		FROM dbo.ContactStipendPayment CSP 
			JOIN 
				(
				SELECT TOP 1 EntityID	
				FROM reporting.SearchResult SR 
				WHERE SR.EntityTypeCode = 'ContactStipendPayment'
					AND SR.PersonID = @PersonID
				) D ON D.EntityID = CSP.ContactStipendPaymentID
	
	SELECT 
		SP.*,
		DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) AS PaymentMonth,

		CASE 
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'January' THEN N'كانون الثاني'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'February' THEN N'شباط'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'March' THEN N'آذار'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'April' THEN N'نيسان'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'May' THEN N'أيار'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'June' THEN N'حزيران'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'July' THEN N'تموز'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'August' THEN N'آب'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'September' THEN N'أيلول'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'October' THEN N'تشرين الأول'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'November' THEN N'تشرين الثاني'
			WHEN DATENAME(mm, DATEADD(mm, @nPaymentMonth,-1)) = 'December' THEN N'كانون الأول'
		END AS ArabicMonth,

		@nPaymentYear AS PaymentYear,
		@cProvinceName AS ProvinceName,
		@cArabicProvinceName AS ArabicProvinceName,
		AU.DepartmentSizeName,
		AUE.ExpenseAmountAuthorized,
		dbo.GetCommunityNameByCommunityID(A.CommunityID) + '-' + A.AssetName + '-' + AU.AssetUnitName AS Center
	FROM reporting.StipendPayment SP
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = SP.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		OUTER APPLY
			(
			SELECT SUM(AUE.ExpenseAmountAuthorized)  as ExpenseAmountAuthorized
			FROM asset.AssetUnitExpense AUE 
			WHERE AUE.PaymentMonth = @nPaymentMonth 
				AND AUE.PaymentYear = @nPaymentYear 
				AND AUE.ProvinceID = @nProvinceID  
				AND AUE.StipendTypeCode = 'PoliceStipend' 
				AND AUE.ConceptNoteID = @nConceptNoteID
				AND AUE.AssetUnitID = AU.AssetUnitID
			) AUE


END
GO
--End procedure reporting.GetStipendPaymentReport