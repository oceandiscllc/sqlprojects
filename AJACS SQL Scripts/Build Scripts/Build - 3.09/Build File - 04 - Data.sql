USE AJACS
GO

--Begin table dropdown.ConceptNoteType
UPDATE CNT
SET 
	CNT.IsActive = CASE WHEN CNT.ConceptNoteTypeCode = 'MER' THEN 0 ELSE 1 END
FROM dropdown.ConceptNoteType CNT
WHERE CNT.ConceptNoteTypeCode IN ('M&E', 'MER', 'Research')
GO

UPDATE CNT
SET 
	CNT.ConceptNoteTypeName = 'Police'
FROM dropdown.ConceptNoteType CNT
WHERE CNT.ConceptNoteTypeCode = 'PoliceDevelopment'
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ConceptNoteType CNT WHERE CNT.ConceptNoteTypeCode = 'Other')
	BEGIN

	INSERT INTO dropdown.ConceptNoteType
		(ConceptNoteTypeCode, ConceptNoteTypeName)
	VALUES
		('Other', 'Other/Admin')

	END
--ENDIF
--End table dropdown.ConceptNoteType

--Begin table dropdown.FundingSource
IF NOT EXISTS (SELECT 1 FROM dropdown.FundingSource FS WHERE FS.FundingSourceCode = 'USEU')
	BEGIN

	INSERT INTO dropdown.FundingSource
		(FundingSourceCode, FundingSourceName)
	VALUES
		('USEU', 'US - EU')

	END
--ENDIF
--End table dropdown.FundingSource

--Begin table dropdown.ContactStipendStatus
TRUNCATE TABLE dropdown.ContactStipendStatus
GO

SET IDENTITY_INSERT dropdown.ContactStipendStatus ON
GO

INSERT INTO dropdown.ContactStipendStatus (ContactStipendStatusID) VALUES (0)
GO

SET IDENTITY_INSERT dropdown.ContactStipendStatus OFF;
GO

INSERT INTO dropdown.ContactStipendStatus 
	(ContactStipendStatusName, DisplayOrder)
VALUES
	('Not Showed', 1),
	('Showed with ID', 2),
	('Showed without ID', 3)
GO
--End table dropdown.ContactStipendStatus
