USE AJACS
GO

--Begin function asset.GetAssetUnitEquipmentEligibility
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit Equipment eligibility data
-- ===================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibility
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) 
				OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) 
				OR (@cFundingSourceCode = 'USEU' AND C.IsUSEquipmentTransferEligible = 1 AND C.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C ON C.ContactID = AU.CommanderContactID
			AND AU.AssetUnitID = @AssetUnitID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function asset.GetAssetUnitEquipmentEligibility

--Begin function asset.GetAssetUnitEquipmentEligibilityNotes
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotesByAssetUnitID'
EXEC utility.DropObject 'asset.GetAssetUnitEquipmentEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with assetunit equipment eligibility data
-- ===================================================================================

CREATE FUNCTION asset.GetAssetUnitEquipmentEligibilityNotes
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = NULL
	DECLARE @cFundingSourceCode VARCHAR(50)

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not equipment elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C WHERE C.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) 
				OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) 
				OR (@cFundingSourceCode = 'USEU' AND C.IsUSEquipmentTransferEligible = 1 AND C.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C ON C.ContactID = AU.CommanderContactID
			AND AU.AssetUnitID = @AssetUnitID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equipment Eligible'
	ELSE
		SET @cReturn = 'Equipment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function asset.GetAssetUnitEquipmentEligibilityNotes

--Begin function dbo.IsAssetUnitEquipmentEligible
EXEC utility.DropObject 'dbo.IsAssetUnitEquipmentEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.17
-- Description:	A function to determine if an asset unit is equipment eligible
-- ===========================================================================

CREATE FUNCTION dbo.IsAssetUnitEquipmentEligible
(
@AssetUnitID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'USEU' AND C2.IsUSEquipmentTransferEligible = 1 AND C2.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM asset.AssetUnit AU
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Community C1 ON C1.CommunityID = A.CommunityID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND AU.AssetUnitID = @AssetUnitID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF
	
	RETURN @bReturn

END
GO
--End function dbo.IsAssetUnitEquipmentEligible

--Begin function dbo.GetCommunityNameByAssetUnitID
EXEC utility.DropObject 'dbo.GetCommunityNameByAssetUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================================
-- Author:			Jonathan Burnham
-- Create date:	2018.05.12
-- Description:	A function to return a CommunityName for a specific AssetUnitID
-- ============================================================================

CREATE FUNCTION dbo.GetCommunityNameByAssetUnitID
(
@AssetUnitID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCommunityName VARCHAR(250) = ''
	
	SELECT @cCommunityName = C.CommunityName
	FROM dbo.Community C
		JOIN asset.Asset A ON C.CommunityID = A.CommunityID
		JOIN asset.AssetUnit AU ON A.AssetID = AU.AssetID
			AND AU.AssetUnitID = @AssetUnitID

	RETURN ISNULL(@cCommunityName, '')

END
GO
--End function dbo.GetCommunityNameByAssetUnitID

--Begin function dbo.GetContactEquipmentEligibility
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.10.19
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibility
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @nIsCompliant = 
		CASE
			WHEN 
				(@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) 
					OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) 
					OR (@cFundingSourceCode = 'USEU' AND C.IsUSEquipmentTransferEligible = 1 AND C.IsUSEquipmentTransferEligible = 1) 
			THEN 1 
			ELSE 0 
			END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 1
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactEquipmentEligibility

--Begin function dbo.GetContactEquipmentEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactEquipmentEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact equpiment eligibility data
-- =================================================================================

CREATE FUNCTION dbo.GetContactEquipmentEligibilityNotes
(
@ContactID INT,
@EquipmentInventoryID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM procurement.EquipmentInventory EI
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EI.ConceptNoteID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND EI.EquipmentInventoryID = @EquipmentInventoryID

	SELECT @cReturn =
		CASE
			WHEN C.CommunityID > 0
			THEN 
				CASE
					WHEN dbo.IsCommunityStipendEligible(C.CommunityID) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			WHEN C.AssetUnitID > 0
			THEN
				CASE
					WHEN 
						(
						SELECT dbo.IsCommunityStipendEligible(A.CommunityID)
						FROM asset.Asset A
							JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
								AND AU.AssetUnitID = C.AssetUnitID
						) = 1 
					THEN '' 
					ELSE 'Assigned community is not equpiment elligible,' 
				END
			END
		 + CASE 
		 		WHEN 
					(@cFundingSourceCode = 'EU' AND C.IsEUEquipmentTransferEligible = 1) 
						OR (@cFundingSourceCode = 'US' AND C.IsUSEquipmentTransferEligible = 1) 
						OR (@cFundingSourceCode = 'USEU' AND C.IsUSEquipmentTransferEligible = 1 AND C.IsUSEquipmentTransferEligible = 1) 
				THEN '' 
				ELSE 'Assigned asset department head vetting has expired,' 
			END
	FROM dbo.Contact C
	WHERE C.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Equpiment Eligible'
	ELSE
		SET @cReturn = 'Equpiment Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactEquipmentEligibilityNotes

--Begin function dbo.GetContactStipendEligibility
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibility
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'USEU' AND C2.IsUSEquipmentTransferEligible = 1 AND C2.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibility

--Begin function dbo.GetContactStipendEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotes
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'USEU' AND C2.IsUSEquipmentTransferEligible = 1 AND C2.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotes