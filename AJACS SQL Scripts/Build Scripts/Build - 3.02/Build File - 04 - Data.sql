USE AJACS
GO

UPDATE AU
SET AU.HeadOfDepartmentContactID = AU.CommanderContactID
FROM asset.AssetUnit AU
WHERE AU.HeadOfDepartmentContactID = 0
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='ConceptNoteAnalysis', 
	@NewMenuItemLink='/conceptnote/analysis', 
	@NewMenuItemText='Activity Analysis', 
	@AfterMenuItemCode='ObjectiveManage', 
	@PermissionableLineageList='ConceptNote.Analysis'
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='LogicalFramework', 
	@NewMenuItemCode='IndicatorAnalysis', 
	@NewMenuItemLink='/indicator/analysis', 
	@NewMenuItemText='Indicator Analysis', 
	@AfterMenuItemCode='ActivityAnalysisList', 
	@PermissionableLineageList='Indicator.Analysis'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO