USE AJACS
GO

--Begin table asset.AssetUnit
EXEC Utility.AddColumn 'asset.AssetUnit', 'HeadOfDepartmentContactID', 'INT', '0'
GO
--End table asset.AssetUnit

--Begin table dbo.DocumentEntity
EXEC Utility.DropObject 'dbo.DocumentEntity'
GO
--End table dbo.DocumentEntity
