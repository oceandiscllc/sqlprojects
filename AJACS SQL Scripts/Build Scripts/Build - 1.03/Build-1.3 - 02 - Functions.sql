USE AJACS
GO

--Begin function dbo.FormatConceptNoteReferenceCode
EXEC utility.DropObject 'dbo.FormatConceptNoteReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
-- =========================================================================

CREATE FUNCTION dbo.FormatConceptNoteReferenceCode
(
@ConceptNoteID INT
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN 'AJACS-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)

END
GO
--End function dbo.FormatConceptNoteReferenceCode

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(300)
	
	SELECT @cRetVal = 'AJACS-AS-' + RIGHT('0000' + CAST(CN.ConceptNoteID AS VARCHAR(10)), 4) + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.FormatConceptNoteTitle