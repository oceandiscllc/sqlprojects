USE AJACS
GO

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC Utility.AddColumn @TableName, 'ConceptNoteContactEquipmentDistributionDate', 'DATE'
EXEC Utility.AddColumn @TableName, 'Remarks', 'VARCHAR(MAX)'
EXEC Utility.AddColumn @TableName, 'SpentToDate', 'NUMERIC(18,2)'

EXEC utility.SetDefaultConstraint @TableName, 'SpentToDate', 'NUMERIC(18,2)', 0
GO
--End table dbo.ConceptNote

--Begin table dbo.ConceptNoteContactEquipment
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContactEquipment'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ConceptNoteContactEquipment
	(
	ConceptNoteContactEquipmentID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	ContactID INT,
	EquipmentCatalogID INT,
	Quantity INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentCatalogID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ConceptNoteContactEquipmentID'
EXEC utility.SetIndexClustered 'IX_ConceptNoteContactEquipment', @TableName, 'ConceptNoteID,ContactID,EquipmentCatalogID'
GO
--End table dbo.ConceptNoteContactEquipment

--Begin table procurement.PurchaseRequest
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseRequest'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseRequest
	(
	PurchaseRequestID INT IDENTITY(1,1) NOT NULL,
	ConceptNoteID INT,
	RequestPersonID INT,
	PointOfContactPersonID INT,
	CurrencyID INT,
	DeliveryDate DATE,
	ReferenceCode VARCHAR(50),
	Address VARCHAR(200),
	AddressCountryID INT,
	Notes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AddressCountryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CurrencyID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PointOfContactPersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RequestPersonID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'PurchaseRequestID'
GO
--End table procurement.PurchaseRequest

--Begin table procurement.PurchaseRequestConceptNoteBudget
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseRequestConceptNoteBudget'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseRequestConceptNoteBudget
	(
	PurchaseRequestConceptNoteBudgetID INT IDENTITY(1,1) NOT NULL,
	PurchaseRequestID INT,
	ConceptNoteBudgetID INT,
	Quantity INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteBudgetID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PurchaseRequestID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PurchaseRequestConceptNoteBudgetID'
EXEC utility.SetIndexClustered 'IX_PurchaseRequestConceptNoteBudget', @TableName, 'PurchaseRequestID,ConceptNoteBudgetID'
GO
--End table procurement.PurchaseRequestConceptNoteBudget

--Begin table procurement.PurchaseRequestConceptNoteEquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseRequestConceptNoteEquipmentCatalog'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseRequestConceptNoteEquipmentCatalog
	(
	PurchaseRequestConceptNoteEquipmentCatalogID INT IDENTITY(1,1) NOT NULL,
	PurchaseRequestID INT,
	ConceptNoteEquipmentCatalogID INT,
	Quantity INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteEquipmentCatalogID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PurchaseRequestID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PurchaseRequestConceptNoteEquipmentCatalogID'
EXEC utility.SetIndexClustered 'IX_PurchaseRequestConceptNoteEquipmentCatalog', @TableName, 'PurchaseRequestID,ConceptNoteEquipmentCatalogID'
GO
--End table procurement.PurchaseRequestConceptNoteEquipmentCatalog

--Begin table procurement.PurchaseRequestSubContractor
DECLARE @TableName VARCHAR(250) = 'procurement.PurchaseRequestSubContractor'

EXEC utility.DropObject 'dbo.PurchaseRequestSubContractor'
EXEC utility.DropObject @TableName

CREATE TABLE procurement.PurchaseRequestSubContractor
	(
	PurchaseRequestSubContractorID INT IDENTITY(1,1) NOT NULL,
	PurchaseRequestID INT,
	SubContractorID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PurchaseRequestID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SubContractorID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PurchaseRequestSubContractorID'
EXEC utility.SetIndexClustered 'IX_PurchaseRequestSubContractor', @TableName, 'PurchaseRequestID,SubContractorID'
GO
--End table procurement.PurchaseRequestSubContractor