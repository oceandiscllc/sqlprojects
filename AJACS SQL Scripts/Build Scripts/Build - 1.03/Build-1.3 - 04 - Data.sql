USE AJACS
GO

--Begin table dbo.MenuItem
UPDATE MI
SET 
	MI.MenuItemCode = 'PurchaseRequestList',
	MI.MenuItemLink = '/purchaserequest/list',
	MI.MenuItemText = 'Purchase Requests'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('PurchaseOrderAdd','PurchaseOrderList','PurchaseRequestAdd','PurchaseRequestList')
GO

DELETE MI
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'EquipmentDistributionList'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ManageWeeklyReports', @NewMenuItemText='Create Weekly Reports', @AfterMenuItemCode='RAPData', @Icon='fa fa-fw fa-calendar'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @NewMenuItemText='Atmospheric Report', @ParentMenuItemCode='ManageWeeklyReports', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ProgramReport', @NewMenuItemLink='/programreport/addupdate', @NewMenuItemText='Program Report', @ParentMenuItemCode='ManageWeeklyReports', @AfterMenuItemCode='WeeklyReport', @PermissionableLineageList='ProgramReport.AddUpdate', @IsActive=0
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.MenuItemCode = 'ManageWeeklyReports'
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID,PermissionableLineage)
SELECT
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'ManageWeeklyReports'),
	MIPL.PermissionableLineage
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND MI.ParentMenuItemID = (SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'ManageWeeklyReports')
GO
		
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactEquipmentList', @NewMenuItemText='Equipment Distribution', @ParentMenuItemCode='Procurement', @NewMenuItemLink='/conceptnotecontactequipment/list', @AfterMenuItemCode='PurchaseRequestList', @PermissionableLineageList='ConceptNoteContactEquipment.List'
GO
--End table dbo.MenuItem

--Begin table permissionable.Permissionable
UPDATE P
SET 
	P.PermissionableCode = 'PurchaseRequest',
	P.PermissionableName = 'Purchase Requests'
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'PurchaseOrder'
GO

UPDATE P
SET P.PermissionableLineage = REPLACE(P.PermissionableLineage, 'PurchaseOrder', 'PurchaseRequest')
FROM permissionable.Permissionable P
GO

UPDATE PP
SET PP.PermissionableLineage = REPLACE(PP.PermissionableLineage, 'PurchaseOrder', 'PurchaseRequest')
FROM permissionable.PersonPermissionable PP
GO

UPDATE MIPL
SET MIPL.PermissionableLineage = REPLACE(MIPL.PermissionableLineage, 'PurchaseOrder', 'PurchaseRequest')
FROM dbo.MenuItemPermissionableLineage MIPL
GO

UPDATE P
SET P.PermissionableName = 'Weekly Atmospheric Report'
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'WeeklyReport'
GO

UPDATE P
SET P.PermissionableName = 'Weekly Atmospheric Report'
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'WeeklyReport'
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableCode LIKE 'EquipmentDistribution%'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ConceptNoteContactEquipment')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(PermissionableCode, PermissionableName, DisplayOrder)
	VALUES
		('ConceptNoteContactEquipment','Equipment Distribution', 0)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'AddUpdate',
		'Add / Edit',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'ConceptNoteContactEquipment'

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'View',
		'View',
		2
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'ConceptNoteContactEquipment'

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'List',
		'List',
		3
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'ConceptNoteContactEquipment'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ProgramReport')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(PermissionableCode, PermissionableName, DisplayOrder)
	VALUES
		('ProgramReport','Weekly Program Report', 0)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'AddUpdate',
		'Add / Edit',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'ProgramReport'

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'View',
		'View',
		2
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'ProgramReport'

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'List',
		'List',
		3
	FROM permissionable.Permissionable P
	WHERE P.PermissionableCode = 'ProgramReport'

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
DELETE DGP
FROM permissionable.DisplayGroupPermissionable DGP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = DGP.PermissionableID
	)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Procurement'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('ConceptNoteContactEquipment')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'ObservationReport'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('ProgramReport')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin Permissions Assignment
IF (SELECT dbo.GetServerSetupValueByServerSetupKey('Environment', 'Prod')) = 'Dev'
	BEGIN
	
	DELETE FROM permissionable.PersonPermissionable WHERE PersonID IN (69,17,26)

	INSERT INTO permissionable.PersonPermissionable
		(PersonID,PermissionableLineage)
	SELECT
		P1.PersonID,
		P2.PermissionableLineage
	FROM dbo.Person P1, permissionable.Permissionable P2
	WHERE P1.PersonID IN (69,17,26)

	END
--ENDIF
GO
--End Permissions Assignment