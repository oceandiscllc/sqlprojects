USE AJACS
GO

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
-- ======================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.Title,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ContactLocation,

		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CourseID,
		C.CourseName
	FROM dbo.ConceptNoteCourse CNC
		JOIN dbo.Course C ON C.CourseID = CNC.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY C.CourseName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Community COM on COM.CommunityID = C.CommunityID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT.ConceptNoteTaskID,
		CNT.ConceptNoteTaskName,
		CNT.ConceptNoteTaskDescription,
		CNT.ParentConceptNoteTaskID,
		CNT.StartDate,
		CNT.EndDate,
		CNT.IsComplete,
		CNT1.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT.ParentConceptNoteTaskID
				) CNT1

	WHERE CNT.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT.ConceptNoteTaskName, CNT.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
-- ==================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.PassportNumber,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID
		
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A stored procedure to get data from the dbo.ConceptNoteContactEquipment table
-- ==========================================================================================
CREATE PROCEDURE dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.ItemName,
		D.Quantity,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(D.ConceptNoteID) AS Title
	FROM
		(
		SELECT
			SUM(CNCE.Quantity) AS Quantity,
			CNCE.EquipmentCatalogID,
			CN.ConceptNoteID
		FROM dbo.ConceptNoteContactEquipment CNCE
			JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
			JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNCE.ConceptNoteID
				AND CN.ConceptNoteContactEquipmentDistributionDate IS NOT NULL
				AND 
					(
						(@EntityTypeCode = 'Community' AND C.CommunityID = @EntityID)
							OR (@EntityTypeCode = 'Province' AND C.ProvinceID = @EntityID)
					)
		GROUP BY CN.ConceptNoteID, CNCE.EquipmentCatalogID
		) D
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = D.EquipmentCatalogID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = D.ConceptNoteID
	ORDER BY 4, 1, 3
	
END
GO
--End procedure dbo.GetConceptNoteContactEquipmentByEntityTypeCodeAndEntityID

--Begin procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID
EXEC Utility.DropObject 'dbo.GetDocumentsByEntityTypeCodeAndEntityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A stored procedure to get data from the dbo.Document table
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentsByEntityTypeCodeAndEntityID

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileExtension,
		D.PhysicalFileName,
		D.PhysicalFilePath,
		D.ContentSubtype,
		D.ContentType,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = @EntityTypeCode
			AND DE.EntityID = @EntityID
	ORDER BY D.DocumentTitle, D.DocumentName, D.DocumentID

END
GO
--End procedure dbo.GetDocumentsByEntityTypeCodeAndEntityID

--Begin procedure dbo.GetEmailAddressesByPermissionableLineage
EXEC Utility.DropObject 'dbo.GetEmailAddressesByPermissionableLineage'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to get data from the dbo.Person table
--
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	Added distinct and list support
-- =====================================================================
CREATE PROCEDURE dbo.GetEmailAddressesByPermissionableLineage

@PermissionableLineage VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		P.EmailAddress,
		P.PersonID
	FROM permissionable.PersonPermissionable PP
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ListToTable(@PermissionableLineage, ',') LTT
				WHERE LTT.ListItem = PP.PermissionableLineage
				)
			AND P.PersonID <> @PersonID
	ORDER BY P.EmailAddress
		
END
GO
--End procedure dbo.GetEmailAddressesByPermissionableLineage

--Begin procedure dbo.GetEmailAddressesByRoleName
EXEC Utility.DropObject 'dbo.GetEmailAddressesByRoleName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A stored procedure to get data from the dbo.Person table
-- =====================================================================
CREATE PROCEDURE dbo.GetEmailAddressesByRoleName

@RoleName VARCHAR(50),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.EmailAddress,
		P.PersonID
	FROM dbo.Person P 
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND R.RoleName = @RoleName
			AND P.PersonID <> @PersonID
	ORDER BY P.EmailAddress
		
END
GO
--End procedure dbo.GetEmailAddressesByRoleName

--Begin procedure dbo.GetSubContractorBySubContractorID
EXEC Utility.DropObject 'dbo.GetSubContractorBySubContractorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.16
-- Description:	A stored procedure to data from the dbo.SubContractor table
-- ========================================================================
CREATE PROCEDURE dbo.GetSubContractorBySubContractorID

@SubContractorID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.CountryID AS AddressCountryID,
		C1.CountryName AS AddressCountryName,
		C2.CountryID AS RegistrationCountryID,
		C2.CountryName AS RegistrationCountryName,
		SC.Address,
		SC.PrimaryContactEmailAddress,
		SC.PrimaryContactName,
		SC.PrimaryContactPhone,
		SC.RegistrationNumber,
		SC.SubContractorID,
		SC.SubContractorName,
		SC.TaxNumber,
		SCBT.SubContractorBusinessTypeID,
		SCBT.SubContractorBusinessTypeName,
		SCRT.SubContractorRelationshipTypeID,
		SCRT.SubContractorRelationshipTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('SubContractor') AS EntityTypeName
	FROM dbo.SubContractor SC
		JOIN dropdown.Country C1 ON C1.CountryID = SC.AddressCountryID
		JOIN dropdown.Country C2 ON C2.CountryID = SC.RegistrationCountryID
		JOIN dropdown.SubContractorBusinessType SCBT ON SCBT.SubContractorBusinessTypeID = SC.SubContractorBusinessTypeID
		JOIN dropdown.SubContractorRelationshipType SCRT ON SCRT.SubContractorRelationshipTypeID = SC.SubContractorRelationshipTypeID
			AND SC.SubContractorID = @SubContractorID

END
GO
--End procedure dbo.GetSubContractorBySubContractorID

--Begin procedure procurement.GetConceptNoteContactEquipmentByConceptNoteID
EXEC Utility.DropObject 'procurement.GetConceptNoteContactEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to get data from the procurement.ConceptNoteContactEquipment table
-- ==================================================================================================
CREATE PROCEDURE procurement.GetConceptNoteContactEquipmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatPersonName(C.FirstName, C.LastName, NULL, 'LastFirst') AS FullName,
		CNCE.Quantity,
		EC.ItemName
	FROM dbo.ConceptNoteContactEquipment CNCE
		JOIN dbo.Contact C ON C.ContactID = CNCE.ContactID
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNCE.EquipmentCatalogID
			AND CNCE.ConceptNoteID = @ConceptNoteID
	ORDER BY FullName, ItemName
	
END
GO
--End procedure procurement.GetConceptNoteContactEquipmentByConceptNoteID

--Begin procedure utility.MenuItemAddUpdate
EXEC Utility.DropObject 'utility.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.03
-- Description:	A stored procedure to add / update a menu item
--
-- Author:			Todd Pires
-- Create Date: 2015.03.22
-- Description:	Added support fot the dbo.MenuItemPermissionableLineage table
-- ==========================================================================
CREATE PROCEDURE utility.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM dbo.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM dbo.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM dbo.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM dbo.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM dbo.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM dbo.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO dbo.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE dbo.MenuItem SET IsActive = @IsActive WHERE MenuItemID = @nNewMenuItemID
			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM dbo.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF
			IF @NewMenuItemLink IS NOT NULL
				UPDATE dbo.MenuItem SET MenuItemLink = @NewMenuItemLink WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			IF @NewMenuItemText IS NOT NULL
				UPDATE dbo.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE dbo.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM dbo.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO dbo.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM dbo.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM dbo.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM dbo.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure utility.MenuItemAddUpdate

--Begin procedure workflow.GetConceptNoteWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetConceptNoteWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A stored procedure to people associated with the workflow steps on a concept note
-- ==============================================================================================
CREATE PROCEDURE workflow.GetConceptNoteWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'ConceptNote.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetConceptNoteWorkflowStepPeople

--Begin procedure workflow.GetSpotReportWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetSpotReportWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A stored procedure to people associated with the workflow steps on a spot report
-- =============================================================================================
CREATE PROCEDURE workflow.GetSpotReportWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'SpotReport.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetSpotReportWorkflowStepPeople

--Begin procedure workflow.GetWeeklyReportWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetWeeklyReportWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A stored procedure to people associated with the workflow steps on a Weekly report
-- =============================================================================================
CREATE PROCEDURE workflow.GetWeeklyReportWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'WeeklyReport'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT WR.WorkflowStepNumber FROM weeklyreport.WeeklyReport WR WHERE WR.WeeklyReportID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'WeeklyReport.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetWeeklyReportWorkflowStepPeople

--Begin procedure workflow.GetWorkflowByEntityTypeCode
EXEC Utility.DropObject 'workflow.GetWorkflowByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.27
-- Description:	A stored procedure to get the workflow for a specific entitytypecode
-- =================================================================================
CREATE PROCEDURE workflow.GetWorkflowByEntityTypeCode

@EntityTypeCode VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		W.WorkflowID,
		W.EntityTypeCode,
		W.WorkflowStepCount
	FROM workflow.Workflow W
	WHERE W.EntityTypeCode = @EntityTypeCode

END
GO
--End procedure workflow.GetWorkflowByEntityTypeCode

