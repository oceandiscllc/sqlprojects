USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'JusticeUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	SELECT
		'JusticeUpdate',
		ET.WorkflowActionCode,
		
		CASE
			WHEN ET.WorkflowActionCode = 'DecrementWorkflow'
			THEN '<p>A previously submitted Justice update has been disapproved:</p><p>&nbsp;</p><p><strong>Justice Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Justice update workflow. Please click the link above to review the updated Justice.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'IncrementWorkflow'
			THEN '<p>An AJACS Justice update has been submitted for your review:</p><p>&nbsp;</p><p><strong>Justice Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Justice update workflow. Please click the link above to review the updated Justice.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'Release'
			THEN '<p>An AJACS Justice update has been released:</p><p>&nbsp;</p><p><strong>Justice Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Justice update workflow. Please click the link above to review the updated Justice.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
		END
	
	FROM dbo.EmailTemplate ET
	WHERE ET.EntityTypeCode = 'RiskUpdate'

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'JusticeUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('JusticeUpdate', '[[Link]]', 'Link', 1),
		('JusticeUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'EquipmentDistribution', 'Equipment Distribution'
GO
EXEC utility.EntityTypeAddUpdate 'JusticeCommunity', 'Justice Community'
GO
EXEC utility.EntityTypeAddUpdate 'JusticeProvince', 'Justice Province'
GO
EXEC utility.EntityTypeAddUpdate 'JusticeUpdate', 'Justice Update'
GO
EXEC utility.EntityTypeAddUpdate 'Workflow', 'Workflow'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='JusticeUpdate', @NewMenuItemLink='/justice/addupdate', @NewMenuItemText='Justice', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='PoliceEngagementUpdate', @PermissionableLineageList='JusticeUpdate.WorkflowStepID%'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentDistributionList', @NewMenuItemLink='/equipmentdistribution/listdistribution', @NewMenuItemText='Equipment Distributions', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentInventoryList', @PermissionableLineageList='EquipmentDistribution.List'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='PermissionableList', @AfterMenuItemCode='PersonList', @NewMenuItemText='Permissionables', @NewMenuItemLink='/permissionable/list', @PermissionableLineageList='Permissionable.List'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PermissionableTemplateList', @AfterMenuItemCode='PermissionableList'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='WorkflowList', @AfterMenuItemCode='ServerSetupList', @NewMenuItemText='Workflows', @NewMenuItemLink='/workflow/list', @PermissionableLineageList='Workflow.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'JusticeUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('JusticeUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'JusticeUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'RiskUpdate'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'JusticeUpdate'),
		WSWA.WorkflowStepNumber,
		WSWA.WorkflowActionID,
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'RiskUpdate'
	
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('JusticeUpdate','Justice', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('JusticeUpdate')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'JusticeUpdate')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'JusticeUpdate','Justice')
		
	END
--ENDIF
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'JusticeUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'JusticeUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'JusticeUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO

DELETE P FROM permissionable.Permissionable P WHERE P.PermissionableCode IN ('CSWGMemberView','CSWGMemberUpdateVetting')
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CSWGMember')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'CSWGMember',
		'View CSWG Member',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.View'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CSWGMemberUpdateVetting')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'CSWGMemberUpdateVetting',
		'CSWG Member Update Vetting',
		2
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.List.CSWGMemberUpdateVetting'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Contact.AddUpdate.CSWGMember')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID,PermissionableCode,PermissionableName,DisplayOrder)
	SELECT
		P.PermissionableID,
		'CSWGMember',
		'Add / Edit CSWG Member',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.AddUpdate'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'FIFUpdate.View')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID,PermissionableCode,PermissionableName,DisplayOrder)
	SELECT
		P.PermissionableID,
		'View',
		'View',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'FIFUpdate'
	
	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('JusticeUpdate','Justice Update', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('JusticeUpdate')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

EXEC utility.ServerSetupKeyAddUpdate 'ShowForceOnCSSFPortal', '1'

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveD',
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveD',
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
