USE AJACS
GO

--Begin table dbo.Community
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Community') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'dbo.Community.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table dbo.Community

--Begin table dbo.CommunityClass
EXEC utility.DropColumn 'dbo.CommunityClass', 'JusticeNotes'
GO
--End table dbo.CommunityClass

--Begin table dbo.CommunityTraining
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityTraining'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityTraining
	(
	CommunityTrainingID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityTraining', 'CommunityID,TrainingTitle'
GO
--End table dbo.CommunityTraining

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'EmployerTypeID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'EmployerTypeID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.EntityType
DECLARE @TableName VARCHAR(250) = 'dbo.EntityType'

EXEC utility.AddColumn @TableName, 'CanRejectAfterFinalApproval', 'BIT'
EXEC utility.AddColumn @TableName, 'HasWorkflow', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'CanRejectAfterFinalApproval', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', 0
GO
--End table dbo.EntityType

--Begin table dbo.Province
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Province') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'dbo.Province.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table dbo.Province

--Begin table dbo.ProvinceClass
EXEC utility.DropColumn 'dbo.ProvinceClass', 'JusticeNotes'
GO
--End table dbo.ProvinceClass

--Begin table dbo.ProvinceTraining
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceTraining'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceTraining
	(
	ProvinceTrainingID INT IDENTITY(1,1) NOT NULL,
	ProvinceID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProvinceTraining', 'ProvinceID,TrainingTitle'
GO
--End table dbo.ProvinceTraining

--Begin table dropdown.CommunityDocumentCenterStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityDocumentCenterStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityDocumentCenterStatus
	(
	CommunityDocumentCenterStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityDocumentCenterStatusCode VARCHAR(50),
	CommunityDocumentCenterStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityDocumentCenterStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityDocumentCenterStatus', @TableName, 'DisplayOrder,CommunityDocumentCenterStatusName', 'CommunityDocumentCenterStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityDocumentCenterStatus ON
GO

INSERT INTO dropdown.CommunityDocumentCenterStatus (CommunityDocumentCenterStatusID, CommunityDocumentCenterStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityDocumentCenterStatus OFF
GO

INSERT INTO dropdown.CommunityDocumentCenterStatus 
	(CommunityDocumentCenterStatusCode,CommunityDocumentCenterStatusName,DisplayOrder)
VALUES
	('NotCurrentlyPlanned','Not Currently Planned', 1),
	('Intended','Intended', 2),
	('Planning','Planning', 3),
	('Implementation','Implementation', 4),
	('CenterOpened','Center Opened', 5),
	('CenterClosed','Center Closed', 6)
GO	
--End table dropdown.CommunityDocumentCenterStatus

--Begin table dropdown.EmployerType
DECLARE @TableName VARCHAR(250) = 'dropdown.EmployerType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EmployerType
	(
	EmployerTypeID INT IDENTITY(0,1) NOT NULL,
	EmployerTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EmployerTypeID'
EXEC utility.SetIndexNonClustered 'IX_EmployerType', @TableName, 'DisplayOrder,EmployerTypeName', 'EmployerTypeID'
GO

SET IDENTITY_INSERT dropdown.EmployerType ON
GO

INSERT INTO dropdown.EmployerType (EmployerTypeID, EmployerTypeName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.EmployerType OFF
GO

INSERT INTO dropdown.EmployerType 
	(EmployerTypeName,DisplayOrder)
VALUES
	('Partner', 1),
	('Civil Society', 2),
	('Governmental', 3),
	('Private', 4),
	('Other', 5)
GO	
--End table dropdown.EmployerType

--Begin table justiceupdate.Community
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('justiceupdate.Community') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'justiceupdate.Community.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table justiceupdate.Community

--Begin table justiceupdate.CommunityClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityClass'

EXEC utility.DropObject @TableName
GO
--End table justiceupdate.CommunityClass

--Begin table justiceupdate.CommunityTraining
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityTraining'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityTraining
	(
	CommunityTrainingID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityTraining', 'CommunityID,TrainingTitle'
GO
--End table justiceupdate.CommunityTraining

--Begin table justiceupdate.Province
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('justiceupdate.Province') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'justiceupdate.Province.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table justiceupdate.Province

--Begin table justiceupdate.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceClass'

EXEC utility.DropObject @TableName
GO
--End table justiceupdate.ProvinceClass

--Begin table justiceupdate.ProvinceTraining
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceTraining'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceTraining
	(
	ProvinceTrainingID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProvinceTraining', 'ProvinceID,TrainingTitle'
GO
--End table justiceupdate.ProvinceTraining

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.AddColumn @TableName, 'ControllerName', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'MethodName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'PermissionCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PermissionableGroupID', 'INT' 
EXEC utility.AddColumn @TableName, 'Description', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableGroupID', 'INT', 0
GO

EXEC Utility.DropObject 'permissionable.TR_Permissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A trigger to update the permissionable.Permissionable table
-- ========================================================================
CREATE TRIGGER permissionable.TR_Permissionable ON permissionable.Permissionable FOR INSERT, UPDATE
AS
SET ARITHABORT ON

DECLARE @cMethodName VARCHAR(250)
DECLARE @nPermissionableID INT

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT I.PermissionableID, I.MethodName
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPermissionableID, @cMethodName
	WHILE @@fetch_status = 0
		BEGIN

		IF @cMethodName IS NULL
			BEGIN

			;
			WITH HD (PermissionableID,ParentPermissionableID,NodeLevel)
				AS 
				(
				SELECT
					P.PermissionableID, 
					P.ParentPermissionableID, 
					1 
				FROM permissionable.Permissionable P
				WHERE P.PermissionableID = @nPermissionableID
		
				UNION ALL
			
				SELECT
					P.PermissionableID, 
					P.ParentPermissionableID, 
					HD.NodeLevel + 1 AS NodeLevel
				FROM permissionable.Permissionable P
					JOIN HD ON HD.ParentPermissionableID = P.PermissionableID 
				)
	
			UPDATE P
			SET P.PermissionableLineage = STUFF(CAST((SELECT '.' + P.PermissionableCode FROM HD JOIN permissionable.Permissionable P ON P.PermissionableID = HD.PermissionableID ORDER BY HD.NodeLevel DESC FOR XML PATH('')) AS VARCHAR(MAX)), 1, 1, '')
			FROM permissionable.Permissionable P
			WHERE P.PermissionableID = @nPermissionableID

			END
		ELSE
			BEGIN

			UPDATE P
			SET P.PermissionableLineage = 			
				P.ControllerName 
					+ '.' 
					+ P.MethodName
					+ CASE
							WHEN P.PermissionCode IS NOT NULL
							THEN '.' + P.PermissionCode
							ELSE ''
						END
	
			FROM permissionable.Permissionable P
			WHERE P.PermissionableID = @nPermissionableID
			
			END
		--ENDIF
		
		FETCH oCursor INTO @nPermissionableID, @cMethodName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'EquipmentDistribution.List')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ControllerName, MethodName)
	VALUES
		('EquipmentDistribution', 'List'),
		('Permissionable', 'List'),
		('Workflow', 'List')

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableGroup
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableGroup'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableGroup
	(
	PermissionableGroupID INT IDENTITY(1,1) NOT NULL,
	PermissionableGroupCode VARCHAR(50),
	PermissionableGroupName VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableGroup', 'DisplayOrder,PermissionableGroupName,PermissionableGroupID'
GO
--End table permissionable.PermissionableGroup

--Begin table procurement.EquipmentDistribution
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistribution'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentDistribution
	(
	EquipmentDistributionID INT IDENTITY(1,1) NOT NULL,
	EquipmentDistributionName VARCHAR(250),
	RecipientContactID INT,
	DeliveredToRecipientDate DATE,
	)

EXEC utility.SetDefaultConstraint @TableName, 'RecipientContactID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentDistributionID'
GO
--End table procurement.EquipmentDistribution

--Begin table procurement.EquipmentDistributionEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistributionEquipmentInventory'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentDistributionEquipmentInventory
	(
	EquipmentDistributionEquipmentInventoryID INT IDENTITY(1,1) NOT NULL,
	EquipmentDistributionID INT,
	EquipmentInventoryID INT,
	Quantity INT,
	DeliveredToEndUserDate DATE,
	EndUserEntityTypeCode VARCHAR(50),
	EndUserEntityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EndUserEntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentDistributionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentDistributionEquipmentInventoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentDistributionEquipmentInventory', 'EquipmentDistributionID,EquipmentInventoryID'
GO
--End table procurement.EquipmentDistributionInventory

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'EquipmentOrderDate', 'DATE'
EXEC utility.AddColumn @TableName, 'EquipmentDeliveredToImplementerDate', 'DATE'
EXEC utility.AddColumn @TableName, 'InServiceDate', 'DATE'
EXEC utility.AddColumn @TableName, 'LicenseKey', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'EquipmentUsageMinutes', 'INT'
EXEC utility.AddColumn @TableName, 'EquipmentUsageGB', 'INT'

EXEC utility.SetDefaultConstraint 'procurement.EquipmentInventory', 'EquipmentUsageMinutes', 'INT', 0
EXEC utility.SetDefaultConstraint 'procurement.EquipmentInventory', 'EquipmentUsageGB', 'INT', 0
GO
--End table procurement.EquipmentInventory

--Begin table procurement.LicenseEquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.LicenseEquipmentCatalog'

EXEC utility.AddColumn @TableName, 'ItemPageNumber', 'VARCHAR(25)'
EXEC utility.AddColumn @TableName, 'ItemLineNumber', 'VARCHAR(25)'
GO
--End table procurement.LicenseEquipmentCatalog

--Begin table workflow.EntityWorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowStepGroupPerson'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.EntityWorkflowStepGroupPerson
	(
	EntityWorkflowStepGroupPersonID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	WorkflowID INT,
	WorkflowStepID INT,
	WorkflowStepGroupID INT,
	WorkflowName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepName VARCHAR(250),
	WorkflowStepGroupName VARCHAR(250),
	PersonID INT,
	IsComplete BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'EntityWorkflowStepGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_EntityWorkflowStepGroupPerson', 'EntityTypeCode,EntityID'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC Utility.AddColumn @TableName, 'WorkflowName', 'VARCHAR(250)'
EXEC Utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStepGroup
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroup'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStepGroup
	(
	WorkflowStepGroupID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepID INT,
	WorkflowStepGroupName VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'WorkflowStepGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorkflowStepGroup', 'WorkflowStepID,WorkflowStepGroupName'
GO
--End table workflow.WorkflowStepGroup

--Begin table workflow.WorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroupPerson'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStepGroupPerson
	(
	WorkflowStepGroupPersonID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepGroupID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'WorkflowStepGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorkflowStepGroupPerson', 'WorkflowStepGroupID,PersonID'
GO
--End table workflow.WorkflowStepGroupPerson

