-- File Name:	Build-1.48 File 01 - AJACS.sql
-- Build Key:	Build-1.48 File 01 - AJACS - 2016.02.26 20.04.45

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		eventlog.GetCommunityTrainingXMLByJusticeUpdateID
--		eventlog.GetProvinceTrainingXMLByJusticeUpdateID
--		workflow.CanHaveAddUpdate
--		workflow.CanHaveListView
--		workflow.GetEntityWorkflowData
--		workflow.GetEntityWorkflowPeople
--		workflow.GetWorkflowStepCount
--		workflow.GetWorkflowStepNumber
--
-- Procedures:
--		[dbo].[GetContactByContactID]
--		dropdown.GetEmployerTypeData
--		eventlog.LogEquipmentDistributionAction
--		eventlog.LogJusticeAction
--		eventlog.LogWorkflowAction
--		fifupdate.AddFIFCommunity
--		fifupdate.AddFIFProvince
--		justiceupdate.AddJusticeCommunity
--		justiceupdate.AddJusticeProvince
--		justiceupdate.ApproveJusticeUpdate
--		justiceupdate.DeleteJusticeCommunity
--		justiceupdate.DeleteJusticeProvince
--		justiceupdate.GetCommunityByCommunityID
--		justiceupdate.GetProvinceByProvinceID
--		procurement.GetEquipmentInventoryByEquipmentInventoryID
--		procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID
--		reporting.GetSpotReportBySpotReportID
--		utility.SetIndexClustered
--		utility.SetIndexNonClustered
--		workflow.DecrementWorkflow
--		workflow.GetWorkflowByWorkflowID
--		workflow.IncrementWorkflow
--		workflow.InitializeEntityWorkflow
--
-- Tables:
--		dbo.CommunityTraining
--		dbo.ProvinceTraining
--		dropdown.CommunityDocumentCenterStatus
--		dropdown.EmployerType
--		justiceupdate.CommunityTraining
--		justiceupdate.ProvinceTraining
--		permissionable.PermissionableGroup
--		procurement.EquipmentDistribution
--		procurement.EquipmentDistributionEquipmentInventory
--		workflow.EntityWorkflowStepGroupPerson
--		workflow.WorkflowStepGroup
--		workflow.WorkflowStepGroupPerson
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
--
-- Author:			Todd Pires
-- Modify date: 2016.02.24
-- Description:	Added ICS compatibility
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTemp VARCHAR(250)

	IF LEFT(@TableName, 3) = 'IX_'
		BEGIN
		
		SET @cTemp = @TableName
		SET @TableName = @IndexName
		SET @IndexName = @cTemp
		
		END
	--ENDIF
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
--
-- Author:			Todd Pires
-- Modify date: 2013.10.10
-- Description:	Added INCLUDE support
--
-- Author:			Todd Pires
-- Modify date: 2016.02.24
-- Description:	Added ICS compatibility
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX),
	@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTemp VARCHAR(250)

	IF LEFT(@TableName, 3) = 'IX_'
		BEGIN
		
		SET @cTemp = @TableName
		SET @TableName = @IndexName
		SET @IndexName = @cTemp
		
		END
	--ENDIF
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered
--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Community
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Community') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'dbo.Community.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table dbo.Community

--Begin table dbo.CommunityClass
EXEC utility.DropColumn 'dbo.CommunityClass', 'JusticeNotes'
GO
--End table dbo.CommunityClass

--Begin table dbo.CommunityTraining
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityTraining'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityTraining
	(
	CommunityTrainingID INT IDENTITY(1,1) NOT NULL,
	CommunityID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityTraining', 'CommunityID,TrainingTitle'
GO
--End table dbo.CommunityTraining

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'EmployerTypeID', 'INT'
EXEC utility.SetDefaultConstraint @TableName, 'EmployerTypeID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.EntityType
DECLARE @TableName VARCHAR(250) = 'dbo.EntityType'

EXEC utility.AddColumn @TableName, 'CanRejectAfterFinalApproval', 'BIT'
EXEC utility.AddColumn @TableName, 'HasWorkflow', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'CanRejectAfterFinalApproval', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'HasWorkflow', 'BIT', 0
GO
--End table dbo.EntityType

--Begin table dbo.Province
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Province') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'dbo.Province.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table dbo.Province

--Begin table dbo.ProvinceClass
EXEC utility.DropColumn 'dbo.ProvinceClass', 'JusticeNotes'
GO
--End table dbo.ProvinceClass

--Begin table dbo.ProvinceTraining
DECLARE @TableName VARCHAR(250) = 'dbo.ProvinceTraining'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ProvinceTraining
	(
	ProvinceTrainingID INT IDENTITY(1,1) NOT NULL,
	ProvinceID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT,
	RegularAttendeeCount INT,
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RegularAttendeeCount', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProvinceTraining', 'ProvinceID,TrainingTitle'
GO
--End table dbo.ProvinceTraining

--Begin table dropdown.CommunityDocumentCenterStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.CommunityDocumentCenterStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.CommunityDocumentCenterStatus
	(
	CommunityDocumentCenterStatusID INT IDENTITY(0,1) NOT NULL,
	CommunityDocumentCenterStatusCode VARCHAR(50),
	CommunityDocumentCenterStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'CommunityDocumentCenterStatusID'
EXEC utility.SetIndexNonClustered 'IX_CommunityDocumentCenterStatus', @TableName, 'DisplayOrder,CommunityDocumentCenterStatusName', 'CommunityDocumentCenterStatusID'
GO

SET IDENTITY_INSERT dropdown.CommunityDocumentCenterStatus ON
GO

INSERT INTO dropdown.CommunityDocumentCenterStatus (CommunityDocumentCenterStatusID, CommunityDocumentCenterStatusName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.CommunityDocumentCenterStatus OFF
GO

INSERT INTO dropdown.CommunityDocumentCenterStatus 
	(CommunityDocumentCenterStatusCode,CommunityDocumentCenterStatusName,DisplayOrder)
VALUES
	('NotCurrentlyPlanned','Not Currently Planned', 1),
	('Intended','Intended', 2),
	('Planning','Planning', 3),
	('Implementation','Implementation', 4),
	('CenterOpened','Center Opened', 5),
	('CenterClosed','Center Closed', 6)
GO	
--End table dropdown.CommunityDocumentCenterStatus

--Begin table dropdown.EmployerType
DECLARE @TableName VARCHAR(250) = 'dropdown.EmployerType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.EmployerType
	(
	EmployerTypeID INT IDENTITY(0,1) NOT NULL,
	EmployerTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'EmployerTypeID'
EXEC utility.SetIndexNonClustered 'IX_EmployerType', @TableName, 'DisplayOrder,EmployerTypeName', 'EmployerTypeID'
GO

SET IDENTITY_INSERT dropdown.EmployerType ON
GO

INSERT INTO dropdown.EmployerType (EmployerTypeID, EmployerTypeName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.EmployerType OFF
GO

INSERT INTO dropdown.EmployerType 
	(EmployerTypeName,DisplayOrder)
VALUES
	('Partner', 1),
	('Civil Society', 2),
	('Governmental', 3),
	('Private', 4),
	('Other', 5)
GO	
--End table dropdown.EmployerType

--Begin table justiceupdate.Community
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('justiceupdate.Community') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'justiceupdate.Community.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table justiceupdate.Community

--Begin table justiceupdate.CommunityClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityClass'

EXEC utility.DropObject @TableName
GO
--End table justiceupdate.CommunityClass

--Begin table justiceupdate.CommunityTraining
DECLARE @TableName VARCHAR(250) = 'justiceupdate.CommunityTraining'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.CommunityTraining
	(
	CommunityTrainingID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	CommunityID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityTraining', 'CommunityID,TrainingTitle'
GO
--End table justiceupdate.CommunityTraining

--Begin table justiceupdate.Province
IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('justiceupdate.Province') AND SC.Name = 'JusticeClassNotes')
	EXEC sp_RENAME 'justiceupdate.Province.JusticeClassNotes', 'JusticeTrainingNotes', 'COLUMN';
--ENDIF
GO
--End table justiceupdate.Province

--Begin table justiceupdate.ProvinceClass
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceClass'

EXEC utility.DropObject @TableName
GO
--End table justiceupdate.ProvinceClass

--Begin table justiceupdate.ProvinceTraining
DECLARE @TableName VARCHAR(250) = 'justiceupdate.ProvinceTraining'

EXEC utility.DropObject @TableName

CREATE TABLE justiceupdate.ProvinceTraining
	(
	ProvinceTrainingID INT IDENTITY(1,1) NOT NULL,
	JusticeUpdateID INT,
	ProvinceID INT,
	TrainingDate DATE,
	TrainingTitle VARCHAR(50),
	AttendeeCount INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'AttendeeCount', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'JusticeUpdateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceTrainingID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProvinceTraining', 'ProvinceID,TrainingTitle'
GO
--End table justiceupdate.ProvinceTraining

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.AddColumn @TableName, 'ControllerName', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'MethodName', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'PermissionCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'PermissionableGroupID', 'INT' 
EXEC utility.AddColumn @TableName, 'Description', 'VARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableGroupID', 'INT', 0
GO

EXEC Utility.DropObject 'permissionable.TR_Permissionable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.07
-- Description:	A trigger to update the permissionable.Permissionable table
-- ========================================================================
CREATE TRIGGER permissionable.TR_Permissionable ON permissionable.Permissionable FOR INSERT, UPDATE
AS
SET ARITHABORT ON

DECLARE @cMethodName VARCHAR(250)
DECLARE @nPermissionableID INT

IF EXISTS (SELECT 1 FROM INSERTED)
	BEGIN

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT I.PermissionableID, I.MethodName
		FROM INSERTED I

	OPEN oCursor
	FETCH oCursor INTO @nPermissionableID, @cMethodName
	WHILE @@fetch_status = 0
		BEGIN

		IF @cMethodName IS NULL
			BEGIN

			;
			WITH HD (PermissionableID,ParentPermissionableID,NodeLevel)
				AS 
				(
				SELECT
					P.PermissionableID, 
					P.ParentPermissionableID, 
					1 
				FROM permissionable.Permissionable P
				WHERE P.PermissionableID = @nPermissionableID
		
				UNION ALL
			
				SELECT
					P.PermissionableID, 
					P.ParentPermissionableID, 
					HD.NodeLevel + 1 AS NodeLevel
				FROM permissionable.Permissionable P
					JOIN HD ON HD.ParentPermissionableID = P.PermissionableID 
				)
	
			UPDATE P
			SET P.PermissionableLineage = STUFF(CAST((SELECT '.' + P.PermissionableCode FROM HD JOIN permissionable.Permissionable P ON P.PermissionableID = HD.PermissionableID ORDER BY HD.NodeLevel DESC FOR XML PATH('')) AS VARCHAR(MAX)), 1, 1, '')
			FROM permissionable.Permissionable P
			WHERE P.PermissionableID = @nPermissionableID

			END
		ELSE
			BEGIN

			UPDATE P
			SET P.PermissionableLineage = 			
				P.ControllerName 
					+ '.' 
					+ P.MethodName
					+ CASE
							WHEN P.PermissionCode IS NOT NULL
							THEN '.' + P.PermissionCode
							ELSE ''
						END
	
			FROM permissionable.Permissionable P
			WHERE P.PermissionableID = @nPermissionableID
			
			END
		--ENDIF
		
		FETCH oCursor INTO @nPermissionableID, @cMethodName
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'EquipmentDistribution.List')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ControllerName, MethodName)
	VALUES
		('EquipmentDistribution', 'List'),
		('Permissionable', 'List'),
		('Workflow', 'List')

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PermissionableGroup
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableGroup'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableGroup
	(
	PermissionableGroupID INT IDENTITY(1,1) NOT NULL,
	PermissionableGroupCode VARCHAR(50),
	PermissionableGroupName VARCHAR(100),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_PermissionableGroup', 'DisplayOrder,PermissionableGroupName,PermissionableGroupID'
GO
--End table permissionable.PermissionableGroup

--Begin table procurement.EquipmentDistribution
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistribution'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentDistribution
	(
	EquipmentDistributionID INT IDENTITY(1,1) NOT NULL,
	EquipmentDistributionName VARCHAR(250),
	RecipientContactID INT,
	DeliveredToRecipientDate DATE,
	)

EXEC utility.SetDefaultConstraint @TableName, 'RecipientContactID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'EquipmentDistributionID'
GO
--End table procurement.EquipmentDistribution

--Begin table procurement.EquipmentDistributionEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistributionEquipmentInventory'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.EquipmentDistributionEquipmentInventory
	(
	EquipmentDistributionEquipmentInventoryID INT IDENTITY(1,1) NOT NULL,
	EquipmentDistributionID INT,
	EquipmentInventoryID INT,
	Quantity INT,
	DeliveredToEndUserDate DATE,
	EndUserEntityTypeCode VARCHAR(50),
	EndUserEntityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EndUserEntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentDistributionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EquipmentDistributionEquipmentInventoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_EquipmentDistributionEquipmentInventory', 'EquipmentDistributionID,EquipmentInventoryID'
GO
--End table procurement.EquipmentDistributionInventory

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'EquipmentOrderDate', 'DATE'
EXEC utility.AddColumn @TableName, 'EquipmentDeliveredToImplementerDate', 'DATE'
EXEC utility.AddColumn @TableName, 'InServiceDate', 'DATE'
EXEC utility.AddColumn @TableName, 'LicenseKey', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'EquipmentUsageMinutes', 'INT'
EXEC utility.AddColumn @TableName, 'EquipmentUsageGB', 'INT'

EXEC utility.SetDefaultConstraint 'procurement.EquipmentInventory', 'EquipmentUsageMinutes', 'INT', 0
EXEC utility.SetDefaultConstraint 'procurement.EquipmentInventory', 'EquipmentUsageGB', 'INT', 0
GO
--End table procurement.EquipmentInventory

--Begin table procurement.LicenseEquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.LicenseEquipmentCatalog'

EXEC utility.AddColumn @TableName, 'ItemPageNumber', 'VARCHAR(25)'
EXEC utility.AddColumn @TableName, 'ItemLineNumber', 'VARCHAR(25)'
GO
--End table procurement.LicenseEquipmentCatalog

--Begin table workflow.EntityWorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowStepGroupPerson'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.EntityWorkflowStepGroupPerson
	(
	EntityWorkflowStepGroupPersonID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	WorkflowID INT,
	WorkflowStepID INT,
	WorkflowStepGroupID INT,
	WorkflowName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepName VARCHAR(250),
	WorkflowStepGroupName VARCHAR(250),
	PersonID INT,
	IsComplete BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsComplete', 'BIT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepNumber', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'EntityWorkflowStepGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_EntityWorkflowStepGroupPerson', 'EntityTypeCode,EntityID'
GO
--End table workflow.EntityWorkflowStepGroupPerson

--Begin table workflow.Workflow
DECLARE @TableName VARCHAR(250) = 'workflow.Workflow'

EXEC Utility.AddColumn @TableName, 'WorkflowName', 'VARCHAR(250)'
EXEC Utility.AddColumn @TableName, 'IsActive', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 0
GO
--End table workflow.Workflow

--Begin table workflow.WorkflowStepGroup
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroup'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStepGroup
	(
	WorkflowStepGroupID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepID INT,
	WorkflowStepGroupName VARCHAR(250)
	)

EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'WorkflowStepGroupID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorkflowStepGroup', 'WorkflowStepID,WorkflowStepGroupName'
GO
--End table workflow.WorkflowStepGroup

--Begin table workflow.WorkflowStepGroupPerson
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroupPerson'

EXEC utility.DropObject @TableName

CREATE TABLE workflow.WorkflowStepGroupPerson
	(
	WorkflowStepGroupPersonID INT IDENTITY(1,1) NOT NULL,
	WorkflowStepGroupID INT,
	PersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'WorkflowStepGroupID', 'INT', 0

EXEC utility.SetPrimaryKeyNonclustered @TableName, 'WorkflowStepGroupPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_WorkflowStepGroupPerson', 'WorkflowStepGroupID,PersonID'
GO
--End table workflow.WorkflowStepGroupPerson


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function eventlog.GetCommunityTrainingXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityClassXMLByJusticeUpdateID'
EXEC utility.DropObject 'eventlog.GetCommunityTrainingXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community classes
-- ================================================================

CREATE FUNCTION eventlog.GetCommunityTrainingXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityTrainings VARCHAR(MAX) = ''
	
	SELECT @cCommunityTrainings = COALESCE(@cCommunityTrainings, '') + D.CommunityTraining
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityTraining'), ELEMENTS) AS CommunityTraining
		FROM justiceupdate.CommunityTraining T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityTrainings>' + ISNULL(@cCommunityTrainings, '') + '</CommunityTrainings>'
	
END
GO
--End function eventlog.GetCommunityTrainingXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceTrainingXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceClassXMLByJusticeUpdateID'
EXEC utility.DropObject 'eventlog.GetProvinceTrainingXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province classes
-- ================================================================

CREATE FUNCTION eventlog.GetProvinceTrainingXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceTrainings VARCHAR(MAX) = ''
	
	SELECT @cProvinceTrainings = COALESCE(@cProvinceTrainings, '') + D.ProvinceTraining
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceTraining'), ELEMENTS) AS ProvinceTraining
		FROM justiceupdate.ProvinceTraining T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceTrainings>' + ISNULL(@cProvinceTrainings, '') + '</ProvinceTrainings>'
	
END
GO
--End function eventlog.GetProvinceTrainingXMLByJusticeUpdateID

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
-- =================================================================================================================

CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0

	IF @EntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
					AND EXISTS
						(
						SELECT 1
						FROM person.PersonProject PP
						WHERE PP.PersonID = WSGP.PersonID
							AND PP.ProjectID = W.ProjectID
						)
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE
		BEGIN
	
		DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
					AND EXISTS
						(
						SELECT 1
						FROM person.PersonProject PP
						WHERE PP.PersonID = EWSGP.PersonID
							AND PP.ProjectID = EWSGP.ProjectID
						)
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.CanHaveListView
EXEC utility.DropObject 'workflow.CanHaveListView'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has List and View accesss to a specific EntityTypeCode and EntityID
-- =====================================================================================================================

CREATE FUNCTION workflow.CanHaveListView
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveListView BIT = 0
	DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	IF @nWorkflowStepNumber > @nWorkflowStepCount 
		AND EXISTS
			(
			SELECT 1
			FROM person.PersonPermissionable PP
			WHERE PP.PersonID = @PersonID
				AND PP.PermissionableLineage IN (@EntityTypeCode + '.List', @EntityTypeCode + '.View')
			)
		SET @nCanHaveListView = 1
	ELSE IF EXISTS 
		(
		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.PersonID = @PersonID
		)
		SET @nCanHaveListView = 1
	--ENDIF
	
	RETURN @nCanHaveListView
END
GO
--End function workflow.CanHaveListView

--Begin function workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowData
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepCount INT,
	CanRejectAfterFinalApproval BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(SELECT MAX(WS2.WorkflowStepNumber) FROM workflow.Workflow W2 JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID AND W2.IsActive = 1 AND W2.EntityTypeCode = W1.EntityTypeCode) AS WorkflowStepCount,
			ET.CanRejectAfterFinalApproval
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.IsActive = 1
				AND W1.EntityTypeCode = @EntityTypeCode
				AND WS1.WorkflowStepNumber = 1
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = W1.EntityTypeCode
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
			BEGIN
			
			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT
				'Approved',
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM dbo.EntityType ET 
			WHERE ET.EntityTypeCode = @EntityTypeCode
			
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData

--Begin function workflow.GetEntityWorkflowPeople
EXEC utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return people assigned to an entityy's current workflow step
-- =======================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowPeople
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepGroupName VARCHAR(250),
	PersonID INT,
	FullName VARCHAR(250),
	EmailAddress VARCHAR(320),
	IsComplete BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepGroupName, PersonID, FullName, EmailAddress, IsComplete)
		SELECT
			WSG.WorkflowStepGroupName,
			WSGP.PersonID,
			dbo.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN person.Person P ON P.PersonID = WSGP.PersonID
		ORDER BY 1, 2
	
		END
	ELSE
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepGroupName, PersonID, FullName, EmailAddress, IsComplete)
		SELECT
			EWSGP.WorkflowStepGroupName, 
			EWSGP.PersonID,
			dbo.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber
		ORDER BY 1, 2
	
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT = ISNULL((SELECT MAX(EWSGP.WorkflowStepNumber) FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID), 0)
	
	RETURN @nWorkflowStepCount
END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = (SELECT TOP 1 EWSGP.WorkflowStepNumber FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID AND IsComplete = 0 ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID)

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber
END
GO
--End function workflow.GetWorkflowStepNumber

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
-- ===================================================================
CREATE PROCEDURE [dbo].[GetContactByContactID]

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.CommunityAssetID,		
		C1.CommunityAssetUnitID,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,		
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,		
		C1.CommunityID,		
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate(
			(SELECT CreateDateTime 
			FROM eventlog.EventLog 
			WHERE eventcode = 'create'
  			AND entitytypecode = 'contact'
  			AND entityid = C1.ContactID)
		)  AS InitialEntryDateFormatted,
		C1.IsActive,		
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.ProvinceID,		
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(
			SELECT CommunitySubGroupName
			FROM dropdown.CommunitySubGroup
			WHERE CommunitySubGroupID = 
				(
				SELECT CommunitySubGroupID 
				FROM dbo.Community
				WHERE CommunityID = C1.CommunityID
				)
		) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.USVettingDate) AS USVettingDateFormatted,
		dbo.FormatDate(CNC.UKVettingDate) AS UKVettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO1.HexColor, '#', '') + '-vetting.png" /> ' AS USVettingIcon,
		'<img src="/assets/img/icons/' + REPLACE(VO2.HexColor, '#', '') + '-vetting.png" /> ' AS UKVettingIcon,
		VO1.VettingOutcomeName AS USVettingOutcomeName,
		VO2.VettingOutcomeName AS UKVettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CNC.USVettingOutcomeID
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CNC.UKVettingOutcomeID
			AND CNC.ContactID = @ContactID
	ORDER BY CNC.USVettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dropdown.GetEmployerTypeData
EXEC utility.DropObject 'dropdown.GetEmployerTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:		Greg Yingling
-- Create date:	2016.02.22
-- Description:	A stored procedure to return data from the dropdown.EmployerType table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEmployerTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EmployerTypeID, 
		T.EmployerTypeName
	FROM dropdown.EmployerType T
	WHERE (T.EmployerTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EmployerTypeName, T.EmployerTypeID

END
GO
--End procedure dropdown.GetEmployerTypeData

--Begin procedure eventlog.LogEquipmentDistributionAction
EXEC utility.DropObject 'eventlog.LogEquipmentDistributionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.10.30
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentDistributionAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentDistribution',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cEquipmentDistributionEquipmentInventory VARCHAR(MAX) 
	
		SELECT 
			@cEquipmentDistributionEquipmentInventory = COALESCE(@cEquipmentDistributionEquipmentInventory, '') + D.EquipmentDistributionEquipmentInventory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('EquipmentDistributionEquipmentInventory'), ELEMENTS) AS EquipmentDistributionEquipmentInventory
			FROM procurement.EquipmentDistributionEquipmentInventory T 
			WHERE T.EquipmentDistributionID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentDistribution',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<EquipmentDistributionEquipmentInventory>' + ISNULL(@cEquipmentDistributionEquipmentInventory, '') + '</EquipmentDistributionEquipmentInventory>') AS XML)
			FOR XML RAW('EquipmentDistribution'), ELEMENTS
			)
		FROM procurement.EquipmentDistribution T
		WHERE T.EquipmentDistributionID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentDistributionAction

--Begin procedure eventlog.LogJusticeAction
EXEC Utility.DropObject 'eventlog.LogJusticeUpdateAction'
EXEC Utility.DropObject 'eventlog.LogJusticeAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add data to the event log
-- ============================================================
CREATE PROCEDURE eventlog.LogJusticeAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'JusticeUpdate',
			T.JusticeUpdateID,
			@Comments
		FROM justiceupdate.JusticeUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.JusticeUpdateID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'JusticeUpdate',
			T.JusticeUpdateID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityTrainingXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceTrainingXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML))
			FOR XML RAW('JusticeUpdate'), ELEMENTS
			)
		FROM justiceupdate.JusticeUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.JusticeUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogJusticeAction

--Begin procedure eventlog.LogWorkflowAction
EXEC utility.DropObject 'eventlog.LogWorkflowAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.10.30
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWorkflowAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Workflow',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cWorkflowSteps VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowSteps = COALESCE(@cWorkflowSteps, '') + D.WorkflowStep 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStep'), ELEMENTS) AS WorkflowStep
			FROM Workflow.WorkflowStep T 
			WHERE T.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroups VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroups = COALESCE(@cWorkflowStepGroups, '') + D.WorkflowStepGroup 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroup'), ELEMENTS) AS WorkflowStepGroup
			FROM Workflow.WorkflowStepGroup T
				JOIN Workflow.WorkflowStep WS ON WS.WorkflowStepID = T.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroupPersons VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroupPersons = COALESCE(@cWorkflowStepGroupPersons, '') + D.WorkflowStepGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroupPerson'), ELEMENTS) AS WorkflowStepGroupPerson
			FROM Workflow.WorkflowStepGroupPerson T 
				JOIN Workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = T.WorkflowStepGroupID
				JOIN Workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Workflow',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<WorkflowSteps>' + ISNULL(@cWorkflowSteps, '') + '</WorkflowSteps>') AS XML),
			CAST(('<WorkflowStepGroups>' + ISNULL(@cWorkflowStepGroups, '') + '</WorkflowStepGroups>') AS XML),
			CAST(('<WorkflowStepGroupPersons>' + ISNULL(@cWorkflowStepGroupPersons, '') + '</WorkflowStepGroupPersons>') AS XML)
			FOR XML RAW('Workflow'), ELEMENTS
			)
		FROM Workflow.Workflow T
		WHERE T.WorkflowID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWorkflowAction

--Begin procedure fifupdate.AddFIFCommunity
EXEC Utility.DropObject 'fifupdate.AddFIFCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Community table
-- ============================================================================
CREATE PROCEDURE fifupdate.AddFIFCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)
	
	INSERT INTO fifupdate.Community
		(CommunityID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@FIFUpdateID,
		C1.FIFCommunityEngagement, 
		C1.FIFJustice, 
		C1.FIFPoliceEngagement, 
		C1.FIFUpdateNotes, 
		C1.IndicatorUpdate, 
		C1.MeetingNotes, 
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO fifupdate.CommunityIndicator
		(FIFUpdateID, CommunityID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.FIFAchievedValue, 0),
		OACI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.FIFAchievedValue,
				CI.FIFNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	;
	WITH CRD AS
		(
		SELECT DISTINCT 
			RR.RiskID,
			RC.CommunityID
		FROM recommendation.RecommendationCommunity RC 
			JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC.RecommendationID
			JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
		)

	INSERT INTO fifupdate.CommunityRisk
		(FIFUpdateID, CommunityID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CR.CommunityID, 
		CR.RiskID,
		ISNULL(CR.FIFRiskValue, 0),
		CR.FIFNotes
	FROM dbo.CommunityRisk CR
		JOIN CRD ON CRD.CommunityID = CR.CommunityID
			AND CRD.RiskID = CR.RiskID

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFCommunity

--Begin procedure fifupdate.AddFIFProvince
EXEC Utility.DropObject 'fifupdate.AddFIFProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Province table
-- ===========================================================================
CREATE PROCEDURE fifupdate.AddFIFProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)

	INSERT INTO fifupdate.Province
		(ProvinceID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@FIFUpdateID,
		P1.FIFCommunityEngagement, 
		P1.FIFJustice, 
		P1.FIFPoliceEngagement, 
		P1.FIFUpdateNotes, 
		P1.IndicatorUpdate, 
		P1.MeetingNotes,
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO fifupdate.ProvinceIndicator
		(FIFUpdateID, ProvinceID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.FIFAchievedValue, 0),
		OAPI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.FIFAchievedValue,
				PRI.FIFNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI

	;
	WITH PRD AS
		(
		SELECT DISTINCT 
			RR.RiskID,
			RP.ProvinceID
		FROM recommendation.RecommendationProvince RP 
			JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
			JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
		)

	INSERT INTO fifupdate.ProvinceRisk
		(FIFUpdateID, ProvinceID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		PR.ProvinceID, 
		PR.RiskID,
		ISNULL(PR.FIFRiskValue, 0),
		PR.FIFNotes
	FROM dbo.ProvinceRisk PR
		JOIN PRD ON PRD.ProvinceID = PR.ProvinceID
			AND PRD.RiskID = PR.RiskID

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFProvince

--Begin procedure justiceupdate.AddJusticeCommunity
EXEC utility.DropObject 'justiceupdate.AddJusticeCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add a community for a justice update batch
-- =============================================================================
CREATE PROCEDURE justiceupdate.AddJusticeCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)
	
	INSERT INTO justiceupdate.Community
		(CommunityID, JusticeUpdateID, JusticeAssessmentNotes, JusticeCommunityDocumentCenterStatusNotes, JusticeTrainingNotes, JusticeCommunityDocumentCenterStatusID, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@JusticeUpdateID,
		C1.JusticeAssessmentNotes, 
		C1.JusticeCommunityDocumentCenterStatusNotes, 
		C1.JusticeTrainingNotes, 
		C1.JusticeCommunityDocumentCenterStatusID, 
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM justiceupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO justiceupdate.CommunityIndicator
		(JusticeUpdateID, CommunityID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.JusticeAchievedValue, 0),
		OACI.JusticeNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'JO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.JusticeAchievedValue,
				CI.JusticeNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO justiceupdate.CommunityRisk
		(JusticeUpdateID, CommunityID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		D.CommunityID,
		R.RiskID,
		ISNULL(OACR.JusticeRiskValue, 0),
		OACR.JusticeNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RC.CommunityID
			FROM recommendation.RecommendationCommunity RC 
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC.RecommendationID
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				CR.JusticeRiskValue, 
				CR.JusticeNotes
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = D.CommunityID
			) OACR

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.AddJusticeCommunity

--Begin procedure justiceupdate.AddJusticeProvince
EXEC utility.DropObject 'justiceupdate.AddJusticeProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add a province for a justice update batch
-- ============================================================================
CREATE PROCEDURE justiceupdate.AddJusticeProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)

	INSERT INTO justiceupdate.Province
		(ProvinceID, JusticeUpdateID, JusticeAssessmentNotes, JusticeCommunityDocumentCenterStatusNotes, JusticeTrainingNotes, JusticeCommunityDocumentCenterStatusID, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@JusticeUpdateID,
		P1.JusticeAssessmentNotes, 
		P1.JusticeCommunityDocumentCenterStatusNotes, 
		P1.JusticeTrainingNotes, 
		P1.JusticeCommunityDocumentCenterStatusID, 
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM justiceupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO justiceupdate.ProvinceIndicator
		(JusticeUpdateID, ProvinceID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.JusticeAchievedValue, 0),
		OAPI.JusticeNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'JO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.JusticeAchievedValue,
				PRI.JusticeNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO justiceupdate.ProvinceRisk
		(JusticeUpdateID, ProvinceID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		D.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.JusticeRiskValue, 0),
		OAPR.JusticeNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RP.ProvinceID
			FROM recommendation.RecommendationProvince RP
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				PR.JusticeRiskValue, 
				PR.JusticeNotes
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = D.ProvinceID
			) OAPR

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.AddJusticeProvince

--Begin procedure justiceupdate.ApproveJusticeUpdate
EXEC utility.DropObject 'justiceupdate.ApproveJusticeUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to approve a justice update batch
-- =================================================================
CREATE PROCEDURE justiceupdate.ApproveJusticeUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nJusticeUpdateID INT = ISNULL((SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogJusticeAction @nJusticeUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogJusticeAction @nJusticeUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.JusticeAssessmentNotes = FU.JusticeAssessmentNotes,
		P.JusticeCommunityDocumentCenterStatusNotes = FU.JusticeCommunityDocumentCenterStatusNotes,
		P.JusticeTrainingNotes = FU.JusticeTrainingNotes,
		P.JusticeCommunityDocumentCenterStatusID = FU.JusticeCommunityDocumentCenterStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN justiceupdate.Province FU ON FU.ProvinceID = P.ProvinceID
			AND FU.JusticeUpdateID = @nJusticeUpdateID

	-- Province Indicator
	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.JusticeAchievedValue, 
		PRI.JusticeNotes
	FROM justiceupdate.ProvinceIndicator PRI

	-- Province Risk
	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.JusticeRiskValue, 
		PR.JusticeNotes
	FROM justiceupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	--Province Documents
	UPDATE DE
	SET DE.EntityTypeCode = 'Province'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'JusticeUpdateProvince'
			AND D.DocumentDescription IN ('Justice Assessment')

	UPDATE D
	SET D.DocumentTypeID = (SELECT TOP 1 DocumentTypeID FROM dropdown.DocumentType WHERE DocumentTypeName like '%justice')
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('Justice Assessment')

	UPDATE C
	SET
		C.JusticeAssessmentNotes = FU.JusticeAssessmentNotes,
		C.JusticeCommunityDocumentCenterStatusNotes = FU.JusticeCommunityDocumentCenterStatusNotes,
		C.JusticeTrainingNotes = FU.JusticeTrainingNotes,
		C.JusticeCommunityDocumentCenterStatusID = FU.JusticeCommunityDocumentCenterStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN justiceupdate.Community FU ON FU.CommunityID = C.CommunityID
			AND FU.JusticeUpdateID = @nJusticeUpdateID

	-- Community Indicator
	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.JusticeAchievedValue, 
		CI.JusticeNotes
	FROM justiceupdate.CommunityIndicator CI

	-- Community Risk
	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.JusticeRiskValue, 
		CR.JusticeNotes
	FROM justiceupdate.CommunityRisk CR

	-- Province Training
	DELETE PRM
	FROM dbo.ProvinceTraining PRM
		JOIN @tOutputProvince O ON O.ProvinceID = PRM.ProvinceID
	
	INSERT INTO dbo.ProvinceTraining
		(ProvinceID, TrainingDate, TrainingTitle, AttendeeCount)
	SELECT
		PRM.ProvinceID,
		PRM.TrainingDate, 
		PRM.TrainingTitle,
		PRM.AttendeeCount
	FROM justiceupdate.ProvinceTraining PRM

	-- Community Training
	DELETE CM
	FROM dbo.CommunityTraining CM
		JOIN @tOutputCommunity O ON O.CommunityID = CM.CommunityID
	
	INSERT INTO dbo.CommunityTraining
		(CommunityID, TrainingDate, TrainingTitle, AttendeeCount)
	SELECT
		CM.CommunityID,
		CM.TrainingDate,
		CM.TrainingTitle, 
		CM.AttendeeCount
	FROM justiceupdate.CommunityTraining CM	

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Community'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'JusticeUpdateCommunity'
			AND D.DocumentDescription IN ('Justice Assessment')	
	UPDATE D
	SET D.DocumentTypeID = (SELECT TOP 1 DocumentTypeID FROM dropdown.DocumentType WHERE DocumentTypeName like '%justice')
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('Justice Assessment')	
	
	DELETE FROM justiceupdate.JusticeUpdate

	TRUNCATE TABLE justiceupdate.Province
	TRUNCATE TABLE justiceupdate.ProvinceIndicator
	TRUNCATE TABLE justiceupdate.ProvinceTraining
	TRUNCATE TABLE justiceupdate.ProvinceRisk

	TRUNCATE TABLE justiceupdate.Community
	TRUNCATE TABLE justiceupdate.CommunityIndicator
	TRUNCATE TABLE justiceupdate.CommunityTraining
	TRUNCATE TABLE justiceupdate.CommunityRisk

END
GO
--End procedure justiceupdate.ApproveJusticeUpdate

--Begin procedure justiceupdate.DeleteJusticeCommunity
EXEC utility.DropObject 'justiceupdate.DeleteJusticeCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to delete a community from a justice update batch
-- =================================================================================
CREATE PROCEDURE justiceupdate.DeleteJusticeCommunity

@CommunityID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)

	DELETE T
	FROM justiceupdate.Community T
	WHERE T.CommunityID = @CommunityID
	
	DELETE T
	FROM justiceupdate.CommunityIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM justiceupdate.CommunityRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)

	DELETE T
	FROM justiceupdate.CommunityTraining T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.DeleteJusticeCommunity

--Begin procedure justiceupdate.DeleteJusticeProvince
EXEC utility.DropObject 'justiceupdate.DeleteJusticeProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to delete a province from a justice update batch
-- ================================================================================
CREATE PROCEDURE justiceupdate.DeleteJusticeProvince

@ProvinceID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)
	
	DELETE T
	FROM justiceupdate.Province T
	WHERE T.ProvinceID = @ProvinceID
	
	DELETE T
	FROM justiceupdate.ProvinceIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM justiceupdate.ProvinceRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)

	DELETE T
	FROM justiceupdate.ProvinceTraining T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.DeleteJusticeProvince

--Begin procedure justiceupdate.GetCommunityByCommunityID
EXEC utility.DropObject 'justiceupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get a community for a justice update batch
-- =============================================================================
CREATE PROCEDURE justiceupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.JusticeAssessmentNotes,
		C.JusticeCommunityDocumentCenterStatusNotes,
		C.JusticeTrainingNotes,
		C.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM dbo.Community C
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = C.JusticeCommunityDocumentCenterStatusID
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CommunityID,
		C1.CommunityName AS EntityName,
		C2.JusticeAssessmentNotes,
		C2.JusticeCommunityDocumentCenterStatusNotes,
		C2.JusticeTrainingNotes,
		C2.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM justiceupdate.Community C2
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = C2.JusticeCommunityDocumentCenterStatusID
		JOIN dbo.Community C1 ON C1.CommunityID = C2.CommunityID
			AND C2.CommunityID = @CommunityID

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.JusticeAchievedValue,
		OACI.JusticeNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.JusticeAchievedValue, 
				CI.JusticeNotes
			FROM justiceupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.JusticeRiskValue,
		OACR.JusticeNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.JusticeRiskValue, 
				CR.JusticeNotes
			FROM justiceupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingsCurrent
	SELECT
		CM.AttendeeCount, 
		CM.TrainingDate, 
		dbo.FormatDate(CM.TrainingDate) AS TrainingDateFormatted,
		CM.CommunityTrainingID AS TrainingID, 
		CM.TrainingTitle
	FROM dbo.CommunityTraining CM
	WHERE CM.CommunityID = @CommunityID

	--EntityTrainingsUpdate
	SELECT
		CM.AttendeeCount, 
		CM.TrainingDate, 
		dbo.FormatDate(CM.TrainingDate) AS TrainingDateFormatted,
		CM.CommunityTrainingID AS TrainingID, 
		CM.TrainingTitle
	FROM justiceupdate.CommunityTraining CM
	WHERE CM.CommunityID = @CommunityID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'JusticeUpdateCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

END
GO
--End procedure justiceupdate.GetCommunityByCommunityID

--Begin procedure justiceupdate.GetProvinceByProvinceID
EXEC utility.DropObject 'justiceupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get a province for a justice update batch
-- ============================================================================
CREATE PROCEDURE justiceupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.ProvinceID,
		P.ProvinceName AS EntityName,
		P.JusticeAssessmentNotes,
		P.JusticeCommunityDocumentCenterStatusNotes,
		P.JusticeTrainingNotes,
		P.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM dbo.Province P
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = P.JusticeCommunityDocumentCenterStatusID
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.ProvinceID,
		P1.ProvinceName AS EntityName,
		P2.JusticeAssessmentNotes,
		P2.JusticeCommunityDocumentCenterStatusNotes,
		P2.JusticeTrainingNotes,
		P2.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM justiceupdate.Province P2
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = P2.JusticeCommunityDocumentCenterStatusID
		JOIN dbo.Province P1 ON P1.ProvinceID = P2.ProvinceID
			AND P2.ProvinceID = @ProvinceID

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PI.ProvinceIndicatorID
			FROM dbo.ProvinceIndicator PI 
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OAPI.JusticeAchievedValue,
		OAPI.JusticeNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PI.JusticeAchievedValue, 
				PI.JusticeNotes
			FROM justiceupdate.ProvinceIndicator PI
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.JusticeRiskValue,
		OAPR.JusticeNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.JusticeRiskValue, 
				PR.JusticeNotes
			FROM justiceupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingsCurrent
	SELECT
		PM.AttendeeCount, 
		PM.TrainingDate, 
		dbo.FormatDate(PM.TrainingDate) AS TrainingDateFormatted,
		PM.ProvinceTrainingID AS TrainingID, 
		PM.TrainingTitle
	FROM dbo.ProvinceTraining PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityTrainingsUpdate
	SELECT
		PM.AttendeeCount, 
		PM.TrainingDate, 
		dbo.FormatDate(PM.TrainingDate) AS TrainingDateFormatted,
		PM.ProvinceTrainingID AS TrainingID, 
		PM.TrainingTitle
	FROM justiceupdate.ProvinceTraining PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'JusticeUpdateProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

END
GO
--End procedure justiceupdate.GetProvinceByProvinceID

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to data from the dbo.SpotReport table
-- =====================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.SummaryMap,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName,
		dbo.GetSpotReportProvincesList(SR.SpotreportID) as ProvinceList,
		dbo.GetSpotReportCommunitiesList(SR.SpotreportID) as CommunityList,
		dbo.FormatStaticGoogleMapForSpotReport(SR.SpotReportID) AS GMap		
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.LicenseEquipmentCatalog table
-- ==========================================================================================
CREATE PROCEDURE procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID

@LicenseEquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentCatalogID,
		EC.ItemName,
		L.LicenseID,
		L.LicenseNumber,
		LEC.BudgetLimit,
		LEC.ECCN,
		LEC.LicenseEquipmentCatalogID,
		LEC.LicenseItemDescription,
		LEC.ReferenceCode,
		LEC.QuantityAuthorized,
		LEC.QuantityDistributed,
		LEC.QuantityObligated,
		LEC.QuantityPending,
		LEC.QuantityPurchased,
		LEC.ItemPageNumber,
		LEC.ItemLineNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('LicensedEquipmentCatalog') AS EntityTypeName
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = LEC.EquipmentCatalogID
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND LEC.LicenseEquipmentCatalogID = @LicenseEquipmentCatalogID
		
END
GO
--End procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
--
-- Author:			Greg Yingling
-- Update date:	2015.05.26
-- Description:	Added the Audit Outcome, Audit Date, and Audit Evidence Call
--
-- Author:			Todd Pires
-- Update date:	2015.11.29
-- Description:	Refactored the audit recordset
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.ExpirationDate,
		dbo.FormatDate(EI.ExpirationDate) AS ExpirationDateFormatted,
		EI.IMEIMACAddress,
		EI.UnitCost,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.EquipmentStatusID,
		ES.EquipmentStatusName,
		EI.EquipmentRemovalReasonID,
		ER.EquipmentRemovalReasonName,
		EI.EquipmentRemovalDate,
		EI.EquipmentOrderDate,
		dbo.FormatDate(EI.EquipmentOrderDate) AS EquipmentOrderDateFormatted,
		EI.EquipmentDeliveredToImplementerDate,
		dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted,
		EI.InServiceDate,
		dbo.FormatDate(EI.InServiceDate) AS InServiceDateFormatted,
		EI.LicenseKey,
		EI.EquipmentUsageMinutes,
		EI.EquipmentUsageGB,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		D.Territory,
		SUM(D.Quantity) AS Quantity,
		dbo.FormatConceptNoteTitle(D.ConceptNoteID) AS ConceptNote,
		dbo.FormatDate(D.DistributionDate) AS DistributionDateFormatted,
		IIF(D.IsLost = 1, 'Yes ', 'No ') AS IsLost
	FROM
		(
		SELECT
			C.CommunityName + ' (Community)' AS Territory,
			CEI.Quantity,
			CEI.ConceptNoteID,
			CEI.DistributionDate,
			CEI.IsLost
		FROM procurement.CommunityEquipmentInventory CEI
			JOIN dbo.Community C ON C.CommunityID = CEI.CommunityID
				AND CEI.EquipmentInventoryID = @EquipmentInventoryID
	
		UNION
	
		SELECT
			P.ProvinceName + ' (Province)' AS Territory,
			PEI.Quantity,
			PEI.ConceptNoteID,
			PEI.DistributionDate,
			PEI.IsLost
		FROM procurement.ProvinceEquipmentInventory PEI
			JOIN dbo.Province P ON P.ProvinceID = PEI.ProvinceID
				AND PEI.EquipmentInventoryID = @EquipmentInventoryID
		) D
	GROUP BY D.Territory, D.ConceptNoteID, D.DistributionDate, D.IsLost
	ORDER BY D.Territory

	SELECT
		CEIA.AuditQuantity, 
		CEIA.AuditDate,
		dbo.FormatDate(CEIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(CEIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = CEIA.DocumentID)
			THEN '<a class="btn btn-info" href="/servefile/getFile/GUID/' + (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = CEIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN CEIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(''Community'', ' + CAST(CEIA.CommunityEquipmentInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.CommunityEquipmentInventoryAudit CEIA
		JOIN procurement.CommunityEquipmentInventory CEI ON CEI.CommunityEquipmentInventoryID = CEIA.CommunityEquipmentInventoryID
			AND CEI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = CEIA.AuditOutcomeID
	
	UNION
	
	SELECT
		PEIA.AuditQuantity, 
		PEIA.AuditDate,
		dbo.FormatDate(PEIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(PEIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = PEIA.DocumentID)
			THEN '<a class="btn btn-info" href="/servefile/getFile/GUID/' + (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = PEIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN PEIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(''Province'', ' + CAST(PEIA.ProvinceEquipmentInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.ProvinceEquipmentInventoryAudit PEIA
		JOIN procurement.ProvinceEquipmentInventory PEI ON PEI.ProvinceEquipmentInventoryID = PEIA.ProvinceEquipmentInventoryID
			AND PEI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = PEIA.AuditOutcomeID
	
	ORDER BY 2 DESC

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure workflow.DecrementWorkflow
EXEC Utility.DropObject 'workflow.DecrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.10.28
-- Description:	A stored procedure to Decrement a workflow
-- =======================================================
CREATE PROCEDURE workflow.DecrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	UPDATE EWSGP
	SET EWSGP.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	SELECT
		@nWorkflowStepNumber AS OldWorkflowStepNumber,
		workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
		workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount

END
GO
--End procedure dbo.DecrementWorkflow

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ============================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		dbo.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.IncrementWorkflow
EXEC Utility.DropObject 'workflow.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.10.28
-- Description:	A stored procedure to increment a workflow
-- =======================================================
CREATE PROCEDURE workflow.IncrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @nWorkflowStepGroupID INT
	
	SELECT TOP 1
		@nWorkflowStepNumber = EWSGP.WorkflowStepNumber,
		@nWorkflowStepGroupID = EWSGP.WorkflowStepGroupID	
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.PersonID = @PersonID
		AND EWSGP.IsComplete = 0
		AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID
	
	IF @nWorkflowStepNumber > 0 AND @nWorkflowStepGroupID > 0
		BEGIN
	
		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGP.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	ELSE
		BEGIN
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			0 AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	--ENDIF

END
GO
--End procedure dbo.IncrementWorkflow

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1

END
GO
--End procedure workflow.InitializeEntityWorkflow
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'JusticeUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	SELECT
		'JusticeUpdate',
		ET.WorkflowActionCode,
		
		CASE
			WHEN ET.WorkflowActionCode = 'DecrementWorkflow'
			THEN '<p>A previously submitted Justice update has been disapproved:</p><p>&nbsp;</p><p><strong>Justice Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Justice update workflow. Please click the link above to review the updated Justice.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'IncrementWorkflow'
			THEN '<p>An AJACS Justice update has been submitted for your review:</p><p>&nbsp;</p><p><strong>Justice Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Justice update workflow. Please click the link above to review the updated Justice.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'Release'
			THEN '<p>An AJACS Justice update has been released:</p><p>&nbsp;</p><p><strong>Justice Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the Justice update workflow. Please click the link above to review the updated Justice.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
		END
	
	FROM dbo.EmailTemplate ET
	WHERE ET.EntityTypeCode = 'RiskUpdate'

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'JusticeUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('JusticeUpdate', '[[Link]]', 'Link', 1),
		('JusticeUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'EquipmentDistribution', 'Equipment Distribution'
GO
EXEC utility.EntityTypeAddUpdate 'JusticeCommunity', 'Justice Community'
GO
EXEC utility.EntityTypeAddUpdate 'JusticeProvince', 'Justice Province'
GO
EXEC utility.EntityTypeAddUpdate 'JusticeUpdate', 'Justice Update'
GO
EXEC utility.EntityTypeAddUpdate 'Workflow', 'Workflow'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='JusticeUpdate', @NewMenuItemLink='/justice/addupdate', @NewMenuItemText='Justice', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='PoliceEngagementUpdate', @PermissionableLineageList='JusticeUpdate.WorkflowStepID%'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentDistributionList', @NewMenuItemLink='/equipmentdistribution/listdistribution', @NewMenuItemText='Equipment Distributions', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentInventoryList', @PermissionableLineageList='EquipmentDistribution.List'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='PermissionableList', @AfterMenuItemCode='PersonList', @NewMenuItemText='Permissionables', @NewMenuItemLink='/permissionable/list', @PermissionableLineageList='Permissionable.List'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PermissionableTemplateList', @AfterMenuItemCode='PermissionableList'
GO
EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='WorkflowList', @AfterMenuItemCode='ServerSetupList', @NewMenuItemText='Workflows', @NewMenuItemLink='/workflow/list', @PermissionableLineageList='Workflow.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem

--Begin table workflow.Workflow
IF NOT EXISTS (SELECT 1 FROM workflow.Workflow W WHERE W.EntityTypeCode = 'JusticeUpdate')
	BEGIN
	
	INSERT INTO workflow.Workflow
		(EntityTypeCode,WorkflowStepCount)
	VALUES
		('JusticeUpdate',3)
	
	INSERT INTO workflow.WorkflowStep
		(WorkflowID, WorkflowStepNumber, WorkflowStepName, WorkflowStatusName)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'JusticeUpdate'),
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WS.WorkflowStatusName
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WS.WorkflowID AND W2.EntityTypeCode = 'RiskUpdate'
	
	INSERT INTO workflow.WorkflowStepWorkflowAction
		(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
	SELECT
		(SELECT W1.WorkflowID FROM workflow.Workflow W1 WHERE W1.EntityTypeCode = 'JusticeUpdate'),
		WSWA.WorkflowStepNumber,
		WSWA.WorkflowActionID,
		WSWA.DisplayOrder
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA1 ON WA1.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W2 ON W2.WorkflowID = WSWA.WorkflowID AND W2.EntityTypeCode = 'RiskUpdate'
	
	END
--ENDIF
GO
--End table workflow.Workflow

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('JusticeUpdate','Justice', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('JusticeUpdate')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'JusticeUpdate')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'JusticeUpdate','Justice')
		
	END
--ENDIF
GO

DECLARE @nPadLength INT
DECLARE @tOutput TABLE (PermissionableID INT NOT NULL PRIMARY KEY, WorkflowStepID INT NOT NULL DEFAULT 0)
DECLARE @tTable TABLE (ParentPermissionableID INT, PermissionableCode VARCHAR(50), PermissionableName VARCHAR(100), DisplayOrder INT, ParentWorkflowStepID INT, WorkflowStepNumber INT, WorkflowStepID INT, DisplayIndex VARCHAR(255))

SELECT @nPadLength = LEN(CAST(COUNT(WS.WorkflowStepID) AS VARCHAR(50))) FROM workflow.WorkflowStep WS

;
WITH HD (DisplayIndex,WorkflowStepID,ParentWorkflowStepID,NodeLevel)
	AS
	(
	SELECT
		CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		1
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND WS.ParentWorkflowStepID = 0
			AND W.EntityTypeCode = 'JusticeUpdate'
		
	UNION ALL
		
	SELECT
		CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY WS.WorkflowID, WS.WorkflowStepNumber, WS.WorkflowStepName) AS VARCHAR(10)), @nPadLength)),
		WS.WorkflowStepID,
		WS.ParentWorkflowStepID,
		HD.NodeLevel + 1 AS NodeLevel
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = 'JusticeUpdate'
		JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
	)

INSERT INTO @tTable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, ParentWorkflowStepID, WorkflowStepNumber, WorkflowStepID, DisplayIndex)
SELECT
	(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'JusticeUpdate'),
	'WorkflowStepID' + CAST(HD.WorkflowStepID AS VARCHAR(10)),
	WS.WorkflowStepName,
	WS.WorkflowStepNumber,
	HD.ParentWorkflowStepID,
	WS.WorkflowStepNumber,
	HD.WorkflowStepID,
	HD.DisplayIndex
FROM HD
	JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
	JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
OUTPUT INSERTED.PermissionableID, 0 INTO @tOutput
SELECT
	T.ParentPermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.ParentPermissionableID ORDER BY T.DisplayIndex),
	1
FROM @tTable T
WHERE T.ParentWorkflowStepID = 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableCode = T.PermissionableCode
		)
ORDER BY T.DisplayIndex

UPDATE O
SET O.WorkflowStepID = CAST(REPLACE(P.PermissionableCode, 'WorkflowStepID', '') AS INT)
FROM @tOutput O
	JOIN permissionable.Permissionable P ON P.PermissionableID = O.PermissionableID

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder, IsWorkflow)
SELECT
	O.PermissionableID, 
	T.PermissionableCode, 
	T.PermissionableName, 
	ROW_NUMBER() OVER (PARTITION BY T.WorkflowStepNumber ORDER BY T.PermissionableName),
	1
FROM @tTable T
	JOIN @tOutput O ON O.WorkflowStepID = T.ParentWorkflowStepID
		AND T.ParentWorkflowStepID > 0
		AND NOT EXISTS
			(
			SELECT 1
			FROM permissionable.Permissionable P
			WHERE P.PermissionableCode = T.PermissionableCode
			)
ORDER BY T.DisplayIndex
GO

DELETE P FROM permissionable.Permissionable P WHERE P.PermissionableCode IN ('CSWGMemberView','CSWGMemberUpdateVetting')
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CSWGMember')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'CSWGMember',
		'View CSWG Member',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.View'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'CSWGMemberUpdateVetting')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
	SELECT
		P.PermissionableID,
		'CSWGMemberUpdateVetting',
		'CSWG Member Update Vetting',
		2
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.List.CSWGMemberUpdateVetting'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'Contact.AddUpdate.CSWGMember')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID,PermissionableCode,PermissionableName,DisplayOrder)
	SELECT
		P.PermissionableID,
		'CSWGMember',
		'Add / Edit CSWG Member',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'Contact.AddUpdate'
	
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'FIFUpdate.View')
	BEGIN
	
	INSERT INTO permissionable.Permissionable 
		(ParentPermissionableID,PermissionableCode,PermissionableName,DisplayOrder)
	SELECT
		P.PermissionableID,
		'View',
		'View',
		1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = 'FIFUpdate'
	
	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('JusticeUpdate','Justice Update', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'JusticeUpdate'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('JusticeUpdate')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

EXEC utility.ServerSetupKeyAddUpdate 'ShowForceOnCSSFPortal', '1'

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveD',
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveD',
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.48 File 01 - AJACS - 2016.02.26 20.04.45')
GO
--End build tracking

