USE AJACS
GO

--Begin function eventlog.GetCommunityTrainingXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetCommunityClassXMLByJusticeUpdateID'
EXEC utility.DropObject 'eventlog.GetCommunityTrainingXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for community classes
-- ================================================================

CREATE FUNCTION eventlog.GetCommunityTrainingXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cCommunityTrainings VARCHAR(MAX) = ''
	
	SELECT @cCommunityTrainings = COALESCE(@cCommunityTrainings, '') + D.CommunityTraining
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('CommunityTraining'), ELEMENTS) AS CommunityTraining
		FROM justiceupdate.CommunityTraining T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<CommunityTrainings>' + ISNULL(@cCommunityTrainings, '') + '</CommunityTrainings>'
	
END
GO
--End function eventlog.GetCommunityTrainingXMLByJusticeUpdateID

--Begin function eventlog.GetProvinceTrainingXMLByJusticeUpdateID
EXEC utility.DropObject 'eventlog.GetProvinceClassXMLByJusticeUpdateID'
EXEC utility.DropObject 'eventlog.GetProvinceTrainingXMLByJusticeUpdateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Jonathan Burnahm
-- Create Date:	2016.02.13
-- Description:	A function to return XML data for Province classes
-- ================================================================

CREATE FUNCTION eventlog.GetProvinceTrainingXMLByJusticeUpdateID
(
@JusticeUpdateID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceTrainings VARCHAR(MAX) = ''
	
	SELECT @cProvinceTrainings = COALESCE(@cProvinceTrainings, '') + D.ProvinceTraining
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('ProvinceTraining'), ELEMENTS) AS ProvinceTraining
		FROM justiceupdate.ProvinceTraining T 
		WHERE T.JusticeUpdateID = @JusticeUpdateID
		) D

	RETURN '<ProvinceTrainings>' + ISNULL(@cProvinceTrainings, '') + '</ProvinceTrainings>'
	
END
GO
--End function eventlog.GetProvinceTrainingXMLByJusticeUpdateID

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
-- =================================================================================================================

CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT,
@ProjectID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0

	IF @EntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
					AND EXISTS
						(
						SELECT 1
						FROM person.PersonProject PP
						WHERE PP.PersonID = WSGP.PersonID
							AND PP.ProjectID = W.ProjectID
						)
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE
		BEGIN
	
		DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
					AND EXISTS
						(
						SELECT 1
						FROM person.PersonProject PP
						WHERE PP.PersonID = EWSGP.PersonID
							AND PP.ProjectID = EWSGP.ProjectID
						)
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.CanHaveListView
EXEC utility.DropObject 'workflow.CanHaveListView'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has List and View accesss to a specific EntityTypeCode and EntityID
-- =====================================================================================================================

CREATE FUNCTION workflow.CanHaveListView
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveListView BIT = 0
	DECLARE @nWorkflowStepCount INT = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	IF @nWorkflowStepNumber > @nWorkflowStepCount 
		AND EXISTS
			(
			SELECT 1
			FROM person.PersonPermissionable PP
			WHERE PP.PersonID = @PersonID
				AND PP.PermissionableLineage IN (@EntityTypeCode + '.List', @EntityTypeCode + '.View')
			)
		SET @nCanHaveListView = 1
	ELSE IF EXISTS 
		(
		SELECT 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.PersonID = @PersonID
		)
		SET @nCanHaveListView = 1
	--ENDIF
	
	RETURN @nCanHaveListView
END
GO
--End function workflow.CanHaveListView

--Begin function workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowData
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepCount INT,
	CanRejectAfterFinalApproval BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(SELECT MAX(WS2.WorkflowStepNumber) FROM workflow.Workflow W2 JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID AND W2.IsActive = 1 AND W2.EntityTypeCode = W1.EntityTypeCode) AS WorkflowStepCount,
			ET.CanRejectAfterFinalApproval
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.IsActive = 1
				AND W1.EntityTypeCode = @EntityTypeCode
				AND WS1.WorkflowStepNumber = 1
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = W1.EntityTypeCode
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
			BEGIN
			
			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT
				'Approved',
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM dbo.EntityType ET 
			WHERE ET.EntityTypeCode = @EntityTypeCode
			
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData

--Begin function workflow.GetEntityWorkflowPeople
EXEC utility.DropObject 'workflow.GetEntityWorkflowPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return people assigned to an entityy's current workflow step
-- =======================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowPeople
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@WorkflowStepNumber INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepGroupName VARCHAR(250),
	PersonID INT,
	FullName VARCHAR(250),
	EmailAddress VARCHAR(320),
	IsComplete BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepGroupName, PersonID, FullName, EmailAddress, IsComplete)
		SELECT
			WSG.WorkflowStepGroupName,
			WSGP.PersonID,
			dbo.FormatPersonnameByPersonID(WSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			0 AS IsComplete
		FROM workflow.Workflow W
			JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
			JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
				AND W.IsActive = 1
				AND W.EntityTypeCode = @EntityTypeCode
				AND WS.WorkflowStepNumber = 1
			JOIN person.Person P ON P.PersonID = WSGP.PersonID
		ORDER BY 1, 2
	
		END
	ELSE
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepGroupName, PersonID, FullName, EmailAddress, IsComplete)
		SELECT
			EWSGP.WorkflowStepGroupName, 
			EWSGP.PersonID,
			dbo.FormatPersonnameByPersonID(EWSGP.PersonID, 'LastFirst') AS FullName,
			P.EmailAddress,
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
			JOIN person.Person P ON P.PersonID = EWSGP.PersonID
				AND EWSGP.EntityTypeCode = @EntityTypeCode
				AND EWSGP.EntityID = @EntityID
				AND EWSGP.WorkflowStepNumber = @WorkflowStepNumber
		ORDER BY 1, 2
	
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData

--Begin function workflow.GetWorkflowStepCount
EXEC utility.DropObject 'workflow.GetWorkflowStepCount'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to get a workflow step count for a specific EntityTypeCode and EntityID
-- ===============================================================================================

CREATE FUNCTION workflow.GetWorkflowStepCount
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepCount INT = ISNULL((SELECT MAX(EWSGP.WorkflowStepNumber) FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID), 0)
	
	RETURN @nWorkflowStepCount
END
GO
--End function workflow.GetWorkflowStepCount

--Begin function workflow.GetWorkflowStepNumber
EXEC utility.DropObject 'workflow.GetWorkflowStepNumber'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to get a workflow step number for a specific EntityTypeCode and EntityID
-- ================================================================================================

CREATE FUNCTION workflow.GetWorkflowStepNumber
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = (SELECT TOP 1 EWSGP.WorkflowStepNumber FROM workflow.EntityWorkflowStepGroupPerson EWSGP WHERE EWSGP.EntityTypeCode = @EntityTypeCode AND EWSGP.EntityID = @EntityID AND IsComplete = 0 ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID)

	IF @nWorkflowStepNumber IS NULL
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) + 1
	--ENDIF
	
	RETURN @nWorkflowStepNumber
END
GO
--End function workflow.GetWorkflowStepNumber
