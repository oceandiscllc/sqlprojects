USE AJACS
GO

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
--
-- Author:			Todd Pires
-- Modify date: 2016.02.24
-- Description:	Added ICS compatibility
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTemp VARCHAR(250)

	IF LEFT(@TableName, 3) = 'IX_'
		BEGIN
		
		SET @cTemp = @TableName
		SET @TableName = @IndexName
		SET @IndexName = @cTemp
		
		END
	--ENDIF
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create date: 2010.12.09
-- Description:	A helper stored procedure for table upgrades
--
-- Author:			Todd Pires
-- Modify date: 2013.10.10
-- Description:	Added INCLUDE support
--
-- Author:			Todd Pires
-- Modify date: 2016.02.24
-- Description:	Added ICS compatibility
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered
	@IndexName VARCHAR(250),
	@TableName VARCHAR(250),
	@Columns VARCHAR(MAX),
	@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @cTemp VARCHAR(250)

	IF LEFT(@TableName, 3) = 'IX_'
		BEGIN
		
		SET @cTemp = @TableName
		SET @TableName = @IndexName
		SET @IndexName = @cTemp
		
		END
	--ENDIF
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered