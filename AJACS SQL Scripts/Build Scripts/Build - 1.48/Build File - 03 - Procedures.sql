USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
--
-- Author:			Greg Yingling
-- Create date:	2016.02.22
-- Description:	Added EmployerType fields
-- ===================================================================
CREATE PROCEDURE [dbo].[GetContactByContactID]

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.CommunityAssetID,		
		C1.CommunityAssetUnitID,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,		
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,		
		C1.CommunityID,		
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,
		C1.EmployerTypeID,
		(SELECT E.EmployerTypeName FROM dropdown.EmployerType E WHERE E.EmployerTypeID = C1.EmployerTypeID) AS EmployerTypeName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate(
			(SELECT CreateDateTime 
			FROM eventlog.EventLog 
			WHERE eventcode = 'create'
  			AND entitytypecode = 'contact'
  			AND entityid = C1.ContactID)
		)  AS InitialEntryDateFormatted,
		C1.IsActive,		
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.ProvinceID,		
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(
			SELECT CommunitySubGroupName
			FROM dropdown.CommunitySubGroup
			WHERE CommunitySubGroupID = 
				(
				SELECT CommunitySubGroupID 
				FROM dbo.Community
				WHERE CommunityID = C1.CommunityID
				)
		) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.USVettingDate) AS USVettingDateFormatted,
		dbo.FormatDate(CNC.UKVettingDate) AS UKVettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO1.HexColor, '#', '') + '-vetting.png" /> ' AS USVettingIcon,
		'<img src="/assets/img/icons/' + REPLACE(VO2.HexColor, '#', '') + '-vetting.png" /> ' AS UKVettingIcon,
		VO1.VettingOutcomeName AS USVettingOutcomeName,
		VO2.VettingOutcomeName AS UKVettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CNC.USVettingOutcomeID
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CNC.UKVettingOutcomeID
			AND CNC.ContactID = @ContactID
	ORDER BY CNC.USVettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dropdown.GetEmployerTypeData
EXEC utility.DropObject 'dropdown.GetEmployerTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:		Greg Yingling
-- Create date:	2016.02.22
-- Description:	A stored procedure to return data from the dropdown.EmployerType table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetEmployerTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.EmployerTypeID, 
		T.EmployerTypeName
	FROM dropdown.EmployerType T
	WHERE (T.EmployerTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.EmployerTypeName, T.EmployerTypeID

END
GO
--End procedure dropdown.GetEmployerTypeData

--Begin procedure eventlog.LogEquipmentDistributionAction
EXEC utility.DropObject 'eventlog.LogEquipmentDistributionAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.10.30
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogEquipmentDistributionAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'EquipmentDistribution',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cEquipmentDistributionEquipmentInventory VARCHAR(MAX) 
	
		SELECT 
			@cEquipmentDistributionEquipmentInventory = COALESCE(@cEquipmentDistributionEquipmentInventory, '') + D.EquipmentDistributionEquipmentInventory 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('EquipmentDistributionEquipmentInventory'), ELEMENTS) AS EquipmentDistributionEquipmentInventory
			FROM procurement.EquipmentDistributionEquipmentInventory T 
			WHERE T.EquipmentDistributionID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'EquipmentDistribution',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<EquipmentDistributionEquipmentInventory>' + ISNULL(@cEquipmentDistributionEquipmentInventory, '') + '</EquipmentDistributionEquipmentInventory>') AS XML)
			FOR XML RAW('EquipmentDistribution'), ELEMENTS
			)
		FROM procurement.EquipmentDistribution T
		WHERE T.EquipmentDistributionID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogEquipmentDistributionAction

--Begin procedure eventlog.LogJusticeAction
EXEC Utility.DropObject 'eventlog.LogJusticeUpdateAction'
EXEC Utility.DropObject 'eventlog.LogJusticeAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add data to the event log
-- ============================================================
CREATE PROCEDURE eventlog.LogJusticeAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'JusticeUpdate',
			T.JusticeUpdateID,
			@Comments
		FROM justiceupdate.JusticeUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.JusticeUpdateID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'JusticeUpdate',
			T.JusticeUpdateID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityTrainingXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceTrainingXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByJusticeUpdateID(T.JusticeUpdateID) AS XML))
			FOR XML RAW('JusticeUpdate'), ELEMENTS
			)
		FROM justiceupdate.JusticeUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.JusticeUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogJusticeAction

--Begin procedure eventlog.LogWorkflowAction
EXEC utility.DropObject 'eventlog.LogWorkflowAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.10.30
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogWorkflowAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Workflow',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cWorkflowSteps VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowSteps = COALESCE(@cWorkflowSteps, '') + D.WorkflowStep 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStep'), ELEMENTS) AS WorkflowStep
			FROM Workflow.WorkflowStep T 
			WHERE T.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroups VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroups = COALESCE(@cWorkflowStepGroups, '') + D.WorkflowStepGroup 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroup'), ELEMENTS) AS WorkflowStepGroup
			FROM Workflow.WorkflowStepGroup T
				JOIN Workflow.WorkflowStep WS ON WS.WorkflowStepID = T.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		DECLARE @cWorkflowStepGroupPersons VARCHAR(MAX) 
	
		SELECT 
			@cWorkflowStepGroupPersons = COALESCE(@cWorkflowStepGroupPersons, '') + D.WorkflowStepGroupPerson 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('WorkflowStepGroupPerson'), ELEMENTS) AS WorkflowStepGroupPerson
			FROM Workflow.WorkflowStepGroupPerson T 
				JOIN Workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = T.WorkflowStepGroupID
				JOIN Workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
					AND WS.WorkflowID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Workflow',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<WorkflowSteps>' + ISNULL(@cWorkflowSteps, '') + '</WorkflowSteps>') AS XML),
			CAST(('<WorkflowStepGroups>' + ISNULL(@cWorkflowStepGroups, '') + '</WorkflowStepGroups>') AS XML),
			CAST(('<WorkflowStepGroupPersons>' + ISNULL(@cWorkflowStepGroupPersons, '') + '</WorkflowStepGroupPersons>') AS XML)
			FOR XML RAW('Workflow'), ELEMENTS
			)
		FROM Workflow.Workflow T
		WHERE T.WorkflowID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogWorkflowAction

--Begin procedure fifupdate.AddFIFCommunity
EXEC Utility.DropObject 'fifupdate.AddFIFCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Community table
-- ============================================================================
CREATE PROCEDURE fifupdate.AddFIFCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)
	
	INSERT INTO fifupdate.Community
		(CommunityID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@FIFUpdateID,
		C1.FIFCommunityEngagement, 
		C1.FIFJustice, 
		C1.FIFPoliceEngagement, 
		C1.FIFUpdateNotes, 
		C1.IndicatorUpdate, 
		C1.MeetingNotes, 
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO fifupdate.CommunityIndicator
		(FIFUpdateID, CommunityID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.FIFAchievedValue, 0),
		OACI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.FIFAchievedValue,
				CI.FIFNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	;
	WITH CRD AS
		(
		SELECT DISTINCT 
			RR.RiskID,
			RC.CommunityID
		FROM recommendation.RecommendationCommunity RC 
			JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC.RecommendationID
			JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
		)

	INSERT INTO fifupdate.CommunityRisk
		(FIFUpdateID, CommunityID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CR.CommunityID, 
		CR.RiskID,
		ISNULL(CR.FIFRiskValue, 0),
		CR.FIFNotes
	FROM dbo.CommunityRisk CR
		JOIN CRD ON CRD.CommunityID = CR.CommunityID
			AND CRD.RiskID = CR.RiskID

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFCommunity

--Begin procedure fifupdate.AddFIFProvince
EXEC Utility.DropObject 'fifupdate.AddFIFProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to add data to the fifupdate.Province table
-- ===========================================================================
CREATE PROCEDURE fifupdate.AddFIFProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FIFUpdateID INT = (SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC)

	INSERT INTO fifupdate.Province
		(ProvinceID, FIFUpdateID, FIFCommunityEngagement, FIFJustice, FIFPoliceEngagement, FIFUpdateNotes, IndicatorUpdate, MeetingNotes, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@FIFUpdateID,
		P1.FIFCommunityEngagement, 
		P1.FIFJustice, 
		P1.FIFPoliceEngagement, 
		P1.FIFUpdateNotes, 
		P1.IndicatorUpdate, 
		P1.MeetingNotes,
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM fifupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO fifupdate.ProvinceIndicator
		(FIFUpdateID, ProvinceID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.FIFAchievedValue, 0),
		OAPI.FIFNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.FIFAchievedValue,
				PRI.FIFNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI

	;
	WITH PRD AS
		(
		SELECT DISTINCT 
			RR.RiskID,
			RP.ProvinceID
		FROM recommendation.RecommendationProvince RP 
			JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
			JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
		)

	INSERT INTO fifupdate.ProvinceRisk
		(FIFUpdateID, ProvinceID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		@FIFUpdateID,
		PR.ProvinceID, 
		PR.RiskID,
		ISNULL(PR.FIFRiskValue, 0),
		PR.FIFNotes
	FROM dbo.ProvinceRisk PR
		JOIN PRD ON PRD.ProvinceID = PR.ProvinceID
			AND PRD.RiskID = PR.RiskID

	EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure fifupdate.AddFIFProvince

--Begin procedure justiceupdate.AddJusticeCommunity
EXEC utility.DropObject 'justiceupdate.AddJusticeCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add a community for a justice update batch
-- =============================================================================
CREATE PROCEDURE justiceupdate.AddJusticeCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)
	
	INSERT INTO justiceupdate.Community
		(CommunityID, JusticeUpdateID, JusticeAssessmentNotes, JusticeCommunityDocumentCenterStatusNotes, JusticeTrainingNotes, JusticeCommunityDocumentCenterStatusID, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@JusticeUpdateID,
		C1.JusticeAssessmentNotes, 
		C1.JusticeCommunityDocumentCenterStatusNotes, 
		C1.JusticeTrainingNotes, 
		C1.JusticeCommunityDocumentCenterStatusID, 
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM justiceupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO justiceupdate.CommunityIndicator
		(JusticeUpdateID, CommunityID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.JusticeAchievedValue, 0),
		OACI.JusticeNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'JO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.JusticeAchievedValue,
				CI.JusticeNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO justiceupdate.CommunityRisk
		(JusticeUpdateID, CommunityID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		D.CommunityID,
		R.RiskID,
		ISNULL(OACR.JusticeRiskValue, 0),
		OACR.JusticeNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RC.CommunityID
			FROM recommendation.RecommendationCommunity RC 
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC.RecommendationID
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				CR.JusticeRiskValue, 
				CR.JusticeNotes
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = D.CommunityID
			) OACR

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.AddJusticeCommunity

--Begin procedure justiceupdate.AddJusticeProvince
EXEC utility.DropObject 'justiceupdate.AddJusticeProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to add a province for a justice update batch
-- ============================================================================
CREATE PROCEDURE justiceupdate.AddJusticeProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)

	INSERT INTO justiceupdate.Province
		(ProvinceID, JusticeUpdateID, JusticeAssessmentNotes, JusticeCommunityDocumentCenterStatusNotes, JusticeTrainingNotes, JusticeCommunityDocumentCenterStatusID, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@JusticeUpdateID,
		P1.JusticeAssessmentNotes, 
		P1.JusticeCommunityDocumentCenterStatusNotes, 
		P1.JusticeTrainingNotes, 
		P1.JusticeCommunityDocumentCenterStatusID, 
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM justiceupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO justiceupdate.ProvinceIndicator
		(JusticeUpdateID, ProvinceID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.JusticeAchievedValue, 0),
		OAPI.JusticeNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'JO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.JusticeAchievedValue,
				PRI.JusticeNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI
	
	INSERT INTO justiceupdate.ProvinceRisk
		(JusticeUpdateID, ProvinceID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		@JusticeUpdateID,
		D.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.JusticeRiskValue, 0),
		OAPR.JusticeNotes
	FROM dbo.Risk R
		JOIN 
			(
			SELECT DISTINCT 
				RR.RiskID,
				RP.ProvinceID
			FROM recommendation.RecommendationProvince RP
				JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
			) D ON D.RiskID = R.RiskID
		OUTER APPLY
			(
			SELECT
				PR.JusticeRiskValue, 
				PR.JusticeNotes
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = D.ProvinceID
			) OAPR

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.AddJusticeProvince

--Begin procedure justiceupdate.ApproveJusticeUpdate
EXEC utility.DropObject 'justiceupdate.ApproveJusticeUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to approve a justice update batch
-- =================================================================
CREATE PROCEDURE justiceupdate.ApproveJusticeUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nJusticeUpdateID INT = ISNULL((SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogJusticeAction @nJusticeUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogJusticeAction @nJusticeUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.JusticeAssessmentNotes = FU.JusticeAssessmentNotes,
		P.JusticeCommunityDocumentCenterStatusNotes = FU.JusticeCommunityDocumentCenterStatusNotes,
		P.JusticeTrainingNotes = FU.JusticeTrainingNotes,
		P.JusticeCommunityDocumentCenterStatusID = FU.JusticeCommunityDocumentCenterStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN justiceupdate.Province FU ON FU.ProvinceID = P.ProvinceID
			AND FU.JusticeUpdateID = @nJusticeUpdateID

	-- Province Indicator
	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.JusticeAchievedValue, 
		PRI.JusticeNotes
	FROM justiceupdate.ProvinceIndicator PRI

	-- Province Risk
	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.JusticeRiskValue, 
		PR.JusticeNotes
	FROM justiceupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	--Province Documents
	UPDATE DE
	SET DE.EntityTypeCode = 'Province'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'JusticeUpdateProvince'
			AND D.DocumentDescription IN ('Justice Assessment')

	UPDATE D
	SET D.DocumentTypeID = (SELECT TOP 1 DocumentTypeID FROM dropdown.DocumentType WHERE DocumentTypeName like '%justice')
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('Justice Assessment')

	UPDATE C
	SET
		C.JusticeAssessmentNotes = FU.JusticeAssessmentNotes,
		C.JusticeCommunityDocumentCenterStatusNotes = FU.JusticeCommunityDocumentCenterStatusNotes,
		C.JusticeTrainingNotes = FU.JusticeTrainingNotes,
		C.JusticeCommunityDocumentCenterStatusID = FU.JusticeCommunityDocumentCenterStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN justiceupdate.Community FU ON FU.CommunityID = C.CommunityID
			AND FU.JusticeUpdateID = @nJusticeUpdateID

	-- Community Indicator
	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, JusticeAchievedValue, JusticeNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.JusticeAchievedValue, 
		CI.JusticeNotes
	FROM justiceupdate.CommunityIndicator CI

	-- Community Risk
	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, JusticeRiskValue, JusticeNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.JusticeRiskValue, 
		CR.JusticeNotes
	FROM justiceupdate.CommunityRisk CR

	-- Province Training
	DELETE PRM
	FROM dbo.ProvinceTraining PRM
		JOIN @tOutputProvince O ON O.ProvinceID = PRM.ProvinceID
	
	INSERT INTO dbo.ProvinceTraining
		(ProvinceID, TrainingDate, TrainingTitle, AttendeeCount)
	SELECT
		PRM.ProvinceID,
		PRM.TrainingDate, 
		PRM.TrainingTitle,
		PRM.AttendeeCount
	FROM justiceupdate.ProvinceTraining PRM

	-- Community Training
	DELETE CM
	FROM dbo.CommunityTraining CM
		JOIN @tOutputCommunity O ON O.CommunityID = CM.CommunityID
	
	INSERT INTO dbo.CommunityTraining
		(CommunityID, TrainingDate, TrainingTitle, AttendeeCount)
	SELECT
		CM.CommunityID,
		CM.TrainingDate,
		CM.TrainingTitle, 
		CM.AttendeeCount
	FROM justiceupdate.CommunityTraining CM	

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Community'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'JusticeUpdateCommunity'
			AND D.DocumentDescription IN ('Justice Assessment')	
	UPDATE D
	SET D.DocumentTypeID = (SELECT TOP 1 DocumentTypeID FROM dropdown.DocumentType WHERE DocumentTypeName like '%justice')
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('Justice Assessment')	
	
	DELETE FROM justiceupdate.JusticeUpdate

	TRUNCATE TABLE justiceupdate.Province
	TRUNCATE TABLE justiceupdate.ProvinceIndicator
	TRUNCATE TABLE justiceupdate.ProvinceTraining
	TRUNCATE TABLE justiceupdate.ProvinceRisk

	TRUNCATE TABLE justiceupdate.Community
	TRUNCATE TABLE justiceupdate.CommunityIndicator
	TRUNCATE TABLE justiceupdate.CommunityTraining
	TRUNCATE TABLE justiceupdate.CommunityRisk

END
GO
--End procedure justiceupdate.ApproveJusticeUpdate

--Begin procedure justiceupdate.DeleteJusticeCommunity
EXEC utility.DropObject 'justiceupdate.DeleteJusticeCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to delete a community from a justice update batch
-- =================================================================================
CREATE PROCEDURE justiceupdate.DeleteJusticeCommunity

@CommunityID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)

	DELETE T
	FROM justiceupdate.Community T
	WHERE T.CommunityID = @CommunityID
	
	DELETE T
	FROM justiceupdate.CommunityIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)
	
	DELETE T
	FROM justiceupdate.CommunityRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)

	DELETE T
	FROM justiceupdate.CommunityTraining T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Community C
		WHERE C.CommunityID = T.CommunityID
		)

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.DeleteJusticeCommunity

--Begin procedure justiceupdate.DeleteJusticeProvince
EXEC utility.DropObject 'justiceupdate.DeleteJusticeProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to delete a province from a justice update batch
-- ================================================================================
CREATE PROCEDURE justiceupdate.DeleteJusticeProvince

@ProvinceID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @JusticeUpdateID INT = (SELECT TOP 1 FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU ORDER BY FU.JusticeUpdateID DESC)
	
	DELETE T
	FROM justiceupdate.Province T
	WHERE T.ProvinceID = @ProvinceID
	
	DELETE T
	FROM justiceupdate.ProvinceIndicator T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)
	
	DELETE T
	FROM justiceupdate.ProvinceRisk T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)

	DELETE T
	FROM justiceupdate.ProvinceTraining T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM justiceupdate.Province P
		WHERE P.ProvinceID = T.ProvinceID
		)

	EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure justiceupdate.DeleteJusticeProvince

--Begin procedure justiceupdate.GetCommunityByCommunityID
EXEC utility.DropObject 'justiceupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get a community for a justice update batch
-- =============================================================================
CREATE PROCEDURE justiceupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.JusticeAssessmentNotes,
		C.JusticeCommunityDocumentCenterStatusNotes,
		C.JusticeTrainingNotes,
		C.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM dbo.Community C
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = C.JusticeCommunityDocumentCenterStatusID
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CommunityID,
		C1.CommunityName AS EntityName,
		C2.JusticeAssessmentNotes,
		C2.JusticeCommunityDocumentCenterStatusNotes,
		C2.JusticeTrainingNotes,
		C2.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM justiceupdate.Community C2
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = C2.JusticeCommunityDocumentCenterStatusID
		JOIN dbo.Community C1 ON C1.CommunityID = C2.CommunityID
			AND C2.CommunityID = @CommunityID

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.JusticeAchievedValue,
		OACI.JusticeNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.JusticeAchievedValue, 
				CI.JusticeNotes
			FROM justiceupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.JusticeRiskValue,
		OACR.JusticeNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.JusticeRiskValue, 
				CR.JusticeNotes
			FROM justiceupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingsCurrent
	SELECT
		CM.AttendeeCount, 
		CM.TrainingDate, 
		dbo.FormatDate(CM.TrainingDate) AS TrainingDateFormatted,
		CM.CommunityTrainingID AS TrainingID, 
		CM.TrainingTitle
	FROM dbo.CommunityTraining CM
	WHERE CM.CommunityID = @CommunityID

	--EntityTrainingsUpdate
	SELECT
		CM.AttendeeCount, 
		CM.TrainingDate, 
		dbo.FormatDate(CM.TrainingDate) AS TrainingDateFormatted,
		CM.CommunityTrainingID AS TrainingID, 
		CM.TrainingTitle
	FROM justiceupdate.CommunityTraining CM
	WHERE CM.CommunityID = @CommunityID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'JusticeUpdateCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

END
GO
--End procedure justiceupdate.GetCommunityByCommunityID

--Begin procedure justiceupdate.GetProvinceByProvinceID
EXEC utility.DropObject 'justiceupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get a province for a justice update batch
-- ============================================================================
CREATE PROCEDURE justiceupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.ProvinceID,
		P.ProvinceName AS EntityName,
		P.JusticeAssessmentNotes,
		P.JusticeCommunityDocumentCenterStatusNotes,
		P.JusticeTrainingNotes,
		P.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM dbo.Province P
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = P.JusticeCommunityDocumentCenterStatusID
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.ProvinceID,
		P1.ProvinceName AS EntityName,
		P2.JusticeAssessmentNotes,
		P2.JusticeCommunityDocumentCenterStatusNotes,
		P2.JusticeTrainingNotes,
		P2.JusticeCommunityDocumentCenterStatusID,
		CS.CommunityDocumentCenterStatusName AS JusticeCommunityDocumentCenterStatusName
	FROM justiceupdate.Province P2
		JOIN dropdown.CommunityDocumentCenterStatus CS ON CS.CommunityDocumentCenterStatusID = P2.JusticeCommunityDocumentCenterStatusID
		JOIN dbo.Province P1 ON P1.ProvinceID = P2.ProvinceID
			AND P2.ProvinceID = @ProvinceID

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PI.ProvinceIndicatorID
			FROM dbo.ProvinceIndicator PI 
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OAPI.JusticeAchievedValue,
		OAPI.JusticeNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'Justice%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PI.JusticeAchievedValue, 
				PI.JusticeNotes
			FROM justiceupdate.ProvinceIndicator PI
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.JusticeRiskValue,
		OAPR.JusticeNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.JusticeRiskValue, 
				PR.JusticeNotes
			FROM justiceupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityTrainingsCurrent
	SELECT
		PM.AttendeeCount, 
		PM.TrainingDate, 
		dbo.FormatDate(PM.TrainingDate) AS TrainingDateFormatted,
		PM.ProvinceTrainingID AS TrainingID, 
		PM.TrainingTitle
	FROM dbo.ProvinceTraining PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityTrainingsUpdate
	SELECT
		PM.AttendeeCount, 
		PM.TrainingDate, 
		dbo.FormatDate(PM.TrainingDate) AS TrainingDateFormatted,
		PM.ProvinceTrainingID AS TrainingID, 
		PM.TrainingTitle
	FROM justiceupdate.ProvinceTraining PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'JusticeUpdateProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('Justice Assessment')
	ORDER BY D.DocumentDescription

END
GO
--End procedure justiceupdate.GetProvinceByProvinceID

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to data from the dbo.SpotReport table
-- =====================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.SummaryMap,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName,
		dbo.GetSpotReportProvincesList(SR.SpotreportID) as ProvinceList,
		dbo.GetSpotReportCommunitiesList(SR.SpotreportID) as CommunityList,
		dbo.FormatStaticGoogleMapForSpotReport(SR.SpotReportID) AS GMap		
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.LicenseEquipmentCatalog table
-- ==========================================================================================
CREATE PROCEDURE procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID

@LicenseEquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EC.EquipmentCatalogID,
		EC.ItemName,
		L.LicenseID,
		L.LicenseNumber,
		LEC.BudgetLimit,
		LEC.ECCN,
		LEC.LicenseEquipmentCatalogID,
		LEC.LicenseItemDescription,
		LEC.ReferenceCode,
		LEC.QuantityAuthorized,
		LEC.QuantityDistributed,
		LEC.QuantityObligated,
		LEC.QuantityPending,
		LEC.QuantityPurchased,
		LEC.ItemPageNumber,
		LEC.ItemLineNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('LicensedEquipmentCatalog') AS EntityTypeName
	FROM procurement.LicenseEquipmentCatalog LEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = LEC.EquipmentCatalogID
		JOIN procurement.License L ON L.LicenseID = LEC.LicenseID
			AND LEC.LicenseEquipmentCatalogID = @LicenseEquipmentCatalogID
		
END
GO
--End procedure procurement.GetLicenseEquipmentCatalogByLicenseEquipmentCatalogID

--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
--
-- Author:			Greg Yingling
-- Update date:	2015.05.26
-- Description:	Added the Audit Outcome, Audit Date, and Audit Evidence Call
--
-- Author:			Todd Pires
-- Update date:	2015.11.29
-- Description:	Refactored the audit recordset
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.ExpirationDate,
		dbo.FormatDate(EI.ExpirationDate) AS ExpirationDateFormatted,
		EI.IMEIMACAddress,
		EI.UnitCost,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.EquipmentStatusID,
		ES.EquipmentStatusName,
		EI.EquipmentRemovalReasonID,
		ER.EquipmentRemovalReasonName,
		EI.EquipmentRemovalDate,
		EI.EquipmentOrderDate,
		dbo.FormatDate(EI.EquipmentOrderDate) AS EquipmentOrderDateFormatted,
		EI.EquipmentDeliveredToImplementerDate,
		dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted,
		EI.InServiceDate,
		dbo.FormatDate(EI.InServiceDate) AS InServiceDateFormatted,
		EI.LicenseKey,
		EI.EquipmentUsageMinutes,
		EI.EquipmentUsageGB,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		D.Territory,
		SUM(D.Quantity) AS Quantity,
		dbo.FormatConceptNoteTitle(D.ConceptNoteID) AS ConceptNote,
		dbo.FormatDate(D.DistributionDate) AS DistributionDateFormatted,
		IIF(D.IsLost = 1, 'Yes ', 'No ') AS IsLost
	FROM
		(
		SELECT
			C.CommunityName + ' (Community)' AS Territory,
			CEI.Quantity,
			CEI.ConceptNoteID,
			CEI.DistributionDate,
			CEI.IsLost
		FROM procurement.CommunityEquipmentInventory CEI
			JOIN dbo.Community C ON C.CommunityID = CEI.CommunityID
				AND CEI.EquipmentInventoryID = @EquipmentInventoryID
	
		UNION
	
		SELECT
			P.ProvinceName + ' (Province)' AS Territory,
			PEI.Quantity,
			PEI.ConceptNoteID,
			PEI.DistributionDate,
			PEI.IsLost
		FROM procurement.ProvinceEquipmentInventory PEI
			JOIN dbo.Province P ON P.ProvinceID = PEI.ProvinceID
				AND PEI.EquipmentInventoryID = @EquipmentInventoryID
		) D
	GROUP BY D.Territory, D.ConceptNoteID, D.DistributionDate, D.IsLost
	ORDER BY D.Territory

	SELECT
		CEIA.AuditQuantity, 
		CEIA.AuditDate,
		dbo.FormatDate(CEIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(CEIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = CEIA.DocumentID)
			THEN '<a class="btn btn-info" href="/servefile/getFile/GUID/' + (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = CEIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN CEIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(''Community'', ' + CAST(CEIA.CommunityEquipmentInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.CommunityEquipmentInventoryAudit CEIA
		JOIN procurement.CommunityEquipmentInventory CEI ON CEI.CommunityEquipmentInventoryID = CEIA.CommunityEquipmentInventoryID
			AND CEI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = CEIA.AuditOutcomeID
	
	UNION
	
	SELECT
		PEIA.AuditQuantity, 
		PEIA.AuditDate,
		dbo.FormatDate(PEIA.AuditDate) AS AuditDateFormatted, 
		dbo.FormatPersonNameByPersonID(PEIA.PersonID, 'LastFirst') AS PersonNameFormatted,
		AO.AuditOutcomeID, 
		AO.AuditOutcomeName,
	
		CASE
			WHEN EXISTS (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = PEIA.DocumentID)
			THEN '<a class="btn btn-info" href="/servefile/getFile/GUID/' + (SELECT D.PhysicalFileName FROM dbo.Document D WHERE D.DocumentID = PEIA.DocumentID) + '">Download</a>'
			ELSE ''
		END AS AuditDocument,

		CASE
			WHEN PEIA.AuditNotes IS NOT NULL
			THEN '<a class="btn btn-info" href="javascript:getAuditNotes(''Province'', ' + CAST(PEIA.ProvinceEquipmentInventoryAuditID AS VARCHAR(10)) + ')">View Notes</a>'
			ELSE ''
		END AS AuditNotes
	
	FROM procurement.ProvinceEquipmentInventoryAudit PEIA
		JOIN procurement.ProvinceEquipmentInventory PEI ON PEI.ProvinceEquipmentInventoryID = PEIA.ProvinceEquipmentInventoryID
			AND PEI.EquipmentInventoryID = @EquipmentInventoryID
		JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = PEIA.AuditOutcomeID
	
	ORDER BY 2 DESC

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure workflow.DecrementWorkflow
EXEC Utility.DropObject 'workflow.DecrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.10.28
-- Description:	A stored procedure to Decrement a workflow
-- =======================================================
CREATE PROCEDURE workflow.DecrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
	UPDATE EWSGP
	SET EWSGP.IsComplete = 0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber - 1
	
	SELECT
		@nWorkflowStepNumber AS OldWorkflowStepNumber,
		workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
		workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount

END
GO
--End procedure dbo.DecrementWorkflow

--Begin procedure workflow.GetWorkflowByWorkflowID
EXEC Utility.DropObject 'workflow.GetWorkflowByWorkflowID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.10.24
-- Description:	A stored procedure to get data from the workflow.Workflow table
--
-- Author:			Todd Pires
-- Create date:	2016.01.03
-- Description:	Implemented the ProjectID field
-- ============================================================================
CREATE PROCEDURE workflow.GetWorkflowByWorkflowID

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTableWS TABLE (WorkflowStepID INT NOT NULL PRIMARY KEY, WorkflowStepGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())
	DECLARE @tTableWSG TABLE (WorkflowStepGroupID INT NOT NULL PRIMARY KEY, WorkflowStepGroupGUID UNIQUEIDENTIFIER NOT NULL DEFAULT newID())

	SELECT 
		W.EntityTypeCode,	
		W.IsActive,
		W.WorkflowID,	
		W.WorkflowName
  FROM workflow.Workflow W
	WHERE W.WorkflowID = @WorkflowID

	INSERT INTO @tTableWS 
		(WorkflowStepID) 
	SELECT WS.WorkflowStepID 
	FROM workflow.WorkflowStep WS 
	WHERE WS.WorkflowID = @WorkflowID 
	
	INSERT INTO @tTableWSG 
		(WorkflowStepGroupID) 
	SELECT WSG.WorkflowStepGroupID 
	FROM workflow.WorkflowStepGroup WSG 
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			AND WS.WorkflowID = @WorkflowID

	SELECT 
		TWS.WorkflowStepGUID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		WS.WorkflowStepID
  FROM @tTableWS TWS
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = TWS.WorkflowStepID
	ORDER BY WS.WorkflowStepNumber, WS.WorkflowStepID

	SELECT 
		TWS.WorkflowStepGUID,
		TWS.WorkflowStepID,
		TWSG.WorkflowStepGroupGUID,
		WSG.WorkflowStepGroupName,
		WSG.WorkflowStepGroupID
  FROM @tTableWSG TWSG
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = TWSG.WorkflowStepGroupID
		JOIN @tTableWS TWS ON TWS.WorkflowStepID = WSG.WorkflowStepID
	ORDER BY WSG.WorkflowStepGroupName, WSG.WorkflowStepGroupID

	SELECT 
		TWSG.WorkflowStepGroupGUID,
		TWSG.WorkflowStepGroupID,
		WSGP.PersonID,
		WSGP.WorkflowStepGroupID,
		dbo.FormatPersonNameByPersonID(WSGP.PersonID, 'LastFirst') AS Fullname
	FROM workflow.WorkflowStepGroupPerson WSGP
		JOIN @tTableWSG TWSG ON TWSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID

END
GO
--End procedure workflow.GetWorkflowByWorkflowID

--Begin procedure workflow.IncrementWorkflow
EXEC Utility.DropObject 'workflow.IncrementWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.10.28
-- Description:	A stored procedure to increment a workflow
-- =======================================================
CREATE PROCEDURE workflow.IncrementWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT

AS
BEGIN

	DECLARE @nWorkflowStepNumber INT
	DECLARE @nWorkflowStepGroupID INT
	
	SELECT TOP 1
		@nWorkflowStepNumber = EWSGP.WorkflowStepNumber,
		@nWorkflowStepGroupID = EWSGP.WorkflowStepGroupID	
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = @EntityTypeCode
		AND EWSGP.EntityID = @EntityID
		AND EWSGP.PersonID = @PersonID
		AND EWSGP.IsComplete = 0
		AND EWSGP.WorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	ORDER BY EWSGP.WorkflowStepNumber, EWSGP.WorkflowStepGroupID
	
	IF @nWorkflowStepNumber > 0 AND @nWorkflowStepGroupID > 0
		BEGIN
	
		UPDATE EWSGP
		SET EWSGP.IsComplete = 1
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.EntityTypeCode = @EntityTypeCode
			AND EWSGP.EntityID = @EntityID
			AND EWSGP.IsComplete = 0
			AND EWSGP.WorkflowStepNumber = @nWorkflowStepNumber
			AND EWSGP.WorkflowStepGroupID = @nWorkflowStepGroupID
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	ELSE
		BEGIN
	
		SELECT
			@nWorkflowStepNumber AS OldWorkflowStepNumber,
			0 AS NewWorkflowStepNumber,
			workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount
	
		END
	--ENDIF

END
GO
--End procedure dbo.IncrementWorkflow

--Begin procedure workflow.InitializeEntityWorkflow
EXEC Utility.DropObject 'workflow.InitializeEntityWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.10.27
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE workflow.InitializeEntityWorkflow

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		W.EntityTypeCode,
		@EntityID,
		W.WorkflowID,
		WS.WorkflowStepID,
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.EntityTypeCode = @EntityTypeCode
			AND W.IsActive = 1

END
GO
--End procedure workflow.InitializeEntityWorkflow