USE AJACS
GO

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
--
-- Author:			Todd Pires
-- Update date:	2015.05.28
-- Description:	Added vetting data
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName,
		ISNULL(CNC2.ConceptNoteID, 0) AS ConceptNoteID
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT CNC1.ConceptNoteID 
			FROM dbo.ConceptNoteClass CNC1
			WHERE CNC1.ClassID = @ClassID
			) CNC2
	
	SELECT
		dbo.GetContactLocationByContactID(CO.ContactID) AS ContactLocation,
		CO.ContactID,
		CNC2.USVettingIcon,
		CNC2.USVettingOutcomeName,
		CNC2.UKVettingIcon,
		CNC2.UKVettingOutcomeName,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT 
				'<img src="/assets/img/icons/' + REPLACE(VO1.HexColor, '#', '') + '-vetting.png" /> ' AS USVettingIcon,
				VO1.VettingOutcomeName AS USVettingOutcomeName,
				'<img src="/assets/img/icons/' + REPLACE(VO2.HexColor, '#', '') + '-vetting.png" /> ' AS UKVettingIcon,
				VO2.VettingOutcomeName AS UKVettingOutcomeName
			FROM dbo.ConceptNoteContact CNC1
				JOIN dbo.ConceptNoteClass CNC2 ON CNC2.ClassID = CL.ClassID
				JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CNC1.USVettingOutcomeID
				JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CNC1.UKVettingOutcomeID
					AND CNC1.ContactID = CC.ContactID
					AND CNC1.ConceptNoteID = CNC2.ConceptNoteID
			) CNC2
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'Class'
			AND DE.EntityID = @ClassID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		CNC.USVettingDate,
		CNC.UKVettingDate,
		dbo.FormatDate(CNC.USVettingDate) AS USVettingDateFormatted,
		dbo.FormatDate(CNC.UKVettingDate) AS UKVettingDateFormatted,
		VO1.VettingOutcomeID AS USVettingOutcomeID,
		VO1.VettingOutcomeName AS USVettingOutcomeName,
		VO2.VettingOutcomeID AS UKVettingOutcomeID,
		VO2.VettingOutcomeName AS UKVettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CNC.USVettingOutcomeID
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CNC.UKVettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.CommunityAssetID,		
		C1.CommunityAssetUnitID,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,		
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,		
		C1.CommunityID,		
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		(SELECT C8.ContactCSWGClassificationName FROM dropdown.ContactCSWGClassification C8 WHERE C8.ContactCSWGClassificationID = C1.ContactCSWGClassificationID) AS ContactCSWGClassificationName,		
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		dbo.FormatDate(
			(SELECT CreateDateTime 
			FROM eventlog.EventLog 
			WHERE eventcode = 'create'
  			AND entitytypecode = 'contact'
  			AND entityid = C1.ContactID)
		)  AS InitialEntryDateFormatted,
		C1.IsActive,		
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.ProvinceID,		
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,		
		C1.RetirementRejectionDate,
		dbo.FormatDate(C1.RetirementRejectionDate) AS RetirementRejectionDateFormatted,	
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName,
		(
			SELECT CommunitySubGroupName
			FROM dropdown.CommunitySubGroup
			WHERE CommunitySubGroupID = 
				(
				SELECT CommunitySubGroupID 
				FROM dbo.Community
				WHERE CommunityID = C1.CommunityID
				)
		) AS Wave
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.USVettingDate) AS USVettingDateFormatted,
		dbo.FormatDate(CNC.UKVettingDate) AS UKVettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO1.HexColor, '#', '') + '-vetting.png" /> ' AS USVettingIcon,
		'<img src="/assets/img/icons/' + REPLACE(VO2.HexColor, '#', '') + '-vetting.png" /> ' AS UKVettingIcon,
		VO1.VettingOutcomeName AS USVettingOutcomeName,
		VO2.VettingOutcomeName AS UKVettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CNC.USVettingOutcomeID
		JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CNC.UKVettingOutcomeID
			AND CNC.ContactID = @ContactID
	ORDER BY CNC.USVettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dbo.GetDocumentByDocumentID
EXEC Utility.DropObject 'dbo.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	A stored procedure to get data from the dbo.Document table
--
-- Author:			Todd Pires
-- Create date:	2016.01.25
-- Description:	Implemented the IsForDonorPortal field
-- =======================================================================
CREATE PROCEDURE dbo.GetDocumentByDocumentID

@DocumentID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentDate, 
		dbo.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle, 
		D.DocumentTypeID, 
		D.IsForDonorPortal,
		D.PhysicalFileExtension, 
		D.PhysicalFileName, 
		D.PhysicalFilePath, 
		D.PhysicalFileSize
  FROM dbo.Document D
	WHERE D.DocumentID = @DocumentID

	SELECT 
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.DocumentEntity DE ON DE.EntityID = P.ProvinceID
			AND DE.EntityTypeCode = 'Province'
			AND DE.DocumentID = @DocumentID

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.Community C
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dbo.DocumentEntity DE ON DE.EntityID = C.CommunityID
			AND DE.EntityTypeCode = 'Community'
			AND DE.DocumentID = @DocumentID
	
END
GO
--End procedure dbo.GetDocumentByDocumentID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:		Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), 0) AS ThumbnailLength,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dropdown.GetContactVettingTypeData
EXEC utility.DropObject 'dropdown.GetContactVettingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.24
-- Description:	A stored procedure to return data from the dropdown.ContactVetting table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetContactVettingTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactVettingTypeID, 
		T.ContactVettingTypeName
	FROM dropdown.ContactVettingType T
	WHERE (T.ContactVettingTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactVettingTypeName, T.ContactVettingTypeID

END
GO
--End procedure dropdown.GetContactVettingTypeData

--Begin procedure eventlog.LogCommunityAction
EXEC utility.DropObject 'eventlog.LogCommunityAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:			Todd Pires
-- Create date: 2015.09.25
-- Description:	Modified to support the Geography data type
--
-- Author:			Todd Pires
-- Create date: 2015.11.01
-- Description:	Added recommendations
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytActionTable
		--ENDIF
		
		SELECT *
		INTO #LogCommuniytActionTable
		FROM dbo.Community C
		WHERE C.CommunityID = @EntityID
		
		ALTER TABLE #LogCommuniytActionTable DROP COLUMN Location
			
		DECLARE @cCommunityRecommendation VARCHAR(MAX) 
	
		SELECT 
			@cCommunityRecommendation = COALESCE(@cCommunityRecommendation, '') + D.CommunityRecommendation
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityRecommendation'), ELEMENTS) AS CommunityRecommendation
			FROM recommendation.RecommendationCommunity T 
			WHERE T.CommunityID = @EntityID
			) D	
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Community',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(('<Location>' + CAST(C.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			(SELECT CAST(eventlog.GetCommunityEngagementCriteriaResultXMLByCommunityID(T.CommunityID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByCommunityID(T.CommunityID) AS XML)),
			CAST(('<CommunityRecommendations>' + ISNULL(@cCommunityRecommendation , '') + '</CommunityRecommendations>') AS XML), 
			(SELECT CAST(eventlog.GetCommunityRiskXMLByCommunityID(T.CommunityID) AS XML))
			FOR XML RAW('Community'), ELEMENTS
			)
		FROM #LogCommuniytActionTable T
			JOIN dbo.Community C ON C.CommunityID = T.CommunityID

		DROP TABLE #LogCommuniytActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAction

--Begin procedure eventlog.LogCommunityAssetAction
EXEC utility.DropObject 'eventlog.LogCommunityAssetAction'
GO

-- ==========================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.09.25
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityAssetAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogCommuniytAssetActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogCommuniytAssetActionTable
		--ENDIF
		
		SELECT *
		INTO #LogCommuniytAssetActionTable
		FROM dbo.CommunityAsset CA
		WHERE CA.CommunityAssetID = @EntityID
		
		ALTER TABLE #LogCommuniytAssetActionTable DROP COLUMN Location

		DECLARE @cCommunityAssetRisks NVARCHAR(MAX) 
	
		SELECT 
			@cCommunityAssetRisks = COALESCE(@cCommunityAssetRisks, '') + D.CommunityAssetRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetRisk'), ELEMENTS) AS CommunityAssetRisk
			FROM dbo.CommunityAssetRisk T 
			WHERE T.CommunityAssetID = @EntityID
			) D	

		DECLARE @cCommunityAssetUnits NVARCHAR(MAX)

		SELECT 
			@cCommunityAssetUnits = COALESCE(@cCommunityAssetUnits, '') + D.CommunityAssetUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('CommunityAssetUnit'), ELEMENTS) AS CommunityAssetUnit
			FROM dbo.CommunityAssetUnit T 
			WHERE T.CommunityAssetID = @EntityID
			) D	
			
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityAsset',
			@EntityID,
			@Comments,
			(
			SELECT 
			T.*, 
			CAST(('<Location>' + CAST(CA.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<CommunityAssetRisks>' + ISNULL(@cCommunityAssetRisks  , '') + '</CommunityAssetRisks>') AS XML),
			CAST(('<CommunityAssetUnits>' + ISNULL(@cCommunityAssetUnits  , '') + '</CommunityAssetUnits>') AS XML)
			FOR XML RAW('CommunityAsset'), ELEMENTS
			)
		FROM #LogCommuniytAssetActionTable T
			JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = T.CommunityAssetID

		DROP TABLE #LogCommuniytAssetActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityAssetAction

--Begin procedure eventlog.LogForceAction
EXEC Utility.DropObject 'eventlog.LogForceAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogForceAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Force',
			T.ForceID,
			@Comments
		FROM force.Force T
		WHERE T.ForceID = @EntityID


		END
	ELSE
		BEGIN

		DECLARE @cForceCommunities VARCHAR(MAX) = ''
		
		SELECT @cForceCommunities = COALESCE(@cForceCommunities, '') + D.ForceCommunity
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceCommunity'), ELEMENTS) AS ForceCommunity
			FROM force.ForceCommunity T 
			WHERE T.ForceID = @EntityID
			) D

		DECLARE @cForceEquipmentResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cForceEquipmentResourceProviders = COALESCE(@cForceEquipmentResourceProviders, '') + D.ForceEquipmentResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceEquipmentResourceProvider'), ELEMENTS) AS ForceEquipmentResourceProvider
			FROM force.ForceEquipmentResourceProvider T 
			WHERE T.ForceID = @EntityID
			) D

		DECLARE @cForceFinancialResourceProviders VARCHAR(MAX) = ''
		
		SELECT @cForceFinancialResourceProviders = COALESCE(@cForceFinancialResourceProviders, '') + D.ForceFinancialResourceProvider
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceFinancialResourceProvider'), ELEMENTS) AS ForceFinancialResourceProvider
			FROM force.ForceFinancialResourceProvider T 
			WHERE T.ForceID = @EntityID
			) D

		DECLARE @cForceRisks VARCHAR(MAX) = ''
		
		SELECT @cForceRisks = COALESCE(@cForceRisks, '') + D.ForceRisk
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceRisk'), ELEMENTS) AS ForceRisk
			FROM force.ForceRisk T 
			WHERE T.ForceID = @EntityID
			) D

		DECLARE @cForceUnits VARCHAR(MAX) = ''
		
		SELECT @cForceUnits = COALESCE(@cForceUnits, '') + D.ForceUnit
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('ForceUnit'), ELEMENTS) AS ForceUnit
			FROM force.ForceUnit T 
			WHERE T.ForceID = @EntityID
			) D

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogForceActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogForceActionTable
		--ENDIF
		
		SELECT T.*
		INTO #LogForceActionTable
		FROM force.Force T
		WHERE T.ForceID = @EntityID
		
		ALTER TABLE #LogForceActionTable DROP COLUMN Location
		
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Force',
			@EntityID,
			@Comments,
			(
			SELECT T.*, 
			CAST(('<Location>' + CAST(F.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<ForceCommunities>' + ISNULL(@cForceCommunities, '') + '</ForceCommunities>') AS XML),
			CAST(('<ForceEquipmentResourceProviders>' + ISNULL(@cForceEquipmentResourceProviders, '') + '</ForceEquipmentResourceProviders>') AS XML),
			CAST(('<ForceFinancialResourceProviders>' + ISNULL(@cForceFinancialResourceProviders, '') + '</ForceFinancialResourceProviders>') AS XML),
			CAST(('<ForceRisks>' + ISNULL(@cForceRisks, '') + '</ForceRisks>') AS XML),
			CAST(('<ForceUnits>' + ISNULL(@cForceUnits, '') + '</ForceUnits>') AS XML)
			FOR XML RAW('Force'), ELEMENTS
			)
		FROM #LogForceActionTable T
			JOIN force.Force F ON F.ForceID = T.ForceID

		DROP TABLE #LogForceActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogForceAction

--Begin procedure eventlog.LogIncidentAction
EXEC utility.DropObject 'eventlog.LogIncidentAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.04.18
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:			Todd Pires
-- Create date: 2015.11.01
-- Description:	Modified to support the Geography data type
-- ==========================================================================
CREATE PROCEDURE eventlog.LogIncidentAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		IF (SELECT OBJECT_ID('tempdb.dbo.#LogIncidentActionTable', 'u')) IS NOT NULL
			DROP TABLE #LogIncidentActionTable
		--ENDIF
		
		SELECT *
		INTO #LogIncidentActionTable
		FROM dbo.Incident I
		WHERE I.IncidentID = @EntityID
		
		ALTER TABLE #LogIncidentActionTable DROP COLUMN Location

		DECLARE @cIncidentCommunities VARCHAR(MAX) 
	
		SELECT 
			@cIncidentCommunities = COALESCE(@cIncidentCommunities, '') + D.IncidentCommunity 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentCommunity'), ELEMENTS) AS IncidentCommunity
			FROM dbo.IncidentCommunity T 
			WHERE T.IncidentID = @EntityID
			) D

		DECLARE @cIncidentProvinces VARCHAR(MAX) 
	
		SELECT 
			@cIncidentProvinces = COALESCE(@cIncidentProvinces, '') + D.IncidentProvince 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('IncidentProvince'), ELEMENTS) AS IncidentProvince
			FROM dbo.IncidentProvince T 
			WHERE T.IncidentID = @EntityID
			) D
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Incident',
			@EntityID,
			@Comments,
			(
			SELECT
			T.*, 
			CAST(('<Location>' + CAST(I.Location AS VARCHAR(MAX)) + '</Location>') AS XML),
			CAST(('<IncidentCommunities>' + ISNULL(@cIncidentCommunities, '') + '</IncidentCommunities>') AS XML),
			CAST(('<IncidentProvinces>' + ISNULL(@cIncidentProvinces, '') + '</IncidentProvinces>') AS XML)
			FOR XML RAW('Incident'), ELEMENTS
			)

		FROM #LogIncidentActionTable T
			JOIN dbo.Incident I ON I.IncidentID = T.IncidentID

		DROP TABLE #LogIncidentActionTable

		END
	--ENDIF

END
GO
--End procedure eventlog.LogIncidentAction

--Begin procedure fifupdate.GetFIFUpdate
EXEC Utility.DropObject 'fifupdate.GetFIFUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to get data from the fifupdate.FIFUpdate table
-- ==============================================================================
CREATE PROCEDURE fifupdate.GetFIFUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FIFUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM fifupdate.FIFUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (FIFUpdateID INT)

		INSERT INTO fifupdate.FIFUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.FIFUpdateID INTO @tOutput
		VALUES 
			(1)

		INSERT INTO workflow.EntityWorkflowStep
			(EntityID, WorkflowStepID)
		SELECT
			(SELECT O.FIFUpdateID FROM @tOutput O),
			WS.WorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'

		SELECT @FIFUpdateID = O.FIFUpdateID FROM @tOutput O
		
		EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='create', @PersonID = @PersonID

		END
	ELSE
		SELECT @FIFUpdateID = FU.FIFUpdateID FROM fifupdate.FIFUpdate FU
	--ENDIF
	
	--FIF
	SELECT
		FU.FIFUpdateID, 
		FU.WorkflowStepNumber 
	FROM fifupdate.FIFUpdate FU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
	ORDER BY C2.CommunityName, C1.CommunityID
	
	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	ORDER BY P2.ProvinceName, P1.ProvinceID
		
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'
			JOIN fifupdate.FIFUpdate FU ON FU.WorkflowStepNumber = WS.WorkflowStepNumber
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	--WorkflowStatus
	SELECT
		'FIFUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @FIFUpdateID

	--WorkflowStepWorkflowAction
	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'FIFUpdate'
			AND WSWA.WorkflowStepNumber = (SELECT FU.WorkflowStepNumber FROM fifupdate.FIFUpdate FU WHERE FU.FIFUpdateID = @FIFUpdateID)
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure fifupdate.GetFIFUpdate

--Begin procedure force.GetForceByEventLogID
EXEC Utility.DropObject 'force.GetForceByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.29
-- Description:	A stored procedure to force data from the eventlog.EventLog table
-- Notes:				Changes here must ALSO be made to force.GetForceByForceID
-- ==============================================================================
CREATE PROCEDURE force.GetForceByEventLogID

@EventLogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		dbo.FormatContactNameByContactID(Force.value('CommanderContactID[1]', 'INT'), 'LastFirstMiddle') AS CommanderFullName,
		Force.value('Comments[1]', 'VARCHAR(250)') AS Comments,
		dbo.FormatContactNameByContactID(Force.value('DeputyCommanderContactID[1]', 'INT'), 'LastFirstMiddle') AS DeputyCommanderFullName,
		Force.value('ForceDescription[1]', 'VARCHAR(250)') AS ForceDescription,
		Force.value('ForceID[1]', 'INT') AS ForceID,
		Force.value('ForceName[1]', 'VARCHAR(250)') AS ForceName,
		Force.value('History[1]', 'VARCHAR(250)') AS History,
		Force.value('Location[1]', 'VARCHAR(MAX)') AS Location,
		Force.value('Notes[1]', 'VARCHAR(250)') AS Notes,
		Force.value('TerritoryID[1]', 'INT') AS TerritoryID,
		Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('TerritoryTypeCode[1]', 'VARCHAR(50)'), Force.value('TerritoryID[1]', 'INT')) AS TerritoryName,
		Force.value('WebLinks[1]', 'VARCHAR(250)') AS WebLinks,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		AOO.AreaOfOperationTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force') AS T(Force)
		JOIN dropdown.AreaOfOperationType AOO ON AOO.AreaOfOperationTypeID = Force.value('AreaOfOperationTypeID[1]', 'INT')
			AND EL.EventLogID = @EventLogID

	SELECT
		Force.value('(CommunityID)[1]', 'INT') AS CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceCommunities/ForceCommunity') AS T(Force)
			JOIN dbo.Community C ON C.CommunityID = Force.value('(CommunityID)[1]', 'INT')
			JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
				AND EL.EventLogID = 88491
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceEquipmentResourceProviders/ForceEquipmentResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = 88491
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(ResourceProviderID)[1]', 'INT') AS ResourceProviderID,
		RP.ResourceProviderName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceFinancialResourceProviders/ForceFinancialResourceProvider') AS T(Force)
			JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = Force.value('(ResourceProviderID)[1]', 'INT')
				AND EL.EventLogID = 88491
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		Force.value('(RiskID)[1]', 'INT') AS RiskID,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceRisks/ForceRisk') AS T(Force)
			JOIN dbo.Risk R ON R.RiskID = Force.value('(RiskID)[1]', 'INT')
			JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
				AND EL.EventLogID = 88491
	ORDER BY R.RiskName, R.RiskID

	SELECT
		Force.value('(ForceUnitID)[1]', 'INT') AS ForceUnitID,
		Force.value('(CommanderContactID)[1]', 'INT') AS CommanderContactID,
		Force.value('(DeputyCommanderContactID)[1]', 'INT') AS DeputyCommanderContactID,
		Force.value('(TerritoryID)[1]', 'INT') AS TerritoryID,
		Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)') AS TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(Force.value('(TerritoryTypeCode)[1]', 'VARCHAR(50)'), Force.value('(TerritoryID)[1]', 'INT')) AS TerritoryName,
		Force.value('(UnitName)[1]', 'VARCHAR(250)') AS UnitName,
		Force.value('(UnitTypeID)[1]', 'INT') AS UnitTypeID,
		UT.UnitTypeName
	FROM eventlog.EventLog EL
		CROSS APPLY EL.EventData.nodes('/Force/ForceUnits/ForceUnit') AS T(Force)
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = Force.value('(UnitTypeID)[1]', 'INT')
				AND EL.EventLogID = 88491
	ORDER BY Force.value('(UnitName)[1]', 'VARCHAR(250)'), Force.value('(ForceUnitID)[1]', 'INT')
	
END
GO
--End procedure force.GetForceByEventLogID

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- Notes:				Changes here must ALSO be made to force.GetForceByEventLogID
-- =========================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderContactID,
		dbo.FormatContactNameByContactID(F.CommanderContactID, 'LastFirstMiddle') AS CommanderFullName,
		F.Comments, 
		F.DeputyCommanderContactID, 
		dbo.FormatContactNameByContactID(F.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderContactID, 
		FU.DeputyCommanderContactID,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure force.GetForceLocations
EXEC Utility.DropObject 'force.GetForceLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Kevin Ross
-- Create date:	2016.00.28
-- Description:	A stored procedure to data about force locations
-- =============================================================
CREATE PROCEDURE force.GetForceLocations

@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT
		F.ForceID,
		F.ForceName,
		F.Location.STAsText() AS Location,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor
	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND 
				(
				IsNull(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(F.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR EXISTS
					(
					SELECT 1
					FROM dbo.Community C
						JOIN force.ForceCommunity FC ON FC.CommunityID = C.CommunityID
							AND C.CommunityID = @CommunityID
							AND FC.ForceID = F.ForceID
					)
				)
			AND 
				(
				@ProvinceID = 0
					OR EXISTS
					(
					SELECT 1
					FROM dbo.Community C
						JOIN force.ForceCommunity FC ON FC.CommunityID = C.CommunityID
							AND C.ProvinceID = @ProvinceID
							AND FC.ForceID = F.ForceID
					)
				)

END
GO
--End procedure force.GetForceLocations

--Begin procedure force.GetForceUnitByForceUnitID
EXEC Utility.DropObject 'force.GetForceUnitByForceUnitID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.01
-- Description:	A stored procedure to data from the force.ForceUnit table
-- ======================================================================
CREATE PROCEDURE force.GetForceUnitByForceUnitID

@ForceUnitID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.FormatContactNameByContactID(FU.CommanderContactID, 'LastFirstMiddle') AS CommanderFullName,
		dbo.FormatContactNameByContactID(FU.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullName,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceUnitID = @ForceUnitID
	
END
GO
--End procedure force.GetForceUnitByForceUnitID

--Begin procedure reporting.GetContact
EXEC Utility.DropObject 'reporting.GetContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.20
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.08.19
-- Description:	Added the phone number country calling codes and the country of birth
--
-- Author:			Justin Branum
-- Create date: 2016.01.21
-- Description: Added New columns regrading Previous Unit data for vetting export report.
-- ==================================================================================
CREATE PROCEDURE reporting.GetContact

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		CAST(CCC1.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.CellPhoneNumber AS CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) as DateOfBirthUKFormatted,
		dbo.FormatUSDate(C1.DateOfBirth) AS DateOfBirthUSFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookpageURL,
		CAST(CCC2.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.FaxNumber AS FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		C1.MiddleName,
		C1.PassportNumber,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDate,
		dbo.FormatUSDate(C1.PassportExpirationDate) AS PassportExpirationUSDate,
		CAST(CCC3.CountryCallingCode AS VARCHAR(10)) + ' ' + C1.PhoneNumber AS PhoneNumber,
		C1.PlaceOfBirth + ', ' + ISNULL(C7.CountryName, '') AS PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		dbo.FormatUSDate(C1.StartDate) AS JoinDateUSFormatted,
		C1.State,
		C1.Title,
		OACAU.CommunityAssetUnitName,
		OACAU.TerritoryName,
		C1.PreviousUnit AS PreviousUnitLocation,
		C1.PreviousRankOrTitle,
		C1.DescriptionOfDuties,
		dbo.FormatUSDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateUS,
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDate,
		dbo.FormatUSDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateUS,
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDate,
		C1.PreviousProfession,
		C1.IsRegimeDefector,
		C1.SARGMinistryAndUnit,
		C1.PreviousDuties,
		C1.CommunityAssetID,
		C1.CommunityAssetUnitID,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		OUTER APPLY
			(
			SELECT 
				CAU.CommunityAssetUnitName,
				CASE
					WHEN CA.ProvinceID > 0
					THEN dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Province', CA.ProvinceID)
					WHEN CA.CommunityID > 0
					THEN dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID('Community', CA.CommunityID)
					ELSE ''
				END AS TerritoryName
			FROM dbo.CommunityAssetUnit CAU
				JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = CAU.CommunityAssetID
				AND CA.CommunityAssetID = C1.CommunityAssetID
			) OACAU
		
END
GO
--End procedure reporting.GetContact