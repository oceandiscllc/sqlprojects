USE AJACS
GO

--Begin table dbo.ConceptNoteContact
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNoteContact'

EXEC utility.DropColumn @TableName, 'ContactVettingTypeID'

EXEC utility.AddColumn @TableName, 'UKVettingOutcomeID', 'INT'
EXEC utility.AddColumn @TableName, 'UKVettingDate', 'DATE'

EXEC utility.SetDefaultConstraint @TableName, 'UKVettingOutcomeID', 'INT', 0

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID JOIN sys.Columns C ON C.Object_ID = T.Object_ID AND S.Name = 'dbo' AND T.Name = 'ConceptNoteContact' AND C.Name = 'VettingOutcomeID')
	EXEC sp_rename 'dbo.ConceptNoteContact.VettingOutcomeID', 'USVettingOutcomeID', 'COLUMN'	
--ENDIF

IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID JOIN sys.Columns C ON C.Object_ID = T.Object_ID AND S.Name = 'dbo' AND T.Name = 'ConceptNoteContact' AND C.Name = 'VettingDate')
	EXEC sp_rename 'dbo.ConceptNoteContact.VettingDate', 'USVettingDate', 'COLUMN'	
--ENDIF
GO
--End table dbo.ConceptNoteContact

--Begin table dbo.ContactVetting
DECLARE @TableName VARCHAR(250) = 'dbo.ContactVetting'

EXEC utility.AddColumn @TableName, 'ContactVettingTypeID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ContactVettingTypeID', 'INT', 0
GO

UPDATE dbo.ContactVetting
SET ContactVettingTypeID = 1
WHERE ContactVettingTypeID = 0
GO
--End table dbo.ContactVetting

--Begin table dbo.Document
EXEC utility.AddColumn 'dbo.Document', 'IsForDonorPortal', 'BIT'
GO

EXEC utility.SetDefaultConstraint 'dbo.Document', 'IsForDonorPortal', 'BIT', 0
GO
--End table dbo.Document

--Begin table dropdown.ContactVettingType
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactVettingType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactVettingType
	(
	ContactVettingTypeID INT IDENTITY(0,1) NOT NULL,
	ContactVettingTypeName VARCHAR(250),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactVettingTypeID'
EXEC utility.SetIndexNonClustered 'IX_ContactVettingType', @TableName, 'DisplayOrder,ContactVettingTypeName', 'ContactVettingTypeID'
GO

SET IDENTITY_INSERT dropdown.ContactVettingType ON
GO

INSERT INTO dropdown.ContactVettingType 
	(ContactVettingTypeID, ContactVettingTypeName, DisplayOrder) 
VALUES 
	(0, NULL, 0),
	(1, 'US', 1),
	(2, 'UK', 2)
GO

SET IDENTITY_INSERT dropdown.ContactVettingType OFF
GO
--End table dropdown.ContactVettingType

--Begin table fifupdate.Community
EXEC utility.AddColumn 'fifupdate.Community', 'MOUDate', 'DATE'
GO
--End table fifupdate.Community

--Begin table fifupdate.Province
EXEC utility.AddColumn 'fifupdate.Province', 'MOUDate', 'DATE'
GO
--End table fifupdate.Province

--Begin table force.Force
ALTER TABLE force.Force ALTER COLUMN ForceDescription NVARCHAR(MAX)
GO
--End table force.Force
