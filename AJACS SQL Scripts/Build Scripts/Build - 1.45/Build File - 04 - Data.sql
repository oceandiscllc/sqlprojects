USE AJACS
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='CSWGList', @NewMenuItemLink='/contact/list?ContactTypeCode=CSWGMember&IsVetting=1', @NewMenuItemText='CSWG Vetting', @ParentMenuItemCode='Contacts', @AfterMenuItemCode='StaffPartnerVettingList', @PermissionableLineageList='Contact.List.CSWGMemberVetting'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode 'Contacts'
GO
--End table dbo.MenuItem

--Begin table dropdown.ContactType
IF NOT EXISTS (SELECT 1 FROM dropdown.ContactType CT WHERE CT.ContactTypeName = 'Commander')
	BEGIN
	
	INSERT INTO dropdown.ContactType 
		(ContactTypeName, DisplayOrder, IsActive) 
	VALUES
		('Commander', 1, 1),
		('Deputy', 2, 1)

	END
--ENDIF
GO
--End table dropdown.ContactType

--End table dropdown.AreaOfOperationType
UPDATE dropdown.AreaOfOperationType
SET HexColor = '#377EB8'
WHERE AreaOfOperationTypeID = 1
GO

UPDATE dropdown.AreaOfOperationType
SET HexColor = '#4DAF4A'
WHERE AreaOfOperationTypeID = 2
GO

UPDATE dropdown.AreaOfOperationType
SET HexColor = '#984EA3'
WHERE AreaOfOperationTypeID = 3
GO

UPDATE dropdown.AreaOfOperationType
SET HexColor = '#FF7F00'
WHERE AreaOfOperationTypeID = 4
GO

UPDATE dropdown.AreaOfOperationType
SET HexColor = '#FFFF33'
WHERE AreaOfOperationTypeID = 5
GO

UPDATE dropdown.AreaOfOperationType
SET HexColor = '#A65628'
WHERE AreaOfOperationTypeID = 6
GO

UPDATE dropdown.AreaOfOperationType
SET HexColor = '#E41A1C'
WHERE AreaOfOperationTypeID = 7
GO
--End table dropdown.AreaOfOperationType

--Begin ServerSetup Keys
EXEC utility.ServerSetupKeyAddUpdate 'ShowAssetsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowAtmosphericReportsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowCommunitiesOnCSSFPortalOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowFindingsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowForcesOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowProvincesOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowIncidentReportsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowLogicalFrameworkOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowRecommendationsOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowRequestsForInformationOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowRisksOnCSSFPortal', '1'
EXEC utility.ServerSetupKeyAddUpdate 'ShowSpotReportsOnCSSFPortal', '1'
GO
--End ServerSetup Keys

--Begin table permissionable.Permissionable
INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	P.PermissionableID,
	'CSWGMemberVetting',
	'CSWG Member Vetting'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.List.CSWGMemberVetting'
		)
GO

INSERT INTO permissionable.Permissionable 
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	P.PermissionableID,
	'ExportCSWGVetting',
	'Export CSWG Vetting'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Contact.List.CSWGMemberVetting'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P
		WHERE P.PermissionableLineage = 'Contact.List.CSWGMemberVetting.ExportCSWGVetting'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
