USE AJACS
GO

--Begin procedure eventlog.LogSurveyResponseAction
EXEC utility.DropObject 'eventlog.LogSurveyResponseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.03
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSurveyResponseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SurveyResponse',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSurveySurveyQuestionResponses NVARCHAR(MAX) 
	
		SELECT 
			@cSurveySurveyQuestionResponses = COALESCE(@cSurveySurveyQuestionResponses, '') + D.SurveySurveyQuestionResponses 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveySurveyQuestionResponses'), ELEMENTS) AS SurveySurveyQuestionResponses
			FROM survey.SurveySurveyQuestionResponse T 
			WHERE T.SurveyResponseID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SurveyResponse',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<SurveySurveyQuestionResponses>' + ISNULL(@cSurveySurveyQuestionResponses, '') + '</SurveySurveyQuestionResponses>') AS XML)
			FOR XML RAW('Survey'), ELEMENTS
			)
		FROM survey.SurveyResponse T
		WHERE T.SurveyResponseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSurveyResponseAction

--Begin survey.GetSurveyQuestionResponsesBySurveyResponseID
EXEC Utility.DropObject 'survey.GetSurveyQuestionResponsesBySurveyResponseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to get survey responses from the database
-- =========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionResponsesBySurveyResponseID

@SurveyResponseID INT

AS
BEGIN

	SELECT
		SSQR.SurveySurveyQuestionResponseID,
		SSQR.SurveyResponseID,
		SSQR.SurveyQuestionID,
		SSQR.SurveyQuestionResponse,
		SQT.SurveyQuestionResponseTypeCode,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'DatePicker'
			THEN dbo.FormatDate(SSQR.SurveyQuestionResponse)
			WHEN SQT.SurveyQuestionResponseTypeCode = 'CommunityPicker'
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CAST(SSQR.SurveyQuestionResponse AS INT))
			WHEN SQT.SurveyQuestionResponseTypeCode = 'ProvincePicker'
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CAST(SSQR.SurveyQuestionResponse AS INT))
			WHEN SQT.SurveyQuestionResponseTypeCode IN ('Select','SelectMultiple')
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			WHEN SQT.SurveyQuestionResponseTypeCode = 'YesNo'
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionCode = SQT.SurveyQuestionResponseTypeCode AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE SSQR.SurveyQuestionResponse
		END AS SurveyQuestionResponseFormatted,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'SelectMultiple'
			THEN (SELECT SQRC.DisplayOrder FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE 0
		END AS SurveyQuestionResponseDisplayOrder,
	
		SSQ.DisplayOrder AS SurveyQuestionDisplayOrder
	FROM survey.SurveySurveyQuestionResponse SSQR
		JOIN survey.SurveyResponse SR ON SR.SurveyResponseID = SSQR.SurveyResponseID
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQR.SurveyQuestionID
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyQuestionID = SSQR.SurveyQuestionID
			AND SSQ.SurveyID = SR.SurveyID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQR.SurveyResponseID = @SurveyResponseID
	ORDER BY SurveyQuestionDisplayOrder, SurveyQuestionResponseDisplayOrder, SurveyQuestionResponse
	
END
GO
--End procedure survey.GetSurveyQuestionResponsesBySurveyResponseID

--Begin survey.GetSurveyResponseBySurveyResponseID
EXEC Utility.DropObject 'survey.GetSurveyResponseBySurveyResponseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.03
-- Description:	A stored procedure to get survey a response from the database
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyResponseBySurveyResponseID

@SurveyResponseID INT

AS
BEGIN

	SELECT
		L.LanguageName,
		SR.LanguageCode,
		SR.SurveyID,
		SR.SurveyResponseID,
		dbo.FormatDateTime(SR.ResponseDateTime) AS ResponseDateTimeFormatted,
		dbo.GetPersonNameByPersonID(SR.PersonID, 'LastFirst') AS FullName
	FROM survey.SurveyResponse SR
		JOIN dropdown.Language L ON L.LanguageCode = SR.LanguageCode
			AND SR.SurveyResponseID = @SurveyResponseID
	
END
GO
--End procedure survey.GetSurveyResponseBySurveyResponseID
