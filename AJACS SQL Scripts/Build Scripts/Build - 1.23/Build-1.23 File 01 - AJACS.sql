-- File Name:	Build-1.23 File 01 - AJACS.sql
-- Build Key:	Build-1.23 File 01 - AJACS - 2015.08.03 21.40.50

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.ListToTable
--
-- Procedures:
--		eventlog.LogSurveyResponseAction
--		survey.GetSurveyQuestionResponsesBySurveyResponseID
--		survey.GetSurveyResponseBySurveyResponseID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.ListToTable
EXEC utility.DropObject 'dbo.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
--
-- Author:			Todd Pires
-- Update date:	2012.06.07
-- Description:	Added the ListItemID column
--
-- Author:			Todd Pires
-- Update date:	2014.06.04
-- Description:	Added MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use CTE's, dropped MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use XML - based on a script by Kshitij Satpute from SQLServerCentral.com
--
-- Author:			Todd Pires
-- Update date:	2015.08.02
-- Description:	Converted to NVARCHAR
-- ===================================================================================================

CREATE FUNCTION dbo.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function dbo.ListToTable
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure eventlog.LogSurveyResponseAction
EXEC utility.DropObject 'eventlog.LogSurveyResponseAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.08.03
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogSurveyResponseAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'SurveyResponse',
			@EntityID,
			@Comments
			)

		END
	ELSE
		BEGIN

		DECLARE @cSurveySurveyQuestionResponses NVARCHAR(MAX) 
	
		SELECT 
			@cSurveySurveyQuestionResponses = COALESCE(@cSurveySurveyQuestionResponses, '') + D.SurveySurveyQuestionResponses 
		FROM
			(
			SELECT
				(SELECT T.* FOR XML RAW('SurveySurveyQuestionResponses'), ELEMENTS) AS SurveySurveyQuestionResponses
			FROM survey.SurveySurveyQuestionResponse T 
			WHERE T.SurveyResponseID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'SurveyResponse',
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST(('<SurveySurveyQuestionResponses>' + ISNULL(@cSurveySurveyQuestionResponses, '') + '</SurveySurveyQuestionResponses>') AS XML)
			FOR XML RAW('Survey'), ELEMENTS
			)
		FROM survey.SurveyResponse T
		WHERE T.SurveyResponseID = @EntityID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogSurveyResponseAction

--Begin survey.GetSurveyQuestionResponsesBySurveyResponseID
EXEC Utility.DropObject 'survey.GetSurveyQuestionResponsesBySurveyResponseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.02
-- Description:	A stored procedure to get survey responses from the database
-- =========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionResponsesBySurveyResponseID

@SurveyResponseID INT

AS
BEGIN

	SELECT
		SSQR.SurveySurveyQuestionResponseID,
		SSQR.SurveyResponseID,
		SSQR.SurveyQuestionID,
		SSQR.SurveyQuestionResponse,
		SQT.SurveyQuestionResponseTypeCode,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'DatePicker'
			THEN dbo.FormatDate(SSQR.SurveyQuestionResponse)
			WHEN SQT.SurveyQuestionResponseTypeCode = 'CommunityPicker'
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CAST(SSQR.SurveyQuestionResponse AS INT))
			WHEN SQT.SurveyQuestionResponseTypeCode = 'ProvincePicker'
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CAST(SSQR.SurveyQuestionResponse AS INT))
			WHEN SQT.SurveyQuestionResponseTypeCode IN ('Select','SelectMultiple')
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			WHEN SQT.SurveyQuestionResponseTypeCode = 'YesNo'
			THEN (SELECT SQRC.SurveyQuestionResponseChoiceText FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionCode = SQT.SurveyQuestionResponseTypeCode AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE SSQR.SurveyQuestionResponse
		END AS SurveyQuestionResponseFormatted,
	
		CASE
			WHEN SQT.SurveyQuestionResponseTypeCode = 'SelectMultiple'
			THEN (SELECT SQRC.DisplayOrder FROM survey.SurveyQuestionResponseChoice SQRC WHERE SQRC.SurveyQuestionID = SSQR.SurveyQuestionID AND SQRC.SurveyQuestionResponseChoiceValue = SSQR.SurveyQuestionResponse AND SQRC.LanguageCode = SR.LanguageCode)
			ELSE 0
		END AS SurveyQuestionResponseDisplayOrder,
	
		SSQ.DisplayOrder AS SurveyQuestionDisplayOrder
	FROM survey.SurveySurveyQuestionResponse SSQR
		JOIN survey.SurveyResponse SR ON SR.SurveyResponseID = SSQR.SurveyResponseID
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQR.SurveyQuestionID
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyQuestionID = SSQR.SurveyQuestionID
			AND SSQ.SurveyID = SR.SurveyID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQR.SurveyResponseID = @SurveyResponseID
	ORDER BY SurveyQuestionDisplayOrder, SurveyQuestionResponseDisplayOrder, SurveyQuestionResponse
	
END
GO
--End procedure survey.GetSurveyQuestionResponsesBySurveyResponseID

--Begin survey.GetSurveyResponseBySurveyResponseID
EXEC Utility.DropObject 'survey.GetSurveyResponseBySurveyResponseID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.08.03
-- Description:	A stored procedure to get survey a response from the database
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyResponseBySurveyResponseID

@SurveyResponseID INT

AS
BEGIN

	SELECT
		L.LanguageName,
		SR.LanguageCode,
		SR.SurveyID,
		SR.SurveyResponseID,
		dbo.FormatDateTime(SR.ResponseDateTime) AS ResponseDateTimeFormatted,
		dbo.GetPersonNameByPersonID(SR.PersonID, 'LastFirst') AS FullName
	FROM survey.SurveyResponse SR
		JOIN dropdown.Language L ON L.LanguageCode = SR.LanguageCode
			AND SR.SurveyResponseID = @SurveyResponseID
	
END
GO
--End procedure survey.GetSurveyResponseBySurveyResponseID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'SurveyResponse')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('SurveyResponse', 'Survey Response')

	END
--ENDIF
GO
--End table dbo.EntityType

/*
--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Document', @AfterMenuItemCode='Community'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RequestForInformation', @AfterMenuItemCode='Document'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Research', @NewMenuItemText='Research', @AfterMenuItemCode='RequestForInformation', @Icon='fa fa-fw fa-balance-scale'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Atmospheric', @ParentMenuItemCode='Research', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='WeeklyReport', @NewMenuItemText='Weekly Atmospheric Report', @ParentMenuItemCode='Atmospheric', @AfterMenuItemCode='IncidentReportList', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SpotReport', @ParentMenuItemCode='Research', @AfterMenuItemCode='Atmospheric', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Surveys', @ParentMenuItemCode='Research', @AfterMenuItemCode='SpotReport', @Icon=''
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ManageWeeklyReports', @AfterMenuItemCode='RAPData', @NewMenuItemText='Implementation'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='RiskManagement', @AfterMenuItemCode='ManageWeeklyReports'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Equipment', @AfterMenuItemCode='Training'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Contacts', @AfterMenuItemCode='Equipment'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Stipends', @AfterMenuItemCode='Contacts'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='OpperationsSupport', @NewMenuItemText='Operations and Implementation Support', @AfterMenuItemCode='Stipends', @Icon='fa fa-fw fa-compass'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Procurement', @AfterMenuItemCode='OpperationsSupport'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Activity', @AfterMenuItemCode='Procurement'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='LogicalFramework', @AfterMenuItemCode='Activity'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Admin', @AfterMenuItemCode='LogicalFramework'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Atmospheric'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Surveys'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPDelivery'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='RAPData'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Training'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Equipment'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Contacts'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Stipends'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Procurement'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Activity'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem
*/

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.23 File 01 - AJACS - 2015.08.03 21.40.50')
GO
--End build tracking

