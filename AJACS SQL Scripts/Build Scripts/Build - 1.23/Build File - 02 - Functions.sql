USE AJACS
GO

--Begin function dbo.ListToTable
EXEC utility.DropObject 'dbo.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2011.12.28
-- Description:	A function to return a table from a delimted list of values
--
-- Author:			Todd Pires
-- Update date:	2012.06.07
-- Description:	Added the ListItemID column
--
-- Author:			Todd Pires
-- Update date:	2014.06.04
-- Description:	Added MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use CTE's, dropped MakeDistinct support
--
-- Author:			Todd Pires
-- Update date:	2014.09.19
-- Description:	Refactored to use XML - based on a script by Kshitij Satpute from SQLServerCentral.com
--
-- Author:			Todd Pires
-- Update date:	2015.08.02
-- Description:	Converted to NVARCHAR
-- ===================================================================================================

CREATE FUNCTION dbo.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function dbo.ListToTable