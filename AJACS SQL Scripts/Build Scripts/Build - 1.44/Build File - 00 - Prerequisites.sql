USE AJACS
GO

--Begin schema force
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'force')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA force'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema force