USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ContactCSWGClassificationID', 'INT'
EXEC utility.AddColumn @TableName, 'DescriptionOfDuties', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'IsRegimeDefector', 'BIT'
EXEC utility.AddColumn @TableName, 'PreviousDuties', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'PreviousProfession', 'NVARCHAR(100)'
EXEC utility.AddColumn @TableName, 'PreviousRankOrTitle', 'NVARCHAR(100)'
EXEC utility.AddColumn @TableName, 'PreviousServiceEndDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PreviousServiceStartDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PreviousUnit', 'NVARCHAR(100)'
EXEC utility.AddColumn @TableName, 'RetirementRejectionDate', 'DATE'
EXEC utility.AddColumn @TableName, 'SARGMinistryAndUnit', 'NVARCHAR(250)'

EXEC utility.SetDefaultConstraint @TableName, 'ContactCSWGClassificationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsRegimeDefector', 'BIT', 0

EXEC utility.DropColumn @TableName, 'RegimeDefector'
GO
--End table dbo.Contact

--Begin table dbo.SpotReportForce
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReportForce'

EXEC utility.DropObject 'dbo.SpotReportClass'
EXEC utility.DropObject @TableName

CREATE TABLE dbo.SpotReportForce
	(
	SpotReportForceID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	ForceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportForceID'
EXEC utility.SetIndexClustered 'IX_SpotReportForce', @TableName, 'SpotReportID,ForceID'
GO
--End table dbo.SpotReportForce

--Begin table dropdown.AreaOfOperationType
DECLARE @TableName VARCHAR(250) = 'dropdown.AreaOfOperationType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AreaOfOperationType
	(
	AreaOfOperationTypeID INT IDENTITY(0,1) NOT NULL,
	AreaOfOperationTypeName VARCHAR(250),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AreaOfOperationTypeID'
EXEC utility.SetIndexNonClustered 'IX_AreaOfOperationType', @TableName, 'DisplayOrder,AreaOfOperationTypeName', 'AreaOfOperationTypeID'
GO

SET IDENTITY_INSERT dropdown.AreaOfOperationType ON
GO

INSERT INTO dropdown.AreaOfOperationType 
	(AreaOfOperationTypeID, AreaOfOperationTypeName) 
VALUES 
	(0, NULL),
	(1, 'FSA Area of Operation'),
	(2, 'Moderate Opposition (MO) Area of Operation'),
	(3, 'Opposition Islamist (OI) Area of Operation'),
	(4, 'Syrian Government (SYG) Area of Operation'),
	(5, 'Syrian government permitted opposition (SGPO) Area of Operation'),
	(6, 'Kurd Area of Operation'),
	(7, 'Violent Extremists Area of Operation')
GO

SET IDENTITY_INSERT dropdown.AreaOfOperationType OFF
GO
--End table dropdown.AreaOfOperationType

--Begin table dropdown.ContactCSWGClassification
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactCSWGClassification'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactCSWGClassification
	(
	ContactCSWGClassificationID INT IDENTITY(0,1) NOT NULL,
	ContactCSWGClassificationCode VARCHAR(50),
	ContactCSWGClassificationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactCSWGClassificationID'
EXEC utility.SetIndexNonClustered 'IX_ContactCSWGClassification', @TableName, 'DisplayOrder,ContactCSWGClassificationName', 'ContactCSWGClassificationID'
GO

SET IDENTITY_INSERT dropdown.ContactCSWGClassification ON
GO

INSERT INTO dropdown.ContactCSWGClassification (ContactCSWGClassificationID, ContactCSWGClassificationName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.ContactCSWGClassification OFF
GO

INSERT INTO dropdown.ContactCSWGClassification 
	(ContactCSWGClassificationCode,ContactCSWGClassificationName,DisplayOrder)
VALUES
	('Primary','Primary', 1),
	('Secondary','Secondary', 2),
	('Retired','Retired', 3),
	('Rejected','Rejected', 4)
GO	
--End table dropdown.ContactCSWGClassification

--Begin table dropdown.ResourceProvider
DECLARE @TableName VARCHAR(250) = 'dropdown.ResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ResourceProvider
	(
	ResourceProviderID INT IDENTITY(0,1) NOT NULL,
	ResourceProviderName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ResourceProviderID'
EXEC utility.SetIndexNonClustered 'IX_ResourceProvider', @TableName, 'DisplayOrder,ResourceProviderName', 'ResourceProviderID'
GO

SET IDENTITY_INSERT dropdown.ResourceProvider ON
GO

INSERT INTO dropdown.ResourceProvider 
	(ResourceProviderID, ResourceProviderName) 
VALUES 
	(0, NULL),
	(1, 'GOI'),
	(2, 'GOK'),
	(3, 'IC North'),
	(4, 'IC South'),
	(5, 'Iran'),
	(6, 'Israel'),
	(7, 'Qatar'),
	(8, 'Russia'),
	(9, 'Saudi Arabia'),
	(10, 'Turkey'),
	(11, 'UAE'),
	(12, 'UK'),
	(13, 'US')
GO

SET IDENTITY_INSERT dropdown.ResourceProvider OFF
GO
--End table dropdown.ResourceProvider

--Begin table dropdown.UnitType
DECLARE @TableName VARCHAR(250) = 'dropdown.UnitType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.UnitType
	(
	UnitTypeID INT IDENTITY(0,1) NOT NULL,
	UnitTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'UnitTypeID'
EXEC utility.SetIndexNonClustered 'IX_UnitType', @TableName, 'DisplayOrder,UnitTypeName', 'UnitTypeID'
GO

SET IDENTITY_INSERT dropdown.UnitType ON
GO

INSERT INTO dropdown.UnitType 
	(UnitTypeID, UnitTypeName, DisplayOrder) 
VALUES 
	(0, NULL, 0),
	(1, 'Division', 1),
	(2, 'Brigade', 2),
	(3, 'Unit', 3),
	(4, 'Sub-Unit', 4),
	(5, 'Operations Room', 5),
	(6, 'Other', 6)
GO

SET IDENTITY_INSERT dropdown.UnitType OFF
GO
--End table dropdown.UnitType

--Begin table force.Force
DECLARE @TableName VARCHAR(250) = 'force.Force'

EXEC utility.DropObject @TableName

CREATE TABLE force.Force
	(
	ForceID INT IDENTITY(1,1) NOT NULL,
	TerritoryTypeCode VARCHAR(50),
	TerritoryID INT,
	ForceName VARCHAR(250),
	ForceDescription VARCHAR(250),
	AreaOfOperationTypeID INT,
	Location GEOMETRY,
	CommanderContactID INT,
	DeputyCommanderContactID INT,
	Notes VARCHAR(MAX),
	History VARCHAR(MAX),
	Comments VARCHAR(MAX),
	WebLinks NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AreaOfOperationTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeputyCommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
	 
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ForceID'
EXEC utility.SetIndexClustered 'IX_Force', @TableName, 'ForceName,ForceID'
GO
--End table force.Force

--Begin table force.ForceCommunity
DECLARE @TableName VARCHAR(250) = 'force.ForceCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceCommunity
	(
	ForceCommunityID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceCommunityID'
EXEC utility.SetIndexNonClustered 'IX_ForceCommunity', @TableName, 'ForceID,CommunityID'
GO
--End table force.ForceCommunity

--Begin table force.ForceEquipmentResourceProvider
DECLARE @TableName VARCHAR(250) = 'force.ForceEquipmentResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceEquipmentResourceProvider
	(
	ForceEquipmentResourceProviderID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceEquipmentResourceProviderID'
EXEC utility.SetIndexNonClustered 'IX_ForceEquipmentResourceProvider', @TableName, 'ForceID,ResourceProviderID'
GO
--End table force.ForceEquipmentResourceProvider

--Begin table force.ForceFinancialResourceProvider
DECLARE @TableName VARCHAR(250) = 'force.ForceFinancialResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceFinancialResourceProvider
	(
	ForceFinancialResourceProviderID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceFinancialResourceProviderID'
EXEC utility.SetIndexNonClustered 'IX_ForceFinancialResourceProvider', @TableName, 'ForceID,ResourceProviderID'
GO
--End table force.ForceFinancialResourceProvider

--Begin table force.ForceRisk
DECLARE @TableName VARCHAR(250) = 'force.ForceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceRisk
	(
	ForceRiskID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceRiskID'
EXEC utility.SetIndexNonClustered 'IX_ForceRisk', @TableName, 'ForceID,RiskID'
GO
--End table force.ForceRisk

--Begin table force.ForceUnit
DECLARE @TableName VARCHAR(250) = 'force.ForceUnit'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceUnit
	(
	ForceUnitID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	UnitName VARCHAR(250),
	UnitTypeID INT,
	CommanderContactID INT,
	DeputyCommanderContactID INT,
	TerritoryTypeCode VARCHAR(50),
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeputyCommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UnitTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceUnitID'
EXEC utility.SetIndexNonClustered 'IX_ForceUnit', @TableName, 'ForceID,UnitName'
GO
--End table force.ForceUnit
