-- File Name:	Build-1.44 File 01 - AJACS.sql
-- Build Key:	Build-1.44 File 01 - AJACS - 2016.01.21 21.07.08

USE AJACS
GO

-- ==============================================================================================================================
-- Procedures:
--		dbo.GetContactByContactID
--		dropdown.GetAreaOfOperationTypeData
--		dropdown.GetContactCSWGClassificationData
--		dropdown.GetResourceProviderData
--		dropdown.GetUnitTypeData
--		fifupdate.ApproveFIFUpdate
--		fifupdate.GetCommunityByCommunityID
--		fifupdate.GetProvinceByProvinceID
--		force.GetForceByForceID
--		reporting.GetSpotReportBySpotReportID
--		workflow.CanIncrementFIFUpdateWorkflow
--		workflow.GetRecommendationUpdateWorkflowStepPeople
--
-- Tables:
--		dbo.SpotReportForce
--		dropdown.AreaOfOperationType
--		dropdown.ContactCSWGClassification
--		dropdown.ResourceProvider
--		dropdown.UnitType
--		force.Force
--		force.ForceCommunity
--		force.ForceEquipmentResourceProvider
--		force.ForceFinancialResourceProvider
--		force.ForceRisk
--		force.ForceUnit
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin schema force
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'force')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA force'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema force
--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ContactCSWGClassificationID', 'INT'
EXEC utility.AddColumn @TableName, 'DescriptionOfDuties', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'IsRegimeDefector', 'BIT'
EXEC utility.AddColumn @TableName, 'PreviousDuties', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'PreviousProfession', 'NVARCHAR(100)'
EXEC utility.AddColumn @TableName, 'PreviousRankOrTitle', 'NVARCHAR(100)'
EXEC utility.AddColumn @TableName, 'PreviousServiceEndDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PreviousServiceStartDate', 'DATE'
EXEC utility.AddColumn @TableName, 'PreviousUnit', 'NVARCHAR(100)'
EXEC utility.AddColumn @TableName, 'RetirementRejectionDate', 'DATE'
EXEC utility.AddColumn @TableName, 'SARGMinistryAndUnit', 'NVARCHAR(250)'

EXEC utility.SetDefaultConstraint @TableName, 'ContactCSWGClassificationID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsRegimeDefector', 'BIT', 0

EXEC utility.DropColumn @TableName, 'RegimeDefector'
GO
--End table dbo.Contact

--Begin table dbo.SpotReportForce
DECLARE @TableName VARCHAR(250) = 'dbo.SpotReportForce'

EXEC utility.DropObject 'dbo.SpotReportClass'
EXEC utility.DropObject @TableName

CREATE TABLE dbo.SpotReportForce
	(
	SpotReportForceID INT IDENTITY(1,1) NOT NULL,
	SpotReportID INT,
	ForceID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SpotReportID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SpotReportForceID'
EXEC utility.SetIndexClustered 'IX_SpotReportForce', @TableName, 'SpotReportID,ForceID'
GO
--End table dbo.SpotReportForce

--Begin table dropdown.AreaOfOperationType
DECLARE @TableName VARCHAR(250) = 'dropdown.AreaOfOperationType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.AreaOfOperationType
	(
	AreaOfOperationTypeID INT IDENTITY(0,1) NOT NULL,
	AreaOfOperationTypeName VARCHAR(250),
	HexColor VARCHAR(7),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'AreaOfOperationTypeID'
EXEC utility.SetIndexNonClustered 'IX_AreaOfOperationType', @TableName, 'DisplayOrder,AreaOfOperationTypeName', 'AreaOfOperationTypeID'
GO

SET IDENTITY_INSERT dropdown.AreaOfOperationType ON
GO

INSERT INTO dropdown.AreaOfOperationType 
	(AreaOfOperationTypeID, AreaOfOperationTypeName) 
VALUES 
	(0, NULL),
	(1, 'FSA Area of Operation'),
	(2, 'Moderate Opposition (MO) Area of Operation'),
	(3, 'Opposition Islamist (OI) Area of Operation'),
	(4, 'Syrian Government (SYG) Area of Operation'),
	(5, 'Syrian government permitted opposition (SGPO) Area of Operation'),
	(6, 'Kurd Area of Operation'),
	(7, 'Violent Extremists Area of Operation')
GO

SET IDENTITY_INSERT dropdown.AreaOfOperationType OFF
GO
--End table dropdown.AreaOfOperationType

--Begin table dropdown.ContactCSWGClassification
DECLARE @TableName VARCHAR(250) = 'dropdown.ContactCSWGClassification'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ContactCSWGClassification
	(
	ContactCSWGClassificationID INT IDENTITY(0,1) NOT NULL,
	ContactCSWGClassificationCode VARCHAR(50),
	ContactCSWGClassificationName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ContactCSWGClassificationID'
EXEC utility.SetIndexNonClustered 'IX_ContactCSWGClassification', @TableName, 'DisplayOrder,ContactCSWGClassificationName', 'ContactCSWGClassificationID'
GO

SET IDENTITY_INSERT dropdown.ContactCSWGClassification ON
GO

INSERT INTO dropdown.ContactCSWGClassification (ContactCSWGClassificationID, ContactCSWGClassificationName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.ContactCSWGClassification OFF
GO

INSERT INTO dropdown.ContactCSWGClassification 
	(ContactCSWGClassificationCode,ContactCSWGClassificationName,DisplayOrder)
VALUES
	('Primary','Primary', 1),
	('Secondary','Secondary', 2),
	('Retired','Retired', 3),
	('Rejected','Rejected', 4)
GO	
--End table dropdown.ContactCSWGClassification

--Begin table dropdown.ResourceProvider
DECLARE @TableName VARCHAR(250) = 'dropdown.ResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ResourceProvider
	(
	ResourceProviderID INT IDENTITY(0,1) NOT NULL,
	ResourceProviderName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'ResourceProviderID'
EXEC utility.SetIndexNonClustered 'IX_ResourceProvider', @TableName, 'DisplayOrder,ResourceProviderName', 'ResourceProviderID'
GO

SET IDENTITY_INSERT dropdown.ResourceProvider ON
GO

INSERT INTO dropdown.ResourceProvider 
	(ResourceProviderID, ResourceProviderName) 
VALUES 
	(0, NULL),
	(1, 'GOI'),
	(2, 'GOK'),
	(3, 'IC North'),
	(4, 'IC South'),
	(5, 'Iran'),
	(6, 'Israel'),
	(7, 'Qatar'),
	(8, 'Russia'),
	(9, 'Saudi Arabia'),
	(10, 'Turkey'),
	(11, 'UAE'),
	(12, 'UK'),
	(13, 'US')
GO

SET IDENTITY_INSERT dropdown.ResourceProvider OFF
GO
--End table dropdown.ResourceProvider

--Begin table dropdown.UnitType
DECLARE @TableName VARCHAR(250) = 'dropdown.UnitType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.UnitType
	(
	UnitTypeID INT IDENTITY(0,1) NOT NULL,
	UnitTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'UnitTypeID'
EXEC utility.SetIndexNonClustered 'IX_UnitType', @TableName, 'DisplayOrder,UnitTypeName', 'UnitTypeID'
GO

SET IDENTITY_INSERT dropdown.UnitType ON
GO

INSERT INTO dropdown.UnitType 
	(UnitTypeID, UnitTypeName, DisplayOrder) 
VALUES 
	(0, NULL, 0),
	(1, 'Division', 1),
	(2, 'Brigade', 2),
	(3, 'Unit', 3),
	(4, 'Sub-Unit', 4),
	(5, 'Operations Room', 5),
	(6, 'Other', 6)
GO

SET IDENTITY_INSERT dropdown.UnitType OFF
GO
--End table dropdown.UnitType

--Begin table force.Force
DECLARE @TableName VARCHAR(250) = 'force.Force'

EXEC utility.DropObject @TableName

CREATE TABLE force.Force
	(
	ForceID INT IDENTITY(1,1) NOT NULL,
	TerritoryTypeCode VARCHAR(50),
	TerritoryID INT,
	ForceName VARCHAR(250),
	ForceDescription VARCHAR(250),
	AreaOfOperationTypeID INT,
	Location GEOMETRY,
	CommanderContactID INT,
	DeputyCommanderContactID INT,
	Notes VARCHAR(MAX),
	History VARCHAR(MAX),
	Comments VARCHAR(MAX),
	WebLinks NVARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AreaOfOperationTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeputyCommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
	 
EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ForceID'
EXEC utility.SetIndexClustered 'IX_Force', @TableName, 'ForceName,ForceID'
GO
--End table force.Force

--Begin table force.ForceCommunity
DECLARE @TableName VARCHAR(250) = 'force.ForceCommunity'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceCommunity
	(
	ForceCommunityID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	CommunityID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceCommunityID'
EXEC utility.SetIndexNonClustered 'IX_ForceCommunity', @TableName, 'ForceID,CommunityID'
GO
--End table force.ForceCommunity

--Begin table force.ForceEquipmentResourceProvider
DECLARE @TableName VARCHAR(250) = 'force.ForceEquipmentResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceEquipmentResourceProvider
	(
	ForceEquipmentResourceProviderID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceEquipmentResourceProviderID'
EXEC utility.SetIndexNonClustered 'IX_ForceEquipmentResourceProvider', @TableName, 'ForceID,ResourceProviderID'
GO
--End table force.ForceEquipmentResourceProvider

--Begin table force.ForceFinancialResourceProvider
DECLARE @TableName VARCHAR(250) = 'force.ForceFinancialResourceProvider'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceFinancialResourceProvider
	(
	ForceFinancialResourceProviderID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	ResourceProviderID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ResourceProviderID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceFinancialResourceProviderID'
EXEC utility.SetIndexNonClustered 'IX_ForceFinancialResourceProvider', @TableName, 'ForceID,ResourceProviderID'
GO
--End table force.ForceFinancialResourceProvider

--Begin table force.ForceRisk
DECLARE @TableName VARCHAR(250) = 'force.ForceRisk'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceRisk
	(
	ForceRiskID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	RiskID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'RiskID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceRiskID'
EXEC utility.SetIndexNonClustered 'IX_ForceRisk', @TableName, 'ForceID,RiskID'
GO
--End table force.ForceRisk

--Begin table force.ForceUnit
DECLARE @TableName VARCHAR(250) = 'force.ForceUnit'

EXEC utility.DropObject @TableName

CREATE TABLE force.ForceUnit
	(
	ForceUnitID INT IDENTITY(1,1) NOT NULL,
	ForceID INT,
	UnitName VARCHAR(250),
	UnitTypeID INT,
	CommanderContactID INT,
	DeputyCommanderContactID INT,
	TerritoryTypeCode VARCHAR(50),
	TerritoryID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DeputyCommanderContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ForceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'TerritoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'UnitTypeID', 'INT', 0

EXEC utility.SetPrimaryKeyClustered @TableName, 'ForceUnitID'
EXEC utility.SetIndexNonClustered 'IX_ForceUnit', @TableName, 'ForceID,UnitName'
GO
--End table force.ForceUnit

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
--
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	Added payment history support
--
-- Author:			Todd Pires
-- Create date:	2015.08.14
-- Description:	Added various additional fields
--
-- Author:			Greg Yingling
-- Create date:	2015.08.02
-- Description:	Added Community Asset and Community Asset Unit fields
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,		
		C1.Address2,		
		C1.Aliases,		
		C1.ArabicFirstName,		
		C1.ArabicLastName,		
		C1.ArabicMiddleName,		
		C1.ArabicMotherName,		
		C1.CellPhoneNumber,		
		C1.CellPhoneNumberCountryCallingCodeID,		
		C1.City,		
		C1.CommunityAssetID,		
		C1.CommunityAssetUnitID,		
		(SELECT CA.CommunityAssetName FROM dbo.CommunityAsset CA WHERE CA.CommunityAssetID = C1.CommunityAssetID) AS CommunityAssetName,		
		(SELECT CAU.CommunityAssetUnitName FROM dbo.CommunityAssetUnit CAU WHERE CAU.CommunityAssetUnitID = C1.CommunityAssetUnitID) AS CommunityAssetUnitName,		
		C1.CommunityID,		
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,		
		C1.ContactCSWGClassificationID,
		C1.ContactID,		
		C1.DateOfBirth,		
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,		
		C1.DescriptionOfDuties,		
		C1.EmailAddress1,		
		C1.EmailAddress2,		
		C1.EmployerName,		
		C1.FaceBookPageURL,		
		C1.FaxNumber,		
		C1.FaxNumberCountryCallingCodeID,		
		C1.FirstName,		
		C1.Gender,		
		C1.GovernmentIDNumber,		
		C1.IsActive,		
		C1.IsRegimeDefector,		
		C1.IsValid,		
		C1.LastName,		
		C1.MiddleName,		
		C1.MotherName,		
		C1.Notes,		
		C1.PassportExpirationDate,		
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,		
		C1.PassportNumber,		
		C1.PhoneNumber,		
		C1.PhoneNumberCountryCallingCodeID,		
		C1.PlaceOfBirth,		
		C1.PostalCode,		
		C1.PreviousDuties,		
		C1.PreviousProfession,		
		C1.PreviousRankOrTitle,		
		C1.PreviousServiceEndDate,		
		dbo.FormatDate(C1.PreviousServiceEndDate) AS PreviousServiceEndDateFormatted,		
		C1.PreviousServiceStartDate,		
		dbo.FormatDate(C1.PreviousServiceStartDate) AS PreviousServiceStartDateFormatted,		
		C1.PreviousUnit,		
		C1.Profession,		
		C1.ProvinceID,		
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,		
		C1.SARGMinistryAndUnit,
		C1.SkypeUserName,		
		C1.StartDate,		
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,		
		C1.State,		
		C1.Title,		
		C2.CountryID AS CitizenshipCountryID1,		
		C2.CountryName AS CitizenshipCountryName1,		
		C3.CountryID AS CitizenshipCountryID2,		
		C3.CountryName AS CitizenshipCountryName2,		
		C5.CountryID,		
		C5.CountryName,		
		C6.CountryID AS GovernmentIDNumberCountryID,		
		C6.CountryName AS GovernmentIDNumberCountryName,		
		C7.CountryID AS PlaceOfBirthCountryID,		
		C7.CountryName AS PlaceOfBirthCountryName,		
		CCC1.CountryCallingCode AS CellPhoneNumberCountryCallingCode,		
		CCC2.CountryCallingCode AS FaxNumberCountryCallingCode,		
		CCC3.CountryCallingCode AS PhoneNumberCountryCallingCode,		
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName,		
		P1.ProjectID,		
		P1.ProjectName,		
		S.StipendID,		
		S.StipendName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Country C7 ON C7.CountryID = C1.PlaceOfBirthCountryID
		JOIN dropdown.CountryCallingCode CCC1 ON CCC1.CountryCallingCodeID = C1.CellPhoneNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC2 ON CCC2.CountryCallingCodeID = C1.FaxNumberCountryCallingCodeID
		JOIN dropdown.CountryCallingCode CCC3 ON CCC3.CountryCallingCodeID = C1.PhoneNumberCountryCallingCodeID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-vetting.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID
	ORDER BY CNC.VettingDate DESC

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT 
		CSP.ContactStipendPaymentID,
		CSP.StipendPaidDate,
		dbo.FormatDate(CSP.StipendPaidDate) AS StipendPaidDateFormatted,
		CSP.StipendAmountPaid,
		CSP.StipendName,
	
		CASE 
			WHEN CSP.CommunityID > 0
			THEN (SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CSP.CommunityID)
			ELSE 'n/a'
		END AS CommunityName,
	
		P.ProvinceName
	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Province P ON P.ProvinceID = CSP.ProvinceID
			AND CSP.ContactID = @ContactID
			AND CSP.StipendPaidDate IS NOT NULL
	ORDER BY CSP.StipendPaidDate DESC, CSP.ContactStipendPaymentID DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dropdown.GetAreaOfOperationTypeData
EXEC utility.DropObject 'dropdown.GetAreaOfOperationTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.AreaOfOperationType table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetAreaOfOperationTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.AreaOfOperationTypeID, 
		T.AreaOfOperationTypeName,
		T.HexColor
	FROM dropdown.AreaOfOperationType T
	WHERE (T.AreaOfOperationTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.AreaOfOperationTypeName, T.AreaOfOperationTypeID

END
GO
--End procedure dropdown.GetAreaOfOperationTypeData

--Begin procedure dropdown.GetContactCSWGClassificationData
EXEC utility.DropObject 'dropdown.GetContactCSWGClassificationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.ContactCSWGClassification table
-- ===============================================================================================
CREATE PROCEDURE dropdown.GetContactCSWGClassificationData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactCSWGClassificationID, 
		T.ContactCSWGClassificationCode,
		T.ContactCSWGClassificationName
	FROM dropdown.ContactCSWGClassification T
	WHERE (T.ContactCSWGClassificationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactCSWGClassificationName, T.ContactCSWGClassificationID

END
GO
--End procedure dropdown.GetContactCSWGClassificationData

--Begin procedure dropdown.GetResourceProviderData
EXEC utility.DropObject 'dropdown.GetResourceProviderData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.ResourceProvider table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetResourceProviderData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ResourceProviderID, 
		T.ResourceProviderName
	FROM dropdown.ResourceProvider T
	WHERE (T.ResourceProviderID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ResourceProviderName, T.ResourceProviderID

END
GO
--End procedure dropdown.GetResourceProviderData

--Begin procedure dropdown.GetUnitTypeData
EXEC utility.DropObject 'dropdown.GetUnitTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.08
-- Description:	A stored procedure to return data from the dropdown.UnitType table
-- ===============================================================================
CREATE PROCEDURE dropdown.GetUnitTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.UnitTypeID, 
		T.UnitTypeName
	FROM dropdown.UnitType T
	WHERE (T.UnitTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.UnitTypeName, T.UnitTypeID

END
GO
--End procedure dropdown.GetUnitTypeData

--Begin procedure fifupdate.ApproveFIFUpdate
EXEC utility.DropObject 'fifupdate.ApproveFIFUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================
-- Author:			Christopher Crouch
-- Create date:	2016.01.14
-- Description:	A procedure to approve a FIF Update
-- ================================================
CREATE PROCEDURE fifupdate.ApproveFIFUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nFIFUpdateID INT = ISNULL((SELECT TOP 1 FU.FIFUpdateID FROM fifupdate.FIFUpdate FU ORDER BY FU.FIFUpdateID DESC), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogFIFAction @nFIFUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogFIFAction @nFIFUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.FIFCommunityEngagement = FU.FIFCommunityEngagement,
		P.FIFJustice = FU.FIFJustice,
		P.FIFPoliceEngagement = FU.FIFPoliceEngagement,
		P.FIFUpdateNotes = FU.FIFUpdateNotes,
		P.IndicatorUpdate = FU.IndicatorUpdate,
		P.MeetingNotes = FU.MeetingNotes
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN fifupdate.Province FU ON FU.ProvinceID = P.ProvinceID
			AND FU.FIFUpdateID = @nFIFUpdateID

	-- Province Indicator
	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.FIFAchievedValue, 
		PRI.FIFNotes
	FROM fifupdate.ProvinceIndicator PRI
	
	-- Province Meeting
	DELETE PRM
	FROM dbo.ProvinceMeeting PRM
		JOIN @tOutputProvince O ON O.ProvinceID = PRM.ProvinceID
	
	INSERT INTO dbo.ProvinceMeeting
		(ProvinceID, MeetingDate, MeetingTitle, AttendeeCount, RegularAttendeeCount)
	SELECT
		PRM.ProvinceID,
		PRM.MeetingDate, 
		PRM.MeetingTitle,
		PRM.AttendeeCount,
		PRM.RegularAttendeeCount
	FROM fifupdate.ProvinceMeeting PRM

	-- Province Risk
	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.FIFRiskValue, 
		PR.FIFNotes
	FROM fifupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Province'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'FIFUpdateProvince'
			AND D.DocumentDescription IN ('MOU Document')

	UPDATE C
	SET
		C.FIFCommunityEngagement = FU.FIFCommunityEngagement,
		C.FIFJustice = FU.FIFJustice,
		C.FIFPoliceEngagement = FU.FIFPoliceEngagement,
		C.FIFUpdateNotes = FU.FIFUpdateNotes,
		C.IndicatorUpdate = FU.IndicatorUpdate,
		C.MeetingNotes = FU.MeetingNotes		
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN fifupdate.Community FU ON FU.CommunityID = C.CommunityID
			AND FU.FIFUpdateID = @nFIFUpdateID

	-- Community Indicator
	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, FIFAchievedValue, FIFNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.FIFAchievedValue, 
		CI.FIFNotes
	FROM fifupdate.CommunityIndicator CI
	
	-- Community Meeting
	DELETE CM
	FROM dbo.CommunityMeeting CM
		JOIN @tOutputCommunity O ON O.CommunityID = CM.CommunityID
	
	INSERT INTO dbo.CommunityMeeting
		(CommunityID, MeetingDate, MeetingTitle, AttendeeCount, RegularAttendeeCount)
	SELECT
		CM.CommunityID,
		CM.MeetingDate,
		CM.MeetingTitle, 
		CM.AttendeeCount,
		CM.RegularAttendeeCount
	FROM fifupdate.CommunityMeeting CM

	-- Community Risk
	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, FIFRiskValue, FIFNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.FIFRiskValue, 
		CR.FIFNotes
	FROM fifupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Community'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'FIFUpdateCommunity'
			AND D.DocumentDescription IN ('MOU Document')
	
	DELETE FROM fifupdate.FIFUpdate

	TRUNCATE TABLE fifupdate.Province
	TRUNCATE TABLE fifupdate.ProvinceIndicator
	TRUNCATE TABLE fifupdate.ProvinceMeeting
	TRUNCATE TABLE fifupdate.ProvinceRisk

	TRUNCATE TABLE fifupdate.Community
	TRUNCATE TABLE fifupdate.CommunityIndicator
	TRUNCATE TABLE fifupdate.CommunityMeeting
	TRUNCATE TABLE fifupdate.CommunityRisk

END
GO
--Begin procedure fifupdate.ApproveFIFUpdate

--Begin procedure fifupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'fifupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to return data from the dbo.Community and fifupdate.Community tables
-- ====================================================================================================
CREATE PROCEDURE fifupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CommunityID,
		C.CommunityName AS EntityName,
		C.FIFCommunityEngagement,
		C.FIFJustice,
		C.FIFPoliceEngagement,
		C.FIFUpdateNotes,
		C.IndicatorUpdate,
		C.MeetingNotes,
		C.MOUDate,
		dbo.FormatDate(C.MOUDate) AS MOUDateFormatted
	FROM dbo.Community C
	WHERE C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CommunityID,
		C1.CommunityName AS EntityName,
		C1.MOUDate,
		dbo.FormatDate(C1.MOUDate) AS MOUDateFormatted,
		C2.FIFCommunityEngagement,
		C2.FIFJustice,
		C2.FIFPoliceEngagement,
		C2.FIFUpdateNotes,
		C2.IndicatorUpdate,
		C2.MeetingNotes
	FROM fifupdate.Community C2
		JOIN dbo.Community C1 ON C1.CommunityID = C2.CommunityID
			AND C2.CommunityID = @CommunityID

	--EntityMeetingsCurrent
	SELECT
		CM.AttendeeCount, 
		CM.MeetingDate, 
		dbo.FormatDate(CM.MeetingDate) AS MeetingDateFormatted,
		CM.CommunityMeetingID AS MeetingID, 
		CM.MeetingTitle, 
		CM.RegularAttendeeCount
	FROM dbo.CommunityMeeting CM
	WHERE CM.CommunityID = @CommunityID

	--EntityMeetingsUpdate
	SELECT
		CM.AttendeeCount, 
		CM.MeetingDate, 
		dbo.FormatDate(CM.MeetingDate) AS MeetingDateFormatted,
		CM.CommunityMeetingID AS MeetingID, 
		CM.MeetingTitle, 
		CM.RegularAttendeeCount
	FROM fifupdate.CommunityMeeting CM
	WHERE CM.CommunityID = @CommunityID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'FIFCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.FIFAchievedValue,
		OACI.FIFNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.FIFAchievedValue, 
				CI.FIFNotes
			FROM fifupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.FIFRiskValue,
		OACR.FIFNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.FIFRiskValue, 
				CR.FIFNotes
			FROM fifupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure fifupdate.GetCommunityByCommunityID

--Begin procedure fifupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'fifupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to return data from the dbo.Province and fifupdate.Province tables
-- ==================================================================================================
CREATE PROCEDURE fifupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.ProvinceID,
		P.ProvinceName AS EntityName,
		P.FIFCommunityEngagement,
		P.FIFJustice,
		P.FIFPoliceEngagement,
		P.FIFUpdateNotes,
		P.IndicatorUpdate,
		P.MeetingNotes,
		P.MOUDate,
		dbo.FormatDate(P.MOUDate) AS MOUDateFormatted
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.ProvinceID,
		P1.ProvinceName AS EntityName,
		P1.MOUDate,
		dbo.FormatDate(P1.MOUDate) AS MOUDateFormatted,
		P2.FIFCommunityEngagement,
		P2.FIFJustice,
		P2.FIFPoliceEngagement,
		P2.FIFUpdateNotes,
		P2.IndicatorUpdate,
		P2.MeetingNotes
	FROM fifupdate.Province P2
		JOIN dbo.Province P1 ON P1.ProvinceID = P2.ProvinceID
			AND P2.ProvinceID = @ProvinceID

	--EntityMeetingsCurrent
	SELECT
		PM.AttendeeCount, 
		PM.MeetingDate, 
		dbo.FormatDate(PM.MeetingDate) AS MeetingDateFormatted,
		PM.ProvinceMeetingID AS MeetingID, 
		PM.MeetingTitle, 
		PM.RegularAttendeeCount
	FROM dbo.ProvinceMeeting PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityMeetingsUpdate
	SELECT
		PM.AttendeeCount, 
		PM.MeetingDate, 
		dbo.FormatDate(PM.MeetingDate) AS MeetingDateFormatted,
		PM.ProvinceMeetingID AS MeetingID, 
		PM.MeetingTitle, 
		PM.RegularAttendeeCount
	FROM fifupdate.ProvinceMeeting PM
	WHERE PM.ProvinceID = @ProvinceID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'FIFProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('FIF Meeting Minutes', 'MOU Document')
	ORDER BY D.DocumentDescription

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PI.ProvinceIndicatorID
			FROM dbo.ProvinceIndicator PI 
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OAPI.FIFAchievedValue,
		OAPI.FIFNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'FIF%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PI.FIFAchievedValue, 
				PI.FIFNotes
			FROM fifupdate.ProvinceIndicator PI
			WHERE PI.IndicatorID = I.IndicatorID
				AND PI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.FIFRiskValue,
		OAPR.FIFNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.FIFRiskValue, 
				PR.FIFNotes
			FROM fifupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure fifupdate.GetProvinceByProvinceID

--Begin procedure force.GetForceByForceID
EXEC utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- ==================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		F.CommanderContactID,
		dbo.FormatContactNameByContactID(F.CommanderContactID, 'LastFirstMiddle') AS CommanderFullName,
		F.Comments, 
		F.DeputyCommanderContactID, 
		dbo.FormatContactNameByContactID(F.DeputyCommanderContactID, 'LastFirstMiddle') AS DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	SELECT
		FU.CommanderContactID, 
		FU.DeputyCommanderContactID,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure reporting.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'reporting.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A stored procedure to data from the dbo.SpotReport table
-- =====================================================================
CREATE PROCEDURE reporting.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName,
		dbo.GetSpotReportProvincesList(SR.SpotreportID) as ProvinceList,
		dbo.GetSpotReportCommunitiesList(SR.SpotreportID) as CommunityList,
		dbo.FormatStaticGoogleMapForSpotReport(SR.SpotReportID) AS GMap		
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure workflow.CanIncrementFIFUpdateWorkflow
EXEC Utility.DropObject 'workflow.CanIncrementFIFUpdateWorkflow'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A procedure to determine if a workflow step can be incremented
-- ===========================================================================

CREATE PROCEDURE workflow.CanIncrementFIFUpdateWorkflow

@EntityID INT


AS
BEGIN

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'FIFUpdate'
			JOIN fifupdate.FIFUpdate F ON F.WorkflowStepNumber = WS.WorkflowStepNumber
				AND F.FIFUpdateID = @EntityID
				AND WS.ParentWorkflowStepID = 0
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT COUNT(EWS.IsComplete) AS IncompleteStepIDCount
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.IsComplete = 0
			AND EWS.EntityID = @EntityID

END
GO
--End procedure workflow.CanIncrementFIFUpdateWorkflow

--Begin procedure workflow.GetRecommendationUpdateWorkflowStepPeople
EXEC Utility.DropObject 'workflow.GetRecommendationUpdateWorkflowStepPeople'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.25
-- Description:	A stored procedure to people associated with the workflow steps on a recommendation update
-- =======================================================================================================
CREATE PROCEDURE workflow.GetRecommendationUpdateWorkflowStepPeople

@EntityID INT,
@IncludePriorSteps BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	WITH HD (WorkflowStepID,ParentWorkflowStepID)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'RecommendationUpdate'
				AND WS.ParentWorkflowStepID = 0
				AND 
					(
					(@IncludePriorSteps = 1 AND WS.WorkflowStepNumber <= (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @EntityID))
						OR WS.WorkflowStepNumber = (SELECT RU.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate RU WHERE RU.RecommendationUpdateID = @EntityID)
					)
		
		UNION ALL
		
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)

	SELECT DISTINCT
		dbo.FormatPersonName(P.FirstName, P.LastName, NULL, 'LastFirst') AS FullName,
		P.EmailAddress,
		P.PersonID
	FROM
		(	
		SELECT
			'Recommendation.AddUpdate.WorkflowStepID' + 
			CASE
				WHEN HD1.ParentWorkflowStepID > 0
				THEN CAST(HD1.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
				ELSE ''
			END 
			+ CAST(HD1.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage
		FROM HD HD1
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD1.WorkflowStepID
				AND NOT EXISTS
					(
					SELECT 1 
					FROM HD HD2 
					WHERE HD2.ParentWorkflowStepID = HD1.WorkflowStepID
					)
		) D
		JOIN permissionable.PersonPermissionable PP ON PP.PermissionableLineage = D.PermissionableLineage
		JOIN dbo.Person P ON P.PersonID = PP.PersonID
	ORDER BY FullName

END
GO
--End procedure workflow.GetRecommendationUpdateWorkflowStepPeople
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'FIFUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	SELECT
		'FIFUpdate',
		ET.WorkflowActionCode,
		
		CASE
			WHEN ET.WorkflowActionCode = 'DecrementWorkflow'
			THEN '<p>A previously submitted FIF update has been disapproved:</p><p>&nbsp;</p><p><strong>FIF Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the FIF update workflow. Please click the link above to review the updated FIF.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'IncrementWorkflow'
			THEN '<p>An AJACS FIF update has been submitted for your review:</p><p>&nbsp;</p><p><strong>FIF Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the FIF update workflow. Please click the link above to review the updated FIF.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'Release'
			THEN '<p>An AJACS FIF update has been released:</p><p>&nbsp;</p><p><strong>FIF Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the FIF update workflow. Please click the link above to review the updated FIF.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
		END
	
	FROM dbo.EmailTemplate ET
	WHERE ET.EntityTypeCode = 'RiskUpdate'

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'FIFUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('FIFUpdate', '[[Link]]', 'Link', 1),
		('FIFUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
UPDATE dbo.EntityType
SET EntityTypeCode = 'FIFUpdate'
WHERE EntityTypeCode = 'FIF'
GO

EXEC utility.EntityTypeAddUpdate 'FIFCommunity', 'FIF Community'
GO
EXEC utility.EntityTypeAddUpdate 'FIFProvince', 'FIF Province'
GO
EXEC utility.EntityTypeAddUpdate 'FIFUpdate', 'FIF Update'
GO
EXEC utility.EntityTypeAddUpdate 'Force', 'Force'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE dbo.MenuItem
SET MenuItemCode = 'FIFUpdate'
WHERE MenuItemCode = 'FIF'
GO

UPDATE permissionable.Permissionable
SET PermissionableCode = 'FIFUpdate'
WHERE PermissionableCode = 'FIF'
GO

UPDATE dbo.MenuItemPermissionableLineage
SET PermissionableLineage = 'FIFUpdate.WorkflowStepID%'
WHERE PermissionableLineage = 'FIF.WorkflowStepID%'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Force', @NewMenuItemLink='/force/list', @NewMenuItemText='Forces', @ParentMenuItemCode='Research', @AfterMenuItemCode='Atmospheric', @PermissionableLineageList='Force.List'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='FIFUpdate', @NewMenuItemLink='/fif/addupdate', @NewMenuItemText='Formalized Institutional Frameworks', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='PoliceEngagementUpdate', @PermissionableLineageList='FIFUpdate.WorkflowStepID%'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
--End table dbo.MenuItem

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Force')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Force','Forces', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable / permissionable.DisplayGroupPermissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		0,
		'Force',
		'Forces'
		)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force'),	'List',	'List Forces'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force'),	'AddUpdate', 'Add / edit a force'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force'),	'View',	'View a force')
		
	INSERT INTO permissionable.DisplayGroupPermissionable
		(DisplayGroupID, PermissionableID)
	SELECT
		DG.DisplayGroupID,
		(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force')
	FROM permissionable.DisplayGroup DG
	WHERE DG.DisplayGroupCode = 'Force'
		
	END
--ENDIF
GO
--End table permissionable.Permissionable / permissionable.DisplayGroupPermissionable

--Begin table workflow.Workflow
UPDATE workflow.Workflow
SET EntityTypeCode = 'FIFUpdate'
WHERE EntityTypeCode = 'FIF'
GO
--End table workflow.Workflow

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.44 File 01 - AJACS - 2016.01.21 21.07.08')
GO
--End build tracking

