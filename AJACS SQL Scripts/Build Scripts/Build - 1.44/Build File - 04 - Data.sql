USE AJACS
GO

--Begin table dbo.EmailTemplate
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplate ET WHERE ET.EntityTypeCode = 'FIFUpdate')
	BEGIN
	
	INSERT INTO dbo.EmailTemplate
		(EntityTypeCode, WorkflowActionCode, EmailText)
	SELECT
		'FIFUpdate',
		ET.WorkflowActionCode,
		
		CASE
			WHEN ET.WorkflowActionCode = 'DecrementWorkflow'
			THEN '<p>A previously submitted FIF update has been disapproved:</p><p>&nbsp;</p><p><strong>FIF Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the FIF update workflow. Please click the link above to review the updated FIF.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'IncrementWorkflow'
			THEN '<p>An AJACS FIF update has been submitted for your review:</p><p>&nbsp;</p><p><strong>FIF Update: </strong>[[Link]]</p><p>&nbsp;</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>&nbsp;</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the FIF update workflow. Please click the link above to review the updated FIF.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
			WHEN ET.WorkflowActionCode = 'Release'
			THEN '<p>An AJACS FIF update has been released:</p><p>&nbsp;</p><p><strong>FIF Update: </strong>[[Link]]</p><p><strong>Comments:</strong><br />[[Comments]]</p><p>You are receiving this email notification from the AJACS system because you have been assigned as a member of the FIF update workflow. Please click the link above to review the updated FIF.</p><p>Please do not reply to this email as it is generated automatically by the AJACS system.</p><p>Thank you,<br /><br />The AJACS Team</p>'
		END
	
	FROM dbo.EmailTemplate ET
	WHERE ET.EntityTypeCode = 'RiskUpdate'

	END
--ENDIF
GO
--End table dbo.EmailTemplate

--Begin table dbo.EmailTemplateField
IF NOT EXISTS (SELECT 1 FROM dbo.EmailTemplateField ETF WHERE ETF.EntityTypeCode = 'FIFUpdate')
	BEGIN

	INSERT INTO dbo.EmailTemplateField
		(EntityTypeCode, PlaceHolderText, PlaceHolderDescription, DisplayOrder)
	VALUES
		('FIFUpdate', '[[Link]]', 'Link', 1),
		('FIFUpdate', '[[Comments]]', 'Comments', 2)

	END
--ENDIF
GO
--End table dbo.EmailTemplateField

--Begin table dbo.EntityType
UPDATE dbo.EntityType
SET EntityTypeCode = 'FIFUpdate'
WHERE EntityTypeCode = 'FIF'
GO

EXEC utility.EntityTypeAddUpdate 'FIFCommunity', 'FIF Community'
GO
EXEC utility.EntityTypeAddUpdate 'FIFProvince', 'FIF Province'
GO
EXEC utility.EntityTypeAddUpdate 'FIFUpdate', 'FIF Update'
GO
EXEC utility.EntityTypeAddUpdate 'Force', 'Force'
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE dbo.MenuItem
SET MenuItemCode = 'FIFUpdate'
WHERE MenuItemCode = 'FIF'
GO

UPDATE permissionable.Permissionable
SET PermissionableCode = 'FIFUpdate'
WHERE PermissionableCode = 'FIF'
GO

UPDATE dbo.MenuItemPermissionableLineage
SET PermissionableLineage = 'FIFUpdate.WorkflowStepID%'
WHERE PermissionableLineage = 'FIF.WorkflowStepID%'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Force', @NewMenuItemLink='/force/list', @NewMenuItemText='Forces', @ParentMenuItemCode='Research', @AfterMenuItemCode='Atmospheric', @PermissionableLineageList='Force.List'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='FIFUpdate', @NewMenuItemLink='/fif/addupdate', @NewMenuItemText='Formalized Institutional Frameworks', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='PoliceEngagementUpdate', @PermissionableLineageList='FIFUpdate.WorkflowStepID%'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Research'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
--End table dbo.MenuItem

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Force')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Force','Forces', 7)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable / permissionable.DisplayGroupPermissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		0,
		'Force',
		'Forces'
		)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force'),	'List',	'List Forces'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force'),	'AddUpdate', 'Add / edit a force'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force'),	'View',	'View a force')
		
	INSERT INTO permissionable.DisplayGroupPermissionable
		(DisplayGroupID, PermissionableID)
	SELECT
		DG.DisplayGroupID,
		(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Force')
	FROM permissionable.DisplayGroup DG
	WHERE DG.DisplayGroupCode = 'Force'
		
	END
--ENDIF
GO
--End table permissionable.Permissionable / permissionable.DisplayGroupPermissionable

--Begin table workflow.Workflow
UPDATE workflow.Workflow
SET EntityTypeCode = 'FIFUpdate'
WHERE EntityTypeCode = 'FIF'
GO
--End table workflow.Workflow

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
