-- File Name:	Build-1.17 File 01 - AJACS.sql
-- Build Key:	Build-1.17 File 01 - AJACS - 2015.06.20 20.32.21

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.GetContactLocationByContactID
--		dbo.IsCommunityStipendEligible
--		dbo.IsProvinceStipendEligible
--
-- Procedures:
--		dbo.GetClassByClassID
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetConceptNoteEquipmentByConceptNoteID
--		dbo.GetContactByContactID
--		dropdown.GetContactTypeData
--		dropdown.GetStipendData
--		procurement.GetEquipmentInventoryByEquipmentInventoryID
--		reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID
--		reporting.GetEquipmentDistributionByProvinceIDOrCommunityID
--		reporting.GetStipendPaymentsByProvince
--
-- Tables:
--		dbo.ContactStipendPayment
--		dropdown.Stipend
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.ConceptNote
ALTER TABLE dbo.ConceptNote ALTER COLUMN BrandingRequirements VARCHAR(MAX)
ALTER TABLE dbo.ConceptNote ALTER COLUMN RiskAssessment VARCHAR(MAX)
ALTER TABLE dbo.ConceptNote ALTER COLUMN RiskMitigationMeasures VARCHAR(MAX)
GO
--End table dbo.ConceptNote
 
--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'MotherName', 'NVARCHAR(200)'
EXEC utility.AddColumn @TableName, 'StartDate', 'DATE'
EXEC utility.AddColumn @TableName, 'StipendID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'StipendID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.ContactStipendPayment
DECLARE @TableName VARCHAR(250) = 'dbo.ContactStipendPayment'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactStipendPayment
	(
	ContactStipendPaymentID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	StipendAmountAuthorized NUMERIC(18,2),
	StipendAmountPaid NUMERIC(18,2),
	StipendName VARCHAR(50),
	PaymentDate DATE,
	CommunityID INT, 
	ProvinceID INT, 
	Comments VARCHAR(MAX)
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StipendAmountAuthorized', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'StipendAmountPaid', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactStipendPaymentID'
EXEC utility.SetIndexClustered 'IX_ContactStipendPayment', @TableName, 'PaymentDate DESC,ContactID,CommunityID,ProvinceID'
GO
--End table dbo.ContactStipendPayment
	 
--Begin table dropdown.ImpactDecision
DECLARE @TableName VARCHAR(250) = 'dropdown.ImpactDecision'

EXEC utility.AddColumn @TableName, 'IsStipendEligible', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsStipendEligible', 'BIT', 1
GO

UPDATE dropdown.ImpactDecision
SET IsStipendEligible = 0
WHERE HexColor = '#FF0000'
GO
--End table dropdown.ImpactDecision
	 
--Begin table dropdown.Stipend 
DECLARE @TableName VARCHAR(250) = 'dropdown.Stipend'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Stipend 
	(
	StipendID INT IDENTITY(0,1) NOT NULL,
	StipendAmount NUMERIC(18,2),
	StipendName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'StipendAmount', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'StipendID'
EXEC utility.SetIndexClustered 'IX_Stipend', @TableName, 'DisplayOrder,StipendName,StipendID'
GO
--End table dropdown.Stipend 

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'ExpirationDate', 'DATE'
GO
--End table procurement.EquipmentInventory

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.GetContactLocationByContactID
EXEC utility.DropObject 'dbo.GetContactLocationByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to return the name of a contact's community or province from a ContactID
-- ================================================================================================

CREATE FUNCTION dbo.GetContactLocationByContactID
(
@ContactID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)
	
	SELECT @cRetVal = 

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END
		
	FROM dbo.Contact C1
	WHERE C1.COntactID = @ContactID
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.GetContactLocationByContactID

--Begin function dbo.IsCommunityStipendEligible
EXEC utility.DropObject 'dbo.IsCommunityStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to determine if a community is stipend eligible
-- =======================================================================

CREATE FUNCTION dbo.IsCommunityStipendEligible
(
@CommunityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 1
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Community C ON C.ImpactDecisionID = ID.ImpactDecisionID
			AND C.CommunityID = @CommunityID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsCommunityStipendEligible

--Begin function dbo.IsProvinceStipendEligible
EXEC utility.DropObject 'dbo.IsProvinceStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to determine if a province is stipend eligible
-- ======================================================================

CREATE FUNCTION dbo.IsProvinceStipendEligible
(
@ProvinceID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 1
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Province P ON P.ImpactDecisionID = ID.ImpactDecisionID
			AND P.ProvinceID = @ProvinceID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsProvinceStipendEligible
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetClassByClassID
EXEC Utility.DropObject 'dbo.GetClassByClassID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to data from the dbo.Class table
--
-- Author:			Todd Pires
-- Update date:	2015.02.13
-- Description:	Made the community name a subselect
--
-- Author:			Greg Yingling
-- Update date:	2015.05.16
-- Description:	Added QualityAssurance field, removed ExternalCapacity Field, added Document query
--
-- Author:			Greg Yingling
-- Update date:	2015.05.21
-- Description:	Attach ConceptNoteID
--
-- Author:			Todd Pires
-- Update date:	2015.05.28
-- Description:	Added vetting data
-- ===============================================================================================
CREATE PROCEDURE dbo.GetClassByClassID

@ClassID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CL.ClassID,
		CL.ClassPointOfContact,
		CL.CommunityID,
		(SELECT CM.CommunityName FROM dbo.Community CM WHERE CM.CommunityID = CL.CommunityID) AS CommunityName,
		CL.EndDate,
		dbo.FormatDate(CL.EndDate) AS EndDateFormatted,
		CL.Instructor1,
		CL.Instructor1Comments,
		CL.Instructor2,
		CL.Instructor2Comments,
		CL.Location,
		CL.Seats,
		CL.StartDate,
		dbo.FormatDate(CL.StartDate) AS StartDateFormatted,
		CL.StudentFeedbackSummary,
		CL.QualityAssuranceFeedback,
		CO.CourseID,
		CO.CourseName,
		ISNULL(CNC2.ConceptNoteID, 0) AS ConceptNoteID
	FROM dbo.Class CL
		JOIN dbo.Course CO ON CO.CourseID = CL.CourseID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT CNC1.ConceptNoteID 
			FROM dbo.ConceptNoteClass CNC1
			WHERE CNC1.ClassID = @ClassID
			) CNC2
	
	SELECT
		dbo.GetContactLocationByContactID(CO.ContactID) AS ContactLocation,
		CO.ContactID,
		CNC2.VettingIcon,
		CNC2.VettingOutcomeName,
		dbo.FormatDate(CO.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(CO.FirstName, CO.LastName, NULL, 'LastFirst') AS FullName
	FROM dbo.ClassContact CC
		JOIN dbo.Class CL ON CL.ClassID = CC.ClassID
		JOIN dbo.Contact CO ON CO.ContactID = CC.ContactID
			AND CL.ClassID = @ClassID
		OUTER APPLY 
			(
			SELECT 
				'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-circle.png" /> ' AS VettingIcon,
				VO.VettingOutcomeName
			FROM dbo.ConceptNoteContact CNC1
				JOIN dbo.ConceptNoteClass CNC2 ON CNC2.ClassID = CL.ClassID
				JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC1.VettingOutcomeID
					AND CNC1.ContactID = CC.ContactID
					AND CNC1.ConceptNoteID = CNC2.ConceptNoteID
			) CNC2
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'Class'
			AND DE.EntityID = @ClassID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure dbo.GetClassByClassID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatPersonName(C1.FirstName, C1.LastName, NULL, 'LastFirst') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue AS QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetConceptNoteEquipmentByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteEquipmentByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Kevin Ross
-- Create Date: 2015.03.29
-- Description:	A stored procedure to get data from the dbo.ConceptNoteEquipmentCatalog table
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
-- ==========================================================================================
CREATE PROCEDURE dbo.GetConceptNoteEquipmentByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue AS QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.UnitOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.UnitOfIssue, 'C', 'en-us') AS TotalCostFormatted
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
	
END
GO
--End procedure dbo.GetConceptNoteEquipmentByConceptNoteID

--Begin procedure dbo.GetContactByContactID
EXEC Utility.DropObject 'dbo.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.23
-- Description:	A stored procedure to data from the dbo.Contact table
--
-- Author:			Todd Pires
-- Create date:	2015.03.14
-- Description:	Implemented concept note level vetting
--
-- Author:			Todd Pires
-- Create date:	2015.05.27
-- Description:	Split the arabic name column into first, middle & last
--
-- Author:			Todd Pires
-- Create date:	2015.05.30
-- Description:	Added the Pasport Expiration & Facebook URL fields
--
-- Author:			Todd Pires
-- Create date:	2015.06.06
-- Description:	Added the IsActive field
--
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	Added stipend support
-- ===================================================================
CREATE PROCEDURE dbo.GetContactByContactID

@ContactID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C1.Address1,
		C1.Address2,
		C1.Aliases,
		C1.ArabicFirstName,
		C1.ArabicMiddleName,
		C1.ArabicLastName,
		C1.CellPhoneNumber,
		C1.City,
		C1.ContactID,
		C1.CommunityID,
		C1.DateOfBirth,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		C1.EmailAddress1,
		C1.EmailAddress2,
		C1.EmployerName,
		C1.FaceBookPageURL,
		C1.FaxNumber,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.IsActive,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		C1.PassportNumber,
		C1.PassportExpirationDate,
		dbo.FormatDate(C1.PassportExpirationDate) AS PassportExpirationDateFormatted,
		C1.PhoneNumber,
		C1.PlaceOfBirth,
		C1.PostalCode,
		C1.Profession,
		C1.ProvinceID,
		C1.SkypeUserName,
		C1.StartDate,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		C1.State,
		C1.Title,
		C2.CountryID AS CitizenshipCountryID1,
		C2.CountryName AS CitizenshipCountryName1,
		C3.CountryID AS CitizenshipCountryID2,
		C3.CountryName AS CitizenshipCountryName2,
		(SELECT C4.CommunityName FROM dbo.Community C4 WHERE C4.CommunityID = C1.CommunityID) AS CommunityName,
		C5.CountryID,
		C5.CountryName,
		C6.CountryID AS GovernmentIDNumberCountryID,
		C6.CountryName AS GovernmentIDNumberCountryName,
		P1.ProjectID,
		P1.ProjectName,
		(SELECT P2.ProvinceName FROM dbo.Province P2 WHERE P2.ProvinceID = C1.ProvinceID) AS ProvinceName,
		S.StipendID,
		S.StipendName,
		dbo.GetEntityTypeNameByEntityTypeCode('Contact') AS EntityTypeName
	FROM dbo.Contact C1
		JOIN dropdown.Country C2 ON C2.CountryID = C1.CitizenshipCountryID1
		JOIN dropdown.Country C3 ON C3.CountryID = C1.CitizenshipCountryID2
		JOIN dropdown.Country C5 ON C5.CountryID = C1.CountryID
		JOIN dropdown.Country C6 ON C6.CountryID = C1.GovernmentIDNumberCountryID
		JOIN dropdown.Project P1 ON P1.ProjectID = C1.ProjectID
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
			AND C1.ContactID = @ContactID

	SELECT
		CN.Title,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = CNC.ConceptNoteID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ContactID = @ContactID

	SELECT
		CA.ContactAffiliationID,
		CA.ContactAffiliationName
	FROM dbo.ContactContactAffiliation CCA
		JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
			AND CCA.ContactID = @ContactID

	SELECT
		CT.ContactTypeID,
		CT.ContactTypeCode,
		CT.ContactTypeName
	FROM dbo.ContactContactType CCT
		JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
			AND CCT.ContactID = @ContactID

	SELECT
		CV.VettingDate,
		dbo.FormatDate(CV.VettingDate) AS VettingDateFormatted,
		'<img src="/assets/img/icons/' + REPLACE(VO.HexColor, '#', '') + '-circle.png" /> ' AS VettingIcon,
		VO.VettingOutcomeName
	FROM dbo.ContactVetting CV
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CV.VettingOutcomeID
			AND CV.ContactID = @ContactID		
	ORDER BY CV.VettingDate DESC
	
END
GO
--End procedure dbo.GetContactByContactID

--Begin procedure dropdown.GetContactTypeData
EXEC Utility.DropObject 'dropdown.GetContactTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.18
-- Description:	A stored procedure to return data from the dropdown.ContactType table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetContactTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactTypeID, 
		T.ContactTypeCode, 
		T.ContactTypeName
	FROM dropdown.ContactType T
	WHERE (T.ContactTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactTypeName, T.ContactTypeID

END
GO
--End procedure dropdown.GetContactTypeData

--Begin procedure dropdown.GetStipendData
EXEC Utility.DropObject 'dropdown.GetStipendData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data from the dropdown.Stipend table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetStipendData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		S.StipendID,
		S.StipendAmount,
		S.StipendName
	FROM dropdown.Stipend S
	ORDER BY S.DisplayOrder, S. StipendName, S.StipendID

END
GO
--End procedure dropdown.GetStipendData


--Begin procedure procurement.GetEquipmentInventoryByEquipmentInventoryID
EXEC Utility.DropObject 'procurement.GetEquipmentInventoryByEquipmentInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================

-- Author:			Todd Pires
-- Create date:	2015.03.18
-- Description:	A stored procedure to data from the procurement.EquipmentInventory table
--
-- Author:			Todd Pires
-- Create date:	2015.03.29
-- Description:	Added the EquipmentCatalogID
--
-- Author:			Greg Yingling
-- Update date:	2015.05.18
-- Description:	Added the Equipment Status and Various Equipment Removal Fields, added Document Call
-- =================================================================================================
CREATE PROCEDURE procurement.GetEquipmentInventoryByEquipmentInventoryID

@EquipmentInventoryID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		D.ConceptNoteID, 
		D.Title, 
		EC.EquipmentCatalogID,
		EC.ItemDescription,
		EC.ItemName,
		EI.BudgetCode,
		EI.Comments,
		EI.EquipmentInventoryID,
		EI.ExpirationDate,
		dbo.FormatDate(EI.ExpirationDate) AS ExpirationDateFormatted,
		EI.IMEIMACAddress,
		EI.UnitCost,
		EI.Quantity,
		EI.SerialNumber,
		EI.SIM,
		EI.Supplier,
		EI.EquipmentStatusID,
		ES.EquipmentStatusName,
		EI.EquipmentRemovalReasonID,
		ER.EquipmentRemovalReasonName,
		EI.EquipmentRemovalDate,
		dbo.FormatDate(EI.EquipmentRemovalDate) AS EquipmentRemovalDateFormatted,
		EI.EquipmentRemovalReporterPersonID,
		dbo.FormatPersonNameByPersonID(EquipmentRemovalReporterPersonID, 'LastFirst') AS EquipmentRemovalReporterNameFormatted,
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentInventory') AS EntityTypeName
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID
		JOIN dropdown.EquipmentRemovalReason ER ON ER.EquipmentRemovalReasonID = EI.EquipmentRemovalReasonID
		OUTER APPLY
			(
			SELECT
				CN.ConceptNoteID,
				CN.Title
			FROM dbo.ConceptNote CN
			WHERE CN.ConceptNoteID = EI.ConceptNoteID
			) D 
	WHERE EI.EquipmentInventoryID = @EquipmentInventoryID
		
	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'EquipmentInventory'
			AND DE.EntityID = @EquipmentInventoryID
	ORDER BY DT.DocumentTypeName

END
GO
--End procedure procurement.GetEquipmentInventoryByEquipmentInventoryID

--Begin procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID
EXEC Utility.DropObject 'reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.31
-- Description:	A stored procedure to get data for the Concept Note report
-- =======================================================================
CREATE PROCEDURE reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dbo.GetContactLocationByContactID(CON.ContactID) AS Location,
		CON.ContactID,
		CON.EmployerName,
		CN.ConceptNoteID,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		dbo.FormatConceptNoteTitle(CN.ConceptNoteID) AS ConceptNoteTitle,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ConceptNoteReferenceCode,
		dbo.FormatContactNameByContactID(CON.ContactID, 'LastFirst') AS Fullname
	FROM
		(
		SELECT DISTINCT
			CNCE.ConceptNoteID,
			CNCE.ContactID
		FROM dbo.ConceptNoteContactEquipment CNCE
		WHERE CNCE.ConceptNoteID = @ConceptNoteID
		) D
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = D.ConceptNoteID
		JOIN dbo.Contact CON ON CON.ContactID = D.ContactID
	ORDER BY Fullname, D.ContactID

END
GO
--End procedure reporting.GetConceptNoteContactEquipmentContactsByConceptNoteID

--Begin procedure reporting.GetEquipmentDistributionByProvinceIDOrCommunityID
EXEC Utility.DropObject 'reporting.GetEquipmentDistributionByProvinceIDOrCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create date:	2015.06.18
-- Description:	A stored procedure to data from the EquipmentDistribution
-- ======================================================================
CREATE PROCEDURE reporting.GetEquipmentDistributionByProvinceIDOrCommunityID

@CommunityID INT = 0, 
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		P.ProvinceName AS CommunityOrProvinceName, 
		EC.ItemName, 
		EC.ItemDescription, 
		EI.SerialNumber, 
		dbo.FormatDate(PEI.DistributionDate) AS DistributionDate, 
		ES.EquipmentStatusName
	FROM procurement.ProvinceEquipmentInventory PEI
		JOIN dbo.Province P ON P.ProvinceID = PEI.ProvinceID 
			AND P.ProvinceID = @ProvinceID
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = PEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EI.EquipmentCatalogID = EC.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID

	UNION ALL

	SELECT 
		C.CommunityName AS CommunityOrProvinceName, 
		EC.ItemName, 
		EC.ItemDescription, 
		EI.SerialNumber, 
		dbo.FormatDate(CEI.DistributionDate) AS DistributionDate, 
		ES.EquipmentStatusName
	FROM procurement.CommunityEquipmentInventory CEI
		JOIN dbo.Community C ON C.CommunityID = CEI.CommunityID 
			AND C.CommunityID = @CommunityID
		JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = CEI.EquipmentInventoryID
		JOIN procurement.EquipmentCatalog EC ON EI.EquipmentCatalogID = EC.EquipmentCatalogID
		JOIN dropdown.EquipmentStatus ES ON ES.EquipmentStatusID = EI.EquipmentStatusID

END
GO
--End procedure reporting.GetEquipmentDistributionByProvinceIDOrCommunityID

--Begin procedure reporting.GetStipendPaymentsByProvince
EXEC Utility.DropObject 'reporting.GetStipendPaymentsByProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data for stipend payments
-- ===================================================================
CREATE PROCEDURE reporting.GetStipendPaymentsByProvince

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C1.CellPhoneNumber,
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		C1.FirstName,
		C1.Gender,
		C1.GovernmentIDNumber,
		C1.LastName,
		C1.MiddleName,
		C1.MotherName,
		dbo.FormatDate(C1.StartDate) AS StartDateFormatted,
		
		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Community C2 JOIN dbo.Province P ON P.ProvinceID = C2.ProvinceID AND C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END AS ProvinceName,
		
		CASE
			WHEN C1.CommunityID > 0 AND dbo.IsCommunityStipendEligible(C1.CommunityID) = 1
			THEN S.StipendAmount
			WHEN C1.ProvinceID > 0 AND dbo.IsProvinceStipendEligible(C1.ProvinceID) = 1
			THEN S.StipendAmount
			ELSE 0
		END AS StipendAmount,
			
		S.StipendName 
	FROM dbo.Contact C1
		JOIN dropdown.Stipend S ON S.StipendID = C1.StipendID
		JOIN reporting.SearchResult RSR ON RSR.EntityID = C1.ContactID
			AND RSR.EntityTypeCode = 'Contact'
			AND RSR.PersonID = @PersonID
	ORDER BY C1.LastName, C1.FirstName, C1.MiddleName, C1.ContactID

END
GO
--End procedure reporting.GetStipendPaymentsByProvince

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Stipends', @NewMenuItemText='Stipends', @AfterMenuItemCode='Contacts', @Icon='fa fa-fw fa-money', @PermissionableLineageList='Contact.List.Stipend,Contact.List.Payment'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StipendList', @NewMenuItemText='Recipients', @ParentMenuItemCode='Stipends', @PermissionableLineageList='Contact.List.Stipend'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PaymentList', @NewMenuItemText='Payments', @NewMenuItemLink='/payment/list', @ParentMenuItemCode='Stipends', @AfterMenuItemCode='StipendList', @PermissionableLineageList='Contact.List.Payment'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Contacts', @PermissionableLineageList='Contact.List.Beneficiary,Contact.List.StaffPartner,Contact.List.StaffPartnerVetting,SubContractor.List'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='BeneficiaryList', @PermissionableLineageList='Contact.List.Beneficiary'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerList', @PermissionableLineageList='Contact.List.StaffPartner'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerVettingList', @PermissionableLineageList='Contact.List.StaffPartnerVetting'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SubContractorList', @PermissionableLineageList='SubContractor.List'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Equipment', @NewMenuItemText='Equipment', @AfterMenuItemCode='Procurement', @Icon='fa fa-fw fa-cog', @PermissionableLineageList='EquipmentCatalog.List,EquipmentInventory.List'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentCatalogList', @ParentMenuItemCode='Equipment'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentInventoryList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentCatalogList'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Activity', @NewMenuItemText='Activities', @AfterMenuItemCode='Stipends', @Icon='fa fa-fw fa-pencil-square-o', @PermissionableLineageList='ConceptNote.List,ConceptNoteContactEquipment.List,ConceptNote.Vetting'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNote', @ParentMenuItemCode='Activity', @Icon=''
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactEquipmentList', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNote'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactVettingList', @NewMenuItemText='Activity Vetting', @NewMenuItemLink='/conceptnote/list?ConceptNoteListMode=ConceptNoteContactVetting', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNoteContactEquipmentList', @PermissionableLineageList='ConceptNote.Vetting'
GO
--End table dbo.MenuItem

--Begin table dropdown.Stipend 
TRUNCATE TABLE dropdown.Stipend
GO

SET IDENTITY_INSERT dropdown.Stipend ON
GO

INSERT INTO dropdown.Stipend (StipendID) VALUES (0)

SET IDENTITY_INSERT dropdown.Stipend OFF
GO

INSERT INTO dropdown.Stipend
	(StipendName,StipendAmount,DisplayOrder)
VALUES
	('Brigadier',300,1),
	('Colonel',300,2),
	('Lieutenant Colonel',300,3),
	('Major',300,4),
	('Captain',300,5),
	('Sergeant Major',150,6),
	('Sergeant',150,7),
	('Policeman',100,8),
	('Policeman Contractor',100,9),
	('Suspended',0,10)
GO	
--End table dropdown.Stipend 

--Begin table permissionable.Permissionable
DECLARE @nPermissionableID INT

SELECT @nPermissionableID = P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'ConceptNote.View.ExportVetting'

SET @nPermissionableID = ISNULL(@nPermissionableID, 0)

IF @nPermissionableID > 0
	BEGIN

	UPDATE permissionable.Permissionable
	SET 
		PermissionableCode = 'Vetting',
		PermissionableName = 'Vetting',
		ParentPermissionableID = (SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ConceptNote')
	WHERE PermissionableID = @nPermissionableID

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Payment',
	'Payment',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.Payment'
		)
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'Chris',
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'Chris',
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.17 File 01 - AJACS - 2015.06.20 20.32.21')
GO
--End build tracking

