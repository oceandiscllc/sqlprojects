USE AJACS
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Stipends', @NewMenuItemText='Stipends', @AfterMenuItemCode='Contacts', @Icon='fa fa-fw fa-money', @PermissionableLineageList='Contact.List.Stipend,Contact.List.Payment'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StipendList', @NewMenuItemText='Recipients', @ParentMenuItemCode='Stipends', @PermissionableLineageList='Contact.List.Stipend'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='PaymentList', @NewMenuItemText='Payments', @NewMenuItemLink='/payment/list', @ParentMenuItemCode='Stipends', @AfterMenuItemCode='StipendList', @PermissionableLineageList='Contact.List.Payment'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Contacts', @PermissionableLineageList='Contact.List.Beneficiary,Contact.List.StaffPartner,Contact.List.StaffPartnerVetting,SubContractor.List'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='BeneficiaryList', @PermissionableLineageList='Contact.List.Beneficiary'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerList', @PermissionableLineageList='Contact.List.StaffPartner'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='StaffPartnerVettingList', @PermissionableLineageList='Contact.List.StaffPartnerVetting'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SubContractorList', @PermissionableLineageList='SubContractor.List'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Equipment', @NewMenuItemText='Equipment', @AfterMenuItemCode='Procurement', @Icon='fa fa-fw fa-cog', @PermissionableLineageList='EquipmentCatalog.List,EquipmentInventory.List'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentCatalogList', @ParentMenuItemCode='Equipment'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='EquipmentInventoryList', @ParentMenuItemCode='Equipment', @AfterMenuItemCode='EquipmentCatalogList'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Activity', @NewMenuItemText='Activities', @AfterMenuItemCode='Stipends', @Icon='fa fa-fw fa-pencil-square-o', @PermissionableLineageList='ConceptNote.List,ConceptNoteContactEquipment.List,ConceptNote.Vetting'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNote', @ParentMenuItemCode='Activity', @Icon=''
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactEquipmentList', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNote'
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='ConceptNoteContactVettingList', @NewMenuItemText='Activity Vetting', @NewMenuItemLink='/conceptnote/list?ConceptNoteListMode=ConceptNoteContactVetting', @ParentMenuItemCode='Activity', @AfterMenuItemCode='ConceptNoteContactEquipmentList', @PermissionableLineageList='ConceptNote.Vetting'
GO
--End table dbo.MenuItem

--Begin table dropdown.Stipend 
TRUNCATE TABLE dropdown.Stipend
GO

SET IDENTITY_INSERT dropdown.Stipend ON
GO

INSERT INTO dropdown.Stipend (StipendID) VALUES (0)

SET IDENTITY_INSERT dropdown.Stipend OFF
GO

INSERT INTO dropdown.Stipend
	(StipendName,StipendAmount,DisplayOrder)
VALUES
	('Brigadier',300,1),
	('Colonel',300,2),
	('Lieutenant Colonel',300,3),
	('Major',300,4),
	('Captain',300,5),
	('Sergeant Major',150,6),
	('Sergeant',150,7),
	('Policeman',100,8),
	('Policeman Contractor',100,9),
	('Suspended',0,10)
GO	
--End table dropdown.Stipend 

--Begin table permissionable.Permissionable
DECLARE @nPermissionableID INT

SELECT @nPermissionableID = P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'ConceptNote.View.ExportVetting'

SET @nPermissionableID = ISNULL(@nPermissionableID, 0)

IF @nPermissionableID > 0
	BEGIN

	UPDATE permissionable.Permissionable
	SET 
		PermissionableCode = 'Vetting',
		PermissionableName = 'Vetting',
		ParentPermissionableID = (SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ConceptNote')
	WHERE PermissionableID = @nPermissionableID

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'Payment',
	'Payment',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'Contact.List'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'Contact.List.Payment'
		)
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'Chris',
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'Chris',
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable