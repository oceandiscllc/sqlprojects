USE AJACS
GO

--Begin table dbo.ConceptNote
ALTER TABLE dbo.ConceptNote ALTER COLUMN BrandingRequirements VARCHAR(MAX)
ALTER TABLE dbo.ConceptNote ALTER COLUMN RiskAssessment VARCHAR(MAX)
ALTER TABLE dbo.ConceptNote ALTER COLUMN RiskMitigationMeasures VARCHAR(MAX)
GO
--End table dbo.ConceptNote
 
--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'MotherName', 'NVARCHAR(200)'
EXEC utility.AddColumn @TableName, 'StartDate', 'DATE'
EXEC utility.AddColumn @TableName, 'StipendID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'StipendID', 'INT', 0
GO
--End table dbo.Contact

--Begin table dbo.ContactStipendPayment
DECLARE @TableName VARCHAR(250) = 'dbo.ContactStipendPayment'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.ContactStipendPayment
	(
	ContactStipendPaymentID INT IDENTITY(1,1) NOT NULL,
	ContactID INT,
	StipendAmountAuthorized NUMERIC(18,2),
	StipendAmountPaid NUMERIC(18,2),
	StipendName VARCHAR(50),
	PaymentDate DATE,
	CommunityID INT, 
	ProvinceID INT, 
	Comments VARCHAR(MAX)
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'StipendAmountAuthorized', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'StipendAmountPaid', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactStipendPaymentID'
EXEC utility.SetIndexClustered 'IX_ContactStipendPayment', @TableName, 'PaymentDate DESC,ContactID,CommunityID,ProvinceID'
GO
--End table dbo.ContactStipendPayment
	 
--Begin table dropdown.ImpactDecision
DECLARE @TableName VARCHAR(250) = 'dropdown.ImpactDecision'

EXEC utility.AddColumn @TableName, 'IsStipendEligible', 'BIT'

EXEC utility.SetDefaultConstraint @TableName, 'IsStipendEligible', 'BIT', 1
GO

UPDATE dropdown.ImpactDecision
SET IsStipendEligible = 0
WHERE HexColor = '#FF0000'
GO
--End table dropdown.ImpactDecision
	 
--Begin table dropdown.Stipend 
DECLARE @TableName VARCHAR(250) = 'dropdown.Stipend'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Stipend 
	(
	StipendID INT IDENTITY(0,1) NOT NULL,
	StipendAmount NUMERIC(18,2),
	StipendName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1
EXEC utility.SetDefaultConstraint @TableName, 'StipendAmount', 'NUMERIC(18,2)', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'StipendID'
EXEC utility.SetIndexClustered 'IX_Stipend', @TableName, 'DisplayOrder,StipendName,StipendID'
GO
--End table dropdown.Stipend 

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'ExpirationDate', 'DATE'
GO
--End table procurement.EquipmentInventory
