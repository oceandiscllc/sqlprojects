USE AJACS
GO

--Begin function dbo.GetContactLocationByContactID
EXEC utility.DropObject 'dbo.GetContactLocationByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to return the name of a contact's community or province from a ContactID
-- ================================================================================================

CREATE FUNCTION dbo.GetContactLocationByContactID
(
@ContactID INT
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)
	
	SELECT @cRetVal = 

		CASE
			WHEN C1.CommunityID > 0
			THEN (SELECT C2.CommunityName FROM dbo.Community C2 WHERE C2.CommunityID = C1.CommunityID)
			WHEN C1.ProvinceID > 0
			THEN (SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = C1.ProvinceID)
			ELSE ''
		END
		
	FROM dbo.Contact C1
	WHERE C1.COntactID = @ContactID
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function dbo.GetContactLocationByContactID

--Begin function dbo.IsCommunityStipendEligible
EXEC utility.DropObject 'dbo.IsCommunityStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to determine if a community is stipend eligible
-- =======================================================================

CREATE FUNCTION dbo.IsCommunityStipendEligible
(
@CommunityID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 1
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Community C ON C.ImpactDecisionID = ID.ImpactDecisionID
			AND C.CommunityID = @CommunityID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsCommunityStipendEligible

--Begin function dbo.IsProvinceStipendEligible
EXEC utility.DropObject 'dbo.IsProvinceStipendEligible'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A function to determine if a province is stipend eligible
-- ======================================================================

CREATE FUNCTION dbo.IsProvinceStipendEligible
(
@ProvinceID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bRetVal BIT = 1
	
	SELECT 
		@bRetVal = ID.IsStipendEligible
	FROM dropdown.ImpactDecision ID
		JOIN dbo.Province P ON P.ImpactDecisionID = ID.ImpactDecisionID
			AND P.ProvinceID = @ProvinceID
	
	RETURN @bRetVal

END
GO
--End function dbo.IsProvinceStipendEligible