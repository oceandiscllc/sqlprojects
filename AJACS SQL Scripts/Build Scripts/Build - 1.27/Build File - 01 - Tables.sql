USE AJACS
GO

--Begin table dbo.CommunityAsset
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAsset'

EXEC utility.AddColumn @TableName, 'CommunityID', 'INT'
EXEC utility.AddColumn @TableName, 'ProvinceID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
GO
--End table dbo.CommunityAsset

--Begin table dbo.CommunityAssetCommunity / dbo.CommunityAssetProvince
EXEC utility.DropObject 'dbo.CommunityAssetCommunity'
EXEC utility.DropObject 'dbo.CommunityAssetProvince'
GO
--End table dbo.CommunityAssetCommunity / dbo.CommunityAssetProvince
