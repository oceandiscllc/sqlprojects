-- File Name:	Build-1.27 File 01 - AJACS.sql
-- Build Key:	Build-1.27 File 01 - AJACS - 2015.08.31 20.38.47

USE 
GO

-- ==============================================================================================================================
-- Procedures:
--		dbo.GetCommunityAssetByCommunityAssetID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.CommunityAsset
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAsset'

EXEC utility.AddColumn @TableName, 'CommunityID', 'INT'
EXEC utility.AddColumn @TableName, 'ProvinceID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
GO
--End table dbo.CommunityAsset

--Begin table dbo.CommunityAssetCommunity / dbo.CommunityAssetProvince
EXEC utility.DropObject 'dbo.CommunityAssetCommunity'
EXEC utility.DropObject 'dbo.CommunityAssetProvince'
GO
--End table dbo.CommunityAssetCommunity / dbo.CommunityAssetProvince

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetCommunityAssetByCommunityAssetID
EXEC Utility.DropObject 'dbo.GetCommunityAssetByCommunityAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to get data from the dbo.CommunityAsset table
--
-- Author:			Kevin Ross
-- Create date: 2015.08.31
-- Description:	Moved CommunityID and ProvinceID back into the dbo.CommunityAsset table
-- ====================================================================================
CREATE PROCEDURE dbo.GetCommunityAssetByCommunityAssetID

@CommunityAssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CA.CommunityAssetDescription,
		CA.Location.STAsText() AS Location,
		CA.CommunityID,
		(SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CA.CommunityID) AS CommunityName,
		CA.ProvinceID,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CA.ProvinceID) AS ProvinceName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CA.CommunityAssetID = @CommunityAssetID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.CommunityAssetRisk CAR ON CAR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CAR.CommunityAssetID = @CommunityAssetID
	
	SELECT
		CAU.CommunityAssetUnitID,
		CAU.CommunityAssetUnitName,
		CAUT.CommunityAssetUnitTypeID,
		CAUT.CommunityAssetUnitTypeName,
		CAUC.CommunityAssetUnitCostRateID,
		CAUC.CommunityAssetUnitCostRate
	FROM dbo.CommunityAssetUnit CAU
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
		JOIN dropdown.CommunityAssetUnitCostRate CAUC ON CAUC.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
			AND CAU.CommunityAssetID = @CommunityAssetID

END
GO
--End procedure dbo.GetCommunityAssetByCommunityAssetID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'bgreen',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'bgreen',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.27 File 01 - AJACS - 2015.08.31 20.38.47')
GO
--End build tracking

