USE AJACS
GO

--Begin procedure dbo.GetCommunityAssetByCommunityAssetID
EXEC Utility.DropObject 'dbo.GetCommunityAssetByCommunityAssetID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Kevin Ross
-- Create date: 2015.08.20
-- Description:	A stored procedure to get data from the dbo.CommunityAsset table
--
-- Author:			Kevin Ross
-- Create date: 2015.08.31
-- Description:	Moved CommunityID and ProvinceID back into the dbo.CommunityAsset table
-- ====================================================================================
CREATE PROCEDURE dbo.GetCommunityAssetByCommunityAssetID

@CommunityAssetID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CA.CommunityAssetDescription,
		CA.Location.STAsText() AS Location,
		CA.CommunityID,
		(SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = CA.CommunityID) AS CommunityName,
		CA.ProvinceID,
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = CA.ProvinceID) AS ProvinceName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		AT.Icon,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CA.CommunityAssetID = @CommunityAssetID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryID,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.CommunityAssetRisk CAR ON CAR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CAR.CommunityAssetID = @CommunityAssetID
	
	SELECT
		CAU.CommunityAssetUnitID,
		CAU.CommunityAssetUnitName,
		CAUT.CommunityAssetUnitTypeID,
		CAUT.CommunityAssetUnitTypeName,
		CAUC.CommunityAssetUnitCostRateID,
		CAUC.CommunityAssetUnitCostRate
	FROM dbo.CommunityAssetUnit CAU
		JOIN dropdown.CommunityAssetUnitType CAUT ON CAUT.CommunityAssetUnitTypeID = CAU.CommunityAssetUnitTypeID
		JOIN dropdown.CommunityAssetUnitCostRate CAUC ON CAUC.CommunityAssetUnitCostRateID = CAU.CommunityAssetUnitCostRateID
			AND CAU.CommunityAssetID = @CommunityAssetID

END
GO
--End procedure dbo.GetCommunityAssetByCommunityAssetID