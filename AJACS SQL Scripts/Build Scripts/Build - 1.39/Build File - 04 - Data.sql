USE AJACS
GO

--Begin table dbo.MenuItem

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Equipment', @NewMenuItemCode='EquipmentManagement', @NewMenuItemLink='/equipmentmanagement/list', @NewMenuItemText='Equipment Management', @BeforeMenuItemCode='ConceptNoteContactEquipmentList', @PermissionableLineageList='EquipmentManagement.List'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='DonorDecision', @NewMenuItemLink='/donordecision/list', @NewMenuItemText='Donor Logs', @BeforeMenuItemCode='EventLogList', @PermissionableLineageList='DonorDecision.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		0,
		'EquipmentManagement',
		'Equipment Management'
		)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement'),	'List',	'List Equipment Locations'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement'),	'Audit', 'Audit Equipment'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement'),	'Transfer',	'Transfer Equipment Between Locations')
		
	INSERT INTO permissionable.DisplayGroupPermissionable
		(DisplayGroupID, PermissionableID)
	SELECT
		DG.DisplayGroupID,
		(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement')
	FROM permissionable.DisplayGroup DG
	WHERE DG.DisplayGroupCode = 'Procurement'
		
	END
--ENDIF
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'Transfer'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision')
	BEGIN

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		0,
		'DonorDecision',
		'Donor Decisions, Meetings & Actions'
		)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'List',	'List Donor Decisions, Meetings & Actions'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'AddUpdateDecision', 'Add / Edit Donor Decisions'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'AddUpdateMeeting',	'Add / Edit Donor Meetings & Actions')

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DonorDecision')
	BEGIN

	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode, DisplayGroupName, DisplayOrder)
	VALUES
		(
		'DonorDecision',
		'Donor Decisions, Meetings & Actions',
		(SELECT MAX(DG.DisplayOrder) + 1 FROM permissionable.DisplayGroup DG)
		)
		
	INSERT INTO permissionable.DisplayGroupPermissionable
		(DisplayGroupID, PermissionableID)
	SELECT
		DG.DisplayGroupID,
		(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision')
	FROM permissionable.DisplayGroup DG
	WHERE DG.DisplayGroupCode = 'DonorDecision'
	
	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
