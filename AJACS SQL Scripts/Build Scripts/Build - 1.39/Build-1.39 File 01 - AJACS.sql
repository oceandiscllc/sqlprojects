-- File Name:	Build-1.39 File 01 - AJACS.sql
-- Build Key:	Build-1.39 File 01 - AJACS - 2015.11.27 23.10.22

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
--		dbo.FormatStaticGoogleMapForSpotReport
--		dbo.FormatStaticGoogleMapForWeeklyReport
--		procurement.GetLastEquipmentAuditDate
--		procurement.GetLastEquipmentAuditOutcome
--
-- Procedures:
--		communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity
--		communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince
--		communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
--		communityprovinceengagementupdate.GetCommunityByCommunityID
--		communityprovinceengagementupdate.GetProvinceByProvinceID
--		dbo.GetCommunityLocations
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.GetRequestForInformationByRequestForInformationID
--		dbo.GetSpotReportBySpotReportID
--		dropdown.GetDonorDecisionDecisionData
--		eventlog.LogCommunityProvinceEngagementAction
--		eventlog.LogContactAction
--		eventlog.LogPoliceEngagementAction
--		procurement.GetEquipmentCatalogByEquipmentCatalogID
--
-- Tables:
--		dropdown.DonorDecisionDecision
--		procurement.CommunityEquipmentInventoryAudit
--		procurement.ProvinceEquipmentInventoryAudit
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dbo.Document
DECLARE @TableName VARCHAR(250) = 'dbo.Document'

EXEC utility.DropColumn @TableName, 'Thumbnail'
EXEC utility.AddColumn @TableName, 'Thumbnail', 'VARBINARY(MAX)'
GO
--End table dbo.Document

--Begin table dropdown.AuditOutcome
IF NOT EXISTS (SELECT 1 FROM dropdown.AuditOutcome AO WHERE AO.AuditOutcomeName = 'Transferred')
	BEGIN
	
	INSERT INTO dropdown.AuditOutcome
		(AuditOutcomeName, DisplayOrder)
	SELECT
		'Transferred',
		MAX(AO.DisplayOrder) + 1
	FROM dropdown.AuditOutcome AO
	
	END
--ENDIF
GO
--End table dropdown.AuditOutcome

--Begin table dropdown.DonorDecisionDecision
DECLARE @TableName VARCHAR(250) = 'dropdown.DonorDecisionDecision'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.DonerDecisionDecision'

CREATE TABLE dropdown.DonorDecisionDecision
	(
	DonorDecisionDecisionID INT IDENTITY(0,1) NOT NULL,
	DonorDecisionDecisionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DonorDecisionDecisionID'
EXEC utility.SetIndexNonClustered 'IX_DonorDecisionDecision', @TableName, 'DisplayOrder,DonorDecisionDecisionName', 'DonorDecisionDecisionID'
GO

SET IDENTITY_INSERT dropdown.DonorDecisionDecision ON
GO

INSERT INTO dropdown.DonorDecisionDecision (DonorDecisionDecisionID) VALUES (0)

SET IDENTITY_INSERT dropdown.DonorDecisionDecision OFF
GO

INSERT INTO dropdown.DonorDecisionDecision 
	(DonorDecisionDecisionName, DisplayOrder)
VALUES
	('Awaits', 1),
	('Approved', 2),
	('Rejected', 3),
	('Deferred', 4)
GO
--End table dropdown.DonorDecisionDecision

--Begin table procurement.CommunityEquipmentInventoryAudit
DECLARE @TableName VARCHAR(250) = 'procurement.CommunityEquipmentInventoryAudit'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.CommunityEquipmentInventoryAudit
	(
	CommunityEquipmentInventoryAuditID INT IDENTITY(1,1) NOT NULL,
	CommunityEquipmentInventoryID INT,
	DocumentID INT,
	AuditOutcomeID INT,
	AuditQuantity INT,
	AuditDate DATE,
	PersonID INT,
	AuditNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuditDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AuditQuantity', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityEquipmentInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityEquipmentInventoryAuditID'
EXEC utility.SetIndexClustered 'IX_CommunityEquipmentInventoryAudit', @TableName, 'CommunityEquipmentInventoryID,AuditDate DESC'
GO
--End table procurement.CommunityEquipmentInventoryAudit

--Begin table procurement.EquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentCatalog'

EXEC utility.AddColumn @TableName, 'ItemMake', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'ItemModel', 'VARCHAR(250)'
GO
--End table procurement.EquipmentCatalog

--Begin table procurement.ProvinceEquipmentInventoryAudit
DECLARE @TableName VARCHAR(250) = 'procurement.ProvinceEquipmentInventoryAudit'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.ProvinceEquipmentInventoryAudit
	(
	ProvinceEquipmentInventoryAuditID INT IDENTITY(1,1) NOT NULL,
	ProvinceEquipmentInventoryID INT,
	DocumentID INT,
	AuditOutcomeID INT,
	AuditQuantity INT,
	AuditDate DATE,
	PersonID INT,
	AuditNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuditDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AuditQuantity', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceEquipmentInventoryID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceEquipmentInventoryAuditID'
EXEC utility.SetIndexClustered 'IX_ProvinceEquipmentInventoryAudit', @TableName, 'ProvinceEquipmentInventoryID,AuditDate DESC'
GO
--End table procurement.ProvinceEquipmentInventoryAudit

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@ConceptNoteID INT,
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(ISNULL(CT.Latitude, 0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(CT.Longitude,0) AS VARCHAR(MAX))
	FROM dbo.ConceptNote CN
	JOIN dbo.ConceptNoteContactEquipment CNCE ON CNCE.ConceptNoteID = CN.ConceptNoteID
		AND CN.ConceptNoteID = @ConceptNoteID
	JOIN dbo.Contact C1 ON C1.ContactID = CNCE.ContactID
	JOIN Community CT ON c1.CommunityID = ct.CommunityID  AND CT.ProvinceID = @ProvinceID
	JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CT.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / LEN('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan

--Begin function dbo.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	 ,@GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	 ,@CommunityMarker varchar(max)=''
	 ,@ProvinceMarker varchar(max)=''
	
	SELECT
		@CommunityMarker += '&markers=icon:' 
			+replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	 FROM SpotReportCommunity SRC
		JOIN dbo.Community C ON SRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND SRC.SpotReportID = @SpotReportID
		

	SELECT
		@ProvinceMarker += '&markers=icon:' 
			+ replace(dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + IT.Icon + '|' 
			+ CAST(ISNULL(I.Latitude,0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(I.Longitude,0) AS VARCHAR(MAX))
	 FROM SpotReportIncident SRI
		JOIN dbo.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	SET @Gresult = @ProvinceMarker + @CommunityMarker

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForSpotReport

--Begin function dbo.FormatStaticGoogleMapForWeeklyReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
--
-- Author:			John Lyons & Todd Pires
-- Create date:	2015.05.09
-- Description:	Implemented the date range and icon grouping
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForWeeklyReport
(
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=600x480'
	DECLARE @cMarkers VARCHAR(MAX) = ''
	
	;
	WITH D AS
		(
		SELECT
			REPLACE(ID.HexColor, '#', '') +  '.png' AS Icon,
			LEFT(CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM WeeklyReport.Community WRC
		JOIN dbo.Community C ON WRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.ProvinceID = @ProvinceID
	
		UNION
	
		SELECT
			it.icon AS Icon,
			LEFT(CAST(ISNULL(I.Latitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(ISNULL(I.Longitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM dbo.Incident I
		JOIN Dropdown.IncidentType IT ON i.incidenttypeid = it.incidenttypeid
			AND I.IncidentDate >= (SELECT TOP 1 weeklyreport.WeeklyReport.StartDate FROM weeklyreport.WeeklyReport)
			AND I.IncidentDate <= (SELECT TOP 1 weeklyreport.WeeklyReport.EndDate FROM weeklyreport.WeeklyReport)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.IncidentID = I.IncidentID 
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Community C 
						WHERE C.CommunityID = IC.CommunityID AND C.ProvinceID = @ProvinceID
						)
	
				UNION
	
				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.IncidentID = I.IncidentID AND  IP.ProvinceID = @ProvinceID
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Province P 
						WHERE P.ProvinceID = IP.ProvinceID AND P.ProvinceID = @ProvinceID
						)
				)
		)
	
	SELECT @cMarkers +=
		'&markers=icon:' 
		+ REPLACE(dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/'
		+ E.Icon
		+ (
			SELECT '|' + D.LatLong 
			FROM D
			WHERE D.Icon = E.Icon
			FOR XML PATH(''), TYPE
			).value('.[1]', 'VARCHAR(MAX)')
	FROM D AS E
	GROUP BY E.Icon
	ORDER BY E.Icon

	IF (LEN(@cMarkers) - LEN(REPLACE(@cMarkers, 'markers', ''))) / len('markers') <= 1
		SELECT @cLink += '&zoom=10'
	--ENDIF

	RETURN (@cLink + @cMarkers)

END
GO
--End function dbo.FormatStaticGoogleMapForWeeklyReport

--Begin function procurement.GetLastEquipmentAuditDate
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditDate'
GO

-- ================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last auditdate for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
-- ================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditDate
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryEquipmentInventoryID INT
)

RETURNS VARCHAR(11)

AS
BEGIN

	DECLARE @cLastAuditDateFormatted VARCHAR(11) = ''
	
	IF @TerritoryTypeCode = 'Community'
		BEGIN
		
		SELECT TOP 1 @cLastAuditDateFormatted = dbo.FormatDate(T.AuditDate)
		FROM procurement.CommunityEquipmentInventoryAudit T
		WHERE T.CommunityEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	ELSE IF @TerritoryTypeCode = 'Province'	
		BEGIN
		
		SELECT TOP 1 @cLastAuditDateFormatted = dbo.FormatDate(T.AuditDate)
		FROM procurement.ProvinceEquipmentInventoryAudit T
		WHERE T.ProvinceEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	--ENDIF
	
	RETURN ISNULL(@cLastAuditDateFormatted, '')

END
GO
--End function procurement.GetLastEquipmentAuditDate

--Begin function procurement.GetLastEquipmentAuditOutcome
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditOutcome'
GO

-- ====================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last Audit Outcome for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
-- ====================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditOutcome
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryEquipmentInventoryID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cLastAuditOutcomeName VARCHAR(50) = ''
	
	IF @TerritoryTypeCode = 'Community'
		BEGIN
		
		SELECT TOP 1 @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM procurement.CommunityEquipmentInventoryAudit T
			JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = T.AuditOutcomeID
				AND T.CommunityEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	ELSE IF @TerritoryTypeCode = 'Province'	
		BEGIN
		
		SELECT TOP 1 @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM procurement.ProvinceEquipmentInventoryAudit T
			JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = T.AuditOutcomeID
				AND T.ProvinceEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	--ENDIF
	
	IF @cLastAuditOutcomeName IS NULL
		BEGIN
		
		SELECT @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM dropdown.AuditOutcome AO
		WHERE AO.AuditOutcomeID = 0

		END
	--ENDIF
	
	RETURN @cLastAuditOutcomeName

END
GO
--End function procurement.GetLastEquipmentAuditOutcome
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetCommunityLocations
EXEC Utility.DropObject 'dbo.GetCommunityLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.13
-- Description:	A stored procedure to get location data from the dbo.Community table
--
-- Author:			Kevin Ross
-- Create date:	2015.09.17
-- Description:	Refactored
-- =================================================================================
CREATE PROCEDURE dbo.GetCommunityLocations
@CommunityID INT = 0,
@ProvinceID INT = 0,
@Boundary VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT 
		C.CommunityID,
		C.CommunityName,
		CAST(C.Latitude AS NUMERIC(13,8)) AS Latitude,
		CAST(C.Longitude AS NUMERIC(13,8)) AS Longitude,
		C.Population,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		ID.ImpactDecisionName, 
		ID.ImpactDecisionID
	FROM dbo.Community C 
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.ImpactDecision ID on ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.IsActive = 1
			AND 
				(
				@CommunityID = 0
					OR C.CommunityID = @CommunityID
				)
			AND 
				(
				@ProvinceID = 0
					OR C.ProvinceID = @ProvinceID
				)
			AND 
				(
				ISNULL(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(C.Location) = 1
				)
	ORDER BY C.CommunityName, C.CommunityID

END
GO
--End procedure dbo.GetCommunityLocations

--Begin procedure dbo.GetRequestForInformationByRequestForInformationID
EXEC Utility.DropObject 'dbo.GetRequestForInformationByRequestForInformationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.17
-- Description:	A stored procedure to get data from the dbo.RequestForInformation table
--
-- Author:			Todd Pires
-- Create date:	2015.04.04
-- Description:	Added the SummaryAnswer field
--
-- Author:			Todd Pires
-- Create date:	2015.04.22
-- Description:	Added the DesiredResponseDate and SpotReportID fields
--
-- Author:			Todd Pires
-- Create date:	2015.11.21
-- Description:	Added the CoverLetterFrom and CoverLetterTo fields
-- ====================================================================================
CREATE PROCEDURE dbo.GetRequestForInformationByRequestForInformationID

@RequestForInformationID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		RFI.Argument,
		RFI.AttachmentsList,
		RFI.Background,
		RFI.CompletedDate,
		dbo.FormatDate(RFI.CompletedDate) AS CompletedDateFormatted,
		RFI.CoverLetterFrom,
		RFI.CoverLetterTo,
		RFI.DesiredResponseDate,
		dbo.FormatDate(RFI.DesiredResponseDate) AS DesiredResponseDateFormatted,
		dbo.FormatDate(RFI.IncidentDate) AS IncidentDateFormatted,
		RFI.IncidentDate,
		dbo.FormatDate(RFI.InProgressDate) AS InProgressDateFormatted,
		RFI.InformationRequested,
		RFI.InProgressDate,
		RFI.Issue,
		RFI.KnownDetails,
		RFI.Location,
		RFI.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(RFI.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonFullname,
		RFI.Recommendation,
		RFI.RequestDate,
		dbo.FormatDate(RFI.RequestDate) AS RequestDateFormatted,
		RFI.RequestForInformationID,
		RFI.RequestForInformationTitle,
		RFI.RequestPersonID,
		dbo.FormatPersonNameByPersonID(RFI.RequestPersonID, 'LastFirst') AS RequestPersonFullname,
		RFI.Resources,
		RFI.Risk,
		RFI.SpotReportID,
		dbo.FormatSpotReportReferenceCode(RFI.SpotReportID) AS SpotReportReferenceCode,
		RFI.SummaryAnswer,
		RFI.Timing,
		C2.CommunityID,
		C2.CommunityName,
		RFIRT.RequestForInformationResultTypeCode,
		RFIRT.RequestForInformationResultTypeID,
		RFIRT.RequestForInformationResultTypeName,
		RFIS.RequestForInformationStatusCode,
		RFIS.RequestForInformationStatusID,
		RFIS.RequestForInformationStatusName,
		dbo.GetEntityTypeNameByEntityTypeCode('RequestForInformation') AS EntityTypeName
	FROM dbo.RequestForInformation RFI
		JOIN dropdown.RequestForInformationResultType RFIRT ON RFIRT.RequestForInformationResultTypeID = RFI.RequestForInformationResultTypeID
		JOIN dropdown.RequestForInformationStatus RFIS ON RFIS.RequestForInformationStatusID = RFI.RequestForInformationStatusID
			AND RFI.RequestForInformationID = @RequestForInformationID
		OUTER APPLY
				(
				SELECT
					C1.CommunityID,
					C1.CommunityName
				FROM dbo.Community C1
				WHERE C1.CommunityID = RFI.CommunityID
				) C2

	SELECT
		D.DocumentID,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'RequestForInformation'
			AND DE.EntityID = @RequestForInformationID

END
GO
--End procedure dbo.GetRequestForInformationByRequestForInformationID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.CommunityID,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.ProvinceID,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		D.Thumbnail,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'SpotReport'
			JOIN dbo.SpotReport SR ON SR.WorkflowStepNumber = WS.WorkflowStepNumber
				AND SR.SpotReportID = @SpotReportID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'SpotReport.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber,
		W.WorkflowStepCount
	FROM HD
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @SpotReportID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'SpotReport'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID) > 0
					THEN (SELECT SR.WorkflowStepNumber FROM dbo.SpotReport SR WHERE SR.SpotReportID = @SpotReportID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder
		
END
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure dropdown.GetDonorDecisionDecisionData
EXEC Utility.DropObject 'dropdown.GetDonerDecisionDecisionData'
EXEC Utility.DropObject 'dropdown.GetDonorDecisionDecisionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.11.13
-- Description:	A stored procedure to return data from the dropdown.DonorDecisionDecision table
-- ============================================================================================
CREATE PROCEDURE dropdown.GetDonorDecisionDecisionData
								 
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DonorDecisionDecisionID,
		T.DonorDecisionDecisionName
	FROM dropdown.DonorDecisionDecision T
	WHERE (T.DonorDecisionDecisionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.DonorDecisionDecisionName, T.DonorDecisionDecisionID

END
GO
--End procedure dropdown.GetDonorDecisionDecisionData

--Begin procedure eventlog.LogCommunityProvinceEngagementAction
EXEC utility.DropObject 'eventlog.LogCommunityProvinceEngagementAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogCommunityProvinceEngagementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityProvinceEngagementUpdate',
			T.CommunityProvinceEngagementUpdateID,
			@Comments
		FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.CommunityProvinceEngagementUpdateID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'CommunityProvinceEngagementUpdate',
			T.CommunityProvinceEngagementUpdateID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityContactXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityProjectXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRecommendationXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceContactXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceProjectXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRecommendationXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByCommunityProvinceEngagementUpdateID(T.CommunityProvinceEngagementUpdateID) AS XML))
			FOR XML RAW('CommunityProvinceEngagementUpdate'), ELEMENTS
			)
		FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.CommunityProvinceEngagementUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogCommunityProvinceEngagementAction

--Begin procedure eventlog.LogContactAction
EXEC utility.DropObject 'eventlog.LogContactAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.28
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Todd Pires
-- Create date: 2015.05.19
-- Description:	Added ContactType support
--
-- Author:		Todd Pires
-- Create date: 2015.06.19
-- Description:	Added ContactStipendPayment support
--
-- Author:		Todd Pires
-- Create date: 2015.06.29
-- Description:	Added EntityIDList support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogContactAction

@EntityID INT = 0,
@EventCode VARCHAR(50) = '',
@PersonID INT = 0,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF
	
	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			T.ContactID,
			@Comments
		FROM dbo.Contact T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.ContactID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Contact',
			T.ContactID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetContactContactAffiliationsXMLByContactID(T.ContactID) AS XML)),
			(SELECT CAST(eventlog.GetContactContactTypesXMLByContactID(T.ContactID) AS XML)),
			(SELECT CAST(eventlog.GetContactStipendPaymentsXMLByContactID(T.ContactID) AS XML)),
			(SELECT CAST(eventlog.GetContactVettingsXMLByContactID(T.ContactID) AS XML))
			FOR XML RAW('Contact'), ELEMENTS
			)
		FROM dbo.Contact T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.ContactID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogContactAction

--Begin procedure eventlog.LogPoliceEngagementAction
EXEC utility.DropObject 'eventlog.LogPoliceEngagementAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.09.11
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPoliceEngagementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			T.PoliceEngagementUpdateID,
			@Comments
		FROM policeengagementupdate.PoliceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PoliceEngagementUpdateID

		END
	ELSE
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'PoliceEngagementUpdate',
			T.PoliceEngagementUpdateID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetCommunityXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityClassXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetCommunityRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceClassXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceIndicatorXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRecommendationXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML)),
			(SELECT CAST(eventlog.GetProvinceRiskXMLByPoliceEngagementUpdateID(T.PoliceEngagementUpdateID) AS XML))
			FOR XML RAW('PoliceEngagementUpdate'), ELEMENTS
			)
		FROM policeengagementupdate.PoliceEngagementUpdate T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PoliceEngagementUpdateID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPoliceEngagementAction


--Begin procedure procurement.GetEquipmentCatalogByEquipmentCatalogID
EXEC Utility.DropObject 'procurement.GetEquipmentCatalogByEquipmentCatalogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.22
-- Description:	A stored procedure to data from the procurement.EquipmentCatalog table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.17
-- Description:	Added Risk, Impact, and Mitigation Notes fields
-- ===================================================================================
CREATE PROCEDURE procurement.GetEquipmentCatalogByEquipmentCatalogID

@EquipmentCatalogID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		EC.EquipmentCatalogID, 
		ISNULL(EC.Impact, 1) AS Impact,
		EC.IsCommon,
		EC.ItemMake,
		EC.ItemModel,
		EC.ItemName, 
		EC.MitigationNotes, 
		EC.Notes, 
		EC.QuantityOfIssue,
		ISNULL(EC.Risk, 1) AS Risk,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.UnitOfIssue, 
		ECC.EquipmentCatalogCategoryID, 
		ECC.EquipmentCatalogCategoryName, 
		dbo.GetEntityTypeNameByEntityTypeCode('EquipmentCatalog') AS EntityTypeName
	FROM procurement.EquipmentCatalog EC
		JOIN dropdown.EquipmentCatalogCategory ECC ON ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
			AND EC.EquipmentCatalogID = @EquipmentCatalogID
		
END
GO
--End procedure procurement.GetEquipmentCatalogByEquipmentCatalogID

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.MenuItem

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Equipment', @NewMenuItemCode='EquipmentManagement', @NewMenuItemLink='/equipmentmanagement/list', @NewMenuItemText='Equipment Management', @BeforeMenuItemCode='ConceptNoteContactEquipmentList', @PermissionableLineageList='EquipmentManagement.List'
GO

EXEC utility.MenuItemAddUpdate @ParentMenuItemCode='Admin', @NewMenuItemCode='DonorDecision', @NewMenuItemLink='/donordecision/list', @NewMenuItemText='Donor Logs', @BeforeMenuItemCode='EventLogList', @PermissionableLineageList='DonorDecision.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		0,
		'EquipmentManagement',
		'Equipment Management'
		)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement'),	'List',	'List Equipment Locations'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement'),	'Audit', 'Audit Equipment'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement'),	'Transfer',	'Transfer Equipment Between Locations')
		
	INSERT INTO permissionable.DisplayGroupPermissionable
		(DisplayGroupID, PermissionableID)
	SELECT
		DG.DisplayGroupID,
		(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'EquipmentManagement')
	FROM permissionable.DisplayGroup DG
	WHERE DG.DisplayGroupCode = 'Procurement'
		
	END
--ENDIF
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableCode = 'Transfer'
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision')
	BEGIN

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		0,
		'DonorDecision',
		'Donor Decisions, Meetings & Actions'
		)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'List',	'List Donor Decisions, Meetings & Actions'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'AddUpdateDecision', 'Add / Edit Donor Decisions'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'AddUpdateMeeting',	'Add / Edit Donor Meetings & Actions')

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DonorDecision')
	BEGIN

	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode, DisplayGroupName, DisplayOrder)
	VALUES
		(
		'DonorDecision',
		'Donor Decisions, Meetings & Actions',
		(SELECT MAX(DG.DisplayOrder) + 1 FROM permissionable.DisplayGroup DG)
		)
		
	INSERT INTO permissionable.DisplayGroupPermissionable
		(DisplayGroupID, PermissionableID)
	SELECT
		DG.DisplayGroupID,
		(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision')
	FROM permissionable.DisplayGroup DG
	WHERE DG.DisplayGroupCode = 'DonorDecision'
	
	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin file Build File - 05 - Supplemental.sql
USE AJACS
GO

--Begin Tables
EXEC utility.AddColumn 'dbo.Community', 'MOUDate', 'DATE'
EXEC utility.AddColumn 'communityprovinceengagementupdate.Community', 'MOUDate', 'DATE'
EXEC utility.AddColumn 'dbo.Community', 'SWOTDate', 'DATE'
EXEC utility.AddColumn 'communityprovinceengagementupdate.Community', 'SWOTDate', 'DATE'
EXEC utility.AddColumn 'dbo.Province', 'MOUDate', 'DATE'
EXEC utility.AddColumn 'communityprovinceengagementupdate.Province', 'MOUDate', 'DATE'
EXEC utility.AddColumn 'dbo.Province', 'SWOTDate', 'DATE'
EXEC utility.AddColumn 'communityprovinceengagementupdate.Province', 'SWOTDate', 'DATE'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Community') AND SC.Name = 'CAPAgreedDate')
	EXEC sp_RENAME 'dbo.Community.CAPAgreedDate', 'CSAPAgreedDate1', 'COLUMN';
--ENDIF
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('communityprovinceengagementupdate.Community') AND SC.Name = 'CAPAgreedDate')
	EXEC sp_RENAME 'communityprovinceengagementupdate.Community.CAPAgreedDate', 'CSAPAgreedDate1', 'COLUMN';
--ENDIF
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.Province') AND SC.Name = 'CAPAgreedDate')
	EXEC sp_RENAME 'dbo.Province.CAPAgreedDate', 'CSAPAgreedDate1', 'COLUMN';
--ENDIF
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('communityprovinceengagementupdate.Province') AND SC.Name = 'CAPAgreedDate')
	EXEC sp_RENAME 'communityprovinceengagementupdate.Province.CAPAgreedDate', 'CSAPAgreedDate1', 'COLUMN';
--ENDIF
GO

EXEC utility.AddColumn 'dbo.Community', 'CSAPAgreedDate2', 'DATE'
EXEC utility.AddColumn 'communityprovinceengagementupdate.Community', 'CSAPAgreedDate2', 'DATE'
EXEC utility.AddColumn 'dbo.Province', 'CSAPAgreedDate2', 'DATE'
EXEC utility.AddColumn 'communityprovinceengagementupdate.Province', 'CSAPAgreedDate2', 'DATE'
--End Tables

--Begin Procedures
--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Community table
-- ====================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

@CommunityIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)
	
	INSERT INTO communityprovinceengagementupdate.Community
		(CommunityID, CommunityProvinceEngagementUpdateID, CommunityEngagementOutput1, CommunityEngagementOutput2, CommunityEngagementOutput3, CommunityEngagementOutput4, CSAPAgreedDate1, CSAPAgreedDate2, LastNeedsAssessmentDate, MOUDate, SWOTDate, TORMOUStatusID, UpdatePersonID)
	SELECT
		C1.CommunityID,
		@CommunityProvinceEngagementUpdateID,
		C1.CommunityEngagementOutput1, 
		C1.CommunityEngagementOutput2, 
		C1.CommunityEngagementOutput3, 
		C1.CommunityEngagementOutput4, 
		C1.CSAPAgreedDate1, 
		C1.CSAPAgreedDate2, 
		C1.LastNeedsAssessmentDate, 
		C1.MOUDate, 
		C1.SWOTDate, 
		C1.TORMOUStatusID,
		@PersonID
	FROM dbo.Community C1
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C1.CommunityID
			AND NOT EXISTS
				(
				SELECT 1
				FROM communityprovinceengagementupdate.Community C2
				WHERE C2.CommunityID = C1.CommunityID
				)

	INSERT INTO communityprovinceengagementupdate.CommunityContact
		(CommunityProvinceEngagementUpdateID, CommunityID, ContactID)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		C.CommunityID,
		C.ContactID
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.CommunityID
			AND C.IsActive = 1
			AND C.IsValid = 1
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
						AND CCT.ContactID = C.ContactID
				)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactAffiliation CCA
					JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
						AND CA.ContactAffiliationName = 'Community Security Working Groups'
						AND CCA.ContactID = C.ContactID
				)

	INSERT INTO communityprovinceengagementupdate.CommunityIndicator
		(CommunityProvinceEngagementUpdateID, CommunityID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OACI.CommunityProvinceEngagementAchievedValue, 0),
		OACI.CommunityProvinceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@CommunityIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityProvinceEngagementAchievedValue,
				CI.CommunityProvinceEngagementNotes
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = CAST(LTT.ListItem AS INT)
			) OACI

	INSERT INTO communityprovinceengagementupdate.CommunityProject
		(CommunityProvinceEngagementUpdateID, CommunityID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		PC.CommunityID, 
		PC.ProjectID, 
		PC.CommunityProvinceEngagementNotes
	FROM project.ProjectCommunity PC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PC.CommunityID
	
	INSERT INTO communityprovinceengagementupdate.CommunityRecommendation
		(CommunityProvinceEngagementUpdateID, CommunityID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RC.CommunityID, 
		RC.RecommendationID, 
		RC.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationCommunity RC
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
	
	INSERT INTO communityprovinceengagementupdate.CommunityRisk
		(CommunityProvinceEngagementUpdateID, CommunityID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RC.CommunityID,
		R.RiskID,
		ISNULL(OACR.CommunityProvinceEngagementRiskValue, 0),
		OACR.CommunityProvinceEngagementNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RC.CommunityID
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementRiskValue, 
				CR.CommunityProvinceEngagementNotes
			FROM dbo.CommunityRisk CR
				JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = CR.CommunityID
					AND CR.RiskID = R.RiskID
			) OACR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'CommunityProvinceEngagementCommunity', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@CommunityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Community'
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementCommunity

--Begin procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince
EXEC Utility.DropObject 'communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.12
-- Description:	A stored procedure to add data to the communityprovinceengagementupdate.Province table
-- ===================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

@ProvinceIDList VARCHAR(MAX),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CommunityProvinceEngagementUpdateID INT = (SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC)

	INSERT INTO communityprovinceengagementupdate.Province
		(ProvinceID, CommunityProvinceEngagementUpdateID, CommunityEngagementOutput1, CommunityEngagementOutput2, CommunityEngagementOutput3, CommunityEngagementOutput4, CSAPAgreedDate1, CSAPAgreedDate2, LastNeedsAssessmentDate, MOUDate, SWOTDate, TORMOUStatusID, UpdatePersonID)
	SELECT
		P1.ProvinceID,
		@CommunityProvinceEngagementUpdateID,
		P1.CommunityEngagementOutput1, 
		P1.CommunityEngagementOutput2, 
		P1.CommunityEngagementOutput3, 
		P1.CommunityEngagementOutput4, 
		P1.CSAPAgreedDate1, 
		P1.CSAPAgreedDate2, 
		P1.LastNeedsAssessmentDate, 
		P1.MOUDate, 
		P1.SWOTDate, 
		P1.TORMOUStatusID,
		@PersonID
	FROM dbo.Province P1
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P1.ProvinceID
			AND NOT EXISTS
				(
				SELECT 1
				FROM communityprovinceengagementupdate.Province P2
				WHERE P2.ProvinceID = P1.ProvinceID
				)

	INSERT INTO communityprovinceengagementupdate.ProvinceContact
		(CommunityProvinceEngagementUpdateID, ProvinceID, ContactID)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		C.ProvinceID,
		C.ContactID
	FROM dbo.Contact C
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ProvinceID
			AND C.IsActive = 1
			AND C.IsValid = 1
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactType CCT
					JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
						AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
						AND CCT.ContactID = C.ContactID
				)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.ContactContactAffiliation CCA
					JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
						AND CA.ContactAffiliationName = 'Community Security Working Groups'
						AND CCA.ContactID = C.ContactID
				)

	INSERT INTO communityprovinceengagementupdate.ProvinceIndicator
		(CommunityProvinceEngagementUpdateID, ProvinceID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		CAST(LTT.ListItem AS INT),
		I.IndicatorID, 
		ISNULL(OAPI.CommunityProvinceEngagementAchievedValue, 0),
		OAPI.CommunityProvinceEngagementNotes
	FROM logicalframework.Indicator I
		CROSS JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CAST(LTT.ListItem AS INT) > 0
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.CommunityProvinceEngagementAchievedValue,
				PRI.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = CAST(LTT.ListItem AS INT)
			) OAPI

	INSERT INTO communityprovinceengagementupdate.ProvinceProject
		(CommunityProvinceEngagementUpdateID, ProvinceID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		PP.ProvinceID, 
		PP.ProjectID, 
		PP.CommunityProvinceEngagementNotes
	FROM project.ProjectProvince PP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PP.ProvinceID
	
	INSERT INTO communityprovinceengagementupdate.ProvinceRecommendation
		(CommunityProvinceEngagementUpdateID, ProvinceID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RP.ProvinceID, 
		RP.RecommendationID, 
		RP.CommunityProvinceEngagementNotes
	FROM recommendation.RecommendationProvince RP
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
	
	INSERT INTO communityprovinceengagementupdate.ProvinceRisk
		(CommunityProvinceEngagementUpdateID, ProvinceID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		@CommunityProvinceEngagementUpdateID,
		RP.ProvinceID, 
		R.RiskID, 
		ISNULL(OAPR.CommunityProvinceEngagementRiskValue, 0),
		OAPR.CommunityProvinceEngagementNotes
	FROM dbo.Risk R
		JOIN recommendation.RecommendationRisk RR ON RR.RiskID = R.RiskID
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = RR.RecommendationID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = RP.ProvinceID
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM dbo.ProvinceRisk PR
				JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = PR.ProvinceID
					AND PR.RiskID = R.RiskID
			) OAPR

	INSERT INTO dbo.DocumentEntity
		(DocumentID, EntityTypeCode, EntityID)
	SELECT
		DE.DocumentID, 
		'CommunityProvinceEngagementProvince', 
		DE.EntityID
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dbo.ListToTable(@ProvinceIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = DE.EntityID
			AND DE.EntityTypeCode = 'Province'
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')

	EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='update', @PersonID = @PersonID

END
GO
--End procedure communityprovinceengagementupdate.AddCommunityProvinceEngagementProvince

--Begin procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.20
-- Description:	A procedure to approve a Community/Province Engagement Update
-- ==========================================================================
CREATE PROCEDURE communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @nCommunityID INT
	DECLARE @nCommunityProvinceEngagementUpdateID INT = ISNULL((SELECT TOP 1 CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU ORDER BY CPEU.CommunityProvinceEngagementUpdateID DESC), 0)
	DECLARE @nContactAffiliationID INT = ISNULL((SELECT CA.ContactAffiliationID FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Community Security Working Groups'), 0)
	DECLARE @nProvinceID INT
	DECLARE @tOutputCommunity TABLE (CommunityID INT)
	DECLARE @tOutputProvince TABLE (ProvinceID INT)

	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'read', @PersonID, NULL
	EXEC eventlog.LogCommunityProvinceEngagementAction @nCommunityProvinceEngagementUpdateID, 'incrementworkflow', @PersonID, NULL

	UPDATE P
	SET
		P.CSAPAgreedDate1 = CPEU.CSAPAgreedDate1,
		P.CSAPAgreedDate2 = CPEU.CSAPAgreedDate2,
		P.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		P.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		P.MOUDate = CPEU.MOUDate, 
		P.SWOTDate = CPEU.SWOTDate,
		P.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.ProvinceID INTO @tOutputProvince
	FROM dbo.Province P
		JOIN communityprovinceengagementupdate.Province CPEU ON CPEU.ProvinceID = P.ProvinceID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputProvince O ON O.ProvinceID = C.ProvinceID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		PC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.ProvinceContact PC

	DELETE PRI
	FROM dbo.ProvinceIndicator PRI
		JOIN @tOutputProvince O ON O.ProvinceID = PRI.ProvinceID
	
	INSERT INTO dbo.ProvinceIndicator
		(ProvinceID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		PRI.ProvinceID,
		PRI.IndicatorID,
		PRI.CommunityProvinceEngagementAchievedValue, 
		PRI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceIndicator PRI

	DELETE PC
	FROM project.ProjectProvince PC
		JOIN @tOutputProvince O ON O.ProvinceID = PC.ProvinceID
	
	INSERT INTO project.ProjectProvince
		(ProvinceID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.ProvinceID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceProject CP

	DELETE RP
	FROM recommendation.RecommendationProvince RP
		JOIN @tOutputProvince O ON O.ProvinceID = RP.ProvinceID

	INSERT INTO recommendation.RecommendationProvince
		(ProvinceID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RecommendationID,
		PR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceRecommendation PR

	DELETE PR
	FROM dbo.ProvinceRisk PR
		JOIN @tOutputProvince O ON O.ProvinceID = PR.ProvinceID
	
	INSERT INTO dbo.ProvinceRisk
		(ProvinceID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		PR.ProvinceID,
		PR.RiskID,
		PR.CommunityProvinceEngagementRiskValue, 
		PR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.ProvinceRisk PR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.ProvinceID
		FROM @tOutputProvince O
		ORDER BY O.ProvinceID
	
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogProvinceAction @nProvinceID, 'read', @PersonID, NULL
		EXEC eventlog.LogProvinceAction @nProvinceID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Province'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputProvince O ON O.ProvinceID = DE.EntityID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementProvince'
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')

	UPDATE C
	SET
		C.CSAPAgreedDate1 = CPEU.CSAPAgreedDate1,
		C.CSAPAgreedDate2 = CPEU.CSAPAgreedDate2,
		C.CommunityEngagementOutput1 = CPEU.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2 = CPEU.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3 = CPEU.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4 = CPEU.CommunityEngagementOutput4,
		C.LastNeedsAssessmentDate = CPEU.LastNeedsAssessmentDate,
		C.MOUDate = CPEU.MOUDate, 
		C.SWOTDate = CPEU.SWOTDate,
		C.TORMOUStatusID = CPEU.TORMOUStatusID
	OUTPUT INSERTED.CommunityID INTO @tOutputCommunity
	FROM dbo.Community C
		JOIN communityprovinceengagementupdate.Community CPEU ON CPEU.CommunityID = C.CommunityID
			AND CPEU.CommunityProvinceEngagementUpdateID = @nCommunityProvinceEngagementUpdateID

	DELETE CCA
	FROM dbo.ContactContactAffiliation CCA
		JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
		JOIN @tOutputCommunity O ON O.CommunityID = C.CommunityID
			AND CCA.ContactAffiliationID = @nContactAffiliationID
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CCT.ContactID = C.ContactID
			)

	INSERT INTO dbo.ContactContactAffiliation
		(ContactID, ContactAffiliationID)
	SELECT
		CC.ContactID,
		@nContactAffiliationID
	FROM communityprovinceengagementupdate.CommunityContact CC

	DELETE CI
	FROM dbo.CommunityIndicator CI
		JOIN @tOutputCommunity O ON O.CommunityID = CI.CommunityID
	
	INSERT INTO dbo.CommunityIndicator
		(CommunityID, IndicatorID, CommunityProvinceEngagementAchievedValue, CommunityProvinceEngagementNotes)
	SELECT
		CI.CommunityID,
		CI.IndicatorID,
		CI.CommunityProvinceEngagementAchievedValue, 
		CI.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityIndicator CI

	DELETE PC
	FROM project.ProjectCommunity PC
		JOIN @tOutputCommunity O ON O.CommunityID = PC.CommunityID
	
	INSERT INTO project.ProjectCommunity
		(CommunityID, ProjectID, CommunityProvinceEngagementNotes)
	SELECT
		CP.CommunityID,
		CP.ProjectID,
		CP.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityProject CP

	DELETE RC
	FROM recommendation.RecommendationCommunity RC
		JOIN @tOutputCommunity O ON O.CommunityID = RC.CommunityID
	
	INSERT INTO recommendation.RecommendationCommunity
		(CommunityID, RecommendationID, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RecommendationID,
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRecommendation CR

	DELETE CR
	FROM dbo.CommunityRisk CR
		JOIN @tOutputCommunity O ON O.CommunityID = CR.CommunityID
	
	INSERT INTO dbo.CommunityRisk
		(CommunityID, RiskID, CommunityProvinceEngagementRiskValue, CommunityProvinceEngagementNotes)
	SELECT
		CR.CommunityID,
		CR.RiskID,
		CR.CommunityProvinceEngagementRiskValue, 
		CR.CommunityProvinceEngagementNotes
	FROM communityprovinceengagementupdate.CommunityRisk CR

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT O.CommunityID
		FROM @tOutputCommunity O
		ORDER BY O.CommunityID
	
	OPEN oCursor
	FETCH oCursor INTO @nCommunityID
	WHILE @@fetch_status = 0
		BEGIN
	
		EXEC eventlog.LogCommunityAction @nCommunityID, 'read', @PersonID, NULL
		EXEC eventlog.LogCommunityAction @nCommunityID, 'update', @PersonID, NULL
			
		FETCH oCursor INTO @nCommunityID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	UPDATE DE
	SET DE.EntityTypeCode = 'Community'
	FROM dbo.DocumentEntity DE
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN @tOutputCommunity O ON O.CommunityID = DE.EntityID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementCommunity'
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')
	
	DELETE FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate

	TRUNCATE TABLE communityprovinceengagementupdate.Province
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceContact
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceProject
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.ProvinceRisk

	TRUNCATE TABLE communityprovinceengagementupdate.Community
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityContact
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityIndicator
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityProject
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRecommendation
	TRUNCATE TABLE communityprovinceengagementupdate.CommunityRisk

END

GO
--End procedure communityprovinceengagementupdate.ApproveCommunityProvinceEngagementUpdate

--Begin procedure communityprovinceengagementupdate.GetCommunityByCommunityID
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetCommunityByCommunityID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dbo.Community and communityprovinceengagementupdate.Community tables
--
-- Author:		Eric Ryan Jones
-- Create date:	2015.11.17
-- Description:	altereted projects to filter for CE projects with component ID = 1 or no component of 0
-- ============================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetCommunityByCommunityID

@CommunityID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		C.CSAPAgreedDate1,
		dbo.FormatDate(C.CSAPAgreedDate1) AS CSAPAgreedDate1Formatted,
		C.CSAPAgreedDate2,
		dbo.FormatDate(C.CSAPAgreedDate2) AS CSAPAgreedDate2Formatted,
		C.CommunityID,
		C.CommunityEngagementOutput1,
		C.CommunityEngagementOutput2,
		C.CommunityEngagementOutput3,
		C.CommunityEngagementOutput4,
		C.CommunityName AS EntityName,
		C.LastNeedsAssessmentDate,
		dbo.FormatDate(C.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		C.MOUDate,
		dbo.FormatDate(C.MOUDate) AS MOUDateFormatted,
		C.SWOTDate,
		dbo.FormatDate(C.SWOTDate) AS SWOTDateFormatted,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM dbo.Community C
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = C.TORMOUStatusID
			AND C.CommunityID = @CommunityID

	--EntityUpdate
	SELECT
		C1.CSAPAgreedDate1,
		dbo.FormatDate(C1.CSAPAgreedDate1) AS CSAPAgreedDate1Formatted,
		C1.CSAPAgreedDate2,
		dbo.FormatDate(C1.CSAPAgreedDate2) AS CSAPAgreedDate2Formatted,
		C1.CommunityID,
		C1.CommunityEngagementOutput1,
		C1.CommunityEngagementOutput2,
		C1.CommunityEngagementOutput3,
		C1.CommunityEngagementOutput4,
		C1.LastNeedsAssessmentDate,
		dbo.FormatDate(C1.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		C1.MOUDate,
		dbo.FormatDate(C1.MOUDate) AS MOUDateFormatted,
		C1.SWOTDate,
		dbo.FormatDate(C1.SWOTDate) AS SWOTDateFormatted,
		C2.CommunityName AS EntityName,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM communityprovinceengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = C1.TORMOUStatusID
			AND C1.CommunityID = @CommunityID

	--EntityContactCurrent
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
	WHERE C.IsActive = 1
		AND C.IsValid = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
					AND CCT.ContactID = C.ContactID
			)
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactAffiliation CCA
				JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
					AND CA.ContactAffiliationName = 'Community Security Working Groups'
					AND CCA.ContactID = C.ContactID
			)
		AND C.CommunityID = @CommunityID
	ORDER BY 2

	--EntityContactUpdate
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN communityprovinceengagementupdate.CommunityContact CC ON CC.ContactID = C.ContactID
			AND CC.CommunityID = @CommunityID
	ORDER BY 2

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Community'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementCommunity'
			AND DE.EntityID = @CommunityID
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')
	ORDER BY D.DocumentDescription

	--EntityFinding
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
			AND F.IsActive = 1
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingCommunity FC
				WHERE FC.FindingID = F.FindingID
					AND FC.CommunityID = @CommunityID
				)
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingIndicator FI
					JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
						AND FI.FindingID = F.FindingID
						AND I.IsActive = 1
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO2'
				)
	ORDER BY F.FindingName, F.FindingID		

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorCommunityNotes(' + CAST(ISNULL(OACI.CommunityIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				CI.CommunityIndicatorID
			FROM dbo.CommunityIndicator CI 
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityIndicatorUpdate
	SELECT
		OACI.CommunityProvinceEngagementAchievedValue,
		OACI.CommunityProvinceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				CI.CommunityProvinceEngagementAchievedValue, 
				CI.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityIndicator CI
			WHERE CI.IndicatorID = I.IndicatorID
				AND CI.CommunityID = @CommunityID
			) OACI
	ORDER BY O1.ObjectiveName, I.IndicatorName

	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(ISNULL(OACP.ProjectCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND 
				(
				C.ComponentID = 0
					OR C.ComponentAbbreviation = 'CE'
				)
		OUTER APPLY
			(
			SELECT
				PC.ProjectCommunityID
			FROM project.ProjectCommunity PC
			WHERE PC.ProjectID = P.ProjectID
				AND PC.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		OACP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(P.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectCommunity PC ON PC.ProjectID = P.ProjectID
			AND PC.CommunityID = @CommunityID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND 
				(
				C.ComponentID = 0
					OR C.ComponentAbbreviation = 'CE'
				)
		OUTER APPLY
			(
			SELECT
				CP.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityProject CP
			WHERE CP.ProjectID = P.ProjectID
				AND CP.CommunityID = @CommunityID
			) OACP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationCommunityNotes(' + CAST(ISNULL(OACR.RecommendationCommunityID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				RC.RecommendationCommunityID
			FROM recommendation.RecommendationCommunity RC
			WHERE RC.RecommendationID = R.RecommendationID
				AND RC.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OACR.CommunityProvinceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationCommunity RC ON RC.RecommendationID = R.RecommendationID
			AND RC.CommunityID = @CommunityID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityRecommendation CR
			WHERE CR.RecommendationID = R.RecommendationID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskCommunityNotes(' + CAST(ISNULL(OACR.CommunityRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityRiskID
			FROM dbo.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OACR.CommunityProvinceEngagementRiskValue,
		OACR.CommunityProvinceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC1.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC1 ON RC1.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationCommunity RC2 
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RC2.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RC2.CommunityID = @CommunityID
				)
		OUTER APPLY
			(
			SELECT
				CR.CommunityProvinceEngagementRiskValue, 
				CR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.CommunityRisk CR
			WHERE CR.RiskID = R.RiskID
				AND CR.CommunityID = @CommunityID
			) OACR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure communityprovinceengagementupdate.GetCommunityByCommunityID

--Begin procedure communityprovinceengagementupdate.GetProvinceByProvinceID
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetProvinceByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.10
-- Description:	A stored procedure to return data from the dbo.Province table and communityprovinceengagementupdate.Province tables
--
-- Author:		Eric Ryan Jones
-- Create date:	2015.11.17
-- Description:	altereted projects to filter for CE projects with component ID = 1 or no component of 0
-- ================================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetProvinceByProvinceID

@ProvinceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--EntityCurrent
	SELECT
		P.CSAPAgreedDate1,
		dbo.FormatDate(P.CSAPAgreedDate1) AS CSAPAgreedDate1Formatted,
		P.CSAPAgreedDate2,
		dbo.FormatDate(P.CSAPAgreedDate2) AS CSAPAgreedDate2Formatted,
		P.ProvinceID,
		P.CommunityEngagementOutput1,
		P.CommunityEngagementOutput2,
		P.CommunityEngagementOutput3,
		P.CommunityEngagementOutput4,
		P.ProvinceName AS EntityName,
		P.LastNeedsAssessmentDate,
		dbo.FormatDate(P.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		P.MOUDate,
		dbo.FormatDate(P.MOUDate) AS MOUDateFormatted,
		P.SWOTDate,
		dbo.FormatDate(P.SWOTDate) AS SWOTDateFormatted,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM dbo.Province P
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = P.TORMOUStatusID
			AND P.ProvinceID = @ProvinceID

	--EntityUpdate
	SELECT
		P1.CSAPAgreedDate1,
		dbo.FormatDate(P1.CSAPAgreedDate1) AS CSAPAgreedDate1Formatted,
		P1.CSAPAgreedDate2,
		dbo.FormatDate(P1.CSAPAgreedDate2) AS CSAPAgreedDate2Formatted,
		P1.ProvinceID,
		P1.CommunityEngagementOutput1,
		P1.CommunityEngagementOutput2,
		P1.CommunityEngagementOutput3,
		P1.CommunityEngagementOutput4,
		P1.LastNeedsAssessmentDate,
		dbo.FormatDate(P1.LastNeedsAssessmentDate) AS LastNeedsAssessmentDateFormatted,
		P1.MOUDate,
		dbo.FormatDate(P1.MOUDate) AS MOUDateFormatted,
		P1.SWOTDate,
		dbo.FormatDate(P1.SWOTDate) AS SWOTDateFormatted,
		P2.ProvinceName AS EntityName,
		TMS.TORMOUStatusID,
		TMS.TORMOUStatusName
	FROM communityprovinceengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
		JOIN dropdown.TORMOUStatus TMS ON TMS.TORMOUStatusID = P1.TORMOUStatusID
			AND P1.ProvinceID = @ProvinceID

	--EntityContactCurrent
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
	WHERE C.IsActive = 1
		AND C.IsValid = 1
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactType CCT
				JOIN dropdown.ContactType CT ON CT.ContactTypeID = CCT.ContactTypeID
					AND CT.ContactTypeCode IN ('Beneficiary','Stipend')
					AND CCT.ContactID = C.ContactID
			)
		AND EXISTS
			(
			SELECT 1
			FROM dbo.ContactContactAffiliation CCA
				JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
					AND CA.ContactAffiliationName = 'Community Security Working Groups'
					AND CCA.ContactID = C.ContactID
			)
		AND C.ProvinceID = @ProvinceID
	ORDER BY 2

	--EntityContactUpdate
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted
	FROM dbo.Contact C
		JOIN communityprovinceengagementupdate.ProvinceContact PC ON PC.ContactID = C.ContactID
			AND PC.ProvinceID = @ProvinceID
	ORDER BY 2

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'Province'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')
	ORDER BY D.DocumentDescription

	--EntityDocumentUpdate
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityProvinceEngagementProvince'
			AND DE.EntityID = @ProvinceID
			AND D.DocumentDescription IN ('CSAP 1 Document','CSAP 2 Document','MOU Document','Needs Assessment Final Document','SWOT Document','TOR Document')
	ORDER BY D.DocumentDescription

	--EntityFinding
	SELECT
		F.FindingID,
		F.FindingName,
		FS.FindingStatusID,
		FS.FindingStatusName,
		FT.FindingTypeID,
		FT.FindingTypeName
	FROM finding.Finding F
		JOIN dropdown.FindingStatus FS ON FS.FindingStatusID = F.FindingStatusID
			AND F.IsActive = 1
		JOIN dropdown.FindingType FT ON FT.FindingTypeID = F.FindingTypeID
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingProvince FP
				WHERE FP.FindingID = F.FindingID
					AND FP.ProvinceID = @ProvinceID
				)
			AND EXISTS
				(
				SELECT 1
				FROM finding.FindingIndicator FI
					JOIN logicalframework.Indicator I ON I.IndicatorID = FI.IndicatorID
						AND FI.FindingID = F.FindingID
						AND I.IsActive = 1
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO2'
				)
	ORDER BY F.FindingName, F.FindingID		

	--EntityIndicatorCurrent
	SELECT 
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getIndicatorProvinceNotes(' + CAST(ISNULL(OAPI.ProvinceIndicatorID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT 
				PRI.ProvinceIndicatorID 
			FROM dbo.ProvinceIndicator PRI 
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityIndicatorUpdate
	SELECT 
		OAPI.CommunityProvinceEngagementAchievedValue,
		OAPI.CommunityProvinceEngagementNotes,
		I.IndicatorID,
		I.IndicatorName,
		IT.IndicatorTypeName,
		O1.ObjectiveName,
		'<a class="btn btn-info" onclick="getNotes(''Indicator'', ' + CAST(I.IndicatorID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM logicalframework.Indicator I
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O1 ON O1.ObjectiveID = I.ObjectiveID
		JOIN logicalframework.Objective O2 ON O2.ObjectiveID = O1.ParentObjectiveID
		JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O2.ComponentReportingAssociationID
			AND CRA.ComponentReportingAssociationCode LIKE 'CEO%'
			AND I.IsActive = 1
		OUTER APPLY
			(
			SELECT
				PRI.CommunityProvinceEngagementAchievedValue, 
				PRI.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceIndicator PRI
			WHERE PRI.IndicatorID = I.IndicatorID
				AND PRI.ProvinceID = @ProvinceID
			) OAPI
	ORDER BY O1.ObjectiveName, I.IndicatorName
	
	--EntityProjectCurrent
	SELECT
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getProjectCommunityNotes(' + CAST(OAPP.ProjectProvinceID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP ON PP.ProjectID = P.ProjectID
			AND PP.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND 
				(
				C.ComponentID = 0
					OR C.ComponentAbbreviation = 'CE'
				)
		OUTER APPLY
			(
			SELECT
				PP.ProjectProvinceID
			FROM project.ProjectProvince PP
			WHERE PP.ProjectID = P.ProjectID
				AND PP.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityProjectUpdate
	SELECT
		OAPP.CommunityProvinceEngagementNotes,
		P.ProjectID,
		P.ProjectName,
		PS.ProjectStatusName,
		'<a class="btn btn-info" onclick="getNotes(''Project'', ' + CAST(P.ProjectID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM project.Project P
		JOIN project.ProjectProvince PP1 ON PP1.ProjectID = P.ProjectID
			AND PP1.ProvinceID = @ProvinceID
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
			AND PS.IsOpen = 1
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND 
				(
				C.ComponentID = 0
					OR C.ComponentAbbreviation = 'CE'
				)
		OUTER APPLY
			(
			SELECT
				PP2.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceProject PP2
			WHERE PP2.ProjectID = P.ProjectID
				AND PP2.ProvinceID = @ProvinceID
			) OAPP
	ORDER BY P.ProjectName, P.ProjectID

	--EntityRecommendationCurrent
	SELECT
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getRecommendationProvinceNotes(' + CAST(ISNULL(OAPR.RecommendationProvinceID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				RP.RecommendationProvinceID
			FROM recommendation.RecommendationProvince RP
			WHERE RP.RecommendationID = R.RecommendationID
				AND RP.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID

	--EntityRecommendationUpdate
	SELECT
		OAPR.CommunityProvinceEngagementNotes,
		R.RecommendationID,
		R.RecommendationName,
		(SELECT 
			STUFF( (
				SELECT 
					', ' + C.ComponentAbbreviation
				FROM dropdown.Component C
					JOIN recommendation.RecommendationComponent RC ON RC.ComponentID = C.ComponentID
					AND RC.RecommendationID = R.RecommendationID
				ORDER BY C.DisplayOrder, C.ComponentAbbreviation FOR XML PATH(''), TYPE).value('text()[1]', 'VARCHAR(MAX)')
				, 1, 2, ''
				)) AS ComponentAbbreviationList,
		'<a class="btn btn-info" onclick="getNotes(''Recommendation'', ' + CAST(R.RecommendationID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM recommendation.Recommendation R
		JOIN recommendation.RecommendationProvince RP ON RP.RecommendationID = R.RecommendationID
			AND RP.ProvinceID = @ProvinceID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationIndicator RI
					JOIN logicalframework.Indicator I ON I.IndicatorID = RI.IndicatorID
						AND RI.RecommendationID = R.RecommendationID
					JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
					JOIN dropdown.ComponentReportingAssociation CRA ON CRA.ComponentReportingAssociationID = O.ComponentReportingAssociationID
						AND CRA.ComponentReportingAssociationCode = 'CEO3'
				)
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceRecommendation PR
			WHERE PR.RecommendationID = R.RecommendationID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RecommendationName, R.RecommendationID
	
	--EntityRiskCurrent
	SELECT 
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getRiskProvinceNotes(' + CAST(ISNULL(OAPR.ProvinceRiskID, 0) AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.ProvinceRiskID
			FROM dbo.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

	--EntityRiskUpdate
	SELECT
		OAPR.CommunityProvinceEngagementRiskValue,
		OAPR.CommunityProvinceEngagementNotes,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName,
		'<a class="btn btn-info" onclick="getNotes(''Risk'', ' + CAST(R.RiskID AS VARCHAR(10)) + ')">View</a>' AS ActionButton
	FROM dbo.Risk R
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND EXISTS
				(
				SELECT 1
				FROM recommendation.RecommendationProvince RP
					JOIN recommendation.RecommendationRisk RR ON RR.RecommendationID = RP.RecommendationID
						AND RR.RiskID = R.RiskID
						AND RP.ProvinceID = @ProvinceID
				)
		OUTER APPLY
			(
			SELECT
				PR.CommunityProvinceEngagementRiskValue, 
				PR.CommunityProvinceEngagementNotes
			FROM communityprovinceengagementupdate.ProvinceRisk PR
			WHERE PR.RiskID = R.RiskID
				AND PR.ProvinceID = @ProvinceID
			) OAPR
	ORDER BY R.RiskName, R.RiskID

END
GO
--End procedure communityprovinceengagementupdate.GetProvinceByProvinceID

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT			
		C1.ContactID,
		dbo.GetContactLocationByContactID(C1.ContactID) AS ContactLocation,
		dbo.FormatDate(C1.DateOfBirth) AS DateOfBirthFormatted,
		dbo.FormatContactNameByContactID(C1.ContactID, 'LastFirstMiddle') AS FullName,
		CNC.VettingDate,
		dbo.FormatDate(CNC.VettingDate) AS VettingDateFormatted,
		VO.VettingOutcomeID,
		VO.VettingOutcomeName
	FROM dbo.ConceptNoteContact CNC
		JOIN dbo.Contact C1 ON C1.ContactID = CNC.ContactID
		JOIN dropdown.VettingOutcome VO ON VO.VettingOutcomeID = CNC.VettingOutcomeID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,

		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	;
	WITH HD (WorkflowStepID,ParentWorkflowStepID,NodeLevel)
		AS
		(
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			1
		FROM workflow.WorkflowStep WS
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.EntityTypeCode = 'ConceptNote'
			JOIN dbo.ConceptNote CN ON CN.WorkflowStepNumber = WS.WorkflowStepNumber
				AND CN.ConceptNoteID = @ConceptNoteID
				AND WS.ParentWorkflowStepID = 0
	
		UNION ALL
	
		SELECT
			WS.WorkflowStepID,
			WS.ParentWorkflowStepID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM workflow.WorkflowStep WS
			JOIN HD ON HD.WorkflowStepID = WS.ParentWorkflowStepID
		)
	
	SELECT
		'ConceptNote.AddUpdate.WorkflowStepID' + 
		CASE
			WHEN HD.ParentWorkflowStepID > 0
			THEN CAST(HD.ParentWorkflowStepID AS VARCHAR(10)) + '.WorkflowStepID'
			ELSE ''
		END 
		+ CAST(HD.WorkflowStepID AS VARCHAR(10)) AS PermissionableLineage,
		EWS.IsComplete,
		HD.WorkflowStepID,
		WS.WorkflowStepName,
		WS.WorkflowStepNumber
	FROM HD
		JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = HD.WorkflowStepID
		JOIN workflow.EntityWorkflowStep EWS ON EWS.WorkflowStepID = HD.WorkflowStepID
			AND HD.NodeLevel = (SELECT MAX(HD.NodeLevel) FROM HD)
			AND EWS.EntityID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'ConceptNote'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID) > 0
					THEN (SELECT CN.WorkflowStepNumber FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
					ELSE 1
				END

	ORDER BY WSWA.DisplayOrder

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID
--End Procedures

--Begin Data
UPDATE D
SET D.DocumentDescription = 'CSAP 1 Document'
FROM dbo.Document D
WHERE D.DocumentDescription IN ('CAP Document', 'CSAP Document 1')
GO

UPDATE D
SET D.DocumentDescription = 'Needs Assessment Final Document'
FROM dbo.Document D
WHERE D.DocumentDescription IN ('Needs Assesment Document', 'Needs Assessment Document')
GO
--End Data

--End file Build File - 05 - Supplemental.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.39 File 01 - AJACS - 2015.11.27 23.10.22')
GO
--End build tracking

