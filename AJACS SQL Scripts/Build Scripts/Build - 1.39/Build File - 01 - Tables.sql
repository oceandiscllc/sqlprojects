USE AJACS
GO

--Begin table dbo.Document
DECLARE @TableName VARCHAR(250) = 'dbo.Document'

EXEC utility.DropColumn @TableName, 'Thumbnail'
EXEC utility.AddColumn @TableName, 'Thumbnail', 'VARBINARY(MAX)'
GO
--End table dbo.Document

--Begin table dropdown.AuditOutcome
IF NOT EXISTS (SELECT 1 FROM dropdown.AuditOutcome AO WHERE AO.AuditOutcomeName = 'Transferred')
	BEGIN
	
	INSERT INTO dropdown.AuditOutcome
		(AuditOutcomeName, DisplayOrder)
	SELECT
		'Transferred',
		MAX(AO.DisplayOrder) + 1
	FROM dropdown.AuditOutcome AO
	
	END
--ENDIF
GO
--End table dropdown.AuditOutcome

--Begin table dropdown.DonorDecisionDecision
DECLARE @TableName VARCHAR(250) = 'dropdown.DonorDecisionDecision'

EXEC utility.DropObject @TableName
EXEC utility.DropObject 'dropdown.DonerDecisionDecision'

CREATE TABLE dropdown.DonorDecisionDecision
	(
	DonorDecisionDecisionID INT IDENTITY(0,1) NOT NULL,
	DonorDecisionDecisionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DonorDecisionDecisionID'
EXEC utility.SetIndexNonClustered 'IX_DonorDecisionDecision', @TableName, 'DisplayOrder,DonorDecisionDecisionName', 'DonorDecisionDecisionID'
GO

SET IDENTITY_INSERT dropdown.DonorDecisionDecision ON
GO

INSERT INTO dropdown.DonorDecisionDecision (DonorDecisionDecisionID) VALUES (0)

SET IDENTITY_INSERT dropdown.DonorDecisionDecision OFF
GO

INSERT INTO dropdown.DonorDecisionDecision 
	(DonorDecisionDecisionName, DisplayOrder)
VALUES
	('Awaits', 1),
	('Approved', 2),
	('Rejected', 3),
	('Deferred', 4)
GO
--End table dropdown.DonorDecisionDecision

--Begin table procurement.CommunityEquipmentInventoryAudit
DECLARE @TableName VARCHAR(250) = 'procurement.CommunityEquipmentInventoryAudit'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.CommunityEquipmentInventoryAudit
	(
	CommunityEquipmentInventoryAuditID INT IDENTITY(1,1) NOT NULL,
	CommunityEquipmentInventoryID INT,
	DocumentID INT,
	AuditOutcomeID INT,
	AuditQuantity INT,
	AuditDate DATE,
	PersonID INT,
	AuditNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuditDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AuditQuantity', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'CommunityEquipmentInventoryID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityEquipmentInventoryAuditID'
EXEC utility.SetIndexClustered 'IX_CommunityEquipmentInventoryAudit', @TableName, 'CommunityEquipmentInventoryID,AuditDate DESC'
GO
--End table procurement.CommunityEquipmentInventoryAudit

--Begin table procurement.EquipmentCatalog
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentCatalog'

EXEC utility.AddColumn @TableName, 'ItemMake', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'ItemModel', 'VARCHAR(250)'
GO
--End table procurement.EquipmentCatalog

--Begin table procurement.ProvinceEquipmentInventoryAudit
DECLARE @TableName VARCHAR(250) = 'procurement.ProvinceEquipmentInventoryAudit'

EXEC utility.DropObject @TableName

CREATE TABLE procurement.ProvinceEquipmentInventoryAudit
	(
	ProvinceEquipmentInventoryAuditID INT IDENTITY(1,1) NOT NULL,
	ProvinceEquipmentInventoryID INT,
	DocumentID INT,
	AuditOutcomeID INT,
	AuditQuantity INT,
	AuditDate DATE,
	PersonID INT,
	AuditNotes VARCHAR(MAX)
	)

EXEC utility.SetDefaultConstraint @TableName, 'AuditDate', 'DATE', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'AuditOutcomeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'AuditQuantity', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceEquipmentInventoryID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceEquipmentInventoryAuditID'
EXEC utility.SetIndexClustered 'IX_ProvinceEquipmentInventoryAudit', @TableName, 'ProvinceEquipmentInventoryID,AuditDate DESC'
GO
--End table procurement.ProvinceEquipmentInventoryAudit
