USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForEquipmentDistributionPlan'
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForEquipmentDistributionPlan
(
@ConceptNoteID INT,
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(ISNULL(CT.Latitude, 0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(CT.Longitude,0) AS VARCHAR(MAX))
	FROM dbo.ConceptNote CN
	JOIN dbo.ConceptNoteContactEquipment CNCE ON CNCE.ConceptNoteID = CN.ConceptNoteID
		AND CN.ConceptNoteID = @ConceptNoteID
	JOIN dbo.Contact C1 ON C1.ContactID = CNCE.ContactID
	JOIN Community CT ON c1.CommunityID = ct.CommunityID  AND CT.ProvinceID = @ProvinceID
	JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = CT.ImpactDecisionID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / LEN('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForEquipmentDistributionPlan

--Begin function dbo.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	 ,@GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	 ,@CommunityMarker varchar(max)=''
	 ,@ProvinceMarker varchar(max)=''
	
	SELECT
		@CommunityMarker += '&markers=icon:' 
			+replace( dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX))
	 FROM SpotReportCommunity SRC
		JOIN dbo.Community C ON SRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND SRC.SpotReportID = @SpotReportID
		

	SELECT
		@ProvinceMarker += '&markers=icon:' 
			+ replace(dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
			+ '/i/' + IT.Icon + '|' 
			+ CAST(ISNULL(I.Latitude,0) AS VARCHAR(MAX)) 
			+ ','
			+ CAST(ISNULL(I.Longitude,0) AS VARCHAR(MAX))
	 FROM SpotReportIncident SRI
		JOIN dbo.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	SET @Gresult = @ProvinceMarker + @CommunityMarker

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForSpotReport

--Begin function dbo.FormatStaticGoogleMapForWeeklyReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
--
-- Author:			John Lyons & Todd Pires
-- Create date:	2015.05.09
-- Description:	Implemented the date range and icon grouping
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForWeeklyReport
(
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=600x480'
	DECLARE @cMarkers VARCHAR(MAX) = ''
	
	;
	WITH D AS
		(
		SELECT
			REPLACE(ID.HexColor, '#', '') +  '.png' AS Icon,
			LEFT(CAST(ISNULL(C.Latitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(ISNULL(C.Longitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM WeeklyReport.Community WRC
		JOIN dbo.Community C ON WRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.ProvinceID = @ProvinceID
	
		UNION
	
		SELECT
			it.icon AS Icon,
			LEFT(CAST(ISNULL(I.Latitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(ISNULL(I.Longitude,0) AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM dbo.Incident I
		JOIN Dropdown.IncidentType IT ON i.incidenttypeid = it.incidenttypeid
			AND I.IncidentDate >= (SELECT TOP 1 weeklyreport.WeeklyReport.StartDate FROM weeklyreport.WeeklyReport)
			AND I.IncidentDate <= (SELECT TOP 1 weeklyreport.WeeklyReport.EndDate FROM weeklyreport.WeeklyReport)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.IncidentID = I.IncidentID 
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Community C 
						WHERE C.CommunityID = IC.CommunityID AND C.ProvinceID = @ProvinceID
						)
	
				UNION
	
				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.IncidentID = I.IncidentID AND  IP.ProvinceID = @ProvinceID
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Province P 
						WHERE P.ProvinceID = IP.ProvinceID AND P.ProvinceID = @ProvinceID
						)
				)
		)
	
	SELECT @cMarkers +=
		'&markers=icon:' 
		+ REPLACE(dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/'
		+ E.Icon
		+ (
			SELECT '|' + D.LatLong 
			FROM D
			WHERE D.Icon = E.Icon
			FOR XML PATH(''), TYPE
			).value('.[1]', 'VARCHAR(MAX)')
	FROM D AS E
	GROUP BY E.Icon
	ORDER BY E.Icon

	IF (LEN(@cMarkers) - LEN(REPLACE(@cMarkers, 'markers', ''))) / len('markers') <= 1
		SELECT @cLink += '&zoom=10'
	--ENDIF

	RETURN (@cLink + @cMarkers)

END
GO
--End function dbo.FormatStaticGoogleMapForWeeklyReport

--Begin function procurement.GetLastEquipmentAuditDate
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditDate'
GO

-- ================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last auditdate for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
-- ================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditDate
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryEquipmentInventoryID INT
)

RETURNS VARCHAR(11)

AS
BEGIN

	DECLARE @cLastAuditDateFormatted VARCHAR(11) = ''
	
	IF @TerritoryTypeCode = 'Community'
		BEGIN
		
		SELECT TOP 1 @cLastAuditDateFormatted = dbo.FormatDate(T.AuditDate)
		FROM procurement.CommunityEquipmentInventoryAudit T
		WHERE T.CommunityEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	ELSE IF @TerritoryTypeCode = 'Province'	
		BEGIN
		
		SELECT TOP 1 @cLastAuditDateFormatted = dbo.FormatDate(T.AuditDate)
		FROM procurement.ProvinceEquipmentInventoryAudit T
		WHERE T.ProvinceEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	--ENDIF
	
	RETURN ISNULL(@cLastAuditDateFormatted, '')

END
GO
--End function procurement.GetLastEquipmentAuditDate

--Begin function procurement.GetLastEquipmentAuditOutcome
EXEC utility.DropObject 'procurement.GetLastEquipmentAuditOutcome'
GO

-- ====================================================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	A function to return the last Audit Outcome for an item in the procurement.CommunityEquipmentInventory or procurement.ProvinceEquipmentInventory tables
-- ====================================================================================================================================================================
CREATE FUNCTION procurement.GetLastEquipmentAuditOutcome
(
@TerritoryTypeCode VARCHAR(50),
@TerritoryEquipmentInventoryID INT
)

RETURNS VARCHAR(50)

AS
BEGIN

	DECLARE @cLastAuditOutcomeName VARCHAR(50) = ''
	
	IF @TerritoryTypeCode = 'Community'
		BEGIN
		
		SELECT TOP 1 @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM procurement.CommunityEquipmentInventoryAudit T
			JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = T.AuditOutcomeID
				AND T.CommunityEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	ELSE IF @TerritoryTypeCode = 'Province'	
		BEGIN
		
		SELECT TOP 1 @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM procurement.ProvinceEquipmentInventoryAudit T
			JOIN dropdown.AuditOutcome AO ON AO.AuditOutcomeID = T.AuditOutcomeID
				AND T.ProvinceEquipmentInventoryID = @TerritoryEquipmentInventoryID
		ORDER BY T.AuditDate DESC

		END
	--ENDIF
	
	IF @cLastAuditOutcomeName IS NULL
		BEGIN
		
		SELECT @cLastAuditOutcomeName = AO.AuditOutcomeName
		FROM dropdown.AuditOutcome AO
		WHERE AO.AuditOutcomeID = 0

		END
	--ENDIF
	
	RETURN @cLastAuditOutcomeName

END
GO
--End function procurement.GetLastEquipmentAuditOutcome