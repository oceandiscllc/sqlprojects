-- File Name:	Build-1.20 File 01 - AJACS.sql
-- Build Key:	Build-1.20 File 01 - AJACS - 2015.07.17 11.36.17

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.FormatConceptNoteReferenceCode
--		dbo.FormatConceptNoteTitle
--		dbo.FormatProgramReportReferenceCode
--		dbo.FormatSpotReportReferenceCode
--		dbo.FormatWeeklyReportReferenceCode
--		eventlog.GetPersonPermissionablesXMLByPersonID
--
-- Procedures:
--		dbo.GetProvinceNameByProvinceID
--		dbo.InitializeEntityWorkflowStep
--		dropdown.GetDocumentTypeData
--		dropdown.GetStipendData
--		dropdown.GetSurveyQuestionResponseTypeData
--		eventlog.LogPersonAction
--		permissionable.ApplyPermissionableTemplate
--		permissionable.GetEntityPermissionables
--		permissionable.GetPermissionableTemplateByPermissionableTemplateID
--		permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID
--		procurement.GetPurchaseRequestByPurchaseRequestID
--		reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
--		reporting.GetStipendPaymentReport
--		survey.GetSurveyBySurveyID
--		survey.GetSurveyQuestionBySurveyQuestionID
--
-- Schemas:
--		survey
--
-- Tables:
--		dropdown.DocumentGroup
--		dropdown.SurveyQuestionResponseType
--		permissionable.PermissionableTemplate
--		permissionable.PermissionableTemplatePermissionable
--		survey.Survey
--		survey.SurveyQuestion
--		survey.SurveyQuestionResponseChoice
--		survey.SurveySurveyQuestion
-- ==============================================================================================================================

--Begin file Build File - 00 - Prerequisites.sql
USE AJACS
GO

--Begin schema survey
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'survey')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA survey'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema survey

--End file Build File - 00 - Prerequisites.sql

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dropdown.DocumentGroup
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentGroup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentGroup
	(
	DocumentGroupID INT IDENTITY(0,1) NOT NULL,
	DocumentGroupCode VARCHAR(50),
	DocumentGroupName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentGroupID'
EXEC utility.SetIndexNonClustered 'IX_DocumentGroup', @TableName, 'DisplayOrder,DocumentGroupName', 'DocumentGroupID'
GO

SET IDENTITY_INSERT dropdown.DocumentGroup ON
GO

INSERT INTO dropdown.DocumentGroup (DocumentGroupID) VALUES (0)

SET IDENTITY_INSERT dropdown.DocumentGroup OFF
GO

INSERT INTO dropdown.DocumentGroup 
	(DocumentGroupCode,DocumentGroupName,DisplayOrder)
VALUES
	('000-099', '000-099 Communications', 1),
	('100-199', '100-199 Project Admin', 2),
	('300-399', '300-399 Inventory and Procurement', 3),
	('500-599', '500-599 General Deliverables', 4),
	('600-699', '600-699 Project Technical', 5),
	('700-799', '700-799 Activities', 6),
	('800-899', '800-899 AJACS SI', 7),
	('900-999', '900-999 Templates and Instructions', 8)
GO
--End table dropdown.DocumentGroup

--Begin table dropdown.DocumentType
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC utility.DropConstraintsAndIndexes @TableName

EXEC utility.AddColumn @TableName, 'DocumentGroupID', 'INT'
EXEC utility.AddColumn @TableName, 'DocumentTypePermissionCode', 'VARCHAR(50)'

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DocumentGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentTypeID'
EXEC utility.SetIndexNonClustered 'IX_DocumentType', @TableName, 'DocumentGroupID,DisplayOrder,DocumentTypeName', 'DocumentTypeID'
GO
--End table dropdown.DocumentType

--Begin table dropdown.SurveyQuestionResponseType
DECLARE @TableName VARCHAR(250) = 'dropdown.SurveyQuestionResponseType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SurveyQuestionResponseType
	(
	SurveyQuestionResponseTypeID INT IDENTITY(0,1) NOT NULL,
	SurveyQuestionResponseTypeCode VARCHAR(50),
	SurveyQuestionResponseTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyQuestionResponseTypeID'
EXEC utility.SetIndexNonClustered 'IX_SurveyQuestionResponseTypeName', @TableName, 'DisplayOrder,SurveyQuestionResponseTypeName', 'SurveyQuestionResponseTypeID'
GO

SET IDENTITY_INSERT dropdown.SurveyQuestionResponseType ON
GO

INSERT INTO dropdown.SurveyQuestionResponseType (SurveyQuestionResponseTypeID) VALUES (0)
GO

SET IDENTITY_INSERT dropdown.SurveyQuestionResponseType OFF
GO

INSERT INTO dropdown.SurveyQuestionResponseType 
	(SurveyQuestionResponseTypeCode, SurveyQuestionResponseTypeName) 
VALUES 
	('CommunityPicker', 'Community Picker'),
	('DatePicker', 'Date Picker'),
	('LongText', 'Long Text'),
	('NumericRange', 'Numeric Range'),
	('ProvincePicker', 'Province Picker'),
	('Select', 'Select'),
	('SelectMultiple', 'Select (Multiple)'),
	('ShortText', 'Short Text'),
	('YesNo', 'Yes / No')
GO
--End table dropdown.SurveyQuestionResponseType

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplate
	(
	PermissionableTemplateID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.PermissionableTemplatePermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplatePermissionable
	(
	PermissionableTemplatePermissionableID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateID INT,
	PermissionableID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PermissionableTemplateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableTemplatePermissionableID'
EXEC utility.SetIndexClustered 'IX_PermissionableTemplatePermissionable', @TableName, 'PermissionableTemplateID,PermissionableID'
GO
--End table permissionable.PermissionableTemplatePermissionable

--Begin table survey.Survey
DECLARE @TableName VARCHAR(250) = 'survey.Survey'

EXEC utility.DropObject @TableName

CREATE TABLE survey.Survey
	(
	SurveyID INT IDENTITY(1,1) NOT NULL,
	SurveyName VARCHAR(250),
	SurveyDescription VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyID'
GO
--End table survey.SurveyQuestion

--Begin table survey.SurveyQuestion
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestion
	(
	SurveyQuestionID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionText NVARCHAR(500),
	SurveyQuestionDescription NVARCHAR(MAX),
	SurveyQuestionResponseTypeID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionResponseTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyQuestionID'
GO
--End table survey.SurveyQuestion

--Begin table survey.SurveyQuestionResponseChoice
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionResponseChoice'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionResponseChoice
	(
	SurveyQuestionResponseChoiceID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	SurveyQuestionResponseChoiceText NVARCHAR(500),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionResponseChoiceID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionResponseChoice', @TableName, 'SurveyQuestionID,DisplayOrder,SurveyQuestionResponseChoiceID'
GO
--End table survey.SurveyQuestionResponse

--Begin table survey.SurveySurveyQuestion
DECLARE @TableName VARCHAR(250) = 'survey.SurveySurveyQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveySurveyQuestion
	(
	SurveySurveyQuestionID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	SurveyQuestionID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveySurveyQuestionID'
EXEC utility.SetIndexClustered 'IX_SurveySurveyQuestion', @TableName, 'SurveyID,DisplayOrder,SurveyQuestionID'
GO
--End table survey.SurveySurveyQuestion

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.FormatConceptNoteReferenceCode
EXEC utility.DropObject 'dbo.FormatConceptNoteReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- =========================================================================

CREATE FUNCTION dbo.FormatConceptNoteReferenceCode
(
@ConceptNoteID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteReferenceCode

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
--
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	Added ISNULL support
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cReturn VARCHAR(300)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SELECT @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4) + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteTitle

--Begin function dbo.FormatProgramReportReferenceCode
EXEC utility.DropObject 'dbo.FormatProgramReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted program report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ===========================================================================

CREATE FUNCTION dbo.FormatProgramReportReferenceCode
(
@ProgramReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-PR-' + RIGHT('0000' + CAST(@ProgramReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatProgramReportReferenceCode

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-SR-' + RIGHT('0000' + CAST(@SpotReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function dbo.FormatWeeklyReportReferenceCode
EXEC utility.DropObject 'dbo.FormatWeeklyReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return a formatted Weekly report reference code
-- ==========================================================================

CREATE FUNCTION dbo.FormatWeeklyReportReferenceCode
(
@WeeklyReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-WR-' + RIGHT('0000' + CAST(@WeeklyReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatWeeklyReportReferenceCode

--Begin function eventlog.GetPersonPermissionablesXMLByPersonID
EXEC utility.DropObject 'eventlog.GetPersonPermissionablesXMLByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.08
-- Description:	A function to return PersonPermissionable data for a specific Person record
-- ========================================================================================

CREATE FUNCTION eventlog.GetPersonPermissionablesXMLByPersonID
(
@PersonID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
	
	SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('PersonPermissionable'), ELEMENTS) AS PersonPermissionable
		FROM permissionable.PersonPermissionable T 
		WHERE T.PersonID = @PersonID
		) D

	RETURN '<PersonPermissionables>' + ISNULL(@cPersonPermissionables, '') + '</PersonPermissionables>'

END
GO
--End function eventlog.GetPersonPermissionablesXMLByPersonID
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin dbo.GetProvinceNameByProvinceID
EXEC Utility.DropObject 'dbo.GetProvinceNameByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.06
-- Description:	A stored procedure to get a province name from a province id
-- =========================================================================
CREATE PROCEDURE dbo.GetProvinceNameByProvinceID

@ProvinceID INT

AS
BEGIN

	SELECT P.ProvinceName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

END
GO
--End procedure dbo.GetProvinceNameByProvinceID

--Begin dbo.InitializeEntityWorkflowStep
EXEC Utility.DropObject 'dbo.InitializeEntityWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.07.06
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE dbo.InitializeEntityWorkflowStep

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStep
		(EntityID, WorkflowStepID)
	SELECT
		@EntityID,
		WS.WorkflowStepID
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = @EntityTypeCode

END
GO
--End procedure dbo.InitializeEntityWorkflowStep

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.20
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@IncludeZero BIT = 0,
@PersonID INT = 0,
@HasAddUpdate BIT = 0,
@HasView BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DG.DocumentGroupID,
		DG.DocumentGroupName,
		DT.DocumentTypeID,
		DT.DocumentTypePermissionCode,
		DT.DocumentTypeName
	FROM dropdown.DocumentType DT
		JOIN dropdown.DocumentGroup DG ON DG.DocumentGroupID = DT.DocumentGroupID
			AND DT.IsActive = 1
			AND (DT.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PersonPermissionable PP
				WHERE PP.PersonID = @PersonID
					AND 
						(
						@HasAddUpdate = 1 AND PP.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.AddUpdate'
							OR @HasView = 1 AND PP.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.View'
						)
				)
	ORDER BY DG.DisplayOrder, DT.DocumentTypeName

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetStipendData
EXEC Utility.DropObject 'dropdown.GetStipendData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data from the dropdown.Stipend table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetStipendData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StipendID,
		T.StipendAmount,
		T.StipendName
	FROM dropdown.Stipend T
	WHERE (T.StipendID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StipendName, T.StipendID

END
GO
--End procedure dropdown.GetStipendData

--Begin procedure dropdown.GetSurveyQuestionResponseTypeData
EXEC Utility.DropObject 'dropdown.GetSurveyQuestionResponseTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.09
-- Description:	A stored procedure to return data from the dropdown.SurveyQuestionResponseType table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetSurveyQuestionResponseTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SurveyQuestionResponseTypeID, 
		T.SurveyQuestionResponseTypeCode,
		T.SurveyQuestionResponseTypeName
	FROM dropdown.SurveyQuestionResponseType T
	WHERE (T.SurveyQuestionResponseTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SurveyQuestionResponseTypeName, T.SurveyQuestionResponseTypeID

END
GO
--End procedure dropdown.GetSurveyQuestionResponseTypeData

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Todd Pires
-- Create date: 2015.07.08
-- Description:	Added EntityIDList support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			T.PersonID,
			@Comments
		FROM dbo.Person T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonID

		END
	ELSE
		BEGIN
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			T.PersonID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetPersonPermissionablesXMLByPersonID(T.PersonID) AS XML))
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM dbo.Person T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin permissionable.ApplyPermissionableTemplate
EXEC Utility.DropObject 'permissionable.ApplyPermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.08
-- Description:	A stored procedure to get apply a permissionable tempalte to one or more person id's
-- =================================================================================================
CREATE PROCEDURE permissionable.ApplyPermissionableTemplate

@PermissionableTemplateID INT,
@PersonID INT,
@PersonIDList VARCHAR(MAX),
@Mode VARCHAR(50) = 'Additive'

AS
BEGIN

	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM dbo.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.PermissionableTemplatePermissionable PTP
			JOIN permissionable.Permissionable P1 ON P1.PermissionableID = PTP.PermissionableID
				AND PTP.PermissionableTemplateID = @PermissionableTemplateID
		) D

	INSERT INTO permissionable.PersonPermissionable
		(PersonID, PermissionableLineage)
	OUTPUT INSERTED.PersonID INTO @tOutput
	SELECT
		T.PersonID,
		T.PermissionableLineage
	FROM @tTable T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.PersonPermissionable PP
		WHERE PP.PersonID = T.PersonID
			AND PP.PermissionableLineage = T.PermissionableLineage
		)

	IF @Mode = 'Exclusive'
		BEGIN

		DELETE PP
		OUTPUT DELETED.PersonID INTO @tOutput
		FROM permissionable.PersonPermissionable PP
			JOIN @tTable T ON T.PersonID = PP.PersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tTable T
					WHERE T.PermissionableLineage = PP.PermissionableLineage
					)
						
		END
	--ENDIF

	SET @PersonIDList = ''
	
	SELECT 
		@PersonIDList = COALESCE(@PersonIDList + ',', '') + CAST(D.PersonID AS VARCHAR(10))
	FROM
		(
		SELECT DISTINCT 
			T.PersonID
		FROM @tOutput T
		) D

	SET @PersonIDList = STUFF(@PersonIDList, 1, 1, '')

	EXEC eventlog.LogPersonAction @EntityID = 0, @EventCode = 'update', @PersonID = @PersonID, @Comments = NULL, @EntityIDList = @PersonIDList

END
GO
--End permissionable.ApplyPermissionableTemplate

--Begin procedure permissionable.GetEntityPermissionables
EXEC Utility.DropObject 'permissionable.GetEntityPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date: 2015.03.08
-- Description:	A stored procedure to get system permissionables
-- =============================================================
CREATE PROCEDURE permissionable.GetEntityPermissionables

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nPadLength INT
	
	SELECT @nPadLength = LEN(CAST(COUNT(P.PermissionableID) AS VARCHAR(50)))
	FROM permissionable.Permissionable P
	
	;
	WITH HD (DisplayIndex,PermissionableID,PermissionableClass,ParentPermissionableID,DisplayGroupID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY DG.DisplayOrder, P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			'perm-' + CAST(P.PermissionableID AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			DG.DisplayGroupID,
			1
		FROM permissionable.Permissionable P
			JOIN permissionable.DisplayGroupPermissionable DGP ON DGP.PermissionableID = P.PermissionableID
			JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = DGP.DisplayGroupID
		WHERE P.ParentPermissionableID = 0
	
		UNION ALL
	
		SELECT
			CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			CAST(HD.PermissionableClass + ' perm-' + CAST(P.PermissionableID AS VARCHAR(10)) AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			HD.DisplayGroupID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM permissionable.Permissionable P
			JOIN HD ON HD.PermissionableID = P.ParentPermissionableID
		)
	
	SELECT
		HD1.NodeLevel,
		HD1.ParentPermissionableID,
		HD1.PermissionableID,
		'group-' + CAST(DG.DisplayGroupID AS VARCHAR(10)) + ' ' + RTRIM(REPLACE(HD1.PermissionableClass, 'perm-' + CAST(HD1.PermissionableID AS VARCHAR(10)), '')) AS PermissionableClass,
		P.PermissionableName,
		P.PermissionableLineage,
		DG.DisplayGroupID,
		DG.DisplayGroupName,
	
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentPermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasChildren,
		
		CASE
			WHEN @EntityTypeCode = 'Person' AND @EntityID > 0 AND EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PP.PermissionableLineage AND PP.PersonID = @EntityID AND P.PermissionableID = HD1.PermissionableID)
			THEN 1
			WHEN @EntityTypeCode = 'PermissionableTemplate' AND @EntityID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @EntityID AND PTP.PermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
	
	FROM HD HD1
		JOIN permissionable.Permissionable P ON P.PermissionableID = HD1.PermissionableID
		JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = HD1.DisplayGroupID
	ORDER BY HD1.DisplayIndex

END
GO
--End procedure permissionable.GetEntityPermissionables

--Begin procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID
EXEC Utility.DropObject 'permissionable.GetPermissionableTemplateByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplate table
-- ================================================================================================
CREATE PROCEDURE permissionable.GetPermissionableTemplateByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PT.PermissionableTemplateID,
		PT.PermissionableTemplateName
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID

--Begin procedure permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID
EXEC Utility.DropObject 'permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.08
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplatePermission table
-- ==========================================================================================================
CREATE PROCEDURE permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PTP.PermissionableID
	FROM permissionable.PermissionableTemplatePermissionable PTP
	WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID

--Begin procedure procurement.GetPurchaseRequestByPurchaseRequestID
EXEC Utility.DropObject 'procurement.GetPurchaseRequestByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.01
-- Description:	A stored procedure to return data for a purchase request
-- =====================================================================
CREATE PROCEDURE procurement.GetPurchaseRequestByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT

	SELECT @nConceptNoteID = PR.ConceptNoteID
	FROM procurement.PurchaseRequest PR
	WHERE PR.PurchaseRequestID = @PurchaseRequestID

	SELECT
		PR.PurchaseRequestID,
		PR.ConceptNoteID,
		PR.RequestPersonID,
		PR.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(PR.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		PR.DeliveryDate,
		dbo.FormatDate(PR.DeliveryDate) AS DeliveryDateFormatted,
		PR.ReferenceCode,
		PR.Address,
		PR.AddressCountryID,
		PR.Notes,
		CUR.CurrencyID,
		CUR.CurrencyName,
		COU.CountryName,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' ' + CN.Title AS ConceptNoteTitle
	FROM procurement.PurchaseRequest PR
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = PR.CurrencyID
		JOIN dropdown.Country COU ON COU.CountryID = PR.AddressCountryID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = PR.ConceptNoteID
	WHERE PR.PurchaseRequestID = @PurchaseRequestID
	
	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.ConceptNoteBudgetID,
		CNB.ItemName,
		ISNULL(OAPRCNB.Quantity, CNB.Quantity) AS Quantity,
		CNB.QuantityOfIssue,
		CNB.UnitOfIssue,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.QuantityOfIssue * ISNULL(OAPRCNB.Quantity, CNB.Quantity) * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.QuantityOfIssue * ISNULL(OAPRCNB.Quantity, CNB.Quantity) * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNB.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT 
				PRCNB.Quantity
			FROM procurement.PurchaseRequestConceptNoteBudget PRCNB 
			WHERE PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID 
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNB

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.QuantityOfIssue * ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost AS TotalCost,
		FORMAT(EC.QuantityOfIssue * ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNEC.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT PRCNEC.Quantity 
			FROM procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC 
			WHERE PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID 
				AND PRCNEC.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNEC

	SELECT
		SC.SubContractorID,
		SC.SubContractorName
	FROM procurement.PurchaseRequestSubContractor PRSC
		JOIN dbo.SubContractor SC ON SC.SubContractorID = PRSC.SubContractorID
			AND PRSC.PurchaseRequestID = @PurchaseRequestID
END
GO
--End procedure procurement.GetPurchaseRequestByPurchaseRequestID

--Begin procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestEquipmentByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purchase request reports
-- ===============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ID INT, 
		BudgetTypeName Varchar(250),
		Quantity NUMERIC(18,4),
		ItemName VARCHAR(250), 
		UnitCost NUMERIC(18,2), 
		UnitOfIssue VARCHAR(250), 
		UnitCostFormatted VARCHAR(50), 
		TotalCost NUMERIC(18,2), 
		TotalCostFormatted VARCHAR(50),
		QuantityOfIssue NUMERIC(18,4)
		)

	INSERT INTO @tTable
		(ID,BudgetTypeName,Quantity,ItemName,UnitCost,UnitOfIssue,UnitCostFormatted,TotalCost,TotalCostFormatted,QuantityOfIssue)
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID as ID,
		'Equipment' as BudgetTypeName,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.ItemName,
		EC.UnitCost,
		CAST(EC.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.QuantityOfIssue
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
		JOIN procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC ON PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID
			AND PRCNEC.PurchaseRequestID = @PurchaseRequestID

	UNION

	SELECT			
		CNB.ConceptNoteBudgetID as ID,
		BT.BudgetTypeName,
		ISNULL(PRCNB.Quantity,CNB.Quantity) AS Quantity,
		CNB.ItemName,
		CNB.UnitCost,
		CAST(CNB.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID	
		JOIN procurement.PurchaseRequestConceptNoteBudget PRCNB ON PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(ID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		T.ID,
		T.BudgetTypeName,
		T.Quantity,
		T.ItemName,
		T.UnitCost,
		T.UnitOfIssue,
		T.UnitCostFormatted,
		T.TotalCost,
		T.TotalCostFormatted,
		T.QuantityOfIssue
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

--Begin procedure reporting.GetStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
		(
		SELECT
			CSP.StipendName,
			CSP.CommunityID,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			CSP.ProvinceID,
			COUNT(CSP.StipendName) AS StipendNameCount
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
		GROUP BY CSP.StipendName, CSP.CommunityID, CSP.ProvinceID
		)

	SELECT
		CASE
			WHEN OAC.CommunityName IS NULL
			THEN (SELECT P.ProvinceName + ' Province' FROM dbo.Province P WHERE P.ProvinceID = (SELECT TOP 1 SD.ProvinceID FROM SD) )
			ELSE OAC.CommunityName
		END AS Center,

		A.[Command],
		B.[Command Count],
		A.[General],
		B.[General Count],
		A.[Colonel],
		B.[Colonel Count],
		A.[Colonel Doctor],
		B.[Colonel Doctor Count],
		A.[Lieutenant Colonel],
		B.[Lieutenant Colonel Count],
		A.[Major],
		B.[Major Count],
		A.[Captain],
		B.[Captain Count],
		A.[Captain Doctor],
		B.[Captain Doctor Count],
		A.[First Lieutenant],
		B.[First Lieutenant Count],
		A.[Contracted Officer],
		B.[Contracted Officer Count],
		A.[First Sergeant],
		B.[First Sergeant Count],
		A.[Sergeant],
		B.[Sergeant Count],
		A.[First Adjutant],
		B.[First Adjutant Count],
		A.[Adjutant],
		B.[Adjutant Count],
		A.[Policeman],
		B.[Policeman Count],
		A.[Contracted Policeman],
		B.[Contracted Policeman Count],
		(ISNULL(A.[Adjutant], 0) + ISNULL(A.[Captain Doctor], 0) + ISNULL(A.[Captain], 0) + ISNULL(A.[Colonel Doctor], 0) + ISNULL(A.[Colonel], 0) + ISNULL(A.[Command], 0) + ISNULL(A.[Contracted Officer], 0) + ISNULL(A.[Contracted Policeman], 0) + ISNULL(A.[First Adjutant], 0) + ISNULL(A.[First Lieutenant], 0) + ISNULL(A.[First Sergeant], 0) + ISNULL(A.[General], 0) + ISNULL(A.[Lieutenant Colonel], 0) + ISNULL(A.[Major], 0) + ISNULL(A.[Policeman], 0) + ISNULL(A.[Sergeant], 0)) AS [Total Stipend],
		(ISNULL(B.[Adjutant Count], 0) + ISNULL(B.[Captain Doctor Count], 0) + ISNULL(B.[Captain Count], 0) + ISNULL(B.[Colonel Doctor Count], 0) + ISNULL(B.[Colonel Count], 0) + ISNULL(B.[Command Count], 0) + ISNULL(B.[Contracted Officer Count], 0) + ISNULL(B.[Contracted Policeman Count], 0) + ISNULL(B.[First Adjutant Count], 0) + ISNULL(B.[First Lieutenant Count], 0) + ISNULL(B.[First Sergeant Count], 0) + ISNULL(B.[General Count], 0) + ISNULL(B.[Lieutenant Colonel Count], 0) + ISNULL(B.[Major Count], 0) + ISNULL(B.[Policeman Count], 0) + ISNULL(B.[Sergeant Count], 0)) AS [Total Count],

		CASE
			WHEN A.CommunityID = 0
			THEN 3000.00
			ELSE 500.00
		END AS [Running Costs]

	FROM
		(
		SELECT
			PVT.*
		FROM
			(
			SELECT
				SD.StipendName,
				SD.CommunityID,
				SD.StipendAmountAuthorized
			FROM SD
			) AS D
		PIVOT
			(
			MAX(D.StipendAmountAuthorized)
			FOR D.StipendName IN
				(
				[Command],[General],[Colonel],[Colonel Doctor],[Lieutenant Colonel],[Major],[Captain],[Captain Doctor],[First Lieutenant],[Contracted Officer],[First Sergeant],[Sergeant],[First Adjutant],[Adjutant],[Policeman],[Contracted Policeman]
				)
			) AS PVT
		) A
		JOIN
			(
			SELECT
				PVT.*
			FROM
				(
				SELECT
					SD.StipendName + ' Count' AS StipendName,
					SD.CommunityID,
					SD.StipendNameCount
				FROM SD
				) AS D
			PIVOT
				(
				MAX(D.StipendNameCount)
				FOR D.StipendName IN
					(
					[Command Count],[General Count],[Colonel Count],[Colonel Doctor Count],[Lieutenant Colonel Count],[Major Count],[Captain Count],[Captain Doctor Count],[First Lieutenant Count],[Contracted Officer Count],[First Sergeant Count],[Sergeant Count],[First Adjutant Count],[Adjutant Count],[Policeman Count],[Contracted Policeman Count]
					)
				) AS PVT
			) B ON B.CommunityID = A.CommunityID
		OUTER APPLY
			(
			SELECT
				C.CommunityName
			FROM dbo.Community C
			WHERE C.CommunityID = A.CommunityID
			) OAC
	ORDER BY OAC.CommunityName, A.CommunityID
	
END
GO
--End procedure reporting.GetStipendPaymentReport

--Begin survey.GetSurveyBySurveyID
EXEC Utility.DropObject 'survey.GetSurveyBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.15
-- Description:	A stored procedure to get a survey from the database
-- =================================================================
CREATE PROCEDURE survey.GetSurveyBySurveyID

@SurveyID INT

AS
BEGIN

	SELECT 
		S.IsActive,
		S.SurveyID, 
		S.SurveyName, 
		S.SurveyDescription
	FROM survey.Survey S
	WHERE S.SurveyID = @SurveyID

	SELECT
		SQ.SurveyQuestionID,
		SQ.SurveyQuestionText,
		SQ.SurveyQuestionDescription,
		SQT.SurveyQuestionResponseTypeCode
	FROM survey.SurveySurveyQuestion SSQ
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQ.SurveyID = @SurveyID
	ORDER BY SSQ.DisplayOrder, SQ.SurveyQuestionText, SSQ.SurveySurveyQuestionID

	SELECT
		SQRC.DisplayOrder, 
		SQRC.SurveyQuestionID,
		SQRC.SurveyQuestionResponseChoiceID,
		SQRC.SurveyQuestionResponseChoiceText
	FROM survey.SurveyQuestionResponseChoice SQRC
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyQuestionID = SQRC.SurveyQuestionID
			AND SSQ.SurveyID = @SurveyID
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
	ORDER BY SQRC.SurveyQuestionID, SQRC.DisplayOrder, SQRC.SurveyQuestionResponseChoiceText, SQRC.SurveyQuestionResponseChoiceID

END
GO
--End procedure survey.GetSurveyBySurveyID

--Begin survey.GetSurveyQuestionBySurveyQuestionID
EXEC Utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.13
-- Description:	A stored procedure to get a survey question from the database
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionBySurveyQuestionID

@SurveyQuestionID INT

AS
BEGIN

	SELECT 
		SQ.IsActive,
		SQ.SurveyQuestionID, 
		SQ.SurveyQuestionDescription, 
		SQ.SurveyQuestionText, 
		SQT.SurveyQuestionResponseTypeID, 
		SQT.SurveyQuestionResponseTypeCode, 
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveyQuestion SQ
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SQ.SurveyQuestionID = @SurveyQuestionID

	SELECT
		SQRC.SurveyQuestionResponseChoiceID, 
		SQRC.SurveyQuestionResponseChoiceText
	FROM survey.SurveyQuestionResponseChoice SQRC
	WHERE SQRC.SurveyQuestionID = @SurveyQuestionID
	ORDER BY SQRC.DisplayOrder, SQRC.SurveyQuestionResponseChoiceText

END
GO
--End procedure survey.GetSurveyQuestionBySurveyQuestionID

--Begin deprecated procedure cleanup
EXEC Utility.DropObject 'reporting.GeConceptNoteByConceptNoteID'
EXEC Utility.DropObject 'reporting.GeConceptNoteIndicatorByConceptNoteID'
GO
--End deprecated procedure cleanup

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Survey')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('Survey', 'Survey')

	END
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Surveys', @NewMenuItemText='Surveys', @BeforeMenuItemCode='Admin', @PermissionableLineageList='SurveyManagement.ListQuestions,SurveyManagement.ListSurveys', @Icon='fa fa-fw fa-check-square-o'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyManagement', @NewMenuItemText='Manage Surveys', @ParentMenuItemCode='Surveys', @PermissionableLineageList='SurveyManagement.ListQuestions,SurveyManagement.ListSurveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyList', @NewMenuItemLink='/surveymanagement/listsurveys', @NewMenuItemText='Surveys', @ParentMenuItemCode='SurveyManagement', @PermissionableLineageList='SurveyManagement.ListSurveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='QuestionList', @NewMenuItemLink='/surveymanagement/listquestions', @NewMenuItemText='Questions', @ParentMenuItemCode='SurveyManagement', @PermissionableLineageList='SurveyManagement.ListQuestions'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyResults', @NewMenuItemText='Survey Results', @ParentMenuItemCode='Surveys', @AfterMenuItemCode='SurveyManagement'
GO
--End table dbo.MenuItem

--Begin table dropdown.DocumentType
UPDATE dropdown.DocumentType
SET DocumentGroupID = (SELECT DG.DocumentGroupID FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = '500-599')
WHERE DocumentTypeID > 0
	AND DocumentGroupID = 0
GO

DECLARE @tTable TABLE (DisplayOrder INT NOT NULL PRIMARY KEY IDENTITY(1,1), DocumentTypeID INT NOT NULL)
INSERT INTO @tTable (DocumentTypeID) SELECT DT.DocumentTypeID FROM dropdown.DocumentType DT WHERE DT.IsActive = 1 AND DT.DocumentTypeID > 0 AND LEFT(DT.DocumentTypeName, 1) <> '5' ORDER BY DT.DisplayOrder, DT.DocumentTypeName

UPDATE DT
SET DT.DocumentTypeName = CAST(T.DisplayOrder + 510 AS VARCHAR(3)) + ' ' + DT.DocumentTypeName
FROM dropdown.DocumentType DT 
	JOIN @tTable T ON T.DocumentTypeID = DT.DocumentTypeID
		AND ISNUMERIC(LEFT(ISNULL(DT.DocumentTypeName, 0), 1)) = 0
GO

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '004 Branding and Marking' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '000-099 Communications' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '004 Branding and Marking')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '100 Client Requests and Approvals' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '100 Client Requests and Approvals')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '101 Internal Admin Correspondence' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '101 Internal Admin Correspondence')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '102 Office and Residence Leases' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '102 Office and Residence Leases')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '103 Various Project Admin' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '103 Various Project Admin')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '104 Hotels Reservations' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '104 Hotels Reservations')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '105 Project Insurance' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '105 Project Insurance')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '106 Security' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '106 Security')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '107 Contact List' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '107 Contact List')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '108 Translations' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '108 Translations')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '109 IT Technical Info' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '109 IT Technical Info')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '301 Project Inventory List' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '301 Project Inventory List')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '302 Procurement' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '302 Procurement')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '303 Shipping Forms and Customs Docs' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '303 Shipping Forms and Customs Docs')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '304 Waivers' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '304 Waivers')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '306 Commodities Tracking' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '306 Commodities Tracking')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '500 RFP for Project' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '500 RFP for Project')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '501 Technical Proposal and Budget' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '501 Technical Proposal and Budget')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '502 Agreements and Mods' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '502 Agreements and Mods')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '503 Work Plans and Budgets' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '503 Work Plans and Budgets')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '504 Meeting Notes' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '504 Meeting Notes')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '505 Trip Reports' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '505 Trip Reports')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '506 Quarterly Reports' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '506 Quarterly Reports')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '507 Annual Reports' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '507 Annual Reports')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '508 M&E Plan' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '508 M&E Plan')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '509 M&E Reporting' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '509 M&E Reporting')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '510 Additional Reports and Deliverables' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '510 Additional Reports and Deliverables')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '600 Project Org Chart' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '600 Project Org Chart')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '601 Community Engagement' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '601 Community Engagement')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '602 Justice' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '602 Justice')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '603 M&E' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '603 M&E')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '604 Policing' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '604 Policing')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '605 Research' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '605 Research')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '700 Activities Manual' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '700-799 Activities' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '700 Activities Manual')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '701 Activities' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '700-799 Activities' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '701 Activities')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '702 Activity Management ' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '700-799 Activities' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '702 Activity Management ')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '801 SI Activities' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '801 SI Activities')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '802 SI Communications' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '802 SI Communications')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '803 SI Finance' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '803 SI Finance')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '804 SI General Deliverables' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '804 SI General Deliverables')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '805 SI Human Resources' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '805 SI Human Resources')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '806 SI Inventory and Procurement' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '806 SI Inventory and Procurement')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '807 SI Project Admin' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '807 SI Project Admin')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '808 SI Project Technical' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '808 SI Project Technical')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '900 Start-Up' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '900 Start-Up')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '901 HR ' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '901 HR ')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '902 Procurement ' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '902 Procurement ')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '903 Finance' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '903 Finance')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '904 Contracts' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '904 Contracts')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '905 Activity Management' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '905 Activity Management')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '906 IT' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '906 IT')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '907 Security' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '907 Security')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '908 Communications' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '908 Communications')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '909 Project Admin' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '909 Project Admin')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '910 Closeout' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '910 Closeout')
GO

UPDATE DT
SET DT.DocumentTypeCode = LEFT(DT.DocumentTypeName, 3)
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IS NULL
GO

UPDATE DT
SET DT.DocumentTypePermissionCode = LEFT(DT.DocumentTypeName, 3)
FROM dropdown.DocumentType DT
WHERE ISNUMERIC(LEFT(ISNULL(DT.DocumentTypeName, ''), 1)) = 1
GO

UPDATE DT
SET DT.DisplayOrder = 0
FROM dropdown.DocumentType DT
GO
--End table dropdown.DocumentType

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DocumentType')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('DocumentType','Document Library', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Survey')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 7
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Survey','Surveys', 8)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Document')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Document','Document List')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DocumentType')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'DocumentType','Document Library')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'SurveyManagement')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'SurveyManagement','Survey Management')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ListQuestions')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ListQuestions',
		'Questions List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'AddUpdateQuestion')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdateQuestion',
		'Add / Edit Questions'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'AddUpdateSurvey')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdateSurvey',
		'Add / Edit Surveys'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ViewQuestion')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ViewQuestion',
		'View Questions'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ViewSurvey')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ViewSurvey',
		'View Surveys'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ListSurveys')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ListSurveys',
		'Surveys List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	(SELECT P1.PermissionableID FROM permissionable.Permissionable P1 WHERE P1.PermissionableLineage = 'DocumentType'),
	DT.DocumentTypePermissionCode,
	DT.DocumentTypeName
FROM dropdown.DocumentType DT
WHERE DT.IsActive = 1
	AND DT.DocumentTypeID > 0
	AND NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P2 
	WHERE P2.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode
	)
ORDER BY DT.DocumentTypeName
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	(SELECT P1.PermissionableID FROM permissionable.Permissionable P1 WHERE P1.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode),
	'AddUpdate',
	'Add / Edit'
FROM dropdown.DocumentType DT
WHERE DT.IsActive = 1
	AND DT.DocumentTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.AddUpdate'
		)
ORDER BY DT.DocumentTypeName
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	(SELECT P1.PermissionableID FROM permissionable.Permissionable P1 WHERE P1.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode),
	'View',
	'View'
FROM dropdown.DocumentType DT
WHERE DT.IsActive = 1
	AND DT.DocumentTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.View'
		)
ORDER BY DT.DocumentTypeName
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN ('Document.AddUpdate','Document.View','Document.List')
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND 
			(
			MI.MenuItemCode = 'Document'
				OR NOT EXISTS
					(
					SELECT 1
					FROM permissionable.Permissionable P
					WHERE P.PermissionableLineage = MIPL.PermissionableLineage
					)
			)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID, PermissionableLineage)
SELECT 
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Document'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage LIKE 'DocumentType.___.%'
GO

DELETE DGP
FROM permissionable.DisplayGroupPermissionable DGP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = DGP.PermissionableID
	)
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'PermissionableTemplate')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'PermissionableTemplate','Permissionable Template')
		
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PermissionableTemplate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PermissionableTemplate.AddUpdate'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'View',
	'View',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PermissionableTemplate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PermissionableTemplate.View'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'List',
	'List',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PermissionableTemplate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PermissionableTemplate.List'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DocumentType'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Document')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DocumentType'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('DocumentType')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Survey'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('SurveyManagement')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.20 File 01 - AJACS - 2015.07.17 11.36.17')
GO
--End build tracking

