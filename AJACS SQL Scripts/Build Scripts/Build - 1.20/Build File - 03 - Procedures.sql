USE AJACS
GO

--Begin dbo.GetProvinceNameByProvinceID
EXEC Utility.DropObject 'dbo.GetProvinceNameByProvinceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.06
-- Description:	A stored procedure to get a province name from a province id
-- =========================================================================
CREATE PROCEDURE dbo.GetProvinceNameByProvinceID

@ProvinceID INT

AS
BEGIN

	SELECT P.ProvinceName
	FROM dbo.Province P
	WHERE P.ProvinceID = @ProvinceID

END
GO
--End procedure dbo.GetProvinceNameByProvinceID

--Begin dbo.InitializeEntityWorkflowStep
EXEC Utility.DropObject 'dbo.InitializeEntityWorkflowStep'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date: 2015.07.06
-- Description:	A stored procedure to initialize a workflow
-- ========================================================
CREATE PROCEDURE dbo.InitializeEntityWorkflowStep

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	INSERT INTO workflow.EntityWorkflowStep
		(EntityID, WorkflowStepID)
	SELECT
		@EntityID,
		WS.WorkflowStepID
	FROM workflow.WorkflowStep WS
		JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
			AND W.EntityTypeCode = @EntityTypeCode

END
GO
--End procedure dbo.InitializeEntityWorkflowStep

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.20
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@IncludeZero BIT = 0,
@PersonID INT = 0,
@HasAddUpdate BIT = 0,
@HasView BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DG.DocumentGroupID,
		DG.DocumentGroupName,
		DT.DocumentTypeID,
		DT.DocumentTypePermissionCode,
		DT.DocumentTypeName
	FROM dropdown.DocumentType DT
		JOIN dropdown.DocumentGroup DG ON DG.DocumentGroupID = DT.DocumentGroupID
			AND DT.IsActive = 1
			AND (DT.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PersonPermissionable PP
				WHERE PP.PersonID = @PersonID
					AND 
						(
						@HasAddUpdate = 1 AND PP.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.AddUpdate'
							OR @HasView = 1 AND PP.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.View'
						)
				)
	ORDER BY DG.DisplayOrder, DT.DocumentTypeName

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure dropdown.GetStipendData
EXEC Utility.DropObject 'dropdown.GetStipendData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.14
-- Description:	A stored procedure to return data from the dropdown.Stipend table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetStipendData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StipendID,
		T.StipendAmount,
		T.StipendName
	FROM dropdown.Stipend T
	WHERE (T.StipendID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StipendName, T.StipendID

END
GO
--End procedure dropdown.GetStipendData

--Begin procedure dropdown.GetSurveyQuestionResponseTypeData
EXEC Utility.DropObject 'dropdown.GetSurveyQuestionResponseTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.09
-- Description:	A stored procedure to return data from the dropdown.SurveyQuestionResponseType table
-- =================================================================================================
CREATE PROCEDURE dropdown.GetSurveyQuestionResponseTypeData
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.SurveyQuestionResponseTypeID, 
		T.SurveyQuestionResponseTypeCode,
		T.SurveyQuestionResponseTypeName
	FROM dropdown.SurveyQuestionResponseType T
	WHERE (T.SurveyQuestionResponseTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.SurveyQuestionResponseTypeName, T.SurveyQuestionResponseTypeID

END
GO
--End procedure dropdown.GetSurveyQuestionResponseTypeData

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:		Todd Pires
-- Create date: 2015.02.10
-- Description:	A stored procedure to add data to the eventlog.EventLog table
--
-- Author:		Todd Pires
-- Create date: 2015.03.11
-- Description:	Add comments support
--
-- Author:		Todd Pires
-- Create date: 2015.07.08
-- Description:	Added EntityIDList support
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL,
@EntityIDList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EntityID > 0
		SET @EntityIDList = CAST(@EntityID AS VARCHAR(10))
	--ENDIF

	IF @EventCode = 'Read'
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			T.PersonID,
			@Comments
		FROM dbo.Person T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonID

		END
	ELSE
		BEGIN
	
		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			'Person',
			T.PersonID,
			@Comments,
			(
			SELECT T.*, 
			(SELECT CAST(eventlog.GetPersonPermissionablesXMLByPersonID(T.PersonID) AS XML))
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM dbo.Person T
			JOIN dbo.ListToTable(@EntityIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = T.PersonID

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin permissionable.ApplyPermissionableTemplate
EXEC Utility.DropObject 'permissionable.ApplyPermissionableTemplate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.08
-- Description:	A stored procedure to get apply a permissionable tempalte to one or more person id's
-- =================================================================================================
CREATE PROCEDURE permissionable.ApplyPermissionableTemplate

@PermissionableTemplateID INT,
@PersonID INT,
@PersonIDList VARCHAR(MAX),
@Mode VARCHAR(50) = 'Additive'

AS
BEGIN

	DECLARE @tOutput TABLE (PersonID INT)
	DECLARE @tTable TABLE (PersonID INT, PermissionableLineage VARCHAR(MAX))

	INSERT INTO @tTable
		(PersonID, PermissionableLineage)
	SELECT
		CAST(LTT.ListItem AS INT),
		D.PermissionableLineage
	FROM dbo.ListToTable(@PersonIDList, ',') LTT,
		(
		SELECT 
			P1.PermissionableLineage
		FROM permissionable.PermissionableTemplatePermissionable PTP
			JOIN permissionable.Permissionable P1 ON P1.PermissionableID = PTP.PermissionableID
				AND PTP.PermissionableTemplateID = @PermissionableTemplateID
		) D

	INSERT INTO permissionable.PersonPermissionable
		(PersonID, PermissionableLineage)
	OUTPUT INSERTED.PersonID INTO @tOutput
	SELECT
		T.PersonID,
		T.PermissionableLineage
	FROM @tTable T
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM permissionable.PersonPermissionable PP
		WHERE PP.PersonID = T.PersonID
			AND PP.PermissionableLineage = T.PermissionableLineage
		)

	IF @Mode = 'Exclusive'
		BEGIN

		DELETE PP
		OUTPUT DELETED.PersonID INTO @tOutput
		FROM permissionable.PersonPermissionable PP
			JOIN @tTable T ON T.PersonID = PP.PersonID
				AND NOT EXISTS
					(
					SELECT 1
					FROM @tTable T
					WHERE T.PermissionableLineage = PP.PermissionableLineage
					)
						
		END
	--ENDIF

	SET @PersonIDList = ''
	
	SELECT 
		@PersonIDList = COALESCE(@PersonIDList + ',', '') + CAST(D.PersonID AS VARCHAR(10))
	FROM
		(
		SELECT DISTINCT 
			T.PersonID
		FROM @tOutput T
		) D

	SET @PersonIDList = STUFF(@PersonIDList, 1, 1, '')

	EXEC eventlog.LogPersonAction @EntityID = 0, @EventCode = 'update', @PersonID = @PersonID, @Comments = NULL, @EntityIDList = @PersonIDList

END
GO
--End permissionable.ApplyPermissionableTemplate

--Begin procedure permissionable.GetEntityPermissionables
EXEC Utility.DropObject 'permissionable.GetEntityPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Todd Pires
-- Create date: 2015.03.08
-- Description:	A stored procedure to get system permissionables
-- =============================================================
CREATE PROCEDURE permissionable.GetEntityPermissionables

@EntityTypeCode VARCHAR(50),
@EntityID INT

AS
BEGIN

	DECLARE @nPadLength INT
	
	SELECT @nPadLength = LEN(CAST(COUNT(P.PermissionableID) AS VARCHAR(50)))
	FROM permissionable.Permissionable P
	
	;
	WITH HD (DisplayIndex,PermissionableID,PermissionableClass,ParentPermissionableID,DisplayGroupID,NodeLevel)
		AS
		(
		SELECT
			CONVERT(varchar(255), RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY DG.DisplayOrder, P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			'perm-' + CAST(P.PermissionableID AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			DG.DisplayGroupID,
			1
		FROM permissionable.Permissionable P
			JOIN permissionable.DisplayGroupPermissionable DGP ON DGP.PermissionableID = P.PermissionableID
			JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = DGP.DisplayGroupID
		WHERE P.ParentPermissionableID = 0
	
		UNION ALL
	
		SELECT
			CONVERT(varchar(255), RTRIM(HD.DisplayIndex) + ',' + RIGHT(REPLICATE('0', @nPadLength) + CAST(ROW_NUMBER() OVER (PARTITION BY NodeLevel ORDER BY P.DisplayOrder, P.PermissionableName) AS VARCHAR(10)), @nPadLength)),
			P.PermissionableID,
			CAST(HD.PermissionableClass + ' perm-' + CAST(P.PermissionableID AS VARCHAR(10)) AS VARCHAR(MAX)),
			P.ParentPermissionableID,
			HD.DisplayGroupID,
			HD.NodeLevel + 1 AS NodeLevel
		FROM permissionable.Permissionable P
			JOIN HD ON HD.PermissionableID = P.ParentPermissionableID
		)
	
	SELECT
		HD1.NodeLevel,
		HD1.ParentPermissionableID,
		HD1.PermissionableID,
		'group-' + CAST(DG.DisplayGroupID AS VARCHAR(10)) + ' ' + RTRIM(REPLACE(HD1.PermissionableClass, 'perm-' + CAST(HD1.PermissionableID AS VARCHAR(10)), '')) AS PermissionableClass,
		P.PermissionableName,
		P.PermissionableLineage,
		DG.DisplayGroupID,
		DG.DisplayGroupName,
	
		CASE
			WHEN EXISTS (SELECT 1 FROM HD HD2 WHERE HD2.ParentPermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasChildren,
		
		CASE
			WHEN @EntityTypeCode = 'Person' AND @EntityID > 0 AND EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PP.PermissionableLineage AND PP.PersonID = @EntityID AND P.PermissionableID = HD1.PermissionableID)
			THEN 1
			WHEN @EntityTypeCode = 'PermissionableTemplate' AND @EntityID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @EntityID AND PTP.PermissionableID = HD1.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
	
	FROM HD HD1
		JOIN permissionable.Permissionable P ON P.PermissionableID = HD1.PermissionableID
		JOIN permissionable.DisplayGroup DG ON DG.DisplayGroupID = HD1.DisplayGroupID
	ORDER BY HD1.DisplayIndex

END
GO
--End procedure permissionable.GetEntityPermissionables

--Begin procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID
EXEC Utility.DropObject 'permissionable.GetPermissionableTemplateByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.04
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplate table
-- ================================================================================================
CREATE PROCEDURE permissionable.GetPermissionableTemplateByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PT.PermissionableTemplateID,
		PT.PermissionableTemplateName
	FROM permissionable.PermissionableTemplate PT
	WHERE PT.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID

--Begin procedure permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID
EXEC Utility.DropObject 'permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.08
-- Description:	A stored procedure to get data from the permissionable.PermissionableTemplatePermission table
-- ==========================================================================================================
CREATE PROCEDURE permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID

@PermissionableTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		PTP.PermissionableID
	FROM permissionable.PermissionableTemplatePermissionable PTP
	WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID
	
END
GO
--End procedure permissionable.GetPermissionableTemplatePermissionsByPermissionableTemplateID

--Begin procedure procurement.GetPurchaseRequestByPurchaseRequestID
EXEC Utility.DropObject 'procurement.GetPurchaseRequestByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Kevin Ross
-- Create date:	2015.04.01
-- Description:	A stored procedure to return data for a purchase request
-- =====================================================================
CREATE PROCEDURE procurement.GetPurchaseRequestByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT

	SELECT @nConceptNoteID = PR.ConceptNoteID
	FROM procurement.PurchaseRequest PR
	WHERE PR.PurchaseRequestID = @PurchaseRequestID

	SELECT
		PR.PurchaseRequestID,
		PR.ConceptNoteID,
		PR.RequestPersonID,
		PR.PointOfContactPersonID,
		dbo.FormatPersonNameByPersonID(PR.PointOfContactPersonID, 'LastFirst') AS PointOfContactPersonNameFormatted,
		PR.DeliveryDate,
		dbo.FormatDate(PR.DeliveryDate) AS DeliveryDateFormatted,
		PR.ReferenceCode,
		PR.Address,
		PR.AddressCountryID,
		PR.Notes,
		CUR.CurrencyID,
		CUR.CurrencyName,
		COU.CountryName,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) + ' ' + CN.Title AS ConceptNoteTitle
	FROM procurement.PurchaseRequest PR
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = PR.CurrencyID
		JOIN dropdown.Country COU ON COU.CountryID = PR.AddressCountryID
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = PR.ConceptNoteID
	WHERE PR.PurchaseRequestID = @PurchaseRequestID
	
	SELECT			
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.ConceptNoteBudgetID,
		CNB.ItemName,
		ISNULL(OAPRCNB.Quantity, CNB.Quantity) AS Quantity,
		CNB.QuantityOfIssue,
		CNB.UnitOfIssue,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.QuantityOfIssue * ISNULL(OAPRCNB.Quantity, CNB.Quantity) * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.QuantityOfIssue * ISNULL(OAPRCNB.Quantity, CNB.Quantity) * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNB.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT 
				PRCNB.Quantity
			FROM procurement.PurchaseRequestConceptNoteBudget PRCNB 
			WHERE PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID 
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNB

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		EC.QuantityOfIssue * ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost AS TotalCost,
		FORMAT(EC.QuantityOfIssue * ISNULL(OAPRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost, 'C', 'en-us') AS TotalCostFormatted,

		CASE
			WHEN OAPRCNEC.Quantity IS NOT NULL
			THEN 1
			ELSE 0 
		END AS IsSelected

	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @nConceptNoteID
		OUTER APPLY 
			(
			SELECT PRCNEC.Quantity 
			FROM procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC 
			WHERE PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID 
				AND PRCNEC.PurchaseRequestID = @PurchaseRequestID
			) AS OAPRCNEC

	SELECT
		SC.SubContractorID,
		SC.SubContractorName
	FROM procurement.PurchaseRequestSubContractor PRSC
		JOIN dbo.SubContractor SC ON SC.SubContractorID = PRSC.SubContractorID
			AND PRSC.PurchaseRequestID = @PurchaseRequestID
END
GO
--End procedure procurement.GetPurchaseRequestByPurchaseRequestID

--Begin procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID
EXEC Utility.DropObject 'reporting.GetPurchaseRequestEquipmentByPurchaseRequestID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			John Lyons
-- Create date:	2015.04.02
-- Description:	A stored procedure to return data for the purchase request reports
-- ===============================================================================
CREATE PROCEDURE reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

@PurchaseRequestID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tTable TABLE 
		(
		ID INT, 
		BudgetTypeName Varchar(250),
		Quantity NUMERIC(18,4),
		ItemName VARCHAR(250), 
		UnitCost NUMERIC(18,2), 
		UnitOfIssue VARCHAR(250), 
		UnitCostFormatted VARCHAR(50), 
		TotalCost NUMERIC(18,2), 
		TotalCostFormatted VARCHAR(50),
		QuantityOfIssue NUMERIC(18,4)
		)

	INSERT INTO @tTable
		(ID,BudgetTypeName,Quantity,ItemName,UnitCost,UnitOfIssue,UnitCostFormatted,TotalCost,TotalCostFormatted,QuantityOfIssue)
	SELECT
		CNEC.ConceptNoteEquipmentCatalogID as ID,
		'Equipment' as BudgetTypeName,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) AS Quantity,
		EC.ItemName,
		EC.UnitCost,
		CAST(EC.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNEC.Quantity, CNEC.Quantity) * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.QuantityOfIssue
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
		JOIN procurement.PurchaseRequestConceptNoteEquipmentCatalog PRCNEC ON PRCNEC.ConceptNoteEquipmentCatalogID = CNEC.ConceptNoteEquipmentCatalogID
			AND PRCNEC.PurchaseRequestID = @PurchaseRequestID

	UNION

	SELECT			
		CNB.ConceptNoteBudgetID as ID,
		BT.BudgetTypeName,
		ISNULL(PRCNB.Quantity,CNB.Quantity) AS Quantity,
		CNB.ItemName,
		CNB.UnitCost,
		CAST(CNB.UnitOfIssue AS VARCHAR(MAX)) AS UnitOfIssue,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue AS TotalCost,
		FORMAT(ISNULL(PRCNB.Quantity,CNB.Quantity) * CNB.UnitCost * CNB.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID	
		JOIN procurement.PurchaseRequestConceptNoteBudget PRCNB ON PRCNB.ConceptNoteBudgetID = CNB.ConceptNoteBudgetID
				AND PRCNB.PurchaseRequestID = @PurchaseRequestID

	IF NOT EXISTS (SELECT 1 FROM @tTable)
		BEGIN

		INSERT INTO @tTable
			(ID)
		VALUES
			(0)

		END
	--ENDIF

	SELECT
		T.ID,
		T.BudgetTypeName,
		T.Quantity,
		T.ItemName,
		T.UnitCost,
		T.UnitOfIssue,
		T.UnitCostFormatted,
		T.TotalCost,
		T.TotalCostFormatted,
		T.QuantityOfIssue
	FROM @tTable T

END
GO
--End procedure reporting.GetPurchaseRequestEquipmentByPurchaseRequestID

--Begin procedure reporting.GetStipendPaymentReport
EXEC Utility.DropObject 'reporting.GetStipendPaymentReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create date:	2015.06.29
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetStipendPaymentReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
		(
		SELECT
			CSP.StipendName,
			CSP.CommunityID,
			SUM(CSP.StipendAmountAuthorized) AS StipendAmountAuthorized,
			CSP.ProvinceID,
			COUNT(CSP.StipendName) AS StipendNameCount
		FROM reporting.SearchResult SR
			JOIN dbo.ContactStipendPayment CSP ON CSP.ContactStipendPaymentID = SR.EntityID
				AND SR.EntityTypeCode = 'ContactStipendPayment'
				AND SR.PersonID = @PersonID
		GROUP BY CSP.StipendName, CSP.CommunityID, CSP.ProvinceID
		)

	SELECT
		CASE
			WHEN OAC.CommunityName IS NULL
			THEN (SELECT P.ProvinceName + ' Province' FROM dbo.Province P WHERE P.ProvinceID = (SELECT TOP 1 SD.ProvinceID FROM SD) )
			ELSE OAC.CommunityName
		END AS Center,

		A.[Command],
		B.[Command Count],
		A.[General],
		B.[General Count],
		A.[Colonel],
		B.[Colonel Count],
		A.[Colonel Doctor],
		B.[Colonel Doctor Count],
		A.[Lieutenant Colonel],
		B.[Lieutenant Colonel Count],
		A.[Major],
		B.[Major Count],
		A.[Captain],
		B.[Captain Count],
		A.[Captain Doctor],
		B.[Captain Doctor Count],
		A.[First Lieutenant],
		B.[First Lieutenant Count],
		A.[Contracted Officer],
		B.[Contracted Officer Count],
		A.[First Sergeant],
		B.[First Sergeant Count],
		A.[Sergeant],
		B.[Sergeant Count],
		A.[First Adjutant],
		B.[First Adjutant Count],
		A.[Adjutant],
		B.[Adjutant Count],
		A.[Policeman],
		B.[Policeman Count],
		A.[Contracted Policeman],
		B.[Contracted Policeman Count],
		(ISNULL(A.[Adjutant], 0) + ISNULL(A.[Captain Doctor], 0) + ISNULL(A.[Captain], 0) + ISNULL(A.[Colonel Doctor], 0) + ISNULL(A.[Colonel], 0) + ISNULL(A.[Command], 0) + ISNULL(A.[Contracted Officer], 0) + ISNULL(A.[Contracted Policeman], 0) + ISNULL(A.[First Adjutant], 0) + ISNULL(A.[First Lieutenant], 0) + ISNULL(A.[First Sergeant], 0) + ISNULL(A.[General], 0) + ISNULL(A.[Lieutenant Colonel], 0) + ISNULL(A.[Major], 0) + ISNULL(A.[Policeman], 0) + ISNULL(A.[Sergeant], 0)) AS [Total Stipend],
		(ISNULL(B.[Adjutant Count], 0) + ISNULL(B.[Captain Doctor Count], 0) + ISNULL(B.[Captain Count], 0) + ISNULL(B.[Colonel Doctor Count], 0) + ISNULL(B.[Colonel Count], 0) + ISNULL(B.[Command Count], 0) + ISNULL(B.[Contracted Officer Count], 0) + ISNULL(B.[Contracted Policeman Count], 0) + ISNULL(B.[First Adjutant Count], 0) + ISNULL(B.[First Lieutenant Count], 0) + ISNULL(B.[First Sergeant Count], 0) + ISNULL(B.[General Count], 0) + ISNULL(B.[Lieutenant Colonel Count], 0) + ISNULL(B.[Major Count], 0) + ISNULL(B.[Policeman Count], 0) + ISNULL(B.[Sergeant Count], 0)) AS [Total Count],

		CASE
			WHEN A.CommunityID = 0
			THEN 3000.00
			ELSE 500.00
		END AS [Running Costs]

	FROM
		(
		SELECT
			PVT.*
		FROM
			(
			SELECT
				SD.StipendName,
				SD.CommunityID,
				SD.StipendAmountAuthorized
			FROM SD
			) AS D
		PIVOT
			(
			MAX(D.StipendAmountAuthorized)
			FOR D.StipendName IN
				(
				[Command],[General],[Colonel],[Colonel Doctor],[Lieutenant Colonel],[Major],[Captain],[Captain Doctor],[First Lieutenant],[Contracted Officer],[First Sergeant],[Sergeant],[First Adjutant],[Adjutant],[Policeman],[Contracted Policeman]
				)
			) AS PVT
		) A
		JOIN
			(
			SELECT
				PVT.*
			FROM
				(
				SELECT
					SD.StipendName + ' Count' AS StipendName,
					SD.CommunityID,
					SD.StipendNameCount
				FROM SD
				) AS D
			PIVOT
				(
				MAX(D.StipendNameCount)
				FOR D.StipendName IN
					(
					[Command Count],[General Count],[Colonel Count],[Colonel Doctor Count],[Lieutenant Colonel Count],[Major Count],[Captain Count],[Captain Doctor Count],[First Lieutenant Count],[Contracted Officer Count],[First Sergeant Count],[Sergeant Count],[First Adjutant Count],[Adjutant Count],[Policeman Count],[Contracted Policeman Count]
					)
				) AS PVT
			) B ON B.CommunityID = A.CommunityID
		OUTER APPLY
			(
			SELECT
				C.CommunityName
			FROM dbo.Community C
			WHERE C.CommunityID = A.CommunityID
			) OAC
	ORDER BY OAC.CommunityName, A.CommunityID
	
END
GO
--End procedure reporting.GetStipendPaymentReport

--Begin survey.GetSurveyBySurveyID
EXEC Utility.DropObject 'survey.GetSurveyBySurveyID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.15
-- Description:	A stored procedure to get a survey from the database
-- =================================================================
CREATE PROCEDURE survey.GetSurveyBySurveyID

@SurveyID INT

AS
BEGIN

	SELECT 
		S.IsActive,
		S.SurveyID, 
		S.SurveyName, 
		S.SurveyDescription
	FROM survey.Survey S
	WHERE S.SurveyID = @SurveyID

	SELECT
		SQ.SurveyQuestionID,
		SQ.SurveyQuestionText,
		SQ.SurveyQuestionDescription,
		SQT.SurveyQuestionResponseTypeCode
	FROM survey.SurveySurveyQuestion SSQ
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SSQ.SurveyID = @SurveyID
	ORDER BY SSQ.DisplayOrder, SQ.SurveyQuestionText, SSQ.SurveySurveyQuestionID

	SELECT
		SQRC.DisplayOrder, 
		SQRC.SurveyQuestionID,
		SQRC.SurveyQuestionResponseChoiceID,
		SQRC.SurveyQuestionResponseChoiceText
	FROM survey.SurveyQuestionResponseChoice SQRC
		JOIN survey.SurveySurveyQuestion SSQ ON SSQ.SurveyQuestionID = SQRC.SurveyQuestionID
			AND SSQ.SurveyID = @SurveyID
		JOIN survey.SurveyQuestion SQ ON SQ.SurveyQuestionID = SSQ.SurveyQuestionID
	ORDER BY SQRC.SurveyQuestionID, SQRC.DisplayOrder, SQRC.SurveyQuestionResponseChoiceText, SQRC.SurveyQuestionResponseChoiceID

END
GO
--End procedure survey.GetSurveyBySurveyID

--Begin survey.GetSurveyQuestionBySurveyQuestionID
EXEC Utility.DropObject 'survey.GetSurveyQuestionBySurveyQuestionID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2015.07.13
-- Description:	A stored procedure to get a survey question from the database
-- ==========================================================================
CREATE PROCEDURE survey.GetSurveyQuestionBySurveyQuestionID

@SurveyQuestionID INT

AS
BEGIN

	SELECT 
		SQ.IsActive,
		SQ.SurveyQuestionID, 
		SQ.SurveyQuestionDescription, 
		SQ.SurveyQuestionText, 
		SQT.SurveyQuestionResponseTypeID, 
		SQT.SurveyQuestionResponseTypeCode, 
		SQT.SurveyQuestionResponseTypeName
	FROM survey.SurveyQuestion SQ
		JOIN dropdown.SurveyQuestionResponseType SQT ON SQT.SurveyQuestionResponseTypeID = SQ.SurveyQuestionResponseTypeID
			AND SQ.SurveyQuestionID = @SurveyQuestionID

	SELECT
		SQRC.SurveyQuestionResponseChoiceID, 
		SQRC.SurveyQuestionResponseChoiceText
	FROM survey.SurveyQuestionResponseChoice SQRC
	WHERE SQRC.SurveyQuestionID = @SurveyQuestionID
	ORDER BY SQRC.DisplayOrder, SQRC.SurveyQuestionResponseChoiceText

END
GO
--End procedure survey.GetSurveyQuestionBySurveyQuestionID

--Begin deprecated procedure cleanup
EXEC Utility.DropObject 'reporting.GeConceptNoteByConceptNoteID'
EXEC Utility.DropObject 'reporting.GeConceptNoteIndicatorByConceptNoteID'
GO
--End deprecated procedure cleanup
