USE AJACS
GO

--Begin table dbo.EntityType
IF NOT EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = 'Survey')
	BEGIN
	
	INSERT INTO dbo.EntityType
		(EntityTypeCode, EntityTypeName)
	VALUES
		('Survey', 'Survey')

	END
--ENDIF
GO
--End table dbo.EntityType

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='Surveys', @NewMenuItemText='Surveys', @BeforeMenuItemCode='Admin', @PermissionableLineageList='SurveyManagement.ListQuestions,SurveyManagement.ListSurveys', @Icon='fa fa-fw fa-check-square-o'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyManagement', @NewMenuItemText='Manage Surveys', @ParentMenuItemCode='Surveys', @PermissionableLineageList='SurveyManagement.ListQuestions,SurveyManagement.ListSurveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyList', @NewMenuItemLink='/surveymanagement/listsurveys', @NewMenuItemText='Surveys', @ParentMenuItemCode='SurveyManagement', @PermissionableLineageList='SurveyManagement.ListSurveys'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='QuestionList', @NewMenuItemLink='/surveymanagement/listquestions', @NewMenuItemText='Questions', @ParentMenuItemCode='SurveyManagement', @PermissionableLineageList='SurveyManagement.ListQuestions'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='SurveyResults', @NewMenuItemText='Survey Results', @ParentMenuItemCode='Surveys', @AfterMenuItemCode='SurveyManagement'
GO
--End table dbo.MenuItem

--Begin table dropdown.DocumentType
UPDATE dropdown.DocumentType
SET DocumentGroupID = (SELECT DG.DocumentGroupID FROM dropdown.DocumentGroup DG WHERE DG.DocumentGroupCode = '500-599')
WHERE DocumentTypeID > 0
	AND DocumentGroupID = 0
GO

DECLARE @tTable TABLE (DisplayOrder INT NOT NULL PRIMARY KEY IDENTITY(1,1), DocumentTypeID INT NOT NULL)
INSERT INTO @tTable (DocumentTypeID) SELECT DT.DocumentTypeID FROM dropdown.DocumentType DT WHERE DT.IsActive = 1 AND DT.DocumentTypeID > 0 AND LEFT(DT.DocumentTypeName, 1) <> '5' ORDER BY DT.DisplayOrder, DT.DocumentTypeName

UPDATE DT
SET DT.DocumentTypeName = CAST(T.DisplayOrder + 510 AS VARCHAR(3)) + ' ' + DT.DocumentTypeName
FROM dropdown.DocumentType DT 
	JOIN @tTable T ON T.DocumentTypeID = DT.DocumentTypeID
		AND ISNUMERIC(LEFT(ISNULL(DT.DocumentTypeName, 0), 1)) = 0
GO

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '004 Branding and Marking' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '000-099 Communications' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '004 Branding and Marking')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '100 Client Requests and Approvals' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '100 Client Requests and Approvals')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '101 Internal Admin Correspondence' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '101 Internal Admin Correspondence')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '102 Office and Residence Leases' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '102 Office and Residence Leases')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '103 Various Project Admin' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '103 Various Project Admin')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '104 Hotels Reservations' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '104 Hotels Reservations')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '105 Project Insurance' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '105 Project Insurance')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '106 Security' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '106 Security')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '107 Contact List' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '107 Contact List')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '108 Translations' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '108 Translations')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '109 IT Technical Info' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '100-199 Project Admin' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '109 IT Technical Info')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '301 Project Inventory List' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '301 Project Inventory List')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '302 Procurement' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '302 Procurement')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '303 Shipping Forms and Customs Docs' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '303 Shipping Forms and Customs Docs')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '304 Waivers' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '304 Waivers')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '306 Commodities Tracking' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '300-399 Inventory and Procurement' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '306 Commodities Tracking')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '500 RFP for Project' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '500 RFP for Project')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '501 Technical Proposal and Budget' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '501 Technical Proposal and Budget')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '502 Agreements and Mods' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '502 Agreements and Mods')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '503 Work Plans and Budgets' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '503 Work Plans and Budgets')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '504 Meeting Notes' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '504 Meeting Notes')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '505 Trip Reports' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '505 Trip Reports')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '506 Quarterly Reports' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '506 Quarterly Reports')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '507 Annual Reports' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '507 Annual Reports')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '508 M&E Plan' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '508 M&E Plan')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '509 M&E Reporting' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '509 M&E Reporting')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '510 Additional Reports and Deliverables' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '500-599 General Deliverables' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '510 Additional Reports and Deliverables')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '600 Project Org Chart' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '600 Project Org Chart')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '601 Community Engagement' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '601 Community Engagement')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '602 Justice' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '602 Justice')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '603 M&E' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '603 M&E')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '604 Policing' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '604 Policing')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '605 Research' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '600-699 Project Technical' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '605 Research')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '700 Activities Manual' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '700-799 Activities' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '700 Activities Manual')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '701 Activities' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '700-799 Activities' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '701 Activities')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '702 Activity Management ' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '700-799 Activities' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '702 Activity Management ')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '801 SI Activities' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '801 SI Activities')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '802 SI Communications' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '802 SI Communications')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '803 SI Finance' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '803 SI Finance')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '804 SI General Deliverables' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '804 SI General Deliverables')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '805 SI Human Resources' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '805 SI Human Resources')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '806 SI Inventory and Procurement' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '806 SI Inventory and Procurement')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '807 SI Project Admin' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '807 SI Project Admin')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '808 SI Project Technical' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '800-899 AJACS SI' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '808 SI Project Technical')

INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '900 Start-Up' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '900 Start-Up')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '901 HR ' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '901 HR ')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '902 Procurement ' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '902 Procurement ')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '903 Finance' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '903 Finance')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '904 Contracts' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '904 Contracts')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '905 Activity Management' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '905 Activity Management')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '906 IT' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '906 IT')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '907 Security' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '907 Security')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '908 Communications' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '908 Communications')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '909 Project Admin' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '909 Project Admin')
INSERT INTO dropdown.DocumentType (DocumentGroupID, DocumentTypeName) SELECT DG.DocumentGroupID, '910 Closeout' FROM dropdown.DocumentGroup DG WHERE DocumentGroupName = '900-999 Templates and Instructions' AND NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeName = '910 Closeout')
GO

UPDATE DT
SET DT.DocumentTypeCode = LEFT(DT.DocumentTypeName, 3)
FROM dropdown.DocumentType DT
WHERE DT.DocumentTypeCode IS NULL
GO

UPDATE DT
SET DT.DocumentTypePermissionCode = LEFT(DT.DocumentTypeName, 3)
FROM dropdown.DocumentType DT
WHERE ISNUMERIC(LEFT(ISNULL(DT.DocumentTypeName, ''), 1)) = 1
GO

UPDATE DT
SET DT.DisplayOrder = 0
FROM dropdown.DocumentType DT
GO
--End table dropdown.DocumentType

--Begin table permissionable.DisplayGroup
IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DocumentType')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 6
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('DocumentType','Document Library', 7)

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Survey')
	BEGIN
	
	UPDATE permissionable.DisplayGroup
	SET DisplayOrder = DisplayOrder + 1
	WHERE DisplayOrder > 7
	
	INSERT INTO permissionable.DisplayGroup
		(DisplayGroupCode,DisplayGroupName,DisplayOrder)
	VALUES
		('Survey','Surveys', 8)

	END
--ENDIF
GO
--End table permissionable.DisplayGroup

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'Document')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'Document','Document List')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DocumentType')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'DocumentType','Document Library')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'SurveyManagement')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'SurveyManagement','Survey Management')
		
	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ListQuestions')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ListQuestions',
		'Questions List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'AddUpdateQuestion')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdateQuestion',
		'Add / Edit Questions'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'AddUpdateSurvey')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'AddUpdateSurvey',
		'Add / Edit Surveys'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ViewQuestion')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ViewQuestion',
		'View Questions'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ViewSurvey')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ViewSurvey',
		'View Surveys'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'ListSurveys')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	SELECT 
		P1.PermissionableID,
		'ListSurveys',
		'Surveys List'
	 FROM permissionable.Permissionable P1 
	 WHERE P1.PermissionableLineage = 'SurveyManagement'

	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	(SELECT P1.PermissionableID FROM permissionable.Permissionable P1 WHERE P1.PermissionableLineage = 'DocumentType'),
	DT.DocumentTypePermissionCode,
	DT.DocumentTypeName
FROM dropdown.DocumentType DT
WHERE DT.IsActive = 1
	AND DT.DocumentTypeID > 0
	AND NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P2 
	WHERE P2.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode
	)
ORDER BY DT.DocumentTypeName
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	(SELECT P1.PermissionableID FROM permissionable.Permissionable P1 WHERE P1.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode),
	'AddUpdate',
	'Add / Edit'
FROM dropdown.DocumentType DT
WHERE DT.IsActive = 1
	AND DT.DocumentTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.AddUpdate'
		)
ORDER BY DT.DocumentTypeName
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName)
SELECT
	(SELECT P1.PermissionableID FROM permissionable.Permissionable P1 WHERE P1.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode),
	'View',
	'View'
FROM dropdown.DocumentType DT
WHERE DT.IsActive = 1
	AND DT.DocumentTypeID > 0
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'DocumentType.' + DT.DocumentTypePermissionCode + '.View'
		)
ORDER BY DT.DocumentTypeName
GO

DELETE P
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage IN ('Document.AddUpdate','Document.View','Document.List')
GO

DELETE MIPL
FROM dbo.MenuItemPermissionableLineage MIPL
	JOIN dbo.MenuItem MI ON MI.MenuItemID = MIPL.MenuItemID
		AND 
			(
			MI.MenuItemCode = 'Document'
				OR NOT EXISTS
					(
					SELECT 1
					FROM permissionable.Permissionable P
					WHERE P.PermissionableLineage = MIPL.PermissionableLineage
					)
			)
GO

INSERT INTO dbo.MenuItemPermissionableLineage
	(MenuItemID, PermissionableLineage)
SELECT 
	(SELECT MI.MenuItemID FROM dbo.MenuItem MI WHERE MI.MenuItemCode = 'Document'),
	P.PermissionableLineage
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage LIKE 'DocumentType.___.%'
GO

DELETE DGP
FROM permissionable.DisplayGroupPermissionable DGP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = DGP.PermissionableID
	)
GO

DELETE PP
FROM permissionable.PersonPermissionable PP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P
	WHERE P.PermissionableLineage = PP.PermissionableLineage
	)
GO

IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'PermissionableTemplate')
	BEGIN
	
	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(0,'PermissionableTemplate','Permissionable Template')
		
	END
--ENDIF
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'AddUpdate',
	'Add / Edit',
	1
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PermissionableTemplate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PermissionableTemplate.AddUpdate'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'View',
	'View',
	2
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PermissionableTemplate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PermissionableTemplate.View'
		)
GO

INSERT INTO permissionable.Permissionable
	(ParentPermissionableID, PermissionableCode, PermissionableName, DisplayOrder)
SELECT
	P1.PermissionableID,
	'List',
	'List',
	3
FROM permissionable.Permissionable P1
WHERE P1.PermissionableLineage = 'PermissionableTemplate'
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.Permissionable P2 
		WHERE P2.PermissionableLineage = 'PermissionableTemplate.List'
		)
GO
--End table permissionable.Permissionable

--Begin table permissionable.DisplayGroupPermissionable
INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DocumentType'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('Document')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'DocumentType'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('DocumentType')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO

INSERT INTO permissionable.DisplayGroupPermissionable
	(DisplayGroupID,PermissionableID)
SELECT
	(SELECT DG.DisplayGroupID FROM permissionable.DisplayGroup DG WHERE DG.DisplayGroupCode = 'Survey'),
	P.PermissionableID
FROM permissionable.Permissionable P
WHERE P.PermissionableCode IN ('SurveyManagement')
	AND NOT EXISTS 
		(
		SELECT 1
		FROM permissionable.DisplayGroupPermissionable DGP
		WHERE DGP.PermissionableID = P.PermissionableID
		)
GO
--End table permissionable.DisplayGroupPermissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'gyingling',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'gyingling',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
