USE AJACS
GO

--Begin function dbo.FormatConceptNoteReferenceCode
EXEC utility.DropObject 'dbo.FormatConceptNoteReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.26
-- Description:	A function to return a formatted concept note reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- =========================================================================

CREATE FUNCTION dbo.FormatConceptNoteReferenceCode
(
@ConceptNoteID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteReferenceCode

--Begin function dbo.FormatConceptNoteTitle
EXEC utility.DropObject 'dbo.FormatConceptNoteTitle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.25
-- Description:	A function to return a formatted concept note title
--
-- Author:			Todd Pires
-- Create date:	2015.06.30
-- Description:	Added ISNULL support
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ================================================================

CREATE FUNCTION dbo.FormatConceptNoteTitle
(
@ConceptNoteID INT
)

RETURNS VARCHAR(300)

AS
BEGIN

	DECLARE @cReturn VARCHAR(300)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SELECT @cReturn = @cSystemName + '-AS-' + RIGHT('0000' + CAST(@ConceptNoteID AS VARCHAR(10)), 4) + ' : ' + CN.Title
	FROM dbo.ConceptNote CN
	WHERE CN.ConceptNoteID = @ConceptNoteID

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatConceptNoteTitle

--Begin function dbo.FormatProgramReportReferenceCode
EXEC utility.DropObject 'dbo.FormatProgramReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted program report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ===========================================================================

CREATE FUNCTION dbo.FormatProgramReportReferenceCode
(
@ProgramReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-PR-' + RIGHT('0000' + CAST(@ProgramReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatProgramReportReferenceCode

--Begin function dbo.FormatSpotReportReferenceCode
EXEC utility.DropObject 'dbo.FormatSpotReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.18
-- Description:	A function to return a formatted Spot report reference code
--
-- Author:			Todd Pires
-- Create date:	2015.07.13
-- Description:	Implemented the dbo.ServerSetup SELECT
-- ===========================================================================

CREATE FUNCTION dbo.FormatSpotReportReferenceCode
(
@SpotReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-SR-' + RIGHT('0000' + CAST(@SpotReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatSpotReportReferenceCode

--Begin function dbo.FormatWeeklyReportReferenceCode
EXEC utility.DropObject 'dbo.FormatWeeklyReportReferenceCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return a formatted Weekly report reference code
-- ==========================================================================

CREATE FUNCTION dbo.FormatWeeklyReportReferenceCode
(
@WeeklyReportID INT
)

RETURNS VARCHAR(100)

AS
BEGIN

	DECLARE @cReturn VARCHAR(100)
	DECLARE @cSystemName VARCHAR(50)
	
	SELECT @cSystemName = SS.ServerSetupValue
	FROM AJACSUtility.dbo.ServerSetup SS
	WHERE SS.ServerSetupKey = 'SystemName'
			
	SET @cReturn = @cSystemName + '-WR-' + RIGHT('0000' + CAST(@WeeklyReportID AS VARCHAR(10)), 4)

	RETURN ISNULL(@cReturn, '')

END
GO
--End function dbo.FormatWeeklyReportReferenceCode

--Begin function eventlog.GetPersonPermissionablesXMLByPersonID
EXEC utility.DropObject 'eventlog.GetPersonPermissionablesXMLByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.07.08
-- Description:	A function to return PersonPermissionable data for a specific Person record
-- ========================================================================================

CREATE FUNCTION eventlog.GetPersonPermissionablesXMLByPersonID
(
@PersonID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cPersonPermissionables VARCHAR(MAX) = ''
	
	SELECT @cPersonPermissionables = COALESCE(@cPersonPermissionables, '') + D.PersonPermissionable
	FROM
		(
		SELECT
			(SELECT T.* FOR XML RAW('PersonPermissionable'), ELEMENTS) AS PersonPermissionable
		FROM permissionable.PersonPermissionable T 
		WHERE T.PersonID = @PersonID
		) D

	RETURN '<PersonPermissionables>' + ISNULL(@cPersonPermissionables, '') + '</PersonPermissionables>'

END
GO
--End function eventlog.GetPersonPermissionablesXMLByPersonID