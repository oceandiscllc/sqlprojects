USE AJACS
GO

--Begin schema survey
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'survey')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA survey'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema survey
