USE AJACS
GO

--Begin table dropdown.DocumentGroup
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentGroup'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DocumentGroup
	(
	DocumentGroupID INT IDENTITY(0,1) NOT NULL,
	DocumentGroupCode VARCHAR(50),
	DocumentGroupName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentGroupID'
EXEC utility.SetIndexNonClustered 'IX_DocumentGroup', @TableName, 'DisplayOrder,DocumentGroupName', 'DocumentGroupID'
GO

SET IDENTITY_INSERT dropdown.DocumentGroup ON
GO

INSERT INTO dropdown.DocumentGroup (DocumentGroupID) VALUES (0)

SET IDENTITY_INSERT dropdown.DocumentGroup OFF
GO

INSERT INTO dropdown.DocumentGroup 
	(DocumentGroupCode,DocumentGroupName,DisplayOrder)
VALUES
	('000-099', '000-099 Communications', 1),
	('100-199', '100-199 Project Admin', 2),
	('300-399', '300-399 Inventory and Procurement', 3),
	('500-599', '500-599 General Deliverables', 4),
	('600-699', '600-699 Project Technical', 5),
	('700-799', '700-799 Activities', 6),
	('800-899', '800-899 AJACS SI', 7),
	('900-999', '900-999 Templates and Instructions', 8)
GO
--End table dropdown.DocumentGroup

--Begin table dropdown.DocumentType
DECLARE @TableName VARCHAR(250) = 'dropdown.DocumentType'

EXEC utility.DropConstraintsAndIndexes @TableName

EXEC utility.AddColumn @TableName, 'DocumentGroupID', 'INT'
EXEC utility.AddColumn @TableName, 'DocumentTypePermissionCode', 'VARCHAR(50)'

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DocumentGroupID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentTypeID'
EXEC utility.SetIndexNonClustered 'IX_DocumentType', @TableName, 'DocumentGroupID,DisplayOrder,DocumentTypeName', 'DocumentTypeID'
GO
--End table dropdown.DocumentType

--Begin table dropdown.SurveyQuestionResponseType
DECLARE @TableName VARCHAR(250) = 'dropdown.SurveyQuestionResponseType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.SurveyQuestionResponseType
	(
	SurveyQuestionResponseTypeID INT IDENTITY(0,1) NOT NULL,
	SurveyQuestionResponseTypeCode VARCHAR(50),
	SurveyQuestionResponseTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyQuestionResponseTypeID'
EXEC utility.SetIndexNonClustered 'IX_SurveyQuestionResponseTypeName', @TableName, 'DisplayOrder,SurveyQuestionResponseTypeName', 'SurveyQuestionResponseTypeID'
GO

SET IDENTITY_INSERT dropdown.SurveyQuestionResponseType ON
GO

INSERT INTO dropdown.SurveyQuestionResponseType (SurveyQuestionResponseTypeID) VALUES (0)
GO

SET IDENTITY_INSERT dropdown.SurveyQuestionResponseType OFF
GO

INSERT INTO dropdown.SurveyQuestionResponseType 
	(SurveyQuestionResponseTypeCode, SurveyQuestionResponseTypeName) 
VALUES 
	('CommunityPicker', 'Community Picker'),
	('DatePicker', 'Date Picker'),
	('LongText', 'Long Text'),
	('NumericRange', 'Numeric Range'),
	('ProvincePicker', 'Province Picker'),
	('Select', 'Select'),
	('SelectMultiple', 'Select (Multiple)'),
	('ShortText', 'Short Text'),
	('YesNo', 'Yes / No')
GO
--End table dropdown.SurveyQuestionResponseType

--Begin table permissionable.PermissionableTemplate
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplate'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplate
	(
	PermissionableTemplateID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'PermissionableTemplateID'
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.PermissionableTemplatePermissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.PermissionableTemplatePermissionable'

EXEC utility.DropObject @TableName

CREATE TABLE permissionable.PermissionableTemplatePermissionable
	(
	PermissionableTemplatePermissionableID INT IDENTITY(1,1) NOT NULL,
	PermissionableTemplateID INT,
	PermissionableID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'PermissionableID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PermissionableTemplateID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PermissionableTemplatePermissionableID'
EXEC utility.SetIndexClustered 'IX_PermissionableTemplatePermissionable', @TableName, 'PermissionableTemplateID,PermissionableID'
GO
--End table permissionable.PermissionableTemplatePermissionable

--Begin table survey.Survey
DECLARE @TableName VARCHAR(250) = 'survey.Survey'

EXEC utility.DropObject @TableName

CREATE TABLE survey.Survey
	(
	SurveyID INT IDENTITY(1,1) NOT NULL,
	SurveyName VARCHAR(250),
	SurveyDescription VARCHAR(MAX),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyID'
GO
--End table survey.SurveyQuestion

--Begin table survey.SurveyQuestion
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestion
	(
	SurveyQuestionID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionText NVARCHAR(500),
	SurveyQuestionDescription NVARCHAR(MAX),
	SurveyQuestionResponseTypeID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionResponseTypeID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'SurveyQuestionID'
GO
--End table survey.SurveyQuestion

--Begin table survey.SurveyQuestionResponseChoice
DECLARE @TableName VARCHAR(250) = 'survey.SurveyQuestionResponseChoice'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveyQuestionResponseChoice
	(
	SurveyQuestionResponseChoiceID INT IDENTITY(1,1) NOT NULL,
	SurveyQuestionID INT,
	SurveyQuestionResponseChoiceText NVARCHAR(500),
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveyQuestionResponseChoiceID'
EXEC utility.SetIndexClustered 'IX_SurveyQuestionResponseChoice', @TableName, 'SurveyQuestionID,DisplayOrder,SurveyQuestionResponseChoiceID'
GO
--End table survey.SurveyQuestionResponse

--Begin table survey.SurveySurveyQuestion
DECLARE @TableName VARCHAR(250) = 'survey.SurveySurveyQuestion'

EXEC utility.DropObject @TableName

CREATE TABLE survey.SurveySurveyQuestion
	(
	SurveySurveyQuestionID INT IDENTITY(1,1) NOT NULL,
	SurveyID INT,
	SurveyQuestionID INT,
	DisplayOrder INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'SurveyQuestionID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveySurveyQuestionID'
EXEC utility.SetIndexClustered 'IX_SurveySurveyQuestion', @TableName, 'SurveyID,DisplayOrder,SurveyQuestionID'
GO
--End table survey.SurveySurveyQuestion
