USE AJACS
GO

--Begin procedure dbo.AddContactStipendPaymentContacts
EXEC Utility.DropObject 'dbo.AddContactStipendPaymentContacts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	A stored procedure to add payees to the ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016..09.18
-- Description:	Refactored for asset support
-- ================================================================================
CREATE PROCEDURE dbo.AddContactStipendPaymentContacts

@PaymentMonth INT,
@PaymentYear INT,
@PersonID INT,
@ContactIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY, StipendTypeCode VARCHAR(50))

	IF @ContactIDList = ''
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Contact'
				AND SR.EntityID = C.ContactID
				AND SR.PersonID = @PersonID
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)

		END
	ELSE
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN dbo.ListToTable(@ContactIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ContactID
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)

		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM @tOutput T WHERE T.StipendTypeCode = 'JusticeStipend')
		BEGIN
		
		INSERT INTO asset.AssetUnitExpense
			(AssetUnitID, ExpenseAmountAuthorized, ProvinceID, PaymentMonth, PaymentYear)
		SELECT DISTINCT
			AU.AssetUnitID,
			AUC.AssetUnitCostName,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			@PaymentMonth,
			@PaymentYear
		FROM asset.AssetUnit AU
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			JOIN dbo.Contact C ON C.AssetUnitID = AU.AssetUnitID
			JOIN @tOutput O ON O.ContactID = C.ContactID
				AND NOT EXISTS
					(
					SELECT 1
					FROM asset.AssetUnitExpense AUE
					WHERE AUE.AssetUnitID = AU.AssetUnitID
						AND AUE.PaymentMonth = @PaymentMonth
						AND AUE.PaymentYear = @PaymentYear
					)

		END
	--ENDIF
	
END
GO
--End procedure dbo.AddContactStipendPaymentContacts

--Begin procedure dbo.ApproveContactStipendPayment
EXEC Utility.DropObject 'dbo.ApproveContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ApproveContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT,
@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET
		CSP.AssetUnitID = C.AssetUnitID,
		CSP.ConceptNoteID = @ConceptNoteID,
		CSP.StipendAuthorizedDate = getDate(),
		CSP.StipendAmountPaid = 
			CASE
				WHEN dbo.GetContactStipendEligibility(CSP.ContactID, @ConceptNoteID) = 1
				THEN CSP.StipendAmountAuthorized
				ELSE 0
			END

	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			AND CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = @StipendTypeCode
			AND CSP.StipendAuthorizedDate IS NULL

	IF @StipendTypeCode = 'JusticeStipend'
		BEGIN

		UPDATE AUE
		SET 
			AUE.ConceptNoteID = @ConceptNoteID,
			AUE.ExpenseAuthorizedDate = getDate(),
			AUE.ExpenseAmountPaid = AUE.ExpenseAmountAuthorized
		FROM asset.AssetUnitExpense AUE
		WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
			AND AUE.ProvinceID = @ProvinceID
			AND AUE.ExpenseAuthorizedDate IS NULL

		END
	--ENDIF

END
GO
--End procedure dbo.ApproveContactStipendPayment

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- Notes:				Changes here must ALSO be made to force.GetForceByEventLogID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
--
-- Author:			Eric Jones
-- Create date:	2016.12.07
-- Description:	implemented eventlog data to populate revision history table
--
-- Author:			Eric Jones
-- Create date:	2016.12.15
-- Description:	added eventlog id to query result set.
-- =========================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Force
	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	--ForceCommunity
	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	--ForceEquipmentResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceEventLog
	SELECT 
		EL.eventLogID,
		EL.eventcode, 
		EL.eventdata, 
		dbo.FormatDateTime(EL.createdatetime) AS createDateFormatted, 
		dbo.FormatPersonName(P.firstname, P.lastname,'','LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
		JOIN person P on p.personid = el.personid
	WHERE EL.EntityTypeCode = 'force' AND EL.entityid = @ForceID AND EL.eventcode != 'read' 

	--ForceFinancialResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceRisk
	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	--ForceUnit
	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullName,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure reporting.GetDistributedInventoryCommunityData
EXEC Utility.DropObject 'reporting.GetDistributedInventoryCommunityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.04.09
-- Description:	A stored procedure to get data from the procurement.DistributedInventory table
-- ===========================================================================================
CREATE PROCEDURE reporting.GetDistributedInventoryCommunityData

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		C.Summary
	FROM dbo.Community C
		JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
	ORDER BY 2, 1

END
GO
--End procedure reporting.GetDistributedInventoryCommunityData

--Begin procedure reporting.GetDistributedInventoryProvinceData
EXEC Utility.DropObject 'reporting.GetDistributedInventoryProvinceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.04.09
-- Description:	A stored procedure to get data from the procurement.DistributedInventory table
-- ===========================================================================================
CREATE PROCEDURE reporting.GetDistributedInventoryProvinceData

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceID,
		P.ProvinceName,
		P.Summary
	FROM dbo.Province P
		JOIN dbo.Community C ON C.ProvinceID = P.ProvinceID
		JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
	ORDER BY 2, 1

END
GO
--End procedure reporting.GetDistributedInventoryProvinceData

--Begin procedure reporting.GetDistributedInventoryMapData
EXEC Utility.DropObject 'reporting.GetDistributedInventoryMapData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date: 2017.04.09
-- Description:	A stored procedure to get map data for an inventory distribution
-- =============================================================================
CREATE PROCEDURE reporting.GetDistributedInventoryMapData

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCommunityList VARCHAR(MAX)
	DECLARE @nProvinceID INT
	DECLARE @tTable TABLE (ProvinceID INT, CommunityNameList VARCHAR(MAX))

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			P.ProvinceID
		FROM dbo.Province P
			JOIN dbo.Community C ON C.ProvinceID = P.ProvinceID
			JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
		ORDER BY 1
		
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		SET @cCommunityList = ''
	
		SELECT @cCommunityList = COALESCE(@cCommunityList + ', ', '') + dbo.GetCommunityNameByCommunityID(C.CommunityID)
		FROM dbo.Community C
			JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
				AND C.ProvinceID = @nProvinceID

		IF @cCommunityList IS NOT NULL AND LEN(RTRIM(@cCommunityList)) > 0
			BEGIN

			INSERT INTO @tTable
				(ProvinceID, CommunityNameList)
			VALUES
				(@nProvinceID, STUFF(@cCommunityList, 1, 2, ''))

			END
		--ENDIF

		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	SELECT
		P.ProvinceName,
		T.CommunityNameList,
		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan(@EquipmentDistributionID, P.ProvinceID) AS MapLink
	FROM dbo.Province P
		JOIN @tTable T ON T.ProvinceID = P.ProvinceID
	ORDER BY 1

END
GO
--End procedure reporting.GetDistributedInventoryMapData
