-- File Name:	Build - 2.7 - AJACS.sql
-- Build Key:	Build - 2.7 - 2017.04.21 16.30.27

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.GetConceptNoteProvinceListByConceptNoteID
--		dbo.GetContactStipendEligibility
--		dbo.GetContactStipendEligibilityNotes
--		dbo.GetLastVettingOutcomeID
--		force.GetCommunityIDByForceID
--		reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID
--
-- Procedures:
--		dbo.AddContactStipendPaymentContacts
--		dbo.ApproveContactStipendPayment
--		force.GetForceByForceID
--		reporting.GetDistributedInventoryCommunityData
--		reporting.GetDistributedInventoryMapData
--		reporting.GetDistributedInventoryProvinceData
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO


--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.GetConceptNoteProvinceListByConceptNoteID
EXEC utility.DropObject 'dbo.GetConceptNoteProvinceListByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.23
-- Description:	A function to get a list of community names based on a conceptnoteid
-- =================================================================================

CREATE FUNCTION dbo.GetConceptNoteProvinceListByConceptNoteID
(
@ConceptNoteID INT
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceNameList NVARCHAR(MAX)

	SELECT @cProvinceNameList = COALESCE(@cProvinceNameList + ', ', '') + P.ProvinceName
	FROM dbo.Province P
		JOIN
			(
			SELECT 
				C.ProvinceID
			FROM dbo.ConceptNoteCommunity CNC
				JOIN dbo.Community C ON C.CommunityID = CNC.CommunityID
					AND CNC.ConceptNoteID = @ConceptNoteID

			UNION

			SELECT 
				CNP.ProvinceID
			FROM dbo.ConceptNoteProvince CNP
			WHERE CNP.ConceptNoteID = @ConceptNoteID
			)	D ON D.ProvinceID = P.ProvinceID
	ORDER BY P.ProvinceName

	RETURN @cProvinceNameList

END
GO
--End function dbo.GetConceptNoteProvinceListByConceptNoteID

--Begin function dbo.GetContactStipendEligibility
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibility
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibility

--Begin function dbo.GetContactStipendEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotes
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotes

--Begin function force.GetCommunityIDByForceID
EXEC utility.DropObject 'force.GetCommunityIDByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.09
-- Description:	A function to get a list of CommunityIDs based on an ForceID
-- =========================================================================

CREATE FUNCTION force.GetCommunityIDByForceID
(
@ForceID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCommunityID INT

	SELECT @nCommunityID = 
		CASE
			WHEN F.TerritoryTypeCode = 'Province' 
			THEN 0
			ELSE F.TerritoryID 
		END

	FROM force.Force F 
	WHERE F.ForceID = @ForceID
	
	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function force.GetCommunityIDByForceID

--Begin function dbo.GetLastVettingOutcomeID
EXEC utility.DropObject 'dbo.GetLastVettingOutcomeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.18
-- Description:	A function to get the last vetting outcomeid for a contactid and vettingtypeid
-- ===========================================================================================

CREATE FUNCTION dbo.GetLastVettingOutcomeID
(
@ContactID INT,
@ContactVettingTypeID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nVettingOutcomeID INT

	SELECT TOP 1 @nVettingOutcomeID = CV.VettingOutcomeID
	FROM dbo.ContactVetting CV
	WHERE CV.ContactID = @ContactID
		AND CV.ContactVettingTypeID = @ContactVettingTypeID
	ORDER BY CV.ContactVettingID DESC
	
	RETURN ISNULL(@nVettingOutcomeID, 0)

END
GO
--End function dbo.GetLastVettingOutcomeID

--Begin function reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID
EXEC utility.DropObject 'reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.09
-- Description:	A function to get a list of CommunityIDs based on an EquipmentDistributionID
-- =========================================================================================

CREATE FUNCTION reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID
(
@EquipmentDistributionID INT
)

RETURNS @tTable TABLE (CommunityID INT NOT NULL PRIMARY KEY)

AS
BEGIN

	INSERT INTO @tTable 
		(CommunityID)
	SELECT DISTINCT
		CASE
			WHEN DI.EndUserEntityTypeCode = 'Asset'
			THEN (SELECT A.CommunityID FROM asset.Asset A WHERE A.AssetID = DI.EndUserEntityID)
			WHEN DI.EndUserEntityTypeCode = 'AssetUnit'
			THEN (SELECT asset.GetCommunityIDByAssetUnitID(DI.EndUserEntityID))
			WHEN DI.EndUserEntityTypeCode = 'Contact'
			THEN (SELECT C.CommunityID FROM dbo.Contact C WHERE C.ContactID = DI.EndUserEntityID)
			WHEN DI.EndUserEntityTypeCode = 'Force'
			THEN (SELECT force.GetCommunityIDByForceID(DI.EndUserEntityID))
		END AS CommunityID
	FROM procurement.DistributedInventory DI
	WHERE DI.EquipmentDistributionID = @EquipmentDistributionID
	
	RETURN

END
GO
--End function reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.AddContactStipendPaymentContacts
EXEC Utility.DropObject 'dbo.AddContactStipendPaymentContacts'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.15
-- Description:	A stored procedure to add payees to the ContactStipendPayment table
--
-- Author:			Todd Pires
-- Create date:	2016..09.18
-- Description:	Refactored for asset support
-- ================================================================================
CREATE PROCEDURE dbo.AddContactStipendPaymentContacts

@PaymentMonth INT,
@PaymentYear INT,
@PersonID INT,
@ContactIDList VARCHAR(MAX) = ''

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tOutput TABLE (ContactID INT NOT NULL PRIMARY KEY, StipendTypeCode VARCHAR(50))

	IF @ContactIDList = ''
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN reporting.SearchResult SR ON SR.EntityTypeCode = 'Contact'
				AND SR.EntityID = C.ContactID
				AND SR.PersonID = @PersonID
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)

		END
	ELSE
		BEGIN

		INSERT INTO dbo.ContactStipendPayment
			(CommunityID,ContactID,PaymentMonth,PaymentYear,ProvinceID,StipendAmountAuthorized,StipendName,StipendTypeCode)
		OUTPUT INSERTED.ContactID, INSERTED.StipendTypeCode INTO @tOutput
		SELECT
			A.CommunityID,
			C.ContactID,
			@PaymentMonth,
			@PaymentYear,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			S.StipendAmount,
			S.StipendName,
			S.StipendTypeCode
		FROM dbo.Contact C
			JOIN dbo.ListToTable(@ContactIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = C.ContactID
			JOIN asset.AssetUnit AU ON AU.AssetUnitID = C.AssetUnitID
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.StipendID > 0
				AND NOT EXISTS
					(
					SELECT 1
					FROM dbo.ContactStipendPayment CSP
					WHERE CSP.ContactID = C.ContactID
						AND CSP.PaymentMonth = @PaymentMonth
						AND CSP.PaymentYear = @PaymentYear
						AND CSP.StipendTypeCode = S.StipendTypeCode
					)
				AND 
					(
					EXISTS
						(
						SELECT 1
						FROM dbo.ContactStipendPayment CSP
						WHERE CSP.PaymentMonth = @PaymentMonth
							AND CSP.PaymentYear = @PaymentYear
							AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
							AND CSP.StipendTypeCode = S.StipendTypeCode
							AND CSP.StipendPaidDate IS NULL
						)
						OR NOT EXISTS
							(
							SELECT 1
							FROM dbo.ContactStipendPayment CSP
							WHERE CSP.PaymentMonth = @PaymentMonth
								AND CSP.PaymentYear = @PaymentYear
								AND CSP.ProvinceID = dbo.GetProvinceIDByCommunityID(A.CommunityID)
								AND CSP.StipendTypeCode = S.StipendTypeCode
							)
					)

		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM @tOutput T WHERE T.StipendTypeCode = 'JusticeStipend')
		BEGIN
		
		INSERT INTO asset.AssetUnitExpense
			(AssetUnitID, ExpenseAmountAuthorized, ProvinceID, PaymentMonth, PaymentYear)
		SELECT DISTINCT
			AU.AssetUnitID,
			AUC.AssetUnitCostName,
			dbo.GetProvinceIDByCommunityID(A.CommunityID),
			@PaymentMonth,
			@PaymentYear
		FROM asset.AssetUnit AU
			JOIN asset.Asset A ON A.AssetID = AU.AssetID
			JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
			JOIN dbo.Contact C ON C.AssetUnitID = AU.AssetUnitID
			JOIN @tOutput O ON O.ContactID = C.ContactID
				AND NOT EXISTS
					(
					SELECT 1
					FROM asset.AssetUnitExpense AUE
					WHERE AUE.AssetUnitID = AU.AssetUnitID
						AND AUE.PaymentMonth = @PaymentMonth
						AND AUE.PaymentYear = @PaymentYear
					)

		END
	--ENDIF
	
END
GO
--End procedure dbo.AddContactStipendPaymentContacts

--Begin procedure dbo.ApproveContactStipendPayment
EXEC Utility.DropObject 'dbo.ApproveContactStipendPayment'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date: 2016.09.27
-- Description:	A stored procedure to update a dbo.ContactStipendPayment record
-- ============================================================================
CREATE PROCEDURE dbo.ApproveContactStipendPayment

@StipendTypeCode VARCHAR(50),
@PaymentGroup INT,
@ProvinceID INT,
@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CSP
	SET
		CSP.AssetUnitID = C.AssetUnitID,
		CSP.ConceptNoteID = @ConceptNoteID,
		CSP.StipendAuthorizedDate = getDate(),
		CSP.StipendAmountPaid = 
			CASE
				WHEN dbo.GetContactStipendEligibility(CSP.ContactID, @ConceptNoteID) = 1
				THEN CSP.StipendAmountAuthorized
				ELSE 0
			END

	FROM dbo.ContactStipendPayment CSP
		JOIN dbo.Contact C ON C.ContactID = CSP.ContactID
			AND CSP.PaymentYear * 100 + CSP.PaymentMonth = @PaymentGroup
			AND CSP.ProvinceID = @ProvinceID
			AND CSP.StipendTypeCode = @StipendTypeCode
			AND CSP.StipendAuthorizedDate IS NULL

	IF @StipendTypeCode = 'JusticeStipend'
		BEGIN

		UPDATE AUE
		SET 
			AUE.ConceptNoteID = @ConceptNoteID,
			AUE.ExpenseAuthorizedDate = getDate(),
			AUE.ExpenseAmountPaid = AUE.ExpenseAmountAuthorized
		FROM asset.AssetUnitExpense AUE
		WHERE AUE.PaymentYear * 100 + AUE.PaymentMonth = @PaymentGroup
			AND AUE.ProvinceID = @ProvinceID
			AND AUE.ExpenseAuthorizedDate IS NULL

		END
	--ENDIF

END
GO
--End procedure dbo.ApproveContactStipendPayment

--Begin procedure force.GetForceByForceID
EXEC Utility.DropObject 'force.GetForceByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.13
-- Description:	A stored procedure to data from the force.Force table
-- Notes:				Changes here must ALSO be made to force.GetForceByEventLogID
--
-- Author:			Todd Pires
-- Create date:	2016.08.13
-- Description:	Implemented CommanderFullName and DeputyCommanderFullName
--
-- Author:			Eric Jones
-- Create date:	2016.12.07
-- Description:	implemented eventlog data to populate revision history table
--
-- Author:			Eric Jones
-- Create date:	2016.12.15
-- Description:	added eventlog id to query result set.
-- =========================================================================
CREATE PROCEDURE force.GetForceByForceID

@ForceID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Force
	SELECT 
		F.CommanderFullName,
		F.Comments, 
		F.DeputyCommanderFullName,
		F.ForceDescription, 
		F.ForceID, 
		F.ForceName, 
		F.History, 
		F.Location.STAsText() AS Location,		
		F.Notes, 
		F.TerritoryID, 
		F.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(F.TerritoryTypeCode, F.TerritoryID) AS TerritoryName,
		F.WebLinks,
		AOT.AreaOfOperationTypeID, 
		AOT.AreaOfOperationTypeName
	FROM Force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND F.ForceID = @ForceID

	--ForceCommunity
	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM force.ForceCommunity FC
		JOIN dbo.Community C ON C.CommunityID = FC.CommunityID
		JOIN dbo.Province P on P.ProvinceID = C.ProvinceID
			AND FC.ForceID = @ForceID
	ORDER BY C.CommunityName, C.CommunityID

	--ForceEquipmentResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceEquipmentResourceProvider FERP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FERP.ResourceProviderID
			AND FERP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceEventLog
	SELECT 
		EL.eventLogID,
		EL.eventcode, 
		EL.eventdata, 
		dbo.FormatDateTime(EL.createdatetime) AS createDateFormatted, 
		dbo.FormatPersonName(P.firstname, P.lastname,'','LastFirst') AS LogNameFormatted
	FROM eventlog.EventLog EL 
		JOIN person P on p.personid = el.personid
	WHERE EL.EntityTypeCode = 'force' AND EL.entityid = @ForceID AND EL.eventcode != 'read' 

	--ForceFinancialResourceProvider
	SELECT
		RP.ResourceProviderID,
		RP.ResourceProviderName
	FROM force.ForceFinancialResourceProvider FFRP
		JOIN dropdown.ResourceProvider RP ON RP.ResourceProviderID = FFRP.ResourceProviderID
			AND FFRP.ForceID = @ForceID
	ORDER BY RP.DisplayOrder, RP.ResourceProviderName, RP.ResourceProviderID

	--ForceRisk
	SELECT
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Overall,
		R.RiskID,
		R.RiskName,
		RC.RiskCategoryName
	FROM force.ForceRisk FR
		JOIN dbo.Risk R ON R.RiskID = FR.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND FR.ForceID = @ForceID
	ORDER BY R.RiskName, R.RiskID

	--ForceUnit
	SELECT
		FU.CommanderFullName, 
		FU.DeputyCommanderFullName,
		FU.ForceUnitID,
		FU.TerritoryID,
		FU.TerritoryTypeCode,
		dbo.GetTerritoryNameByTerritoryTypeCodeAndTerritoryID(FU.TerritoryTypeCode, FU.TerritoryID) AS TerritoryName,
		FU.UnitName,
		UT.UnitTypeID,
		UT.UnitTypeName
	FROM force.ForceUnit FU
		JOIN dropdown.UnitType UT ON UT.UnitTypeID = FU.UnitTypeID
			AND FU.ForceID = @ForceID
	ORDER BY FU.UnitName, FU.ForceUnitID
	
END
GO
--End procedure force.GetForceByForceID

--Begin procedure reporting.GetDistributedInventoryCommunityData
EXEC Utility.DropObject 'reporting.GetDistributedInventoryCommunityData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.04.09
-- Description:	A stored procedure to get data from the procurement.DistributedInventory table
-- ===========================================================================================
CREATE PROCEDURE reporting.GetDistributedInventoryCommunityData

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.CommunityID,
		C.CommunityName,
		C.Summary
	FROM dbo.Community C
		JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
	ORDER BY 2, 1

END
GO
--End procedure reporting.GetDistributedInventoryCommunityData

--Begin procedure reporting.GetDistributedInventoryProvinceData
EXEC Utility.DropObject 'reporting.GetDistributedInventoryProvinceData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date: 2017.04.09
-- Description:	A stored procedure to get data from the procurement.DistributedInventory table
-- ===========================================================================================
CREATE PROCEDURE reporting.GetDistributedInventoryProvinceData

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ProvinceID,
		P.ProvinceName,
		P.Summary
	FROM dbo.Province P
		JOIN dbo.Community C ON C.ProvinceID = P.ProvinceID
		JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
	ORDER BY 2, 1

END
GO
--End procedure reporting.GetDistributedInventoryProvinceData

--Begin procedure reporting.GetDistributedInventoryMapData
EXEC Utility.DropObject 'reporting.GetDistributedInventoryMapData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date: 2017.04.09
-- Description:	A stored procedure to get map data for an inventory distribution
-- =============================================================================
CREATE PROCEDURE reporting.GetDistributedInventoryMapData

@EquipmentDistributionID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCommunityList VARCHAR(MAX)
	DECLARE @nProvinceID INT
	DECLARE @tTable TABLE (ProvinceID INT, CommunityNameList VARCHAR(MAX))

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			P.ProvinceID
		FROM dbo.Province P
			JOIN dbo.Community C ON C.ProvinceID = P.ProvinceID
			JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
		ORDER BY 1
		
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		SET @cCommunityList = ''
	
		SELECT @cCommunityList = COALESCE(@cCommunityList + ', ', '') + dbo.GetCommunityNameByCommunityID(C.CommunityID)
		FROM dbo.Community C
			JOIN reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID(@EquipmentDistributionID) DIC ON DIC.CommunityID = C.CommunityID
				AND C.ProvinceID = @nProvinceID

		IF @cCommunityList IS NOT NULL AND LEN(RTRIM(@cCommunityList)) > 0
			BEGIN

			INSERT INTO @tTable
				(ProvinceID, CommunityNameList)
			VALUES
				(@nProvinceID, STUFF(@cCommunityList, 1, 2, ''))

			END
		--ENDIF

		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	SELECT
		P.ProvinceName,
		T.CommunityNameList,
		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan(@EquipmentDistributionID, P.ProvinceID) AS MapLink
	FROM dbo.Province P
		JOIN @tTable T ON T.ProvinceID = P.ProvinceID
	ORDER BY 1

END
GO
--End procedure reporting.GetDistributedInventoryMapData

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

UPDATE P
SET 
	P.PermissionCode = 'ExportJO',
	P.Description = 'Export the vetting list for vetting type JO.'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Vetting.List.Export'
GO

INSERT INTO permissionable.Permissionable
	(DisplayOrder, IsGlobal, ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsActive, IsSuperAdministrator)
SELECT
	P.DisplayOrder, 
	P.IsGlobal, 
	P.ControllerName, 
	P.MethodName, 
	'ExportUK', 
	P.PermissionableGroupID, 
	'Export the vetting list for vetting type UK.',
	P.IsActive, 
	P.IsSuperAdministrator
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Vetting.List.ExportJO'
GO

INSERT INTO permissionable.Permissionable
	(DisplayOrder, IsGlobal, ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsActive, IsSuperAdministrator)
SELECT
	P.DisplayOrder, 
	P.IsGlobal, 
	P.ControllerName, 
	P.MethodName, 
	'ExportUS', 
	P.PermissionableGroupID, 
	'Export the vetting list for vetting type US.',
	P.IsActive, 
	P.IsSuperAdministrator
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Vetting.List.ExportJO'
GO

UPDATE PPL
SET PPL.PermissionableLineage = 'Vetting.List.ExportJO'
FROM permissionable.PersonPermissionable PPL
WHERE PPL.PermissionableLineage = 'Vetting.List.Export'
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	PPL.PersonID,
	'Vetting.List.ExportUK'
FROM permissionable.PersonPermissionable PPL
WHERE PPL.PermissionableLineage = 'Vetting.List.ExportJO'
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	PPL.PersonID,
	'Vetting.List.ExportUS'
FROM permissionable.PersonPermissionable PPL
WHERE PPL.PermissionableLineage = 'Vetting.List.ExportJO'
GO

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC permissionable.SavePermissionableGroup 'Administration', 'Administration', 0;
EXEC permissionable.SavePermissionableGroup 'Community', 'Community', 0;
EXEC permissionable.SavePermissionableGroup 'Contact', 'Contact', 0;
EXEC permissionable.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0;
EXEC permissionable.SavePermissionableGroup 'Documents', 'Documents', 0;
EXEC permissionable.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0;
EXEC permissionable.SavePermissionableGroup 'Equipment', 'Equipment', 0;
EXEC permissionable.SavePermissionableGroup 'Implementation', 'Implementation', 0;
EXEC permissionable.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0;
EXEC permissionable.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0;
EXEC permissionable.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0;
EXEC permissionable.SavePermissionableGroup 'Province', 'Province', 0;
EXEC permissionable.SavePermissionableGroup 'Research', 'Research', 0;
EXEC permissionable.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0;
EXEC permissionable.SavePermissionableGroup 'Training', 'Training', 0;
EXEC permissionable.SavePermissionableGroup 'Workflows', 'Workflows', 0;
--End table permissionable.PermissionableGroup

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataExport', @DESCRIPTION='Data Export', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataExport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DataImport', @DESCRIPTION='Data Import', @DISPLAYORDER=0, @ISGLOBAL=1, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='DataImport.Default', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='Add / edit an email template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='List EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EmailTemplateAdministration', @DESCRIPTION='View EmailTemplateAdministration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EmailTemplateAdministration.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='List EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EventLog', @DESCRIPTION='View EventLog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='EventLog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Exports', @DESCRIPTION='Business License Report Exports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='BusinessLicenseReport', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Exports.BusinessLicenseReport', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='User Can Receive Email', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='CanRecieveEmail', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.CanRecieveEmail.CanRecieveEmail', @PERMISSIONCODE='CanRecieveEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='Download dashboard charts as images', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Default', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Default.DownloadChart', @PERMISSIONCODE='DownloadChart';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Main', @DESCRIPTION='View ColdFusion Errors SiteConfiguration', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Error', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Main.Error.ViewCFErrors', @PERMISSIONCODE='ViewCFErrors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Add / edit a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='Delete a system permission', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Permissionable', @DESCRIPTION='View the list of system permissions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Permissionable.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Add / edit a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='Delete a permissionable template', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='List PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PermissionableTemplate', @DESCRIPTION='View PermissionableTemplate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='PermissionableTemplate.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Add / edit a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='Delete a person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='List Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View Person', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Person', @DESCRIPTION='View list of permissionables on view page', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewPermissionables', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='Person.ViewPermissionables', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='Add / edit a server setup key', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ServerSetup', @DESCRIPTION='List the server setup keys', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Administration', @PERMISSIONABLELINEAGE='ServerSetup.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Add / edit a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Recieve email for Update to Impact Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.AddUpdate.ImpactUpdateEmail', @PERMISSIONCODE='ImpactUpdateEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='List Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the analysis tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Analysis', @PERMISSIONCODE='Analysis';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Export Equipment Distributions Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='Implementation Community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Implementation', @PERMISSIONCODE='Implementation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Community', @DESCRIPTION='View the information tab for a community', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Community', @PERMISSIONABLELINEAGE='Community.View.Information', @PERMISSIONCODE='Information';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add contacts to a stipend payment list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddContactStipendPaymentContacts', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddContactStipendPaymentContacts', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Project Staff - ASI', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Project Staff - Creative', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Add / edit contacts of type Sub-Contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.AddUpdate.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows view of justice stipends payments', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the cash hand over report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the op funds report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend activity report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Close out the stipend justice & police payment process', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='JusticePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.JusticePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows import of payees in payment system', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.AddContactStipendPaymentContacts', @PERMISSIONCODE='AddContactStipendPaymentContacts';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allows access to the bulk transfer functionality', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.CanHaveBulkTransfer', @PERMISSIONCODE='CanHaveBulkTransfer';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type CE Team in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export payees from the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ExportPayees', @PERMISSIONCODE='ExportPayees';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Field Staff in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type IO4 in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Justice Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='A bypass to allow users not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Other in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Police Stipend in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Project Staff - ASI in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Project Staff - Creative in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Include contacts of type Sub-Contractor in the contact list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.List.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the cash handover report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.CashHandOverExport', @PERMISSIONCODE='CashHandOverExport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the op funds report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.OpFundsReport', @PERMISSIONCODE='OpFundsReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend activity report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendActivity', @PERMISSIONCODE='StipendActivity';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Export the stipend payment report from the payment List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='PolicePaymentList', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.PolicePaymentList.StipendPaymentReport', @PERMISSIONCODE='StipendPaymentReport';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='Allow access to the stipend payment report on the justice stipends list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ReconcileContactStipendPayment', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.ReconcileContactStipendPayment', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type CE Team', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Field Staff', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type IO4', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Justice Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Stakeholder', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Partners: Supplier/Vendor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Police Stipend', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Project Staff - ASI', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Project Staff - Creative', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View contacts of type Sub-Contractors', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Contact', @DESCRIPTION='View the more info button on the vetting history data table', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Contact', @PERMISSIONABLELINEAGE='Contact.View.VettingMoreInfo', @PERMISSIONCODE='VettingMoreInfo';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Add contacts to an activity from the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.AddToConceptNote', @PERMISSIONCODE='AddToConceptNote';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type CE Team in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.CETeam', @PERMISSIONCODE='CETeam';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type JO.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportJO', @PERMISSIONCODE='ExportJO';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type UK.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportUK', @PERMISSIONCODE='ExportUK';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Export the vetting list for vetting type US.', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ExportUS', @PERMISSIONCODE='ExportUS';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Field Staff in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.FieldStaff', @PERMISSIONCODE='FieldStaff';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type IO4 in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.IO4', @PERMISSIONCODE='IO4';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeOther', @PERMISSIONCODE='JusticeOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Justice Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.JusticeStipend', @PERMISSIONCODE='JusticeStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Stakeholder in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersStakeholder', @PERMISSIONCODE='PartnersStakeholder';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Partners: Supplier/Vendor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PartnersSupplierVendor', @PERMISSIONCODE='PartnersSupplierVendor';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Other in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceOther', @PERMISSIONCODE='PoliceOther';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Police Stipend in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.PoliceStipend', @PERMISSIONCODE='PoliceStipend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Project Staff - ASI in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ProjectStaffASI', @PERMISSIONCODE='ProjectStaffASI';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Project Staff - Creative in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.ProjectStaffCreative', @PERMISSIONCODE='ProjectStaffCreative';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Include contacts of type Sub-Contractor in the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.SubContractors', @PERMISSIONCODE='SubContractors';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive an email when a vetting outcome has changed for one or more contacts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeChangeNotification', @PERMISSIONCODE='VettingOutcomeChangeNotification';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeConsider', @PERMISSIONCODE='VettingOutcomeConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Do Not Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeDoNotConsider', @PERMISSIONCODE='VettingOutcomeDoNotConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Insufficient Data"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeInsufficientData', @PERMISSIONCODE='VettingOutcomeInsufficientData';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Not Vetted"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeNotVetted', @PERMISSIONCODE='VettingOutcomeNotVetted';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Pending Internal Review"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomePendingInternalReview', @PERMISSIONCODE='VettingOutcomePendingInternalReview';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Allows a user to change a change a vetting outcome after a contact has been flagged as "Do Not Consider"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeReConsider', @PERMISSIONCODE='VettingOutcomeReConsider';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Assign a vetting outcome of "Submitted for Vetting"', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingOutcomeSubmittedforVetting', @PERMISSIONCODE='VettingOutcomeSubmittedforVetting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update UK vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUK', @PERMISSIONCODE='VettingTypeUK';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Update US vetting data on the vetting list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.List.VettingTypeUS', @PERMISSIONCODE='VettingTypeUS';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Vetting', @DESCRIPTION='Receive the monthly vetting expiration counts e-mail', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Notification', @PERMISSIONABLEGROUPCODE='ContactVetting', @PERMISSIONABLELINEAGE='Vetting.Notification.ExpirationCountEmail', @PERMISSIONCODE='ExpirationCountEmail';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.004', @PERMISSIONCODE='004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.100', @PERMISSIONCODE='100';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.101', @PERMISSIONCODE='101';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.102', @PERMISSIONCODE='102';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.103', @PERMISSIONCODE='103';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.104', @PERMISSIONCODE='104';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.105', @PERMISSIONCODE='105';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.106', @PERMISSIONCODE='106';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.107', @PERMISSIONCODE='107';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.108', @PERMISSIONCODE='108';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.109', @PERMISSIONCODE='109';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.301', @PERMISSIONCODE='301';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.302', @PERMISSIONCODE='302';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.303', @PERMISSIONCODE='303';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.304', @PERMISSIONCODE='304';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.306', @PERMISSIONCODE='306';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.500', @PERMISSIONCODE='500';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.501', @PERMISSIONCODE='501';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.502', @PERMISSIONCODE='502';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.503', @PERMISSIONCODE='503';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.504', @PERMISSIONCODE='504';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.505', @PERMISSIONCODE='505';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.506', @PERMISSIONCODE='506';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.507', @PERMISSIONCODE='507';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.508', @PERMISSIONCODE='508';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.509', @PERMISSIONCODE='509';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.510', @PERMISSIONCODE='510';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.511', @PERMISSIONCODE='511';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.512', @PERMISSIONCODE='512';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.513', @PERMISSIONCODE='513';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.514', @PERMISSIONCODE='514';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.515', @PERMISSIONCODE='515';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.516', @PERMISSIONCODE='516';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.517', @PERMISSIONCODE='517';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.518', @PERMISSIONCODE='518';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.519', @PERMISSIONCODE='519';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.520', @PERMISSIONCODE='520';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.521', @PERMISSIONCODE='521';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 522 Archived Full Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.522', @PERMISSIONCODE='522';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.600', @PERMISSIONCODE='600';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.601', @PERMISSIONCODE='601';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.602', @PERMISSIONCODE='602';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.603', @PERMISSIONCODE='603';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.604', @PERMISSIONCODE='604';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.605', @PERMISSIONCODE='605';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.700', @PERMISSIONCODE='700';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.701', @PERMISSIONCODE='701';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.702', @PERMISSIONCODE='702';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.801', @PERMISSIONCODE='801';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.802', @PERMISSIONCODE='802';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.803', @PERMISSIONCODE='803';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.804', @PERMISSIONCODE='804';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.805', @PERMISSIONCODE='805';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.806', @PERMISSIONCODE='806';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.807', @PERMISSIONCODE='807';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.808', @PERMISSIONCODE='808';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.900', @PERMISSIONCODE='900';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.901', @PERMISSIONCODE='901';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.902', @PERMISSIONCODE='902';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.903', @PERMISSIONCODE='903';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.904', @PERMISSIONCODE='904';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.905', @PERMISSIONCODE='905';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.906', @PERMISSIONCODE='906';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.907', @PERMISSIONCODE='907';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.908', @PERMISSIONCODE='908';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.909', @PERMISSIONCODE='909';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.910', @PERMISSIONCODE='910';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='Add / edit documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.AddUpdate.Doc006', @PERMISSIONCODE='Doc006';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View the document library', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 004 Branding and Marking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.004', @PERMISSIONCODE='004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 100 Client Requests and Approvals', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.100', @PERMISSIONCODE='100';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 101 Internal Admin Correspondence', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.101', @PERMISSIONCODE='101';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 102 Office and Residence Leases', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.102', @PERMISSIONCODE='102';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 103 Various Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.103', @PERMISSIONCODE='103';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 104 Hotels Reservations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.104', @PERMISSIONCODE='104';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 105 Project Insurance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.105', @PERMISSIONCODE='105';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 106 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.106', @PERMISSIONCODE='106';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 107 Contact List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.107', @PERMISSIONCODE='107';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 108 Translations', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.108', @PERMISSIONCODE='108';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 109 IT Technical Info', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.109', @PERMISSIONCODE='109';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 301 Project Inventory List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.301', @PERMISSIONCODE='301';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 302 Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.302', @PERMISSIONCODE='302';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 303 Shipping Forms and Customs Docs', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.303', @PERMISSIONCODE='303';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 304 Waivers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.304', @PERMISSIONCODE='304';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 306 Commodities Tracking', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.306', @PERMISSIONCODE='306';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 500 RFP for Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.500', @PERMISSIONCODE='500';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 501 Technical Proposal and Budget', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.501', @PERMISSIONCODE='501';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 502 Agreements and Mods', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.502', @PERMISSIONCODE='502';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 503 Work Plans and Budgets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.503', @PERMISSIONCODE='503';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 504 Meeting Notes', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.504', @PERMISSIONCODE='504';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 505 Trip Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.505', @PERMISSIONCODE='505';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 506 Quarterly Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.506', @PERMISSIONCODE='506';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 507 Annual Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.507', @PERMISSIONCODE='507';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 508 M&E Plan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.508', @PERMISSIONCODE='508';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 509 M&E Reporting', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.509', @PERMISSIONCODE='509';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 510 Additional Reports and Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.510', @PERMISSIONCODE='510';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 511 Additional Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.511', @PERMISSIONCODE='511';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 512 Contact Stipend Payment Reconcilliation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.512', @PERMISSIONCODE='512';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 513 Critical Assessment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.513', @PERMISSIONCODE='513';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 514 Daily Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.514', @PERMISSIONCODE='514';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 515 Provincial Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.515', @PERMISSIONCODE='515';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 516 RFI Response', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.516', @PERMISSIONCODE='516';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 517 Spot Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.517', @PERMISSIONCODE='517';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 518 Syria Weekly Information Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.518', @PERMISSIONCODE='518';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 519 Weekly Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.519', @PERMISSIONCODE='519';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 520 Weekly Program Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.520', @PERMISSIONCODE='520';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 521 Other Document', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.521', @PERMISSIONCODE='521';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 522 Archived Full Atmospheric Report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.522', @PERMISSIONCODE='522';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 600 Project Org Chart', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.600', @PERMISSIONCODE='600';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 601 Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.601', @PERMISSIONCODE='601';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 602 Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.602', @PERMISSIONCODE='602';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 603 M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.603', @PERMISSIONCODE='603';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 604 Policing', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.604', @PERMISSIONCODE='604';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 605 Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.605', @PERMISSIONCODE='605';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 700 Activities Manual', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.700', @PERMISSIONCODE='700';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 701 Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.701', @PERMISSIONCODE='701';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 702 Activity Management ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.702', @PERMISSIONCODE='702';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 801 SI Activities', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.801', @PERMISSIONCODE='801';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 802 SI Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.802', @PERMISSIONCODE='802';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 803 SI Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.803', @PERMISSIONCODE='803';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 804 SI General Deliverables', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.804', @PERMISSIONCODE='804';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 805 SI Human Resources', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.805', @PERMISSIONCODE='805';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 806 SI Inventory and Procurement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.806', @PERMISSIONCODE='806';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 807 SI Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.807', @PERMISSIONCODE='807';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 808 SI Project Technical', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.808', @PERMISSIONCODE='808';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 900 Start-Up', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.900', @PERMISSIONCODE='900';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 901 HR ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.901', @PERMISSIONCODE='901';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 902 Procurement ', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.902', @PERMISSIONCODE='902';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 903 Finance', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.903', @PERMISSIONCODE='903';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 904 Contracts', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.904', @PERMISSIONCODE='904';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 905 Activity Management', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.905', @PERMISSIONCODE='905';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 906 IT', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.906', @PERMISSIONCODE='906';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 907 Security', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.907', @PERMISSIONCODE='907';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 908 Communications', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.908', @PERMISSIONCODE='908';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 909 Project Admin', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.909', @PERMISSIONCODE='909';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View 910 Closeout', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.910', @PERMISSIONCODE='910';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Research Documents', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc001', @PERMISSIONCODE='Doc001';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Project Reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc002', @PERMISSIONCODE='Doc002';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Financial and Management Information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc003', @PERMISSIONCODE='Doc003';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Meeting Minutes and Papers', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc004', @PERMISSIONCODE='Doc004';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Monitoring and Evaluation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc005', @PERMISSIONCODE='Doc005';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Document', @DESCRIPTION='View documents of type Other', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Documents', @PERMISSIONABLELINEAGE='Document.View.Doc006', @PERMISSIONCODE='Doc006';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit a donor decision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateDecision', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateDecision', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='Add / edit donor meetings & actions', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateMeeting', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.AddUpdateMeeting', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='List Donor Decisions, Meetings & Actions DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='DonorDecision', @DESCRIPTION='View DonorDecision', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='DonorDecision', @PERMISSIONABLELINEAGE='DonorDecision.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='Add / edit the equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='List EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentCatalog', @DESCRIPTION='View EquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Add or update an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Audit', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Audit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Create an equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Create', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Create', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='Delete an active equipment distribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.FinalizeEquipmentDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListDistributedInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistributedInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistributedInventory', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListDistribution', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListDistribution', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.ListInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListInventory', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.ListInventory', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.SetDeliveryDate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='SetDeliveryDate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.SetDeliveryDate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentDistribution', @DESCRIPTION='EquipmentDistribution.Transfer', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Transfer', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentDistribution.Transfer', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Add / edit the equipment inventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='List EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='Export EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentInventory', @DESCRIPTION='View EquipmentInventory', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentInventory.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='Audit Equipment EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Audit', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.Audit', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='EquipmentManagement', @DESCRIPTION='List Equipment Locations EquipmentManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Equipment', @PERMISSIONABLELINEAGE='EquipmentManagement.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='Add / edit a community asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='List CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityAsset', @DESCRIPTION='View CommunityAsset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityAsset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='Add / edit a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View the list of community rounds', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRound', @DESCRIPTION='View a community round', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRound.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.AddUpdate', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.List', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='CommunityRoundActivity', @DESCRIPTION='CommunityRoundActivity.View', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='CommunityRoundActivity.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='Add / edit a project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='List Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Project', @DESCRIPTION='View Project', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Implementation', @PERMISSIONABLELINEAGE='Project.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='Add / edit an indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='List Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Indicator', @DESCRIPTION='View Indicator', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Indicator.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='Add / edit an indicatortype', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='List IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='IndicatorType', @DESCRIPTION='View IndicatorType', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='IndicatorType.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='Add / edit a milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='List Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Milestone', @DESCRIPTION='View Milestone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Milestone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Add / edit an objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='M & E Overview Charts Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='ChartList', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.ChartList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='List Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Manage Objectives & Indicators Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Manage', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Manage', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='Overview Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Overview', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.Overview', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Objective', @DESCRIPTION='View Objective', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='LogicalFramework', @PERMISSIONABLELINEAGE='Objective.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit a concep nNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Budget ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.ExportConceptNoteBudget', @PERMISSIONCODE='ExportConceptNoteBudget';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Add / edit activity finances', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Finances', @PERMISSIONCODE='Finances';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk pane a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.AddUpdate.Risk', @PERMISSIONCODE='Risk';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Vetting List ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export Vetting ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='VettingList', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.VettingList.ExportVetting', @PERMISSIONCODE='ExportVetting';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Access to Justice', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetAccesstoJustice', @PERMISSIONCODE='BudgetAccesstoJustice';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Communication', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunication', @PERMISSIONCODE='BudgetCommunication';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Community Engagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetCommunityEngagement', @PERMISSIONCODE='BudgetCommunityEngagement';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Integrated Legitimate Structures', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetIntegratedLegitimateStructures', @PERMISSIONCODE='BudgetIntegratedLegitimateStructures';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type M&E', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetM&E', @PERMISSIONCODE='BudgetM&E';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type MER', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetMER', @PERMISSIONCODE='BudgetMER';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Police Development', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetPoliceDevelopment', @PERMISSIONCODE='BudgetPoliceDevelopment';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type RAP', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetRAP', @PERMISSIONCODE='BudgetRAP';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the budget on concept notes of component type Research', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.BudgetResearch', @PERMISSIONCODE='BudgetResearch';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='Export ConceptNote', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNote', @DESCRIPTION='View the risk panel in a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNote.View.Risk', @PERMISSIONCODE='Risk';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Add / edit equipment associated with a concept note', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Finalize Equipment Distribution ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='FinalizeEquipmentDistribution', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.FinalizeEquipmentDistribution', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='List ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='View ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ConceptNoteContactEquipment', @DESCRIPTION='Export ConceptNoteContactEquipment', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='ConceptNoteContactEquipment.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='Add / edit a license', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='List License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='License', @DESCRIPTION='View License', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='License.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Add / edit the license equipment catalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='List LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='Export LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='LicenseEquipmentCatalog', @DESCRIPTION='View LicenseEquipmentCatalog', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='LicenseEquipmentCatalog.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Add / edit a purchase request', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='List PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='View PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='PurchaseRequest', @DESCRIPTION='Export PurchaseRequest', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='PurchaseRequest.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Add or update a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='Delete a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Delete', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.Delete', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='List workplans', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workplan', @DESCRIPTION='View a workplan', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Operations', @PERMISSIONABLELINEAGE='Workplan.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Add / edit a program report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='List ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='View ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='ProgramReport', @DESCRIPTION='Export ProgramReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='ProgramReports', @PERMISSIONABLELINEAGE='ProgramReport.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Add / edit a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='List Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the analysis tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Analysis', @PERMISSIONCODE='Analysis';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Export Equipment Distributions Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.ExportEquipmentDistribution', @PERMISSIONCODE='ExportEquipmentDistribution';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='Implementation Province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Implementation', @PERMISSIONCODE='Implementation';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Province', @DESCRIPTION='View the information tab for a province', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Province', @PERMISSIONABLELINEAGE='Province.View.Information', @PERMISSIONCODE='Information';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='Add / edit an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View the list of assets', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Asset', @DESCRIPTION='View an asset', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Asset.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='AssetUnit', @DESCRIPTION='A bypass to allow asset units not equipment transfer eligible to be displayed on the equipment distribution list', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=1, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='AssetUnit.List.NonEquipmentTransferEligible', @PERMISSIONCODE='NonEquipmentTransferEligible';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='Add / edit an atmospheric report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='List Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Atmospheric', @DESCRIPTION='View Atmospheric', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Atmospheric.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='Add / edit a finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='List Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Finding', @DESCRIPTION='View Finding', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Finding.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='Add / edit a force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='List Forces Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Force', @DESCRIPTION='View a force Force', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Force.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='Add / edit an incident report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='List Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Incident', @DESCRIPTION='View Incident', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Incident.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Add / edit an media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View the list of media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='Export selected media reports', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='MediaReport', @DESCRIPTION='View a media report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='MediaReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Add / edit a recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='List Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='Export Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Recommendation', @DESCRIPTION='View Recommendation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Recommendation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Add a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Add', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Add', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='List RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Export RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.List.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Edit a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='Amend a request for information', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='Update', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.Update.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='RequestForInformation', @DESCRIPTION='View RequestForInformation', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='RequestForInformation.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Add / edit a risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='Export Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.AddUpdate.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='List Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Risk', @DESCRIPTION='View Risk', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Risk.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Add / edit a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Amend a spot report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.AddUpdate.Amend', @PERMISSIONCODE='Amend';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='List SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='View SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Approved SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Approved', @PERMISSIONCODE='Approved';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='Export SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SpotReport', @DESCRIPTION='In Work SpotReport', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SpotReport.View.InWork', @PERMISSIONCODE='InWork';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey question', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Add / edit a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdateSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AddUpdateSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Administer a survey', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AdministerSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.AdministerSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Export Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ExportSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ExportSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Questions List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListQuestions', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListQuestions', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='List Survey Responses SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveyResponses', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveyResponses', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='Surveys List SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ListSurveys', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ListSurveys', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Questions SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewQuestion', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewQuestion', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SurveyManagement', @DESCRIPTION='View Surveys SurveyManagement', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='ViewSurvey', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='SurveyManagement.ViewSurvey', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Weekly Report Add / Update', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='Export the weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.AddUpdate.Export', @PERMISSIONCODE='Export';
EXEC permissionable.SavePermissionable @CONTROLLERNAME='WeeklyReport', @DESCRIPTION='View the completed weekly report', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='WeeklyReport.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='Add / edit a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View the list of zones', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Zone', @DESCRIPTION='View a zone', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Research', @PERMISSIONABLELINEAGE='Zone.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='Add / edit a sub-contractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='List SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='SubContractor', @DESCRIPTION='View SubContractor', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Subcontractor', @PERMISSIONABLELINEAGE='SubContractor.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='Add / edit a class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='List Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Class', @DESCRIPTION='View Class', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Class.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='Add / edit a course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='List Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Course', @DESCRIPTION='View Course', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Training', @PERMISSIONABLELINEAGE='Course.View', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='Add / edit a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='AddUpdate', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.AddUpdate', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View the list of workflows', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='List', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.List', @PERMISSIONCODE=NULL;
EXEC permissionable.SavePermissionable @CONTROLLERNAME='Workflow', @DESCRIPTION='View a workflow', @DISPLAYORDER=0, @ISGLOBAL=0, @ISSUPERADMINISTRATOR=0, @METHODNAME='View', @PERMISSIONABLEGROUPCODE='Workflows', @PERMISSIONABLELINEAGE='Workflow.View', @PERMISSIONCODE=NULL;
--End table permissionable.Permissionable

--Begin update super administrtor permissionables
EXEC utility.UpdateSuperAdministratorPersonPermissionables
--End update super administrtor permissionables

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO
--End table dbo.MenuItemPermissionableLineage

--Begin build tracking
INSERT INTO utility.BuildLog (BuildKey) VALUES ('Build - 2.7 - 2017.04.21 16.30.27')
GO
--End build tracking

