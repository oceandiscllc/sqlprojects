USE AJACS
GO

UPDATE P
SET 
	P.PermissionCode = 'ExportJO',
	P.Description = 'Export the vetting list for vetting type JO.'
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Vetting.List.Export'
GO

INSERT INTO permissionable.Permissionable
	(DisplayOrder, IsGlobal, ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsActive, IsSuperAdministrator)
SELECT
	P.DisplayOrder, 
	P.IsGlobal, 
	P.ControllerName, 
	P.MethodName, 
	'ExportUK', 
	P.PermissionableGroupID, 
	'Export the vetting list for vetting type UK.',
	P.IsActive, 
	P.IsSuperAdministrator
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Vetting.List.ExportJO'
GO

INSERT INTO permissionable.Permissionable
	(DisplayOrder, IsGlobal, ControllerName, MethodName, PermissionCode, PermissionableGroupID, Description, IsActive, IsSuperAdministrator)
SELECT
	P.DisplayOrder, 
	P.IsGlobal, 
	P.ControllerName, 
	P.MethodName, 
	'ExportUS', 
	P.PermissionableGroupID, 
	'Export the vetting list for vetting type US.',
	P.IsActive, 
	P.IsSuperAdministrator
FROM permissionable.Permissionable P
WHERE P.PermissionableLineage = 'Vetting.List.ExportJO'
GO

UPDATE PPL
SET PPL.PermissionableLineage = 'Vetting.List.ExportJO'
FROM permissionable.PersonPermissionable PPL
WHERE PPL.PermissionableLineage = 'Vetting.List.Export'
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	PPL.PersonID,
	'Vetting.List.ExportUK'
FROM permissionable.PersonPermissionable PPL
WHERE PPL.PermissionableLineage = 'Vetting.List.ExportJO'
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	PPL.PersonID,
	'Vetting.List.ExportUS'
FROM permissionable.PersonPermissionable PPL
WHERE PPL.PermissionableLineage = 'Vetting.List.ExportJO'
GO
