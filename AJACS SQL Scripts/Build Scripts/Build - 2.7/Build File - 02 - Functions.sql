USE AJACS
GO

--Begin function dbo.GetConceptNoteProvinceListByConceptNoteID
EXEC utility.DropObject 'dbo.GetConceptNoteProvinceListByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2017.03.23
-- Description:	A function to get a list of community names based on a conceptnoteid
-- =================================================================================

CREATE FUNCTION dbo.GetConceptNoteProvinceListByConceptNoteID
(
@ConceptNoteID INT
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	DECLARE @cProvinceNameList NVARCHAR(MAX)

	SELECT @cProvinceNameList = COALESCE(@cProvinceNameList + ', ', '') + P.ProvinceName
	FROM dbo.Province P
		JOIN
			(
			SELECT 
				C.ProvinceID
			FROM dbo.ConceptNoteCommunity CNC
				JOIN dbo.Community C ON C.CommunityID = CNC.CommunityID
					AND CNC.ConceptNoteID = @ConceptNoteID

			UNION

			SELECT 
				CNP.ProvinceID
			FROM dbo.ConceptNoteProvince CNP
			WHERE CNP.ConceptNoteID = @ConceptNoteID
			)	D ON D.ProvinceID = P.ProvinceID
	ORDER BY P.ProvinceName

	RETURN @cProvinceNameList

END
GO
--End function dbo.GetConceptNoteProvinceListByConceptNoteID

--Begin function dbo.GetContactStipendEligibility
EXEC utility.DropObject 'dbo.GetContactStipendEligibility'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibility
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @nIsCompliant INT

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @nIsCompliant = 
		dbo.IsCommunityStipendEligible(A.CommunityID)
		 + CAST(A.IsActive  AS INT)
		 + CAST(AU.IsActive AS INT)
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN 1 ELSE 0 END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) THEN 1 ELSE 0 END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF ISNULL(@nIsCompliant, 0) = 5
		SET @bReturn = 1
	--ENDIF

	RETURN @bReturn

END
GO
--End function dbo.GetContactStipendEligibility

--Begin function dbo.GetContactStipendEligibilityNotes
EXEC utility.DropObject 'dbo.GetContactStipendEligibilityNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2016.09.27
-- Description:	A function to return a table with contact stipend eligibility data
--
-- Author:			Todd Pires
-- Update date:	2017.04.20
-- Description:	Added ConceptNoteID support
-- ===============================================================================

CREATE FUNCTION dbo.GetContactStipendEligibilityNotes
(
@ContactID INT,
@ConceptNoteID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cFundingSourceCode VARCHAR(50)
	DECLARE @cReturn VARCHAR(MAX) = NULL

	SELECT @cFundingSourceCode = FS.FundingSourceCode
	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT @cReturn =
		CASE WHEN dbo.IsCommunityStipendEligible(A.CommunityID) = 1 THEN '' ELSE 'Assigned asset community is not stipend elligible,' END
		 + CASE WHEN A.IsActive = 1 THEN '' ELSE 'Assigned asset is not active,' END
		 + CASE WHEN AU.IsActive = 1 THEN '' ELSE 'Assigned asset department is not active,' END
		 + CASE WHEN A.RelocationDate IS NULL OR DATEADD(d, 90, A.RelocationDate) < getDate() THEN '' ELSE 'Assigned asset tepmorary relocation period has expired,' END
		 + CASE WHEN EXISTS (SELECT 1 FROM dbo.Contact C1 WHERE C1.ContactID = AU.CommanderContactID) THEN '' ELSE 'Department has no department head assigned,' END
		 + CASE WHEN (@cFundingSourceCode = 'EU' AND C2.IsEUEquipmentTransferEligible = 1) OR (@cFundingSourceCode = 'US' AND C2.IsUSEquipmentTransferEligible = 1) THEN '' ELSE 'Assigned asset department head vetting has expired,' END
	FROM dbo.Contact C1
		JOIN asset.AssetUnit AU ON AU.AssetUnitID = C1.AssetUnitID
		JOIN asset.Asset A ON A.AssetID = AU.AssetID
		JOIN dbo.Contact C2 ON C2.ContactID = AU.CommanderContactID
			AND C1.ContactID = @ContactID

	IF @cReturn IS NULL
		SET @cReturn = 'Invalid Data'
	ELSE IF @cReturn = ''
		SET @cReturn = 'Stipend Eligible'
	ELSE
		SET @cReturn = 'Stipend Ineligible,' + @cReturn
	--ENDIF

	RETURN REPLACE(@cReturn, ',', '&#10;')

END
GO
--End function dbo.GetContactStipendEligibilityNotes

--Begin function force.GetCommunityIDByForceID
EXEC utility.DropObject 'force.GetCommunityIDByForceID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.09
-- Description:	A function to get a list of CommunityIDs based on an ForceID
-- =========================================================================

CREATE FUNCTION force.GetCommunityIDByForceID
(
@ForceID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nCommunityID INT

	SELECT @nCommunityID = 
		CASE
			WHEN F.TerritoryTypeCode = 'Province' 
			THEN 0
			ELSE F.TerritoryID 
		END

	FROM force.Force F 
	WHERE F.ForceID = @ForceID
	
	RETURN ISNULL(@nCommunityID, 0)

END
GO
--End function force.GetCommunityIDByForceID

--Begin function dbo.GetLastVettingOutcomeID
EXEC utility.DropObject 'dbo.GetLastVettingOutcomeID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.18
-- Description:	A function to get the last vetting outcomeid for a contactid and vettingtypeid
-- ===========================================================================================

CREATE FUNCTION dbo.GetLastVettingOutcomeID
(
@ContactID INT,
@ContactVettingTypeID INT
)

RETURNS INT

AS
BEGIN

	DECLARE @nVettingOutcomeID INT

	SELECT TOP 1 @nVettingOutcomeID = CV.VettingOutcomeID
	FROM dbo.ContactVetting CV
	WHERE CV.ContactID = @ContactID
		AND CV.ContactVettingTypeID = @ContactVettingTypeID
	ORDER BY CV.ContactVettingID DESC
	
	RETURN ISNULL(@nVettingOutcomeID, 0)

END
GO
--End function dbo.GetLastVettingOutcomeID

--Begin function reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID
EXEC utility.DropObject 'reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create date:	2017.04.09
-- Description:	A function to get a list of CommunityIDs based on an EquipmentDistributionID
-- =========================================================================================

CREATE FUNCTION reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID
(
@EquipmentDistributionID INT
)

RETURNS @tTable TABLE (CommunityID INT NOT NULL PRIMARY KEY)

AS
BEGIN

	INSERT INTO @tTable 
		(CommunityID)
	SELECT DISTINCT
		CASE
			WHEN DI.EndUserEntityTypeCode = 'Asset'
			THEN (SELECT A.CommunityID FROM asset.Asset A WHERE A.AssetID = DI.EndUserEntityID)
			WHEN DI.EndUserEntityTypeCode = 'AssetUnit'
			THEN (SELECT asset.GetCommunityIDByAssetUnitID(DI.EndUserEntityID))
			WHEN DI.EndUserEntityTypeCode = 'Contact'
			THEN (SELECT C.CommunityID FROM dbo.Contact C WHERE C.ContactID = DI.EndUserEntityID)
			WHEN DI.EndUserEntityTypeCode = 'Force'
			THEN (SELECT force.GetCommunityIDByForceID(DI.EndUserEntityID))
		END AS CommunityID
	FROM procurement.DistributedInventory DI
	WHERE DI.EquipmentDistributionID = @EquipmentDistributionID
	
	RETURN

END
GO
--End function reporting.GetDistributedInventoryCommunitiesByDistributedInventoryID
