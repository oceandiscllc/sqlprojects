USE AJACS
GO

--Begin table dbo.MenuItem
UPDATE dbo.MenuItem
SET MenuItemText = 'Field Activities'
WHERE MenuItemCode = 'ProjectList'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='DonorDecision', @NewMenuItemLink='/donordecision/list', @NewMenuItemText='Donor Logs', @BeforeMenuItemCode='EventLogList', @PermissionableLineageList='DonorDecision.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--End table dbo.MenuItem

/*
--Begin table dropdown.DocumentType
IF NOT EXISTS (SELECT 1 FROM dropdown.DocumentType DT WHERE DT.DocumentTypeCode = 'BiWeeklyReport')
	BEGIN
	
	INSERT INTO dropdown.DocumentType
		(DocumentTypeCode, DocumentTypeName, DocumentGroupID, DocumentTypePermissionCode)
	VALUES
		(
		'BiWeeklyReport',
		'522 Program Report',
		4,
		522
		)

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		(SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DocumentType'),
		'522',
		'522 Program Report'
		)
		
	END
--ENDIF
GO
--End table dropdown.DocumentType
*/

--Begin table permissionable.Permissionable
IF NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = 'DonorDecision.List')
	BEGIN

	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		(
		0,
		'DonorDecision',
		'Donor Decisions, Meetings & Actions'
		)


	INSERT INTO permissionable.Permissionable
		(ParentPermissionableID, PermissionableCode, PermissionableName)
	VALUES
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'List',	'List Donor Decisions, Meetings & Actions'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'AddUpdateDecision', 'Add / Edit Donor Decisions'),
		((SELECT P.PermissionableID FROM permissionable.Permissionable P WHERE P.PermissionableCode = 'DonorDecision'),	'AddUpdateMeeting',	'Add / Edit Donor Meetings & Actions')

	END
--ENDIF
GO
--End table permissionable.Permissionable

--Begin table permissionable.PersonPermissionable
DELETE PP
FROM permissionable.PersonPermissionable PP
	JOIN dbo.Person P ON P.PersonID = PP.PersonID
		AND P.UserName IN 
			(
			'DaveR',
			'bgreen',
			'christopher.crouch',
			'gyingling',
			'jburnham',
			'JCole',
			'jlyons',
			'kevin',
			'Naveen',
			'Rabaa',
			'todd.pires'
			)

INSERT INTO permissionable.PersonPermissionable
	(PersonID,PermissionableLineage)
SELECT
	P1.PersonID,
	P2.PermissionableLineage
FROM dbo.Person P1, permissionable.Permissionable P2
WHERE P1.UserName IN 
	(
	'DaveR',
	'bgreen',
	'christopher.crouch',
	'gyingling',
	'jburnham',
	'JCole',
	'jlyons',
	'kevin',
	'Naveen',
	'Rabaa',
	'todd.pires'
	)
GO
--End table permissionable.PersonPermissionable
