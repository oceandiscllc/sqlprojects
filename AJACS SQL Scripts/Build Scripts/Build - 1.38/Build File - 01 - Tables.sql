USE AJACS
GO

--Begin table dbo.DonorDecision
DECLARE @TableName VARCHAR(250) = 'dbo.DonorDecision'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.DonorDecision
	(
	DonorDecisionID INT IDENTITY(1,1) NOT NULL,
	RaisedDate DATE,
	DiscussedDate DATE,
	Issue NVARCHAR(max),
	Note NVARCHAR(250),
	KMSLink NVARCHAR(2048),
	RequiredActions NVARCHAR(max),
	DonorDecisionDecisionID INT,
	Rationale NVARCHAR(max),
	DeadlineDate DATE,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DonorDecisionDecisionID', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DonorDecisionID'
GO
--End table dbo.DonorDecision

--Begin table dbo.DonorMeeting
DECLARE @TableName VARCHAR(250) = 'dbo.DonorMeeting'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.DonorMeeting
	(
	DonorMeetingID INT IDENTITY(1,1) NOT NULL,
	MeetingDate DATE,
	Issue NVARCHAR(max),
	MeetingAction NVARCHAR(250),
	DueDate DATE,
	Owner NVARCHAR(250),
	DonorMeetingStatusID INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DonorMeetingStatusID', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DonorMeetingID'
GO
--End table dbo.DonorMeeting

--Begin table dbo.DonorMeetingDonorMeetingComponent
DECLARE @TableName VARCHAR(250) = 'dbo.DonorMeetingDonorMeetingComponent'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.DonorMeetingDonorMeetingComponent
	(
	DonorMeetingDonorMeetingComponentID INT IDENTITY(1,1) NOT NULL,
	DonorMeetingID INT,
	DonorMeetingComponentID INT
	)

EXEC utility.SetDefaultConstraINT @TableName, 'DonorMeetingID', 'INT', 0
EXEC utility.SetDefaultConstraINT @TableName, 'DonorMeetingComponentID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'DonorMeetingDonorMeetingComponentID'
EXEC utility.SetIndexClustered 'IX_DonorMeetingDonorMeetingComponent', @TableName, 'DonorMeetingID,DonorMeetingComponentID'
GO
--End table dbo.DonorMeetingComponent

--Begin table dbo.RequestForInformation
DECLARE @TableName VARCHAR(250) = 'dbo.RequestForInformation'

EXEC utility.AddColumn @TableName, 'Argument', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'AttachmentsList', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'CoverLetterFrom', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'CoverLetterTo', 'VARCHAR(250)'
EXEC utility.AddColumn @TableName, 'Background', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Issue', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Recommendation', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Resources', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Risk', 'VARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'Timing', 'VARCHAR(MAX)'
GO
--End table dbo.RequestForInformation

--Begin table dropdown.DonorMeetingComponent
DECLARE @TableName VARCHAR(250) = 'dropdown.DonorMeetingComponent'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DonorMeetingComponent
	(
	DonorMeetingComponentID INT IDENTITY(0,1) NOT NULL,
	DonorMeetingComponentName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DonorMeetingComponentID'
EXEC utility.SetIndexNonClustered 'IX_DonorMeetingComponent', @TableName, 'DisplayOrder,DonorMeetingComponentName', 'DonorMeetingComponentID'
GO

SET IDENTITY_INSERT dropdown.DonorMeetingComponent ON
GO

INSERT INTO dropdown.DonorMeetingComponent (DonorMeetingComponentID) VALUES (0)

SET IDENTITY_INSERT dropdown.DonorMeetingComponent OFF
GO

INSERT INTO dropdown.DonorMeetingComponent 
	(DonorMeetingComponentName)
VALUES
	('Community Engagement'),
	('FIF'),
	('Justice'),
	('Police'),
	('Research')
GO
--End table dropdown.DonorMeetingComponent

--Begin table dropdown.DonorMeetingDecision
DECLARE @TableName VARCHAR(250) = 'dropdown.DonorMeetingDecision'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DonorMeetingDecision
	(
	DonorMeetingDecisionID INT IDENTITY(0,1) NOT NULL,
	DonorMeetingDecisionName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DonorMeetingDecisionID'
EXEC utility.SetIndexNonClustered 'IX_DonorMeetingDecision', @TableName, 'DisplayOrder,DonorMeetingDecisionName', 'DonorMeetingDecisionID'
GO

SET IDENTITY_INSERT dropdown.DonorMeetingDecision ON
GO

INSERT INTO dropdown.DonorMeetingDecision (DonorMeetingDecisionID) VALUES (0)

SET IDENTITY_INSERT dropdown.DonorMeetingDecision OFF
GO

INSERT INTO dropdown.DonorMeetingDecision 
	(DonorMeetingDecisionName)
VALUES
	('Awaits'),
	('Approved'),
	('Deferred'),
	('Rejected')
GO
--End table dropdown.DonorMeetingDecision

--Begin table dropdown.DonorMeetingStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.DonorMeetingStatus'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.DonorMeetingStatus
	(
	DonorMeetingStatusID INT IDENTITY(0,1) NOT NULL,
	DonorMeetingStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'DonorMeetingStatusID'
EXEC utility.SetIndexNonClustered 'IX_DonorMeetingStatus', @TableName, 'DisplayOrder,DonorMeetingStatusName', 'DonorMeetingStatusID'
GO

SET IDENTITY_INSERT dropdown.DonorMeetingStatus ON
GO

INSERT INTO dropdown.DonorMeetingStatus (DonorMeetingStatusID) VALUES (0)

SET IDENTITY_INSERT dropdown.DonorMeetingStatus OFF
GO

INSERT INTO dropdown.DonorMeetingStatus 
	(DonorMeetingStatusName, DisplayOrder)
VALUES
	('N/A', 1),
	('In Progress', 2),
	('Overdue', 3),
	('Completed', 4)
GO
--End table dropdown.DonorMeetingStatus

--Begin table project.Project
DECLARE @TableName VARCHAR(250) = 'project.Project'

EXEC utility.AddColumn @TableName, 'ComponentID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'ComponentID', 'INT', 0
GO
--End table dbo.RequestForInformation

