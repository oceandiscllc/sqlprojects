-- File Name:	Build-1.57 File 01 - AJACS.sql
-- Build Key:	Build-1.57 File 01 - AJACS - 2016.05.13 21.53.25

USE AJACS
GO

-- ==============================================================================================================================
-- Functions:
--		dbo.GetEventNameByEventCode
--		workflow.CanHaveAddUpdate
--
-- Procedures:
--		dbo.CloneConceptNote
--		dbo.GetConceptNoteByConceptNoteID
--		dbo.ValidateLogin
--		dropdown.GetContactVettingTypeData
--		dropdown.GetVettingOutcomeData
--		permissionable.GetPermissionableByPermissionableID
--		permissionable.GetPermissionables
--		workflow.ResetEntityWorkflowStepGroupPerson
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
USE AJACS
GO

--Begin table dropdown.VettingOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.VettingOutcome'

EXEC utility.AddColumn @TableName, 'VettingOutcomeCode', 'VARCHAR(50)'
GO

UPDATE VO
SET VO.VettingOutcomeCode = REPLACE(VO.VettingOutcomeName, ' ', '')
FROM dropdown.VettingOutcome VO
GO
--End table dropdown.VettingOutcome

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.AddColumn @TableName, 'IsSuperAdministrator', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
GO
--End table permissionable.Permissionable

--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql
USE AJACS
GO

--Begin function dbo.GetEventNameByEventCode
EXEC utility.DropObject 'dbo.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
--
-- Author:			Todd Pires
-- Create date:	2015.04.30
-- Description:	Added failedlogin support
-- ====================================================================
CREATE FUNCTION dbo.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'finalize'
			THEN 'Finalize'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'read'
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			WHEN @EventCode = 'vettingupdate'
			THEN 'Vetting Update'
			ELSE @EventCode + ' not found'
		END

	RETURN @EventCodeName

END
GO
--End function dbo.GetEventNameByEventCode

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T ORDER BY T.CommunityProvinceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'FIFUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM fifupdate.FIFUpdate T ORDER BY T.FIFUpdateID DESC
		ELSE IF @EntityTypeCode = 'JusticeUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM justiceupdate.JusticeUpdate T ORDER BY T.JusticeUpdateID DESC
		ELSE IF @EntityTypeCode = 'PoliceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate T ORDER BY T.PoliceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM RiskUpdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM WeeklyReport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
		
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = @nWorkflowStepNumber
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE IF @EntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate
--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
USE AJACS
GO

--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.USVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.USVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.UKVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.UKVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID
			FROM dbo.ContactVetting CV12
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID
			FROM dbo.ContactVetting CV22
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1
				
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
--
-- Author:			Todd Pires
-- Create date: 2016.05.09
-- Description:	Implemented the new workflow system
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,ActualTotalAmount,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,
		C.ActualTotalAmount,
		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,
		'Clone of:  ' + C.Title,
		C.VettingRequirements,
		1
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
	SELECT
		EWSGP.EntityTypeCode, 
		EWSGP.EntityID, 
		EWSGP.WorkflowID, 
		EWSGP.WorkflowStepID, 
		EWSGP.WorkflowStepGroupID, 
		EWSGP.WorkflowName, 
		EWSGP.WorkflowStepNumber, 
		EWSGP.WorkflowStepName, 
		EWSGP.WorkflowStepGroupName, 
		EWSGP.PersonID, 
		0
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.EntityTypeCode = 'ConceptNote'
		AND EWSGP.EntityID = @nConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO	dbo.ConceptNoteAmmendment
		(ConceptNoteID, AmmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Ammendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Ammendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote

--Begin procedure dbo.ValidateLogin
EXEC Utility.DropObject 'dbo.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date: 2015.02.05
-- Description:	A stored procedure to validate user logins
--
-- Author:			Todd Pires
-- Create date: 2015.03.05
-- Description:	Changed the way the IsAccountLocked variable is set
--
-- Author:			John Lyons
-- Create date: 2015.07.29
-- Description:	Added two factor support
--
-- Author:			Brandon Green
-- Create date: 2015.07.30
-- Description:	Added password expiration support
--
-- Author:			Todd Pires
-- Create date:	2015.08.31
-- Description:	Implemented the FormatPersonNameByPersonID function
--
-- Author:			Todd Pires
-- Create date:	2016.05.08
-- Description:	Added the IsSuperAdministrator bit
-- ================================================================
CREATE PROCEDURE dbo.ValidateLogin

@UserName VARCHAR(250),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bCreateNewPassword BIT = 0
	DECLARE @bIsAccountLockedOut BIT
	DECLARE @bIsActive BIT
	DECLARE @bIsLegacyPassword BIT
	DECLARE @bIsPasswordExpired BIT
	DECLARE @bIsPhoneVerified BIT
	DECLARE @bIsProfileUpdateRequired BIT = 0
	DECLARE @bIsSuperAdministrator BIT
	DECLARE @bIsTwoFactorEnabled INT
	DECLARE @bIsValidPassword BIT
	DECLARE @bIsValidUserName BIT
	DECLARE @cEmailAddress VARCHAR(320)
	DECLARE @cFullName VARCHAR(250)
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @cPhone VARCHAR(64)
	DECLARE @cRoleName VARCHAR(50)
	DECLARE @cUsername VARCHAR(250)
	DECLARE @nCountryCallingCodeID INT
	DECLARE @nI INT = 0
	DECLARE @nInvalidLoginAttempts INT
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPasswordDuration INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE 
		(
		EmailAddress VARCHAR(320),
		IsAccountLockedOut BIT NOT NULL DEFAULT 0,
		IsActive BIT NOT NULL DEFAULT 0,
		IsPasswordExpired BIT NOT NULL DEFAULT 0,
		IsProfileUpdateRequired BIT NOT NULL DEFAULT 0,
		IsSuperAdministrator BIT NOT NULL DEFAULT 0,
		IsValidPassword BIT NOT NULL DEFAULT 0,
		IsValidUserName BIT NOT NULL DEFAULT 0,
		PersonID INT NOT NULL DEFAULT 0,
		FullName VARCHAR(250),
		RoleName VARCHAR(50),
		Username VARCHAR(250),
		CountryCallingCodeID INT,
		Phone VARCHAR(64),
		IsPhoneVerified BIT
		)

	SET @bIsTwoFactorEnabled = CAST((SELECT dbo.GetServerSetupValueByServerSetupKey('TwoFactorEnabled', '0')) AS INT)

	SELECT
		@cEmailAddress = P.EmailAddress,
		@cFullName = dbo.FormatPersonNameByPersonID(P.PersonID, 'FirstLast'),
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nInvalidLoginAttempts = P.InvalidLoginAttempts,
		@bIsAccountLockedOut = P.IsAccountLockedOut,
		@bIsActive = P.IsActive,
		@bIsLegacyPassword = P.IsLegacyPassword,
		@nPasswordDuration = CAST(dbo.GetServerSetupValueByServerSetupKey('PasswordDuration', '30') AS INT),
		
		@bIsPasswordExpired = 
			CASE
				WHEN P.PasswordExpirationDateTime IS NOT NULL AND P.PasswordExpirationDateTime < SYSUTCDateTime()
				THEN 1
				ELSE 0
			END,

		@bIsProfileUpdateRequired = 
			CASE
				WHEN P.EmailAddress IS NULL OR LEN(LTRIM(P.EmailAddress)) = 0
					OR P.FirstName IS NULL OR LEN(LTRIM(P.FirstName)) = 0
					OR P.LastName IS NULL OR LEN(LTRIM(P.LastName)) = 0
					OR P.Organization IS NULL OR LEN(LTRIM(P.Organization)) = 0
					OR (@bIsTwoFactorEnabled = 1 AND (P.Phone IS NULL OR LEN(LTRIM(P.Phone)) = 0))
					OR (@bIsTwoFactorEnabled = 1 AND P.CountryCallingCodeID = 0)
					OR (@bIsTwoFactorEnabled = 1 AND P.IsPhoneVerified = 0)
				THEN 1
				ELSE 0
			END,

		@bIsSuperAdministrator = P.IsSuperAdministrator,
		@nPersonID = ISNULL(P.PersonID, 0),
		@cRoleName = R.RoleName,
		@cUserName = P.UserName,
		@nCountryCallingCodeID = P.CountryCallingCodeID,
		@cPhone =P.Phone,
		@bIsPhoneVerified = P.IsPhoneVerified
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.UserName = @UserName

	SET @nPersonID = ISNULL(@nPersonID, 0)
	SET @bIsValidUserName = CASE WHEN @nPersonID = 0 THEN 0 ELSE 1 END

	IF @bIsValidUserName = 0
		BEGIN

		INSERT INTO @tPerson 
			(PersonID) 
		VALUES 
			(0)
		
		END
	ELSE
		BEGIN

		IF @bIsLegacyPassword = 1
			BEGIN

			SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256', @Password + @cPasswordSalt))

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = LOWER(AJACSUtility.dbo.udf_hashBytes ('SHA256',  @cPasswordHash + @cPasswordSalt))
				SET @nI = @nI + 1

				END
			--END WHILE

			SET @bCreateNewPassword = 1
			SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

			END
		--ENDIF

		IF @bIsLegacyPassword = 0 OR @bCreateNewPassword = 1
			BEGIN

			SET @nI = 0

			IF @bCreateNewPassword = 1
				SELECT @cPasswordSalt = NewID()
			--ENDIF

			SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @cPasswordSalt), 2)

			WHILE (@nI < 65536)
				BEGIN

				SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @cPasswordSalt), 2)
				SET @nI = @nI + 1

				END
			--END WHILE

			IF @bCreateNewPassword = 0
				SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END
			--ENDIF
			
			END
		--ENDIF

		INSERT INTO @tPerson 
			(PersonID,EmailAddress,IsAccountLockedOut,IsActive,IsProfileUpdateRequired,IsSuperAdministrator,IsValidPassword,IsValidUserName,FullName,RoleName,UserName,CountryCallingCodeID,Phone,IsPhoneVerified) 
		VALUES 
			(
			@nPersonID,
			@cEmailAddress,
			@bIsAccountLockedOut,
			@bIsActive,
			@bIsProfileUpdateRequired,
			@bIsSuperAdministrator,
			@bIsValidPassword,
			@bIsValidUserName,
			@cFullName,
			@cRoleName,
			@cUserName,
			@nCountryCallingCodeID,
			@cPhone,
			@bIsPhoneVerified
			)
		
		END
	--ENDIF	

	IF @bIsValidUserName = 1 AND @bIsActive = 1
		BEGIN
		
		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(dbo.GetServerSetupValueByServerSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1		
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF
			SET @bIsAccountLockedOut = CASE WHEN @nInvalidLoginAttempts >= @nInvalidLoginLimit THEN 1 ELSE @bIsAccountLockedOut END

			UPDATE dbo.Person
			SET 
				InvalidLoginAttempts = @nInvalidLoginAttempts,
				IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID
			
			UPDATE @tPerson
			SET IsAccountLockedOut = @bIsAccountLockedOut
			WHERE PersonID = @nPersonID

			END
		ELSE 
			BEGIN
			
			IF @bIsPasswordExpired = 1
				BEGIN
			
				UPDATE @tPerson
				SET IsPasswordExpired = 1
				WHERE PersonID = @nPersonID

				END
			ELSE
				BEGIN

				UPDATE dbo.Person
				SET 
					InvalidLoginAttempts = 0,
					IsLegacyPassword = 0,
					Password = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordHash
							ELSE Password
						END,

					PasswordSalt = 
						CASE
							WHEN @bCreateNewPassword = 1
							THEN @cPasswordSalt
							ELSE PasswordSalt
						END

				WHERE PersonID = @nPersonID

				END
			--ENDIF

			END
		--ENDIF

		END
	--ENDIF
		
	SELECT * FROM @tPerson

	SELECT 
		PP.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.PersonPermissionable PP
	WHERE PP.PersonID = @nPersonID
			
	UNION

	SELECT 
		P.PermissionableLineage,
		1 AS HasPermissionable
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	ORDER BY 1

END
GO
--End procedure dbo.ValidateLogin

--Begin procedure dropdown.GetContactVettingTypeData
EXEC utility.DropObject 'dropdown.GetContactVettingTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.24
-- Description:	A stored procedure to return data from the dropdown.ContactVetting table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetContactVettingTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ContactVettingTypeID, 
		T.ContactVettingTypeCode, 
		T.ContactVettingTypeName
	FROM dropdown.ContactVettingType T
	WHERE (T.ContactVettingTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ContactVettingTypeName, T.ContactVettingTypeID

END
GO
--End procedure dropdown.GetContactVettingTypeData

--Begin procedure dropdown.GetVettingOutcomeData
EXEC utility.DropObject 'dropdown.GetVettingOutcomeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create date:	2016.01.24
-- Description:	A stored procedure to return data from the dropdown.ContactVetting table
-- =====================================================================================
CREATE PROCEDURE dropdown.GetVettingOutcomeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.VettingOutcomeID, 
		T.VettingOutcomeCode, 
		T.VettingOutcomeName
	FROM dropdown.VettingOutcome T
	WHERE (T.VettingOutcomeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.VettingOutcomeName, T.VettingOutcomeID

END
GO
--End procedure dropdown.GetVettingOutcomeData

--Begin procedure permissionable.GetEntityPermissionables
EXEC Utility.DropObject 'permissionable.GetEntityPermissionables'
GO
--End procedure permissionable.GetEntityPermissionables

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC Utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Brandon Green
-- Create date:	2015.10.02
-- Description:	A stored procedure to data from the permissionable.Permissionable table
-- ====================================================================================
CREATE PROCEDURE permissionable.GetPermissionableByPermissionableID

@PermissionableID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.PermissionableID,
		P.ControllerName,
		P.MethodName,
		P.PermissionCode,
		P.PermissionableGroupID,
		P.Description,
		P.IsActive,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.DisplayOrder
	FROM permissionable.Permissionable P
	WHERE P.PermissionableID = @PermissionableID
		
END
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.09
-- Description:	A stored procedure to get data from the permissionable.Permissionable table
-- ========================================================================================
CREATE PROCEDURE permissionable.GetPermissionables

@PersonID INT = 0,
@PermissionableTemplateID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.ControllerName,
		P.Description,
		P.DisplayOrder,
		P.IsGlobal,
		P.IsSuperAdministrator,
		P.MethodName,
		P.PermissionableLineage,
		P.PermissionCode,
		P.PermissionableID,
		PG.PermissionableGroupCode,
		PG.PermissionableGroupName,
		
		CASE
			WHEN @PersonID > 0 AND EXISTS (SELECT 1 FROM permissionable.PersonPermissionable PP WHERE PP.PersonID = @PersonID AND PP.PermissionableLineage = P.PermissionableLineage)
			THEN 1
			WHEN @PermissionableTemplateID > 0 AND EXISTS (SELECT 1 FROM permissionable.PermissionableTemplatePermissionable PTP WHERE PTP.PermissionableTemplateID = @PermissionableTemplateID AND PTP.PermissionableID = P.PermissionableID)
			THEN 1
			ELSE 0
		END AS HasPermissionable
						
	FROM permissionable.Permissionable P
		JOIN permissionable.PermissionableGroup PG ON PG.PermissionableGroupID = P.PermissionableGroupID
	ORDER BY PG.DisplayOrder, PG.PermissionableGroupName, P.DisplayOrder, P.PermissionableLineage
		
END
GO
--End procedure permissionable.GetPermissionables

--Begin procedure workflow.ResetEntityWorkflowStepGroupPerson
EXEC Utility.DropObject 'workflow.ResetEntityWorkflowStepGroupPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create date:	2016.05.06
-- Description:	A stored procedure to update already crated workflows with the current personnel assignments
-- =========================================================================================================
CREATE PROCEDURE workflow.ResetEntityWorkflowStepGroupPerson

@WorkflowID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tEntityWorkflowStepGroupPerson TABLE
		(
		EntityTypeCode VARCHAR(50),
		EntityID INT,
		WorkflowID INT,
		WorkflowStepID INT,
		WorkflowStepGroupID INT,
		WorkflowName VARCHAR(250),
		WorkflowStepNumber INT,
		WorkflowStepName VARCHAR(250),
		WorkflowStepGroupName VARCHAR(250),
		PersonID INT,
		IsComplete BIT
		)	
		
	;
	WITH WSGP AS
		(
		SELECT
			W.WorkflowID,  
			WS.WorkflowStepID, 
			WSG.WorkflowStepGroupID, 
			WSGP.PersonID
		FROM workflow.WorkflowStepGroupPerson WSGP 
			JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
			JOIN workflow.WorkflowStep WS ON WS.WorkflowStepID = WSG.WorkflowStepID
			JOIN workflow.Workflow W ON W.WorkflowID = WS.WorkflowID
				AND W.WorkflowID = @WorkflowID
		)
	
	INSERT INTO @tEntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
	SELECT
		D.EntityTypeCode, 
		D.EntityID, 
		D.WorkflowID, 
		D.WorkflowStepID, 
		D.WorkflowStepGroupID, 
		D.WorkflowName, 
		D.WorkflowStepNumber, 
		D.WorkflowStepName, 
		D.WorkflowStepGroupName, 
		E.PersonID,
		D.IsComplete
	FROM
		(
		SELECT
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsComplete
		FROM workflow.EntityWorkflowStepGroupPerson EWSGP
		WHERE EWSGP.WorkflowID = @WorkflowID
		GROUP BY
			EWSGP.EntityTypeCode, 
			EWSGP.EntityID, 
			EWSGP.WorkflowID, 
			EWSGP.WorkflowStepID, 
			EWSGP.WorkflowStepGroupID, 
			EWSGP.WorkflowName, 
			EWSGP.WorkflowStepNumber, 
			EWSGP.WorkflowStepName, 
			EWSGP.WorkflowStepGroupName, 
			EWSGP.IsComplete
		) D
		CROSS APPLY
			(
			SELECT
				WSGP.PersonID
			FROM WSGP
			WHERE WSGP.WorkflowID = D.WorkflowID
				AND WSGP.WorkflowStepID = D.WorkflowStepID
				AND WSGP.WorkflowStepGroupID = D.WorkflowStepGroupID
			) E
	ORDER BY 
		D.EntityID,
		D.WorkflowID,
		D.WorkflowStepID,
		D.WorkflowStepGroupID, 
		D.IsComplete

	DELETE EWSGP
	FROM workflow.EntityWorkflowStepGroupPerson EWSGP
	WHERE EWSGP.WorkflowID = @WorkflowID

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID, IsComplete)
	SELECT
		T.EntityTypeCode, 
		T.EntityID, 
		T.WorkflowID, 
		T.WorkflowStepID, 
		T.WorkflowStepGroupID, 
		T.WorkflowName, 
		T.WorkflowStepNumber, 
		T.WorkflowStepName, 
		T.WorkflowStepGroupName, 
		T.PersonID, 
		T.IsComplete
	FROM @tEntityWorkflowStepGroupPerson T

END
GO
--End procedure workflow.ResetEntityWorkflowStepGroupPerson

--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
USE AJACS
GO

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'ContactVetting', 'Contact Vetting'
GO
--End table dbo.EntityType

--Begin table dropdown.ContactAffiliation
UPDATE CA
SET CA.DisplayOrder = 0
FROM dropdown.ContactAffiliation CA
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Field Engineers')
	BEGIN

	INSERT INTO dropdown.ContactAffiliation
		(ContactAffiliationName)
	VALUES
		('Field Engineers'),
		('Field Monitors'),
		('Field Officers'),
		('Field Researchers'),
		('Finance Field Officers'),
		('International Associates'),
		('Local Associates'),
		('Local Staff'),
		('Logistic Field Officers')
	
	END
--ENDIF
GO
--Begin table dropdown.ContactAffiliation

--Begin table permissionable.PersonPermissionable
INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	P.PersonID,
	'Vetting.List.VettingOutcomeChangeNotification'
FROM dbo.ListToTable(dbo.GetServerSetupValueByServerSetupKey('VettingMailTo', ''), ',') LTT
	JOIN dbo.Person P ON P.EmailAddress = LTRIM(RTRIM(LTT.ListItem))
GO

DELETE D
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY PP.PersonID, PP.PermissionableLineage ORDER BY PP.PersonID, PP.PermissionableLineage) AS RowIndex,
		PP.PersonID,
		PP.PermissionableLineage
	FROM permissionable.PersonPermissionable PP
	) D
WHERE D.RowIndex > 1
GO
--End table permissionable.PersonPermissionable

--End file Build File - 04 - Data.sql

--Begin table permissionable.PermissionableGroup
TRUNCATE TABLE permissionable.PermissionableGroup
GO

EXEC utility.SavePermissionableGroup 'Administration', 'Administration', 0
GO
EXEC utility.SavePermissionableGroup 'Community', 'Community', 0
GO
EXEC utility.SavePermissionableGroup 'CommunityRound', 'CommunityRound', 0
GO
EXEC utility.SavePermissionableGroup 'Contact', 'Contact', 0
GO
EXEC utility.SavePermissionableGroup 'ContactVetting', 'Contact Vetting', 0
GO
EXEC utility.SavePermissionableGroup 'Documents', 'Documents', 0
GO
EXEC utility.SavePermissionableGroup 'DonorDecision', 'Donor Decision', 0
GO
EXEC utility.SavePermissionableGroup 'Equipment', 'Equipment', 0
GO
EXEC utility.SavePermissionableGroup 'Implementation', 'Implementation', 0
GO
EXEC utility.SavePermissionableGroup 'Research', 'Insight & Understanding', 0
GO
EXEC utility.SavePermissionableGroup 'LogicalFramework', 'Monitoring & Evaluation', 0
GO
EXEC utility.SavePermissionableGroup 'Operations', 'Operations & Implementation Support', 0
GO
EXEC utility.SavePermissionableGroup 'ProgramReports', 'Program Reports', 0
GO
EXEC utility.SavePermissionableGroup 'Province', 'Province', 0
GO
EXEC utility.SavePermissionableGroup 'RapidAssessments', 'Rapid Assessments', 0
GO
EXEC utility.SavePermissionableGroup 'Subcontractor', 'Subcontractors', 0
GO
EXEC utility.SavePermissionableGroup 'Training', 'Training', 0
GO
EXEC utility.SavePermissionableGroup 'Workflows', 'Workflows', 0
GO
--End table permissionable.PermissionableGroup

--Begin table permissionable.PermissionableTemplate
UPDATE PTP SET PTP.PermissionableLineage = P.PermissionableLineage FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableID = PTP.PermissionableID
GO
--End table permissionable.PermissionableTemplate

--Begin table permissionable.Permissionable
TRUNCATE TABLE permissionable.Permissionable
GO

EXEC utility.SavePermissionable 'DataExport', 'Default', NULL, 'DataExport.Default', 'Data Export', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'DataImport', 'Default', NULL, 'DataImport.Default', 'Data Import', 1, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'AddUpdate', NULL, 'EmailTemplateAdministration.AddUpdate', 'Add / edit an email template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'List', NULL, 'EmailTemplateAdministration.List', 'List EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EmailTemplateAdministration', 'View', NULL, 'EmailTemplateAdministration.View', 'View EmailTemplateAdministration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'List', NULL, 'EventLog.List', 'List EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'EventLog', 'View', NULL, 'EventLog.View', 'View EventLog', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Exports', 'BusinessLicenseReport', NULL, 'Exports.BusinessLicenseReport', 'Business License Report Exports', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Main', 'Error', 'ViewCFErrors', 'Main.Error.ViewCFErrors', 'View ColdFusion Errors SiteConfiguration', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'AddUpdate', NULL, 'Permissionable.AddUpdate', 'Add / edit a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'Delete', NULL, 'Permissionable.Delete', 'Delete a system permission', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Permissionable', 'List', NULL, 'Permissionable.List', 'View the list of system permissions', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'AddUpdate', NULL, 'PermissionableTemplate.AddUpdate', 'Add / edit a permissionable template', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'List', NULL, 'PermissionableTemplate.List', 'List PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'PermissionableTemplate', 'View', NULL, 'PermissionableTemplate.View', 'View PermissionableTemplate', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'AddUpdate', NULL, 'Person.AddUpdate', 'Add / edit a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'Delete', NULL, 'Person.Delete', 'Delete a person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'List', NULL, 'Person.List', 'List Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Person', 'View', NULL, 'Person.View', 'View Person', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'AddUpdate', NULL, 'ServerSetup.AddUpdate', 'Add / edit a server setup key', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'ServerSetup', 'List', NULL, 'ServerSetup.List', 'List the server setup keys', 0, 0, 'Administration'
GO
EXEC utility.SavePermissionable 'Community', 'AddUpdate', NULL, 'Community.AddUpdate', 'Add / edit a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'List', NULL, 'Community.List', 'List Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', NULL, 'Community.View', 'View Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Analysis', 'Community.View.Analysis', 'View the analysis tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Export', 'Community.View.Export', 'Export Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'ExportEquipmentDistribution', 'Community.View.ExportEquipmentDistribution', 'Export Equipment Distributions Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Implementation', 'Community.View.Implementation', 'Implementation Community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'Community', 'View', 'Information', 'Community.View.Information', 'View the information tab for a community', 0, 0, 'Community'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'AddUpdate', NULL, 'CommunityRound.AddUpdate', 'CommunityRound.AddUpdate', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'List', NULL, 'CommunityRound.List', 'CommunityRound.List', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'CommunityRound', 'View', NULL, 'CommunityRound.View', 'CommunityRound.View', 0, 0, 'CommunityRound'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'CETeam', 'Contact.AddUpdate.CETeam', 'Add / edit contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'FieldStaff', 'Contact.AddUpdate.FieldStaff', 'Add / edit contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'IO4', 'Contact.AddUpdate.IO4', 'Add / edit contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeOther', 'Contact.AddUpdate.JusticeOther', 'Add / edit contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'JusticeStipend', 'Contact.AddUpdate.JusticeStipend', 'Add / edit contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersStakeholder', 'Contact.AddUpdate.PartnersStakeholder', 'Add / edit contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PartnersSupplierVendor', 'Contact.AddUpdate.PartnersSupplierVendor', 'Add / edit contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceOther', 'Contact.AddUpdate.PoliceOther', 'Add / edit contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'PoliceStipend', 'Contact.AddUpdate.PoliceStipend', 'Add / edit contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'ProjectStaff', 'Contact.AddUpdate.ProjectStaff', 'Add / edit contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'AddUpdate', 'SubContractors', 'Contact.AddUpdate.SubContractors', 'Add / edit contacts of type Sub-Contractor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'CETeam', 'Contact.List.CETeam', 'Include contacts of type CE Team in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ExportPayees', 'Contact.List.ExportPayees', 'Export payees from the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'FieldStaff', 'Contact.List.FieldStaff', 'Include contacts of type Field Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'IO4', 'Contact.List.IO4', 'Include contacts of type IO4 in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeOther', 'Contact.List.JusticeOther', 'Include contacts of type Justice Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'JusticeStipend', 'Contact.List.JusticeStipend', 'Include contacts of type Justice Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersStakeholder', 'Contact.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PartnersSupplierVendor', 'Contact.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceOther', 'Contact.List.PoliceOther', 'Include contacts of type Police Other in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'PoliceStipend', 'Contact.List.PoliceStipend', 'Include contacts of type Police Stipend in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'ProjectStaff', 'Contact.List.ProjectStaff', 'Include contacts of type Project Staff in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'List', 'SubContractors', 'Contact.List.SubContractors', 'Include contacts of type Sub-Contractor in the contact list', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', NULL, 'Contact.PaymentList', 'View the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'CashHandOverExport', 'Contact.PaymentList.CashHandOverExport', 'Export the cash handover report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'OpFundsReport', 'Contact.PaymentList.OpFundsReport', 'Export the op funds report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendActivity', 'Contact.PaymentList.StipendActivity', 'Export the stipend activity report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'PaymentList', 'StipendPaymentReport', 'Contact.PaymentList.StipendPaymentReport', 'Export the stipend payment report from the payment List', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'CETeam', 'Contact.View.CETeam', 'View contacts of type CE Team', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'FieldStaff', 'Contact.View.FieldStaff', 'View contacts of type Field Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'IO4', 'Contact.View.IO4', 'View contacts of type IO4', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeOther', 'Contact.View.JusticeOther', 'View contacts of type Justice Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'JusticeStipend', 'Contact.View.JusticeStipend', 'View contacts of type Justice Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersStakeholder', 'Contact.View.PartnersStakeholder', 'View contacts of type Partners: Stakeholder', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PartnersSupplierVendor', 'Contact.View.PartnersSupplierVendor', 'View contacts of type Partners: Supplier/Vendor', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceOther', 'Contact.View.PoliceOther', 'View contacts of type Police Other', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'PoliceStipend', 'Contact.View.PoliceStipend', 'View contacts of type Police Stipend', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'ProjectStaff', 'Contact.View.ProjectStaff', 'View contacts of type Project Staff', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'SubContractors', 'Contact.View.SubContractors', 'View contacts of type Sub-Contractors', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Contact', 'View', 'VettingMoreInfo', 'Contact.View.VettingMoreInfo', 'View the more info button on the vetting history data table', 0, 0, 'Contact'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'CETeam', 'Vetting.List.CETeam', 'Include contacts of type CE Team in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'Export', 'Vetting.List.Export', 'Export the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'FieldStaff', 'Vetting.List.FieldStaff', 'Include contacts of type Field Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'IO4', 'Vetting.List.IO4', 'Include contacts of type IO4 in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeOther', 'Vetting.List.JusticeOther', 'Include contacts of type Justice Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'JusticeStipend', 'Vetting.List.JusticeStipend', 'Include contacts of type Justice Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersStakeholder', 'Vetting.List.PartnersStakeholder', 'Include contacts of type Partners: Stakeholder in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PartnersSupplierVendor', 'Vetting.List.PartnersSupplierVendor', 'Include contacts of type Partners: Supplier/Vendor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceOther', 'Vetting.List.PoliceOther', 'Include contacts of type Police Other in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'PoliceStipend', 'Vetting.List.PoliceStipend', 'Include contacts of type Police Stipend in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'ProjectStaff', 'Vetting.List.ProjectStaff', 'Include contacts of type Project Staff in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'SubContractors', 'Vetting.List.SubContractors', 'Include contacts of type Sub-Contractor in the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeChangeNotification', 'Vetting.List.VettingOutcomeChangeNotification', 'Recieve an email when a vetting outcome has changed for one or more contacts', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeConsider', 'Vetting.List.VettingOutcomeConsider', 'Assign a vetting outcome of "Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeDoNotConsider', 'Vetting.List.VettingOutcomeDoNotConsider', 'Assign a vetting outcome of "Do Not Consider"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeInsufficientData', 'Vetting.List.VettingOutcomeInsufficientData', 'Assign a vetting outcome of "Insufficient Data"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeNotVetted', 'Vetting.List.VettingOutcomeNotVetted', 'Assign a vetting outcome of "Not Vetted"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomePendingInternalReview', 'Vetting.List.VettingOutcomePendingInternalReview', 'Assign a vetting outcome of "Pending Internal Review"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingOutcomeSubmittedforVetting', 'Vetting.List.VettingOutcomeSubmittedforVetting', 'Assign a vetting outcome of "Submitted for Vetting"', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUK', 'Vetting.List.VettingTypeUK', 'Update UK vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'List', 'VettingTypeUS', 'Vetting.List.VettingTypeUS', 'Update US vetting data on the vetting list', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Vetting', 'Notification', 'ExpirationCountEmail', 'Vetting.Notification.ExpirationCountEmail', 'Recieve the monthly vetting expiration counts e-mail', 0, 0, 'ContactVetting'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', NULL, 'Document.AddUpdate', '', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '004', 'Document.AddUpdate.004', 'Add / edit 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '100', 'Document.AddUpdate.100', 'Add / edit 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '101', 'Document.AddUpdate.101', 'Add / edit 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '102', 'Document.AddUpdate.102', 'Add / edit 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '103', 'Document.AddUpdate.103', 'Add / edit 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '104', 'Document.AddUpdate.104', 'Add / edit 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '105', 'Document.AddUpdate.105', 'Add / edit 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '106', 'Document.AddUpdate.106', 'Add / edit 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '107', 'Document.AddUpdate.107', 'Add / edit 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '108', 'Document.AddUpdate.108', 'Add / edit 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '109', 'Document.AddUpdate.109', 'Add / edit 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '301', 'Document.AddUpdate.301', 'Add / edit 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '302', 'Document.AddUpdate.302', 'Add / edit 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '303', 'Document.AddUpdate.303', 'Add / edit 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '304', 'Document.AddUpdate.304', 'Add / edit 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '306', 'Document.AddUpdate.306', 'Add / edit 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '500', 'Document.AddUpdate.500', 'Add / edit 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '501', 'Document.AddUpdate.501', 'Add / edit 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '502', 'Document.AddUpdate.502', 'Add / edit 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '503', 'Document.AddUpdate.503', 'Add / edit 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '504', 'Document.AddUpdate.504', 'Add / edit 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '505', 'Document.AddUpdate.505', 'Add / edit 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '506', 'Document.AddUpdate.506', 'Add / edit 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '507', 'Document.AddUpdate.507', 'Add / edit 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '508', 'Document.AddUpdate.508', 'Add / edit 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '509', 'Document.AddUpdate.509', 'Add / edit 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '510', 'Document.AddUpdate.510', 'Add / edit 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '511', 'Document.AddUpdate.511', 'Add / edit 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '512', 'Document.AddUpdate.512', 'Add / edit 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '513', 'Document.AddUpdate.513', 'Add / edit 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '514', 'Document.AddUpdate.514', 'Add / edit 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '515', 'Document.AddUpdate.515', 'Add / edit 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '516', 'Document.AddUpdate.516', 'Add / edit 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '517', 'Document.AddUpdate.517', 'Add / edit 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '518', 'Document.AddUpdate.518', 'Add / edit 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '519', 'Document.AddUpdate.519', 'Add / edit 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '520', 'Document.AddUpdate.520', 'Add / edit 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '521', 'Document.AddUpdate.521', 'Add / edit 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '600', 'Document.AddUpdate.600', 'Add / edit 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '601', 'Document.AddUpdate.601', 'Add / edit 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '602', 'Document.AddUpdate.602', 'Add / edit 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '603', 'Document.AddUpdate.603', 'Add / edit 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '604', 'Document.AddUpdate.604', 'Add / edit 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '605', 'Document.AddUpdate.605', 'Add / edit 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '700', 'Document.AddUpdate.700', 'Add / edit 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '701', 'Document.AddUpdate.701', 'Add / edit 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '702', 'Document.AddUpdate.702', 'Add / edit 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '801', 'Document.AddUpdate.801', 'Add / edit 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '802', 'Document.AddUpdate.802', 'Add / edit 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '803', 'Document.AddUpdate.803', 'Add / edit 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '804', 'Document.AddUpdate.804', 'Add / edit 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '805', 'Document.AddUpdate.805', 'Add / edit 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '806', 'Document.AddUpdate.806', 'Add / edit 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '807', 'Document.AddUpdate.807', 'Add / edit 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '808', 'Document.AddUpdate.808', 'Add / edit 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '900', 'Document.AddUpdate.900', 'Add / edit 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '901', 'Document.AddUpdate.901', 'Add / edit 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '902', 'Document.AddUpdate.902', 'Add / edit 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '903', 'Document.AddUpdate.903', 'Add / edit 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '904', 'Document.AddUpdate.904', 'Add / edit 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '905', 'Document.AddUpdate.905', 'Add / edit 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '906', 'Document.AddUpdate.906', 'Add / edit 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '907', 'Document.AddUpdate.907', 'Add / edit 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '908', 'Document.AddUpdate.908', 'Add / edit 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '909', 'Document.AddUpdate.909', 'Add / edit 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'AddUpdate', '910', 'Document.AddUpdate.910', 'Add / edit 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'List', NULL, 'Document.List', 'View the document library', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '004', 'Document.View.004', 'View 004 Branding and Marking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '100', 'Document.View.100', 'View 100 Client Requests and Approvals', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '101', 'Document.View.101', 'View 101 Internal Admin Correspondence', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '102', 'Document.View.102', 'View 102 Office and Residence Leases', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '103', 'Document.View.103', 'View 103 Various Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '104', 'Document.View.104', 'View 104 Hotels Reservations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '105', 'Document.View.105', 'View 105 Project Insurance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '106', 'Document.View.106', 'View 106 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '107', 'Document.View.107', 'View 107 Contact List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '108', 'Document.View.108', 'View 108 Translations', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '109', 'Document.View.109', 'View 109 IT Technical Info', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '301', 'Document.View.301', 'View 301 Project Inventory List', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '302', 'Document.View.302', 'View 302 Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '303', 'Document.View.303', 'View 303 Shipping Forms and Customs Docs', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '304', 'Document.View.304', 'View 304 Waivers', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '306', 'Document.View.306', 'View 306 Commodities Tracking', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '500', 'Document.View.500', 'View 500 RFP for Project', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '501', 'Document.View.501', 'View 501 Technical Proposal and Budget', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '502', 'Document.View.502', 'View 502 Agreements and Mods', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '503', 'Document.View.503', 'View 503 Work Plans and Budgets', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '504', 'Document.View.504', 'View 504 Meeting Notes', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '505', 'Document.View.505', 'View 505 Trip Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '506', 'Document.View.506', 'View 506 Quarterly Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '507', 'Document.View.507', 'View 507 Annual Reports', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '508', 'Document.View.508', 'View 508 M&E Plan', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '509', 'Document.View.509', 'View 509 M&E Reporting', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '510', 'Document.View.510', 'View 510 Additional Reports and Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '511', 'Document.View.511', 'View 511 Additional Atmospheric', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '512', 'Document.View.512', 'View 512 Contact Stipend Payment Reconcilliation', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '513', 'Document.View.513', 'View 513 Critical Assessment', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '514', 'Document.View.514', 'View 514 Daily Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '515', 'Document.View.515', 'View 515 Provincial Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '516', 'Document.View.516', 'View 516 RFI Response', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '517', 'Document.View.517', 'View 517 Spot Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '518', 'Document.View.518', 'View 518 Syria Weekly Information Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '519', 'Document.View.519', 'View 519 Weekly Atmospheric Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '520', 'Document.View.520', 'View 520 Weekly Program Report', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '521', 'Document.View.521', 'View 521 Other Document', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '600', 'Document.View.600', 'View 600 Project Org Chart', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '601', 'Document.View.601', 'View 601 Community Engagement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '602', 'Document.View.602', 'View 602 Justice', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '603', 'Document.View.603', 'View 603 M&E', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '604', 'Document.View.604', 'View 604 Policing', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '605', 'Document.View.605', 'View 605 Research', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '700', 'Document.View.700', 'View 700 Activities Manual', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '701', 'Document.View.701', 'View 701 Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '702', 'Document.View.702', 'View 702 Activity Management ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '801', 'Document.View.801', 'View 801 SI Activities', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '802', 'Document.View.802', 'View 802 SI Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '803', 'Document.View.803', 'View 803 SI Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '804', 'Document.View.804', 'View 804 SI General Deliverables', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '805', 'Document.View.805', 'View 805 SI Human Resources', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '806', 'Document.View.806', 'View 806 SI Inventory and Procurement', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '807', 'Document.View.807', 'View 807 SI Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '808', 'Document.View.808', 'View 808 SI Project Technical', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '900', 'Document.View.900', 'View 900 Start-Up', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '901', 'Document.View.901', 'View 901 HR ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '902', 'Document.View.902', 'View 902 Procurement ', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '903', 'Document.View.903', 'View 903 Finance', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '904', 'Document.View.904', 'View 904 Contracts', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '905', 'Document.View.905', 'View 905 Activity Management', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '906', 'Document.View.906', 'View 906 IT', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '907', 'Document.View.907', 'View 907 Security', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '908', 'Document.View.908', 'View 908 Communications', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '909', 'Document.View.909', 'View 909 Project Admin', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'Document', 'View', '910', 'Document.View.910', 'View 910 Closeout', 0, 0, 'Documents'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateDecision', NULL, 'DonorDecision.AddUpdateDecision', 'Add / edit a donor decision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'AddUpdateMeeting', NULL, 'DonorDecision.AddUpdateMeeting', 'Add / edit donor meetings & actions', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'List', NULL, 'DonorDecision.List', 'List Donor Decisions, Meetings & Actions DonorDecision', 1, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'DonorDecision', 'View', NULL, 'DonorDecision.View', 'View DonorDecision', 0, 0, 'DonorDecision'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'AddUpdate', NULL, 'EquipmentCatalog.AddUpdate', 'Add / edit the equipment catalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'List', NULL, 'EquipmentCatalog.List', 'List EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentCatalog', 'View', NULL, 'EquipmentCatalog.View', 'View EquipmentCatalog', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Audit', NULL, 'EquipmentDistribution.Audit', 'EquipmentDistribution.Audit', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Create', NULL, 'EquipmentDistribution.Create', 'EquipmentDistribution.Create', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'FinalizeEquipmentDistribution', NULL, 'EquipmentDistribution.FinalizeEquipmentDistribution', 'EquipmentDistribution.FinalizeEquipmentDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistributedInventory', NULL, 'EquipmentDistribution.ListDistributedInventory', 'EquipmentDistribution.ListDistributedInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListDistribution', NULL, 'EquipmentDistribution.ListDistribution', 'EquipmentDistribution.ListDistribution', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'ListInventory', NULL, 'EquipmentDistribution.ListInventory', 'EquipmentDistribution.ListInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'SetDeliveryDate', NULL, 'EquipmentDistribution.SetDeliveryDate', 'EquipmentDistribution.SetDeliveryDate', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistribution', 'Transfer', NULL, 'EquipmentDistribution.Transfer', 'EquipmentDistribution.Transfer', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'AddUpdate', NULL, 'EquipmentDistributionPlan.AddUpdate', 'Add / edit an equipment distribution plan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'List', NULL, 'EquipmentDistributionPlan.List', 'List EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', NULL, 'EquipmentDistributionPlan.View', 'View EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', 'EquipmentDistributionPlan', 'EquipmentDistributionPlan.View.EquipmentDistributionPlan', 'Export Equipment Distribution Plan EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentDistributionPlan', 'View', 'EquipmentHandoverSheet', 'EquipmentDistributionPlan.View.EquipmentHandoverSheet', 'Export Equipment Handover Sheet EquipmentDistributionPlan', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'AddUpdate', NULL, 'EquipmentInventory.AddUpdate', 'Add / edit the equipment inventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', NULL, 'EquipmentInventory.List', 'List EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'List', 'Export', 'EquipmentInventory.List.Export', 'Export EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentInventory', 'View', NULL, 'EquipmentInventory.View', 'View EquipmentInventory', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'Audit', NULL, 'EquipmentManagement.Audit', 'Audit Equipment EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'EquipmentManagement', 'List', NULL, 'EquipmentManagement.List', 'List Equipment Locations EquipmentManagement', 0, 0, 'Equipment'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'AddUpdate', NULL, 'CommunityAsset.AddUpdate', 'Add / edit a community asset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'List', NULL, 'CommunityAsset.List', 'List CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityAsset', 'View', NULL, 'CommunityAsset.View', 'View CommunityAsset', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'AddUpdate', NULL, 'CommunityMemberSurvey.AddUpdate', 'Add / edit a community member survey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityMemberSurvey', 'View', NULL, 'CommunityMemberSurvey.View', 'View CommunityMemberSurvey', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagement', 'AddUpdate', NULL, 'CommunityProvinceEngagement.AddUpdate', 'Add / edit a community province engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'CommunityProvinceEngagementUpdate', 'Export', NULL, 'CommunityProvinceEngagementUpdate.Export', 'Export CommunityProvinceEngagementUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'FIFUpdate', 'View', NULL, 'FIFUpdate.View', 'View FIFUpdate', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Justice', 'AddUpdate', NULL, 'Justice.AddUpdate', 'Add / edit a justice report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'AddUpdate', NULL, 'KeyEvent.AddUpdate', 'Add / edit a key event', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'List', NULL, 'KeyEvent.List', 'List KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'KeyEvent', 'View', NULL, 'KeyEvent.View', 'View KeyEvent', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'PoliceEngagement', 'AddUpdate', NULL, 'PoliceEngagement.AddUpdate', 'Add / edit a police engagement report', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'AddUpdate', NULL, 'Project.AddUpdate', 'Add / edit a project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'List', NULL, 'Project.List', 'List Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Project', 'View', NULL, 'Project.View', 'View Project', 0, 0, 'Implementation'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'AddUpdate', NULL, 'Atmospheric.AddUpdate', 'Add / edit an atmospheric report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'List', NULL, 'Atmospheric.List', 'List Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Atmospheric', 'View', NULL, 'Atmospheric.View', 'View Atmospheric', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'AddUpdate', NULL, 'Finding.AddUpdate', 'Add / edit a finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'List', NULL, 'Finding.List', 'List Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Finding', 'View', NULL, 'Finding.View', 'View Finding', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'AddUpdate', NULL, 'Force.AddUpdate', 'Add / edit a force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'List', NULL, 'Force.List', 'List Forces Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Force', 'View', NULL, 'Force.View', 'View a force Force', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'AddUpdate', NULL, 'Incident.AddUpdate', 'Add / edit an incident report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'List', NULL, 'Incident.List', 'List Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Incident', 'View', NULL, 'Incident.View', 'View Incident', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'AddUpdate', NULL, 'Recommendation.AddUpdate', 'Add / edit a recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', NULL, 'Recommendation.List', 'List Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'List', 'Export', 'Recommendation.List.Export', 'Export Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Recommendation', 'View', NULL, 'Recommendation.View', 'View Recommendation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Add', NULL, 'RequestForInformation.Add', 'Add a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', NULL, 'RequestForInformation.List', 'List RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'List', 'Export', 'RequestForInformation.List.Export', 'Export RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', NULL, 'RequestForInformation.Update', 'Edit a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'Update', 'Amend', 'RequestForInformation.Update.Amend', 'Amend a request for information', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'RequestForInformation', 'View', NULL, 'RequestForInformation.View', 'View RequestForInformation', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'AddUpdate', NULL, 'Risk.AddUpdate', 'Add / edit a risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', NULL, 'Risk.List', 'List Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'List', 'Export', 'Risk.List.Export', 'Export Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Risk', 'View', NULL, 'Risk.View', 'View Risk', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', NULL, 'SpotReport.AddUpdate', 'Add / edit a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'AddUpdate', 'Amend', 'SpotReport.AddUpdate.Amend', 'Amend a spot report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'List', NULL, 'SpotReport.List', 'List SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', NULL, 'SpotReport.View', 'View SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Approved', 'SpotReport.View.Approved', 'Approved SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'Export', 'SpotReport.View.Export', 'Export SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SpotReport', 'View', 'InWork', 'SpotReport.View.InWork', 'In Work SpotReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateQuestion', NULL, 'SurveyManagement.AddUpdateQuestion', 'Add / edit a survey question', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AddUpdateSurvey', NULL, 'SurveyManagement.AddUpdateSurvey', 'Add / edit a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'AdministerSurvey', NULL, 'SurveyManagement.AdministerSurvey', 'Administer a survey', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ExportSurveyResponses', NULL, 'SurveyManagement.ExportSurveyResponses', 'Export Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListQuestions', NULL, 'SurveyManagement.ListQuestions', 'Questions List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveyResponses', NULL, 'SurveyManagement.ListSurveyResponses', 'List Survey Responses SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ListSurveys', NULL, 'SurveyManagement.ListSurveys', 'Surveys List SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewQuestion', NULL, 'SurveyManagement.ViewQuestion', 'View Questions SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'SurveyManagement', 'ViewSurvey', NULL, 'SurveyManagement.ViewSurvey', 'View Surveys SurveyManagement', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', NULL, 'WeeklyReport.AddUpdate', 'Add / edit a weekly report', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'AddUpdate', 'Export', 'WeeklyReport.AddUpdate.Export', 'Export WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'List', NULL, 'WeeklyReport.List', 'List WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'WeeklyReport', 'View', NULL, 'WeeklyReport.View', 'View WeeklyReport', 0, 0, 'Research'
GO
EXEC utility.SavePermissionable 'Indicator', 'AddUpdate', NULL, 'Indicator.AddUpdate', 'Add / edit an indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'List', NULL, 'Indicator.List', 'List Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Indicator', 'View', NULL, 'Indicator.View', 'View Indicator', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'AddUpdate', NULL, 'IndicatorType.AddUpdate', 'Add / edit an indicatortype', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'List', NULL, 'IndicatorType.List', 'List IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'IndicatorType', 'View', NULL, 'IndicatorType.View', 'View IndicatorType', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'AddUpdate', NULL, 'Milestone.AddUpdate', 'Add / edit a milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'List', NULL, 'Milestone.List', 'List Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Milestone', 'View', NULL, 'Milestone.View', 'View Milestone', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'AddUpdate', NULL, 'Objective.AddUpdate', 'Add / edit an objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'ChartList', NULL, 'Objective.ChartList', 'M & E Overview Charts Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'List', NULL, 'Objective.List', 'List Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Manage', NULL, 'Objective.Manage', 'Manage Objectives & Indicators Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'Overview', NULL, 'Objective.Overview', 'Overview Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'Objective', 'View', NULL, 'Objective.View', 'View Objective', 0, 0, 'LogicalFramework'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', NULL, 'ConceptNote.AddUpdate', 'Add / edit a concep nNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'ExportConceptNoteBudget', 'ConceptNote.AddUpdate.ExportConceptNoteBudget', 'Export Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'AddUpdate', 'Finances', 'ConceptNote.AddUpdate.Finances', 'Add / edit activity finances', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', NULL, 'ConceptNote.List', 'List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'List', 'Export', 'ConceptNote.List.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', NULL, 'ConceptNote.VettingList', 'Vetting List ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'VettingList', 'ExportVetting', 'ConceptNote.VettingList.ExportVetting', 'Export Vetting ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', NULL, 'ConceptNote.View', 'View ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'Export', 'ConceptNote.View.Export', 'Export ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNote', 'View', 'ViewBudget', 'ConceptNote.View.ViewBudget', 'View Budget ConceptNote', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'AddUpdate', NULL, 'ConceptNoteContactEquipment.AddUpdate', 'Add / edit equipment associated with a concept note', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'FinalizeEquipmentDistribution', NULL, 'ConceptNoteContactEquipment.FinalizeEquipmentDistribution', 'Finalize Equipment Distribution ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'List', NULL, 'ConceptNoteContactEquipment.List', 'List ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', NULL, 'ConceptNoteContactEquipment.View', 'View ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ConceptNoteContactEquipment', 'View', 'Export', 'ConceptNoteContactEquipment.View.Export', 'Export ConceptNoteContactEquipment', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'AddUpdate', NULL, 'License.AddUpdate', 'Add / edit a license', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'List', NULL, 'License.List', 'List License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'License', 'View', NULL, 'License.View', 'View License', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'AddUpdate', NULL, 'LicenseEquipmentCatalog.AddUpdate', 'Add / edit the license equipment catalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', NULL, 'LicenseEquipmentCatalog.List', 'List LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'List', 'Export', 'LicenseEquipmentCatalog.List.Export', 'Export LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'LicenseEquipmentCatalog', 'View', NULL, 'LicenseEquipmentCatalog.View', 'View LicenseEquipmentCatalog', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'AddUpdate', NULL, 'PurchaseRequest.AddUpdate', 'Add / edit a purchase request', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'List', NULL, 'PurchaseRequest.List', 'List PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', NULL, 'PurchaseRequest.View', 'View PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'PurchaseRequest', 'View', 'Export', 'PurchaseRequest.View.Export', 'Export PurchaseRequest', 0, 0, 'Operations'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'AddUpdate', NULL, 'ProgramReport.AddUpdate', 'Add / edit a program report', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'List', NULL, 'ProgramReport.List', 'List ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', NULL, 'ProgramReport.View', 'View ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'ProgramReport', 'View', 'Export', 'ProgramReport.View.Export', 'Export ProgramReport', 0, 0, 'ProgramReports'
GO
EXEC utility.SavePermissionable 'Province', 'AddUpdate', NULL, 'Province.AddUpdate', 'Add / edit a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'List', NULL, 'Province.List', 'List Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', NULL, 'Province.View', 'View Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Analysis', 'Province.View.Analysis', 'View the analysis tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'ExportEquipmentDistribution', 'Province.View.ExportEquipmentDistribution', 'Export Equipment Distributions Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Implementation', 'Province.View.Implementation', 'Implementation Province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'Province', 'View', 'Information', 'Province.View.Information', 'View the information tab for a province', 0, 0, 'Province'
GO
EXEC utility.SavePermissionable 'DailyReport', 'AddUpdate', NULL, 'DailyReport.AddUpdate', 'Add / edit a daily report', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'List', NULL, 'DailyReport.List', 'List DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'DailyReport', 'View', NULL, 'DailyReport.View', 'View DailyReport', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'AddUpdate', NULL, 'FocusGroupSurvey.AddUpdate', 'Add / edit a focus group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'FocusGroupSurvey', 'View', NULL, 'FocusGroupSurvey.View', 'View FocusGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'AddUpdate', NULL, 'KeyInformantSurvey.AddUpdate', 'Add / edit a key informant survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'KeyInformantSurvey', 'View', NULL, 'KeyInformantSurvey.View', 'View KeyInformantSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', NULL, 'RAPData.List', 'List RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RAPData', 'List', 'Export', 'RAPData.List.Export', 'Export RAPData', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'RapidPerceptionSurvey', 'View', NULL, 'RapidPerceptionSurvey.View', 'View RapidPerceptionSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'AddUpdate', NULL, 'StakeholderGroupSurvey.AddUpdate', 'Add / edit a stakeholder group survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StakeholderGroupSurvey', 'View', NULL, 'StakeholderGroupSurvey.View', 'View StakeholderGroupSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'AddUpdate', NULL, 'StationCommanderSurvey.AddUpdate', 'Add / edit a station commander survey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'StationCommanderSurvey', 'View', NULL, 'StationCommanderSurvey.View', 'View StationCommanderSurvey', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'AddUpdate', NULL, 'Team.AddUpdate', 'Add / edit a team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'List', NULL, 'Team.List', 'List Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'Team', 'View', NULL, 'Team.View', 'View Team', 0, 0, 'RapidAssessments'
GO
EXEC utility.SavePermissionable 'SubContractor', 'AddUpdate', NULL, 'SubContractor.AddUpdate', 'Add / edit a sub-contractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'List', NULL, 'SubContractor.List', 'List SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'SubContractor', 'View', NULL, 'SubContractor.View', 'View SubContractor', 0, 0, 'Subcontractor'
GO
EXEC utility.SavePermissionable 'Class', 'AddUpdate', NULL, 'Class.AddUpdate', 'Add / edit a class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'List', NULL, 'Class.List', 'List Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Class', 'View', NULL, 'Class.View', 'View Class', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'AddUpdate', NULL, 'Course.AddUpdate', 'Add / edit a course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'List', NULL, 'Course.List', 'List Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Course', 'View', NULL, 'Course.View', 'View Course', 0, 0, 'Training'
GO
EXEC utility.SavePermissionable 'Workflow', 'AddUpdate', NULL, 'Workflow.AddUpdate', 'Add / edit a workflow', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'List', NULL, 'Workflow.List', 'View the list of workflows', 0, 0, 'Workflows'
GO
EXEC utility.SavePermissionable 'Workflow', 'View', NULL, 'Workflow.View', 'View a workflow', 0, 0, 'Workflows'
GO
--End table permissionable.Permissionable

--Begin table person.PersonPermissionable
EXEC utility.UpdateSuperAdministratorPersonPermissionables
GO
--End table person.PersonPermissionable

--Begin table dbo.MenuItemPermissionableLineage
DELETE MIPL FROM dbo.MenuItemPermissionableLineage MIPL WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = MIPL.PermissionableLineage)
GO--End table dbo.MenuItemPermissionableLineage

--Begin table permissionable.PersonPermissionable
DELETE PP FROM permissionable.PersonPermissionable PP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PP.PermissionableLineage)
GO--End table permissionable.PersonPermissionable

--Begin table permissionable.PermissionableTemplate
DELETE PTP FROM permissionable.PermissionableTemplatePermissionable PTP WHERE NOT EXISTS (SELECT 1 FROM permissionable.Permissionable P WHERE P.PermissionableLineage = PTP.PermissionableLineage)
GO

UPDATE PTP SET PTP.PermissionableID = P.PermissionableID FROM permissionable.PermissionableTemplatePermissionable PTP JOIN permissionable.Permissionable P ON P.PermissionableLineage = PTP.PermissionableLineage
GO
--End table permissionable.PermissionableTemplate

--Begin build tracking
INSERT INTO Utility.BuildLog (BuildKey) VALUES ('Build-1.57 File 01 - AJACS - 2016.05.13 21.53.25')
GO
--End build tracking

