USE AJACS
GO

--Begin function dbo.GetEventNameByEventCode
EXEC utility.DropObject 'dbo.GetEventNameByEventCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.27
-- Description:	A function to return an EventCodeName from an EventCode
--
-- Author:			Todd Pires
-- Create date:	2015.04.30
-- Description:	Added failedlogin support
-- ====================================================================
CREATE FUNCTION dbo.GetEventNameByEventCode
(
@EventCode VARCHAR(50)
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @EventCodeName VARCHAR(50)

	SELECT @EventCodeName = 
		CASE
			WHEN @EventCode = 'cancelworkflow'
			THEN 'Workflow - Cancel'
			WHEN @EventCode = 'create'
			THEN 'Create'
			WHEN @EventCode = 'decrementworkflow'
			THEN 'Workflow - Disapprove'
			WHEN @EventCode = 'delete'
			THEN 'Delete'
			WHEN @EventCode = 'failedlogin'
			THEN 'Failed Login'
			WHEN @EventCode = 'finalize'
			THEN 'Finalize'
			WHEN @EventCode = 'holdworkflow'
			THEN 'Workflow - Hold'
			WHEN @EventCode = 'incrementworkflow'
			THEN 'Workflow - Approve'
			WHEN @EventCode = 'list'
			THEN 'List'
			WHEN @EventCode = 'login'
			THEN 'Login'
			WHEN @EventCode = 'read'
			THEN 'View'
			WHEN @EventCode = 'rerelease'
			THEN 'Re-release'
			WHEN @EventCode = 'save'
			THEN 'Save'
			WHEN @EventCode = 'unholdworkflow'
			THEN 'Workflow - Unhold'
			WHEN @EventCode = 'update'
			THEN 'Update'
			WHEN @EventCode = 'vettingupdate'
			THEN 'Vetting Update'
			ELSE @EventCode + ' not found'
		END

	RETURN @EventCodeName

END
GO
--End function dbo.GetEventNameByEventCode

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T ORDER BY T.CommunityProvinceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'FIFUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM fifupdate.FIFUpdate T ORDER BY T.FIFUpdateID DESC
		ELSE IF @EntityTypeCode = 'JusticeUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM justiceupdate.JusticeUpdate T ORDER BY T.JusticeUpdateID DESC
		ELSE IF @EntityTypeCode = 'PoliceEngagementUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM policeengagementupdate.PoliceEngagementUpdate T ORDER BY T.PoliceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM RiskUpdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport'
			SELECT TOP 1 @nWorkflowStepNumber = T.WorkflowStepNumber FROM WeeklyReport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
		
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = @nWorkflowStepNumber
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE IF @EntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate