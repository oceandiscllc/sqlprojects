USE AJACS
GO

--Begin table dropdown.VettingOutcome
DECLARE @TableName VARCHAR(250) = 'dropdown.VettingOutcome'

EXEC utility.AddColumn @TableName, 'VettingOutcomeCode', 'VARCHAR(50)'
GO

UPDATE VO
SET VO.VettingOutcomeCode = REPLACE(VO.VettingOutcomeName, ' ', '')
FROM dropdown.VettingOutcome VO
GO
--End table dropdown.VettingOutcome

--Begin table permissionable.Permissionable
DECLARE @TableName VARCHAR(250) = 'permissionable.Permissionable'

EXEC utility.AddColumn @TableName, 'IsSuperAdministrator', 'BIT'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', 0
GO
--End table permissionable.Permissionable
