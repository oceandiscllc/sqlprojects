USE AJACS
GO

--Begin table dbo.EntityType
EXEC utility.EntityTypeAddUpdate 'ContactVetting', 'Contact Vetting'
GO
--End table dbo.EntityType

--Begin table dropdown.ContactAffiliation
UPDATE CA
SET CA.DisplayOrder = 0
FROM dropdown.ContactAffiliation CA
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.ContactAffiliation CA WHERE CA.ContactAffiliationName = 'Field Engineers')
	BEGIN

	INSERT INTO dropdown.ContactAffiliation
		(ContactAffiliationName)
	VALUES
		('Field Engineers'),
		('Field Monitors'),
		('Field Officers'),
		('Field Researchers'),
		('Finance Field Officers'),
		('International Associates'),
		('Local Associates'),
		('Local Staff'),
		('Logistic Field Officers')
	
	END
--ENDIF
GO
--Begin table dropdown.ContactAffiliation

--Begin table permissionable.PersonPermissionable
INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	P.PersonID,
	'Vetting.List.VettingOutcomeChangeNotification'
FROM dbo.ListToTable(dbo.GetServerSetupValueByServerSetupKey('VettingMailTo', ''), ',') LTT
	JOIN dbo.Person P ON P.EmailAddress = LTRIM(RTRIM(LTT.ListItem))
GO

DELETE D
FROM
	(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY PP.PersonID, PP.PermissionableLineage ORDER BY PP.PersonID, PP.PermissionableLineage) AS RowIndex,
		PP.PersonID,
		PP.PermissionableLineage
	FROM permissionable.PersonPermissionable PP
	) D
WHERE D.RowIndex > 1
GO
--End table permissionable.PersonPermissionable
