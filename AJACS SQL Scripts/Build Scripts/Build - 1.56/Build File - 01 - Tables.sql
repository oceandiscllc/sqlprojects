USE AJACS
GO

--Begin table workflow.EntityWorkflowStep
DECLARE @TableName VARCHAR(250) = 'workflow.EntityWorkflowStep'

EXEC utility.DropObject @TableName
GO
--End table workflow.EntityWorkflowStep

--Begin table workflow.WorkflowAction
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowAction'

EXEC utility.DropObject @TableName
GO
--End table workflow.WorkflowAction

--Begin table workflow.WorkflowStep
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStep'

EXEC utility.DropColumn @TableName, 'ParentWorkflowStepID'
EXEC utility.DropColumn @TableName, 'WorkflowStatusName'
GO
--End table workflow.WorkflowStep

--Begin table workflow.WorkflowStepGroup
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepGroup'

EXEC utility.DropColumn @TableName, 'LegacyWorkflowStepID'
EXEC utility.DropColumn @TableName, 'PermissionableWorkflowStepID'
GO
--End table workflow.WorkflowStepGroup

--Begin table workflow.WorkflowStepWorkflowAction
DECLARE @TableName VARCHAR(250) = 'workflow.WorkflowStepWorkflowAction'

EXEC utility.DropObject @TableName
GO
--End table workflow.WorkflowStepWorkflowAction
