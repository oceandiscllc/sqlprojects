USE AJACS
GO


--Begin procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate
EXEC Utility.DropObject 'communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.11
-- Description:	A stored procedure to get data from the communityprovinceengagementupdate.CommunityProvinceEngagementUpdate table
-- ==============================================================================================================================
CREATE PROCEDURE communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CommunityProvinceEngagementUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (CommunityProvinceEngagementUpdateID INT)

		INSERT INTO communityprovinceengagementupdate.CommunityProvinceEngagementUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.CommunityProvinceEngagementUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @CommunityProvinceEngagementUpdateID = O.CommunityProvinceEngagementUpdateID FROM @tOutput O
		
		EXEC eventlog.LogCommunityProvinceEngagementAction @EntityID=@CommunityProvinceEngagementUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='CommunityProvinceEngagementUpdate', @EntityID=@CommunityProvinceEngagementUpdateID

		END
	ELSE
		SELECT @CommunityProvinceEngagementUpdateID = CPEU.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('CommunityProvinceEngagementUpdate', @CommunityProvinceEngagementUpdateID)
	
	--CommunityProvinceEngagement
	SELECT
		CPEU.CommunityProvinceEngagementUpdateID, 
		CPEU.WorkflowStepNumber 
	FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate CPEU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM communityprovinceengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('CommunityProvinceEngagementUpdate', @CommunityProvinceEngagementUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('CommunityProvinceEngagementUpdate', @CommunityProvinceEngagementUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Community Province Engagement'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Community Province Engagement'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Community Province Engagement'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Community Province Engagement'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'CommunityProvinceEngagementUpdate'
		AND EL.EntityID = @CommunityProvinceEngagementUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure communityprovinceengagementupdate.GetCommunityProvinceEngagementUpdate

--Begin procedure dbo.CloneConceptNote
EXEC Utility.DropObject 'dbo.CloneConceptNote'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create date: 2015.08.27
-- Description:	A stored procedure to clone a concept note
-- =======================================================
CREATE PROCEDURE dbo.CloneConceptNote

@ConceptNoteID INT,
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nConceptNoteID INT
	DECLARE @tOutput TABLE (ConceptNoteID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	INSERT INTO dbo.ConceptNote
		(ActivityCode,ActualOutput,ActualTotalAmount,AwardeeSubContractorID1,AwardeeSubContractorID2,Background,BeneficiaryDetails,BrandingRequirements,CanManageContacts,ConceptNoteContactEquipmentDistributionDate,ConceptNoteGroupID,ConceptNoteStatusID,ConceptNoteTypeCode,ConceptNoteTypeID,ContactImportID,CurrencyID,DeobligatedAmount,DescriptionOfImpact,FemaleAdultCount,FemaleAdultCountActual,FemaleAdultDetails,FemaleYouthCount,FemaleYouthCountActual,FemaleYouthDetails,FinalAwardAmount,FinalReportDate,FundingSourceID,ImplementerID,IsEquipmentHandoverComplete,IsFinalPaymentMade,MaleAdultCount,MaleAdultCountActual,MaleAdultDetails,MaleYouthCount,MaleYouthCountActual,MaleYouthDetails,MonitoringEvaluation,Objectives,OtherDeliverable,PlanNotes,PointOfContactPersonID1,PointOfContactPersonID2,RiskAssessment,RiskMitigationMeasures,SoleSourceJustification,SpentToDate,SubmissionDate,SuccessStories,Summary,SummaryOfBackground,SummaryOfImplementation,TaskCode,Title,VettingRequirements,WorkflowStepNumber)
	OUTPUT INSERTED.ConceptNoteID INTO @tOutput
	SELECT
		C.ActivityCode,
		C.ActualOutput,
		C.ActualTotalAmount,
		C.AwardeeSubContractorID1,
		C.AwardeeSubContractorID2,
		C.Background,
		C.BeneficiaryDetails,
		C.BrandingRequirements,
		C.CanManageContacts,
		C.ConceptNoteContactEquipmentDistributionDate,
		C.ConceptNoteGroupID,
		(SELECT CNS.ConceptNoteStatusID FROM dropdown.ConceptNoteStatus CNS WHERE CNS.ConceptNoteStatusCode = 'Active'),
		'ConceptNote',
		C.ConceptNoteTypeID,
		C.ContactImportID,
		C.CurrencyID,
		C.DeobligatedAmount,
		C.DescriptionOfImpact,
		C.FemaleAdultCount,
		C.FemaleAdultCountActual,
		C.FemaleAdultDetails,
		C.FemaleYouthCount,
		C.FemaleYouthCountActual,
		C.FemaleYouthDetails,
		C.FinalAwardAmount,
		C.FinalReportDate,
		C.FundingSourceID,
		C.ImplementerID,
		C.IsEquipmentHandoverComplete,
		C.IsFinalPaymentMade,
		C.MaleAdultCount,
		C.MaleAdultCountActual,
		C.MaleAdultDetails,
		C.MaleYouthCount,
		C.MaleYouthCountActual,
		C.MaleYouthDetails,
		C.MonitoringEvaluation,
		C.Objectives,
		C.OtherDeliverable,
		C.PlanNotes,
		C.PointOfContactPersonID1,
		C.PointOfContactPersonID2,
		C.RiskAssessment,
		C.RiskMitigationMeasures,
		C.SoleSourceJustification,
		C.SpentToDate,
		C.SubmissionDate,
		C.SuccessStories,
		C.Summary,
		C.SummaryOfBackground,
		C.SummaryOfImplementation,
		C.TaskCode,
		'Clone of:  ' + C.Title,
		C.VettingRequirements,
		1
	FROM dbo.ConceptNote C
	WHERE C.ConceptNoteID = @ConceptNoteID

	SELECT @nConceptNoteID = T.ConceptNoteID
	FROM @tOutput T

	INSERT INTO workflow.EntityWorkflowStepGroupPerson
		(EntityTypeCode, EntityID, WorkflowID, WorkflowStepID, WorkflowStepGroupID, WorkflowName, WorkflowStepNumber, WorkflowStepName, WorkflowStepGroupName, PersonID)
	SELECT
		'ConceptNote', 
		@nConceptNoteID, 
		W.WorkflowID, 
		WS.WorkflowStepID, 
		WSG.WorkflowStepGroupID, 
		W.WorkflowName, 
		WS.WorkflowStepNumber, 
		WS.WorkflowStepName, 
		WSG.WorkflowStepGroupName, 
		WSGP.PersonID
	FROM workflow.Workflow W
		JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
		JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
		JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
			AND W.WorkflowID = 
				(
				SELECT EWSGP.WorkflowID
				FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				WHERE EWSGP.EntityTypeCode = 'ConceptNote'
					AND EWSGP.EntityID = @nConceptNoteID
				)

	INSERT INTO	dbo.ConceptNoteAmmendment
		(ConceptNoteID, AmmendmentNumber, Date, Description, Cost)
	SELECT
		@nConceptNoteID,
		CNA.AmmendmentNumber, 
		CNA.Date, 
		CNA.Description, 
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteAuthor
		(ConceptNoteID, PersonID)
	SELECT
		@nConceptNoteID,
		CNA.PersonID
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteBudget
		(ConceptNoteID, ItemName, BudgetTypeID, Quantity, UnitCost, Ammendments, ItemDescription, NotesToFile, SpentToDate, UnitOfIssue, BudgetSubTypeID, QuantityOfIssue)
	SELECT
		@nConceptNoteID,
		CNB.ItemName, 
		CNB.BudgetTypeID, 
		CNB.Quantity, 
		CNB.UnitCost, 
		CNB.Ammendments, 
		CNB.ItemDescription, 
		CNB.NotesToFile, 
		CNB.SpentToDate, 
		CNB.UnitOfIssue, 
		CNB.BudgetSubTypeID, 
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
	WHERE CNB.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteCommunity
		(ConceptNoteID, CommunityID)
	SELECT
		@nConceptNoteID,
		CNC.CommunityID
	FROM dbo.ConceptNoteCommunity CNC
	WHERE CNC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEquipmentCatalog
		(ConceptNoteID, EquipmentCatalogID, Quantity, BudgetSubTypeID)
	SELECT
		@nConceptNoteID,
		CNEC.EquipmentCatalogID, 
		CNEC.Quantity, 
		CNEC.BudgetSubTypeID
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
	WHERE CNEC.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteEthnicity
		(ConceptNoteID, EthnicityID)
	SELECT
		@nConceptNoteID,
		CNE.EthnicityID
	FROM dbo.ConceptNoteEthnicity CNE
	WHERE CNE.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteIndicator
		(ConceptNoteID, IndicatorID, TargetQuantity, Comments, ActualQuantity, ActualNumber)
	SELECT
		@nConceptNoteID,
		CNI.IndicatorID, 
		CNI.TargetQuantity, 
		CNI.Comments, 
		CNI.ActualQuantity, 
		CNI.ActualNumber
	FROM dbo.ConceptNoteIndicator CNI
	WHERE CNI.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteProvince
		(ConceptNoteID, ProvinceID)
	SELECT
		@nConceptNoteID,
		CNP.ProvinceID
	FROM dbo.ConceptNoteProvince CNP
	WHERE CNP.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteRisk
		(ConceptNoteID, RiskID)
	SELECT
		@nConceptNoteID,
		CNR.RiskID
	FROM dbo.ConceptNoteRisk CNR
	WHERE CNR.ConceptNoteID = @ConceptNoteID

	INSERT INTO	dbo.ConceptNoteTask
		(ConceptNoteID, ParentConceptNoteTaskID, SubContractorID, ConceptNoteTaskName, ConceptNoteTaskDescription, StartDate, EndDate, SourceConceptNoteTaskID)
	SELECT
		@nConceptNoteID,
		CNT.ParentConceptNoteTaskID, 
		CNT.SubContractorID, 
		CNT.ConceptNoteTaskName, 
		CNT.ConceptNoteTaskDescription, 
		CNT.StartDate, 
		CNT.EndDate, 
		CNT.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT
	WHERE CNT.ConceptNoteID = @ConceptNoteID

	UPDATE CNT1
	SET CNT1.ParentConceptNoteTaskID = CNT2.ConceptNoteTaskID
	FROM dbo.ConceptNoteTask CNT1
		JOIN dbo.ConceptNoteTask CNT2 ON CNT2.SourceConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
			AND CNT1.ParentConceptNoteTaskID <> 0
			AND CNT1.ConceptNoteID = @nConceptNoteID

	EXEC eventlog.LogConceptNoteAction @EntityID=@nConceptNoteID, @EventCode='create', @PersonID=@PersonID

	SELECT T.ConceptNoteID
	FROM @tOutput T

END
GO
--End procedure dbo.CloneConceptNote


--Begin procedure dbo.GetConceptNoteByConceptNoteID
EXEC Utility.DropObject 'dbo.GetConceptNoteByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.21
-- Description:	A stored procedure to data from the dbo.ConceptNote table
--
-- Author:			Greg Yingling
-- Update date:	2015.05.02
-- Description:	Added BudgetSubType data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.06
-- Description:	Added BudgetSubType data and ItemDescription to EquipmentCatalog data
--
-- Author:			Greg Yingling
-- Update date:	2015.05.24
-- Description:	Not every class has a community
--
-- Author:			Greg Yingling
-- Update Date: 2015.06.05
-- Description:	Added QuantityOfIssue
--
-- Author:			Kevin Ross
-- Update Date: 2015.07.24
-- Description:	Added risk recordset
--
-- Author:			Todd Pires
-- Update Date: 2015.08.23
-- Description:	Changed the contact name format
-- ==================================================================================
CREATE PROCEDURE dbo.GetConceptNoteByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ConceptNote', @ConceptNoteID)

	SELECT
		CN.ActivityCode, 
		CN.ActualOutput,
		CN.Background, 
		CN.BeneficiaryDetails,
		CN.CanManageContacts,
		CN.ConceptNoteContactEquipmentDistributionDate,
		dbo.FormatDate(CN.ConceptNoteContactEquipmentDistributionDate) AS ConceptNoteContactEquipmentDistributionDateFormatted,
		CN.ConceptNoteID,
		CN.ConceptNoteTypeCode,
		CN.EndDate,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,
		CN.IsEquipmentHandoverComplete,
		CN.IsFinalPaymentMade,
		CN.MonitoringEvaluation, 
		CN.Objectives, 
		CN.OtherDeliverable,
		CN.PlanNotes,
		CN.PointOfContactPersonID1,
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID1, 'LastFirst') AS PointOfContactPerson1NameFormatted,
		CN.PointOfContactPersonID2, 
		dbo.FormatPersonNameByPersonID(PointOfContactPersonID2, 'LastFirst') AS PointOfContactPerson2NameFormatted,
		CN.AwardeeSubContractorID1,
		SC1.SubContractorName AS AwardeeSubContractorName1,
		CN.AwardeeSubContractorID2,
		SC2.SubContractorName AS AwardeeSubContractorName2,
		CN.MaleAdultCount,
		CN.FemaleAdultCount,
		CN.MaleYouthCount,
		CN.FemaleYouthCount,
		CN.MaleAdultCountActual,
		CN.FemaleAdultCountActual,
		CN.MaleYouthCountActual,
		CN.FemaleYouthCountActual,
		CN.MaleAdultDetails,
		CN.FemaleAdultDetails,
		CN.MaleYouthDetails,
		CN.FemaleYouthDetails,
		CN.FinalAwardAmount,
		CN.ActualTotalAmount,
		CN.DeobligatedAmount,
		CN.SummaryOfBackground,
		CN.SummaryOfImplementation,
		CN.DescriptionOfImpact,
		CN.SuccessStories,
		CN.BeneficiaryDetails,
		CN.RiskAssessment,
		CN.RiskMitigationMeasures,
		CN.BrandingRequirements,
		CN.Remarks,
		CN.SpentToDate,
		CN.StartDate,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		CN.SoleSourceJustification, 
		CN.SubmissionDate, 
		dbo.FormatDate(CN.SubmissionDate) AS SubmissionDateFormatted, 
		CN.FinalReportDate,
		dbo.FormatDate(CN.FinalReportDate) AS FinalReportDateFormatted,
		CN.Summary,
		CN.TaskCode,
		CN.Title,
		CN.VettingRequirements,
		CN.WorkflowStepNumber,
		CN.ConceptNoteFinanceTaskID,
		CNS.ConceptNoteStatusID,
		CNS.ConceptNoteStatusName,
		CNT.ConceptNoteTypeID,
		CNT.ConceptNoteTypeName,
		CUR.CurrencyID,
		CUR.CurrencyName,
		CUR.ISOCurrencyCode,
		FS.FundingSourceID,
		FS.FundingSourceName,
		I.ImplementerID,
		I.ImplementerName,
		dbo.GetEntityTypeNameByEntityTypeCode('ConceptNote') AS EntityTypeName,
		dbo.FormatConceptNoteReferenceCode(@ConceptNoteID) AS ReferenceCode,
		((SELECT ISNULL(SUM(CNF.DRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID) - (SELECT ISNULL(SUM(CNF.CRAmt), 0) FROM dbo.ConceptNoteFinance CNF WHERE CNF.TaskID = CN.ConceptNoteFinanceTaskID)) AS CalculatedTotalAmountDispersed
	FROM dbo.ConceptNote CN
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID
		JOIN dropdown.Currency CUR ON CUR.CurrencyID = CN.CurrencyID
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		JOIN dbo.SubContractor SC1 ON SC1.SubContractorID = CN.AwardeeSubContractorID1
		JOIN dbo.SubContractor SC2 ON SC2.SubContractorID = CN.AwardeeSubContractorID2
			AND CN.ConceptNoteID = @ConceptNoteID

	SELECT 
		dbo.GetServerSetupValueByServerSetupKey('ConceptNoteBackgroundText', '') AS ConceptNoteBackgroundText

	SELECT			
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName,
		BT.BudgetTypeID,
		BT.BudgetTypeName,
		CNB.Ammendments,
		CNB.ConceptNoteBudgetID,
		CNB.ItemDescription,
		CNB.ItemName,
		CNB.NotesToFile,
		CNB.Quantity,
		CNB.SpentToDate,
		FORMAT(CNB.SpentToDate, 'C', 'en-us') AS SpentToDateFormatted,
		CNB.UnitCost,
		FORMAT(CNB.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost AS TotalCost,
		FORMAT(CNB.Quantity * CNB.QuantityOfIssue * CNB.UnitCost, 'C', 'en-us') AS TotalCostFormatted,
		CNB.UnitOfIssue,
		CNB.QuantityOfIssue
	FROM dbo.ConceptNoteBudget CNB
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNB.BudgetSubTypeID
		JOIN dropdown.BudgetType BT ON BT.BudgetTypeID = CNB.BudgetTypeID
			AND CNB.ConceptNoteID = @ConceptNoteID

	SELECT
		C.CommunityID,
		C.CommunityName
	FROM dbo.Community C
		JOIN dbo.ConceptNoteCommunity CNC ON CNC.CommunityID = C.CommunityID
			AND CNC.ConceptNoteID = @ConceptNoteID

	SELECT
		C.ContactID,
		C.Gender,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,

		CASE
			WHEN C.USVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.USVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.USVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS USVettingIcon,

		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,

		CASE
			WHEN C.UKVettingExpirationDate >= (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/32AC41-vetting.png" /> '
			WHEN C.UKVettingExpirationDate < (SELECT CN.EndDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
				AND C.UKVettingExpirationDate >= (SELECT CN.StartDate FROM dbo.ConceptNote CN WHERE CN.ConceptNoteID = @ConceptNoteID)
			THEN '<img src="/assets/img/icons/FCEB1F-vetting.png" /> '
			ELSE '<img src="/assets/img/icons/EA1921-vetting.png" /> '
		END AS UKVettingIcon

	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.ConceptNoteContact CNC0 ON CNC0.ContactID = CV0.ContactID
					AND CNC0.ConceptNoteID = @ConceptNoteID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.ConceptNoteContact CNC1 ON CNC1.ContactID = C1.ContactID
					AND CNC1.ConceptNoteID = @ConceptNoteID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT CV12.VettingOutcomeID
			FROM dbo.ContactVetting CV12
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT CV22.VettingOutcomeID
			FROM dbo.ContactVetting CV22
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
	ORDER BY 3, 1
				
	SELECT
		E.EthnicityID,
		E.EthnicityName
	FROM dbo.ConceptNoteEthnicity CNE
		JOIN dropdown.Ethnicity E ON E.EthnicityID = CNE.EthnicityID
			AND CNE.ConceptNoteID = @ConceptNoteID
	ORDER BY E.EthnicityName

	SELECT
		C.ClassID,
		C.EndDate,
		dbo.FormatDate(C.EndDate) AS EndDateFormatted,
		C.Location,
		C.StartDate,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		COU.CourseName,
		COM.CommunityName
	FROM dbo.ConceptNoteClass CNC
		JOIN dbo.Class C ON C.ClassID = CNC.ClassID
		JOIN dbo.Course COU on COU.CourseID = C.CourseID
			AND CNC.ConceptNoteID = @ConceptNoteID
		OUTER APPLY 
			(
			SELECT C1.CommunityName 
			FROM dbo.Community C1
			WHERE C1.CommunityID = C.CommunityID
			) COM
	ORDER BY COU.CourseName

	SELECT
		CNEC.ConceptNoteEquipmentCatalogID,
		CNEC.Quantity,
		EC.EquipmentCatalogID,
		EC.ItemName,
		EC.UnitCost,
		EC.UnitOfIssue,
		EC.QuantityOfIssue,
		FORMAT(EC.UnitCost, 'C', 'en-us') AS UnitCostFormatted,
		CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue AS TotalCost,
		FORMAT(CNEC.Quantity * EC.UnitCost * EC.QuantityOfIssue, 'C', 'en-us') AS TotalCostFormatted,
		EC.ItemDescription,
		BST.BudgetSubTypeID,
		BST.BudgetSubTypeName
	FROM dbo.ConceptNoteEquipmentCatalog CNEC
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = CNEC.EquipmentCatalogID
			AND CNEC.ConceptNoteID = @ConceptNoteID
		JOIN dropdown.BudgetSubType BST ON BST.BudgetSubTypeID = CNEC.BudgetSubTypeID
			
	SELECT
		CNI.ActualNumber,
		CNI.ActualQuantity,
		CNI.Comments,
		CNI.TargetQuantity,
		I.IndicatorID,
		I.IndicatorDescription,
		I.IndicatorName,
		IT.IndicatorTypeID,
		IT.IndicatorTypeName,
		O.ObjectiveID,
		O.ObjectiveName,
		OT.ObjectiveTypeID,
		OT.ObjectiveTypeName
	FROM dbo.ConceptNoteIndicator CNI
		JOIN logicalframework.Indicator I ON I.IndicatorID = CNI.IndicatorID
		JOIN dropdown.IndicatorType IT ON IT.IndicatorTypeID = I.IndicatorTypeID
		JOIN logicalframework.Objective O ON O.ObjectiveID = I.ObjectiveID
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O.ObjectiveTypeID
			AND CNI.ConceptNoteID = @ConceptNoteID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN dbo.ConceptNoteProvince CNP ON CNP.ProvinceID = P.ProvinceID
			AND CNP.ConceptNoteID = @ConceptNoteID
			
	SELECT
		CNT1.ConceptNoteTaskDescription,
		CNT1.ConceptNoteTaskID,
		CNT1.ConceptNoteTaskName,
		CNT1.EndDate,
		dbo.FormatDate(CNT1.EndDate) AS EndDateFormatted,
		CNT1.IsComplete,
		CNT1.ParentConceptNoteTaskID,
		CNT1.StartDate,
		dbo.FormatDate(CNT1.StartDate) AS StartDateFormatted,
		CNT3.ConceptNoteTaskName AS ParentConceptNoteTaskName,
		SC1.SubContractorID,
		SC1.SubContractorName
	FROM dbo.ConceptNoteTask CNT1
		OUTER APPLY
				(
				SELECT
					SC2.SubContractorID,
					SC2.SubContractorName
				FROM dbo.SubContractor SC2
				WHERE SC2.SubContractorID = CNT1.SubContractorID
				) SC1
		OUTER APPLY
				(
				SELECT
					CNT2.ConceptNoteTaskName
				FROM dbo.ConceptNoteTask CNT2
				WHERE CNT2.ConceptNoteTaskID = CNT1.ParentConceptNoteTaskID
				) CNT3
	WHERE CNT1.ConceptNoteID = @ConceptNoteID
	ORDER BY CNT1.ConceptNoteTaskName, CNT1.ConceptNoteTaskID

	SELECT
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID,
		DT.DocumentTypeCode,
		DT.DocumentTypeID,
		DT.DocumentTypeName
	FROM dbo.DocumentEntity DE 
		JOIN dbo.Document D ON D.DocumentID = DE.DocumentID
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND DE.EntityTypeCode = 'ConceptNote'
			AND DE.EntityID = @ConceptNoteID
	ORDER BY DT.DocumentTypeName
	
	SELECT			
		CNA.PersonID,
		dbo.FormatPersonNameByPersonID(CNA.PersonID, 'LastFirst') AS FullName
	FROM dbo.ConceptNoteAuthor CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT			
		CNA.ConceptNoteAmmendmentID,
		CNA.AmmendmentNumber,
		CNA.Date,
		dbo.FormatDate(CNA.Date) AS DateFormatted,
		CNA.Description,
		CNA.Cost
	FROM dbo.ConceptNoteAmmendment CNA
	WHERE CNA.ConceptNoteID = @ConceptNoteID

	SELECT
		R.RiskID,
		R.RiskName,
		R.DateRaised,
		dbo.FormatDate(R.DateRaised) AS DateRaisedFormatted,
		R.Impact * R.Likelihood AS Overall,
		RC.RiskCategoryName
	FROM dbo.Risk R
		JOIN dbo.ConceptNoteRisk CNR ON CNR.RiskID = R.RiskID
		JOIN dropdown.RiskCategory RC ON RC.RiskCategoryID = R.RiskCategoryID
			AND CNR.ConceptNoteID = @ConceptNoteID
	
	SELECT 
		P.ProjectID, 
		P.ProjectName,
		PS.ProjectStatusName
	FROM project.Project P
		JOIN dropdown.ProjectStatus PS ON PS.ProjectStatusID = P.ProjectStatusID
		JOIN dropdown.Component C ON C.ComponentID = P.ComponentID
			AND P.ConceptNoteID = @ConceptNoteID AND @ConceptNoteID > 0

	SELECT
		CNF.TransactionID,
		CNF.TaskID,
		CNF.DRAmt,
		CNF.CRAmt,
		CNF.VendID
	FROM dbo.ConceptNoteFinance CNF
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteFinanceTaskID = CNF.TaskID
			AND CN.ConceptNoteID = @ConceptNoteID
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ConceptNote', @ConceptNoteID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ConceptNote', @ConceptNoteID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Concept Note'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Concept Note'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Concept Note'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Concept Note'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ConceptNote'
		AND EL.EntityID = @ConceptNoteID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
	
END
GO
--End procedure dbo.GetConceptNoteByConceptNoteID

--Begin procedure dbo.GetEmailTemplateByEmailTemplateID
EXEC Utility.DropObject 'dbo.GetEmailTemplateByEmailTemplateID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.05.31
-- Description:	A stored procedure to data from the dbo.EmailTemplate table
-- ========================================================================
CREATE PROCEDURE dbo.GetEmailTemplateByEmailTemplateID

@EmailTemplateID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ET1.EmailTemplateID, 
		ET1.EntityTypeCode, 
		ET1.WorkflowActionCode, 
		ET1.EmailText,
		ET2.EntityTypeName,
		ET1.WorkflowActionCode AS WorkflowActionName
	FROM dbo.EmailTemplate ET1
		JOIN dbo.EntityType ET2 ON ET2.EntityTypeCode = ET1.EntityTypeCode
			AND ET1.EmailTemplateID = @EmailTemplateID

	SELECT
		ETF.PlaceHolderText, 
		ETF.PlaceHolderDescription
	FROM dbo.EmailTemplateField ETF
		JOIN dbo.EmailTemplate ET ON ET.EntityTypeCode = ETF.EntityTypeCode
			AND ET.EmailTemplateID = @EmailTemplateID
	ORDER BY ETF.DisplayOrder

END
GO
--End procedure dbo.GetEmailTemplateByEmailTemplateID

--Begin procedure dbo.GetSpotReportBySpotReportID
EXEC Utility.DropObject 'dbo.GetSpotReportBySpotReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.03.06
-- Description:	A stored procedure to data from the dbo.SpotReport table
--
-- Author:			Todd Pires
-- Update date:	2015.03.09
-- Description:	Added the workflow step reqult set
--
-- Author:			Todd Pires
-- Update date:	2015.04.19
-- Description:	Added the SpotReportReferenceCode, multi community & province support
--
-- Author:		Eric Jones
-- Update date:	2016.01.21
-- Description:	Added the Force support
-- ==================================================================================
CREATE PROCEDURE dbo.GetSpotReportBySpotReportID

@SpotReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('SpotReport', @SpotReportID)

	SELECT
		ID.ImpactDecisionID, 
		ID.ImpactDecisionName AS RecommendedImpactDecisionName,
		SC.StatusChangeID,
		SC.StatusChangeName AS RecommendedStatusChangeName,
		SR.AdditionalInformation,
		SR.AnalystComments,
		SR.Implications,
		SR.IncidentDetails,
		SR.IsCritical,
		SR.Recommendation,
		SR.Resourcing,
		SR.ResourcingStatus,
		SR.RiskMitigation,
		SR.SpotReportDate,
		dbo.FormatDate(SR.SpotReportDate) AS SpotReportDateFormatted,
		SR.SpotReportID,
		dbo.FormatSpotReportReferenceCode(SR.SpotReportID) AS SpotReportReferenceCode,
		SR.SpotReportTitle,
		SR.Summary,
		SR.WorkflowStepNumber,
		CAST(N'' AS xml).value('xs:base64Binary(xs:hexBinary(sql:column("SR.SummaryMap")))', 'varchar(MAX)') AS SummaryMap,
		SR.SummaryMapZoom,
		reporting.IsDraftReport('SpotReport', SR.SpotReportID) AS IsDraft,
		dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') + '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '-' + SC.Direction + '.png' AS RecommendedIcon,
		dbo.GetEntityTypeNameByEntityTypeCode('SpotReport') AS EntityTypeName
	FROM dbo.SpotReport SR
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = SR.ImpactDecisionID
		JOIN dropdown.StatusChange SC ON SC.StatusChangeID = SR.StatusChangeID
			AND SR.SpotReportID = @SpotReportID

	SELECT
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.SpotReportCommunity SRC
		JOIN dbo.Community C ON C.CommunityID = SRC.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
			AND SRC.SpotReportID = @SpotReportID
	ORDER BY C.CommunityName, P.ProvinceName, C.CommunityID

	SELECT
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.SpotReportProvince SRP
		JOIN dbo.Province P ON P.ProvinceID = SRP.ProvinceID
			AND SRP.SpotReportID = @SpotReportID
	ORDER BY P.ProvinceName, P.ProvinceID

	SELECT
		I.IncidentID,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.IncidentName
	FROM dbo.SpotReportIncident SRI
		JOIN dbo.Incident I ON I.IncidentID = SRI.IncidentID
			AND SRI.SpotReportID = @SpotReportID
	ORDER BY I.IncidentName, I.IncidentID
	
	SELECT
		F.ForceName,
		F.ForceDescription,
		F.ForceID
	FROM dbo.SpotReportForce SRF
		JOIN force.Force F ON F.ForceID = SRF.ForceID
			AND SRF.SpotReportID = @SpotReportID
	ORDER BY F.ForceName, F.ForceID
	
	SELECT
		D.DocumentName,
		IsNull(D.DocumentDescription, '') + ' (' + D.DocumentName + ')' AS DocumentNameFormatted,
		D.PhysicalFileName,
		D.Thumbnail,
		ISNULL(DATALENGTH(D.Thumbnail), 0) AS ThumbnailLength,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'SpotReport'
			AND DE.EntityID = @SpotReportID
	
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('SpotReport', @SpotReportID) EWD
	
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('SpotReport', @SpotReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Spot Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Spot Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Spot Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Spot Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'SpotReport'
		AND EL.EntityID = @SpotReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END 
GO
--End procedure dbo.GetSpotReportBySpotReportID

--Begin procedure fifupdate.GetFIFUpdate
EXEC Utility.DropObject 'fifupdate.GetFIFUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2015.12.27
-- Description:	A stored procedure to get data from the fifupdate.FIFUpdate table
-- ==============================================================================
CREATE PROCEDURE fifupdate.GetFIFUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FIFUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM fifupdate.FIFUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (FIFUpdateID INT)

		INSERT INTO fifupdate.FIFUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.FIFUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @FIFUpdateID = O.FIFUpdateID FROM @tOutput O
		
		EXEC eventlog.LogFIFAction @EntityID=@FIFUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='FIFUpdate', @EntityID=@FIFUpdateID

		END
	ELSE
		SELECT @FIFUpdateID = FU.FIFUpdateID FROM fifupdate.FIFUpdate FU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('FIFUpdate', @FIFUpdateID)

	--FIF
	SELECT
		FU.FIFUpdateID, 
		FU.WorkflowStepNumber 
	FROM fifupdate.FIFUpdate FU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
	ORDER BY C2.CommunityName, C1.CommunityID
	
	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM fifupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	ORDER BY P2.ProvinceName, P1.ProvinceID
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('FIFUpdate', @FIFUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('FIFUpdate', @FIFUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created FIF Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected FIF Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved FIF Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated FIF Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'FIFUpdate'
		AND EL.EntityID = @FIFUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure fifupdate.GetFIFUpdate

--Begin procedure justiceupdate.GetJusticeUpdate
EXEC Utility.DropObject 'justiceupdate.GetJusticeUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Jonathan Burnham
-- Create Date: 2016.02.13
-- Description:	A stored procedure to get data for a justice update batch
-- ======================================================================
CREATE PROCEDURE justiceupdate.GetJusticeUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @JusticeUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM justiceupdate.JusticeUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (JusticeUpdateID INT)

		INSERT INTO justiceupdate.JusticeUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.JusticeUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @JusticeUpdateID = O.JusticeUpdateID FROM @tOutput O
		
		EXEC eventlog.LogJusticeAction @EntityID=@JusticeUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='JusticeUpdate', @EntityID=@JusticeUpdateID

		END
	ELSE
		SELECT @JusticeUpdateID = FU.JusticeUpdateID FROM justiceupdate.JusticeUpdate FU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('JusticeUpdate', @JusticeUpdateID)

	--Justice
	SELECT
		FU.JusticeUpdateID, 
		FU.WorkflowStepNumber 
	FROM justiceupdate.JusticeUpdate FU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDATETIME(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM justiceupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID
	ORDER BY C2.CommunityName, C1.CommunityID
	
	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDATETIME(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM justiceupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	ORDER BY P2.ProvinceName, P1.ProvinceID
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('JusticeUpdate', @JusticeUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('JusticeUpdate', @JusticeUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Justice Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Justice Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Justice Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Justice Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'JusticeUpdate'
		AND EL.EntityID = @JusticeUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure justiceupdate.GetJusticeUpdateID

--Begin procedure policeengagementupdate.GetPoliceEngagementUpdate
EXEC Utility.DropObject 'policeengagementupdate.GetPoliceEngagementUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.09.11
-- Description:	A stored procedure to get data from the policeengagementupdate.PoliceEngagementUpdate table
-- ========================================================================================================
CREATE PROCEDURE policeengagementupdate.GetPoliceEngagementUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PoliceEngagementUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM policeengagementupdate.PoliceEngagementUpdate)
		BEGIN
		
		DECLARE @tOutput TABLE (PoliceEngagementUpdateID INT)

		INSERT INTO policeengagementupdate.PoliceEngagementUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.PoliceEngagementUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @PoliceEngagementUpdateID = O.PoliceEngagementUpdateID FROM @tOutput O
		
		EXEC eventlog.LogPoliceEngagementAction @EntityID=@PoliceEngagementUpdateID, @EventCode='create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='PoliceEngagementUpdate', @EntityID=@PoliceEngagementUpdateID

		END
	ELSE
		SELECT @PoliceEngagementUpdateID = PEU.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate PEU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('PoliceEngagementUpdate', @PoliceEngagementUpdateID)
	
	--PoliceEngagement
	SELECT
		PEU.PoliceEngagementUpdateID, 
		PEU.WorkflowStepNumber 
	FROM policeengagementupdate.PoliceEngagementUpdate PEU

	--Community
	SELECT
		C1.CommunityID,
		C2.CommunityName,
		dbo.FormatDateTime(C1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(C1.UpdatePersonID, 'LastFirst') AS FullName
	FROM policeengagementupdate.Community C1
		JOIN dbo.Community C2 ON C2.CommunityID = C1.CommunityID

	--Province
	SELECT
		P1.ProvinceID,
		P2.ProvinceName,
		dbo.FormatDateTime(P1.UpdateDateTime) AS UpdateDateTimeFormatted,
		dbo.FormatPersonNameByPersonID(P1.UpdatePersonID, 'LastFirst') AS FullName
	FROM policeengagementupdate.Province P1
		JOIN dbo.Province P2 ON P2.ProvinceID = P1.ProvinceID
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('PoliceEngagementUpdate', @PoliceEngagementUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('PoliceEngagementUpdate', @PoliceEngagementUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Police Engagement Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Police Engagement Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Police Engagement Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Police Engagement Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'PoliceEngagementUpdate'
		AND EL.EntityID = @PoliceEngagementUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure policeengagementupdate.GetPoliceEngagementUpdate

--Begin procedure recommendationupdate.GetRecommendationUpdate
EXEC Utility.DropObject 'recommendationupdate.GetRecommendationUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.16
-- Description:	A stored procedure to get data from the recommendationupdate.RecommendationUpdate table
-- ====================================================================================================
CREATE PROCEDURE recommendationupdate.GetRecommendationUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRecommendationUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM recommendationupdate.RecommendationUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RecommendationUpdateID INT)

		INSERT INTO recommendationupdate.RecommendationUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RecommendationUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @nRecommendationUpdateID = O.RecommendationUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRecommendationUpdateAction @EntityID=@nRecommendationUpdateID, @EventCode='Create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='RecommendationUpdate', @EntityID=@nRecommendationUpdateID

		END
	ELSE
		SELECT @nRecommendationUpdateID = RU.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate RU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('RecommendationUpdate', @nRecommendationUpdateID)
	
	SELECT
		RU.RecommendationUpdateID, 
		RU.WorkflowStepNumber 
	FROM recommendationupdate.RecommendationUpdate RU
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('RecommendationUpdate', @nRecommendationUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('RecommendationUpdate', @nRecommendationUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Recommendation Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Recommendation Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Recommendation Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Recommendation Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'RecommendationUpdate'
		AND EL.EntityID = @nRecommendationUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure recommendationupdate.GetRecommendationUpdate

--Begin procedure riskupdate.GetRiskUpdate
EXEC Utility.DropObject 'riskupdate.GetRiskUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:			Todd Pires
-- Create date:	2015.08.08
-- Description:	A stored procedure to get data from the riskupdate.RiskUpdate table
-- ================================================================================
CREATE PROCEDURE riskupdate.GetRiskUpdate

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nRiskUpdateID INT
	
	IF NOT EXISTS (SELECT 1 FROM riskupdate.RiskUpdate RU)
		BEGIN
		
		DECLARE @tOutput TABLE (RiskUpdateID INT)

		INSERT INTO riskupdate.RiskUpdate 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.RiskUpdateID INTO @tOutput
		VALUES 
			(1)

		SELECT @nRiskUpdateID = O.RiskUpdateID FROM @tOutput O
		
		EXEC eventlog.LogRiskUpdateAction @EntityID=@nRiskUpdateID, @EventCode='Create', @PersonID = @PersonID
		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='RiskUpdate', @EntityID=@nRiskUpdateID

		END
	ELSE
		SELECT @nRiskUpdateID = RU.RiskUpdateID FROM riskupdate.RiskUpdate RU
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('RiskUpdate', @nRiskUpdateID)
	
	SELECT
		RU.RiskUpdateID, 
		RU.WorkflowStepNumber 
	FROM riskupdate.RiskUpdate RU
	
	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('RiskUpdate', @nRiskUpdateID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('RiskUpdate', @nRiskUpdateID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Risk Update'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Risk Update'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Risk Update'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Risk Update'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'RiskUpdate'
		AND EL.EntityID = @nRiskUpdateID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END 
GO
--End procedure riskupdate.GetRiskUpdate

--Begin procedure weeklyreport.GetProgramReportByProgramReportID
EXEC Utility.DropObject 'weeklyreport.GetProgramReportByProgramReportID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.11
-- Description:	A stored procedure to data from the weeklyreport.ProgramReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.13
-- Description:	Added ResearchPrevious, ResearchProjected, removed Remarks
-- =================================================================================
CREATE PROCEDURE weeklyreport.GetProgramReportByProgramReportID

@ProgramReportID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('ProgramReport', @ProgramReportID)

	SELECT
		PR.EngagementPrevious,
		PR.EngagementProjected,
		PR.JusticePrevious,
		PR.JusticeProjected,
		PR.MonitoringPrevious,
		PR.MonitoringProjected,
		PR.OperationsPrevious,
		PR.OperationsProjected,
		PR.OverallPrevious,
		PR.OverallProjected,
		PR.PolicingPrevious,
		PR.PolicingProjected,
		dbo.FormatProgramReportReferenceCode(PR.ProgramReportID) AS ProgramReportReferenceCode,
		PR.ProgramReportEndDate,
		dbo.FormatDate(PR.ProgramReportEndDate) AS ProgramReportEndDateFormatted,
		PR.ProgramReportID,
		PR.ProgramReportName,
		PR.ProgramReportStartDate,
		dbo.FormatDate(PR.ProgramReportStartDate) AS ProgramReportStartDateFormatted,
		PR.ResearchPrevious,
		PR.ResearchProjected,
		PR.StructuresPrevious,
		PR.StructuresProjected,
		PR.WorkflowStepNumber,
		dbo.GetEntityTypeNameByEntityTypeCode('ProgramReport') AS EntityTypeName
	FROM weeklyreport.ProgramReport PR
	WHERE PR.ProgramReportID = @ProgramReportID

	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'ProgramReport'
			AND DE.EntityID = @ProgramReportID
	ORDER BY D.DocumentDescription

	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('ProgramReport', @ProgramReportID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('ProgramReport', @ProgramReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
				CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Program Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Program Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Program Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Program Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'ProgramReport'
		AND EL.EntityID = @ProgramReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime
		
END
GO
--End procedure weeklyreport.GetProgramReportByProgramReportID

--Begin procedure weeklyreport.GetWeeklyReport
EXEC Utility.DropObject 'weeklyreport.GetWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.12
-- Description:	A stored procedure to get data from the weeklyreport.WeeklyReport table
--
-- Author:			Todd Pires
-- Create date:	2015.05.09
-- Description:	Added date range and reference code support
-- ====================================================================================
CREATE PROCEDURE weeklyreport.GetWeeklyReport

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nWeeklyReportID INT
	
	IF NOT EXISTS (SELECT 1 FROM weeklyreport.WeeklyReport WR)
		BEGIN
		
		DECLARE @tOutput TABLE (WeeklyReportID INT)

		INSERT INTO weeklyreport.WeeklyReport 
			(WorkflowStepNumber) 
		OUTPUT INSERTED.WeeklyReportID INTO @tOutput
		VALUES 
			(1)

		SELECT @nWeeklyReportID = O.WeeklyReportID FROM @tOutput O

		EXEC workflow.InitializeEntityWorkflow @EntityTypeCode='WeeklyReport', @EntityID=@nWeeklyReportID

		END
	ELSE
		SELECT @nWeeklyReportID = WR.WeeklyReportID FROM weeklyreport.WeeklyReport WR
	--ENDIF
	
	DECLARE @nWorkflowStepNumber INT = workflow.GetWorkflowStepNumber('WeeklyReport', @nWeeklyReportID)
	
	SELECT
		WR.EndDate,
		dbo.FormatDate(WR.EndDate) AS EndDateFormatted,
		WR.StartDate,
		dbo.FormatDate(WR.StartDate) AS StartDateFormatted,
		dbo.FormatWeeklyReportReferenceCode(WR.WeeklyReportID) AS ReferenceCode,
		WR.WeeklyReportID, 
		WR.WorkflowStepNumber,
		WR.SummaryMapZoom 
	FROM weeklyreport.WeeklyReport WR

		SELECT
		C.CommunityID,
		C.CommunityName,
		C.CommunityEngagementStatusID,
		C.ImpactDecisionID,
		ID.ImpactDecisionName,
		'/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') + '.png' AS Icon, 
		ID.HexColor,
		C2.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunity SMC WHERE SMC.CommunityID = C.CommunityID AND SMC.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM weeklyreport.Community C
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dbo.Community C2 ON C2.CommunityID = C.CommunityID
			 AND C.WeeklyReportID = @nWeeklyReportID

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		AT.AssetTypeID,
		AT.AssetTypeName,
		'/assets/img/icons/' + AT.Icon AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
			AND CAT.CommunityAssetTypeID = 1
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		CA.CommunityAssetID,
		CA.CommunityAssetName,
		CAT.CommunityAssetTypeID,
		CAT.CommunityAssetTypeName,
		ZT.ZoneTypeID,
		ZT.ZoneTypeName,
		ZT.HexColor,
		'/assets/img/icons/' + REPLACE(ZT.HexColor, '#', '') + '.png' AS Icon,
		CA.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapCommunityAsset SMCA WHERE SMCA.CommunityAssetID = CA.CommunityAssetID AND SMCA.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.CommunityAsset CA
		JOIN dropdown.CommunityAssetType CAT ON CAT.CommunityAssetTypeID = CA.CommunityAssetTypeID
		JOIN dropdown.ZoneType ZT ON ZT.ZoneTypeID = CA.ZoneTypeID
			AND CAT.CommunityAssetTypeID = 2
			AND 
				(
					EXISTS(SELECT 1 FROM weeklyreport.Community C WHERE C.CommunityID = CA.CommunityID AND C.WeeklyReportID = @nWeeklyReportID)
					OR
					EXISTS(SELECT 1 FROM weeklyreport.Province P WHERE P.ProvinceID = CA.ProvinceID AND P.WeeklyReportID = @nWeeklyReportID)
				)

	SELECT
		I.IncidentID,
		I.IncidentName,
		IT.IncidentTypeID,
		IT.IncidentTypeName,
		'/assets/img/icons/' + IT.Icon AS Icon,
		dbo.FormatDate(I.IncidentDate) AS IncidentDateFormatted,
		I.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapIncident SMI WHERE SMI.IncidentID = I.IncidentID AND SMI.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM dbo.Incident I
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID = I.IncidentTypeID
			AND (
				EXISTS	(
					SELECT 1
					FROM dbo.IncidentCommunity IC
						JOIN weeklyreport.Community C ON C.CommunityID = IC.CommunityID
							AND IC.IncidentID = I.IncidentID
							AND C.WeeklyReportID = @nWeeklyReportID
				)
				OR
				EXISTS (
					SELECT 1
					FROM dbo.IncidentProvince IP
						JOIN weeklyreport.Province P ON P.ProvinceID = IP.ProvinceID
							AND IP.IncidentID = I.IncidentID
							AND P.WeeklyReportID = @nWeeklyReportID
				)
			)		

	SELECT
		F.ForceID,
		F.ForceName,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor,
		'/assets/img/icons/' + REPLACE(AOT.HexColor, '#', '') + '.png' AS Icon,
		F.Location.STAsText() AS Location,
		CASE WHEN EXISTS(SELECT 1 FROM weeklyreport.SummaryMapForce SMF WHERE SMF.ForceID = F.ForceID AND SMF.WeeklyReportID = @nWeeklyReportID) THEN 1 
		ELSE 0 
		END AS IsMapped
	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND EXISTS
				(
					SELECT 1
					FROM force.ForceCommunity FC
						JOIN weeklyreport.Community C ON C.CommunityID = FC.CommunityID
							AND FC.ForceID = F.ForceID
							AND C.WeeklyReportID = @nWeeklyReportID
				)

	--WorkflowData
	SELECT
		EWD.WorkflowStepName,
		EWD.WorkflowStepNumber,
		EWD.WorkflowStepCount,
		EWD.CanRejectAfterFinalApproval
	FROM workflow.GetEntityWorkflowData('WeeklyReport', @nWeeklyReportID) EWD
	
	--WorkflowPeople
	SELECT
		EWP.WorkflowStepGroupName, 
		EWP.FullName, 
		EWP.IsComplete
	FROM workflow.GetEntityWorkflowPeople('WeeklyReport', @nWeeklyReportID, @nWorkflowStepNumber) EWP
	ORDER BY 1, 2

	--WorkflowEventLog
	SELECT
		EL.EventLogID,
		dbo.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS FullName,
		dbo.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
	
		CASE
			WHEN EL.EventCode = 'create'
			THEN 'Created Weekly Report'
			WHEN EL.EventCode = 'decrementworkflow'
			THEN 'Rejected Weekly Report'
			WHEN EL.EventCode = 'incrementworkflow'
			THEN 'Approved Weekly Report'
			WHEN EL.EventCode = 'update'
			THEN 'Updated Weekly Report'
		END AS EventAction,
	
		EL.Comments
	FROM eventlog.EventLog EL
	WHERE EL.EntityTypeCode = 'WeeklyReport'
		AND EL.EntityID = @nWeeklyReportID
		AND EL.EventCode IN ('create','decrementworkflow','incrementworkflow','update')
	ORDER BY EL.CreateDateTime

END
GO
--End procedure weeklyreport.GetWeeklyReport

--Begin deprecated workflow related objects
EXEC Utility.DropObject 'dbo.InitializeEntityWorkflowStep'
EXEC Utility.DropObject 'riskupdate.GetRiskAddUpdate'
EXEC Utility.DropObject 'workflow.CanIncrementCommunityProvinceEngagementUpdateWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementConceptNoteWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementFIFUpdateWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementJusticeUpdateWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementPoliceEngagementUpdateWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementProgramReportWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementRecommendationUpdateWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementRiskUpdateWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementSpotReportWorkflow'
EXEC Utility.DropObject 'workflow.CanIncrementWeeklyReportWorkflow'
EXEC Utility.DropObject 'workflow.GetCommunityProvinceEngagementUpdateWorkflowData'
EXEC Utility.DropObject 'workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetCommunityProvinceEngagementUpdateWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetConceptNoteWorkflowData'
EXEC Utility.DropObject 'workflow.GetConceptNoteWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetEquipmentDistributionPlanWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetFIFUpdateWorkflowData'
EXEC Utility.DropObject 'workflow.GetFIFUpdateWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetJusticeUpdateWorkflowData'
EXEC Utility.DropObject 'workflow.GetJusticeUpdateWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetPoliceEngagementUpdateWorkflowData'
EXEC Utility.DropObject 'workflow.GetPoliceEngagementUpdateWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetProgramReportWorkflowData'
EXEC Utility.DropObject 'workflow.GetProgramReportWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetRecommendationUpdateWorkflowData'
EXEC Utility.DropObject 'workflow.GetRecommendationUpdateWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetRiskUpdateWorkflowData'
EXEC Utility.DropObject 'workflow.GetRiskUpdateWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetSpotReportWorkflowData'
EXEC Utility.DropObject 'workflow.GetSpotReportWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetWeeklyReportWorkflowData'
EXEC Utility.DropObject 'workflow.GetWeeklyReportWorkflowStepPeople'
EXEC Utility.DropObject 'workflow.GetWorkflowStepIDsByEntityTypeCodeAndWorkflowStepNumber'
EXEC Utility.DropObject 'workflow.procurement.GetEquipmentDistributionPlanByConceptNoteID'
EXEC Utility.DropObject 'workflow.SetEntityWorkflowStepInComplete'
GO
--End deprecated workflow related objects

