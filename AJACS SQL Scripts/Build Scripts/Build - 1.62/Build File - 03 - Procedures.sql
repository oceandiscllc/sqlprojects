USE AJACS
GO

--Begin procedure dbo.GetCommunityRoundByCommunityRoundID
EXEC utility.DropObject 'dbo.GetCommunityRoundByCommunityRoundID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create date: 2015.12.17
-- Description:	A stored procedure to get CommunityRound data
-- ==========================================================
CREATE PROCEDURE dbo.GetCommunityRoundByCommunityRoundID

@CommunityRoundID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		C.CommunityEngagementStatusID,
		C.CommunityName,
		C.ImpactDecisionID,
		C.PolicePresenceCategoryID,
		C.Population,
		CES.CommunityEngagementStatusName,
		CG.CommunityGroupName,
		CR.ActivitiesOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.ActivityImplementationEndDate,
		dbo.FormatDate(CR.ActivityImplementationEndDate) AS ActivityImplementationEndDateFormatted, 
		CR.ActivityImplementationStartDate,
		dbo.FormatDate(CR.ActivityImplementationStartDate) AS ActivityImplementationStartDateFormatted, 
		CR.ApprovedActivityCount,
		CR.AreaManagerPersonID,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		CR.CommunityCode,
		CR.CommunityID,
		CR.CommunityRoundCivilDefenseCoverageID,
		CR.CommunityRoundGroupID,
		CR.CommunityRoundID,
		CR.CommunityRoundJusticeActivityID,
		CR.CommunityRoundOutput11StatusID,
		CR.CommunityRoundOutput12StatusID,
		CR.CommunityRoundOutput13StatusID,
		CR.CommunityRoundOutput14StatusID,
		CR.CommunityRoundPreAssessmentStatusID,
		CR.CommunityRoundRAPInfoID,
		CR.CommunityRoundRoundID,
		CR.CommunityRoundTamkeenID,
		CR.CSAP2PeerReviewMeetingDate,
		dbo.FormatDate(CR.CSAP2PeerReviewMeetingDate) AS CSAP2PeerReviewMeetingDateFormatted, 
		CR.CSAP2SubmittedDate,
		dbo.FormatDate(CR.CSAP2SubmittedDate) AS CSAP2SubmittedDateFormatted, 
		CR.CSAPBeginDevelopmentDate,
		dbo.FormatDate(CR.CSAPBeginDevelopmentDate) AS CSAPBeginDevelopmentDateFormatted, 
		CR.CSAPCommunityAnnouncementDate,
		dbo.FormatDate(CR.CSAPCommunityAnnouncementDate) AS CSAPCommunityAnnouncementDateFormatted, 
		CR.CSAPFinalizedDate,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		CR.CSAPProcessStartDate,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		CR.FemaleQuestionnairCount,
		CR.FieldFinanceOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerContactName,
		CR.FieldLogisticOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerContactName,
		CR.FieldOfficerContactID,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficerContactName,
		CR.FinalSWOTDate,
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted, 
		CR.InitialSCADate,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted, 
		CR.IsActive,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.KickoffMeetingCount,
		CR.MOUDate,
		dbo.FormatDate(CR.MOUDate) AS MOUDateFormatted, 
		CR.NewCommunityEngagementCluster,
		CR.Preassessment,
		CR.Postassessment,
		CR.ProjectOfficerPersonID,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		CR.QuestionnairCount,
		CR.SCAFinalizedDate,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted, 
		CR.SCAMeetingCount,
		CR.SensitizationDefineFSPRoleDate,
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted, 
		CR.SensitizationDiscussCommMakeupDate,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted, 
		CR.SensitizationMeetingMOUDate,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted, 
		CR.ToRDate,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted, 
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusName,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName AS CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		ID.ImpactDecisionName,
		P.ProvinceID,
		P.ProvinceName,
		PPC.PolicePresenceCategoryName,
		CRES.CommunityRoundEngagementStatusID,
		CRES.CommunityRoundEngagementStatusName,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Male'  
		) AS CSWGMaleCount,
		(
		SELECT COUNT(C.ContactID)
		FROM ContactContactAffiliation CCA
			JOIN dbo.Contact C ON C.ContactID = CCA.ContactID
			JOIN dropdown.ContactAffiliation CA ON CA.ContactAffiliationID = CCA.ContactAffiliationID
				AND CA.ContactAffiliationName = 'Community'
				AND C.CommunityID = CR.CommunityID
				AND C.IsActive = 1
				AND C.Gender = 'Female' 
		) AS CSWGFemaleCount
	FROM dbo.CommunityRound CR
		JOIN dbo.Community C ON C.CommunityID = CR.CommunityID
		JOIN dropdown.CommunityGroup CG ON CG.CommunityGroupID = C.CommunityGroupID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityROundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID
			AND CR.CommunityRoundID = @CommunityRoundID

	--EntityDocumentCurrent
	SELECT
		D.DocumentID,
		D.DocumentDescription,
		D.DocumentName,
		D.DocumentTitle,
		D.PhysicalFileName,
		DE.DocumentEntityID
	FROM dbo.Document D
		JOIN dbo.DocumentEntity DE ON DE.DocumentID = D.DocumentID
			AND DE.EntityTypeCode = 'CommunityRound'
			AND DE.EntityID = @CommunityRoundID
			AND D.DocumentDescription IN ('MOU Document','TOR Document')
	ORDER BY D.DocumentDescription

	--Contacts
	SELECT
		C.ContactID,
		dbo.FormatContactNameByContactID(C.ContactID, 'LastFirstMiddle') AS FullName,
		C.Gender,
		C.Title,
		dbo.FormatDate(C.StartDate) AS StartDateFormatted,
		C.EmployerName,
		dbo.FormatDate(C.USVettingExpirationDate) AS USVettingDateFormatted,
		ISNULL(OACV12.VettingOutcomeID, 0) AS USVettingOutcomeID,
		OACV12.VettingOutcomeName AS USVettingOutcomeName,
		dbo.FormatDate(C.UKVettingExpirationDate) AS UKVettingDateFormatted,
		ISNULL(OACV22.VettingOutcomeID, 0) AS UKVettingOutcomeID,
		OACV22.VettingOutcomeName AS UKVettingOutcomeName,
		CCSWGC.ContactCSWGClassificationID,
		CCSWGC.ContactCSWGClassificationName
	FROM
		(
		SELECT
			D.ContactID,
			ISNULL(OACV11.ContactVettingID, 0) AS USContactVettingID,
			ISNULL(OACV21.ContactVettingID, 0) AS UKContactVettingID
		FROM	
			(
			SELECT 
				MAX(CV0.ContactVettingID) AS ContactVettingID,
				CV0.ContactID
			FROM dbo.ContactVetting CV0
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = CV0.ContactID
			AND CRC.CommunityRoundID = @CommunityRoundID
			GROUP BY CV0.ContactID

			UNION

			SELECT 
				0 AS ContactVettingID,
				C1.ContactID
			FROM dbo.Contact C1
				JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C1.ContactID
					AND CRC.CommunityRoundID = @CommunityRoundID
					AND C1.IsActive = 1
					AND NOT EXISTS
						(
						SELECT 1
						FROM dbo.ContactVetting CV1
						WHERE CV1.ContactID = C1.ContactID
						)
			) D
			OUTER APPLY
				(
				SELECT
					MAX(CV11.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV11
				WHERE CV11.ContactID = D.ContactID
					AND CV11.ContactVettingTypeID = 1
				) OACV11
			OUTER APPLY
				(
				SELECT
					MAX(CV21.ContactVettingID) AS ContactVettingID
				FROM dbo.ContactVetting CV21
				WHERE CV21.ContactID = D.ContactID
					AND CV21.ContactVettingTypeID = 2
				) OACV21
		) E
		OUTER APPLY
			(
			SELECT 
				CV12.VettingOutcomeID,
				VO1.VettingOutcomeName
			FROM dbo.ContactVetting CV12
			JOIN dropdown.VettingOutcome VO1 ON VO1.VettingOutcomeID = CV12.VettingOutcomeID
			WHERE CV12.ContactVettingID = E.USContactVettingID
			) OACV12
		OUTER APPLY
			(
			SELECT 
				CV22.VettingOutcomeID,
				VO2.VettingOutcomeName
			FROM dbo.ContactVetting CV22
			JOIN dropdown.VettingOutcome VO2 ON VO2.VettingOutcomeID = CV22.VettingOutcomeID
			WHERE CV22.ContactVettingID = E.UKContactVettingID
			) OACV22
		JOIN dbo.Contact C ON C.ContactID = E.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
	ORDER BY 3, 1

	--Updates
	SELECT
		CRU.CommunityRoundUpdateID, 
		CRU.CommunityRoundID, 
		CRU.FOUpdates, 
		CRU.POUpdates, 
		CRU.NextStepsUpdates, 
		CRU.NextStepsProcess, 
		CRU.Risks, 
		CRU.UpdateDate,
		dbo.FormatDate(CRU.UpdateDate) AS UpdateDateFormatted
	FROM dbo.CommunityRoundUpdate CRU
	WHERE CRU.CommunityRoundID = @CommunityRoundID
	ORDER BY CRU.UpdateDate
		
END
GO
--End procedure dbo.GetCommunityRoundByCommunityRoundID

--Begin procedure force.GetForceLocations
EXEC Utility.DropObject 'force.GetForceLocations'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:			Kevin Ross
-- Create date:	2016.00.28
-- Description:	A stored procedure to data about force locations
-- =============================================================
CREATE PROCEDURE force.GetForceLocations

@Boundary VARCHAR(MAX) = '',
@CommunityID INT = 0,
@ProvinceID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @BoundaryGeography GEOMETRY

	IF IsNull(@Boundary, '') != ''
	BEGIN
		SELECT @BoundaryGeography = GEOMETRY::STGeomFromText(@Boundary, 4326)
	END

	SELECT
		F.ForceID,
		F.ForceName,
		F.Location.STAsText() AS Location,
		AOT.AreaOfOperationTypeID,
		AOT.AreaOfOperationTypeName,
		AOT.HexColor
	FROM force.Force F
		JOIN dropdown.AreaOfOperationType AOT ON AOT.AreaOfOperationTypeID = F.AreaOfOperationTypeID
			AND 
				(
				IsNull(@Boundary, '') = ''
					OR @BoundaryGeography.STIntersects(F.Location) = 1
				)
			AND 
				(
				@CommunityID = 0
					OR EXISTS
					(
					SELECT 1
					FROM dbo.Community C
						JOIN force.ForceCommunity FC ON FC.CommunityID = C.CommunityID
							AND C.CommunityID = @CommunityID
							AND FC.ForceID = F.ForceID
					)
				)
			AND 
				(
				@ProvinceID = 0
					OR EXISTS
					(
					SELECT 1
					FROM dbo.Community C
						JOIN force.ForceCommunity FC ON FC.CommunityID = C.CommunityID
							AND C.ProvinceID = @ProvinceID
							AND FC.ForceID = F.ForceID
					)
					OR (F.TerritoryTypeCode = 'Province' AND F.TerritoryID = @ProvinceID)
				)

END
GO
--End procedure force.GetForceLocations

--Begin procedure dropdown.GetFieldOfficerHiredTypeData
EXEC utility.DropObject 'dropdown.GetFieldOfficerHiredTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dropdown.GetFieldOfficerHiredTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.FieldOfficerHiredTypeID, 
		T.FieldOfficerHiredTypeName
	FROM dropdown.FieldOfficerHiredType T
	WHERE (T.FieldOfficerHiredTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.FieldOfficerHiredTypeName, T.FieldOfficerHiredTypeID

END
GO
--End procedure dropdown.GetFieldOfficerHiredTypeData

--Begin procedure reporting.GetCommunityRoundList
EXEC utility.DropObject 'reporting.GetCommunityRoundList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:		Justin Branum
-- Create date:	2016.05.29
-- Description:	A stored procedure to get data from the dbo.CommunityRound table
-- =============================================================================

CREATE PROCEDURE reporting.GetCommunityRoundList

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.communityName,
		C.Population,
		CES.CommunityEngagementStatusName,
		dbo.FormatPersonNameByPersonID(CR.ActivitiesOfficerPersonID, 'LastFirstTitle') AS ActivitiesOfficerPersonName,
		CR.CommunityCode,
		CR.CommunityRoundID,
		CR.CommunityRoundRoundID,
		CR.FemaleQuestionnairCount,
		dbo.FormatContactNameByContactID(CR.FieldFinanceOfficerContactID, 'LastFirstTitle') AS FieldFinanceOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldLogisticOfficerContactID, 'LastFirstTitle') AS FieldLogisticOfficerPersonName,
		dbo.FormatContactNameByContactID(CR.FieldOfficerContactID, 'LastFirstTitle') AS FieldOfficer,
		CR.IsFieldOfficerHired,
		CR.IsFieldOfficerIntroToFSPandLAC AS IsFieldOfficerIntroToFSPandLA,
		CR.IsFieldOfficerIntroToPLO,
		CR.IsFieldOfficerTrainedOnProgramObjectives,
		CR.IsSCAAdjusted,
		CR.NewCommunityEngagementCluster,
		CR.PostAssessment,
		CR.QuestionnairCount,
		CR.SCAMeetingCount,
		CRCDC.CommunityRoundCivilDefenseCoverageName,
		CRES.CommunityRoundEngagementStatusName,
		CRG.CommunityRoundGroupName,
		CRJA.CommunityRoundJusticeActivityName,
		CROS1.CommunityRoundOutputStatusName AS CommunityRoundOutput11StatusNam,
		CROS2.CommunityRoundOutputStatusName AS CommunityRoundOutput12StatusName,
		CROS3.CommunityRoundOutputStatusName AS CommunityRoundOutput13StatusName,
		CRPAS.CommunityRoundPreAssessmentStatusName,
		CRR.CommunityRoundRoundName,
		CRRI.CommunityRoundRAPInfoName,
		CRT.CommunityRoundTamkeenName,
		CSG.CommunitySubGroupName,
		dbo.FormatDate(CR.CSAPFinalizedDate) AS CSAPFinalizedDateFormatted,
		dbo.FormatDate(CR.CSAPProcessStartDate) AS CSAPProcessStartDateFormatted,
		dbo.FormatDate(CR.FinalSWOTDate) AS FinalSWOTDateFormatted,
		dbo.FormatDate(CR.InitialSCADate) AS InitialSCADateFormatted,
		dbo.FormatDate(CR.SCAFinalizedDate) AS SCAFinalizedDateFormatted,
		dbo.FormatDate(CR.SensitizationDefineFSPRoleDate) AS SensitizationDefineFSPRoleDateFormatted,
		dbo.FormatDate(CR.SensitizationDiscussCommMakeupDate) AS SensitizationDiscussCommMakeupDateFormatted,
		dbo.FormatDate(CR.SensitizationMeetingMOUDate) AS SensitizationMeetingMOUDateFormatted,
		dbo.FormatDate(CR.ToRDate) AS ToRDateFormatted,
		dbo.FormatPersonNameByPersonID(CR.AreaManagerPersonID, 'LastFirstTitle') AS AreaManagerPersonName,
		dbo.FormatPersonNameByPersonID(CR.ProjectOfficerPersonID, 'LastFirstTitle') AS ProjectOfficerPersonName,
		ID.ImpactDecisionName,
		P.provinceName,
		PPC.PolicePresenceCategoryName,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
			AND C.IsActive = 1
		) AS CSWGCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Male' 
		) AS CSWGMaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.IsActive = 1
		AND C.Gender = 'Female' 
		) AS CSWGFemaleCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.USVettingExpirationDate > GetDate()
		) AS CSWGRAMActiveCount,
		(
		SELECT
		count(C.ContactID)
		FROM dbo.Contact C
		JOIN dbo.CommunityRoundContact CRC ON CRC.ContactID = C.ContactID
		JOIN dropdown.ContactCSWGClassification CCSWGC ON CCSWGC.ContactCSWGClassificationID = C.ContactCSWGClassificationID
			AND CRC.CommunityRoundID = CR.CommunityRoundID
		AND C.UKVettingExpirationDate  > GetDate()
		) AS CSWGUKActiveCount,
		dbo.FormatDate(
		(
		SELECT MAX(UpdateDate)
		FROM dbo.CommunityRoundUpdate 
		WHERE CommunityRoundID = CR.CommunityRoundID
		)) AS CurrentUpdateDateFormatted,
		(
		SELECT Top 1 
		CONCAT(FOUpdates, POUpdates, NextStepsUpdates,NextStepsProcess, Risks)
		FROM dbo.CommunityRoundUpdate
		WHERE UpdateDate = 
			(
			SELECT MAX(UpdateDate)
			FROM dbo.CommunityRoundUpdate 
			WHERE CommunityRoundID = CR.CommunityRoundID
			)
			AND CommunityRoundID = CR.CommunityRoundID
			) AS CurrentUpdateData,
			dbo.FormatDate(
			(
				SELECT MAX(UpdateDate)
				FROM dbo.CommunityRoundUpdate 
				WHERE CommunityRoundID = CR.CommunityRoundID
				AND UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
			)) AS LastWeekUpdateDateFormatted
			,(SELECT Top 1 
				CONCAT(FOUpdates, POUpdates, NextStepsUpdates,NextStepsProcess, Risks)
				FROM dbo.CommunityRoundUpdate
				WHERE UpdateDate <> (SELECT MAX(UpdateDate)
									FROM dbo.CommunityRoundUpdate 
									WHERE CommunityRoundID = CR.CommunityRoundID)
				AND CommunityRoundID = CR.CommunityRoundID
				) AS LastWeekUpdateData
	FROM dbo.CommunityRound CR
		JOIN reporting.SearchResult RSR ON RSR.EntityID = CR.communityroundID
				AND RSR.EntityTypeCode = 'CommunityRound'
				AND RSR.PersonID = @PersonID
		JOIN dbo.community C ON C.communityID = CR.communityID
		JOIN dbo.province P ON P.provinceID = C.provinceID
		JOIN dropdown.CommunitySubGroup CSG ON CSG.CommunitySubGroupID = C.CommunitySubGroupID
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
		JOIN dropdown.CommunityEngagementStatus CES ON CES.CommunityEngagementStatusID = C.CommunityEngagementStatusID
		JOIN dropdown.CommunityRoundRound CRR ON CRR.CommunityRoundRoundID = CR.CommunityRoundRoundID
		JOIN dropdown.CommunityRoundGroup CRG ON CRG.CommunityRoundGroupID = CR.CommunityRoundGroupID
		JOIN dropdown.CommunityRoundEngagementStatus CRES ON CRES.CommunityRoundEngagementStatusID = CR.CommunityRoundEngagementStatusID
		JOIN dropdown.PolicePresenceCategory PPC ON PPC.PolicePresenceCategoryID = C.PolicePresenceCategoryID
		JOIN dropdown.CommunityRoundRAPInfo CRRI ON CRRI.CommunityRoundRAPInfoID = CR.CommunityRoundRAPInfoID
		JOIN dropdown.CommunityRoundJusticeActivity CRJA ON CRJA.CommunityRoundJusticeActivityID = CR.CommunityRoundJusticeActivityID
		JOIN dropdown.CommunityRoundTamkeen CRT ON CRT.CommunityRoundTamkeenID = CR.CommunityRoundTamkeenID	
		JOIN dropdown.CommunityRoundCivilDefenseCoverage CRCDC ON CRCDC.CommunityRoundCivilDefenseCoverageID = CR.CommunityRoundCivilDefenseCoverageID
		JOIN dropdown.CommunityRoundOutputStatus CROS1 ON CROS1.CommunityRoundOutputStatusID = CR.CommunityRoundOutput11StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS2 ON CROS2.CommunityRoundOutputStatusID = CR.CommunityRoundOutput12StatusID
		JOIN dropdown.CommunityRoundOutputStatus CROS3 ON CROS3.CommunityRoundOutputStatusID = CR.CommunityRoundOutput13StatusID
		JOIN dropdown.CommunityRoundPreAssessmentStatus CRPAS ON CRPAS.CommunityRoundPreAssessmentStatusID = CR.CommunityRoundPreAssessmentStatusID

END
GO
--End procedure reporting.GetCommunityRoundList

--Begin procedure reporting.GetConceptNotes
EXEC Utility.DropObject 'reporting.GetConceptNotes'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2015.04.01
-- Description:	A stored procedure to get data for the concept notes report
-- ========================================================================
CREATE PROCEDURE reporting.GetConceptNotes

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CN.ConceptNoteID,
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,
		FS.FundingSourceName,
		CN.Title,
		CN.TaskCode,
		
		CASE (SELECT TOP 1  WorkflowName FROM workflow.EntityWorkflowStepGroupPerson WHERE EntityID = CN.ConceptNoteID AND EntityTypeCode = 'Conceptnote')
			WHEN 'US Activity Workflow'
			THEN
				CASE
					WHEN  CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
							WHEN CN.WorkflowStepNumber <7
							THEN 'Development'
							WHEN CN.WorkflowStepNumber = 7
							THEN 'Implementation'
							ELSE 'Closedown'
						END
				END
			ELSE
				CASE 
					WHEN CNS.ConceptNoteStatusCode IN ('OnHold','Cancelled','Closed') THEN CNS.ConceptNoteStatusName
					ELSE
						CASE
							WHEN CN.WorkflowStepNumber < 6
							THEN 'Development'
							WHEN CN.WorkflowStepNumber = 5
							THEN 'Submitted for Approval'
							WHEN CN.WorkflowStepNumber = 6
							THEN 'Implementation'
							ELSE 'Closedown'
						END
				END
		END AS Status,

		CN.Remarks,
		I.ImplementerName,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID1, 'LastFirst') AS FullName1,
		dbo.FormatPersonNameByPersonID(CN.PointOfContactPersonID2, 'LastFirst') AS FullName2,
		(
		SELECT TOP 1
			SC.SubContractorName
		FROM dbo.ConceptNoteTask CNT
			JOIN dbo.SubContractor SC ON SC.SubContractorID = CNT.SubContractorID
				AND CNT.ConceptNoteID = CN.ConceptNoteID
		) AS Partner,
		dbo.FormatDate(CN.StartDate) AS StartDateFormatted,
		dbo.FormatDate(CN.EndDate) AS EndDateFormatted,

		CASE 
			WHEN CN.TaskCode IN ('Cancelled','Development')	
			THEN FORMAT(0, 'C', 'en-us') 
			WHEN CN.TaskCode ='Closed'						
			THEN FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us')
			ELSE FORMAT((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmmendmentTotal,0) + isnull(OACNCE.ConceptNoteEquimentTotal, 0)), 'C', 'en-us') 
		END as totalBudget,
		
		CASE 
			WHEN CN.TaskCode IN ('Cancelled','Development')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT((IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0)) , 'C', 'en-us') 
		END as TotalSpent,
		
		CASE 
			WHEN CN.TaskCode IN ('Closed','Cancelled','OnHold','Development')
			THEN FORMAT(0, 'C', 'en-us') 
			ELSE FORMAT(((IsNull(OACNB.TotalBudget,0) + IsNull(OACNA.ConceptNoteAmmendmentTotal,0)+ isnull(OACNCE.ConceptNoteEquimentTotal, 0)) - (IsNull(CN.ActualTotalAmount,0) + IsNull(OACNF.ConceptNoteFinanceTotal,0))), 'C', 'en-us') 
		END AS TotalRemaining

	FROM dbo.ConceptNote CN
		JOIN dropdown.FundingSource FS ON FS.FundingSourceID = CN.FundingSourceID
		JOIN dropdown.ConceptNoteStatus CNS ON CNS.ConceptNoteStatusID = CN.ConceptNoteStatusID
		JOIN dropdown.Implementer I ON I.ImplementerID = CN.ImplementerID
		OUTER APPLY
			(
			SELECT
				SUM(CNB.Quantity * CNB.UnitCost * CNB.QuantityOfIssue) AS TotalBudget
			FROM dbo.ConceptNoteBudget CNB
			WHERE CNB.ConceptNoteID = CN.ConceptNoteID
			) OACNB
		OUTER APPLY
			(
			SELECT
				SUM(CNF.drAmt) - SUM(CNF.CrAmt) as ConceptNoteFinanceTotal
			FROM dbo.ConceptNoteFinance CNF
			WHERE CN.ConceptNoteFinanceTaskID = CNF.TaskID
			) OACNF
		OUTER APPLY
			(
			SELECT
				SUM(CNA.Cost) as ConceptNoteAmmendmentTotal
			FROM dbo.ConceptNoteAmmendment CNA
			WHERE CNA.ConceptNoteID = CN.ConceptNoteID
			) OACNA
		OUTER APPLY
			(
			SELECT
				SUM(CNCE.Quantity * EC.UnitCost * EC.QuantityOfIssue) as ConceptNoteEquimentTotal
			FROM dbo.ConceptNoteEquipmentCatalog CNCE
			JOIN procurement.EquipmentCatalog EC ON CNCE.EquipmentCatalogID = EC.EquipmentCatalogID
			AND CNCE.ConceptNoteID = CN.ConceptNoteID
			) OACNCE
	ORDER BY 2

END
GO
--End procedure reporting.GetConceptNotes
