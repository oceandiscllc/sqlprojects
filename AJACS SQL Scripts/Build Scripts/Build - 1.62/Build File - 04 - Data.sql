USE AJACS
GO

--Begin table dbo.MenuItem
EXEC utility.MenuItemAddUpdate @DeleteMenuItemCode='CommunityProvinceEngagementUpdate'
GO

EXEC utility.MenuItemAddUpdate 
	@NewMenuItemCode='CommunityRound', 
	@ParentMenuItemCode='Implementation', 
	@BeforeMenuItemCode='PoliceEngagementUpdate', 
	@NewMenuItemLink='/communityround/list',
	@NewMenuItemText='Community Rounds',
	@PermissionableLineageList='CommunityRound.List'
GO

EXEC utility.MenuItemAddUpdate 
	@NewMenuItemCode='CommunityProvinceEngagementReport', 
	@ParentMenuItemCode='LogicalFramework', 
	@AfterMenuItemCode='ProgramReport', 
	@PermissionableLineageList='CommunityProvinceEngagement.Export'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='LogicalFramework'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO
--Begin table dbo.MenuItem