USE AJACS
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.CommunityRound') AND SC.Name = 'ActivitiesOfficerContactID')
	EXEC sp_RENAME 'dbo.CommunityRound.ActivitiesOfficerContactID', 'ActivitiesOfficerPersonID', 'COLUMN';
--ENDIF
GO

--Begin table dbo.CommunityRound
EXEC utility.AddColumn 'dbo.CommunityRound', 'FieldOfficerHiredTypeID', 'INT'
EXEC utility.SetDefaultConstraint'dbo.CommunityRound', 'FieldOfficerHiredTypeID', 'INT', 0
GO
--End table dbo.CommunityRound

--Begin table dropdown.FieldOfficerHiredType
DECLARE @TableName VARCHAR(250) = 'dropdown.FieldOfficerHiredType'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.FieldOfficerHiredType
	(
	FieldOfficerHiredTypeID INT IDENTITY(0,1) NOT NULL,
	FieldOfficerHiredTypeName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', 1

EXEC utility.SetPrimaryKeyClustered @TableName, 'FieldOfficerHiredTypeID'
EXEC utility.SetIndexNonClustered 'IX_FieldOfficerHiredType', @TableName, 'DisplayOrder,FieldOfficerHiredTypeName', 'FieldOfficerHiredTypeID'
GO

SET IDENTITY_INSERT dropdown.FieldOfficerHiredType ON
GO

INSERT INTO dropdown.FieldOfficerHiredType (FieldOfficerHiredTypeID, FieldOfficerHiredTypeName) VALUES (0, NULL)
GO

SET IDENTITY_INSERT dropdown.FieldOfficerHiredType OFF
GO

INSERT INTO dropdown.FieldOfficerHiredType 
	(FieldOfficerHiredTypeName,DisplayOrder)
VALUES
	('Recruiting', 1),
	('Vetting', 2),
	('Complete', 3)
GO	
--End table dropdown.FieldOfficerHiredType
